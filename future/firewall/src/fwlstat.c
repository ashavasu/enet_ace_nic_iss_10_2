/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fwlstat.c,v 1.14 2014/03/16 11:41:01 siva Exp $
 * 
 * Description:This file contains functions for state table management
 * i.e. searching,creating, adding and deleting.
 ********************************************************************/
#include "fwlinc.h"
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif

/* Local ProtoType Declarations */
PRIVATE UINT4       FwlStateFormHashKey
PROTO ((tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo));

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlStatefulInit                                  */
/*                                                                          */
/*    Description        : Initialises the Data structures for Stateful     */
/*                         firewall module.                                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlStatefulInit (void)
{
    /* pStateTable is used to store the Stateful entries. 
     * This table is searched for a Stateful entry match. 
     * It uses the Protocol number, Src & Dest IP addresses, Src & Dest Ports 
     * to calculate the hash value. */

    /* 1. Init stateful table (hash table)
     * 2. Init Outbound and Inbound session list (SLL) */
    FWL_STATE_HASH_LIST = TMO_HASH_Create_Table (FWL_STATE_HASH_LIMIT, NULL,
                                                 FWL_ZERO);

    TMO_SLL_Init (FWL_OUT_SESSION_LIST);
    TMO_SLL_Init (FWL_IN_SESSION_LIST);

    FWL_STATE_V6_HASH_LIST =
        TMO_HASH_Create_Table (FWL_STATE_V6_HASH_LIMIT, NULL, FWL_ZERO);
    TMO_SLL_Init (FWL_OUT_V6_SESSION_LIST);
    TMO_SLL_Init (FWL_IN_V6_SESSION_LIST);
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlPktStatefulInspect                            */
/*                                                                          */
/*    Description        : Processes the incoming packet and calls the      */
/*                         respective protocol handler.                     */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateIcmpHdr-- Pointer to the Icmp Packet       */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         u4TotalHdrLen-- Total Header Length of Packet    */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                                                                          */
/*    Output(s)          : pu1LogTrigger-- The Log trigger value            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

UINT4
FwlPktStatefulInspect (tCRU_BUF_CHAIN_HEADER * pBuf,
                       tIpHeader * pStateIpHdr,
                       tIcmpInfo * pStateIcmpHdr,
                       tHLInfo * pStateHLInfo,
                       UINT4 u4TotalHdrLen,
                       UINT1 u1Direction, UINT1 *pu1LogTrigger)
{
    UINT4               u4Status = FWL_NOT_MATCH;

    if (pStateIpHdr->u1Proto == FWL_TCP)
    {
        /* TCP Handler */
        u4Status = FwlTCPPktStatefulInspect (pBuf,
                                             pStateIpHdr,
                                             pStateHLInfo,
                                             u4TotalHdrLen,
                                             u1Direction, pu1LogTrigger);
    }
    else if (pStateIpHdr->u1Proto == FWL_UDP)
    {
        /* UDP Handler */
        u4Status = FwlUDPPktStatefulInspect (pBuf,
                                             pStateIpHdr,
                                             pStateHLInfo,
                                             u1Direction, pu1LogTrigger);
    }
    else if ((pStateIpHdr->u1Proto == FWL_ICMP)
             || (pStateIpHdr->u1Proto == FWL_ICMPV6))
    {
        /* ICMP Handler */
        u4Status = FwlICMPPktStatefulInspect (pStateIpHdr,
                                              pStateHLInfo,
                                              pStateIcmpHdr,
                                              u1Direction, pu1LogTrigger);
    }
    return u4Status;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTCPPktStatefulInspect                         */
/*                                                                          */
/*    Description        : Processes the TCP packet, for all single         */
/*                         connection applications and only one multi       */
/*                         connection application application - FTP.        */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
UINT4
FwlTCPPktStatefulInspect (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo,
                          UINT4 u4Headlen, UINT1 u1Direction,
                          UINT1 *pu1LogTrigger)
{
    tStatefulSessionNode *pStateNode = NULL;
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    tInUserSessionNode *pInUserSessionNode = NULL;
    UINT4               u4DummyIfNum = FWL_ONE;
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;
    UINT1               u1TcpFlags = FWL_ZERO;
    UINT1               u1PktTcpHdrLen = FWL_ZERO;
    INT4                i4RetVal = OSIX_SUCCESS;
    tTMO_SLL           *pInitFlowLst = NULL;

    if (pStateIpHdr->u1IpVersion == FWL_IP_VERSION_4)
    {
        pInitFlowLst = &gFwlTcpInitFlowList;
    }
    else if (pStateIpHdr->u1IpVersion == FWL_IP_VERSION_6)
    {
        pInitFlowLst = &gFwlV6TcpInitFlowList;
    }
    else
    {
        return FWL_FAILURE;
    }
    /* Get the TCP flags Byte */
    FWL_GET_1_BYTE (pBuf,
                    (UINT4) (pStateIpHdr->u1HeadLen + FWL_TCP_CODE_BIT_OFFSET),
                    u1TcpFlags);
    u1TcpFlags = u1TcpFlags & FWL_OFFSET_3f;
    u1PktTcpHdrLen = (UINT1) FwlGetTransportHeaderLength (pBuf,
                                                          FWL_TCP,
                                                          pStateIpHdr->
                                                          u1HeadLen);

    u4CurTime = FWL_GET_SYS_TIME (&u4CurTime);
    /* If Traffic is HTTP and URL filtering is  enabled  with filters  
     * being configured Then  wait  for  the first  data packet */

    if ((u1TcpFlags == FWL_TCP_SYN_MASK) && ((u1Direction == FWL_DIRECTION_IN)
                                             ||
                                             (((pStateHLInfo->u2DestPort) ==
                                               FWL_HTTP)
                                              && (gFwlAclInfo.
                                                  u1UrlFilteringEnable ==
                                                  FWL_FILTERING_ENABLED)
                                              &&
                                              (TMO_SLL_Count
                                               (&FwlUrlFilterList)))))
    {
        /* Set the flow as control flow */
        FWL_FL_UTIL_UPDATE_FLOWTYPE (pBuf);
    }

    if (FwlMatchStateFilter (pStateIpHdr, pStateHLInfo, NULL, u1Direction,
                             pu1LogTrigger, &pStateNode) != FWL_MATCH)
    {
        /* If Entry NOT FOUND in stateful table, then 
         * search in tcp init flow table:
         *
         * 1. If not found in tcp init flow table:
         *    1.a  SYN only pkt without data,  return FWL_NEW_SESSION
         *    1.b  Not a SYN only pkt - drop/log pkt, return FWL_FAILURE
         * 
         * 2. If found: 
         *    2.a call FwlTcpInitStateHandler.
         *    2.b call FwlTcpUpdateInitFlowSeqAckWin
         *    2.c SYN set && ACK not set:
         *       -If(State==RELATED)Then Initialise pStateNode.Allow Pkt.
         */

        /* If entry not found in stateful table, then
         * search in tcp init flow table */
        if (FwlMatchTcpInitFlowNode (pStateIpHdr, pStateHLInfo,
                                     u1Direction, pu1LogTrigger,
                                     &pStateNode) != FWL_MATCH)
        {
            /* If Entry Match Not Found in InitFLowTable then, return FAILURE
             * if the current packet is a Non TCP SYN Only pkt. */
            if (u1TcpFlags != FWL_TCP_SYN_MASK)
            {                    /* This pkt is a Non Tcp SYN only Pkt */
                /* Log Message */
                FwlLogMessage (FWL_TCP_NON_SYN_ONLY_PKT, u4DummyIfNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_ATTACK);
                return FWL_FAILURE;
            }
            else                /* This pkt is a Tcp SYN only Pkt */
            {
                /* Check if this SYN only pkt is with data. */
                if ((pStateIpHdr->u2TotalLen -
                     (pStateIpHdr->u1HeadLen + u1PktTcpHdrLen)) != FWL_ZERO)
                {
                    /* A new session is allowed only for SYN only packet with 
                     * no data. */
                    /* Log Message for non-SYN packets */
                    FwlLogMessage (FWL_TCP_SYN_WITH_DATA, u4DummyIfNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_INVALID_TCP_PKT);
                    return FWL_FAILURE;
                }
            }

            /* As the current pkt is a TCP SYN Only pkt search in Partial Link 
             * Table for an Entry Match. If Match found process it Else treat 
             * it as NEW_SESSION. 
             */
            return (FwlProcessPartialLinks (pBuf, pStateIpHdr,
                                            pStateHLInfo,
                                            u4DummyIfNum,
                                            u1Direction,
                                            pStateNode,
                                            pOutUserSessionNode,
                                            pInUserSessionNode));

        }
        else                    /* Entry Match Found */
        {
            /* Verify the Current matched Init entry has timed-out. */
            if ((u4CurTime - pStateNode->u4Timestamp) >=
                (UINT4) (FWL_TCP_INIT_FLOW_TIMEOUT * FWL_SYS_TIME_FACTOR))
            {

                /* Init Entry has TIMED-OUT. */
                /* 1. Delete the Node Entry.
                 * 2. If (SYN only Pkt) Return FWL_NEW_SESSION to do Rule-Check.
                 *    Else Drop the Pkt. And Log it. */
                TMO_SLL_Delete (pInitFlowLst, (tTMO_SLL_NODE *) pStateNode);
                FwlTcpInitFlowMemFree ((UINT1 *) pStateNode);

                /* A new session is allowed only for SYN only 
                 * packet with no data */
                if (u1TcpFlags == FWL_TCP_SYN_MASK)
                {
                    if ((pStateIpHdr->u2TotalLen -
                         (pStateIpHdr->u1HeadLen + u1PktTcpHdrLen)) == FWL_ZERO)
                    {
                        /* This pkt is a Tcp SYN only Pkt */
                        /* PARTIAL_LINKS PROCESSING HERE. */
                        u4Status = FwlProcessPartialLinks (pBuf, pStateIpHdr,
                                                           pStateHLInfo,
                                                           u4DummyIfNum,
                                                           u1Direction,
                                                           pStateNode,
                                                           pOutUserSessionNode,
                                                           pInUserSessionNode);
                        return u4Status;
                    }
                    else
                    {
                        /* Log Message for non-SYN packets */
                        FwlLogMessage (FWL_TCP_SYN_WITH_DATA, u4DummyIfNum,
                                       pBuf, FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                       (UINT1 *) FWL_MSG_INVALID_TCP_PKT);
                        return FWL_FAILURE;
                    }
                }
                else            /* Invalid TCP Pkt - Not a SYN pkt  */
                {
                    /* Log Message for non-SYN packets */
                    FwlLogMessage (FWL_TCP_NON_SYN_ONLY_PKT, u4DummyIfNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    return FWL_FAILURE;
                }

            }                    /*DELETE the Current Matched Init entry if it has timed-out. */

            /* InitFlowNode Entry has NOT Timed-out */
            /* Perform Pkt processing against the Matched InitNode */

            if (pStateNode->u1LocalState == FWL_RELATED)
            {
                /* If (SYN only pkt) Then initialise
                 * pStateNode. Allow Pkt.
                 * Else Drop the pkt. Also Log it */
                if (((pStateIpHdr->u2TotalLen -
                      (pStateIpHdr->u1HeadLen + u1PktTcpHdrLen)) == FWL_ZERO) &&
                    (u1TcpFlags == FWL_TCP_SYN_MASK))
                {
                    /* This pkt is a Tcp SYN only Pkt */
                    /* Initialise pStateNode */
                    FwlInitialiseInitFlowNode (pBuf, pStateIpHdr, pStateNode,
                                               pStateHLInfo, u1Direction,
                                               u4CurTime);
                    return FWL_STATEFUL_ENTRY_MATCH;
                }                /*End-of-RELATED-Node-Initialsation */
                else            /* Received a Non SYN only pkt. */
                {
                    /* Received a Non SYN only pkt. */
                    /* Log Message */
                    FwlLogMessage (FWL_TCP_NON_SYN_ONLY_PKT, u4DummyIfNum,
                                   pBuf, *pu1LogTrigger, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);

                    return FWL_FAILURE;
                }
            }                    /*End-of-RELATED-State-Processing */

            /* 2.a If found, call FwlTcpInitStateHandler(). */
            if (FwlTcpInitStateHandler (pBuf, pStateNode, u1Direction,
                                        pStateIpHdr, pStateHLInfo)
                == FWL_FAILURE)
            {
                return (FWL_FAILURE);
            }
            else
            {
                /* 2.b Update the Session Node's TcpInfo fields. */
                FwlTcpUpdateInitFlowSeqAckWin (pBuf, pStateIpHdr, pStateHLInfo,
                                               pStateNode, u1Direction);

                /* Update the Timestamp */
                if (pStateNode->u1LocalState != FWL_TCP_STATE_CLOSED)
                {
                    pStateNode->u4Timestamp = u4CurTime;
                }

                /* Update the TCP States in Packet for the following conditions
                 * if (Traffic is HTTP and URL filtering is enabled 
                 * with filters being configured)
                 * {
                 * Then wait for the first data packet
                 * }
                 * else
                 * {
                 * update the state info. for configuring flows in NP
                 * }*/

                if ((pStateHLInfo->u2DestPort == FWL_HTTP) &&
                    (gFwlAclInfo.u1UrlFilteringEnable ==
                     FWL_FILTERING_ENABLED)
                    && (TMO_SLL_Count (&FwlUrlFilterList)))
                {
                    /* Set the flow as control flow */
                    FWL_FL_UTIL_UPDATE_FLOWTYPE (pBuf);
                }
                else
                {
                    FL_FWL_UPDATE_FLOWDATA_TCPSTATES (pBuf, pStateNode);
                }

                if ((pStateNode->u1LocalState == FWL_TCP_STATE_EST) ||
                    (pStateNode->u1RemoteState == FWL_TCP_STATE_EST))
                {
                    /* Set Both States to Established */
                    pStateNode->u1LocalState = pStateNode->u1RemoteState
                        = FWL_TCP_STATE_EST;
                    /* Verify if StateTable if FULL */
                    /* Do concurrent sessions check. If FULL drop Session */
                    if (FwlInspectConcurrentSessions (pBuf, pStateIpHdr,
                                                      pStateHLInfo,
                                                      u4DummyIfNum, u1Direction,
                                                      &pOutUserSessionNode,
                                                      &pInUserSessionNode)
                        == FWL_FAILURE)
                    {
                        /* Then delete the Node Entry in SLL */
                        TMO_SLL_Delete (pInitFlowLst,
                                        (tTMO_SLL_NODE *) pStateNode);
                        /* Release the Init Node Memory */
                        FwlTcpInitFlowMemFree ((UINT1 *) pStateNode);
                        /* Note: Logging already in above function call */
                        return (FWL_FAILURE);
                    }

                    if (FwlUpdateConcurrentSessionInfo (*pStateIpHdr,
                                                        *pStateHLInfo,
                                                        u1Direction,
                                                        pOutUserSessionNode,
                                                        pInUserSessionNode)
                        == FWL_FAILURE)
                    {
                        /* This happens, when the system no longer has enough
                         * memory. So delete the stateful node for this session.
                         */

                        /* Then delete the Node Entry in SLL */
                        TMO_SLL_Delete (pInitFlowLst,
                                        (tTMO_SLL_NODE *) pStateNode);
                        /* Release the Init Node Memory */
                        FwlTcpInitFlowMemFree ((UINT1 *) pStateNode);

                        /* Log the error message as resource unavailable */
                        FwlLogMessage (FWL_MEMORY_FAILURE, u4DummyIfNum, pBuf,
                                       *pu1LogTrigger, FWLLOG_CRITICAL_LEVEL,
                                       NULL);
                        return (FWL_FAILURE);
                    }            /*End-of-FwlUpdateConcurrentSessionInfo */

                    /* Move the InitFlow SLL Node to StateTable. */
                    /* Then delete the Node Entry */
                    TMO_SLL_Delete (pInitFlowLst, (tTMO_SLL_NODE *) pStateNode);

                    if (FwlAddStateFilter (pStateNode, u1Direction)
                        == FWL_FAILURE)
                    {
                        /* Release the Init Node Memory */
                        FwlTcpInitFlowMemFree ((UINT1 *) pStateNode);
                        /* Dropping the Session. And current pkt. 
                         * Log Message */
                        FwlLogMessage (FWL_MEMORY_FAILURE, u4DummyIfNum,
                                       pBuf, *pu1LogTrigger,
                                       FWLLOG_CRITICAL_LEVEL, NULL);

                        return FWL_FAILURE;
                    }

                    if (FwlFTPdynSessionHandler (pBuf, pStateIpHdr,
                                                 pStateHLInfo,
                                                 u4Headlen, u1Direction,
                                                 pu1LogTrigger) == FWL_FAILURE)
                    {
                        /* Log Message */
                        FwlLogMessage (FWL_MEMORY_FAILURE, u4DummyIfNum,
                                       pBuf, *pu1LogTrigger,
                                       FWLLOG_CRITICAL_LEVEL, NULL);

                        return (FWL_FAILURE);
                    }
                    /* Release the Init Node Memory */
                    FwlTcpInitFlowMemFree ((UINT1 *) pStateNode);
                    return (FWL_STATEFUL_ENTRY_MATCH);
                }
                else
                {
                    return (FWL_STATEFUL_ENTRY_MATCH);
                }                /*End-of-Call-to-FTPdynamicSessionHandler */
            }
        }                        /*End-of-InitFlowMatch-Processing */
    }                            /*End-of-FwlMatchStateFilter-Failure-Processing */

    /* Drop pkt and return FWL_FAILURE :
     *  1. If SYN/SYN-ACK bits set (Post connection attack). 
     *  2. If TCP header is not valid (invalid seq, ack, window).
     *  3. If the FwlTcpEstStateHandler() detects pkt is invalid.
     *  
     * Else return FWL_STATEFUL_ENTRY_MATCH to Allow pkt.
     */

    if (((pStateHLInfo->u1Fin) || (pStateHLInfo->u1Rst)) &&
        ((pStateNode->u1LocalState == FWL_TCP_STATE_EST) &&
         (pStateNode->u1RemoteState == FWL_TCP_STATE_EST)))
    {
        /* Query the NP to get the TCP states, if valid states are returned
         * then update the 'pStateNode' otherwise do nothing */
        FWL_FL_TASKLOCK ();
        if (pStateHLInfo->u1Fin)
        {
#ifdef FLOWMGR_WANTED
            i4RetVal =
                FWL_UTIL_UPD_STATEFUL_TCPSTATES (pStateNode->LocalIP.v4Addr,
                                                 pStateNode->RemoteIP.v4Addr,
                                                 FWL_LOCAL_PORT
                                                 (pStateNode, FWL_TCP),
                                                 FWL_REMOTE_PORT
                                                 (pStateNode, FWL_TCP),
                                                 FWL_TCP, pStateNode);
#else
            FWL_UTIL_UPD_STATEFUL_TCPSTATES (pStateNode->LocalIP.v4Addr,
                                             pStateNode->RemoteIP.v4Addr,
                                             FWL_LOCAL_PORT (pStateNode,
                                                             FWL_TCP),
                                             FWL_REMOTE_PORT (pStateNode,
                                                              FWL_TCP), FWL_TCP,
                                             pStateNode);
#endif
            /*
             * If NP Does not support fetching Ack/Seq/Win of Tcp Session.
             * Use the values Ack/Seq/Win From Recieved Packet.
             */
#ifdef FLOWMGR_WANTED
            if (i4RetVal == OSIX_ERR_OS_DOES_NOT_SUPP)
            {
                if (u1Direction == FWL_DIRECTION_IN)
                {
                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->u4EndSeqNum =
         pStateHLInfo->u4SeqNum;

                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->u4MaxAckNum =
         pStateHLInfo->u4SeqNum + pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        u4EndSeqNum = pStateHLInfo->u4AckNum;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        u4MaxAckNum =
                        pStateHLInfo->u4AckNum +
                        pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        u4MaxWindow = pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->u4MaxWindow =
         pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->
         i1WindowScaling = FWL_ZERO;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        i1WindowScaling = FWL_ZERO;
                }
                else
                {
                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->u4EndSeqNum =
         pStateHLInfo->u4AckNum;

                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->u4MaxAckNum =
         pStateHLInfo->u4AckNum + pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        u4EndSeqNum = pStateHLInfo->u4SeqNum;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        u4MaxAckNum =
                        pStateHLInfo->u4SeqNum +
                        pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        u4MaxWindow = pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->u4MaxWindow =
         pStateHLInfo->u2WindowAdvertised;

                    ((tTcpInfo *) &
                     (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo))->
         i1WindowScaling = FWL_ZERO;

                    ((tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo))->
                        i1WindowScaling = FWL_ZERO;
                }
            }
#endif /* FLOWMGR_WANTED */
        }
        FWL_FL_TASKUNLOCK ();
    }

    if ((pStateHLInfo->u1Fin == FWL_ZERO) &&
        (i4RetVal != OSIX_ERR_OS_DOES_NOT_SUPP))
    {
        if (FwlTcpValidateTcpSeqAck (pBuf, pStateIpHdr, pStateHLInfo,
                                     pStateNode, u1Direction,
                                     FWL_VALIDATE_SEQ_ACK) == FWL_FAILURE)
        {
            /* Log Message */
            FwlLogMessage (FWL_TCP_INVALID_SEQ_ACK, u4DummyIfNum, pBuf,
                           *pu1LogTrigger, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }
    }

    /* 3. If found, call FwlTcpEstStateHandler(). */
    if (FwlTcpEstStateHandler (pBuf, pStateNode, u1Direction,
                               pStateIpHdr, pStateHLInfo) == FWL_FAILURE)
    {
        return (FWL_FAILURE);
    }
    else
    {
        /* Update the Seq,Ack,Window values of the session from the current pkt. */
        /* NOTE: Here we ignore the return value, as the call is only to 
         * update fields. */
        FwlTcpValidateTcpSeqAck (pBuf, pStateIpHdr, pStateHLInfo,
                                 pStateNode, u1Direction, FWL_UPDATE_SEQ_ACK);

        /* Update the Timestamp */
        if (pStateNode->u1LocalState != FWL_TCP_STATE_CLOSED)
        {
            pStateNode->u4Timestamp = u4CurTime;
        }

        /* FTP dynamic connection handler. */
        if (FwlFTPdynSessionHandler (pBuf, pStateIpHdr, pStateHLInfo,
                                     u4Headlen, u1Direction,
                                     pu1LogTrigger) == FWL_FAILURE)
        {
            /* Log Message */
            FwlLogMessage (FWL_MEMORY_FAILURE, u4DummyIfNum,
                           pBuf, *pu1LogTrigger, FWLLOG_CRITICAL_LEVEL, NULL);

            return (FWL_FAILURE);
        }
        else
        {
            /* Update the TCP States in Packet */
            FL_FWL_UPDATE_FLOWDATA_TCPSTATES (pBuf, pStateNode);
            return (FWL_STATEFUL_ENTRY_MATCH);
        }
    }
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInitialiseInitFlowNode                        */
/*                                                                          */
/*    Description        : Initialises the InitFlow entry found to be in    */
/*                         RELATED state.                                   */
/*                                                                          */
/*    Input(s)           : pStateNode   -- Pointer to InitFlow Node         */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
FwlInitialiseInitFlowNode (tCRU_BUF_CHAIN_HEADER * pBuf,
                           tIpHeader * pStateIpHdr,
                           tStatefulSessionNode * pStateNode,
                           tHLInfo * pStateHLInfo, UINT1 u1Direction,
                           UINT4 u4CurTime)
{
    tTcpInfo           *pSendrInfo = NULL;
    tTcpInfo           *pRcvrInfo = NULL;
    UINT1               u1PktTcpHdrLen = FWL_ZERO;
    INT1                i1WinScale = FWL_ZERO;

    u1PktTcpHdrLen = (UINT1) FwlGetTransportHeaderLength (pBuf,
                                                          FWL_TCP,
                                                          pStateIpHdr->
                                                          u1HeadLen);

    UNUSED_PARAM (u1PktTcpHdrLen);
    i1WinScale = FwlTcpExtractWindowScalingOption (pBuf, pStateIpHdr);

    /* Initialise pStateNode */
    if (u1Direction == FWL_DIRECTION_OUT)
    {
        pStateNode->u1LocalState = FWL_TCP_STATE_SYN_SENT;
        pStateNode->u1RemoteState = FWL_TCP_STATE_LISTEN;
    }
    else
    {
        pStateNode->u1LocalState = FWL_TCP_STATE_LISTEN;
        pStateNode->u1RemoteState = FWL_TCP_STATE_SYN_SENT;
    }

    if (u1Direction == FWL_DIRECTION_IN)
    {                            /* Pkt from WAN(Remote/Sender) to LAN(Local/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
    }
    else                        /* (u1Direction == FWL_DIRECTION_OUT) */
    {                            /* Pkt from LAN(Local/Sender) to WAN(Remote/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
    }

    /* Perform the initialisation */
    pSendrInfo->u4EndSeqNum = (pStateHLInfo->u4SeqNum) + FWL_ONE;
    pSendrInfo->u4MaxAckNum = (pStateHLInfo->u4SeqNum) + FWL_ONE;
    pSendrInfo->u4MaxWindow =
        (UINT4) FWL_MAX (pStateHLInfo->u2WindowAdvertised, FWL_ONE);
    if (i1WinScale != TCP_NO_WINDOW_SCALING)
    {
        pSendrInfo->i1WindowScaling = i1WinScale;
    }
    else
    {
        pSendrInfo->i1WindowScaling = FWL_ZERO;
    }
    pRcvrInfo->u4EndSeqNum = FWL_ZERO;
    pRcvrInfo->u4MaxAckNum = FWL_ZERO;
    pRcvrInfo->u4MaxWindow = FWL_ONE;
    pRcvrInfo->i1WindowScaling = FWL_ZERO;

    pSendrInfo->u2Port = pStateHLInfo->u2SrcPort;
    pStateNode->u4Timestamp = u4CurTime;
    pStateNode->u1Direction = u1Direction;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlFTPdynSessionHandler                          */
/*                                                                          */
/*    Description        : This routine makes a dynamic entry into InitFlow */
/*                         Table, if given pkt is a FTP pkt with PORT or    */
/*                         PASV cmd. Else it returns success.               */
/*                                                                          */
/*    Input(s)           : pBuf          -- Pointer to the packet           */
/*                         pStateIpHdr   -- Pointer to the Ip hdr           */
/*                         pStateHLInfo  -- Pointer to the Transport Hdr    */
/*                         u4Headlen     -- Specifies the length of IpHdr   */
/*                         u1Direction   -- Specifies IN or OUT             */
/*                         pu1LogTrigger -- Specifies Log Level             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if dynamic node added, otherwise     */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
UINT4
FwlFTPdynSessionHandler (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tIpHeader * pStateIpHdr,
                         tHLInfo * pStateHLInfo, UINT4 u4Headlen,
                         UINT1 u1Direction, UINT1 *pu1LogTrigger)
{
    INT1                ai1Cmd[FWL_FTP_PORT_LEN] = { FWL_ZERO };
    INT1                ai1IpAddr[FWL_IPADDR_LEN_MAX] = { FWL_ZERO };
    INT1                ai1Port[FWL_PORT_LEN_MAX] = { FWL_ZERO };
    INT1               *pai1IpAddr = ai1IpAddr;
    INT1               *pai1Port = ai1Port;
    UINT1               u1Cmd = FWL_ZERO;    /* ftp port/pasv command */
    UINT4               u4Offset = FWL_ZERO;
    /* ip address string offset in port/pasv cmd */
    tFwlGlobalInfo      PayloadData;
    tHLInfo             NewHLInfo;
    tStatefulSessionNode *pInitFlowNode = NULL;

    /* Initialise NewHLInfo */
    MEMSET (&NewHLInfo, FWL_ZERO, sizeof (tHLInfo));

    MEMSET (&PayloadData, FWL_ZERO, sizeof (tFwlGlobalInfo));
    /* Check whether it is a FTP packet standard or non-standard port 
     * and if so, check whether it is a PORT command */
    if ((pStateHLInfo->u2DestPort == FWL_FTP_CTRL_PORT) ||
        (pStateHLInfo->u2DestPort == gu4NonStdFtpPort))
    {
        FWL_FL_UTIL_UPDATE_FLOWTYPE (pBuf);
        /* Look for PORT Command */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1Cmd, u4Headlen,
                                   FWL_FTP_PORT_LEN);
        ai1Cmd[FWL_INDEX_4] = '\0';
        if ((FWL_STRCMP (ai1Cmd, "PORT")) == FWL_STRCMP_SUCCESS)
        {
            FWL_DBG (FWL_DBG_INSPECT, "\n PORT command met \n");
            u1Cmd = FWL_FTP_PORT;
            u4Offset = u4Headlen + FWL_FIVE;

            FwlFtpGetPayloadIpPort (pBuf, pai1IpAddr, pai1Port,
                                    u4Offset, u1Cmd, &PayloadData);
        }
        else if ((FWL_STRCMP (ai1Cmd, "EPRT")) == FWL_STRCMP_SUCCESS)
        {
            FWL_DBG (FWL_DBG_INSPECT, "\n EPRT command met \n");

            u1Cmd = FWL_FTP_EPRT;
            /* Moving the offset to the end of FTP command 'EPRT ' */
            u4Offset = u4Headlen + FWL_FIVE;
            FwlFtpGetIpPortFromExtdPayload (pBuf, pai1IpAddr, pai1Port,
                                            u4Offset, u1Cmd, &PayloadData);
        }
        else
        {
            return FWL_SUCCESS;
        }
    }
    else if (pStateHLInfo->u2SrcPort == FWL_FTP_CTRL_PORT)
    {
        FWL_FL_UTIL_UPDATE_FLOWTYPE (pBuf);
        /* Look for PASV Command */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1Cmd, u4Headlen,
                                   FWL_FTP_PORT_LEN);
        ai1Cmd[FWL_INDEX_3] = '\0';
        /* 227 is the FTP code when the FTP server enters passive mode */
        if ((FWL_STRCMP (ai1Cmd, "227")) == FWL_STRCMP_SUCCESS)
        {
            FWL_DBG (FWL_DBG_INSPECT, "\n PASV command met \n");
            u1Cmd = FWL_FTP_PASV;
            u4Offset = u4Headlen;

            FwlFtpGetPayloadIpPort (pBuf, pai1IpAddr, pai1Port,
                                    u4Offset, u1Cmd, &PayloadData);
        }
        else if ((FWL_STRCMP (ai1Cmd, EPSV_REPLY_CODE)) == FWL_STRCMP_SUCCESS)
        {
            FWL_DBG (FWL_DBG_INSPECT, "\n EPSV command met \n");
            u1Cmd = FWL_FTP_EPSV;
            u4Offset = u4Headlen;
            FwlFtpGetIpPortFromExtdPayload (pBuf, pai1IpAddr, pai1Port,
                                            u4Offset, u1Cmd, &PayloadData);
        }
        else
        {
            return FWL_SUCCESS;
        }
    }

    else
    {
        return FWL_SUCCESS;
    }

    if (PayloadData.u2TranslatedLocPort == FWL_ZERO)
    {
        /* Failed to retrieve proper Port no */
        FWL_DBG (FWL_DBG_INSPECT, "\nFailed to retrieve proper Port"
                 "number from FTP command\n");
        return (FWL_FAILURE);
    }
    NewHLInfo.u2SrcPort = PayloadData.u2TranslatedLocPort;
    NewHLInfo.u2DestPort = FWL_ZERO;    /* Destination port is ephemeral */

    /* If this entry is already present in the TcpInitFlow table,
     * then return without adding again. */
    if (FwlMatchTcpInitFlowNode (pStateIpHdr, &NewHLInfo,
                                 u1Direction, pu1LogTrigger,
                                 &pInitFlowNode) == FWL_MATCH)
    {
        return FWL_SUCCESS;
    }

    /* PASV Command is present. Add a new filter with RELATED State */
    if (FwlAddTcpInitFlowNode (pBuf, pStateIpHdr,
                               &NewHLInfo, FWL_RELATED,
                               u1Direction, *pu1LogTrigger, FWL_ZERO)
        == FWL_SUCCESS)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return (FWL_FAILURE);
    }
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUDPPktStatefulInspect                         */
/*                                                                          */
/*    Description        : Processes the UDP packet.                        */
/*                                                                          */
/*    Input(s)           : pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
UINT4
FwlUDPPktStatefulInspect (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo,
                          UINT1 u1Direction, UINT1 *pu1LogTrigger)
{
    tStatefulSessionNode *pStateNode = NULL;
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4DummyIfNum = FWL_ONE;
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    tInUserSessionNode *pInUserSessionNode = NULL;

    /* Call FwlMatchStateFilter().  
     * 1. If (entry not found in stateful table), then
     *        return FWL_NEW_SESSION.
     * 2. If (found) then 
     *        2.a Update Node's Timestamp value with current_time.
     *        2.b If Node's State is NEW then change it to EST.  
     *        2.c Log/Allow and return FWL_SUCCESS.  
     */

    if (FwlMatchStateFilter (pStateIpHdr, pStateHLInfo, NULL, u1Direction,
                             pu1LogTrigger, &pStateNode) == FWL_MATCH)
    {
        /* Update the Timestamp */
        if (pStateNode->u4Timestamp != FWL_ZERO)
        {
            pStateNode->u4Timestamp = FWL_GET_SYS_TIME (&u4CurTime);
        }

        if (pStateNode->u1LocalState == FWL_NEW)
        {
            pStateNode->u1LocalState = FWL_EST;
        }

        /* TftpAlgProcessing to be done only for IPv6 traffic, for IPv4
         * irrespective of NAT enabled/disabled (only run time) partial links
         * will be added from nat alg */
        if ((pStateIpHdr->u1IpVersion == FWL_IP_VERSION_6) &&
            (pStateIpHdr->u1Proto == FWL_UDP) &&
            (pStateHLInfo->u2DestPort == FWL_TFTP_PORT))
        {
            TftpAlgProcessing (pStateIpHdr, pStateHLInfo, u1Direction);
        }

        return (FWL_STATEFUL_ENTRY_MATCH);
    }
    else
    {
        /* PARTIAL_LINKS PROCESSING HERE. */
        return FwlProcessPartialLinks (pBuf, pStateIpHdr,
                                       pStateHLInfo,
                                       u4DummyIfNum,
                                       u1Direction,
                                       pStateNode,
                                       pOutUserSessionNode, pInUserSessionNode);
    }

}                                /* FwlUDPPktStatefulInspect() */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlICMPPktStatefulInspect                        */
/*                                                                          */
/*    Description        : Processes the ICMP packet, for both IN and OUT   */
/*                         directions.                                      */
/*                                                                          */
/*    Input(s)           : pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         pStateIcmpHdr-- Pointer to the Icmp Packet       */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
UINT4
FwlICMPPktStatefulInspect (tIpHeader * pStateIpHdr,
                           tHLInfo * pStateHLInfo,
                           tIcmpInfo * pStateIcmpHdr,
                           UINT1 u1Direction, UINT1 *pu1LogTrigger)
{
    tStatefulSessionNode *pStateNode = NULL;
    UINT4               u4CurTime = FWL_ZERO;

    /* Call FwlMatchStateFilter().
     * 1. If (entry not found in stateful table), then
     *        return FWL_NEW_SESSION.
     * 2. If (found) then
     *        2.a call Validate_ICMP_Packet .   
     *        2.b Update Node's Timestamp value with current_time.
     *        2.c Log/Allow and return FWL_SUCCESS.
     */

    if (FwlMatchStateFilter (pStateIpHdr, pStateHLInfo, pStateIcmpHdr,
                             u1Direction, pu1LogTrigger,
                             &pStateNode) == FWL_MATCH)
    {
        /* Update the Timestamp */
        if (pStateNode->u4Timestamp != FWL_ZERO)
        {
            pStateNode->u4Timestamp = FWL_GET_SYS_TIME (&u4CurTime);
        }
        return FWL_STATEFUL_ENTRY_MATCH;
    }
    else
    {
        return FWL_NEW_SESSION;
    }
}

/* Utility functions .... */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetTransportHeaderLength                      */
/*                                                                          */
/*    Description        : Gets the header length of the Transport layer    */
/*                         (TCP/UDP).                                       */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         u1PktType    -- Whether it is TCP/UDP            */
/*                         u1Headlen    -- Ip Header Length                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u1Length     -- Length of the Transport layer    */
/*                                         header.                          */
/****************************************************************************/
PUBLIC UINT4
FwlGetTransportHeaderLength (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType,
                             UINT1 u1Headlen)
{
    UINT4               u4HeaderLenOffset = FWL_ZERO;
    UINT1               u1Length = FWL_ZERO;

    u1Length = FWL_ZERO;
    u4HeaderLenOffset = FWL_ZERO;

    if (FWL_TCP == u1PktType)
    {
        u4HeaderLenOffset = (UINT4) (FWL_TCP_LENGTH_OFFSET + u1Headlen);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Length,
                                   u4HeaderLenOffset, FWL_HEADER_LEN);
        u1Length = (UINT1) ((u1Length >> FWL_FOUR) & FWL_HEADER_LEN_MASK);
        u1Length = (UINT1) (u1Length * FWL_FOUR);
        return u1Length;
    }
    else if (FWL_UDP == u1PktType)
    {
        UINT2               u2UdpLen = FWL_ZERO;
        /* Get the length in the UDP header */
        FWL_GET_2_BYTE (pBuf, (UINT4) (FWL_UDP_LENGTH_OFFSET + u1Headlen),
                        u2UdpLen);
        return (u2UdpLen);
    }

    return u1Length;

}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddStateFilter                                */
/*                                                                          */
/*    Description        : Adds the filter to the Stateful Filter Hash List */
/*                                                                          */
/*    Input(s)           : pTempNode  -- Pointer with info for New node to  */
/*                                       be added to the Hash List.         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS / FWL_FAILURE                        */
/****************************************************************************/

UINT4
FwlAddStateFilter (tStatefulSessionNode * pTempNode, UINT1 u1Direction)
{
    tStatefulSessionNode *pStateInfo = NULL;
    tTMO_HASH_TABLE    *pHashTab = NULL;
    UINT4               u4HashKey = FWL_ZERO;
    tIpHeader           IpHdr;
    tHLInfo             HLInfo;

    UNUSED_PARAM (u1Direction);
    if (pTempNode == NULL)
    {
        return FWL_FAILURE;
    }
    MEMSET (&IpHdr, FWL_ZERO, sizeof (tIpHeader));
    MEMSET (&HLInfo, FWL_ZERO, sizeof (tHLInfo));

    /* State Filter node Allocation */
    /* Check the return status and return if fails */
    if (FwlStateMemAllocate ((UINT1 **) (VOID *) &pStateInfo) != FWL_SUCCESS)
    {
        return FWL_FAILURE;
    }

    TMO_HASH_Init_Node ((tTMO_HASH_NODE *) pStateInfo);
    /* Initialise params for Hash key Generation */
    FWL_IP_ADDR_COPY (&IpHdr.SrcAddr, &pTempNode->LocalIP);
    FWL_IP_ADDR_COPY (&IpHdr.DestAddr, &pTempNode->RemoteIP);

    IpHdr.u1Proto = pTempNode->u1Protocol;

    if ((pTempNode->u1Protocol != FWL_ICMP) &&
        (pTempNode->u1Protocol != FWL_ICMPV6))
    {
        HLInfo.u2SrcPort = (UINT2) FWL_LOCAL_PORT (pTempNode, IpHdr.u1Proto);
        HLInfo.u2DestPort = (UINT2) FWL_REMOTE_PORT (pTempNode, IpHdr.u1Proto);
    }

    /* Generate the Hash Key */
    u4HashKey = FwlStateFormHashKey (&IpHdr, &HLInfo);

    /* Copy the passed Node contents into New node */
    FWL_MEMCPY (pStateInfo, pTempNode, sizeof (tStatefulSessionNode));

    if (pTempNode->LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pHashTab = FWL_STATE_HASH_LIST;
    }
    else if (pTempNode->LocalIP.u4AddrType == FWL_IP_VERSION_6)
    {
        pHashTab = FWL_STATE_V6_HASH_LIST;
    }
    else
    {
        FwlStateMemFree ((UINT1 *) pStateInfo);
        return FWL_FAILURE;
    }
    /* Add the Node in the StateFilterHashList Hash Table */
    TMO_HASH_Add_Node (pHashTab,
                       (tTMO_HASH_NODE *) pStateInfo, u4HashKey, NULL);
    return FWL_SUCCESS;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInitStateFilterNode                           */
/*                                                                          */
/*    Description        : Initialises a New State Node to be added.        */
/*                                                                          */
/*    Input(s)           : pStateIpHdr   -- Pointer to the Ip hdr           */
/*                         pStateHLInfo  -- Pointer to the Transport Hdr    */
/*                         pStateIcmpHdr -- Pointer to the Icmp Hdr         */
/*                         u1Direction   -- State of the Filter             */
/*                         u1LogLevel    -- Log Level of the filter         */
/*                         pInitNode     -- Node to be initialsed           */
/*                                                                          */
/*    Output(s)          : pInitNode     -- Node to be initialsed           */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
FwlInitStateFilterNode (tIpHeader * pIpHdr,
                        tHLInfo * pHLInfo,
                        tIcmpInfo * pIcmpHdr,
                        UINT1 u1Direction,
                        UINT1 u1LogLevel, tStatefulSessionNode * pInitNode)
{
    UINT4               u4CurTime = FWL_ZERO;
    UINT1               u1Proto = FWL_ZERO;
    tHLInfo             HLInfo;
    tIpHeader           IpHdr;

    /* Copy the passed Info into Temp variables */
    FWL_MEMCPY (&HLInfo, pHLInfo, sizeof (tHLInfo));
    FWL_MEMCPY (&IpHdr, pIpHdr, sizeof (tIpHeader));

    /* Init local variable */
    u1Proto = IpHdr.u1Proto;

    /* Based on Direction set the Src,Dest Addr and Port
     * variables to represent Local and Remote Hosts. */
    if (u1Direction == FWL_DIRECTION_IN)
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            FWL_SWAP_IPV4_ADDR (IpHdr.SrcAddr.v4Addr, IpHdr.DestAddr.v4Addr);
        }
        else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
        {
            FWL_SWAP_IPV6_ADDR (IpHdr.SrcAddr.v6Addr.u4_addr,
                                IpHdr.DestAddr.v6Addr.u4_addr);
        }

        if (u1Proto == FWL_UDP)
        {
            FWL_SWAP_PORT (HLInfo.u2SrcPort, HLInfo.u2DestPort);
        }
    }                            /*End-of-IF-Block */

    /* Init the Node */
    MEMSET (pInitNode, FWL_ZERO, sizeof (tStatefulSessionNode));

    /* Update the Node information */
    FWL_IP_ADDR_COPY (&pInitNode->LocalIP, &IpHdr.SrcAddr);
    FWL_IP_ADDR_COPY (&pInitNode->RemoteIP, &IpHdr.DestAddr);
    pInitNode->u1Protocol = u1Proto;
    pInitNode->u4Timestamp = FWL_GET_SYS_TIME (&u4CurTime);
    pInitNode->u1LogLevel = u1LogLevel;
    pInitNode->u1Direction = u1Direction;

    if (u1Proto == FWL_UDP)
    {
        /* Initialise the UDP info fields */
        FWL_LOCAL_UDP_PORT (pInitNode) = HLInfo.u2SrcPort;
        FWL_REMOTE_UDP_PORT (pInitNode) = HLInfo.u2DestPort;
        pInitNode->u1LocalState = FWL_NEW;
        pInitNode->u1RemoteState = FWL_NEW;
    }
    else                        /* (u1Proto == FWL_ICMP) */
    {
        /* Initialise the ICMP info fields */
        FWL_ICMP_IP_ID (pInitNode) = pIcmpHdr->u2IcmpSeqNum;
        pInitNode->u1LocalState = FWL_NEW;
        pInitNode->u1RemoteState = FWL_NEW;
    }

}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGarbageCollect                                */
/*                                                                          */
/*    Description        : Deletes the filter from the Stateful Filter List */
/*                         based on timeout.                                */
/*                                                                          */
/*                Case 1 : If u4Flush == FWL_FLUSH_STATEFUL_ENTRIES, then   */
/*                         delete all entreis in the state table and        */
/*                         session table.                                   */
/*                                                                          */
/*                Case 2 : If the node is TCP and its state is closed, then */
/*                         its timestamp is marked as stale (0) and this    */
/*                         node should be deleted.                          */
/*                Case 3 : If the node has timed out, then thi should be    */
/*                         deleted.                                         */
/*                                                                          */
/*                Case 4 : If u4CommitRules == FWL_COMMIT, then all the     */
/*                         nodes in the stateful table will be checked      */
/*                         against each rule in the ACL table, starting     */
/*                         with highest priority. There are two cases:      */
/*                                                                          */
/*                     4a: If the rule has been modified to DENY or         */
/*                         if there is no rule which matches the state      */
/*                         entry, then the state entry is deleted and       */
/*                         session count is updated.                        */
/*                     4b: If the log level is modified in the rule, update */
/*                         the same log level in the state entry also.      */
/*                                                                          */
/*    Input(s)           : u4Flush - Flushes state entries if flag is set   */
/*                       : u4CommitRules - FWL_COMMIT | FWL_DONT_COMMIT     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
UINT4
FwlGarbageCollect (UINT4 u4Flush, UINT4 u4CommitRules)
{
    tStatefulSessionNode *pState = NULL;
    tStatefulSessionNode *pNextNode = NULL;
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4TimeoutFactor = FWL_ZERO;
    UINT4               u4HashIndex = FWL_ZERO;
    UINT4               u4Status = FWL_NOT_MATCH;
    UINT1               u1Protocol = FWL_ZERO;
    UINT2               u2DestPort = FWL_ZERO;
    UINT2               u2SrcPort = FWL_ZERO;
    UINT4               u4Timestamp = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;
    tTMO_HASH_TABLE    *apHashTab[FWL_HASH_TABLE_LEN] = { NULL };
    apHashTab[FWL_INDEX_0] = FWL_STATE_HASH_LIST;
    apHashTab[FWL_INDEX_1] = FWL_STATE_V6_HASH_LIST;
    apHashTab[FWL_INDEX_2] = NULL;
    for (u4Count = FWL_ZERO; apHashTab[u4Count] != NULL; u4Count++)
    {
        pState = (tStatefulSessionNode *) NULL;
        u4HashIndex = FWL_ZERO;

        TMO_HASH_Scan_Table (apHashTab[u4Count], u4HashIndex)
        {
            pState = (tStatefulSessionNode *)
                TMO_HASH_Get_First_Bucket_Node (apHashTab[u4Count],
                                                u4HashIndex);

            u4Status = FWL_NOT_MATCH;    /* Init this everytime */

            while (pState != NULL)
            {
                u1Protocol = pState->u1Protocol;
                /* For Fwl Idle timer expiry, Check for the traffic in NP for 
                 * the given Node and if the entry is active then update the 
                 * u4Timestamp of the node */

                if ((u1Protocol == FWL_TCP) || (u1Protocol == FWL_UDP))
                {
                    u2SrcPort = (UINT2) FWL_LOCAL_PORT (pState, u1Protocol);
                    u2DestPort = (UINT2) FWL_REMOTE_PORT (pState, u1Protocol);

                    if ((u4Flush == FWL_DONT_FLUSH) &&
                        (u4CommitRules == FWL_DONT_COMMIT))
                    {
#ifdef FLOWMGR_WANTED
                        if (FWL_FL_UTIL_GET_TIMESTAMP (pState->LocalIP.v4Addr,
                                                       pState->RemoteIP.v4Addr,
                                                       u2SrcPort, u2DestPort,
                                                       u1Protocol) ==
                            OSIX_SUCCESS)
                        {
                            FWL_GET_SYS_TIME (&u4Timestamp);
                        }
#endif /* FLOWMGR_WANTED */
                    }
                    else
                    {
                        u4Timestamp = FWL_ZERO;
                    }
                }
                FWL_GET_SYS_TIME (&u4CurTime);
                switch (u1Protocol)
                {
                    case FWL_TCP:
                        if ((pState->u1LocalState == FWL_TCP_STATE_EST) &&
                            (pState->u1RemoteState == FWL_TCP_STATE_EST))
                        {        /* EST state */
                            u4TimeoutFactor = FWL_TCP_EST_FLOW_TIMEOUT;
#ifdef FLOWMGR_WANTED
                            if (u4Timestamp != FWL_ZERO)
                            {
                                pState->u4Timestamp = u4Timestamp;
                            }
#endif /* FLOWMGR_WANTED */

                        }
                        else if (((pState->u1LocalState ==
                                   FWL_TCP_STATE_LAST_ACK)
                                  && (pState->u1RemoteState ==
                                      FWL_TCP_STATE_FIN_WAIT1))
                                 ||
                                 ((pState->u1LocalState ==
                                   FWL_TCP_STATE_FIN_WAIT1)
                                  && (pState->u1RemoteState ==
                                      FWL_TCP_STATE_LAST_ACK))
                                 ||
                                 ((pState->u1LocalState ==
                                   FWL_TCP_STATE_CLOSING)
                                  && (pState->u1RemoteState ==
                                      FWL_TCP_STATE_CLOSING))
                                 || (pState->u1LocalState ==
                                     FWL_TCP_STATE_CLOSED))
                        {
                            u4TimeoutFactor = FWL_TCP_FIN_FLOW_TIMEOUT;
                        }
                        else
                        {
                            /* Timeout can be set to a smaller value, as this
                             * connection already received a FIN */
                            u4TimeoutFactor = FWL_TCP_FIN_FLOW_TIMEOUT
                                * FWL_FOUR;
                        }
                        break;

                    case FWL_UDP:
                        if (pState->u1LocalState == FWL_NEW)
                        {        /* NEW */
                            u4TimeoutFactor = FWL_UDP_FLOW_TINY_TIMEOUT;
                        }
                        else
                        {
                            u4TimeoutFactor = FWL_UDP_FLOW_LARGE_TIMEOUT;
                        }
#ifdef FLOWMGR_WANTED
                        if (u4Timestamp != FWL_ZERO)
                        {
                            pState->u4Timestamp = u4Timestamp;
                        }
#endif /* FLOWMGR_WANTED */
                        break;

                    case FWL_ICMP:
                        u4TimeoutFactor = FWL_ICMP_FLOW_TIMEOUT;
                        break;

                    default:
                        /* This should not happen,as we store only TCP/UDP/ICMP                          * This node should be deleted as it containts garbage 
                         * value
                         */
                        u4TimeoutFactor = FWL_ZERO;
                }                /*End-of-Switch */

                if ((u4Flush == FWL_FLUSH_STATEFUL_ENTRIES) ||
                    (pState->u4Timestamp == FWL_ZERO) ||
                    ((u4CurTime - pState->u4Timestamp) >
                     (u4TimeoutFactor * FWL_SYS_TIME_FACTOR)))
                {
                    u4Status = FWL_DELETE_STATE_ENTRY;
                }
                else if (u4CommitRules == FWL_COMMIT)    /* Case 4 */
                {
                    /* Check the state entry against all the configured
                     * rules. Check for the cases 1 and 2, as given in 
                     * the function description */
                    u4Status = FwlMatchStateEntryInAcl (&pState);
                }
                else
                {
                    u4Status = FWL_NOT_MATCH;
                }

                if (u4Status == FWL_DELETE_STATE_ENTRY)
                {

                    /* Before deleting the node, the sessions node needs
                     *to be updated.For entries marked as stale (Timestamp = 0),
                     * the updation of session count is already done. */
                    if (((pState) && (pState->u4Timestamp)) != FWL_ZERO)
                    {
                        FwlRemoveUserSession (u4Flush, pState);
                    }
                    pNextNode = (tStatefulSessionNode *)
                        TMO_HASH_Get_Next_Bucket_Node (apHashTab[u4Count],
                                                       u4HashIndex,
                                                       (tTMO_HASH_NODE *)
                                                       pState);
                    /* Call Tmo Delete Node */
                    TMO_HASH_Delete_Node (apHashTab[u4Count],
                                          (tTMO_HASH_NODE *) pState,
                                          u4HashIndex);

                    /* Delete the entry from NP */
                    /* Only TCP & UDP entries are added in NP,so remove only TCP
                     * and UDP entries */

                    if ((u1Protocol == FWL_TCP) || (u1Protocol == FWL_UDP))
                    {
                        FL_FWL_DEL_HW_ENTRIES (pState->LocalIP.v4Addr,
                                               pState->RemoteIP.v4Addr,
                                               u2SrcPort, u2DestPort,
                                               u1Protocol);
                    }
                    /* Free the Node. */
                    if (pState != NULL)
                    {
                        FwlStateMemFree ((UINT1 *) pState);
                    }
                    pState = pNextNode;

                }                /* u4Status == FWL_DELETE_STATE_ENTRY */
                else
                {
                    pState = (tStatefulSessionNode *)
                        TMO_HASH_Get_Next_Bucket_Node (apHashTab[u4Count],
                                                       u4HashIndex,
                                                       (tTMO_HASH_NODE *)
                                                       pState);
                }
            }                    /*End of While-Loop */
        }                        /*End of TMO_HASH_Scan_Table */
    }

    FwlDeleteTcpInitFlowNode (u4Flush);
    FwlDeletePartialLinksListNodes (u4Flush);
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u2DestPort);
    UNUSED_PARAM (u4Timestamp);
    return FWL_SUCCESS;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      :  FwlDeleteStateTableEntryOnDemand                */
/*                                                                          */
/*    Description        : Deletes  a State table entry on receive of a     */
/*                         request from SIP                                 */
/*                                                                          */
/*    Input(s)           : pNodeToDelete -- State Table Node Pointer        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT4
FwlDeleteStateTableEntryOnDemand (tStatefulSessionNode * pNodeToDelete)
{
    tStatefulSessionNode *pState = NULL;
    UINT4               u4HashIndex = FWL_ZERO;
    UINT4               u4Status = FWL_NOT_MATCH;
    UINT4               u4IpAddr = FWL_ZERO;
    UINT4               u4IpAddr1 = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT2               u2Port1 = FWL_ZERO;
    UINT2               u2SrcPort = FWL_ZERO;
    UINT2               u2DestPort = FWL_ZERO;
    UINT1               u1FoundFlag = FWL_FAILURE;

    pState = (tStatefulSessionNode *) NULL;
    u4HashIndex = FWL_ZERO;

    if (pNodeToDelete->u1Direction == FWL_DIRECTION_IN)
    {
        u4IpAddr = pNodeToDelete->LocalIP.v4Addr;
        u2Port =
            (UINT2) FWL_LOCAL_PORT (pNodeToDelete, pNodeToDelete->u1Protocol);
    }
    else
    {
        u4IpAddr = pNodeToDelete->RemoteIP.v4Addr;
        u2Port =
            (UINT2) FWL_REMOTE_PORT (pNodeToDelete, pNodeToDelete->u1Protocol);
    }

    TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
    {
        pState = (tStatefulSessionNode *)
            TMO_HASH_Get_First_Bucket_Node (FWL_STATE_HASH_LIST, u4HashIndex);

        u4Status = FWL_NOT_MATCH;    /* Init this everytime */

        UNUSED_PARAM (u4Status);
        while (pState != NULL)
        {

            if (pState->u1Direction == FWL_DIRECTION_IN)
            {
                u4IpAddr1 = pState->LocalIP.v4Addr;
                u2Port1 = (UINT2) FWL_LOCAL_PORT (pState, pState->u1Protocol);
            }
            else
            {
                u4IpAddr1 = pState->RemoteIP.v4Addr;
                u2Port1 = (UINT2) FWL_REMOTE_PORT (pState, pState->u1Protocol);
            }

            if ((u4IpAddr == u4IpAddr1) && (u2Port == u2Port1) &&
                (pState->u1Direction == pNodeToDelete->u1Direction) &&
                (pState->u1Protocol == pNodeToDelete->u1Protocol))
            {

                FwlRemoveUserSession (FWL_DONT_FLUSH, pState);

                /* Delete the entry from NP */
                /* Only TCP & UDP entries are added in NP, so remove only TCP
                 * and UDP entries */

                if ((pState->u1Protocol == FWL_TCP) ||
                    (pState->u1Protocol == FWL_UDP))
                {
                    u2SrcPort =
                        (UINT2) FWL_LOCAL_PORT (pState, pState->u1Protocol);
                    u2DestPort =
                        (UINT2) FWL_REMOTE_PORT (pState, pState->u1Protocol);

                    FL_FWL_DEL_HW_ENTRIES (pState->LocalIP.v4Addr,
                                           pState->RemoteIP.v4Addr, u2SrcPort,
                                           u2DestPort, pState->u1Protocol);
                }

                /* Call Tmo Delete Node */
                TMO_HASH_Delete_Node (FWL_STATE_HASH_LIST,
                                      (tTMO_HASH_NODE *) pState, u4HashIndex);

                /* Free the Nodes. */
                FwlStateMemFree ((UINT1 *) pState);

                u1FoundFlag = FWL_SUCCESS;

                break;

            }                    /* u4Status == FWL_DELETE_STATE_ENTRY */
            else
            {
                pState = (tStatefulSessionNode *)
                    TMO_HASH_Get_Next_Bucket_Node (FWL_STATE_HASH_LIST,
                                                   u4HashIndex,
                                                   (tTMO_HASH_NODE *) pState);
            }
        }                        /*End of While-Loop */
        if (u1FoundFlag == FWL_SUCCESS)
        {
            break;
        }
    }                            /*End of TMO_HASH_Scan_Table */
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u2DestPort);
    return (u1FoundFlag);

}

INT4
FwlAppDeleteStateTableEntry (UINT4 u4LocalIP, UINT2 u2LocalPort,
                             UINT4 u4RemoteIP, UINT2 u2RemotePort,
                             UINT2 u2Protocol)
{
    tStatefulSessionNode *pState = NULL;
    UINT4               u4HashIndex = FWL_ZERO;
    UINT4               u4Status = FWL_NOT_MATCH;
    UINT2               u2SrcPort = FWL_ZERO;
    UINT2               u2DestPort = FWL_ZERO;
    UINT1               u1FoundFlag = FWL_FAILURE;

    pState = (tStatefulSessionNode *) NULL;
    u4HashIndex = FWL_ZERO;

    TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
    {
        pState = (tStatefulSessionNode *)
            TMO_HASH_Get_First_Bucket_Node (FWL_STATE_HASH_LIST, u4HashIndex);

        u4Status = FWL_NOT_MATCH;    /* Init this everytime */
        UNUSED_PARAM (u4Status);

        while (pState != NULL)
        {

            if ((pState->LocalIP.v4Addr == u4LocalIP) &&
                (pState->RemoteIP.v4Addr == u4RemoteIP) &&
                (FWL_LOCAL_PORT (pState, pState->u1Protocol) ==
                 u2LocalPort) &&
                (FWL_REMOTE_PORT (pState, pState->u1Protocol) ==
                 u2RemotePort) && (pState->u1Protocol == u2Protocol))
            {

                FwlRemoveUserSession (FWL_DONT_FLUSH, pState);

                /* Delete the entry from NP */
                /* Only TCP & UDP entries are added in NP, so remove only TCP
                 * and UDP entries */

                if ((pState->u1Protocol == FWL_TCP) ||
                    (pState->u1Protocol == FWL_UDP))
                {
                    u2SrcPort =
                        (UINT2) FWL_LOCAL_PORT (pState, pState->u1Protocol);
                    u2DestPort =
                        (UINT2) FWL_REMOTE_PORT (pState, pState->u1Protocol);

                    FL_FWL_DEL_HW_ENTRIES (pState->LocalIP.v4Addr,
                                           pState->RemoteIP.v4Addr, u2SrcPort,
                                           u2DestPort, pState->u1Protocol);
                }

                /* Call Tmo Delete Node */
                TMO_HASH_Delete_Node (FWL_STATE_HASH_LIST,
                                      (tTMO_HASH_NODE *) pState, u4HashIndex);

                /* Free the Nodes. */
                FwlStateMemFree ((UINT1 *) pState);

                u1FoundFlag = FWL_SUCCESS;

                break;

            }                    /* u4Status == FWL_DELETE_STATE_ENTRY */
            else
            {
                pState = (tStatefulSessionNode *)
                    TMO_HASH_Get_Next_Bucket_Node (FWL_STATE_HASH_LIST,
                                                   u4HashIndex,
                                                   (tTMO_HASH_NODE *) pState);
            }
        }                        /*End of While-Loop */
        if (u1FoundFlag == FWL_SUCCESS)
        {
            break;
        }
    }                            /*End of TMO_HASH_Scan_Table */
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u2DestPort);

    return (u1FoundFlag);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlFtpGetPayloadIpPort                           */
/*                                                                          */
/*    Description        : Gets the Port number in the FTP payload.         */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the buffer            */
/*                         pai1IpAddr    -- Ip Address in the FTP payload   */
/*                         pai1Port      -- Port number in the FTP payload  */
/*                         u1Cmd        -- PORT / PASSV command             */
/*                         pGlobalInfo  -- Pointer to the structure to hold */
/*                                         port number in the FTP payload.  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlFtpGetPayloadIpPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                        INT1 *pai1IpAddr,
                        INT1 *pai1Port, UINT4 u4Offset,
                        UINT1 u1Cmd, tFwlGlobalInfo * pGlobalInfo)
{
    UINT1               u1Byte = FWL_ZERO;
    UINT1               u1Index = FWL_ZERO;
    UINT1               u1CommaCount = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT4               u4PktSize = FWL_ZERO;

    u1CommaCount = FWL_ZERO;
    u1Byte = FWL_ZERO;
    u1Index = FWL_ZERO;
    u2Port = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FWlFtpGetPayloadIpPort \n");
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (u1Cmd == FWL_FTP_PASV)
    {
        /* Search for the '(' after end of 227 string */
        while (u4Offset < u4PktSize)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, u4Offset++,
                                       FWL_ONE_BYTE);
            if (u1Byte == '(')
            {
                break;
            }
        }
        if (u4Offset >= u4PktSize)
        {
            FWL_DBG (FWL_DBG_INSPECT, "\n No \'(\' found in PASV reply \n");
            return;
        }
    }

    /* extract IP address and Port number */

    /*
     * The IP address and port number (in ascii) in the payload is separated
     * by commas. Hence the IP address in nothing but the value separated by
     * four commas. for example
     * 10.0.0.1 is represented by 31 30, 30, 30, 31,
     * Then each byte is extracted until the fourth comma is reached and
     * stored in ai1IpAddress. Similarly port is also read.
     * The FTP payload is delimited by 0x0d 0x0a.
     */
    while (u4Offset < u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, u4Offset++,
                                   FWL_ONE_BYTE);
        if (u1Byte == FWL_OFFSET_0d)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, u4Offset++,
                                       FWL_ONE_BYTE);
            if (u1Byte == FWL_OFFSET_0a)
            {
                /*The end of FTP payload is reached */
                switch (u1Cmd)
                {
                    case FWL_FTP_PORT:
                    case FWL_FTP_PASV:
                        pai1Port[u1Index] = FWL_STRING_END;
                        break;
                    default:
                        pai1Port[--u1Index] = FWL_STRING_END;
                        break;
                }
                break;
            }
        }
        else
        {
            /* IP addr is read */
            if (u1CommaCount < FWL_FTP_IP_COMMA_COUNT)
            {
                if ((u1Byte == ',')
                    && (u1CommaCount < (FWL_FTP_IP_COMMA_COUNT - FWL_ONE)))
                {
                    pai1IpAddr[u1Index++] = '.';
                    u1CommaCount++;
                }
                else
                {
                    if ((u1Byte == ',')
                        && (u1CommaCount++ == (FWL_FTP_IP_COMMA_COUNT
                                               - FWL_ONE)))
                    {
                        pai1IpAddr[u1Index] = FWL_STRING_END;
                        u1Index = FWL_ZERO;
                    }
                    else
                    {
                        pai1IpAddr[u1Index++] = (INT1) u1Byte;
                    }
                }
            }
            /* Port is read */
            else
            {
                if (u1Byte == ',')
                {
                    pai1Port[u1Index++] = FWL_STRING_END;
                    u2Port =
                        (UINT2) (FWL_ATOI (pai1Port) * FWL_FTP_PORT_DIV_FACTOR);
                    u1Index = FWL_ZERO;
                }
                else
                {
                    pai1Port[u1Index++] = (INT1) u1Byte;

                }
            }
        }
    }                            /* End of While Loop */

    /* Reached end of packet but didn't find 0xd 0xa then end the port 
     * string with FWL_STRING_END 
     */
    if (u4Offset >= u4PktSize)
    {
        /*The end of FTP payload is reached */
        switch (u1Cmd)
        {
            case FWL_FTP_PORT:
            case FWL_FTP_PASV:
                pai1Port[u1Index] = FWL_STRING_END;
                break;
            default:
                pai1Port[--u1Index] = FWL_STRING_END;
                break;
        }
    }

    /* conversion of ascii to integer of the port value */
    u2Port = (UINT2) (u2Port + FWL_ATOI (pai1Port));
    pGlobalInfo->u2TranslatedLocPort = u2Port;

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlFtpGetPayloadIpPort \n");
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchStateFilter                              */
/*                                                                          */
/*    Description        : Matches the filter for the Pkt against Stateful  */
/*                         filtering.                                       */
/*                                                                          */
/*    Input(s)           : pStateIPHdr  -- Pointer to the IP Header         */
/*                         pStateHLInfo -- Pointer to the Transport Header  */
/*                         pStateIcmpHdr-- Pointer to the Icmp Header       */
/*                         u1PktDirection  -- Direction IN or OUT           */
/*                                                                          */
/*    Output(s)          : ppStateNode -- stores Pointer to matched node    */
/*                         pu1LogTrigger -- stores the Log level value      */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
UINT4
FwlMatchStateFilter (tIpHeader * pStateIpHdr,
                     tHLInfo * pStateHLInfo,
                     tIcmpInfo * pStateIcmpHdr,
                     UINT1 u1PktDirection,
                     UINT1 *pu1LogTrigger, tStatefulSessionNode ** ppStateNode)
{

    tStatefulSessionNode *pState = NULL;
    tTMO_HASH_TABLE    *pHashTab = NULL;
    UINT4               u4Status = FWL_NOT_MATCH;
    UINT4               u4FragOffsetBit = FWL_ZERO;
    UINT4               u4HashKey = FWL_ZERO;
    tFwlIpAddr          aIpAddr[FWL_TWO];
    UINT2               au2Port[FWL_TWO] = { FWL_ZERO };
    UINT1               u1Protocol = FWL_ZERO;
    UINT1               u1Local = FWL_ZERO;

    u1Local = (UINT1) (u1PktDirection == FWL_DIRECTION_IN);

    if (!(NULL == pStateHLInfo))
    {
        /* Update port info for TCP/UDP */
        au2Port[FWL_INDEX_0] = pStateHLInfo->u2SrcPort;
        au2Port[FWL_INDEX_1] = pStateHLInfo->u2DestPort;
    }
    else
    {
        return u4Status;
    }

    FWL_IP_ADDR_COPY (&aIpAddr[FWL_INDEX_0], &pStateIpHdr->SrcAddr);
    FWL_IP_ADDR_COPY (&aIpAddr[FWL_INDEX_1], &pStateIpHdr->DestAddr);

    u1Protocol = pStateIpHdr->u1Proto;

    /* Get the HashKey from the input. */
    u4HashKey = FwlStateFormHashKey (pStateIpHdr, pStateHLInfo);

    if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pHashTab = FWL_STATE_HASH_LIST;
        u4FragOffsetBit = FWL_FRAG_OFFSET_BIT;
    }
    else
    {
        pHashTab = FWL_STATE_V6_HASH_LIST;
        u4FragOffsetBit = FWL_IP6_FRAG_OFFSET_BIT;
    }

    TMO_HASH_Scan_Bucket (pHashTab, u4HashKey, pState, tStatefulSessionNode *)
    {
        if (pState != NULL)
        {
            /* Ignore Nodes that are marked for deletion. */
            if (pState->u4Timestamp == FWL_ZERO)
            {
                continue;
            }

            /* First check, if the IP Address and Protocol matches.... */
            if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_4)
            {
                if (!((pState->LocalIP.v4Addr == aIpAddr[u1Local].v4Addr) &&
                      (pState->RemoteIP.v4Addr == aIpAddr[!u1Local].v4Addr) &&
                      (pState->u1Protocol == u1Protocol)))
                {
                    continue;
                }
            }
            else if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_6)
            {
                if (!
                    ((Ip6AddrCompare
                      (pState->LocalIP.v6Addr,
                       aIpAddr[u1Local].v6Addr) == IP6_ZERO)
                     &&
                     (Ip6AddrCompare
                      (pState->RemoteIP.v6Addr,
                       aIpAddr[!u1Local].v6Addr) == IP6_ZERO)
                     && (pState->u1Protocol == u1Protocol)))
                {
                    continue;
                }
            }

            if ((pStateIpHdr->u2FragOffset & u4FragOffsetBit)
                >= FWL_FRAG_OFFSET_TINY)
            {
                /* Allow non-first fragmented packets */
                *pu1LogTrigger = pState->u1LogLevel;
                *ppStateNode = pState;
                return (FWL_MATCH);
            }
            /* If IP addresses and protocol matches, 
             * check the port numbers (for tcp/udp) */
            switch (u1Protocol)
            {
                case FWL_TCP:
                case FWL_UDP:

                    if ((FWL_LOCAL_PORT (pState, u1Protocol) ==
                         au2Port[u1Local])
                        && (FWL_REMOTE_PORT (pState, u1Protocol) ==
                            au2Port[!u1Local]))
                    {
                        *pu1LogTrigger = pState->u1LogLevel;
                        *ppStateNode = pState;
                        return (FWL_MATCH);
                    }
                    break;

                case FWL_ICMP:
                    /* 1. If the direction matches, allow only Echo request packets
                     * 2. If the direction fails, allow only Echo reply packets */

                    if (((u1PktDirection == pState->u1Direction) &&
                         (pStateIcmpHdr->u1Type == FWL_ICMP_TYPE_ECHO_REQUEST))
                        || ((u1PktDirection != pState->u1Direction)
                            && (pStateIcmpHdr->u1Type ==
                                FWL_ICMP_TYPE_ECHO_REPLY)))
                    {
                        /* Check the sequence number */
                        if (FWL_ICMP_IP_ID (pState) ==
                            pStateIcmpHdr->u2IcmpSeqNum)
                        {
                            *pu1LogTrigger = pState->u1LogLevel;
                            *ppStateNode = pState;
                            return (FWL_MATCH);
                        }
                    }
                    else if ((pStateIpHdr->u2FragOffset & u4FragOffsetBit)
                             >= FWL_FRAG_OFFSET_TINY)
                    {
                        /* Allow non-first fragmented packets */
                        *pu1LogTrigger = pState->u1LogLevel;
                        *ppStateNode = pState;
                        return (FWL_MATCH);
                    }
                    break;

                case FWL_ICMPV6:
                    /* 1. If the direction matches, allow only Echo 
                     * request packets
                     * 2. If the direction fails, allow only Echo 
                     * reply packets */
                    if (((u1PktDirection == pState->u1Direction) &&
                         (pStateIcmpHdr->u1Type ==
                          FWL_ICMPv6_TYPE_ECHO_REQUEST))
                        || ((u1PktDirection != pState->u1Direction)
                            && (pStateIcmpHdr->u1Type ==
                                FWL_ICMPv6_TYPE_ECHO_REPLY)))
                    {
                        /* Check the sequence number */
                        if (FWL_ICMP_IP_ID (pState) ==
                            pStateIcmpHdr->u2IcmpSeqNum)
                        {
                            *pu1LogTrigger = pState->u1LogLevel;
                            *ppStateNode = pState;
                            return (FWL_MATCH);
                        }
                    }
                    else if (pStateIpHdr->u2FragOffset & u4FragOffsetBit)
                    {
                        /* Allow non-first fragmented packets */
                        *pu1LogTrigger = pState->u1LogLevel;
                        *ppStateNode = pState;
                        return (FWL_MATCH);
                    }
                    break;

                default:
                    continue;
            }                    /* switch (u1Protocol) */
        }                        /* pState != NULL */
    }                            /* Scan Hash Nodes */
    return u4Status;

}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlStateFormHashKey                              */
/*                                                                          */
/*    Description        : This forms the Hash key depending on the input.  */
/*                                                                          */
/*    Input(s)           : pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Hash Key                                         */
/****************************************************************************/

PRIVATE UINT4
FwlStateFormHashKey (tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo)
{
    UINT4               u4HashValue = FWL_ZERO;
    INT4                i4Cnt = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FwlStateFormHashKey \n");

    if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        u4HashValue =
            (pStateIpHdr->SrcAddr.v4Addr ^ pStateIpHdr->DestAddr.
             v4Addr ^ pStateHLInfo->u2SrcPort ^ pStateHLInfo->
             u2DestPort ^ pStateIpHdr->u1Proto);
    }
    else if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_6)
    {
        for (i4Cnt = FWL_ZERO; i4Cnt < FWL_FOUR; i4Cnt++)
        {
            u4HashValue +=
                u4HashValue ^ pStateIpHdr->SrcAddr.v6Addr.
                u4_addr[i4Cnt] ^ pStateIpHdr->DestAddr.v6Addr.u4_addr[i4Cnt];
        }
        u4HashValue +=
            u4HashValue ^ pStateHLInfo->u2SrcPort ^ pStateHLInfo->
            u2DestPort ^ pStateIpHdr->u1Proto;
    }

    u4HashValue &= FWL_STATE_HASH_BIT_MASK;

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlStateFormHashKey \n");
    return (u4HashValue);
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTcpEstStateHandler                            */
/*                                                                          */
/*    Description        : Processes incoming packet against TCP session    */
/*                         state in State-Table.                            */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         pStateNode   -- Pointer to Node to be intialised */
/*                         u1Direction  -- Specifies IN or OUT              */
/*                         pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

UINT4
FwlTcpEstStateHandler (tCRU_BUF_CHAIN_HEADER * pBuf,
                       tStatefulSessionNode * pStateNode,
                       UINT1 u1Direction,
                       tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo)
{
    tTcpInfo           *pSendrInfo = NULL;
    tTcpInfo           *pRcvrInfo = NULL;
    UINT1              *pu1RcvrState = NULL;
    UINT1              *pu1SendrState = NULL;
    UINT1               u1TcpFlags = FWL_ZERO;
    UINT4               u4DummyIfNum = FWL_ZERO;

    UNUSED_PARAM (pSendrInfo);
    /* Get the TCP flags Byte */
    FWL_GET_1_BYTE (pBuf,
                    (UINT4) (pStateIpHdr->u1HeadLen + FWL_TCP_CODE_BIT_OFFSET),
                    u1TcpFlags);
    u1TcpFlags = u1TcpFlags & FWL_OFFSET_3f;

    if (u1Direction == FWL_DIRECTION_IN)
    {                            /* Pkt from WAN(Remote/Sender) 
                                   to LAN(Local/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pu1SendrState = &(pStateNode->u1RemoteState);
        pu1RcvrState = &(pStateNode->u1LocalState);
    }
    else                        /* (u1Direction == FWL_DIRECTION_OUT) */
    {                            /* Pkt from LAN(Local/Sender) 
                                   to WAN(Remote/Receiver) */
        pSendrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.LocalTcpInfo);
        pRcvrInfo = (tTcpInfo *) & (pStateNode->ProtHdr.TcpHdr.RemoteTcpInfo);
        pu1SendrState = &(pStateNode->u1LocalState);
        pu1RcvrState = &(pStateNode->u1RemoteState);
    }

    /* NOTE: Below POST SYN Attack check is done after RST Handling. 
     * As RST pkt validation restricts RST+ACK, RST only. And RST+SYN
     * is considered as invalid. So it is immaterial which check is done
     * first ( RST check or SYN check). */

    /* RST Handling in Synchronized states */
    /* NOTES:- 
     * 1. A RST pkt is valid if Seq.No. is within window and RST Flag is set. 
     * 2. Other flags are ignored. 
     * 3. If ACK set it need not be validated. 
     * 4. Seq.No. ZERO is INVALID. */
    if (pStateHLInfo->u1Rst)
    {
        if ((u1TcpFlags == FWL_TCP_RST_MASK) ||
            (u1TcpFlags == (FWL_TCP_RST_MASK | FWL_TCP_ACK_MASK)))
        {
            *pu1SendrState = FWL_TCP_STATE_CLOSED;
            *pu1RcvrState = FWL_TCP_STATE_CLOSED;
            pStateNode->u4Timestamp = FWL_ZERO;
            FwlRemoveUserSession (FWL_DONT_FLUSH, pStateNode);
            return FWL_SUCCESS;
        }                        /*End-of-Valid-RST-Check */
        else
        {
            /* RST Attack - Log as Invalid TCP packet */
            FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }
    }                            /*End-of-Rst-Check */

    /* SYN, SYN-ACK Handling in Established State. */
    if (pStateHLInfo->u1Syn)
    {
        if (*pu1RcvrState >= FWL_TCP_STATE_SYN_RCVD)
        {
            /* Log as POST_SYN Attack */
            FwlLogMessage (FWL_TCP_POST_SYN_ATTACK, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }
    }                            /*End-of-POST_SYN-Attack-Check */

    switch (*pu1RcvrState)
    {
        case FWL_TCP_STATE_LISTEN:
        {
            /* switch(Pkt-Type) */
            /* case  Syn: if (ValidSeqAck) Then Allow pkt */
            if (u1TcpFlags == FWL_TCP_SYN_MASK)
            {                    /* This pkt is a retransmitted Tcp SYN 
                                   only Pkt */
                /* No updations are to be done in matching entry */
                return FWL_SUCCESS;
            }
            /* Any other pkt is to be dropped */
            /* case (SynAck/Ack/Rst/Fin/DataOnlyPkt):   
               default:
               Drop the pkt.
             */
            FwlLogMessage (FWL_TCP_PKT_INVALID_4_STATE, u4DummyIfNum, pBuf,
                           pStateNode->u1LogLevel, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        }                        /*End-of-CASE */

        case FWL_TCP_STATE_SYN_SENT:
        case FWL_TCP_STATE_SYN_RCVD:
        {
            if (pStateHLInfo->u1Fin)
            {
                if (pStateHLInfo->u1Ack)
                {
                    if ((*pu1SendrState == FWL_TCP_STATE_EST) ||
                        (*pu1SendrState == FWL_TCP_STATE_SYN_RCVD))
                    {
                        *pu1SendrState = FWL_TCP_STATE_FIN_WAIT1;
                        return FWL_SUCCESS;
                    }
                }
            }
            return FWL_SUCCESS;
        }                        /*End-of-CASE */

        case FWL_TCP_STATE_EST:
        {
            /* In this state only FIN/Data/Acks2Data pkts make sense */
            if (pStateHLInfo->u1Fin)
            {
                switch (*pu1SendrState)
                {
                    case FWL_TCP_STATE_SYN_RCVD:
                    case FWL_TCP_STATE_EST:
                        *pu1SendrState = FWL_TCP_STATE_FIN_WAIT1;
                        return FWL_SUCCESS;

                    case FWL_TCP_STATE_FIN_WAIT1:
                        /* In this case this is a retransmitted FIN */
                        return FWL_SUCCESS;
                    default:
                    {
                        /* Allow the pkt. */
                        return FWL_SUCCESS;
                    }            /*End-of-DEFAULT-CASE */
                }                /*End-of-Switch */
            }                    /*End-of-Fin-Check */

            /* Allow all Non-FIN, Non-Rst, Non-Syn Pkts [Data/Ack pkts] */
            return FWL_SUCCESS;
        }                        /*End-of-CASE */

        case FWL_TCP_STATE_FIN_WAIT1:
        {
            if (pStateHLInfo->u1Fin)
            {
                if (pStateHLInfo->u1Ack)
                {
                    if ((*pu1SendrState <= FWL_TCP_STATE_EST)
                        && (pStateHLInfo->u4AckNum == pRcvrInfo->u4EndSeqNum))
                    {
                        *pu1SendrState = FWL_TCP_STATE_LAST_ACK;
                        return FWL_SUCCESS;
                    }
                }

                switch (*pu1SendrState)
                {
                    case FWL_TCP_STATE_SYN_RCVD:
                    case FWL_TCP_STATE_EST:
                        *pu1SendrState = FWL_TCP_STATE_FIN_WAIT1;
                        return FWL_SUCCESS;

                    case FWL_TCP_STATE_FIN_WAIT1:
                        /* In this case this is a retransmitted FIN */
                        return FWL_SUCCESS;

                    case FWL_TCP_STATE_CLOSE_WAIT:
                        *pu1SendrState = FWL_TCP_STATE_LAST_ACK;
                        return FWL_SUCCESS;

                    default:
                    {
                        /* Allow the pkt. */
                        return FWL_SUCCESS;
                    }            /*End-of-DEFAULT-CASE */
                }                /*End-of-Switch */
            }                    /*End-of-Fin-Check */

            if (pStateHLInfo->u1Ack)
            {
                if (pStateHLInfo->u4AckNum == pRcvrInfo->u4EndSeqNum)
                {
                    switch (*pu1SendrState)
                    {
                        case FWL_TCP_STATE_SYN_RCVD:
                        case FWL_TCP_STATE_EST:
                            *pu1SendrState = FWL_TCP_STATE_CLOSE_WAIT;
                            return FWL_SUCCESS;

                        case FWL_TCP_STATE_FIN_WAIT1:
                            /* Simultaneous Closing by applications */
                            *pu1SendrState = FWL_TCP_STATE_CLOSING;
                            *pu1RcvrState = FWL_TCP_STATE_CLOSING;
                            return FWL_SUCCESS;

                        default:
                        {
                            /* Allow the pkt. */
                            return FWL_SUCCESS;
                        }        /*End-of-DEFAULT-CASE */
                    }            /*End-of-Switch */
                }                /*End-of-IF-Block */

                /* May be Normal Data Ack. So allow the packet. */
                return FWL_SUCCESS;
            }                    /*End-of-Ack-Check */

            /* Allow all Non-FIN, Non-Rst, Non-Syn Pkts */
            return FWL_SUCCESS;
        }                        /*End-of-CASE */

        case FWL_TCP_STATE_CLOSING:
        {
            if (pStateHLInfo->u1Fin)
            {
                return FWL_SUCCESS;
            }

            if (pStateHLInfo->u1Ack)
            {
                if (pStateHLInfo->u4AckNum == pRcvrInfo->u4EndSeqNum)
                {
                    switch (*pu1SendrState)
                    {
                        case FWL_TCP_STATE_CLOSING:
                            /* Move Sender's state to FWL_TCP_STATE_TIME_WAIT */
                            /* Terminate the session. Mark it for Deletion. */
                            *pu1SendrState = FWL_TCP_STATE_CLOSED;
                            *pu1RcvrState = FWL_TCP_STATE_CLOSED;
                            pStateNode->u4Timestamp = FWL_ZERO;
                            FwlRemoveUserSession (FWL_DONT_FLUSH, pStateNode);
                            return FWL_SUCCESS;

                        default:
                            return FWL_SUCCESS;
                    }            /*End-of-Switch */
                }
            }

            /* Allow all Non-FIN, Non-Rst, Non-Syn Pkts */
            return FWL_SUCCESS;
        }                        /*End-of-CASE */

/*    case FWL_TCP_STATE_TIME_WAIT: */
        case FWL_TCP_STATE_CLOSE_WAIT:
        {
            /* Allow all packets that have been validated 
             * for Sequence Number and Ack Number.         
             * Non RST, SYN are harmless pkts and so can be allowed here, 
             * leaving for the destined Hosts TCP stack to drop them. */
            return FWL_SUCCESS;

        }                        /*End-of-CASE */

        case FWL_TCP_STATE_LAST_ACK:
        {
            if (pStateHLInfo->u1Fin)
            {
                /* Allow all packets that have been validated 
                 * for Sequence Number and Ack Number.         
                 * Non RST, SYN are harmless pkts and so can be allowed here, 
                 * leaving for the destined Hosts TCP stack to drop them. */
                return FWL_SUCCESS;
            }

            if (pStateHLInfo->u1Ack)
            {
                if (pStateHLInfo->u4AckNum == pRcvrInfo->u4EndSeqNum)
                {
                    switch (*pu1SendrState)
                    {
                        case FWL_TCP_STATE_FIN_WAIT1:
                            /*   case FWL_TCP_STATE_FIN_WAIT2:    */
                            /* Move Sender's state to FWL_TCP_STATE_TIME_WAIT */
                            /* Terminate the session. Mark it for Deletion. */
                            *pu1SendrState = FWL_TCP_STATE_CLOSED;
                            *pu1RcvrState = FWL_TCP_STATE_CLOSED;
                            pStateNode->u4Timestamp = FWL_ZERO;
                            FwlRemoveUserSession (FWL_DONT_FLUSH, pStateNode);
                            return FWL_SUCCESS;

                        default:
                            return FWL_SUCCESS;
                    }            /*End-of-Switch */
                }
                return FWL_SUCCESS;
            }

            /* Allow all Non-FIN, Non-Rst, Non-Syn Pkts */
            return FWL_SUCCESS;
        }                        /*End-of-CASE */

        default:                /* INVALID Rcvr Tcp-State. */
        {
            break;
        }                        /*End-of-DEFAULT-CASE */
    }                            /*End-of-Main-Switch */

    return FWL_SUCCESS;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlFtpGetIpPortFromExtdPayload                   */
/*                                                                          */
/*    Description        : Gets the Port number in the FTP payload if the   */
/*                         FTP command is EPRT or EPSV                      */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the buffer            */
/*                         pai1IpAddr    -- Ip Address in the FTP payload   */
/*                         pai1Port      -- Port number in the FTP payload  */
/*                         u1Cmd        -- EPRT / EPSV command              */
/*                         pGlobalInfo  -- Pointer to the structure to hold */
/*                                         port number in the FTP payload.  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None (In the case of failure, translated port    */
/*                               would be zero)                             */
/****************************************************************************/
VOID
FwlFtpGetIpPortFromExtdPayload (tCRU_BUF_CHAIN_HEADER * pBuf,
                                INT1 *pai1IpAddr,
                                INT1 *pai1Port, UINT4 u4Offset,
                                UINT1 u1Cmd, tFwlGlobalInfo * pGlobalInfo)
{
    UINT1               u1Byte = FWL_ZERO;
    UINT1               u1Index = FWL_ZERO;
    UINT1               u1PortIndex = FWL_ZERO;
    UINT1               u1PipeCount = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT4               u4PktSize = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering FwlFtpGetIpPortFromExtdPayload \n");
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (u1Cmd == FWL_FTP_EPSV)
    {
        /* Search for the '(' after end of 229 string */
        while (u4Offset < u4PktSize)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, u4Offset++,
                                       FWL_ONE_BYTE);
            if (u1Byte == '(')
            {
                break;
            }
        }
        if (u4Offset >= u4PktSize)
        {
            FWL_DBG (FWL_DBG_INSPECT, "\n No \'(\' found in EPSV reply \n");
            return;
        }
    }

    /* extract IP address and Port number */

    /*
     * The IP address and port number (in ascii) in the payload is separated
     * by '|' . 
     * In the case of IPv4, data format is 
     *           EPRT |1|122.203.111.77|37492|\r\n  
     * In the case of IPv6, data format is 
     *           EPRT |2|1080::8:800:200C:417A|37492|\r\n
     * In both cases of IPv4 & IPV6, reply to EPSV would be
     *           229 (|||37492|)\r\n
     * The FTP payload is delimited by 0x0d 0x0a.
     */
    while (u4Offset < u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, u4Offset++,
                                   FWL_ONE_BYTE);
        /* Check for '\r' */
        if (u1Byte == FWL_OFFSET_0d)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Byte, u4Offset++,
                                       FWL_ONE_BYTE);

            /* Check for '\n' */
            if (u1Byte == FWL_OFFSET_0a)
            {
                pai1Port[u1PortIndex] = FWL_STRING_END;
                break;
            }
        }
        /* Check for '|' */
        else if (u1Byte == FWL_OFFSET_7c)
        {
            u1PipeCount++;
        }
        else
        {
            /* IP addr is read */
            if (u1PipeCount == FWL_FTP_IP_PIPE_COUNT)
            {
                if (u1Index < FWL_IPADDR_LEN_MAX)
                {
                    pai1IpAddr[u1Index++] = (INT1) u1Byte;
                }
            }
            /* Port is read */
            else if (u1PipeCount == FWL_FTP_PORT_PIPE_COUNT)
            {
                if (u1PortIndex < FWL_PORT_LEN_MAX)
                {
                    pai1Port[u1PortIndex++] = (INT1) u1Byte;
                }
            }
        }
    }                            /* End of While Loop */

    /* Reached end of packet but didn't find 0xd 0xa then end the port 
     * string with FWL_STRING_END 
     */
    if (u4Offset >= u4PktSize)
    {
        /*The end of FTP payload is reached */
        pai1IpAddr[u1Index] = FWL_STRING_END;
        pai1Port[u1PortIndex] = FWL_STRING_END;
    }

    /* conversion of ascii to integer of the port value */
    u2Port = (UINT2) (u2Port + FWL_ATOI (pai1Port));
    pGlobalInfo->u2TranslatedLocPort = u2Port;

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlFtpGetIpPortFromExtdPayload \n");
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlStatePortMatchExists                            */
/*                                                                           */
/*     DESCRIPTION      : This function matches if the local port information*/
/*                        and remote port information passed as args matches */
/*                        the port information in pStateFilterInfo.          */
/*                                                                           */
/*     INPUT            : pStateFilterInfo  - Stateful entry                 */
/*                        i4FwlStateLocalPort - Local Port.                  */
/*                        i4FwlStateRemotePort - Remote Port.                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : FWL_SUCCESS if match exists                        */
/*                        else return FWL_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT1
FwlStatePortMatchExists (tStatefulSessionNode * pStateFilterInfo,
                         INT4 i4FwlStateLocalPort, INT4 i4FwlStateRemotePort)
{
    if (pStateFilterInfo->u1Protocol == FWL_ICMP)
    {
        if ((FWL_ICMP_IP_ID (pStateFilterInfo) == i4FwlStateLocalPort) &&
            (FWL_REMOTE_PORT (pStateFilterInfo,
                              pStateFilterInfo->u1Protocol) ==
             i4FwlStateRemotePort))
        {
            return FWL_SUCCESS;
        }
    }
    else
    {
        if ((FWL_LOCAL_PORT (pStateFilterInfo,
                             pStateFilterInfo->u1Protocol) ==
             i4FwlStateLocalPort)
            && (FWL_REMOTE_PORT (pStateFilterInfo, pStateFilterInfo->u1Protocol)
                == i4FwlStateRemotePort))
        {
            return FWL_SUCCESS;
        }
    }
    return FWL_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlStateGetStatefulTableEntries                    */
/*                                                                           */
/*     DESCRIPTION      : This function gets the stateful Table Entries      */
/*                                                                           */
/*     INPUT            : u4HashIndex - Hash Index                           */
/*                                                                           */
/*     OUTPUT           : ppStateFilterInfo - Stateful Table entry            */
/*                                                                           */
/*     RETURNS          : FWL_SUCCESS or FWL_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlStateGetStatefulTableEntries (UINT4 u4HashIndex,
                                 tStatefulSessionNode ** ppStateFilterInfo)
{
    INT4                i4RetVal = FWL_FAILURE;
    tStatefulSessionNode *pTempStateInfo = NULL;

    /* Check whether the Hash List has some entries in it. If so return those
     * entries or else move to the next Hash Bucket */
    if (TMO_SLL_Count ((&(FWL_STATE_HASH_LIST)->HashList[(u4HashIndex)]))
        != FWL_ZERO)
    {
        if (*ppStateFilterInfo == NULL)
        {
            pTempStateInfo = (tStatefulSessionNode *)
                TMO_HASH_Get_First_Bucket_Node (FWL_STATE_HASH_LIST,
                                                u4HashIndex);
        }
        else
        {
            pTempStateInfo = (tStatefulSessionNode *)
                TMO_HASH_Get_Next_Bucket_Node (FWL_STATE_HASH_LIST,
                                               u4HashIndex,
                                               (tTMO_HASH_NODE *)
                                               * ppStateFilterInfo);
        }
        if (pTempStateInfo != NULL)
        {
            *ppStateFilterInfo = pTempStateInfo;
            i4RetVal = FWL_SUCCESS;
        }
        else
        {
            *ppStateFilterInfo = NULL;
        }
    }
    else
    {
        *ppStateFilterInfo = NULL;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlStateGetIpv6StatefulTableEntries                */
/*                                                                           */
/*     DESCRIPTION      : This function gets the stateful Table Entries      */
/*                                                                           */
/*     INPUT            : u4HashIndex - Hash Index                           */
/*                                                                           */
/*     OUTPUT           : ppStateFilterInfo - Stateful Table entry           */
/*                                                                           */
/*     RETURNS          : FWL_SUCCESS or FWL_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlStateGetIpv6StatefulTableEntries (UINT4 u4HashIndex,
                                     tStatefulSessionNode ** ppStateFilterInfo)
{
    INT4                i4RetVal = FWL_FAILURE;
    tStatefulSessionNode *pTempStateInfo = NULL;

    /* Check whether the Hash List has some entries in it. If so return those
     * entries or else move to the next Hash Bucket */
    if (TMO_SLL_Count ((&(FWL_STATE_V6_HASH_LIST)->HashList[(u4HashIndex)])) !=
        FWL_ZERO)
    {
        if (*ppStateFilterInfo == NULL)
        {
            pTempStateInfo = (tStatefulSessionNode *)
                TMO_HASH_Get_First_Bucket_Node (FWL_STATE_V6_HASH_LIST,
                                                u4HashIndex);
        }
        else
        {
            pTempStateInfo = (tStatefulSessionNode *)
                TMO_HASH_Get_Next_Bucket_Node (FWL_STATE_V6_HASH_LIST,
                                               u4HashIndex,
                                               (tTMO_HASH_NODE *)
                                               * ppStateFilterInfo);
        }
        if (pTempStateInfo != NULL)
        {
            *ppStateFilterInfo = pTempStateInfo;
            i4RetVal = FWL_SUCCESS;
        }
        else
        {
            *ppStateFilterInfo = NULL;
        }
    }
    else
    {
        *ppStateFilterInfo = NULL;
    }
    return i4RetVal;

}

/****************************************************************************/
/*                           End of fwlstat.c                               */
/****************************************************************************/
