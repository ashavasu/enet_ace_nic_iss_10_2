/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlblkwt.c,v 1.10 2015/04/23 11:59:16 siva Exp $
 *
 * Description: File containing CLI routines for firewall module
 * *******************************************************************/

#ifndef FWLBLKWT_C_
#define FWLBLKWT_C_

#include "fwlinc.h"

PRIVATE INT4 WalkFnGetBlackListMatch PROTO ((tRBElem * pRBElem,
                                             eRBVisit visit,
                                             UINT4 u4Level, void *pArg,
                                             void *pOut));

PRIVATE INT4 WalkFnGetWhiteListMatch PROTO ((tRBElem * pRBElem,
                                             eRBVisit visit,
                                             UINT4 u4Level, void *pArg,
                                             void *pOut));

/* FIREWALL BlackList and WhiteList Initialization */
/*****************************************************************************
* Function Name      : FwlBlackWhiteInitLists                               
* Description        : This function is used to initialize the BlackList    
*                      and WhiteList RBTree.                                
*                      This function is called from FwlAclInit.   
* Input(s)           : None                                                 
* Output(s)          : None                                                 
* Global Variables                                                          
* Referred           : gFwlBlackList                                 
*                      gFwlWhiteList                                
* Global Variables                                                          
* Modified           : gFwlBlackList                                 
*                      gFwlWhiteList                                
* Return Value(s)    : None.                                                
*                                                                           
*****************************************************************************/
PUBLIC VOID
FwlBlackWhiteInitLists (VOID)
{
    /* gFwlBlackList maintains the Ipv4/Ipv6 Type, Address and Mask  
     * information which is to be black listed.
     */
    gFwlBlackList = RBTreeCreateEmbedded (FSAP_OFFSETOF (tFwlBlackListEntry,
                                                         FwlBlackListNode),
                                          BlackEntryCmp);
    if (gFwlBlackList == NULL)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "Creation of BlackList RBTree in firewall failed \r\n");
        return;
    }

    /* gFwlWhiteList maintains the Ipv4/Ipv6 Type, Address and Mask  
     * information which is to be White listed.
     */
    gFwlWhiteList = RBTreeCreateEmbedded (FSAP_OFFSETOF (tFwlWhiteListEntry,
                                                         FwlWhiteListNode),
                                          WhiteEntryCmp);
    if (gFwlWhiteList == NULL)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "Creation of WhiteList RBTree in firewall failed \r\n");
        return;
    }
    return;
}

/****************************************************************************
* Function Name      : FwlBlackWhiteDeInitLists                             
* Description        : This function is used to release all the memory      
*                      associated with BlackList and WhiteList Ip Address. 
*                      This function is invoked from FwlAclShutdown.             
* Input(s)           : None                                                 
* Output(s)          : None                                                 
* Global Variables                                                          
* Referred           : gFwlBlackList                                 
*                      gFwlWhiteList                                
* Global Variables                                                          
* Modified           : gFwlBlackList                                 
*                      gFwlWhiteList                                
* Return Value(s)    : None.                                                
*                                                                           
****************************************************************************/
PUBLIC VOID
FwlBlackWhiteDeInitLists (VOID)
{
    if (NULL != gFwlBlackList)
    {
        RBTreeDelete (gFwlBlackList);
    }
    if (NULL != gFwlWhiteList)
    {
        RBTreeDelete (gFwlWhiteList);
    }
    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : BlackEntryCmp
*
*    DESCRIPTION      : Function to compare the input keys (IPVX Address, its
*                       type and subnet mask) and returns the comparision
*                       output using which the BlackList Table gets traversed 
*                       for BlackList table walk purpose.
*
*    INPUT            : prbElem1 - Input Key 1
*                       prbElem2 - Input Key 2
*
*    OUTPUT           : None
*
*    RETURNS          : 0  if keys of both the elements are same.
*                       -1 if key of first element is less than second
*                          element key.
*                       1  if key of first element is greater than second
*                          element key.
****************************************************************************/
PUBLIC INT4
BlackEntryCmp (tRBElem * prbElem1, tRBElem * prbElem2)
{
    tFwlBlackListEntry *pConfigEntry1 = (tFwlBlackListEntry *) prbElem1;
    tFwlBlackListEntry *pConfigEntry2 = (tFwlBlackListEntry *) prbElem2;
    INT4                i4CmpVal = FWL_ZERO;

    i4CmpVal = MEMCMP (&pConfigEntry1->FwlBlackListIpAddr,
                       &pConfigEntry2->FwlBlackListIpAddr, sizeof (tFwlIpAddr));
    if (i4CmpVal != FWL_ZERO)
    {
        return i4CmpVal;
    }
    else if (pConfigEntry1->u2PrefixLen > pConfigEntry2->u2PrefixLen)
    {
        return FWL_ONE;
    }
    else if (pConfigEntry1->u2PrefixLen < pConfigEntry2->u2PrefixLen)
    {
        return FWL_INVALID;
    }
    else
    {
        return FWL_ZERO;
    }
}

/****************************************************************************
*
*    FUNCTION NAME    : WhiteEntryCmp
*
*    DESCRIPTION      : Function to compare the input keys (IPVX Address, its
*                       type and subnet mask) and returns the comparision
*                       output using which the WhiteList Table gets traversed 
*                       for WhiteList table walk purpose.
*
*    INPUT            : prbElem1 - Input Key 1
*                       prbElem2 - Input Key 2
*
*    OUTPUT           : None
*
*    RETURNS          : 0  if keys of both the elements are same.
*                       -1 if key of first element is less than second
*                          element key.
*                       1  if key of first element is greater than second
*                          element key.
****************************************************************************/
PUBLIC INT4
WhiteEntryCmp (tRBElem * prbElem1, tRBElem * prbElem2)
{
    tFwlWhiteListEntry *pConfigEntry1 = (tFwlWhiteListEntry *) prbElem1;
    tFwlWhiteListEntry *pConfigEntry2 = (tFwlWhiteListEntry *) prbElem2;
    INT4                i4CmpVal = FWL_ZERO;

    i4CmpVal = MEMCMP (&pConfigEntry1->FwlWhiteListIpAddr,
                       &pConfigEntry2->FwlWhiteListIpAddr, sizeof (tFwlIpAddr));
    if (i4CmpVal != FWL_ZERO)
    {
        return i4CmpVal;
    }
    else if (pConfigEntry1->u2PrefixLen > pConfigEntry2->u2PrefixLen)
    {
        return FWL_ONE;
    }
    else if (pConfigEntry1->u2PrefixLen < pConfigEntry2->u2PrefixLen)
    {
        return FWL_INVALID;
    }
    else
    {
        return FWL_ZERO;
    }
}

/*****************************************************************************
*  Function Name   : FwlBlackListEntryCreate                                
*  Description     : This function creates the BlackList  entry in the      
*                    BlackList database.                                    
*  Input(s)        : i4AddrType - Address Type (IPv4/IPv6)                  
*                    pIpAddr    - BlackList IpAddress                       
*                    u4IpMask   - BlackList Subnet Mask                     
*                    i4RowStatus   - Row status value.                      
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlBlackList. 
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  FWL_FAILURE on failure                    
*                                 FWL_SUCCESS on success                    
*****************************************************************************/
PUBLIC INT4
FwlBlackListEntryCreate (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                         UINT4 u4IpMask, INT4 i4EntryType)
{
    tFwlBlackListEntry *pFwlBlackListNode = NULL;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    /* Allocate memory for the Firewall BlackList entry. */
    pFwlBlackListNode = (tFwlBlackListEntry *) MemAllocMemBlk
        (FWL_BLACK_LIST_PID);
    if (NULL == pFwlBlackListNode)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "FwlBlackListNode Alloc Failed\r\n");
        return FWL_FAILURE;
    }

    pFwlBlackListNode->FwlBlackListIpAddr.u4AddrType = (UINT4) i4AddrType;

    if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
    {
        i4RetVal = FWL_INET_ATON (pIpAddr->pu1_OctetList, &Addr);
        if (i4RetVal == SNMP_FAILURE)
        {
            MemReleaseMemBlock (FWL_BLACK_LIST_PID,
                                (UINT1 *) pFwlBlackListNode);
            return FWL_FAILURE;
        }
        pFwlBlackListNode->FwlBlackListIpAddr.v4Addr = OSIX_NTOHL (Addr.u4Addr);
    }
    else
    {
        INET_ATON6 ((CHR1 *) pIpAddr->pu1_OctetList, &IP6Addr);
        MEMCPY (pFwlBlackListNode->FwlBlackListIpAddr.v6Addr.u1_addr,
                IP6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }

    pFwlBlackListNode->u2PrefixLen = (UINT2) u4IpMask;

    pFwlBlackListNode->i4EntryType = i4EntryType;

    i4RetVal = (INT4) RBTreeAdd (gFwlBlackList, (tRBElem *) pFwlBlackListNode);
    if (i4RetVal == FWL_FAILURE)
    {
        return FWL_FAILURE;
    }

    pFwlBlackListNode->i4RowStatus = FWL_ACTIVE;

    return FWL_SUCCESS;
}

/*****************************************************************************
*  Function Name   : FwlBlackListEntryDelete                                
*  Description     : This function deletes the BlackList  entry in the      
*                    BlackList database.                                    
*  Input(s)        : i4AddrType - Address Type (IPv4/IPv6)                  
*                    pIpAddr    - BlackList IpAddress                       
*                    u4IpMask   - BlackList Subnet Mask                     
*                    i4RowStatus   - Row status value.                      
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlBlackList
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  FWL_FAILURE on failure                    
*                                 FWL_SUCCESS on success                    
*****************************************************************************/
PUBLIC INT4
FwlBlackListEntryDelete (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                         UINT4 u4IpMask)
{
    tFwlBlackListEntry *pFwlBlackListNode = NULL;

    /* Get the RBTree Node for the particular indices */
    pFwlBlackListNode = FwlGetBlackListEntry (i4AddrType, pIpAddr, u4IpMask);

    if (NULL == pFwlBlackListNode)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "The given BlackList entry is not found \r\n");
        return FWL_FAILURE;
    }

    pFwlBlackListNode->u4BlkListHitCount = FWL_ZERO;

    /* Remove the node if it is available */
    RBTreeRem (gFwlBlackList, (tRBElem *) pFwlBlackListNode);
    MemReleaseMemBlock (FWL_BLACK_LIST_PID, (UINT1 *) pFwlBlackListNode);

    return FWL_SUCCESS;
}

/*****************************************************************************
*  Function Name   : FwlGetBlackListEntry
*  Description     : This function gets the BlackList entry for the given 
*                    indices in the BlackList database.                                    
*  Input(s)        : i4AddrType - Address Type (IPv4/IPv6)                  
*                    pIpAddr    - BlackList IpAddress                       
*                    u4PrefLen   - BlackList Subnet Mask                     
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlBlackList. 
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  NULL on failure                    
*                                 pFwlBlackListNode on success                    
*****************************************************************************/
PUBLIC tFwlBlackListEntry *
FwlGetBlackListEntry (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                      UINT4 u4PrefLen)
{
    tFwlBlackListEntry *pFwlBlackListNode = NULL;
    tFwlBlackListEntry  FwlBlackListInfo;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&FwlBlackListInfo, FWL_ZERO, sizeof (tFwlBlackListEntry));
    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    FwlBlackListInfo.FwlBlackListIpAddr.u4AddrType = (UINT4) i4AddrType;

    if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
    {
        i4RetVal = FWL_INET_ATON (pIpAddr->pu1_OctetList, &Addr);
        if (i4RetVal == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        FwlBlackListInfo.FwlBlackListIpAddr.v4Addr = OSIX_NTOHL (Addr.u4Addr);

    }
    else
    {
        INET_ATON6 ((CHR1 *) pIpAddr->pu1_OctetList, &IP6Addr);
        MEMCPY (FwlBlackListInfo.FwlBlackListIpAddr.v6Addr.u1_addr,
                IP6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }

    FwlBlackListInfo.u2PrefixLen = (UINT2) u4PrefLen;

    pFwlBlackListNode = ((tFwlBlackListEntry *) RBTreeGet (gFwlBlackList,
                                                           (tRBElem *) &
                                                           FwlBlackListInfo));

    return pFwlBlackListNode;
}

/*****************************************************************************
*  Function Name   : FwlWhiteListEntryCreate                                
*  Description     : This function creates the WhiteList  entry in the      
*                    WhiteList database.                                    
*  Input(s)        : i4AddrType - Address Type (IPv4/IPv6)                  
*                    pIpAddr    - WhiteList IpAddress                       
*                    u4PrefLen   - WhiteList Subnet Mask                     
*                    i4RowStatus   - Row status value.                      
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlWhiteList. 
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  FWL_FAILURE on failure                    
*                                 FWL_SUCCESS on success                    
*****************************************************************************/
PUBLIC INT4
FwlWhiteListEntryCreate (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                         UINT4 u4PrefLen)
{
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    /* Allocate memory for the Firewall WhiteList entry. */
    pFwlWhiteListNode =
        (tFwlWhiteListEntry *) MemAllocMemBlk (FWL_WHITE_LIST_PID);
    if (NULL == pFwlWhiteListNode)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "FwlWhiteListNode Alloc Failed\r\n");
        return FWL_FAILURE;
    }

    pFwlWhiteListNode->FwlWhiteListIpAddr.u4AddrType = (UINT4) i4AddrType;

    if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
    {
        i4RetVal = FWL_INET_ATON (pIpAddr->pu1_OctetList, &Addr);
        if (i4RetVal == SNMP_FAILURE)
        {
            MemReleaseMemBlock (FWL_WHITE_LIST_PID,
                                (UINT1 *) pFwlWhiteListNode);
            return FWL_FAILURE;
        }
        pFwlWhiteListNode->FwlWhiteListIpAddr.v4Addr = OSIX_NTOHL (Addr.u4Addr);
    }
    else
    {
        INET_ATON6 ((CHR1 *) pIpAddr->pu1_OctetList, &IP6Addr);
        MEMCPY (pFwlWhiteListNode->FwlWhiteListIpAddr.v6Addr.u1_addr,
                IP6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }

    pFwlWhiteListNode->u2PrefixLen = (UINT2) u4PrefLen;

    i4RetVal = (INT4) RBTreeAdd (gFwlWhiteList, (tRBElem *) pFwlWhiteListNode);
    if (i4RetVal == FWL_FAILURE)
    {
        return FWL_FAILURE;
    }

    pFwlWhiteListNode->i4RowStatus = FWL_ACTIVE;

    return FWL_SUCCESS;

}

/*****************************************************************************
*  Function Name   : FwlWhiteListEntryDelete                                
*  Description     : This function deletes the WhiteList  entry in the      
*                    WhiteList database.                                    
*  Input(s)        : i4AddrType - Address Type (IPv4/IPv6)                  
*                    pIpAddr    - WhiteList IpAddress                       
*                    u4PrefLen   - WhiteList Subnet Mask                     
*                    i4RowStatus   - Row status value.                      
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlWhiteList
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  FWL_FAILURE on failure                    
*                                 FWL_SUCCESS on success                    
*****************************************************************************/
PUBLIC INT4
FwlWhiteListEntryDelete (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                         UINT4 u4PrefLen)
{
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;

    /* Get the RBTree Node for the particular indices */
    pFwlWhiteListNode = FwlGetWhiteListEntry (i4AddrType, pIpAddr, u4PrefLen);

    if (NULL == pFwlWhiteListNode)
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                 "The given WhiteList entry is not found \r\n");
        return FWL_FAILURE;
    }

    pFwlWhiteListNode->u4WhiteListHitCount = FWL_ZERO;

    /* Remove the node if it is available */
    RBTreeRem (gFwlWhiteList, (tRBElem *) pFwlWhiteListNode);
    MemReleaseMemBlock (FWL_WHITE_LIST_PID, (UINT1 *) pFwlWhiteListNode);
    return FWL_SUCCESS;
}

/*****************************************************************************
*  Function Name   : FwlGetWhiteListEntry
*  Description     : This function gets the WhiteList entry for the given 
*                    indices in the WhiteList database.                                    
*  Input(s)        : i4AddrType - Address Type (IPv4/IPv6)                  
*                    pIpAddr    - WhiteList IpAddress                       
*                    u4PrefLen   - WhiteList Subnet Mask                     
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlWhiteList. 
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  NULL on failure                    
*                                 pFwlWhiteListNode on success                    
*****************************************************************************/
PUBLIC tFwlWhiteListEntry *
FwlGetWhiteListEntry (INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                      UINT4 u4PrefLen)
{
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;
    tFwlWhiteListEntry  FwlWhiteListInfo;
    tUtlInAddr          Addr;
    tUtlIn6Addr         IP6Addr;
    INT4                i4RetVal = FWL_ZERO;

    MEMSET (&FwlWhiteListInfo, FWL_ZERO, sizeof (tFwlWhiteListEntry));
    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));

    FwlWhiteListInfo.FwlWhiteListIpAddr.u4AddrType = (UINT4) i4AddrType;

    if (IPVX_ADDR_FMLY_IPV4 == i4AddrType)
    {
        i4RetVal = FWL_INET_ATON (pIpAddr->pu1_OctetList, &Addr);
        if (i4RetVal == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        FwlWhiteListInfo.FwlWhiteListIpAddr.v4Addr = OSIX_NTOHL (Addr.u4Addr);

    }
    else
    {
        INET_ATON6 ((CHR1 *) pIpAddr->pu1_OctetList, &IP6Addr);
        MEMCPY (FwlWhiteListInfo.FwlWhiteListIpAddr.v6Addr.u1_addr,
                IP6Addr.u1addr, IPVX_IPV6_ADDR_LEN);
    }

    FwlWhiteListInfo.u2PrefixLen = (UINT2) u4PrefLen;

    pFwlWhiteListNode = ((tFwlWhiteListEntry *) RBTreeGet (gFwlWhiteList,
                                                           (tRBElem *) &
                                                           FwlWhiteListInfo));

    return pFwlWhiteListNode;

}

/*****************************************************************************
*  Function Name   : FwlHandleBlackListInfoFromIDS
*  Description     : This function gets the linear buffer and extracts the 
*                    information which is to be black listed from IDS module
*                    and adds the address to BlackList DataStructure.
*                    pu1LinearBuf - Packet consist of blackList information.
*  Output(s)       : None
*                  :
*  <OPTIONAL Fields>           : None
*  Global Variables Referred   : None
*  Global variables Modified   : None
*  Exceptions or Operating System Error Handling : None
*  Use of Recursion            :  None
*  Returns                     :  FWL_FAILURE on failure
*                                 FWL_SUCCESS on success
*****************************************************************************/
PUBLIC INT4
FwlHandleBlackListInfoFromIDS (UINT1 *pu1LinearBuf)
{
    UINT1              *pu1PktBuf = pu1LinearBuf;
    UINT1               u1Version = FWL_ZERO;
    UINT4               u4PrefLen = FWL_ZERO;
    UINT2               u2VlanTag = FWL_ZERO;
    INT4                i4AddrType = FWL_ZERO;
    tFwlBlackListEntry *pFwlBlackListNode = NULL;
    UINT1               au1IpvxAddr[FWL_IPV6_ADDR_LEN] = { FWL_ZERO };
    tUtlInAddr          Addr = { FWL_ZERO };
    tUtlIn6Addr         In6Addr;
    tIp6Addr            I6Addr;
    tSNMP_OCTET_STRING_TYPE BlkListIpAddr;

    MEMSET (&Addr, FWL_ZERO, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));
    MEMSET (&I6Addr, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&BlkListIpAddr, FWL_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    BlkListIpAddr.pu1_OctetList = au1IpvxAddr;

    /*Moving the offset to 12 bytes to check for Vlan tag */
    pu1PktBuf += CFA_VLAN_TAG_OFFSET;
    MEMCPY (&u2VlanTag, pu1PktBuf, 2);
    u2VlanTag =(UINT2) OSIX_NTOHS(u2VlanTag);
    if(u2VlanTag == CFA_VLAN_PROTOCOL_ID)
    {
        /*In case of IVR moving the offest to 18 bytes(srcAddr(6)+destAddr(6)+vlanTag(4)+ipProtoByte(2)) to retrieve ip version. */
        pu1PktBuf += 6*sizeof(UINT1);
    }
    else
    {
        /*In case of router port moving the offest to 14bytes(srcAddr(6)+destAddr(6)+ipProtoByte(2)) to retrieve ip version. */
       pu1PktBuf += 2*sizeof(UINT1);
    }
    FWL_GET_LINEAR_1BYTE (u1Version, pu1PktBuf);
    pu1PktBuf += FWL_ONE;
    u1Version = (UINT1) ((u1Version & IP_HEADER_VER_MASK) >>
                         IP_HDR_VER_OFFSET_BITS);

    if (u1Version == IP_VERSION_4)
    {
        /* If version value is 4. This is IPV4 packet */
        pu1PktBuf += (IP_PKT_OFF_SRC - FWL_ONE);
        FWL_GET_LINEAR_4BYTE (Addr.u4Addr, pu1PktBuf);
        pu1PktBuf += FWL_FOUR;

        Addr.u4Addr = OSIX_NTOHL (Addr.u4Addr);
        BlkListIpAddr.i4_Length = FWL_IPV4_ADDR_LEN;
        STRNCPY (BlkListIpAddr.pu1_OctetList, (UINT1 *) INET_NTOA (Addr),
                 (size_t) BlkListIpAddr.i4_Length);

        i4AddrType = IPVX_ADDR_FMLY_IPV4;
        u4PrefLen = IPVX_IPV4_MAX_MASK_LEN;
    }
    else if (u1Version == IPV6_HEADER_VERSION)
    {
        /* If version value is 6. This is IPV6 packet */
        pu1PktBuf += (IP6_OFFSET_FOR_SRCADDR_FIELD - FWL_ONE);

        BlkListIpAddr.i4_Length = FWL_IPV6_ADDR_LEN;
        MEMCPY (I6Addr.u4_addr, pu1PktBuf, IPVX_IPV6_ADDR_LEN);
        MEMCPY (In6Addr.u1addr, I6Addr.u1_addr, IPVX_IPV6_ADDR_LEN);
        STRNCPY (BlkListIpAddr.pu1_OctetList,
                 (UINT1 *) INET_NTOA6 (In6Addr),
                 (size_t) BlkListIpAddr.i4_Length);

        i4AddrType = IPVX_ADDR_FMLY_IPV6;
        u4PrefLen = IP6_ADDR_MAX_PREFIX;
    }
    /* Trap sent to indicate attack-packet identified by IDS */
    IdsSendAttackPktTrap (BlkListIpAddr.pu1_OctetList);
    pFwlBlackListNode = FwlGetBlackListEntry (i4AddrType, &BlkListIpAddr,
                                              u4PrefLen);

    if (NULL == pFwlBlackListNode)
    {
        if (FWL_SUCCESS != FwlBlackListEntryCreate (i4AddrType,
                                                    &BlkListIpAddr,
                                                    u4PrefLen,
                                                    FWL_BLACKLIST_DYNAMIC))
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                     "%% ERROR: Black List Dynamic Addition Failed. \r\n");
            return FWL_FAILURE;
        }
    }

    return FWL_SUCCESS;

}

/*****************************************************************************
*  Function Name   : FwlMatchBlackListEntry
*  Description     : This function returns success if blackList entry is found
*                    for the given SrcAddress/DestAddress.
*  Input(s)        : u1Direction - FlowDirection FWL_DIRECTION_IN/ 
*                                  FWL_DIRECTION_OUT
*                    SrcAddr     - Source Address of the Packet (In/Out).
*                    DestAddr    - Destination Address of the Packet (In/Out).
*  Output(s)       : None
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlBlackList. 
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  FWL_FAILURE on failure                    
*                                 FWL_SUCCESS on success                    
*****************************************************************************/

PUBLIC INT4
FwlMatchBlackListEntry (UINT1 u1Direction, tFwlIpAddr SrcAddr,
                        tFwlIpAddr DestAddr)
{
    INT4                i4RetVal = FWL_FAILURE;
    UINT4               u4Count = FWL_ZERO;
    tFwlBlackListEntry *pFwlBlackListNode = NULL;

    RBTreeCount (gFwlBlackList, &u4Count);

    if (u4Count)
    {
        switch (u1Direction)
        {
            case FWL_DIRECTION_IN:
                RBTreeWalk (gFwlBlackList, (tRBWalkFn) WalkFnGetBlackListMatch,
                            &SrcAddr, &pFwlBlackListNode);
                if (NULL != pFwlBlackListNode)
                {
                    i4RetVal = FWL_SUCCESS;
                }
                break;
            case FWL_DIRECTION_OUT:
                RBTreeWalk (gFwlBlackList, (tRBWalkFn) WalkFnGetBlackListMatch,
                            &DestAddr, &pFwlBlackListNode);

                if (NULL != pFwlBlackListNode)
                {
                    i4RetVal = FWL_SUCCESS;
                }
                break;
            default:
                break;
        }
    }
    return i4RetVal;
}

/*****************************************************************************
*  Function Name   : FwlMatchWhiteListEntry
*  Description     : This function returns success if blackList entry is found
*                    for the given SrcAddress/DestAddress.                                 
*  Input(s)        : u1Direction - FlowDirection FWL_DIRECTION_IN/
*                                  FWL_DIRECTION_OUT
*                    SrcAddr     - Source Address of the Packet (In/Out).
*                    DestAddr    - Destination Address of the Packet (In/Out).
*  Output(s)       : None                                                   
*                  :                                                        
*  <OPTIONAL Fields>           : None                                       
*  Global Variables Referred   : gFwlWhiteList. 
*  Global variables Modified   : None                                       
*  Exceptions or Operating System Error Handling : None                     
*  Use of Recursion            :  None                                      
*  Returns                     :  FWL_FAILURE on failure             
*                                 FWL_SUCCESS on success                    
*****************************************************************************/

PUBLIC INT4
FwlMatchWhiteListEntry (UINT1 u1Direction, tFwlIpAddr SrcAddr,
                        tFwlIpAddr DestAddr)
{
    INT4                i4RetVal = FWL_FAILURE;
    UINT4               u4Count = FWL_ZERO;
    tFwlWhiteListEntry *pFwlWhiteListNode = NULL;

    RBTreeCount (gFwlWhiteList, &u4Count);

    if (u4Count)
    {
        switch (u1Direction)
        {
            case FWL_DIRECTION_IN:
                RBTreeWalk (gFwlWhiteList, (tRBWalkFn) WalkFnGetWhiteListMatch,
                            &SrcAddr, &pFwlWhiteListNode);
                if (NULL != pFwlWhiteListNode)
                {
                    i4RetVal = FWL_SUCCESS;
                }
                break;
            case FWL_DIRECTION_OUT:
                RBTreeWalk (gFwlWhiteList, (tRBWalkFn) WalkFnGetWhiteListMatch,
                            &DestAddr, &pFwlWhiteListNode);

                if (NULL != pFwlWhiteListNode)
                {
                    i4RetVal = FWL_SUCCESS;
                }
                break;
            default:
                break;
        }
    }
    return i4RetVal;
}

/****************************************************************************
*
*    FUNCTION NAME    : WalkFnGetBlackListMatch
*
*    DESCRIPTION      : Walk action routine to get the black list Node, if   
*                       the corresponding blacklist IP Address matches with 
*                       the given ip address.
*
*    INPUT            : pRBElem -  pointer to the current Node
*                       visit   - indicates the order of the tree traversal.
*                       u4Level - indicates the present level of the tree
*                       pArg    - Argument passed by the user function
*
*    OUTPUT           : pOut    - Output that needs to be indicated to the
*                                 user function (Pointer to the
*                                 pBlackListNode or NULL)
*
*    RETURNS          : RB_WALK_BREAK / RB_WALK_CONT
*
****************************************************************************/

PRIVATE INT4
WalkFnGetBlackListMatch (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                         void *pArg, void *pOut)
{
    tFwlBlackListEntry *pBlackListNode = NULL;
    UINT4               u4Mask = FWL_ZERO;
    INT4                i4AddrIndex = FWL_ZERO;
    INT1                i1MatchFound = FWL_TRUE;
    tIp6Addr            Mask;
    tFwlIpAddr          FwlAddr;

    MEMSET (&Mask, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&FwlAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    UNUSED_PARAM (u4Level);
    FwlAddr = *(tFwlIpAddr *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pBlackListNode = (tFwlBlackListEntry *) pRBElem;

            if (IPVX_ADDR_FMLY_IPV4 ==
                pBlackListNode->FwlBlackListIpAddr.u4AddrType)
            {
                IPV4_MASKLEN_TO_MASK (u4Mask, pBlackListNode->u2PrefixLen);
                if (((pBlackListNode->FwlBlackListIpAddr.v4Addr & u4Mask) ==
                     (FwlAddr.v4Addr & u4Mask))
                    && (pBlackListNode->FwlBlackListIpAddr.v4Addr <=
                        FwlAddr.v4Addr))
                {
                    /* The given address is matched with BlackList Entry */
                    pBlackListNode->u4BlkListHitCount++;
                    *(tFwlBlackListEntry **) pOut = pBlackListNode;
                    return RB_WALK_BREAK;
                }
            }
            else if (IPVX_ADDR_FMLY_IPV6 ==
                     pBlackListNode->FwlBlackListIpAddr.u4AddrType)
            {
                GET_IPV6_MASK (pBlackListNode->u2PrefixLen, Mask.u4_addr);
                for (i4AddrIndex = FWL_ZERO; i4AddrIndex < FWL_FOUR;
                     i4AddrIndex++)
                {
                    if (((pBlackListNode->FwlBlackListIpAddr.v6Addr.
                          u4_addr[i4AddrIndex] & Mask.u4_addr[i4AddrIndex]) !=
                         (FwlAddr.v6Addr.u4_addr[i4AddrIndex] & Mask.
                          u4_addr[i4AddrIndex]))
                        || (pBlackListNode->FwlBlackListIpAddr.v6Addr.
                            u4_addr[i4AddrIndex] >
                            FwlAddr.v6Addr.u4_addr[i4AddrIndex]))
                    {
                        i1MatchFound = FWL_FALSE;
                        break;
                    }
                }
                if (FWL_TRUE == i1MatchFound)
                {
                    pBlackListNode->u4BlkListHitCount++;
                    *(tFwlBlackListEntry **) pOut = pBlackListNode;
                    return RB_WALK_BREAK;
                }
            }
        }
    }
    return RB_WALK_CONT;
}

/****************************************************************************
*
*    FUNCTION NAME    : WalkFnGetWhiteListMatch
*
*    DESCRIPTION      : Walk action routine to get the white list Node, if   
*                       the corresponding whitelist IP Address matches with 
*                       the given ip address.
*
*    INPUT            : pRBElem -  pointer to the current Node
*                       visit   - indicates the order of the tree traversal.
*                       u4Level - indicates the present level of the tree
*                       pArg    - Argument passed by the user function
*
*    OUTPUT           : pOut    - Output that needs to be indicated to the
*                                 user function (Pointer to the
*                                 pWhiteListNode or NULL)
*
*    RETURNS          : RB_WALK_BREAK / RB_WALK_CONT
*
****************************************************************************/

PRIVATE INT4
WalkFnGetWhiteListMatch (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                         void *pArg, void *pOut)
{
    tFwlWhiteListEntry *pWhiteListNode = NULL;
    UINT4               u4Mask = FWL_ZERO;
    INT4                i4AddrIndex = FWL_ZERO;
    INT1                i1MatchFound = FWL_TRUE;
    tIp6Addr            Mask;
    tFwlIpAddr          FwlAddr;

    MEMSET (&Mask, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&FwlAddr, FWL_ZERO, sizeof (tFwlIpAddr));

    UNUSED_PARAM (u4Level);
    FwlAddr = *(tFwlIpAddr *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pWhiteListNode = (tFwlWhiteListEntry *) pRBElem;

            if (IPVX_ADDR_FMLY_IPV4 ==
                pWhiteListNode->FwlWhiteListIpAddr.u4AddrType)
            {
                IPV4_MASKLEN_TO_MASK (u4Mask, pWhiteListNode->u2PrefixLen);
                if (((pWhiteListNode->FwlWhiteListIpAddr.v4Addr & u4Mask) ==
                     (FwlAddr.v4Addr & u4Mask))
                    && (pWhiteListNode->FwlWhiteListIpAddr.v4Addr <=
                        FwlAddr.v4Addr))
                {
                    /* The given address is matched with WhiteList Entry */
                    pWhiteListNode->u4WhiteListHitCount++;
                    *(tFwlWhiteListEntry **) pOut = pWhiteListNode;
                    return RB_WALK_BREAK;
                }
            }
            else if (IPVX_ADDR_FMLY_IPV6 ==
                     pWhiteListNode->FwlWhiteListIpAddr.u4AddrType)
            {
                GET_IPV6_MASK (pWhiteListNode->u2PrefixLen, Mask.u4_addr);
                for (i4AddrIndex = FWL_ZERO; i4AddrIndex < FWL_FOUR;
                     i4AddrIndex++)
                {
                    if (((pWhiteListNode->FwlWhiteListIpAddr.v6Addr.
                          u4_addr[i4AddrIndex] & Mask.u4_addr[i4AddrIndex]) !=
                         (FwlAddr.v6Addr.u4_addr[i4AddrIndex] & Mask.
                          u4_addr[i4AddrIndex]))
                        || (pWhiteListNode->FwlWhiteListIpAddr.v6Addr.
                            u4_addr[i4AddrIndex] >
                            FwlAddr.v6Addr.u4_addr[i4AddrIndex]))
                    {
                        i1MatchFound = FWL_FALSE;
                        break;
                    }
                }
                if (FWL_TRUE == i1MatchFound)
                {
                    pWhiteListNode->u4WhiteListHitCount++;
                    *(tFwlWhiteListEntry **) pOut = pWhiteListNode;
                    return RB_WALK_BREAK;
                }
            }
        }
    }
    return RB_WALK_CONT;
}
#endif
