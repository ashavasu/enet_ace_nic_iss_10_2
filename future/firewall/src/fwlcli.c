/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlcli.c,v 1.42 2016/03/19 13:06:25 siva Exp $
 *
 * Description: File containing CLI routines for firewall module
 * *******************************************************************/

#ifndef __FWLCLI_C__
#define __FWLCLI_C__
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : fwlcli.c                                         |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Future Sofware Ltd                               |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : FIREWALL                                         |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for CLI firewall commands        |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#include "fwlinc.h"
#include "fsfwlwr.h"
#include "fsfwlcli.h"
#include "fssocket.h"
#ifdef FIREWALL_WANTED

#define   SYS_DEF_PORT_LIST_SIZE      ((SYS_DEF_MAX_INTERFACES + 31)/32 * 4)
INT4
cli_process_fwl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS] = { NULL };
    UINT1               au1PortList[SYS_DEF_PORT_LIST_SIZE] = { FWL_ZERO };
    UINT1               au1IfName[FWL_INTERFACE_NAME_LEN] = { FWL_ZERO };
    UINT4               u4IfIndex = FWL_ZERO;
    UINT4               u4Val = FWL_ZERO;
    UINT4               u4ErrCode = FWL_ZERO;
    UINT4               u4ListType = FWL_ZERO;
    UINT4               u4IpAddress = FWL_ZERO;
    UINT4               u4MemAllocateStatus = FWL_ZERO;
    INT4                i4RetStatus = FWL_ZERO;
    INT4                i4DosAttackType = FWL_ZERO;
    INT4                i4ProtoType = FWL_ZERO;
    INT4                i4TrafficMode = FWL_ZERO;
    INT4                i4RateLimit = FWL_ZERO;
    INT4                i4BurstSize = FWL_ZERO;
    INT4                i4PortNo = FWL_ZERO;
    INT4                i4HeaderLength = FWL_ZERO;
    INT1                i1Argno = FWL_ZERO;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Threshold = FWL_ZERO;
    INT4                i4RpfCheck = FWL_ZERO;
    INT4                i4RpfMode = FWL_ZERO;
    UINT2               u2PrefixLen = FWL_ZERO;
    UINT1              *pu1Inst = NULL;
    INT1                i1DotFnd = FWL_ZERO;
    INT1                i1ListHitCount = FWL_NOT_SET;
    tFwlAccessList     *pFwlAccessList = NULL;
    tFwlFilterEntry    *pFwlFilterEntry = NULL;
    tFwlIpAddr         *pAddr = NULL;
    tFwlIpAddr          AddrPrefix;
    tUtlInAddr          InAddr;
    tIp6Addr            DmzIp6Addr;
    tIp6Addr           *pIp6addr = NULL;
    va_start (ap, u4Command);

    InAddr.u4Addr = FWL_ZERO;
    MEMSET (&AddrPrefix, FWL_ZERO, sizeof (tFwlIpAddr));
    /* MEMSET (&FwlFilterEntry, FWL_ZERO, sizeof (tFwlFilterEntry)); */
    /* second arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store fourteen arguements at the max. This is because fwl commands do not
     * take more than fourteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (TRUE)
    {
        args[i1Argno++] = va_arg (ap, UINT1 *);
        if (i1Argno == FWL_TEN)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, FwlLock, FwlUnLock);
    FwlLock ();

    switch (u4Command)
    {
        case CLI_SET_FWL_STATUS:
            i4RetVal =
                FwlCliSetFwlStatus (CliHandle,
                                    (UINT4) CLI_ATOI (args[FWL_INDEX_0]));
            break;
        case CLI_SET_FWL_DOS_ENABLE:
            
            i4DosAttackType = (CLI_PTR_TO_I4 (args[0]));
            i4HeaderLength  = FWL_TCP_MIN_HDR;
            i4PortNo  = (CLI_PTR_TO_I4 (args[1]));
            i4RetVal =
                FwlCliSetFwlDosAttack (CliHandle,i4DosAttackType,FWL_ONE,
										i4HeaderLength,i4PortNo);
            break;
        case CLI_SET_FWL_DOS_DISABLE:
            
            i4DosAttackType = (CLI_PTR_TO_I4 (args[0]));
            i4PortNo = (CLI_PTR_TO_I4 (args[1]));
            i4RetVal =
                FwlCliSetFwlDosAttack (CliHandle,i4DosAttackType,FWL_ZERO,
										i4HeaderLength,i4PortNo);
            break;
        case CLI_SET_FWL_RATE_LIMIT:

            i4ProtoType = (CLI_PTR_TO_I4 (args[0]));
            i4TrafficMode = (CLI_PTR_TO_I4 (args[3]));
             if ((args[2]) != NULL)
            {
                i4RateLimit = *(INT4 *) (VOID *) args[2];
                i4BurstSize = *(INT4 *) (VOID *) args[2];

            }
            if ((args[1]) != NULL)
            {
                i4PortNo = (CLI_PTR_TO_I4 (args[1]));
            }
			else
			{
				if (i4ProtoType == FWL_RATE_LIMIT_TCP)
				{
					i4PortNo = FWL_DEF_TCP_PORT; 
				}
				else if(i4ProtoType == FWL_RATE_LIMIT_UDP)
				{
					i4PortNo = FWL_DEF_UDP_PORT;
				}
				else
				{
					i4PortNo = FWL_DEF_ICMP_PORT;
				}
			}

        
            i4RetVal =
                FwlCliSetFwlRateLimit (CliHandle,i4ProtoType,
											i4PortNo,i4RateLimit,
										i4BurstSize,i4TrafficMode);
            break;

         case CLI_RESET_FWL_RATE_LIMIT:

            i4ProtoType = (CLI_PTR_TO_I4 (args[0]));
            if ((args[1]) != NULL)
            {
                i4PortNo = (CLI_PTR_TO_I4 (args[1]));
            }
			else
			{
				if (i4ProtoType == FWL_RATE_LIMIT_TCP)
				{
					i4PortNo = FWL_DEF_TCP_PORT; 
				}
				else if(i4ProtoType == FWL_RATE_LIMIT_UDP)
				{
					i4PortNo = FWL_DEF_UDP_PORT;
				}
				else
				{
					i4PortNo = FWL_DEF_ICMP_PORT;
				}
		    }

        
            
            i4RetVal =
                FwlCliReSetFwlRateLimit (CliHandle,i4ProtoType,
											i4PortNo);
            break;

        case CLI_SET_FWL_ICMP_ERROR:
            i4RetVal =
                FwlCliSetIcmpErrorMessage (CliHandle,
                                           (UINT4)
                                           CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_ICMPv6_ERROR:
            i4RetVal =
                FwlCliSetIcmpv6ErrorMessage (CliHandle,
                                             (UINT4)
                                             CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_VERIFY_REV_PATH:
            i4RetVal =
                FwlCliSetIPSpoofing (CliHandle,
                                     (UINT4) CLI_ATOI (args[FWL_INDEX_0]));
            break;
       case CLI_SET_FWL_RPF:
            i4RpfCheck = (INT4) CLI_ATOI (args[FWL_INDEX_0]);
            i4RpfMode = CLI_PTR_TO_I4 (args[1]);

            i4RetVal =
                FwlCliSetRpfCheck (CliHandle,i4RpfCheck,i4RpfMode);
            break;


        case CLI_SET_FWL_IPV6_VERIFY_REV_PATH:
            i4RetVal =
                FwlCliSetIPv6Spoofing (CliHandle,
                                       (UINT4) CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_IP_SRC_ROUTE:
            i4RetVal =
                FwlCliSetIPSourceRoute (CliHandle,
                                        (UINT4) CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_INSPECT_TCP:
            i4RetVal =
                FwlCliSetTcpIntercept (CliHandle,
                                       (UINT4) CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_TCP_HALF_OPEN:
            i4RetVal =
                FwlCliSetTcpInterceptThreshold (CliHandle,
                                                *((UINT4 *) (VOID *)
                                                  args[FWL_INDEX_0]));

            break;

        case CLI_SET_FWL_TCP_SYN_WAIT:
            i4RetVal =
                FwlCliSetTcpSynWait (CliHandle,
                                     *((UINT4 *) (VOID *) args[FWL_INDEX_0]));
            break;

        case CLI_ADD_FWL_FILTER:
            /*
             * args[0] = Firewall filter name
             * args[1] = Source IP range - for ex 10.0.0.0/16 | any
             * args[2] = Destination IP range | any
             * args[3] = Protocol filter is: 
             *           tcp  | udp  |
             *           icmp | igmp | ggp  | ip   | egp |
             *           igp  | nvp  | irtp | idpr | rsvp |
             *           mhrp | igrp | ospf
             *
             * args[4] = Source Port range - for ex. >=23
             * args[5] = Destination Port range - for ex. >1023
             * args[6] = established
             * args[7] = reset
             *
             */
            u4MemAllocateStatus = MemAllocateMemBlock (FWL_CLI_FILTER_PID,
                                                       (UINT1 **) (VOID *)
                                                       &pFwlFilterEntry);

            if (u4MemAllocateStatus == MEM_FAILURE)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                         "\n Memory Pool Exhausts for Rule List - "
                         "Dynamic Allocation Done \n");
            }

            if (pFwlFilterEntry == NULL)
            {
                CliPrintf (CliHandle, "unable to allocate buffer\n");
                return CLI_FAILURE;
            }
            /* Copy the firewall fiter name which is the index of the table */
            if (CLI_STRLEN (args[FWL_INDEX_0]) >
                FWL_MAX_FILTER_NAME_LEN - FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is "
                           "too long !\r\n");

                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pFwlFilterEntry->au1FilterName, args[FWL_INDEX_0]);

            if (CLI_STRCMP (pFwlFilterEntry->au1FilterName, "fildef") ==
                FWL_ZERO)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name has "
                           "to be other than fildef !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            /* If the source and destination address is specified as "any"
             * use 0.0.0.0/0 as range
             */

            /*Source IP address range */
            if (STRNCASECMP (args[FWL_INDEX_1], "any", FWL_THREE_BYTES) ==
                FWL_ZERO)
            {
                CLI_STRCPY (pFwlFilterEntry->au1SrcAddress, "0.0.0.0/0");
            }
            else
            {
                if (STRLEN (args[FWL_INDEX_1]) > FWL_MAX_ADDR_LEN)
                {
                    CliPrintf (CliHandle, "%%ERROR: Maximum source address  "
                               "length can be 85 !\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                CLI_STRCPY (pFwlFilterEntry->au1SrcAddress, args[FWL_INDEX_1]);
            }

            /* Dest IP address range */
            if (STRNCASECMP (args[FWL_INDEX_2], "any", FWL_THREE_BYTES) ==
                FWL_ZERO)
            {
                CLI_STRCPY (pFwlFilterEntry->au1DestAddress, "0.0.0.0/0");
            }
            else
            {
                if (CLI_STRLEN (args[FWL_INDEX_2]) < FWL_MAX_ADDR_LEN)
                {
                    CLI_STRCPY (pFwlFilterEntry->au1DestAddress,
                                args[FWL_INDEX_2]);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "%%ERROR: Maximum destination address  "
                               "length can be 85 !\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;

                }
            }
            /* Protocol - for ex. tcp, udp, ip, etc */
            if ((args[FWL_INDEX_3] != NULL) && (CLI_STRCMP (args[FWL_INDEX_3],
                                                            "other") !=
                                                FWL_ZERO))
            {
                pFwlFilterEntry->u4IsProtoPres = TRUE;
                if ((pFwlFilterEntry->u4Protocol =
                     (UINT4) cli_get_fwl_filter_protocol (args[FWL_INDEX_3])) ==
                    FWL_ZERO)
                {
                    CliPrintf (CliHandle, "%%ERROR: Invalid Protocol !\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }
            else if ((args[FWL_INDEX_3] != NULL)
                     && (CLI_STRCMP (args[FWL_INDEX_3], "other") == FWL_ZERO))
            {
                pFwlFilterEntry->u4IsProtoPres = TRUE;
                pFwlFilterEntry->u4Protocol = *(UINT4 *) (VOID *)
                    args[FWL_INDEX_4];
                /* other proto */
            }
            else
            {
                pFwlFilterEntry->u4IsProtoPres = FALSE;
                pFwlFilterEntry->u4Protocol = FWL_PROTO_ANY;    /* default proto */
            }
            /* User has specified the src port range */
            if (args[FWL_INDEX_5] != NULL)
            {
                /* If the source port range is specified as "any" set >1
                 * If protocol is other than udp/tcp/any then dont set source port */
                if ((args[FWL_INDEX_3] != NULL) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") != FWL_ZERO))
                {
                    CliPrintf (CliHandle,
                               "%%ERROR: Source-Port entry for TCP, UDP"
                               " protocols only!\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                if (STRNCASECMP (args[FWL_INDEX_5], "any", FWL_THREE_BYTES) ==
                    FWL_ZERO)
                {
                    pFwlFilterEntry->u4SrcPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1SrcPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4SrcPortPres = TRUE;
                    if (STRLEN (args[FWL_INDEX_5]) > FWL_MAX_PORT_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "%%ERROR: Maximum source address range "
                                   "length can be 12 !\r\n");
                        i4RetVal = CLI_FAILURE;
                        break;

                    }
                    CLI_STRCPY (pFwlFilterEntry->au1SrcPortRange,
                                args[FWL_INDEX_5]);
                }
            }
            else
            {
                if ((args[FWL_INDEX_3] == NULL) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") == FWL_ZERO))
                {
                    pFwlFilterEntry->u4SrcPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1SrcPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4SrcPortPres = FALSE;
                }
            }

            /* User has specified the dest port range */
            if (args[FWL_INDEX_6] != NULL)
            {
                /* If the destination port range is specified as "any" set >1
                 * If protocol is other than udp/tcp/any then dont set dest-port */
                if ((args[FWL_INDEX_3] != NULL) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") != FWL_ZERO))
                {
                    CliPrintf (CliHandle, "%%ERROR: Destination-Port entry"
                               "for TCP,UDP protocols only!\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }

                if (STRNCASECMP (args[FWL_INDEX_6], "any", FWL_THREE_BYTES) ==
                    FWL_ZERO)
                {
                    pFwlFilterEntry->u4DestPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1DestPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4DestPortPres = TRUE;
                    if ((CLI_STRLEN (args[6])) < FWL_MAX_PORT_LEN)
                    {
                        CLI_STRNCPY (pFwlFilterEntry->au1DestPortRange,
                                     args[FWL_INDEX_6],
                                     MEM_MAX_BYTES (STRLEN (args[6]),
                                                    FWL_MAX_PORT_LEN));
                    }
                }
            }
            else
            {
                if ((args[FWL_INDEX_3] == NULL) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") == FWL_ZERO))
                {
                    pFwlFilterEntry->u4DestPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1DestPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4DestPortPres = FALSE;
                }
            }

            /* Check if the user has set "established" or "reset"
             * and the protocol field is tcp */
            if ((args[FWL_INDEX_3] != NULL) && (CLI_STRCASECMP
                                                (args[FWL_INDEX_3], "tcp") !=
                                                FWL_ZERO) &&
                ((args[FWL_INDEX_7] != NULL) || (args[FWL_INDEX_8] != NULL)))
            {
                CliPrintf (CliHandle, "%%ERROR: established or reset can be "
                           "configured only for TCP !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            /* Check if the user has set "established" and the protocol 
             * field is tcp */
            if ((args[FWL_INDEX_7] != NULL) && (args[FWL_INDEX_3] != NULL) &&
                (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") == FWL_ZERO))
            {
                pFwlFilterEntry->u4EsPres = TRUE;
                pFwlFilterEntry->u4Established = CLI_FWL_TCP_ACK_ESTABLISH;
            }
            else
            {
                pFwlFilterEntry->u4EsPres = FALSE;
            }

            /* Check if the user has set "reset" and the protocol 
             * field is tcp */
            if ((args[FWL_INDEX_8] != NULL) && (args[FWL_INDEX_3] != NULL) &&
                (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") == FWL_ZERO))
            {
                pFwlFilterEntry->u4RstPres = TRUE;
                pFwlFilterEntry->u4Reset = CLI_FWL_TCP_RESET;
            }
            else
            {
                pFwlFilterEntry->u4RstPres = FALSE;
            }
            i4RetVal = FwlCliAddFilter (CliHandle, pFwlFilterEntry);

            break;

        case CLI_ADD_FWL_IPV6_FILTER:
            /*
             * args[0] = Firewall filter name
             * args[1] = Source IP range - for ex 1000::4/128 | any
             * args[2] = Destination IP range | any
             * args[3] = Protocol filter is:
             *           tcp  | udp  |
             *           icmp | igmp | ggp  | ip   | egp |
             *           igp  | nvp  | irtp | idpr | rsvp |
             *           mhrp | igrp | ospf
             *
             * args[4] = Source Port range - for ex. >=23
             * args[5] = Destination Port range - for ex. >1023
             * args[6] = established
             * args[7] = reset
             * args[8] = Dscp Value
             * args[9] = Flow label
             *
             */

            u4MemAllocateStatus = MemAllocateMemBlock (FWL_CLI_FILTER_PID,
                                                       (UINT1 **) (VOID *)
                                                       &pFwlFilterEntry);

            if (u4MemAllocateStatus == MEM_FAILURE)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                         "\n Memory Pool Exhausts for Rule List - "
                         "Dynamic Allocation Done \n");
            }

            if (pFwlFilterEntry == NULL)
            {
                CliPrintf (CliHandle, "unable to allocate buffer\n");
                return CLI_FAILURE;
            }

            /* Copy the firewall fiter name which is the index of the table */
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_FILTER_NAME_LEN -
                FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is "
                           "too long !\r\n");

                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pFwlFilterEntry->au1FilterName, args[FWL_INDEX_0]);

            if (CLI_STRCMP (pFwlFilterEntry->au1FilterName, "fildef") ==
                FWL_ZERO)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name has "
                           "to be other than fildef !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            /* If the source and destination address is specified as "any"
             * use ::/0 as range
             */

            /*Source IP address range */
            if (STRNCASECMP (args[FWL_INDEX_1], "any", FWL_THREE_BYTES) ==
                FWL_ZERO)
            {
                CLI_STRCPY (pFwlFilterEntry->au1SrcAddress, "::/0");
            }
            else
            {
                if (CLI_STRLEN (args[FWL_INDEX_1]) < FWL_MAX_ADDR_LEN)
                {
                    CLI_STRCPY (pFwlFilterEntry->au1SrcAddress,
                                args[FWL_INDEX_1]);
                }
                else
                {
                    CliPrintf (CliHandle, "%%ERROR: Source IP address is "
                               "too long !\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }

            /* Dest IP address range */
            if (STRNCASECMP (args[FWL_INDEX_2], "any", FWL_THREE_BYTES) ==
                FWL_ZERO)
            {
                CLI_STRCPY (pFwlFilterEntry->au1DestAddress, "::/0");
            }
            else
            {
                if (CLI_STRLEN (args[FWL_INDEX_2]) < FWL_MAX_ADDR_LEN)
                {
                    CLI_STRCPY (pFwlFilterEntry->au1DestAddress,
                                args[FWL_INDEX_2]);
                }
                else
                {
                    CliPrintf (CliHandle, "%%ERROR: Destination IP address is "
                               "too long !\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }
            /* Protocol - for ex. tcp, udp, ip, etc */
            if ((args[FWL_INDEX_3] != NULL) && (CLI_STRCMP (args[FWL_INDEX_3],
                                                            "other") !=
                                                FWL_ZERO))
            {
                pFwlFilterEntry->u4IsProtoPres = TRUE;
                if ((pFwlFilterEntry->u4Protocol =
                     (UINT4) cli_get_fwl_filter_protocol (args[FWL_INDEX_3])) ==
                    0)
                {
                    CliPrintf (CliHandle, "%%ERROR: Invalid Protocol !\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
            }
            else if ((args[FWL_INDEX_3] != NULL) &&
                     (CLI_STRCMP (args[FWL_INDEX_3], "other") == FWL_ZERO))
            {
                pFwlFilterEntry->u4IsProtoPres = TRUE;
                pFwlFilterEntry->u4Protocol = *(UINT4 *) (VOID *)
                    args[FWL_INDEX_4];
                /* Other proto */
            }
            else
            {
                pFwlFilterEntry->u4IsProtoPres = FALSE;
                pFwlFilterEntry->u4Protocol = FWL_PROTO_ANY;    /* default proto */
            }
            /* User has specified the src port range */
            if (args[FWL_INDEX_5] != NULL)
            {
                /* If the source port range is specified as "any" set >1
                 * If protocol is other than udp/tcp/any then dont set source poo
                 rt */
                if ((args[FWL_INDEX_3] != NULL) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") != FWL_ZERO))
                {
                    CliPrintf (CliHandle,
                               "%%ERROR: Source-Port entry for TCP, UDP"
                               " protocols only!\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                if (STRNCASECMP (args[FWL_INDEX_5], "any", FWL_THREE_BYTES) ==
                    FWL_ZERO)
                {
                    pFwlFilterEntry->u4SrcPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1SrcPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4SrcPortPres = TRUE;
                    if (CLI_STRLEN (args[FWL_INDEX_5]) < FWL_MAX_ADDR_LEN)
                    {
                        CLI_STRCPY (pFwlFilterEntry->au1SrcPortRange,
                                    args[FWL_INDEX_5]);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%%ERROR: Source Port Range is "
                                   "too long !\r\n");
                        i4RetVal = CLI_FAILURE;
                        break;

                    }
                }
            }
            else
            {
                if ((args[FWL_INDEX_3] == NULL) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") == FWL_ZERO))
                {
                    pFwlFilterEntry->u4SrcPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1SrcPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4SrcPortPres = FALSE;
                }
            }
            /* User has specified the dest port range */
            if (args[FWL_INDEX_6] != NULL)
            {
                /* If the destination port range is specified as "any" set >1
                 * If protocol is other than udp/tcp/any then dont set dest-portt
                 */
                if ((args[FWL_INDEX_3] != NULL) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") != FWL_ZERO) &&
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") != FWL_ZERO))
                {
                    CliPrintf (CliHandle, "%%ERROR: Destination-Port entry"
                               "for TCP,UDP protocols only!\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;
                }

                if (STRNCASECMP (args[FWL_INDEX_6], "any", FWL_THREE_BYTES) ==
                    FWL_ZERO)
                {
                    pFwlFilterEntry->u4DestPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1DestPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4DestPortPres = TRUE;
                    if ((CLI_STRLEN (args[FWL_INDEX_6])) < FWL_MAX_PORT_LEN)
                    {
                        CLI_STRNCPY (pFwlFilterEntry->au1DestPortRange,
                                     args[FWL_INDEX_6],
                                     MEM_MAX_BYTES (STRLEN (args[6]),
                                                    FWL_MAX_PORT_LEN));
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%% ERROR: Invalid Range!\r\n");
                    }
                }
            }
            else
            {
                if ((args[FWL_INDEX_3] == NULL) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "tcp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "udp") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "any") == FWL_ZERO) ||
                    (CLI_STRCASECMP (args[FWL_INDEX_3], "other") == FWL_ZERO))
                {
                    pFwlFilterEntry->u4DestPortPres = TRUE;
                    CLI_STRCPY (pFwlFilterEntry->au1DestPortRange, ">1");
                }
                else
                {
                    pFwlFilterEntry->u4DestPortPres = FALSE;
                }
            }
            /* Check whether user has specified Dscp and flow id */
            if ((args[FWL_INDEX_7] != NULL))
            {
                pFwlFilterEntry->u4IsDscpPres = TRUE;
                pFwlFilterEntry->i4Dscp =
                    (*(INT4 *) (VOID *) (args[FWL_INDEX_7]));
            }

            if ((args[FWL_INDEX_8] != NULL))
            {
                pFwlFilterEntry->u4IsFlowIdPres = TRUE;
                pFwlFilterEntry->u4FlowId =
                    (*(UINT4 *) (VOID *) (args[FWL_INDEX_8]));
            }
            i4RetVal = FwlCliAddIPv6Filter (CliHandle, pFwlFilterEntry);

            MemReleaseMemBlock (FWL_CLI_FILTER_PID, (UINT1 *) pFwlFilterEntry);
            break;

        case CLI_DEL_FWL_FILTER:
        case CLI_DEL_FWL_IPV6_FILTER:
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_FILTER_NAME_LEN -
                FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is too long !\r\n");

                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal = FwlCliDeleteFilter (CliHandle, args[FWL_INDEX_0]);
            break;

        case CLI_SET_FWL_FILTER_ACCOUNTING:
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_FILTER_NAME_LEN -
                FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is too long !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal =
                FwlCliSetFwlFilterAccounting (CliHandle, args[FWL_INDEX_0],
                                              (UINT4)
                                              CLI_ATOI (args[FWL_INDEX_1]));
            break;

        case CLI_CLEAR_FWL_FILTER_ACCOUNTING:
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_FILTER_NAME_LEN -
                FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is too long !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal = FwlCliClearFwlFilterAccounting (CliHandle,
                                                       args[FWL_INDEX_0]);
            break;

        case CLI_ADD_FWL_ACCESS_LIST:
            u4MemAllocateStatus = MemAllocateMemBlock (FWL_CLI_ACCESS_LIST_PID,
                                                       (UINT1 **) (VOID *)
                                                       &pFwlAccessList);

            if (u4MemAllocateStatus == MEM_FAILURE)
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, ALL_FAILURE_TRC, "FWL",
                         "\n Memory Pool Exhausts for Rule List - "
                         "Dynamic Allocation Done \n");
            }

            if (pFwlAccessList == NULL)
            {
                CliPrintf (CliHandle, "unable to allocate buffer\n");
                return CLI_FAILURE;
            }

            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_ACL_NAME_LEN - FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: ACL name is too long !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pFwlAccessList->au1AclName, args[FWL_INDEX_0]);

            if (CLI_STRCMP (pFwlAccessList->au1AclName, "acldef") == FWL_ZERO)
            {
                CliPrintf (CliHandle, "%%ERROR: Acl name has "
                           "to be other than fildef !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pFwlAccessList->au1FilterComb, args[FWL_INDEX_1]);
            pFwlAccessList->u4Direction = (UINT4) CLI_ATOI (args[FWL_INDEX_2]);
            pFwlAccessList->u4Permit = (UINT4) CLI_ATOI (args[FWL_INDEX_3]);
            pFwlAccessList->u4Priority =
                *((UINT4 *) (VOID *) args[FWL_INDEX_4]);
            pFwlAccessList->u4LogTrigger = (UINT4) CLI_ATOI (args[FWL_INDEX_5]);
            pFwlAccessList->u4FragAction = (UINT4) CLI_ATOI (args[FWL_INDEX_6]);
            pFwlAccessList->u4Interface = FWL_GLOBAL_IDX;
            i4RetVal = FwlCliAddAccessList (CliHandle, pFwlAccessList);

            break;

        case CLI_DEL_FWL_ACCESS_LIST:
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_ACL_NAME_LEN - FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: ACL name is too long !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal = FwlCliDeleteAccessList (CliHandle,
                                               FWL_GLOBAL_IDX,
                                               args[FWL_INDEX_0],
                                               (UINT4)
                                               CLI_ATOI (args[FWL_INDEX_1]));
            break;

        case CLI_SET_IP_INSPECT_OPTION:
            i4RetVal = FwlCliSetIpOption (CliHandle, FWL_GLOBAL_IDX,
                                          (UINT4) CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_SET_ICMP_INSPECT_OPTION:
            i4RetVal = FwlCliSetIcmpOption (CliHandle, FWL_GLOBAL_IDX,
                                            (UINT4)
                                            CLI_ATOI (args[FWL_INDEX_0]),
                                            (UINT4)
                                            CLI_ATOI (args[FWL_INDEX_1]));
            break;

        case CLI_SET_ICMPV6_INSPECT_OPTION:
            i4RetVal = FwlCliSetIcmpv6Option (CliHandle, FWL_GLOBAL_IDX,
                                              (*(INT4 *) (VOID *)
                                               (args[FWL_INDEX_0])));

            break;

        case CLI_SET_IP_FILTER_FRAGMENTS:
            if (args[FWL_INDEX_1] != NULL)
            {
                u4Val = *((UINT4 *) (VOID *) args[FWL_INDEX_1]);
            }
            else
            {
                u4Val = FWL_ZERO;
            }
            i4RetVal = FwlCliSetFragOption (CliHandle, FWL_GLOBAL_IDX,
                                            (UINT4)
                                            CLI_ATOI (args[FWL_INDEX_0]),
                                            u4Val);
            break;
        case CLI_SET_FWL_DMZ:
            i4RetVal =
                FwlCliSetDmzHost (CliHandle,
                                  *((UINT4 *) (VOID *) args[FWL_INDEX_0]));
            break;

        case CLI_DEL_FWL_DMZ:
            i4RetVal =
                FwlCliDeleteDmzHost (CliHandle,
                                     *((UINT4 *) (VOID *) args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_IPV6_DMZ:
            if ((UINT4 *) (VOID *) (args[FWL_INDEX_0]) != NULL)
            {
                MEMSET (&DmzIp6Addr, FWL_ZERO, sizeof (tIp6Addr));
                INET_ATON6 (args[FWL_INDEX_0], &DmzIp6Addr);
            }

            i4RetVal = FwlCliSetDmzIPv6Host (CliHandle, DmzIp6Addr);
            break;

        case CLI_DEL_FWL_IPV6_DMZ:
            if ((UINT4 *) (VOID *) (args[FWL_INDEX_0]) != NULL)
            {
                MEMSET (&DmzIp6Addr, FWL_ZERO, sizeof (tIp6Addr));
                INET_ATON6 (args[FWL_INDEX_0], &DmzIp6Addr);
            }
            i4RetVal = FwlCliDeleteDmzIPv6Host (CliHandle, DmzIp6Addr);
            break;

        case CLI_PASS_NETBIOS_LAN2WAN:
            i4RetVal = FwlCliSetPassNetbiosLan2Wan (CliHandle,
                                                    (UINT4)
                                                    CLI_ATOI (args
                                                              [FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_URL_FILTERING:
            i4RetVal = FwlCliSetUrlFiltering (CliHandle,
                                              CLI_ATOI (args[FWL_INDEX_0]));
            break;

        case CLI_ADD_URL_FILTER:
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_URL_FILTER_STRING_LEN -
                FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: URL Length is too long !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal = FwlCliAddUrlFilter (CliHandle, args[FWL_INDEX_0]);
            break;

        case CLI_DEL_URL_FILTER:
            if (CLI_STRLEN (args[FWL_INDEX_0]) > FWL_MAX_URL_FILTER_STRING_LEN -
                FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: URL Length is too long !\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal = FwlCliDeleteUrlFilter (CliHandle, args[FWL_INDEX_0]);
            break;

        case CLI_SET_FWL_TRAP:
            i4RetVal = FwlCliSetGblTrap (CLI_ATOI (args[FWL_INDEX_0]));
            break;
        case CLI_SET_GBL_THRESHOLD:
            i4RetVal = FwlCliSetGblThreshold (*((INT4 *) (VOID *)
                                                args[FWL_INDEX_0]));
            break;
        case CLI_CLEAR_GBL_STATS:
            i4RetVal = FwlCliClearGlobalStats ();
            break;
        case CLI_CLEAR_IPV6_GBL_STATS:
            i4RetVal = FwlCliClearIPv6GlobalStats ();
            break;
        case CLI_CLEAR_INT_STATS:
            /* args[0] - interface type for Fa ports */
            /* args[1] - interface identifier for Fa ports */
            /* args[2] - interface type for Gi ports */
            /* args[3] - interface identifier for Gi ports */
            /* args[4] - interface type for PPP links */
            /* args[5] - interface identifier for PPP links */

            MEMSET (au1PortList, FWL_ZERO, SYS_DEF_PORT_LIST_SIZE);

            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[FWL_INDEX_1] != NULL)
            {
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_0],
                                     (INT1 *) args[FWL_INDEX_1],
                                     au1PortList,
                                     (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if gigabitethernet ports are given */
            if (args[FWL_INDEX_3] != NULL)
            {
                /* Currently all interfaces are represented as ethernet */
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_2],
                                     (INT1 *) args[FWL_INDEX_3],
                                     au1PortList, BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Add PPP links as member if given */
            if (args[FWL_INDEX_5] != NULL)
            {
                i4RetVal = CfaCliGetIfList ((INT1 *) args[FWL_INDEX_4],
                                            (INT1 *) args[FWL_INDEX_5],
                                            au1PortList,
                                            BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid PPP Link(s) \r\n");
                    break;
                }
            }

            /* Add IVR ports also as member if given */
            if (args[FWL_INDEX_7] != NULL)
            {
                i4RetVal = CliStrToPortList ((UINT1 *) args[FWL_INDEX_7],
                                             au1PortList,
                                             SYS_DEF_PORT_LIST_SIZE,
                                             CFA_L3IPVLAN);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan port(s) \r\n");
                    break;
                }
            }
            for (u4IfIndex = FWL_ONE; u4IfIndex <= SYS_DEF_MAX_INTERFACES;
                 u4IfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (au1PortList, u4IfIndex,
                                         SYS_DEF_PORT_LIST_SIZE, i4RetStatus);
                if (i4RetStatus == OSIX_FALSE)
                {
                    continue;
                }

                i4RetVal = FwlCliClearIntfStats (u4IfIndex);
                if (i4RetVal == CLI_FAILURE)
                {
                    break;
                }
            }
            break;
        case CLI_CLEAR_IPV6_INT_STATS:
            /* args[0] - interface type for Fa ports */
            /* args[1] - interface identifier for Fa ports */
            /* args[2] - interface type for Gi ports */
            /* args[3] - interface identifier for Gi ports */
            /* args[4] - interface type for PPP links */
            /* args[5] - interface identifier for PPP links */

            MEMSET (au1PortList, FWL_ZERO, SYS_DEF_PORT_LIST_SIZE);

            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[FWL_INDEX_1] != NULL)
            {
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_0],
                                     (INT1 *) args[FWL_INDEX_1],
                                     au1PortList,
                                     (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if gigabitethernet ports are given */
            if (args[FWL_INDEX_3] != NULL)
            {
                /* Currently all interfaces are represented as ethernet */
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_2],
                                     (INT1 *) args[FWL_INDEX_3],
                                     au1PortList, BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Add PPP links as member if given */
            if (args[FWL_INDEX_5] != NULL)
            {
                i4RetVal = CfaCliGetIfList ((INT1 *) args[FWL_INDEX_4],
                                            (INT1 *) args[FWL_INDEX_5],
                                            au1PortList,
                                            BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid PPP Link(s) \r\n");
                    break;
                }
            }

            /* Add IVR ports also as member if given */
            if (args[FWL_INDEX_7] != NULL)
            {
                i4RetVal = CliStrToPortList ((UINT1 *) args[FWL_INDEX_7],
                                             au1PortList,
                                             SYS_DEF_PORT_LIST_SIZE,
                                             CFA_L3IPVLAN);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan port(s) \r\n");
                    break;
                }
            }
            for (u4IfIndex = FWL_INDEX_1; u4IfIndex <= SYS_DEF_MAX_INTERFACES;
                 u4IfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (au1PortList, u4IfIndex,
                                         SYS_DEF_PORT_LIST_SIZE, i4RetStatus);
                if (i4RetStatus == OSIX_FALSE)
                {
                    continue;
                }

                i4RetVal = FwlCliClearIntfIPv6Stats (u4IfIndex);
                if (i4RetVal == CLI_FAILURE)
                {
                    break;
                }
            }
            break;
        case CLI_SET_FWL_IP_ADDR_IN_LIST:
        case CLI_DEL_FWL_IP_ADDR_IN_LIST:
        {
            u4ListType = (UINT4) CLI_GET_BLACK_OR_WHITE_LIST_TYPE ();
            pAddr = &AddrPrefix;
            /*if (NULL == pAddr)
               {
               break;
               } */
            i1DotFnd = CliIsDelimit ('.', (CONST CHR1 *) args[FWL_INDEX_0]);
            if ((pIp6addr =
                 str_to_ip6addr ((UINT1 *) args[FWL_INDEX_0])) != NULL)
            {
                pAddr->u4AddrType = IPVX_ADDR_FMLY_IPV6;
                MEMCPY (pAddr->v6Addr.u1_addr, pIp6addr, IPVX_IPV6_ADDR_LEN);
            }
            else if (FWL_ZERO != i1DotFnd)
            {
                pAddr->u4AddrType = IPVX_ADDR_FMLY_IPV4;
                if (FWL_ZERO == FWL_INET_ATON (args[FWL_INDEX_0], &InAddr))
                {
                    CLI_SET_ERR (CLI_FWL_INVALID_UCAST_ADDR);
                    CliPrintf (CliHandle, "\r%% Invalid unicast address \r\n");

                    break;
                }
                u4IpAddress = InAddr.u4Addr;
                pAddr->v4Addr = u4IpAddress;
            }
            else
            {
                CLI_SET_ERR (CLI_FWL_UNSUPP_ADD_FLY_ERR);
                CliPrintf(CliHandle,"\r%% Firewall unsupported family error \r\n");
                break;
            }

            u2PrefixLen = (UINT2) CLI_ATOI (args[FWL_INDEX_1]);
            if (CLI_SET_FWL_IP_ADDR_IN_LIST == u4Command)
            {
                if (CLI_BLK_LIST == u4ListType)
                {
                    i4RetVal = FwlCliConfIPAddressInBlkList (CliHandle,
                                                             pAddr,
                                                             u2PrefixLen,
                                                             FWL_CREATE_AND_GO);
                }
                if (CLI_WHITE_LIST == u4ListType)
                {
                    i4RetVal = FwlCliConfIPAddressInWhiteList (CliHandle,
                                                               pAddr,
                                                               u2PrefixLen,
                                                               FWL_CREATE_AND_GO);
                }
            }
            else
            {
                if (CLI_BLK_LIST == u4ListType)
                {
                    i4RetVal = FwlCliConfIPAddressInBlkList (CliHandle,
                                                             pAddr,
                                                             u2PrefixLen,
                                                             FWL_DESTROY);
                }
                if (CLI_WHITE_LIST == u4ListType)
                {
                    i4RetVal = FwlCliConfIPAddressInWhiteList (CliHandle,
                                                               pAddr,
                                                               u2PrefixLen,
                                                               FWL_DESTROY);
                }
            }

        }
            break;

        case CLI_SET_FWL_LOG_SIZE:
            i4RetVal = FwlCliSetLogFileSize (CliHandle,
                                             *((UINT4 *) (VOID *)
                                               args[FWL_INDEX_0]));
            break;

        case CLI_SET_FWL_LOG_SIZE_THRESHOLD:
            i4RetVal =
                FwlCliSetLogSizeThreshold (CliHandle,
                                           *((UINT4 *) (VOID *)
                                             args[FWL_INDEX_0]));
            break;

        case CLI_PURGE_FWL_BLACK_WHITE_LIST:
        {
            u4ListType = CLI_PTR_TO_U4 (args[FWL_INDEX_0]);
            i4RetVal = FwlCliPurgeBlackWhiteListDataBase (CliHandle,
                                                          u4ListType);

        }
            break;
        case CLI_SHOW_FWL_URL_FILTERS:
            i4RetVal = FwlCliShowUrlFilters (CliHandle);
            break;

        case CLI_SHOW_FWL_CONFIG:
            i4RetVal = FwlCliShowFwlGlobalConfig (CliHandle);
            break;
        case CLI_SHOW_FWL_STATS:
            i4RetVal = FwlCliShowFwlGlobalStats (CliHandle);
            break;
        case CLI_SHOW_FWL_IPV6_STATS:
            i4RetVal = FwlCliShowFwlIPv6GlobalStats (CliHandle);
            break;

        case CLI_SHOW_FWL_FILTERS:
            i4RetVal = FwlCliShowFwlFilters (CliHandle);
            break;
        case CLI_SHOW_FWL_IPV6_FILTERS:
            i4RetVal = FwlCliShowFwlIPv6Filters (CliHandle);
            break;
        case CLI_SHOW_FWL_ACCESS_LISTS:
            i4RetVal = FwlCliShowAccessLists (CliHandle);
            break;
        case CLI_SHOW_FWL_LOGS:
            i4RetVal = FwlCliShowLogs (CliHandle);
            break;
        case CLI_SHOW_FWL_DMZ:
            i4RetVal = FwlCliShowDmzHost (CliHandle);
            break;
        case CLI_SHOW_FWL_IPV6_DMZ:
            i4RetVal = FwlCliShowIPv6DmzHost (CliHandle);
            break;
        case CLI_SHOW_FWL_MISC:
            break;
        case CLI_SHOW_STATE_FILTER:
            i4RetVal = FwlShowStatefulTable (CliHandle);
            break;
        case CLI_FWL_COMMIT:
            i4RetVal = FwlCliCommit ();
            break;
        case CLI_CLEAR_FWL_LOGS:
            i4RetVal = FwlCliClearLogs ();
            break;
        case CLI_SHOW_INITFLOW_FILTERS:
            i4RetVal = FwlShowInitFlowTable (CliHandle);
            break;
        case CLI_SHOW_FWL_INT_CONFIG:
            i4RetVal = FwlCliShowInterfaceConfig (CliHandle);
            break;

        case CLI_SHOW_FWL_INT_STATS_ALL:
            /* Check whether ALL ports has to be displayed */
            i4RetVal = FwlCliShowInterfaceStats (CliHandle);
            break;

        case CLI_SHOW_FWL_INT_STATS:

            /* args[0] - interface type for Fa/gi/PPP ports */
            /* args[1] - interface identifier for Fa/gi/PPP ports */
            MEMSET (au1PortList, FWL_ZERO, SYS_DEF_PORT_LIST_SIZE);

            /* build member port list */
            /* Check if Fastethernet/Gigabitethernet/PPP ports are given */
            if (args[FWL_INDEX_1] != NULL)
            {
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_0],
                                     (INT1 *) args[FWL_INDEX_1],
                                     au1PortList,
                                     (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if IVR port is given */
            if (args[FWL_INDEX_3] != NULL)
            {
                i4RetVal = CliStrToPortList ((UINT1 *) args[FWL_INDEX_3],
                                             au1PortList,
                                             SYS_DEF_PORT_LIST_SIZE,
                                             CFA_L3IPVLAN);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan port(s) \r\n");
                    break;
                }
            }
            for (u4IfIndex = FWL_ONE; u4IfIndex <= SYS_DEF_MAX_INTERFACES;
                 u4IfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (au1PortList, u4IfIndex,
                                         SYS_DEF_PORT_LIST_SIZE, i4RetStatus);
                if (i4RetStatus == OSIX_FALSE)
                {
                    continue;
                }
                if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName)
                    != CLI_SUCCESS)
                {
                    continue;
                }
                FwlCliFormatShowInterfaceStats (CliHandle, u4IfIndex,
                                                au1IfName);
            }
            break;
        case CLI_SHOW_FWL_IPV6_INT_STATS_ALL:
            /* Check whether ALL ports has to be displayed */
            i4RetVal = FwlCliShowIPv6InterfaceStats (CliHandle);
            break;

        case CLI_SHOW_FWL_IPV6_INT_STATS:

            /* args[0] - interface type for Fa/gi/PPP ports */
            /* args[1] - interface identifier for Fa/gi/PPP ports */
            MEMSET (au1PortList, FWL_ZERO, SYS_DEF_PORT_LIST_SIZE);

            /* build member port list */
            /* Check if Fastethernet/Gigabitethernet/PPP ports are given */
            if (args[FWL_INDEX_1] != NULL)
            {
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_0],
                                     (INT1 *) args[FWL_INDEX_1],
                                     au1PortList,
                                     (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if IVR port is given */
            if (args[FWL_INDEX_3] != NULL)
            {
                i4RetVal = CliStrToPortList ((UINT1 *) args[FWL_INDEX_3],
                                             au1PortList,
                                             SYS_DEF_PORT_LIST_SIZE,
                                             CFA_L3IPVLAN);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan port(s) \r\n");
                    break;
                }
            }
            for (u4IfIndex = FWL_ONE; u4IfIndex <= SYS_DEF_MAX_INTERFACES;
                 u4IfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (au1PortList, u4IfIndex,
                                         SYS_DEF_PORT_LIST_SIZE, i4RetStatus);
                if (i4RetStatus == OSIX_FALSE)
                {
                    continue;
                }
                if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName)
                    != CLI_SUCCESS)
                {
                    continue;
                }
                FwlCliFormatShowIPv6InterfaceStats (CliHandle, u4IfIndex,
                                                    au1IfName);
            }
            break;
        case CLI_FWL_SHOW_BLACK_WHITE_IP_ADDRESS:
        {
            if ((NULL != args[FWL_INDEX_0]) && (NULL != args[FWL_INDEX_2]))
            {
                u4ListType = CLI_BLK_LIST;
                i1ListHitCount = FWL_SET;
            }
            else if ((NULL != args[FWL_INDEX_1]) && (NULL != args[FWL_INDEX_2]))
            {
                u4ListType = CLI_WHITE_LIST;
                i1ListHitCount = FWL_SET;
            }
            else if (NULL != args[FWL_INDEX_0])
            {
                u4ListType = CLI_BLK_LIST;
            }
            else if (NULL != args[FWL_INDEX_1])
            {
                u4ListType = CLI_WHITE_LIST;
            }
            else
            {
                u4ListType = (CLI_BLK_LIST | CLI_WHITE_LIST);
                i1ListHitCount = FWL_SET;
            }

            i4RetVal = FwlCliShowBlackWhiteListDataBase (CliHandle,
                                                         u4ListType,
                                                         i1ListHitCount);
        }
            break;
        case CLI_SET_UNTRUSTED_PORT:
            /* args[0] - interface type for Fa ports */
            /* args[1] - interface identifier for Fa ports */
            /* args[2] - interface type for Gi ports */
            /* args[3] - interface identifier for Gi ports */
            /* args[4] - interface type for PPP links */
            /* args[5] - interface identifier for PPP links */

            MEMSET (au1PortList, FWL_ZERO, SYS_DEF_PORT_LIST_SIZE);

            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[FWL_INDEX_1] != NULL)
            {
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_0],
                                     (INT1 *) args[FWL_INDEX_1],
                                     au1PortList,
                                     (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if gigabitethernet ports are given */
            if (args[FWL_INDEX_3] != NULL)
            {
                /* Currently all interfaces are represented as ethernet */
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_2],
                                     (INT1 *) args[FWL_INDEX_3],
                                     au1PortList, BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Add PPP links as member if given */
            if (args[FWL_INDEX_5] != NULL)
            {
                i4RetVal = CfaCliGetIfList ((INT1 *) args[FWL_INDEX_4],
                                            (INT1 *) args[FWL_INDEX_5],
                                            au1PortList,
                                            BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid PPP Link(s) \r\n");
                    break;
                }
            }

            /* Add IVR ports also as member if given */
            if (args[FWL_INDEX_7] != NULL)
            {
                i4RetVal = CliStrToPortList ((UINT1 *) args[FWL_INDEX_7],
                                             au1PortList,
                                             SYS_DEF_PORT_LIST_SIZE,
                                             CFA_L3IPVLAN);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan port(s) \r\n");
                    break;
                }
            }
            /* Set the Threshold value for the Interface over which the Firewall
             * has to be enabled */
            if (args[FWL_INDEX_8] != NULL)
            {
                i4Threshold = *((INT4 *) (VOID *) args[FWL_INDEX_8]);
            }
            else
                i4Threshold = FWL_DEF_TRAP_THRESHOLD;

            for (u4IfIndex = FWL_ONE; u4IfIndex <= SYS_DEF_MAX_INTERFACES;
                 u4IfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (au1PortList, u4IfIndex,
                                         SYS_DEF_PORT_LIST_SIZE, i4RetStatus);
                if (i4RetStatus == OSIX_FALSE)
                {
                    continue;
                }

                i4RetVal = FwlCliSetUntrustedPort (CliHandle, u4IfIndex,
                                                   i4Threshold);
                if (i4RetVal == CLI_FAILURE)
                {
                    break;
                }
            }
            break;

        case CLI_FWL_SHOW_PINHOLE:
            i4RetVal = FwlCliShowHiddenPartialEntry (CliHandle);
            break;

        case CLI_SET_FWL_DEBUG_LEVEL:
            i4RetVal = nmhSetFwlGlobalDebug (FWL_ENABLE);
            break;

        case CLI_RESET_FWL_DEBUG_LEVEL:
            i4RetVal = nmhSetFwlGlobalDebug (FWL_DISABLE);
            break;

        case CLI_RESET_UNTRUSTED_PORT:
            /* args[0] - interface type for Fa ports */
            /* args[1] - interface identifier for Fa ports */
            /* args[2] - interface type for Gi ports */
            /* args[3] - interface identifier for Gi ports */
            /* args[4] - interface type for PPP links */
            /* args[5] - interface identifier for PPP links */

            MEMSET (au1PortList, FWL_ZERO, SYS_DEF_PORT_LIST_SIZE);

            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[FWL_INDEX_1] != NULL)
            {
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_0],
                                     (INT1 *) args[FWL_INDEX_1],
                                     au1PortList,
                                     (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if gigabitethernet ports are given */
            if (args[FWL_INDEX_3] != NULL)
            {
                /* Currently all interfaces are represented as ethernet */
                i4RetVal =
                    CfaCliGetIfList ((INT1 *) args[FWL_INDEX_2],
                                     (INT1 *) args[FWL_INDEX_3],
                                     au1PortList, BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Add PPP link also as member if given */
            if (args[FWL_INDEX_5] != NULL)
            {
                i4RetVal = CfaCliGetIfList ((INT1 *) args[FWL_INDEX_4],
                                            (INT1 *) args[FWL_INDEX_5],
                                            au1PortList,
                                            BRG_MAX_PHY_PLUS_LOG_PORTS);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid PPP Link(s) \r\n");
                    break;
                }
            }

            /* Add IVR ports also as member if given */
            if (args[FWL_INDEX_7] != NULL)
            {
                i4RetVal = CliStrToPortList ((UINT1 *) args[FWL_INDEX_7],
                                             au1PortList,
                                             SYS_DEF_PORT_LIST_SIZE,
                                             CFA_L3IPVLAN);
                if (i4RetVal == OSIX_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan port(s) \r\n");
                    break;
                }
            }

            for (u4IfIndex = FWL_ONE; u4IfIndex <= SYS_DEF_MAX_INTERFACES;
                 u4IfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (au1PortList, u4IfIndex,
                                         SYS_DEF_PORT_LIST_SIZE, i4RetStatus);

                if (i4RetStatus == OSIX_FALSE)
                {
                    continue;
                }

                i4RetVal = FwlCliReSetUntrustedPort (CliHandle, u4IfIndex);
                if (i4RetVal == CLI_FAILURE)
                {
                    break;
                }
            }
            break;

        default:
            CliPrintf (CliHandle, "ERROR: Unknown command !\r\n");
            i4RetVal = CLI_FAILURE;
            break;
    }
    UNUSED_PARAM (pu1Inst);
    if (pFwlAccessList != NULL)
    {
        MemReleaseMemBlock (FWL_CLI_ACCESS_LIST_PID, (UINT1 *) pFwlAccessList);
    }

    if (pFwlFilterEntry != NULL)
    {
        MemReleaseMemBlock (FWL_CLI_FILTER_PID, (UINT1 *) pFwlFilterEntry);
    }
    if ((CLI_FAILURE == i4RetVal) && (CLI_SUCCESS == CLI_GET_ERR (&u4ErrCode)))
    {
        if ((u4ErrCode > FWL_ZERO) && (u4ErrCode < CLI_FWL_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", FwlCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (FWL_ZERO);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);
    FwlUnLock ();
    CliUnRegisterLock (CliHandle);
    return i4RetVal;
}

/***********************************************************************/
/*  Function Name : FwlCliSetGblTrap                                   */
/*  Description   : Function to Set the GLobal Trap Enable or disable  */
/*  Input(s)      : i4FwlGlobalTrap                                    */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/
INT4
FwlCliSetGblTrap (INT4 i4FwlGlobalTrap)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlGlobalTrap (&u4ErrorCode, i4FwlGlobalTrap) == SNMP_SUCCESS)
    {
        nmhSetFwlGlobalTrap (i4FwlGlobalTrap);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***********************************************************************/
/*  Function Name : FwlCliSetGblThreshold                              */
/*  Description   : Function to Set the GLobal Threshold               */
/*  Input(s)      : i4Threshold - Thresholg that is to be set          */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/
INT4
FwlCliSetGblThreshold (INT4 i4Threshold)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlTrapThreshold (&u4ErrorCode, i4Threshold) == SNMP_SUCCESS)
    {
        nmhSetFwlTrapThreshold (i4Threshold);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***********************************************************************/
/*  Function Name : FwlCliClearGlobalStats                             */
/*  Description   : Function to clear the Firewall Global Statistics   */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/
INT4
FwlCliClearGlobalStats (VOID)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlStatClear (&u4ErrorCode, FWL_ENABLE) == SNMP_SUCCESS)
    {
        nmhSetFwlStatClear (FWL_ENABLE);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***********************************************************************/
/*  Function Name : FwlCliClearIPv6GlobalStats                           */
/*  Description   : Function to clear the Firewall IPv6 Global Statistics*/
/*  Input(s)      : None                                                 */
/*  Output(s)     : None                                                 */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                              */
/*************************************************************************/
INT4
FwlCliClearIPv6GlobalStats (VOID)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlStatClearIPv6 (&u4ErrorCode, FWL_ENABLE) == SNMP_SUCCESS)
    {
        nmhSetFwlStatClearIPv6 (FWL_ENABLE);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***********************************************************************/
/*  Function Name : FwlCliClearIntfStats                               */
/*  Description   : Function to clear the Firewall Interface Statistics*/
/*  Input(s)      : u4IfIndex  - Interface Index                       */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/
INT4
FwlCliClearIntfStats (UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlStatIfClear (&u4ErrorCode, (INT4) u4IfIndex,
                                 FWL_ENABLE) == SNMP_SUCCESS)
    {

        nmhSetFwlStatIfClear ((INT4) u4IfIndex, FWL_ENABLE);
        return CLI_SUCCESS;

    }
    return CLI_FAILURE;
}

/****************************************************************************/
/*  Function Name : FwlCliClearIntfIPv6Stats                                */
/*  Description   : Function to clear the Firewall Interface IPv6 Statistics*/
/*  Input(s)      : u4IfIndex  - Interface Index                            */
/*  Output(s)     : None                                                    */
/*  Return        : None                                                    */
/****************************************************************************/
INT4
FwlCliClearIntfIPv6Stats (UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (nmhTestv2FwlStatIfClearIPv6 (&u4ErrorCode, (INT4) u4IfIndex,
                                     FWL_ENABLE) == SNMP_SUCCESS)
    {

        nmhSetFwlStatIfClearIPv6 ((INT4) u4IfIndex, FWL_ENABLE);
        return CLI_SUCCESS;

    }
    return CLI_FAILURE;
}

/***********************************************************************/
/*  Function Name : cli_get_fwl_filter_protocol                        */
/*  Description   : Function to get the Protocol from the input string */
/*  Input(s)      : pu1Prot - Pointer to the String                    */
/*  Output(s)     : None                                               */
/*  Return        : Protocol Number                                    */
/***********************************************************************/
INT4
cli_get_fwl_filter_protocol (UINT1 *pu1Prot)
{
    if (CLI_STRCASECMP (pu1Prot, "icmp") == FWL_ZERO)
    {
        return FWL_PROTO_ICMP;
    }
    if (CLI_STRCASECMP (pu1Prot, "igmp") == FWL_ZERO)
    {
        return FWL_PROTO_IGMP;
    }
    if (CLI_STRCASECMP (pu1Prot, "ggp") == FWL_ZERO)
    {
        return FWL_PROTO_GGP;
    }
    if (CLI_STRCASECMP (pu1Prot, "ip") == FWL_ZERO)
    {
        return FWL_PROTO_IP;
    }
    if (CLI_STRCASECMP (pu1Prot, "tcp") == FWL_ZERO)
    {
        return FWL_PROTO_TCP;
    }
    if (CLI_STRCASECMP (pu1Prot, "egp") == FWL_ZERO)
    {
        return FWL_PROTO_EGP;
    }
    if (CLI_STRCASECMP (pu1Prot, "igp") == FWL_ZERO)
    {
        return FWL_PROTO_IGP;
    }
    if (CLI_STRCASECMP (pu1Prot, "nvp") == FWL_ZERO)
    {
        return FWL_PROTO_NVP;
    }
    if (CLI_STRCASECMP (pu1Prot, "udp") == FWL_ZERO)
    {
        return FWL_PROTO_UDP;
    }
    if (CLI_STRCASECMP (pu1Prot, "irtp") == FWL_ZERO)
    {
        return FWL_PROTO_IRTP;
    }
    if (CLI_STRCASECMP (pu1Prot, "idpr") == FWL_ZERO)
    {
        return FWL_PROTO_IDPR;
    }
    if (CLI_STRCASECMP (pu1Prot, "rsvp") == FWL_ZERO)
    {
        return FWL_PROTO_RSVP;
    }
    if (CLI_STRCASECMP (pu1Prot, "mhrp") == FWL_ZERO)
    {
        return FWL_PROTO_MHRP;
    }
    if (CLI_STRCASECMP (pu1Prot, "icmpv6") == FWL_ZERO)
    {
        return FWL_PROTO_ICMPV6;
    }
    if (CLI_STRCASECMP (pu1Prot, "igrp") == FWL_ZERO)
    {
        return FWL_PROTO_IGRP;
    }
    if (CLI_STRCASECMP (pu1Prot, "ospf") == FWL_ZERO)
    {
        return FWL_PROTO_OSPF;
    }
    if (CLI_STRCASECMP (pu1Prot, "any") == FWL_ZERO)
    {
        return FWL_PROTO_ANY;
    }
    if (CLI_STRCASECMP (pu1Prot, "other") == FWL_ZERO)
    {
        return FWL_ONE;
    }
    return FWL_ZERO;
}

INT4
FwlCliSetFwlStatus (tCliHandle CliHandle, UINT4 u4FwlStatus)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalMasterControlSwitch (&u4Error,
                                               (INT4) u4FwlStatus) !=
        SNMP_SUCCESS)
    {
        if (u4Error == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "\r%%Firewall Status can take only values"
                       "enable or disable \r\n");
            return (CLI_FAILURE);
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    nmhSetFwlGlobalMasterControlSwitch (u4FwlStatus);
    return (CLI_SUCCESS);
}

/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosAttack                              */
/*  Description   : Function to prevent the DOS attacks                */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/

INT4
FwlCliSetFwlDosAttack (tCliHandle CliHandle, INT4 i4AttackType ,INT4 i4SetValue,
				       INT4 i4HeaderLength ,INT4 i4PortNo)
{
     INT4                i4RetVal = FWL_ZERO;
     switch (i4AttackType)
	 {
		case FWL_DOS_ATTACK_REDIRECT:
  	            i4RetVal = FwlCliSetFwlDosAttackRedirect(CliHandle ,i4SetValue);  
              break;
		case FWL_DOS_ATTACK_SMURF:
  	            i4RetVal = FwlCliSetFwlDosAttackSmurf(CliHandle ,i4SetValue);  
              break;
		case FWL_DOS_LAND_ATTACK:
  	            i4RetVal = FwlCliSetFwlDosLandAttack(CliHandle ,i4SetValue);  
              break;
		case FWL_DOS_SHORT_HEADER:
  	            i4RetVal = FwlCliSetFwlDosShortHeader(CliHandle ,i4SetValue,
				                               i4HeaderLength);  
              break;
		case FWL_DOS_SNORK_ATTACK:
  	            i4RetVal = FwlCliSetFwlDosSnorkAttack(CliHandle ,i4SetValue,
				                               i4PortNo);  
              break;
		default :
			 break ;
			 
	 }
      
     return (i4RetVal);
}
/***********************************************************************/
/*  Function Name : FwlCliReSetFwlRateLimit                            */
/*  Description   : Function which is used to disable rate limit the   */
/*  given control traffic                                              */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliReSetFwlRateLimit (tCliHandle CliHandle, INT4 i4ProtoType ,INT4 i4PortNo)
{
  UINT4               u4ErrorCode = 0;
  UNUSED_PARAM (i4ProtoType);
    if (nmhTestv2FwlRateLimitRowStatus(&u4ErrorCode,i4PortNo,FWL_DESTROY)
                                    == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Not a valid port no \r\n");
        return CLI_FAILURE;
    }
    if ( nmhSetFwlRateLimitRowStatus(i4PortNo,FWL_DESTROY)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***********************************************************************/
/*  Function Name : FwlCliReSetFwlRateLimit                            */
/*  Description   : Function which is used to enable rate limit the    */
/*  given control traffic                                              */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/



INT4
FwlCliSetFwlRateLimit (tCliHandle CliHandle, INT4 i4ProtoType ,INT4 i4PortNo,INT4 i4RateLimit,
				       INT4 i4BurstSize ,INT4 i4TrafficMode)
{
  UINT4               u4ErrorCode = 0;
   if (nmhTestv2FwlRateLimitRowStatus(&u4ErrorCode,i4PortNo,FWL_CREATE_AND_WAIT)
									== SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Already a rule exist for the given protocol/port \r\n");
        return CLI_FAILURE;
    }
    if ( nmhSetFwlRateLimitRowStatus(i4PortNo,FWL_CREATE_AND_WAIT) 
										== SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

  if (nmhTestv2FwlRateLimitPortNumber (&u4ErrorCode,i4PortNo,i4PortNo) 
											== SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRateLimitPortNumber (i4PortNo,
                                    i4PortNo) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    if (nmhTestv2FwlRateLimitPortType (&u4ErrorCode,i4PortNo,
                                         i4ProtoType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRateLimitPortType (i4PortNo,
                                      i4ProtoType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    if (nmhTestv2FwlRateLimitTrafficMode (&u4ErrorCode,i4PortNo,i4TrafficMode) 
											== SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRateLimitTrafficMode(i4PortNo,
                                    i4TrafficMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    if (nmhTestv2FwlRateLimitValue(&u4ErrorCode,i4PortNo,i4RateLimit)
									== SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRateLimitValue(i4PortNo,i4RateLimit) 
										== SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FwlRateLimitBurstSize (&u4ErrorCode,i4PortNo,i4BurstSize) 
											== SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRateLimitBurstSize(i4PortNo,
                                    i4BurstSize) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    if (nmhTestv2FwlRateLimitRowStatus(&u4ErrorCode,i4PortNo,FWL_ACTIVE)
									== SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ( nmhSetFwlRateLimitRowStatus(i4PortNo,FWL_ACTIVE) 
										== SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to enable DOS LAND attack   */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosLandAttack  (tCliHandle CliHandle,INT4 i4SetAttackValue)
{
 

     UINT4               u4Error = FWL_ZERO;
           if (nmhTestv2FwlDosLandAttack (&u4Error,
                            i4SetAttackValue) !=  SNMP_SUCCESS)
           {
                        return (CLI_FAILURE);
           }
           if(nmhSetFwlDosLandAttack (i4SetAttackValue) != SNMP_SUCCESS)
		   {
			 CLI_FATAL_ERROR (CliHandle);
              return CLI_FAILURE;

		   }
            
     return (CLI_SUCCESS);
}

/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to enable DOS Shortheader  attack */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosShortHeader  (tCliHandle CliHandle,INT4 i4SetAttackValue ,
							INT4 i4HeaderLength)
{
     UINT4               u4Error = FWL_ZERO ;
 
     UNUSED_PARAM(i4SetAttackValue); 

     if (nmhTestv2FwlDosShortHeaderAttack (&u4Error,
                       i4HeaderLength) !=  SNMP_SUCCESS)
     {
             CliPrintf (CliHandle, "%%Invalid value !\r\n");
             return (CLI_FAILURE);
     }
     if(nmhSetFwlDosShortHeaderAttack (i4HeaderLength) != SNMP_SUCCESS)
	 {
			CLI_FATAL_ERROR (CliHandle);
              return CLI_FAILURE;

	 }

  return (CLI_SUCCESS);
            
}
/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to enable/disable DOS Snork */
/*  attack                                                             */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosSnorkAttack  (tCliHandle CliHandle,INT4 i4SetAttackValue ,
							INT4 i4PortNo)
{
     INT4                i4RetVal = FWL_ZERO;
     if(i4SetAttackValue == FWL_ONE)
	 {
          i4RetVal = FwlCliSetFwlDosSnorkAttackEnable(CliHandle,
										i4PortNo);
	 }
	 else
	 {
          i4RetVal = FwlCliSetFwlDosSnorkAttackDisable(CliHandle,
										i4PortNo);
	 }


     return (i4RetVal);
}
/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to enable DOS Snork attack  */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosSnorkAttackEnable  (tCliHandle CliHandle,INT4 i4PortNo)
{

 UINT4               u4ErrorCode = 0;
    if (nmhTestv2FwlSnorkRowStatus(&u4ErrorCode,i4PortNo,FWL_CREATE_AND_WAIT)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlSnorkRowStatus(i4PortNo,FWL_CREATE_AND_WAIT)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
   if (nmhTestv2FwlSnorkRowStatus(&u4ErrorCode,i4PortNo,FWL_ACTIVE)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlSnorkRowStatus(i4PortNo,FWL_ACTIVE)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }


    return CLI_SUCCESS;

}

/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to disable DOS snork attack */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosSnorkAttackDisable  (tCliHandle CliHandle,INT4 i4PortNo)
{
		UINT4               u4ErrorCode = 0;
    if (nmhTestv2FwlSnorkRowStatus(&u4ErrorCode,i4PortNo,FWL_DESTROY)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlSnorkRowStatus(i4PortNo,FWL_DESTROY)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

return CLI_SUCCESS;
}
/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to enable accept redirect attack   */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosAttackRedirect  (tCliHandle CliHandle,INT4 i4SetAttackValue)
{
  
     UINT4               u4Error = FWL_ZERO;
           if (nmhTestv2FwlDosAttackAcceptRedirect (&u4Error,
                            (INT4) i4SetAttackValue) !=       SNMP_SUCCESS)
           {
                       CliPrintf (CliHandle, "%%Invalid value !\r\n");
                        return (CLI_FAILURE);
           }
           if(nmhSetFwlDosAttackAcceptRedirect (i4SetAttackValue) != SNMP_SUCCESS)
		   {
				CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;

		   }

return CLI_SUCCESS;
            
}
/***********************************************************************/
/*  Function Name : FwlCliSetFwlDosLandAttack                          */
/*  Description   : Function which is used to enable DOS smurf attack   */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlDosAttackSmurf  (tCliHandle CliHandle,INT4 i4SetAttackValue)
{
  
     UINT4               u4Error = FWL_ZERO;
           if (nmhTestv2FwlDosAttackAcceptSmurfAttack (&u4Error,
                            i4SetAttackValue) !=       SNMP_SUCCESS)
           {
                       CliPrintf (CliHandle, "%%Invalid value !\r\n");
                        return (CLI_FAILURE);
           }
           if(nmhSetFwlDosAttackAcceptSmurfAttack (i4SetAttackValue) != SNMP_SUCCESS)
	       {
				CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
		   }
return CLI_SUCCESS;
            
}

INT4
FwlCliSetIcmpErrorMessage (tCliHandle CliHandle, UINT4 u4IcmpError)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalICMPControlSwitch (&u4Error,
                                             (INT4) u4IcmpError) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Invalid value !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlGlobalICMPControlSwitch (u4IcmpError);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetIcmpv6ErrorMessage (tCliHandle CliHandle, UINT4 u4IcmpError)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalICMPv6ControlSwitch (&u4Error,
                                               (INT4) u4IcmpError) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Invalid value !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlGlobalICMPv6ControlSwitch (u4IcmpError);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetIPSpoofing (tCliHandle CliHandle, UINT4 u4VerifyRevPath)
{
    UINT4               u4Error = FWL_ZERO;
    if (nmhTestv2FwlGlobalIpSpoofFiltering (&u4Error,
                                            (INT4) u4VerifyRevPath) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Ip Spoofing can take only values"
                   "enable or disable !\r\n");
        return (CLI_FAILURE);
    }
    if(nmhSetFwlGlobalIpSpoofFiltering (u4VerifyRevPath) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    
    return (CLI_SUCCESS);
}

/***********************************************************************/
/*  Function Name : FwlCliSetRpfCheck                                  */
/*  Description   : Function which is used to enable Rpf check         */
/*                  on interface                                       */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/

INT4
FwlCliSetRpfCheck (tCliHandle CliHandle, INT4 i4RpfCheck , INT4 i4RpfMode)
{
     INT4                i4RetVal = FWL_ZERO;
     INT4                i4IfIndex = FWL_ZERO;
     i4IfIndex = CLI_GET_IFINDEX ();

     if(i4RpfCheck == FWL_ONE)
     {
          i4RetVal = FwlCliSetFwlRpfCheckEnable(CliHandle,i4IfIndex,
                                        i4RpfMode);
     }
     else
     {
          i4RetVal = FwlCliSetFwlRpfCheckDisable(CliHandle,i4IfIndex,
                                        i4RpfMode);
     }

   
    return (i4RetVal);
}

/***********************************************************************/
/*  Function Name : FwlCliSetFwlRpfCheckEnable                         */
/*  Description   : Function which is used to enable Rpf check         */
/*                  on interface                                       */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/


INT4
FwlCliSetFwlRpfCheckEnable  (tCliHandle CliHandle,INT4 i4IfIndex,INT4 i4RpfMode)
{

 UINT4               u4ErrorCode = 0;
    if (nmhTestv2FwlRpfRowStatus(&u4ErrorCode,i4IfIndex,FWL_CREATE_AND_WAIT)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRpfRowStatus(i4IfIndex,FWL_CREATE_AND_WAIT)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FwlRpfMode(&u4ErrorCode,i4IfIndex,i4RpfMode)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRpfMode(i4IfIndex,i4RpfMode)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FwlRpfRowStatus(&u4ErrorCode,i4IfIndex,FWL_ACTIVE)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRpfRowStatus(i4IfIndex,FWL_ACTIVE)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }


    return CLI_SUCCESS;

}

/***********************************************************************/
/*  Function Name : FwlCliSetFwlRpfCheckDisable                        */
/*  Description   : Function which is used to enable Rpf check         */
/*                  on interface                                       */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS/CLI_FAILURE                            */
/***********************************************************************/



INT4
FwlCliSetFwlRpfCheckDisable  (tCliHandle CliHandle,INT4 i4IfIndex ,INT4 i4RpfMode)
{
        UINT4               u4ErrorCode = 0;
    if (nmhTestv2FwlRpfRowStatus(&u4ErrorCode,i4IfIndex,FWL_DESTROY)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2FwlRpfMode(&u4ErrorCode,i4IfIndex,i4RpfMode)
                                    == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFwlRpfMode(i4IfIndex,i4RpfMode)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }


    if (nmhSetFwlRpfRowStatus(i4IfIndex,FWL_DESTROY)
                                        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

return CLI_SUCCESS;
}



INT4
FwlCliSetIPv6Spoofing (tCliHandle CliHandle, UINT4 u4VerifyRevPath)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalIpv6SpoofFiltering (&u4Error,
                                              (INT4) u4VerifyRevPath) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Ip Spoofing can take only values"
                   "enable or disable !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlGlobalIpv6SpoofFiltering (u4VerifyRevPath);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetIPSourceRoute (tCliHandle CliHandle, UINT4 u4SrcRoute)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalSrcRouteFiltering (&u4Error,
                                             (INT4) u4SrcRoute) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%IP Source Route can only take a value "
                   "greater than zero !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlGlobalSrcRouteFiltering (u4SrcRoute);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetTcpIntercept (tCliHandle CliHandle, UINT4 u4InspectTcp)
{
    UINT4               u4Error = FWL_ZERO;
    UINT4               u4FwlSwitch = FWL_ZERO;

    nmhGetFwlGlobalMasterControlSwitch ((INT4 *) &u4FwlSwitch);

    if (u4FwlSwitch != FWL_ENABLE)
    {
        CliPrintf (CliHandle, "%%Inspect TCP option can be enabled only "
                   "after enabling Firewall\r\n So First Enable "
                   "Firewall !\r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2FwlGlobalTcpIntercept (&u4Error, (INT4) u4InspectTcp) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%TCP Intercept can only take a value "
                   "greater than zero !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlGlobalTcpIntercept (u4InspectTcp);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetTcpInterceptThreshold (tCliHandle CliHandle, UINT4 u4SynPackets)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlDefnTcpInterceptThreshold (&u4Error,
                                               (INT4) u4SynPackets) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Ip inspect tcp half open can take values"
                   "only between 0 and 65536 !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlDefnTcpInterceptThreshold (u4SynPackets);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetTcpSynWait (tCliHandle CliHandle, UINT4 u4SynWait)
{
    UINT4               u4Error = FWL_ZERO;
    INT4                i4TcpInterceptEnable = FWL_INVALID;

    nmhGetFwlGlobalTcpIntercept (&i4TcpInterceptEnable);
    if (i4TcpInterceptEnable != FWL_ENABLE)
    {
        CliPrintf (CliHandle, "%%Enable the TCP intercept option !\r\n");
        return (CLI_FAILURE);
    }
    if (nmhTestv2FwlDefnInterceptTimeout (&u4Error, u4SynWait) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Ip inspect tcp syn wait can take values"
                   " only between 0 and 65536 !\r\n");
        return (CLI_FAILURE);
    }
    nmhSetFwlDefnInterceptTimeout (u4SynWait);
    return (CLI_SUCCESS);
}

INT4
FwlCliDeleteFilter (tCliHandle CliHandle, UINT1 *pu1FilterName)
{
    tSNMP_OCTET_STRING_TYPE FilterName;
    UINT1               au1FilterName[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };
    UINT4               u4FilterExists = FWL_ZERO;

    if (STRCMP (pu1FilterName, "default") == FWL_ZERO)
    {
        CliPrintf (CliHandle, "%%Cannot delete default Filter !\r\n");
        return (CLI_FAILURE);
    }

    FilterName.i4_Length = (INT4) STRLEN (pu1FilterName);
    FilterName.pu1_OctetList = au1FilterName;
    MEMCPY (FilterName.pu1_OctetList, pu1FilterName, FilterName.i4_Length);

    if (nmhTestv2FwlFilterRowStatus (&u4FilterExists,
                                     &FilterName, FWL_DESTROY) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    nmhSetFwlFilterRowStatus (&FilterName, FWL_DESTROY);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliSetFwlFilterAccounting                       */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disable packet accounting    */
/*                          for a particular filter.                          */
/*                                                                           */
/*     INPUT            : pu1FilterName      - Filter Name                   */
/*                        u4FilterAccounting - Packet accounting enable or   */
/*                                               disable                      */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliSetFwlFilterAccounting (tCliHandle CliHandle, UINT1 *pu1FilterName,
                              UINT4 u4FilterAccounting)
{
    tSNMP_OCTET_STRING_TYPE FilterName;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT1                i1RetVal = CLI_SUCCESS;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    MEMSET (au1FilterName, FWL_ZERO, sizeof (au1FilterName));
    i1RetVal = CLI_SUCCESS;

    FilterName.i4_Length = (INT4) STRLEN (pu1FilterName);
    FilterName.pu1_OctetList = au1FilterName;
    MEMCPY (FilterName.pu1_OctetList, pu1FilterName, FilterName.i4_Length);

    if (nmhTestv2FwlFilterAccounting (&u4ErrorCode, &FilterName,
                                      (INT4) u4FilterAccounting) ==
        SNMP_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_NO_CREATION)
        {
            CliPrintf (CliHandle, "%%ERROR: Filter  %s does not exist \r\n",
                       pu1FilterName);
            i1RetVal = CLI_FAILURE;
        }
        else
        {
            CliPrintf (CliHandle,
                       "%%ERROR: Invalid filter Accounting value %d \r\n",
                       u4FilterAccounting);
            i1RetVal = CLI_FAILURE;
        }
    }
    else if (nmhSetFwlFilterAccounting (&FilterName, u4FilterAccounting)
             == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%ERROR: Set routine for Filter Accouting"
                   " failed!\r\n");
        i1RetVal = CLI_FAILURE;
    }
    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliClearFwlFilterAccounting                     */
/*                                                                           */
/*     DESCRIPTION      : This function clears the packet accounting          */
/*                        information for a particular filter.               */
/*                                                                           */
/*     INPUT            : pu1FilterName      - Filter Name                   */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliClearFwlFilterAccounting (tCliHandle CliHandle, UINT1 *pu1FilterName)
{

    tSNMP_OCTET_STRING_TYPE FilterName;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT1                i1RetVal = CLI_SUCCESS;
    UINT1               au1FilterName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };

    MEMSET (au1FilterName, FWL_ZERO, sizeof (au1FilterName));
    i1RetVal = CLI_SUCCESS;

    FilterName.i4_Length = (INT4) STRLEN (pu1FilterName);
    FilterName.pu1_OctetList = au1FilterName;
    MEMCPY (FilterName.pu1_OctetList, pu1FilterName, FilterName.i4_Length);

    if (nmhTestv2FwlFilterHitClear (&u4ErrorCode, &FilterName,
                                    FWL_TRUE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%ERROR: Test routine for Filter Hit Clear "
                   "failed, code %d !\r\n", u4ErrorCode);
        i1RetVal = CLI_FAILURE;
    }
    else if (nmhSetFwlFilterHitClear (&FilterName, FWL_TRUE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%ERROR: Set routine for Filter Hit Clear"
                   " failed!\r\n");
        i1RetVal = CLI_FAILURE;
    }
    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliAddFilter                                    */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall filter.           */
/*                                                                           */
/*     INPUT            : pu1FilterName - Filter Name                        */
/*                        pu1SrcAddress - Source Address                     */
/*                        pu1DestAddress - Destination Address               */
/*                        u4ProtoPres    - Flag set it protocol present      */
/*                        u4Protocol     - Protocol                          */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliAddFilter (tCliHandle CliHandle, tFwlFilterEntry * pFwlFilterEntry)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    UINT1               au1Name[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };
    UINT1               au1SrcAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    UINT1               au1DestAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4IsProtoPres = pFwlFilterEntry->u4IsProtoPres;
    UINT4               u4Protocol = pFwlFilterEntry->u4Protocol;
    UINT4               u4Error = FWL_ZERO;
    UINT1              *pu1FilterName = pFwlFilterEntry->au1FilterName;
    UINT1              *pu1SrcAddress = pFwlFilterEntry->au1SrcAddress;
    UINT1              *pu1DestAddress = pFwlFilterEntry->au1DestAddress;

    MEMSET (au1Name, FWL_ZERO, sizeof (au1Name));
    MEMSET (au1SrcAddr, FWL_ZERO, sizeof (au1SrcAddr));
    MEMSET (au1DestAddr, FWL_ZERO, sizeof (au1DestAddr));

    Name.i4_Length = (INT4) STRLEN (pu1FilterName);
    Name.pu1_OctetList = au1Name;
    MEMCPY (Name.pu1_OctetList, pu1FilterName, Name.i4_Length);

    DestAddr.i4_Length = (INT4) STRLEN (pu1DestAddress);
    DestAddr.pu1_OctetList = au1DestAddr;
    MEMCPY (DestAddr.pu1_OctetList, pu1DestAddress, DestAddr.i4_Length);
    SrcAddr.i4_Length = (INT4) STRLEN (pu1SrcAddress);
    SrcAddr.pu1_OctetList = au1SrcAddr;
    MEMCPY (SrcAddr.pu1_OctetList, pu1SrcAddress, SrcAddr.i4_Length);
    if ((i4RetVal = nmhGetFwlFilterRowStatus (&Name, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* ICSA Fix -S-
         * filter name should not be same as already 
         * existing acl name */
        INT4                i4IfaceIndex = FWL_GLOBAL_IDX;

        if (nmhGetFwlAclRowStatus (i4IfaceIndex, &Name,
                                   FWL_DIRECTION_OUT, &i4RowStatus)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%A matching ACL name already exists for "
                       "this filter name.\r\nFilter name should be "
                       "unique!\r\n");
            return (CLI_FAILURE);
        }

        if (nmhGetFwlAclRowStatus (i4IfaceIndex, &Name,
                                   FWL_DIRECTION_IN, &i4RowStatus)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%A matching ACL name already exists for "
                       "this filter name. Filter name should be" "unique!\r\n");
            return (CLI_FAILURE);
        }
        /* ICSA Fix -E- */

        if (nmhSetFwlFilterRowStatus (&Name, FWL_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Max filters configured. Cannot add "
                       "a new filter !\r\n");
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_NOT_IN_SERVICE);
        }
    }

    /* Set the Address type */
    if (nmhTestv2FwlFilterAddrType (&u4Error, &Name, FWL_IP_VERSION_4) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Invalid AddressType \r\n");
        nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
        return (CLI_FAILURE);
    }
    else
    {
        nmhSetFwlFilterAddrType (&Name, FWL_IP_VERSION_4);
    }

    if (nmhTestv2FwlFilterSrcAddress (&u4Error, &Name, &SrcAddr) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Source IP Address or range is improper \r\n"
                   " Supported formats : 1. [<range_opr>]<addr>/(0..32) \r\n"
                   "                     2. <range_opr><start_addr><range_opr>"
                   "<end_addr>\r\n"
                   " range_opr can be <,>,>=,<="
                   " addr can a.b.c.d or a.b.c or a.b"
                   "(any valid internet format)");
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
        }
        else if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
        }
        return (CLI_FAILURE);
    }

    if (nmhTestv2FwlFilterDestAddress (&u4Error, &Name, &DestAddr) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "%%Destination IP Address or range is improper \r\n"
                   " Supported formats : 1. [<range_opr>]<addr>/(0..32) \r\n"
                   "                     2. <range_opr><start_addr><range_opr>"
                   "<end_addr>\r\n"
                   " range_opr can be <,>,>=,<= \r\n"
                   " addr can a.b.c.d or a.b.c or a.b"
                   "(any valid internet format) \r\n");

        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
        }
        else if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
        }
        return (CLI_FAILURE);
    }

    if (u4IsProtoPres == TRUE)
    {
        if (nmhTestv2FwlFilterProtocol (&u4Error,
                                        &Name,
                                        (INT4) u4Protocol) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Protocol can only take one the following "
                       "values\r\n1 - ICMP , 2 - IGMP , "
                       "3 - GGP, 4 - IP , 6 - TCP , "
                       "8 - EGP , 9 - IGP , 11 - NVP , "
                       "17 - UDP , 28 - IRTP , 35 - IDPR ,"
                       "46 - RSVP , 48 - MHRP , "
                       "88 - IGRP , 89 - OSPF !\r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
            }
            else if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
            }

            return (CLI_FAILURE);
        }
    }

    /* Now update the source, destination port ranges */
    i4RetVal = FwlCliUpdateFilter (CliHandle, pFwlFilterEntry);
    if (i4RetVal == CLI_FAILURE)
    {
        nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
        return (CLI_FAILURE);
    }
    else if (i4RetVal == CLI_SUCCESS)
    {
        nmhSetFwlFilterSrcAddress (&Name, &SrcAddr);
        nmhSetFwlFilterDestAddress (&Name, &DestAddr);
        if ((u4IsProtoPres == TRUE) || (i4RetVal == SNMP_SUCCESS))
        {
            nmhSetFwlFilterProtocol (&Name, u4Protocol);
        }
        nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
        return (CLI_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliAddIPv6Filter                                */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall filter.           */
/*                                                                           */
/*     INPUT            : pu1FilterName - Filter Name                        */
/*                        pu1SrcAddress - Source Address                     */
/*                        pu1DestAddress - Destination Address               */
/*                        u4ProtoPres    - Flag set it protocol present      */
/*                        u4Protocol     - Protocol                          */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliAddIPv6Filter (tCliHandle CliHandle, tFwlFilterEntry * pFwlFilterEntry)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    UINT1               au1Name[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };
    UINT1               au1SrcAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    UINT1               au1DestAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4IsProtoPres = pFwlFilterEntry->u4IsProtoPres;
    UINT4               u4Protocol = pFwlFilterEntry->u4Protocol;
    UINT4               u4IsDscpPres = pFwlFilterEntry->u4IsDscpPres;
    UINT4               u4IsFlowIdPres = pFwlFilterEntry->u4IsFlowIdPres;
    UINT4               u4Error = FWL_ZERO;
    UINT4               u4FlowId = pFwlFilterEntry->u4FlowId;
    INT4                i4Dscp = pFwlFilterEntry->i4Dscp;
    UINT1              *pu1FilterName = pFwlFilterEntry->au1FilterName;
    UINT1              *pu1SrcAddress = pFwlFilterEntry->au1SrcAddress;
    UINT1              *pu1DestAddress = pFwlFilterEntry->au1DestAddress;

    MEMSET (au1Name, FWL_ZERO, sizeof (au1Name));
    MEMSET (au1SrcAddr, FWL_ZERO, sizeof (au1SrcAddr));
    MEMSET (au1DestAddr, FWL_ZERO, sizeof (au1DestAddr));

    Name.i4_Length = (INT4) STRLEN (pu1FilterName);
    Name.pu1_OctetList = au1Name;
    MEMCPY (Name.pu1_OctetList, pu1FilterName, Name.i4_Length);
    if (Name.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle, "\r %% ERROR: Cannot allocate memory. \r\n");
        return CLI_FAILURE;
    }

    DestAddr.i4_Length = (INT4) STRLEN (pu1DestAddress);
    DestAddr.pu1_OctetList = au1DestAddr;
    MEMCPY (DestAddr.pu1_OctetList, pu1DestAddress, DestAddr.i4_Length);

    if (DestAddr.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle, "\r %% ERROR: Cannot allocate memory. \r\n");
        return CLI_FAILURE;
    }
    SrcAddr.i4_Length = (INT4) STRLEN (pu1SrcAddress);
    SrcAddr.pu1_OctetList = au1SrcAddr;
    MEMCPY (SrcAddr.pu1_OctetList, pu1SrcAddress, SrcAddr.i4_Length);
    if (SrcAddr.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle, "\r %% ERROR: Cannot allocate memory. \r\n");
        return CLI_FAILURE;
    }

    if ((i4RetVal = nmhGetFwlFilterRowStatus (&Name, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* ICSA Fix -S-
         * filter name should not be same as already
         * existing acl name */
        INT4                i4IfaceIndex = FWL_GLOBAL_IDX;

        if (nmhGetFwlAclRowStatus (i4IfaceIndex, &Name,
                                   FWL_DIRECTION_OUT,
                                   &i4RowStatus) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%A matching ACL name already exists for "
                       "this filter name.\r\nFilter name should be "
                       "unique!\r\n");
            return (CLI_FAILURE);
        }

        if (nmhGetFwlAclRowStatus (i4IfaceIndex, &Name,
                                   FWL_DIRECTION_IN,
                                   &i4RowStatus) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%A matching ACL name already exists for "
                       "this filter name. Filter name should be" "unique!\r\n");
            return (CLI_FAILURE);
        }
        /* ICSA Fix -E- */

        if (nmhSetFwlFilterRowStatus (&Name, FWL_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Max filters configured. Cannot add "
                       "a new filter !\r\n");
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_NOT_IN_SERVICE);
        }
    }

    /* Set the Address type */
    if (nmhTestv2FwlFilterAddrType (&u4Error, &Name, FWL_IP_VERSION_6) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Invalid AddressType \r\n");
        nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
        return (CLI_FAILURE);
    }
    else
    {
        nmhSetFwlFilterAddrType (&Name, FWL_IP_VERSION_6);
    }

    if (Name.i4_Length < FWL_MAX_FILTER_NAME_LEN)
    {

        if (nmhTestv2FwlFilterSrcAddress (&u4Error, &Name, &SrcAddr) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Source IPv6 Address/range is improper \r\n"
                       " Supported formats : 1. [<range_opr>]<addr>/(0..128) \r\n"
                       "                     2. <range_opr><start_addr>\
                     <range_opr><end_addr>\r\n" " range_opr can be <,>,>=,<= \r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
            }
            else if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
            }
            return (CLI_FAILURE);
        }
    }
    if (Name.i4_Length < FWL_MAX_FILTER_NAME_LEN)
    {
        if (nmhTestv2FwlFilterDestAddress (&u4Error, &Name, &DestAddr) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Destination IP Address or range is"
                       "improper \r\\n"
                       " Supported formats : 1. [<range_opr>]<addr>/(0..128) \r\n"
                       "                     2. <range_opr><start_addr>\
                       <range_opr><end_addr>\r\n" " range_opr can be <,>,>=,<= \r\n");

            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
            }
            else if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
            }
            return (CLI_FAILURE);
        }
    }
    if (u4IsProtoPres == TRUE)
    {
        if (nmhTestv2FwlFilterProtocol (&u4Error,
                                        &Name,
                                        (INT4) u4Protocol) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Protocol can only take one the following "
                       "values\r\n1 - ICMP , 2 - IGMP , "
                       "3 - GGP, 4 - IP , 6 - TCP , "
                       "8 - EGP , 9 - IGP , 11 - NVP , "
                       "17 - UDP , 28 - IRTP , 35 - IDPR ,"
                       "46 - RSVP , 48 - MHRP , "
                       "88 - IGRP , 89 - OSPF !\r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
            }
            else if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
            }

            return (CLI_FAILURE);
        }
    }
    if (u4IsDscpPres == TRUE)
    {
        if (nmhTestv2FwlFilterDscp (&u4Error, &Name, i4Dscp) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Invalid Dscp Value!!!"
                       "Valid Range is 0 to 63! \r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
            }
            else if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
            }

            return (CLI_FAILURE);
        }
        else
        {
            nmhSetFwlFilterDscp (&Name, i4Dscp);
        }
    }
    if (u4IsFlowIdPres == TRUE)
    {
        if (nmhTestv2FwlFilterFlowId (&u4Error,
                                      &Name, u4FlowId) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Invalid Flow Lable!!!"
                       "Valid Range is 0 to 1048575!\r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
            }
            else if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
            }

            return (CLI_FAILURE);
        }
        else
        {
            nmhSetFwlFilterFlowId (&Name, u4FlowId);
        }
    }

    /* Now update the source, destination port ranges */
    i4RetVal = FwlCliUpdateFilter (CliHandle, pFwlFilterEntry);
    if (i4RetVal == CLI_FAILURE)
    {
        nmhSetFwlFilterRowStatus (&Name, FWL_DESTROY);
        return (CLI_FAILURE);
    }
    else if (i4RetVal == CLI_SUCCESS)
    {
        if ((Name.i4_Length < FWL_MAX_FILTER_NAME_LEN) &&
            (SrcAddr.i4_Length < FWL_MAX_ADDR_LEN) &&
            (DestAddr.i4_Length < FWL_MAX_ADDR_LEN))
        {
            nmhSetFwlFilterSrcAddress (&Name, &SrcAddr);
            nmhSetFwlFilterDestAddress (&Name, &DestAddr);
        }
        if ((u4IsProtoPres == TRUE) || (i4RetVal == SNMP_SUCCESS))
        {
            nmhSetFwlFilterProtocol (&Name, u4Protocol);
        }
        nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
        return (CLI_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliUpdateFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall filter.           */
/*                                                                           */
/*     INPUT            : pu1FilterName - Filter Name                        */
/*                        pu1SrcAddress - Source Address                     */
/*                        pu1DestAddress - Destination Address               */
/*                        u4ProtoPres    - Flag set it protocol present      */
/*                        u4Protocol     - Protocol                          */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliUpdateFilter (tCliHandle CliHandle, tFwlFilterEntry * pFwlFilterEntry)
{
    UINT1              *pu1FilterName = pFwlFilterEntry->au1FilterName;
    UINT4               u4SrcPortPres = pFwlFilterEntry->u4SrcPortPres;
    UINT1              *pu1SrcPortRange = pFwlFilterEntry->au1SrcPortRange;
    UINT4               u4DestPortPres = pFwlFilterEntry->u4DestPortPres;
    UINT1              *pu1DestPortRange = pFwlFilterEntry->au1DestPortRange;
    UINT4               u4EsPres = pFwlFilterEntry->u4EsPres;
    UINT4               u4Established = pFwlFilterEntry->u4Established;
    UINT4               u4RstPres = pFwlFilterEntry->u4RstPres;
    UINT4               u4Reset = pFwlFilterEntry->u4Reset;
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE DestPort;
    tSNMP_OCTET_STRING_TYPE SrcPort;
    UINT1               au1Name[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1DestPort[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1SrcPort[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    INT4                i4RowStatus = FWL_ZERO;
    UINT4               u4Error = FWL_ZERO;

    MEMSET (au1Name, FWL_ZERO, FWL_MAX_FILTER_NAME_LEN);

    Name.i4_Length = (INT4) STRLEN (pu1FilterName);
    Name.pu1_OctetList = au1Name;
    MEMCPY (Name.pu1_OctetList, pu1FilterName, Name.i4_Length);
    Name.pu1_OctetList[Name.i4_Length] = FWL_END_OF_STRING;

    if ((u4DestPortPres == FALSE) &&
        (u4SrcPortPres == FALSE) && (u4EsPres == FALSE) && (u4RstPres == FALSE))
    {
        return (CLI_SUCCESS);
    }

    if (nmhGetFwlFilterRowStatus (&Name, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%No such filter exists.\r\n"
                   "First add the filter first using the "
                   "\"firewall filter add\" command !\r\n");
        return (CLI_FAILURE);
    }

    if (u4SrcPortPres == TRUE)
    {
        SrcPort.i4_Length = (INT4) STRLEN (pu1SrcPortRange);
        SrcPort.pu1_OctetList = au1SrcPort;
        MEMCPY (SrcPort.pu1_OctetList, pu1SrcPortRange, SrcPort.i4_Length);

        if (nmhTestv2FwlFilterSrcPort (&u4Error, &Name, &SrcPort) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Specify Source Port range (1..65535) as: >,<,=,>=,<=.\r\n"
                       "Example: >6000 or <=9000 or =540 etc\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4DestPortPres == TRUE)
    {
        DestPort.i4_Length = (INT4) STRLEN (pu1DestPortRange);
        DestPort.pu1_OctetList = au1DestPort;
        MEMCPY (DestPort.pu1_OctetList, pu1DestPortRange, DestPort.i4_Length);

        if (nmhTestv2FwlFilterDestPort (&u4Error, &Name, &DestPort) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Specify Destination Port range (1..65535) as: >,<,=,>=,<=.\r\n"
                       "Example: >6000 or <=9000 or =540 etc\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4EsPres == TRUE)
    {
        if (nmhTestv2FwlFilterAckBit (&u4Error, &Name, (INT4) u4Established) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Improper value for Established !\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4RstPres == TRUE)
    {
        if (nmhTestv2FwlFilterRstBit (&u4Error, &Name, (INT4) u4Reset) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Improper value for Reset !\r\n");
            return (CLI_FAILURE);
        }
    }

    if (nmhGetFwlFilterRowStatus (&Name, &i4RowStatus) != SNMP_SUCCESS)
    {
        /* NOTE: this section of the code will never be entered */
        return (CLI_FAILURE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_NOT_IN_SERVICE);
        }
    }

    if (u4SrcPortPres == TRUE)
    {
        nmhSetFwlFilterSrcPort (&Name, &SrcPort);
    }

    if (u4DestPortPres == TRUE)
    {
        nmhSetFwlFilterDestPort (&Name, &DestPort);
    }

    if (u4EsPres == TRUE)
    {
        nmhSetFwlFilterAckBit (&Name, u4Established);
    }

    if (u4RstPres == TRUE)
    {
        nmhSetFwlFilterRstBit (&Name, u4Reset);
    }

    nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
    return (CLI_SUCCESS);
}

INT4
FwlCliAddAccessList (tCliHandle CliHandle, tFwlAccessList * pFwlAccessList)
{
    UINT1              *pu1AclName = pFwlAccessList->au1AclName;
    UINT1              *pu1FilterName = pFwlAccessList->au1FilterComb;
    UINT4               u4Interface = pFwlAccessList->u4Interface;
    UINT4               u4Direction = pFwlAccessList->u4Direction;
    UINT4               u4Permit = pFwlAccessList->u4Permit;
    UINT4               u4Priority = pFwlAccessList->u4Priority;
    UINT4               u4LogTrigger = (UINT4) pFwlAccessList->u4LogTrigger;
    UINT2               u2FragAction = (UINT2) pFwlAccessList->u4FragAction;
    tSNMP_OCTET_STRING_TYPE AclName;
    tSNMP_OCTET_STRING_TYPE FilterSet;
    UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT1               au1FilterSet[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    INT4                i4RowStatus = FWL_ZERO;
    UINT4               u4Error = FWL_ZERO;
    UINT4               u4NewAclFlag = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;

    AclName.i4_Length = (INT4) STRLEN (pu1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, pu1AclName, AclName.i4_Length);

    FilterSet.i4_Length = (INT4) STRLEN (pu1FilterName);
    FilterSet.pu1_OctetList = au1FilterSet;
    MEMCPY (FilterSet.pu1_OctetList, pu1FilterName, FilterSet.i4_Length);

    if ((FilterSet.pu1_OctetList == NULL) || (AclName.pu1_OctetList == NULL))
    {
        CliPrintf (CliHandle, "%%Access List could not "
                   "be created due to lack of " "resources !\r\n");
        return (CLI_FAILURE);
    }

    if (!((u4Permit == FWL_DENY) || (u4Permit == FWL_PERMIT)))
    {
        CliPrintf (CliHandle, "%%Action can be either permit " "or deny !\r\n");
        return (CLI_FAILURE);
    }

    if (u4Priority <= FWL_ZERO)
    {
        CliPrintf (CliHandle, "%%Priority should be greater than zero !\r\n");
        return (CLI_FAILURE);
    }

    /* ICSA Fix -S-
     * ACL name should not be same as already 
     * filter name */

    if ((i4RetVal = nmhGetFwlFilterRowStatus (&AclName, &i4RowStatus))
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%A matching Filter name already exists for "
                   "this ACL name.\r\nACL name should be " "unique!\r\n");
        return (CLI_FAILURE);
    }
    /* ICSA Fix -E- */

    if ((i4RetVal = nmhGetFwlRuleRowStatus (&AclName, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        if (nmhSetFwlRuleRowStatus (&AclName, FWL_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);

        }
        if (nmhTestv2FwlRuleFilterSet (&u4Error, &AclName, &FilterSet) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Check the filter set specified or specify "
                       "filter combination like filter1,filter2 !\r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlRuleRowStatus (&AclName, FWL_DESTROY);
            }
            return (CLI_FAILURE);
        }

        if (nmhSetFwlRuleFilterSet (&AclName, &FilterSet) != SNMP_SUCCESS)
        {
            nmhSetFwlRuleRowStatus (&AclName, FWL_DESTROY);
            return (CLI_FAILURE);
        }
    }
    else
    {
        tSNMP_OCTET_STRING_TYPE RuleFilterSet;
        UINT1               au1RuleFilterSet[MAX_OCTETSTRING_SIZE] =
            { FWL_ZERO };

        RuleFilterSet.i4_Length = FWL_INDEX_256;
        RuleFilterSet.pu1_OctetList = au1RuleFilterSet;

        /* Rule exists. Now get the filter set for the corresponding acl name */
        if (nmhGetFwlRuleFilterSet (&AclName, &RuleFilterSet) == SNMP_SUCCESS)
        {
            RuleFilterSet.pu1_OctetList[RuleFilterSet.i4_Length] = FWL_ZERO;
            FilterSet.pu1_OctetList[FilterSet.i4_Length] = FWL_ZERO;

            /* Check if the filter set is different from the user specified one.
             * If so dont allow the user to modify the filter set */
            if (STRCMP (RuleFilterSet.pu1_OctetList,
                        FilterSet.pu1_OctetList) != FWL_ZERO)
            {
                CliPrintf (CliHandle, "%%Filter Set cannot be modified\r\n");
                return (CLI_FAILURE);
            }
        }
    }
    nmhSetFwlRuleRowStatus (&AclName, FWL_ACTIVE);
    if (nmhGetFwlAclRowStatus ((INT4) u4Interface, &AclName,
                               (INT4) u4Direction,
                               &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2FwlAclRowStatus
            (&u4Error, (INT4) u4Interface, &AclName, (INT4) u4Direction,
             FWL_CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            nmhSetFwlRuleRowStatus (&AclName, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        if (nmhSetFwlAclRowStatus (u4Interface, &AclName, u4Direction,
                                   FWL_CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        u4NewAclFlag = FWL_ONE;
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlAclRowStatus (u4Interface, &AclName, u4Direction,
                                   FWL_NOT_IN_SERVICE);
        }
    }

    nmhSetFwlAclAction (u4Interface, &AclName, u4Direction, u4Permit);

    i4RetVal =
        nmhTestv2FwlAclSequenceNumber (&u4Error, (INT4) u4Interface, &AclName,
                                       (INT4) u4Direction, (INT4) u4Priority);
    if (SNMP_FAILURE == i4RetVal)
    {
        if (FWL_ONE == u4NewAclFlag)
        {
            nmhSetFwlAclRowStatus (u4Interface, &AclName, u4Direction, DESTROY);
        }
        return (CLI_FAILURE);
    }

    if (nmhSetFwlAclSequenceNumber (u4Interface, &AclName, u4Direction,
                                    u4Priority) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Cannot assign the given priority "
                   "since an Access list already \r\n"
                   "exists with this priority !\r\n");
        return (CLI_FAILURE);
    }

    nmhSetFwlAclLogTrigger (u4Interface, &AclName, u4Direction, u4LogTrigger);
    nmhSetFwlAclFragAction (u4Interface, &AclName, u4Direction, u2FragAction);

    nmhSetFwlAclRowStatus (u4Interface, &AclName, u4Direction, FWL_ACTIVE);

    return (CLI_SUCCESS);
}

INT4
FwlCliDeleteAccessList (tCliHandle CliHandle,
                        UINT4 u4Interface, UINT1 *pu1AclName, UINT4 u4Direction)
{
    INT4                i4Status = SNMP_FAILURE;
    UINT4               u4ErrorCode = FWL_ZERO;
    tSNMP_OCTET_STRING_TYPE AclName;
    UINT1               au1AclName[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };

    AclName.i4_Length = (INT4) STRLEN (pu1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, pu1AclName, AclName.i4_Length);
    i4Status = nmhSetFwlAclRowStatus (u4Interface,
                                      &AclName, u4Direction, FWL_DESTROY);
    if (i4Status == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% ACL does not exist !\r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2FwlRuleRowStatus (&u4ErrorCode,
                                   &AclName, FWL_DESTROY) == SNMP_SUCCESS)
    {
        nmhSetFwlRuleRowStatus (&AclName, FWL_DESTROY);
    }

    return (CLI_SUCCESS);
}

INT4
FwlCliSetIpOption (tCliHandle CliHandle, UINT4 u4Interface, UINT4 u4IpOption)
{
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Error = FWL_ZERO;

    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         * testing */
        nmhSetFwlIfRowStatus (u4Interface, FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
        }
    }

    if (nmhTestv2FwlIfIpOptions
        (&u4Error, (INT4) u4Interface, (INT4) u4IpOption) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Ip Options should be one of the "
                   "following values \r\n1 2 3 4 5 6 !\r\n");
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
            return (CLI_FAILURE);
        }
    }
    nmhSetFwlIfIpOptions (u4Interface, u4IpOption);
    nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);

    return (CLI_SUCCESS);
}

INT4
FwlCliSetIcmpOption (tCliHandle CliHandle,
                     UINT4 u4Interface, UINT4 u4IcmpType, UINT4 u4IcmpCode)
{
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Error = FWL_ZERO;

    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         * testing */
        nmhSetFwlIfRowStatus (u4Interface, FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
        }
    }

    if (nmhTestv2FwlIfICMPType (&u4Error, (INT4) u4Interface, (INT4) u4IcmpType)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%ICMP type should be one of the "
                   "following values \r\n0 3 4 5 8 11 12 "
                   "13 14 15 16 17 18 !\r\n");
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2FwlIfICMPCode (&u4Error, (INT4) u4Interface, (INT4) u4IcmpCode)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%ICMP Code should be one of the "
                   "following values \r\n1 2 3 4 5 6 7 8 9 " "10 11 12 !\r\n");
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
            return (CLI_FAILURE);
        }
        return (CLI_FAILURE);
    }
    nmhSetFwlIfICMPType (u4Interface, u4IcmpType);
    nmhSetFwlIfICMPCode (u4Interface, u4IcmpCode);
    nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);

    return (CLI_SUCCESS);
}

INT4
FwlCliSetIcmpv6Option (tCliHandle CliHandle, UINT4 u4Interface, INT4 i4IcmpType)
{
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Error = FWL_ZERO;

    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         * testing */
        nmhSetFwlIfRowStatus (u4Interface, FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
        }
    }

    if (nmhTestv2FwlIfICMPv6MsgType (&u4Error, (INT4) u4Interface, i4IcmpType)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%ICMPv6 message type is not valid !!\r\n");
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
            return (CLI_FAILURE);
        }
    }
    nmhSetFwlIfICMPv6MsgType (u4Interface, i4IcmpType);
    nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);

    return (CLI_SUCCESS);
}

INT4
FwlCliSetFragOption (tCliHandle CliHandle,
                     UINT4 u4Interface, UINT4 u4Fragment, UINT4 u4FragmentSize)
{
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Error = FWL_ZERO;

    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        /* DSL_ADD : Change FWL_CREATE_AND_GO to FWL_CREATE_AND_WAIT after
         * testing */
        nmhSetFwlIfRowStatus (u4Interface, FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
        }
    }

    if (nmhTestv2FwlIfFragments
        (&u4Error, (INT4) u4Interface, (INT4) u4Fragment) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Argument should be one of the"
                   "following values \r\ntiny " "large none !\r\n");
        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
            return (CLI_FAILURE);
        }
        return (CLI_FAILURE);
    }

    nmhSetFwlIfFragments (u4Interface, u4Fragment);

    /* Set fragent size if fragment type is Large */
    if (u4Fragment == FWL_LARGE_FRAGMENT)
    {
        if (nmhTestv2FwlIfFragmentSize (&u4Error, (INT4) u4Interface,
                                        u4FragmentSize) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Fragment size should be in the "
                       "range 1..65500!\r\n");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
                return (CLI_FAILURE);
            }
            if (i4RowStatus == FWL_ACTIVE)
            {
                nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
                return (CLI_FAILURE);
            }
            return (CLI_FAILURE);
        }
        nmhSetFwlIfFragmentSize (u4Interface, u4FragmentSize);
    }

    nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);

    return (CLI_SUCCESS);
}

INT4
FwlCliFormatStatus (tCliHandle CliHandle, INT4 i4Status, CONST CHR1 * pu1String)
{

    if (i4Status == FWL_ENABLE)
    {
        CliPrintf (CliHandle, pu1String, "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, pu1String, "Disabled");
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliShowFwlGlobalStats (tCliHandle CliHandle)
{
    tCliFwlShowStats    CliFwlGlobalStats;

    nmhGetFwlStatInspectedPacketsCount (&CliFwlGlobalStats.u4PktsCount);

    nmhGetFwlStatTotalPacketsDenied (&CliFwlGlobalStats.u4PktsDenied);

    nmhGetFwlStatTotalPacketsAccepted (&CliFwlGlobalStats.u4PktsAccepted);

    nmhGetFwlStatTotalIcmpPacketsDenied (&CliFwlGlobalStats.u4IcmpDenied);

    nmhGetFwlStatTotalSynPacketsDenied (&CliFwlGlobalStats.u4SynDenied);

    nmhGetFwlStatTotalIpSpoofedPacketsDenied (&CliFwlGlobalStats.
                                              u4SpoofsDenied);

    nmhGetFwlStatTotalSrcRoutePacketsDenied (&CliFwlGlobalStats.
                                             u4SrcRouteDenied);

    nmhGetFwlStatTotalTinyFragmentPacketsDenied (&CliFwlGlobalStats.
                                                 u4TinyFragmentDenied);

    nmhGetFwlStatTotalLargeFragmentPacketsDenied (&CliFwlGlobalStats.
                                                  u4LargeFragmentDenied);

    nmhGetFwlStatTotalFragmentedPacketsDenied (&CliFwlGlobalStats.
                                               u4TotalFragmentDenied);

    nmhGetFwlStatTotalIpOptionPacketsDenied (&CliFwlGlobalStats.
                                             u4IpOptionsDenied);

    nmhGetFwlStatTotalAttacksPacketsDenied (&CliFwlGlobalStats.u4AttacksDenied);
    FwlCliFormatShowGlobalStats (CliHandle, &CliFwlGlobalStats);
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatShowGlobalStats (tCliHandle CliHandle,
                             tCliFwlShowStats * pCliFwlGlobalStats)
{

    CliPrintf (CliHandle, "\r\nFirewall Statistics\r\n\r\n");
    CliPrintf (CliHandle, "Total Packets Inspected          : %lu\r\n",
               pCliFwlGlobalStats->u4PktsCount);
    CliPrintf (CliHandle, "Total Packets Dropped            : %lu\r\n",
               pCliFwlGlobalStats->u4PktsDenied);
    CliPrintf (CliHandle, "Total Packets Accepted           : %lu\r\n\r\n",
               pCliFwlGlobalStats->u4PktsAccepted);
    CliPrintf (CliHandle, "ICMP Packets Dropped             : %lu\r\n",
               pCliFwlGlobalStats->u4IcmpDenied);
    CliPrintf (CliHandle, "SYN Packets Dropped              : %lu\r\n",
               pCliFwlGlobalStats->u4SynDenied);
    CliPrintf (CliHandle, "IP Spoofed Packets Dropped       : %lu\r\n",
               pCliFwlGlobalStats->u4SpoofsDenied);
    CliPrintf (CliHandle, "Source Route Packets Dropped     : %lu\r\n",
               pCliFwlGlobalStats->u4SrcRouteDenied);
    CliPrintf (CliHandle, "Tiny Fragment Packets Dropped    : %lu\r\n",
               pCliFwlGlobalStats->u4TinyFragmentDenied);
    CliPrintf (CliHandle, "Large Fragment Packets Dropped   : %lu\r\n",
               pCliFwlGlobalStats->u4LargeFragmentDenied);
    CliPrintf (CliHandle, "Total Fragment Packets Dropped   : %lu\r\n",
               pCliFwlGlobalStats->u4TotalFragmentDenied);
    CliPrintf (CliHandle, "Packets with IP Options Dropped  : %lu\r\n",
               pCliFwlGlobalStats->u4IpOptionsDenied);
    CliPrintf (CliHandle, "Suspicious Attacks Dropped       : %lu\r\n",
               pCliFwlGlobalStats->u4AttacksDenied);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

INT4
FwlCliShowFwlIPv6GlobalStats (tCliHandle CliHandle)
{
    tCliFwlShowStats    CliFwlGlobalStats;

    nmhGetFwlStatIPv6InspectedPacketsCount (&CliFwlGlobalStats.u4IPv6PktsCount);

    nmhGetFwlStatIPv6TotalPacketsDenied (&CliFwlGlobalStats.u4IPv6PktsDenied);

    nmhGetFwlStatIPv6TotalPacketsAccepted
        (&CliFwlGlobalStats.u4IPv6PktsAccepted);

    nmhGetFwlStatIPv6TotalIcmpPacketsDenied
        (&CliFwlGlobalStats.u4IPv6IcmpDenied);

    nmhGetFwlStatIPv6TotalSpoofedPacketsDenied
        (&CliFwlGlobalStats.u4IPv6SpoofDenied);

    nmhGetFwlStatIPv6TotalAttacksPacketsDenied
        (&CliFwlGlobalStats.u4IPv6AttackPktsDenied);
    FwlCliFormatShowIPv6GlobalStats (CliHandle, &CliFwlGlobalStats);
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatShowIPv6GlobalStats (tCliHandle CliHandle,
                                 tCliFwlShowStats * pCliFwlGlobalStats)
{

    CliPrintf (CliHandle, "\r\nFirewall IPv6 Statistics\r\n\r\n");
    CliPrintf (CliHandle, "Total IPv6 Packets Inspected     : %lu\r\n",
               pCliFwlGlobalStats->u4IPv6PktsCount);
    CliPrintf (CliHandle, "Total IPv6 Packets Dropped       : %lu\r\n",
               pCliFwlGlobalStats->u4IPv6PktsDenied);
    CliPrintf (CliHandle, "Total IPv6 Packets Accepted      : %lu\r\n\r\n",
               pCliFwlGlobalStats->u4IPv6PktsAccepted);
    CliPrintf (CliHandle, "ICMP IPv6 Packets Dropped        : %lu\r\n",
               pCliFwlGlobalStats->u4IPv6IcmpDenied);
    CliPrintf (CliHandle, "IPv6 Spoofed Packets Dropped     : %lu\r\n",
               pCliFwlGlobalStats->u4IPv6SpoofDenied);
    CliPrintf (CliHandle, "Suspicious IPv6 Attacks Dropped  : %lu\r\n",
               pCliFwlGlobalStats->u4IPv6AttackPktsDenied);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

INT4
FwlCliShowInterfaceConfig (tCliHandle CliHandle)
{
    UINT1               au1IfName[FWL_INTERFACE_NAME_LEN] = { FWL_ZERO };
    UINT4               u4Interface = FWL_ZERO;
    UINT4               u4PrevInterface = FWL_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    INT1                i1BannerFlag = FALSE;
    INT4                i4RetValFwlIfIfType = FWL_INVALID;
    INT4                i4TrapThreshold = FWL_INVALID;

    i1RetVal = nmhGetFirstIndexFwlDefnIfTable ((INT4 *) &u4Interface);

    while (i1RetVal == SNMP_SUCCESS)
    {
        /* If the Interface is not External, then skip that while listing 
           out external interfaces (Untrusted Ports) */
        if ((u4Interface == FWL_GLOBAL_IDX) ||
            ((nmhGetFwlIfIfType ((INT4) u4Interface, &i4RetValFwlIfIfType) ==
              SNMP_SUCCESS) && (i4RetValFwlIfIfType != FWL_EXTERNAL_IF)))
        {
            u4PrevInterface = u4Interface;
            i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                      (INT4 *) &u4Interface);
            continue;
        }
        if (CfaCliGetIfName (u4Interface, (INT1 *) au1IfName) != CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid interface\r\n");
            return (CLI_FAILURE);
        }
        if (i1BannerFlag == FALSE)
        {
            CliPrintf (CliHandle, "\r\nFirewall Interface configurations");
            CliPrintf (CliHandle, "\r\n---------------------------------\r\n");
            i1BannerFlag = TRUE;
        }
        CliPrintf (CliHandle, "\r\nUntrusted Port                     : ");
        if (u4Interface != FWL_ZERO)
        {
            CliPrintf (CliHandle, "%s\r\n", au1IfName);
        }
        nmhGetFwlIfTrapThreshold ((INT4) u4Interface, &i4TrapThreshold);
        CliPrintf (CliHandle, "Packets Denied Threshold           : %ld",
                   i4TrapThreshold);
        CliPrintf (CliHandle, "\r\n");

        u4PrevInterface = u4Interface;
        i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                  (INT4 *) &u4Interface);
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliShowInterfaceStats (tCliHandle CliHandle)
{
    UINT1               au1IfName[FWL_INTERFACE_NAME_LEN] = { FWL_ZERO };
    UINT4               u4Interface = FWL_ZERO;
    UINT4               u4PrevInterface = FWL_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFwlDefnIfTable ((INT4 *) &u4Interface);
    while (i1RetVal != SNMP_FAILURE)
    {
        if (u4Interface == FWL_GLOBAL_IDX)
        {
            u4PrevInterface = u4Interface;
            i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                      (INT4 *) &u4Interface);
            continue;
        }
        if (CfaCliGetIfName (u4Interface, (INT1 *) au1IfName) != CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid interface\r\n\r\n");
            return (CLI_FAILURE);
        }
        FwlCliFormatShowInterfaceStats (CliHandle, u4Interface, au1IfName);
        u4PrevInterface = u4Interface;
        i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                  (INT4 *) &u4Interface);
    }
    return CLI_SUCCESS;
}

INT4
FwlCliFormatShowInterfaceStats (tCliHandle CliHandle,
                                UINT4 u4Interface, UINT1 *pu1IfName)
{
    UINT4               u4Val = FWL_ZERO;

    CliPrintf (CliHandle, "\r\nFirewall Interface statistics for interface %s"
               "\r\n\r\n", pu1IfName);
    nmhGetFwlStatIfPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "Packets Dropped                  : %lu\r\n", u4Val);
    nmhGetFwlStatIfPacketsAccepted ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "Packets Accepted                 : %lu\r\n", u4Val);
    nmhGetFwlStatIfSynPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "SYN Packets Dropped              : %lu\r\n", u4Val);
    nmhGetFwlStatIfIcmpPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "ICMP Packets Dropped             : %lu\r\n", u4Val);
    nmhGetFwlStatIfIpSpoofedPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "IP Spoofed Packets Dropped       : %lu\r\n", u4Val);
    nmhGetFwlStatIfSrcRoutePacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "Source Route Packets Dropped     : %lu\r\n", u4Val);
    nmhGetFwlStatIfTinyFragmentPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "Tiny Fragment Packets Dropped    : %lu\r\n", u4Val);
    nmhGetFwlStatIfFragmentPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "Fragmented Packets Dropped       : %lu\r\n", u4Val);
    nmhGetFwlStatIfIpOptionPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "Packets with IP Options Dropped  : %lu\r\n", u4Val);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

INT4
FwlCliShowIPv6InterfaceStats (tCliHandle CliHandle)
{
    UINT1               au1IfName[FWL_INTERFACE_NAME_LEN] = { FWL_ZERO };
    UINT4               u4Interface = FWL_ZERO;
    UINT4               u4PrevInterface = FWL_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexFwlDefnIfTable ((INT4 *) &u4Interface);
    while (i1RetVal != SNMP_FAILURE)
    {
        if (u4Interface == FWL_GLOBAL_IDX)
        {
            u4PrevInterface = u4Interface;
            i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                      (INT4 *) &u4Interface);
            continue;
        }
        if (CfaCliGetIfName (u4Interface, (INT1 *) au1IfName) != CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid interface\r\n\r\n");
            return (CLI_FAILURE);
        }
        FwlCliFormatShowIPv6InterfaceStats (CliHandle, u4Interface, au1IfName);
        u4PrevInterface = u4Interface;
        i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                  (INT4 *) &u4Interface);
    }
    return CLI_SUCCESS;
}

INT4
FwlCliFormatShowIPv6InterfaceStats (tCliHandle CliHandle,
                                    UINT4 u4Interface, UINT1 *pu1IfName)
{
    UINT4               u4Val = FWL_ZERO;

    CliPrintf (CliHandle, "\r\nFirewall IPv6 Interface statisticsi"
               "for interface %s\r\n\r\n", pu1IfName);
    nmhGetFwlStatIfIPv6PacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "IPv6 Packets Dropped             : %lu\r\n", u4Val);
    nmhGetFwlStatIfIPv6PacketsAccepted ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "IPv6 Packets Accepted            : %lu\r\n", u4Val);
    nmhGetFwlStatIfIPv6IcmpPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "ICMPv6 Packets Dropped           : %lu\r\n", u4Val);
    nmhGetFwlStatIfIPv6SpoofedPacketsDenied ((INT4) u4Interface, &u4Val);
    CliPrintf (CliHandle, "IPv6 Spoofed Packets Dropped     : %lu\r\n", u4Val);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

INT4
FwlCliShowAccessLists (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE FwlAclName;
    tSNMP_OCTET_STRING_TYPE NextFwlAclName;
    tSNMP_OCTET_STRING_TYPE Temp;
    UINT1               au1FwlAclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT1               au1NextFwlAclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    UINT1               au1Temp[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
    INT4                i4NextFwlAclIfIndex = FWL_INVALID;
    INT4                i4NextFwlAclDirection = FWL_INVALID;
    INT4                i4FwlAclDirection = FWL_INVALID;
    INT4                i4FwlAclIfIndex = FWL_INVALID;
    INT4                i4rc = FWL_INVALID;
    UINT1               au1IfName[FWL_INTERFACE_NAME_LEN_MAX] = { FWL_ZERO };
    INT4                i4SeqNum = FWL_ZERO;
    INT4                i4LogTrigger = FWL_ZERO;
    INT4                i4Action = FWL_ZERO;
    INT4                i4FragAction = FWL_ZERO;

    CliPrintf (CliHandle, "\r\nFirewall Access Lists\r\n\r\n");
    CliPrintf (CliHandle,
               "     ACL Name            Iface      Filter Combination\
       Dire-  Action  Prio- Log  Fragmented\r\n");
    CliPrintf (CliHandle,
               "                                                       \
      ction          rity       Packet\r\n");
    CliPrintf (CliHandle,
               "--------------------     ---------  ---------------------\
    ------ ------  ----- ---- ----------\r\n");

    FwlAclName.pu1_OctetList = au1FwlAclName;
    NextFwlAclName.pu1_OctetList = au1NextFwlAclName;
    Temp.pu1_OctetList = au1Temp;

    i4rc =
        nmhGetFirstIndexFwlDefnAclTable (&i4FwlAclIfIndex, &FwlAclName,
                                         &i4FwlAclDirection);
    if (i4rc == SNMP_SUCCESS)
    {
        MEMCPY (NextFwlAclName.pu1_OctetList, FwlAclName.pu1_OctetList,
                FwlAclName.i4_Length);
        NextFwlAclName.i4_Length = FwlAclName.i4_Length;
        au1NextFwlAclName[NextFwlAclName.i4_Length] = FWL_END_OF_STRING;
        i4NextFwlAclIfIndex = i4FwlAclIfIndex;
        i4NextFwlAclDirection = i4FwlAclDirection;

        while (TRUE)
        {
            MEMSET (au1IfName, '\0', sizeof (au1IfName));

            nmhGetFwlAclSequenceNumber (i4NextFwlAclIfIndex, &NextFwlAclName,
                                        i4NextFwlAclDirection, &i4SeqNum);

            nmhGetFwlAclAction (i4NextFwlAclIfIndex, &NextFwlAclName,
                                i4NextFwlAclDirection, &i4Action);

            nmhGetFwlAclLogTrigger (i4NextFwlAclIfIndex, &NextFwlAclName,
                                    i4NextFwlAclDirection, &i4LogTrigger);

            nmhGetFwlAclFragAction (i4NextFwlAclIfIndex, &NextFwlAclName,
                                    i4NextFwlAclDirection, &i4FragAction);

            if (i4NextFwlAclIfIndex == FWL_GLOBAL_IDX)
            {
                STRNCPY (au1IfName, "Global", CLI_STRLEN ("Global"));
            }
            else
            {
                CfaCliGetIfName ((UINT4) i4NextFwlAclIfIndex,
                                 (INT1 *) au1IfName);
            }
            CliPrintf (CliHandle, "%-25s", au1NextFwlAclName);
            CliPrintf (CliHandle, "%-11s", au1IfName);

            nmhGetFwlRuleFilterSet (&NextFwlAclName, &Temp);
            au1Temp[Temp.i4_Length] = '\0';

            CliPrintf (CliHandle, "%-26s", au1Temp);

            if (i4NextFwlAclDirection == FWL_DIRECTION_IN)
            {
                CliPrintf (CliHandle, "%-7s", "in");
            }
            else
            {
                CliPrintf (CliHandle, "%-7s", "out");
            }

            if (i4Action == FWL_PERMIT)
            {
                STRCPY (&au1Temp, "permit");
            }
            else
            {
                STRCPY (&au1Temp, "deny");
            }
            CliPrintf (CliHandle, "%-8s", au1Temp);
            CliPrintf (CliHandle, "%-6d", i4SeqNum);
            if (i4LogTrigger == FWL_LOG_NONE)
            {
                STRCPY (&au1Temp, "NO");
            }
            else if (i4LogTrigger == FWL_LOG_BRF)
            {
                STRCPY (&au1Temp, "BRF");
            }
            else if (i4LogTrigger == FWL_LOG_DTL)
            {
                STRCPY (&au1Temp, "DTL");
            }
            CliPrintf (CliHandle, "%-5s", au1Temp);
            if (i4FragAction == FWL_PERMIT)
            {
                STRCPY (&au1Temp, "permit");
            }
            else
            {
                STRCPY (&au1Temp, "deny");
            }
            CliPrintf (CliHandle, "%-8s", au1Temp);
            CliPrintf (CliHandle, "\r\n");

            /* get the next elem */
            MEMCPY (FwlAclName.pu1_OctetList, NextFwlAclName.pu1_OctetList,
                    NextFwlAclName.i4_Length);
            FwlAclName.i4_Length = NextFwlAclName.i4_Length;

            i4FwlAclIfIndex = i4NextFwlAclIfIndex;
            i4FwlAclDirection = i4NextFwlAclDirection;
            i4rc =
                nmhGetNextIndexFwlDefnAclTable (i4FwlAclIfIndex,
                                                &i4NextFwlAclIfIndex,
                                                &FwlAclName, &NextFwlAclName,
                                                i4FwlAclDirection,
                                                &i4NextFwlAclDirection);
            if (i4rc == SNMP_FAILURE)
            {
                break;
            }
        }
    }

    return (CLI_SUCCESS);
}

INT4
FwlCliShowFwlFilters (tCliHandle CliHandle)
{
    FwlCliFormatShowAddressFilters (CliHandle);
    return (CLI_SUCCESS);
}

INT4
FwlCliShowFwlIPv6Filters (tCliHandle CliHandle)
{
    FwlCliFormatShowAddressIPv6Filters (CliHandle);
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatShowAddressFilters (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE FwlFilterName;
    tSNMP_OCTET_STRING_TYPE NextFwlFilterName;
    tSNMP_OCTET_STRING_TYPE Temp;
    UINT1               au1FwlFilterName[FWL_MAX_FILTER_NAME_LEN] =
        { FWL_ZERO };
    UINT1               au1NextFwlFilterName[FWL_MAX_FILTER_NAME_LEN] =
        { FWL_ZERO };
    UINT1               au1Temp[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    INT4                i4Proto = FWL_ZERO;
    INT4                i4AddrType = FWL_ZERO;
    INT4                i4rc = FWL_INVALID;
    INT4                i4FilterAccounting = FWL_ZERO;
    UINT4               u4FilterHitCount = FWL_ZERO;

    CliPrintf (CliHandle, "\r\n%50s\r\n", "Firewall Filters");
    CliPrintf (CliHandle, "%50s\r\n", "----------------");
    CliPrintf (CliHandle,
               "Filter                   Proto  Source              "
               "Destination        Src   Dest  Packet     Hit\r\n");
    CliPrintf (CliHandle,
               "                                Address             "
               "Address            port  port  Accounting Count\r\n");
    CliPrintf (CliHandle,
               "------                   -----  -------             "
               "-------            ----  ----  ---------- -----\r\n");

    FwlFilterName.pu1_OctetList = au1FwlFilterName;
    NextFwlFilterName.pu1_OctetList = au1NextFwlFilterName;
    Temp.pu1_OctetList = au1Temp;

    i4rc = nmhGetFirstIndexFwlDefnFilterTable (&FwlFilterName);

    if (i4rc == SNMP_SUCCESS)
    {
        MEMCPY (NextFwlFilterName.pu1_OctetList, FwlFilterName.pu1_OctetList,
                FwlFilterName.i4_Length);
        NextFwlFilterName.i4_Length = FwlFilterName.i4_Length;

        while (TRUE)
        {
            nmhGetFwlFilterAddrType (&NextFwlFilterName, &i4AddrType);
            if (i4AddrType == FWL_IP_VERSION_4)
            {
                au1NextFwlFilterName[NextFwlFilterName.i4_Length] =
                    FWL_END_OF_STRING;

                CliPrintf (CliHandle, "%-25s", au1NextFwlFilterName);

                nmhGetFwlFilterProtocol (&NextFwlFilterName, &i4Proto);
                FwlCliFormatFilterProtocol (CliHandle, (UINT1) i4Proto);

                nmhGetFwlFilterSrcAddress (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-20s", Temp.pu1_OctetList);

                nmhGetFwlFilterDestAddress (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-19s", Temp.pu1_OctetList);

                nmhGetFwlFilterSrcPort (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-6s", Temp.pu1_OctetList);

                nmhGetFwlFilterDestPort (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-6s", Temp.pu1_OctetList);

                nmhGetFwlFilterAccounting (&NextFwlFilterName,
                                           &i4FilterAccounting);
                CliPrintf (CliHandle, "%-11s",
                           ((i4FilterAccounting ==
                             FWL_ENABLE) ? "ENABLED" : "DISABLED"));

                nmhGetFwlFilterHitsCount (&NextFwlFilterName,
                                          &u4FilterHitCount);
                CliPrintf (CliHandle, "%d\r\n", u4FilterHitCount);
            }

            /* get the next elem */
            MEMCPY (FwlFilterName.pu1_OctetList,
                    NextFwlFilterName.pu1_OctetList,
                    NextFwlFilterName.i4_Length);
            FwlFilterName.i4_Length = NextFwlFilterName.i4_Length;

            i4rc =
                nmhGetNextIndexFwlDefnFilterTable (&FwlFilterName,
                                                   &NextFwlFilterName);

            if (i4rc == SNMP_FAILURE)
            {
                break;
            }
        }
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatShowAddressIPv6Filters (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE FwlFilterName;
    tSNMP_OCTET_STRING_TYPE NextFwlFilterName;
    tSNMP_OCTET_STRING_TYPE Temp;
    UINT1               au1FwlFilterName[FWL_MAX_FILTER_NAME_LEN] =
        { FWL_ZERO };
    UINT1               au1NextFwlFilterName[FWL_MAX_FILTER_NAME_LEN] =
        { FWL_ZERO };
    UINT1               au1Temp[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    INT4                i4Proto = FWL_ZERO;
    INT4                i4AddrType = FWL_ZERO;
    INT4                i4rc = FWL_INVALID;
    INT4                i4FilterAccounting = FWL_ZERO;
    INT4                i4Dscp = FWL_ZERO;
    UINT4               u4FilterHitCount = FWL_ZERO;
    UINT4               u4FlowId = FWL_ZERO;

    CliPrintf (CliHandle, "\r\n%50s\r\n", "Firewall Filters");
    CliPrintf (CliHandle, "%50s\r\n", "----------------");
    CliPrintf (CliHandle,
               "Filter                   Proto  Source              "
               "Destination        Src   Dest  Packet     Hit  \r\n");
    CliPrintf (CliHandle,
               "                                Address             "
               "Address            port  port  Accounting Count    Dscp "
               "   FlowLabel\r\n");

    CliPrintf (CliHandle,
               "------                   -----  -------             "
               "-------            ----  ----  ---------- -----\r\n");

    FwlFilterName.pu1_OctetList = au1FwlFilterName;
    NextFwlFilterName.pu1_OctetList = au1NextFwlFilterName;
    Temp.pu1_OctetList = au1Temp;

    i4rc = nmhGetFirstIndexFwlDefnFilterTable (&FwlFilterName);

    if (i4rc == SNMP_SUCCESS)
    {
        MEMCPY (NextFwlFilterName.pu1_OctetList, FwlFilterName.pu1_OctetList,
                FwlFilterName.i4_Length);
        NextFwlFilterName.i4_Length = FwlFilterName.i4_Length;

        while (TRUE)
        {
            nmhGetFwlFilterAddrType (&NextFwlFilterName, &i4AddrType);
            if (i4AddrType == FWL_IP_VERSION_6)
            {
                au1NextFwlFilterName[NextFwlFilterName.i4_Length] =
                    FWL_END_OF_STRING;

                CliPrintf (CliHandle, "%-25s", au1NextFwlFilterName);

                nmhGetFwlFilterProtocol (&NextFwlFilterName, &i4Proto);
                FwlCliFormatFilterProtocol (CliHandle, (UINT1) i4Proto);

                nmhGetFwlFilterSrcAddress (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-20s", Temp.pu1_OctetList);

                nmhGetFwlFilterDestAddress (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-19s", Temp.pu1_OctetList);

                nmhGetFwlFilterSrcPort (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-6s", Temp.pu1_OctetList);

                nmhGetFwlFilterDestPort (&NextFwlFilterName, &Temp);
                CliPrintf (CliHandle, "%-6s", Temp.pu1_OctetList);

                nmhGetFwlFilterAccounting (&NextFwlFilterName,
                                           &i4FilterAccounting);
                CliPrintf (CliHandle, "%-11s",
                           ((i4FilterAccounting == FWL_ENABLE) ?
                            "ENABLED" : "DISABLED"));

                nmhGetFwlFilterHitsCount
                    (&NextFwlFilterName, &u4FilterHitCount);
                CliPrintf (CliHandle, "%-11d", u4FilterHitCount);

                nmhGetFwlFilterDscp (&NextFwlFilterName, &i4Dscp);
                CliPrintf (CliHandle, "%-11d", i4Dscp);

                nmhGetFwlFilterFlowId (&NextFwlFilterName, &u4FlowId);
                CliPrintf (CliHandle, "%-11d\r\n", u4FlowId);
            }

            /* get the next elem */
            MEMCPY (FwlFilterName.pu1_OctetList,
                    NextFwlFilterName.pu1_OctetList,
                    NextFwlFilterName.i4_Length);
            FwlFilterName.i4_Length = NextFwlFilterName.i4_Length;

            i4rc =
                nmhGetNextIndexFwlDefnFilterTable (&FwlFilterName,
                                                   &NextFwlFilterName);

            if (i4rc == SNMP_FAILURE)
            {
                break;
            }
        }
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliSetUntrustedPort (tCliHandle CliHandle, UINT4 u4Interface,
                        INT4 i4TrapThreshold)
{
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Error = FWL_ZERO;
    UINT4               u4IfType = FWL_EXTERNAL_IF;

    if ((i4RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        if (nmhTestv2FwlIfRowStatus
            (&u4Error, (INT4) u4Interface, FWL_CREATE_AND_GO) == SNMP_FAILURE)
        {
            if (u4Error == SNMP_ERR_NO_CREATION)
            {
                CliPrintf (CliHandle, "%%ERROR: Configured Untrust Ports "
                           "Exceeded Max Conf value!\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n%%Specify Valid Interface !\r\n");
            }
            return (CLI_FAILURE);
        }
        nmhSetFwlIfRowStatus (u4Interface, FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
    }
    else
    {
        nmhSetFwlIfRowStatus (u4Interface, FWL_NOT_IN_SERVICE);
    }

    if (nmhTestv2FwlIfIfType (&u4Error, (INT4) u4Interface, (INT4) u4IfType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Firewall interface type can be only "
                   "external or internal\r\n");

        if (i4RetVal != SNMP_SUCCESS)
        {
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        return (CLI_FAILURE);
    }
    if ((FWL_DEF_TRAP_THRESHOLD != i4TrapThreshold) && (i4TrapThreshold !=
                                                        FWL_ZERO))
    {
        if (nmhTestv2FwlIfTrapThreshold (&u4Error, (INT4) u4Interface,
                                         i4TrapThreshold) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Firewall Trap Threshold can be only "
                       "between %ld -%ld\r\n", FWL_MIN_TRAP_THRESHOLD,
                       FWL_MAX_TRAP_THRESHOLD);
            nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
            return (CLI_FAILURE);
        }
        nmhSetFwlIfTrapThreshold ((INT4) u4Interface, i4TrapThreshold);
    }

    if (nmhSetFwlIfIfType (u4Interface, u4IfType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%This I/f is not an WAN Port."
                   "Error while creating Untrusted Port \r\n");
        nmhSetFwlIfRowStatus (u4Interface, FWL_DESTROY);
        return (CLI_FAILURE);
    }
    nmhSetFwlIfRowStatus (u4Interface, FWL_ACTIVE);
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatIcmpType (tCliHandle CliHandle, UINT1 u1IcmpType)
{

    switch (u1IcmpType)
    {
        case FWL_ECHO_REPLY:
            CliPrintf (CliHandle, "echo-reply\r\n");
            break;

        case FWL_DEST_UNREACHABLE:
            CliPrintf (CliHandle, "destination-unreachable\r\n");
            break;

        case FWL_SOURCE_QUENCH:
            CliPrintf (CliHandle, "source-quench\r\n");
            break;

        case FWL_REDIRECT:
            CliPrintf (CliHandle, "redirect-message\r\n");
            break;

        case FWL_ECHO_REQUEST:
            CliPrintf (CliHandle, "echo-request\r\n");
            break;

        case FWL_TIME_EXCEEDED:
            CliPrintf (CliHandle, "time-exceeded\r\n");
            break;

        case FWL_PARAMETER_PROBLEM:
            CliPrintf (CliHandle, "parameter-problem\r\n");
            break;

        case FWL_TIMESTAMP_REQUEST:
            CliPrintf (CliHandle, "timestamp-request\r\n");
            break;

        case FWL_TIMESTAMP_REPLY:
            CliPrintf (CliHandle, "timestamp-reply\r\n");
            break;

        case FWL_INFORMATION_REQUEST:
            CliPrintf (CliHandle, "information-request\r\n");
            break;

        case FWL_INFORMATION_REPLY:
            CliPrintf (CliHandle, "information-reply\r\n");
            break;

        case FWL_ADDR_MASK_REQUEST:
            CliPrintf (CliHandle, "address-mask-request\r\n");
            break;

        case FWL_ADDR_MASK_REPLY:
            CliPrintf (CliHandle, "address-mask-reply\r\n");
            break;

        default:
            CliPrintf (CliHandle, "no-icmp-type\r\n");
            break;
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatIcmpCode (tCliHandle CliHandle, UINT1 u1IcmpCode)
{

    switch (u1IcmpCode)
    {
        case FWL_NETWORK_UNREACHABLE:
            CliPrintf (CliHandle, "network-unreachable\r\n");
            break;

        case FWL_HOST_UNREACHABLE:
            CliPrintf (CliHandle, "host-unreachable\r\n");
            break;

        case FWL_PROTOCOL_UNREACHABLE:
            CliPrintf (CliHandle, "protocol-unreachable\r\n");
            break;

        case FWL_PORT_UNREACHABLE:
            CliPrintf (CliHandle, "port-unreachable\r\n");
            break;

        case FWL_FRAGMENT_NEED:
            CliPrintf (CliHandle, "fragment-needed\r\n");
            break;

        case FWL_SOURCE_ROUTE_FAIL:
            CliPrintf (CliHandle, "source-route-fail\r\n");
            break;

        case FWL_DEST_NETWORK_UNKNOWN:
            CliPrintf (CliHandle, "destination-network-unknown\r\n");
            break;

        case FWL_DEST_HOST_UNKNOWN:
            CliPrintf (CliHandle, "destination-host-unknown\r\n");
            break;

        case FWL_SRC_HOST_ISOLATED:
            CliPrintf (CliHandle, "source-host-isolated\r\n");
            break;

        case FWL_DEST_NETWORK_ADMIN_PROHIBITED:
            CliPrintf (CliHandle, "destination-network-admin-prohibited\r\n");
            break;

        case FWL_DEST_HOST_ADMIN_PROHIBITED:
            CliPrintf (CliHandle, "destination-host-admin-prohibited\r\n");
            break;

        case FWL_NETWORK_UNREACHABLE_TOS:
            CliPrintf (CliHandle, "network-unreachable-tos\r\n");
            break;

        case FWL_HOST_UNREACHABLE_TOS:
            CliPrintf (CliHandle, "host-unreachable-tos\r\n");
            break;

        default:
            CliPrintf (CliHandle, "no-icmp-code\r\n");
            break;
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatFilterProtocol (tCliHandle CliHandle, UINT1 u1Protocol)
{
    switch (u1Protocol)
    {
        case FWL_ICMP:
        {
            CliPrintf (CliHandle, "%-7s", "icmp");
        }
            break;

        case FWL_ICMPV6:
        {
            CliPrintf (CliHandle, "%-7s", "icmpv6");
        }
            break;

        case FWL_IGMP:
        {
            CliPrintf (CliHandle, "%-7s", "igmp");
        }
            break;

        case FWL_GGP:
        {
            CliPrintf (CliHandle, "%-7s", "ggp");
        }
            break;

        case FWL_IP:
        {
            CliPrintf (CliHandle, "%-7s", "ip");
        }
            break;

        case FWL_TCP:
        {
            CliPrintf (CliHandle, "%-7s", "tcp");
        }
            break;

        case FWL_EGP:
        {
            CliPrintf (CliHandle, "%-7s", "egp");
        }
            break;

        case FWL_IGP:
        {
            CliPrintf (CliHandle, "%-7s", "igp");
        }
            break;

        case FWL_NVP:
        {
            CliPrintf (CliHandle, "%-7s", "nvp");
        }
            break;

        case FWL_UDP:
        {
            CliPrintf (CliHandle, "%-7s", "udp");
        }
            break;

        case FWL_IRTP:
        {
            CliPrintf (CliHandle, "%-7s", "irtp");
        }
            break;

        case FWL_IDPR:
        {
            CliPrintf (CliHandle, "%-7s", "idpr");
        }
            break;

        case FWL_RSVP:
        {
            CliPrintf (CliHandle, "%-7s", "rsvp");
        }
            break;

        case FWL_MHRP:
        {
            CliPrintf (CliHandle, "%-7s", "mhrp");
        }
            break;

        case FWL_IGRP:
        {
            CliPrintf (CliHandle, "%-7s", "igrp");
        }
            break;

        case FWL_OSPFIGP:
        {
            CliPrintf (CliHandle, "%-7s", "ospf");
        }
            break;

        default:
            if ((u1Protocol >= FWL_ONE) && (u1Protocol < FWL_PROTO_ANY))
            {
                CliPrintf (CliHandle, "%-7d", u1Protocol);
            }
            else
            {
                CliPrintf (CliHandle, "%-7s", "any");
            }
    }
    return (CLI_SUCCESS);
}

INT4
FwlCliSetDmzHost (tCliHandle CliHandle, UINT4 u4FwlDmzIpIndex)
{
    UINT4               u4DmzExists = FWL_ZERO;
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = nmhGetFwlDmzRowStatus (u4FwlDmzIpIndex, &i4RowStatus);

    if (i4RetVal != SNMP_SUCCESS)
    {

        if (nmhTestv2FwlDmzRowStatus (&u4DmzExists, u4FwlDmzIpIndex,
                                      FWL_CREATE_AND_GO) != SNMP_FAILURE)
        {
            if (nmhSetFwlDmzRowStatus (u4FwlDmzIpIndex, FWL_CREATE_AND_GO)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            nmhSetFwlDmzRowStatus (u4FwlDmzIpIndex, FWL_ACTIVE);
        }
        else
        {
            if (u4DmzExists == SNMP_ERR_NO_CREATION)
            {
                CliPrintf (CliHandle, "%%ERROR: Maximum number of "
                           "DMZ hosts reached !!! \r\n");
                return CLI_FAILURE;
            }
            if (u4DmzExists == SNMP_ERR_WRONG_VALUE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "%% ERROR : Cannot Add the DMZ Entry. "
                       "Exceeds the Maximum Entry Limit  !\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "%% ERROR : DMZ host already Exits !\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
FwlCliSetDmzIPv6Host (tCliHandle CliHandle, tIp6Addr DmzIp6Addr)
{
    UINT4               u4DmzExists = FWL_ZERO;
    UINT4               u4Error = FWL_ZERO;
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE DmzIp6Host;
    DmzIp6Host.pu1_OctetList =
        (UINT1 *) MemAllocMemBlk (FWL_SNMP_OCTETSTRING_PID);

    if (NULL == DmzIp6Host.pu1_OctetList)
    {
        CliPrintf (CliHandle,
                   "%% ERROR: The IPv6 Dmz memory does not exits !\r\n");
        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
        return CLI_FAILURE;
    }

    MEMCPY (DmzIp6Host.pu1_OctetList, &DmzIp6Addr, IP6_ADDR_SIZE);
    DmzIp6Host.i4_Length = IP6_ADDR_SIZE;

    i4RetVal = nmhGetFwlDmzIpv6RowStatus (&DmzIp6Host, &i4RowStatus);

    if (i4RetVal != SNMP_SUCCESS)
    {

        if (nmhTestv2FwlDmzIpv6RowStatus (&u4DmzExists, &DmzIp6Host,
                                          FWL_CREATE_AND_GO) != SNMP_FAILURE)
        {
            if (nmhSetFwlDmzIpv6RowStatus (&DmzIp6Host, FWL_CREATE_AND_GO)
                != SNMP_SUCCESS)
            {
                MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID,
                                    DmzIp6Host.pu1_OctetList);
                return CLI_FAILURE;
            }
            nmhSetFwlDmzIpv6RowStatus (&DmzIp6Host, FWL_ACTIVE);
        }
        else
        {
            if (u4DmzExists == SNMP_ERR_NO_CREATION)
            {
                CliPrintf (CliHandle, "%%ERROR: Maximum number of "
                           "DMZIPv6 hosts reached !!! \r\n");
                MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID,
                                    DmzIp6Host.pu1_OctetList);
                return CLI_FAILURE;
            }
            if (u4DmzExists == SNMP_ERR_WRONG_VALUE)
            {
                MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID,
                                    DmzIp6Host.pu1_OctetList);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "%% ERROR : Cannot Add the DMZIpv6 Entry. "
                       "Exceeds the Maximum Entry Limit  !\r\n");
            MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID,
                                DmzIp6Host.pu1_OctetList);
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "%% ERROR : DMZ IPv6 host already Exits !\r\n");
        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
        return CLI_FAILURE;
    }

    if (nmhTestv2FwlDmzAddressType (&u4Error, &DmzIp6Host, FWL_IP_VERSION_6) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%Address Type is invalid \r\n");
        nmhSetFwlDmzIpv6RowStatus (&DmzIp6Host, FWL_DESTROY);
        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
        return (CLI_FAILURE);
    }
    nmhSetFwlDmzAddressType (&DmzIp6Host, FWL_IP_VERSION_6);

    MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliShowDmzHost                                  */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a firewall Dmz host          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliShowDmzHost (tCliHandle CliHandle)
{

    UINT4               u4Count = FWL_ZERO;
    UINT4               u4FwlDmzIpIndex = FWL_ZERO;
    CHR1               *pu1String = NULL;

    CliPrintf (CliHandle, "\r\nFirewall DMZ Host\r\n\r\n");
    CliPrintf (CliHandle, " Count               Dmz Host      \r\n");
    CliPrintf (CliHandle, "---------          --------------- \r\n");

    if ((nmhGetFirstIndexFwlDefnDmzTable (&u4FwlDmzIpIndex)) == SNMP_FAILURE)
    {

        return (CLI_FAILURE);
    }
    do
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4FwlDmzIpIndex);
        CliPrintf (CliHandle, "%3d                  %-5s\r\n", ++u4Count,
                   pu1String);
    }
    while (nmhGetNextIndexFwlDefnDmzTable (u4FwlDmzIpIndex, &u4FwlDmzIpIndex) ==
           SNMP_SUCCESS);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliShowIPv6DmzHost                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a firewall Dmz host          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliShowIPv6DmzHost (tCliHandle CliHandle)
{

    UINT4               u4Count = FWL_ZERO;
    tSNMP_OCTET_STRING_TYPE DmzIpv6Index;
    tIp6Addr            DmzIp6Host;
    MEMSET (&DmzIp6Host, FWL_ZERO, sizeof (tIp6Addr));

    DmzIpv6Index.pu1_OctetList =
        (UINT1 *) MemAllocMemBlk (FWL_SNMP_OCTETSTRING_PID);

    if (NULL == DmzIpv6Index.pu1_OctetList)
    {
        CliPrintf (CliHandle,
                   "%% ERROR: The IPv6 Dmz memory does not exits !\r\n");
        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID,
                            DmzIpv6Index.pu1_OctetList);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nFirewall IPv6 DMZ Host\r\n\r\n");
    CliPrintf (CliHandle, " Count               Dmz IPv6 Host      \r\n");
    CliPrintf (CliHandle, "---------          --------------- \r\n");

    if ((nmhGetFirstIndexFwlDefnIPv6DmzTable (&DmzIpv6Index)) == SNMP_FAILURE)
    {

        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID,
                            DmzIpv6Index.pu1_OctetList);
        return (CLI_FAILURE);
    }
    do
    {
        MEMCPY (&DmzIp6Host, DmzIpv6Index.pu1_OctetList, IP6_ADDR_SIZE);

        CliPrintf (CliHandle, "%3d                  %-5s\r\n", ++u4Count,
                   Ip6PrintNtop (&DmzIp6Host));
    }
    while (nmhGetNextIndexFwlDefnIPv6DmzTable
           (&DmzIpv6Index, &DmzIpv6Index) == SNMP_SUCCESS);
    MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIpv6Index.pu1_OctetList);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliDeleteDmz                                    */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a firewall Dmz host          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        pu1FilterName - Dmz host Ip address                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliDeleteDmzHost (tCliHandle CliHandle, UINT4 u4FwlDmzIpIndex)
{
    UINT4               u4FwlDmzExists = FWL_ZERO;

    if (nmhTestv2FwlDmzRowStatus (&u4FwlDmzExists, u4FwlDmzIpIndex, FWL_DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% ERROR: The Dmz does not exits !\r\n");
        return CLI_FAILURE;
    }

    nmhSetFwlDmzRowStatus (u4FwlDmzIpIndex, FWL_DESTROY);
    return CLI_SUCCESS;

}                                /* End of Function. */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliDeleteDmzIPv6Host                            */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a firewall Dmz host          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        pu1FilterName - Dmz host Ip address                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliDeleteDmzIPv6Host (tCliHandle CliHandle, tIp6Addr DmzIp6Addr)
{
    UINT4               u4FwlDmzExists = FWL_ZERO;
    tSNMP_OCTET_STRING_TYPE DmzIp6Host;

    DmzIp6Host.pu1_OctetList =
        (UINT1 *) MemAllocMemBlk (FWL_SNMP_OCTETSTRING_PID);

    if (NULL == DmzIp6Host.pu1_OctetList)
    {
        CliPrintf (CliHandle,
                   "%% ERROR: The IPv6 Dmz memory does not exits !\r\n");
        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
        return CLI_FAILURE;
    }

    MEMCPY (DmzIp6Host.pu1_OctetList, &DmzIp6Addr, IP6_ADDR_SIZE);
    DmzIp6Host.i4_Length = IP6_ADDR_SIZE;

    if (nmhTestv2FwlDmzIpv6RowStatus (&u4FwlDmzExists,
                                      &DmzIp6Host, FWL_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% ERROR: The IPv6 Dmz does not exits !\r\n");
        MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
        return CLI_FAILURE;
    }

    nmhSetFwlDmzIpv6RowStatus (&DmzIp6Host, FWL_DESTROY);
    MemReleaseMemBlock (FWL_SNMP_OCTETSTRING_PID, DmzIp6Host.pu1_OctetList);
    return CLI_SUCCESS;

}                                /* End of Function. */

INT4
FwlCliShowFwlGlobalConfig (tCliHandle CliHandle)
{
    tCliFwlShowConfig   CliFwlShowConfig;
    UINT4               u4Interface = FWL_ZERO;
    UINT4               u4PrevInterface = FWL_ZERO;
    UINT1               au1IfName[FWL_INTERFACE_NAME_LEN] = { FWL_ZERO };
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1              *au1List = NULL;
    UINT4               u4OffSet = FWL_ZERO;
    INT4                i4RetValFwlIfIfType = FWL_INVALID;
    UINT1               au1ErrorMsg[FWL_ERROR_MSG_LENGTH] = { FWL_ZERO };
    UINT1               au1Separator[FWL_INDEX_3] = { FWL_ZERO };
    au1List = (UINT1 *) MemAllocMemBlk (FWL_WEB_LOG_PID);
    if (NULL == au1List)
    {
        return (CLI_FAILURE);
    }
    STRCPY (au1ErrorMsg, "Not configured");
    STRCPY (au1Separator, ", ");
    nmhGetFwlGlobalMasterControlSwitch (&CliFwlShowConfig.i4Status);

    nmhGetFwlIfICMPType ((INT4) FWL_GLOBAL_IDX, &CliFwlShowConfig.i4IcmpType);

    nmhGetFwlGlobalICMPControlSwitch (&CliFwlShowConfig.i4IcmpError);

    nmhGetFwlGlobalICMPv6ControlSwitch (&CliFwlShowConfig.i4Icmpv6Error);

    nmhGetFwlGlobalIpSpoofFiltering (&CliFwlShowConfig.i4IpSpoof);

    nmhGetFwlGlobalIpv6SpoofFiltering (&CliFwlShowConfig.i4Ipv6Spoof);

    nmhGetFwlGlobalNetBiosFiltering (&CliFwlShowConfig.i4NetBios);

    nmhGetFwlGlobalSrcRouteFiltering (&CliFwlShowConfig.i4SourceRoute);

    nmhGetFwlGlobalTcpIntercept (&CliFwlShowConfig.i4TcpSyn);

    nmhGetFwlGlobalTrace (&CliFwlShowConfig.i4Trace);

    nmhGetFwlGlobalMaxFilters (&CliFwlShowConfig.i4MaxFilters);

    nmhGetFwlGlobalMaxRules (&CliFwlShowConfig.i4MaxAcl);

    nmhGetFwlDefnInterceptTimeout ((UINT4 *) &CliFwlShowConfig.i4SynWait);

    nmhGetFwlDefnTcpInterceptThreshold (&CliFwlShowConfig.i4SynAllow);

    nmhGetFwlGlobalUrlFiltering ((INT4 *) &CliFwlShowConfig.
                                 u4FwlUrlFilterStatus);

    nmhGetFwlGlobalTrap (&CliFwlShowConfig.i4TrapStatus);

    nmhGetFwlTrapThreshold (&CliFwlShowConfig.i4TrapThreshold);

    nmhGetFwlIfIpOptions (FWL_GLOBAL_IDX,
                          &CliFwlShowConfig.i4RetValFwlIfIpOptions);

    nmhGetFwlIfICMPv6MsgType (FWL_GLOBAL_IDX,
                              &CliFwlShowConfig.i4RetValFwlIcmpv6MsgType);

    nmhGetFwlIfFragments (FWL_GLOBAL_IDX,
                          &CliFwlShowConfig.i4RetValFwlIfFragments);

    nmhGetFwlIfFragmentSize (FWL_GLOBAL_IDX,
                             (UINT4 *) (VOID *) &CliFwlShowConfig.
                             u2MaxFragmentSize);

    FwlCliFormatShowConfig (CliHandle, &CliFwlShowConfig);

    /* In Natera, firewall type is external on the wan interface 
     * and internal on the lan interface. This helps in attack
     * prevention and DoS attakcs. The configuration details
     * are displayed in the following function */
    i1RetVal = nmhGetFirstIndexFwlDefnIfTable ((INT4 *) &u4Interface);
    while (i1RetVal != SNMP_FAILURE)
    {
        /* If the Interface is not External, then skip that while listing 
           out external interfaces (Untrusted Ports) */
        if ((u4Interface == FWL_GLOBAL_IDX) ||
            ((nmhGetFwlIfIfType ((INT4) u4Interface, &i4RetValFwlIfIfType) ==
              SNMP_SUCCESS) && (i4RetValFwlIfIfType != FWL_EXTERNAL_IF)))
        {
            u4PrevInterface = u4Interface;
            i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                      (INT4 *) &u4Interface);
            continue;
        }
        if (CfaCliGetIfName (u4Interface, (INT1 *) au1IfName) != CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid interface\r\n");
            MemReleaseMemBlock (FWL_WEB_LOG_PID, au1List);
            return (CLI_FAILURE);
        }
        else
        {
            MEMCPY (au1List + u4OffSet, au1IfName, STRLEN (au1IfName));
            u4OffSet += STRLEN (au1IfName);
            MEMCPY (au1List + u4OffSet, au1Separator, STRLEN (au1Separator));
            u4OffSet += FWL_TWO;
        }
        u4PrevInterface = u4Interface;
        i1RetVal = nmhGetNextIndexFwlDefnIfTable ((INT4) u4PrevInterface,
                                                  (INT4 *) &u4Interface);
    }

    if (u4OffSet != FWL_ZERO)
        au1List[u4OffSet - FWL_TWO] = '\0';

    if (u4OffSet == FWL_ZERO)
    {
        MEMCPY (au1List, au1ErrorMsg, STRLEN (au1ErrorMsg));
    }
    CliPrintf (CliHandle, "Untrusted Port                     : %s\r\n",
               au1List);
    MemReleaseMemBlock (FWL_WEB_LOG_PID, au1List);
    return (CLI_SUCCESS);
}

INT4
FwlCliFormatShowConfig (tCliHandle CliHandle,
                        tCliFwlShowConfig * pCliFwlShowConfig)
{
    CliPrintf (CliHandle, "\r\nFirewall Configuration\r\n");
    CliPrintf (CliHandle, "----------------------\r\n\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4Status,
                        "Firewall Status                    : %s\r\n");
    FwlCliFormatStatus (CliHandle,
                        (INT4) pCliFwlShowConfig->u4FwlUrlFilterStatus,
                        "URL Filtering Status               : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4IcmpError,
                        "Generate ICMP Error Message        : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4Icmpv6Error,
                        "Generate ICMPv6 Error Message      : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4NetBios,
                        "Filter NetBIOS Packets             : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4IpSpoof,
                        "Examine IP Spoofing Attack         : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4Ipv6Spoof,
                        "Examine IPv6 Spoofing Attack       : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4TcpSyn,
                        "Examine TCP SYN packets option     : %s\r\n");
    FwlCliFormatStatus (CliHandle, pCliFwlShowConfig->i4TrapStatus,
                        "Trap Status                        : %s\r\n");

    CliPrintf (CliHandle,
               "Max Filters                        : %ld\r\n",
               pCliFwlShowConfig->i4MaxFilters);
    CliPrintf (CliHandle,
               "Max Access-Lists                   : %ld\r\n",
               pCliFwlShowConfig->i4MaxAcl);
    CliPrintf (CliHandle,
               "TCP SYN wait timeout               : %ld Second(s)\r\n",
               pCliFwlShowConfig->i4SynWait);
    CliPrintf (CliHandle,
               "TCP SYN Packets allowed            : %ld\r\n",
               pCliFwlShowConfig->i4SynAllow);

    CliPrintf (CliHandle,
               "Trap Threshold Configured          : %ld\r\n",
               pCliFwlShowConfig->i4TrapThreshold);

    if (pCliFwlShowConfig->i4IcmpType == (INT4) CLI_ATOI (CLI_FWL_ECHO_REQUEST))
    {
        CliPrintf (CliHandle,
                   "Icmp Inspect Option               : Enabled\r\n");
    }
    else if (pCliFwlShowConfig->i4IcmpType ==
             (INT4) CLI_ATOI (CLI_FWL_ICMP_CODE_NONE))
    {
        CliPrintf (CliHandle,
                   " Icmp Inspect Option               : Disabled\r\n");
    }
    CliPrintf (CliHandle, "IP Inspect Option                  : ");

    switch (pCliFwlShowConfig->i4RetValFwlIfIpOptions)
    {
        case FWL_SOURCE_ROUTE_OPTION:
            CliPrintf (CliHandle, "%s\r\n", "src-route");
            break;

        case FWL_TRACE_ROUTE_OPTION:
            CliPrintf (CliHandle, "%s\r\n", "trc-route");
            break;

        case FWL_RECORD_ROUTE_OPTION:
            CliPrintf (CliHandle, "%s\r\n", "record-route");
            break;

        case FWL_TIMESTAMP_OPTION:
            CliPrintf (CliHandle, "%s\r\n", "Timestamp");
            break;
        case FWL_ANY_OPTION:
            CliPrintf (CliHandle, "%s\r\n",
                       "Any (src-route, record-route , trc-route & timestamp)");
            break;

        default:
            CliPrintf (CliHandle, "%s\r\n", "None");
    }
    CliPrintf (CliHandle, "Icmpv6 Inspect Option              : ");
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_INSPECT_ALL) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_DESTINATION_UNREACHABLE) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for "
                   "Destination Unreachable");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_TIME_EXCEEDED) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for "
                   "Time exceeded");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_PARAMETER_PROBLEM) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for "
                   "Parameter Problem");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_ECHO_REQUEST) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect status Enabled for "
                   "Echo Request");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_ECHO_REPLY) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for " "Echo Reply");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_REDIRECT) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for Redirect");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_INFORMATION_REQUEST) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for "
                   "Information Request");
    }
    if ((pCliFwlShowConfig->i4RetValFwlIcmpv6MsgType &
         CLI_FWL_ICMPV6_INFORMATION_REPLY) != FWL_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  ICMPv6 Inspect Enabled for "
                   "Information Reply");
    }

    CliPrintf (CliHandle, "\r\nFilter Fragments Option            : ");
    if (pCliFwlShowConfig->i4RetValFwlIfFragments == FWL_ANY_FRAGMENT)
    {
        CliPrintf (CliHandle, "Any Fragment\r\n");
    }

    else if (pCliFwlShowConfig->i4RetValFwlIfFragments == FWL_LARGE_FRAGMENT)
    {
        CliPrintf (CliHandle, "Large (Size - %d Bytes)\r\n",
                   pCliFwlShowConfig->u2MaxFragmentSize);
    }

    else if (pCliFwlShowConfig->i4RetValFwlIfFragments == FWL_TINY_FRAGMENT)
    {
        CliPrintf (CliHandle, "Tiny Fragment\r\n");
    }
    else
        CliPrintf (CliHandle, "%s\r\n", "No Fragments");

    return (CLI_SUCCESS);
}

INT4
FwlSetNetbiosFiltering (UINT1 u1NetBiosFlag)
{
    nmhSetFwlGlobalNetBiosFiltering (u1NetBiosFlag);
    return (CLI_SUCCESS);
}

/***********************************************************************/
/*  Function Name : FwlCliClearLogs                                    */
/*  Description   : Function to clear the Firewall Logs                */
/*  Input(s)      : None                                               */
/*  Output(s)     : None                                               */
/*  Return        : CLI_SUCCESS / CLI_FAILURE                          */
/***********************************************************************/
INT4
FwlCliClearLogs (VOID)
{
    FILE               *pFp = NULL;
    UINT1               au1FwlLogFileName[FWL_MAX_FILE_NAME_LEN] = { FWL_ZERO };
    CHR1                au1CurDate[FWL_MAX_TIME_LEN + 2] = { FWL_ZERO };
    UINT4               u4StorageChkFlag = FWL_ZERO;
    UINT4               u4OverWriteFlag = FWL_ZERO;

    MEMSET (au1FwlLogFileName, FWL_ZERO, sizeof (au1FwlLogFileName));
    MEMSET (au1CurDate, FWL_ZERO, sizeof (au1CurDate));

    UtlGetCurDate (au1CurDate);

    SNPRINTF ((CHR1 *) au1FwlLogFileName, sizeof (au1FwlLogFileName),
              "%s/%s_%s", FWL_LOG_FILE_PATH, au1CurDate, FWL_FILE_NAME_EXT);

    IssCustCheckLogOption (&u4StorageChkFlag, &u4OverWriteFlag);
    if (LOG_FILE_STORAGE == u4StorageChkFlag)
    {
        if ((pFp = fopen ((CONST CHR1 *) au1FwlLogFileName, "w")) != NULL)
        {
            fclose (pFp);
        }
    }
    else
    {
        if (ISS_FAILURE == IssDeleteLocalDir (FWL_LOG_FILE_PATH))
        {
            return CLI_FAILURE;
        }
    }

    pFp = NULL;

    /* SNORT alert file is cleared */
    MEMSET (au1FwlLogFileName, FWL_ZERO, sizeof (au1FwlLogFileName));

    SNPRINTF ((CHR1 *) au1FwlLogFileName, sizeof (au1FwlLogFileName),
              "%s/%s_%s", IDS_LOG_FILE_PATH, au1CurDate, IDS_FILE_NAME_EXT);

    if (LOG_FILE_STORAGE == u4StorageChkFlag)
    {
        if ((pFp = fopen ((CONST CHR1 *) au1FwlLogFileName, "w")) != NULL)
        {
            fclose (pFp);
        }
    }
    else
    {
        if (ISS_FAILURE == IssDeleteLocalDir (IDS_LOG_FILE_PATH))
        {
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

INT4
FwlCliShowLogs (tCliHandle CliHandle)
{
    FILE               *pLogfp = NULL;
    CHR1                au1FwlLogFile[FWL_MAX_FILE_NAME_LEN] = { FWL_ZERO };
    CHR1                au1SendLine[FWL_MAX_FILE_READ_LEN] = { FWL_ZERO };
    CHR1                au1CurDate[FWL_MAX_TIME_LEN + 2] = { FWL_ZERO };

    UtlGetCurDate (au1CurDate);

    CliPrintf (CliHandle, "\r\nFirewall Logs\r\n\r\n");

    /* Firewall log file is read & displayed */
    MEMSET (au1FwlLogFile, '\0', sizeof (au1FwlLogFile));
    SNPRINTF (au1FwlLogFile, sizeof (au1FwlLogFile), "%s/%s_%s",
              FWL_LOG_FILE_PATH, au1CurDate, FWL_FILE_NAME_EXT);

    if ((pLogfp = fopen (au1FwlLogFile, "r")) != NULL)
    {
        while ((fgets (au1SendLine, FWL_MAX_FILE_READ_LEN, pLogfp)) != NULL)
        {
            CliPrintf (CliHandle, "%s", au1SendLine);
        }
        fclose (pLogfp);
    }

    pLogfp = NULL;

    MEMSET (au1FwlLogFile, '\0', sizeof (au1FwlLogFile));

    /* Snort alert file is read & displayed */
    SNPRINTF (au1FwlLogFile, sizeof (au1FwlLogFile), "%s/%s_%s",
              IDS_LOG_FILE_PATH, au1CurDate, IDS_FILE_NAME_EXT);

    if ((pLogfp = fopen (au1FwlLogFile, "r")) != NULL)
    {
        while ((fgets (au1SendLine, FWL_MAX_FILE_READ_LEN, pLogfp)) != NULL)
        {
            CliPrintf (CliHandle, au1SendLine);
        }
        fclose (pLogfp);
    }

    return (CLI_SUCCESS);
}

INT4
FwlShowStatefulTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LocalAddress;
    tSNMP_OCTET_STRING_TYPE RemoteAddress;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    CHR1               *pu1String = NULL;
    INT4                i4StateType = FWL_ZERO;
    INT4                i4LocalAddrType = FWL_ZERO;
    INT4                i4RemoteAddrType = FWL_ZERO;
    INT4                i4Protocol = FWL_ZERO;
    INT4                i4Direction = FWL_ZERO;
    INT4                i4LocalPort = FWL_ZERO;
    INT4                i4RemotePort = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;
    UINT1               au1Localv4Addr[FWL_IPV4_PREFIX_LEN +
                                       FWL_ONE] = { FWL_ZERO };
    UINT1               au1Remotev4Addr[FWL_IPV4_PREFIX_LEN +
                                        FWL_ONE] = { FWL_ZERO };
    UINT1               au1Localv6Addr[FWL_IPV6_PREFIX_LEN +
                                       FWL_ONE] = { FWL_ZERO };
    UINT1               au1Remotev6Addr[FWL_IPV6_PREFIX_LEN +
                                        FWL_ONE] = { FWL_ZERO };

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (au1Localv4Addr, FWL_ZERO, FWL_IPV4_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Remotev4Addr, FWL_ZERO, FWL_IPV4_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Localv6Addr, FWL_ZERO, FWL_IPV6_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Remotev6Addr, FWL_ZERO, FWL_IPV6_PREFIX_LEN + FWL_ONE);
    LocalAddress.pu1_OctetList = au1Localv4Addr;
    LocalAddress.i4_Length = FWL_IPV4_PREFIX_LEN;
    RemoteAddress.pu1_OctetList = au1Remotev4Addr;
    RemoteAddress.i4_Length = FWL_IPV4_PREFIX_LEN;

    CliPrintf (CliHandle, "\r\nStateful Table\r\n"
               "--------------------\r\n\r\n");
    CliPrintf (CliHandle, "No        Src             Destination \
    Proto      Src Port   Dest Port  Session\r\n" "          IP              IP          \
    ICMP Seq                         Dir  \r\n" "-------  ---------------  --------------- -------\
    ---------- ---------- -------\r\n");

    while (SNMP_SUCCESS ==
           nmhGetNextIndexFwlStateTable (FWL_STATEFUL, &i4StateType,
                                         IPVX_ADDR_FMLY_IPV4, &i4LocalAddrType,
                                         &LocalAddress, &LocalAddress,
                                         IPVX_ADDR_FMLY_IPV4, &i4RemoteAddrType,
                                         &RemoteAddress, &RemoteAddress,
                                         i4LocalPort, &i4LocalPort,
                                         i4RemotePort, &i4RemotePort,
                                         i4Protocol, &i4Protocol,
                                         i4Direction, &i4Direction))
    {
        u4Count++;

        if ((u4Count > FWL_STATE_HASH_LIMIT) || (i4StateType != FWL_STATEFUL))
        {
            break;
        }

        CliPrintf (CliHandle, "%-10d", u4Count);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             &LocalAddress, &FwlLocalIpAddr);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, (FwlLocalIpAddr.uIpAddr.Ip4Addr));
        CliPrintf (CliHandle, "%-16s", pu1String);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             &RemoteAddress, &FwlRemoteIpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                   (FwlRemoteIpAddr.uIpAddr.Ip4Addr));
        CliPrintf (CliHandle, "%-17s", pu1String);

        switch (i4Protocol)
        {
            case FWL_TCP:
                CliPrintf (CliHandle, "%-7s", "TCP");
                break;
            case FWL_UDP:
                CliPrintf (CliHandle, "%-7s", "UDP");
                break;
            case FWL_ICMP:
                CliPrintf (CliHandle, "%-7s", "ICMP");
                break;
            default:
                CliPrintf (CliHandle, "%-7u", i4Protocol);
                break;
        }

        CliPrintf (CliHandle, "%-11d", i4LocalPort);

        if (i4Protocol == FWL_ICMP)
        {
            CliPrintf (CliHandle, "%-11s", "-NA-");
        }
        else
        {
            CliPrintf (CliHandle, "%-11d", i4RemotePort);
        }

        CliPrintf (CliHandle, " %s\r\n",
                   (i4Direction == FWL_DIRECTION_OUT) ? "Out" : "In");
        CliPrintf (CliHandle, "\r\n");
    }                            /* End of while */

    LocalAddress.pu1_OctetList = au1Localv6Addr;
    LocalAddress.i4_Length = FWL_IPV6_PREFIX_LEN;
    RemoteAddress.pu1_OctetList = au1Remotev6Addr;
    RemoteAddress.i4_Length = FWL_IPV6_PREFIX_LEN;

    while (SNMP_SUCCESS ==
           nmhGetNextIndexFwlStateTable (FWL_STATEFUL, &i4StateType,
                                         IPVX_ADDR_FMLY_IPV6, &i4LocalAddrType,
                                         &LocalAddress, &LocalAddress,
                                         IPVX_ADDR_FMLY_IPV6, &i4RemoteAddrType,
                                         &RemoteAddress, &RemoteAddress,
                                         i4LocalPort, &i4LocalPort,
                                         i4RemotePort, &i4RemotePort,
                                         i4Protocol, &i4Protocol,
                                         i4Direction, &i4Direction))
    {
        u4Count++;

        if (u4Count > FWL_STATE_V6_HASH_LIMIT)
        {
            break;
        }
        CliPrintf (CliHandle, "%-10d", u4Count);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             &LocalAddress, &FwlLocalIpAddr);
        CliPrintf (CliHandle, "%-16s",
                   Ip6PrintNtop (&FwlLocalIpAddr.uIpAddr.Ip6Addr));

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             &RemoteAddress, &FwlRemoteIpAddr);
        CliPrintf (CliHandle, "%-16s",
                   Ip6PrintNtop (&FwlRemoteIpAddr.uIpAddr.Ip6Addr));

        switch (i4Protocol)
        {
            case FWL_TCP:
                CliPrintf (CliHandle, "%-7s", "TCP");
                break;
            case FWL_UDP:
                CliPrintf (CliHandle, "%-7s", "UDP");
                break;
            case FWL_ICMPV6:
                CliPrintf (CliHandle, "%-7s", "ICMPv6");
                break;
            default:
                CliPrintf (CliHandle, "%-7u", i4Protocol);
                break;
        }

        CliPrintf (CliHandle, "%-11d", i4LocalPort);

        if (i4Protocol == FWL_ICMP)
        {
            CliPrintf (CliHandle, "%-11s", "-NA-");
        }
        else
        {
            CliPrintf (CliHandle, "%-11d", i4RemotePort);
        }

        CliPrintf (CliHandle, " %s\r\n",
                   (i4Direction == FWL_DIRECTION_OUT) ? "Out" : "In");
        CliPrintf (CliHandle, "\r\n");
    }                            /* End of while */
    return (CLI_SUCCESS);
}                                /*End of Function */

INT4
FwlShowInitFlowTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LocalAddress;
    tSNMP_OCTET_STRING_TYPE RemoteAddress;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    CHR1               *pu1String = NULL;
    INT4                i4StateType = FWL_ZERO;
    INT4                i4LocalAddrType = FWL_ZERO;
    INT4                i4RemoteAddrType = FWL_ZERO;
    UINT4               u4TimeStamp = FWL_ZERO;
    INT4                i4Protocol = FWL_ZERO;
    INT4                i4Direction = FWL_ZERO;
    INT4                i4LocalPort = FWL_ZERO;
    INT4                i4RemotePort = FWL_ZERO;
    UINT1               au1Localv4Addr[FWL_IPV4_PREFIX_LEN +
                                       FWL_ONE] = { FWL_ZERO };
    UINT1               au1Remotev4Addr[FWL_IPV4_PREFIX_LEN +
                                        FWL_ONE] = { FWL_ZERO };
    UINT1               au1Localv6Addr[FWL_IPV6_PREFIX_LEN +
                                       FWL_ONE] = { FWL_ZERO };
    UINT1               au1Remotev6Addr[FWL_IPV6_PREFIX_LEN +
                                        FWL_ONE] = { FWL_ZERO };
    INT4                i4AppCallStatus = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (au1Localv4Addr, FWL_ZERO, FWL_IPV4_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Remotev4Addr, FWL_ZERO, FWL_IPV4_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Localv6Addr, FWL_ZERO, FWL_IPV6_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Remotev6Addr, FWL_ZERO, FWL_IPV6_PREFIX_LEN + FWL_ONE);
    LocalAddress.pu1_OctetList = au1Localv4Addr;
    LocalAddress.i4_Length = FWL_IPV4_PREFIX_LEN;
    RemoteAddress.pu1_OctetList = au1Remotev4Addr;
    RemoteAddress.i4_Length = FWL_IPV4_PREFIX_LEN;

    CliPrintf (CliHandle, "\r\nTcp Init Flow Table\r\n"
               "--------------------\r\n\r\n");
    CliPrintf (CliHandle, "No        Src             Destination     Src\
               Destination   Proto   Marked  \r\n");
    CliPrintf (CliHandle, "          IP                 IP           Port\
               Port                  Stale   \r\n");
    CliPrintf (CliHandle, "--------  --------------- --------------- ------\
               ---------  --------- ---------\r\n");

    while (SNMP_SUCCESS ==
           nmhGetNextIndexFwlStateTable (FWL_INITFLOW, &i4StateType,
                                         IPVX_ADDR_FMLY_IPV4, &i4LocalAddrType,
                                         &LocalAddress, &LocalAddress,
                                         IPVX_ADDR_FMLY_IPV4, &i4RemoteAddrType,
                                         &RemoteAddress, &RemoteAddress,
                                         i4LocalPort, &i4LocalPort,
                                         i4RemotePort, &i4RemotePort,
                                         i4Protocol, &i4Protocol,
                                         i4Direction, &i4Direction))
    {
        u4Count++;

        if (u4Count > FWL_MAX_TCP_INIT_FLOW_NODES)
        {
            break;
        }

        if (SNMP_FAILURE ==
            nmhGetFwlStateCallStatus (FWL_INITFLOW, IPVX_ADDR_FMLY_IPV4,
                                      &LocalAddress, IPVX_ADDR_FMLY_IPV4,
                                      &RemoteAddress, i4LocalPort, i4RemotePort,
                                      i4Protocol, i4Direction,
                                      &i4AppCallStatus))
        {
            return CLI_FAILURE;
        }

        if (SNMP_FAILURE ==
            nmhGetFwlStateEstablishedTime (FWL_INITFLOW, IPVX_ADDR_FMLY_IPV4,
                                           &LocalAddress, IPVX_ADDR_FMLY_IPV4,
                                           &RemoteAddress, i4LocalPort,
                                           i4RemotePort, i4Protocol,
                                           i4Direction, &u4TimeStamp))
        {
            return CLI_FAILURE;
        }
        /*Below code displays whether entry is marked for deletion or not. */
        if ((i4AppCallStatus == FWL_APP_HOLD) ||
            (i4AppCallStatus == FWL_APP_UNHOLD))
        {
            continue;
        }
        CliPrintf (CliHandle, "%-10d", u4Count);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             &LocalAddress, &FwlLocalIpAddr);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, (FwlLocalIpAddr.uIpAddr.Ip4Addr));
        CliPrintf (CliHandle, "%-16s", pu1String);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             &RemoteAddress, &FwlRemoteIpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                   (FwlRemoteIpAddr.uIpAddr.Ip4Addr));
        CliPrintf (CliHandle, "%-17s", pu1String);

        CliPrintf (CliHandle, "%-11d", i4LocalPort);

        CliPrintf (CliHandle, "%-11d", i4RemotePort);

        CliPrintf (CliHandle, "%-8u", i4Protocol);
        if (u4TimeStamp != FWL_ZERO)
        {
            CliPrintf (CliHandle, "No\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Yes\r\n");
        }

    }                            /*End-of get-next */

    LocalAddress.pu1_OctetList = au1Localv6Addr;
    LocalAddress.i4_Length = FWL_IPV6_PREFIX_LEN;
    RemoteAddress.pu1_OctetList = au1Remotev6Addr;
    RemoteAddress.i4_Length = FWL_IPV6_PREFIX_LEN;

    while (SNMP_SUCCESS ==
           nmhGetNextIndexFwlStateTable (FWL_INITFLOW, &i4StateType,
                                         IPVX_ADDR_FMLY_IPV6, &i4LocalAddrType,
                                         &LocalAddress, &LocalAddress,
                                         IPVX_ADDR_FMLY_IPV6, &i4RemoteAddrType,
                                         &RemoteAddress, &RemoteAddress,
                                         i4LocalPort, &i4LocalPort,
                                         i4RemotePort, &i4RemotePort,
                                         i4Protocol, &i4Protocol,
                                         i4Direction, &i4Direction))
    {
        u4Count++;

        if (u4Count > FWL_MAX_TCP_INIT_FLOW_NODES)
        {
            break;
        }

        CliPrintf (CliHandle, "%-10d", u4Count);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             &LocalAddress, &FwlLocalIpAddr);

        CliPrintf (CliHandle, "%-16s",
                   Ip6PrintNtop (&FwlLocalIpAddr.uIpAddr.Ip6Addr));

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV6,
                             &RemoteAddress, &FwlRemoteIpAddr);

        CliPrintf (CliHandle, "%-16s",
                   Ip6PrintNtop (&FwlRemoteIpAddr.uIpAddr.Ip6Addr));

        CliPrintf (CliHandle, "%-11d", i4LocalPort);

        CliPrintf (CliHandle, "%-11d", i4RemotePort);

        CliPrintf (CliHandle, "%-8u", i4Protocol);
        if (u4TimeStamp != FWL_ZERO)
        {
            CliPrintf (CliHandle, "No\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Yes\r\n");
        }

    }                            /*End-of get-next */

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}                                /*End of Function */

INT4
FwlCliCommit (VOID)
{
    if (SEC_KERN_USER == gi4SysOperMode)
    {
        return FwlCliCommitInKernel ();
    }
    else
    {
        FwlCommitAcl ();
        return CLI_SUCCESS;
    }
}
INT4
FwlCliShowTimeoutConfigParam (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\r\n"
               "Idle timer.................: %d secs  Default (%d)\r\n"
               "TCP half open timeout......: %d secs  Default (%d)\r\n"
               "TCP Estabished timeout.....: %d secs  Default (%d)\r\n"
               "TCP session closing timeout: %d secs  Default (%d)\r\n"
               "UDP new session lifetime...: %d secs  Default (%d)\r\n"
               "UDP stateful lifetime......: %d secs  Default (%d)\r\n"
               "ICMP session lifetime......: %d secs  Default (%d)\r\n"
               "\r\n",
               FWL_IDLE_TIMEOUT, FWL_DEFAULT_IDLE_TIMER_VALUE,
               FWL_TCP_INIT_FLOW_TIMEOUT, FWL_DEFAULT_TCP_INIT_FLOW_TIMEOUT,
               FWL_TCP_EST_FLOW_TIMEOUT, FWL_DEFAULT_TCP_EST_FLOW_TIMEOUT,
               FWL_TCP_FIN_FLOW_TIMEOUT, FWL_DEFAULT_TCP_FIN_FLOW_TIMEOUT,
               FWL_UDP_FLOW_LARGE_TIMEOUT, FWL_DEFAULT_UDP_FLOW_LARGE_TIMEOUT,
               FWL_UDP_FLOW_TINY_TIMEOUT,
               FWL_DEFAULT_UDP_FLOW_LARGE_TIMEOUT / FWL_TWO,
               FWL_ICMP_FLOW_TIMEOUT, FWL_DEFAULT_ICMP_FLOW_TIMEOUT);
    return (CLI_SUCCESS);
}

INT4
FwlCliSetUdpInspectCount (UINT2 u2UdpCount)
{
    gFwlAclInfo.u2UdpLimit = u2UdpCount;
    return (CLI_SUCCESS);
}

INT4
FwlCliSetIcmpInspectCount (UINT2 u2IcmpCount)
{
    gFwlAclInfo.u2IcmpLimit = u2IcmpCount;
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliShowHiddenPartialEntry                       */
/*                                                                           */
/*     DESCRIPTION      : This function gets the firewall pin holes created  */
/*                        for sip calls from kernel space                    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliShowHiddenPartialEntry (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LocalAddress;
    tSNMP_OCTET_STRING_TYPE RemoteAddress;
    tFwlIpAddr          FwlLocalIpAddr;
    tFwlIpAddr          FwlRemoteIpAddr;
    CHR1               *pu1String = NULL;
    INT4                i4StateType = FWL_ZERO;
    INT4                i4LocalAddrType = FWL_ZERO;
    INT4                i4RemoteAddrType = FWL_ZERO;
    INT4                i4Protocol = FWL_ZERO;
    INT4                i4Direction = FWL_ZERO;
    INT4                i4LocalPort = FWL_ZERO;
    INT4                i4RemotePort = FWL_ZERO;
    UINT1               au1Localv4Addr[FWL_IPV4_PREFIX_LEN +
                                       FWL_ONE] = { FWL_ZERO };
    UINT1               au1Remotev4Addr[FWL_IPV4_PREFIX_LEN +
                                        FWL_ONE] = { FWL_ZERO };
    INT4                i4AppCallStatus = FWL_ZERO;

    MEMSET (&FwlRemoteIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (&FwlLocalIpAddr, FWL_ZERO, sizeof (tFwlIpAddr));
    MEMSET (au1Localv4Addr, FWL_ZERO, FWL_IPV4_PREFIX_LEN + FWL_ONE);
    MEMSET (au1Remotev4Addr, FWL_ZERO, FWL_IPV4_PREFIX_LEN + FWL_ONE);
    LocalAddress.pu1_OctetList = au1Localv4Addr;
    LocalAddress.i4_Length = FWL_IPV4_PREFIX_LEN;
    RemoteAddress.pu1_OctetList = au1Remotev4Addr;
    RemoteAddress.i4_Length = FWL_IPV4_PREFIX_LEN;

    PrintFwlPinHolesHdr (CliHandle);

    while (SNMP_SUCCESS ==
           nmhGetNextIndexFwlStateTable (FWL_PARTIAL, &i4StateType,
                                         IPVX_ADDR_FMLY_IPV4, &i4LocalAddrType,
                                         &LocalAddress, &LocalAddress,
                                         IPVX_ADDR_FMLY_IPV4, &i4RemoteAddrType,
                                         &RemoteAddress, &RemoteAddress,
                                         i4LocalPort, &i4LocalPort,
                                         i4RemotePort, &i4RemotePort,
                                         i4Protocol, &i4Protocol,
                                         i4Direction, &i4Direction))
    {
        if (SNMP_FAILURE ==
            nmhGetFwlStateCallStatus (FWL_PARTIAL, IPVX_ADDR_FMLY_IPV4,
                                      &LocalAddress, IPVX_ADDR_FMLY_IPV4,
                                      &RemoteAddress, i4LocalPort, i4RemotePort,
                                      i4Protocol, i4Direction,
                                      &i4AppCallStatus))
        {
            return CLI_FAILURE;
        }

        if ((i4AppCallStatus != FWL_APP_HOLD) &&
            (i4AppCallStatus != FWL_APP_UNHOLD))
        {
            continue;
        }

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             &LocalAddress, &FwlLocalIpAddr);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, (FwlLocalIpAddr.uIpAddr.Ip4Addr));
        CliPrintf (CliHandle, "%-17s", pu1String);

        CliPrintf (CliHandle, "%-11d", i4LocalPort);

        FwlUtilGetIpAddress (IPVX_ADDR_FMLY_IPV4,
                             &RemoteAddress, &FwlRemoteIpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                   (FwlRemoteIpAddr.uIpAddr.Ip4Addr));
        CliPrintf (CliHandle, "%-18s", pu1String);

        CliPrintf (CliHandle, "%-11d", i4RemotePort);

        CliPrintf (CliHandle, "%-7s",
                   ((i4Protocol == FWL_TCP) ? "TCP" : "UDP"));

        CliPrintf (CliHandle, "%-6s",
                   ((i4Direction == FWL_DIRECTION_IN) ? "IN" : "OUT"));

        CliPrintf (CliHandle, "%-7s",
                   ((i4AppCallStatus == FWL_APP_UNHOLD) ? "On" : "Off"));
        CliPrintf (CliHandle, "\r\n");
    }                            /* End of while */

    CliPrintf (CliHandle, "\r\n");
    return (FWL_SUCCESS);
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : FwlGetFwlConfigPrompt                                 */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns TRUE if given pi1ModeName is valid.             */
/*                    Returns FALSE if the given pi1ModeName is not valid     */
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/

INT1
FwlGetFwlConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = FWL_ZERO;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    u4Len = STRLEN (FIREWALL_CLI_MODE);

    if (STRNCMP (pi1ModeName, FIREWALL_CLI_MODE, u4Len) != FWL_ZERO)
    {
        return FALSE;
    }
    STRCPY (pi1DispStr, "(config-firewall)#");
    return TRUE;
}

/***********************************************************************/
/*  Function Name : FwlCliAddDefaultAcl                                */
/*  Description   : This function to add default rule in the Hw        */
/*                :                                                    */
/*  Input(s)      : CliHandle :- Pointer to CliHandle                  */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :CLI_SUCCESS or CLI_FAILURE                          */
/***********************************************************************/
INT4
FwlCliAddDefaultAcl (tCliHandle CliHandle)
{
    tFwlAccessList      FwlAccessList;
    tFwlFilterEntry     FwlFilterEntry;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE Name;

    MEMSET (&FwlFilterEntry, FWL_ZERO, sizeof (tFwlFilterEntry));
    MEMSET (&FwlAccessList, FWL_ZERO, sizeof (tFwlAccessList));
    CLI_STRCPY (FwlAccessList.au1AclName, "acldef");

    Name.pu1_OctetList = FwlAccessList.au1AclName;
    Name.i4_Length = (INT4) CLI_STRLEN (FwlAccessList.au1AclName);
    if ((i4RetVal = nmhGetFwlAclRowStatus (FWL_GLOBAL_IDX, &Name,
                                           FWL_DIRECTION_OUT,
                                           &i4RowStatus)) == SNMP_SUCCESS)
    {
        return (CLI_SUCCESS);
    }

    CLI_STRCPY (FwlFilterEntry.au1FilterName, "fildef");
    CLI_STRCPY (FwlFilterEntry.au1SrcAddress, "0.0.0.0/0");
    CLI_STRCPY (FwlFilterEntry.au1DestAddress, "0.0.0.0/0");
    FwlFilterEntry.u4IsProtoPres = FALSE;
    FwlFilterEntry.u4Protocol = FWL_PROTO_ANY;    /* default proto */
    FwlFilterEntry.u4SrcPortPres = TRUE;
    CLI_STRCPY (FwlFilterEntry.au1SrcPortRange, ">1");
    FwlFilterEntry.u4DestPortPres = TRUE;
    CLI_STRCPY (FwlFilterEntry.au1DestPortRange, ">1");
    FwlFilterEntry.u4EsPres = FALSE;
    FwlFilterEntry.u4RstPres = FALSE;
    FwlFilterEntry.u4Established = FALSE;
    FwlFilterEntry.u4Reset = FALSE;

    i4RetVal = FwlCliAddFilter (CliHandle, &FwlFilterEntry);

    if (i4RetVal == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CLI_STRCPY (FwlAccessList.au1AclName, "acldef");
    CLI_STRCPY (FwlAccessList.au1FilterComb, "fildef");
    FwlAccessList.u4Direction = (UINT4) CLI_ATOI (CLI_FWL_ACL_DIR_OUT);
    FwlAccessList.u4Permit = (UINT4) CLI_ATOI (CLI_FWL_ACL_REJECT);
    FwlAccessList.u4Priority = FWL_PROTO_ANY;
    FwlAccessList.u4LogTrigger = (UINT4) CLI_ATOI (CLI_FWL_LOG_NONE);
    FwlAccessList.u4FragAction = (UINT4) CLI_ATOI (CLI_FWL_ACL_PERMIT);
    FwlAccessList.u4Interface = FWL_GLOBAL_IDX;
    i4RetVal = FwlCliAddAccessList (CliHandle, &FwlAccessList);
    return (i4RetVal);
}

INT4
FwlCliSetPassNetbiosLan2Wan (tCliHandle CliHandle,
                             UINT4 u4FwlNetBiosLan2WanStat)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalNetBiosLan2Wan (&u4Error,
                                          (INT4) u4FwlNetBiosLan2WanStat) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Firewall Status can take only values"
                   "enable or disable \r\n");
        return (CLI_SUCCESS);
    }
    nmhSetFwlGlobalNetBiosLan2Wan (u4FwlNetBiosLan2WanStat);
    return (CLI_SUCCESS);
}

/* Url Filtering Code Functions Begin Here */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliSetUrlFiltering                              */
/*                                                                           */
/*     DESCRIPTION      : Fuction to Enable/Disable UrlFiltering.            */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4UrlFilteringStatus - ENABLE/DISABLE.             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliSetUrlFiltering (tCliHandle CliHandle, INT4 i4UrlFilteringStatus)
{
    UINT4               u4Error = FWL_ZERO;

    if (nmhTestv2FwlGlobalUrlFiltering (&u4Error, i4UrlFilteringStatus)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% ERROR: URL Filtering can take only values "
                   "enable or disable !\r\n");
        return CLI_FAILURE;
    }
    nmhSetFwlGlobalUrlFiltering (i4UrlFilteringStatus);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliAddUrlFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall Url filter entry. */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        pu1FilterName - Url Filter Name                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliAddUrlFilter (tCliHandle CliHandle, UINT1 *pu1FilterName)
{
    tSNMP_OCTET_STRING_TYPE Name;
    UINT4               u4FilterExists = FWL_ZERO;
    INT4                i4RowStatus = FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               au1Name[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };

    MEMSET (&Name, FWL_ZERO, sizeof (Name));
    Name.i4_Length = (INT4) STRLEN (pu1FilterName);
    Name.pu1_OctetList = au1Name;
    if (Name.pu1_OctetList == NULL)
    {

        CliPrintf (CliHandle, "\r %% ERROR: Cannot allocate memory. \r\n");
        return CLI_FAILURE;
    }
    MEMCPY (Name.pu1_OctetList, pu1FilterName, Name.i4_Length);

    if ((i4RetVal = nmhGetFwlUrlFilterRowStatus (&Name, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        if (nmhTestv2FwlUrlFilterRowStatus (&u4FilterExists,
                                            &Name,
                                            FWL_CREATE_AND_GO) != SNMP_FAILURE)
        {
            if (nmhSetFwlUrlFilterRowStatus (&Name, FWL_CREATE_AND_GO)
                != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%% ERROR: Max Url filters configured. "
                           "Cannot add a new filter !\r\n");
                return CLI_FAILURE;
            }
            nmhSetFwlUrlFilterRowStatus (&Name, FWL_ACTIVE);
        }
    }
    else
    {
        CliPrintf (CliHandle, "%% ERROR:Filter already exits !\r\n");
        return CLI_SUCCESS;
    }

    return CLI_SUCCESS;
}                                /* End of Function. */

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : FwlGetBlkWhiteConfigPrompt                              */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns OSIX_TRUE if given pi1ModeName is valid.        */
/*                    Returns OSIX_FALSE if the given pi1ModeName is not valid*/
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : OSIX_TRUE/OSIX_FALSE                                    */
/*                                                                            */
/******************************************************************************/

INT1
FwlGetBlkWhiteConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = FWL_ZERO;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return OSIX_FALSE;
    }

    u4Len = STRLEN (LIST_CLI_MODE);

    if (STRNCMP (pi1ModeName, LIST_CLI_MODE, u4Len) != FWL_ZERO)
    {
        return OSIX_FALSE;
    }
    STRCPY (pi1DispStr, "(config-list)#");
    return OSIX_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliConfIPAddressInBlkList                       */
/*                                                                           */
/*     DESCRIPTION      : This function creates an entry in the              */
/*                        fwlDefnBlkListTable.                               */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliConfIPAddressInBlkList (tCliHandle CliHandle, tFwlIpAddr * pAddr,
                              UINT2 u2PrefixLen, UINT1 u1Action)
{
    UINT4               u4ErrCode = FWL_ZERO;
    UINT4               u4BlkListIpMask = FWL_ZERO;
    INT4                i4BlkListIpAddrType = FWL_ZERO;
    UINT4               u4LocalIp = FWL_ZERO;
    UINT1               au1Array[FWL_IPV6_ADDR_LEN] = { FWL_ZERO };
    CHR1               *pString = NULL;
    tSNMP_OCTET_STRING_TYPE BlkListIpAddr;
    tUtlIn6Addr         IP6Addr;

    MEMSET (&au1Array, FWL_ZERO, FWL_IPV6_ADDR_LEN);
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));
    BlkListIpAddr.pu1_OctetList = &au1Array[FWL_INDEX_0];
    BlkListIpAddr.i4_Length = FWL_ZERO;

    i4BlkListIpAddrType = (INT4) pAddr->u4AddrType;
    u4BlkListIpMask = u2PrefixLen;

    if (IPVX_ADDR_FMLY_IPV4 == i4BlkListIpAddrType)
    {
        u4LocalIp = OSIX_NTOHL (pAddr->v4Addr);
        CLI_CONVERT_IPADDR_TO_STR (pString, u4LocalIp);

        BlkListIpAddr.i4_Length = FWL_IPV4_ADDR_LEN;
        MEMCPY (BlkListIpAddr.pu1_OctetList, (UINT1 *) pString,
                BlkListIpAddr.i4_Length);
    }
    else
    {
        MEMCPY (IP6Addr.u1addr, pAddr->v6Addr.u1_addr, IPVX_IPV6_ADDR_LEN);
        pString = INET_NTOA6 (IP6Addr);
        BlkListIpAddr.i4_Length = FWL_IPV6_ADDR_LEN;
        MEMCPY (BlkListIpAddr.pu1_OctetList, (UINT1 *) pString,
                BlkListIpAddr.i4_Length);
    }

    if (SNMP_SUCCESS != nmhTestv2FwlBlkListRowStatus (&u4ErrCode,
                                                      i4BlkListIpAddrType,
                                                      &BlkListIpAddr,
                                                      u4BlkListIpMask,
                                                      u1Action))
    {
        if (SNMP_ERR_NO_CREATION == u4ErrCode)
        {
            CliPrintf (CliHandle, "%%ERROR:Maximum number of Entries reached!!"
                       "! \r\n");
        }
        else
        {
            CLI_SET_ERR (CLI_FWL_ERR_INVALID_BLACK_WHITE_LIST);
        }
        return CLI_FAILURE;

    }
    if (SNMP_SUCCESS != nmhSetFwlBlkListRowStatus (i4BlkListIpAddrType,
                                                   &BlkListIpAddr,
                                                   u4BlkListIpMask, u1Action))
    {
        CliPrintf (CliHandle, "%% ERROR: Black List Addition/Deletion Failed."
                   " \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliConfIPAddressInWhiteList                     */
/*                                                                           */
/*     DESCRIPTION      : This function creates an entry in the              */
/*                        fwlDefnWhiteListTable.                             */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliConfIPAddressInWhiteList (tCliHandle CliHandle, tFwlIpAddr * pAddr,
                                UINT2 u2PrefixLen, UINT1 u1Action)
{
    UINT4               u4ErrCode = FWL_ZERO;
    UINT4               u4WhiteListIpMask = FWL_ZERO;
    INT4                i4WhiteListIpAddrType = FWL_ZERO;
    UINT4               u4LocalIp = FWL_ZERO;
    UINT1               au1Array[FWL_IPV6_ADDR_LEN] = { FWL_ZERO };
    CHR1               *pString = NULL;
    tSNMP_OCTET_STRING_TYPE WhiteListIpAddr;
    tUtlIn6Addr         IP6Addr;

    MEMSET (&au1Array, FWL_ZERO, FWL_IPV6_ADDR_LEN);
    MEMSET (&IP6Addr, FWL_ZERO, sizeof (tUtlIn6Addr));
    WhiteListIpAddr.pu1_OctetList = &au1Array[FWL_INDEX_0];
    WhiteListIpAddr.i4_Length = FWL_ZERO;

    i4WhiteListIpAddrType = (INT4) pAddr->u4AddrType;
    u4WhiteListIpMask = u2PrefixLen;

    if (IPVX_ADDR_FMLY_IPV4 == i4WhiteListIpAddrType)
    {
        u4LocalIp = OSIX_NTOHL (pAddr->v4Addr);
        CLI_CONVERT_IPADDR_TO_STR (pString, u4LocalIp);

        WhiteListIpAddr.i4_Length = FWL_IPV4_ADDR_LEN;
        MEMCPY (WhiteListIpAddr.pu1_OctetList, (UINT1 *) pString,
                WhiteListIpAddr.i4_Length);
    }
    else
    {
        MEMCPY (IP6Addr.u1addr, pAddr->v6Addr.u1_addr, IPVX_IPV6_ADDR_LEN);
        pString = INET_NTOA6 (IP6Addr);
        WhiteListIpAddr.i4_Length = FWL_IPV6_ADDR_LEN;
        MEMCPY (WhiteListIpAddr.pu1_OctetList, (UINT1 *) pString,
                WhiteListIpAddr.i4_Length);
    }

    if (SNMP_SUCCESS != nmhTestv2FwlWhiteListRowStatus (&u4ErrCode,
                                                        i4WhiteListIpAddrType,
                                                        &WhiteListIpAddr,
                                                        u4WhiteListIpMask,
                                                        u1Action))
    {
        if (SNMP_ERR_NO_CREATION == u4ErrCode)
        {
            CliPrintf (CliHandle, "%%ERROR: Maximum number of Entries reached"
                       "!!! \r\n");
        }
        else
        {
            CLI_SET_ERR (CLI_FWL_ERR_INVALID_BLACK_WHITE_LIST);
        }
        return CLI_FAILURE;

    }
    if (SNMP_SUCCESS != nmhSetFwlWhiteListRowStatus (i4WhiteListIpAddrType,
                                                     &WhiteListIpAddr,
                                                     u4WhiteListIpMask,
                                                     u1Action))
    {
        CliPrintf (CliHandle, "%% ERROR: White List Addition/Deletion Failed."
                   " \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliPurgeBlackWhiteListDataBase                  */
/*                                                                           */
/*     DESCRIPTION      : This function deletes BlackList/WhiteList/Both     */
/*                        based on the ListType given.                       */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliPurgeBlackWhiteListDataBase (tCliHandle CliHandle, UINT4 u4ListType)
{
    INT4                i4AddrType = FWL_ZERO;
    INT4                i4NextAddrType = FWL_ZERO;
    INT4                i4ListFound = FWL_TRUE;
    UINT4               u4PrefLen = FWL_ZERO;
    UINT4               u4NextPrefLen = FWL_ZERO;
    UINT1               au1Array[FWL_IPV6_ADDR_LEN] = { FWL_ZERO };
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tSNMP_OCTET_STRING_TYPE NextIpAddr;

    MEMSET (&au1Array, FWL_ZERO, FWL_IPV6_ADDR_LEN);
    IpAddr.pu1_OctetList = &au1Array[FWL_INDEX_0];
    IpAddr.i4_Length = FWL_ZERO;

    UNUSED_PARAM (CliHandle);
    if (u4ListType & CLI_BLK_LIST)
    {
        if (SNMP_FAILURE == nmhGetFirstIndexFwlDefnBlkListTable (&i4AddrType,
                                                                 &IpAddr,
                                                                 &u4PrefLen))
        {
            i4ListFound = FWL_FALSE;
        }
        if (FWL_TRUE == i4ListFound)
        {
            i4NextAddrType = i4AddrType;
            u4NextPrefLen = u4PrefLen;
            MEMCPY (&NextIpAddr, &IpAddr, sizeof (IpAddr));
            do
            {
                if (SNMP_FAILURE == nmhSetFwlBlkListRowStatus (i4NextAddrType,
                                                               &NextIpAddr,
                                                               u4NextPrefLen,
                                                               FWL_DESTROY))
                {
                    return CLI_FAILURE;
                }
                i4AddrType = i4NextAddrType;
                u4PrefLen = u4NextPrefLen;
                MEMCPY (&IpAddr, &NextIpAddr, sizeof (NextIpAddr));
            }
            while (SNMP_FAILURE != nmhGetNextIndexFwlDefnBlkListTable
                   (i4AddrType,
                    &i4NextAddrType,
                    &IpAddr, &NextIpAddr, u4PrefLen, &u4NextPrefLen));
        }
    }

    if (u4ListType & CLI_WHITE_LIST)
    {
        MEMSET (&au1Array, FWL_ZERO, IPVX_IPV6_ADDR_LEN);
        if (SNMP_FAILURE == nmhGetFirstIndexFwlDefnWhiteListTable (&i4AddrType,
                                                                   &IpAddr,
                                                                   &u4PrefLen))
        {
            return CLI_FAILURE;
        }
        i4NextAddrType = i4AddrType;
        u4NextPrefLen = u4PrefLen;
        MEMCPY (&NextIpAddr, &IpAddr, sizeof (IpAddr));
        do
        {
            if (SNMP_FAILURE == nmhSetFwlWhiteListRowStatus (i4NextAddrType,
                                                             &NextIpAddr,
                                                             u4NextPrefLen,
                                                             FWL_DESTROY))
            {
                return CLI_FAILURE;
            }
            i4AddrType = i4NextAddrType;
            u4PrefLen = u4NextPrefLen;
            MEMCPY (&IpAddr, &NextIpAddr, sizeof (NextIpAddr));
        }
        while (SNMP_FAILURE != nmhGetNextIndexFwlDefnWhiteListTable (i4AddrType,
                                                                     &i4NextAddrType,
                                                                     &IpAddr,
                                                                     &NextIpAddr,
                                                                     u4PrefLen,
                                                                     &u4NextPrefLen));
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliSetLogSize                                   */
/*                                                                           */
/*     DESCRIPTION      : Sets the file size for firewall log file.          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4FwlLogFileSize - File size for firewall log file.*/
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliSetLogFileSize (tCliHandle CliHandle, UINT4 u4FwlLogFileSize)
{
    UINT4               u4ErrorCode = FWL_ZERO;
    if (nmhTestv2FwlGlobalLogFileSize (&u4ErrorCode, u4FwlLogFileSize) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Firewall log File size\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFwlGlobalLogFileSize (u4FwlLogFileSize) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliSetLogSizeThreshold                          */
/*                                                                           */
/*     DESCRIPTION      : Sets the threshold value for firewall log file size*/
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4FwlLogSizeThreshold - Threshold value for        */
/*                                                firewall log file size.    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliSetLogSizeThreshold (tCliHandle CliHandle, UINT4 u4FwlLogSizeThreshold)
{
    UINT4               u4ErrorCode = FWL_ZERO;

    if (SNMP_FAILURE == nmhTestv2FwlGlobalLogSizeThreshold
        (&u4ErrorCode, u4FwlLogSizeThreshold))
    {
        CliPrintf (CliHandle, "\r%% Invalid Firewall Log Threshold size \r\n");
        return CLI_FAILURE;
    }
    if (SNMP_FAILURE == nmhSetFwlGlobalLogSizeThreshold (u4FwlLogSizeThreshold))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliShowUrlFilters                               */
/*                                                                           */
/*     DESCRIPTION      : This function shows  Url filter table              */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliShowUrlFilters (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE UrlString;
    tSNMP_OCTET_STRING_TYPE NextFwlUrlString;
    UINT1               au1FwlUrlString[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };
    UINT4               u4UrlHitCount = FWL_ZERO;

    CliPrintf (CliHandle, "\r\nHitCount      URL String\r\n");
    CliPrintf (CliHandle, "----------------------------\r\n");

    UrlString.pu1_OctetList = au1FwlUrlString;
    UrlString.i4_Length = FWL_MAX_URL_FILTER_STRING_LEN;

    MEMSET (UrlString.pu1_OctetList, FWL_ZERO, FWL_MAX_URL_FILTER_STRING_LEN);

    if ((nmhGetFirstIndexFwlUrlFilterTable (&UrlString)) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    MEMCPY (&NextFwlUrlString, &UrlString, sizeof (UrlString));
    do
    {

        /* To get the Url Hitcount */
        nmhGetFwlUrlHitCount (&NextFwlUrlString, &u4UrlHitCount);
        NextFwlUrlString.pu1_OctetList[NextFwlUrlString.i4_Length] = FWL_ZERO;

        CliPrintf (CliHandle, "%-10d %s\r\n", u4UrlHitCount,
                   NextFwlUrlString.pu1_OctetList);

        MEMCPY (&UrlString, &NextFwlUrlString, sizeof (UrlString));
    }
    while (nmhGetNextIndexFwlUrlFilterTable (&UrlString, &NextFwlUrlString)
           == SNMP_SUCCESS);

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliShowBlackWhiteListDataBase                   */
/*                                                                           */
/*     DESCRIPTION      : This function  Displays the Blacklist/             */
/*                        Whitelist entry.                                   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4ListType - BlackList/WhiteList/Both              */
/*                        i1FilterCount - Need to display Filtercount or not */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliShowBlackWhiteListDataBase (tCliHandle CliHandle, UINT4 u4ListType,
                                  INT1 i1ListHitCount)
{
    INT4                i4AddrType = FWL_ZERO;
    INT4                i4NextAddrType = FWL_ZERO;
    INT4                i4EntryType = FWL_ZERO;
    INT4                i4ListFound = FWL_TRUE;
    UINT4               u4PrefLen = FWL_ZERO;
    UINT4               u4NextPrefLen = FWL_ZERO;
    UINT4               u4HitCount = FWL_ZERO;
    UINT1               au1Array[FWL_IPV6_ADDR_LEN] = { FWL_ZERO };
    tSNMP_OCTET_STRING_TYPE IPAddr;
    tSNMP_OCTET_STRING_TYPE NextIPAddr;

    MEMSET (&au1Array, FWL_ZERO, FWL_IPV6_ADDR_LEN);
    MEMSET (&IPAddr, FWL_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextIPAddr, FWL_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPAddr.pu1_OctetList = &au1Array[FWL_INDEX_0];

    if (u4ListType & CLI_BLK_LIST)
    {

        if (SNMP_FAILURE == nmhGetFirstIndexFwlDefnBlkListTable (&i4AddrType,
                                                                 &IPAddr,
                                                                 &u4PrefLen))
        {
            i4ListFound = FWL_FALSE;
        }

        if (FWL_TRUE == i4ListFound)
        {
            CliPrintf (CliHandle, "\r\nBlackList Address Table \r\n");
            CliPrintf (CliHandle, "----------------------- \r\n");
            CliPrintf (CliHandle, "\r\nAddressType     IPAddress"
                       "                                SubnetMask    EntryType");
            if (FWL_SET == i1ListHitCount)
            {
                CliPrintf (CliHandle, "    BlackListHitCount \r\n");
            }
            else
            {
                CliPrintf (CliHandle, " \r\n");
            }

            CliPrintf (CliHandle,
                       "-----------     ---------                                ----------"
                       "    ---------");
            if (FWL_SET == i1ListHitCount)
            {
                CliPrintf (CliHandle, "    ----------------- \r\n");
            }
            else
            {
                CliPrintf (CliHandle, " \r\n");
            }

            i4NextAddrType = i4AddrType;
            u4NextPrefLen = u4PrefLen;
            MEMCPY (&NextIPAddr, &IPAddr, sizeof (IPAddr));
            do
            {
                if (IPVX_ADDR_FMLY_IPV4 == i4NextAddrType)
                {
                    CliPrintf (CliHandle, "%-15s", "IPV4");
                }
                else
                {
                    CliPrintf (CliHandle, "%-15s", "IPV6");
                }
                CliPrintf (CliHandle, "%-42s", NextIPAddr.pu1_OctetList);
                CliPrintf (CliHandle, "%-15d", u4NextPrefLen);

                nmhGetFwlBlkListEntryType (i4NextAddrType, &NextIPAddr,
                                           u4NextPrefLen, &i4EntryType);
                if (FWL_SET == i4EntryType)
                {
                    CliPrintf (CliHandle, "%-15s", "Dynamic");
                }
                else
                {
                    CliPrintf (CliHandle, "%-15s", "Static");
                }
                if (FWL_SET == i1ListHitCount)
                {
                    nmhGetFwlBlkListHitsCount (i4NextAddrType, &NextIPAddr,
                                               u4NextPrefLen, &u4HitCount);
                    CliPrintf (CliHandle, "%-15d", u4HitCount);
                }
                CliPrintf (CliHandle, " \r\n");
                i4AddrType = i4NextAddrType;
                u4PrefLen = u4NextPrefLen;
                MEMCPY (&IPAddr, &NextIPAddr, sizeof (NextIPAddr));
            }
            while (SNMP_FAILURE != nmhGetNextIndexFwlDefnBlkListTable
                   (i4AddrType,
                    &i4NextAddrType,
                    &IPAddr, &NextIPAddr, u4PrefLen, &u4NextPrefLen));
        }
    }

    if (u4ListType & CLI_WHITE_LIST)
    {
        MEMSET (&au1Array, FWL_ZERO, FWL_IPV6_ADDR_LEN);
        if (SNMP_FAILURE == nmhGetFirstIndexFwlDefnWhiteListTable (&i4AddrType,
                                                                   &IPAddr,
                                                                   &u4PrefLen))
        {
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, "\r\nWhiteList Address Table \r\n");
        CliPrintf (CliHandle, "----------------------- \r\n");
        CliPrintf (CliHandle, "\r\nAddressType     IPAddress       SubnetMask");
        if (FWL_SET == i1ListHitCount)
        {
            CliPrintf (CliHandle, "    WhiteListHitCount \r\n");
        }
        else
        {
            CliPrintf (CliHandle, " \r\n");
        }

        CliPrintf (CliHandle, "-----------     ---------       ----------");
        if (FWL_SET == i1ListHitCount)
        {
            CliPrintf (CliHandle, "    ----------------- \r\n");
        }
        else
        {
            CliPrintf (CliHandle, " \r\n");
        }

        i4NextAddrType = i4AddrType;
        u4NextPrefLen = u4PrefLen;
        MEMCPY (&NextIPAddr, &IPAddr, sizeof (IPAddr));
        do
        {
            if (IPVX_ADDR_FMLY_IPV4 == i4NextAddrType)
            {
                CliPrintf (CliHandle, "%-15s", "IPV4");
            }
            else
            {
                CliPrintf (CliHandle, "%-15s", "IPV6");
            }
            CliPrintf (CliHandle, "%-15s", NextIPAddr.pu1_OctetList);
            CliPrintf (CliHandle, "%-15d", u4NextPrefLen);

            if (FWL_SET == i1ListHitCount)
            {
                nmhGetFwlWhiteListHitsCount (i4NextAddrType, &NextIPAddr,
                                             u4NextPrefLen, &u4HitCount);
                CliPrintf (CliHandle, "%-15d", u4HitCount);
            }
            CliPrintf (CliHandle, " \r\n");
            i4AddrType = i4NextAddrType;
            u4PrefLen = u4NextPrefLen;
            MEMCPY (&IPAddr, &NextIPAddr, sizeof (NextIPAddr));

        }
        while (SNMP_FAILURE != nmhGetNextIndexFwlDefnWhiteListTable (i4AddrType,
                                                                     &i4NextAddrType,
                                                                     &IPAddr,
                                                                     &NextIPAddr,
                                                                     u4PrefLen,
                                                                     &u4NextPrefLen));
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliDeleteUrlFilter                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a firewall Url filter entry  */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        pu1FilterName - Url Filter Name                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlCliDeleteUrlFilter (tCliHandle CliHandle, UINT1 *pu1FilterName)
{
    tSNMP_OCTET_STRING_TYPE UrlFilterName;
    UINT4               u4FilterExists = FWL_ZERO;
    UINT1               au1UrlFilterName[MAX_OCTETSTRING_SIZE] = { FWL_ZERO };

    MEMSET (&UrlFilterName, FWL_ZERO, sizeof (UrlFilterName));
    UrlFilterName.i4_Length = (INT4) STRLEN (pu1FilterName);
    UrlFilterName.pu1_OctetList = au1UrlFilterName;
    MEMCPY (UrlFilterName.pu1_OctetList,
            pu1FilterName, UrlFilterName.i4_Length);

    if (nmhTestv2FwlUrlFilterRowStatus (&u4FilterExists,
                                        &UrlFilterName,
                                        FWL_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% ERROR: The Url Filter does not exits !\r\n");
        return CLI_FAILURE;
    }
    nmhSetFwlUrlFilterRowStatus (&UrlFilterName, FWL_DESTROY);
    return CLI_SUCCESS;

}                                /* End of Function. */

INT4
FwlCliReSetUntrustedPort (tCliHandle CliHandle, UINT4 u4Interface)
{
    UINT4               u4Error = FWL_ZERO;
    INT4                i4RowStatus = FWL_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4IfType = FWL_INTERNAL_IF;

    if ((i1RetVal = nmhGetFwlIfRowStatus ((INT4) u4Interface, &i4RowStatus))
        != SNMP_SUCCESS)
    {
        if (nmhTestv2FwlIfRowStatus
            (&u4Error, (INT4) u4Interface, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% ERROR : This I/f (%d) is not an Untrusted Port."
                       "Error while creating Untrusted Port \r\n", u4Interface);
            return CLI_FAILURE;
        }
        nmhSetFwlIfRowStatus (u4Interface, CREATE_AND_WAIT);
        nmhSetFwlIfRowStatus (u4Interface, NOT_IN_SERVICE);
    }
    else
    {
        if (nmhTestv2FwlIfRowStatus
            (&u4Error, (INT4) u4Interface, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% ERROR : This I/f (%d) is not an Untrusted Port."
                       "Error while creating Untrusted Port \r\n", u4Interface);
            return CLI_FAILURE;
        }
        nmhSetFwlIfRowStatus (u4Interface, NOT_IN_SERVICE);
    }
    if (nmhTestv2FwlIfIfType (&u4Error, (INT4) u4Interface, (INT4) u4IfType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%% ERROR : This I/f (%d) is not an Untrusted Port."
                   "Error while setting it as Trusted Port \r\n", u4Interface);
        nmhSetFwlIfRowStatus (u4Interface, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFwlIfIfType (u4Interface, u4IfType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%% ERROR : This I/f (%d) is not an Untrusted Port."
                   "Error while setting it as Trusted Port \r\n", u4Interface);
        nmhSetFwlIfRowStatus (u4Interface, DESTROY);
        return CLI_FAILURE;

    }

    if (nmhTestv2FwlIfRowStatus (&u4Error, (INT4) u4Interface, ACTIVE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%% ERROR : This I/f (%d) is not an Untrusted Port."
                   "Error while making this trusted Port to Active \r\n",
                   u4Interface);
        nmhSetFwlIfRowStatus (u4Interface, DESTROY);
        return CLI_FAILURE;
    }
    nmhSetFwlIfRowStatus (u4Interface, ACTIVE);
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  PrintFwlPinHolesHdr                                    *
*                                                                         *
* Description   :  This function prints the CLI header Firewall Pin holes *
*                  added for SIP ALG                                      *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
VOID
PrintFwlPinHolesHdr (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "Local            Local      Remote            Remote\
               Proto  Direc Timer\r\n");

    CliPrintf (CliHandle, "IpAddress        Port       Address           Port\
               col    tion  Status\r\n");

    CliPrintf (CliHandle,
               "---------------- ---------- ----------------- ----------\
               ------ ----- -------\r\n");
}

/*****************************************************************************/
/* Function Name      : FwlShowRunningConfig                                 */
/*                                                                           */
/* Description        : Displays configurations done in FIREWALL  module     */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
FwlShowRunningConfig (tCliHandle CliHandle)
{
    if (FwlShowRunningConfigScalar (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (FwlShowRunningConfigTabular (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

#endif /* FIREWALL_WANTED */
#endif /* __FWLCLI_C__ */
