/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlurl.c,v 1.6 2014/01/25 13:52:27 siva Exp $
 *
 * Description: This file is used to process the packets
 *              against the URL filters.This file also contains all 
 *              routines for maintaining the URL-Filter-List.
 *
 *******************************************************************/
#include "fwlinc.h"
#include "fwlurl.h"

static UINT1        gaHTTP_POST_METHOD[] = "POST";
static UINT1        gaHTTP_GET_METHOD[] = "GET";
static UINT1        gaHTTP_PUT_METHOD[] = "PUT";
static UINT1        gaHTTP_DELETE_METHOD[] = "DELETE";

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUrlFilteringInit                              */
/*                                                                          */
/*    Description        : Initialises the Data structures for Url          */
/*                         Filtering feature.                               */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlUrlFilteringInit (VOID)
{
    gFwlAclInfo.u1UrlFilteringEnable = FWL_FILTERING_DISABLED;
    TMO_SLL_Init (&FwlUrlFilterList);
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUrlFilterMemAllocate                          */
/*                                                                          */
/*    Description        : Allocates memory for a UrlFilterList node.       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MEM_SUCCESS or MEM_FAILURE                       */
/****************************************************************************/

PUBLIC UINT4
FwlUrlFilterMemAllocate (UINT1 **ppu1Block)
{
    UINT4               u4MemAllocateStatus = MEM_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlUrlFilterMemAllocate\n");

    u4MemAllocateStatus = MemAllocateMemBlock (FWL_URL_FILTER_PID, ppu1Block);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlUrlFilterMemAllocate\n");
    return u4MemAllocateStatus;
}                                /*End of the Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetDefaultUrlFilterValue                      */
/*                                                                          */
/*    Description        : Set the Default values for UrlFilter Node        */
/*                                                                          */
/*    Input(s)           : au1FilterName   --Url Filter Name                */
/*                                                                          */
/*    Output(s)          : pFilterNode     --Pointer to Filter Node Created */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
FwlSetDefaultUrlFilterValue (UINT1 au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN],
                             tFwlUrlFilterInfo * pFilterNode)
{
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetDefaultUrlFilterValue\n");
    /* Memory allocation and initialisation of the Filter node with
     * default values .
     */
    TMO_SLL_Init_Node (&pFilterNode->nextFwlUrlFilterInfo);
    FWL_STRNCPY ((INT1 *) pFilterNode->au1UrlStr, (INT1 *) au1FilterName,
                 FWL_MAX_URL_FILTER_STRING_LEN - FWL_ONE);
    pFilterNode->u4UrlHitCount = FWL_DEFAULT_COUNT;
    pFilterNode->u1RowStatus = FWL_NOT_READY;

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetDefaultUrlFilterValue\n");
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddUrlFilter                                  */
/*                                                                          */
/*    Description        : Add the Filter Node to Url Filter List           */
/*                                                                          */
/*    Input(s)           : pFilterNode  -- Pointer to the UrlFilter Node    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
FwlAddUrlFilter (tFwlUrlFilterInfo * pFilterNode)
{
    tFwlUrlFilterInfo  *pFilterTmpNode = NULL;
    tFwlUrlFilterInfo  *pFilterPrevNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into function FwlAddUrlFilter \n");

    TMO_SLL_Scan (&FwlUrlFilterList, pFilterTmpNode, tFwlUrlFilterInfo *)
    {
        if (STRLEN (pFilterTmpNode->au1UrlStr) >
            STRLEN (pFilterNode->au1UrlStr))
        {
            break;
        }
        if (STRLEN (pFilterTmpNode->au1UrlStr) ==
            STRLEN (pFilterNode->au1UrlStr))
        {
            if (FWL_STRCMP (pFilterTmpNode->au1UrlStr,
                            pFilterNode->au1UrlStr) > FWL_ZERO)
            {
                break;
            }
        }
        pFilterPrevNode = pFilterTmpNode;
    }
    TMO_SLL_Insert (&FwlUrlFilterList, (tTMO_SLL_NODE *) pFilterPrevNode,
                    (tTMO_SLL_NODE *) pFilterNode);

    FWL_DBG (FWL_DBG_EXIT, "\nExiting from function FwlAddUrlFilter \n");

}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeleteUrlFilter                               */
/*                                                                          */
/*    Description        : Deletes the Url Filter From the UrlFilter List   */
/*                                                                          */
/*    Input(s)           : au1FilterName -- String containing Filter name   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if Filter Name exists , otherwisw        */
/*                         FAILURE                                          */
/****************************************************************************/

PUBLIC UINT4
FwlDeleteUrlFilter (UINT1 au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN])
{
    UINT4               u4Status = FWL_ZERO;
    tFwlUrlFilterInfo  *pFilterNode = NULL;

    u4Status = FWL_FAILURE;
    pFilterNode = (tFwlUrlFilterInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDeleteUrlFilter \n");
    /* search for the filter node in the UrlFilter list that is to be deleted
     * Get the filter pointer from the UrlFilter list
     */
    pFilterNode = FwlSearchUrlFilter (au1FilterName);
    if (pFilterNode != NULL)
    {
        TMO_SLL_Delete (&FwlUrlFilterList, (tTMO_SLL_NODE *) pFilterNode);
        FWL_DBG (FWL_DBG_INSPECT, "\n Url Filter Node is deleted \n");
        /* Free the Node */
        if (MemReleaseMemBlock (FWL_URL_FILTER_PID,
                                (UINT1 *) pFilterNode) == MEM_SUCCESS)
        {
            u4Status = FWL_SUCCESS;
            FWL_DBG (FWL_DBG_INSPECT, "\n Memory Free - Url Filter Node \n");
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDeleteUrlFilter \n");
    return u4Status;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSearchUrlFilter                               */
/*                                                                          */
/*    Description        : Searches the UrlFilter List for the given filter */
/*                         name                                             */
/*                                                                          */
/*    Input(s)           : au1FilterName -- String containing UrlFilter name*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the filter node if exists, otherwise  */
/*                         returns NULL.                                    */
/****************************************************************************/

PUBLIC tFwlUrlFilterInfo *
FwlSearchUrlFilter (UINT1 au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN])
{
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    UINT1               u1Status = FWL_ZERO;
    u1Status = FWL_NOT_MATCH;
    pFilterNode = (tFwlUrlFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlSearchUrlFilter \n");
    /* Search the UrlFilter List for the Particular Url filter Name. If found
     * return the pointer to the UrlFilter Node , else NULL
     */
    TMO_SLL_Scan (&FwlUrlFilterList, pFilterNode, tFwlUrlFilterInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) au1FilterName,
             (INT1 *) pFilterNode->au1UrlStr) == FWL_STRING_EQUAL)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }
    if (u1Status == FWL_MATCH)
    {
        FWL_DBG1 (FWL_DBG_INSPECT,
                  "\n pFilterNode->au1UrlStr = %s\n", pFilterNode->au1UrlStr);
        FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlSearchUrlFilter \n");
        return pFilterNode;
    }
    return NULL;

}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchUrlFilter                                */
/*                                                                          */
/*    Description        : This functions looks for a URL Filter in the     */
/*                         the HTTP packet to be filtered.                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_MATCH if URL match found. Else FWL_NOT_MATCH */
/****************************************************************************/

PUBLIC UINT4
FwlMatchUrlFilter (tCRU_BUF_CHAIN_HEADER * pBuf,
                   UINT4 u4HttpPktOffset, UINT4 u4HttpPktLen)
{
    INT1               *pi1UrlInfo = NULL;
    tFwlUrlFilterInfo  *pFilterNode = NULL;
    UINT1               u1Status = FWL_NOT_MATCH;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn.  FwlMatchUrlFilter \n");

    /* Extract the Url from HTTP-pkt if its a HTTP Req-Pkt */
    /* Allocate memory for 'pi1UrlInfo' and copy extracted URL into it. */
    if (FwlGetUrlFromHttpPkt (pBuf, u4HttpPktOffset, u4HttpPktLen,
                              &pi1UrlInfo) == FWL_FAILURE)
    {
        return FWL_NOT_MATCH;    /* No URL found to be Filtered */
    }

    /* Get URL Filters from FwlUrlFilterList */

    pFilterNode = (tFwlUrlFilterInfo *) NULL;
    u1Status = FWL_NOT_MATCH;
    /* Search the UrlFilter List for the Particular Url filter Name. If found
     * Increment the UrlHitCount of UrlFilter Node.
     */
    TMO_SLL_Scan (&FwlUrlFilterList, pFilterNode, tFwlUrlFilterInfo *)
    {
        if (FWL_STRSTR
            ((UINT1 *) pi1UrlInfo, (UINT1 *) (pFilterNode->au1UrlStr)) != NULL)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }                            /* End of TMO_SLL_Scan */
    if (u1Status == FWL_MATCH)
    {
        FWL_DBG1 (FWL_DBG_INSPECT,
                  "\n UrlFilterFound: pFilterNode->au1UrlStr = %s\n",
                  pFilterNode->au1UrlStr);
        FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlMatchUrlFilter \n");
        /* Increment the Hit-Count of the URL Filter. */
        ((tFwlUrlFilterInfo *) pFilterNode)->u4UrlHitCount++;
    }

    /* Free Memory allocated for URL string extracted from HTTP packet. */
    if (MemReleaseMemBlock (FWL_URL_LENGTH_PID, (UINT1 *) pi1UrlInfo) !=
        MEM_SUCCESS)
    {
        return FWL_FAILURE;
    }
    return u1Status;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetUrlFromHttpPkt                             */
/*                                                                          */
/*    Description        : Function to extract a URL from the HTTP packet.  */
/*                                                                          */
/*    Input(s)           : pBuf - pointer to HTTP packet None               */
/*                         u4HttpPktOffset - Offset of the HTTP pkt in pBuf */
/*                         u4HttpPktLen - HTTP packet length                */
/*                                                                          */
/*    Output(s)          : pUrlInfo - pointer to the extracted Url String.  */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if URL is found. Else FWL_FAILURE    */
/****************************************************************************/

PUBLIC UINT4
FwlGetUrlFromHttpPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT4 u4HttpPktOffset,
                      UINT4 u4HttpPktLen, INT1 **ppi1UrlInfo)
{
    INT1                i1Char = FWL_ZERO;
    INT1                ai1Method[FWL_MAX_METHOD_NAME_LEN] = { FWL_ZERO };
    UINT4               u4Len = FWL_ZERO;
    UINT4               u4UrlStartOffset = FWL_ZERO;
    UINT4               u4UrlLength = FWL_ZERO;
    UINT4               u4Status = FWL_FAILURE;
    UINT4               u4HostStartOffset = FWL_ZERO;
    UINT4               u4HostLen = FWL_ZERO;

    MEMSET (ai1Method, FWL_ZERO, FWL_MAX_METHOD_NAME_LEN);
    *ppi1UrlInfo = NULL;

    /* Read HTTP Request Method GET/POST/PUT/DELETE/HEAD */
    for (u4Len = FWL_ZERO; (u4Len < FWL_MAX_METHOD_NAME_LEN) &&
         (u4Len < u4HttpPktLen); u4Len++)
    {
        /* Get a char from the Http-Pkt */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1Char,
                                   u4HttpPktOffset + u4Len, sizeof (INT1));
        /* Check if it is Space char */
        if (i1Char == ' ')
        {
            ai1Method[u4Len] = '\0';
            break;
        }
        ai1Method[u4Len] = i1Char;
    }                            /*End-of-For-Loop */

    /* Check for the host field, if present then fetch the string present */
    u4Status = FwlGetHttpHostField (pBuf, u4HttpPktOffset, u4HttpPktLen,
                                    &u4HostStartOffset, &u4HostLen);

    if ((!(STRCMP (ai1Method, gaHTTP_GET_METHOD) == FWL_ZERO)) &&
        (!(STRCMP (ai1Method, gaHTTP_PUT_METHOD) == FWL_ZERO)) &&
        (!(STRCMP (ai1Method, gaHTTP_POST_METHOD) == FWL_ZERO)) &&
        (!(STRCMP (ai1Method, gaHTTP_DELETE_METHOD) == FWL_ZERO)) &&
        (u4Status == FWL_FAILURE))
    {
        return FWL_FAILURE;
    }

    /* Read HTTP Request URL */

    if ((STRCMP (ai1Method, gaHTTP_GET_METHOD) == FWL_ZERO) ||
        (STRCMP (ai1Method, gaHTTP_PUT_METHOD) == FWL_ZERO) ||
        (STRCMP (ai1Method, gaHTTP_POST_METHOD) == FWL_ZERO) ||
        (STRCMP (ai1Method, gaHTTP_DELETE_METHOD) == FWL_ZERO))
    {
        u4Len++;                /* Increment the char position count. */
        u4UrlStartOffset = u4HttpPktOffset + u4Len;
        for (u4UrlLength = FWL_ZERO; u4Len < u4HttpPktLen; u4Len++)
        {
            /* Get a char from the Http-Pkt */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1Char,
                                       u4HttpPktOffset + u4Len, sizeof (INT1));
            /* Check if it is Space char */
            if (i1Char == ' ')
            {
                break;
            }
            u4UrlLength++;
        }                        /*End-of-For-Loop */
    }
    if (u4Status == FWL_SUCCESS)
    {
        u4UrlLength += u4HostLen;
    }

    if (!(FWL_ZERO == u4UrlLength))
    {
        /* Allocate memory. */
        *ppi1UrlInfo = MemAllocMemBlk (FWL_URL_LENGTH_PID);
        if (*ppi1UrlInfo == NULL)
        {
            return FWL_FAILURE;
        }
        /* Copy the Host Name string from pBuf */
        if ((u4HostLen != FWL_ZERO) && (u4HostStartOffset != FWL_ZERO))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) *ppi1UrlInfo,
                                       u4HostStartOffset, u4HostLen);
        }
        /* Append the UrlString from pBuf. */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) *ppi1UrlInfo + u4HostLen,
                                   u4UrlStartOffset, (u4UrlLength - u4HostLen));
        (*ppi1UrlInfo)[u4UrlLength] = '\0';
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }
}                                /*End-of-Function */

/******************************************************************************
*  Function Name    : FwlGetHttpHostField
*  Description        : This functions does the following,
*                       1. Checks for the host field
*                       2. If host field is present then returns the offset
*                          and the length
*
*  Input(s)        : pBuf - Pointer to the Http packet.
*  Output(s)        : None
*  Returns        : FWL_SUCCESS or FWL_FAILURE
*******************************************************************************/

UINT4
FwlGetHttpHostField (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4HttpPktOffset,
                     UINT4 u4HttpPktLen, UINT4 *pu4BeginOffset,
                     UINT4 *pu4HostFieldLen)
{

    INT1                i1ScanChar = FWL_ZERO;
    INT1                i1HeaderEndScanChar = FWL_ZERO;
    INT1                i1NextScanChar = FWL_ZERO;
    INT1                i1HeaderEndNextScanChar = FWL_ZERO;
    UINT4               u4Index = FWL_ZERO;
    UINT4               u4CurrentOffset = FWL_ZERO;
    UINT4               u4PktSize = FWL_ZERO;

    u4Index = FWL_ZERO;
    UNUSED_PARAM (u4Index);
    *pu4BeginOffset = FWL_ZERO;
    *pu4HostFieldLen = FWL_ZERO;

    u4CurrentOffset = u4HttpPktOffset;

    /* Get byte by byte and check whether we have the "Host" field present
     * in the HTTP header, if it is there then the offset and length is 
     * extracted.
     */
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar, u4CurrentOffset,
                               FWL_ONE_BYTE);
    u4CurrentOffset++;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                               u4CurrentOffset, FWL_ONE_BYTE);
    u4CurrentOffset++;
    while (u4CurrentOffset <= u4PktSize)
    {

        /* check for the end of any fields present in http header. If it is
         * the end of any field then the next few four bytes are scanned for
         * 'host' field.
         */

        if ((i1ScanChar == FWL_OFFSET_0d) && (i1NextScanChar == FWL_OFFSET_0a))
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar,
                                       u4CurrentOffset, FWL_ONE_BYTE);
            u4CurrentOffset++;
            if ((i1ScanChar == 'H') || (i1ScanChar == FWL_OFFSET_0d))
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                                           u4CurrentOffset, FWL_ONE_BYTE);
                u4CurrentOffset++;
                if ((i1NextScanChar == 'o')
                    || (i1NextScanChar == FWL_OFFSET_0a))
                {
                    if ((i1ScanChar == FWL_OFFSET_0d) &&
                        (i1NextScanChar == FWL_OFFSET_0a))
                    {
                        break;
                    }
                    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1ScanChar,
                                               u4CurrentOffset, FWL_ONE_BYTE);
                    u4CurrentOffset++;
                    if (i1ScanChar == 's')
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *) &i1NextScanChar,
                                                   u4CurrentOffset,
                                                   FWL_ONE_BYTE);
                        u4CurrentOffset++;
                        if (i1NextScanChar == 't')
                        {
                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &i1ScanChar,
                                                       u4CurrentOffset,
                                                       FWL_ONE_BYTE);
                            if (i1ScanChar == ':')
                            {
                                u4CurrentOffset += FWL_TWO;    /* This
                                                             * is to bypass the space
                                                             * following the ':' */
                                *pu4BeginOffset = u4CurrentOffset;
                                for (;
                                     u4CurrentOffset <
                                     (u4HttpPktLen + u4HttpPktOffset);
                                     u4CurrentOffset++)
                                {
                                    /* Get a char from the Http-Pkt */
                                    CRU_BUF_Copy_FromBufChain (pBuf,
                                                               (UINT1 *)
                                                               &i1ScanChar,
                                                               u4CurrentOffset,
                                                               sizeof (INT1));
                                    /* Check if it the end of the host field */
                                    if (i1ScanChar == FWL_OFFSET_0d)
                                    {
                                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                                   (UINT1 *)
                                                                   &i1NextScanChar,
                                                                   u4CurrentOffset
                                                                   + FWL_ONE,
                                                                   sizeof
                                                                   (INT1));
                                        if (i1NextScanChar == FWL_OFFSET_0a)
                                        {
                                            return FWL_SUCCESS;
                                        }
                                    }
                                    *pu4HostFieldLen = *pu4HostFieldLen +
                                        FWL_ONE;
                                }    /*End-of-For-Loop */
                            }
                        }
                    }
                }
            }

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1HeaderEndScanChar,
                                       u4CurrentOffset, FWL_ONE_BYTE);
            u4CurrentOffset++;
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &i1HeaderEndNextScanChar,
                                       u4CurrentOffset, FWL_ONE_BYTE);
            u4CurrentOffset++;
            if ((i1HeaderEndScanChar == FWL_OFFSET_0d) &&
                (i1HeaderEndNextScanChar == FWL_OFFSET_0a))
            {
                break;
            }
            else
            {
                i1ScanChar = i1HeaderEndScanChar;
                i1NextScanChar = i1HeaderEndNextScanChar;
            }
        }
        else
        {
            i1ScanChar = i1NextScanChar;
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &i1NextScanChar,
                                       u4CurrentOffset, FWL_ONE_BYTE);
            u4CurrentOffset++;
        }

    }
    *pu4BeginOffset = FWL_ZERO;
    *pu4HostFieldLen = FWL_ZERO;
    return FWL_FAILURE;
}

/****************************************************************************/
/*                 End of the file -- fwlurl.c                             */
/****************************************************************************/
