/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltftp.c,v 1.2 2013/10/31 11:13:30 siva Exp $
 *
 * Description: File containing routines related to TFTP ALG 
 * configuration of firewall module
 * *******************************************************************/
#ifndef __FWLTFTP_C__
#define __FWLTFTP_C__

#include "fwlinc.h"

/***************************************************************************
* Function Name  :  TftpAlgProcessing
* Description    :  This function process the partial links by scanning and if
*                   no entry exists, adds newly. TFTP partial links are added
*                   to allow the reply traffic from server.
*                   i.e., from any port to specific
* traffic originated port. 
                    scan the partial link list.                        
*
* Input (s)    :  1. pStateIpHdr -  Pointer to the Ip header
                  2. pStateHLInfo - Pointer to the Transport Header     
                  3. u1Direction -  Specifies IN or OUT
*
* Output (s)   :  None
* Returns      :  FWL_SUCCESS or FWL_FAILURE. 
*
****************************************************************************/

PUBLIC UINT4
TftpAlgProcessing (tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo,
                   UINT1 u1Direction)
{
    tPartialInfo        sPartialInfo;
    UINT4               u4Status = FWL_FAILURE;
    tStatefulSessionNode *pPartialLinkNode = NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the function TftpAlgProcessing \n");

    if ((pStateIpHdr == NULL) || (pStateHLInfo == NULL))
    {
        return u4Status;
    }

    MEMSET (&sPartialInfo, FWL_ZERO, sizeof (tPartialInfo));

    /* search in the partial links list whether the entry already exists */
    u4Status =
        FwlMatchPartialLinksNode (pStateIpHdr, pStateHLInfo, u1Direction,
                                  &pPartialLinkNode);

    if (u1Direction == FWL_DIRECTION_IN)
    {
        /* If Entry already exist do not call partiallinks to add.  */
        if (u4Status != FWL_SUCCESS)
        {
            /* change the direction while adding partial links, so that
             * reply/reverse traffic can match */
            sPartialInfo.u1Direction = FWL_DIRECTION_OUT;
            FWL_IP_ADDR_COPY (&sPartialInfo.LocalIP, &pStateIpHdr->DestAddr);
            FWL_IP_ADDR_COPY (&sPartialInfo.RemoteIP, &pStateIpHdr->SrcAddr);
            /* local port is set with 0 for allowing any port */
            sPartialInfo.u2LocalPort = FWL_ZERO;
            sPartialInfo.u2RemotePort = pStateHLInfo->u2SrcPort;
            sPartialInfo.u1Protocol = pStateIpHdr->u1Proto;
            sPartialInfo.u1PersistFlag = FWL_NON_PERSISTENT;
            FwlAddPartialLink (&sPartialInfo);
            u4Status = FWL_SUCCESS;

        }
    }
    else
    {
        /* If Entry already exists do not call partiallinks to add.  */
        if (u4Status != FWL_SUCCESS)
        {
            /* change the direction while adding partial links, so that
             * reply/reverse traffic can match */
            sPartialInfo.u1Direction = FWL_DIRECTION_IN;
            FWL_IP_ADDR_COPY (&sPartialInfo.LocalIP, &pStateIpHdr->SrcAddr);
            FWL_IP_ADDR_COPY (&sPartialInfo.RemoteIP, &pStateIpHdr->DestAddr);
            sPartialInfo.u2LocalPort = pStateHLInfo->u2SrcPort;
            /* remote port is set with 0 for allowing any port */
            sPartialInfo.u2RemotePort = FWL_ZERO;
            sPartialInfo.u1Protocol = pStateIpHdr->u1Proto;
            sPartialInfo.u1PersistFlag = FWL_NON_PERSISTENT;
            FwlAddPartialLink (&sPartialInfo);
            u4Status = FWL_SUCCESS;
        }
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the function TftpAlgProcessing \n");
    return u4Status;
}

#endif /* __FWLTFTP_C__ */
