/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlutil.c,v 1.26 2013/10/31 11:13:30 siva Exp $
 *
 * Description:This file contains routines for coding     
 *             and decoding the strings.              
 *
 *******************************************************************/
#include "fwlinc.h"
#ifdef NPAPI_WANTED
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif
#endif
unFwlUtilCallBackEntry FWL_UTIL_CALL_BACK[FWL_CUST_MAX_CALL_BACK_EVENTS];

/****************************************************************************/
/*                          PROTOTYPE DECLARATION                           */
/****************************************************************************/

PRIVATE INT1        FwlValidateRuleCombination
PROTO ((tFilterInfo * pFilterNode, tRuleInfo * pRuleNode));
PRIVATE INT1 FwlCheckFilterCombination PROTO ((UINT2 u2Filter1_MinValue,
                                               UINT2 u2Filter1_MaxValue,
                                               UINT2 u2Filter2_MinValue,
                                               UINT2 u2Filter2_MaxValue,
                                               UINT2 *pu2MinValue,
                                               UINT2 *pu2MaxValue));
PRIVATE UINT4 FwlUtilIsIpAddrPartofRange PROTO ((UINT4 u4IpAddress,
                                                 UINT4 u4IpStartAddr,
                                                 UINT4 u4IpEndAddr));

/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlConcatAddrAndMask                             */
/*                                                                          */
/*    Description        : Gets the address and mask and returns it in      */
/*                         required notation (a.b.c.d / x).                 */
/*                                                                          */
/*    Input(s)           : u4Addr       --  IP Address                      */
/*                         u4Mask       --  IP Address Mask                 */
/*                                                                          */
/*    Output(s)          : pu1Addr      -- Pointer string in required       */
/*                                         notation.                        */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlConcatAddrAndMask (UINT4 u4Addr, UINT4 u4Mask, UINT1 *pu1Addr)
#else
PUBLIC VOID
FwlConcatAddrAndMask (u4Addr, u4Mask, pu1Addr)
     UINT4               u4Addr;
     UINT4               u4Mask;
     UINT1              *pu1Addr;
#endif
{
    UINT1               u1StringIndex = FWL_ZERO;
    UINT1               u1Index = FWL_ZERO;
    UINT4               u4ByteAddr = FWL_ZERO;
    UINT4               u4No_Shift = FWL_ZERO;
    UINT4               u4Mask1 = FWL_ZERO;
    UINT1               u1DivFactor = FWL_ZERO;
    INT1                i1TmpChar = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlConcatAdrAndMask\n");

    u4No_Shift = FWL_SHIFT_VALUE;
    u4Mask1 = FWL_MASK_VALUE;
    u4ByteAddr = FWL_ZERO;
    u1StringIndex = FWL_ZERO;
    u1DivFactor = FWL_ZERO;

    /* This loop is executed 4 times since the sizeof IP address is 4 bytes. */
    for (u1Index = FWL_ZERO; u1Index < sizeof (UINT4); u1Index++)
    {
        /* Get the length of the each 8 bit of the IP address. The IP address
         * length of each 8 bit(in decimal form) varies from 0 to 3. So to
         * calculate the length the u1DivFactor is used. If the length is 3 it
         * is 100, else if length is  2 then iu1DivFactor is 10 else u1DivFactor
         * is 1. To find the length, each 8 bit value is dived by 100 or 10.
         */
        u4ByteAddr = ((u4Addr & u4Mask1) >> u4No_Shift);
        if ((u4ByteAddr / FWL_DIV_FACTOR_100) != FWL_ZERO)
        {
            u1DivFactor = FWL_DIV_FACTOR_100;
        }
        else if ((u4ByteAddr / FWL_DIV_FACTOR_10) != FWL_ZERO)
        {
            u1DivFactor = FWL_DIV_FACTOR_10;
        }
        else
        {
            u1DivFactor = FWL_ONE;
        }
        /* Extract the address value and store it in dotted notation. This
         * is done for the 24 bits of the IP Address.
         */
        while (u1DivFactor != FWL_ZERO)
        {
            i1TmpChar = (INT1) ((u4ByteAddr / u1DivFactor) + FWL_ASCII_VALUE);
            *(pu1Addr + u1StringIndex) = (UINT1) i1TmpChar;
            u1StringIndex++;
            u4ByteAddr = u4ByteAddr % u1DivFactor;
            u1DivFactor /= ((UINT1) FWL_DIV_FACTOR_10);
        }                        /* end of while */

        /* extract the last 8 bit of the IP address */
        *(pu1Addr + u1StringIndex) = '.';
        u1StringIndex++;
        u4No_Shift = u4No_Shift - FWL_REMAINING_SHIFT_VALUE;
        u4Mask1 = u4Mask1 >> FWL_REMAINING_SHIFT_VALUE;
    }                            /* end of for */

    u1StringIndex--;
    *(pu1Addr + u1StringIndex) = '/';
    u1StringIndex++;
    u4ByteAddr = u4Mask;
    u1Index = FWL_ZERO;

    /* get the mask value . It is got by counting the number of 1's in the
     * Mask variable.
     */
    while (u4ByteAddr != FWL_ZERO)
    {
        if ((u4ByteAddr & FWL_MASK_1_BIT) != FWL_ZERO)
        {
            u1Index++;
        }
        u4ByteAddr = (u4ByteAddr >> FWL_ONE);
    }                            /* end of while */

    /* concat the mask value with the address  and the required notaion
     * is got.
     */
    *(pu1Addr + u1StringIndex) =
        (UINT1) ((u1Index / FWL_DIV_FACTOR_10) + FWL_ASCII_VALUE);
    u1StringIndex++;
    *(pu1Addr + u1StringIndex) =
        (UINT1) ((u1Index % FWL_DIV_FACTOR_10) + FWL_ASCII_VALUE);
    u1StringIndex++;
    *(pu1Addr + u1StringIndex) = '\0';

    /* FWL_DBG1(FWL_DBG_CONFIG, "\n Address and MAsk value = %s \n", pu1Addr); */

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting of the fn. FwlConcatAdrAndMask\n");

}                                /* End of function -- FwlConcatAddrAndmask */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlParseMinAndMaxPort                            */
/*                                                                          */
/*    Description        : Parses the port into Min and Max Port Value      */
/*                                                                          */
/*    Input(s)           : au1Port  -- Port value with operators            */
/*                                                                          */
/*    Output(s)          : pu2MaxPort  -- Max Port value                    */
/*                         pu2MinPort  -- Min port value                    */
/*                                                                          */
/*    Returns            : SUCCESS if Values is valid, otherwise FAILURE    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlParseMinAndMaxPort (UINT1 au1Port[FWL_MAX_PORT_LEN],
                       UINT2 *pu2MaxPort, UINT2 *pu2MinPort)
#else
PUBLIC INT1
FwlParseMinAndMaxPort (au1Port, pu2MaxPort, pu2MinPort)
     UINT1               au1Port[FWL_MAX_PORT_LEN];
     UINT2              *pu2MaxPort;
     UINT2              *pu2MinPort;
#endif
{
    UINT2               u2PortLen = FWL_ZERO;
    INT1                i1Status = FWL_ZERO;
    UINT1               au1Operator[FWL_MAX_OPERATOR_LEN] = { FWL_ZERO };
    UINT2               u2Index1 = FWL_ZERO;
    UINT2               u2Index2 = FWL_ZERO;
    UINT4               u4Num = FWL_ZERO;
    UINT4               u4Min = FWL_ZERO;
    INT1                i1NonDigitCnt = FWL_ZERO;

    /* Initialise local variables */
    i1NonDigitCnt = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlParseMinAndMaxPort\n");

    u2Index1 = FWL_ZERO;
    u2Index2 = FWL_ZERO;
    u4Num = FWL_ZERO;
    u4Min = FWL_ZERO;
    i1Status = SNMP_SUCCESS;

    u2PortLen = (UINT2) FWL_STRLEN ((INT1 *) au1Port);

    /* Validating w.r.t. string length */
    if ((u2PortLen < FWL_MIN_PORT_LEN) || (u2PortLen > FWL_MAX_PORT_LEN))
    {
        return SNMP_FAILURE;
    }
    au1Operator[u2Index1] = au1Port[u2Index1];
    u2Index1++;
    au1Operator[u2Index1] = FWL_END_OF_STRING;

    /* This function gets an input string as <operator> <Number> . It should be
     * parsed to get the Max and Min Value based on the operator.
     */
    if (((au1Port[u2Index1] - FWL_ASCII_VALUE) < FWL_ZERO) ||
        ((au1Port[u2Index1] - FWL_ASCII_VALUE) > FWL_CHECK_VAL_9))
    {
        /* extract the operators >, <, =. */
        au1Operator[u2Index1] = au1Port[u2Index1];
        u2Index1++;
        au1Operator[u2Index1] = FWL_END_OF_STRING;
    }

    FWL_DBG1 (FWL_DBG_CONFIG, "\n Operator Value = %s\n", au1Operator);

    /* then extract the operators <=, >= , != */
    u2Index1 = (UINT2) FWL_STRLEN ((INT1 *) au1Operator);
    /* Get the Port value one by one. Convert the string into an integer and
     * store it in u4Num.  */
    for (u2Index2 = u2Index1; u2Index2 < u2PortLen; u2Index2++)
    {
        if (((au1Port[u2Index2] - FWL_ASCII_VALUE) >= FWL_ZERO) &&
            ((au1Port[u2Index2] - FWL_ASCII_VALUE) <= FWL_CHECK_VAL_9))
        {
            /* No. of digits check. Max no. of digits = 5. */
            if (u2Index2 >= (FWL_FIVE + u2Index1))
            {
                return SNMP_FAILURE;
            }
            else
            {
                u4Num =
                    (u4Num * FWL_DIV_FACTOR_10) + (UINT4) (au1Port[u2Index2] -
                                                           FWL_ASCII_VALUE);
            }
        }
        else
        {
            if ((au1Port[u2Index1] == '-') &&
                (i1NonDigitCnt == FWL_ZERO) && (u2Index2))
            {
                /* Validation of Port Number */
                if ((u4Num > FWL_MAX_PORT_VALUE)
                    || (u4Num < FWL_MIN_PORT_VALUE))
                {
                    return SNMP_FAILURE;
                }
                else            /* First Number is valid */
                {
                    i1NonDigitCnt++;
                    /* Copy the First number into Min */
                    u4Min = u4Num;
                    /* Init Digit counter and 'u4Num' for Second number */
                    u2Index2 = FWL_ZERO;
                    u4Num = FWL_ZERO;
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }                        /*End-of-main-Else */
    }                            /* end of for loop */

    /* Validation of Port Number */
    if ((u4Num > FWL_MAX_PORT_VALUE) || (u4Num < FWL_MIN_PORT_VALUE))
    {
        i1Status = SNMP_FAILURE;
        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                 "\nInvalid Value [out of valid range\n");
        return SNMP_FAILURE;
    }
    else                        /* Number valid */
    {
        if (!(FWL_ZERO == i1NonDigitCnt))
        {
            if (u4Min > u4Num)
            {
                return SNMP_FAILURE;    /* Invalid (Min > Max) */
            }

            if (u4Min == u4Num)
            {
                SPRINTF ((CHR1 *) au1Port, "%u", u4Min);
            }

            /* Update the Output params */
            *pu2MinPort = (UINT2) u4Min;
            *pu2MaxPort = (UINT2) u4Num;
        }
        else
        {
            (*pu2MinPort) = (*pu2MaxPort) = (UINT2) u4Num;
        }
        u2Index1 = FWL_ZERO;
        u2Index2 = (UINT2) (u2Index1 + FWL_ONE);

        /* based on the operator fill in the values of MAX and MIN port. if 
         * parsing can be done then return SUCCESS, else FAILURE. 
         */
        switch (au1Operator[u2Index1])
        {
            case '>':
                if (au1Operator[u2Index2] == '=')
                {
                    *pu2MinPort = (UINT2) u4Num;
                }
                else if (au1Operator[u2Index2] == '\0')
                {
                    /* Check if invalid MAX PORT (>65535) is given as input.If 
                     * so return error, because >65535 is an invalid port 
                     * number */
                    if (u4Num == FWL_MAX_PORT_VALUE)
                    {
                        return SNMP_FAILURE;

                    }
                    *pu2MinPort = (UINT2) (u4Num + FWL_ONE);
                }
                else
                {
                    return SNMP_FAILURE;
                }
                *pu2MaxPort = FWL_MAX_PORT;
                break;
            case '<':
                if (au1Operator[u2Index2] == '=')
                {
                    *pu2MaxPort = (UINT2) (u4Num);
                }
                else if (au1Operator[u2Index2] == '\0')
                {
                    /* Check if invalid MIN PORT (<1) is given as input.
                     * If so return error, because <1 is an invalid port 
                     * number */
                    if (u4Num == FWL_MIN_PORT_VALUE)
                    {
                        return SNMP_FAILURE;

                    }
                    *pu2MaxPort = (UINT2) (u4Num - FWL_ONE);
                }
                else
                {
                    return SNMP_FAILURE;
                }
                *pu2MinPort = FWL_MIN_PORT;
                break;
            case '!':
                *pu2MinPort = (UINT2) (u4Num - FWL_ONE);
                *pu2MaxPort = (UINT2) (u4Num + FWL_ONE);
                break;
            case '=':
                if (au1Operator[u2Index2] == '\0')
                {
                    *pu2MinPort = (UINT2) u4Num;
                    *pu2MaxPort = (UINT2) u4Num;
                }
                else
                {
                    return SNMP_FAILURE;
                }
                break;
            default:
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nInvalid Operator. "
                         "The operators allowed are <, >, =, !=. \n");
                return (SNMP_FAILURE);
                break;
        }                        /* end of Switch */
    }                            /*End-of-Else-Block */

    FWL_DBG1 (FWL_DBG_CONFIG, "\n MAX PORT = %d\n", *pu2MaxPort);
    FWL_DBG1 (FWL_DBG_CONFIG, "\n MIN PORT = %d\n", *pu2MinPort);

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting out of the fn. FwlParseMinAndMaxPort\n");

    return (SNMP_SUCCESS);
    UNUSED_PARAM (i1Status);
}                                /* end of function -- FwlParseMinAndMaxPort */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlParseFilterSet                                */
/*                                                                          */
/*    Description        : Parses the Filter Set and gives the list of      */
/*                         Filters                                          */
/*                                                                          */
/*    Input(s)           : au1FilterSet -- String That has the List of      */
/*                                          Filters                         */
/*                                                                          */
/*    Output(s)          : pRuleNode     -- Pointer to Rule Node            */
/*                                                                          */
/*    Returns            : SUCCESS if Parsing is done , otherwise FAILURE   */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlParseFilterSet (UINT1 au1FilterSet[FWL_MAX_FILTER_SET_LEN],
                   tRuleInfo * pRuleNode)
#else
PUBLIC INT1
FwlParseFilterSet (au1FilterSet, pRuleNode)
     UINT1               au1FilterSet[FWL_MAX_FILTER_SET_LEN];
     tRuleInfo          *pRuleNode;
#endif
{
    INT1                i1Status = FWL_ZERO;
    INT1                i1SrcStatus = FWL_ZERO;
    INT1                i1DestStatus = FWL_ZERO;
    UINT2               u2Index1 = FWL_ZERO;
    UINT2               u2Index2 = FWL_ZERO;
    UINT2               u2Index3 = FWL_ZERO;
    UINT2               u2CounterAnd = FWL_ZERO;
    UINT2               u2CounterOr = FWL_ZERO;
    UINT2               u2FilterSetLen = FWL_ZERO;
    UINT2               u2MinSrcPort = FWL_ZERO;
    UINT2               u2MaxSrcPort = FWL_ZERO;
    UINT2               u2MinDestPort = FWL_ZERO;
    UINT2               u2MaxDestPort = FWL_ZERO;
    UINT1              
        au1FilterName[FWL_MAX_FILTERS_IN_RULE][FWL_MAX_FILTER_NAME_LEN];

    UINT4               u4Count = FWL_ZERO;
    tFilterInfo        *pFilterNode = NULL;
    tFilterInfo        *pFilterNode1 = NULL;
    tFilterInfo        *pFilterNode2 = NULL;
    tFilterInfo        *pTempFilter = NULL;
    tFilterInfo        *pNextTempFilter = NULL;

    u2Index1 = FWL_ZERO;
    u2Index2 = FWL_ZERO;
    u2Index3 = FWL_ZERO;
    u2FilterSetLen = FWL_ZERO;
    i1Status = SNMP_SUCCESS;
    u2CounterOr = FWL_DEFAULT_COUNT;
    u2CounterAnd = FWL_DEFAULT_COUNT;
    pFilterNode = (tFilterInfo *) NULL;
    pFilterNode1 = (tFilterInfo *) NULL;
    pFilterNode2 = (tFilterInfo *) NULL;
    pTempFilter = (tFilterInfo *) NULL;
    pNextTempFilter = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlParseFilterSet  \n");

    u2FilterSetLen = (UINT2) (FWL_STRLEN ((INT1 *) au1FilterSet));

    /* first the Filter set string is extracted into a list of filter names
     * and the combining condition ( & and ,) is also extracted.
     */

    while ((u2Index1 < u2FilterSetLen) &&
           (u2Index2 < FWL_MAX_FILTER_NAME_LEN) &&
           (u2Index3 < FWL_MAX_FILTERS_IN_RULE))
    {
        u2Index2 = FWL_ZERO;
        /* CHANGE1 : The loop conditon was wrong. It didnt correctly check the
         * operators and didnt parse the Filter Name from the Filter Set.
         */
        while ((u2Index1 < u2FilterSetLen) &&
               (u2Index2 < FWL_MAX_FILTER_NAME_LEN - FWL_ONE) &&
               (au1FilterSet[u2Index1] != '&') &&
               (au1FilterSet[u2Index1] != ','))
        {
            au1FilterName[u2Index3][u2Index2] = au1FilterSet[u2Index1];
            u2Index2++;
            u2Index1++;
        }                        /* end of while */

        au1FilterName[u2Index3][u2Index2] = FWL_END_OF_STRING;
        if (au1FilterSet[u2Index1] == '&')
        {
            u2CounterAnd++;
        }
        else if (au1FilterSet[u2Index1] == ',')
        {
            u2CounterOr++;
        }
        else
        {
            i1Status = SNMP_SUCCESS;
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n The operators allowed are '&' and ','\n");

        }
        u2Index3++;
        u2Index1++;
    }                            /* end of while */
    if (u2Index1 < u2FilterSetLen)
    {
        CLI_SET_ERR (CLI_FWL_MAX_FILTER);
        i1Status = SNMP_FAILURE;
    }
    else
    {                            /* Parsing of filterset is complete */
        FWL_DBG1 (FWL_DBG_CONFIG, "\n AND Operator = %d\n", u2CounterAnd);
        FWL_DBG1 (FWL_DBG_CONFIG, "\n OR Operator  = %d\n", u2CounterOr);

        /* if the operator other than & and , are used, return FAILURE */
        if ((u2CounterAnd != FWL_DEFAULT_COUNT) &&
            (u2CounterOr != FWL_DEFAULT_COUNT))
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Both operators are not allowed to combine "
                     "in the same filter set \n");
            CLI_SET_ERR (CLI_FWL_INVALID_OPERATOR);
            i1Status = SNMP_FAILURE;
        }
        else
        {                        /* No & and , operator are combined */
            /* Based on the operator the condition flag is updated. */
            if (u2CounterAnd != FWL_DEFAULT_COUNT)
            {
                pRuleNode->u1FilterConditionFlag = FWL_FILTER_AND;
            }
            else
            {
                pRuleNode->u1FilterConditionFlag = FWL_FILTER_OR;
            }
            /* store the list of pointers corresponding to the filter name */
            for (u2Index1 = FWL_ZERO; u2Index1 < u2Index3; u2Index1++)
            {
                pRuleNode->apFilterInfo[u2Index1] =
                    FwlDbaseSearchFilter (au1FilterName[u2Index1]);

                FWL_DBG2 (FWL_DBG_CONFIG, "\n Filter Name%d = %s\n",
                          u2Index1, au1FilterName[u2Index1]);

                /* increment the reference count for the coressponding filters
                 * that are applied in the rule table.
                 */
                if (pRuleNode->apFilterInfo[u2Index1] == NULL)
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Filter %s not defined\n",
                                  au1FilterName[u2Index1]);
                    CLI_SET_ERR (CLI_FWL_NO_SUCH_ACL);
                    i1Status = SNMP_FAILURE;
                    break;
                }
                else
                {
                    /* CHANGE2 : The invalid  combination of rules was not 
                     * checked. So this new function was added to check the 
                     * invalid combination of rules.
                     */
                    pFilterNode = pRuleNode->apFilterInfo[u2Index1];
                    if (pFilterNode->u1RowStatus == FWL_ACTIVE)
                    {
                        /*Combination of filters with different address type is
                         * not allowed.Hence check the address type of the 
                         * filter set*/
                        for (u4Count = FWL_ZERO;
                             (((pTempFilter = pRuleNode->apFilterInfo[u4Count])
                               != NULL)
                              && (u4Count < FWL_MAX_FILTERS_IN_RULE -
                                  FWL_ONE)); u4Count++)
                        {
                            pNextTempFilter =
                                pRuleNode->apFilterInfo[u4Count + FWL_ONE];
                            if (pNextTempFilter != NULL)
                            {
                                if (pTempFilter->u2AddrType !=
                                    pNextTempFilter->u2AddrType)
                                {
                                    i1Status = SNMP_FAILURE;
                                }
                            }
                        }
                        if ((pRuleNode->u1FilterConditionFlag == FWL_FILTER_AND)
                            && (u2Index1 == u2Index3 - FWL_ONE))
                        {
                            i1Status = FwlValidateRuleCombination (pRuleNode->
                                                                   apFilterInfo
                                                                   [u2Index1],
                                                                   pRuleNode);
                        }
                        if (i1Status == SNMP_SUCCESS)
                        {
                            /* Removed incrementing the filter 
                             * reference count in the parsing function. Now
                             * this will be done in FwlAddFilterSet() during 
                             * the set routine.
                             */
                        }
                        else
                        {
                            pRuleNode->apFilterInfo[u2Index1] = NULL;
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                     "\n Invalid Filter Combination \n");
                            CLI_SET_ERR (CLI_FWL_INVALID_FILTER_COMBINATION);
                            break;
                        }
                    }
                    else
                    {
                        i1Status = SNMP_FAILURE;
                        pRuleNode->apFilterInfo[u2Index1] = NULL;
                        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n Filter %s is not Active \n",
                                      pFilterNode->au1FilterName);
                        CLI_SET_ERR (CLI_FWL_FILTER_NOT_ACTIVE);
                        break;
                    }
                }
                FWL_DBG1 (FWL_DBG_CONFIG, "\n Filter Reference Count = %d\n",
                          pRuleNode->apFilterInfo[u2Index1]->u1FilterRefCount);
            }                    /* end of for */
            /* As the filter reference count is not incremented in 
             * this function, there is no need to decrement it if the filter set
             * is invalid.
             */

            /* Store the Min and Max Port Ranges if the 
             * Filter Combination is AND Type */
            if (pRuleNode->u1FilterConditionFlag != FWL_FILTER_AND)
            {
                /* Rule validation for Filters OR'd is not needed */
            }
            else
            {
                /* Compare the Filter Combination and get their max and min 
                 * port ranges and iterate thru all the filter sets by comparing 
                 * the min and max port values provided by 
                 * FwlCheckFilterCombination () */
                u2Index1 = FWL_ZERO;
                pRuleNode->apFilterInfo[u2Index1] =
                    FwlDbaseSearchFilter (au1FilterName[u2Index1]);
                pRuleNode->apFilterInfo[u2Index1 + FWL_ONE] =
                    FwlDbaseSearchFilter (au1FilterName[u2Index1 + FWL_ONE]);
                if ((pRuleNode->apFilterInfo[u2Index1] == NULL) ||
                    (pRuleNode->apFilterInfo[u2Index1 + FWL_ONE] == NULL))
                {
                    CLI_SET_ERR (CLI_FWL_NO_SUCH_ACL);
                    i1Status = SNMP_FAILURE;

                }
                else
                {
                    pFilterNode1 = pRuleNode->apFilterInfo[u2Index1];
                    pFilterNode2 = pRuleNode->apFilterInfo[u2Index1 + FWL_ONE];

                    u2Index1 = (UINT2) (u2Index1 + FWL_ONE);
                    if (pFilterNode == NULL)
                    {
                        CLI_SET_ERR (CLI_FWL_NO_SUCH_ACL);
                        return SNMP_FAILURE;
                    }

                    if ((pFilterNode->u2SrcMinPort != FWL_DEFAULT_PORT)
                        && (pFilterNode->u2SrcMaxPort != FWL_DEFAULT_PORT))
                    {
                        i1SrcStatus =
                            FwlCheckFilterCombination (pFilterNode1->
                                                       u2SrcMinPort,
                                                       pFilterNode1->
                                                       u2SrcMaxPort,
                                                       pFilterNode2->
                                                       u2SrcMinPort,
                                                       pFilterNode2->
                                                       u2SrcMaxPort,
                                                       &u2MinSrcPort,
                                                       &u2MaxSrcPort);
                        if (i1SrcStatus == FWL_FAILURE)
                        {
                            CLI_SET_ERR (CLI_FWL_PORT_MISMATCH);
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        u2MinSrcPort = FWL_MIN_PORT_VALUE;
                        u2MaxSrcPort = FWL_MAX_PORT_VALUE;
                    }

                    if ((pFilterNode->u2DestMinPort != FWL_DEFAULT_PORT)
                        && (pFilterNode->u2DestMaxPort != FWL_DEFAULT_PORT))
                    {
                        i1DestStatus =
                            FwlCheckFilterCombination (pFilterNode1->
                                                       u2DestMinPort,
                                                       pFilterNode1->
                                                       u2DestMaxPort,
                                                       pFilterNode2->
                                                       u2DestMinPort,
                                                       pFilterNode2->
                                                       u2DestMaxPort,
                                                       &u2MinDestPort,
                                                       &u2MaxDestPort);
                        if (i1DestStatus == FWL_FAILURE)
                        {
                            CLI_SET_ERR (CLI_FWL_PORT_MISMATCH);
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        u2MinDestPort = FWL_MIN_PORT_VALUE;
                        u2MinDestPort = FWL_MAX_PORT_VALUE;
                    }

                    u2Index1++;
                    /* Scan thru' the other filters and check whether the 
                     * Rule Combination is valid or not */
                    while ((u2Index1 < u2Index3) &&
                           ((pRuleNode->apFilterInfo[u2Index1]) =
                            FwlDbaseSearchFilter (au1FilterName[u2Index1])) !=
                           NULL)
                    {
                        pFilterNode = pRuleNode->apFilterInfo[u2Index1];
                        if ((pFilterNode->u2SrcMinPort != FWL_DEFAULT_PORT)
                            && (pFilterNode->u2SrcMaxPort != FWL_DEFAULT_PORT))
                        {
                            i1SrcStatus =
                                FwlCheckFilterCombination (u2MinSrcPort,
                                                           u2MaxSrcPort,
                                                           pFilterNode->
                                                           u2SrcMinPort,
                                                           pFilterNode->
                                                           u2SrcMaxPort,
                                                           &u2MinSrcPort,
                                                           &u2MaxSrcPort);
                            if (i1SrcStatus == FWL_FAILURE)
                            {
                                CLI_SET_ERR (CLI_FWL_PORT_MISMATCH);
                                i1Status = SNMP_FAILURE;
                                break;
                            }
                        }
                        else
                        {
                            u2MinSrcPort = FWL_MIN_PORT_VALUE;
                            u2MaxSrcPort = FWL_MAX_PORT_VALUE;
                        }
                        if ((pFilterNode->u2DestMinPort != FWL_DEFAULT_PORT)
                            && (pFilterNode->u2DestMaxPort != FWL_DEFAULT_PORT))
                        {
                            i1DestStatus =
                                FwlCheckFilterCombination (u2MinDestPort,
                                                           u2MaxDestPort,
                                                           pFilterNode->
                                                           u2DestMinPort,
                                                           pFilterNode->
                                                           u2DestMaxPort,
                                                           &u2MinDestPort,
                                                           &u2MaxDestPort);
                            if (i1DestStatus == FWL_FAILURE)
                            {
                                CLI_SET_ERR (CLI_FWL_PORT_MISMATCH);
                                i1Status = SNMP_FAILURE;
                                break;
                            }
                        }
                        else
                        {
                            u2MinDestPort = FWL_MIN_PORT_VALUE;
                            u2MaxDestPort = FWL_MAX_PORT_VALUE;
                        }
                        u2Index1++;
                    }
                    pRuleNode->u2MinSrcPort = u2MinSrcPort;
                    pRuleNode->u2MaxSrcPort = u2MaxSrcPort;
                    pRuleNode->u2MinDestPort = u2MinDestPort;
                    pRuleNode->u2MaxDestPort = u2MaxDestPort;
                }
            }
        }                        /* end of else (No & or , operator combined */
    }
    if (i1Status == SNMP_SUCCESS)
    {
        STRCPY ((INT1 *) pRuleNode->au1FilterSet, au1FilterSet);
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting out of the fn. FwlParseFilterSet  \n");
    return (i1Status);
}                                /* End of Function -- FwlParseFilterSet */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckFilterCombination                        */
/*                                                                          */
/*    Description        : Checks whether the port ranges specified in the  */
/*                         Rule's AND'ed together are valid or not          */
/*                                                                          */
/*    Input(s)           : u2Filter1_MinValue - Filter 1's Min Port Number    */
/*                         u2Filter1_MaxValue - Filter 1's Max Port Number    */
/*                         u2Filter2_MinValue - Filter 2's Min Port Number    */
/*                         u2Filter2_MaxValue - Filter 1's Max Port Number    */
/*                                                                          */
/*                                                                          */
/*    Output(s)          : pu2MinValue - Valid Min Port No.                 */
/*                         pu2MaxValue - Valid Max Port No.                 */
/*    Returns            : FWL_SUCCESS if parsing is done else FWL_FAILURE  */
/*                                                                          */
/****************************************************************************/
#ifdef __STDC__
PRIVATE INT1
FwlCheckFilterCombination (UINT2 u2Filter1_MinValue,
                           UINT2 u2Filter1_MaxValue,
                           UINT2 u2Filter2_MinValue,
                           UINT2 u2Filter2_MaxValue,
                           UINT2 *pu2MinValue, UINT2 *pu2MaxValue)
#else
PRIVATE INT1
FwlCheckFilterCombination (u2Filter1_MinValue,
                           u2Filter1_MaxValue,
                           u2Filter2_MinValue,
                           u2Filter2_MaxValue, pu2MinValue, pu2MaxValue)
     UINT2               u2Filter1_MinValue;
     UINT2               u2Filter1_MaxValue;
     UINT2               u2Filter2_MinValue;
     UINT2               u2Filter2_MaxValue;
     UINT2              *pu2MinValue;
     UINT2              *pu2MaxValue;
#endif
{
    if ((u2Filter1_MinValue == FWL_MIN_PORT_VALUE) &&
        (u2Filter2_MinValue == FWL_MIN_PORT_VALUE))
    {
        *pu2MinValue = FWL_MIN_PORT_VALUE;
        if (u2Filter1_MinValue == u2Filter1_MaxValue)
        {
            *pu2MaxValue = u2Filter1_MaxValue;
        }
        else if (u2Filter2_MinValue == u2Filter2_MaxValue)
        {
            *pu2MaxValue = u2Filter2_MaxValue;
        }
        else
        {
            *pu2MaxValue =
                u2Filter1_MaxValue <
                u2Filter2_MaxValue ? u2Filter1_MaxValue : u2Filter2_MaxValue;
        }
        return FWL_SUCCESS;
    }
    if ((u2Filter1_MaxValue == FWL_MAX_PORT_VALUE) &&
        (u2Filter2_MaxValue == FWL_MAX_PORT_VALUE))
    {
        if (u2Filter1_MinValue == u2Filter1_MaxValue)
        {
            *pu2MinValue = u2Filter1_MinValue;
        }
        else if (u2Filter2_MinValue == u2Filter2_MaxValue)
        {
            *pu2MinValue = u2Filter2_MinValue;
        }
        else
        {
            *pu2MinValue =
                u2Filter1_MinValue <
                u2Filter2_MinValue ? u2Filter1_MinValue : u2Filter2_MinValue;
        }
        *pu2MaxValue = FWL_MAX_PORT_VALUE;
        return FWL_SUCCESS;
    }
    if ((u2Filter1_MinValue == u2Filter1_MaxValue) &&
        (u2Filter2_MinValue == u2Filter2_MaxValue))
    {
        if (u2Filter1_MinValue == u2Filter2_MinValue)
        {
            /* Filters with same port range */
            *pu2MinValue = *pu2MaxValue = u2Filter1_MinValue;
            return FWL_SUCCESS;
        }
        else
        {
            /* Filters Combined [Using '='] can't have diff. port ranges */
            return FWL_FAILURE;
        }
    }
    else if ((u2Filter1_MinValue == u2Filter1_MaxValue))
    {
        if ((u2Filter1_MinValue < u2Filter2_MinValue) ||
            (u2Filter1_MinValue > u2Filter2_MaxValue))
        {
            return FWL_FAILURE;
        }
        else
        {
            *pu2MinValue = *pu2MaxValue = u2Filter1_MinValue;
            return FWL_SUCCESS;
        }

    }
    else if ((u2Filter2_MinValue == u2Filter2_MaxValue))
    {
        if ((u2Filter2_MinValue < u2Filter1_MinValue) ||
            (u2Filter2_MinValue > u2Filter1_MaxValue))
        {
            return FWL_FAILURE;
        }
        else
        {
            *pu2MinValue = *pu2MaxValue = u2Filter2_MinValue;
            return FWL_SUCCESS;
        }
    }
    if (u2Filter1_MinValue == u2Filter2_MinValue)
    {
        if (u2Filter1_MaxValue < u2Filter2_MaxValue)
        {
            *pu2MinValue = u2Filter1_MinValue;
            *pu2MaxValue = u2Filter1_MaxValue;
            return FWL_SUCCESS;
        }
        else
        {
            *pu2MinValue = u2Filter2_MinValue;
            *pu2MaxValue = u2Filter2_MaxValue;
            return FWL_SUCCESS;
        }
    }
    if (u2Filter1_MaxValue == u2Filter2_MaxValue)
    {
        if (u2Filter1_MinValue < u2Filter2_MinValue)
        {
            *pu2MinValue = u2Filter1_MinValue;
            *pu2MaxValue = u2Filter1_MaxValue;
            return FWL_SUCCESS;
        }
        else
        {
            *pu2MinValue = u2Filter2_MinValue;
            *pu2MaxValue = u2Filter2_MaxValue;
            return FWL_SUCCESS;
        }
    }
    if (((u2Filter1_MinValue < u2Filter2_MinValue) &&
         (u2Filter1_MaxValue < u2Filter2_MaxValue)) ||
        ((u2Filter1_MinValue > u2Filter2_MinValue) &&
         (u2Filter1_MaxValue > u2Filter2_MaxValue)))
    {
        if (u2Filter1_MinValue < u2Filter2_MinValue)
        {
            if (u2Filter1_MaxValue - u2Filter2_MinValue < FWL_ZERO)
            {
                return FWL_FAILURE;
            }
            else
            {
                *pu2MinValue = u2Filter2_MinValue;
                *pu2MaxValue = u2Filter1_MaxValue;
                return FWL_SUCCESS;
            }
        }
        else
        {
            if (u2Filter2_MaxValue - u2Filter1_MinValue < FWL_ZERO)
            {
                return FWL_FAILURE;
            }
            else
            {
                *pu2MinValue = u2Filter1_MinValue;
                *pu2MaxValue = u2Filter2_MaxValue;
                return FWL_SUCCESS;
            }
        }
    }
    else if ((u2Filter1_MinValue > u2Filter2_MinValue) &&
             (u2Filter1_MaxValue < u2Filter2_MaxValue))
    {
        *pu2MinValue = u2Filter1_MinValue;
        *pu2MaxValue = u2Filter1_MaxValue;
        return FWL_SUCCESS;
    }
    else if ((u2Filter1_MinValue < u2Filter2_MinValue) &&
             (u2Filter1_MaxValue > u2Filter2_MaxValue))
    {
        *pu2MinValue = u2Filter2_MinValue;
        *pu2MaxValue = u2Filter2_MaxValue;
        return FWL_SUCCESS;
    }

    /* Invalid Port Range Combination other than the above ends up here */
    return FWL_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlParseIpAddr                                   */
/*                                                                          */
/*    Description        : Gets the string containing address and separates */
/*                         them as start and end IP.                        */
/*                                                                          */
/*    Input(s)           : au1Addr             -- Start and End addr        */
/*                                               -- or Addr and Mask          */
/*                                                                          */
/*    Output(s)          : pu4StartAddr  -- address seperated from string   */
/*                         pu4EndAddr    -- address seperated from string   */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if parsing is done else FWL_FAILURE  */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlParseIpAddr (UINT1 au1Addr[FWL_MAX_ADDR_LEN],
                UINT4 *pu4StartAddress, UINT4 *pu4EndAddress)
#else
PUBLIC INT1
FwlParseIpAddr (au1Addr[FWL_MAX_ADDR_LEN], *pu4StartAddress, *pu4EndAddress)
     UINT1               au1Addr[FWL_MAX_ADDR_LEN];
     UINT4              *pu4StartAddress;
     UINT4              *pu4EndAddress;
#endif
{
    UINT1               au1Operator[FWL_MAX_OPERATOR_LEN] = { FWL_ZERO };
    UINT1               au1DummyIpAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    UINT4               u4ByteAddr = FWL_ZERO;
    UINT4               u4Addr = FWL_ZERO;
    UINT4               u4Mask = FWL_ZERO;
    UINT2               u2AddrLen = FWL_ZERO;
    INT1                i1Status = FWL_SUCCESS;
    UINT1               u1Count = FWL_ZERO;
    UINT1               u1StringIndex = FWL_ZERO;
    UINT1               u1MultFactor = FWL_TEN;
    UINT1               u1OperatorIndex = FWL_ZERO;
    UINT1               u1DummyIpIndex = FWL_ZERO;

    MEMSET (au1Operator, FWL_ZERO, sizeof (au1Operator));
    MEMSET (au1DummyIpAddr, FWL_ZERO, sizeof (au1DummyIpAddr));

    *pu4StartAddress = FWL_ZERO;
    *pu4EndAddress = FWL_ZERO;
    u2AddrLen = (UINT2) (STRLEN ((INT1 *) au1Addr));

    UNUSED_PARAM (u2AddrLen);
    /* extract the operators >, <, =, >=, <= */
    while (((((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) < FWL_ZERO) ||
             ((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) > FWL_CHECK_VAL_9))
            && (au1Addr[u1StringIndex] != FWL_END_OF_STRING)) &&
           (u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE))
    {
        if (u1StringIndex < FWL_MAX_OPERATOR_LEN - FWL_ONE)
        {
            au1Operator[u1StringIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            au1Operator[u1StringIndex] = FWL_END_OF_STRING;
        }
    }
    u1OperatorIndex = u1StringIndex;

    /* extract the IP address */
    while (((((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) >= FWL_ZERO) &&
             ((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) <= FWL_CHECK_VAL_9)) ||
            (au1Addr[u1StringIndex] == '.')) &&
           (u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE))
    {
        au1DummyIpAddr[u1DummyIpIndex] = au1Addr[u1StringIndex];
        u1StringIndex++;
        u1DummyIpIndex++;
        au1DummyIpAddr[u1DummyIpIndex] = FWL_END_OF_STRING;
    }

    /* IP address string to UINT4 conversion, big endian notation */

    u4Addr = FWL_INET_ADDR (au1DummyIpAddr);

    MEMSET (au1DummyIpAddr, FWL_ZERO, sizeof (au1DummyIpAddr));
    u1DummyIpIndex = FWL_ZERO;

    if (u4Addr == FWL_BROADCAST_ADDR)
    {
        i1Status = FWL_FAILURE;
    }
    else
    {
        /* Check whether the next character to IP address
         * is mask or operator */

        /* In case of mask */

        if ((au1Addr[u1StringIndex] == '/') &&
            (u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE))
        {
            u1StringIndex++;

            while ((au1Addr[u1StringIndex] != '\0') &&
                   (u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE))
            {
                if ((((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) >= FWL_ZERO) &&
                     ((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) <=
                      FWL_CHECK_VAL_9)))
                {
                    u4ByteAddr = (u4ByteAddr * u1MultFactor) +
                        ((UINT4) (au1Addr[u1StringIndex] - FWL_ASCII_VALUE));
                    u1StringIndex++;
                    u1Count++;
                }
                else
                {
                    return FWL_FAILURE;
                }
            }                    /* end of while */

            /* The MASK Value can be only upto 32 */

            if ((u1Count < FWL_MIN_MASK_VAL)
                && (u4ByteAddr <= FWL_MAX_MASK_VAL))
            {
                for (u1StringIndex = FWL_ZERO; u1StringIndex < u4ByteAddr;
                     u1StringIndex++)
                {
                    u4Mask = ((u4Mask >> FWL_ONE) | FWL_32ND_BIT_SET);
                }

            }
            else
            {
                i1Status = FWL_FAILURE;
            }
        }                        /* end of if au1Addr[u1StringIndex] == '/' */

        /* Incase of Operator */

        /* extract the operators <, <=, and second IP address */

        else if (((au1Addr[u1StringIndex] == '<')
                  || (au1Addr[u1StringIndex] == '>'))
                 && ((u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE)
                     && (u1OperatorIndex < FWL_MAX_OPERATOR_LEN - FWL_ONE)))
        {
            au1Operator[u1OperatorIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            u1OperatorIndex++;
            if ((au1Addr[u1StringIndex] == '=')
                && ((u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE) &&
                    (u1OperatorIndex < FWL_MAX_OPERATOR_LEN - FWL_ONE)))

            {
                au1Operator[u1OperatorIndex] = au1Addr[u1StringIndex];
                u1StringIndex++;
                u1OperatorIndex++;
                au1Operator[u1OperatorIndex] = FWL_END_OF_STRING;
            }
            else
            {
                if (u1OperatorIndex < FWL_MAX_OPERATOR_LEN - FWL_ONE)
                {
                    au1Operator[u1OperatorIndex] = FWL_END_OF_STRING;
                }
            }

            /* extract the second IP address */

            while (((((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) >= FWL_ZERO) &&
                     ((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) <=
                      FWL_CHECK_VAL_9)) ||
                    (au1Addr[u1StringIndex] == '.')) &&
                   (u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE) &&
                   (u1DummyIpIndex < FWL_MAX_ADDR_LEN - FWL_ONE))

            {
                au1DummyIpAddr[u1DummyIpIndex] = au1Addr[u1StringIndex];
                u1StringIndex++;
                u1DummyIpIndex++;
                au1DummyIpAddr[u1DummyIpIndex] = FWL_END_OF_STRING;
            }

            *pu4EndAddress = FWL_INET_ADDR (au1DummyIpAddr);

            if (*pu4EndAddress == FWL_BROADCAST_ADDR)
            {
                i1Status = FWL_FAILURE;
            }
        }                        /* end of else if au1Addr[u1StringIndex] == '<' */
        else
        {
            i1Status = FWL_FAILURE;
        }

    }                            /* end of If pu4Addr != FWL_FAILURE */

    u1OperatorIndex = FWL_ZERO;

    switch (au1Operator[u1OperatorIndex])
    {
        case '>':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '=')
            {
                *pu4StartAddress = u4Addr;
                u1OperatorIndex++;

                /* In case of Range specified */

                if (au1Operator[u1OperatorIndex] == '<')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        *pu4EndAddress = *pu4EndAddress - FWL_ONE;
                    }
                }

                /* In case of mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO) ||
                        (u4ByteAddr == FWL_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        *pu4EndAddress = ((u4Addr & u4Mask) |
                                          (~u4Mask)) - FWL_ONE;
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }
            }
            else
            {
                *pu4StartAddress = (u4Addr) + FWL_ONE;

                /* Incase of Range Specified */

                if (au1Operator[u1OperatorIndex] == '<')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        *pu4EndAddress = *pu4EndAddress - FWL_ONE;
                    }
                }

                /* Incase of Mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO) ||
                        (u4ByteAddr == FWL_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        *pu4EndAddress = ((u4Addr & u4Mask) |
                                          (~u4Mask)) - FWL_ONE;
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }

            }
            break;
        case '<':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '=')
            {
                u1OperatorIndex++;

                /* In case of Range specified */

                if (au1Operator[u1OperatorIndex] == '>')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '=')
                    {
                        /* The pu4EndAddress contains the 
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress;
                        *pu4EndAddress = u4Addr;
                    }
                    else if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        /* The pu4EndAddress contains the 
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress + FWL_ONE;
                        *pu4EndAddress = u4Addr;
                    }
                    else
                    {
                        i1Status = FWL_FAILURE;
                    }
                }

                /* In case of mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO) ||
                        (u4ByteAddr == FWL_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        *pu4StartAddress = (u4Addr & u4Mask) + FWL_ONE;
                        *pu4EndAddress = u4Addr;
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }
            }
            else
            {

                /* Incase of Range Specified */

                if (au1Operator[u1OperatorIndex] == '>')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '=')
                    {
                        /* The pu4EndAddress contains the
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress;
                        *pu4EndAddress = u4Addr - FWL_ONE;
                    }
                    else if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        /* The pu4EndAddress contains the
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress + FWL_ONE;
                        *pu4EndAddress = u4Addr - FWL_ONE;
                    }
                    else
                    {
                        i1Status = FWL_FAILURE;
                    }
                }

                /* Incase of Mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO) ||
                        (u4ByteAddr == FWL_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        *pu4StartAddress = (u4Addr & u4Mask) + FWL_ONE;
                        *pu4EndAddress = u4Addr - FWL_ONE;

                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }

            }
            break;
        case '\0':
            /* Fall through */
        case '=':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '\0')
            {
                /* have same src and dest addr in case of mask 32 */

                if (u4ByteAddr == FWL_MAX_MASK_VAL)
                {
                    *pu4StartAddress = *pu4EndAddress = u4Addr;
                }

                /* in case of 'any' string parsed as '0.0.0.0/0' 
                 * is given, then it means all IP addresses */

                else if ((u4ByteAddr == FWL_ZERO) && (u4Addr == FWL_ZERO))
                {
                    *pu4StartAddress = (u4Addr & u4Mask);
                    *pu4EndAddress = ((u4Addr & u4Mask) | (~u4Mask));
                }
                else
                {
                    *pu4StartAddress = (u4Addr & u4Mask) + FWL_ONE;
                    *pu4EndAddress = ((u4Addr & u4Mask) | (~u4Mask)) - FWL_ONE;
                }
            }
            else
            {
                i1Status = FWL_FAILURE;
            }
            break;
        default:
            i1Status = FWL_FAILURE;
            break;
    }                            /* End of switch au1Operator[u1OperatorIndex] */

    if ((*pu4StartAddress) > (*pu4EndAddress))
    {
        i1Status = FWL_FAILURE;
    }

    if (i1Status == FWL_FAILURE)
    {
        *pu4StartAddress = FWL_ZERO;
        *pu4EndAddress = FWL_ZERO;
    }

    return (i1Status);

}                                /* End of function -- FwlParseIpAddr */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlParseIpv6Addr                                 */
/*                                                                          */
/*    Description        : Gets the string containing address and separates */
/*                         them as start and end IPv6 address.              */
/*                                                                          */
/*    Input(s)           : au1Addr             -- Start and End addr        */
/*                                               -- or Addr and Mask        */
/*                                                                          */
/*    Output(s)          : pu4StartAddr  -- address seperated from string   */
/*                         pu4EndAddr    -- address seperated from string   */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if parsing is done else FWL_FAILURE  */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlParseIpv6Addr (UINT1 au1Addr[FWL_MAX_ADDR_LEN],
                  tIp6Addr * pStartAddress, tIp6Addr * pEndAddress)
#else
PUBLIC INT1
FwlParseIpv6Addr (au1Addr[FWL_MAX_ADDR_LEN], *pStartAddress, *pEndAddress)
     UINT1               au1Addr[FWL_MAX_ADDR_LEN];
     tIp6Addr           *pStartAddress;
     tIp6Addr           *pEndAddress;
#endif
{
    UINT1               au1Operator[FWL_MAX_OPERATOR_LEN] = { FWL_ZERO };
    UINT1               au1DummyIpAddr[FWL_MAX_ADDR_LEN] = { FWL_ZERO };
    UINT4               u4ByteAddr = FWL_ZERO;
    UINT2               u2AddrLen = FWL_ZERO;
    INT1                i1Status = FWL_SUCCESS;
    UINT1               u1Count = FWL_ZERO;
    UINT1               u1StringIndex = FWL_ZERO;
    UINT1               u1MultFactor = FWL_PREFIX_LEN_MULT_FACTOR;
    UINT1               u1OperatorIndex = FWL_ZERO;
    UINT1               u1DummyIpIndex = FWL_ZERO;
    tIp6Addr            Ipv6Address, Mask;
    INT4                i4RetVal = FWL_FAILURE;
    INT4                i4AddrIndex = FWL_ZERO;
    INT4                i4StrCmpVal = FWL_ZERO;

    MEMSET (au1Operator, FWL_ZERO, sizeof (au1Operator));
    MEMSET (au1DummyIpAddr, FWL_ZERO, sizeof (au1DummyIpAddr));
    MEMSET (&Ipv6Address, FWL_ZERO, sizeof (tIp6Addr));
    MEMSET (&Mask, FWL_ZERO, sizeof (tIp6Addr));

    u2AddrLen = (UINT2) (STRLEN ((INT1 *) au1Addr));
    /* extract the operators >, <, =, >=, <= */

    UNUSED_PARAM (u2AddrLen);
    while ((au1Addr[u1StringIndex] != FWL_END_OF_STRING) &&
           (u1StringIndex < (FWL_MAX_OPERATOR_LEN - FWL_ONE)))
    {
        if (((au1Addr[u1StringIndex] == '<') || (au1Addr[u1StringIndex] == '>')
             || ((au1Addr[u1StringIndex] == '='))))
        {
            if ((au1Addr[u1StringIndex] == '>') ||
                (au1Addr[u1StringIndex] == '<') ||
                (au1Addr[u1StringIndex] == '='))
            {
                au1Operator[u1StringIndex] = au1Addr[u1StringIndex];
                u1OperatorIndex++;
                u1StringIndex++;
                au1Operator[u1StringIndex] = FWL_END_OF_STRING;
            }
        }
        else
        {
            u1StringIndex++;
        }
    }

    i4StrCmpVal = FWL_STRCMP (au1Addr, "::/0");

    if (!(FWL_ZERO == u1OperatorIndex))
    {
        u1StringIndex = u1OperatorIndex;
    }
    else
    {
        u1StringIndex = FWL_ZERO;
    }

    /* extract the IPv6 address */
    while ((u1StringIndex < FWL_MAX_ADDR_LEN - FWL_ONE) &&
           (au1Addr[u1StringIndex] != FWL_END_OF_STRING))
    {
        if (((au1Addr[u1StringIndex] >= '0') && (au1Addr[u1StringIndex] <= '9'))
            || ((au1Addr[u1StringIndex] >= 'a')
                && (au1Addr[u1StringIndex] <= 'f'))
            || ((au1Addr[u1StringIndex] >= 'A')
                && (au1Addr[u1StringIndex] <= 'F'))
            || (au1Addr[u1StringIndex] == ':'))
        {
            au1DummyIpAddr[u1DummyIpIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            u1DummyIpIndex++;
            au1DummyIpAddr[u1DummyIpIndex] = FWL_END_OF_STRING;

        }
        else if ((au1Addr[u1StringIndex] == '/') || (au1Addr[u1StringIndex]
                                                     == '<')
                 || (au1Addr[u1StringIndex] == '>'))
        {
            break;
        }
        else
        {
            return CLI_FAILURE;
        }

    }

    /* IP address string to tIp6Addr conversion, big endian notation */
    i4RetVal = INET_ATON6 (au1DummyIpAddr, &Ipv6Address);

    if ((i4StrCmpVal == FWL_ZERO) && (i4RetVal == FWL_ONE))
    {

        /* User has speciifed any address. So return success */
        for (i4AddrIndex = FWL_ZERO; i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX;
             i4AddrIndex++)
        {
            pStartAddress->u4_addr[i4AddrIndex] =
                (Ipv6Address.u4_addr[i4AddrIndex] & Mask.u4_addr[i4AddrIndex]);
        }
        for (i4AddrIndex = FWL_ZERO; i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX;
             i4AddrIndex++)
        {
            pEndAddress->u4_addr[i4AddrIndex] =
                ((Ipv6Address.u4_addr[i4AddrIndex] &
                  Mask.u4_addr[i4AddrIndex]) | (~Mask.u4_addr[i4AddrIndex]));
        }

        return FWL_SUCCESS;
    }
    else if (i4RetVal == FWL_ZERO)
    {
        return FWL_FAILURE;
    }

    MEMSET (au1DummyIpAddr, FWL_ZERO, sizeof (au1DummyIpAddr));
    u1DummyIpIndex = FWL_ZERO;

    /* Check whether the next character to IPv6 address
     * is mask or operator */

    /* In case of mask */

    if ((au1Addr[u1StringIndex] == '/') &&
        (u1StringIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)))
    {
        u1StringIndex++;

        while ((au1Addr[u1StringIndex] != '\0') &&
               (u1StringIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)))
        {
            if ((((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) >= FWL_ZERO) &&
                 ((au1Addr[u1StringIndex] - FWL_ASCII_VALUE) <= FWL_NINE)) &&
                (u1StringIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)))
            {
                u4ByteAddr = ((u4ByteAddr * (UINT4) u1MultFactor) +
                              (UINT4) (au1Addr[u1StringIndex] -
                                       FWL_ASCII_VALUE));
                u1StringIndex++;
                u1Count++;
            }
            else
            {
                return FWL_FAILURE;
            }
        }                        /* end of while */

        /* The MASK Value can be only upto 128 */

        if ((u1Count < FWL_MAX_BYTE_ADDR_INDEX) &&
            (u4ByteAddr <= IP6_ADDR_MAX_PREFIX))
        {
            GET_IPV6_MASK (u4ByteAddr, Mask.u4_addr);
        }
        else
        {
            i1Status = FWL_FAILURE;
        }
    }                            /* end of if au1Addr[u1StringIndex] == '/' */

    /* Incase of Operator */

    /* extract the operators <, <=, and second IP address */

    else if (((au1Addr[u1StringIndex] == '<') ||
              (au1Addr[u1StringIndex] == '>')) &&
             (u1StringIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)) &&
             (u1OperatorIndex < (FWL_MAX_OPERATOR_LEN - FWL_ONE)))
    {
        au1Operator[u1OperatorIndex] = au1Addr[u1StringIndex];
        u1StringIndex++;
        u1OperatorIndex++;
        if ((au1Addr[u1StringIndex] == '=') &&
            (u1StringIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)) &&
            (u1OperatorIndex < (FWL_MAX_OPERATOR_LEN - FWL_ONE)))
        {
            au1Operator[u1OperatorIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            u1OperatorIndex++;
            au1Operator[u1OperatorIndex] = FWL_END_OF_STRING;
        }
        else
        {
            au1Operator[u1OperatorIndex] = FWL_END_OF_STRING;
        }

        /* extract the second IPv6 address */

        while (((ISXDIGIT (au1Addr[u1StringIndex])) ||
                (au1Addr[u1StringIndex] == ':')) &&
               (u1StringIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)) &&
               (u1DummyIpIndex < (FWL_MAX_ADDR_LEN - FWL_ONE)))

        {
            au1DummyIpAddr[u1DummyIpIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            u1DummyIpIndex++;
            au1DummyIpAddr[u1DummyIpIndex] = FWL_END_OF_STRING;
        }
        i4RetVal = INET_ATON6 (au1DummyIpAddr, pEndAddress);

    }                            /* end of else if au1Addr[u1StringIndex] == '<' */
    else
    {
        i1Status = FWL_FAILURE;
    }

    u1OperatorIndex = FWL_ZERO;

    switch (au1Operator[u1OperatorIndex])
    {
        case '>':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '=')
            {
                Ip6AddrCopy (pStartAddress, &Ipv6Address);
                u1OperatorIndex++;

                /* In case of Range specified */

                if (au1Operator[u1OperatorIndex] == '<')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] =
                            (UINT1) (pEndAddress->
                                     u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] -
                                     FWL_ONE);
                    }
                }

                /* In case of mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO)
                        || (u4ByteAddr == IP6_ADDR_MAX_PREFIX))
                    {
                        /* range cannot carry a mask of 0 or 128 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        for (i4AddrIndex = FWL_ZERO;
                             i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX;
                             i4AddrIndex++)
                        {
                            pEndAddress->u4_addr[i4AddrIndex] =
                                ((Ipv6Address.u4_addr[i4AddrIndex] &
                                  Mask.u4_addr[i4AddrIndex]) |
                                 (~Mask.u4_addr[i4AddrIndex]));
                        }
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }
            }
            else
            {
                Ip6AddrCopy (pStartAddress, &Ipv6Address);
                pStartAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] =
                    (UINT1) (pStartAddress->u1_addr[IPVX_IPV6_ADDR_LEN -
                                                    FWL_ONE] + FWL_ONE);

                /* Incase of Range Specified */

                if (au1Operator[u1OperatorIndex] == '<')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] =
                            (UINT1) ((pEndAddress->
                                      u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE])
                                     - FWL_ONE);
                    }
                }

                /* Incase of Mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO)
                        || (u4ByteAddr == IP6_ADDR_MAX_PREFIX))
                    {
                        /* range cannot carry a mask of 0 or 128 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        for (i4AddrIndex = FWL_ZERO;
                             i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX;
                             i4AddrIndex++)
                        {
                            pEndAddress->u4_addr[i4AddrIndex] =
                                ((Ipv6Address.u4_addr[i4AddrIndex] &
                                  Mask.u4_addr[i4AddrIndex]) |
                                 (~Mask.u4_addr[i4AddrIndex]));
                        }
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }

            }
            break;
        case '<':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '=')
            {
                u1OperatorIndex++;

                /* In case of Range specified */

                if (au1Operator[u1OperatorIndex] == '>')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '=')
                    {
                        /* The pu4EndAddress contains the 
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        Ip6AddrCopy (pStartAddress, pEndAddress);
                        Ip6AddrCopy (pEndAddress, &Ipv6Address);
                    }
                    else if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        /* The pu4EndAddress contains the 
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] =
                            (UINT1) (pEndAddress->
                                     u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] +
                                     FWL_ONE);
                        Ip6AddrCopy (pStartAddress, pEndAddress);
                        Ip6AddrCopy (pEndAddress, &Ipv6Address);
                    }
                    else
                    {
                        i1Status = FWL_FAILURE;
                    }
                }

                /* In case of mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO)
                        || (u4ByteAddr == IP6_ADDR_MAX_PREFIX))
                    {
                        /* range cannot carry a mask of 0 or 128 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        for (i4AddrIndex = FWL_ZERO;
                             i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX;
                             i4AddrIndex++)
                        {
                            pStartAddress->u4_addr[i4AddrIndex] =
                                ((Ipv6Address.u4_addr[i4AddrIndex] &
                                  Mask.u4_addr[i4AddrIndex]));
                        }
                        Ip6AddrCopy (pEndAddress, &Ipv6Address);
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }
            }
            else
            {
                /* Incase of Range Specified */

                if (au1Operator[u1OperatorIndex] == '>')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '=')
                    {
                        /* The pu4EndAddress contains the
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        Ip6AddrCopy (pStartAddress, pEndAddress);
                        Ipv6Address.u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] =
                            (UINT1) (Ipv6Address.
                                     u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] -
                                     FWL_ONE);
                        Ip6AddrCopy (pEndAddress, &Ipv6Address);

                    }
                    else if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        /* The pu4EndAddress contains the
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        Ip6AddrCopy (pStartAddress, pEndAddress);
                        pStartAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE]
                            = (UINT1) (pStartAddress->
                                       u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] +
                                       FWL_ONE);

                        Ip6AddrCopy (pEndAddress, &Ipv6Address);
                        {
                            pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE]
                                = (UINT1) (pEndAddress->
                                           u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE]
                                           - FWL_ONE);
                        }

                    }
                    else
                    {
                        i1Status = FWL_FAILURE;
                    }
                }

                /* Incase of Mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == FWL_ZERO)
                        || (u4ByteAddr == IP6_ADDR_MAX_PREFIX))
                    {
                        /* range cannot carry a mask of 0 or 128 */
                        i1Status = FWL_FAILURE;
                    }
                    else
                    {
                        for (i4AddrIndex = FWL_ZERO;
                             i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX;
                             i4AddrIndex++)
                        {
                            pStartAddress->u4_addr[i4AddrIndex] =
                                (Ipv6Address.u4_addr[i4AddrIndex] &
                                 Mask.u4_addr[i4AddrIndex]);
                            pStartAddress->u1_addr[IPVX_IPV6_ADDR_LEN -
                                                   FWL_ONE] =
                                (UINT1) (pStartAddress->
                                         u1_addr[IPVX_IPV6_ADDR_LEN -
                                                 FWL_ONE] + FWL_ONE);
                        }

                        Ip6AddrCopy (pEndAddress, &Ipv6Address);
                        if (pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN -
                                                 FWL_ONE] != FWL_ZERO)
                        {
                            pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN -
                                                 FWL_ONE] =
                                (UINT1) (pEndAddress->
                                         u1_addr[IPVX_IPV6_ADDR_LEN -
                                                 FWL_ONE] - FWL_ONE);
                        }
                    }
                }
                else
                {
                    i1Status = FWL_FAILURE;
                }
            }
            break;
        case '\0':
            /* Fall through */
        case '=':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '\0')
            {
                /* have same src and dest addr in case of mask 128 */

                if (u4ByteAddr == IP6_ADDR_MAX_PREFIX)
                {
                    Ip6AddrCopy (pStartAddress, &Ipv6Address);
                    Ip6AddrCopy (pEndAddress, &Ipv6Address);
                }
                else
                {
                    for (i4AddrIndex = FWL_ZERO;
                         i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX; i4AddrIndex++)
                    {
                        pStartAddress->u4_addr[i4AddrIndex] =
                            (Ipv6Address.u4_addr[i4AddrIndex] &
                             Mask.u4_addr[i4AddrIndex]);
                        pStartAddress->u1_addr[IPVX_IPV6_ADDR_LEN -
                                               FWL_ONE] =
                            (UINT1) (pStartAddress->
                                     u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE] +
                                     FWL_ONE);
                    }

                    for (i4AddrIndex = FWL_ZERO;
                         i4AddrIndex < FWL_MAX_BYTE_ADDR_INDEX; i4AddrIndex++)
                    {
                        pEndAddress->u4_addr[i4AddrIndex] =
                            ((Ipv6Address.u4_addr[i4AddrIndex] &
                              Mask.u4_addr[i4AddrIndex]) |
                             (~Mask.u4_addr[i4AddrIndex]));
                        pEndAddress->u1_addr[FWL_INDEX_15] =
                            (UINT1) ((pEndAddress->u1_addr[FWL_INDEX_15]) -
                                     FWL_ONE);
                    }
                }
            }
            else
            {
                i1Status = FWL_FAILURE;
            }
            break;
        default:
            i1Status = FWL_FAILURE;
            break;
    }                            /* End of switch au1Operator[u1OperatorIndex] */

    if ((pStartAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE]) >
        (pEndAddress->u1_addr[IPVX_IPV6_ADDR_LEN - FWL_ONE]))
    {
        i1Status = FWL_FAILURE;
    }
    if (i1Status == FWL_FAILURE)
    {
        pStartAddress = NULL;
        pEndAddress = NULL;
    }

    return (i1Status);

}                                /* End of function -- FwlParseIpv6Addr */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlValidateRuleCombination                       */
/*                                                                          */
/*    Description        : Checks whether the combination of filters are    */
/*                         valid.                                           */
/*                                                                          */
/*    Input(s)           : pRuleNode    -- Pointer to the Rule Node         */
/*                         pFilterNode  -- Pointer to the FilterNode        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SUCCESS if combination is valid else FAILURE.    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PRIVATE INT1
FwlValidateRuleCombination (tFilterInfo * pFilterNode, tRuleInfo * pRuleNode)
#else
PRIVATE INT1
FwlValidateRuleCombination (pFilterNode, pRuleNode)
     tFilterInfo        *pFilterNode;
     tRuleInfo          *pRuleNode;
#endif
{
    INT1                i1Status = FWL_ZERO;
    tFilterInfo        *pTempFilter = NULL;
    tFilterInfo        *pNextTempFilter = NULL;
    UINT4               u4Count = FWL_ZERO;

    i1Status = SNMP_SUCCESS;
    for (u4Count = FWL_ZERO; ((pTempFilter = pRuleNode->apFilterInfo[u4Count])
                              != NULL) && (u4Count < FWL_MAX_FILTERS_IN_RULE -
                                           FWL_ONE); u4Count++)
    {
        /* Check whether the filter combination is of same address type
         * Combination of filters with differnet address type is not
         * allowed*/
        if (u4Count != (FWL_MAX_FILTERS_IN_RULE - FWL_ONE))
        {
            pNextTempFilter = pRuleNode->apFilterInfo[u4Count + FWL_ONE];
            if (pNextTempFilter != NULL)
            {
                if (pTempFilter->u2AddrType != pNextTempFilter->u2AddrType)
                {
                    i1Status = SNMP_FAILURE;
                }
            }
        }
        if (FWL_STRCMP
            ((INT1 *) pTempFilter->au1FilterName,
             (INT1 *) pFilterNode->au1FilterName) == FWL_STRING_EQUAL)
        {
            break;
        }

        FWL_DBG (FWL_DBG_ENTRY,
                 "\nEntering into the function FwlValidateRuleCombination\n");

        /* Check whether the rest of the filters associated with the Rule node
         * is same as of Incoming Filter node [except Src and Dest Port Range]
         * */
        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
        {
            if ((pFilterNode->SrcStartAddr.v4Addr != FWL_DEFAULT_ADDRESS) &&
                (pTempFilter->SrcStartAddr.v4Addr != FWL_DEFAULT_ADDRESS))
            {
                if ((pFilterNode->SrcStartAddr.v4Addr <
                     pTempFilter->SrcStartAddr.v4Addr)
                    && (pFilterNode->SrcStartAddr.v4Addr >
                        pTempFilter->SrcEndAddr.v4Addr)
                    && (pFilterNode->SrcEndAddr.v4Addr <
                        pTempFilter->SrcStartAddr.v4Addr)
                    && (pFilterNode->SrcEndAddr.v4Addr >
                        pTempFilter->SrcEndAddr.v4Addr))
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
            if ((pFilterNode->DestStartAddr.v4Addr != FWL_DEFAULT_ADDRESS) &&
                (pTempFilter->DestStartAddr.v4Addr != FWL_DEFAULT_ADDRESS))
            {
                if ((pFilterNode->DestStartAddr.v4Addr <
                     pTempFilter->DestStartAddr.v4Addr)
                    && (pFilterNode->DestStartAddr.v4Addr >
                        pTempFilter->DestEndAddr.v4Addr)
                    && (pFilterNode->DestEndAddr.v4Addr <
                        pTempFilter->DestStartAddr.v4Addr)
                    && (pFilterNode->DestEndAddr.v4Addr >
                        pTempFilter->DestEndAddr.v4Addr))
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
        }
        else
        {
            if ((!(IS_ADDR_UNSPECIFIED (pFilterNode->SrcStartAddr.v6Addr))) &&
                (!(IS_ADDR_UNSPECIFIED (pTempFilter->SrcStartAddr.v6Addr))))
            {
                if ((FWL_ZERO != Ip6IsAddrGreater
                     (&pTempFilter->SrcStartAddr.v6Addr,
                      &pFilterNode->SrcStartAddr.v6Addr))
                    && (FWL_ZERO != Ip6IsAddrGreater
                        (&pFilterNode->SrcStartAddr.v6Addr,
                         &pTempFilter->SrcEndAddr.v6Addr))
                    && (FWL_ZERO != Ip6IsAddrGreater
                        (&pTempFilter->SrcStartAddr.v6Addr,
                         &pFilterNode->SrcEndAddr.v6Addr))
                    && (FWL_ZERO != Ip6IsAddrGreater
                        (&pFilterNode->SrcEndAddr.v6Addr,
                         &pTempFilter->SrcEndAddr.v6Addr)))
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
            if ((!(IS_ADDR_UNSPECIFIED (pFilterNode->DestStartAddr.v6Addr))) &&
                (!(IS_ADDR_UNSPECIFIED (pTempFilter->DestStartAddr.v6Addr))))
            {
                if ((FWL_MATCH == Ip6IsAddrGreater
                     (&pTempFilter->DestStartAddr.v6Addr,
                      &pFilterNode->DestStartAddr.v6Addr))
                    && (FWL_MATCH == Ip6IsAddrGreater
                        (&pFilterNode->DestStartAddr.v6Addr,
                         &pTempFilter->DestEndAddr.v6Addr))
                    && (FWL_MATCH == Ip6IsAddrGreater
                        (&pTempFilter->DestStartAddr.v6Addr,
                         &pFilterNode->DestEndAddr.v6Addr))
                    && (FWL_MATCH == Ip6IsAddrGreater
                        (&pFilterNode->DestEndAddr.v6Addr,
                         &pTempFilter->DestEndAddr.v6Addr)))
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
        }
        if ((pFilterNode->u1Proto != FWL_DEFAULT_PROTO) &&
            (pTempFilter->u1Proto != FWL_DEFAULT_PROTO))
        {
            if (pFilterNode->u1Proto != pTempFilter->u1Proto)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
        if ((pFilterNode->u1TcpAck != FWL_TCP_ACK_ANY) &&
            (pTempFilter->u1TcpAck != FWL_TCP_ACK_ANY))
        {
            if (pFilterNode->u1TcpAck != pTempFilter->u1TcpAck)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
        if ((pFilterNode->u1TcpRst != FWL_TCP_RST_ANY) &&
            (pTempFilter->u1TcpRst != FWL_TCP_RST_ANY))
        {
            if (pFilterNode->u1TcpRst != pTempFilter->u1TcpRst)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
        if ((pFilterNode->u1Tos != FWL_TOS_ANY) &&
            (pTempFilter->u1Tos != FWL_TOS_ANY))
        {
            if (pFilterNode->u1Tos != pTempFilter->u1Tos)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
    }
    FWL_DBG (FWL_DBG_EXIT,
             "\nExiting from function FwlValidateRuleCombination\n");
    return (i1Status);
}                                /* End of function -- FwlValidateRuleCombination */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlValidateIpAddress                               */
/*     DESCRIPTION      : This function returns the type of the IpAdress it  */
/*                        belongs.i,e BroadCast,MultiCast etc.               */
/*     INPUT            : u4IpAddr - IpAddress for Validation                */
/*     OUTPUT           : The Type IpAdrress belongs.                        */
/*     RETURNS          : FWL_BCAST_ADDR     or                              */
/*                        FWL_MCAST_ADDR     or                              */
/*                        FWL_ZERO_NETW_ADDR or                              */
/*                        FWL_LOOPBACK_ADDR  or                              */
/*                        FWL_UCAST_ADDR     or                              */
/*                        FWL_INVALID_ADDR   or                              */
/*                        FWL_CLASSE_ADDR                                    */
/*****************************************************************************/

PUBLIC INT4
FwlValidateIpAddress (UINT4 u4IpAddr)
{
    /*If Address is 255.255.255.255 */
    if ((u4IpAddr & FWL_SET_ALL_BITS) == FWL_BROADCAST_ADDR)
    {
        return (FWL_BCAST_ADDR);
    }

    /*If Address is 127.x.x.x */
    if ((u4IpAddr & FWL_NET_ADDR_LOOP_BACK) == FWL_LOOP_BACK_ADDR)
    {
        return (FWL_LOOPBACK_ADDR);
    }

    /*If Address Mulicast, i.e between 224.x.x.x and 239.x.x.x */
    if ((u4IpAddr & FWL_NET_ADDR_MASK) == FWL_CLASS_D_ADDR)
    {
        return (FWL_MCAST_ADDR);
    }

    /*If Address is 0.0.0.0 */
    if ((u4IpAddr & FWL_SET_ALL_BITS) == FWL_ZERO)
    {
        return (FWL_ZERO_ADDR);
    }

    /*If Address is 0.x.x.x */
    if ((u4IpAddr & FWL_NET_ADDR_LOOP_BACK) == FWL_ZERO)
    {
        return (FWL_ZERO_NETW_ADDR);
    }

    /*If Address is CLASS-E, i.e betwwen 240.x.x.x and 254.x.x.x */
    if ((u4IpAddr & FWL_NET_ADDR_MASK) == FWL_CLASS_E_ADDR)
    {
        return (FWL_CLASSE_ADDR);
    }

    /*If Address 255.x.x.x */
    if ((u4IpAddr & FWL_NET_ADDR_LOOP_BACK) == FWL_NET_ADDR_LOOP_BACK)
    {
        return (FWL_INVALID_ADDR);
    }

    /*If Address CLASS-A and A.0.0.0 or A.255.255.255 */
    if ((u4IpAddr & FWL_32ND_BIT_SET) == FWL_ZERO)
    {
        if ((u4IpAddr & FWL_SET_24_BITS) == FWL_ZERO)
        {
            return (FWL_CLASS_NETADDR);
        }

        else if ((u4IpAddr & FWL_SET_24_BITS) == FWL_SET_24_BITS)
        {
            return (FWL_CLASS_BCASTADDR);
        }
    }
    /*If Address CLASS-B and B.0.0.0 or B.255.255.255 */
    if ((u4IpAddr & FWL_CLASS_B_MASK) == FWL_CLASS_B_ADDR)
    {
        if ((u4IpAddr & FWL_SET_16_BITS) == FWL_ZERO)
        {
            return (FWL_CLASS_NETADDR);
        }

        else if ((u4IpAddr & FWL_SET_16_BITS) == FWL_SET_16_BITS)
        {
            return (FWL_CLASS_BCASTADDR);
        }
    }

    /*If Address CLASS-C and C.0.0.0 or C.255.255.255 */
    if ((u4IpAddr & FWL_CLASS_B_MASK) == FWL_CLASS_C_ADDR)
    {
        if ((u4IpAddr & FWL_SET_8_BITS) == FWL_ZERO)
        {
            return (FWL_CLASS_NETADDR);
        }

        else if ((u4IpAddr & FWL_SET_8_BITS) == FWL_SET_8_BITS)
        {
            return (FWL_CLASS_BCASTADDR);
        }
    }
    /* check whether the given ip is a broadcast address 
     * for any of our subnet */
    if (SecUtilIpifIsBroadCastLocal (u4IpAddr) == IP_SUCCESS)
    {
        return (FWL_CLASS_BCASTADDR);
    }
    return (FWL_UCAST_ADDR);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlChkIfExtInterface                               */
/*     DESCRIPTION      : This function checks if the given interface is an  */
/*                        external (global) interface.                       */
/*     INPUT            : u4IfaceNum - Interface Number                      */
/*     OUTPUT           : None                                               */
/*     RETURNS          : FWL_SUCCESS if it is an external interface         */
/*                        FWL_FAILURE if it is an internal interface         */
/*****************************************************************************/

PUBLIC UINT4
FwlChkIfExtInterface (UINT4 u4IfaceNum)
{
    INT4                i4RetValue = FWL_FAILURE;
    if (nmhGetFwlIfIfType ((INT4) (u4IfaceNum), &i4RetValue) == SNMP_SUCCESS)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }
}

/*****************************************************************************/
/*      Function    :  FwlUtilGetIfMaxFragmentSize                           */
/*      Input       :  The Indices FwlIfIndex,                               */
/*                     pu2MaxFragmentSize where the value of Max Fragment    */
/*                     size is filled                                        */
/*      Output      :  The Get Routine Take the Indices & store the Value of */
/*                     Maximum Fragment Size in pu2MaxFragmentSize.          */
/*      Returns     :  SNMP_SUCCESS                                          */
/*****************************************************************************/

PUBLIC INT1
FwlUtilGetIfMaxFragmentSize (INT4 i4FwlIfIndex, UINT2 *pu2MaxFragmentSize)
{
    UINT4               u4IfaceNum = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;

    pIfaceNode = (tIfaceInfo *) NULL;
    u4IfaceNum = (UINT4) i4FwlIfIndex;

    /* Search the interface number and get the corresponding fragment value */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu2MaxFragmentSize = (UINT2) pIfaceNode->u2MaxFragmentSize;

    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddFilterSet                                  */
/*                                                                          */
/*    Description        : Adds the filter set to the rule                  */
/*                                                                          */
/*    Input(s)           : au1FilterSet -- String that has the list of      */
/*                                          filters                         */
/*                                                                          */
/*    Output(s)          : pRuleNode     -- Pointer to rule node            */
/*                                                                          */
/*    Returns            : SUCCESS if Parsing is done , otherwise FAILURE   */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlAddFilterSet (UINT1 au1FilterSet[FWL_MAX_FILTER_SET_LEN],
                 tRuleInfo * pRuleNode)
#else
PUBLIC INT1
FwlAddFilterSet (au1FilterSet, pRuleNode)
     UINT1               au1FilterSet[FWL_MAX_FILTER_SET_LEN];
     tRuleInfo          *pRuleNode;
#endif
{
    INT1                i1Status = FWL_ZERO;
    UINT2               u2Index1 = FWL_ZERO;
    UINT2               u2Index2 = FWL_ZERO;
    UINT2               u2Index3 = FWL_ZERO;
    UINT2               u2CounterAnd = FWL_ZERO;
    UINT2               u2CounterOr = FWL_ZERO;
    UINT2               u2FilterSetLen = FWL_ZERO;
    UINT1              
        au1FilterName[FWL_MAX_FILTERS_IN_RULE][FWL_MAX_FILTER_NAME_LEN];
    tFilterInfo        *pFilterNode = NULL;

    u2Index1 = FWL_ZERO;
    u2Index2 = FWL_ZERO;
    u2Index3 = FWL_ZERO;
    u2FilterSetLen = FWL_ZERO;
    i1Status = SNMP_SUCCESS;
    u2CounterOr = FWL_DEFAULT_COUNT;
    u2CounterAnd = FWL_DEFAULT_COUNT;
    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlAddFilterSet  \n");

    u2FilterSetLen = (UINT2) (FWL_STRLEN ((INT1 *) au1FilterSet));

    /* first the Filter set string is extracted into a list of filter names
     * and the combining condition ( & and ,) is also extracted.
     */

    while ((u2Index1 < u2FilterSetLen) &&
           (u2Index2 < FWL_MAX_FILTER_NAME_LEN) &&
           (u2Index3 < FWL_MAX_FILTERS_IN_RULE))
    {
        u2Index2 = FWL_ZERO;
        /* CHANGE1 : The loop conditon was wrong. It didnt correctly check the
         * operators and didnt parse the Filter Name from the Filter Set.
         */
        while ((u2Index1 < u2FilterSetLen) &&
               (u2Index2 < FWL_MAX_FILTER_NAME_LEN - FWL_ONE) &&
               (au1FilterSet[u2Index1] != '&') &&
               (au1FilterSet[u2Index1] != ','))
        {
            au1FilterName[u2Index3][u2Index2] = au1FilterSet[u2Index1];
            u2Index2++;
            u2Index1++;
        }                        /* end of while */

        au1FilterName[u2Index3][u2Index2] = FWL_END_OF_STRING;
        if (au1FilterSet[u2Index1] == '&')
        {
            u2CounterAnd++;
        }
        else if (au1FilterSet[u2Index1] == ',')
        {
            u2CounterOr++;
        }
        else
        {
            i1Status = SNMP_SUCCESS;
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n The operators allowed are '&' and ','\n");

        }
        u2Index3++;
        u2Index1++;
    }                            /* end of while */
    if (u2Index1 < u2FilterSetLen)
    {
        CLI_SET_ERR (CLI_FWL_MAX_FILTER);
        i1Status = SNMP_FAILURE;
    }
    else
    {                            /* Parsing of filterset is complete */
        FWL_DBG1 (FWL_DBG_CONFIG, "\n AND Operator = %d\n", u2CounterAnd);
        FWL_DBG1 (FWL_DBG_CONFIG, "\n OR Operator  = %d\n", u2CounterOr);

        /* if the operator other than & and , are used, return FAILURE */
        if ((u2CounterAnd != FWL_DEFAULT_COUNT) &&
            (u2CounterOr != FWL_DEFAULT_COUNT))
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                     "\n Both operators are not allowed to combine "
                     "in the same filter set \n");
            CLI_SET_ERR (CLI_FWL_INVALID_OPERATOR);
            i1Status = SNMP_FAILURE;
        }
        else
        {                        /* No & and , operator are combined */
            /* Based on the operator the condition flag is updated. */
            if (u2CounterAnd != FWL_DEFAULT_COUNT)
            {
                pRuleNode->u1FilterConditionFlag = FWL_FILTER_AND;
            }
            else
            {
                pRuleNode->u1FilterConditionFlag = FWL_FILTER_OR;
            }

            /* store the list of pointers corresponding to the filter name */
            for (u2Index1 = FWL_ZERO; u2Index1 < u2Index3; u2Index1++)
            {
                pRuleNode->apFilterInfo[u2Index1] =
                    FwlDbaseSearchFilter (au1FilterName[u2Index1]);

                FWL_DBG2 (FWL_DBG_CONFIG, "\n Filter Name%d = %s\n",
                          u2Index1, au1FilterName[u2Index1]);

                /* increment the reference count for the coressponding filters
                 * that are applied in the rule table.
                 */
                if (pRuleNode->apFilterInfo[u2Index1] == NULL)
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\n Filter %s not defined\n",
                                  au1FilterName[u2Index1]);
                    CLI_SET_ERR (CLI_FWL_NO_SUCH_ACL);
                    i1Status = SNMP_FAILURE;
                    break;
                }
                else
                {
                    /* CHANGE2 : The invalid  combination of rules was not 
                     * checked. So this new function was added to check the 
                     * invalid combination of rules.
                     */
                    pFilterNode = pRuleNode->apFilterInfo[u2Index1];
                    if (pFilterNode->u1RowStatus == FWL_ACTIVE)
                    {
                        if ((pRuleNode->u1FilterConditionFlag == FWL_FILTER_AND)
                            && (u2Index1 == u2Index3 - FWL_ONE))
                        {
                            i1Status = FwlValidateRuleCombination (pRuleNode->
                                                                   apFilterInfo
                                                                   [u2Index1],
                                                                   pRuleNode);
                        }
                        if (i1Status == SNMP_SUCCESS)
                        {
                            INC_FILTER_REF_COUNT (pFilterNode);
                        }
                        else
                        {
                            pRuleNode->apFilterInfo[u2Index1] = NULL;
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                     "\n Invalid Filter Combination \n");
                            CLI_SET_ERR (CLI_FWL_INVALID_FILTER_COMBINATION);
                            break;
                        }
                    }
                    else
                    {
                        i1Status = SNMP_FAILURE;
                        pRuleNode->apFilterInfo[u2Index1] = NULL;
                        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n Filter %s is not Active \n",
                                      pFilterNode->au1FilterName);
                        CLI_SET_ERR (CLI_FWL_FILTER_NOT_ACTIVE);
                        break;
                    }
                }
                FWL_DBG1 (FWL_DBG_CONFIG, "\n Filter Reference Count = %d\n",
                          pRuleNode->apFilterInfo[u2Index1]->u1FilterRefCount);
            }                    /* end of for */

            /* if Status is FAILURE, then the some filters are not found in
             * the filter list. So the Filter Set is invalid and the reference
             * count must be reverted back.
             */
            if (i1Status == SNMP_FAILURE)
            {
                for (u2Index1 = FWL_ZERO; u2Index1 < u2Index3; u2Index1++)
                {
                    if (pRuleNode->apFilterInfo[u2Index1] != NULL)
                    {
                        pFilterNode = pRuleNode->apFilterInfo[u2Index1];
                        pRuleNode->apFilterInfo[u2Index1] = NULL;
                        DEC_FILTER_REF_COUNT (pFilterNode);
                    }
                }                /*end of for */
            }
        }                        /* end of else (No & or , operator combined */
    }                            /*end of else (parsing is complete */

    if (i1Status == SNMP_SUCCESS)
    {
        STRCPY ((INT1 *) pRuleNode->au1FilterSet, au1FilterSet);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting out of the fn. FwlAddFilterSet  \n");

    return (i1Status);

}                                /* End of Function -- FwlAddFilterSet */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUtlSetIfTypeExternal                          */
/*                                                                          */
/*    Description        : Sets the Wan Iface as External                   */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

VOID
FwlUtlSetIfTypeExternal (UINT4 u4IfaceNum)
{
    if (u4IfaceNum > FWL_ZERO)
    {
        nmhSetFwlIfRowStatus ((INT4) u4IfaceNum, FWL_CREATE_AND_GO);
        nmhSetFwlIfRowStatus ((INT4) u4IfaceNum, FWL_NOT_IN_SERVICE);
        nmhSetFwlIfIfType ((INT4) u4IfaceNum, FWL_EXTERNAL_IF);
        nmhSetFwlIfRowStatus ((INT4) u4IfaceNum, FWL_ACTIVE);
    }
}                                /* End of Function -- FwlUtlSetIfTypeExternal */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlIsIpAddressValid                                */
/*     DESCRIPTION      : This function checks if the given ip address is    */
/*                        valid.                                             */
/*     INPUT            : u4IpAddress- IP address                            */
/*     OUTPUT           : None                                               */
/*     RETURNS          : FWL_SUCCESS if it is a valid ip address            */
/*                        FWL_FAILURE if it is a invalid ip address          */
/*****************************************************************************/
PUBLIC INT4
FwlIsIpAddressValid (UINT4 u4IpAddress)
{
    if ((u4IpAddress & FWL_NET_ADDR_LOOP_BACK) == FWL_LOOP_BACK_ADDR)    /* 127.0.0.1 */
    {
        return (FWL_FAILURE);
    }
    return (FWL_SUCCESS);
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*     FUNCTION NAME    : FwlUtilUpdStatefulTcpStates                        */
/*     DESCRIPTION      : This function fetches the stateful table node      */
/*                        corresponding to the flow manager tuple information*/
/*     INPUT            : NpTuple - Tuple in flow manager                    */
/*     OUTPUT           : pNode - stateful table node                        */
/*     RETURNS          : FWL_SUCCESS                                        */
/*                        FWL_FAILURE                                        */
/*****************************************************************************/
INT4
FwlUtilUpdStatefulTcpStates (UINT4 u4SrcIp, UINT4 u4DestIp, UINT2 u2SrcPort,
                             UINT2 u2DestPort, UINT1 u1Protocol,
                             tStatefulSessionNode * pNode)
{
    UINT1               u1Log;
    tIpHeader           IpHdr;
    tHLInfo             HlInfo;

    MEMSET (&IpHdr, FWL_ZERO, sizeof (tIpHeader));
    MEMSET (&HlInfo, FWL_ZERO, sizeof (tHLInfo));

    if (u1Protocol != FWL_TCP)
    {
        return OSIX_FAILURE;
    }

    IpHdr.SrcAddr.v4Addr = u4SrcIp;
    IpHdr.DestAddr.v4Addr = u4DestIp;
    IpHdr.u1Proto = u1Protocol;
    HlInfo.u2SrcPort = u2SrcPort;
    HlInfo.u2DestPort = u2DestPort;

    /* Search the State table in the both the direction */

    /* If pNode is NULL then scan the stateful table */
    if (pNode == NULL)
    {
        if (FwlMatchStateFilter (&IpHdr, &HlInfo, NULL, FWL_DIRECTION_IN,
                                 &u1Log, &pNode) != FWL_MATCH)
        {
            if (FwlMatchStateFilter (&IpHdr, &HlInfo, NULL, FWL_DIRECTION_OUT,
                                     &u1Log, &pNode) != FWL_MATCH)
            {
                return OSIX_FAILURE;
            }
        }
    }

    if (pNode == NULL)
    {
        return OSIX_FAILURE;
    }
#ifdef FLOWMGR_WANTED
    /*
     * Query TCP states from NP and update the pNode.
     * Return Value May also contain OSIX_FAILURE,OSIX_ERR_OS_DOES_NOT_SUPP. 
     */
    return (FlFwlGetFwlTcpStates (u4SrcIp, u4DestIp, u2SrcPort,
                                  u2DestPort, u1Protocol,
                                  pNode->LocalIP.v4Addr, pNode->RemoteIP.v4Addr,
                                  &pNode->ProtHdr.TcpHdr.LocalTcpInfo,
                                  &pNode->ProtHdr.TcpHdr.RemoteTcpInfo));
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FlFwlUpdateFwlInitTable                          */
/*    Description         : Function does the following,                     */
/*                          1. Remove from TCP init table                    */
/*                          2. Add to Stateful table with states as EST      */
/*    Input(s)            : u4SrcIp, u4DestIp, u2SrcPort,u2Destport,u1Proto  */
/*    Output(s)           : None                                             */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
PUBLIC INT4
FlFwlUpdateFwlInitTable (UINT4 u4SrcIp, UINT4 u4DestIp, UINT2 u2SrcPort,
                         UINT2 u2DestPort, UINT1 u1Proto)
{
    tIpHeader           StateIpHdr;
    tHLInfo             StateHLInfo;
    UINT1               u1LogTrigger;
    tStatefulSessionNode *pInitStateNode = NULL;

    MEMSET (&StateIpHdr, FWL_ZERO, sizeof (tIpHeader));
    MEMSET (&StateHLInfo, FWL_ZERO, sizeof (tHLInfo));

    StateIpHdr.SrcAddr.v4Addr = u4SrcIp;
    StateIpHdr.DestAddr.v4Addr = u4DestIp;
    StateIpHdr.u1Proto = u1Proto;

    StateHLInfo.u2SrcPort = u2SrcPort;
    StateHLInfo.u2DestPort = u2DestPort;

    /* Since this API will be called while configuring SYN+ACK 
     * packet from WAN, direction used is IN */

    if (FwlMatchTcpInitFlowNode (&StateIpHdr, &StateHLInfo,
                                 FWL_DIRECTION_IN, &u1LogTrigger,
                                 &pInitStateNode) == FWL_MATCH)
    {
        pInitStateNode->u1LocalState = FWL_TCP_STATE_EST;
        pInitStateNode->u1RemoteState = FWL_TCP_STATE_EST;

        /* Move the InitFlow SLL Node to StateTable. */
        /* Then delete the Node Entry */
        TMO_SLL_Delete (&gFwlTcpInitFlowList, (tTMO_SLL_NODE *) pInitStateNode);

        if (FwlAddStateFilter (pInitStateNode, pInitStateNode->u1Direction)
            == FWL_FAILURE)
        {
            /* Release the Init Node Memory */
            FwlTcpInitFlowMemFree ((UINT1 *) pInitStateNode);
            /* Dropping the Session. And current pkt.
             *                          * Log Message */
            return OSIX_FAILURE;
        }
        /* Release the Init Node Memory */
        FwlTcpInitFlowMemFree ((UINT1 *) pInitStateNode);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}
#endif /* NPAPI_WANTED */
/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilGetFwlStatus                                    */
/*                                                                           */
/* Description      :                                                        */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : OSIX_SUCCESS / OSIX_FAILURE                           */
/*****************************************************************************/

PUBLIC UINT1
FwlUtilGetFwlStatus (UINT4 u4IfNum)
{
    if ((gu1FirewallStatus == FWL_ENABLE) &&
        (FwlChkIfExtInterface (u4IfNum) == FWL_SUCCESS))
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilGetGlobalStatus                                 */
/*                                                                           */
/* Description      :                                                        */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : Global Firewall Status                                */
/*****************************************************************************/

PUBLIC UINT1
FwlUtilGetGlobalStatus ()
{
    INT4                i4FwlStatus = FWL_ZERO;

    nmhGetFwlGlobalMasterControlSwitch (&i4FwlStatus);

    return ((UINT1) i4FwlStatus);
}

#ifndef SECURITY_KERNEL_WANTED

/*****************************************************************************/
/* Function Name    : FwlUtilReleaseWebLog                                   */
/*                                                                           */
/* Description      : Function to release the memory block allocated for the */
/*                    web log                                                */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Return Value      : FWL_SUCCESS or FWL_FAILURE                            */
/*****************************************************************************/

INT4
FwlUtilReleaseWebLog (UINT1 *pu1LogPtr)
{
    if (MemReleaseMemBlock (FWL_WEB_LOG_PID, pu1LogPtr) == MEM_SUCCESS)
    {
        return FWL_SUCCESS;
    }
    return FWL_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlUtilGetSysLogId                                 */
/*                                                                           */
/*     DESCRIPTION      : This function fetches the ID registered with SysLog*/
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Syslog ID registered with syslog module            */
/*                                                                           */
/*****************************************************************************/

INT4
FwlUtilGetSysLogId ()
{
    return gi4FwlSysLogId;
}

#endif

/*****************************************************************************/
/* Function Name      : FwlUtilRegisterCallBack                              */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gaFwlCustCallBack                                    */
/*                                                                           */
/* Return Value(s)    : FWL_SUCCESS/FWL_FAILURE                              */
/*****************************************************************************/
INT4
FwlUtilRegisterCallBack (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = FWL_SUCCESS;

    switch (u4Event)
    {
        case FWL_CUST_IF_CHECK_EVENT:
            FWL_UTIL_CALL_BACK[u4Event].pFwlCustIfCheck =
                pFsCbInfo->pFwlCustIfCheck;
            break;

        default:
            i4RetVal = FWL_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FwlUtilCallBack                                      */
/*                                                                           */
/* Description        : This function processes the callback events invoked  */
/*                      in the program.                                      */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gaFwlUtilCallBack                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FWL_SUCCESS/FWL_FAILURE                              */
/*****************************************************************************/
INT4
FwlUtilCallBack (UINT4 u4Event)
{
    INT4                i4RetVal = FWL_SUCCESS;

    /* This will be executed only when the call back is registered priorly */
    switch (u4Event)
    {
        case FWL_CUST_IF_CHECK_EVENT:
            if (NULL != (FWL_UTIL_CALL_BACK[u4Event]).pFwlCustIfCheck)
            {
                if (ISS_FAILURE ==
                    FWL_UTIL_CALL_BACK[u4Event].pFwlCustIfCheck ())
                {
                    i4RetVal = FWL_FAILURE;
                }
            }
            break;
        default:
            i4RetVal = FWL_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilAclLookup                                       */
/*                                                                           */
/* Description      : Function will get the ACL Node for the given ACL Name  */
/*                    and returns whether the filter parameters              */
/*                    matched with Nat parameters (src ip, dest ip           */
/*                    and dest port)                                         */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Return Value      : OSIX_SUCCESS or OSIX_FAILURE                          */
/*****************************************************************************/

PUBLIC INT4
FwlUtilAclLookup (tFwlPacketInfo * pFwlPacketNode, UINT1 *pu1AclName,
                  INT4 i4Type)
{
    tAclInfo           *pAclNode = (tAclInfo *) NULL;
    tIfaceInfo         *pIfaceNode = (tIfaceInfo *) NULL;
    tRuleInfo          *pRuleNode = (tRuleInfo *) NULL;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT2               u2Index = FWL_ZERO;

    /* It is ensured that lock is not taken anywhere in Firewall module
     * before calling this function. */
    FwlLock ();
    while (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        if ((pIfaceNode = FwlDbaseSearchIface (u4IfaceNum)) == NULL)
        {
            u4IfaceNum++;
            continue;
        }
        if (pFwlPacketNode->u4Direction == FWL_DIRECTION_IN)
        {
            pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                pu1AclName);

            if (pAclNode != NULL)
            {
                pRuleNode = FwlDbaseSearchRule (pAclNode->au1FilterName);
            }
            else
            {
                u4IfaceNum++;
                continue;
            }
            while ((u2Index < FWL_MAX_FILTERS_IN_RULE) && (pRuleNode != NULL)
                   && (pRuleNode->apFilterInfo[u2Index] != NULL))
            {
                /* Populate the output variable u4OutIpAddr with
                 * the destination IP address (local IP address) corresponding
                 * to it and return when the following conditions are satisfied.
                 * (i) The source IP address(WAN side IP address) in the
                 * access list matches the outside IP  address.
                 * (ii) If the Start address and the end address are equal
                 */
                if (pRuleNode->apFilterInfo[u2Index]->SrcStartAddr.v4Addr
                    == pFwlPacketNode->u4DestIpAddr)
                {
                    pFwlPacketNode->u4OutIpAddr =
                        pRuleNode->apFilterInfo[u2Index]->DestStartAddr.v4Addr;
                    FwlUnLock ();
                    return OSIX_SUCCESS;
                }
                u2Index++;
            }
        }
        else
        {
            pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                pu1AclName);
            if (pAclNode != NULL)
            {
                pRuleNode = FwlDbaseSearchRule (pAclNode->au1FilterName);
            }
            else
            {
                u4IfaceNum++;
                continue;
            }
            while ((u2Index < FWL_MAX_FILTERS_IN_RULE) && (pRuleNode != NULL)
                   && (pRuleNode->apFilterInfo[u2Index] != NULL))
            {
                if (FWL_STATIC_FILTER == i4Type)
                {
                    if ((pRuleNode->apFilterInfo[u2Index]->DestStartAddr.
                         v4Addr == pFwlPacketNode->u4DestIpAddr)
                        && (pRuleNode->apFilterInfo[u2Index]->SrcStartAddr.
                            v4Addr == pFwlPacketNode->u4SrcIpAddr))
                    {
                        if (pRuleNode->apFilterInfo[u2Index]->u2DestMinPort ==
                            pFwlPacketNode->u4DestPort)
                        {
                            FwlUnLock ();
                            pFwlPacketNode->u4PortMatch = FWL_TRUE;
                            return OSIX_SUCCESS;
                        }
                        else
                        {
                            /* Out Port information can be any
                             * Hence no need of further probing.
                             */
                            FwlUnLock ();
                            return OSIX_SUCCESS;
                        }
                    }
                }
                else
                {
                    if ((FWL_TRUE ==
                         FwlUtilIsIpAddrPartofRange (pFwlPacketNode->
                                                     u4DestIpAddr,
                                                     pRuleNode->
                                                     apFilterInfo[u2Index]->
                                                     DestStartAddr.v4Addr,
                                                     pRuleNode->
                                                     apFilterInfo[u2Index]->
                                                     DestEndAddr.v4Addr))
                        && (FWL_TRUE ==
                            FwlUtilIsIpAddrPartofRange (pFwlPacketNode->
                                                        u4SrcIpAddr,
                                                        pRuleNode->
                                                        apFilterInfo[u2Index]->
                                                        SrcStartAddr.v4Addr,
                                                        pRuleNode->
                                                        apFilterInfo[u2Index]->
                                                        SrcEndAddr.v4Addr)))
                    {
                        if (pFwlPacketNode->u4DestPort ==
                            pRuleNode->apFilterInfo[u2Index]->u2DestMinPort)
                        {
                            FwlUnLock ();
                            pFwlPacketNode->u4PortMatch = FWL_TRUE;
                            return OSIX_SUCCESS;
                        }
                        else
                        {
                            /* Out Port information can be any
                             * Hence no need of further probing.
                             */
                            FwlUnLock ();
                            return OSIX_SUCCESS;
                        }
                    }
                }
                u2Index++;
            }
        }
        u4IfaceNum++;
    }
    FwlUnLock ();
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FwlUtilGetAclAction                                    */
/*                                                                           */
/* Description      : Function to get Firewall ACL action (PERMIT/DENY)      */
/*                    Also Validates Filters for the given ACL Name.         */
/*                                                                           */
/* Input Parameters  : None                                                  */
/*                                                                           */
/* Return Value      : OSIX_SUCCESS or OSIX_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
FwlUtilGetAclAction (INT4 i4Type, UINT1 *pu1AclName)
{
    tAclInfo           *pAclNode = (tAclInfo *) NULL;
    tIfaceInfo         *pIfaceNode = (tIfaceInfo *) NULL;
    tRuleInfo          *pRuleNode = (tRuleInfo *) NULL;
    UINT4               u4Action = FWL_DENY;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT4               u4IsFilterValid = FWL_TRUE;
    INT4                i4AclFound = FWL_FALSE;
    UINT2               u2Index = FWL_ZERO;
    /* It is ensured that lock is not taken anywhere in Firewall module
     * before calling this function. */
    FwlLock ();
    for (u4IfaceNum = FWL_ZERO; u4IfaceNum < FWL_MAX_NUM_OF_IF; u4IfaceNum++)
    {
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode == NULL)
        {
            continue;
        }
        pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                            pu1AclName);

        if (pAclNode == NULL)
        {
            continue;
        }
        i4AclFound = FWL_TRUE;
        /* Only access lists of type permit are allowed for binding with Policy
         * NAT.
         */
        if (FWL_PERMIT == pAclNode->u1Action)
        {
            u4Action = FWL_PERMIT;
        }
        pRuleNode = FwlDbaseSearchRule (pAclNode->au1FilterName);
        if (pRuleNode == NULL)
        {
            continue;
        }
        while ((u2Index < FWL_MAX_FILTERS_IN_RULE)
               && (pRuleNode->apFilterInfo[u2Index] != NULL))
        {
            if (FWL_STATIC_FILTER == i4Type)
            {
                /* For Static Policy entry creation, the source prefix 
                 * and destination prefix must be full.
                 */
                if ((pRuleNode->apFilterInfo[u2Index]->SrcStartAddr.v4Addr !=
                     pRuleNode->apFilterInfo[u2Index]->SrcEndAddr.v4Addr) &&
                    (pRuleNode->apFilterInfo[u2Index]->DestStartAddr.v4Addr !=
                     pRuleNode->apFilterInfo[u2Index]->DestEndAddr.v4Addr))
                {
                    u4IsFilterValid = FWL_FALSE;
                }
                else
                {
                    u4IsFilterValid = FWL_TRUE;
                }
            }
            u2Index++;
        }
    }

    if (FWL_FALSE == i4AclFound)
    {
        for (u4IfaceNum = FWL_ZERO; u4IfaceNum < FWL_MAX_NUM_OF_IF;
             u4IfaceNum++)
        {
            pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
            if (pIfaceNode == NULL)
            {
                continue;
            }
            pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                pu1AclName);

            if (pAclNode == NULL)
            {
                continue;
            }
            if (FWL_PERMIT == pAclNode->u1Action)
            {
                u4Action = FWL_PERMIT;
            }
            pRuleNode = FwlDbaseSearchRule (pAclNode->au1FilterName);
            if (pRuleNode == NULL)
            {
                continue;
            }
            while ((u2Index < FWL_MAX_FILTERS_IN_RULE)
                   && (pRuleNode->apFilterInfo[u2Index] != NULL))
            {
                /* For Static Policy entry creation, the source prefix 
                 * and destination prefix must be full.
                 */
                if (FWL_STATIC_FILTER == i4Type)
                {
                    if ((pRuleNode->apFilterInfo[u2Index]->SrcStartAddr.
                         v4Addr !=
                         pRuleNode->apFilterInfo[u2Index]->SrcEndAddr.v4Addr)
                        || (pRuleNode->apFilterInfo[u2Index]->DestStartAddr.
                            v4Addr !=
                            pRuleNode->apFilterInfo[u2Index]->DestEndAddr.
                            v4Addr))
                    {
                        u4IsFilterValid = FWL_FALSE;
                    }
                    else
                    {
                        u4IsFilterValid = FWL_TRUE;
                    }
                }
                u2Index++;
            }
        }
    }
    if ((FWL_PERMIT == u4Action) && (FWL_TRUE == u4IsFilterValid))
    {
        FwlUnLock ();
        return OSIX_SUCCESS;
    }

    FwlUnLock ();
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : FwlUtilIsIpAddrPartofRange                             */
/*  Description     : This function identifies if the given IP address is    */
/*                    within the address range configured.                   */
/*  Input(s)        : u4IpAddress - The IP address to be checked.            */
/*                    u4IpStartAddr  - Start Address                         */
/*                    u4IpEndAddr - End Address                              */
/*  Output(s)       : pu4IpAddr - Global IP Address                          */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : None.                                      */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  FWL_TRUE  if IP address is within range.  */
/*                                 FWL_FALSE if IP address is not within range*/
/*****************************************************************************/
PRIVATE UINT4
FwlUtilIsIpAddrPartofRange (UINT4 u4IpAddress,
                            UINT4 u4IpStartAddr, UINT4 u4IpEndAddr)
{
    if ((u4IpAddress >= u4IpStartAddr) && (u4IpAddress <= u4IpEndAddr))
    {
        return (FWL_TRUE);
    }
    return FWL_FALSE;
}

/*****************************************************************************/
/*  Function Name   : FwlUtilUpdateLogParams                                 */
/*  Description     : This function used to update the maximum size & thresh */
/*                    value of the logs above which trap is generated        */
/*  Input(s)        : u1MsgType -  1 indicates the log threshold size        */
/*                                 2 indicates the maximum size of logs      */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*                                                                           */
/*  Returns         : None                                                   */
/*                                                                           */
/****************************************************************************/

VOID
FwlUtilUpdateLogParams (UINT1 u1MsgType, UINT4 u4Value)
{
    if (u1MsgType == FWL_MSG_TYPE_LOG_SIZE_THRESH)
    {
        gu4FwlLogSizeThreshold = u4Value;
    }
    else if (u1MsgType == FWL_MSG_TYPE_MAX_LOG_SIZE)
    {
        gu4FwlMaxLogSize = u4Value;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlUtilGetIpAddress                                */
/*                                                                           */
/*     DESCRIPTION      : This function copies the IP address to pFwlIpAddr  */
/*                                                                           */
/*     INPUT            : i4Afi  - Address Family Identifier                 */
/*                                                                           */
/*                        pOctetString - IPv4 /IPv6 address                  */
/*                                                                           */
/*     OUTPUT           : pFwlIpAddr - Pointer to Fwl IPvx address           */
/*                                                                           */
/*     RETURNS          : FWL_SUCCESS or FWL_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
FwlUtilGetIpAddress (INT4 i4Afi, tSNMP_OCTET_STRING_TYPE * pOctetString,
                     tFwlIpAddr * pFwlIpAddr)
{
    UINT4               u4PeerAddress = FWL_ZERO;

    switch (i4Afi)
    {
        case IPVX_ADDR_FMLY_IPV4:
            MEMCPY ((CHR1 *) & u4PeerAddress,
                    (CHR1 *) pOctetString->pu1_OctetList, sizeof (UINT4));
            pFwlIpAddr->uIpAddr.Ip4Addr = u4PeerAddress;
            break;

        case IPVX_ADDR_FMLY_IPV6:
            MEMCPY (pFwlIpAddr->uIpAddr.Ip6Addr.ip6_addr_u.u1ByteAddr,
                    pOctetString->pu1_OctetList, FWL_IPV6_PREFIX_LEN);
            break;
        default:
            return FWL_FAILURE;
    }
    return FWL_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FwlUtilRestoreIdsStatus                            */
/*     DESCRIPTION      : This function restores the previously saved IDS    */
/*                        status. This function is invoked when snort is     */
/*                        successfully restarted after loading rules         */
/*     INPUT            : NONE                                               */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
FwlUtilRestoreIdsStatus ()
{
    /* This is needed to intimate IDS status to kernel */
    nmhSetFwlGlobalIdsStatus ((INT4) gu4PrevIdsStatus);
    return;
}

/*****************************************************************************/
/*  Function Name   : FwlUtilUpdateIdsRulesStatus                            */
/*  Description     : This function set the IDS global rules status          */
/*  Input(s)        : u4Status - Status of IDS                               */
/*  Output(s)       : None                                                   */
/*  Return (s)      :  OSIX_SUCCESS                                          */
/*****************************************************************************/
INT4
FwlUtilUpdateIdsRulesStatus (UINT4 u4Status)
{
    gi4IdsRulesStatus = (INT4) u4Status;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*              End of File fwlutils.c                                       */
/*****************************************************************************/
