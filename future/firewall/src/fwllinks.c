/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fwllinks.c,v 1.9 2013/10/31 11:13:30 siva Exp $
 * 
 * Description: This file contains functions for firewall partial
 * link list management.
 * 
 ********************************************************************/

#include "fwlinc.h"
#include "sipalg.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlPartialLinksListInit                          */
/*                                                                          */
/*    Description        : Initialises the Data structure for PartialLinks  */
/*                         table.                                           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
PUBLIC VOID
FwlPartialLinksListInit (VOID)
{
    /* gFwlPartialLinksList is used to store entries of partial-links.
     * This table is searched for a Partial link entry match. 
     * It uses the Protocol number, Src & Dest IP addresses, Src & Dest Ports 
     * to match an entry. */

    TMO_SLL_Init (&gFwlPartialLinksList);

    TMO_SLL_Init (&gFwlV6PartialLinksList);
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddPartialLinkNode                            */
/*                                                                          */
/*    Description        : Adds the filter to the gFwlPartialLinksList      */
/*                                                                          */
/*    Input(s)           : pStateIpHdr  -- Pointer to the Ip hdr            */
/*                         pStateHLInfo -- Pointer to the Transport Hdr     */
/*                         u1State      -- State of the Filter              */
/*                         u1LogLevel   -- Log Level of the filter          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS                                      */
/****************************************************************************/
PUBLIC UINT4
FwlAddPartialLinkNode (tIpHeader * pStateIpHdr,
                       tHLInfo * pStateHLInfo,
                       UINT1 u1State, UINT1 u1Direction, UINT1 u1LogLevel,
                       UINT1 u1AppCallStatus)
{
    UINT4               u4CurTime = FWL_ZERO;
    tStatefulSessionNode *pPartialNode = NULL;
    tTMO_SLL           *pPartialLst = NULL;
    tIpHeader           IpHdr;
    tHLInfo             HLInfo;

    /* Validate the Protocol. */
    if ((pStateIpHdr->u1Proto != FWL_TCP) && (pStateIpHdr->u1Proto != FWL_UDP))
    {
        return FWL_FAILURE;
    }

    /* Copy IpHdr and HLInfo contents into local variables */
    FWL_MEMCPY (&IpHdr, pStateIpHdr, sizeof (tIpHeader));
    FWL_MEMCPY (&HLInfo, pStateHLInfo, sizeof (tHLInfo));

    /* Allocate memory for the new node to be added. */
    if (FwlPartialLinksMemAllocate ((UINT1 **) (VOID *) &pPartialNode) ==
        MEM_FAILURE)
    {
        return (FWL_FAILURE);
    }
    /* Init the Node */
    MEMSET (pPartialNode, FWL_ZERO, sizeof (tStatefulSessionNode));
    TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pPartialNode);

    /* Update the Node information */
    if (IpHdr.SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pPartialNode->LocalIP.v4Addr = IpHdr.SrcAddr.v4Addr;
        pPartialNode->LocalIP.u4AddrType = FWL_IP_VERSION_4;
        pPartialNode->RemoteIP.v4Addr = IpHdr.DestAddr.v4Addr;
        pPartialNode->RemoteIP.u4AddrType = FWL_IP_VERSION_4;

        pPartialLst = &gFwlPartialLinksList;
    }
    else
    {
        Ip6AddrCopy (&(pPartialNode->LocalIP.v6Addr), &(IpHdr.SrcAddr.v6Addr));
        pPartialNode->LocalIP.u4AddrType = FWL_IP_VERSION_6;
        Ip6AddrCopy (&(pPartialNode->RemoteIP.v6Addr),
                     &(IpHdr.DestAddr.v6Addr));
        pPartialNode->RemoteIP.u4AddrType = FWL_IP_VERSION_6;

        pPartialLst = &gFwlV6PartialLinksList;
    }

    /* Update Local & Remote Port fields. */
    switch (IpHdr.u1Proto)
    {
        case FWL_TCP:
            FWL_LOCAL_TCP_PORT (pPartialNode) = HLInfo.u2SrcPort;
            FWL_REMOTE_TCP_PORT (pPartialNode) = HLInfo.u2DestPort;
            break;
        case FWL_UDP:
            FWL_LOCAL_UDP_PORT (pPartialNode) = HLInfo.u2SrcPort;
            FWL_REMOTE_UDP_PORT (pPartialNode) = HLInfo.u2DestPort;
            break;
        default:
            break;
    }                            /*End-of-Switch */

    /* Set State(PERSISTENT/NON_PERSISTENT) for this partial link. */
    pPartialNode->u1LocalState = u1State;
    pPartialNode->u1RemoteState = u1State;

    pPartialNode->u1Protocol = IpHdr.u1Proto;
    pPartialNode->u4Timestamp = FWL_GET_SYS_TIME (&u4CurTime);
    pPartialNode->u1LogLevel = u1LogLevel;
    pPartialNode->u1Direction = u1Direction;
    pPartialNode->u1AppCallStatus = u1AppCallStatus;
    /* Add the Node in the gFwlPartialLinksList */

    /* NOTE: Below call adds the new node at the beginning of the List. *
     * Adding of the new node at the begin of the List is required      *
     * because all the updated partial entries are to matched before    *
     * PERSISTANT partial entries are scanned for a matching entry.     */
    TMO_SLL_Insert_In_Middle (pPartialLst,
                              (tTMO_SLL_NODE *) & (pPartialLst->Head),
                              (tTMO_SLL_NODE *) pPartialNode,
                              (tTMO_SLL_NODE *) pPartialLst->Head.pNext);

    return FWL_SUCCESS;
}                                /*End of Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeleteAppPartialListNode                      */
/*                                                                          */
/*    Description        : Deletes the Partial Node from Partial Links      */
/*                         List                                             */
/*                                                                          */
/*    Input(s)           : pPartialState -- Pointer to the Partial Node     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS/FWL_FAILURE                          */
/****************************************************************************/

INT4
FwlDeleteAppPartialListNode (tStatefulSessionNode * pPartialState)
{
    tStatefulSessionNode *pPartialNode = NULL;
    UINT4               u4IpAddr = FWL_ZERO;
    UINT4               u4IpAddr1 = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT2               u2Port1 = FWL_ZERO;

    pPartialNode = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlPartialLinksList);

    if (pPartialState->u1Direction == FWL_DIRECTION_IN)
    {
        u4IpAddr = pPartialState->LocalIP.v4Addr;
        u2Port =
            (UINT2) FWL_LOCAL_PORT (pPartialState, pPartialState->u1Protocol);
    }
    else
    {
        u4IpAddr = pPartialState->RemoteIP.v4Addr;
        u2Port =
            (UINT2) FWL_REMOTE_PORT (pPartialState, pPartialState->u1Protocol);
    }

    while (pPartialNode != NULL)
    {
        if (pPartialNode->u1Direction == FWL_DIRECTION_IN)
        {
            u4IpAddr1 = pPartialNode->LocalIP.v4Addr;
            u2Port1 =
                (UINT2) FWL_LOCAL_PORT (pPartialNode, pPartialNode->u1Protocol);
        }
        else
        {
            u4IpAddr1 = pPartialNode->RemoteIP.v4Addr;
            u2Port1 =
                (UINT2) FWL_REMOTE_PORT (pPartialNode,
                                         pPartialNode->u1Protocol);
        }

        if ((u4IpAddr == u4IpAddr1) && (pPartialState->u1Protocol ==
                                        pPartialNode->u1Protocol)
            && (pPartialState->u1Direction == pPartialNode->u1Direction)
            && (u2Port == u2Port1))
        {

            /* Call Tmo Delete Node */
            TMO_SLL_Delete (&gFwlPartialLinksList,
                            (tTMO_SLL_NODE *) pPartialNode);
            FwlPartialLinksMemFree ((UINT1 *) pPartialNode);
            return FWL_SUCCESS;
        }
        else
        {
            pPartialNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlPartialLinksList,
                                                       (tTMO_SLL_NODE *)
                                                       pPartialNode);
        }
    }                            /*End of While-Loop */

    return FWL_FAILURE;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCleanAllAppEntry                              */
/*                                                                          */
/*    Description        : Deletes all the Sip Entered Firewall Entries     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS                                      */
/****************************************************************************/
PUBLIC VOID
FwlCleanAllAppEntry ()
{

    tStatefulSessionNode *pState = NULL;
    tStatefulSessionNode *pNextNode = NULL;
    tStatefulSessionNode *pPartialNode = NULL;
    tStatefulSessionNode *pInitFlowNode = NULL;
    UINT4               u4HashIndex = FWL_ZERO;

    pState = (tStatefulSessionNode *) NULL;
    u4HashIndex = FWL_ZERO;
    UNUSED_PARAM (u4HashIndex);
    UNUSED_PARAM (pState);
    /* Deleting All TCP Init Flow Entries */

    pNextNode = NULL;
    pInitFlowNode = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlTcpInitFlowList);

    while (pInitFlowNode != NULL)
    {

        if ((pInitFlowNode->u1AppCallStatus == FWL_APP_HOLD) ||
            (pInitFlowNode->u1AppCallStatus == FWL_APP_UNHOLD))
        {

            pNextNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlTcpInitFlowList,
                                                       (tTMO_SLL_NODE *)
                                                       pInitFlowNode);
            /* Call Tmo Delete Node */
            TMO_SLL_Delete (&gFwlTcpInitFlowList,
                            (tTMO_SLL_NODE *) pInitFlowNode);

            FwlTcpInitDelCountPerNode (pInitFlowNode->LocalIP);
            FwlTcpInitFlowMemFree ((UINT1 *) pInitFlowNode);
            pInitFlowNode = pNextNode;
        }
        else
        {
            pInitFlowNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlTcpInitFlowList,
                                                       (tTMO_SLL_NODE *)
                                                       pInitFlowNode);
        }
    }                            /*End of While-Loop */

    /* Deleting Partial List Entries */
    pPartialNode = NULL;
    pNextNode = NULL;

    pPartialNode = (tStatefulSessionNode *)
        TMO_SLL_First (&gFwlPartialLinksList);

    while (pPartialNode != NULL)
    {

        if ((pPartialNode->u1AppCallStatus == FWL_APP_HOLD) ||
            (pPartialNode->u1AppCallStatus == FWL_APP_UNHOLD))
        {

            pNextNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlPartialLinksList,
                                                       (tTMO_SLL_NODE *)
                                                       pPartialNode);
            /* Call Tmo Delete Node */
            TMO_SLL_Delete (&gFwlPartialLinksList,
                            (tTMO_SLL_NODE *) pPartialNode);

            FwlPartialLinksMemFree ((UINT1 *) pPartialNode);
            pPartialNode = pNextNode;
        }
        else
        {
            pPartialNode =
                (tStatefulSessionNode *) TMO_SLL_Next (&gFwlPartialLinksList,
                                                       (tTMO_SLL_NODE *)
                                                       pPartialNode);
        }
    }                            /*End of While-Loop */

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAppUpdateStateEntryCallStatus                 */
/*                                                                          */
/*    Description        : This function updates hold status in state table */
/*                         entries                                          */
/*                                                                          */
/*    Input(s)           : pPartialMatchNode - Pointer to Partial           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS                                      */
/****************************************************************************/

INT4
FwlAppUpdateStateEntryCallStatus (tStatefulSessionNode * pPartialMatchNode)
{
    tStatefulSessionNode *pState = NULL;
    UINT4               u4HashIndex = FWL_ZERO;
    UINT4               u4IpAddr = FWL_ZERO;
    UINT4               u4IpAddr1 = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT2               u2Port1 = FWL_ZERO;

    pState = (tStatefulSessionNode *) NULL;
    u4HashIndex = FWL_ZERO;

    if (pPartialMatchNode->u1Direction == FWL_DIRECTION_IN)
    {
        u4IpAddr = pPartialMatchNode->LocalIP.v4Addr;
        u2Port = (UINT2) FWL_LOCAL_PORT (pPartialMatchNode,
                                         pPartialMatchNode->u1Protocol);
    }
    else
    {
        u4IpAddr = pPartialMatchNode->RemoteIP.v4Addr;
        u2Port = (UINT2) FWL_REMOTE_PORT (pPartialMatchNode,
                                          pPartialMatchNode->u1Protocol);
    }

    TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
    {
        pState = (tStatefulSessionNode *)
            TMO_HASH_Get_First_Bucket_Node (FWL_STATE_HASH_LIST, u4HashIndex);

        while (pState != NULL)
        {

            if (pState->u1Direction == FWL_DIRECTION_IN)
            {
                u4IpAddr1 = pState->LocalIP.v4Addr;
                u2Port1 = (UINT2) FWL_LOCAL_PORT (pState, pState->u1Protocol);
            }
            else
            {
                u4IpAddr1 = pState->RemoteIP.v4Addr;
                u2Port1 = (UINT2) FWL_REMOTE_PORT (pState, pState->u1Protocol);
            }

            if ((u4IpAddr == u4IpAddr1) && (u2Port == u2Port1) &&
                (pState->u1Protocol == pPartialMatchNode->u1Protocol) &&
                ((pState->u1AppCallStatus == FWL_APP_UNHOLD) ||
                 (pState->u1AppCallStatus == FWL_APP_HOLD)))
            {
                pState->u1AppCallStatus = pPartialMatchNode->u1AppCallStatus;
            }

            pState = (tStatefulSessionNode *)
                TMO_HASH_Get_Next_Bucket_Node (FWL_STATE_HASH_LIST,
                                               u4HashIndex,
                                               (tTMO_HASH_NODE *) pState);

        }                        /*End of While-Loop */

    }                            /*End of TMO_HASH_Scan_Table */

    return (FWL_SUCCESS);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSearchStateTableEntry                         */
/*                                                                          */
/*    Description        : Searches for a state entry of a corresponding    */
/*                         Partial Entry                                    */
/*                                                                          */
/*    Input(s)           : pPartialMatchNode - Pointer to Partial           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pState - Pointer to State or NULL                */
/****************************************************************************/

tStatefulSessionNode *
FwlSearchStateTableEntry (tStatefulSessionNode * pPartialMatchNode)
{
    tStatefulSessionNode *pState = NULL;
    UINT4               u4HashIndex = FWL_ZERO;
    UINT4               u4IpAddr = FWL_ZERO;
    UINT4               u4IpAddr1 = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT2               u2Port1 = FWL_ZERO;

    pState = (tStatefulSessionNode *) NULL;
    u4HashIndex = FWL_ZERO;

    if (pPartialMatchNode->u1Direction == FWL_DIRECTION_IN)
    {
        u4IpAddr = pPartialMatchNode->LocalIP.v4Addr;
        u2Port = (UINT2) FWL_LOCAL_PORT (pPartialMatchNode,
                                         pPartialMatchNode->u1Protocol);
    }
    else
    {
        u4IpAddr = pPartialMatchNode->RemoteIP.v4Addr;
        u2Port = (UINT2) FWL_REMOTE_PORT (pPartialMatchNode,
                                          pPartialMatchNode->u1Protocol);
    }

    TMO_HASH_Scan_Table (FWL_STATE_HASH_LIST, u4HashIndex)
    {
        pState = (tStatefulSessionNode *)
            TMO_HASH_Get_First_Bucket_Node (FWL_STATE_HASH_LIST, u4HashIndex);

        while (pState != NULL)
        {

            if (pState->u1Direction == FWL_DIRECTION_IN)
            {
                u4IpAddr1 = pState->LocalIP.v4Addr;
                u2Port1 = (UINT2) FWL_LOCAL_PORT (pState, pState->u1Protocol);
            }
            else
            {
                u4IpAddr1 = pState->RemoteIP.v4Addr;
                u2Port1 = (UINT2) FWL_REMOTE_PORT (pState, pState->u1Protocol);
            }

            if ((u4IpAddr == u4IpAddr1) && (u2Port == u2Port1) &&
                (pState->u1Protocol == pPartialMatchNode->u1Protocol) &&
                ((pState->u1AppCallStatus == FWL_APP_UNHOLD) ||
                 (pState->u1AppCallStatus == FWL_APP_HOLD)))
            {
                return (pState);

            }
            else
            {
                pState = (tStatefulSessionNode *)
                    TMO_HASH_Get_Next_Bucket_Node
                    (FWL_STATE_HASH_LIST,
                     u4HashIndex, (tTMO_HASH_NODE *) pState);
            }

        }                        /*End of While-Loop */

    }                            /*End of TMO_HASH_Scan_Table */

    return (NULL);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlClosePinholeEntry                             */
/*                                                                          */
/*    Description        : Deletes the Firewall pinhole on request which    */
/*                         which was created for sip (partial, tcpinit or   */
 /*                        state)                                           */
/*                                                                          */
/*    Input(s)           : pPinholeInfo -- Pointer to the Partial Node      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS/FWL_FAILURE                          */
/****************************************************************************/

PUBLIC INT4
FwlClosePinholeEntry (tPartialInfo * pPinholeInfo)
{
    tStatefulSessionNode *pStateNode = NULL;
    INT4                i4Status = FWL_SUCCESS;

    i4Status = (INT4) FwlStateMemAllocate ((UINT1 **) (VOID *) &pStateNode);

    if (i4Status == FWL_FAILURE)
    {
        return (FWL_FAILURE);
    }

    pStateNode->LocalIP.v4Addr = pPinholeInfo->LocalIP.v4Addr;

    pStateNode->RemoteIP.v4Addr = pPinholeInfo->RemoteIP.v4Addr;

    pStateNode->u1Direction = pPinholeInfo->u1Direction;
    pStateNode->u1Protocol = pPinholeInfo->u1Protocol;

    if (pStateNode->u1Protocol == FWL_TCP)
    {
        FWL_LOCAL_TCP_PORT (pStateNode) = pPinholeInfo->u2LocalPort;
        FWL_REMOTE_TCP_PORT (pStateNode) = pPinholeInfo->u2RemotePort;
    }
    else
    {
        FWL_LOCAL_UDP_PORT (pStateNode) = pPinholeInfo->u2LocalPort;
        FWL_REMOTE_UDP_PORT (pStateNode) = pPinholeInfo->u2RemotePort;
    }

    pStateNode->u1AppCallStatus = pPinholeInfo->u1AppCallStatus;

    /* Deleting Partial Entry */
    FwlDeleteAppPartialListNode (pStateNode);

    FwlStateMemFree ((UINT1 *) pStateNode);

    return (i4Status);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeletePartialLinksListNodes                   */
/*                                                                          */
/*    Description        : Deletes the filter from the Stateful Filter List */
/*                                                                          */
/*    Input(s)           : u4Flush -- Flush type value in the timer         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlDeletePartialLinksListNodes (UINT4 u4Flush)
{
    tStatefulSessionNode *pPartialNode = NULL;
    tStatefulSessionNode *pNextNode = NULL;
    tTMO_SLL           *apPartialLst[FWL_THREE] =
        { &gFwlPartialLinksList, &gFwlV6PartialLinksList, NULL };
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4Timeout = FWL_ZERO;
    UINT4               u4NewTimeout = FWL_ZERO;
    UINT4               u4Count = FWL_ZERO;

    for (u4Count = FWL_ZERO; apPartialLst[u4Count] != NULL; u4Count++)
    {
        pPartialNode = (tStatefulSessionNode *) NULL;
        FWL_GET_SYS_TIME (&u4CurTime);
        pPartialNode = (tStatefulSessionNode *)
            TMO_SLL_First (apPartialLst[u4Count]);

        while (pPartialNode != NULL)
        {
            /* Partial link Timeout value. */
            u4Timeout = (pPartialNode->u1RemoteState == FWL_PERSISTENT) ?
                FWL_PERSIST_PARTIAL_LINK_TIMEOUT :
                FWL_NON_PERSIST_PARTIAL_LINK_TIMEOUT;

            u4NewTimeout = u4Timeout * FWL_SYS_TIME_FACTOR;

            if (((u4Flush == FWL_FLUSH_STATEFUL_ENTRIES) ||
                 ((u4CurTime - pPartialNode->u4Timestamp) > u4NewTimeout)) &&
                (pPartialNode->u1RemoteState != FWL_APP_PERSISTENT))
            {

                pNextNode =
                    (tStatefulSessionNode *)
                    TMO_SLL_Next (apPartialLst[u4Count],
                                  (tTMO_SLL_NODE *) pPartialNode);
                TMO_SLL_Delete (apPartialLst[u4Count],
                                (tTMO_SLL_NODE *) pPartialNode);

                FwlPartialLinksMemFree ((UINT1 *) pPartialNode);

                pPartialNode = pNextNode;

            }
            else
            {
                pPartialNode =
                    (tStatefulSessionNode *)
                    TMO_SLL_Next (apPartialLst[u4Count],
                                  (tTMO_SLL_NODE *) pPartialNode);
            }
        }                        /*End of While-Loop */
    }

    return FWL_SUCCESS;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchPartialLinksNode                         */
/*                                                                          */
/*    Description        : Matches the filter for the Pkt against Partial   */
/*                         Links SLL                                        */
/*                                                                          */
/*    Input(s)           : pStateIPHdr  -- Pointer to the IP Header         */
/*                         pStateHLInfo -- Pointer to the Transport Header  */
/*                         u1PktDirection -- Direction IN or OUT            */
/*                                                                          */
/*    Output(s)          : ppPartialLinkNode -- stores Pointer to matched   */
/*                                              node                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlMatchPartialLinksNode (tIpHeader * pStateIpHdr,
                          tHLInfo * pStateHLInfo,
                          UINT1 u1Direction,
                          tStatefulSessionNode ** ppPartialLinkNode)
{
    /* Scan the SLL. For each Node:
     *    1. Check 5-tuple.
     *    2. If matches then return node.
     */
    tStatefulSessionNode *pPartialLinkInfo = NULL;
    tTMO_SLL           *pPartialLst = NULL;
    tIp6Addr            ZeroIp6Addr;
    UINT4               u4Status = FWL_NOT_MATCH;
    tFwlIpAddr          aIpAddr[FWL_TWO];
    UINT2               au2Port[FWL_TWO] = { FWL_ZERO };
    UINT1               u1Local = FWL_ZERO;
    UINT1               u1Proto = FWL_ZERO;
    UINT1               u1Check = FWL_ZERO;

    MEMSET (&ZeroIp6Addr, FWL_ZERO, sizeof (tIp6Addr));

    u1Proto = pStateIpHdr->u1Proto;

    if ((u1Proto != FWL_TCP) && (u1Proto != FWL_UDP))
    {
        return FWL_FAILURE;
    }

    u1Local = (UINT1) (u1Direction == FWL_DIRECTION_IN);
    au2Port[FWL_INDEX_0] = pStateHLInfo->u2SrcPort;
    au2Port[FWL_INDEX_1] = pStateHLInfo->u2DestPort;

    FWL_IP_ADDR_COPY (&aIpAddr[FWL_INDEX_0], &pStateIpHdr->SrcAddr);
    FWL_IP_ADDR_COPY (&aIpAddr[FWL_INDEX_1], &pStateIpHdr->DestAddr);

    if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pPartialLst = &gFwlPartialLinksList;
    }
    else
    {
        pPartialLst = &gFwlV6PartialLinksList;
    }

    TMO_SLL_Scan (pPartialLst, pPartialLinkInfo, tStatefulSessionNode *)
    {
        if (pPartialLinkInfo != NULL)
        {
            /* Always for any Partial Link LocalIP, Proto and Direction are 
             * Known. The unknown combinations are as follows:
             *         1. LocalPort=0.
             *         2. RemotePort=0.
             *         3. RemoteIP=0, RemotePort=0.
             * All other fields are known and should match. */
            if ((pPartialLinkInfo->u1RemoteState != FWL_APP_PERSISTENT) ||
                (u1Direction == FWL_DIRECTION_IN))
            {
                /* For SIP incoming traffic match should be against local ip
                 * also*/
                if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_4)
                {
                    u1Check = (UINT1)
                        (pPartialLinkInfo->LocalIP.v4Addr ==
                         aIpAddr[u1Local].v4Addr);
                }
                else if (aIpAddr[u1Local].u4AddrType == FWL_IP_VERSION_6)
                {
                    if (Ip6AddrCompare
                        (pPartialLinkInfo->LocalIP.v6Addr,
                         aIpAddr[u1Local].v6Addr) == IP6_ZERO)
                    {
                        u1Check = FWL_TRUE;
                    }
                }
            }
            else
            {
                u1Check = FWL_TRUE;
            }
            if ((pPartialLinkInfo->u1Direction == u1Direction) &&
                (pPartialLinkInfo->u1Protocol == u1Proto) && (u1Check))
            {
                /* Check the REMOTE Port */
                if (FWL_REMOTE_PORT (pPartialLinkInfo, u1Proto) != FWL_ZERO)
                {
                    if (FWL_REMOTE_PORT (pPartialLinkInfo, u1Proto) !=
                        au2Port[!u1Local])
                    {
                        continue;
                    }
                }

                /* Check the REMOTE IP, addrtype would be set non 0 
                 * if theres an address */
                if ((pPartialLinkInfo->RemoteIP.v4Addr != FWL_ZERO) ||
                    (Ip6AddrCompare
                     (pPartialLinkInfo->RemoteIP.v6Addr,
                      ZeroIp6Addr) != IP6_ZERO))
                {
                    if ((aIpAddr[!u1Local].u4AddrType == FWL_IP_VERSION_4) &&
                        (pPartialLinkInfo->RemoteIP.v4Addr !=
                         aIpAddr[!u1Local].v4Addr))
                    {
                        continue;
                    }
                    else if ((aIpAddr[!u1Local].u4AddrType == FWL_IP_VERSION_6)
                             &&
                             (Ip6AddrCompare
                              (pPartialLinkInfo->RemoteIP.v6Addr,
                               aIpAddr[!u1Local].v6Addr) != IP6_ZERO))
                    {
                        continue;
                    }
                }
                else
                {
                    if (FWL_REMOTE_PORT (pPartialLinkInfo, u1Proto) != FWL_ZERO)
                    {
                        continue;
                    }
                }

                /* Check LOCAL Port */
                if (FWL_LOCAL_PORT (pPartialLinkInfo, u1Proto) != FWL_ZERO)
                {
                    if (FWL_LOCAL_PORT (pPartialLinkInfo, u1Proto) !=
                        au2Port[u1Local])
                    {
                        continue;
                    }
                }
                else
                {
                    /* addrtype would be set non 0 if theres an address */
                    if ((pPartialLinkInfo->RemoteIP.u4AddrType == FWL_ZERO) ||
                        (FWL_REMOTE_PORT (pPartialLinkInfo, u1Proto) ==
                         FWL_ZERO))
                    {
                        continue;
                    }
                }                /*End-of-Check-Local-Port */

                *ppPartialLinkNode = pPartialLinkInfo;
                return FWL_MATCH;
            }                    /*Check Proto Match */
        }                        /* pPartialLinkInfo != NULL */
    }                            /*End-of-TMO_SLL_Scan */

    return u4Status;

}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSearchPartialLinkNode                         */
/*                                                                          */
/*    Description        : Matches the filter for the Pkt against Partial   */
/*                         Links SLL                                        */
/*                                                                          */
/*    Input(s)           : pStateIPHdr  -- Pointer to the IP Header         */
/*                         pStateHLInfo -- Pointer to the Transport Header  */
/*                         u1PktDirection -- Direction IN or OUT            */
/*                                                                          */
/*    Output(s)          : ppPartialLinkNode -- stores Pointer to matched   */
/*                                              node                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlSearchPartialLinkNode (tIpHeader * pStateIpHdr,
                          tHLInfo * pStateHLInfo,
                          UINT1 u1PktDirection,
                          tStatefulSessionNode ** ppPartialLinkNode,
                          UINT1 u1AppCallStatus)
{
    /* Scan the SLL. For each Node:
     *    1. Check 5-tuple.
     *    2. If matches then return node. */
    tStatefulSessionNode *pPartialLinkInfo = NULL;
    tTMO_SLL           *pPartialLst = NULL;
    UINT4               u4Status = FWL_NOT_MATCH;
    UINT1               u1Proto = FWL_ZERO;

    /* 
     * If the state table entry is of sip traffic, don't
     * go ahead with the search, instead return success.
     */
    if ((u1AppCallStatus == FWL_APP_HOLD) ||
        (u1AppCallStatus == FWL_APP_UNHOLD))
    {
        return (FWL_SUCCESS);
    }

    u1Proto = pStateIpHdr->u1Proto;

    if ((u1Proto != FWL_TCP) && (pStateIpHdr->u1Proto != FWL_UDP))
    {
        return FWL_FAILURE;
    }

    if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pPartialLst = &gFwlPartialLinksList;
    }
    else
        pPartialLst = &gFwlV6PartialLinksList;

    TMO_SLL_Scan (pPartialLst, pPartialLinkInfo, tStatefulSessionNode *)
    {
        if (pPartialLinkInfo != NULL)
        {
            /* First check, if IP Address Direction and Protocol matches.... */
            if (!
                ((pPartialLinkInfo->LocalIP.v4Addr ==
                  pStateIpHdr->SrcAddr.v4Addr)
                 && (pPartialLinkInfo->RemoteIP.v4Addr ==
                     pStateIpHdr->DestAddr.v4Addr)
                 && (pPartialLinkInfo->u1Direction == u1PktDirection)
                 && (pPartialLinkInfo->u1Protocol == u1Proto)))
            {
                continue;
            }

            /* If IP addresses and proto match, check the port numbers. */
            if ((FWL_LOCAL_PORT (pPartialLinkInfo, u1Proto) ==
                 pStateHLInfo->u2SrcPort) &&
                (FWL_REMOTE_PORT (pPartialLinkInfo, u1Proto) ==
                 pStateHLInfo->u2DestPort))
            {
                /* Matching Node Found. */
                *ppPartialLinkNode = pPartialLinkInfo;
                u4Status = FWL_MATCH;
                break;
            }
        }                        /* pPartialLinkInfo != NULL */
    }                            /*End-of-TMO_SLL_Scan */
    return u4Status;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAppMatchPartialNode                           */
/*                                                                          */
/*    Description        : Matches the filter for an application in Partial */
/*                         Links SLL                                        */
/*                                                                          */
/*    Input(s)           : pStateIPHdr  -- Pointer to the IP Header         */
/*                         pStateHLInfo -- Pointer to the Transport Header  */
/*                         u1Direction -- Direction IN or OUT               */
/*                                                                          */
/*    Output(s)          : ppPartialLinkNode -- stores Pointer to matched   */
/*                                              node                        */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/

PUBLIC INT4
FwlAppMatchPartialNode (UINT4 u4LocIpAddr, UINT2 u2LocPort,
                        UINT4 u4RemoteIpAddr, UINT2 u2RemotePort,
                        UINT1 u1Direction, UINT1 u1ProtoNum,
                        tStatefulSessionNode ** ppPartialLinkNode)
{
    tStatefulSessionNode *pPartialLinkInfo = NULL;

    INT4                i4Status = FWL_NOT_MATCH;
    UINT4               u4IpAddr = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    UINT4               u4PartialIpAddr = FWL_ZERO;
    UINT2               u2PartialPort = FWL_ZERO;

    if ((u1ProtoNum != FWL_TCP) && (u1ProtoNum != FWL_UDP))
    {
        return FWL_FAILURE;
    }

    if (u1Direction == FWL_DIRECTION_IN)
    {
        u2Port = u2LocPort;
        u4IpAddr = u4LocIpAddr;
    }
    else
    {
        u2Port = u2RemotePort;
        u4IpAddr = u4RemoteIpAddr;
    }

    TMO_SLL_Scan (&gFwlPartialLinksList, pPartialLinkInfo,
                  tStatefulSessionNode *)
    {
        if (u1Direction == FWL_DIRECTION_IN)
        {
            /* For incoming traffic match should be against 
             * Local IP and Local Port */
            u4PartialIpAddr = pPartialLinkInfo->LocalIP.v4Addr;
            u2PartialPort = (UINT2) FWL_LOCAL_PORT (pPartialLinkInfo,
                                                    pPartialLinkInfo->
                                                    u1Protocol);
        }
        else
        {
            /* For outgoing traffic match should be against 
             * Remote IP and Remote Port */
            u4PartialIpAddr = pPartialLinkInfo->RemoteIP.v4Addr;
            u2PartialPort = (UINT2) FWL_REMOTE_PORT (pPartialLinkInfo,
                                                     pPartialLinkInfo->
                                                     u1Protocol);
        }

        if ((pPartialLinkInfo->u1Direction == u1Direction) &&
            (pPartialLinkInfo->u1Protocol == u1ProtoNum) &&
            (u4PartialIpAddr == u4IpAddr) && (u2PartialPort == u2Port))
        {
            *ppPartialLinkNode = pPartialLinkInfo;
            return FWL_MATCH;
        }
    }                            /*End-of-TMO_SLL_Scan */

    return i4Status;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdatePartialLink                             */
/*                                                                          */
/*    Description        : Updates an existing link with updated info       */
/*                                                                          */
/*    Input(s)           : pExistingLink -- Pointer to existing link        */
/*                         pUpdateLink -- Pointer to Update node            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlUpdatePartialLink (tPartialInfo * pExistingLink, tPartialInfo * pUpdateLink)
{
    tIpHeader           ExistingIpHdr;
    tIpHeader           UpdateIpHdr;
    tHLInfo             ExistingHLInfo;
    tHLInfo             UpdateHLInfo;
    tStatefulSessionNode UpdateSessionInfo;
    tStatefulSessionNode ExistingSessionInfo;
    tStatefulSessionNode *pExistingSessionInfo = NULL;
    tStatefulSessionNode *pPartialNode = NULL;
    tTMO_SLL           *pPartialLst = NULL;
    UINT1               u1PktDirection = FWL_ZERO;
    UINT1               u1FuncFlag = FWL_ADD_PARTIAL;
    UINT1               u1Status = FWL_FAILURE;
    INT4                i4Status = FWL_FAILURE;

    /* Input Validation. */
    if (pExistingLink == NULL)
    {
        return FWL_FAILURE;
    }

    /* Validate the Protocol. */
    if ((pExistingLink->u1Protocol != FWL_TCP) &&
        (pExistingLink->u1Protocol != FWL_UDP))
    {
        return FWL_FAILURE;
    }

    if (pExistingLink->LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pPartialLst = &gFwlPartialLinksList;
    }
    else
        pPartialLst = &gFwlV6PartialLinksList;

    /* Set the Functionality Flag. */
    (pUpdateLink == NULL) ? (u1FuncFlag = FWL_ADD_PARTIAL) :
        (u1FuncFlag = FWL_UPDATE_PARTIAL);
    /* Init the State Node */
    MEMSET (&ExistingSessionInfo, FWL_ZERO, sizeof (tStatefulSessionNode));
    /* Init the Node & Update for the existing Node information */
    FWL_FILL_PARTIAL_ENTRY (&ExistingIpHdr, &ExistingHLInfo, pExistingLink);

    if (u1FuncFlag == FWL_UPDATE_PARTIAL)
    {
        /* Init the Node & Update for the new Node information */
        MEMSET (&UpdateSessionInfo, FWL_ZERO, sizeof (tStatefulSessionNode));
        FWL_FILL_PARTIAL_ENTRY (&UpdateIpHdr, &UpdateHLInfo, pUpdateLink);
    }

    /* NOTE: The below code assumes that when this fn is called 
     * for update the u1Direction field values passed in the input 
     * variables(pExistingLink, pUpdateLink) are the same. */
    u1PktDirection = pExistingLink->u1Direction;

    switch (pExistingLink->u1Protocol)
    {
        case FWL_TCP:
        case FWL_UDP:
        {
            /* Following Table explains the logic applied below. */

         /*-------------------------------------------------------------------*/
            /* u1Func | Match | u4Persist | Action                               */
            /*   Flag | Found |   Flag    |                                      */
         /*-------------------------------------------------------------------*/
            /* ADD    | YES   | YES       | return SUCCESS.                      */
            /* ADD    | YES   | NO        | return SUCCESS.                      */
            /* ADD    | NO    | YES       | AddPartialLinkNode, return SUCCESS.  */
            /* ADD    | NO    | NO        | AddPartialLinkNode, return SUCCESS.  */
            /* UPDATE | YES   | YES       | Delete Found Node, Add Update Node.  */
            /* UPDATE | YES   | NO        | Delete Found Node, Add Update Node.  */
            /* UPDATE | NO    | YES       | Add Existing Node, Update Node.      */
            /* UPDATE | NO    | NO        | Add Update Node.                     */
         /*-------------------------------------------------------------------*/

            if (FwlSearchPartialLinkNode (&ExistingIpHdr, &ExistingHLInfo,
                                          u1PktDirection, &pExistingSessionInfo,
                                          pExistingLink->u1AppCallStatus)
                == FWL_MATCH)
            {
                if (u1FuncFlag == FWL_ADD_PARTIAL)
                {                /* Partial Link already exists. So, return. */
                    u1Status = FWL_SUCCESS;
                    break;
                }
                else
                {
                    /* Call API to delete this node, and then add new to do 
                     * overwriting. Thus, Delete the existing link. */

                    /* Delete the found Node and free the memory. */
                    TMO_SLL_Delete (pPartialLst,
                                    (tTMO_SLL_NODE *) pExistingSessionInfo);
                    FwlPartialLinksMemFree ((UINT1 *) pExistingSessionInfo);

                }
            }
            else                /* node not found */
            {
                if ((pExistingLink->u1PersistFlag == FWL_PERSISTENT) ||
                    (u1FuncFlag == FWL_ADD_PARTIAL))
                {
                    if (pExistingLink->u1PersistFlag == FWL_APP_PERSISTENT)
                    {
                        /* 
                         * Trying to get a match in the state table entry if 
                         * not found searching in partial list 
                         */
                        i4Status = FwlAppMatchPartialNode (pExistingLink->
                                                           LocalIP.v4Addr,
                                                           pExistingLink->
                                                           u2LocalPort,
                                                           pExistingLink->
                                                           RemoteIP.v4Addr,
                                                           pExistingLink->
                                                           u2RemotePort,
                                                           pExistingLink->
                                                           u1Direction,
                                                           pExistingLink->
                                                           u1Protocol,
                                                           &pPartialNode);
                        if (i4Status == FWL_MATCH)
                        {
                            return (FWL_SUCCESS);
                        }

                    }

                }

                /* Add partial link only. */
                FwlAddPartialLinkNode (&ExistingIpHdr, &ExistingHLInfo,
                                       pExistingLink->u1PersistFlag,
                                       u1PktDirection, FWL_LOG_BRF,
                                       pExistingLink->u1AppCallStatus);
                if (u1FuncFlag == FWL_ADD_PARTIAL)
                {
                    u1Status = FWL_SUCCESS;
                    break;
                }
            }
        }                        /* node not found */

            if (u1FuncFlag == FWL_UPDATE_PARTIAL)
            {
                /*add new node */
                u1Status =
                    (UINT1) FwlAddPartialLinkNode (&UpdateIpHdr, &UpdateHLInfo,
                                                   /*pUpdateLink->u1PersistFlag */
                                                   FWL_NON_PERSISTENT,
                                                   u1PktDirection, FWL_LOG_BRF,
                                                   FWL_ZERO);
            }
            /*End-of-Case */
            break;

        default:                /* Proto other than TCP/UDP/ICMP. */
            break;
    }                            /*End-of-Switch */

    return u1Status;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDeletePartialLink                             */
/*                                                                          */
/*    Description        : Function to delete partial link info from list.  */
/*                                                                          */
/*    Input(s)           : pLinkToDelete -- Pointer to link to be deleted.  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlDeletePartialLink (tPartialInfo * pLinkToDelete)
{
    tIpHeader           DeleteIpHdr;
    tHLInfo             DeleteHLInfo;
    tStatefulSessionNode DeleteSessionInfo;
    tStatefulSessionNode *pDeleteSessionInfo = NULL;
    tTMO_SLL           *pPartialLst = NULL;
    UINT1               u1PktDirection = FWL_ZERO;

    /* Validate Input */
    if (pLinkToDelete == NULL)
    {
        return FWL_FAILURE;
    }

    /* Validate the Protocol. */
    if ((pLinkToDelete->u1Protocol != FWL_TCP) &&
        (pLinkToDelete->u1Protocol != FWL_UDP))
    {
        return FWL_FAILURE;
    }

    /* We don't delete node with persistent flag up, Because it is
     * persistent link. To delete a persistent link first update the
     * link to become non-persistent link. And then call this function
     * for deleting this link. */
    if (pLinkToDelete->u1PersistFlag == FWL_PERSISTENT)
    {
        return FWL_FAILURE;
    }

    if (pLinkToDelete->LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pPartialLst = &gFwlPartialLinksList;
    }
    else if (pLinkToDelete->LocalIP.u4AddrType == FWL_IP_VERSION_6)
    {
        pPartialLst = &gFwlV6PartialLinksList;
    }
    else
    {
        return FWL_FAILURE;
    }

    MEMSET (&DeleteSessionInfo, FWL_ZERO, sizeof (tStatefulSessionNode));
    MEMSET (&DeleteHLInfo, FWL_ZERO, sizeof (tHLInfo));
    MEMSET (&DeleteIpHdr, FWL_ZERO, sizeof (tIpHeader));

    /* Delete the existing Node information */

    FWL_FILL_PARTIAL_ENTRY (&DeleteIpHdr, &DeleteHLInfo, pLinkToDelete);
    u1PktDirection = pLinkToDelete->u1Direction;

    switch (pLinkToDelete->u1Protocol)
    {
        case FWL_TCP:
        case FWL_UDP:
        {

            if (FwlSearchPartialLinkNode (&DeleteIpHdr, &DeleteHLInfo,
                                          u1PktDirection, &pDeleteSessionInfo,
                                          FWL_ZERO) == FWL_MATCH)
            {
                /*node found, now we will delete it and free the memory */
                TMO_SLL_Delete (pPartialLst,
                                (tTMO_SLL_NODE *) pDeleteSessionInfo);
                FwlPartialLinksMemFree ((UINT1 *) pDeleteSessionInfo);
                return FWL_SUCCESS;
            }
            return FWL_FAILURE;
        }                        /*End-of-Case */
            break;

        default:                /* Proto other than TCP/UDP/ICMP. */
            break;
    }                            /*End-of-Switch */
    return FWL_FAILURE;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAppNotifyStateTuple                           */
/*                                                                          */
/*    Description        : Function to Send Event to SIP, internally calls  */
/*                         Nat Event Utility for sending event with a tuple */
/*                                                                          */
/*    Input(s)           : pStateNode -- Pointer to node, from which tuples */
/*                         has to be send.                                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS/FWL_FAILURE                          */
/****************************************************************************/

INT4
FwlAppNotifyStateTuple (tStatefulSessionNode * pStateNode)
{
    UINT4               u4SrcIp = FWL_ZERO;
    UINT4               u4DestIp = FWL_ZERO;
    UINT2               u2SrcPort = FWL_ZERO;
    UINT2               u2DestPort = FWL_ZERO;

    if (pStateNode == NULL)
    {
        return (FWL_FAILURE);
    }

    if ((pStateNode->u1AppCallStatus != FWL_APP_UNHOLD) &&
        (pStateNode->u1AppCallStatus != FWL_APP_HOLD))
    {
        return (FWL_SUCCESS);
    }

    /* 
     * If the notification is for non-sip and non-rtp ports 
     * ((port % 2)!= 0) then don't bother about sending 
     * the notification.
     */

    /* 
     * Event Notification to sip is not required for pinholes 
     * created internally through port map add request 
     */
    if (pStateNode->u1Direction == FWL_DIRECTION_IN)
    {
        return (FWL_FAILURE);
    }

    /* 
     * Also for RTCP ports that is created through explicit pinhole request,
     * no need to send notification to sip 
     */
    if ((FWL_REMOTE_PORT (pStateNode, pStateNode->u1Protocol) %
         MODULO_DIVISOR_2) != FWL_ZERO)
    {
        return (FWL_FAILURE);
    }

    if (pStateNode->u1Direction == FWL_DIRECTION_OUT)
    {
        u4SrcIp = pStateNode->LocalIP.v4Addr;
    }
    else
    {
        u4DestIp = pStateNode->LocalIP.v4Addr;
    }

    /* LOCAL PORT */
    if (pStateNode->u1Direction == FWL_DIRECTION_OUT)
    {
        u2SrcPort = (UINT2) FWL_LOCAL_PORT (pStateNode, pStateNode->u1Protocol);
    }
    else
    {
        u2DestPort =
            (UINT2) FWL_LOCAL_PORT (pStateNode, pStateNode->u1Protocol);
    }

    if (pStateNode->u1Direction == FWL_DIRECTION_OUT)
    {
        u4DestIp = pStateNode->RemoteIP.v4Addr;
    }
    else
    {
        u4SrcIp = pStateNode->RemoteIP.v4Addr;
    }

    /* OUT PORT */
    if (pStateNode->u1Direction == FWL_DIRECTION_OUT)
    {
        u2DestPort =
            (UINT2) FWL_REMOTE_PORT (pStateNode, pStateNode->u1Protocol);
    }
    else
    {
        u2SrcPort =
            (UINT2) FWL_REMOTE_PORT (pStateNode, pStateNode->u1Protocol);
    }

    UNUSED_PARAM (u4SrcIp);
    UNUSED_PARAM (u4DestIp);
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u2DestPort);

    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlProcessPartialLinks                           */
/*                                                                          */
/*    Description        : Inspects the sessions count and limits the       */
/*                         new session based on direction                   */
/*                                                                          */
/*    Input(s)           : pBuf - Pointer to the packet                     */
/*                         pIpHdr - IP Header info                          */
/*                         pHLInfo - TCP/UDP header info                    */
/*                         u4IfaceNum - Interface number                    */
/*                         u1Direction - IN or OUT                          */
/*                                                                          */
/*    Output(s)          : ppOutUserSessionNode - store out session node    */
/*                                    if out node is found, otherwise NULL  */
/*                       : ppInUserSessionNode - store in session node      */
/*                                    if in node is found, otherwise NULL   */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlProcessPartialLinks (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tIpHeader * pStateIpHdr,
                        tHLInfo * pStateHLInfo,
                        UINT4 u4IfaceNum,
                        UINT1 u1Direction,
                        tStatefulSessionNode * pStateNode,
                        tOutUserSessionNode * pOutUserSessionNode,
                        tInUserSessionNode * pInUserSessionNode)
{
    UINT4               u4CurTime = FWL_ZERO;
    UINT4               u4Timeout = FWL_ZERO;
    tStatefulSessionNode NewStateNode;
    tTMO_SLL           *pPartialLst = NULL;
    UINT1               u1AppCallStatus = FWL_ZERO;

    u4CurTime = FWL_GET_SYS_TIME (&u4CurTime);

    if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_4)
    {
        pPartialLst = &gFwlPartialLinksList;
    }
    else if (pStateIpHdr->SrcAddr.u4AddrType == FWL_IP_VERSION_6)
    {
        pPartialLst = &gFwlV6PartialLinksList;
    }
    else
    {
        return FWL_FAILURE;
    }
    /* If Matching entry not found in Tcp-Init-Flow-Table then,
     * search in Partial-Links-Table. */
    if (FwlMatchPartialLinksNode (pStateIpHdr, pStateHLInfo,
                                  u1Direction, &pStateNode) != FWL_MATCH)
    {
        /* Entry Match Not Found in Partial Links Table. Then this          
         * pkt is for a NEW connection. */
        return FWL_NEW_SESSION;
    }
    else                        /* Partial-Links-Table Entry Match Found */
    {
        /* Check if the Current matched Partial-Link has timed-out. */

        /* Set Partial-link Timeout value. */
        u4Timeout = (pStateNode->u1RemoteState == FWL_PERSISTENT) ?
            FWL_PERSIST_PARTIAL_LINK_TIMEOUT :
            FWL_NON_PERSIST_PARTIAL_LINK_TIMEOUT;
        if (((u4CurTime - pStateNode->u4Timestamp) >=
             u4Timeout * FWL_SYS_TIME_FACTOR)
            && (pStateNode->u1RemoteState != FWL_APP_PERSISTENT))

        {
            /* Partial-Link has TIMED-OUT. */
            /* 1. Delete the Node Entry.
             * 2. If (SYN only Pkt) Return FWL_NEW_SESSION to do Rule-Check.
             *    Else Drop the Pkt. And Log it. */
            TMO_SLL_Delete (pPartialLst, (tTMO_SLL_NODE *) pStateNode);
            FwlPartialLinksMemFree ((UINT1 *) pStateNode);

            /* NOTE: Here we assume that there can be only one matching 
             * entry in the Partial Link Table. So do the same processing
             * as in the case of - Match Not Found in Partial Links Table.
             * So this TCP SYN Only pkt is for new connection. */
            return FWL_NEW_SESSION;
        }                        /*End-of-Timedout-Partial-Link-Entry-Processing. */
        /* DO MATCHED PARTIAL LINK ENTRY PROCESSING. */

        /* Partial Link has NOT Timed-out */
        /* Perform Pkt processing against the Matched Partial-Link */

        /* NOW, DO CONCURRENT SESSIONS CHECK TO VERIFY WHETHER THIS NEW
         * SESSION CAN BE ADDED OR NOT. */
        /* Do concurrent sessions check. If FULL drop Session */
        if (FwlConcurrentSessionsProcess (pBuf, pStateIpHdr,
                                          pStateHLInfo,
                                          u4IfaceNum, u1Direction,
                                          &pOutUserSessionNode,
                                          &pInUserSessionNode,
                                          FWL_DONT_UPDATE_TCP) == FWL_FAILURE)
        {
            /* NOTE:-
             * 1. Here the partial link is left as it is.     *
             * 2. Logging already done in above function call.*/
            return (FWL_FAILURE);
        }

/* Updating Call Hold status from Partial->(State Table / TcpInitFlow Table) */
        if (pStateNode->u1RemoteState == FWL_APP_PERSISTENT)
        {
            /* 
             * Updating back pointers so for passing SIP flag in
             * TcpInitFlow list node or Stateful table node 
             */
            u1AppCallStatus = pStateNode->u1AppCallStatus;
        }
        else
        {
            u1AppCallStatus = FWL_NON_SIP;
        }
        /* Now as Concurrent sessions check has allowed this new session,
         * add this to Init Flow Table if it is a TCP pkt else into StateTable
         * if it is a UDP pkt. */
        switch (pStateIpHdr->u1Proto)
        {
            case FWL_TCP:
            {
                if (FwlAddTcpInitFlowNode (pBuf, pStateIpHdr, pStateHLInfo,
                                           FWL_TCP_STATE_LISTEN,
                                           u1Direction, FWL_LOG_BRF,
                                           u1AppCallStatus) == FWL_FAILURE)
                {
                    /* Revert the Concurrent Session Update. */
                    FwlInitStateFilterNode (pStateIpHdr, pStateHLInfo,
                                            NULL, u1Direction,
                                            pStateNode->u1LogLevel,
                                            &NewStateNode);

                    /* Dropping the Session. And current pkt. Log Message */
                    FwlLogMessage (FWL_MEMORY_FAILURE, u4IfaceNum,
                                   pBuf, pStateNode->u1LogLevel,
                                   FWLLOG_CRITICAL_LEVEL, NULL);
                    return FWL_FAILURE;
                }

            }
                break;

            case FWL_UDP:
            {
                FwlInitStateFilterNode (pStateIpHdr, pStateHLInfo,
                                        NULL, u1Direction,
                                        pStateNode->u1LogLevel, &NewStateNode);

                NewStateNode.u1AppCallStatus = u1AppCallStatus;

                /* Return value is checked after breaking from Switch. */
                if (FwlAddStateFilter (&NewStateNode, u1Direction)
                    == FWL_FAILURE)
                {
                    /* Revert the Concurrent Session Update. */
                    FwlRemoveUserSession (FWL_DONT_FLUSH, &NewStateNode);

                    /* Dropping the Session. And current pkt. Log Message */
                    FwlLogMessage (FWL_MEMORY_FAILURE, u4IfaceNum,
                                   pBuf, pStateNode->u1LogLevel,
                                   FWLLOG_CRITICAL_LEVEL, NULL);
                    return FWL_FAILURE;
                }
            }
                break;

            default:            /* Partial Link Processing is only for TCP/UDP. */
                return FWL_FAILURE;
        }                        /*End-of-Switch */

        /* If the new session has been successfully added to TcpInitFlow
         * Table/State-Table then delete the matched Partial link from the 
         * Partial-Links Table if it is NON-PERSISTENT Partial-Link. */
        if (pStateNode->u1RemoteState == FWL_NON_PERSISTENT)
        {
            /* Delete this NON-PERSISTENT Partial-Link. */
            /* Delete the found Node and free the memory. */
            TMO_SLL_Delete (pPartialLst, (tTMO_SLL_NODE *) pStateNode);
            FwlPartialLinksMemFree ((UINT1 *) pStateNode);
        }
        return FWL_STATEFUL_ENTRY_MATCH;
    }                            /*End-of-Partial-Links-Table Entry Match Found */

}                                /*End-of-Function */

/****************************************************************************/
/*                           End of fwllinks.c                              */
/****************************************************************************/
