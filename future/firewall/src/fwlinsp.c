/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlinsp.c,v 1.22 2015/03/30 06:13:21 siva Exp $
 *
 * Description:This file is used to inspect the packets   
 *             against the configured filters/rules and   
 *             takes the action specified in it.         
 *
 *******************************************************************/
#include "fwlinc.h"
#ifdef SIP_WANTED
#include "sip.h"
#endif
#ifdef TR69_WANTED
#include "tr.h"
#endif
#include "secmod.h"
#include "fwlglob.h"
tFwlAclInfo         gFwlAclInfo;
UINT1               gu1FirewallStatus = FWL_ZERO;
tFwlMemPoolId       gFwlMemPoolId;
FS_ULONG            gu4FwlTmrListId;
UINT1               gu1FwlIfaceFlag = FWL_ZERO;
tTmrAppTimer        gFwlCurrentTmr;    /* for Firewall Log's systime */
tTmrAppTimer        gFwlStatefulTmr;    /* for stateful inspection */
int                 sock1Fd = 0;
tTMO_SLL            FwlUrlFilterList;    /* URL Filter List */
tTMO_SLL            gFwlTcpInitFlowList;    /* Pointer to TCP Init flow List */
tTMO_SLL            gFwlPartialLinksList;    /* Pointer to Partial Links List */
tStatefulSessionInfo gFwlStateInfo;    /* Global Firewall Session Table Info */
tTMO_SLL            gFwlV6TcpInitFlowList;    /* Pointer to TCP Init flow List */
tTMO_SLL            gFwlV6PartialLinksList;    /* Pointer to Partial Links List */
tStatefulSessionInfo gFwlV6StateInfo;    /* Global Firewall Session Table Info */
UINT1               gu1AclScheduleStatus = FWL_ACTIVE;

UINT4               gu4NonStdFtpPort = FWL_ZERO;
tIfaceInfo          gIfaceInfo;
/*****************************************************************************/
/*       P R O T O T Y P E    D E C L A R A T I O N S                        */
/*****************************************************************************/

PRIVATE UINT4 FwlCheckTinyFragment PROTO ((UINT2 u2FragOffset, UINT2 u2TotalLen,
                                           UINT1 u1IpHdrLen));

PRIVATE UINT4 FwlInspectFragment PROTO ((UINT1 u1Fragment,
                                         UINT2 u2MaxFragmentSize,
                                         UINT1 u1Proto,
                                         UINT2 u2FragOffset,
                                         UINT2 u2TotalLen,
                                         UINT1 u1IpHdrLen, UINT4 *u4Attack));

PRIVATE UINT4 FwlInspectIpOption PROTO ((UINT1 u1CnfgOption,
                                         UINT4 u4PktOpCode, UINT4 *u4Attack));

PRIVATE UINT4       FwlInspectPktConfigFilter
PROTO ((tIfaceInfo * pIfaceNode,
        tIpHeader * pIp,
        tHLInfo * pHLInfo,
        tIcmpInfo * pIcmp,
        UINT1 u1Direction, UINT1 *pu1LogTrigger, tCRU_BUF_CHAIN_HEADER * pBuf));

PRIVATE UINT4 FwlMatchPktConfigSimpleFilter PROTO ((tFilterInfo * pFilterInfo,
                                                    tIpHeader * pIpInfo,
                                                    tHLInfo * pHighLayInfo,
                                                    UINT1 u1RuleCondition));

PRIVATE UINT4 FwlCheckPort PROTO ((tFilterInfo * pFilterNode,
                                   tHLInfo * pHLHdr));

PRIVATE UINT4 FwlMatchPktConfigRule PROTO ((tAclInfo * pAclFilter,
                                            tIpHeader * pIpHdr,
                                            tHLInfo * pHLHdr));

PRIVATE UINT4 FwlChkDhcpPkts PROTO ((UINT4 u4IfaceIndex,
                                     UINT1 u1Direction,
                                     tFwlIpAddr u4SrcAddr,
                                     tFwlIpAddr u4DestAddr,
                                     UINT2 u2SrcPort, UINT2 u2DestPort));

PRIVATE UINT4 FwlCheckIfIsRipPacket PROTO ((tHLInfo * pHLInfo));

PRIVATE UINT4 FwlCheckIfIsRipngPacket PROTO ((tHLInfo * pHLInfo));

#ifdef TR69_WANTED
PRIVATE UINT4 FwlCheckIfIsTr69Packet PROTO ((tHLInfo * pHLInfo));
PRIVATE UINT2 FwlGetTr69AcsPortNum PROTO ((VOID));
UINT2               gu2Tr69AcsPortNum = FWL_ZERO;
#endif

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectPkt                                    */
/*                                                                          */
/*    Description        : Inspects the packets against the configured      */
/*                         filter/rule.                                     */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the packet            */
/*                         u4IfaceNum   -- interface number                 */
/*                         u1Direction -- Specifies IN or OUT Filter        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlInspectPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT4 u4IfaceNum, UINT1 u1Direction)
#else
PUBLIC UINT4
FwlInspectPkt (pBuf, u4IfaceNum, u1Direction)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfaceNum = FWL_ZERO;
     UINT1               u1Direction = FWL_ZERO;
#endif
{
    tIfaceInfo         *pIfaceInfo = NULL;
    tIpHeader           IpHdr;
    tIcmpInfo           IcmpHdr;
    tHLInfo             HLInfo;
    UINT4               u4Attack = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;
    UINT1               u1LogTrigger = FWL_LOG_NONE;
    UINT4               u4TransHdrLen = FWL_ZERO;
    UINT4               u4TotalHdrLen = FWL_ZERO;
    UINT4               u4DmzStatus = FWL_ZERO;
    UINT4               u4Index = FWL_ZERO;
    tFwlIpAddr          IfIpAddr;
    UINT1               u1Fragment = FWL_ZERO;
    UINT1               u1MoreFragBit = FWL_ZERO;
    UINT2               u2MaxFragmentSize = FWL_ZERO;
    tFwlIpAddr          u4TmpIpAddress;
    tIp6Addr           *pIp6Addr = NULL;
    tStatefulSessionNode NewStateNode;
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    tInUserSessionNode *pInUserSessionNode = NULL;
    tStatefulSessionNode *pStateNode = NULL;
#ifdef SIP_WANTED
    tSipSignalPort      SipSignalPort;
#endif
    UINT1               u1SignalPort = FALSE;
    UINT1               u1AddrMatch = OSIX_FALSE;
    UINT1               u1DmzHstCnt = FWL_ZERO;
    UINT2               u2FragOffsetBit = FWL_ZERO;
    tSecModuleData     *pSecData = (tSecModuleData *) & pBuf->ModuleData;
    tEnetV2Header      *pEthAddr = (tEnetV2Header *) pSecData->DestMACAddr;
    UINT1               u1NwType = FWL_ZERO;

    /* Initialise all Local variables */

    MEMSET (&IpHdr, FWL_ZERO, sizeof (tIpHeader));
    MEMSET (&IcmpHdr, FWL_ZERO, sizeof (tIcmpInfo));
    MEMSET (&HLInfo, FWL_ZERO, sizeof (tHLInfo));

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlInspectPkt \n");

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n IP Header Checking\n");

    u4Status = FwlUpdateIpHeaderInfoFromPkt (pBuf, &IpHdr);
    /*Before processing the packet check 
     * if the dest address is ISS and interface is a LAN interfece or
     * If packet is going out in a LAN interface 
     * In both the cases return SUCCESS without processing the packet*/
    SecUtilGetIfNwType (u4IfaceNum, &u1NwType);
    if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
    {
        if (((SecUtilIpIfIsOurAddress (IpHdr.DestAddr.v4Addr) == TRUE) &&
             (CFA_NETWORK_TYPE_LAN == u1NwType)) ||
            ((SEC_OUTBOUND == u1Direction)
             && (CFA_NETWORK_TYPE_WAN != u1NwType)))
        {
            return FWL_SUCCESS;
        }
    }
    else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
    {
        if (((Sec6UtilIpIfIsOurAddress (&(IpHdr.DestAddr.v6Addr), &u4Index) ==
              OSIX_SUCCESS) && (CFA_NETWORK_TYPE_LAN == u1NwType))
            || ((SEC_OUTBOUND == u1Direction)
                && (CFA_NETWORK_TYPE_WAN != u1NwType)))
        {
            return FWL_SUCCESS;
        }
    }
    switch (u4Status)
    {
        case FWL_INVALID_IP_VERSION:
            INC_INSPECT_COUNT;
            INC_DISCARD_COUNT;
            INC_V6_INSPECT_COUNT;
            INC_V6_DISCARD_COUNT;
            INC_ATTACKS_DISCARD_COUNT;
            INC_V6_ATTACKS_DISCARD_COUNT;

            if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            else if (IF_NUM_OF_FWL_V6_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }

            FwlLogMessage (FWL_INVALID_IP_VERSION,
                           u4IfaceNum, NULL, FWL_LOG_BRF,
                           FWLLOG_INFO_LEVEL, (UINT1 *) NULL);
            return FWL_FAILURE;

        case FWL_IPHDR_LEN_FIELDS_INVALID:
            /* Scenarios: 
             * IP header length is < 20 or > 60 
             * Total length is < IP header length
             * Actual received bytes of Pkt is <> Total-Length in IP Header */
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_DISCARD_COUNT;
                INC_ATTACKS_DISCARD_COUNT;
            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_DISCARD_COUNT;
                INC_V6_ATTACKS_DISCARD_COUNT;

            }
            if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            else if (IF_NUM_OF_FWL_V6_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            FwlLogMessage (FWL_IPHDR_LEN_FIELDS_INVALID,
                           u4IfaceNum, pBuf, FWL_LOG_BRF,
                           FWLLOG_CRITICAL_LEVEL, (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;

        case FWL_IP_PKT_TOO_BIG:
            /* Total-Length > pre-defined Maximum length, drop the packet */
            INC_INSPECT_COUNT;
            INC_DISCARD_COUNT;
            FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_ALERT_LEVEL,
                           (UINT1 *) FWL_MSG_PKT_TOO_BIG);
            return FWL_FAILURE;

        case FWL_INVALID_IP_OPTIONS:
        case FWL_ZERO_LEN_IP_OPTION:
        case FWL_TCP_SHTHDR:
        case FWL_UDP_SHTHDR:
        case FWL_ICMP_SHTHDR:
        case FWL_INVALID_NH:
        case FWL_INVALID_HOPLIMIT:
            /* Scenarios:
             *  - IP Packet with Invalid IP Options 
             *  - IP Option with Length exceeding IP-Options Field
             *  - IP Option with Zero Length 
             *  - TCP/UDP/ICMP short header
             */
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_DISCARD_COUNT;
                INC_ATTACKS_DISCARD_COUNT;
            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_DISCARD_COUNT;
                INC_V6_ATTACKS_DISCARD_COUNT;

            }
            if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            else if (IF_NUM_OF_FWL_V6_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            FwlLogMessage (u4Status, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            return FWL_FAILURE;
        default:
            break;
    }                            /*End-of-Switch-Case */

    /* If In/Out packet is matched with black list entry,
       Drop the packet */

    /* Updation of the necessary field in the IP header structure */
    FWL_DBG (FWL_DBG_CTRL_FLOW, "\n Control transfers to Buffer interface \n");

    /* Check the existence of the interface of IfaceNum in the List */
    pIfaceInfo = gFwlAclInfo.apIfaceList[u4IfaceNum];
    if ((pIfaceInfo == NULL) || (pIfaceInfo->u1RowStatus != FWL_ACTIVE))
    {
        /* Here it is assumed that all the WAN interfaces will be configured 
         * as Untrusted ports. So for LAN interfaces use the configuration 
         * that are set for an untrusted port */

        pIfaceInfo = gFwlAclInfo.apIfaceList[FWL_GLOBAL_IDX];
        if ((pIfaceInfo == NULL) || (pIfaceInfo->u1RowStatus != FWL_ACTIVE))
        {
            return FWL_FAILURE;
        }
    }

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n Tiny Fragment Checking\n");

    /* --------------------------------
     * Tiny Fragment Attack Prevention
     * --------------------------------
     * Prevent tiny fragment attack : Check this for both lan and wan. 
     * Not doing for LAN will result in printing invalid higher layer info
     */

    if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TINY_FRAGMENT_ATTACK))
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            if (IpHdr.u1Proto == FWL_TCP)
            {
                /* As per RFC 1858, Only TCP is vulnerable to Tiny Fragment Attack */
                if (FwlCheckTinyFragment (IpHdr.u2FragOffset, IpHdr.u2TotalLen,
                                          IpHdr.u1HeadLen) == FWL_SUCCESS)
                {
                    /* Tiny Fragmented Packet is to be denied. */
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                             "\n Fragment Matches, deny the Packet\n");
                    INC_FRAG_DISCARD_COUNT;
                    INC_IFACE_FRAG_DISCARD_COUNT (pIfaceInfo);
                    INC_IFACE_TINY_FRAG_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (FWL_TINY_FRAGMENT_ATTACK, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    goto FWL_DISCARD_PKT;
                }
            }
        }
    }

    if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
    {
        u2FragOffsetBit = FWL_FRAG_OFFSET_BIT;
        u1MoreFragBit = (UINT1) (IpHdr.u2FragOffset & FWL_MF_BIT_MASK);
    }
    else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
    {
        u2FragOffsetBit = FWL_IP6_FRAG_OFFSET_BIT;
        u1MoreFragBit = (UINT1) (IpHdr.u2FragOffset & FWL_IP6_MF_BIT_MASK);
    }

    /* Update HLInfo structure if the packet is UDP or TCP 
     * and the packet should have offset 0 i.e. first fragment or
     * a complete packet...*/
    if (((IpHdr.u1Proto == FWL_TCP) || (IpHdr.u1Proto == FWL_UDP)) &&
        ((IpHdr.u2FragOffset & u2FragOffsetBit) == FWL_ZERO))
    {
        FwlUpdateHLInfoFromPkt (pBuf, &HLInfo, IpHdr.u1Proto, IpHdr.u1HeadLen);
        /* Get the Transport(TCP/UDP) Header Length */
        u4TransHdrLen = FwlGetTransportHeaderLength (pBuf,
                                                     IpHdr.u1Proto,
                                                     IpHdr.u1HeadLen);
        if (IpHdr.u1Proto == FWL_TCP)
        {
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TCP_SHTHDR))
            {
                /* TCP Header length should be minimum 20 */
                if (u4TransHdrLen < FWL_TCP_HEADER_LEN)
                {
                    FwlLogMessage (FWL_TCP_SHTHDR, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    goto FWL_DISCARD_PKT;
                }
            }
            u4TotalHdrLen = IpHdr.u1HeadLen + u4TransHdrLen;
        }
        else if ((IpHdr.u1Proto == FWL_UDP) && (u1MoreFragBit == FWL_ZERO))
        {
            /* UDP packet and not a fragmented packet */
            /* Prevent possible targa2/saihyousen/nestea attacks
             * UDP length should be equal to (Total IP len - IP header len) */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_UDP_LEN_FIELD_INVALID))
            {
                if (u4TransHdrLen !=
                    (UINT4) (IpHdr.u2TotalLen - IpHdr.u1HeadLen))
                {
                    FwlLogMessage (FWL_UDP_LEN_FIELD_INVALID, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    goto FWL_DISCARD_PKT;
                }
            }
            u4TotalHdrLen = IpHdr.u2TotalLen;
        }
        else
        {
            /* Packet is UDP and 1st fragmemt */
            u4TotalHdrLen = IpHdr.u2TotalLen;
        }
    }

    /* Update ICMP Info structure if the packet is ICMP */
    if (((IpHdr.u1Proto == FWL_ICMP) || (IpHdr.u1Proto == FWL_ICMPV6)) &&
        ((IpHdr.u2FragOffset & u2FragOffsetBit) == FWL_ZERO))
    {
        /* Updation of Icmp type and Code in the ICMP info struct  */
        FwlUpdateIcmpv4v6InfoFromPkt (pBuf, &IcmpHdr, IpHdr.u1HeadLen);
    }

    if (FwlMatchBlackListEntry (u1Direction,
                                IpHdr.SrcAddr, IpHdr.DestAddr) == FWL_SUCCESS)
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            INC_INSPECT_COUNT;
            INC_DISCARD_COUNT;
        }
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_DISCARD_COUNT;
        }
        FwlLogMessage (FWL_BLACK_WHITE_LIST_IP_PKT, u4IfaceNum, pBuf,
                       FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                       (UINT1 *) FWL_MSG_BLACK_LIST_IP_ADDRESS);
        return FWL_FAILURE;
    }
    /* Check the packet is for a connection already established */
    u4Status = FwlPktStatefulInspect (pBuf, &IpHdr, &IcmpHdr, &HLInfo,
                                      u4TotalHdrLen, u1Direction,
                                      &u1LogTrigger);
    switch (u4Status)
    {
        case FWL_STATEFUL_ENTRY_MATCH:
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                           u1LogTrigger, FWLLOG_INFO_LEVEL,
                           (UINT1 *) FWL_MSG_MATCH);
            return FWL_SUCCESS;

        case FWL_NEW_SESSION:
        default:
            break;
    }                            /*End-of-Switch */

#ifdef SIP_WANTED
    /* Getting the currently used port for SIP signalling */
    SipUtilGetSignalPort (&SipSignalPort);
    /* Bypass Incoming SIP Packets */
    if ((HLInfo.u2SrcPort == SipSignalPort.u2TcpListenPort) ||
        (HLInfo.u2SrcPort == SipSignalPort.u2UdpListenPort) ||
        (HLInfo.u2SrcPort == SipSignalPort.u2TlsListenPort) ||
        (HLInfo.u2DestPort == SipSignalPort.u2TcpListenPort) ||
        (HLInfo.u2DestPort == SipSignalPort.u2UdpListenPort) ||
        (HLInfo.u2DestPort == SipSignalPort.u2TlsListenPort))
    {
        u1SignalPort = TRUE;
    }
#endif

    if (IpHdr.u1Proto == FWL_UDP)
    {
        /* Bypass RIP Packets */
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            u4Status = FwlCheckIfIsRipPacket (&HLInfo);
        }
        else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
        {
            u4Status = FwlCheckIfIsRipngPacket (&HLInfo);
        }
        switch (u4Status)
        {
            case FWL_SUCCESS:
                if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                {
                    INC_INSPECT_COUNT;
                    INC_PERMIT_COUNT;
                }
                else
                {
                    INC_V6_INSPECT_COUNT;
                    INC_V6_PERMIT_COUNT;
                }
                FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                               (UINT1 *) FWL_MSG_RIP_PKT);

                return FWL_SUCCESS;

                /* Let the packet go for further check */
            default:
                break;
        }

    }
    else if (IpHdr.u1Proto == FWL_TCP)
    {
#ifdef TR69_WANTED
        /* Bypass Tr69 Packets */
        if (FwlCheckIfIsTr69Packet (&HLInfo) == FWL_SUCCESS)
        {
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
            }
            return FWL_SUCCESS;
        }
#endif

    }
    else if (IpHdr.u1Proto == FWL_OSPFIGP)
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            INC_INSPECT_COUNT;
            INC_PERMIT_COUNT;
        }
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_PERMIT_COUNT;
        }
        FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                       FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                       (UINT1 *) FWL_MSG_OSPF_PKT);

        return FWL_SUCCESS;
    }
    else if (IpHdr.u1Proto == FWL_IGMP)
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            INC_INSPECT_COUNT;
            INC_PERMIT_COUNT;
        }
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_PERMIT_COUNT;
        }
        FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                       FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                       (UINT1 *) FWL_MSG_IGMP_PKT);

        return FWL_SUCCESS;
    }

    /* Update ICMP Info structure if the packet is ICMP */
    if (((IpHdr.u1Proto == FWL_ICMP) || (IpHdr.u1Proto == FWL_ICMPV6)) &&
        ((IpHdr.u2FragOffset & u2FragOffsetBit) == FWL_ZERO))
    {
        /* Updation of Icmp type and Code in the ICMP info struct  */
        FwlUpdateIcmpv4v6InfoFromPkt (pBuf, &IcmpHdr, IpHdr.u1HeadLen);
    }

    switch (IcmpHdr.u1Type)
    {
        case ROUTER_SOLICITATION:
        case ROUTER_ADVERTISEMENT:
        case NEIGHBOUR_SOLICITATION:
        case NEIGHBOUR_ADVERTISEMENT:
        case ROUTE_REDIRECT:
        {
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
            }
            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                           (UINT1 *) FWL_MSG_IGMP_PKT);

            return FWL_SUCCESS;
        }
        default:
            break;
    }

    /* On any interface (WAN or LAN), we can allow packets that are going out
     * from the interface itself */
    if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
    {
        if (CfaGetIfIpAddr ((INT4) u4IfaceNum, &(IfIpAddr.v4Addr)) ==
            OSIX_FAILURE)
        {
            FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
            return FWL_FAILURE;
        }
        if (IpHdr.SrcAddr.v4Addr == IfIpAddr.v4Addr)
        {
            u1AddrMatch = OSIX_TRUE;
        }

        u1DmzHstCnt = (UINT1) TMO_SLL_Count (&gFwlDmzList);
    }
    else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
    {
        pIp6Addr = Sec6UtilGetGlobalAddr (u4IfaceNum, NULL);
        if (pIp6Addr == NULL)
        {
            FWL_DBG (FWL_DBG_EXIT, "\n      Exiting from fn. FwlInspectPkt \n");
            return FWL_FAILURE;
        }
        MEMCPY (&(IfIpAddr.v6Addr), pIp6Addr, sizeof (tIp6Addr));
        if (Ip6AddrCompare (IpHdr.SrcAddr.v6Addr, IfIpAddr.v6Addr) == IP6_ZERO)
        {
            u1AddrMatch = OSIX_TRUE;
        }

        u1DmzHstCnt = (UINT1) TMO_SLL_Count (&gFwlIPv6DmzList);
    }

    if (((FWL_DIRECTION_OUT == u1Direction) && (u1AddrMatch == OSIX_TRUE))
        || (u1SignalPort == TRUE))
    {
#ifdef SIP_WANTED
        if (u1SignalPort == FALSE)
        {
#endif /* SIP_WANTED */
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                     "\n Source IP is same as our IP; So allowing Packet\n");
#ifdef SIP_WANTED
        }
        else
        {
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                     "\n SIP Signalling; So allowing Packet\n");
        }
#endif /* SIP_WANTED */

        if (FwlChkIfExtInterface (u4IfaceNum) == FWL_SUCCESS)
        {
            /* We have to complete the stateful table */
            switch (IpHdr.u1Proto)
            {
                case FWL_UDP:

                    u4Status =
                        FwlPktStatefulInspect (pBuf, &IpHdr, &IcmpHdr, &HLInfo,
                                               u4TotalHdrLen, u1Direction,
                                               &u1LogTrigger);

                    if (u4Status != FWL_STATEFUL_ENTRY_MATCH)
                    {
                        FwlInitStateFilterNode (&IpHdr, &HLInfo, &IcmpHdr,
                                                u1Direction, u1LogTrigger,
                                                &NewStateNode);

                        /* Return value is checked after breaking from Switch. */
                        u4Status =
                            FwlAddStateFilter (&NewStateNode, u1Direction);
                        if (u4Status == FWL_FAILURE)
                        {
                            FwlLogMessage (FWL_MEMORY_FAILURE, u4IfaceNum, pBuf,
                                           u1LogTrigger, FWLLOG_CRITICAL_LEVEL,
                                           (UINT1 *)
                                           FWL_MSG_STATEFUL_TABLE_FULL);
                            goto FWL_DISCARD_PKT;
                        }
                    }
                    break;

                case FWL_TCP:

                    u4Status =
                        FwlPktStatefulInspect (pBuf, &IpHdr, &IcmpHdr,
                                               &HLInfo, u4TotalHdrLen,
                                               u1Direction, &u1LogTrigger);

                    if (u4Status != FWL_STATEFUL_ENTRY_MATCH)
                    {
                        FwlMatchTcpInitFlowNode (&IpHdr, &HLInfo, u1Direction,
                                                 &u1LogTrigger, &pStateNode);

                        if (pStateNode == NULL)
                        {
                            FwlAddTcpInitFlowNode (pBuf, &IpHdr, &HLInfo,
                                                   FWL_NEW, u1Direction,
                                                   u1LogTrigger, FWL_ZERO);
                        }

                        if (u4Status == FWL_SUCCESS)
                        {
                            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                            {
                                INC_INSPECT_COUNT;
                                INC_PERMIT_COUNT;
                                INC_IFACE_PERMIT_COUNT (pIfaceInfo);
                            }
                            else
                            {
                                INC_V6_INSPECT_COUNT;
                                INC_V6_PERMIT_COUNT;
                                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);
                            }
                            /* The Packet is to be permitted */
                            return (FWL_SUCCESS);
                        }
                    }

                    break;

                default:
                    FwlPktStatefulInspect (pBuf, &IpHdr, &IcmpHdr, &HLInfo,
                                           u4TotalHdrLen, u1Direction,
                                           &u1LogTrigger);
                    break;
            }

        }
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            INC_INSPECT_COUNT;
            INC_PERMIT_COUNT;
            INC_IFACE_PERMIT_COUNT (pIfaceInfo);
        }
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_PERMIT_COUNT;
            INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);
        }

        return FWL_SUCCESS;
    }

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n Large Fragmentation Checking \n");

    /* --------------------------------
     * Large Fragment Attack Prevention
     * --------------------------------
     * As there is no configuration for fragment handling for LAN
     * interface, we make use of DEFAULT interface parameters */

    if ((FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TINY_FRAGMENT_ATTACK)) ||
        (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_LARGE_FRAGMENT_ATTACK)))
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            u1Fragment = gIfaceInfo.u1Fragment;
            u2MaxFragmentSize = gIfaceInfo.u2MaxFragmentSize;

            /* Check whether Fragment field is set in the interface node */
            if (u1Fragment != FWL_NO_FRAGMENT)
            {
                /* Check the packet against the tiny and large fragment */
                if (FwlInspectFragment (u1Fragment,
                                        u2MaxFragmentSize,
                                        IpHdr.u1Proto, IpHdr.u2FragOffset,
                                        IpHdr.u2TotalLen, IpHdr.u1HeadLen,
                                        &u4Attack) == FWL_SUCCESS)
                {
                    /* Fragmented Packet is to be denied. */
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                             "\n Fragment Matches, deny the Packet\n");
                    INC_FRAG_DISCARD_COUNT;
                    INC_IFACE_FRAG_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (u4Attack, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    goto FWL_DISCARD_PKT;
                }
            }
        }
    }
    if (((IpHdr.u1Proto) == FWL_UDP) &&
        (((HLInfo.u2SrcPort == FWL_DHCP_SERVER_PORT) &&
          (HLInfo.u2DestPort == FWL_DHCP_CLIENT_PORT)) ||
         ((HLInfo.u2SrcPort == FWL_DHCP_CLIENT_PORT) &&
          (HLInfo.u2DestPort == FWL_DHCP_SERVER_PORT)) ||
         ((HLInfo.u2SrcPort == FWL_DHCP_SERVER_PORT) &&
          (HLInfo.u2DestPort == FWL_DHCP_SERVER_PORT))))
    {
        if (FwlChkDhcpPkts (u4IfaceNum,
                            u1Direction,
                            IpHdr.SrcAddr,
                            IpHdr.DestAddr,
                            HLInfo.u2SrcPort, HLInfo.u2DestPort) == FWL_SUCCESS)
        {
            /* ICSA V08, V09, V11 & V12:
             * Log traffic from/to FSR's private/public interface */
            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf, FWL_LOG_BRF,
                           FWLLOG_INFO_LEVEL,
                           (FWL_DIRECTION_OUT == u1Direction) ?
                           (UINT1 *) FWL_MSG_TRUST_LAN_TO :
                           (UINT1 *) FWL_MSG_TRUST_LAN_FROM);
            return (FWL_SUCCESS);
        }
    }

    /* Check if there is any node available for DMZ 
     * If node is there then atleast one DMZ host is configured */
    if (u1DmzHstCnt)
    {
        u4DmzStatus = FwlInspectDmzPkt (pBuf, IpHdr, HLInfo, IcmpHdr,
                                        u4IfaceNum, u1Direction, u4TotalHdrLen);
        switch (u4DmzStatus)
        {
            case FWL_FAILURE:
            {
                return FWL_FAILURE;
            }
            case FWL_SUCCESS:
            {
                if (FwlProcessPktForRateLimiting (pBuf, pIfaceInfo, &HLInfo,
                                                  &IcmpHdr, u4IfaceNum,
                                                  IpHdr.u1Proto) == FWL_FAILURE)
                {
                    goto FWL_DISCARD_PKT;
                }

                u4Attack =
                    FwlCheckAttacks (pBuf, u4IfaceNum, &IpHdr, &HLInfo,
                                     &IcmpHdr, u1Direction, u4TransHdrLen);

                if (u4Attack != FWL_PERMITTED)
                {
                    INC_ATTACKS_DISCARD_COUNT;

                    if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
                    {
                        FwlGenerateAttackSumTrap ();
                    }
                    INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (u4Attack, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_DMZ_MSG_ATTACK);
                    goto FWL_DISCARD_PKT;
                }
                return FWL_SUCCESS;
            }
            default:
                break;
        }
    }                            /* DMZ check ends here */

    /* -------------------------------------------------------------------
     * 
     * Handle packets that are destined from/to the router's LAN interface 
     *
     * --------------------------------------------------------------------
     */
    if (FwlChkIfExtInterface (u4IfaceNum) != FWL_SUCCESS)
    {
        switch (u1Direction)
        {
            case FWL_DIRECTION_OUT:
                /* To prevent dead lock, dont log messages, when syslog packets 
                 * are being sent out */
                if (HLInfo.u2DestPort == SYSLOG_PORT)
                {
                    if ((IpHdr.u1IpVersion == FWL_IP_VERSION_4) &&
                        (SecUtilIpIfIsOurAddress (IpHdr.SrcAddr.v4Addr) ==
                         TRUE))
                    {
                        return (FWL_SUCCESS);
                    }
                    else if ((IpHdr.u1IpVersion == FWL_IP_VERSION_6) &&
                             (Sec6UtilIpIfIsOurAddress (&(IpHdr.SrcAddr.v6Addr),
                                                        &u4Index) ==
                              OSIX_SUCCESS))
                    {
                        return (FWL_SUCCESS);
                    }
                }
                break;

            case FWL_DIRECTION_IN:
                /* ICSA V18 & V19:
                 * Do attack prevention on inbound packets from LAN */
                u4Attack =
                    FwlCheckAttacks (pBuf, u4IfaceNum, &IpHdr, &HLInfo,
                                     &IcmpHdr, u1Direction, u4TransHdrLen);

                if (FWL_PERMITTED != u4Attack)
                {
                    FwlLogMessage (u4Attack, u4IfaceNum, pBuf, FWL_LOG_BRF,
                                   FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);

                    goto FWL_DISCARD_PKT;
                }

                /* ICSA V18: Targa attack generates random source ip address
                 * Drop this packet, if it does not belong to lan subnet or 
                 * there is no route to reach it in lan side */

                /* If the Mac does not belong to us then it implies it */
                /* is a switched packet and for the same we need not check */
                /* if it belongs to our LAN subnet */
                if (SecUtilCheckIfLocalMac (pEthAddr, u4IfaceNum) ==
                    CFA_SUCCESS)
                {
                    if (FwlProcessPktForLanInFiltering (&IpHdr) == FWL_FAILURE)
                    {
                        FwlLogMessage (FWL_INVALID_SRC_IP, u4IfaceNum, pBuf,
                                       FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                                       (UINT1 *) FWL_MSG_ATTACK);
                        goto FWL_DISCARD_PKT;
                    }
                }

                /* Handle packets destined to the router */
                /* Only syn packets are checked */
                if (((IpHdr.u1IpVersion == FWL_IP_VERSION_4) &&
                     (SecUtilIpIfIsOurAddress (IpHdr.DestAddr.v4Addr) == TRUE))
                    || ((IpHdr.u1IpVersion == FWL_IP_VERSION_6) &&
                        (Sec6UtilIpIfIsOurAddress (&(IpHdr.SrcAddr.v6Addr),
                                                   &u4Index) == OSIX_SUCCESS)))
                {
                    if (((IpHdr.u2FragOffset & u2FragOffsetBit) == FWL_ZERO) &&
                        ((HLInfo.u1Syn == FWL_SET)
                         && (HLInfo.u1Ack != FWL_SET)))
                        /* Rate limit tcp/udp/icmp traffic at LAN Interface */
                        if (FwlProcessRouterPktForRateLimiting
                            (pBuf, pIfaceInfo, &HLInfo, &IcmpHdr, u4IfaceNum,
                             IpHdr.u1Proto) == FWL_FAILURE)
                        {
                            goto FWL_DISCARD_PKT;
                        }
                }
                break;

            default:
                return (FWL_FAILURE);
        }

        u4TmpIpAddress = (FWL_DIRECTION_OUT == u1Direction) ?
            IpHdr.SrcAddr : IpHdr.DestAddr;

        /* ICSA V08, V09, V11 & V12: 
         *
         * Log the packets only if it is to/from the router
         */
        if (((IpHdr.u1IpVersion == FWL_IP_VERSION_4) &&
             ((SecUtilIpIfIsOurAddress (u4TmpIpAddress.v4Addr) == TRUE) ||
              (FwlValidateIpAddress (u4TmpIpAddress.v4Addr) == FWL_MCAST_ADDR)))
            || ((IpHdr.u1IpVersion == FWL_IP_VERSION_6) &&
                ((Sec6UtilIpIfIsOurAddress (&(u4TmpIpAddress.v6Addr), &u4Index)
                  == OSIX_SUCCESS)
                 || (Ip6AddrType (&(u4TmpIpAddress.v6Addr)) == ADDR6_MULTI))))
        {
            /* ICSA V08, V09, V11 & V12:
             * Log traffic from/to FSR's private/public interface */
            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf, FWL_LOG_BRF,
                           FWLLOG_INFO_LEVEL,
                           (FWL_DIRECTION_OUT == u1Direction) ?
                           (UINT1 *) FWL_MSG_TRUST_LAN_TO :
                           (UINT1 *) FWL_MSG_TRUST_LAN_FROM);
            /* Update the statistics */
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
            }
        }
        return (FWL_SUCCESS);
    }                            /* FwlChkIfExtInterface(u4IfaceNum) != FWL_SUCCESS */

    /* 
     * --------------
     * URL Filtering 
     * --------------
     */
    if ((gFwlAclInfo.u1UrlFilteringEnable == FWL_FILTERING_ENABLED)
        && (TMO_SLL_Count (&FwlUrlFilterList)))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                 "\n Processing for  URL Filtering \n");

        /* Check if current packet is a HTTP packet */
        if ((IpHdr.u1Proto == FWL_TCP) && (HLInfo.u2DestPort == FWL_HTTP))
        {
            /* Check the transport header length and return failure 
             * if found to be invalid. Also increment the stats upon failure
             */
            if (IpHdr.u2TotalLen == FWL_ZERO)
            {
                if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                {
                    INC_INSPECT_COUNT;
                    INC_DISCARD_COUNT;
                }
                else
                {
                    INC_V6_INSPECT_COUNT;
                    INC_V6_PERMIT_COUNT;
                }
                FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                               (UINT1 *) FWL_MSG_MALFORMED_PKT);
                return FWL_FAILURE;
            }
            else
            {
                /* Perform URL Filtering for HTTP Packets */
                if (FwlMatchUrlFilter (pBuf,
                                       u4TotalHdrLen,
                                       IpHdr.u2TotalLen - u4TotalHdrLen)
                    == FWL_MATCH)
                {
                    if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                    {
                        INC_INSPECT_COUNT;
                        INC_DISCARD_COUNT;
                    }
                    else
                    {
                        INC_V6_INSPECT_COUNT;
                        INC_V6_DISCARD_COUNT;
                    }
                    FwlLogMessage (FWL_URL_FIL, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                                   (UINT1 *) FWL_MSG_URL_FILTER);
                    return FWL_FAILURE;
                }
            }
        }
    }                            /*End of IF-Check for if URL-FILTERING is ENABLED. */

    /* If In/Out packet is matched with WhiteList entry,
     * Allow the packet */
    if (FwlMatchWhiteListEntry (u1Direction,
                                IpHdr.SrcAddr, IpHdr.DestAddr) == FWL_SUCCESS)
    {
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            INC_INSPECT_COUNT;
            INC_PERMIT_COUNT;
        }
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_PERMIT_COUNT;
        }
        FwlLogMessage (FWL_BLACK_WHITE_LIST_IP_PKT, u4IfaceNum, pBuf,
                       FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                       (UINT1 *) FWL_MSG_WHITE_LIST_IP_ADDRESS);
        return FWL_SUCCESS;
    }

   /*--------------------------------------------------------
    * 
    * From here, the packet is proccessed for WAN interface *
    * 
    --------------------------------------------------------*/

    /* Allow non-first fragmented packets if the ACL rule is 
     * "fragment permit" */

    if (IpHdr.u2FragOffset & u2FragOffsetBit)
    {

        /* Check the packet is for a connection already established */
        u4Status = FwlPktStatefulInspect (pBuf, &IpHdr, &IcmpHdr, &HLInfo,
                                          u4TotalHdrLen, u1Direction,
                                          &u1LogTrigger);
        switch (u4Status)
        {
            case FWL_STATEFUL_ENTRY_MATCH:
                if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                {
                    INC_INSPECT_COUNT;
                    INC_PERMIT_COUNT;
                    INC_IFACE_PERMIT_COUNT (pIfaceInfo);

                }
                else
                {
                    INC_V6_INSPECT_COUNT;
                    INC_V6_PERMIT_COUNT;
                    INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

                }
                FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                               u1LogTrigger, FWLLOG_INFO_LEVEL,
                               (UINT1 *) FWL_MSG_MATCH);
                return FWL_SUCCESS;

            case FWL_FAILURE:
                /* Return FWL_FAILURE */
                goto FWL_DISCARD_PKT;

            case FWL_NEW_SESSION:
            default:
                break;
        }                        /*End-of-Switch */

        u4Status = FwlInspectPktConfigFilter (&gIfaceInfo, &IpHdr, &HLInfo,
                                              &IcmpHdr, u1Direction,
                                              &u1LogTrigger, pBuf);
        switch (u4Status)
        {
            case FWL_FRAG_PERMIT:

                if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                {
                    INC_INSPECT_COUNT;
                    INC_PERMIT_COUNT;
                    INC_IFACE_PERMIT_COUNT (pIfaceInfo);

                }
                else
                {
                    INC_V6_INSPECT_COUNT;
                    INC_V6_PERMIT_COUNT;
                    INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

                }
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");

                /* The Packet is to be permitted */
                return (FWL_SUCCESS);

            case FWL_NOT_MATCH:
                goto FWL_DISCARD_PKT;

            case FWL_SUCCESS:
            default:
                break;
        }                        /* End-of-Switch */
    }                            /* non-first fragment */

    /* -----------------------------------
     * Validations for External Interface:
     * -----------------------------------
     *  1. Attack Checks and service independant filtering
     *  2. Stateful Inspection
     *  3. Rule-lookup (ACLs) 
     */

    if (u1Direction == FWL_DIRECTION_IN)
    {
        /* when a packet comes in from the global network, check if it is an 
         * attack and drop the packet if so.
         */
        u4Attack =
            FwlCheckAttacks (pBuf, u4IfaceNum, &IpHdr, &HLInfo, &IcmpHdr,
                             u1Direction, u4TransHdrLen);
        if (u4Attack != FWL_PERMITTED)
        {
            INC_ATTACKS_DISCARD_COUNT;

            if (IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT)
            {
                FwlGenerateAttackSumTrap ();
            }
            INC_IFACE_ATTACKS_DISCARD_COUNT (pIfaceInfo);
            FwlLogMessage (u4Attack, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_ATTACK);
            goto FWL_DISCARD_PKT;
        }

        /* Inspect the packets against the service independent filters */
        if (FwlProcessPktForServiceIndpFiltering (&IpHdr, &HLInfo,
                                                  pIfaceInfo, pBuf,
                                                  u4IfaceNum) == FWL_FAILURE)
        {
            goto FWL_DISCARD_PKT;
        }
    }                            /* direction is IN, check for all types of attacks */

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n Packet is processed Interface Specific Filter \n");

    /* Check whether IP option field is set in the interface node */
    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n Ip Options Checking \n");

    /* IP Option checking is applicable only for IPv4 */
    if ((IpHdr.u1IpVersion == FWL_IP_VERSION_4) &&
        ((IpHdr.u4Options != FWL_ZERO) ||
         (gIfaceInfo.u1IpOption != FWL_NO_OPTION)))
    {
        /* Check the packet against the IP options */
        if (IpHdr.u1HeadLen > FWL_IP_HEADER_LEN)
        {
            if (FwlInspectIpOption (gIfaceInfo.u1IpOption,
                                    IpHdr.u4Options, &u4Attack) == FWL_SUCCESS)
            {
                /* Packet is to be denied. */
                MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                         "\n Ip Option matches, deny the packet \n");
                INC_IP_OPTION_DISCARD_COUNT;
                INC_IFACE_IP_OPTION_DISCARD_COUNT (pIfaceInfo);
                INC_IFACE_SRC_ROUTE_DISCARD_COUNT (pIfaceInfo);
                FwlLogMessage (u4Attack, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_ATTACK);
                goto FWL_DISCARD_PKT;
            }
        }
    }

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n ICMP Type and Code Checking \n");
    if ((IpHdr.u1Proto == FWL_ICMP) || (IpHdr.u1Proto == FWL_ICMPV6))
    {
        if (FwlProcessPktForRateLimiting (pBuf, pIfaceInfo, &HLInfo,
                                          &IcmpHdr, u4IfaceNum, IpHdr.u1Proto)
            == FWL_FAILURE)
        {
            goto FWL_DISCARD_PKT;
        }

        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            u4Status = FwlInspectIcmpOption (pBuf, pIfaceInfo, &IpHdr, &IcmpHdr,
                                             u4IfaceNum, u1Direction);
        }
        else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
        {
            u4Status =
                FwlInspectIcmp6Option (pBuf, pIfaceInfo, &IpHdr, &IcmpHdr,
                                       u4IfaceNum, u1Direction);
        }

        switch (u4Status)
        {
            case FWL_IN_ICMP_ALLOW:
            case FWL_OUT_ICMP_ALLOW:
                return (FWL_SUCCESS);

            case FWL_IN_ICMP_DISCARD:
            case FWL_OUT_ICMP_DISCARD:
                return (FWL_FAILURE);

            default:
                break;
        }                        /*End-of-Switch */
    }                            /*End-of-IF-Block */

    /* Check the packet is for a connection already established */
    u4Status = FwlPktStatefulInspect (pBuf, &IpHdr, &IcmpHdr, &HLInfo,
                                      u4TotalHdrLen, u1Direction,
                                      &u1LogTrigger);
    switch (u4Status)
    {
        case FWL_STATEFUL_ENTRY_MATCH:
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                           u1LogTrigger, FWLLOG_INFO_LEVEL,
                           (UINT1 *) FWL_MSG_MATCH);
            return FWL_SUCCESS;

        case FWL_FAILURE:
            /* Return FWL_FAILURE */
            goto FWL_DISCARD_PKT;

        case FWL_NEW_SESSION:
        default:
            break;
    }                            /*End-of-Switch */

    /* ICSA V18+ */
    if (FwlProcessPktForRateLimiting (pBuf, pIfaceInfo, &HLInfo,
                                      &IcmpHdr, u4IfaceNum, IpHdr.u1Proto)
        == FWL_FAILURE)
    {
        goto FWL_DISCARD_PKT;
    }

    /* Check the packet against the configured 
     * IN_FILTER and OUT_FILTER */
    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                  "\n Checking against Configured InFilter on interface %d \n",
                  u4IfaceNum);

    u4Status =
        FwlInspectPktConfigFilter (&gIfaceInfo, &IpHdr, &HLInfo, &IcmpHdr,
                                   u1Direction, &u1LogTrigger, pBuf);
    switch (u4Status)
    {
        case FWL_FRAG_PERMIT:

            INC_INSPECT_COUNT;
            INC_PERMIT_COUNT;
            INC_IFACE_PERMIT_COUNT (pIfaceInfo);
            FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");

            /* The Packet is to be permitted */
            return (FWL_SUCCESS);

        case FWL_NOT_MATCH:
            goto FWL_DISCARD_PKT;

        case FWL_SUCCESS:
        default:
            break;
    }                            /*End-of-Switch */

    /* If the access list allowed this packet, the packet is valid.
     * do concurrent session limiting. */

    switch (IpHdr.u1Proto)
    {
        case FWL_TCP:
        case FWL_UDP:
        case FWL_ICMP:
        case FWL_ICMPV6:

            pOutUserSessionNode = NULL;
            pInUserSessionNode = NULL;

            if (FwlInspectConcurrentSessions (pBuf, &IpHdr, &HLInfo,
                                              u4IfaceNum, u1Direction,
                                              &pOutUserSessionNode,
                                              &pInUserSessionNode)
                == FWL_FAILURE)
            {
                goto FWL_DISCARD_PKT;
            }
            break;

        default:
            /* Stateful session is maintained only for TCP/UDP/ICMP.
             * So for other protocols, allow the packet, increment
             * the statistics and log the message*/
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                           (UINT1 *) FWL_MSG_MATCH);
            FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
            /* The Packet is to be permitted */
            return (FWL_SUCCESS);
    }                            /*End-of-Switch */

    /* The packet is valid and the session is within the threshold. 
     * So record the connection in the state table. 
     * If it is a TCP packet then, add entry into tcp init flow list. 
     * If it is a UDP or ICMP packet add entry into Stateful-Table 
     */
    switch (IpHdr.u1Proto)
    {
        case FWL_TCP:
        {
            u4Status = FwlAddTcpInitFlowNode (pBuf, &IpHdr, &HLInfo, FWL_NEW,
                                              u1Direction, u1LogTrigger,
                                              FWL_ZERO);
            if (u4Status == FWL_SUCCESS)
            {
                if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                {
                    INC_INSPECT_COUNT;
                    INC_PERMIT_COUNT;
                    INC_IFACE_PERMIT_COUNT (pIfaceInfo);

                }
                else
                {
                    INC_V6_INSPECT_COUNT;
                    INC_V6_PERMIT_COUNT;
                    INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

                }

                FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                               u1LogTrigger, FWLLOG_INFO_LEVEL,
                               (UINT1 *) FWL_MSG_MATCH);
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
                /* The Packet is to be permitted */
                return (FWL_SUCCESS);
            }
            break;
        }                        /* case TCP */

        case FWL_UDP:
        case FWL_ICMP:
        case FWL_ICMPV6:
        {
            FwlInitStateFilterNode (&IpHdr, &HLInfo, &IcmpHdr, u1Direction,
                                    u1LogTrigger, &NewStateNode);

            /* Return value is checked after breaking from Switch. */
            u4Status = FwlAddStateFilter (&NewStateNode, u1Direction);
            break;
        }

        default:
            /* Stateful session is maintained only for TCP/UDP/ICMP.
             * So for other protocols, allow the packet, increment
             * the statistics and log the message*/
            if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
            {
                INC_INSPECT_COUNT;
                INC_PERMIT_COUNT;
                INC_IFACE_PERMIT_COUNT (pIfaceInfo);

            }
            else
            {
                INC_V6_INSPECT_COUNT;
                INC_V6_PERMIT_COUNT;
                INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

            }

            FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_INFO_LEVEL,
                           (UINT1 *) FWL_MSG_MATCH);
            FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
            /* The Packet is to be permitted */
            return (FWL_SUCCESS);
    }                            /*End-of-Switch */

    /* Log and Return Failure, if (u4Status == FWL_FAILURE). */
    if (FWL_FAILURE == u4Status)
    {
        FwlLogMessage (FWL_MEMORY_FAILURE, u4IfaceNum, pBuf,
                       u1LogTrigger, FWLLOG_CRITICAL_LEVEL,
                       (UINT1 *) FWL_MSG_STATEFUL_TABLE_FULL);
        goto FWL_DISCARD_PKT;
    }

    if (FwlUpdateConcurrentSessionInfo (IpHdr, HLInfo, u1Direction,
                                        pOutUserSessionNode,
                                        pInUserSessionNode) == FWL_SUCCESS)
    {
        /* TftpAlgProcessing to be done only for IPv6 traffic, for IPv4
         * irrespective of NAT enabled/disabled (only run time) partial links
         * will be added from nat alg */
        if ((IpHdr.u1IpVersion == FWL_IP_VERSION_6) &&
            (IpHdr.u1Proto == FWL_UDP) && (HLInfo.u2DestPort == FWL_TFTP_PORT))
        {
            TftpAlgProcessing (&IpHdr, &HLInfo, u1Direction);
        }
        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            INC_INSPECT_COUNT;
            INC_PERMIT_COUNT;
            INC_IFACE_PERMIT_COUNT (pIfaceInfo);

        }
        else
        {
            INC_V6_INSPECT_COUNT;
            INC_V6_PERMIT_COUNT;
            INC_V6_IFACE_PERMIT_COUNT (pIfaceInfo);

        }

        FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                       u1LogTrigger, FWLLOG_INFO_LEVEL,
                       (UINT1 *) FWL_MSG_MATCH);

        FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
        return FWL_SUCCESS;
    }
    else
    {
        /* This happens, when the system is no longer having enough
         * memory. So delete the stateful node for this session.
         * Log the error message as resource unavailable */

        /* If it is a TCP packet then, delete the entry from 
         * tcp init flow list.
         * Else if it is a UDP or ICMP packet delete entry from 
         * Stateful-Table
         */

        /* Log the error message as resource unavailable */
        FwlLogMessage (FWL_MEMORY_FAILURE, u4IfaceNum, pBuf,
                       u1LogTrigger, FWLLOG_CRITICAL_LEVEL, NULL);

    }

  FWL_DISCARD_PKT:
    if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
    {
        INC_INSPECT_COUNT;
        INC_DISCARD_COUNT;
        INC_IFACE_DISCARD_COUNT (pIfaceInfo);
    }
    else
    {
        INC_V6_INSPECT_COUNT;
        INC_V6_DISCARD_COUNT;
        INC_V6_IFACE_DISCARD_COUNT (pIfaceInfo);
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlInspectPkt \n");
    return FWL_FAILURE;
}                                /* End of the function -- FwlInspectPkt  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckIfIsRipPacket                            */
/*                                                                          */
/*    Description        : Inspects the UDP broadcast packet, if it is RIP  */
/*                         packet then allow the same.                      */
/*                                                                          */
/*    Input(s)           : pHLInfo  -- Contains info about the Port Nums.   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_RIP_BROADCAST_PKT - On Success.              */
/*                         FWL_FAILURE - On Failure.                        */
/****************************************************************************/

UINT4
FwlCheckIfIsRipPacket (tHLInfo * pHLInfo)
{
    /* 
     * For UDP protocol, allow Firewall to accept RIP broadcast
     * packet, the source and destination port will be 520.
     */
    if (pHLInfo->u2DestPort == FWL_UDP_RIP_PORT)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }
}

/******************************************************************************/
/*                                                                            */
/*    Function Name      : FwlCheckIfIsRipngPacket                            */
/*                                                                            */
/*    Description        : Inspects the UDP broadcast packet, if it is RIPng  */
/*                         packet then allow the same.                        */
/*                                                                            */
/*    Input(s)           : pHLInfo  -- Contains info about the Port Nums.     */
/*                                                                            */
/*    Output(s)          : None.                                              */
/*                                                                            */
/*    Returns            : FWL_RIP_BROADCAST_PKT - On Success.                */
/*                         FWL_FAILURE - On Failure.                          */
/******************************************************************************/

UINT4
FwlCheckIfIsRipngPacket (tHLInfo * pHLInfo)
{
    /* 
     * For UDP protocol, allow Firewall to accept RIPng
     * packet, the source and destination port will be 521.
     */
    if (pHLInfo->u2DestPort == FWL_UDP_RIPng_PORT)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }
}

#ifdef TR69_WANTED
/****************************************************************************/
/*    Function Name      : FwlSetTr69AcsPortNum                             */
/*    Description        : Sets the ACS port Number                         */
/*    Input(s)           : u2PortNo - Port Number                           */
/*    Output(s)          : None                                             */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
FwlSetTr69AcsPortNum (UINT2 u2PortNo)
{
    gu2Tr69AcsPortNum = u2PortNo;
    return;
}

/****************************************************************************/
/*    Function Name      : FwlGetTr69AcsPortNum                             */
/*    Description        : Gets the ACS Port Number                         */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*    Returns            : ACS port Number                                  */
/****************************************************************************/
UINT2
FwlGetTr69AcsPortNum ()
{
    return gu2Tr69AcsPortNum;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckIfIsTr69Packet                           */
/*                                                                          */
/*    Description        : Inspects the TCP packet, if it is Tr69           */
/*                         packet then allow the same.                      */
/*                                                                          */
/*    Input(s)           : pHLInfo  -- Contains info about the Port Nums.   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS / FWL_FAILURE                        */
/****************************************************************************/
UINT4
FwlCheckIfIsTr69Packet (tHLInfo * pHLInfo)
{
    UINT2               u2PortNo;
    /* 
     * For TCP protocol, allow Firewall to accept Tr69 packet.
     */
    u2PortNo = FwlGetTr69AcsPortNum ();

    if ((u2PortNo != FWL_ZERO) &&
        ((u2PortNo == pHLInfo->u2DestPort) || (u2PortNo == pHLInfo->u2SrcPort)))
    {
        return FWL_SUCCESS;
    }

    return FWL_FAILURE;
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectFragment                               */
/*                                                                          */
/*    Description        : Inspects the packets for Tiny and Large Fragment */
/*                                                                          */
/*    Input(s)           : u1Fragment -- Fragment field in the interfaceInfo*/
/*                         u2MaxFragmentSize - Max Fragment size filed in   */
/*                                             the interfaceInfo            */
/*                         u1Proto    -- Protocol type in IP pkt            */
/*                         u2FragOffset - Fragment offset value of IP pkt   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is fragmented, otherwise   */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
#ifdef __STDC__
PRIVATE UINT4
FwlInspectFragment (UINT1 u1Fragment, UINT2 u2MaxFragmentSize, UINT1 u1Proto,
                    UINT2 u2FragOffset, UINT2 u2TotalLen, UINT1 u1IpHdrLen,
                    UINT4 *u4Attack)
#else
PRIVATE UINT4
FwlInspectFragment (u1fragment, u2MaxFragmentSize, u1Proto,
                    u2FragOffset, u2TotalLen, u1IpHdrLen, u4Attack)
     UINT1               u1Fragment = FWL_ZERO;
     UINT2               u2MaxFragmentSize = FWL_ZERO;
     UINT1               u1Proto = FWL_ZERO;
     UINT2               u2FragOffset = FWL_ZERO;
     UINT2               u2TotalLen = FWL_ZERO;
     UINT1               u1IpHdrLen = FWL_ZERO;
     UINT4              *u4Attack;
#endif
{
    UINT1               u1FragHit = FWL_NOT_MATCH;
    UINT2               u2FragOffsetFlags = FWL_ZERO;
    UINT2               u2FragOffsetValue = FWL_ZERO;
    UINT2               u2IpPktLen = FWL_ZERO;

    UNUSED_PARAM (u1Proto);
    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlInspectPktFragment \n");

    /* Get the fragment offset flags: High order 3 bits
     * * Rsvd : DF : MF */
    u2FragOffsetFlags = (u2FragOffset & FWL_FRAG_FLAG_BIT);

    /* Get the fragment offset value : Low order 13 bits */
    u2FragOffsetValue = (u2FragOffset & FWL_FRAG_OFFSET_BIT);

    /* Check the packet for Tiny Fragment */
    /* RFC 1858 details about the Tiny fragment attack */
    if ((u2FragOffsetValue == FWL_ZERO) && (u1Proto == FWL_TCP))
    {
        if (u2FragOffsetFlags & FWL_MF_BIT_MASK)    /* Check if MF bit is set */
        {
            /* offset value will be multiple of 8 bytes. 
             * If the total length is less than 
             * (IP header length + offset length),
             * then the packet should be dropped */

            u2IpPktLen = (UINT2) (u1IpHdrLen + (FWL_FRAG_OFFSET_TINY *
                                                FWL_EIGHT_BYTES));

            if (u2TotalLen < u2IpPktLen)
            {
                *u4Attack = FWL_TINY_FRAGMENT_ATTACK;

                /* Update the global statistics */
                INC_TINY_FRAG_ATTACK_DISCARD_COUNT;
                u1FragHit = FWL_MATCH;
            }
        }
    }

    else if ((u2FragOffsetValue <= FWL_FRAG_OFFSET_TINY)
             && (u1Proto == FWL_TCP))
    {
        *u4Attack = FWL_TINY_FRAGMENT_ATTACK;

        /* Update the global statistics */
        INC_TINY_FRAG_ATTACK_DISCARD_COUNT;
        u1FragHit = FWL_MATCH;
    }

    /* Checking the packet for Large fragment */
    else if ((u1Fragment == FWL_LARGE_FRAGMENT) ||
             (u1Fragment == FWL_ANY_FRAGMENT))
    {
        /* Offset value is multiple of 8 bytes */
        if ((u2FragOffsetValue * FWL_EIGHT) > u2MaxFragmentSize)
        {
            *u4Attack = FWL_LARGE_FRAGMENT_ATTACK;

            /* Update the global statistics */
            INC_LARGE_FRAG_ATTACK_DISCARD_COUNT;
            u1FragHit = FWL_MATCH;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlInspectPktFragment \n");

    if (u1FragHit == FWL_MATCH)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }

}                                /* End of the function -- FwlInspectPktFragment */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectIpOption                               */
/*                                                                          */
/*    Description        : Inspects the packets for IP options              */
/*                                                                          */
/*    Input(s)           : u1CnfgOption - Ip Option configured in Iface node*/
/*                         u1PktOpCode  - Ip Option code bit in the pkt     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet matches the configured IP  */
/*                         option, otherwise FWL_FAILURE.                   */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlInspectIpOption (UINT1 u1CnfgOption, UINT4 u4PktOpCode, UINT4 *u4Attack)
#else
PRIVATE UINT4
FwlInspectIpOption (u1CnfgOption, u4PktOpCode, u4Attack)
     UINT1               u1CnfgOption = FWL_ZERO;
     UINT4               u4PktOpCode = FWL_ZERO;
     UINT1              *u4Attack;
#endif
{
    UINT4               u4Status = FWL_FAILURE;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlInspectIpOption \n");

    switch (u1CnfgOption)
    {
        case FWL_ANY_OPTION:
            *u4Attack = FWL_IP_OPTION;
            if ((u4PktOpCode & FWL_IP_OPT_LSROUTE_BIT) ||
                (u4PktOpCode & FWL_IP_OPT_SSROUTE_BIT))
            {
                INC_SRC_ROUTE_ATTACK_DISCARD_COUNT;
            }
            u4Status = FWL_SUCCESS;
            break;

        case FWL_SOURCE_ROUTE_OPTION:
            if ((u4PktOpCode & FWL_IP_OPT_LSROUTE_BIT) ||
                (u4PktOpCode & FWL_IP_OPT_SSROUTE_BIT))
            {
                *u4Attack = FWL_SOURCE_ROUTE_ATTACK;
                INC_SRC_ROUTE_ATTACK_DISCARD_COUNT;
                u4Status = FWL_SUCCESS;
            }
            break;

        case FWL_RECORD_ROUTE_OPTION:
            if (u4PktOpCode & FWL_IP_OPT_RROUTE_BIT)
            {
                *u4Attack = FWL_RECORD_ROUTE_ATTACK;
                u4Status = FWL_SUCCESS;
            }
            break;

        case FWL_TIMESTAMP_OPTION:
            if (u4PktOpCode & FWL_IP_OPT_TSTAMP_BIT)
            {
                *u4Attack = FWL_TIMESTAMP_ATTACK;
                u4Status = FWL_SUCCESS;
            }
            break;

        case FWL_TRACE_ROUTE_OPTION:
            if (u4PktOpCode & FWL_IP_OPT_TROUTE_BIT)
            {
                *u4Attack = FWL_TRACE_ROUTE_ATTACK;
                u4Status = FWL_SUCCESS;
            }

        default:
            break;
    }                            /*End_of_Switch_Case */

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlInspectIpOption \n");
    return u4Status;
}                                /* End of the function -- FwlInspectIpOption  */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectPktConfigFilter                        */
/*                                                                          */
/*    Description        : Inspects the pkts against the IN or OUT filters  */
/*                                                                          */
/*    Input(s)           : pIfaceNode -- Pointer to the Interface node      */
/*                         pIp        -- Pointer to the IP head structure   */
/*                         pHLInfo    -- Pointer to the Higher layer info   */
/*                         pIcmp      -- Pointer to the ICMP info           */
/*                         u1Direction-- Specifies which Filter is to be    */
/*                                       applied IN filter or OUT filter    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SUCCESS if packet is to be permited otherwise    */
/*                         FAILURE.                                         */
/****************************************************************************/

PRIVATE UINT4
FwlInspectPktConfigFilter (tIfaceInfo * pIfaceNode,
                           tIpHeader * pIp,
                           tHLInfo * pHLInfo,
                           tIcmpInfo * pIcmp,
                           UINT1 u1Direction,
                           UINT1 *pu1LogTrigger, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tAclInfo           *pAclInfo = NULL;
    tTMO_SLL           *pAclLst = NULL;
    tFilterInfo        *pFilter = NULL;
    UINT1               u1Status = FWL_ZERO;
    UINT1               u1AclType = FWL_ZERO;
    UINT4               u4IfaceNum = FWL_ZERO;
    UINT2               u2FragOffsetFlags = FWL_ZERO;

    UNUSED_PARAM (pIcmp);

    u1Status = FWL_NOT_MATCH;
    pAclInfo = (tAclInfo *) NULL;
    pFilter = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlInspectPktConfigFilter \n");

    /* Get the filter list pointer from the interface structure */
    if (pIp->u1IpVersion == FWL_IP_VERSION_4)
    {
        pAclLst = (u1Direction == FWL_DIRECTION_IN) ?
            &pIfaceNode->inFilterList : &pIfaceNode->outFilterList;
    }
    else
    {
        pAclLst = (u1Direction == FWL_DIRECTION_IN) ?
            &pIfaceNode->inIPv6FilterList : &pIfaceNode->outIPv6FilterList;
    }

    u4IfaceNum = pIfaceNode->u4IfaceNum;

    /* Scan the IN list and check the packet against the
     * configured Filter, or Rules
     */
    TMO_SLL_Scan (pAclLst, pAclInfo, tAclInfo *)
    {
        /* 
         * P2R4_1 - The Check made for RowStatus must be added to 
         * support on the Fly configuration.
         */
        if (pAclInfo->u1RowStatus == FWL_ACTIVE)
        {
            switch (pAclInfo->u1AclType)
            {
                case ACL_FILTER:
                    /* P2R4_1 - The Check made for RowStatus must be modified to
                     * support on the Fly configuration.
                     */
                    if ((pFilter = (tFilterInfo *) pAclInfo->pAclName) != NULL)
                    {
                        if (pFilter->u1RowStatus == FWL_ACTIVE)
                        {

                            /* First check, if globalAclSchedule is ENABLED and if 
                             * this ACL is a scheduled one and global schedule 
                             * status is FWL_ACTIVE. */

                            if ((gu1AclScheduleStatus == FWL_NOT_IN_SERVICE) &&
                                (pAclInfo->u4Scheduled == FWL_ACL_SCHEDULED))
                            {
                                /* Move to next ACL as this ACL is NOT_IN_SERVICE */
                                continue;
                            }

                            if (FwlMatchPktConfigSimpleFilter (pFilter, pIp,
                                                               pHLInfo,
                                                               FWL_FILTER_OR) ==
                                FWL_MATCH)
                            {
                                u1Status = FWL_MATCH;
                                u1AclType = FWL_FILTER_MATCH;
                                MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc,
                                              FWL_TRC_INSPECT, "FWL",
                                              "\n Packet matches against the "
                                              "filter %s \n",
                                              pFilter->au1FilterName);
                            }
                        }
                    }
                    break;

                case ACL_RULE:
                    FWL_DBG (FWL_DBG_INSPECT, "\n checks against the Rules \n");

                    /* First check, if globalAclSchedule is ENABLED and if 
                     * this ACL is a schduled one and global schedule 
                     * status is FWL_ACTIVE. */
                    if ((gu1AclScheduleStatus == FWL_NOT_IN_SERVICE) &&
                        (pAclInfo->u4Scheduled == FWL_ACL_SCHEDULED))
                    {
                        /* Move to next ACL as this ACL is NOT_IN_SERVICE */
                        continue;
                    }

                    if (FwlMatchPktConfigRule (pAclInfo, pIp,
                                               pHLInfo) == FWL_MATCH)
                    {
                        u1Status = FWL_MATCH;
                        MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT,
                                      "FWL",
                                      "\n Packet matches against Rule %s \n",
                                      pAclInfo->au1FilterName);
                        u1AclType = FWL_RULE_MATCH;
                    }
                    break;

                default:
                    break;
            }                    /* End of the Switch */
        }                        /* End of If */
        if (u1Status == FWL_MATCH)
        {
            break;                /* Breaks from the TMO_SLL_Scan */
        }
    }                            /* End of the TMO_SLL_Scan */

    if (u1Status == FWL_MATCH)
    {

        *pu1LogTrigger = pAclInfo->u1LogTrigger;

        /* Since the firewall does not have the provision 
         * to do fragmentation and reassembly, fragmented 
         * packets should be allowed to pass through, if the rule is set.*/
        if ((pIp->u2FragOffset & FWL_FRAG_OFFSET_BIT) >= FWL_FRAG_OFFSET_TINY)
        {
            if (pAclInfo->u1FragAction == FWL_PERMIT)
            {
                FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                               pAclInfo->u1LogTrigger,
                               FWLLOG_INFO_LEVEL, (UINT1 *) FWL_MSG_MATCH);
                return (FWL_FRAG_PERMIT);
            }
            else
            {
                FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                               pAclInfo->u1LogTrigger,
                               FWLLOG_ALERT_LEVEL, (UINT1 *) FWL_MSG_ACL_DENY);

                return (FWL_NOT_MATCH);
            }
        }
        /* Check for the first fragmented packet with the fragment action */
        else
        {
            if ((pIp->u2FragOffset & FWL_FRAG_OFFSET_BIT) == FWL_ZERO)
            {
                /* 
                 * For Normal ICMP Pkt MF bit will not be set, but for 
                 * fragmented packets though the fragment offset will 
                 * be zero but the MF bit will be set 
                 */

                u2FragOffsetFlags = (pIp->u2FragOffset & FWL_FRAG_FLAG_BIT);

                /* Check if MF bit is set, if yes then it is frag pkt */
                if (u2FragOffsetFlags & FWL_MF_BIT_MASK)
                {
                    if (pAclInfo->u1FragAction == FWL_PERMIT)
                    {
                        FwlLogMessage (FWL_PERMITTED, u4IfaceNum, pBuf,
                                       pAclInfo->u1LogTrigger,
                                       FWLLOG_INFO_LEVEL,
                                       (UINT1 *) FWL_MSG_MATCH);
                        return (FWL_SUCCESS);
                    }
                    else
                    {
                        FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                                       pAclInfo->u1LogTrigger,
                                       FWLLOG_ALERT_LEVEL,
                                       (UINT1 *) FWL_MSG_ACL_DENY);

                        return (FWL_NOT_MATCH);
                    }
                }                /* MF bit check */
            }
        }
        if (pAclInfo->u1Action == FWL_PERMIT)
        {
            return (FWL_SUCCESS);
        }
        else
        {
            FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                           pAclInfo->u1LogTrigger,
                           FWLLOG_ALERT_LEVEL, (UINT1 *) FWL_MSG_ACL_DENY);

            return (FWL_NOT_MATCH);
        }
    }
    /* There is no entry in the ACL table. So deny the packet */
    FwlLogMessage (FWL_STATIC_FILTER, u4IfaceNum, pBuf,
                   FWL_LOG_BRF, FWLLOG_INFO_LEVEL, (UINT1 *) FWL_MSG_NOMATCH);

    FWL_DBG (FWL_DBG_EXIT, "\n Packet is to be Denied \n");
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. "
             "FwlInspectPktConfigFilter \n");
    UNUSED_PARAM (u1AclType);
    return (FWL_NOT_MATCH);
}                                /* End of the function -- FwlInspectPktConfigFilter */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchPktConfigSimpleFilter                    */
/*                                                                          */
/*    Description        : Inspects the pkts against the configured simple  */
/*                         filters.                                         */
/*                                                                          */
/*    Input(s)           : pFilterInfo-- Pointer to the simple filter.      */
/*                         pIpInfo    -- Pointer to the IP Header info      */
/*                         pHighLayInfo- Pointer to the TCP/UDP Header info */
/*                                       If the packet is an ICMP pkt then  */
/*                                       this structure holds no info about */
/*                                       TCP/UDP src and Dest port, and it  */
/*                                       is not used for the further        */
/*                                       processing.                        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_MATCH if the packet is matched against the   */
/*                         filter, otherwise FWL_NOT_MATCH.                 */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlMatchPktConfigSimpleFilter (tFilterInfo * pFilterInfo,
                               tIpHeader * pIpInfo, tHLInfo * pHighLayInfo,
                               UINT1 u1RuleCondition)
#else
PRIVATE UINT4
FwlMatchPktConfigSimpleFilter (pFilterInfo, pIpInfo, pHighLayInfo,
                               u1RuleCondition)
     tFilterInfo        *pFilterInfo;
     tIpHeader          *pIpInfo;
     tHLInfo            *pHighLayInfo;
     UINT1               u1RuleCondition = FWL_ZERO;
#endif
{
    UINT2               u2FragOffsetBit = FWL_ZERO;

    tIp6Addr            DefaultIp6Addr;
    MEMSET (&DefaultIp6Addr, FWL_ZERO, sizeof (tIp6Addr));

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. FwlMatchPktConfigSimpleFilter\n");
    /* Protocol field cheching */
    if (pFilterInfo->u1Proto != FWL_DEFAULT_PROTO)
    {
        if (pIpInfo->u1Proto != pFilterInfo->u1Proto)
        {
            return FWL_NOT_MATCH;
        }
    }

    /* Source address field checking */
    if (pIpInfo->u1IpVersion == FWL_IP_VERSION_4)
    {
        if (pFilterInfo->SrcStartAddr.v4Addr != FWL_DEFAULT_ADDRESS)
        {
            if ((pIpInfo->SrcAddr.v4Addr < pFilterInfo->SrcStartAddr.v4Addr) ||
                (pIpInfo->SrcAddr.v4Addr > pFilterInfo->SrcEndAddr.v4Addr))
            {
                return FWL_NOT_MATCH;
            }
        }

        /* Destination address field checking */
        if (pFilterInfo->DestStartAddr.v4Addr != FWL_DEFAULT_ADDRESS)
        {
            if ((pIpInfo->DestAddr.v4Addr < pFilterInfo->DestStartAddr.v4Addr)
                || (pIpInfo->DestAddr.v4Addr > pFilterInfo->DestEndAddr.v4Addr))
            {
                return FWL_NOT_MATCH;
            }
        }
        u2FragOffsetBit = FWL_FRAG_OFFSET_BIT;
    }
    else if (pIpInfo->u1IpVersion == FWL_IP_VERSION_6)
    {
        if (pFilterInfo->i4Dscp != FWL_DEFAULT_DSCP)
        {
            if (pIpInfo->u1Tos != (UINT1) pFilterInfo->i4Dscp)
            {
                return FWL_NOT_MATCH;
            }
        }

        if (pFilterInfo->u4FlowId != FWL_DEFAULT_FLOWID)
        {
            if (pIpInfo->u4FlowLabel != pFilterInfo->u4FlowId)
            {
                return FWL_NOT_MATCH;
            }
        }
        if (Ip6AddrCompare (pFilterInfo->SrcStartAddr.v6Addr, DefaultIp6Addr) !=
            IP6_ZERO)
        {
            if ((Ip6AddrCompare
                 (pIpInfo->SrcAddr.v6Addr,
                  pFilterInfo->SrcStartAddr.v6Addr) == IP6_MINUS_ONE)
                ||
                (Ip6AddrCompare
                 (pIpInfo->SrcAddr.v6Addr,
                  pFilterInfo->SrcEndAddr.v6Addr) == IP6_ONE))
            {
                return FWL_NOT_MATCH;
            }
        }

        /* Destination address field checking */
        if (Ip6AddrCompare (pFilterInfo->DestStartAddr.v6Addr, DefaultIp6Addr)
            != IP6_ZERO)
        {
            if ((Ip6AddrCompare
                 (pIpInfo->DestAddr.v6Addr,
                  pFilterInfo->DestStartAddr.v6Addr) == IP6_MINUS_ONE)
                ||
                (Ip6AddrCompare
                 (pIpInfo->DestAddr.v6Addr,
                  pFilterInfo->DestEndAddr.v6Addr) == IP6_ONE))
            {
                return FWL_NOT_MATCH;
            }
        }
        u2FragOffsetBit = FWL_IP6_FRAG_OFFSET_BIT;
    }

    /* Check the packet for TOS field */
    if (pFilterInfo->u1Tos != FWL_DEFAULT_ADDRESS)
    {
        if (pIpInfo->u1Tos != pFilterInfo->u1Tos)
        {
            return FWL_NOT_MATCH;
        }
    }

    /* Checking of Source and destination port if the packet is UDP or TCP */
    switch (pIpInfo->u1Proto)
    {
        case FWL_TCP:
            /* return match for fragmented packets
             * */

            /* Get the fragment offset value : Low order 13 bits 
             * Port number info will not be present in the fragmented
             * packets (except the first) */
            if ((pIpInfo->u2FragOffset & u2FragOffsetBit) >=
                FWL_FRAG_OFFSET_TINY)
            {
                return (FWL_MATCH);
            }
            break;

        case FWL_UDP:

            /*  
             * return match for fragmented packets
             * */

            /* Get the fragment offset value : Low order 13 bits 
             * Port number info will not be present in the fragmented
             * packets (except the first) */
            if ((pIpInfo->u2FragOffset & u2FragOffsetBit) >=
                FWL_FRAG_OFFSET_TINY)
            {
                return (FWL_MATCH);
            }
            break;

        default:                /* Pkts of type of ICMP fall under this default catogary */
            break;

    }                            /* End of Switch case */

    /* For Rule's that are OR'ed together FwlCheckPort () validates whether
     * the Incoming packets Port range have to be blocked/accepted. 
     * For AND Type, the final port range that has been arrived 
     * while installing Rule is used for blocking/accepting */
    if (u1RuleCondition == FWL_FILTER_OR)
    {
        if (((pIpInfo->u1Proto == FWL_UDP) ||
             (pIpInfo->u1Proto == FWL_TCP)) &&
            (FwlCheckPort (pFilterInfo, pHighLayInfo) == FWL_NOT_MATCH))
        {
            return FWL_NOT_MATCH;
        }
    }

    /* 
     * Check whether packet accounting is enabled for the particular filter
     * to which the packet is mapped to
     *
     * If packet accounting is enabled then increment the hit count 
     *
     */

    if (pFilterInfo->u1FilterAccounting == FWL_ENABLE)
    {
        INC_FILTER_HIT_COUNT (pFilterInfo);
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlMatchPktConfigSimpleFilter\n");

    /* The packet is matching against the filter, and the action is to be 
     * taken as specified in the filter.
     */
    return FWL_MATCH;

}                                /* End of the function -- FwlMatchPktConfigSimpleFilter */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckPort                                     */
/*                                                                          */
/*    Description        : Inspects the pkts against the configured TCP and */
/*                         UDP ports.                                       */
/*                                                                          */
/*    Input(s)           : pFilterNode -- Pointer to the simple filter.     */
/*                         pHLHdr      -- Pointer to the High layer info    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_MATCH if the packet is matched against the   */
/*                         Filter, otherwise FWL_NOT_MATCH.                 */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlCheckPort (tFilterInfo * pFilterNode, tHLInfo * pHLHdr)
#else
PRIVATE UINT4
FwlCheckPort (pFilterNode, pHLHdr)
     tFilterInfo        *pFilterNode;
     tHLInfo            *pHLHdr;
#endif
{
    UINT4               u4Status = FWL_ZERO;

    u4Status = FWL_NOT_MATCH;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlCheckPort \n");

    /* Checking the packet against the Source port */
    if ((pFilterNode->u2SrcMaxPort != FWL_DEFAULT_PORT) ||
        (pFilterNode->u2SrcMinPort != FWL_DEFAULT_PORT))
    {
        if (pFilterNode->u2SrcMaxPort == pFilterNode->u2SrcMinPort)
        {
            if (pHLHdr->u2SrcPort == pFilterNode->u2SrcMinPort)
            {
                u4Status = FWL_MATCH;
            }
            else
            {
                return FWL_NOT_MATCH;
            }
        }
        else
        {
            if ((pHLHdr->u2SrcPort <= pFilterNode->u2SrcMaxPort) &&
                (pHLHdr->u2SrcPort >= pFilterNode->u2SrcMinPort))
            {
                u4Status = FWL_MATCH;
            }
            else
            {
                return FWL_NOT_MATCH;
            }
        }
    }

    if ((pFilterNode->u2DestMaxPort != FWL_DEFAULT_PORT) ||
        (pFilterNode->u2DestMinPort != FWL_DEFAULT_PORT))
    {
        if (pFilterNode->u2DestMaxPort == pFilterNode->u2DestMinPort)
        {
            if (pHLHdr->u2DestPort == pFilterNode->u2DestMinPort)
            {
                u4Status = FWL_MATCH;
            }
            else
            {
                return FWL_NOT_MATCH;
            }
        }
        else
        {
            if ((pHLHdr->u2DestPort <= pFilterNode->u2DestMaxPort) &&
                (pHLHdr->u2DestPort >= pFilterNode->u2DestMinPort))
            {
                u4Status = FWL_MATCH;
            }
            else
            {
                return FWL_NOT_MATCH;
            }
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlCheckPort \n");

    return u4Status;            /* FWL_MATCH */

}                                /* end of the function -- FwlCheckPort */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlMatchPktConfigRule                            */
/*                                                                          */
/*    Description        : Inspects the pkts against the configured Rules.  */
/*                         More than one filter is combined and assigned to */
/*                         Rule.                                            */
/*                                                                          */
/*    Input(s)           : pAclFilter  -- Pointer to the Rule.              */
/*                         pIpHdr      -- Pointer to the IP hdr info        */
/*                         pHLHdr      -- Pointer to the High layer info    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_MATCH if the packet is matched against the   */
/*                         Filter, otherwise FWL_NOT_MATCH.                 */
/****************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
FwlMatchPktConfigRule (tAclInfo * pAclFilter,
                       tIpHeader * pIpHdr, tHLInfo * pHLHdr)
#else
PRIVATE UINT4
FwlMatchPktConfigRule (pAclFilter, pIpHdr, pHLHdr)
     tAclInfo           *pAclFilter;
     tIpHeader          *pIpHdr;
     tHLInfo            *pHLHdr;
#endif
{
    tRuleInfo          *pRuleFilter = NULL;
    tFilterInfo        *pFilterNode = NULL;
    UINT1               u1FilterIndex = FWL_ZERO;
    UINT1               u1Status = FWL_ZERO;

    u1Status = FWL_NOT_MATCH;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlMatchPktConfigRule \n");

    if ((pRuleFilter = (tRuleInfo *) pAclFilter->pAclName) == NULL)
    {
        /* Pointer to the rule is NULL */
        return FWL_NOT_MATCH;
    }
    /* 
     * P2R4_1 - The Check made for RowStatus must be added to 
     * support on the Fly configuration.
     */
    if (pRuleFilter->u1RowStatus != FWL_ACTIVE)
    {
        /* the rule is not ACTIVE */
        return FWL_NOT_MATCH;
    }

    switch (pRuleFilter->u1FilterConditionFlag)
    {

            /* If the filter are combined by operator & */
        case FWL_FILTER_AND:
            for (u1FilterIndex = FWL_ZERO; u1FilterIndex <
                 FWL_MAX_FILTERS_IN_RULE; u1FilterIndex++)
            {
                if ((pFilterNode = pRuleFilter->apFilterInfo[u1FilterIndex]) !=
                    NULL)
                {
                    if (FwlMatchPktConfigSimpleFilter (pFilterNode, pIpHdr,
                                                       pHLHdr, FWL_FILTER_AND)
                        == FWL_MATCH)
                    {
                        u1Status = FWL_MATCH;
                        continue;
                    }
                    u1Status = FWL_NOT_MATCH;
                    break;        /* breaks from the For loop if the pkt not match */
                }
                break;            /* breaks from the for Loop if filter pointer is NULL */
            }

            /* Check whether the Incoming Packets has to be allowed/blocked */

            /* Checking the packet against the Source port */
            if ((pHLHdr->u2SrcPort <= pRuleFilter->u2MaxSrcPort) &&
                (pHLHdr->u2SrcPort >= pRuleFilter->u2MinSrcPort))
            {
                u1Status = FWL_MATCH;
            }
            else
            {
                u1Status = FWL_NOT_MATCH;
                break;
            }

            /* Checking the packet against the Destination port */
            if ((pHLHdr->u2DestPort <= pRuleFilter->u2MaxDestPort) &&
                (pHLHdr->u2DestPort >= pRuleFilter->u2MinDestPort))
            {
                u1Status = FWL_MATCH;
            }
            else
                u1Status = FWL_NOT_MATCH;

            break;                /* breaks from the switch */

        case FWL_FILTER_OR:
            for (u1FilterIndex = FWL_ZERO; u1FilterIndex <
                 FWL_MAX_FILTERS_IN_RULE; u1FilterIndex++)
            {
                if ((pFilterNode = pRuleFilter->apFilterInfo[u1FilterIndex]) !=
                    NULL)
                {
                    if (FwlMatchPktConfigSimpleFilter (pFilterNode, pIpHdr,
                                                       pHLHdr, FWL_FILTER_OR)
                        == FWL_MATCH)
                    {
                        u1Status = FWL_MATCH;
                        break;    /* The pkt is got matched, break from For loop */
                    }
                    continue;
                }
                break;            /* breaks from the for Loop if filter pointer is NULL */
            }
            break;                /* breaks from the switch */

        default:
            break;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlMatchPktConfigRule \n");

    if (u1Status == FWL_MATCH)
    {
        return FWL_MATCH;
    }
    return FWL_NOT_MATCH;

}                                /* End of the function -- FwlMatchPktConfigRule */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckSNMPTrapPacket                           */
/*                                                                          */
/*    Description        : This function checks whether the Packet is an    */
/*                         SNMP Packet and allows it to reach the Manager   */
/*                                                                          */
/*    Input(s)           : pbuf - Packet                                    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS or FWL_FAILURE                       */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlCheckSNMPTrapPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
#else
PUBLIC UINT4
FwlCheckSNMPTrapPacket (pBuf)
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{
    tIpHeader           IpHdr;
    tHLInfo             HLInfo;

    FWL_DBG (FWL_DBG_INSPECT, "\n Entering into the SNMP TRAP CHECK Module \n");

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlCheckSNMPTrapPacket \n");

    /* Uopdate the IP Header and UDP header */
    if (FWL_SUCCESS != (FwlUpdateIpHeaderInfoFromPkt (pBuf, &IpHdr)))
    {
        return FWL_FAILURE;
    }
    FwlUpdateHLInfoFromPkt (pBuf, &HLInfo, IpHdr.u1Proto, IpHdr.u1HeadLen);

    /* 
     * Check whether the Packet is the SNMP Trap Packet .
     * The Trap packet will be an UDP packet with source port 161 and
     * destination port as 162.
     */
    if ((IpHdr.u1Proto == FWL_UDP) &&
        (HLInfo.u2DestPort == FWL_SNMP_TRAP_DEST_PORT) &&
        (HLInfo.u2SrcPort == FWL_SNMP_TRAP_SRC_PORT))
    {
        gFwlAclInfo.u1TrapSentFlag = FWL_TRAP_SENT;
        FWL_DBG (FWL_DBG_INSPECT, "\n  SNMP TRAP is sent to the Manager\n");
        return FWL_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlCheckSNMPTrapPacket \n");
    return FWL_FAILURE;

}                                /* End of fn. FwlCheckSNMPTrapPacket */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckAttacks                                  */
/*                                                                          */
/*    Description        : This function checks for any kinds of attacks    */
/*                         attempted by a hacker from the WAN side          */
/*                                                                          */
/*    Input(s)           : pbuf - Packet                                    */
/*                         u4IfaceNum - Interface Index                     */
/*                         pIpHdr - Ip header                               */
/*                         pHLInfo - Higher layer header info TCP/UDP       */
/*                         pIcmpHdr - ICMP hdr info                         */
/*                         u1Direction - direction of the pkt whether it    */
/*                                       is IN/OUT                          */
/*                         u4TransHdrLen - Transport Protocol header length */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_PERMITTED or the AttackType                  */
/****************************************************************************/
UINT4
FwlCheckAttacks (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfaceNum,
                 tIpHeader * pIpHdr, tHLInfo * pHLInfo, tIcmpInfo * pIcmpHdr,
                 UINT1 u1Direction, UINT4 u4TransHdrLen)
{
    UINT4               u4RetVal = FWL_ZERO;
    UINT1               u1Urg = FWL_ZERO;
    UINT1               u1Push = FWL_ZERO;
    UINT1               u1OptPtr = FWL_ZERO;
    UINT1               u1TimeStamp = FWL_ZERO;

    UNUSED_PARAM (u1Direction);

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlCheckAttacks \n");

    if (pIpHdr->u1IpVersion == FWL_IP_VERSION_4)
    {
        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_LAND_ATTACK))
        {
            /* Check for Land Attack */
            if (pIpHdr->SrcAddr.v4Addr == pIpHdr->DestAddr.v4Addr)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n Land Attack detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_LAND_ATTACK;
            }
        }
        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_INVALID_SRC_IP))
        {
            /* Check for Invalid source address */
            if ((FwlIsIpAddressValid (pIpHdr->SrcAddr.v4Addr) == FWL_FAILURE) ||
                (FwlValidateIpAddress (pIpHdr->SrcAddr.v4Addr) ==
                 FWL_MCAST_ADDR))
            {
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_INVALID_SRC_IP;
            }
        }

        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_INVALID_DST_IP))
        {
            /* Check for Invalid destination address */
            if (FwlIsIpAddressValid (pIpHdr->DestAddr.v4Addr) == FWL_FAILURE)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_INVALID_DST_IP;
            }
        }

        u4RetVal = (UINT4) FwlValidateIpAddress (pIpHdr->DestAddr.v4Addr);

        if ((u4RetVal == FWL_BCAST_ADDR) ||
            (u4RetVal == FWL_CLASS_BCASTADDR)
            || (u4RetVal == FWL_CLASS_NETADDR))
        {
            /* Check for Smurf Attack */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_SMURF_ATTACK))
            {
                if (pIpHdr->u1Proto == FWL_ICMP)
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Smurf Attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_SMURF_ATTACK;
                }
            }
            if (pIpHdr->u1Proto == FWL_UDP)
            {
                if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_FRAGGLE_ATTACK))
                {
                    /* Fraggle attack prevention:
                     * This attack is generated to UDP port no 7,
                     * with destination IP as broadcast address. 
                     */
                    if (pHLInfo->u2DestPort == FWL_UDP_PORT_SEVEN)
                    {
                        FWL_DBG (FWL_DBG_EXIT, "\n Fraggle Attack detected \n");
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n Exiting FwlCheckAttacks() \n");
                        return FWL_FRAGGLE_ATTACK;
                    }
                }
                /* Internal Fix
                 * Allow NetBIOS advertisement among LAN hosts 
                 * src & dest port=137 - netbios-ns : name query service 
                 * src & dest port=138 - netbios-dgm : domain/workgroup 
                 *                                               announcement */
                if ((FwlChkIfExtInterface (u4IfaceNum) != FWL_SUCCESS) &&
                    (FWL_DIRECTION_IN == u1Direction))
                {
                    if (((pHLInfo->u2DestPort == FWL_NETBIOS_NS)
                         && (pHLInfo->u2SrcPort == FWL_NETBIOS_NS))
                        || ((pHLInfo->u2DestPort == FWL_NETBIOS_DGM)
                            && (pHLInfo->u2SrcPort == FWL_NETBIOS_DGM)))
                    {
                        FWL_DBG (FWL_DBG_EXIT, "\n NetBIOS Query\n");
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n Exiting FwlCheckAttacks() \n");
                        return FWL_PERMITTED;
                    }
                }
            }                    /* Proto = UDP */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_BCAST_STORM_ATTACK))
            {

                /* Broadcast Packet should be dropped */
                FWL_DBG (FWL_DBG_EXIT, "\n Broadcast Storm detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_BCAST_STORM_ATTACK;
            }

        }

        if (pIpHdr->u1Proto == FWL_TCP)
        {
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TCP_SHTHDR))
            {
                /* TCP short header attack prevention */
                if ((pIpHdr->u2FragOffset & FWL_FRAG_OFFSET_BIT) == FWL_ZERO)
                {
                    if (((pIpHdr->u2TotalLen - pIpHdr->u1HeadLen) <
                         FWL_TCP_HEADER_LEN)
                        || (u4TransHdrLen < FWL_TCP_HEADER_LEN))
                    {
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n TCP short header attack detected \n");
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n Exiting FwlCheckAttacks() \n");
                        return FWL_TCP_SHTHDR;
                    }
                }                /*End-of-TCP-Short-Header-Check. */
            }

            /* Getting the urgent bit value */
            u1Urg = (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_URG_MASK) >>
                             FWL_FIVE);

            /* Getting the Push bit value */
            u1Push = (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_PUSH_MASK) >>
                              FWL_THREE);

            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TCP_NULL_ATTACK))
            {
                /* TCP NULL scan attack check */
                if ((pHLInfo->u1Fin == FWL_ZERO) &&
                    (u1Urg == FWL_ZERO) &&
                    (u1Push == FWL_ZERO) &&
                    (pHLInfo->u1Rst == FWL_ZERO) &&
                    (pHLInfo->u1Ack == FWL_ZERO) && (pHLInfo->u1Syn ==
                                                     FWL_ZERO))
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n TCP NULL Attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_TCP_NULL_ATTACK;
                }
            }

            /* TCP SYN-FIN attack check */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TCP_SYN_FIN_ATTACK))
            {
                if ((pHLInfo->u1Fin) && (pHLInfo->u1Syn))
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n TCP SYN_FIN Attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_TCP_SYN_FIN_ATTACK;
                }
            }

            /* Invalid Urgent Pointer Offset */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_INVALID_URGENT_OFFSET))
            {
                if (u1Urg == FWL_URG_SET)
                {
                    if ((pHLInfo->u2UrgPtr + (UINT4) pIpHdr->u1HeadLen +
                         u4TransHdrLen) > (pIpHdr->u2TotalLen))
                    {
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\nInvalid Urgent Offset Attack detected\n");
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n Exiting FwlCheckAttacks() \n");
                        return FWL_INVALID_URGENT_OFFSET;
                    }
                }                /* FWL_URG_SET */
            }
        }                        /* Proto = TCP */
        else if (pIpHdr->u1Proto == FWL_UDP)
        {
            /* UDP Short-Header Attack check: UDP header with length < 8 */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_UDP_SHTHDR))
            {
                if ((pIpHdr->u2FragOffset & FWL_FRAG_OFFSET_BIT) == FWL_ZERO)
                {
                    if ((pIpHdr->u2TotalLen - pIpHdr->u1HeadLen) <
                        FWL_UDP_HEADER_LEN)
                    {
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n UDP short header detected \n");
                        FWL_DBG (FWL_DBG_EXIT,
                                 "\n Exiting FwlCheckAttacks() \n");
                        return FWL_UDP_SHTHDR;
                    }
                }
            }

            /* W2k Domain controller attack prevention, 
             * attack on port no 464
             */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_W2KDC_ATTACK))
            {
                if (pHLInfo->u2DestPort == FWL_W2KDC_PORT_NUMBER)
                {
                    FWL_DBG (FWL_DBG_EXIT,
                             "\n W2k Domain controller Attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_W2KDC_ATTACK;
                }
            }

            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_SNORK_ATTACK))
            {
                /* Preventing Snork Attack */
                if ((pHLInfo->u2DestPort == FWL_SNORK_ATTACK_PORT_135) &&
                    ((pHLInfo->u2SrcPort == FWL_SNORK_ATTACK_PORT_7) ||
                     (pHLInfo->u2SrcPort == FWL_SNORK_ATTACK_PORT_19) ||
                     (pHLInfo->u2SrcPort == FWL_SNORK_ATTACK_PORT_135)))
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Snork Attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_SNORK_ATTACK;
                }
            }

            /* Prevention of Ascend attack:
             * This attack is specific to Ascend Routers.
             * Port No 9 is discard Port
             */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_ASCEND_ATTACK))
            {
                if (pHLInfo->u2DestPort == FWL_ASCEND_ATTACK_PORT)
                {
                    FWL_DBG (FWL_DBG_EXIT, "\n Ascend Attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_ASCEND_ATTACK;
                }
            }

            /* UDP port loopback attack prevention:
             * This attack is generated when echo packets are exchanged 
             * endlessly between src port 7, 17 or 19 and dest port 7, 17 or 19.
             */
            if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_UDP_LOOPBACK_ATTACK))
            {
                if (((pHLInfo->u2SrcPort ==
                      FWL_UDP_LOOPBACK_ATTACK_PORT_7) ||
                     (pHLInfo->u2SrcPort ==
                      FWL_UDP_LOOPBACK_ATTACK_PORT_17) ||
                     (pHLInfo->u2SrcPort ==
                      FWL_UDP_LOOPBACK_ATTACK_PORT_19)) &&
                    ((pHLInfo->u2DestPort ==
                      FWL_UDP_LOOPBACK_ATTACK_PORT_7) ||
                     (pHLInfo->u2DestPort ==
                      FWL_UDP_LOOPBACK_ATTACK_PORT_17) ||
                     (pHLInfo->u2DestPort == FWL_UDP_LOOPBACK_ATTACK_PORT_19)))
                {
                    FWL_DBG (FWL_DBG_EXIT,
                             "\n UDP port loopback attack detected \n");
                    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                    return FWL_UDP_LOOPBACK_ATTACK;
                }
            }
        }                        /* FWL_UDP */
    }

    if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_UNKNOWN_IP))
    {

        /* Unknown IP Protocol detection */
        if ((pIpHdr->u1Proto < FWL_MIN_VALID_PROTO_NUM)
            || (pIpHdr->u1Proto > FWL_MAX_VALID_PROTO_NUM))
        {
            FWL_DBG (FWL_DBG_EXIT, "\n Unknown IP protocol detected \n");
            FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
            return FWL_UNKNOWN_IP;
        }
    }

    if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_TCP_XMAS_ATTACK))
    {
        if (pIpHdr->u1Proto == FWL_TCP)
        {
            /* TCP X-MAS scan attack check
             * PUSH, URGENT and FIN flag are all set to some probable number
             */

            /* Getting the urgent bit value */
            u1Urg = (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_URG_MASK) >>
                             FWL_FIVE);

            /* Getting the Push bit value */
            u1Push = (UINT1) ((pHLInfo->u1TcpCodeBit & FWL_TCP_PUSH_MASK) >>
                              FWL_THREE);

            if ((pHLInfo->u1Fin) && (u1Urg) && (u1Push))
            {
                FWL_DBG (FWL_DBG_EXIT, "\n TCP X-MAS Attack detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_TCP_XMAS_ATTACK;
            }
        }
    }

    if (pIpHdr->u1Proto == FWL_ICMP)
    {
        /* Dropping ICMP Router Advertisement Messages */
        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_IRDP_ADVT))
        {
            if (pIcmpHdr->u1Type == FWL_IRDP_ADVERTISEMENT)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n IRDP Advertisement detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_IRDP_ADVT;
            }
        }
        /* Dropping ICMP Timestamp Request Messages */
        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_ICMP_TIMESTAMP_REQ_ATTACK))
        {
            if (pIcmpHdr->u1Type == FWL_ICMP_TYPE_TIMESTAMP_REQ)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n ICMP Timestamp Request detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_ICMP_TIMESTAMP_REQ_ATTACK;
            }
        }

        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_ICMP_MASK_REQ_ATTACK))
        {
            /* Dropping ICMP_MASK_REQ query Messages */
            if (pIcmpHdr->u1Type == FWL_ICMP_TYPE_MASK_REQ)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n ICMP_MASK_REQ query detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_ICMP_MASK_REQ_ATTACK;
            }
        }

        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_ICMP_TIMESTAMP_REPLY_ATTACK))
        {
            /* Dropping ICMP Timestamp Reply Messages */
            if (pIcmpHdr->u1Type == FWL_ICMP_TYPE_TIMESTAMP_REPLY)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n ICMP Timestamp Reply detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_ICMP_TIMESTAMP_REPLY_ATTACK;
            }
        }

        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_ICMP_MASK_REPLY_ATTACK))
        {
            /* Dropping ICMP_MASK_REPLY Messages */
            if (pIcmpHdr->u1Type == FWL_ICMP_TYPE_MASK_REPLY)
            {
                FWL_DBG (FWL_DBG_EXIT, "\n ICMP_MASK_REPLY detected \n");
                FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
                return FWL_ICMP_MASK_REPLY_ATTACK;
            }
        }

        if (FWL_NP_HW_FILTER_NOT_EXISTS (FWL_UNALIGNED_TIMESTAMP))
        {
            if (pIpHdr->u1HeadLen > FWL_IP_HEADER_LEN)
            {
                FWL_GET_1_BYTE (pBuf, FWL_IP_OPTION_OFFSET, u1TimeStamp);

                if (u1TimeStamp == FWL_TIME_STAMP_OPTION)
                {
                    if ((pIcmpHdr->u1Type == FWL_ICMP_TYPE_ECHO_REQUEST) ||
                        (pIcmpHdr->u1Type == FWL_ICMP_TYPE_ECHO_REPLY))
                    {
                        FWL_GET_1_BYTE (pBuf, FWL_OPTION_PTR_OFFSET, u1OptPtr);

                        /* Valid IP option pointer value starts from 5 and cannot
                         * exceed 37 for timestamp packets */
                        if (!((u1OptPtr == FWL_IP_POINTER_VALUE_5) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_9) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_13) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_17) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_21) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_25) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_29) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_33) ||
                              (u1OptPtr == FWL_IP_POINTER_VALUE_37)))
                        {
                            FWL_DBG (FWL_DBG_EXIT,
                                     "\n Unaligned Timestamp option Attack detected \n");
                            FWL_DBG (FWL_DBG_EXIT,
                                     "\n Exiting FwlCheckAttacks() \n");
                            return FWL_UNALIGNED_TIMESTAMP;
                        }
                    }
                }
            }
        }
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting FwlCheckAttacks() \n");
    return FWL_PERMITTED;
}                                /* End of function FwlCheckAttacks() */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlChkDhcpPkts                                   */
/*                                                                          */
/*    Description        : This function checks whether the packet belongs  */
/*                         to dhcp server/client in router for inward       */
/*                         direction and dhcp client/server in private      */
/*                         network for outward direction                    */
/*                                                                          */
/*    Input(s)           : u4IfaceNum  - Interface number                   */
/*                         u1Direction - direction of the pkt - IN/OUT      */
/*                         u4SrcAddr   - Source IP address                  */
/*                         u4DestAddr  - Destination IP address             */
/*                         u2SrcPort   - UDP source port                    */
/*                         u2DestPort  - UDP destination port               */
/*                                                                          */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS OR FWL_FAILURE                       */
/****************************************************************************/

PRIVATE UINT4
FwlChkDhcpPkts (UINT4 u4IfaceNum, UINT1 u1Direction,
                tFwlIpAddr SrcAddr, tFwlIpAddr DestAddr,
                UINT2 u2SrcPort, UINT2 u2DestPort)
{
    UINT4               u4Index = FWL_ZERO;

    UNUSED_PARAM (u4IfaceNum);

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
             "\n Checking DHCP Packets \n");
    /* 
     * FSR (WAN) DHCP CLIENT <------>  WAN (ISP) DHCP SERVER
     * 
     * DHCP Client Request messages and DHCP Server Response
     * messages should be allowed to pass through for wan interface.
     */

    /*
     * LAN DHCP CLIENT <------>  FSR (LAN) DHCP SERVER
     * 
     * DHCP Client Request messages and DHCP Server Response
     * messages should be allowed to pass through for lan interface.
     */
    if (u1Direction == FWL_DIRECTION_OUT)
    {
        /* 
         * DHCP client may receive OFFER messages from DHCP server (CAS).
         * Firewall should allow this packet if it has:
         *
         * Source address    ==> Router's IP
         * Source Port       ==> DHCP Server  (67)
         * Destination Port  ==> DHCP Client (68)
         *
         * DHCP client (CAS) sends requests to DHCP server. 
         * Firewall should allow this packet if it has:
         *
         * Source address    ==> Router's IP or 0.0.0.0
         * Source Port       ==> DHCP Client  (68)
         * Destination Port  ==> DHCP Server (67)
         */
        if (((u2SrcPort == FWL_DHCP_SERVER_PORT) &&
             (u2DestPort == FWL_DHCP_CLIENT_PORT) &&
             (SecUtilIpIfIsOurAddress (SrcAddr.v4Addr) == TRUE)) ||
            ((u2SrcPort == FWL_DHCP_CLIENT_PORT) &&
             (u2DestPort == FWL_DHCP_SERVER_PORT) &&
             (SecUtilIpIfIsOurAddress (SrcAddr.v4Addr) == TRUE)))
        {
            return (FWL_SUCCESS);
        }
        if (((u2SrcPort == FWL_DHCPv6_SERVER_PORT) &&
             (u2DestPort == FWL_DHCPv6_CLIENT_PORT) &&
             (Sec6UtilIpIfIsOurAddress (&(SrcAddr.v6Addr), &u4Index)
              == OSIX_SUCCESS)) ||
            ((u2SrcPort == FWL_DHCPv6_CLIENT_PORT) &&
             (u2DestPort == FWL_DHCPv6_SERVER_PORT) &&
             (Sec6UtilIpIfIsOurAddress (&(SrcAddr.v6Addr), &u4Index)
              == OSIX_SUCCESS)))
        {
            return (FWL_SUCCESS);
        }
    }
    else if (u1Direction == FWL_DIRECTION_IN)
    {
        /* 
         * DHCP client (CAS) may send requests to DHCP server. Firewall 
         * should allow this packet if it has:
         *  
         * Destination address  ==> Router's IP or 0.0.0.0
         * Source Port          ==> DHCP Server (67)
         * Destination Port     ==> DHCP Client (68)
         * 
         * DHCP client may send DISCOVER messages to the DHCP Server (CAS)
         * Firewall should allow this packet if it has:
         * 
         * Destination address  ==> Router's IP or 255.255.255.255
         * Source Port          ==> DHCP Client  (68)
         * Destination Port     ==> DHCP Server (67)
         *
         * DHCP Relay Packets should be allowed by the Firewall
         *
         * Source Port          ==> DHCP Relay (67)
         * Destination Port     ==> DHCP Server (67)
         */
        if (((u2DestPort == FWL_DHCP_CLIENT_PORT) &&
             (u2SrcPort == FWL_DHCP_SERVER_PORT)) ||
            ((u2SrcPort == FWL_DHCP_SERVER_PORT) &&
             (u2DestPort == FWL_DHCP_SERVER_PORT) &&
             (FwlValidateIpAddress (DestAddr.v4Addr) == FWL_BCAST_ADDR)) ||
            ((u2SrcPort == FWL_DHCP_CLIENT_PORT) &&
             (u2DestPort == FWL_DHCP_SERVER_PORT) &&
             (FwlValidateIpAddress (DestAddr.v4Addr) == FWL_BCAST_ADDR)))
        {
            return (FWL_SUCCESS);
        }
        if (((u2DestPort == FWL_DHCP_CLIENT_PORT) &&
             (u2SrcPort == FWL_DHCP_SERVER_PORT)) ||
            ((u2SrcPort == FWL_DHCP_SERVER_PORT) &&
             (u2DestPort == FWL_DHCP_SERVER_PORT) &&
             (Ip6AddrType (&(DestAddr.v6Addr)) == ADDR6_MULTI)) ||
            ((u2SrcPort == FWL_DHCP_CLIENT_PORT) &&
             (u2DestPort == FWL_DHCP_SERVER_PORT) &&
             (Ip6AddrType (&(DestAddr.v6Addr)) == ADDR6_MULTI)))
        {
            return (FWL_SUCCESS);
        }
    }
    return (FWL_FAILURE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlLogRawIP                                      */
/*                                                                          */
/*    Description        : This function logs raw ip packet as denied       */
/*                                                                          */
/*    Input(s)           : pbuf - Packet                                    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
PUBLIC VOID
FwlLogRawIP (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    /* ICSA V07
     * Log traffic from/to FSR's private/public interface */
    UINT1               u1IpVersion = FWL_ZERO;
    FwlLogMessage (FWL_ZERO_LEN_IP_DATA, FWL_ZERO /* dummy index */ ,
                   pBuf, FWL_LOG_BRF, FWLLOG_INFO_LEVEL, NULL);
    FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, u1IpVersion);
    u1IpVersion >>= FWL_FOUR;

    if (u1IpVersion == FWL_IP_VERSION_4)
    {
        INC_INSPECT_COUNT;
        INC_DISCARD_COUNT;
    }
    else
    {
        INC_V6_INSPECT_COUNT;
        INC_V6_DISCARD_COUNT;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlLogRawIP \n");

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlLogInvalidIPPkt                               */
/*                                                                          */
/*    Description        : This function logs invalid ip packet             */
/*                                                                          */
/*    Input(s)           : pbuf - Packet                                    */
/*                       : u4IfaceNum - Interface index                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
PUBLIC VOID
FwlLogInvalidIPPkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    /* ICSA V07
     * Log traffic from/to FSR's private/public interface */
    UINT1               u1IpVersion = FWL_ZERO;
    FwlLogMessage (FWL_MALFORMED_IP_PKT, FWL_ZERO /* dummy index */ ,
                   pBuf, FWL_LOG_BRF, FWLLOG_INFO_LEVEL, NULL);

    FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, u1IpVersion);
    u1IpVersion >>= FWL_FOUR;
    if (u1IpVersion == FWL_IP_VERSION_4)
    {
        INC_INSPECT_COUNT;
        INC_DISCARD_COUNT;
    }
    else
    {
        INC_V6_INSPECT_COUNT;
        INC_V6_DISCARD_COUNT;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from fn. FwlLogInvalidIPPkt \n");

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlCheckTinyFragment                             */
/*                                                                          */
/*    Description        : Inspects the packets for Tiny Fragment           */
/*                                                                          */
/*    Input(s)           : u2FragOffset - Fragment offset value of IP pkt   */
/*                       : u2TotalLen - Total length of the IP packet       */
/*                       : u1HeadLen - IP Header length                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is Tiny, otherwise         */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
UINT4
FwlCheckTinyFragment (UINT2 u2FragOffset, UINT2 u2TotalLen, UINT1 u1IpHdrLen)
{
    UINT1               u1FragHit = FWL_NOT_MATCH;
    UINT2               u2FragOffsetFlags = FWL_ZERO;
    UINT2               u2FragOffsetValue = FWL_ZERO;
    UINT2               u2IpPktLen = FWL_ZERO;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering the fn. FwlCheckTinyFragment \n");

    /* Get the fragment offset flags: High order 3 bits
     * * Rsvd : DF : MF */
    u2FragOffsetFlags = (u2FragOffset & FWL_FRAG_FLAG_BIT);

    /* Get the fragment offset value : Low order 13 bits */
    u2FragOffsetValue = (u2FragOffset & FWL_FRAG_OFFSET_BIT);

    /* Check the packet for Tiny Fragment */
    /* RFC 1858 details about the Tiny fragment attack */
    if (u2FragOffsetValue == FWL_ZERO)
    {
        if (u2FragOffsetFlags & FWL_MF_BIT_MASK)    /* Check if MF bit is set */
        {
            /* offset value will be multiple of 8 bytes. 
             * If the total length is less than 
             * (IP header length + offset length),
             * then the packet should be dropped */

            u2IpPktLen = (UINT2) (u1IpHdrLen + (FWL_FRAG_OFFSET_TINY *
                                                FWL_EIGHT_BYTES));

            if (u2TotalLen < u2IpPktLen)
            {
                /* Update the global statistics */
                INC_TINY_FRAG_ATTACK_DISCARD_COUNT;
                u1FragHit = FWL_MATCH;
            }
        }
    }
    else if (u2FragOffsetValue <= FWL_FRAG_OFFSET_TINY)
    {
        /* Update the global statistics */
        INC_TINY_FRAG_ATTACK_DISCARD_COUNT;
        u1FragHit = FWL_MATCH;
    }

    FWL_DBG (FWL_DBG_EXIT, "\n Exiting the fn. FwlInspectPktFragment \n");

    if (u1FragHit == FWL_MATCH)
    {
        return FWL_SUCCESS;
    }
    else
    {
        return FWL_FAILURE;
    }

}                                /* FwlCheckTinyFragment() */

/****************************************************************************/
/*                 End of the file -- fwlinsp.c                             */
/****************************************************************************/
