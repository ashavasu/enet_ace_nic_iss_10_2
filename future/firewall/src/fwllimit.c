
/********************************************************************
 * Copyright (C)  2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwllimit.c,v 1.8 2013/10/31 11:13:30 siva Exp $
 *
 * Description:This file is used for rate limiting 
 *             of tcp/udp/icmp packets
 *
 *******************************************************************/
#include "fwlinc.h"

/* Local Prototypes */
PRIVATE tOutUserSessionNode *FwlAddOutUserSessionNode
PROTO ((tFwlIpAddr LocalIP));

PRIVATE tInUserSessionNode *FwlAddInUserSessionNode
PROTO ((tFwlIpAddr LocalIP, UINT2 u2ServicePort, UINT1 u1Proto));

PRIVATE UINT4       FwlDelOutUserSessionNode
PROTO ((UINT4 u4Flush, tFwlIpAddr LocalIP));

PRIVATE UINT4 FwlDelInUserSessionNode PROTO ((UINT4 u4Flush, tFwlIpAddr LocalIP,
                                              UINT2 u2Port, UINT1 u1Proto));

PRIVATE UINT4 FwlDecrementOutUserSession PROTO ((tFwlIpAddr LocalIP));

PRIVATE UINT4
    FwlDecrementInUserSession PROTO ((tFwlIpAddr LocalIP, UINT2 u2Port,
                                      UINT1 u1proto));
PRIVATE tOutUserSessionNode *FwlGetOutUserSessionNode
PROTO ((tFwlIpAddr LocalIP));

PRIVATE tInUserSessionNode *FwlGetInUserSessionNode
PROTO ((tFwlIpAddr LocalIP, UINT2 u2Port, UINT1 u1Proto));

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlProcessPktForRateLimiting                     */
/*                                                                          */
/*    Description        : Inspects the packets to do rate limiting         */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the Packet            */
/*                         pIfaceInfo   -- Pointer to the Interface         */
/*                         pHLInfo      -- Transport Header Info            */
/*                         pIcmp        -- ICMP Header Info                 */
/*                         u4IfaceNum   -- interface number                 */
/*                         u1Proto      -- Protocol  number                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
PUBLIC UINT4
FwlProcessPktForRateLimiting (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIfaceInfo * pIfaceInfo,
                              tHLInfo * pHLInfo,
                              tIcmpInfo * pIcmp,
                              UINT4 u4IfaceNum, UINT1 u1Proto)
{

    switch (u1Proto)
    {
        case FWL_TCP:

            if (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE)
            {

                /* Process the packet against the TCP SYN flooding prevention */
                if ((pHLInfo->u1Syn == FWL_SET) &&
                    (pHLInfo->u1Ack == FWL_NOT_SET))
                {
                    if (gFwlAclInfo.u2SynPktsAllowed >=
                        gFwlAclInfo.u2TcpSynLimit)
                    {
                        /* Discard the packet and update the statistics */
                        INC_TCP_SYN_DISCARD_COUNT;
                        INC_IFACE_TCP_SYN_DISCARD_COUNT (pIfaceInfo);
                        FwlLogMessage (FWL_TCP_SYN_FLOODING, u4IfaceNum, pBuf,
                                       FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                       (UINT1 *) FWL_MSG_ATTACK);

                        return FWL_FAILURE;
                    }
                    else
                    {
                        gFwlAclInfo.u2SynPktsAllowed++;
                    }
                }
            }                    /*  Intercept Packets for rate limiting ? */
            break;

        case FWL_UDP:
            if (gFwlAclInfo.u1UdpFloodInspect == FWL_ENABLE)
            {
                if (gFwlAclInfo.u2UdpPktsAllowed >= gFwlAclInfo.u2UdpLimit)
                {
                    /* Discard the packet and update the statistics */
                    INC_UDP_DISCARD_COUNT;
                    INC_IFACE_UDP_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (FWL_UDP_FLOODING, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);

                    return FWL_FAILURE;
                }
                else
                {
                    gFwlAclInfo.u2UdpPktsAllowed++;
                }
            }
            break;

        case FWL_ICMP:
        case FWL_ICMPV6:
            if (gFwlAclInfo.u1IcmpFloodInspect == FWL_ENABLE)
            {
                if (((pIcmp->u1Type == FWL_ICMP_TYPE_ECHO_REQUEST) ||
                     (pIcmp->u1Type == FWL_ICMPv6_TYPE_ECHO_REQUEST)) &&
                    (gFwlAclInfo.u2IcmpPktsAllowed >= gFwlAclInfo.u2IcmpLimit))
                {
                    /* Discard the packet and update the statistics */
                    INC_ICMP_DISCARD_COUNT;
                    INC_IFACE_ICMP_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (FWL_ICMP_FLOODING, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);

                    return FWL_FAILURE;
                }
                else
                {
                    gFwlAclInfo.u2IcmpPktsAllowed++;
                }
            }
            break;

        default:
            break;
    }                            /* switch (u1Proto) */

    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlProcessRouterPktForRateLimiting               */
/*                                                                          */
/*    Description        : Inspects the packets to do rate limiting         */
/*                                                                          */
/*    Input(s)           : pBuf         -- Pointer to the Packet            */
/*                         pIfaceInfo   -- Pointer to the Interface         */
/*                         pHLInfo      -- Transport Header Info            */
/*                         pIcmp        -- ICMP Header Info                 */
/*                         u4IfaceNum   -- interface number                 */
/*                         u1Proto      -- Protocol  number                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/
PUBLIC UINT4
FwlProcessRouterPktForRateLimiting (tCRU_BUF_CHAIN_HEADER * pBuf,
                                    tIfaceInfo * pIfaceInfo,
                                    tHLInfo * pHLInfo,
                                    tIcmpInfo * pIcmp,
                                    UINT4 u4IfaceNum, UINT1 u1Proto)
{
    UNUSED_PARAM (pIcmp);

    switch (u1Proto)
    {
        case FWL_TCP:

            if (gFwlAclInfo.u1TcpInterceptEnable == FWL_ENABLE)
            {

                /* Process the packet against the TCP SYN flooding prevention */
                if ((pHLInfo->u1Syn == FWL_SET) &&
                    (pHLInfo->u1Ack == FWL_NOT_SET))
                {
                    if (gFwlAclInfo.u2RtrSynPktsAllowed >=
                        gFwlAclInfo.u2TcpSynLimit)
                    {
                        /* Discard the packet and update the statistics */
                        INC_TCP_SYN_DISCARD_COUNT;
                        INC_IFACE_TCP_SYN_DISCARD_COUNT (pIfaceInfo);
                        FwlLogMessage (FWL_TCP_SYN_FLOODING, u4IfaceNum, pBuf,
                                       FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                       (UINT1 *) FWL_MSG_ATTACK);

                        return FWL_FAILURE;
                    }
                    else
                    {
                        gFwlAclInfo.u2RtrSynPktsAllowed++;
                    }
                }
            }                    /*  Intercept Packets for rate limiting ? */
            break;

        case FWL_UDP:
            if (gFwlAclInfo.u1UdpFloodInspect == FWL_ENABLE)
            {
                if (gFwlAclInfo.u2UdpPktsAllowed >= gFwlAclInfo.u2UdpLimit)
                {
                    /* Discard the packet and update the statistics */
                    INC_UDP_DISCARD_COUNT;
                    INC_IFACE_UDP_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (FWL_UDP_FLOODING, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);

                    return FWL_FAILURE;
                }
                else
                {
                    gFwlAclInfo.u2UdpPktsAllowed++;
                }
            }
            break;

        case FWL_ICMP:
        case FWL_ICMPV6:
            if (gFwlAclInfo.u1IcmpFloodInspect == FWL_ENABLE)
            {
                if (gFwlAclInfo.u2IcmpPktsAllowed >= gFwlAclInfo.u2IcmpLimit)
                {
                    /* Discard the packet and update the statistics */
                    INC_ICMP_DISCARD_COUNT;
                    INC_IFACE_ICMP_DISCARD_COUNT (pIfaceInfo);
                    FwlLogMessage (FWL_ICMP_FLOODING, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);

                    return FWL_FAILURE;
                }
                else
                {
                    gFwlAclInfo.u2IcmpPktsAllowed++;
                }
            }
            break;

        default:
            break;
    }                            /* switch (u1Proto) */

    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlConcurrentSessionsProcess                     */
/*                                                                          */
/*    Description        : Inspects the sessions count and limits the       */
/*                         new session based on direction                   */
/*                                                                          */
/*    Input(s)           : pBuf - Pointer to the packet                     */
/*                         pIpHdr - IP Header info                          */
/*                         pHLInfo - TCP/UDP header info                    */
/*                         u4IfaceNum - Interface number                    */
/*                         u1Direction - IN or OUT                          */
/*                         u1TcpUpdateFlag - Indicates if Update is to be   */
/*                                    done if proto is TCP.                 */
/*                                                                          */
/*    Output(s)          : ppOutUserSessionNode - store out session node    */
/*                                    if out node is found, otherwise NULL  */
/*                       : ppInUserSessionNode - store in session node      */
/*                                    if in node is found, otherwise NULL   */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlConcurrentSessionsProcess (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIpHeader * pIpHdr,
                              tHLInfo * pHLInfo,
                              UINT4 u4IfaceNum,
                              UINT1 u1Direction,
                              tOutUserSessionNode ** ppOutUserSessionNode,
                              tInUserSessionNode ** ppInUserSessionNode,
                              UINT1 u1TcpUpdateFlag)
{
    /* Perform Concurrent Sessions Limiting, only if enabled */
    if (gFwlAclInfo.u1LimitConcurrentSess == FWL_DISABLE)
    {
        return (FWL_SUCCESS);
    }

    if (FwlInspectConcurrentSessions (pBuf,
                                      pIpHdr,
                                      pHLInfo,
                                      u4IfaceNum,
                                      u1Direction,
                                      ppOutUserSessionNode,
                                      ppInUserSessionNode) == FWL_FAILURE)
    {
        /*Logging already done in above function call. */
        return (FWL_FAILURE);
    }

    /* Update the concurrent session if protocol is not TCP
     * or u1TcpUpdateFlag is set. */
    if ((pIpHdr->u1Proto != FWL_TCP) || (u1TcpUpdateFlag == FWL_UPDATE_TCP))
    {
        if (FwlUpdateConcurrentSessionInfo (*pIpHdr,
                                            *pHLInfo,
                                            u1Direction,
                                            *ppOutUserSessionNode,
                                            *ppInUserSessionNode)
            == FWL_FAILURE)
        {
            /* This happens, when the system is no longer have enough
             * memory. Log the error message as resource unavailable */
            FwlLogMessage (FWL_MEMORY_FAILURE, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL, NULL);

            return (FWL_FAILURE);
        }
    }                            /*End-of-FwlUpdateConcurrentSessionInfo */

    return FWL_SUCCESS;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlInspectConcurrentSessions                     */
/*                                                                          */
/*    Description        : Inspects the sessions count and limits the       */
/*                         new session based on direction                   */
/*                                                                          */
/*    Input(s)           : pBuf - Pointer to the packet                     */
/*                         pIpHdr - IP Header info                          */
/*                         HLInfo - TCP/UDP header info                     */
/*                         u4IfaceNum - Interface number                    */
/*                         u1Direction - IN or OUT                          */
/*                                                                          */
/*    Output(s)          : ppOutUserSessionNode - store out session node    */
/*                                    if out node is found, otherwise NULL  */
/*                       : ppInUserSessionNode - store in session node      */
/*                                    if in node is found, otherwise NULL   */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlInspectConcurrentSessions (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tIpHeader * pIpHdr,
                              tHLInfo * pHLInfo,
                              UINT4 u4IfaceNum,
                              UINT1 u1Direction,
                              tOutUserSessionNode ** ppOutUserSessionNode,
                              tInUserSessionNode ** ppInUserSessionNode)
{
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    tInUserSessionNode *pInUserSessionNode = NULL;
    UINT1               u1Proto = pIpHdr->u1Proto;

    /* If number of total sessions in the State Hash Table 
     * reaches FWL_MAX_STATEFUL_SESSIONS_LIMIT, drop the packet */

    if (FWL_TOTAL_SESSION_COUNT >= FWL_MAX_STATEFUL_SESSIONS_LIMIT)
    {
        FwlLogMessage (FWL_SESSION_LIMIT_ATTACK, u4IfaceNum, pBuf,
                       FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                       (UINT1 *) FWL_MSG_STATEFUL_TABLE_FULL);
        return (FWL_FAILURE);
    }

    /* Perform Concurrent Sessions Limiting, only if enabled */
    if (gFwlAclInfo.u1LimitConcurrentSess == FWL_DISABLE)
    {
        return (FWL_SUCCESS);
    }

    if (u1Direction == FWL_DIRECTION_IN)
    {
        /* If number of inbound sessions in the State Hash Table
         * reaches FWL_MAX_IN_STATEFUL_SESSIONS_LIMIT the packet */

        if (FWL_IN_TOTAL_SESSION_COUNT >= FWL_MAX_IN_STATEFUL_SESSIONS_LIMIT)
        {
            FwlLogMessage (FWL_SESSION_LIMIT_ATTACK, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_IN_TOTAL_SESSIONS_FULL);
            return (FWL_FAILURE);
        }

        /* Rate limit for inbound udp packets */
        if (u1Proto == FWL_UDP)
        {
            if (FWL_IN_TOTAL_UDP_SESSION_COUNT >= FWL_MAX_IN_UDP_SESSIONS_LIMIT)
            {
                FwlLogMessage (FWL_SESSION_LIMIT_ATTACK, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_IN_TOTAL_UDP_SESSIONS_FULL);
                return (FWL_FAILURE);
            }
        }                        /* u1Proto = FWL_UDP */

        /* Rate limit for inbound icmp packets */
        if (u1Proto == FWL_ICMP)
        {
            if (FWL_IN_TOTAL_ICMP_SESSION_COUNT >=
                FWL_MAX_IN_ICMP_SESSIONS_LIMIT)
            {
                FwlLogMessage (FWL_SESSION_LIMIT_ATTACK, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_IN_TOTAL_ICMP_SESSIONS_FULL);
                return (FWL_FAILURE);
            }
        }                        /* u1Proto == FWL_ICMP */

        /* For an inbound packet, the destination address and port
         * refers to the DMZ host in the LAN. */
        pInUserSessionNode = FwlGetInUserSessionNode (pIpHdr->DestAddr,
                                                      pHLInfo->u2DestPort,
                                                      u1Proto);

        /* If they match, check the sessions limit for the user */
        if (pInUserSessionNode != NULL)
        {
            UINT2               u2InLimit =
                pInUserSessionNode->u2InSessionLimit;

            /* If the Administrator has not configured inbound limit,
             * then use default limit FWL_MAX_IN_USER_SESSIONS_COUNT */

            if (pInUserSessionNode->u2InSessionCount >=
                ((u2InLimit == FWL_ZERO) ? FWL_MAX_IN_USER_SESSIONS_LIMIT
                 : u2InLimit))
            {
                FwlLogMessage (FWL_SESSION_LIMIT_ATTACK, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_IN_USER_SESSION_FULL);
                return (FWL_FAILURE);
            }
            *ppInUserSessionNode = pInUserSessionNode;
            return (FWL_SUCCESS);

        }                        /* (pInUserSessionNode != NULL) */
    }
    else                        /* Dir = Out */
    {
        pOutUserSessionNode = FwlGetOutUserSessionNode (pIpHdr->SrcAddr);

        if (pOutUserSessionNode != NULL)
        {
            if (pOutUserSessionNode->u2OutSessionCount >=
                FWL_MAX_OUT_USER_SESSIONS_LIMIT)
            {
                FwlLogMessage (FWL_SESSION_LIMIT_ATTACK, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_OUT_USER_SESSION_FULL);
                return (FWL_FAILURE);
            }
            *ppOutUserSessionNode = pOutUserSessionNode;
            return (FWL_SUCCESS);
        }                        /* (pOutUserSessionNode != NULL) */

    }                            /* Dir = OUT */

    return (FWL_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddOutUserSessionNode                         */
/*                                                                          */
/*    Description        : Adds a node to the outbound session List         */
/*                                                                          */
/*    Input(s)           : u4LocalIP - IP address of the user in the out    */
/*                                     session list                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : tOutUserSessionNode * | NULL                     */
/****************************************************************************/
PRIVATE tOutUserSessionNode *
FwlAddOutUserSessionNode (tFwlIpAddr LocalIP)
{
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    UINT4               u4MemAllocateStatus = MEM_FAILURE;
    tTMO_SLL           *pOutSessLst = NULL;

    u4MemAllocateStatus =
        MemAllocateMemBlock (FWL_OUT_SESS_PID,
                             (UINT1 **) (VOID *) &pOutUserSessionNode);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        return (NULL);
    }

    FWL_BZERO (pOutUserSessionNode, tOutUserSessionNode);

    TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pOutUserSessionNode);

    FWL_IP_ADDR_COPY (&pOutUserSessionNode->LocalIP, &LocalIP);

    if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pOutSessLst = FWL_OUT_SESSION_LIST;
    }
    else
    {
        pOutSessLst = FWL_OUT_V6_SESSION_LIST;
    }

    TMO_SLL_Add (pOutSessLst, (tTMO_SLL_NODE *) pOutUserSessionNode);

    return (pOutUserSessionNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlAddInUserSessionNode                          */
/*                                                                          */
/*    Description        : Adds a node to the inbound session List          */
/*                                                                          */
/*    Input(s)           : u4LocalIP - IP address of the user in the in     */
/*                                     session list                         */
/*                         u2Port    - Port number of the service           */
/*                         u1proto   - Protocol number (TCP/UDP)            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : tInUserSessionNode * | NULL                      */
/****************************************************************************/
PRIVATE tInUserSessionNode *
FwlAddInUserSessionNode (tFwlIpAddr LocalIP, UINT2 u2ServicePort, UINT1 u1Proto)
{
    tInUserSessionNode *pInUserSessionNode = NULL;
    tTMO_SLL           *pInSessLst = NULL;
    UINT4               u4MemAllocateStatus = MEM_FAILURE;

    u4MemAllocateStatus =
        MemAllocateMemBlock (FWL_IN_SESS_PID,
                             (UINT1 **) (VOID *) &pInUserSessionNode);
    if (u4MemAllocateStatus == MEM_FAILURE)
    {
        return (NULL);
    }
    FWL_BZERO (pInUserSessionNode, tInUserSessionNode);

    TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pInUserSessionNode);

    FWL_IP_ADDR_COPY (&pInUserSessionNode->LocalIP, &LocalIP);
    if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pInSessLst = FWL_IN_SESSION_LIST;
    }
    else
    {
        pInSessLst = FWL_IN_V6_SESSION_LIST;
    }

    pInUserSessionNode->u2ServicePort = u2ServicePort;
    pInUserSessionNode->u1Protocol = u1Proto;

    TMO_SLL_Add (pInSessLst, (tTMO_SLL_NODE *) pInUserSessionNode);

    return (pInUserSessionNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDelOutUserSessionNode                         */
/*                                                                          */
/*    Description        : Deletes the node from the outbound session List  */
/*                                                                          */
/*    Input(s)           : u4Flush      -- For flushing the session list    */
/*                         u4LocalIP - IP address of the user in the out    */
/*                                     session list                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PRIVATE UINT4
FwlDelOutUserSessionNode (UINT4 u4Flush, tFwlIpAddr LocalIP)
{
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    tOutUserSessionNode *pNextNode = NULL;
    tTMO_SLL           *pOutSessLst = NULL;
    UINT4               u4AddrMatch = OSIX_FALSE;

    /* Perform Concurrent Sessions Limiting, only if enabled */
    if (gFwlAclInfo.u1LimitConcurrentSess == FWL_DISABLE)
    {
        return (FWL_SUCCESS);
    }

    if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pOutSessLst = FWL_OUT_SESSION_LIST;
    }
    else
    {
        pOutSessLst = FWL_OUT_V6_SESSION_LIST;
    }
    pOutUserSessionNode = (tOutUserSessionNode *) TMO_SLL_First (pOutSessLst);

    while (pOutUserSessionNode != NULL)
    {
        /* If firewall is disabled (FWL_FLUSH_STATEFUL_ENTRIES)
         * or the out session count is 0, then delete the node */
        if ((LocalIP.u4AddrType == FWL_IP_VERSION_4) &&
            (pOutUserSessionNode->LocalIP.v4Addr == LocalIP.v4Addr))
        {
            u4AddrMatch = OSIX_TRUE;
        }
        else if ((LocalIP.u4AddrType == FWL_IP_VERSION_6) &&
                 (Ip6AddrCompare
                  (pOutUserSessionNode->LocalIP.v6Addr,
                   LocalIP.v6Addr) == IP6_ZERO))
        {
            u4AddrMatch = OSIX_TRUE;
        }
        if ((u4Flush == FWL_FLUSH_STATEFUL_ENTRIES) ||
            ((u4AddrMatch == OSIX_TRUE) &&
             (pOutUserSessionNode->u2OutSessionCount == FWL_ZERO)))
        {
            pNextNode =
                (tOutUserSessionNode *) TMO_SLL_Next (pOutSessLst,
                                                      (tTMO_SLL_NODE *)
                                                      pOutUserSessionNode);
            /* Call Tmo Delete Node */
            TMO_SLL_Delete (pOutSessLst, (tTMO_SLL_NODE *) pOutUserSessionNode);
            MemReleaseMemBlock (FWL_OUT_SESS_PID,
                                (UINT1 *) pOutUserSessionNode);

            pOutUserSessionNode = pNextNode;
        }
        else
        {
            pOutUserSessionNode =
                (tOutUserSessionNode *) TMO_SLL_Next (pOutSessLst,
                                                      (tTMO_SLL_NODE *)
                                                      pOutUserSessionNode);
        }

    }                            /* End of While-Loop */

    if (u4Flush == FWL_FLUSH_STATEFUL_ENTRIES)
    {
        FWL_OUT_TOTAL_SESSION_COUNT = FWL_ZERO;
    }
    return FWL_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDelInUserSessionNode                          */
/*                                                                          */
/*    Description        : Deletes the node from the inbound session List   */
/*                                                                          */
/*    Input(s)           : u4Flush      -- For flushing the session list    */
/*                         u4LocalIP - IP address of the user in the in     */
/*                                     session list                         */
/*                         u2Port    - Port number of the service which the */
/*                                     is listening                         */
/*                         u1proto   - Protocol number (TCP/UDP)            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PRIVATE UINT4
FwlDelInUserSessionNode (UINT4 u4Flush, tFwlIpAddr LocalIP, UINT2 u2Port,
                         UINT1 u1Proto)
{
    tInUserSessionNode *pInUserSessionNode = NULL;
    tInUserSessionNode *pNextNode = NULL;
    tTMO_SLL           *pInSessLst = NULL;
    INT1                i1AddrMatch = FWL_INVALID;

    if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {

        pInSessLst = FWL_IN_SESSION_LIST;
    }
    else
        pInSessLst = FWL_IN_V6_SESSION_LIST;

    pInUserSessionNode = (tInUserSessionNode *) TMO_SLL_First (pInSessLst);

    while (pInUserSessionNode != NULL)
    {
        /* Node can be deleted if;
         * 1. Firewall is disabled (FWL_FLUSH_STATEFUL_ENTRIES)
         * 2. In session count is 0 and administrator has not
         *    configured the limit (u2InLimit == 0)
         */
        if (((pInUserSessionNode->LocalIP.u4AddrType == FWL_IP_VERSION_4) &&
             (pInUserSessionNode->LocalIP.v4Addr == LocalIP.v4Addr))
            || ((pInUserSessionNode->LocalIP.u4AddrType == FWL_IP_VERSION_6) &&
                (Ip6AddrCompare
                 (pInUserSessionNode->LocalIP.v6Addr,
                  LocalIP.v6Addr) == IP6_ZERO)))
        {
            i1AddrMatch = FWL_TRUE;
        }

        if ((u4Flush == FWL_FLUSH_STATEFUL_ENTRIES) ||
            ((i1AddrMatch == FWL_TRUE) &&
             (pInUserSessionNode->u2ServicePort == u2Port) &&
             (pInUserSessionNode->u1Protocol == u1Proto) &&
             (pInUserSessionNode->u2InSessionCount == FWL_ZERO) &&
             (pInUserSessionNode->u2InSessionLimit == FWL_ZERO)))
        {
            pNextNode =
                (tInUserSessionNode *) TMO_SLL_Next (pInSessLst,
                                                     (tTMO_SLL_NODE *)
                                                     pInUserSessionNode);
            /* Call Tmo Delete Node */
            TMO_SLL_Delete (pInSessLst, (tTMO_SLL_NODE *) pInUserSessionNode);
            MemReleaseMemBlock (FWL_IN_SESS_PID, (UINT1 *) pInUserSessionNode);

            pInUserSessionNode = pNextNode;
        }
        else
        {
            pInUserSessionNode =
                (tInUserSessionNode *) TMO_SLL_Next (pInSessLst,
                                                     (tTMO_SLL_NODE *)
                                                     pInUserSessionNode);
        }

    }                            /* End of While-Loop */

    if (u4Flush == FWL_FLUSH_STATEFUL_ENTRIES)
    {
        FWL_IN_TOTAL_SESSION_COUNT = FWL_ZERO;
        FWL_IN_TOTAL_UDP_SESSION_COUNT = FWL_ZERO;
        FWL_IN_TOTAL_ICMP_SESSION_COUNT = FWL_ZERO;
    }
    return FWL_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDecrementOutUserSession                       */
/*                                                                          */
/*    Description        : Decrement outbound session count in the outlist  */
/*                         and total out session count                      */
/*                                                                          */
/*    Input(s)           : LocalIP - IP address of the user in the out    */
/*                                     session list                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PRIVATE UINT4
FwlDecrementOutUserSession (tFwlIpAddr LocalIP)
{
    tOutUserSessionNode *pOutUserSessionNode = NULL;

    pOutUserSessionNode = FwlGetOutUserSessionNode (LocalIP);

    if (pOutUserSessionNode != NULL)
    {
        FWL_DEC_OUT_USER_SESSION_COUNT (pOutUserSessionNode);
        FWL_DEC_OUT_TOTAL_SESSION_COUNT;
        return (FWL_SUCCESS);
    }
    return (FWL_FAILURE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDecrementInUserSession                        */
/*                                                                          */
/*    Description        : Decrement inbound session count in the inlist    */
/*                         and total in session count                       */
/*                                                                          */
/*    Input(s)           : u4LocalIP - IP address of the user in the out    */
/*                                     session list                         */
/*                         u2Port    - Port number of the service which the */
/*                                     is listening                         */
/*                         u1Proto   - Protocol number (TCP/UDP)            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PRIVATE UINT4
FwlDecrementInUserSession (tFwlIpAddr LocalIP, UINT2 u2Port, UINT1 u1Proto)
{
    tInUserSessionNode *pInUserSessionNode = NULL;

    if (u1Proto == FWL_ICMP)
    {
        FWL_DEC_IN_TOTAL_ICMP_SESSION_COUNT;
        FWL_DEC_IN_TOTAL_SESSION_COUNT;
        return (FWL_SUCCESS);
    }

    pInUserSessionNode = FwlGetInUserSessionNode (LocalIP, u2Port, u1Proto);

    if (pInUserSessionNode != NULL)
    {
        FWL_DEC_IN_USER_SESSION_COUNT (pInUserSessionNode);

        if (pInUserSessionNode->u1Protocol == FWL_UDP)
        {
            FWL_DEC_IN_TOTAL_UDP_SESSION_COUNT;
        }
        FWL_DEC_IN_TOTAL_SESSION_COUNT;
        return (FWL_SUCCESS);
    }
    return (FWL_FAILURE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetOutUserSessionNode                         */
/*                                                                          */
/*    Description        : Get the node from the outbound session List      */
/*                                                                          */
/*    Input(s)           : u4LocalIP - IP address of the user in the out    */
/*                                     session list                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : tOutUserSessionNode * | NULL                     */
/****************************************************************************/
PRIVATE tOutUserSessionNode *
FwlGetOutUserSessionNode (tFwlIpAddr LocalIP)
{
    tOutUserSessionNode *pOutUserSessionNode = NULL;
    tTMO_SLL           *pOutSessLst = NULL;

    if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pOutSessLst = FWL_OUT_SESSION_LIST;
    }
    else
        pOutSessLst = FWL_OUT_V6_SESSION_LIST;

    TMO_SLL_Scan (pOutSessLst, pOutUserSessionNode, tOutUserSessionNode *)
    {
        if ((LocalIP.u4AddrType == FWL_IP_VERSION_4) &&
            (pOutUserSessionNode->LocalIP.v4Addr == LocalIP.v4Addr))
        {
            break;
        }
        else if ((LocalIP.u4AddrType == FWL_IP_VERSION_6) &&
                 (Ip6AddrCompare
                  (pOutUserSessionNode->LocalIP.v6Addr,
                   LocalIP.v6Addr) == IP6_ZERO))
        {
            break;
        }
    }                            /* End TMO_SLL_Scan */

    return (pOutUserSessionNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlGetInUserSessionNode                          */
/*                                                                          */
/*    Description        : Get the node from the inbound session List       */
/*                                                                          */
/*    Input(s)           : LocalIP - IP address of the user in the in     */
/*                                     session list                         */
/*                         u2Port    - Port number of the service which the */
/*                                     is listening                         */
/*                         u1proto   - Protocol number (TCP/UDP)            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : tInUserSessionNode * | NULL                      */
/****************************************************************************/
PRIVATE tInUserSessionNode *
FwlGetInUserSessionNode (tFwlIpAddr LocalIP, UINT2 u2Port, UINT1 u1Proto)
{
    tInUserSessionNode *pInUserSessionNode = NULL;
    tTMO_SLL           *pInSessLst = NULL;

    if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
    {
        pInSessLst = FWL_IN_SESSION_LIST;
    }
    else
        pInSessLst = FWL_IN_V6_SESSION_LIST;

    TMO_SLL_Scan (pInSessLst, pInUserSessionNode, tInUserSessionNode *)
    {
        /* for an inbound packet, the destination address and port
         * refers to the DMZ host in the LAN. 
         */
        if (LocalIP.u4AddrType == FWL_IP_VERSION_4)
        {
            if ((pInUserSessionNode->LocalIP.v4Addr == LocalIP.v4Addr) &&
                (pInUserSessionNode->u1Protocol == u1Proto) &&
                (pInUserSessionNode->u2ServicePort == u2Port))
            {
                break;
            }
        }
        else if (LocalIP.u4AddrType == FWL_IP_VERSION_6)
        {
            if ((pInUserSessionNode->u1Protocol == u1Proto) &&
                (pInUserSessionNode->u2ServicePort == u2Port) &&
                (Ip6AddrCompare
                 (pInUserSessionNode->LocalIP.v6Addr,
                  LocalIP.v6Addr) == IP6_ZERO))
            {
                break;
            }
        }
    }                            /* End TMO_SLL_Scan */

    return (pInUserSessionNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlUpdateConcurrentSessionInfo                   */
/*                                                                          */
/*    Description        : Updates the node entries and sessions count      */
/*                                                                          */
/*    Input(s)           : IpHdr - IP Header info                           */
/*                         HLInfo - TCP/UDP header info                     */
/*                         u1Direction - Direction (IN/OUT)                 */
/*                         u4IfaceNum - Interface number                    */
/*                         pOutUserSessionNode - out session node           */
/*                         pInUserSessionNode - in session node             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FWL_SUCCESS | FWL_FAILURE                        */
/****************************************************************************/
PUBLIC UINT4
FwlUpdateConcurrentSessionInfo (tIpHeader IpHdr,
                                tHLInfo HLInfo,
                                UINT1 u1Direction,
                                tOutUserSessionNode * pOutUserSessionNode,
                                tInUserSessionNode * pInUserSessionNode)
{
    /* Perform Concurrent Sessions Limiting, only if enabled */
    if (gFwlAclInfo.u1LimitConcurrentSess == FWL_DISABLE)
    {
        return (FWL_SUCCESS);
    }

    if (u1Direction == FWL_DIRECTION_OUT)
    {
        if (pOutUserSessionNode == NULL)
        {
            if ((pOutUserSessionNode =
                 FwlAddOutUserSessionNode (IpHdr.SrcAddr)) == NULL)
            {
                /* This happens, when the system is no longer have enough
                 * memory. So delete the stateful node for this session.
                 * Log the error message as resource unavailable */
                return FWL_FAILURE;
            }
        }
        /* Increment the outbound session count */
        FWL_INC_OUT_USER_SESSION_COUNT (pOutUserSessionNode);
        FWL_INC_OUT_TOTAL_SESSION_COUNT;
    }
    else                        /* Direction = IN */
    {
        if (pInUserSessionNode == NULL)
        {
            if (IpHdr.u1Proto != FWL_ICMP)
            {
                if ((pInUserSessionNode = FwlAddInUserSessionNode (IpHdr.DestAddr,    /* Local IP */
                                                                   HLInfo.u2DestPort,    /*Loc Service */
                                                                   IpHdr.
                                                                   u1Proto)) ==
                    NULL)
                {
                    /* This happens, when the system is no longer have enough
                     * memory. So delete the stateful node for this session.
                     * Log the error message as resource unavailable */
                    return FWL_FAILURE;
                }
            }                    /* proto != icmp */
        }                        /* pInUserSessionNode == NULL */

        /* Increment the Total inbound session count */
        FWL_INC_IN_TOTAL_SESSION_COUNT;

        switch (IpHdr.u1Proto)
        {
            case FWL_UDP:
                FWL_INC_IN_TOTAL_UDP_SESSION_COUNT;
                /* Dont break, fall through to update inbound count */
            case FWL_TCP:
                FWL_INC_IN_USER_SESSION_COUNT (pInUserSessionNode);
                break;
            case FWL_ICMP:
                FWL_INC_IN_TOTAL_ICMP_SESSION_COUNT;
                break;
            default:
                break;
        }                        /*End-of-Switch */
    }                            /* Dir = IN */

    return FWL_SUCCESS;
}                                /*End-of-Function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlRemoveUserSession                             */
/*                                                                          */
/*    Description        : Decrements and then deletes the session.         */
/*                                                                          */
/*    Input(s)           : u4Flush - Flush type                             */
/*                         pState - State Entry Nodember                    */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
PUBLIC VOID
FwlRemoveUserSession (UINT4 u4Flush, tStatefulSessionNode * pState)
{
    /* Perform Concurrent Sessions Limiting, only if enabled */
    if (gFwlAclInfo.u1LimitConcurrentSess == FWL_DISABLE)
    {
        return;
    }

    if (pState->u1Direction == FWL_DIRECTION_OUT)
    {
        FwlDecrementOutUserSession (pState->LocalIP);
        FwlDelOutUserSessionNode (u4Flush, pState->LocalIP);
    }
    else
    {
        UINT2               u2Port = FWL_ZERO;
        u2Port = (UINT2) FWL_LOCAL_PORT (pState, pState->u1Protocol);

        FwlDecrementInUserSession (pState->LocalIP, u2Port, pState->u1Protocol);
        FwlDelInUserSessionNode (u4Flush, pState->LocalIP,
                                 u2Port, pState->u1Protocol);
    }
}                                /* End-of-Function */

/* ------- EoF FWLLIMIT.C -------- */
