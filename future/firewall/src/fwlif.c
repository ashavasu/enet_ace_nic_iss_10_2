/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlif.c,v 1.9 2014/11/23 09:17:32 siva Exp $
 *
 * Description:This file provides the routines to forwarding      
 *             module to call the accesslist to process the packet
 *             against the configured filters.                
 *
 *******************************************************************/
#include "fwlinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : FwlAclProcessPktForInFiltering             */
/*                                                                           */
/*    Description               : This routine is called by the external     */
/*                                module (Forwarding or Fast forwarding)     */
/*                                whenever the packet is coming in through   */
/*                                an interface, that to be inspected         */
/*                                against the configured InFilters by the   */
/*                                AccessList.                                */
/*                                                                           */
/*    Input(s)                  : pBuf         -- Pointer to the incoming pkt*/
/*                                u4InIfaceNum -- Incoming interface number  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : SUCCESS if the packet is to permitted,     */
/*                                otherwise FAILURE.                         */
/*****************************************************************************/

/* This function is being called by the Forwarding module (IP), after the 
 * after the basic packet validation and before the routing decision has
 * been made.
 */

#ifdef __STDC__
PUBLIC UINT4
FwlAclProcessPktForInFiltering (tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 u4InIfaceNum)
#else
PUBLIC UINT4
FwlAclProcessPktForInFiltering (pBuf, u4InIfaceNum)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4InIfaceNum;
#endif
{
    tIfaceInfo         *pIfaceNode = NULL;
    tHLInfo             HLInfo;
    tIpHeader           IpHdr;
    tIcmpInfo           IcmpHdr;
    UINT4               u4Attack = FWL_ZERO;
    UINT4               u4Status = FWL_ZERO;
    UINT4               u4TransHdrLen = FWL_ZERO;
    UINT2               u2FragOffsetBit = FWL_ZERO;
    UINT1               u1IpVer = FWL_ZERO;
    UINT1               u1TcpCodeBit = FWL_ZERO;
    UINT1               u1Urg = FWL_ZERO;

    FWL_DBG (FWL_DBG_IF, "\n Entering into the Interface module \n");

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering the fn. Reset the FwlAclProcessPktForInFiltering \n");
    /* Checking whether the Firewall is enabled or not */

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
                  "\n The Packet is received by Firewall on interface %d "
                  "in the IN direction\n", u4InIfaceNum);

    if (gu1FirewallStatus == FWL_ENABLE)
    {
        FWL_DBG (FWL_DBG_CTRL_FLOW,
                 "\n Fwl is enable, "
                 "control transfers to Inspection Module \n");
#ifndef SECURITY_KERNEL_WANTED
        FwlLock ();
#endif
        if (FwlInspectPkt (pBuf, u4InIfaceNum, FWL_DIRECTION_IN) == FWL_SUCCESS)
        {
#ifndef SECURITY_KERNEL_WANTED
            FwlUnLock ();
#endif
            /* The packet is permitted */
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting fn. FwlAclProcessPktForInFiltering \n");
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ACTION, "FWL",
                     "\n The Packet is permitted \n");
            MOD_TRC (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
                     "\n The Packet exits from Firewall \n");
            return FWL_SUCCESS;
        }
        else
        {
            /* Deny the packet */
            /* If ICMP_GENERATE switch is enabled then generate the ICMP
             * Error message.
             */
#ifndef SECURITY_KERNEL_WANTED
            FwlUnLock ();
#endif
            MOD_TRC (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
                     "\n The Packet is dropped by the Firewall \n");
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ACTION, "FWL",
                     "\n The Packet is Denied \n");

            FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, u1IpVer);
            u1IpVer >>= FWL_FOUR;

            if ((u1IpVer == FWL_IP_VERSION_4) &&
                (gFwlAclInfo.u1IcmpGenerate == FWL_ICMP_GENERATE))
            {
                /* Call the ICMP routine to generate error message of type
                 * Communication Administratively Prohibited 
                 */
                FwlErrorMessageGenerate (pBuf, u4InIfaceNum);
            }
            else if ((u1IpVer == FWL_IP_VERSION_6) &&
                     (gFwlAclInfo.u1Icmpv6Generate == FWL_ICMPv6_GENERATE))
            {
                /* Call the ICMP routine to generate error message of type
                 * Communication Administratively Prohibited 
                 */
                FwlErrorV6MessageGenerate (pBuf, u4InIfaceNum);
            }

            /* Check whether the Packet discarded count have exceeded the 
             * Threshold limit */
            pIfaceNode = FwlDbaseSearchIface (u4InIfaceNum);
            if (NULL != pIfaceNode)
            {
                if (FwlChkIfExtInterface (u4InIfaceNum) == FWL_SUCCESS)
                {
                    if ((pIfaceNode->ifaceStat.u4PktsDenyCount != FWL_ZERO) &&
                        (pIfaceNode->ifaceStat.u4PktsDenyCount %
                         (UINT4) pIfaceNode->ifaceStat.i4TrapThreshold) ==
                        FWL_ZERO)
                    {
                        /* Send Trap to the SNMP Manager with the corresponding
                         * Interface Index */
                        if (FwlSendThresholdExceededTrap (u4InIfaceNum,
                                                          pIfaceNode->ifaceStat.
                                                          u4PktsDenyCount)
                            == FWL_FAILURE)
                        {
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                                     "\n Failed while Sending the Threshold "
                                     "Exceeded Trap\n");
                        }
                    }
                }
                if ((gFwlAclInfo.u4TotalPktsDenyCount != FWL_ZERO) &&
                    (gFwlAclInfo.u4TotalPktsDenyCount %
                     (UINT4) gFwlAclInfo.i4TrapThreshold) == FWL_ZERO)
                {
                    /* Send Trap to the SNMP Manager with the Global Interface
                     * Interface Index */
                    if (FwlSendThresholdExceededTrap (FWL_GLOBAL_IDX,
                                                      gFwlAclInfo.
                                                      u4TotalPktsDenyCount)
                        == FWL_FAILURE)
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                                 "\n Failed while Sending the Threshold "
                                 "Exceeded Trap\n");
                    }
                }
            }
            /* Packet is to be Denied and the called module 
             * should release the Packet buffer
             */
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting fn. FwlAclProcessPktForInFiltering \n");

            return FWL_FAILURE;
        }
    }
    else
    {
        u4Status = FwlUpdateIpHeaderInfoFromPkt (pBuf, &IpHdr);

        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
        {
            u2FragOffsetBit = FWL_FRAG_OFFSET_BIT;
        }
        else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
        {
            u2FragOffsetBit = FWL_IP6_FRAG_OFFSET_BIT;
        }

        if (((IpHdr.u1Proto == FWL_TCP) || (IpHdr.u1Proto == FWL_UDP)) &&
            ((IpHdr.u2FragOffset & u2FragOffsetBit) == FWL_ZERO))
        {
            FwlUpdateHLInfoFromPkt (pBuf, &HLInfo, IpHdr.u1Proto,
                                    IpHdr.u1HeadLen);
            /* Get the Transport(TCP/UDP) Header Length */
            u4TransHdrLen = FwlGetTransportHeaderLength (pBuf,
                                                         IpHdr.u1Proto,
                                                         IpHdr.u1HeadLen);
        }

        /* Update ICMP Info structure if the packet is ICMP */
        if (((IpHdr.u1Proto == FWL_ICMP) || (IpHdr.u1Proto == FWL_ICMPV6)) &&
            ((IpHdr.u2FragOffset & u2FragOffsetBit) == FWL_ZERO))
        {
            /* Updation of Icmp type and Code in the ICMP info struct  */
            FwlUpdateIcmpv4v6InfoFromPkt (pBuf, &IcmpHdr, IpHdr.u1HeadLen);
        }

        u4Attack =
            FwlCheckAttacks (pBuf, u4InIfaceNum, &IpHdr, &HLInfo,
                             &IcmpHdr, FWL_DIRECTION_IN, u4TransHdrLen);
        if (FWL_PERMITTED != u4Attack)
        {
            FwlLogMessage (u4Attack, u4InIfaceNum, pBuf, FWL_LOG_BRF,
                           FWLLOG_CRITICAL_LEVEL, (UINT1 *) FWL_MSG_ATTACK);

            return FWL_FAILURE;
        }
        /* Inspect for Win Nuke Attack:
         * Port Number 137-139: NetBIOS
         * Port Number 445: SMB
         */
        if (gFwlAclInfo.u1NetBiosFilteringEnable == FWL_FILTERING_ENABLED)
        {
            if ((HLInfo.u2DestPort == FWL_SMB_PORT_NUMBER) ||
                ((HLInfo.u2DestPort >= FWL_NET_BIOS_PORT_NUMBER_137) &&
                 (HLInfo.u2DestPort <= FWL_NET_BIOS_PORT_NUMBER_139)))
            {
                if (IpHdr.u1Proto == FWL_TCP)
                {
                    FWL_GET_1_BYTE (pBuf, (UINT4)
                                    (IpHdr.u1HeadLen +
                                     FWL_TCP_CODE_BIT_OFFSET), u1TcpCodeBit);
                    /* Getting the urgent bit value */
                    u1Urg = (UINT1) ((u1TcpCodeBit & FWL_TCP_URG_MASK) >>
                                     FWL_FIVE);

                    if (u1Urg == FWL_URG_SET)
                    {
                        FwlLogMessage (FWL_WIN_NUKE_ATTACK, u4InIfaceNum, pBuf,
                                       FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                       (UINT1 *) FWL_MSG_ATTACK);
                        if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                        {
                            INC_INSPECT_COUNT;
                            INC_DISCARD_COUNT;
                        }
                        else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
                        {
                            INC_V6_INSPECT_COUNT;
                            INC_V6_DISCARD_COUNT;
                        }

                        return FWL_FAILURE;

                    }
                }
                FwlLogMessage (FWL_NETBIOS_FIL, u4InIfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_NETBIOS_FILTER);

                /* Increment the statistics. */
                if (IpHdr.u1IpVersion == FWL_IP_VERSION_4)
                {
                    INC_INSPECT_COUNT;
                    INC_DISCARD_COUNT;
                }
                else if (IpHdr.u1IpVersion == FWL_IP_VERSION_6)
                {
                    INC_V6_INSPECT_COUNT;
                    INC_V6_DISCARD_COUNT;
                }

                return FWL_FAILURE;
            }
        }
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlAclProcessPktForInFiltering \n");

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ACTION, "FWL",
             "\n Firewall is disabled \n");
    MOD_TRC (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
             "\n The Packet exits from Firewall \n");

    /* Firewall is not enabled, returns SUCCESS */
    UNUSED_PARAM (u4Status);
    return FWL_SUCCESS;
}                                /* End of the function -- FwlAclProcessPktForInFiltering */

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : FwlAclProcessPktForOutFiltering            */
/*                                                                           */
/*    Description               : This routine is called by the external     */
/*                                module (Forwarding or Fast forwarding)     */
/*                                whenever the packet is going out through   */
/*                                an interface, that to be inspected         */
/*                                against the configured OutFilters by the   */
/*                                AccessList.                                */
/*                                                                           */
/*    Input(s)                  : pBuf         -- Pointer to the outgoing pkt*/
/*                                u4OutIfaceNum -- Outgoing interface number */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : SUCCESS if the packet is to permitted,     */
/*                                otherwise FAILURE.                         */
/*****************************************************************************/

/* This function is called by the forwarding module (IP), after the route 
 * lookup (once the outgoing interface is determined) and before 
 * fragmentation is being made.
 */

#ifdef __STDC__
PUBLIC UINT4
FwlAclProcessPktForOutFiltering (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 u4OutIfaceNum)
#else
PUBLIC UINT4
FwlAclProcessPktForOutFiltering (pBuf, u4OutIfaceNum)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4OutIfaceNum;
#endif
{
    tIfaceInfo         *pIfaceNode = NULL;
    UINT1               u1IpVer = FWL_ZERO;

    /* Checking whether the Firewall is enabled or not */

    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
                  "\n The Packet is received by Firewall on interface\
                   %d in the OUT direction \n", u4OutIfaceNum);

    if (gu1FirewallStatus == FWL_ENABLE)
    {
        FWL_DBG (FWL_DBG_CTRL_FLOW,
                 "\n Fwl is enable, "
                 "control transfers to Inspection Module \n");
#ifndef SECURITY_KERNEL_WANTED
        FwlLock ();
#endif
        if (FwlInspectPkt (pBuf, u4OutIfaceNum, FWL_DIRECTION_OUT) ==
            FWL_SUCCESS)
        {
#ifndef SECURITY_KERNEL_WANTED
            FwlUnLock ();
#endif
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting fn. FwlAclProcessPktForOutFiltering \n");
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ACTION, "FWL",
                     "\n The Packet is permitted \n");
            MOD_TRC (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
                     "\n The Packet exits from Firewall \n");

            /* The packet is permitted */
            return FWL_SUCCESS;
        }
        else
        {
            /* Deny the packet */
            /* If ICMP_GENERATE switch is enabled then generate the ICMP
             * Error message.
             */
#ifndef SECURITY_KERNEL_WANTED
            FwlUnLock ();
#endif
            MOD_TRC (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
                     "\n The Packet is dropped by the Firewall \n");
            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ACTION, "FWL",
                     "\n The Packet is Denied\n");

            FWL_GET_1_BYTE (pBuf, FWL_IP_HLEN_OFFSET, u1IpVer);
            u1IpVer >>= FWL_FOUR;

            if ((u1IpVer == FWL_IP_VERSION_4) &&
                (gFwlAclInfo.u1IcmpGenerate == FWL_ICMP_GENERATE))
            {
                /* Call the ICMP routine to generate error message of type
                 * Communication Administratively Prohibited 
                 */
                MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ERROR, "FWL",
                         "\n ICMP Error Message is Generated "
                         "for denied Packet \n");
                FwlErrorMessageGenerate (pBuf, u4OutIfaceNum);
            }
            else if ((u1IpVer == FWL_IP_VERSION_6) &&
                     (gFwlAclInfo.u1Icmpv6Generate == FWL_ICMPv6_GENERATE))
            {
                /* Call the ICMP routine to generate error message of type
                 * Communication Administratively Prohibited 
                 */
                FwlErrorV6MessageGenerate (pBuf, u4OutIfaceNum);
            }

            /* Check whether the Packet discarded count have exceeded the 
             * Threshold limit */
            pIfaceNode = FwlDbaseSearchIface (u4OutIfaceNum);
            if (NULL != pIfaceNode)
            {
                if (FwlChkIfExtInterface (u4OutIfaceNum) == FWL_SUCCESS)
                {
                    if ((pIfaceNode->ifaceStat.u4PktsDenyCount != FWL_ZERO) &&
                        (pIfaceNode->ifaceStat.u4PktsDenyCount %
                         (UINT4) pIfaceNode->ifaceStat.i4TrapThreshold) ==
                        FWL_ZERO)
                    {
                        /* Send Trap to the SNMP Manager with the corresponding
                         * Interface Index */
                        if (FwlSendThresholdExceededTrap (u4OutIfaceNum,
                                                          pIfaceNode->ifaceStat.
                                                          u4PktsDenyCount)
                            == FWL_FAILURE)
                        {
                            MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                                     "\n Failed while Sending the Threshold "
                                     "Exceeded Trap\n");
                        }
                    }
                }
                if ((gFwlAclInfo.u4TotalPktsDenyCount != FWL_ZERO) &&
                    (gFwlAclInfo.u4TotalPktsDenyCount %
                     (UINT4) gFwlAclInfo.i4TrapThreshold) == FWL_ZERO)
                {
                    /* Send Trap to the SNMP Manager with the Global Interface
                     * Interface Index */
                    if (FwlSendThresholdExceededTrap (FWL_GLOBAL_IDX,
                                                      gFwlAclInfo.
                                                      u4TotalPktsDenyCount)
                        == FWL_FAILURE)
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_TRAP, "FWL",
                                 "\n Failed while Sending the Threshold "
                                 "Exceeded Trap\n");
                    }
                }
            }

            /* Packet is to be Denied and the called module 
             * should release the Packet buffer
             */
            FWL_DBG (FWL_DBG_EXIT,
                     "\n Exiting fn. FwlAclProcessPktForOutFiltering \n");

            return FWL_FAILURE;
        }
    }
    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting the fn. FwlAclProcessPktForOutFiltering \n");

    MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_ACTION, "FWL",
             "\n Firewall is disabled \n");
    MOD_TRC (gFwlAclInfo.u4FwlTrc, DATA_PATH_TRC, "FWL",
             "\n The Packet exits from Firewall \n");
    /* Firewall is not enabled, returns SUCCESS */
    return FWL_SUCCESS;

}                                /* End of the function -- FwlAclProcessPktForOutFiltering */

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : FwlHandleInterfaceIndication               */
/*                                                                           */
/*    Description               : Handles indications to Firewall on         */
/*                                Creation, Enabling, Disabling or Deletion  */
/*                                of Interface.                              */
/*                                                                           */
/*    Inputs                    : u4Interface - Interface number on which    */
/*                                the indication is received.                */
/*                                u4Status - Indicates the status of the     */
/*                                no IP for the interface                    */
/*              CREATE_AND_GO - Interface has been created with IP assigned  */
/*              NOT_IN_SERVICE - The interface became down                   */
/*              ACTIVE - The Interface became active, with IP assigned to it */
/*              DESTROY - The interface just got destroyed                   */
/*                                                                           */
/*    Return                    : FWL_SUCCESS or FWL_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
FwlHandleInterfaceIndication (UINT4 u4Interface, UINT4 u4Status)
{
    tIfaceInfo         *pIfaceNode = NULL;
    /* If the incoming status is CREATE_AND_WAIT or CREATE_AND_GO, enable 
     * the firewall on the interface. If the status is DESTROY,
     * disable firewall on the interface. Currently there is no need to handle
     * NOT_IN_SERVICE or ACTIVE */

    pIfaceNode = FwlDbaseSearchIface (u4Interface);

    switch (u4Status)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            if (pIfaceNode != NULL)
            {
                /* FireWall already enabled for the interface */
                return FWL_SUCCESS;
            }
            else
            {
                /* interface node allocation and initialisation */
                if (FwlIfaceMemAllocate ((UINT1 **) (VOID *) &pIfaceNode) ==
                    FWL_SUCCESS)
                {
                    FwlSetDefaultIfaceValue (u4Interface, pIfaceNode);
                    pIfaceNode->u1IfaceType = FWL_EXTERNAL_IF;
                }
                else
                {
                    /* send a trap for memory failure */
                    FwlGenerateMemFailureTrap (FWL_IFACE_ALLOC_FAILURE);
                    INC_MEM_FAILURE_COUNT;
                    gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
                }
            }
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            /* Nothing to do */
            break;

        case DESTROY:
            if (pIfaceNode != NULL)
            {
                u4Status = FwlDbaseDeleteIface (u4Interface);
                if (u4Status != FWL_SUCCESS)
                {
                    /* Unable to delete the interface */
                    return FWL_FAILURE;
                }
            }

            break;

        default:
            break;
    }
    return FWL_SUCCESS;
}

/****************************************************************************/
/*                 End of the file -- fwlif.h                               */
/****************************************************************************/
