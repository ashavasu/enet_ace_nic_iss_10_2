/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlserv.c,v 1.16 2015/11/04 06:36:08 siva Exp $
 *
 * Description:This file is used to process the packets   
 *              against the service independent filters and
 *              takes the action specified in it.        
 *
 *******************************************************************/
#include "fwlinc.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlProcessPktForServiceIndpFiltering             */
/*                                                                          */
/*    Description        : Inspects the packets against the Service indepen-*/
/*                         dent filters.                                    */
/*                                                                          */
/*    Input(s)           : pIpHeader    -- Pointer to the IP Header         */
/*                         pIfaceInfo   -- Pointer to the Interface         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if packet is to permited, otherwise  */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlProcessPktForServiceIndpFiltering (tIpHeader * pIpHeader,
                                      tHLInfo * pHLInfo,
                                      tIfaceInfo * pIfaceInfo,
                                      tCRU_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 u4IfaceNum)
#else
PUBLIC UINT4
FwlProcessPktForServiceIndpFiltering (pIpHeader, pHLInfo,
                                      pIfaceInfo, pBuf, u4IfaceNum)
     tIpHdr             *pIpHeader;
     tHLInfo            *pHLInfo;
     tIfaceInfo         *pIfaceInfo;
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4IfaceNum = FWL_ZERO;
#endif

{
    UINT4               u4IfIndex = FWL_ZERO;
    INT4                i4Status = FWL_FAILURE;

    UINT1               u1PktOpCode = FWL_ZERO;
    UINT1               u1Urg = FWL_ZERO;    /* check for urgent bit set/not set */
    UINT1               u1TcpCodeBit = FWL_ZERO;    /* fot getting Tcp Flags */

    UNUSED_PARAM (u1PktOpCode);

    FWL_DBG (FWL_DBG_ENTRY, "\n FwlProcessPktForServiceIndpFiltering \n");

    /* Process the packet against the IP address spoofing */
    if ((gFwlAclInfo.u1IpSpoofFilteringEnable == FWL_FILTERING_ENABLED) ||
        (gFwlAclInfo.u1Ipv6SpoofFilteringEnable == FWL_FILTERING_ENABLED))
    {
        MOD_TRC (gFwlAclInfo.u4FwlTrc, FWL_TRC_INSPECT, "FWL",
                 "\n IP Address Spoof Checking\n");

        if (pIpHeader->u1IpVersion == FWL_IP_VERSION_4)
        {
            i4Status =
                SecUtilIpIsLocalNet (pIpHeader->SrcAddr.v4Addr, &u4IfIndex);
        }
        else if (pIpHeader->u1IpVersion == FWL_IP_VERSION_6)
        {
            i4Status =
                Sec6UtilIpIsLocalNet (&pIpHeader->SrcAddr.v6Addr, &u4IfIndex);
        }

        if (i4Status == FWL_SUCCESS)
        {
            if (FwlChkIfExtInterface (u4IfIndex) == FWL_FAILURE)
            {
                /* 
                 * The packet is spoofed one and Update the interface
                 * specific statistics
                 */
                if (pIpHeader->u1IpVersion == FWL_IP_VERSION_4)
                {

                    INC_IFACE_SPOOF_DISCARD_COUNT (pIfaceInfo);

                    /* Update the global statistics */
                    INC_SPOOF_DISCARD_COUNT;
                }
                else
                {
                    INC_V6_IFACE_SPOOF_DISCARD_COUNT (pIfaceInfo);
                    INC_V6_SPOOF_DISCARD_COUNT;

                }

                FwlLogMessage (FWL_IP_SPOOF_ATTACK, u4IfaceNum, pBuf,
                               FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                               (UINT1 *) FWL_MSG_ATTACK);

                return FWL_FAILURE;
            }
        }
    }

    /* Inspect for Win Nuke Attack:
     * Port Number 137-139: NetBIOS
     * Port Number 445: SMB
     */
    if (gFwlAclInfo.u1NetBiosFilteringEnable == FWL_FILTERING_ENABLED)
    {
        if ((pHLInfo->u2DestPort == FWL_SMB_PORT_NUMBER) ||
            ((pHLInfo->u2DestPort >= FWL_NET_BIOS_PORT_NUMBER_137) &&
             (pHLInfo->u2DestPort <= FWL_NET_BIOS_PORT_NUMBER_139)))
        {
            if (pIpHeader->u1Proto == FWL_TCP)
            {
                FWL_GET_1_BYTE (pBuf, (UINT4)
                                (pIpHeader->u1HeadLen +
                                 FWL_TCP_CODE_BIT_OFFSET), u1TcpCodeBit);
                /* Getting the urgent bit value */
                u1Urg = (UINT1) ((u1TcpCodeBit & FWL_TCP_URG_MASK) >> FWL_FIVE);

                if (u1Urg == FWL_URG_SET)
                {
                    /* Statistics updation will be done by FwlInspectPkt */
                    FwlLogMessage (FWL_WIN_NUKE_ATTACK, u4IfaceNum, pBuf,
                                   FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                                   (UINT1 *) FWL_MSG_ATTACK);
                    return FWL_FAILURE;

                }
            }

            FwlLogMessage (FWL_NETBIOS_FIL, u4IfaceNum, pBuf,
                           FWL_LOG_BRF, FWLLOG_CRITICAL_LEVEL,
                           (UINT1 *) FWL_MSG_NETBIOS_FILTER);

            /* Statistics updation will be done by FwlInspectPkt */
            return FWL_FAILURE;
        }
    }

    FWL_DBG (FWL_DBG_EXIT, "\n FwlProcessPktForServiceIndpFiltering \n");
    return FWL_SUCCESS;

}                                /* End of fn. FwlProcessPktForServiceIndpFiltering */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlProcessPktForLanInFiltering                     */
/*     DESCRIPTION      : Check whether source ip address in the ip header   */
/*                        can be reached through the lan interface           */
/*     INPUT            : tIpHdr - IP Header information                     */
/*     OUTPUT           : None.                                              */
/*     RETURNS          : FWL_SUCCESS   or                                   */
/*                        FWL_FAILURE                                        */
/*****************************************************************************/
PUBLIC UINT4
FwlProcessPktForLanInFiltering (tIpHeader * pIpHeader)
{
#ifdef IP6_WANTED
    tNetIpv6RtInfoQueryMsg RtIPv6Query;
    tNetIpv6RtInfo      NetIp6RtInfo;
#endif
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4IfIndex = FWL_ZERO;
    UINT2               u2Port = FWL_ZERO;
    FWL_DBG (FWL_DBG_ENTRY, "\n FwlProcessPktForLanInFiltering \n");

    /* Route lookup should be done for only unicast packet .... */

    if (pIpHeader->u1IpVersion == FWL_IP_VERSION_4)
    {
        if (FwlValidateIpAddress (pIpHeader->SrcAddr.v4Addr) != FWL_UCAST_ADDR)
        {
            return (FWL_FAILURE);
        }
        MEMSET (&NetIpRtInfo, FWL_ZERO, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, FWL_ZERO, sizeof (tRtInfoQueryMsg));

        RtQuery.u4DestinationIpAddress = pIpHeader->SrcAddr.v4Addr;
        RtQuery.u4DestinationSubnetMask = FWL_SUBNET_MASK;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
        {
            return (FWL_FAILURE);
        }
        u2Port = (UINT2) NetIpRtInfo.u4RtIfIndx;
        NetIpv4GetCfaIfIndexFromPort (u2Port, &u4IfIndex);
    }
#ifdef IP6_WANTED
    else if (pIpHeader->u1IpVersion == FWL_IP_VERSION_6)
    {
        if (Ip6AddrType (&(pIpHeader->SrcAddr.v6Addr)) != ADDR6_UNICAST)
        {
            return (FWL_FAILURE);
        }
        MEMSET (&NetIp6RtInfo, FWL_ZERO, sizeof (tNetIpv6RtInfo));
        MEMSET (&RtIPv6Query, FWL_ZERO, sizeof (tNetIpv6RtInfoQueryMsg));
        MEMCPY (NetIp6RtInfo.Ip6Dst.u1_addr, pIpHeader->SrcAddr.v6Addr.u1_addr,
                FWL_IP6ADDR_LEN);
        NetIp6RtInfo.u1Prefixlen = (UINT1) FWL_IP6ADDR_LEN;
        RtIPv6Query.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        if (NetIpv6GetRoute (&RtIPv6Query, &NetIp6RtInfo) != NETIPV6_SUCCESS)
        {
            return FWL_FAILURE;
        }
        u4IfIndex = NetIp6RtInfo.u4Index;
    }
#endif

    /* Interface should not be external... */
    if (FwlChkIfExtInterface (u4IfIndex) == FWL_FAILURE)
    {
        FWL_DBG (FWL_DBG_EXIT, "\n FwlProcessPktForLanInFiltering \n");
        return (FWL_SUCCESS);
    }

    FWL_DBG (FWL_DBG_EXIT, "\n FwlProcessPktForLanInFiltering \n");
    return (FWL_FAILURE);
}

/****************************************************************************/
/*             End of the file -- fwlserv.c                                 */
/****************************************************************************/
