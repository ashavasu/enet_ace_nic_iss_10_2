##############################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.2 2011/05/16 11:21:56 siva Exp $
#
# Description: This file contains all the warning options
#              that are used for building this module.
##############################################################

#
# (C) 1999 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 13 march 2000                                 |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+

#include the LR make.h and make.rule
include ../LR/make.h
include ../LR/make.rule

#Project base directories
PROJECT_NAME   = FutureFirewall
PROJECT_BASE_DIR  = ${BASE_DIR}/firewall
PROJECT_SOURCE_DIR   = $(PROJECT_BASE_DIR)/src
PROJECT_INCLUDE_DIR = $(PROJECT_BASE_DIR)/inc
PROJECT_OBJECT_DIR   = $(PROJECT_BASE_DIR)/obj

#Project compilation switches
PROJECT_COMPILATION_SWITCHES = 

#Project include files
PROJECT_INCLUDE_FILES = $(PROJECT_INCLUDE_DIR)/fwlbufif.h \
                       $(PROJECT_INCLUDE_DIR)/fwlconfig.h \
                       $(PROJECT_INCLUDE_DIR)/fwldebug.h \
                       $(PROJECT_INCLUDE_DIR)/fwldefn.h \
                       $(PROJECT_INCLUDE_DIR)/fwlglob.h \
                       $(PROJECT_INCLUDE_DIR)/fwlinc.h \
                       $(PROJECT_INCLUDE_DIR)/fwlif.h \
                       $(PROJECT_INCLUDE_DIR)/fwlmacs.h \
                       $(PROJECT_INCLUDE_DIR)/fwlport.h \
                       $(PROJECT_INCLUDE_DIR)/fwlgen.h \
                       $(PROJECT_INCLUDE_DIR)/fwlproto.h \
                       $(PROJECT_INCLUDE_DIR)/fwlsnif.h \
                       $(PROJECT_INCLUDE_DIR)/fwltdfs.h \
                       $(PROJECT_INCLUDE_DIR)/fwlweb.h \
                       $(PROJECT_INCLUDE_DIR)/fwltcp.h \
                       $(PROJECT_INCLUDE_DIR)/fwlstat.h \
                       $(PROJECT_INCLUDE_DIR)/fwltmrif.h

ifeq (DSECURITY_KERNEL_MAKE_WANTED, $(findstring DSECURITY_KERNEL_MAKE_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
PROJECT_INCLUDE_FILES += $(PROJECT_INCLUDE_DIR)/fwlwtdfs.h \
                       $(PROJECT_INCLUDE_DIR)/fwlwdefs.h \
                       $(PROJECT_INCLUDE_DIR)/fwlwincs.h
endif                       


#project final include directories
PROJECT_FINAL_INCLUDES_DIRS   = -I$(PROJECT_INCLUDE_DIR) \
           $(COMMON_INCLUDE_DIRS) 

#Project final include files
PROJECT_FINAL_INCLUDE_FILES   = $(PROJECT_INCLUDE_FILES)

#dependencies
PROJECT_DEPENDENCIES        =   $(COMMON_DEPENDENCIES) \
           $(PROJECT_FINAL_INCLUDE_FILES) \
           $(PROJECT_BASE_DIR)/Makefile \
           $(PROJECT_BASE_DIR)/make.h
