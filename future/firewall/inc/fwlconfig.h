/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlconfig.h,v 1.2 2010/12/09 05:52:13 siva Exp $
 *
 * Description:This file contains all constants related         
 *              to configuration of Firewall      
 *
 *******************************************************************/

/*****************************************************************************/
/*   The maximum number of Filter pointers can be stored in a Rule structure */ 
/*****************************************************************************/

#define  FWL_MAX_FILTERS_IN_RULE   8

/*****************************************************************************/
/*   The Typical number of Rules, filters and Acl filters values             */ 
/*****************************************************************************/
#define  FWL_MAX_NUM_OF_RULES  MAX_FWL_RULES_INFO

/* 
 * This specifies the Typical num of Rules that can be  configured. This value 
 * also be changed in run time through MIB Object.  
 */

#define  FWL_MAX_NUM_OF_FILTERS  MAX_FWL_FILTERS_INFO
/* 
 * This is to configure the Filters and this also can be changed during
 * Run time through MIB object.  
 */

#define  FWL_MAX_NUM_OF_ACL_FILTER  MAX_FWL_ACL_INFO

/* 
 * The Maximum number of acl filters.
 */

#define  FWL_DEFAULT_TCP_SYN_LIMIT           50    /*
                                                    * The default syn limit per 
                                                    * configured time period.
                                                    */

#define  FWL_DEFAULT_INTERCEPT_TIME_OUT       1    /*
                                                    * Intercept time out in 
                                                    * seconds
                                                    */

#define FWL_DEFAULT_UDP_RATE_LIMIT                50
#define FWL_DEFAULT_ICMP_RATE_LIMIT               5

/*
 * Macros for the Trap Treshold above which Trap would
 * sent to the Manager
 */

#define FWL_MIN_TRAP_THRESHOLD                    50
#define FWL_MAX_TRAP_THRESHOLD                    50000
#define FWL_DEF_TRAP_THRESHOLD                    FWL_MIN_TRAP_THRESHOLD

         
/*****************************************************************************/
/*                         End of  File fwlconfig.h                          */
/*****************************************************************************/
