/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlif.h,v 1.6 2013/07/27 13:10:09 siva Exp $
 *
 * Description:This file contains constants, typedefs &   
 *             macros related to interface.           
 *
 *******************************************************************/

#ifndef _FWLIF_H 
#define _FWLIF_H 

/*****************************************************************************/
/*       Structure to hold the IP header Information                         */
/*****************************************************************************/

typedef struct {
       UINT1        u1VerHeadLen;   /* Includes IP version & IP header Len */
       UINT1        u1Tos;          /* Holds either TOS or DSCP values */
       UINT2        u2TotalLen;
       UINT2        u2Id;
       UINT2        u2FragOffset;   /* Includes Fragment Offset and Flags */
       UINT1        u1TimeToLive;   /* IP header TTL */
       UINT1        u1Proto;
       UINT2        u2HeaderCkSum;
       UINT4        u4SrcAddr;
       UINT4        u4DestAddr;
       UINT4        u4Options;      /* Holds the Code bits alone */
       tFwlIpAddr   SrcAddr;
       tFwlIpAddr   DestAddr;
       UINT4        u4FlowLabel;
       UINT1        u1IpVersion;
       UINT1        u1HeadLen;      /* IP header length */
       UINT1        u1OptCount;     /* IP options count */
       UINT1        au1Padding;
} tIpHeader;


/*****************************************************************************/
/*       Structure to hold the Higher layer information                      */
/*****************************************************************************/

typedef struct {
    UINT2  u2SrcPort;
    UINT2  u2DestPort;
    UINT4  u4SeqNum;
    UINT4  u4AckNum;
    UINT1  u1DataOffset;      /* TCP data offset */
    UINT1  u1TcpCodeBit;      /* TCP Flags Bits  */
    UINT2  u2WindowAdvertised;
    UINT2  u2CkSum;
    UINT2  u2UrgPtr;          /* Urgent pointer  */
    UINT1  u1Ack;
    UINT1  u1Rst;
    UINT1  u1Fin;
    UINT1  u1Syn;

} tHLInfo;

/*****************************************************************************/
/*       Structure to hold the ICMP Type and Code Info                       */
/*****************************************************************************/

typedef struct {
    UINT1  u1Type;
    UINT1  u1Code;
    UINT2  u2CkSum;
    UINT2  u2IcmpSeqNum; /* If ICMP type is Router Advertisement 
                            this would hold Lifetime of entries present */
    UINT2  u2Id;         /* If ICMP type is Router Advertisement this would
                            hold entry count & entry size */
} tIcmpInfo;

#endif /* _FWLIF_H */

/*****************************************************************************/
/*       End of file -- fwlif.h                                              */
/*****************************************************************************/
