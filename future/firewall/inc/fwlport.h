/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlport.h,v 1.1 2010/11/10 14:59:40 prabuc Exp $
 *
 * Description:This file contains definitions and         
 *             variables that are modified during porting 
 *
 *******************************************************************/

/* The following value is got from CFA which is defined in the LR make.h*/
#define  FWL_SYS_TIME_FACTOR          SYS_NUM_OF_TIME_UNITS_IN_A_SEC             
/* The maximum number of Interfaces that are allowed - given by CFA */ 
#define  FWL_MAX_NUM_OF_IF            ( CFA_MAX_INTERFACES_IN_SYS + 1 ) 

#define FWL_INIT_COMPLETE(u4Status) 	lrInitComplete(u4Status)

/*****************************************************************************/
/*              End Of File fwlport.h                                        */
/*****************************************************************************/
