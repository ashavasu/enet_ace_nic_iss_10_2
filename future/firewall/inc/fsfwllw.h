/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfwllw.h,v 1.12 2016/02/27 10:04:59 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlGlobalMasterControlSwitch ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalICMPControlSwitch ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalIpSpoofFiltering ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalSrcRouteFiltering ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalTinyFragmentFiltering ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalTcpIntercept ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalTrap ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalTrace ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalMaxFilters ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalMaxRules ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalUrlFiltering ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalNetBiosFiltering ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalNetBiosLan2Wan ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalICMPv6ControlSwitch ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalIpv6SpoofFiltering ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalLogFileSize ARG_LIST((UINT4 *));

INT1
nmhGetFwlGlobalLogSizeThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFwlGlobalIdsLogSize ARG_LIST((UINT4 *));

INT1
nmhGetFwlGlobalIdsLogThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFwlGlobalIdsVersionInfo ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlGlobalReloadIds ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalIdsStatus ARG_LIST((INT4 *));

INT1
nmhGetFwlGlobalLoadIdsRules ARG_LIST((INT4 *));

INT1
nmhGetFwlDosAttackAcceptRedirect ARG_LIST((INT4 *));


INT1
nmhGetFwlDosAttackAcceptSmurfAttack ARG_LIST((INT4 *));

INT1
nmhGetFwlDosLandAttack ARG_LIST((INT4 *));

INT1
nmhGetFwlDosShortHeaderAttack ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlGlobalMasterControlSwitch ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalICMPControlSwitch ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalIpSpoofFiltering ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalSrcRouteFiltering ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalTinyFragmentFiltering ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalTcpIntercept ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalTrap ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalTrace ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalUrlFiltering ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalNetBiosFiltering ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalNetBiosLan2Wan ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalICMPv6ControlSwitch ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalIpv6SpoofFiltering ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalLogFileSize ARG_LIST((UINT4 ));

INT1
nmhSetFwlGlobalLogSizeThreshold ARG_LIST((UINT4 ));

INT1
nmhSetFwlGlobalIdsLogSize ARG_LIST((UINT4 ));

INT1
nmhSetFwlGlobalIdsLogThreshold ARG_LIST((UINT4 ));

INT1
nmhSetFwlGlobalReloadIds ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalIdsStatus ARG_LIST((INT4 ));

INT1
nmhSetFwlGlobalLoadIdsRules ARG_LIST((INT4 ));

INT1
nmhSetFwlDosAttackAcceptRedirect ARG_LIST((INT4 ));


INT1
nmhSetFwlDosAttackAcceptSmurfAttack ARG_LIST((INT4 ));
INT1
nmhSetFwlDosLandAttack ARG_LIST((INT4 ));

INT1
nmhSetFwlDosShortHeaderAttack ARG_LIST((INT4 ));




/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlGlobalMasterControlSwitch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalICMPControlSwitch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalIpSpoofFiltering ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalSrcRouteFiltering ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalTinyFragmentFiltering ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalTcpIntercept ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalTrap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalUrlFiltering ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalNetBiosFiltering ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalNetBiosLan2Wan ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalICMPv6ControlSwitch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalIpv6SpoofFiltering ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalLogFileSize ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FwlGlobalLogSizeThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FwlGlobalIdsLogSize ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FwlGlobalIdsLogThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FwlGlobalReloadIds ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalIdsStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlGlobalLoadIdsRules ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlDosAttackAcceptRedirect ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlDosAttackAcceptSmurfAttack ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlDosLandAttack ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlDosShortHeaderAttack ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlGlobalMasterControlSwitch ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalICMPControlSwitch ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalIpSpoofFiltering ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalSrcRouteFiltering ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalTinyFragmentFiltering ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalTcpIntercept ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalUrlFiltering ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalNetBiosFiltering ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalNetBiosLan2Wan ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalICMPv6ControlSwitch ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalIpv6SpoofFiltering ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalLogFileSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalLogSizeThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalIdsLogSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalIdsLogThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalReloadIds ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalIdsStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlGlobalLoadIdsRules ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlDefnTcpInterceptThreshold ARG_LIST((INT4 *));

INT1
nmhGetFwlDefnInterceptTimeout ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlDefnTcpInterceptThreshold ARG_LIST((INT4 ));

INT1
nmhSetFwlDefnInterceptTimeout ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlDefnTcpInterceptThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlDefnInterceptTimeout ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnTcpInterceptThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlDefnInterceptTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnFilterTable. */
INT1
nmhValidateIndexInstanceFwlDefnFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FwlDefnFilterTable  */

INT1
nmhGetFirstIndexFwlDefnFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlFilterSrcAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlFilterDestAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlFilterProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterSrcPort ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlFilterDestPort ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlFilterAckBit ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterRstBit ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterTos ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterAccounting ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterHitClear ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterHitsCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFwlFilterAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterFlowId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFwlFilterDscp ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlFilterSrcAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFwlFilterDestAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFwlFilterProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterSrcPort ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFwlFilterDestPort ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFwlFilterAckBit ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterRstBit ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterTos ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterAccounting ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterHitClear ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterFlowId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFwlFilterDscp ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlFilterSrcAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FwlFilterDestAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FwlFilterProtocol ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterSrcPort ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FwlFilterDestPort ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FwlFilterAckBit ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterRstBit ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterTos ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterAccounting ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterHitClear ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterFlowId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FwlFilterDscp ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlFilterRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnRuleTable. */
INT1
nmhValidateIndexInstanceFwlDefnRuleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FwlDefnRuleTable  */

INT1
nmhGetFirstIndexFwlDefnRuleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnRuleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlRuleFilterSet ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlRuleRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlRuleFilterSet ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFwlRuleRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlRuleFilterSet ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FwlRuleRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnRuleTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnAclTable. */
INT1
nmhValidateIndexInstanceFwlDefnAclTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnAclTable  */

INT1
nmhGetFirstIndexFwlDefnAclTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnAclTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlAclAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFwlAclSequenceNumber ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFwlAclAclType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFwlAclLogTrigger ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFwlAclFragAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFwlAclRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlAclAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFwlAclSequenceNumber ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFwlAclLogTrigger ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFwlAclFragAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFwlAclRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlAclAction ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FwlAclSequenceNumber ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FwlAclLogTrigger ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FwlAclFragAction ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FwlAclRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnAclTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnIfTable. */
INT1
nmhValidateIndexInstanceFwlDefnIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnIfTable  */

INT1
nmhGetFirstIndexFwlDefnIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlIfIfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfIpOptions ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfFragments ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfFragmentSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlIfICMPType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfICMPCode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfICMPv6MsgType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlIfIfType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfIpOptions ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfFragments ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfFragmentSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFwlIfICMPType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfICMPCode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfICMPv6MsgType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlIfIfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfIpOptions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfFragments ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfFragmentSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FwlIfICMPType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfICMPCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfICMPv6MsgType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnDmzTable. */
INT1
nmhValidateIndexInstanceFwlDefnDmzTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnDmzTable  */

INT1
nmhGetFirstIndexFwlDefnDmzTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnDmzTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlDmzRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlDmzRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlDmzRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnDmzTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlUrlFilterTable. */
INT1
nmhValidateIndexInstanceFwlUrlFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FwlUrlFilterTable  */

INT1
nmhGetFirstIndexFwlUrlFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlUrlFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlUrlHitCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFwlUrlFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlUrlFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlUrlFilterRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlUrlFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlStatInspectedPacketsCount ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalPacketsAccepted ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalIcmpPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalSynPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalIpSpoofedPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalSrcRoutePacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalTinyFragmentPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalFragmentedPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalLargeFragmentPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalIpOptionPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatTotalAttacksPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatMemoryAllocationFailCount ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatIPv6InspectedPacketsCount ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatIPv6TotalPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatIPv6TotalPacketsAccepted ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatIPv6TotalIcmpPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatIPv6TotalSpoofedPacketsDenied ARG_LIST((UINT4 *));

INT1
nmhGetFwlStatIPv6TotalAttacksPacketsDenied ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FwlStateTable. */
INT1
nmhValidateIndexInstanceFwlStateTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlStateTable  */

INT1
nmhGetFirstIndexFwlStateTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlStateTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlStateEstablishedTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFwlStateLocalState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFwlStateRemoteState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ,INT4 *));
INT1
nmhGetFwlStateLogLevel ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFwlStateCallStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FwlStatIfTable. */
INT1
nmhValidateIndexInstanceFwlStatIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlStatIfTable  */

INT1
nmhGetFirstIndexFwlStatIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlStatIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlStatIfFilterCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlStatIfPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfPacketsAccepted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfSynPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfIcmpPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfIpSpoofedPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfSrcRoutePacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfTinyFragmentPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfFragmentPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfIpOptionPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfClear ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlIfTrapThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlStatIfIPv6PacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfIPv6PacketsAccepted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfIPv6IcmpPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfIPv6SpoofedPacketsDenied ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFwlStatIfClearIPv6 ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlStatIfClear ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlIfTrapThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlStatIfClearIPv6 ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlStatIfClear ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlIfTrapThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlStatIfClearIPv6 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlStatIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlStatClear ARG_LIST((INT4 *));

INT1
nmhGetFwlStatClearIPv6 ARG_LIST((INT4 *));

INT1
nmhGetFwlTrapThreshold ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlStatClear ARG_LIST((INT4 ));

INT1
nmhSetFwlStatClearIPv6 ARG_LIST((INT4 ));

INT1
nmhSetFwlTrapThreshold ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlStatClear ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlStatClearIPv6 ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FwlTrapThreshold ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlStatClear ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlStatClearIPv6 ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlTrapThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlTrapMemFailMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlTrapAttackMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlTrapFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFwlIdsTrapFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlTrapMemFailMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFwlTrapAttackMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlTrapMemFailMessage ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FwlTrapAttackMessage ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlTrapMemFailMessage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlTrapAttackMessage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnBlkListTable. */
INT1
nmhValidateIndexInstanceFwlDefnBlkListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnBlkListTable  */

INT1
nmhGetFirstIndexFwlDefnBlkListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnBlkListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlBlkListHitsCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFwlBlkListEntryType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFwlBlkListRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlBlkListRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlBlkListRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnBlkListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnWhiteListTable. */
INT1
nmhValidateIndexInstanceFwlDefnWhiteListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnWhiteListTable  */

INT1
nmhGetFirstIndexFwlDefnWhiteListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnWhiteListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlWhiteListHitsCount ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFwlWhiteListRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlWhiteListRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlWhiteListRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlDefnWhiteListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlDefnIPv6DmzTable. */
INT1
nmhValidateIndexInstanceFwlDefnIPv6DmzTable ARG_LIST
((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FwlDefnIPv6DmzTable  */

INT1
nmhGetFirstIndexFwlDefnIPv6DmzTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlDefnIPv6DmzTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlDmzAddressType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFwlDmzIpv6RowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlDmzAddressType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFwlDmzIpv6RowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlDmzAddressType ARG_LIST((UINT4 *  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FwlDmzIpv6RowStatus ARG_LIST((UINT4 *  , tSNMP_OCTET_STRING_TYPE * ,INT4));

/* Low Level DEP Routines for.  */
INT1
nmhDepv2FwlDosAttackAcceptRedirect ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlDosAttackAcceptSmurfAttack ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlDosLandAttack ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FwlDosShortHeaderAttack ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



INT1
nmhDepv2FwlDefnIPv6DmzTable ARG_LIST((UINT4 *, tSnmpIndexList*,
                                      tSNMP_VAR_BIND*));




/* Proto Validate Index Instance for FwlRateLimitTable. */
INT1
nmhValidateIndexInstanceFwlRateLimitTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlRateLimitTable  */

INT1
nmhGetFirstIndexFwlRateLimitTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlRateLimitTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlRateLimitPortNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlRateLimitPortType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlRateLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlRateLimitBurstSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlRateLimitRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlRateLimitPortNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlRateLimitPortType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlRateLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlRateLimitBurstSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlRateLimitRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhTestv2FwlRateLimitPortNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlRateLimitPortType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlRateLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlRateLimitBurstSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlRateLimitRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhGetFwlRateLimitTrafficMode ARG_LIST((INT4 ,INT4 *));


INT1
nmhSetFwlRateLimitTrafficMode ARG_LIST((INT4  ,INT4 ));


INT1
nmhTestv2FwlRateLimitTrafficMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlRateLimitTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
                                                                                          

/* Proto Validate Index Instance for FwlSnorkTable. */
INT1
nmhValidateIndexInstanceFwlSnorkTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlSnorkTable  */

INT1
nmhGetFirstIndexFwlSnorkTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlSnorkTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlSnorkRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlSnorkRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlSnorkRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlSnorkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FwlRpfTable. */
INT1
nmhValidateIndexInstanceFwlRpfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlRpfTable  */

INT1
nmhGetFirstIndexFwlRpfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFwlRpfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFwlRpfMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFwlRpfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFwlRpfMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFwlRpfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FwlRpfMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FwlRpfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FwlRpfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));




