/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlstat.h,v 1.3 2011/05/30 14:50:36 siva Exp $
 *
 * Description:This file contains all constants related        
 *             to implemetation of stateful table
 *
 *******************************************************************/
#ifndef _FWLSTAT_H
#define _FWLSTAT_H  1

/* Stateful Table Bucket Size */
#define FWL_STATE_HASH_LIMIT        6000
#define FWL_STATE_V6_HASH_LIMIT        6000

#define FWL_STATE_HASH_BIT_MASK (FWL_STATE_HASH_LIMIT - 1)

/* Max number of nodes stored in TCP init flow table */ 
#define FWL_MAX_TCP_INIT_FLOW_NODES  (FWL_STATE_HASH_LIMIT * 80/100) /* 80% */

/* Max number of total sessions (outbound + inbound) stored in stateful table */
#define FWL_MAX_STATEFUL_SESSIONS_LIMIT      FWL_STATE_HASH_LIMIT
         
/* Max number of nodes stored in Partial-Links table */ 
#define FWL_MAX_PARTIAL_LINKS_NODES    (FWL_STATE_HASH_LIMIT * 50/100) /* 50% */ 

/* Max number of inbound sessions that can be stored in stateful table */
#define FWL_MAX_IN_STATEFUL_SESSIONS_LIMIT   (FWL_STATE_HASH_LIMIT * 25/100)
         
#define FWL_MAX_OUT_USER_SESSIONS_LIMIT      (FWL_STATE_HASH_LIMIT * 10/100) 
                                             /* Max simultaneous outbound 
                                                sessions allowed for a 
                                                user */
                                                    
                                                    
#define FWL_MAX_IN_USER_SESSIONS_LIMIT       (FWL_STATE_HASH_LIMIT * 10/100)
                                             /* Max simultaneous inbound
                                                sessions allowed to a 
                                                DMZ Host */

#define FWL_MAX_IN_UDP_SESSIONS_LIMIT      (FWL_STATE_HASH_LIMIT * 20/100)
#define FWL_MAX_IN_ICMP_SESSIONS_LIMIT     (FWL_STATE_HASH_LIMIT * 20/100)

/* Default values for Timeout Intervals */
#define FWL_DEFAULT_TCP_INIT_FLOW_TIMEOUT    20  /* for 3-way handshake */
#define FWL_DEFAULT_TCP_EST_FLOW_TIMEOUT   3600  /* for established */
#define FWL_DEFAULT_TCP_FIN_FLOW_TIMEOUT     60  /* for 4-way handshake */
#define FWL_DEFAULT_UDP_FLOW_LARGE_TIMEOUT   60  /* udp lifetime */
#define FWL_DEFAULT_ICMP_FLOW_TIMEOUT        20  /* icmp lifetime */

/* Macros for Timeout Intervals */
#define FWL_TCP_INIT_FLOW_TIMEOUT     10 /* 10 secs for 3-Way Handshake */ 
#define FWL_TCP_EST_FLOW_TIMEOUT      3600 /* 1 Hour */
#define FWL_TCP_FIN_FLOW_TIMEOUT      60  /* 1 Min */

#define FWL_ICMP_FLOW_TIMEOUT         5
#define FWL_UDP_FLOW_TINY_TIMEOUT     300
#define FWL_UDP_FLOW_LARGE_TIMEOUT    300
#define FWL_PERSIST_PARTIAL_LINK_TIMEOUT       3600 /* One hour */
#define FWL_NON_PERSIST_PARTIAL_LINK_TIMEOUT   30 /* half minute */  
#define FWL_COMMIT                    1
#define FWL_DONT_COMMIT               2

#define FWL_FLUSH_STATEFUL_ENTRIES    1
#define FWL_DONT_FLUSH                2 

#endif /* _FWLSTAT_H */

