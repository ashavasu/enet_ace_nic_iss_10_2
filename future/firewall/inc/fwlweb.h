/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlweb.h,v 1.1 2010/11/10 15:00:01 prabuc Exp $
 *
 * Description: This file contains prototypes for Firwall routines web 
 *              interface.
 *
 *******************************************************************/

#ifndef __FWLWEB_H__
#define __FWLWEB_H__

VOID IssPrintIpInterfaces (tHttp *pHttp, UINT4, UINT1, UINT1);
VOID IssProcessFwlAclPageGet(tHttp *pHttp);
VOID IssProcessFwlAclPageSet(tHttp *pHttp);
VOID IssProcessFwlIntfPageGet (tHttp * pHttp);
VOID IssProcessFwlIntfPageSet (tHttp * pHttp);

#endif/*__FWLWEB_H__*/
