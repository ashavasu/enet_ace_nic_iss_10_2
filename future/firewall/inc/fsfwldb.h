/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfwldb.h,v 1.14 2016/02/27 10:04:59 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSFWLDB_H
#define _FSFWLDB_H

UINT1 FwlDefnFilterTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FwlDefnRuleTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FwlDefnAclTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FwlDefnIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FwlDefnDmzTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FwlUrlFilterTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FwlStateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FwlStatIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FwlDefnBlkListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FwlDefnWhiteListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FwlDefnIPv6DmzTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FwlRateLimitTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FwlSnorkTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FwlRpfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsfwl [] ={1,3,6,1,4,1,2076,16};
tSNMP_OID_TYPE fsfwlOID = {8, fsfwl};


UINT4 FwlGlobalMasterControlSwitch [ ] ={1,3,6,1,4,1,2076,16,1,1};
UINT4 FwlGlobalICMPControlSwitch [ ] ={1,3,6,1,4,1,2076,16,1,2};
UINT4 FwlGlobalIpSpoofFiltering [ ] ={1,3,6,1,4,1,2076,16,1,3};
UINT4 FwlGlobalSrcRouteFiltering [ ] ={1,3,6,1,4,1,2076,16,1,4};
UINT4 FwlGlobalTinyFragmentFiltering [ ] ={1,3,6,1,4,1,2076,16,1,5};
UINT4 FwlGlobalTcpIntercept [ ] ={1,3,6,1,4,1,2076,16,1,6};
UINT4 FwlGlobalTrap [ ] ={1,3,6,1,4,1,2076,16,1,7};
UINT4 FwlGlobalTrace [ ] ={1,3,6,1,4,1,2076,16,1,8};
UINT4 FwlGlobalDebug [ ] ={1,3,6,1,4,1,2076,16,1,9};
UINT4 FwlGlobalMaxFilters [ ] ={1,3,6,1,4,1,2076,16,1,10};
UINT4 FwlGlobalMaxRules [ ] ={1,3,6,1,4,1,2076,16,1,11};
UINT4 FwlGlobalUrlFiltering [ ] ={1,3,6,1,4,1,2076,16,1,12};
UINT4 FwlGlobalNetBiosFiltering [ ] ={1,3,6,1,4,1,2076,16,1,13};
UINT4 FwlGlobalNetBiosLan2Wan [ ] ={1,3,6,1,4,1,2076,16,1,14};
UINT4 FwlGlobalICMPv6ControlSwitch [ ] ={1,3,6,1,4,1,2076,16,1,15};
UINT4 FwlGlobalIpv6SpoofFiltering [ ] ={1,3,6,1,4,1,2076,16,1,16};
UINT4 FwlGlobalLogFileSize [ ] ={1,3,6,1,4,1,2076,16,1,17};
UINT4 FwlGlobalLogSizeThreshold [ ] ={1,3,6,1,4,1,2076,16,1,18};
UINT4 FwlGlobalIdsLogSize [ ] ={1,3,6,1,4,1,2076,16,1,19};
UINT4 FwlGlobalIdsLogThreshold [ ] ={1,3,6,1,4,1,2076,16,1,20};
UINT4 FwlGlobalIdsVersionInfo [ ] ={1,3,6,1,4,1,2076,16,1,21};
UINT4 FwlGlobalReloadIds [ ] ={1,3,6,1,4,1,2076,16,1,22};
UINT4 FwlGlobalIdsStatus [ ] ={1,3,6,1,4,1,2076,16,1,23};
UINT4 FwlGlobalLoadIdsRules [ ] ={1,3,6,1,4,1,2076,16,1,24};
UINT4 FwlDosAttackAcceptRedirect [ ] ={1,3,6,1,4,1,2076,16,1,25};
UINT4 FwlDosAttackAcceptSmurfAttack [ ] ={1,3,6,1,4,1,2076,16,1,26};
UINT4 FwlDosLandAttack [ ] ={1,3,6,1,4,1,2076,16,1,27};
UINT4 FwlDosShortHeaderAttack [ ] ={1,3,6,1,4,1,2076,16,1,28};
UINT4 FwlDefnTcpInterceptThreshold [ ] ={1,3,6,1,4,1,2076,16,2,1};
UINT4 FwlDefnInterceptTimeout [ ] ={1,3,6,1,4,1,2076,16,2,2};
UINT4 FwlFilterFilterName [ ] ={1,3,6,1,4,1,2076,16,2,3,1,1};
UINT4 FwlFilterSrcAddress [ ] ={1,3,6,1,4,1,2076,16,2,3,1,2};
UINT4 FwlFilterDestAddress [ ] ={1,3,6,1,4,1,2076,16,2,3,1,3};
UINT4 FwlFilterProtocol [ ] ={1,3,6,1,4,1,2076,16,2,3,1,4};
UINT4 FwlFilterSrcPort [ ] ={1,3,6,1,4,1,2076,16,2,3,1,5};
UINT4 FwlFilterDestPort [ ] ={1,3,6,1,4,1,2076,16,2,3,1,6};
UINT4 FwlFilterAckBit [ ] ={1,3,6,1,4,1,2076,16,2,3,1,7};
UINT4 FwlFilterRstBit [ ] ={1,3,6,1,4,1,2076,16,2,3,1,8};
UINT4 FwlFilterTos [ ] ={1,3,6,1,4,1,2076,16,2,3,1,9};
UINT4 FwlFilterAccounting [ ] ={1,3,6,1,4,1,2076,16,2,3,1,10};
UINT4 FwlFilterHitClear [ ] ={1,3,6,1,4,1,2076,16,2,3,1,11};
UINT4 FwlFilterHitsCount [ ] ={1,3,6,1,4,1,2076,16,2,3,1,12};
UINT4 FwlFilterAddrType [ ] ={1,3,6,1,4,1,2076,16,2,3,1,13};
UINT4 FwlFilterFlowId [ ] ={1,3,6,1,4,1,2076,16,2,3,1,14};
UINT4 FwlFilterDscp [ ] ={1,3,6,1,4,1,2076,16,2,3,1,15};
UINT4 FwlFilterRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,3,1,16};
UINT4 FwlRuleRuleName [ ] ={1,3,6,1,4,1,2076,16,2,4,1,1};
UINT4 FwlRuleFilterSet [ ] ={1,3,6,1,4,1,2076,16,2,4,1,2};
UINT4 FwlRuleRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,4,1,3};
UINT4 FwlAclIfIndex [ ] ={1,3,6,1,4,1,2076,16,2,5,1,1};
UINT4 FwlAclAclName [ ] ={1,3,6,1,4,1,2076,16,2,5,1,2};
UINT4 FwlAclDirection [ ] ={1,3,6,1,4,1,2076,16,2,5,1,3};
UINT4 FwlAclAction [ ] ={1,3,6,1,4,1,2076,16,2,5,1,4};
UINT4 FwlAclSequenceNumber [ ] ={1,3,6,1,4,1,2076,16,2,5,1,5};
UINT4 FwlAclAclType [ ] ={1,3,6,1,4,1,2076,16,2,5,1,6};
UINT4 FwlAclLogTrigger [ ] ={1,3,6,1,4,1,2076,16,2,5,1,7};
UINT4 FwlAclFragAction [ ] ={1,3,6,1,4,1,2076,16,2,5,1,8};
UINT4 FwlAclRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,5,1,9};
UINT4 FwlIfIfIndex [ ] ={1,3,6,1,4,1,2076,16,2,6,1,1};
UINT4 FwlIfIfType [ ] ={1,3,6,1,4,1,2076,16,2,6,1,2};
UINT4 FwlIfIpOptions [ ] ={1,3,6,1,4,1,2076,16,2,6,1,3};
UINT4 FwlIfFragments [ ] ={1,3,6,1,4,1,2076,16,2,6,1,4};
UINT4 FwlIfFragmentSize [ ] ={1,3,6,1,4,1,2076,16,2,6,1,5};
UINT4 FwlIfICMPType [ ] ={1,3,6,1,4,1,2076,16,2,6,1,6};
UINT4 FwlIfICMPCode [ ] ={1,3,6,1,4,1,2076,16,2,6,1,7};
UINT4 FwlIfICMPv6MsgType [ ] ={1,3,6,1,4,1,2076,16,2,6,1,8};
UINT4 FwlIfRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,6,1,9};
UINT4 FwlDmzIpIndex [ ] ={1,3,6,1,4,1,2076,16,2,7,1,1};
UINT4 FwlDmzRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,7,1,2};
UINT4 FwlUrlString [ ] ={1,3,6,1,4,1,2076,16,2,8,1,1};
UINT4 FwlUrlHitCount [ ] ={1,3,6,1,4,1,2076,16,2,8,1,2};
UINT4 FwlUrlFilterRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,8,1,3};
UINT4 FwlStatInspectedPacketsCount [ ] ={1,3,6,1,4,1,2076,16,3,1};
UINT4 FwlStatTotalPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,2};
UINT4 FwlStatTotalPacketsAccepted [ ] ={1,3,6,1,4,1,2076,16,3,3};
UINT4 FwlStatTotalIcmpPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,4};
UINT4 FwlStatTotalSynPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,5};
UINT4 FwlStatTotalIpSpoofedPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,6};
UINT4 FwlStatTotalSrcRoutePacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,7};
UINT4 FwlStatTotalTinyFragmentPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,8};
UINT4 FwlStatTotalFragmentedPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,9};
UINT4 FwlStatTotalLargeFragmentPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,10};
UINT4 FwlStatTotalIpOptionPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,11};
UINT4 FwlStatTotalAttacksPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,12};
UINT4 FwlStatMemoryAllocationFailCount [ ] ={1,3,6,1,4,1,2076,16,3,13};
UINT4 FwlStatIPv6InspectedPacketsCount [ ] ={1,3,6,1,4,1,2076,16,3,14};
UINT4 FwlStatIPv6TotalPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,15};
UINT4 FwlStatIPv6TotalPacketsAccepted [ ] ={1,3,6,1,4,1,2076,16,3,16};
UINT4 FwlStatIPv6TotalIcmpPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,17};
UINT4 FwlStatIPv6TotalSpoofedPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,18};
UINT4 FwlStatIPv6TotalAttacksPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,19};
UINT4 FwlStateType [ ] ={1,3,6,1,4,1,2076,16,5,1,1,1};
UINT4 FwlStateLocalIpAddrType [ ] ={1,3,6,1,4,1,2076,16,5,1,1,2};
UINT4 FwlStateLocalIpAddress [ ] ={1,3,6,1,4,1,2076,16,5,1,1,3};
UINT4 FwlStateRemoteIpAddrType [ ] ={1,3,6,1,4,1,2076,16,5,1,1,4};
UINT4 FwlStateRemoteIpAddress [ ] ={1,3,6,1,4,1,2076,16,5,1,1,5};
UINT4 FwlStateLocalPort [ ] ={1,3,6,1,4,1,2076,16,5,1,1,6};
UINT4 FwlStateRemotePort [ ] ={1,3,6,1,4,1,2076,16,5,1,1,7};
UINT4 FwlStateProtocol [ ] ={1,3,6,1,4,1,2076,16,5,1,1,8};
UINT4 FwlStateDirection [ ] ={1,3,6,1,4,1,2076,16,5,1,1,9};
UINT4 FwlStateEstablishedTime [ ] ={1,3,6,1,4,1,2076,16,5,1,1,10};
UINT4 FwlStateLocalState [ ] ={1,3,6,1,4,1,2076,16,5,1,1,11};
UINT4 FwlStateRemoteState [ ] ={1,3,6,1,4,1,2076,16,5,1,1,12};
UINT4 FwlStateLogLevel [ ] ={1,3,6,1,4,1,2076,16,5,1,1,13};
UINT4 FwlStateCallStatus [ ] ={1,3,6,1,4,1,2076,16,5,1,1,14};
UINT4 FwlStatIfIfIndex [ ] ={1,3,6,1,4,1,2076,16,3,20,1,1};
UINT4 FwlStatIfFilterCount [ ] ={1,3,6,1,4,1,2076,16,3,20,1,2};
UINT4 FwlStatIfPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,3};
UINT4 FwlStatIfPacketsAccepted [ ] ={1,3,6,1,4,1,2076,16,3,20,1,4};
UINT4 FwlStatIfSynPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,5};
UINT4 FwlStatIfIcmpPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,6};
UINT4 FwlStatIfIpSpoofedPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,7};
UINT4 FwlStatIfSrcRoutePacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,8};
UINT4 FwlStatIfTinyFragmentPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,9};
UINT4 FwlStatIfFragmentPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,10};
UINT4 FwlStatIfIpOptionPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,11};
UINT4 FwlStatIfClear [ ] ={1,3,6,1,4,1,2076,16,3,20,1,12};
UINT4 FwlIfTrapThreshold [ ] ={1,3,6,1,4,1,2076,16,3,20,1,13};
UINT4 FwlStatIfIPv6PacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,14};
UINT4 FwlStatIfIPv6PacketsAccepted [ ] ={1,3,6,1,4,1,2076,16,3,20,1,15};
UINT4 FwlStatIfIPv6IcmpPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,16};
UINT4 FwlStatIfIPv6SpoofedPacketsDenied [ ] ={1,3,6,1,4,1,2076,16,3,20,1,17};
UINT4 FwlStatIfClearIPv6 [ ] ={1,3,6,1,4,1,2076,16,3,20,1,18};
UINT4 FwlStatClear [ ] ={1,3,6,1,4,1,2076,16,3,21};
UINT4 FwlStatClearIPv6 [ ] ={1,3,6,1,4,1,2076,16,3,22};
UINT4 FwlTrapThreshold [ ] ={1,3,6,1,4,1,2076,16,3,23};
UINT4 FwlTrapMemFailMessage [ ] ={1,3,6,1,4,1,2076,16,4,1,1};
UINT4 FwlTrapAttackMessage [ ] ={1,3,6,1,4,1,2076,16,4,1,2};
UINT4 FwlIfIndex [ ] ={1,3,6,1,4,1,2076,16,4,1,3};
UINT4 FwlTrapEvent [ ] ={1,3,6,1,4,1,2076,16,4,1,4};
UINT4 FwlTrapEventTime [ ] ={1,3,6,1,4,1,2076,16,4,1,5};
UINT4 FwlTrapFileName [ ] ={1,3,6,1,4,1,2076,16,4,1,6};
UINT4 FwlIdsTrapEvent [ ] ={1,3,6,1,4,1,2076,16,4,1,7};
UINT4 FwlIdsTrapEventTime [ ] ={1,3,6,1,4,1,2076,16,4,1,8};
UINT4 FwlIdsTrapFileName [ ] ={1,3,6,1,4,1,2076,16,4,1,9};
UINT4 FwlIdsAttackPktIp [ ] ={1,3,6,1,4,1,2076,16,4,1,10};
UINT4 FwlBlkListIpAddressType [ ] ={1,3,6,1,4,1,2076,16,2,9,1,1};
UINT4 FwlBlkListIpAddress [ ] ={1,3,6,1,4,1,2076,16,2,9,1,2};
UINT4 FwlBlkListIpMask [ ] ={1,3,6,1,4,1,2076,16,2,9,1,3};
UINT4 FwlBlkListHitsCount [ ] ={1,3,6,1,4,1,2076,16,2,9,1,4};
UINT4 FwlBlkListEntryType [ ] ={1,3,6,1,4,1,2076,16,2,9,1,5};
UINT4 FwlBlkListRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,9,1,6};
UINT4 FwlWhiteListIpAddressType [ ] ={1,3,6,1,4,1,2076,16,2,10,1,1};
UINT4 FwlWhiteListIpAddress [ ] ={1,3,6,1,4,1,2076,16,2,10,1,2};
UINT4 FwlWhiteListIpMask [ ] ={1,3,6,1,4,1,2076,16,2,10,1,3};
UINT4 FwlWhiteListHitsCount [ ] ={1,3,6,1,4,1,2076,16,2,10,1,4};
UINT4 FwlWhiteListRowStatus [ ] ={1,3,6,1,4,1,2076,16,2,10,1,5};
UINT4 FwlDmzAddressType [ ] ={1,3,6,1,4,1,2076,16,2,11,1,1};
UINT4 FwlDmzIpv6Index [ ] ={1,3,6,1,4,1,2076,16,2,11,1,2};
UINT4 FwlDmzIpv6RowStatus [ ] ={1,3,6,1,4,1,2076,16,2,11,1,3};
UINT4 FwlRateLimitPortIndex [ ] ={1,3,6,1,4,1,2076,16,6,1,1,1};
UINT4 FwlRateLimitPortNumber [ ] ={1,3,6,1,4,1,2076,16,6,1,1,2};
UINT4 FwlRateLimitPortType [ ] ={1,3,6,1,4,1,2076,16,6,1,1,3};
UINT4 FwlRateLimitValue [ ] ={1,3,6,1,4,1,2076,16,6,1,1,4};
UINT4 FwlRateLimitBurstSize [ ] ={1,3,6,1,4,1,2076,16,6,1,1,5};
UINT4 FwlRateLimitTrafficMode [ ] ={1,3,6,1,4,1,2076,16,6,1,1,6};
UINT4 FwlRateLimitRowStatus [ ] ={1,3,6,1,4,1,2076,16,6,1,1,7};
UINT4 FwlSnorkPortNo [ ] ={1,3,6,1,4,1,2076,16,7,1,1,1};
UINT4 FwlSnorkRowStatus [ ] ={1,3,6,1,4,1,2076,16,7,1,1,2};
UINT4 FwlRpfInIndex [ ] ={1,3,6,1,4,1,2076,16,8,1,1,1};
UINT4 FwlRpfMode [ ] ={1,3,6,1,4,1,2076,16,8,1,1,2};
UINT4 FwlRpfRowStatus [ ] ={1,3,6,1,4,1,2076,16,8,1,1,3};







tMbDbEntry fsfwlMibEntry[]= {

{{10,FwlGlobalMasterControlSwitch}, NULL, FwlGlobalMasterControlSwitchGet, FwlGlobalMasterControlSwitchSet, FwlGlobalMasterControlSwitchTest, FwlGlobalMasterControlSwitchDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FwlGlobalICMPControlSwitch}, NULL, FwlGlobalICMPControlSwitchGet, FwlGlobalICMPControlSwitchSet, FwlGlobalICMPControlSwitchTest, FwlGlobalICMPControlSwitchDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalIpSpoofFiltering}, NULL, FwlGlobalIpSpoofFilteringGet, FwlGlobalIpSpoofFilteringSet, FwlGlobalIpSpoofFilteringTest, FwlGlobalIpSpoofFilteringDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FwlGlobalSrcRouteFiltering}, NULL, FwlGlobalSrcRouteFilteringGet, FwlGlobalSrcRouteFilteringSet, FwlGlobalSrcRouteFilteringTest, FwlGlobalSrcRouteFilteringDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FwlGlobalTinyFragmentFiltering}, NULL, FwlGlobalTinyFragmentFilteringGet, FwlGlobalTinyFragmentFilteringSet, FwlGlobalTinyFragmentFilteringTest, FwlGlobalTinyFragmentFilteringDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "1"},

{{10,FwlGlobalTcpIntercept}, NULL, FwlGlobalTcpInterceptGet, FwlGlobalTcpInterceptSet, FwlGlobalTcpInterceptTest, FwlGlobalTcpInterceptDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FwlGlobalTrap}, NULL, FwlGlobalTrapGet, FwlGlobalTrapSet, FwlGlobalTrapTest, FwlGlobalTrapDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalTrace}, NULL, FwlGlobalTraceGet, FwlGlobalTraceSet, FwlGlobalTraceTest, FwlGlobalTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FwlGlobalDebug}, NULL, FwlGlobalDebugGet, FwlGlobalDebugSet, FwlGlobalDebugTest, FwlGlobalDebugDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalMaxFilters}, NULL, FwlGlobalMaxFiltersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "100"},

{{10,FwlGlobalMaxRules}, NULL, FwlGlobalMaxRulesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "100"},

{{10,FwlGlobalUrlFiltering}, NULL, FwlGlobalUrlFilteringGet, FwlGlobalUrlFilteringSet, FwlGlobalUrlFilteringTest, FwlGlobalUrlFilteringDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalNetBiosFiltering}, NULL, FwlGlobalNetBiosFilteringGet, FwlGlobalNetBiosFilteringSet, FwlGlobalNetBiosFilteringTest, FwlGlobalNetBiosFilteringDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalNetBiosLan2Wan}, NULL, FwlGlobalNetBiosLan2WanGet, FwlGlobalNetBiosLan2WanSet, FwlGlobalNetBiosLan2WanTest, FwlGlobalNetBiosLan2WanDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalICMPv6ControlSwitch}, NULL, FwlGlobalICMPv6ControlSwitchGet, FwlGlobalICMPv6ControlSwitchSet, FwlGlobalICMPv6ControlSwitchTest, FwlGlobalICMPv6ControlSwitchDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlGlobalIpv6SpoofFiltering}, NULL, FwlGlobalIpv6SpoofFilteringGet, FwlGlobalIpv6SpoofFilteringSet, FwlGlobalIpv6SpoofFilteringTest, FwlGlobalIpv6SpoofFilteringDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FwlGlobalLogFileSize}, NULL, FwlGlobalLogFileSizeGet, FwlGlobalLogFileSizeSet, FwlGlobalLogFileSizeTest, FwlGlobalLogFileSizeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1048576"},
                        
{{10,FwlGlobalLogSizeThreshold}, NULL, FwlGlobalLogSizeThresholdGet, FwlGlobalLogSizeThresholdSet, FwlGlobalLogSizeThresholdTest, FwlGlobalLogSizeThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "70"},
    
{{10,FwlGlobalIdsLogSize}, NULL, FwlGlobalIdsLogSizeGet, FwlGlobalIdsLogSizeSet, FwlGlobalIdsLogSizeTest, FwlGlobalIdsLogSizeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1048576"},
                             
{{10,FwlGlobalIdsLogThreshold}, NULL, FwlGlobalIdsLogThresholdGet, FwlGlobalIdsLogThresholdSet, FwlGlobalIdsLogThresholdTest, FwlGlobalIdsLogThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "70"},

{{10,FwlGlobalIdsVersionInfo}, NULL, FwlGlobalIdsVersionInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, ""},

{{10,FwlGlobalReloadIds}, NULL, FwlGlobalReloadIdsGet, FwlGlobalReloadIdsSet, FwlGlobalReloadIdsTest, FwlGlobalReloadIdsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FwlGlobalIdsStatus}, NULL, FwlGlobalIdsStatusGet, FwlGlobalIdsStatusSet, FwlGlobalIdsStatusTest, FwlGlobalIdsStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FwlGlobalLoadIdsRules}, NULL, FwlGlobalLoadIdsRulesGet, FwlGlobalLoadIdsRulesSet, FwlGlobalLoadIdsRulesTest, FwlGlobalLoadIdsRulesDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FwlDosAttackAcceptRedirect}, NULL, FwlDosAttackAcceptRedirectGet, FwlDosAttackAcceptRedirectSet, FwlDosAttackAcceptRedirectTest, FwlDosAttackAcceptRedirectDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FwlDosAttackAcceptSmurfAttack}, NULL, FwlDosAttackAcceptSmurfAttackGet, FwlDosAttackAcceptSmurfAttackSet, FwlDosAttackAcceptSmurfAttackTest, FwlDosAttackAcceptSmurfAttackDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FwlDosLandAttack}, NULL, FwlDosLandAttackGet, FwlDosLandAttackSet, FwlDosLandAttackTest, FwlDosLandAttackDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FwlDosShortHeaderAttack}, NULL, FwlDosShortHeaderAttackGet, FwlDosShortHeaderAttackSet, FwlDosShortHeaderAttackTest, FwlDosShortHeaderAttackDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},


{{10,FwlDefnTcpInterceptThreshold}, NULL, FwlDefnTcpInterceptThresholdGet, FwlDefnTcpInterceptThresholdSet, FwlDefnTcpInterceptThresholdTest, FwlDefnTcpInterceptThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "50"},

{{10,FwlDefnInterceptTimeout}, NULL, FwlDefnInterceptTimeoutGet, FwlDefnInterceptTimeoutSet, FwlDefnInterceptTimeoutTest, FwlDefnInterceptTimeoutDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FwlFilterFilterName}, GetNextIndexFwlDefnFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlDefnFilterTableINDEX, 1, 0, 0, NULL},

{{12,FwlFilterSrcAddress}, GetNextIndexFwlDefnFilterTable, FwlFilterSrcAddressGet, FwlFilterSrcAddressSet, FwlFilterSrcAddressTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterDestAddress}, GetNextIndexFwlDefnFilterTable, FwlFilterDestAddressGet, FwlFilterDestAddressSet, FwlFilterDestAddressTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterProtocol}, GetNextIndexFwlDefnFilterTable, FwlFilterProtocolGet, FwlFilterProtocolSet, FwlFilterProtocolTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "255"},

{{12,FwlFilterSrcPort}, GetNextIndexFwlDefnFilterTable, FwlFilterSrcPortGet, FwlFilterSrcPortSet, FwlFilterSrcPortTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterDestPort}, GetNextIndexFwlDefnFilterTable, FwlFilterDestPortGet, FwlFilterDestPortSet, FwlFilterDestPortTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterAckBit}, GetNextIndexFwlDefnFilterTable, FwlFilterAckBitGet, FwlFilterAckBitSet, FwlFilterAckBitTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 1, 0, "3"},

{{12,FwlFilterRstBit}, GetNextIndexFwlDefnFilterTable, FwlFilterRstBitGet, FwlFilterRstBitSet, FwlFilterRstBitTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 1, 0, "3"},

{{12,FwlFilterTos}, GetNextIndexFwlDefnFilterTable, FwlFilterTosGet, FwlFilterTosSet, FwlFilterTosTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterAccounting}, GetNextIndexFwlDefnFilterTable, FwlFilterAccountingGet, FwlFilterAccountingSet, FwlFilterAccountingTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "2"},

{{12,FwlFilterHitClear}, GetNextIndexFwlDefnFilterTable, FwlFilterHitClearGet, FwlFilterHitClearSet, FwlFilterHitClearTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "2"},

{{12,FwlFilterHitsCount}, GetNextIndexFwlDefnFilterTable, FwlFilterHitsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlDefnFilterTableINDEX, 1, 0, 0, NULL},

{{12,FwlFilterAddrType}, GetNextIndexFwlDefnFilterTable, FwlFilterAddrTypeGet, FwlFilterAddrTypeSet, FwlFilterAddrTypeTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, NULL},

{{12,FwlFilterFlowId}, GetNextIndexFwlDefnFilterTable, FwlFilterFlowIdGet, FwlFilterFlowIdSet, FwlFilterFlowIdTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterDscp}, GetNextIndexFwlDefnFilterTable, FwlFilterDscpGet, FwlFilterDscpSet, FwlFilterDscpTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 0, "0"},

{{12,FwlFilterRowStatus}, GetNextIndexFwlDefnFilterTable, FwlFilterRowStatusGet, FwlFilterRowStatusSet, FwlFilterRowStatusTest, FwlDefnFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnFilterTableINDEX, 1, 0, 1, NULL},

{{12,FwlRuleRuleName}, GetNextIndexFwlDefnRuleTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlDefnRuleTableINDEX, 1, 0, 0, NULL},

{{12,FwlRuleFilterSet}, GetNextIndexFwlDefnRuleTable, FwlRuleFilterSetGet, FwlRuleFilterSetSet, FwlRuleFilterSetTest, FwlDefnRuleTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FwlDefnRuleTableINDEX, 1, 0, 0, NULL},

{{12,FwlRuleRowStatus}, GetNextIndexFwlDefnRuleTable, FwlRuleRowStatusGet, FwlRuleRowStatusSet, FwlRuleRowStatusTest, FwlDefnRuleTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnRuleTableINDEX, 1, 0, 1, NULL},

{{12,FwlAclIfIndex}, GetNextIndexFwlDefnAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlDefnAclTableINDEX, 3, 0, 0, NULL},

{{12,FwlAclAclName}, GetNextIndexFwlDefnAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlDefnAclTableINDEX, 3, 0, 0, NULL},

{{12,FwlAclDirection}, GetNextIndexFwlDefnAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlDefnAclTableINDEX, 3, 0, 0, NULL},

{{12,FwlAclAction}, GetNextIndexFwlDefnAclTable, FwlAclActionGet, FwlAclActionSet, FwlAclActionTest, FwlDefnAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnAclTableINDEX, 3, 0, 0, NULL},

{{12,FwlAclSequenceNumber}, GetNextIndexFwlDefnAclTable, FwlAclSequenceNumberGet, FwlAclSequenceNumberSet, FwlAclSequenceNumberTest, FwlDefnAclTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlDefnAclTableINDEX, 3, 0, 0, NULL},

{{12,FwlAclAclType}, GetNextIndexFwlDefnAclTable, FwlAclAclTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FwlDefnAclTableINDEX, 3, 1, 0, "2"},

{{12,FwlAclLogTrigger}, GetNextIndexFwlDefnAclTable, FwlAclLogTriggerGet, FwlAclLogTriggerSet, FwlAclLogTriggerTest, FwlDefnAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnAclTableINDEX, 3, 0, 0, "1"},

{{12,FwlAclFragAction}, GetNextIndexFwlDefnAclTable, FwlAclFragActionGet, FwlAclFragActionSet, FwlAclFragActionTest, FwlDefnAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnAclTableINDEX, 3, 0, 0, NULL},

{{12,FwlAclRowStatus}, GetNextIndexFwlDefnAclTable, FwlAclRowStatusGet, FwlAclRowStatusSet, FwlAclRowStatusTest, FwlDefnAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnAclTableINDEX, 3, 0, 1, NULL},

{{12,FwlIfIfIndex}, GetNextIndexFwlDefnIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlDefnIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlIfIfType}, GetNextIndexFwlDefnIfTable, FwlIfIfTypeGet, FwlIfIfTypeSet, FwlIfIfTypeTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 0, "2"},

{{12,FwlIfIpOptions}, GetNextIndexFwlDefnIfTable, FwlIfIpOptionsGet, FwlIfIpOptionsSet, FwlIfIpOptionsTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 0, "4"},

{{12,FwlIfFragments}, GetNextIndexFwlDefnIfTable, FwlIfFragmentsGet, FwlIfFragmentsSet, FwlIfFragmentsTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 0, "3"},

{{12,FwlIfFragmentSize}, GetNextIndexFwlDefnIfTable, FwlIfFragmentSizeGet, FwlIfFragmentSizeSet, FwlIfFragmentSizeTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 0, "30000"},

{{12,FwlIfICMPType}, GetNextIndexFwlDefnIfTable, FwlIfICMPTypeGet, FwlIfICMPTypeSet, FwlIfICMPTypeTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 0, "255"},

{{12,FwlIfICMPCode}, GetNextIndexFwlDefnIfTable, FwlIfICMPCodeGet, FwlIfICMPCodeSet, FwlIfICMPCodeTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 1, 0, "255"},

{{12,FwlIfICMPv6MsgType}, GetNextIndexFwlDefnIfTable, FwlIfICMPv6MsgTypeGet, FwlIfICMPv6MsgTypeSet, FwlIfICMPv6MsgTypeTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 0, "0"},

{{12,FwlIfRowStatus}, GetNextIndexFwlDefnIfTable, FwlIfRowStatusGet, FwlIfRowStatusSet, FwlIfRowStatusTest, FwlDefnIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIfTableINDEX, 1, 0, 1, NULL},

{{12,FwlDmzIpIndex}, GetNextIndexFwlDefnDmzTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FwlDefnDmzTableINDEX, 1, 0, 0, NULL},

{{12,FwlDmzRowStatus}, GetNextIndexFwlDefnDmzTable, FwlDmzRowStatusGet, FwlDmzRowStatusSet, FwlDmzRowStatusTest, FwlDefnDmzTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnDmzTableINDEX, 1, 0, 1, NULL},

{{12,FwlUrlString}, GetNextIndexFwlUrlFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlUrlFilterTableINDEX, 1, 0, 0, NULL},

{{12,FwlUrlHitCount}, GetNextIndexFwlUrlFilterTable, FwlUrlHitCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlUrlFilterTableINDEX, 1, 0, 0, NULL},

{{12,FwlUrlFilterRowStatus}, GetNextIndexFwlUrlFilterTable, FwlUrlFilterRowStatusGet, FwlUrlFilterRowStatusSet, FwlUrlFilterRowStatusTest, FwlUrlFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlUrlFilterTableINDEX, 1, 0, 1, NULL},

{{12,FwlBlkListIpAddressType}, GetNextIndexFwlDefnBlkListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlDefnBlkListTableINDEX, 3, 0, 0, NULL},

{{12,FwlBlkListIpAddress}, GetNextIndexFwlDefnBlkListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlDefnBlkListTableINDEX, 3, 0, 0, NULL},

{{12,FwlBlkListIpMask}, GetNextIndexFwlDefnBlkListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FwlDefnBlkListTableINDEX, 3, 0, 0, NULL},

{{12,FwlBlkListHitsCount}, GetNextIndexFwlDefnBlkListTable, FwlBlkListHitsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlDefnBlkListTableINDEX, 3, 0, 0, NULL},

{{12,FwlBlkListEntryType}, GetNextIndexFwlDefnBlkListTable, FwlBlkListEntryTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FwlDefnBlkListTableINDEX, 3, 0, 0, NULL},

{{12,FwlBlkListRowStatus}, GetNextIndexFwlDefnBlkListTable, FwlBlkListRowStatusGet, FwlBlkListRowStatusSet, FwlBlkListRowStatusTest, FwlDefnBlkListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnBlkListTableINDEX, 3, 0, 1, NULL},

{{12,FwlWhiteListIpAddressType}, GetNextIndexFwlDefnWhiteListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlDefnWhiteListTableINDEX, 3, 0, 0, NULL},

{{12,FwlWhiteListIpAddress}, GetNextIndexFwlDefnWhiteListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlDefnWhiteListTableINDEX, 3, 0, 0, NULL},

{{12,FwlWhiteListIpMask}, GetNextIndexFwlDefnWhiteListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FwlDefnWhiteListTableINDEX, 3, 0, 0, NULL},

{{12,FwlWhiteListHitsCount}, GetNextIndexFwlDefnWhiteListTable, FwlWhiteListHitsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlDefnWhiteListTableINDEX, 3, 0, 0, NULL},

{{12,FwlWhiteListRowStatus}, GetNextIndexFwlDefnWhiteListTable, FwlWhiteListRowStatusGet, FwlWhiteListRowStatusSet, FwlWhiteListRowStatusTest, FwlDefnWhiteListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnWhiteListTableINDEX, 3, 0, 1, NULL},

{{12,FwlDmzAddressType}, GetNextIndexFwlDefnIPv6DmzTable, FwlDmzAddressTypeGet, FwlDmzAddressTypeSet, FwlDmzAddressTypeTest, FwlDefnIPv6DmzTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIPv6DmzTableINDEX, 1, 0, 0, NULL},

{{12,FwlDmzIpv6Index}, GetNextIndexFwlDefnIPv6DmzTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlDefnIPv6DmzTableINDEX, 1, 0, 0, NULL},

{{12,FwlDmzIpv6RowStatus}, GetNextIndexFwlDefnIPv6DmzTable, FwlDmzIpv6RowStatusGet, FwlDmzIpv6RowStatusSet, FwlDmzIpv6RowStatusTest, FwlDefnIPv6DmzTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlDefnIPv6DmzTableINDEX, 1, 0, 1, NULL},

{{10,FwlStatInspectedPacketsCount}, NULL, FwlStatInspectedPacketsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalPacketsDenied}, NULL, FwlStatTotalPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalPacketsAccepted}, NULL, FwlStatTotalPacketsAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalIcmpPacketsDenied}, NULL, FwlStatTotalIcmpPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalSynPacketsDenied}, NULL, FwlStatTotalSynPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalIpSpoofedPacketsDenied}, NULL, FwlStatTotalIpSpoofedPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalSrcRoutePacketsDenied}, NULL, FwlStatTotalSrcRoutePacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalTinyFragmentPacketsDenied}, NULL, FwlStatTotalTinyFragmentPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalFragmentedPacketsDenied}, NULL, FwlStatTotalFragmentedPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalLargeFragmentPacketsDenied}, NULL, FwlStatTotalLargeFragmentPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalIpOptionPacketsDenied}, NULL, FwlStatTotalIpOptionPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatTotalAttacksPacketsDenied}, NULL, FwlStatTotalAttacksPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatMemoryAllocationFailCount}, NULL, FwlStatMemoryAllocationFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatIPv6InspectedPacketsCount}, NULL, FwlStatIPv6InspectedPacketsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatIPv6TotalPacketsDenied}, NULL, FwlStatIPv6TotalPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatIPv6TotalPacketsAccepted}, NULL, FwlStatIPv6TotalPacketsAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatIPv6TotalIcmpPacketsDenied}, NULL, FwlStatIPv6TotalIcmpPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatIPv6TotalSpoofedPacketsDenied}, NULL, FwlStatIPv6TotalSpoofedPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FwlStatIPv6TotalAttacksPacketsDenied}, NULL, FwlStatIPv6TotalAttacksPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FwlStatIfIfIndex}, GetNextIndexFwlStatIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfFilterCount}, GetNextIndexFwlStatIfTable, FwlStatIfFilterCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfPacketsAccepted}, GetNextIndexFwlStatIfTable, FwlStatIfPacketsAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfSynPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfSynPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfIcmpPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfIcmpPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfIpSpoofedPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfIpSpoofedPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfSrcRoutePacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfSrcRoutePacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfTinyFragmentPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfTinyFragmentPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfFragmentPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfFragmentPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfIpOptionPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfIpOptionPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfClear}, GetNextIndexFwlStatIfTable, FwlStatIfClearGet, FwlStatIfClearSet, FwlStatIfClearTest, FwlStatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlStatIfTableINDEX, 1, 0, 0, "2"},

{{12,FwlIfTrapThreshold}, GetNextIndexFwlStatIfTable, FwlIfTrapThresholdGet, FwlIfTrapThresholdSet, FwlIfTrapThresholdTest, FwlStatIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlStatIfTableINDEX, 1, 0, 0, "50"},

{{12,FwlStatIfIPv6PacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfIPv6PacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfIPv6PacketsAccepted}, GetNextIndexFwlStatIfTable, FwlStatIfIPv6PacketsAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfIPv6IcmpPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfIPv6IcmpPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfIPv6SpoofedPacketsDenied}, GetNextIndexFwlStatIfTable, FwlStatIfIPv6SpoofedPacketsDeniedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FwlStatIfTableINDEX, 1, 0, 0, NULL},

{{12,FwlStatIfClearIPv6}, GetNextIndexFwlStatIfTable, FwlStatIfClearIPv6Get, FwlStatIfClearIPv6Set, FwlStatIfClearIPv6Test, FwlStatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlStatIfTableINDEX, 1, 0, 0, "2"},

{{10,FwlStatClear}, NULL, FwlStatClearGet, FwlStatClearSet, FwlStatClearTest, FwlStatClearDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlStatClearIPv6}, NULL, FwlStatClearIPv6Get, FwlStatClearIPv6Set, FwlStatClearIPv6Test, FwlStatClearIPv6Dep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FwlTrapThreshold}, NULL, FwlTrapThresholdGet, FwlTrapThresholdSet, FwlTrapThresholdTest, FwlTrapThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "50"},

{{11,FwlTrapMemFailMessage}, NULL, FwlTrapMemFailMessageGet, FwlTrapMemFailMessageSet, FwlTrapMemFailMessageTest, FwlTrapMemFailMessageDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FwlTrapAttackMessage}, NULL, FwlTrapAttackMessageGet, FwlTrapAttackMessageSet, FwlTrapAttackMessageTest, FwlTrapAttackMessageDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FwlIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FwlTrapEvent}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FwlTrapEventTime}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FwlTrapFileName}, NULL, FwlTrapFileNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FwlIdsTrapEvent}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FwlIdsTrapEventTime}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FwlIdsTrapFileName}, NULL, FwlIdsTrapFileNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FwlIdsAttackPktIp}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FwlStateType}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateLocalIpAddrType}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateLocalIpAddress}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateRemoteIpAddrType}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateRemoteIpAddress}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateLocalPort}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateRemotePort}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateProtocol}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateDirection}, GetNextIndexFwlStateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateEstablishedTime}, GetNextIndexFwlStateTable, FwlStateEstablishedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateLocalState}, GetNextIndexFwlStateTable, FwlStateLocalStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateRemoteState}, GetNextIndexFwlStateTable, FwlStateRemoteStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FwlStateTableINDEX, 9, 0, 0, NULL},

{{12,FwlStateLogLevel}, GetNextIndexFwlStateTable, FwlStateLogLevelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FwlStateTableINDEX, 9, 0, 0, "1"},

{{12,FwlStateCallStatus}, GetNextIndexFwlStateTable, FwlStateCallStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FwlStateTableINDEX, 9, 0, 0, "0"},

{{12,FwlRateLimitPortIndex}, GetNextIndexFwlRateLimitTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlRateLimitTableINDEX, 1, 0, 0, NULL},

{{12,FwlRateLimitPortNumber}, GetNextIndexFwlRateLimitTable, FwlRateLimitPortNumberGet, FwlRateLimitPortNumberSet, FwlRateLimitPortNumberTest, FwlRateLimitTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlRateLimitTableINDEX, 1, 0, 0, NULL},

{{12,FwlRateLimitPortType}, GetNextIndexFwlRateLimitTable, FwlRateLimitPortTypeGet, FwlRateLimitPortTypeSet, FwlRateLimitPortTypeTest, FwlRateLimitTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlRateLimitTableINDEX, 1, 0, 0, NULL},

{{12,FwlRateLimitValue}, GetNextIndexFwlRateLimitTable, FwlRateLimitValueGet, FwlRateLimitValueSet, FwlRateLimitValueTest, FwlRateLimitTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlRateLimitTableINDEX, 1, 0, 0, NULL},

{{12,FwlRateLimitBurstSize}, GetNextIndexFwlRateLimitTable, FwlRateLimitBurstSizeGet, FwlRateLimitBurstSizeSet, FwlRateLimitBurstSizeTest, FwlRateLimitTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FwlRateLimitTableINDEX, 1, 0, 0, NULL},

{{12,FwlRateLimitTrafficMode}, GetNextIndexFwlRateLimitTable, FwlRateLimitTrafficModeGet, FwlRateLimitTrafficModeSet, FwlRateLimitTrafficModeTest, FwlRateLimitTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlRateLimitTableINDEX, 1, 0, 0, NULL},

{{12,FwlRateLimitRowStatus}, GetNextIndexFwlRateLimitTable, FwlRateLimitRowStatusGet, FwlRateLimitRowStatusSet, FwlRateLimitRowStatusTest, FwlRateLimitTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlRateLimitTableINDEX, 1, 0, 1, NULL},

{{12,FwlSnorkPortNo}, GetNextIndexFwlSnorkTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlSnorkTableINDEX, 1, 0, 0, NULL},

{{12,FwlSnorkRowStatus}, GetNextIndexFwlSnorkTable, FwlSnorkRowStatusGet, FwlSnorkRowStatusSet, FwlSnorkRowStatusTest, FwlSnorkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlSnorkTableINDEX, 1, 0, 1, NULL},

{{12,FwlRpfInIndex}, GetNextIndexFwlRpfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FwlRpfTableINDEX, 1, 0, 0, NULL},

{{12,FwlRpfMode}, GetNextIndexFwlRpfTable, FwlRpfModeGet, FwlRpfModeSet, FwlRpfModeTest, FwlRpfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlRpfTableINDEX, 1, 0, 0, "0"},

{{12,FwlRpfRowStatus}, GetNextIndexFwlRpfTable, FwlRpfRowStatusGet, FwlRpfRowStatusSet, FwlRpfRowStatusTest, FwlRpfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FwlRpfTableINDEX, 1, 0, 1, NULL},

};
tMibData fsfwlEntry = { 162, fsfwlMibEntry };

#endif /* _FSFWLDB_H */

