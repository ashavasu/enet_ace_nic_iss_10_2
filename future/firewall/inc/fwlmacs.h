/********************************************************************
 * Copyright (C)  2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlmacs.h,v 1.10 2014/10/17 15:06:52 siva Exp $
 *
 * Description: This file contains all macro definitions
 *
 *******************************************************************/

/*****************************************************************************/
/*       Manipulation related  Macros                                        */
/*****************************************************************************/

#define  COUNTER_INC(var, value)  (var) = (COUNTER32) ((var) + (value)); 
#define  COUNTER_DEC(var, value)  (var) = (UINT2) ((var) - (value)); 
#define  COUNTER_DECR(var, value)  (var) = (GAUGE) ((var) - (value)); 

/*****************************************************************************/
/*       To check whethere the interface is external or internal.            */
/*****************************************************************************/

#define  IS_IFACE_INDEX_LOCAL(u4IfaceNum) \
         ((gFwlAclInfo.apIfaceList[u4IfaceNum] == NULL)?1:\
         (gFwlAclInfo.apIfaceList[u4IfaceNum]->u1IfaceType == FWL_INTERNAL_IF))

/*****************************************************************************/
/*       Macros for Interface specific Statistical Counters                  */
/*****************************************************************************/

#define  INC_IFACE_CONFIG_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4AclConfigCount, 1);
                
#define  DEC_IFACE_CONFIG_COUNT(pIface) \
         COUNTER_DEC(pIface->ifaceStat.u4AclConfigCount, 1);
                
#define  INC_IFACE_PERMIT_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4PktsPermitCount, 1);

#define  INC_IFACE_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4PktsDenyCount, 1);

#define  INC_IFACE_SPOOF_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IPAddrSpoofedPktsDenyCount, 1);

#define  INC_IFACE_SRC_ROUTE_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4SrcRouteAttackPktsDenyCount, 1);

#define  INC_IFACE_TINY_FRAG_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4TinyFragAttackPktsDenyCount, 1);

#define  INC_IFACE_TCP_SYN_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4SynPktsDenyCount, 1);

#define  INC_IFACE_UDP_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4UdpPktsDenyCount, 1);

#define  INC_IFACE_ICMP_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IcmpPktsDenyCount, 1);

#define  INC_IFACE_FRAG_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4FragmentPktsDenyCount, 1);

#define  INC_IFACE_IP_OPTION_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IpOptionPktsDenyCount, 1);

#define  INC_IFACE_ICMP_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IcmpPktsDenyCount, 1);

#define  INC_IFACE_ATTACKS_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4OtherAttacksPktsDenyCount, 1);

/*****************************************************************************/
/*       Macros for Global Statistical Counters                              */
/*****************************************************************************/

#define  INC_PERMIT_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalPktsPermitCount, 1);

#define  INC_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalPktsDenyCount, 1);

#define  INC_SPOOF_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPAddressSpoofedPktsDenyCount, 1);

#define  INC_SRC_ROUTE_ATTACK_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalSrcRouteAttackPktsDenyCount, 1);

#define  INC_TINY_FRAG_ATTACK_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalTinyFragAttackPktsDenyCount, 1);

#define  INC_LARGE_FRAG_ATTACK_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalLargeFragAttackPktsDenyCount, 1);

#define  INC_TCP_SYN_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalSynPktsDenyCount, 1);

#define  INC_UDP_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalUdpPktsDenyCount, 1);

#define  INC_ICMP_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIcmpPktsDenyCount, 1);

#define  INC_FRAG_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalFragmentPktsDenyCount, 1);

#define  INC_IP_OPTION_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIpOptionPktsDenyCount, 1);

#define  INC_ICMP_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIcmpPktsDenyCount, 1);

#define  INC_ATTACKS_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalAttacksPktsDenyCount, 1);

#define  INC_INSPECT_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalPktsInspectCount, 1);

#define  INC_MEM_FAILURE_COUNT \
         COUNTER_INC(gFwlAclInfo.u4MemFailCount, 1);

#define  INC_LOG_HIT_COUNT(pLogInfo) \
         COUNTER_INC(pLogInfo->u4HitCount, 1);

#define  IF_NUM_OF_FWL_ATTACKS_EXCEEDS_LIMIT \
         ((gFwlAclInfo.u4TotalAttacksPktsDenyCount > FWL_NUM_ATTACKS)?  \
          (FWL_TRUE):(FWL_FALSE))
               

/*****************************************************************************/
/*       Macros for IPv6  Statistical Counters                              */
/*****************************************************************************/

#define  INC_V6_PERMIT_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPv6PktsPermitCount, 1);
#define  INC_V6_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPv6PktsDenyCount, 1);
#define  INC_V6_INSPECT_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPv6PktsInspectCount, 1);
#define  INC_V6_ICMP_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPv6IcmpPktsDenyCount, 1);
#define  INC_V6_SPOOF_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPv6AddressSpoofedPktsDenyCount, 1);
#define  INC_V6_ATTACKS_DISCARD_COUNT \
         COUNTER_INC(gFwlAclInfo.u4TotalIPv6AttackPktsDenyCount, 1);
#define  IF_NUM_OF_FWL_V6_ATTACKS_EXCEEDS_LIMIT \
         ((gFwlAclInfo.u4TotalIPv6AttackPktsDenyCount > FWL_NUM_ATTACKS)?  \
          (FWL_TRUE):(FWL_FALSE))

#define  INC_V6_IFACE_PERMIT_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IPv6PktsPermitCount, 1);

#define  INC_V6_IFACE_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IPv6PktsDenyCount, 1);

#define  INC_V6_IFACE_ICMP_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4Icmpv6PktsDenyCount, 1);

#define  INC_V6_IFACE_SPOOF_DISCARD_COUNT(pIface) \
         COUNTER_INC(pIface->ifaceStat.u4IPv6AddrSpoofedPktsDenyCount, 1);

/*****************************************************************************/
/*       Macros for Filters or Rules specific Statistical Counters           */
/*****************************************************************************/

#define  INC_FILTER_REF_COUNT(pFilter)  \
         COUNTER_INC(pFilter->u1FilterRefCount,1); 
#define  DEC_FILTER_REF_COUNT(pFilter) \
         COUNTER_DECR(pFilter->u1FilterRefCount,1);
#define  INC_FILTER_HIT_COUNT(pFilter) \
         COUNTER_INC(pFilter->u4FilterHitCount,1);
#define  INC_RULE_REF_COUNT(pRule) \
         COUNTER_INC(pRule->u1RuleRefCount,1);
#define  DEC_RULE_REF_COUNT(pRule) \
         COUNTER_DECR(pRule->u1RuleRefCount,1);

/*****************************************************************************/
/*       Macros for getting a specific no. of bytes                          */
/*****************************************************************************/

#define FWL_GET_1_BYTE(pMsg, u4Offset, u1Val)\
        CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *)&u1Val,(u4Offset),1)
                                                                                
#define FWL_GET_2_BYTE(pMsg, u4Offset, u2Val)                     \
do{                                                           \
        CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *)&u2Val,(u4Offset),2); \
        u2Val = OSIX_NTOHS((u2Val));                            \
}while(0)


#define  FWL_GET_4_BYTE(pBuf, offset, value) \
           CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)&value, (offset), 4); \
           value = IP_NTOHL(value)

#define  FWL_GET_N_BYTE(pBuf, offset, value, noOfBytes) \
           CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)&value, (offset), noOfBytes);

#define  FWL_GET_N_BYTE_FROM_BUF(pBuf, offset, value, noOfBytes) \
           CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)value, (offset), noOfBytes);
#define FWL_GET_LINEAR_1BYTE(u1Val, pu1PktBuf) \
   do {                             \
      u1Val = *pu1PktBuf;           \
   } while(0)

#define FWL_GET_LINEAR_4BYTE(u4Val, pu1PktBuf)            \
do {                                        \
      memcpy (&u4Val, pu1PktBuf, 4);\
      u4Val = (UINT4)OSIX_NTOHL(u4Val);               \
} while(0)



#define FWL_STATE_HASH_LIST       gFwlStateInfo.pStateTable
#define FWL_OUT_SESSION_LIST      &gFwlStateInfo.OutSessionsInfo.OutSessionList
#define FWL_IN_SESSION_LIST       &gFwlStateInfo.InSessionsInfo.InSessionList

#define FWL_STATE_V6_HASH_LIST    gFwlV6StateInfo.pStateTable
#define FWL_OUT_V6_SESSION_LIST   &gFwlV6StateInfo.OutSessionsInfo.OutSessionList
#define FWL_IN_V6_SESSION_LIST    &gFwlV6StateInfo.InSessionsInfo.InSessionList

/* Get Total Outbound sessions */   
#define FWL_OUT_TOTAL_SESSION_COUNT   \
   gFwlStateInfo.OutSessionsInfo.u2OutTotalSessionCount

/* Get Total Inbound sessions */   
#define FWL_IN_TOTAL_SESSION_COUNT    \
   gFwlStateInfo.InSessionsInfo.u2InTotalSessionCount

/* Get Total sessions (outbound+inbound) */
#define FWL_TOTAL_SESSION_COUNT       \
   (FWL_OUT_TOTAL_SESSION_COUNT + FWL_IN_TOTAL_SESSION_COUNT)

/* Get Total inbound UDP sessions */
#define FWL_IN_TOTAL_UDP_SESSION_COUNT       \
   gFwlStateInfo.u2InTotalUdpCnt
   
/* Get Total inbound ICMP sessions */
#define FWL_IN_TOTAL_ICMP_SESSION_COUNT       \
   gFwlStateInfo.u2InTotalIcmpCnt
   
/* Increment Total Outbound session count */   
#define FWL_INC_OUT_TOTAL_SESSION_COUNT \
   COUNTER_INC(FWL_OUT_TOTAL_SESSION_COUNT, 1)
   
/* Decrement Total Outbound session count */   
#define FWL_DEC_OUT_TOTAL_SESSION_COUNT \
   COUNTER_DEC(FWL_OUT_TOTAL_SESSION_COUNT, 1)
   
/* Increment Total Inbound session count */   
#define FWL_INC_IN_TOTAL_SESSION_COUNT \
   COUNTER_INC(FWL_IN_TOTAL_SESSION_COUNT, 1)
   
/* Decrement Total Inbound session count */   
#define FWL_DEC_IN_TOTAL_SESSION_COUNT \
   COUNTER_DEC(FWL_IN_TOTAL_SESSION_COUNT, 1)
   
/* Increment Total Inbound UDP session count */   
#define FWL_INC_IN_TOTAL_UDP_SESSION_COUNT \
   COUNTER_INC(FWL_IN_TOTAL_UDP_SESSION_COUNT, 1)
   
/* Decrement Total Inbound UDP session count */   
#define FWL_DEC_IN_TOTAL_UDP_SESSION_COUNT \
   COUNTER_DEC(FWL_IN_TOTAL_UDP_SESSION_COUNT, 1)

/* Increment Total Inbound ICMP session count */   
#define FWL_INC_IN_TOTAL_ICMP_SESSION_COUNT \
   COUNTER_INC(FWL_IN_TOTAL_ICMP_SESSION_COUNT, 1)
   
/* Decrement Total Inbound ICMP session count */   
#define FWL_DEC_IN_TOTAL_ICMP_SESSION_COUNT \
   COUNTER_DEC(FWL_IN_TOTAL_ICMP_SESSION_COUNT, 1)

/* Increment per user outbound session count */   
#define FWL_INC_OUT_USER_SESSION_COUNT(pOutNode) \
   COUNTER_INC(pOutNode->u2OutSessionCount, 1)
   
/* Decrement per user outbound session count */   
#define FWL_DEC_OUT_USER_SESSION_COUNT(pOutNode) \
   COUNTER_DEC(pOutNode->u2OutSessionCount, 1)
   
/* Increment per user inbound session count */   
#define FWL_INC_IN_USER_SESSION_COUNT(pInNode) \
   COUNTER_INC(pInNode->u2InSessionCount, 1)
   
/* Decrement per user inbound session count */   
#define FWL_DEC_IN_USER_SESSION_COUNT(pInNode) \
   COUNTER_DEC(pInNode->u2InSessionCount, 1)
   
#define FWL_LOCAL_TCP_PORT(x)     ((x)->ProtHdr.TcpHdr.LocalTcpInfo.u2Port)
#define FWL_REMOTE_TCP_PORT(x)    ((x)->ProtHdr.TcpHdr.RemoteTcpInfo.u2Port)

#define FWL_LOCAL_UDP_PORT(x)     ((x)->ProtHdr.UdpHdr.u2LocalPort)
#define FWL_REMOTE_UDP_PORT(x)    ((x)->ProtHdr.UdpHdr.u2RemotePort)

#define FWL_LOCAL_PORT(x,p) (((p==FWL_TCP) ? FWL_LOCAL_TCP_PORT(x) : \
                             FWL_LOCAL_UDP_PORT(x)))
   
#define FWL_REMOTE_PORT(x,p) (((p==FWL_TCP) ? FWL_REMOTE_TCP_PORT(x) : \
                             FWL_REMOTE_UDP_PORT(x)))

#define FWL_ICMP_SEQ_NUM(x) (x->ProtHdr.IcmpHdr.u2IcmpSeqNum)

#define FWL_ICMP_IP_ID(x) (x->ProtHdr.IcmpHdr.u2IcmpId)
   
#define FWL_CHECK_PORT_RANGE(p, max, min)   ( (p<=max) && (p>=min) )

#define FWL_BZERO(p,t)            MEMSET(p, 0, sizeof(t))

#define FWL_MAX(x,y)              ( ((x) >= (y)) ? x : y )

#define FWL_MOVE_LOG_BUF_PTR(ptr, curlen, totlen) \
do\
{\
   totlen = (INT2) (totlen + curlen);\
   ptr += curlen;\
} while(0)

#define FWL_IP_ADDR_COPY(pDst, pSrc)  \
do {   \
    MEMCPY (pDst, pSrc, sizeof(tFwlIpAddr));    \
} while (0)

#define FWL_SWAP_PORT(FstPort, ScndPort)                                \
    do {                                                                   \
    FstPort ^= ScndPort;                                                \
    ScndPort ^= FstPort;                                                \
    FstPort ^= ScndPort;                                                \
    } while (0)

#define FWL_SWAP_IPV4_ADDR(FstAddr, ScndAddr)                           \
    do                                                                  \
    {                                                                   \
    FstAddr ^= ScndAddr ;                                               \
    ScndAddr ^= FstAddr;                                                \
    FstAddr ^= ScndAddr ;                                               \
    } while (0)

#define FWL_SWAP_IPV6_ADDR(FstAddr, ScndAddr)                           \
    do                                                                  \
    {                                                                   \
    int cnt = 0;                                                        \
    for (cnt=0; cnt<4; cnt++)                                           \
    {                                                                   \
        FstAddr[cnt] ^= ScndAddr[cnt];                                  \
        ScndAddr[cnt] ^= FstAddr[cnt];                                  \
        FstAddr[cnt] ^= ScndAddr[cnt];                                  \
    }                                                                   \
    } while (0)

#define FWL_SWAP_IP_AND_PORT(pIpHdr,pHLInfo)     \
do\
{ \
 FWL_SWAP_IPV4_ADDR((pIpHdr)->SrcAddr.v4Addr, ((pIpHdr)->DestAddr.v4Addr));\
 FWL_SWAP_PORT(((pHLInfo)->u2SrcPort), ((pHLInfo)->u2DestPort));\
} while(0)

#define FWL_FILL_PARTIAL_ENTRY(pIpHdr, pHLInfo, pInfo) \
do\
{ \
   MEMSET((pIpHdr), 0, sizeof(tIpHeader)); \
   MEMSET((pHLInfo), 0, sizeof(tHLInfo)); \
   MEMCPY (&((pIpHdr)->SrcAddr), &((pInfo)->LocalIP), sizeof(tFwlIpAddr));   \
   MEMCPY (&((pIpHdr)->DestAddr), &((pInfo)->RemoteIP), sizeof(tFwlIpAddr));   \
   (pIpHdr)->u1Proto     = (pInfo)->u1Protocol; \
   (pHLInfo)->u2SrcPort  = (pInfo)->u2LocalPort; \
   (pHLInfo)->u2DestPort = (pInfo)->u2RemotePort; \
} while(0)



/* Macros for returning Log Level depending upon 
 * the Send E-mail alerts options are ENABLED or DISABLED */
#define FWL_EMAIL_WEB_LOG_LEVEL(x) ((x) = ((gFwlAclInfo.u1EmailAlertStatus & \
                                            FWL_EMAIL_FOR_WEB_ACCESS_MASK) ? \
                                           FWLLOG_ALERT_LEVEL : FWLLOG_INFO_LEVEL))

#define FWL_EMAIL_DOS_LOG_LEVEL(x) ((x) = ((gFwlAclInfo.u1EmailAlertStatus & \
                                            FWL_EMAIL_FOR_DOS_ATTACK_MASK) ? \
                                           FWLLOG_ALERT_LEVEL : FWLLOG_INFO_LEVEL))

#define FWL_EMAIL_TCP_LOG_LEVEL(x) ((x) = ((gFwlAclInfo.u1EmailAlertStatus & \
                                            FWL_EMAIL_FOR_TCP_FLAG_MASK) ? \
                                           FWLLOG_ALERT_LEVEL : FWLLOG_INFO_LEVEL))

/* Macros for defining the global custom callback variables that are used
 * in the firewall module. This will be used to invoke custom functions */
#define FWL_UTIL_CALL_BACK  gaFwlUtilCallBack

#define FWL_NP_HW_FILTER_NOT_EXISTS(x) gau1FwlNpHwFilterCap[x]==0
/*****************************************************************************/
/*                        End of File fwlmacs.h                              */
/*****************************************************************************/

