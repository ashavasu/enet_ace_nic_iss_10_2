 /********************************************************
 *   Automatically generated by FuturePostmosy
 *  
 *   Do not Edit!!!
 *  
 *   $Id: fsfwl.h,v 1.11 2016/02/27 10:04:58 siva Exp $
 *  
 **********************************************************/


#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_table[] = {
{"ccitt",        "0"},
{"iso",        "1"},
{"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
{"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000"},
{"org",        "1.3"},
{"dod",        "1.3.6"},
{"internet",        "1.3.6.1"},
{"directory",        "1.3.6.1.1"},
{"mgmt",        "1.3.6.1.2"},
{"mib-2",        "1.3.6.1.2.1"},
{"ip",        "1.3.6.1.2.1.4"},
{"transmission",        "1.3.6.1.2.1.10"},
{"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
{"rmon",        "1.3.6.1.2.1.16"},
{"statistics",        "1.3.6.1.2.1.16.1"},
{"etherStatsHighCapacityEntry",        "1.3.6.1.2.1.16.1.7.1"},
{"history",        "1.3.6.1.2.1.16.2"},
{"hosts",        "1.3.6.1.2.1.16.4"},
{"hostTopN",        "1.3.6.1.2.1.16.5"},
{"matrix",        "1.3.6.1.2.1.16.6"},
{"filter",        "1.3.6.1.2.1.16.7"},
{"capture",        "1.3.6.1.2.1.16.8"},
{"tokenRing",        "1.3.6.1.2.1.16.10"},
{"protocolDist",        "1.3.6.1.2.1.16.12"},
{"nlHost",        "1.3.6.1.2.1.16.14"},
{"nlMatrix",        "1.3.6.1.2.1.16.15"},
{"alHost",        "1.3.6.1.2.1.16.16"},
{"alMatrix",        "1.3.6.1.2.1.16.17"},
{"usrHistory",        "1.3.6.1.2.1.16.18"},
{"probeConfig",        "1.3.6.1.2.1.16.19"},
{"rmonConformance",        "1.3.6.1.2.1.16.20"},
{"dot1dBridge",        "1.3.6.1.2.1.17"},
{"dot1dStp",        "1.3.6.1.2.1.17.2"},
{"dot1dTp",        "1.3.6.1.2.1.17.4"},
{"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
{"dns",        "1.3.6.1.2.1.32"},
{"experimental",        "1.3.6.1.3"},
{"private",        "1.3.6.1.4"},
{"enterprises",        "1.3.6.1.4.1"},
{"ARICENT-MPLS-TP-MIB",        "1.3.6.1.4.1.2076.13.4"},
{"firewall",        "1.3.6.1.4.1.2076.16"},
{"fwlGlobal",        "1.3.6.1.4.1.2076.16.1"},
{"fwlGlobalMasterControlSwitch",        "1.3.6.1.4.1.2076.16.1.1"},
{"fwlGlobalICMPControlSwitch",        "1.3.6.1.4.1.2076.16.1.2"},
{"fwlGlobalIpSpoofFiltering",        "1.3.6.1.4.1.2076.16.1.3"},
{"fwlGlobalSrcRouteFiltering",        "1.3.6.1.4.1.2076.16.1.4"},
{"fwlGlobalTinyFragmentFiltering",        "1.3.6.1.4.1.2076.16.1.5"},
{"fwlGlobalTcpIntercept",        "1.3.6.1.4.1.2076.16.1.6"},
{"fwlGlobalTrap",        "1.3.6.1.4.1.2076.16.1.7"},
{"fwlGlobalTrace",        "1.3.6.1.4.1.2076.16.1.8"},
{"fwlGlobalDebug",        "1.3.6.1.4.1.2076.16.1.9"},
{"fwlGlobalMaxFilters",        "1.3.6.1.4.1.2076.16.1.10"},
{"fwlGlobalMaxRules",        "1.3.6.1.4.1.2076.16.1.11"},
{"fwlGlobalUrlFiltering",        "1.3.6.1.4.1.2076.16.1.12"},
{"fwlGlobalNetBiosFiltering",        "1.3.6.1.4.1.2076.16.1.13"},
{"fwlGlobalNetBiosLan2Wan",        "1.3.6.1.4.1.2076.16.1.14"},
{"fwlGlobalICMPv6ControlSwitch",        "1.3.6.1.4.1.2076.16.1.15"},
{"fwlGlobalIpv6SpoofFiltering",        "1.3.6.1.4.1.2076.16.1.16"},
{"fwlGlobalLogFileSize",        "1.3.6.1.4.1.2076.16.1.17"},
{"fwlGlobalLogSizeThreshold",        "1.3.6.1.4.1.2076.16.1.18"},
{"fwlGlobalIdsLogSize",        "1.3.6.1.4.1.2076.16.1.19"},
{"fwlGlobalIdsLogThreshold",        "1.3.6.1.4.1.2076.16.1.20"},
{"fwlGlobalIdsVersionInfo",        "1.3.6.1.4.1.2076.16.1.21"},
{"fwlGlobalReloadIds",        "1.3.6.1.4.1.2076.16.1.22"},
{"fwlGlobalIdsStatus",        "1.3.6.1.4.1.2076.16.1.23"},
{"fwlGlobalLoadIdsRules",        "1.3.6.1.4.1.2076.16.1.24"},
{"fwlDosAttackAcceptRedirect",        "1.3.6.1.4.1.2076.16.1.25"},
{"fwlDosAttackAcceptSmurfAttack",        "1.3.6.1.4.1.2076.16.1.26"},
{"fwlDosLandAttack",        "1.3.6.1.4.1.2076.16.1.27"},
{"fwlDosShortHeaderAttack",        "1.3.6.1.4.1.2076.16.1.28"},
{"fwlDefinition",        "1.3.6.1.4.1.2076.16.2"},
{"fwlDefnTcpInterceptThreshold",        "1.3.6.1.4.1.2076.16.2.1"},
{"fwlDefnInterceptTimeout",        "1.3.6.1.4.1.2076.16.2.2"},
{"fwlDefnFilterTable",        "1.3.6.1.4.1.2076.16.2.3"},
{"fwlDefnFilterEntry",        "1.3.6.1.4.1.2076.16.2.3.1"},
{"fwlFilterFilterName",        "1.3.6.1.4.1.2076.16.2.3.1.1"},
{"fwlFilterSrcAddress",        "1.3.6.1.4.1.2076.16.2.3.1.2"},
{"fwlFilterDestAddress",        "1.3.6.1.4.1.2076.16.2.3.1.3"},
{"fwlFilterProtocol",        "1.3.6.1.4.1.2076.16.2.3.1.4"},
{"fwlFilterSrcPort",        "1.3.6.1.4.1.2076.16.2.3.1.5"},
{"fwlFilterDestPort",        "1.3.6.1.4.1.2076.16.2.3.1.6"},
{"fwlFilterAckBit",        "1.3.6.1.4.1.2076.16.2.3.1.7"},
{"fwlFilterRstBit",        "1.3.6.1.4.1.2076.16.2.3.1.8"},
{"fwlFilterTos",        "1.3.6.1.4.1.2076.16.2.3.1.9"},
{"fwlFilterAccounting",        "1.3.6.1.4.1.2076.16.2.3.1.10"},
{"fwlFilterHitClear",        "1.3.6.1.4.1.2076.16.2.3.1.11"},
{"fwlFilterHitsCount",        "1.3.6.1.4.1.2076.16.2.3.1.12"},
{"fwlFilterAddrType",        "1.3.6.1.4.1.2076.16.2.3.1.13"},
{"fwlFilterFlowId",        "1.3.6.1.4.1.2076.16.2.3.1.14"},
{"fwlFilterDscp",        "1.3.6.1.4.1.2076.16.2.3.1.15"},
{"fwlFilterRowStatus",        "1.3.6.1.4.1.2076.16.2.3.1.16"},
{"fwlDefnRuleTable",        "1.3.6.1.4.1.2076.16.2.4"},
{"fwlDefnRuleEntry",        "1.3.6.1.4.1.2076.16.2.4.1"},
{"fwlRuleRuleName",        "1.3.6.1.4.1.2076.16.2.4.1.1"},
{"fwlRuleFilterSet",        "1.3.6.1.4.1.2076.16.2.4.1.2"},
{"fwlRuleRowStatus",        "1.3.6.1.4.1.2076.16.2.4.1.3"},
{"fwlDefnAclTable",        "1.3.6.1.4.1.2076.16.2.5"},
{"fwlDefnAclEntry",        "1.3.6.1.4.1.2076.16.2.5.1"},
{"fwlAclIfIndex",        "1.3.6.1.4.1.2076.16.2.5.1.1"},
{"fwlAclAclName",        "1.3.6.1.4.1.2076.16.2.5.1.2"},
{"fwlAclDirection",        "1.3.6.1.4.1.2076.16.2.5.1.3"},
{"fwlAclAction",        "1.3.6.1.4.1.2076.16.2.5.1.4"},
{"fwlAclSequenceNumber",        "1.3.6.1.4.1.2076.16.2.5.1.5"},
{"fwlAclAclType",        "1.3.6.1.4.1.2076.16.2.5.1.6"},
{"fwlAclLogTrigger",        "1.3.6.1.4.1.2076.16.2.5.1.7"},
{"fwlAclFragAction",        "1.3.6.1.4.1.2076.16.2.5.1.8"},
{"fwlAclRowStatus",        "1.3.6.1.4.1.2076.16.2.5.1.9"},
{"fwlDefnIfTable",        "1.3.6.1.4.1.2076.16.2.6"},
{"fwlDefnIfEntry",        "1.3.6.1.4.1.2076.16.2.6.1"},
{"fwlIfIfIndex",        "1.3.6.1.4.1.2076.16.2.6.1.1"},
{"fwlIfIfType",        "1.3.6.1.4.1.2076.16.2.6.1.2"},
{"fwlIfIpOptions",        "1.3.6.1.4.1.2076.16.2.6.1.3"},
{"fwlIfFragments",        "1.3.6.1.4.1.2076.16.2.6.1.4"},
{"fwlIfFragmentSize",        "1.3.6.1.4.1.2076.16.2.6.1.5"},
{"fwlIfICMPType",        "1.3.6.1.4.1.2076.16.2.6.1.6"},
{"fwlIfICMPCode",        "1.3.6.1.4.1.2076.16.2.6.1.7"},
{"fwlIfICMPv6MsgType",        "1.3.6.1.4.1.2076.16.2.6.1.8"},
{"fwlIfRowStatus",        "1.3.6.1.4.1.2076.16.2.6.1.9"},
{"fwlDefnDmzTable",        "1.3.6.1.4.1.2076.16.2.7"},
{"fwlDefnDmzEntry",        "1.3.6.1.4.1.2076.16.2.7.1"},
{"fwlDmzIpIndex",        "1.3.6.1.4.1.2076.16.2.7.1.1"},
{"fwlDmzRowStatus",        "1.3.6.1.4.1.2076.16.2.7.1.2"},
{"fwlUrlFilterTable",        "1.3.6.1.4.1.2076.16.2.8"},
{"fwlUrlFilterEntry",        "1.3.6.1.4.1.2076.16.2.8.1"},
{"fwlUrlString",        "1.3.6.1.4.1.2076.16.2.8.1.1"},
{"fwlUrlHitCount",        "1.3.6.1.4.1.2076.16.2.8.1.2"},
{"fwlUrlFilterRowStatus",        "1.3.6.1.4.1.2076.16.2.8.1.3"},
{"fwlDefnBlkListTable",        "1.3.6.1.4.1.2076.16.2.9"},
{"fwlDefnBlkListEntry",        "1.3.6.1.4.1.2076.16.2.9.1"},
{"fwlBlkListIpAddressType",        "1.3.6.1.4.1.2076.16.2.9.1.1"},
{"fwlBlkListIpAddress",        "1.3.6.1.4.1.2076.16.2.9.1.2"},
{"fwlBlkListIpMask",        "1.3.6.1.4.1.2076.16.2.9.1.3"},
{"fwlBlkListHitsCount",        "1.3.6.1.4.1.2076.16.2.9.1.4"},
{"fwlBlkListEntryType",        "1.3.6.1.4.1.2076.16.2.9.1.5"},
{"fwlBlkListRowStatus",        "1.3.6.1.4.1.2076.16.2.9.1.6"},
{"fwlDefnWhiteListTable",        "1.3.6.1.4.1.2076.16.2.10"},
{"fwlDefnWhiteListEntry",        "1.3.6.1.4.1.2076.16.2.10.1"},
{"fwlWhiteListIpAddressType",        "1.3.6.1.4.1.2076.16.2.10.1.1"},
{"fwlWhiteListIpAddress",        "1.3.6.1.4.1.2076.16.2.10.1.2"},
{"fwlWhiteListIpMask",        "1.3.6.1.4.1.2076.16.2.10.1.3"},
{"fwlWhiteListHitsCount",        "1.3.6.1.4.1.2076.16.2.10.1.4"},
{"fwlWhiteListRowStatus",        "1.3.6.1.4.1.2076.16.2.10.1.5"},
{"fwlDefnIPv6DmzTable",        "1.3.6.1.4.1.2076.16.2.11"},
{"fwlDefnIPv6DmzEntry",        "1.3.6.1.4.1.2076.16.2.11.1"},
{"fwlDmzAddressType",        "1.3.6.1.4.1.2076.16.2.11.1.1"},
{"fwlDmzIpv6Index",        "1.3.6.1.4.1.2076.16.2.11.1.2"},
{"fwlDmzIpv6RowStatus",        "1.3.6.1.4.1.2076.16.2.11.1.3"},
{"fwlStatistics",        "1.3.6.1.4.1.2076.16.3"},
{"fwlStatInspectedPacketsCount",        "1.3.6.1.4.1.2076.16.3.1"},
{"fwlStatTotalPacketsDenied",        "1.3.6.1.4.1.2076.16.3.2"},
{"fwlStatTotalPacketsAccepted",        "1.3.6.1.4.1.2076.16.3.3"},
{"fwlStatTotalIcmpPacketsDenied",        "1.3.6.1.4.1.2076.16.3.4"},
{"fwlStatTotalSynPacketsDenied",        "1.3.6.1.4.1.2076.16.3.5"},
{"fwlStatTotalIpSpoofedPacketsDenied",        "1.3.6.1.4.1.2076.16.3.6"},
{"fwlStatTotalSrcRoutePacketsDenied",        "1.3.6.1.4.1.2076.16.3.7"},
{"fwlStatTotalTinyFragmentPacketsDenied",        "1.3.6.1.4.1.2076.16.3.8"},
{"fwlStatTotalFragmentedPacketsDenied",        "1.3.6.1.4.1.2076.16.3.9"},
{"fwlStatTotalLargeFragmentPacketsDenied",        "1.3.6.1.4.1.2076.16.3.10"},
{"fwlStatTotalIpOptionPacketsDenied",        "1.3.6.1.4.1.2076.16.3.11"},
{"fwlStatTotalAttacksPacketsDenied",        "1.3.6.1.4.1.2076.16.3.12"},
{"fwlStatMemoryAllocationFailCount",        "1.3.6.1.4.1.2076.16.3.13"},
{"fwlStatIPv6InspectedPacketsCount",        "1.3.6.1.4.1.2076.16.3.14"},
{"fwlStatIPv6TotalPacketsDenied",        "1.3.6.1.4.1.2076.16.3.15"},
{"fwlStatIPv6TotalPacketsAccepted",        "1.3.6.1.4.1.2076.16.3.16"},
{"fwlStatIPv6TotalIcmpPacketsDenied",        "1.3.6.1.4.1.2076.16.3.17"},
{"fwlStatIPv6TotalSpoofedPacketsDenied",        "1.3.6.1.4.1.2076.16.3.18"},
{"fwlStatIPv6TotalAttacksPacketsDenied",        "1.3.6.1.4.1.2076.16.3.19"},
{"fwlStatIfTable",        "1.3.6.1.4.1.2076.16.3.20"},
{"fwlStatIfEntry",        "1.3.6.1.4.1.2076.16.3.20.1"},
{"fwlStatIfIfIndex",        "1.3.6.1.4.1.2076.16.3.20.1.1"},
{"fwlStatIfFilterCount",        "1.3.6.1.4.1.2076.16.3.20.1.2"},
{"fwlStatIfPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.3"},
{"fwlStatIfPacketsAccepted",        "1.3.6.1.4.1.2076.16.3.20.1.4"},
{"fwlStatIfSynPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.5"},
{"fwlStatIfIcmpPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.6"},
{"fwlStatIfIpSpoofedPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.7"},
{"fwlStatIfSrcRoutePacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.8"},
{"fwlStatIfTinyFragmentPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.9"},
{"fwlStatIfFragmentPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.10"},
{"fwlStatIfIpOptionPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.11"},
{"fwlStatIfClear",        "1.3.6.1.4.1.2076.16.3.20.1.12"},
{"fwlIfTrapThreshold",        "1.3.6.1.4.1.2076.16.3.20.1.13"},
{"fwlStatIfIPv6PacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.14"},
{"fwlStatIfIPv6PacketsAccepted",        "1.3.6.1.4.1.2076.16.3.20.1.15"},
{"fwlStatIfIPv6IcmpPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.16"},
{"fwlStatIfIPv6SpoofedPacketsDenied",        "1.3.6.1.4.1.2076.16.3.20.1.17"},
{"fwlStatIfClearIPv6",        "1.3.6.1.4.1.2076.16.3.20.1.18"},
{"fwlStatClear",        "1.3.6.1.4.1.2076.16.3.21"},
{"fwlStatClearIPv6",        "1.3.6.1.4.1.2076.16.3.22"},
{"fwlTrapThreshold",        "1.3.6.1.4.1.2076.16.3.23"},
{"fwlTraps",        "1.3.6.1.4.1.2076.16.4"},
{"fwlTrapTypes",        "1.3.6.1.4.1.2076.16.4.0"},
{"fwlTrapControl",        "1.3.6.1.4.1.2076.16.4.1"},
{"fwlTrapMemFailMessage",        "1.3.6.1.4.1.2076.16.4.1.1"},
{"fwlTrapAttackMessage",        "1.3.6.1.4.1.2076.16.4.1.2"},
{"fwlIfIndex",        "1.3.6.1.4.1.2076.16.4.1.3"},
{"fwlTrapEvent",        "1.3.6.1.4.1.2076.16.4.1.4"},
{"fwlTrapEventTime",        "1.3.6.1.4.1.2076.16.4.1.5"},
{"fwlTrapFileName",        "1.3.6.1.4.1.2076.16.4.1.6"},
{"fwlIdsTrapEvent",        "1.3.6.1.4.1.2076.16.4.1.7"},
{"fwlIdsTrapEventTime",        "1.3.6.1.4.1.2076.16.4.1.8"},
{"fwlIdsTrapFileName",        "1.3.6.1.4.1.2076.16.4.1.9"},
{"fwlIdsAttackPktIp",        "1.3.6.1.4.1.2076.16.4.1.10"},
{"fwlState",        "1.3.6.1.4.1.2076.16.5"},
{"fwlStateTable",        "1.3.6.1.4.1.2076.16.5.1"},
{"fwlStateEntry",        "1.3.6.1.4.1.2076.16.5.1.1"},
{"fwlStateType",        "1.3.6.1.4.1.2076.16.5.1.1.1"},
{"fwlStateLocalIpAddrType",        "1.3.6.1.4.1.2076.16.5.1.1.2"},
{"fwlStateLocalIpAddress",        "1.3.6.1.4.1.2076.16.5.1.1.3"},
{"fwlStateRemoteIpAddrType",        "1.3.6.1.4.1.2076.16.5.1.1.4"},
{"fwlStateRemoteIpAddress",        "1.3.6.1.4.1.2076.16.5.1.1.5"},
{"fwlStateLocalPort",        "1.3.6.1.4.1.2076.16.5.1.1.6"},
{"fwlStateRemotePort",        "1.3.6.1.4.1.2076.16.5.1.1.7"},
{"fwlStateProtocol",        "1.3.6.1.4.1.2076.16.5.1.1.8"},
{"fwlStateDirection",        "1.3.6.1.4.1.2076.16.5.1.1.9"},
{"fwlStateEstablishedTime",        "1.3.6.1.4.1.2076.16.5.1.1.10"},
{"fwlStateLocalState",        "1.3.6.1.4.1.2076.16.5.1.1.11"},
{"fwlStateRemoteState",        "1.3.6.1.4.1.2076.16.5.1.1.12"},
{"fwlStateLogLevel",        "1.3.6.1.4.1.2076.16.5.1.1.13"},
{"fwlStateCallStatus",        "1.3.6.1.4.1.2076.16.5.1.1.14"},
{"fwlRateLimit",        "1.3.6.1.4.1.2076.16.6"},
{"fwlRateLimitTable",        "1.3.6.1.4.1.2076.16.6.1"},
{"fwlRateLimitEntry",        "1.3.6.1.4.1.2076.16.6.1.1"},
{"fwlRateLimitPortIndex",        "1.3.6.1.4.1.2076.16.6.1.1.1"},
{"fwlRateLimitPortNumber",        "1.3.6.1.4.1.2076.16.6.1.1.2"},
{"fwlRateLimitPortType",        "1.3.6.1.4.1.2076.16.6.1.1.3"},
{"fwlRateLimitValue",        "1.3.6.1.4.1.2076.16.6.1.1.4"},
{"fwlRateLimitBurstSize",        "1.3.6.1.4.1.2076.16.6.1.1.5"},
{"fwlRateLimitTrafficMode",        "1.3.6.1.4.1.2076.16.6.1.1.6"},
{"fwlRateLimitRowStatus",        "1.3.6.1.4.1.2076.16.6.1.1.7"},
{"fwlSnork",        "1.3.6.1.4.1.2076.16.7"},
{"fwlSnorkTable",        "1.3.6.1.4.1.2076.16.7.1"},
{"fwlSnorkEntry",        "1.3.6.1.4.1.2076.16.7.1.1"},
{"fwlSnorkPortNo",        "1.3.6.1.4.1.2076.16.7.1.1.1"},
{"fwlSnorkRowStatus",        "1.3.6.1.4.1.2076.16.7.1.1.2"},
{"fwlRpf",        "1.3.6.1.4.1.2076.16.8"},
{"fwlRpfTable",        "1.3.6.1.4.1.2076.16.8.1"},
{"fwlRpfEntry",        "1.3.6.1.4.1.2076.16.8.1.1"},
{"fwlRpfInIndex",        "1.3.6.1.4.1.2076.16.8.1.1.1"},
{"fwlRpfMode",        "1.3.6.1.4.1.2076.16.8.1.1.2"},
{"fwlRpfRowStatus",        "1.3.6.1.4.1.2076.16.8.1.1.3"},
{"issExt",        "1.3.6.1.4.1.2076.81.8"},
{"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
{"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
{"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
{"security",        "1.3.6.1.5"},
{"snmpV2",        "1.3.6.1.6"},
{"snmpDomains",        "1.3.6.1.6.1"},
{"snmpProxys",        "1.3.6.1.6.2"},
{"snmpModules",        "1.3.6.1.6.3"},
{"snmpTraps",        "1.3.6.1.6.3.1.1.5"},
{"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
{"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
{"ieee802dot1mibs",        "1.3.111.2.802.1.1"},
{"joint-iso-ccitt",        "2"},
{0,0},
};

#endif /* _SNMP_MIB_H */
