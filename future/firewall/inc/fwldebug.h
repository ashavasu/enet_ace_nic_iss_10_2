/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwldebug.h,v 1.2 2011/09/24 09:15:55 siva Exp $
 *
 * Description:This file contains the MACRO definitions   
 *             for the debugging purpose. 
 *
 *******************************************************************/
#define  FWL_DBG_FLAG  gFwlAclInfo.u4FwlDbg 

#ifdef TRACE_WANTED

#define  FWL_DBG(Value, Fmt)           UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt)

#define  FWL_DBG1(Value, Fmt, Arg)     UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt,          \
                                                Arg)

#define  FWL_DBG2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt,          \
                                                Arg1, Arg2)

#define  FWL_DBG3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3)

#define  FWL_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt,          \
                                                Arg1, Arg2,   \
                                                Arg3, Arg4)

#define  FWL_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5)

#define  FWL_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(FWL_DBG_FLAG, \
                                                Value,        \
                                                "FWL",        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6)
#else
#define  FWL_DBG(Value, Fmt)           
#define  FWL_DBG1(Value, Fmt, Arg)     
#define  FWL_DBG2(Value, Fmt, Arg1, Arg2)                      
#define  FWL_DBG3(Value, Fmt, Arg1, Arg2, Arg3)                
#define  FWL_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          
#define  FWL_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    
#define  FWL_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) 
#endif
/* Debug categories */
#define  FWL_DBG_OFF          UTL_DBG_OFF         
#define  FWL_DBG_ALL          UTL_DBG_ALL         
#define  FWL_DBG_INIT_SHUTDN  UTL_DBG_INIT_SHUTDN 
#define  FWL_DBG_CTRL_IF      UTL_DBG_CTRL_IF     
#define  FWL_DBG_STATUS_IF    UTL_DBG_STATUS_IF   
#define  FWL_DBG_SNMP_IF      UTL_DBG_SNMP_IF     
#define  FWL_DBG_BUF_IF       UTL_DBG_BUF_IF      
#define  FWL_DBG_MEM_IF       UTL_DBG_MEM_IF      
#define  FWL_DBG_RX           UTL_DBG_RX          
#define  FWL_DBG_TX           UTL_DBG_TX          

#define  FWL_DBG_ENTRY        0x00010000 
#define  FWL_DBG_EXIT         0x00020000 
#define  FWL_DBG_INTMD        0x00040000 
#define  FWL_DBG_PKT_EXTRACT  0x00080000 
#define  FWL_DBG_CTRL_FLOW    0x00100000 
#define  FWL_DBG_IF           0x00200000 
#define  FWL_DBG_CONFIG       0x00400000 
#define  FWL_DBG_DBASE        0x00800000 
#define  FWL_DBG_INSPECT      0x01000000 
#define  FWL_DBG_ERROR        0x02000000 
#define  FWL_DBG_MEM          0x04000000 

/************************** Firewall specific Trace values ******************/                                      

#define  FWL_TRC_ACTION       0x00010000 
#define  FWL_TRC_INSPECT      0x00020000 
#define  FWL_TRC_ERROR        0x00040000 
#define  FWL_TRC_TRAP         0x00080000 


          
/*****************************************************************************/
/*              End Of File fwldebug.h                                       */
/*****************************************************************************/
