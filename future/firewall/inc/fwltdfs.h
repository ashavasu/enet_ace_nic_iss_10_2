/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltdfs.h,v 1.13 2015/07/15 10:59:50 siva Exp $
 *
 * Description:This file contains type definitions        
 *             relating to Firewall               
 *
 *******************************************************************/

#ifndef _FWLTDFS_H
#define _FWLTDFS_H



#define FWL_MAX_PORT_LEN            12

/*****************************************************************************/
/*   General type definitions                                                */
/*****************************************************************************/

typedef UINT4         COUNTER32;  /*  A four byte counter used to count the 
                                   *  packets dropped, rejected, permitted,
                                   *  Fragmented etc. 
                                   */

typedef UINT2         COUNTER16;  /*  A two byte counter used to count the 
                                   *  general stats info.
                                   */
 
typedef UINT1         GAUGE;      /*  A two byte gauge used as a reference
                                   *  counter for Rule, Filter referentials.
                                   */ 


/*****************************************************************************/
/*   Buffer Pool Id Related                                                  */
/*****************************************************************************/

typedef struct {
    tMemPoolId  rulePoolId;     /* Rule data structure free pool Id */
    tMemPoolId  filterPoolId;   /* Filter data structure free poolId */
    tMemPoolId  aclInfoPoolId;  /* AclFilter data strucute free pool Id */
    tMemPoolId  ifacePoolId;    /* Interface data structure free pool Id */
    tMemPoolId  logFreePoolId;     /* Log buffers free pool id */
    tMemPoolId  statePoolId;       /* State Filter Hash Table pool Id */
    tMemPoolId  tcpInitFlowPoolId; /* TCP Init Flow List pool Id */
    tMemPoolId  partialLinksPoolId; /* Partial Links List pool Id */   
    tMemPoolId  inUserSessionPoolId; /* In User Session List pool Id */   
    tMemPoolId  outUserSessionPoolId; /* Out User Session List pool Id */   
    tMemPoolId  dmzPoolId;           /* DMZ List pool Id */   
    tMemPoolId  urlFilterPoolId;     /* URL filter List pool Id */   
} tFwlMemPoolId;

/*****************************************************************************/
/*   Filter Structure                                                      */
/*****************************************************************************/

typedef struct {
        tTMO_SLL_NODE  nextFilterInfo;
    UINT4          u4FlowId;
        UINT1          au1FilterName [FWL_MAX_FILTER_NAME_LEN];

        /* The following four members are used during SNMP GET operation. If not
         * present the ports and the IP address will not be in format as in SET. */

        UINT1          au1SrcPort[FWL_MAX_PORT_LEN];
        UINT1          au1DestPort[FWL_MAX_PORT_LEN];
        UINT1          au1SrcAddr[FWL_MAX_ADDR_LEN];
        UINT1          au1DestAddr[FWL_MAX_ADDR_LEN];
        UINT1          u1Pad[2];

        tFwlIpAddr     SrcStartAddr;
        tFwlIpAddr     SrcEndAddr;
        tFwlIpAddr     DestStartAddr;
        tFwlIpAddr     DestEndAddr;
        COUNTER32      u4FilterHitCount;
        UINT2          u2SrcMaxPort;
        UINT2          u2SrcMinPort;
        UINT2          u2DestMaxPort;
        UINT2          u2DestMinPort;
    UINT2          u2AddrType;
        UINT1          u1Proto;
        UINT1          u1Tos;     /*  TOS support */
        UINT1          u1TcpAck;
        UINT1          u1TcpRst;
        GAUGE          u1FilterRefCount;
        UINT1          u1RowStatus;
        UINT1          u1FilterAccounting; /* enable or disable filter accounting*/
        UINT1          u2Reserved;
        UINT1          au1Padding[2];
    INT4           i4Dscp;
} tFilterInfo;  

/*****************************************************************************/
/*   Rule Structure                                                          */
/*****************************************************************************/

typedef struct {
    tTMO_SLL_NODE  nextRuleInfo;
    UINT1          au1RuleName [FWL_MAX_RULE_NAME_LEN];
    UINT1          au1FilterSet [FWL_MAX_FILTER_SET_LEN]; /* This string is  
                                                           * used during 
                                                           * SNMP GET. 
                                                           */
    tFilterInfo    *apFilterInfo [FWL_MAX_FILTERS_IN_RULE];
    UINT2          u2MinSrcPort;
    UINT2          u2MaxSrcPort;
    UINT2          u2MinDestPort;
    UINT2          u2MaxDestPort;
    UINT1          u1FilterConditionFlag;        /* Specifies, the filter
                                                  * combination is "&" or
                                                  * "Or" operator.
                                                  */
    GAUGE          u1RuleRefCount;
    UINT1          u1RowStatus;
    UINT1          u1Reserved; 
} tRuleInfo;

/*****************************************************************************/
/*   Logging information structure for each type of attack                   */
/*****************************************************************************/

typedef struct FwlLogBuffer{
    UINT1 au1Data[FWL_MAX_LOG_BUF_SIZE];
    struct FwlLogBuffer *pNext;
} tFwlLogBuffer;

/*****************************************************************************/
/*  Statistics Info Structure per interface                                  */
/*****************************************************************************/

typedef struct {
    COUNTER32  u4AclConfigCount;
    COUNTER32  u4PktsPermitCount;
    COUNTER32  u4PktsDenyCount;
    COUNTER32  u4SynPktsDenyCount;
    COUNTER32  u4UdpPktsDenyCount;
    COUNTER32  u4FragmentPktsDenyCount;
    COUNTER32  u4IpOptionPktsDenyCount;
    COUNTER32  u4IcmpPktsDenyCount;
    COUNTER32  u4SrcRouteAttackPktsDenyCount; /* Pkts denied due to Src
                                               * Route Attack. 
                                               */
    COUNTER32  u4TinyFragAttackPktsDenyCount; /* Pkts denied due to Tiny
                                               * Fragment attack.
                                               */
    COUNTER32  u4IPAddrSpoofedPktsDenyCount;  /* Pkts denied due to IP
                                               * Src address spoofing and
                                               * these counters will be 
                                               * updated only for the
                                               * external interfaces.
                                               */
    COUNTER32 u4OtherAttacksPktsDenyCount;
    COUNTER32 u4IPv6PktsPermitCount;
    COUNTER32 u4IPv6PktsDenyCount;
    COUNTER32 u4Icmpv6PktsDenyCount;
    COUNTER32 u4IPv6AddrSpoofedPktsDenyCount;
    INT4           i4TrapThreshold;           /* Threshold value beyond which
                                                 trap would be generated */
} tIfaceStat;

/*****************************************************************************/
/*  Interface Structure -- This structure holds the all interface based      */
/*                         configurations like Icmp type & code, Ip option   */
/*                         and Fragment.                                     */
/*****************************************************************************/

typedef struct {
    tTMO_SLL    inFilterList;  /* Linked list of filters that are needed 
                                * to be checked when the packet comes in 
                                * through an interface.
                                */
    tTMO_SLL    inIPv6FilterList;
    tTMO_SLL    outFilterList; /* Linked list of filters that have to be
                                * checked against the packet goes out 
                                * through an interface.
                                */
    tTMO_SLL    outIPv6FilterList;
    tIfaceStat  ifaceStat;  /* Holds the iface statistics variables */
    UINT4       u4IfaceNum;
    UINT2       u2MaxFragmentSize; /* Max Fragmented pkt size to be allowed */
    UINT1       u1IfaceType;   /* Spcifies whether internal or external */
    UINT1       u1Fragment;    /* verify tiny or large fragments */
    UINT1       u1IpOption;
    UINT1       u1IcmpType;
    UINT1       u1IcmpCode;
    UINT1       u1RowStatus;
    UINT1       u1LogTrigger;   
    UINT1       u1Pad[3];
    INT4        i4Icmpv6MsgType;
} tIfaceInfo;    

typedef struct _FwlDOSThresholds
{
   UINT4 u4FwlTcpInitDropTimeout;   /* UINT1  u1DenyConnectionForMins; */
   UINT4 u4FwlTcpInitThreshAction;  /* UINT1  u1IsDenyOrDelete; */
   UINT4 u4FwlTcpInitLowerThresh;   /* UINT1  u1MaxIncompleteLow; */
   UINT4 u4FwlTcpInitUpperThresh;   /* UINT1  u1MaxIncompleteHigh; */
   UINT4 u4FwlTcpInitMaxPerNode;    /* UINT1  u1TcpMaxIncomplete; */
   UINT1 u1OneMinLow;
   UINT1 u1OneMinHigh;
   UINT1 u1Reserved[2];
}tFwlDOSThresholds;

/* Structure only for Web */
typedef struct _FwlWebShowDOSThresholds
{
   UINT4 u4FwlTcpInitDropTimeout;   /* UINT1  u1DenyConnectionForMins; */
   UINT4 u4FwlTcpInitThreshAction;  /* UINT1  u1IsDenyOrDelete; */
   UINT4 u4FwlTcpInitLowerThresh;   /* UINT1  u1MaxIncompleteLow; */
   UINT4 u4FwlTcpInitUpperThresh;   /* UINT1  u1MaxIncompleteHigh; */
   UINT4 u4FwlTcpInitMaxPerNode;    /* UINT1  u1TcpMaxIncomplete; */
   UINT4 u4NetBiosFiltering;
   UINT4 u4RespondPing;
   UINT1 u1OneMinLow;
   UINT1 u1OneMinHigh;
   UINT1 u1Reserved[2];
}tFwlWebShowDOSThresholds; 


typedef struct FirewallTrapMsg
{
    UINT1  au1FwlLogFileName[FWL_MAX_FILE_NAME_LEN];   /*  Firewall Log File*/
    CHR1   ac1DateTime[FWL_TRAP_TIME_STR_LEN];         /*  Event Time  */
    UINT4  u4Event;                                    /*  Event Type  */
}tFirewallTrapMsg;

typedef struct IdsTrapMsg
{
    UINT1       au1IdsLogFileName[IDS_FILENAME_LEN];      /* IdsLog File */
    CHR1        ac1DateTime[IDS_TRAP_TIME_STR_LEN];       /* Event Time  */
    UINT4       u4Event;                                  /* Event Type  */
}tIdsTrapMsg;


/*****************************************************************************/
/*   Global Firewall Accesslist stucture                                     */
/*****************************************************************************/

typedef struct {
    tTMO_SLL       filterList;
    tTMO_SLL       ruleList;
    tIfaceInfo     *apIfaceList[FWL_MAX_NUM_OF_IF];
    tTmrAppTimer   TcpInterceptTimer;    /* Timer for Syn Flooding Control */
    tTmrAppTimer   UdpInterceptTimer;    /* Timer for Udp Flooding Control */
    tTmrAppTimer   IcmpInterceptTimer;   /* Timer for Icmp Flooding Control */
    tFwlDOSThresholds  FwlDOSThresholds;
    UINT1          au1TrapMemFailMessage[FWL_MAX_TRAP_LEN];
    UINT1          au1TrapAttackMessage[FWL_MAX_TRAP_LEN];

    UINT4          u4FwlDbg;
    UINT4          u4FwlTrc;
    COUNTER32      u4MemFailCount;
    COUNTER32      u4TotalIPAddressSpoofedPktsDenyCount;
    COUNTER32      u4TotalSynPktsDenyCount;
    COUNTER32      u4TotalUdpPktsDenyCount;
    COUNTER32      u4TotalSrcRouteAttackPktsDenyCount;
    COUNTER32      u4TotalTinyFragAttackPktsDenyCount;
    COUNTER32      u4TotalLargeFragAttackPktsDenyCount;
    COUNTER32      u4TotalPktsPermitCount;
    COUNTER32      u4TotalPktsDenyCount;
    COUNTER32      u4TotalFragmentPktsDenyCount;
    COUNTER32      u4TotalIpOptionPktsDenyCount;
    COUNTER32      u4TotalIcmpPktsDenyCount;
    COUNTER32      u4TotalAttacksPktsDenyCount;
    COUNTER32      u4TotalPktsInspectCount;
    
    COUNTER32      u4TotalIPv6PktsInspectCount;
    COUNTER32      u4TotalIPv6PktsDenyCount;
    COUNTER32      u4TotalIPv6PktsPermitCount;
    COUNTER32      u4TotalIPv6IcmpPktsDenyCount;
    COUNTER32      u4TotalIPv6AddressSpoofedPktsDenyCount;
    COUNTER32      u4TotalIPv6AttackPktsDenyCount;
    
    INT4           i4MemStatus;
    INT4           i4TrapThreshold;          /* Threshold value beyond which
                                                trap would be generated */
    INT4           i4DosAttackRedirect;      /*DOS attack*/
    INT4           i4SmurfAttack;     
    INT4           i4LandAttack;     
    INT4           i4ShortHeaderlength;     
    INT4           i4SetFlag;     
   
    UINT2          gu2IdleTimerValue;
    UINT2          gu2TcpInitFlowTimeout;
    UINT2          gu2TcpEstFlowTimeout;
    UINT2          gu2TcpFinFlowTimeout;

    UINT2          gu2IcmpFlowTimeout;
    UINT2          gu2UdpFlowLargeTimeout;
    UINT2          u2RtrSynPktsAllowed;
    UINT2          u2SynPktsAllowed;

    UINT2          u2TcpSynLimit;     /* No.of connections establishment pkts */
    UINT2          u2UdpPktsAllowed;
    UINT2          u2UdpLimit;
    UINT2          u2IcmpPktsAllowed;

    UINT2          u2IcmpLimit;
    UINT2          u2TcpInterceptTimeOut;    /* Configured in seconds */
    UINT2          u2UdpInterceptTimeOut;    /* Configured in seconds */
    UINT2          u2IcmpInterceptTimeOut;    /* Configured in seconds */

    UINT1          u1NetBiosLan2WanEnable;
    UINT1          u1EmailAlertStatus;
    UINT1          u1TrapSentFlag;
    UINT1          u1LimitConcurrentSess;

    UINT1          u1IcmpGenerate;
    UINT1          u1Icmpv6Generate;
    UINT1          u1IpSpoofFilteringEnable;
    UINT1          u1Ipv6SpoofFilteringEnable;
    UINT1          u1SrcRouteFilteringEnable;
    UINT1          u1TinyFragFilteringEnable;

    UINT1          u1TcpInterceptEnable;
    UINT1          u1UdpFloodInspect; 
    UINT1          u1IcmpFloodInspect;
    UINT1          u1NetBiosFilteringEnable;

    UINT1          u1UrlFilteringEnable; 
    UINT1          u1TrapEnable;
    UINT1          u1InspectIcmpErrMsg;
    UINT1          u1InspectIcmpv6ErrMsg;
    UINT1          u1Reserved;  
    UINT1          au1Padding;  
} tFwlAclInfo;

/*****************************************************************************/
/*   Acl info Structure - This reflects the Acl table defined in the MIB     */
/*****************************************************************************/

typedef struct {
    tTMO_SLL_NODE  nextFilterInfo;
    UINT1          au1FilterName[FWL_MAX_ACL_NAME_LEN];
    VOID           *pAclName;
    UINT4          u4StartTime;   
    UINT4          u4EndTime;   
    UINT4          u4Scheduled;  /* FWL_ENABLE -or- FWL_DISABLE */ 
    UINT2          u2SeqNum;
    UINT1          u1Action;
    UINT1          u1RowStatus;                          
    UINT1          u1LogTrigger;  
    UINT1          u1FragAction;  
    UINT1          u1AclType;
    UINT1          u1Pad;   
} tAclInfo;


/*****************************************************************************/
/*   Structure related type definitions                                      */
/*****************************************************************************/

typedef tAclInfo                 tInFilterInfo;
typedef tInFilterInfo            tOutFilterInfo;
typedef tInFilterInfo            tInIpv6FilterInfo;
typedef tOutFilterInfo           tOutIpv6FilterInfo;


/*****************************************************************************/
/*   Structure for Stateful Firewall Filter                                  */
/*****************************************************************************/
typedef struct StatefulFilterInfo
{
   tTMO_SLL_NODE        StatefulFilterInfo;
   tTmrAppTimer         *pStateAppTimer;
   tFwlIpAddr           SrcAddr;
   tFwlIpAddr           DestAddr;
   UINT4                u4FilterNo;
   UINT4                u4CurTime; 
   UINT2                u2SrcPort ;
   UINT2                u2DestPort;
   UINT1                u1Protocol;
   UINT1                u1State;
   UINT1                u1LogTrigger;
   UINT1                u1Pad;
} tStatefulFilterInfo;

typedef struct FwlGlobalInfo {
    tFwlIpAddr  TranslatedLocIpAddr;
    UINT2  u2TranslatedLocPort;
    UINT2  u2Reserved;
}tFwlGlobalInfo ;

/*********************** Added for DMZ support ******************************/

typedef struct FwlDmzInfo {
   tTMO_SLL_NODE     nextFwlDmzInfo;
   UINT4  u4IpIndex;   /* IP address of DMZ host*/
   UINT1  u1RowStatus; /* Row status of DMZ table*/
   UINT1 au1Pad[3];    /* Padding bytes */
}tFwlDmzInfo;

/**********************Added for IPv6 DMZ support ***************************/

typedef struct FwlIPv6DmzInfo {
    tTMO_SLL_NODE     nextFwlIPv6DmzInfo;
    tIp6Addr dmzIpv6Index;   /* IPv6 address of DMZ host*/
    UINT1  u1DmzAddressType; /*Address Type of the DMZ Host*/
    UINT1  u1RowStatus; /* Row status of DMZ table*/
    UINT1 au1Pad[2];    /* Padding bytes */
}tFwlIPv6DmzInfo;

/*****************************************************************************/
/*             Structure for URL Filtering                                   */
/*****************************************************************************/

typedef struct FwlUrlFilterInfo
{
   tTMO_SLL_NODE     nextFwlUrlFilterInfo;
   UINT1             au1UrlStr[FWL_MAX_URL_FILTER_STRING_LEN];
   UINT4             u4UrlHitCount;
   UINT1             u1RowStatus;
   UINT1             u1Pad[3];
} tFwlUrlFilterInfo;

/* New structures defined as part of FireWaLL REDesign */

/* 
 * The following structures are used for limiting concurrent
 * sessions based on limiting factor 
 */

/*****************************************************************************/
/*       Structure for storing Outbound Session List                         */
/*****************************************************************************/

typedef struct _OutSessionsInfo 
{
   tTMO_SLL  OutSessionList;         /* List of Outbound Sessions made */ 
   UINT2     u2OutTotalSessionCount; /* Total Count of OutSessions existing */
   UINT2     u2OutSessionLimit;      /* Configured Out-bound Sessions Limit */
} tOutSessionsInfo;

/*****************************************************************************/
/*       Structure for storing Outbound Session Node                         */
/*****************************************************************************/

typedef struct _OutUserSessionNode 
{
   tTMO_SLL_NODE  NextOutUserSessionNode;     
   tFwlIpAddr     LocalIP;         /* LAN User IP */
   UINT2          u2OutSessionCount; /* Count of outbound sessions */
   UINT2          u2Reserved;
} tOutUserSessionNode;



/*****************************************************************************/
/*       Structure for storing Inbound Session List                          */
/*****************************************************************************/

typedef struct _InSessionsInfo 
{
    
   tTMO_SLL  InSessionList;          /* List of Inbound Sessions made */
   UINT2     u2InTotalSessionCount;  /* Total Count of InSessions existing */
   UINT2     u2InTotalSessionLimit;  /* Configured In InSessions Limit */
} tInSessionsInfo;


/*****************************************************************************/
/*       Structure for storing Inbound Session Node                          */
/*****************************************************************************/

typedef struct _InUserSessionNode 
{
   tTMO_SLL_NODE   NextInUserSessionNode;     
   tFwlIpAddr      LocalIP;
   UINT2           u2ServicePort;
   UINT2           u2InSessionCount;
   UINT2           u2InSessionLimit;
   UINT1           u1Protocol;
   UINT1           u1Reserved;

} tInUserSessionNode;


/*****************************************************************************/
/*       Structure for storing Stateful Session Table                        */
/*****************************************************************************/

typedef struct _StatefulSessionInfo
{
     tTMO_HASH_TABLE   *pStateTable;     
     tOutSessionsInfo   OutSessionsInfo; 
     tInSessionsInfo    InSessionsInfo;
     UINT2              u2InTotalIcmpCnt; 
     UINT2              u2InTotalUdpCnt;
} tStatefulSessionInfo;


/* Application CallBack Structure */

typedef union FwlUtilCallBackEntry
{
    INT4 (*pFwlCustIfCheck) PROTO ((VOID));

}unFwlUtilCallBackEntry;

/* Firewall Log message & classification */ 
typedef struct FwlLog 
{
    UINT1     au1FwlAttack[FWL_MAX_ATTACK_STR_SIZE]; /* Msg describing attack*/
    UINT1     u1Classification; /* Attack classification*/
    UINT1     u1Priority;  /* Priority level of attack */
}tFwlLogInfo;


/* Black List & White List Ip Address Structure */
typedef struct FwlBlackListEntry 
{
    tRBNodeEmbd         FwlBlackListNode;
    tFwlIpAddr          FwlBlackListIpAddr;
    COUNTER32           u4BlkListHitCount; /* No. of times this Entry is matched
                                            * while processing the packet */
    INT4  i4EntryType; /* Holds value static or dynamic  
                                      * 1. Static - Entry created by 
                                      * administrator
                                      * 2. Dynamic - Entry Created through 
                                      * snort module dynamically */
    INT4                i4RowStatus;
    UINT2               u2PrefixLen;
    UINT1               au1Pad[2];
}tFwlBlackListEntry;

typedef struct FwlWhiteListEntry 
{
    tRBNodeEmbd         FwlWhiteListNode;
    tFwlIpAddr          FwlWhiteListIpAddr;
    COUNTER32           u4WhiteListHitCount; /* No. of times this Entry is 
                                              * matched while processing the
                                              * packet */
    INT4                i4RowStatus;
    UINT2               u2PrefixLen;
    UINT1               au1Pad[2];
}tFwlWhiteListEntry;
#endif /* End for _FWLTDFS_H  */

/*****************************************************************************/
/*                End of file fwltdfs.h                                      */
/*****************************************************************************/
