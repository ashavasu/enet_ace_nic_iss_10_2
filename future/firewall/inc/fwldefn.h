/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwldefn.h,v 1.13 2014/03/16 11:41:01 siva Exp $
 *
 * Description:This file contains all constants related        
 *             to implemetation of AccessList     
 *
 *******************************************************************/
#ifndef _FWLDEFN_H
#define _FWLDEFN_H

/*****************************************************************************/
/*     The maximum length for the rule,Filter name and Pattern               */
/*****************************************************************************/
#define   FWL_MAX_USER_NAME_LEN       20
#define   FWL_MAX_PATTERN_LEN         12
#define   FWL_MAX_TRAP_LEN           252
#define   FWL_MAX_DMZ_ENTRIES         MAX_FWL_DMZ_INFO
#define   FWL_MAX_IPV6_DMZ_ENTRIES    MAX_FWL_DMZ_INFO

/*****************************************************************************/
/*   To initialise the Variables                                             */ 
/*****************************************************************************/

#define   FWL_INIT                     0

/*****************************************************************************/
/*            Constants used in comparisions  functions                      */
/*****************************************************************************/

#define   FWL_EQUAL                    0
#define   FWL_LESS                     1
#define   FWL_GREATER                  2
#define   FWL_NOT_EQUAL                3

/*****************************************************************************/
/*            Constansts related to different flags used                     */
/*****************************************************************************/

#define   FWL_FILTER_MATCH             1
#define   FWL_RULE_MATCH               3

/*****************************************************************************/
/*            The values for Flags and true values                           */
/*****************************************************************************/

#define   FWL_FLAG_NOT_SET             0
#define   FWL_FLAG_SET                 1
#define   FWL_SET                      1
#define   FWL_NOT_SET                  0
#define   FWL_TRUE                     1
#define   FWL_FALSE                    0

/*****************************************************************************/
/*            The values for Match and Not match                             */
/*****************************************************************************/

#define   FWL_MATCH                    1
#define   FWL_NOT_MATCH                2
#define   FWL_STATEFUL_ENTRY_MATCH     3
#define   FWL_DELETE_STATE_ENTRY       4
#define   FWL_FRAG_PERMIT              5
#define   FWL_NEW_SESSION              6

/*****************************************************************************/
/*            The values for ICMP Allow and Discard Packet                   */
/*****************************************************************************/
#define FWL_OUT_ICMP_ALLOW    10
#define FWL_IN_ICMP_ALLOW     11
#define FWL_OUT_ICMP_DISCARD  12
#define FWL_IN_ICMP_DISCARD   13

/*****************************************************************************/
/*            The values for IN_FILTER and OUT_FILTER                        */
/*****************************************************************************/

#define FWL_IN_FILTER        1
#define FWL_OUT_FILTER       2

/******************************************************************************/
/*             IANA assigned Internet Protocol Numbers                        */
/******************************************************************************/

#define FWL_ICMP           1   /*  Internet Control Message Protocol       */
#define FWL_IGMP           2   /*  Internet Group Management Protocol      */
#define FWL_GGP            3   /*  Gateway -To- Gateway Protocol           */
#define FWL_IP             4   /*  Internet Protocol                       */
#define FWL_TCP            6   /*  Transmission Control Protocol           */
#define FWL_EGP            8   /*  Exterior Gateway Protocol               */
#define FWL_IGP            9   /*  Interior Gateway Protocol               */
#define FWL_NVP           11   /*  Network Voice Protocol                  */
#define FWL_UDP           17   /*  User Datagram protocol                  */
#define FWL_IRTP          28   /*  Internet Reliable Transaction Protocol  */
#define FWL_IDPR          35   /*  Inter-Domain Policy Routing Protocol    */
#define FWL_RSVP          46   /*  Reservation Protocol                    */
#define FWL_GRE_PPTP      47   /*  PPTP with GRE header                    */
#define FWL_MHRP          48   /*  Mobile Host Routing Protocol            */
#define FWL_ICMPV6        58   /*  Internet Control Message Protocol v6    */
#define FWL_IGRP          88   /*  Cisco specific                          */
#define FWL_OSPFIGP       89   /*  Open Shortest Path First                */
#define FWL_HTTP          80   /*  HTTP                                    */
#define FWL_PIM          103   /*  PIM                                     */
#define FWL_ANY          255   /*  Any case                                */
/* Port Numbers */
#define FWL_SSH_PORT      22   /*  SSH                                     */
#define FWL_TELNET_PORT   23   /*  TELNET                                  */
#define FWL_PPTP_PORT     1723 /*  PPTP Control Connection                 */

#define FWL_MIN_VALID_PROTO_NUM    1    /* Min Valid protocol number */
#define FWL_MAX_VALID_PROTO_NUM    254  /* Max Valid protocol number */

/*****************************************************************************/
/*   Typical Header values for IP, TCP and UDP                               */
/*****************************************************************************/

#define FWL_IP_HEADER_LEN        20  /* Minimum Header length of IP packet  */
#define FWL_TCP_HEADER_LEN       20  /* Minimum Header length of TCP packet */
#define FWL_UDP_HEADER_LEN        8  /* Minimum Header length of UDP packet */
#define FWL_ICMP_HEADER_LEN       8  /* Minimum Header length of ICMP packet     */
#define FWL_HEAD_LEN_FACTOR       4  /* Factor to multiply to get exact len */
#define FWL_MAX_IP_PKT_LEN    65535  /* Maximum IP packet length */
#define FWL_IP_VERSION_4       4  /* Ip Header Version Field valid value. */ 

#define FWL_IP6_HEADER_LEN        40 /* IPv6 Header length of a packet excluding nH */
#define FWL_IP_VERSION_6          6  /* Ip Header Version Field valid value. */
#define FWL_IP6_NH_OFFSET         6  /* offset for NH field in IP6 Hdr */
#define FWL_HOP_BY_HOP_HDR        0
#define FWL_DEST_OPT_HDR          60
#define FWL_ROUTING_HDR           43
#define FWL_NO_NXT_HDR            59
#define FWL_FRAGMENT_HDR          44
#define FWL_AUTHENTICATION_HDR    51
#define FWL_ENCAP_SEC_PAY_HDR     50
#define FWL_FRAGMENT_HEADER_LEN   8 
#define FWL_IP6_MF_BIT_MASK       0x00001 
#define FWL_MAX_BYTE_ADDR_INDEX   4
#define FWL_PREFIX_LEN_MULT_FACTOR 10

/*****************************************************************************/
/*      Offset for IP Packet Header fields to get from the Buffer            */
/*****************************************************************************/

#define  FWL_IP_HLEN_OFFSET        0
#define  FWL_IP_TOS_OFFSET         1
#define  FWL_IP_TOTAL_LEN_OFFSET   2
#define  FWL_IP_ID_OFFSET          4
#define  FWL_IP_FRAGMENT_OFFSET    6
#define  FWL_IP_TTL_OFFSET         8
#define  FWL_IP_PROTO_OFFSET       9
#define  FWL_IP_SRC_ADDR_OFFSET   12
#define  FWL_IP_DEST_ADDR_OFFSET  16
#define  FWL_IP_OPTION_OFFSET     20
#define  FWL_OPTION_LEN_OFFSET    21
#define  FWL_URGENT_PTR_OFFSET    18
#define  FWL_SEQ_OFFSET           4
#define FWL_TCP_HDRLEN_OFFSET     12
#define FWL_OPTION_PTR_OFFSET     22
#define FWL_TIME_STAMP_OPTION     0x44
#define  FWL_TCP_SEQ_OFFSET        4
#define  FWL_TCP_ACK_OFFSET        8
#define  FWL_TCP_DATA_OFFSET      12
#define  FWL_TCP_WINDOW_OFFSET    14
#define  FWL_TCP_FLAG_BITS_COUNT   7

#define  FWL_IP6_FIRST_NH_OFFSET   6
/*****************************************************************************/
/*      Offset for IPv6 Packet Header fields to get from the Buffer            */
/*****************************************************************************/

#define FWL_IP6_SRC_ADDR_OFFSET   8
#define FWL_IP6_DST_ADDR_OFFSET   24
#define FWL_IP6_PAYLOAD_LEN_OFFSET  4
#define FWL_IP6_HOP_LIMIT_OFFSET   7

/*****************************************************************************/
/*      Offset for TCP Packet Header fields to get from the Buffer           */
/*****************************************************************************/

#define FWL_TCP_CODE_BIT_OFFSET   13  /* For Ack and Rst bits offset       */

/*****************************************************************************/
/*      Offset for ICMP Packet Header fields to get from the buffer          */
/*****************************************************************************/

#define FWL_ICMP_CODE_OFFSET      1 
#define FWL_ICMP_CKSUM_OFFSET     2 
#define FWL_ICMP_ID_OFFSET        4
#define FWL_ICMP_SEQNO_OFFSET     6
#define FWL_ICMP_PAYLOAD_OFFSET   8

/*****************************************************************************/
/*      ICMP Code and Type values for the Error Message generation           */
/*****************************************************************************/

#define FWL_ICMP_ERROR_TYPE     3  /* Destination Unreachable */
#define FWL_ICMP_ERROR_CODE    13  /* Communication Administratively 
                                    Prohibited */
                                
/*****************************************************************************/
/*      Constants for the status of the Filtering service.                   */
/*****************************************************************************/

#define  FWL_ENABLE                                    1
#define  FWL_DISABLE                                   2
#define  FWL_FILTERING_ENABLED                         1
#define  FWL_FILTERING_DISABLED                        2

/*****************************************************************************/
/*      Constants used for the Filtering Action.                             */
/*****************************************************************************/
#define  FWL_NONE                                      0
#define  FWL_PERMIT                                    1
#define  FWL_DENY                                      2
#define  FWL_COPY_TO_CPU                               3
#define  FWL_SWITCH                                    4

/*****************************************************************************/
/*      Constants used for the Filter Direction.                             */
/*****************************************************************************/

#define  FWL_DIRECTION_IN                              1
#define  FWL_DIRECTION_OUT                             2

/*****************************************************************************/
/*      Constants used for the Scheduling Action                             */
/*****************************************************************************/

#define  FWL_ACL_SCHEDULED                             1
#define  FWL_ACL_ALWAYS                                2

/*****************************************************************************/
/*      Constants for the IP fragments.                                      */
/*****************************************************************************/

#define  FWL_TINY_FRAGMENT                             1
#define  FWL_LARGE_FRAGMENT                            2
#define  FWL_ANY_FRAGMENT                              3
#define  FWL_NO_FRAGMENT                               4
#define  FWL_DEFAULT_FRAGMENT_SIZE                     30000
#define  FWL_MIN_FRAGMENT_SIZE                         1
#define  FWL_MAX_FRAGMENT_SIZE                         65500
#define  FWL_FRAG_NOT_SET                              0

/*****************************************************************************/
/*      Constants related to the IP options.                                 */
/*****************************************************************************/

#define  FWL_SOURCE_ROUTE_OPTION                       1
#define  FWL_RECORD_ROUTE_OPTION                       2
#define  FWL_TIMESTAMP_OPTION                          3
#define  FWL_ANY_OPTION                                4
#define  FWL_NO_OPTION                                 5
#define  FWL_TRACE_ROUTE_OPTION                   6
 
/*****************************************************************************/
/*      Constants related to TCP Ack and Rst bits.                           */
/*****************************************************************************/

#define  FWL_TCP_ACK_ESTABLISH                         1
#define  FWL_TCP_ACK_NOT_ESTABLISH                     2
#define  FWL_TCP_ACK_ANY                               3

#define  FWL_TCP_RST_SET                               1
#define  FWL_TCP_RST_NOT_SET                           2
#define  FWL_TCP_RST_ANY                               3
#define  FWL_URG_SET                                   1

/*****************************************************************************/
/*           ICMP Type and Code Values                                       */
/*****************************************************************************/

#define  FWL_NO_ICMP_TYPE                            255
#define  FWL_NO_ICMP_CODE                            255

/*****************************************************************************/
/*          ICMPv6 Message Type Bit Values                                   */
/*****************************************************************************/
#define FWL_ICMPV6_NO_INSPECT                    0x00000000
#define FWL_ICMPV6_INSPECT_ALL                   0x000003FF
#define FWL_ICMPV6_DESTINATION_UNREACHABLE       0x00000001
#define FWL_ICMPV6_TIME_EXCEEDED                 0x00000002
#define FWL_ICMPV6_PARAMETER_PROBLEM             0x00000004
#define FWL_ICMPV6_ECHO_REQUEST                  0x00000008
#define FWL_ICMPV6_ECHO_REPLY                    0x00000010
#define FWL_ICMPV6_REDIRECT                      0x00000020
#define FWL_ICMPV6_INFORMATION_REQUEST           0x00000040
#define FWL_ICMPV6_INFORMATION_REPLY             0x00000080

/****************************************************************************/
/* Decimal value for the above bit masks are defined below                  */
/****************************************************************************/

#define FWL_ICMPV6_TYPE_DESTINATION_UNREACHABLE           1
#define FWL_ICMPV6_TYPE_TIME_EXCEEDED                     3
#define FWL_ICMPV6_TYPE_PARAMETER_PROBLEM                 4
#define FWL_ICMPV6_TYPE_ECHO_REQUEST                    128
#define FWL_ICMPV6_TYPE_ECHO_REPLY                      129
#define FWL_ICMPV6_TYPE_REDIRECT                        137
#define FWL_ICMPV6_TYPE_INFORMATION_REQUEST             139
#define FWL_ICMPV6_TYPE_INFORMATION_REPLY               140
/*           Masks for the IP options                                        */
/*****************************************************************************/

#define  FWL_LOOSE_ROUTE_MASK                       0x03
#define  FWL_STRICT_ROUTE_MASK                      0x09
#define  FWL_REC_ROUTE_MASK                         0x07
#define  FWL_TIMESTAMP_MASK                         0x44
#define  FWL_OPTION_MASK                            0x7f

/*****************************************************************************/
/*           Fragment related masks                                          */
/*****************************************************************************/

#define  FWL_FRAG_OFFSET_ZERO                     0x0000
#define  FWL_FRAG_OFFSET_MASK                     0x0001
#define  FWL_MF_BIT_MASK                          0x2000
#define  FWL_DF_BIT_MASK                          0x4000
#define  FWL_FRAG_FLAG_BIT                        0xE000
#define  FWL_FRAG_OFFSET_BIT                      0x1fff
#define  FWL_FRAG_OFFSET_TINY                     3
#define  FWL_IP_HEAD_LEN_MASK                     0x0f
#define FWL_IP6_FRAG_OFFSET_BIT                   0xfff8

/*****************************************************************************/
/*           IP Tos mask                                                     */
/*****************************************************************************/

#define  FWL_IP_TOS_MASK                            0x1c

/*****************************************************************************/
/*           IPv6 mask values                                                */
/*****************************************************************************/

#define  FWL_IPv6_DSCP_MASK                         0x0ff00000
#define  FWL_IPv6_FLOWID_MASK                       0x000fffff

/*****************************************************************************/
/*           TCP/UDP port and code bits related masks                        */
/*****************************************************************************/

#define  FWL_TCP_FIN_MASK                           0x01
#define  FWL_TCP_SYN_MASK                           0x02
#define  FWL_TCP_RST_MASK                           0x04
#define  FWL_TCP_ACK_MASK                           0x10

/****************************************************************************/
/*   MAX Port and Min Port                                                  */
/****************************************************************************/

#define  FWL_MAX_PORT_VALUE                         65535
#define  FWL_MIN_PORT_VALUE                         1
#define  FWL_MAX_PORT                               65535
#define  FWL_MIN_PORT                               1

/****************************************************************************/
/*         Values for ICMP Control Switch                                   */
/****************************************************************************/

#define  FWL_ICMP_GENERATE                             1
#define  FWL_ICMP_SUPPRESS                             2
#define  FWL_ICMPv6_GENERATE                           3

/****************************************************************************/
/*                 Values for ACL Type                                      */
/****************************************************************************/

#define  ACL_FILTER                                    1
#define  ACL_RULE                                      2


/****************************************************************************/
/*          Constants for the filter combined flag in the Rule Structure    */
/****************************************************************************/

#define  FWL_FILTER_AND                                1
#define  FWL_FILTER_OR                                 2

/****************************************************************************/
/*  Constants used to specify whether the interface is internal/external    */
/****************************************************************************/

#define  FWL_INTERNAL_IF                               1
#define  FWL_EXTERNAL_IF                               2

/****************************************************************************/
/*   constants used for logging                                              */
/****************************************************************************/
#define  FWL_MAX_LOG_ENTRIES                          MAX_FWL_LOG_BUFFER
#define  MAX_HOST_NAME_LEN                            25
#define  SYSLOG_PORT                                  514
#define  FWL_IP_PKT_TOO_BIG                           255 
#define  FWL_LOGFILE_LIMIT_SIZE            (1024*1024)   /* Total Size 1 MB */
#define  FWL_PARTIAL_LOG_SIZE              (1024)    /* 1 KB */
#define  FWL_MIN_LOG_SIZE                   FWL_PARTIAL_LOG_SIZE
#define  FWL_MIN_LOG_SIZE_THRESH            1             /*  1%  */
#define  FWL_MAX_LOG_SIZE_THRESH            99            /*  99% */
#define  FWL_DEF_LOG_SIZE_THRESH            70 /* 70% of max size */ 
#define  FWL_MAX_LOG_BUFFER                FWL_MAX_LOG_ENTRIES * \
                                                 FWL_MAX_LOG_BUF_SIZE 
#define FWL_MAX_FILE_NAME_LEN            128
#define FWL_MAX_FILE_READ_LEN            81
#define FWL_TRAP_TIME_STR_LEN            24

#define IDS_FILE_NAME_EXT                "ids.alert"
/****************************************************************************/
/*   constants related to Timer                                             */
/****************************************************************************/

#define  FWL_MAX_TIME_LEN                             21

/****************************************************************************/
/*   constants used for generating Traps                                    */
/****************************************************************************/

#define  FWL_IFACE_ALLOC_FAILURE                       1
#define  FWL_STATIC_FILTER_ALLOC_FAILURE               2
#define  FWL_STATIC_RULE_ALLOC_FAILURE                 3
#define  FWL_ACL_ALLOC_FAILURE                         4
#define  FWL_LOG_TABLE_FAILURE                         5

#define  FWL_MEM_FAILURE_TRAP                          1
#define  FWL_ATTACK_SUM_TRAP                           2
#define  FWL_TRAP_EVENT                                3

#define  FWL_TRAP_SENT                                 1
#define  FWL_TRAP_NOT_SENT                             2

#define  FWL_SNMP_TRAP_SRC_PORT                     0xa1
#define  FWL_SNMP_TRAP_DEST_PORT                    0xa2

#define FWL_TRAP_FILE_SIZE_EXCEEDED                    1
#define FWL_TRAP_LOGSIZE_THRESHOLD_HIT                 2

/****************************************************************************/
/*   constants used for validating IP Address                               */
/****************************************************************************/

#define FWL_BCAST_ADDR                               255
#define FWL_MCAST_ADDR                               224
#define FWL_ZERO_NETW_ADDR                             0
#define FWL_ZERO_ADDR                                100
#define FWL_LOOPBACK_ADDR                            127
#define FWL_UCAST_ADDR                                 1
#define FWL_INVALID_ADDR                              -1
#define FWL_CLASS_NETADDR                             10  
#define FWL_CLASS_BCASTADDR                          200  
#define FWL_CLASSE_ADDR                              240

/****************************************************************************/
/*   constants used for Stateful Firewall Implementation                    */
/****************************************************************************/
#define FWL_HEADER_LEN_MASK    0x0f
#define FWL_TCP_LENGTH_OFFSET  12
#define FWL_HEADER_LEN         1
#define FWL_FTP_PORT_LEN       5
#define FWL_STRCMP_SUCCESS     0
#define FWL_NEW                1
#define FWL_EST                2
#define FWL_RELATED            3
#define FWL_IPADDR_LEN_MAX     40
#define FWL_FTP_PORT_LEN_MAX   10
#define FWL_PORT_LEN_MAX       10
#define FWL_INDEFINITE         1
#define FWL_FTP_PORT           1
#define FWL_FTP_PASV           2 
#define FWL_FTP_EPRT           3
#define FWL_FTP_EPSV           4
#define EPSV_REPLY_CODE        "229"
#define FWL_STRING_END         '\0'
#define FWL_FTP_IP_COMMA_COUNT  4
#define FWL_FTP_IP_PIPE_COUNT   2
#define FWL_FTP_PORT_PIPE_COUNT   3
#define FWL_FTP_PORT_DIV_FACTOR 256
#define FWL_DATA_PORT           20
#define FWL_AUTH_PORT           113
#define FWL_ETH                 6
#define FWL_ASYNC               84
#define FWL_MP                  108
#define FWL_HDLC                118
#define FWL_AAL5                49
#define FWL_ATMVC               134
#define FWL_IPOA                114
#define FWL_SYNC_ASYNC          254    /* not standard */
#define FWL_L2TP                255    /* not standard */
#define FWL_MPLS                166
#define FWL_RFC1483             159
#define FWL_IP6ADDR_LEN         16

/* Macros for State-Table Entry States */
#define FWL_NEW                1
#define FWL_EST                2
#define FWL_RELATED            3

/* Macros for TCP Connection States */
#define FWL_TCP_STATE_LISTEN             10
#define FWL_TCP_STATE_SYN_SENT           11
#define FWL_TCP_STATE_SYN_RCVD           12
#define FWL_TCP_STATE_EST                13
#define FWL_TCP_STATE_FIN_WAIT1          14
#define FWL_TCP_STATE_FIN_WAIT2          15
#define FWL_TCP_STATE_CLOSING            16
#define FWL_TCP_STATE_TIME_WAIT          17
#define FWL_TCP_STATE_CLOSE_WAIT         18
#define FWL_TCP_STATE_LAST_ACK           19
#define FWL_TCP_STATE_CLOSED             20


#define FWL_ATOI                       ATOI
#define FWL_NTOHL                      OSIX_NTOHL
#define FWL_INET_ADDR(ipaddr)          FWL_NTOHL( INET_ADDR(ipaddr) )

/* Constants for Attack Prevention */

#define FWL_TCP_URG_MASK               0x20
#define FWL_TCP_PUSH_MASK              0x08
#define FWL_IRDP_ADVERTISEMENT         9
#define FWL_ICMP_TYPE_TIMESTAMP_REQ    13
#define FWL_ICMP_TYPE_TIMESTAMP_REPLY  14
#define FWL_ICMP_TYPE_MASK_REQ         17
#define FWL_ICMP_TYPE_MASK_REPLY       18

/* IP Options. */
#define   FWL_MAX_IP_OPTIONS_LEN      60
/* Timestamp belongs to class 2 (debugging). All others are 0 (control). */
#define   FWL_IP_OPT_EOL              0
#define   FWL_IP_OPT_NOP              1
#define   FWL_IP_OPT_SECURITY         130
#define   FWL_IP_OPT_LSROUTE          131
#define   FWL_IP_OPT_TSTAMP           68   /* class 2 */
#define   FWL_IP_OPT_ESECURITY        133
#define   FWL_IP_OPT_RROUTE           7
#define   FWL_IP_OPT_STREAM_IDENT     136
#define   FWL_IP_OPT_SSROUTE          137
#define   FWL_IP_OPT_ROUTERALERT      148
#define   FWL_IP_OPT_TROUTE           82 

/* Code bits for IP_OPTION TYPES */
#define FWL_IP_OPT_EOL_BIT              1
#define FWL_IP_OPT_NOP_BIT              2
#define FWL_IP_OPT_SECURITY_BIT         4
#define FWL_IP_OPT_LSROUTE_BIT          8
#define FWL_IP_OPT_TSTAMP_BIT           16
#define FWL_IP_OPT_ESECURITY_BIT        32
#define FWL_IP_OPT_RROUTE_BIT           64
#define FWL_IP_OPT_STREAM_IDENT_BIT     128
#define FWL_IP_OPT_SSROUTE_BIT          256
#define FWL_IP_OPT_ROUTERALERT_BIT      512
#define FWL_IP_OPT_TROUTE_BIT  1024

/* Constants for Remote Management */

#define FWL_FTP_CTRL_PORT              21
#define FWL_TELNET_PORT                23
#define FWL_SMTP_PORT                  25
#define FWL_DNS_PORT                   53
#define FWL_HTTP_PORT                  80
#define FWL_POP3_PORT                  110
#define FWL_IMAP_PORT                  143
#define FWL_HTTPS_PORT                 443  /*  HTTPS  */
#define FWL_SNMP_PORT                  161
#define FWL_SNMP_TRAP_PORT             162
#define FWL_SNTP_PORT                  123 
#define FWL_DHCP_CLIENT_PORT     68

/* For RIP port */
#define FWL_UDP_RIP_PORT               520
#define FWL_UDP_RIPng_PORT             521

/* Following macros defined for the support of  allowing DHCP client/server pkts */
#define FWL_DHCP_SERVER_PORT  67
#define FWL_DHCP_CLIENT_PORT  68

/* Following macros defined for the support of  allowing DHCPv6 client/server pkts */
#define FWL_DHCPv6_CLIENT_PORT  546
#define FWL_DHCPv6_SERVER_PORT  547

/* Following macros are defined for allowing wan side ping */
#define FWL_ICMP_TYPE_ECHO_REQUEST 8
#define FWL_ICMP_TYPE_ECHO_REPLY 0
#define FWL_ICMP_INSPECT_OPTION     8

#define FWL_TFTP_PORT            69

/* Following macros are defined for allowing wan side ping6 */
#define FWL_ICMPv6_TYPE_ECHO_REQUEST 128
#define FWL_ICMPv6_TYPE_ECHO_REPLY 129

/* Neighbour Discovery Message Types */

#define  ROUTER_SOLICITATION      133
#define  ROUTER_ADVERTISEMENT     134
#define  NEIGHBOUR_SOLICITATION   135
#define  NEIGHBOUR_ADVERTISEMENT  136
#define  ROUTE_REDIRECT           137

/* Other defined macros */
#define FWL_MIN_ICMP_HEADER_SIZE      8
#define FWL_MIN_UDP_HEADER_SIZE       8
#define FWL_MIN_TCP_HEADER_SIZE       20
#define FWL_UDP_LENGTH_OFFSET         4 


/* Maximum URL-String size allowed in Firewall CLI URL FILTER ADD/DEL COMMAND */
#define FWL_MAX_URL_FILTER_ENTRIES      MAX_FWL_URL_FILTERS
/* Macros for FwlTcpValidateSeqAck Function. */
#define FWL_VALIDATE_SEQ_ACK            1
#define FWL_UPDATE_SEQ_ACK              2

/* For firewall email alert */
#define FWL_EMAIL_FOR_TCP_FLAG         1
#define FWL_EMAIL_FOR_DOS_ATTACK       2
#define FWL_EMAIL_FOR_WEB_ACCESS       3
#define FWL_EMAIL_FOR_TCP_FLAG_MASK    0x01
#define FWL_EMAIL_FOR_DOS_ATTACK_MASK  0x02
#define FWL_EMAIL_FOR_WEB_ACCESS_MASK  0x04
#define FWL_EMAIL_FOR_TCP_DOS_WEB_MASK 0x07
#define FWL_EMAIL_DISABLE_ALL_MASK     0x00 

/* For configuring ACL Scheduler */
/* setting days in week*/
#define FWL_ACL_SCHED_SUNDAY         1
#define FWL_ACL_SCHED_MONDAY         2
#define FWL_ACL_SCHED_TUESDAY        3
#define FWL_ACL_SCHED_WEDNESDAY      4
#define FWL_ACL_SCHED_THURSDAY       5
#define FWL_ACL_SCHED_FRIDAY         6
#define FWL_ACL_SCHED_SATURDAY       7
#define FWL_ACL_SCHED_EVERYDAY       8

#define FWL_ACL_SCHED_MONDAY_MASK    0x02
#define FWL_ACL_SCHED_TUESDAY_MASK   0x04
#define FWL_ACL_SCHED_WEDNESDAY_MASK 0x08
#define FWL_ACL_SCHED_THURSDAY_MASK  0x10
#define FWL_ACL_SCHED_FRIDAY_MASK    0x20
#define FWL_ACL_SCHED_SATURDAY_MASK  0x40
#define FWL_ACL_SCHED_EVERYDAY_MASK  0x80

#define FWL_DOS_SESSION_PER_MINUTE   1
#define FWL_DOS_INCOMPLETE           2
#define FWL_DOS_PER_NODE             3


#define FWL_TCP_INIT_THRESH_DEL_OLD  1
#define FWL_TCP_INIT_THRESH_DROP     2


#define FWL_DENY_MIN_VALUE           1
#define FWL_DENY_MAX_VALUE           256

/* Macros related to ACL Scheduler */
#define  FWL_TIME_NOT_SET            (UINT1)-1
/* Time related definations for scheduling */
#define  FWL_SECONDS_IN_MINUTE       60  
#define  FWL_SECONDS_IN_HOUR         3600 
#define  FWL_SECONDS_IN_DAY          86400 
#define  FWL_SECONDS_IN_WEEK         604800 


/* Compare Func Result Values */
#define FWL_LESS                     1
#define FWL_GREATER                  2
#define FWL_WITHIN                   3

#define  FWL_ACTIVE                  1
#define  FWL_NOT_IN_SERVICE          2


/* Defines for Updating and Adding partial Links */   
#define FWL_ADD_PARTIAL              1
#define FWL_UPDATE_PARTIAL           2

/* Defines for Updating the Concurrent Session Info for TCP protocol. */
#define FWL_UPDATE_TCP               1
#define FWL_DONT_UPDATE_TCP          2

#define FWL_MAX_PORT_NAME_LENGTH    24

/*Defines for Firewall Logging */
#define FWL_CMD_DEF_BUF_SIZE 256
#define FWL_CMD_EXEC_MSG "Command Executed"

#define FWL_IPV4_ADDR_LEN         16
#define FWL_IPV6_ADDR_LEN         40

#define FWL_BLACKLIST_STATIC        0
#define FWL_BLACKLIST_DYNAMIC       1

/* This macro represents the starting value of the priorities for the default
 * rules. It is set in such a way that the maximum value of the default rules'
 * priorites will be the maximum possible value,i.e. 65535, always.
 * So whenever any new default rule is to be added, the value of this macro has
 * to be decremented accordingly.
 */
/* As per customer requirements, default ACLs priority is changed to 7000 */
#define FWL_DEF_RULE_PRIORITY_MIN   7000
#define FWL_RULE_FIRST_FILTER_INDEX 0

/* Maximum size of firewall message string array */
#define FWL_MAX_ATTACK_STR_SIZE  102
/* Maximum size of firewall attack classification string array */
#define FWL_MAX_CLASS_TYPE   13


#ifdef FLOWMGR_WANTED

#define FWL_FL_UTIL_UPDATE_FLOWTYPE(pBuf) \
FlUtilUpdateFlowTypeFlowData(pBuf, FLOW_CTRL_FLOW)

#define FWL_FL_TASKLOCK() FlMainTaskLock()
#define FWL_FL_TASKUNLOCK() FlMainTaskUnlock()
#define FWL_UTIL_UPD_STATEFUL_TCPSTATES(u4LocIp, u4RemIp, u2SrcPort, u2RemPort,u1Proto, pStateNode)   FwlUtilUpdStatefulTcpStates(u4LocIp, u4RemIp, u2SrcPort, u2RemPort,u1Proto, pStateNode) 

#define FL_FWL_UPDATE_FLOWDATA_TCPSTATES(pBuf, pStateNode) \
FlFwlUpdateFlowDataTcpStates(pBuf, pStateNode)

#define FWL_FL_UTIL_GET_TIMESTAMP(u4LocIP, u4RemIP, u2SrcPort, u2DestPort, u1Proto)   FlUtilGetTimestamp(u4LocIP, u4RemIP, u2SrcPort, u2DestPort, u1Proto)

#define FL_FWL_DEL_HW_ENTRIES(u4LocalIP,u4RemoteIP,u2SrcPort,u2DestPort,u1Proto) FlFwlDelHwEntries(u4LocalIP,u4RemoteIP,u2SrcPort,u2DestPort,u1Proto)

#define FL_FWL_DEL_IFINDEX_HW_ENTRIES(u4IfIndex) FlIpHandleDelIfIndexEntries((UINT2) u4IfIndex)
#else
    
#define FWL_FL_UTIL_UPDATE_FLOWTYPE(pBuf)   
#define FWL_FL_TASKLOCK()  
#define FWL_FL_TASKUNLOCK()   
#define FWL_UTIL_UPD_STATEFUL_TCPSTATES(u4LocIp, u4RemIp, u2SrcPort, u2RemPort,                                         u1Proto, pStateNode)   

#define FL_FWL_UPDATE_FLOWDATA_TCPSTATES(pBuf, pStateNode)     
#define FWL_FL_UTIL_GET_TIMESTAMP(u4LocIP, u4RemIP, u2SrcPort, u2DestPort, u1Proto)   OSIX_FAILURE

#define FL_FWL_DEL_HW_ENTRIES(u4LocalIP,u4RemoteIP,u2SrcPort,u2DestPort,u1Proto)     
#define FL_FWL_DEL_IFINDEX_HW_ENTRIES(u4IfIndex) 
    
#endif /* FLOWMGR_WANTED */

/* Macro definitions for State Table */
#define FWL_STATEFUL                   1
#define FWL_PARTIAL                    2
#define FWL_INITFLOW                   3

/* Prefix length */
#define FWL_IPV4_PREFIX_LEN            4
#define FWL_IPV6_PREFIX_LEN            16

#define FWL_INVALID            -1
#define FWL_ZERO                0
#define FWL_ONE_BYTE            1
#define FWL_TWO_BYTES           2
#define FWL_THREE_BYTES         3
#define FWL_FOUR_BYTES          4
#define FWL_FIVE_BYTES          5
#define FWL_SEVEN_BYTES         7
#define FWL_EIGHT_BYTES         8
#define FWL_NINE_BYTES          9
#define FWL_ELEVEN_BYTES       11

#define FWL_END_TIME           1440

#define MODULO_DIVISOR_2        2

#define FWL_ONE                 1
#define FWL_TWO                 2     
#define FWL_THREE               3    
#define FWL_FOUR                4     
#define FWL_FIVE                5     
#define FWL_SIX                 6     
#define FWL_EIGHT               8     
#define FWL_NINE                9     
#define FWL_TEN                 10  
#define FWL_TWELVE              12   
#define FWL_THIRTEEN            13
#define FWL_FOURTEEN            14  
#define FWL_FIFTEEN             15 
#define FWL_SIXTEEN             16 
#define FWL_TWENTY              20
#define FWL_MAX_ROW_NUM         23
#define FWL_HASH_TABLE_LEN      3
#define FWL_IP_ADDR_LEN         2
#define FWL_PORT_NO_LEN         2


#define FWL_INDEX_0              0
#define FWL_INDEX_1              1
#define FWL_INDEX_2              2
#define FWL_INDEX_3              3
#define FWL_INDEX_4              4
#define FWL_INDEX_5              5
#define FWL_INDEX_6              6
#define FWL_INDEX_7              7
#define FWL_INDEX_8              8
#define FWL_INDEX_9              9
#define FWL_INDEX_10             10
#define FWL_INDEX_11             11
#define FWL_INDEX_12             12
#define FWL_INDEX_13             13
#define FWL_INDEX_14             14
#define FWL_INDEX_15             15
#define FWL_INDEX_16             16
#define FWL_INDEX_20             20
#define FWL_DATE_LEN             32

#define FWL_INDEX_256            256
#define FWL_INDEX_257            257

#define FWL_PROTO_ICMP           1
#define FWL_PROTO_IGMP           2
#define FWL_PROTO_GGP            3
#define FWL_PROTO_IP             4
#define FWL_PROTO_TCP            6
#define FWL_PROTO_EGP            8
#define FWL_PROTO_IGP            9
#define FWL_PROTO_NVP            11
#define FWL_PROTO_UDP            17
#define FWL_PROTO_IRTP           28
#define FWL_PROTO_IDPR           35
#define FWL_PROTO_RSVP           46
#define FWL_PROTO_MHRP           48
#define FWL_PROTO_ICMPV6         58
#define FWL_PROTO_IGRP           88
#define FWL_PROTO_OSPF           89
#define FWL_PROTO_ANY            255

#define FWL_OID_1                1
#define FWL_OID_3                3
#define FWL_OID_4                4
#define FWL_OID_5                5
#define FWL_OID_6                6
#define FWL_OID_16               16
#define FWL_OID_2076             2076


#define FWL_CONF                 0
#define FWL_INFO                 1
#define FWL_DOS                  2
#define FWL_CMD_DECODE           3
#define FWL_BAD_TRAFFIC          4
#define FWL_MISC                 5
#define FWL_INFO_LEAK            6
#define FWL_PRIV_GAIN            7
#define FWL_SUSP_LOGIN           8
#define FWL_DYNAMIC_MSG          9
#define FWL_ACCEPT               10
#define FWL_NON_STANDARD_EVENT   11
#define FWL_MAX_CLASSIFICATION   12 







#define FWL_INTERFACE_NAME_LEN       32
#define FWL_INTERFACE_NAME_LEN_MAX   20

#define FWL_OFFSET_0a             0x0a
#define FWL_OFFSET_0d             0x0d
#define FWL_OFFSET_3f             0x3f
#define FWL_OFFSET_01             0x01
#define FWL_OFFSET_7c             0x7c
#define FWL_OFFSET_8000           0x8000
#define FWL_OFFSET_1FFF           0x1FFF
#define FWL_OFFSET_4000           0x4000
#define FWL_OFFSET_2000           0x2000
#define FWL_OFFSET_0F             0x0F
#define FWL_OFFSET_F0             0xF0
#define FWL_OFFSET_0f             0x0f
#define FWL_OFFSET_f              0xf
#define FWL_SUBNET_MASK           0xFFFFFFFF
#define FWL_INT_MAX               65536

#define FWL_BROADCAST_ADDR        0xffffffff
#define FWL_DEFAULT_ADDR          0x00000000
#define FWL_32ND_BIT_SET          0x80000000
#define FWL_SET_TRACE_INIT    0x00000000

#define FWL_UDP_PORT_SEVEN                 7
#define FWL_SMB_PORT_NUMBER              445
#define FWL_NET_BIOS_PORT_NUMBER_137     137
#define FWL_NET_BIOS_PORT_NUMBER_139     139

#define FWL_DIV_FACTOR_100               100 
#define FWL_DIV_FACTOR_10                10 
#define FWL_MASK_1_BIT                   0x00000001
#define FWL_CHECK_VAL_9                  9
#define FWL_MAX_MASK_VAL                 32
#define FWL_MIN_MASK_VAL                 3

#define FWL_LOG_FILE_LEN 256
#define FWL_FILE_READ_LEN 128

#define FWL_THRESHOLD_LOW              80
#define FWL_THRESHOLD_HIGH            100
#define FWL_THRESHOLD_MAX_NODE            30

#define  FWL_SET_ALL_BITS                         0xffffffff
#define FWL_NET_ADDR_LOOP_BACK                    0xff000000
#define FWL_NET_ADDR_MASK                         0xf0000000
#define FWL_CLASS_B_ADDR                          0x80000000
#define FWL_CLASS_C_ADDR                          0xc0000000
#define FWL_CLASS_D_ADDR                          0xe0000000
#define FWL_CLASS_B_MASK                          0xc0000000
#define FWL_CLASS_C_MASK                          0xe0000000
#define FWL_LOOP_BACK_ADDR                        0x7f000000
#define FWL_CLASS_E_ADDR                          0xf0000000
#define FWL_SET_24_BITS                           0x00ffffff
#define FWL_SET_16_BITS                           0x0000ffff
#define FWL_SET_8_BITS                            0x000000ff

#define FWL_NETBIOS_NS   137
#define FWL_NETBIOS_DGM  138
#define FWL_W2KDC_PORT_NUMBER 464
#define FWL_ASCEND_ATTACK_PORT 9
#define FWL_UDP_LOOPBACK_ATTACK_PORT_7    7
#define FWL_UDP_LOOPBACK_ATTACK_PORT_19   19
#define FWL_UDP_LOOPBACK_ATTACK_PORT_17   17

#define FWL_SNORK_ATTACK_PORT_135    135
#define FWL_SNORK_ATTACK_PORT_7       7
#define FWL_SNORK_ATTACK_PORT_19    19

/*MACROS for OID's*/

#define FWL_FIREWALL_TRAP_OID     { 1, 3, 6, 1, 4, 1, 2076, 16, 4, 0, 3 }
#define FWL_IF_INDEX_OID          { 1, 3, 6, 1, 4, 1, 2076, 16, 4, 1, 3 }
#define FWL_IF_IF_INDEX_OID       { 1, 3, 6, 1, 4, 1, 2076, 16, 2, 6, 1, 1, 0 }
#define FWL_PKT_DENIED_OID        { 1, 3, 6, 1, 4, 1, 2076, 16, 3, 14, 1, 3 }
#define FWL_IDS_PREFIX_TRAP_OID   { 1, 3, 6, 1, 4, 1 }
#define FWL_IDS_SUFFIX_TRAP_OID   { 16, 4, 1 }
#define FWL_IDS_SNMP_TRAP_OID     { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 }

#define FWL_IP_POINTER_VALUE_5    5   
#define FWL_IP_POINTER_VALUE_9    9 
#define FWL_IP_POINTER_VALUE_13   13
#define FWL_IP_POINTER_VALUE_17   17
#define FWL_IP_POINTER_VALUE_21   21
#define FWL_IP_POINTER_VALUE_25   25
#define FWL_IP_POINTER_VALUE_29   29
#define FWL_IP_POINTER_VALUE_33   33
#define FWL_IP_POINTER_VALUE_37   37
#define FWL_ERROR_MSG_LENGTH      30



#endif /* End for _FWLDEFN_H */
/****************************************************************************/
/*                 End of the file -- fwldefn.h                             */
/****************************************************************************/
