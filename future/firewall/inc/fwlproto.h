/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlproto.h,v 1.22 2016/02/27 10:04:59 siva Exp $
 *
 * Description:This file contains the all extern Proto    
 *             types functions.                        
 *
 *******************************************************************/
#ifndef _FWLPROTO_H
#define _FWLPROTO_H


/************************** fwlicmp.c *************************************/
PUBLIC UINT4 
FwlInspectIcmpOption PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                             tIfaceInfo *pIfaceInfo,
                             tIpHeader *pIpHdr,
                             tIcmpInfo *pIcmpHdr,
                             UINT4      u4IfaceNum,
                             UINT1      u1Direction
                            ));


/************************** fwllimit.c *************************************/
PUBLIC UINT4
FwlInspectIcmp6Option PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                             tIfaceInfo *pIfaceInfo,
                             tIpHeader *pIpHdr,
                             tIcmpInfo *pIcmpHdr,
                             UINT4      u4IfaceNum,
                             UINT1      u1Direction
                            ));
PUBLIC UINT4
FwlProcessPktForRateLimiting (tCRU_BUF_CHAIN_HEADER *pBuf, 
                              tIfaceInfo *pIfaceInfo,
                              tHLInfo *pHLInfo,
                              tIcmpInfo *pIcmp,
                              UINT4 u4IfaceNum, 
                              UINT1 u1Proto);
PUBLIC UINT4
FwlProcessRouterPktForRateLimiting (tCRU_BUF_CHAIN_HEADER *pBuf, 
                              tIfaceInfo *pIfaceInfo,
                              tHLInfo *pHLInfo,
                              tIcmpInfo *pIcmp,
                              UINT4 u4IfaceNum, 
                              UINT1 u1Proto);
PUBLIC UINT4 
FwlInspectConcurrentSessions PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                     tIpHeader *pIpHdr, 
                                     tHLInfo   *pHLInfo,
                                     UINT4      u4IfaceNum,
                                     UINT1      u1Direction,
                                     tOutUserSessionNode **ppOutUserSessionNode,
                                     tInUserSessionNode **ppInUserSessionNode));

PUBLIC UINT4
FwlUpdateConcurrentSessionInfo PROTO ((tIpHeader        IpHdr,
                                   tHLInfo              HLInfo,
                                   UINT1                u1Direction,
                                   tOutUserSessionNode *pOutUserSessionNode,
                                   tInUserSessionNode  *pInUserSessionNode));

PUBLIC VOID
FwlRemoveUserSession PROTO ((UINT4 u4Flush, 
                             tStatefulSessionNode *pState));
   
/************************** fwlinsp.c *************************************/

PUBLIC UINT4 FwlInspectPkt         PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                         UINT4             u4IfaceNum,
                                         UINT1           u1Direction));

PUBLIC UINT4 FwlCheckSNMPTrapPacket PROTO((tCRU_BUF_CHAIN_HEADER *pBuf));

UINT4 FwlCheckAttacks PROTO((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfaceNum,
                             tIpHeader *pIpHdr, tHLInfo *pHLInfo, 
                             tIcmpInfo *pIcmpHdr, UINT1 u1Direction,
                             UINT4 u4TransHdrLen));

/******************************* fwlurl.c *************************************/

PUBLIC VOID FwlUrlFilteringInit(VOID);

PUBLIC UINT4 FwlUrlFilterMemAllocate  PROTO((UINT1 **ppu1Block));

PUBLIC VOID FwlAddUrlFilter   PROTO((tFwlUrlFilterInfo *pFilterNode));

PUBLIC UINT4 FwlDeleteUrlFilter
               PROTO((UINT1 au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN]));

PUBLIC VOID  FwlSetDefaultUrlFilterValue
                 PROTO((UINT1   au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN],
                        tFwlUrlFilterInfo   *pFilterNode));

PUBLIC tFwlUrlFilterInfo *FwlSearchUrlFilter
               PROTO((UINT1 au1FilterName[FWL_MAX_URL_FILTER_STRING_LEN]));

PUBLIC UINT4 FwlMatchUrlFilter PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                             UINT4       u4HttpPktOffset,
                                             UINT4       u4HttpPktLen));
PUBLIC UINT4
FwlGetUrlFromHttpPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                             UINT4                  u4HttpPktOffset,
                             UINT4                  u4HttpPktLen,
                             INT1                  **ppi1UrlInfo));

PUBLIC UINT4
FwlGetHttpHostField PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                     UINT4 u4HttpPktOffset, UINT4 u4HttpPktLen, 
                     UINT4 *pu4BeginOffset, UINT4 *pu4HostFieldLen));
/************************** fwlerror.c ************************************/

PUBLIC VOID FwlGenerateMemFailureTrap  PROTO((UINT1 u1NodeName)); 
PUBLIC VOID FwlGenerateAttackSumTrap PROTO((VOID)); 

PUBLIC tFwlLogBuffer *FwlGetFreeLogBuffer PROTO((VOID));

PUBLIC VOID FwlClearLogBuffer (VOID);

PUBLIC VOID FwlUpdateLogLevel4SendEmailAlerts PROTO((UINT4 u4AttackType, UINT4* pu4LogLevel));

/************************** fwlinit.c *************************************/

/* P2R4 : Added this function */
INT1 FwlInitDefaultRule PROTO((INT1 *, INT1 *, UINT4, UINT4, UINT1, UINT1));
VOID FwlClearGlobalStats PROTO((VOID));
VOID FwlEnableDefIptables PROTO((VOID));
VOID FwlClearIPv6GlobalStats PROTO((VOID));
VOID FwlClearInterfaceStats PROTO((UINT4 u4IfIndex));
VOID FwlClearInterfaceIPv6Stats PROTO((UINT4 u4IfIndex));

/************************** fwlbufif.c ************************************/

PUBLIC UINT4 FwlUpdateIpHeaderInfoFromPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                                tIpHeader            *pIpInfo));

PUBLIC UINT4 FwlUpdateIpv4HeaderInfoFromPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                                tIpHeader            *pIpInfo));

PUBLIC UINT4 FwlUpdateIpv6HeaderInfoFromPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                                tIpHeader            *pIpInfo));

PUBLIC UINT4 FwlParseIpOptions PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                     tIpHeader *pIpHdr));


PUBLIC VOID FwlUpdateIcmpv4v6InfoFromPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                            tIcmpInfo             *pIcmp,
                                             UINT1              u1IpHeadLen));

PUBLIC VOID FwlUpdateHLInfoFromPkt PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                          tHLInfo               *pHLInfo,
                                          UINT1                 u1Proto,
                                          UINT1                 u1IpHeadLen));

PUBLIC UINT4 FwlFilterMemAllocate  PROTO((UINT1 **ppu1Block));

PUBLIC UINT4 FwlSnorkMemAllocate  PROTO((UINT1 **ppu1Block));
PUBLIC UINT4 FwlRpfMemAllocate  PROTO((UINT1 **ppu1Block));

PUBLIC UINT4 FwlRuleMemAllocate    PROTO((UINT1 **ppu1Block));

PUBLIC UINT4 FwlAclMemAllocate PROTO((UINT1 **ppu1Block));

PUBLIC UINT4 FwlIfaceMemAllocate        PROTO((UINT1 **ppu1Block));

PUBLIC UINT4 FwlStateMemAllocate   PROTO((UINT1 **ppu1Block));

PUBLIC UINT4 FwlTcpInitFlowMemAllocate PROTO((UINT1 **ppu1Block));

PUBLIC VOID FwlFilterMemFree           PROTO((UINT1 *pu1Block));
PUBLIC VOID FwlSnorkMemFree           PROTO((UINT1 *pu1Block));
PUBLIC VOID FwlRpfEntryMemFree           PROTO((UINT1 *pu1Block));

PUBLIC VOID FwlRuleMemFree           PROTO((UINT1 *pu1Block));

PUBLIC VOID FwlAclMemFree PROTO((UINT1 *pu1Block));

PUBLIC VOID FwlIfaceMemFree           PROTO((UINT1 *pu1Block));

PUBLIC VOID FwlStateMemFree   PROTO((UINT1 *pu1Block));

PUBLIC VOID FwlTcpInitFlowMemFree PROTO((UINT1 *pu1Block));

PUBLIC UINT4 FwlPartialLinksMemAllocate PROTO((UINT1 **ppu1Block));

PUBLIC VOID FwlPartialLinksMemFree PROTO((UINT1 *pu1Block));


/************************** fwldbase.c ************************************/

PUBLIC tFilterInfo *FwlDbaseSearchFilter
                       PROTO((UINT1 au1FilterName[FWL_MAX_FILTER_NAME_LEN]));
PUBLIC tFwlSnorkInfo *FwlDbaseSearchSnork  PROTO((INT4 i4PortNo));
PUBLIC tFwlRpfEntryInfo *FwlDbaseSearchRpfEntry  PROTO((INT4 i4PortNo));

PUBLIC tRuleInfo *FwlDbaseSearchRule 
                          PROTO((UINT1 au1RuleName[FWL_MAX_RULE_NAME_LEN]));

PUBLIC tIfaceInfo *FwlDbaseSearchIface PROTO((UINT4 u4IfaceNum));

PUBLIC tAclInfo *FwlDbaseSearchAclFilter
                        PROTO((tTMO_SLL *pAclList,
                               UINT1   au1AclName[FWL_MAX_FILTER_NAME_LEN]));

PUBLIC UINT4  FwlDbaseAddAclFilterInSequence PROTO((UINT4          u4IfaceNum,
                                                    UINT2          u2SeqNum,
                                                    tAclInfo       *pAclNode,
                                                    INT4          i4Direction));

PUBLIC UINT4 FwlDbaseDeleteFilter 
                       PROTO((UINT1 au1FilterName[FWL_MAX_FILTER_NAME_LEN]));
PUBLIC UINT4 FwlDbaseDeleteSnork 
                       PROTO((INT4 i4PortNo));

PUBLIC UINT4 FwlDbaseDeleteRpfEntry 
                       PROTO((INT4 i4PortNo));

PUBLIC UINT4 FwlDbaseDeleteRule 
                             PROTO((UINT1 au1RuleName[FWL_MAX_RULE_NAME_LEN]));

PUBLIC UINT4 FwlDbaseDeleteAcl 
                          PROTO((UINT4   u4IfaceNum,
                                 INT4    i4Direction,
                                 UINT1   au1AclName[FWL_MAX_FILTER_NAME_LEN]));
                             
PUBLIC UINT4 FwlDbaseDeleteIface PROTO((UINT4 u4IfaceNum));

/* CHANGE2 : refer CHANGE2 of fwldbase.c */
PUBLIC VOID FwlDbaseAddFilter   PROTO((tFilterInfo *pFilterNode));

PUBLIC VOID FwlDbaseAddSnork   PROTO((tFwlSnorkInfo *pFilterNode));

PUBLIC VOID FwlDbaseAddRpfEntry   PROTO((tFwlRpfEntryInfo *plRpfEntryNode));
PUBLIC VOID FwlDbaseAddRule   PROTO((tRuleInfo *pRuleNode));

PUBLIC UINT4
FwlMatchStateEntryInAcl PROTO((tStatefulSessionNode **ppStateEntry));

/************************** fwlsnif.c ************************************/                             
PUBLIC INT1  FwlValidateProtocol PROTO((INT4 i4Protocol));

PUBLIC INT1  FwlValidateIcmpType PROTO((INT4 i4IcmpType));

PUBLIC INT1  FwlValidateIcmpCode  PROTO((INT4 i4IcmpCode));

PUBLIC VOID  FwlSetDefaultFilterValue
                         PROTO((UINT1   au1FilterName[FWL_MAX_FILTER_NAME_LEN], 
                                tFilterInfo   *pFilterNode));

PUBLIC VOID  FwlSetDefaultRuleValue 
                         PROTO((UINT1        au1RuleName[FWL_MAX_RULE_NAME_LEN],
                                tRuleInfo    *pRuleNode));

PUBLIC VOID  FwlSetDefaultIfaceValue PROTO((UINT4         u4IfaceNum,
                                            tIfaceInfo   *pIfaceNode));

PUBLIC INT1  FwlSetDefaultAclFilterValue
                         PROTO((UINT1    au1AclName[FWL_MAX_ACL_NAME_LEN],
                               tAclInfo  *pAclFilterNode));

PUBLIC INT1 FwlSetInitForAclNode PROTO((UINT4 u4IfaceNum,
                                       INT4 i4Direction,
                                       UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN]));

/* CHANGE1 : Refer CHANGE1 in fwlsnif.c*/         
PUBLIC INT1 FwlSetActionValueForAclNode PROTO((tIfaceInfo *pIfaceNode,
                                               INT4       i4Direction,
                                        UINT1  au1AclName[FWL_MAX_ACL_NAME_LEN],
                                                INT4       i4Action));
PUBLIC INT1 FwlSetSeqNumValueForAclNode PROTO((tIfaceInfo  *pIfaceNode,
                                               INT4        i4Direction,
                                               UINT1       
                                               au1AclName[FWL_MAX_ACL_NAME_LEN],
                                               UINT2        u2SeqNum));

PUBLIC VOID FwlSetRowStatusValueForAclNode 
                       PROTO((UINT4       u4IfaceNum,
                              INT4        i4Direction,
                              UINT1       au1AclName[FWL_MAX_ACL_NAME_LEN],     
                              UINT1        u1RowStatus));

PUBLIC INT1 FwlTestRowStatusValueForAclNode 
                       PROTO((UINT4       u4IfaceCount,
                              UINT4       u4IfaceIndex,
                              UINT4       u4IfaceNum,
                              INT4        i4Direction,
                              UINT1       au1AclName[FWL_MAX_ACL_NAME_LEN],     
                              UINT1        u1RowStatus));

/* NAETRA_ADD : Added prototypes to remove warnings */
PUBLIC INT1 FwlSetStartTimeForAclNode 
                      PROTO((tIfaceInfo * pIfaceNode,
                             INT4 i4Direction,
                             UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                             UINT4 u4StartTime));

PUBLIC INT1 FwlSetEndTimeForAclNode 
                      PROTO((tIfaceInfo * pIfaceNode,
                             INT4 i4Direction,
                             UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                             UINT4 u4EndTime));
/* NAETRA_ADD */

/************************** fwlutils.c ***********************************/

PUBLIC VOID  FwlConcatAddrAndMask PROTO((UINT4 u4Addr,
                                         UINT4 u4Mask,
                                         UINT1 *pu1Addr));

PUBLIC INT1  FwlParseMinAndMaxPort PROTO((UINT1   au1Port[FWL_MAX_PORT_LEN],
                                          UINT2   *pu2MaxPort,
                                          UINT2   *pu2MinPort));
                  
PUBLIC INT1  FwlParseFilterSet
                          PROTO((UINT1     au1FilterSet[FWL_MAX_FILTER_SET_LEN],
                                 tRuleInfo *pRuleNode));                

/* NAETRA_ADD : Added a function to add the filter set in the set routine */
PUBLIC INT1  FwlAddFilterSet
                          PROTO((UINT1     au1FilterSet[FWL_MAX_FILTER_SET_LEN],
                                 tRuleInfo *pRuleNode));                

PUBLIC INT1 FwlParseIpAddr PROTO((UINT1     au1Addr[FWL_MAX_ADDR_LEN],
          UINT4     *pu4StartAddress,
          UINT4 *pu4EndAddress));

PUBLIC INT1 FwlParseIpv6Addr PROTO((UINT1     au1Addr[FWL_MAX_ADDR_LEN],
          tIp6Addr *pStartAddress,
          tIp6Addr *pEndAddress));


PUBLIC INT1 FwlValidatePortNumber (UINT1 *pTestValFwlFilterPort );

PUBLIC INT4 FwlValidateIpAddress(UINT4 u4IpAddr);

PUBLIC VOID FwlUtlSetIfTypeExternal PROTO ((UINT4 u4IfaceNum));
PUBLIC INT4 FwlIsIpAddressValid(UINT4 u4IpAddress);

PUBLIC INT4 FwlUtilAddFwlLogToFile PROTO ((UINT1 *pMsg));

/* ICSA Fix: V18 -S- */
PUBLIC INT4 FwlUtilInitLogFileSize (VOID);
PUBLIC VOID FwlUtilInitLogDirSize (CONST CHR1 *, UINT4 *);
PUBLIC INT4 FwlUtilRetainPartialLog (UINT1 *pu1FwlLogFile);

PUBLIC INT4 FwlUtilReleaseWebLog(UINT1 *pu1LogPtr);

/************************** fwlserv.c ***********************************/

PUBLIC UINT4 FwlProcessPktForServiceIndpFiltering PROTO((tIpHeader   *pIpHeader,
                                                         tHLInfo      *pHLInfo, 
                                                       tIfaceInfo *pIfaceInfo,
             tCRU_BUF_CHAIN_HEADER *pbuf,UINT4 u4IfaceNum));
PUBLIC UINT4 FwlProcessPktForLanInFiltering PROTO((tIpHeader * pIpHeader));
                         
/************************** fwltmrif.c ***********************************/

PUBLIC VOID FwlTmrInitTmr PROTO((VOID));

PUBLIC VOID FwlTmrDeInitTmr PROTO((VOID));

PUBLIC VOID FwlTmrRestartTmr PROTO((tTmrAppTimer  *pTimer,
                                    UINT1          u1TmrId,
                                    UINT4          u4Interval));

PUBLIC VOID FwlTmrSetTimer   PROTO((tTmrAppTimer *pTimer,
                                    UINT1         u1TmrId,
                                    UINT4         u4Interval));

PUBLIC VOID FwlTmrDeleteTimer PROTO((tTmrAppTimer *pTimer));

PUBLIC VOID FwlTmrDeleteAllTimers PROTO((VOID));

VOID FwlRestartIdleTimer PROTO ((VOID));
PUBLIC VOID FwlAclScheduleSetStatus PROTO ((INT4 i4Status));

/************************** fwlstat.c ***********************************/

VOID FwlStatefulInit PROTO ((VOID));

UINT4 FwlPktStatefulInspect PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                   tIpHeader             *pStateIpHdr,
                                   tIcmpInfo             *pStateIcmpHdr,
                                   tHLInfo               *pStateHLInfo,
                                   UINT4                  u4TotalHdrLen,
                                   UINT1                  u1Direction,
                                   UINT1                 *pu1LogTrigger));


UINT4 FwlGetTransportHeaderLength PROTO((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                         UINT1 u1PktType, 
                                         UINT1 u1Headlen));

UINT4 FwlFTPdynSessionHandler PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                               tIpHeader *pStateIpHdr,tHLInfo *pStateHLInfo,
                               UINT4 u4Headlen,UINT1 u1Direction,
                               UINT1 *pu1LogTrigger));

VOID FwlFtpGetPayloadIpPort PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                             INT1 * ai1IpAddr,
                             INT1 * ai1Port,
                             UINT4 u4Headlen, 
                             UINT1 u1Cmd,
                             tFwlGlobalInfo *PayloadData));

VOID FwlFtpGetIpPortFromExtdPayload PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                             INT1 * pai1IpAddr, 
                             INT1 * pai1Port,
                             UINT4 u4Headlen, 
                             UINT1 u1Cmd,
                             tFwlGlobalInfo *PayloadData));

UINT4 FwlTCPPktStatefulInspect PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                           tIpHeader * pStateIpHdr,
                                           tHLInfo *pStateHLInfo,
                                           UINT4 u4Headlen,
                                           UINT1 u1Direction,
                                           UINT1 *pu1LogTrigger));

UINT4 FwlUDPPktStatefulInspect PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                       tIpHeader *pStateIpHdr,
                                       tHLInfo *pStateHLInfo,
                                       UINT1 u1Direction,
                                       UINT1 *pu1LogTrigger));

UINT4 FwlICMPPktStatefulInspect PROTO ((tIpHeader *pStateIpHdr,
                                                    tHLInfo *pStateHLInfo,
                                                    tIcmpInfo *pStateIcmpHdr,
                                                    UINT1 u1Direction,
                                        UINT1 *pu1LogTrigger));

VOID FwlInitStateFilterNode PROTO ((tIpHeader *pIpHdr,
                                    tHLInfo   *pHLInfo,
                                    tIcmpInfo *pIcmpHdr,
                                    UINT1      u1Direction,
                                    UINT1      u1LogLevel,
                     tStatefulSessionNode     *pInitNode));

   
UINT4 FwlAddStateFilter PROTO ((tStatefulSessionNode *pTempNode, UINT1 u1Direction));

UINT4 FwlGarbageCollect PROTO ((UINT4 u4Flush, UINT4 u4CommitRules));

UINT4 FwlMatchStateFilter PROTO ((tIpHeader * pIpHdrInfo,
                          tHLInfo * pHLHdrInfo,
                          tIcmpInfo *pIcmpHdr,
                          UINT1 u1Direction,
                          UINT1 *pu1LogTrigger,
                          tStatefulSessionNode **pStateNode));

UINT4 FwlTcpEstStateHandler PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                            tStatefulSessionNode *pStateNode,
                            UINT1 u1Direction,
                            tIpHeader *pStateIpHdr,
                            tHLInfo *pStateHLInfo));

UINT4 FwlDeleteStatefulFilter PROTO ((tIpHeader  *pStateIpHdr,
                           tHLInfo    *pStateHLInfo,
                           tIcmpInfo  *pStateIcmpHdr,
                           UINT1       u1PktDirection));

/************************** fwltcp.c ***********************************/

VOID 
FwlInitialiseInitFlowNode PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                          tIpHeader *pStateIpHdr,
                          tStatefulSessionNode *pStateNode,
                          tHLInfo *pStateHLInfo, 
                          UINT1 u1Direction,
                          UINT4 u4CurTime));

VOID FwlTcpInitFlowListInit(void);

UINT4 FwlAddTcpInitFlowNode PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                            tIpHeader *pStateIpHdr,
                            tHLInfo *pStateHLInfo,
                            UINT1 u1State,
                            UINT1 u1Direction,                      
                            UINT1 u1LogLevel,
                            UINT1 u1AppCallStatus)); 

UINT4 FwlDeleteTcpInitFlowNode PROTO((UINT4 u4Flush));

UINT4 FwlMatchTcpInitFlowNode PROTO((tIpHeader *pStateIpHdr,
                          tHLInfo *pStateHLInfo,
                          UINT1 u1Direction,
                          UINT1 *pu1LogTrigger,
                          tStatefulSessionNode **ppInitFlowNode));

UINT4 FwlTcpInitFlowGetAgedOutNode PROTO((tStatefulSessionNode**
                                          ppInitFlowNode, tTMO_SLL * pInitFlowLst));
   

UINT4 FwlTcpInitStateHandler PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                              tStatefulSessionNode *pStateNode,
                              UINT1 u1Direction,
                              tIpHeader *pStateIpHdr,
                              tHLInfo *pStateHLInfo));

UINT4 FwlTcpValidateTcpSeqAck PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                               tIpHeader            *pStateIpHdr,
                               tHLInfo              *pStateHLInfo,
                               tStatefulSessionNode *pStateNode,
                               UINT1                 u1Direction,
                               UINT1                 u1UpdateFlag));

VOID FwlTcpUpdateInitFlowSeqAckWin PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                    tIpHeader             *pStateIpHdr,
                                    tHLInfo               *pStateHLInfo,                                            tStatefulSessionNode  *pStateNode,
                                    UINT1                  u1Direction));

/************************** fwllinks.c ***********************************/
PUBLIC UINT4
FwlProcessPartialLinks PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                        tIpHeader             *pStateIpHdr, 
                        tHLInfo               *pStateHLInfo,
                        UINT4                  u4IfaceNum,
                        UINT1                  u1Direction,
                        tStatefulSessionNode  *pStateNode,
                        tOutUserSessionNode   *pOutUserSessionNode, 
                        tInUserSessionNode    *pInUserSessionNode)); 
PUBLIC VOID 
FwlPartialLinksListInit (VOID);

PUBLIC UINT4
FwlAddPartialLinkNode(tIpHeader *pStateIpHdr,
                      tHLInfo *pStateHLInfo,
                      UINT1 u1State,                      
                      UINT1 u1Direction,
                      UINT1 u1LogLevel,
                      UINT1 u1AppCallStatus);

PUBLIC UINT4 
FwlDeletePartialLinksListNodes (UINT4 u4Flush);

PUBLIC UINT4 
FwlMatchPartialLinksNode (tIpHeader *pStateIpHdr,
                          tHLInfo *pStateHLInfo,
                          UINT1 u1Direction,
                          tStatefulSessionNode **ppPartialLinkNode);

PUBLIC UINT4 
FwlSearchPartialLinkNode (tIpHeader *pStateIpHdr,
                          tHLInfo *pStateHLInfo,
                          UINT1 u1PktDirection,
                          tStatefulSessionNode **ppPartialLinkNode,
                          UINT1 u1AppCallType);


/************************** fwldmz.c ***********************************/

PUBLIC VOID FwlDmzInit PROTO((VOID));

PUBLIC UINT4
FwlDmzMemAllocate PROTO ((UINT1 **ppu1Block));

PUBLIC UINT4
FwlIPv6DmzMemAllocate PROTO ((UINT1 **ppu1Block));

PUBLIC VOID
FwlSetDmzValue PROTO( (UINT4 u4DmzIpAddress, tFwlDmzInfo *pDmzNode));

PUBLIC VOID
FwlAddDmzNode PROTO((tFwlDmzInfo *pDmzNode));

PUBLIC VOID
FwlAddIPv6DmzNode PROTO ((tFwlIPv6DmzInfo *pDmzNode));

PUBLIC UINT4
FwlDeleteDmzNode PROTO ( (UINT4 u4DmzIpAddress));

PUBLIC tFwlDmzInfo*
FwlSearchDmzNode PROTO ((UINT4 u4DmzIpAddress));

PUBLIC UINT4
FwlDeleteIPv6DmzNode PROTO ((tIp6Addr DmzIpHost));

PUBLIC tFwlIPv6DmzInfo*
FwlSearchIPv6DmzNode PROTO ((tIp6Addr DmzIpHost));


PUBLIC VOID
FwlSetDefaultDmzValue PROTO ((UINT4 u4FwlDmzIpIndex ,   
                              tFwlDmzInfo  *pFwlDmzNode));

PUBLIC UINT4
FwlInspectDmzPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, tIpHeader IpHdr, tHLInfo HLInfo,
                 tIcmpInfo IcmpHdr, UINT4 u4IfaceNum, UINT1 u1Direction, 
                 UINT4 u4TotalHdrLen));

/************************** fwlblkwt.c **********************************/
PUBLIC VOID FwlBlackWhiteInitLists PROTO ((VOID));
PUBLIC VOID FwlBlackWhiteDeInitLists PROTO ((VOID));

PUBLIC INT4 BlackEntryCmp PROTO ((tRBElem * prbElem1, tRBElem * prbElem2));
PUBLIC INT4 WhiteEntryCmp PROTO ((tRBElem * prbElem1, tRBElem * prbElem2));

PUBLIC INT4 FwlBlackListEntryCreate PROTO ((INT4 i4AddrType, 
                                            tSNMP_OCTET_STRING_TYPE * pIpAddr,
                                            UINT4 u4IpMask,
                                            INT4 i4EntryType));
PUBLIC INT4 FwlBlackListEntryDelete PROTO ((INT4 i4AddrType, 
                                            tSNMP_OCTET_STRING_TYPE * pIpAddr,
                                            UINT4 u4IpMask));
PUBLIC tFwlBlackListEntry * 
FwlGetBlackListEntry PROTO ((INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                             UINT4 u4IpMask));

PUBLIC INT4 FwlWhiteListEntryCreate PROTO ((INT4 i4AddrType, 
                                            tSNMP_OCTET_STRING_TYPE * pIpAddr,
                                            UINT4 u4IpMask));
PUBLIC INT4 FwlWhiteListEntryDelete PROTO ((INT4 i4AddrType, 
                                            tSNMP_OCTET_STRING_TYPE * pIpAddr,
                                            UINT4 u4IpMask));
PUBLIC tFwlWhiteListEntry * 
FwlGetWhiteListEntry PROTO ((INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pIpAddr,
                             UINT4 u4IpMask));

PUBLIC INT4 FwlMatchBlackListEntry PROTO ((UINT1 u1Direction, 
                                           tFwlIpAddr SrcAddr,
                                           tFwlIpAddr DestAddr));
PUBLIC INT4 FwlMatchWhiteListEntry PROTO ((UINT1 u1Direction,
                                           tFwlIpAddr SrcAddr,
                                           tFwlIpAddr DestAddr));

/************************** fwlutil.c ***********************************/
PUBLIC INT1
FwlUtilGetIfMaxFragmentSize (INT4 i4FwlIfIndex, UINT2 *pu2MaxFragmentSize);


/************************** fsfwllw.c *************************************/
PUBLIC VOID 
FwlGetAclCmdString( UINT4 u4IfaceNum, UINT1 *pu1AclName, INT4 i4Direction, UINT1 *pu1CmdStr);

PUBLIC VOID 
FwlGetFilterCmdString( tFilterInfo* pFilterNode, INT1 *pu1CmdStr );

PUBLIC UINT4
TftpAlgProcessing (tIpHeader * pStateIpHdr, tHLInfo * pStateHLInfo,
                          UINT1 u1Direction);

/******************Extern declarations ******************************************/

extern UINT1 * GetHostName PROTO((VOID));

extern UINT4 CfaGetIpAddr PROTO ((UINT4 u4Index, UINT4 *pu4IpAddr));

extern UINT4 CfaIsLocalNetworkIpAddr PROTO ((UINT4 u4IpAddress));

extern UINT4 CfaGetBroadCastAddress PROTO ((UINT4 *pu4IpAddr));

extern INT4 AddFwlLogToFile PROTO ((UINT1 *pMsg));

#endif /* End of _FWLPROTO_H */

PUBLIC  UINT4 IpGetIfIndexFromPort      PROTO ((UINT2 u2Port));
VOID   FwlTcpInitDelOldEntries PROTO ((VOID));
VOID   FwlTcpInitThreshDropTmrCb PROTO ((VOID));
UINT4  FwlTcpInitCheckThreshPerNode PROTO ((tFwlIpAddr));
PUBLIC UINT4
FwlConcurrentSessionsProcess (tCRU_BUF_CHAIN_HEADER *pBuf, 
                              tIpHeader             *pIpHdr, 
                              tHLInfo               *pHLInfo,
                              UINT4                  u4IfaceNum,
                              UINT1                  u1Direction,
                              tOutUserSessionNode  **ppOutUserSessionNode, 
                              tInUserSessionNode   **ppInUserSessionNode,
                              UINT1                  u1TcpUpdateFlag); 

/* REVISIT-FWL MIB objects to be added for these protos */


INT1 nmhSetFwlLogTrigger PROTO ((UINT4 u4Interface, UINT1 u1LogTrigger,
                                UINT1 *pu1AclName, UINT4 u4Direction));

INT1 nmhSetFwlFragAction PROTO ((UINT4 u4Interface, UINT1 u1FragAction,
                          UINT1 *pu1AclName, UINT4 u4Direction));
INT4
FwlDeleteTcpInitFlowOnDemand PROTO ((tStatefulSessionNode *pStateNode));
INT4 
FwlIsEntryInPartialList PROTO ((tStatefulSessionNode *pStateTableNode));
INT4
FwlDeleteStateTableEntryOnDemand PROTO ((tStatefulSessionNode *pNodeToDelete));
PUBLIC VOID FwlCleanAllSipEntry PROTO ((VOID));
INT4 
FwlCleanPartialNode PROTO ((tStatefulSessionNode *pPartialNode));
INT4
FwlAppNotifyStateTuple PROTO ((tStatefulSessionNode *pStateNode));

PUBLIC INT4
FwlDeleteAppPartialListNode PROTO ((tStatefulSessionNode *pPartialNode));

PUBLIC INT4
FwlAppMatchPartialNode PROTO((UINT4 u4LocIpAddr,UINT2 u2LocPort,
                              UINT4 u4RemoteIpAddr, UINT2 u2RemotePort,
                              UINT1 u1Direction, UINT1 u1ProtoNum,
                              tStatefulSessionNode **ppPartialLinkNode));

tStatefulSessionNode *
FwlSearchStateTableEntry PROTO ((tStatefulSessionNode * pPartialMatchNode));
INT4
FwlAppUpdateStateEntryCallStatus PROTO ((tStatefulSessionNode 
                                         * pPartialMatchNode));


extern INT4         IpIsBroadCastLocal (UINT4 u4Addr);

extern INT1 nmhGetFirstIndexFsSyslogConfigTable ARG_LIST((INT4 *));
extern INT1 nmhGetFsSyslogConfigLogLevel ARG_LIST((INT4 ,INT4 *));
PUBLIC INT1 nmhGetFsSyslogLogging ARG_LIST((INT4 *));


PUBLIC INT4 VlanCfaCliGetIfList (INT1 *pi1IfName,
                                 INT1 *pi1IfListStr,
                                 UINT1 *pu1IfListArray,
                                 UINT4 u4IfListArrayLen);

PUBLIC INT4 FwlUtilCallBack PROTO ((UINT4 u4Event));

INT4 FwlStateGetStatefulTableEntries PROTO ((UINT4 u4HashIndex,
                               tStatefulSessionNode ** ppStateFilterInfo));
INT4 FwlStateGetIpv6StatefulTableEntries PROTO ((UINT4 u4HashIndex,
                               tStatefulSessionNode ** ppStateFilterInfo));


INT4
FwlUtilGetIpAddress PROTO ((INT4 i4Afi, tSNMP_OCTET_STRING_TYPE * pOctetString,
                     tFwlIpAddr *pFwlIpAddr));

INT1 
FwlStatePortMatchExists (tStatefulSessionNode *pStateFilterInfo, 
                              INT4 i4FwlStateLocalPort, 
                              INT4 i4FwlStateRemotePort);
PUBLIC VOID FwlUtilGetTimeStr PROTO ((CHR1 *pc1TimeStr));

/* Firewall Log Funcion prototypes */

PUBLIC VOID FwlLogIpHeaderInfo PROTO ((UINT1 *pLogBuffer, tIpHeader *pIpHdr, 
                                       tHLInfo *pHLInfo, INT4 *pi4Size));
PUBLIC VOID FwlLogTcpHeaderInfo PROTO ((UINT1 *pLogBuffer, tHLInfo *pHLInfo, 
                                       INT4 *pi4Size, UINT1 u1ValidHdrFlag));
PUBLIC VOID FwlLogIcmpInfo PROTO ((UINT1 *pLogBuffer, tIcmpInfo *pIcmpHdr, 
                                   INT4 *pi4size, UINT1 u1ValidHdrFlag));
PUBLIC VOID FwlLogIpOptionsInfo PROTO ((UINT1 *pLogBuffer, tIpHeader *pIpHdr,
                                        INT4  *pi4Size));

/* Trap Related Function Prototypes */

VOID FwlSendTrapMessage PROTO ((UINT4 u4MsgType,
                                UINT1 *pu1FwlLogFileName));

PUBLIC VOID FwlTrapSendNotifications PROTO ((tFirewallTrapMsg *pFwlTrapMsg));

PUBLIC VOID IdsTrapSendNotifications PROTO ((tIdsTrapMsg *pIdsTrapMsg));

PUBLIC VOID IdsSendAttackPktTrap PROTO ((UINT1 *pu1BlackListIpAddr));

PUBLIC tSNMP_OID_TYPE* FwlMakeObjIdFromDotNew PROTO ((INT1 *pi1TextStr));

PUBLIC UINT1* FwlParseSubIdNew PROTO ((UINT1 *pu1TempPtr, 
                                       UINT4 *pu4Value));

INT4 FwlSetHwFilterCapabilities PROTO((UINT1 *pFwlHwFilterCapability));

/*****************************************************************************/
/*              End Of File fwlproto.h                                       */
/*****************************************************************************/
