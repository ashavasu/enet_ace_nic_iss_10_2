/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlbufif.h,v 1.8 2016/02/27 10:04:59 siva Exp $
 *
 * Description:This file contains constants, typedefs &   
 *             macros related to Buffer interface.        
 *
 *******************************************************************/
#ifndef _FWLBUFIF_H
#define _FWLBUFIF_H

/*****************************************************************************/
/*            Macros for defining respective PIDs (Pool Id)                  */
/*****************************************************************************/

#define  FWL_RULE_PID          gaFIREWALLMemPoolIds[MAX_FWL_RULES_INFO_SIZING_ID]
#define  FWL_FILTER_PID        gaFIREWALLMemPoolIds[MAX_FWL_FILTERS_INFO_SIZING_ID]
#define  FWL_ACL_INFO_PID      gaFIREWALLMemPoolIds[MAX_FWL_ACL_INFO_SIZING_ID]
#define  FWL_IFACE_PID         gaFIREWALLMemPoolIds[MAX_FWL_INTERFACE_INFO_SIZING_ID]
#define  FWL_LOG_FREE_PID      gaFIREWALLMemPoolIds[MAX_FWL_LOG_BUFFER_SIZING_ID]
#define  FWL_STATE_PID         gaFIREWALLMemPoolIds[MAX_FWL_STATEFUL_SESSION_SIZING_ID]
#define  FWL_TCP_INIT_FLOW_PID gaFIREWALLMemPoolIds[MAX_FWL_TCP_INIT_FLOWS_SIZING_ID]
#define  FWL_PARTIAL_LINKS_PID gaFIREWALLMemPoolIds[MAX_FWL_PARTIAL_LINKS_SIZING_ID]
#define  FWL_IN_SESS_PID       gaFIREWALLMemPoolIds[MAX_FWL_IN_USER_SESSION_SIZING_ID]
#define  FWL_OUT_SESS_PID      gaFIREWALLMemPoolIds[MAX_FWL_OUT_USER_SESSION_SIZING_ID]
#define  FWL_DMZ_PID           gaFIREWALLMemPoolIds[MAX_FWL_DMZ_INFO_SIZING_ID]
#define  FWL_IPV6_DMZ_PID      gaFIREWALLMemPoolIds[MAX_FWL_IPV6_DMZ_INFO_SIZING_ID]
#define  FWL_URL_FILTER_PID    gaFIREWALLMemPoolIds[MAX_FWL_URL_FILTERS_SIZING_ID]
#define  FWL_URL_LENGTH_PID    gaFIREWALLMemPoolIds[MAX_FWL_URL_LENGTH_NUM_SIZING_ID]
#define  FWL_PARTIAL_LOG_PID   gaFIREWALLMemPoolIds[MAX_FWL_PARTIAL_LOG_BLOCK_SIZING_ID]
#define  FWL_WEB_LOG_PID       gaFIREWALLMemPoolIds[MAX_FWL_WEB_LOG_BUFFER_SIZING_ID]
#define  FWL_BLACK_LIST_PID    gaFIREWALLMemPoolIds[MAX_FWL_BLACK_LIST_SIZING_ID]
#define  FWL_WHITE_LIST_PID    gaFIREWALLMemPoolIds[MAX_FWL_WHITE_LIST_SIZING_ID]
#define  FWL_SNMP_OCTETSTRING_PID gaFIREWALLMemPoolIds[MAX_FWL_SNMP_OCTETSTRING_SIZING_ID]
#define  FWL_CLI_FILTER_PID     gaFIREWALLMemPoolIds[MAX_FWL_CLI_FILTER_SIZING_ID]
#define  FWL_CLI_ACCESS_LIST_PID gaFIREWALLMemPoolIds[MAX_FWL_CLI_ACCESS_LIST_SIZING_ID]
#define  FWL_SNORK_ATTACK_PID  gaFIREWALLMemPoolIds[MAX_FWL_SNORK_ATTACK_SIZING_ID]
#define  FWL_RPF_CHECK_PID  gaFIREWALLMemPoolIds[MAX_FWL_RPF_CHECK_SIZING_ID]
#endif

/*****************************************************************************/
/*              End Of File fwlbufif.h                                       */
/*****************************************************************************/
