/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlextn.h,v 1.11 2016/02/27 10:04:59 siva Exp $
 *
 * Description: This file contains all global variable definitions.
 *
 *******************************************************************/
#ifndef _FWL_EXTN_H
#define _FWL_EXTN_H
extern CONST CHR1  *FwlCliErrString [];
/* Firewall global structure */
extern tFwlAclInfo       gFwlAclInfo;
extern tFwlRateLimit gFwlRateLimit[FWL_RATE_LIMIT_MAX_PORT];

/* Bufif specific variables listed here */
extern tFwlMemPoolId     gFwlMemPoolId;

/* Timer global list Id */
extern FS_ULONG             gu4FwlTmrListId;

/* Flag for Iface entry */
extern UINT1             gu1FwlIfaceFlag;

extern tFwlLogBuffer     *gpFwlStartLogBuffer;
extern tFwlLogBuffer     *gpFwlLastLogBuffer;
extern UINT1             gu1LogCount;
extern int               sock1Fd;
extern tTMO_SLL          FwlUrlFilterList;
extern tTmrAppTimer   gFwlStatefulTmr; /* for stateful inspection */

/* Pointer to TCP Init flow List */
extern tTMO_SLL          gFwlTcpInitFlowList; 
/* Pointer to Partial Links List */
extern tTMO_SLL          gFwlPartialLinksList;   /* Pointer to Partial Links List */

/* Global Firewall Session Table Information */
extern tStatefulSessionInfo gFwlStateInfo;  /* Global Firewall Session 
                                               Table Info */
extern tTMO_SLL             gFwlV6TcpInitFlowList;
extern tTMO_SLL             gFwlV6PartialLinksList;   /* Pointer to Partial Links List */
extern tStatefulSessionInfo gFwlV6StateInfo;  /* Global Firewall Session Table Info */

extern UINT1                gu1AclScheduleStatus;

extern UINT4 gu4NonStdFtpPort;

extern tTMO_SLL          gFwlDmzList;           /* DMZ List */ 
extern tTMO_SLL          gFwlIPv6DmzList;      /* IPv6 DMZ List */
extern tTMO_SLL          gFwlSnorkList;        /*Snork List*/
extern tTMO_SLL          gFwlRpfEntryList;        /*uRPF enabled List*/

extern tIfaceInfo  gIfaceInfo;

/* Logging size information */
extern UINT4               gu4FwlLogSizeThreshold;
extern UINT4               gu4FwlMaxLogSize;
extern CHR1                gau1FwlLogFile[256];

/* Related to IDS logging */
extern UINT4           gu4IdsMaxLogSize;
extern UINT4           gu4IdsLogThreshold;
extern CHR1            gau1IdsLogFile [IDS_FILENAME_LEN];
#endif /* End of FIREWALL_EXTN */

/*****************************************************************************/
/*                End of file fwlextn.h                                      */
/*****************************************************************************/
