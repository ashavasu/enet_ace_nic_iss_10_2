/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltcp.h,v 1.2 2011/05/30 14:50:36 siva Exp $
 *
 * Description:This file contains type definitions
 *             relating to Tcp State Handler Routines.
 *
 *******************************************************************/
#ifndef _FWLTCP_H
#define _FWLTCP_H

/* Defines for TCP ACK window checks */
#define FWL_TCP_MAX_ACK_WINDOW        1073741824 /*the maximum value of the 
                                                   total window available */
/* previous value is 66000 > 65535. The maximum window is 2^16 and maximum scaling factor is 14,
   so the FWL_TCP_MAX_ACK_WINDOW is 2^30 */ 

/* Macros for Sequence No. & Ack No. checks. */
#define FWL_SEQ_GE(x,y)   ( (INT4) ((x) - (y)) >= 0 )
#define FWL_SEQ_GT(x,y)   ( (INT4) ((x) - (y)) > 0 )

#define FWL_MAX_TCP_UPPER_THRESH 100
#define FWL_MAX_TCP_LOWER_THRESH 80
#define FWL_TCP_INIT_MAX_PER_NODE 30

#define FWL_TCP_INIT_THRESH_DEL_OLD 1
#define FWL_TCP_INIT_THRESH_DROP    2
#define  TCP_EOOL             0        /*End of option list*/
#define  TCP_NOOP             1        /*No operation*/
#define  TCP_MSS              2   
#define  TCP_SCALING          3        /*window scaling option */
#define  TCP_TIMESTAMP        8        /*Time Stamp option */

#define  TCP_EOOL_LEN             1        /*End of option list*/
#define  TCP_NOOP_LEN             1        /*No operation*/
#define  TCP_MSS_LEN              4  
#define  TCP_SCALING_LEN          3
#define  TCP_TIMESTAMP_LEN        10
#define  TCP_MIN_OPT_LENGTH       2
#define  TCP_MAX_WINDOW_SCALING   14  
#define  TCP_NO_WINDOW_SCALING    -1

/* Number of half open connections per node */

typedef struct NodeStatTag
{
   tFwlIpAddr   IpAddr;
   UINT4    u4Count;
} tNodeStat;

/* Function Porotypes */
UINT4 FwlTcpInitCheckThreshold PROTO((tFwlIpAddr LocalAddr, 
                               tStatefulSessionNode **ppInitFlowNode));
UINT4 FwlTcpInitDelCountPerNode PROTO((tFwlIpAddr u4LocalAddr));
INT1 FwlTcpExtractWindowScalingOption PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                 tIpHeader *pStateIpHdr));

#endif /* End for _FWLTCP_H  */

/*****************************************************************************/
/*                End of file fwltcp.h                                      */
/*****************************************************************************/

