/********************************************************************
 * Copyright (C)  2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlsnif.h,v 1.4 2011/07/26 09:54:02 siva Exp $
 *
 * Description:This file contains all constants related   
 *             to SNMP (configuration management).       
 *
 *******************************************************************/

#ifndef _FWLSNIF_H
#define _FWLSNIF_H

/*****************************************************************************/
/*         Maximum Length for Address and Mask,ports,etc.                    */
/*****************************************************************************/

#define  FWL_MAX_ADDR_LEN_MASK          24
#define  FWL_MAX_OPERATOR_LEN            5 

/*****************************************************************************/
/*                          SNMP RowStatus values                            */
/*****************************************************************************/

#define  FWL_ACTIVE                      1
#define  FWL_NOT_IN_SERVICE              2
#define  FWL_NOT_READY                   3
#define  FWL_CREATE_AND_GO               4
#define  FWL_CREATE_AND_WAIT             5
#define  FWL_DESTROY                     6

/*****************************************************************************/
/*                Different type of Access Period                            */
/*****************************************************************************/
#define  FWL_MINS                        1
#define  FWL_HOURS                       2
#define  FWL_DAYS                        3

/*****************************************************************************/
/*                   Default values for address,mask,ports,etc               */
/*****************************************************************************/

#define  FWL_DEFAULT_ADDRESS             0
#define  FWL_DEFAULT_PORT                0
#define  FWL_DEFAULT_PROTO             255
#define  FWL_DEFAULT_MASK       0xffffffff
#define  FWL_DEFAULT_COUNT               0
#define  FWL_DEFAULT_OFFSET              0
#define  FWL_DEFAULT_LENGTH              0
#define  FWL_DEFAULT_SEQ_NUM             0
#define  FWL_DEFAULT_ACL_TYPE            1
#define  FWL_DEFAULT_ACTION              0
#define  FWL_DEFAULT_SERVICES            0
#define  FWL_DEFAULT_TIMEOUT             0
#define  FWL_DEFAULT_DSCP                0
#define  FWL_DEFAULT_FLOWID              0

/*****************************************************************************/
/*                 Values used for String Comparison                         */
/*****************************************************************************/

#define  FWL_STRING_EQUAL   0    
#define  FWL_END_OF_STRING  '\0' 
#define  FWL_NULL_STRING    "\0" 

/*****************************************************************************/
/*       The Following constant is used to Specify the Global interface      */
/*****************************************************************************/

#define  FWL_GLOBAL_IFACE_NUM                 0

/*****************************************************************************/
/*                 Values used for Index in ACL Table                        */
/*****************************************************************************/

#define  FWL_IFACE_INDEX                      3
#define  FWL_ACL_NAME_INDEX                   3
#define  FWL_DEFAULT_INDEX                    4
#define  FWL_IN_TO_OUT_TRANSIT                1


/*****************************************************************************/
/*                 Values used for ICMP Type                                 */
/*****************************************************************************/

#define  FWL_ECHO_REPLY                       0
#define  FWL_DEST_UNREACHABLE                 3
#define  FWL_SOURCE_QUENCH                    4
#define  FWL_REDIRECT                         5
#define  FWL_ECHO_REQUEST                     8
#define  FWL_ROUTER_ADVERTISE                 9
#define  FWL_ROUTER_SOLICIT                  10
#define  FWL_TIME_EXCEEDED                   11
#define  FWL_PARAMETER_PROBLEM               12
#define  FWL_TIMESTAMP_REQUEST               13
#define  FWL_TIMESTAMP_REPLY                 14
#define  FWL_INFORMATION_REQUEST             15
#define  FWL_INFORMATION_REPLY               16
#define  FWL_ADDR_MASK_REQUEST               17
#define  FWL_ADDR_MASK_REPLY                 18

/*****************************************************************************/
/*                 Values used for ICMP Code                                  */
/*****************************************************************************/

#define  FWL_NETWORK_UNREACHABLE              0
#define  FWL_HOST_UNREACHABLE                 1
#define  FWL_PROTOCOL_UNREACHABLE             2
#define  FWL_PORT_UNREACHABLE                 3
#define  FWL_FRAGMENT_NEED                    4
#define  FWL_SOURCE_ROUTE_FAIL                5
#define  FWL_DEST_NETWORK_UNKNOWN             6
#define  FWL_DEST_HOST_UNKNOWN                7
#define  FWL_SRC_HOST_ISOLATED                8
#define  FWL_DEST_NETWORK_ADMIN_PROHIBITED    9
#define  FWL_DEST_HOST_ADMIN_PROHIBITED      10
#define  FWL_NETWORK_UNREACHABLE_TOS         11
#define  FWL_HOST_UNREACHABLE_TOS            12
#define  FWL_PKT_FILTERED                    13    /* Packet filtered */
#define  FWL_PREC_VIOLATION                  14    /* Precedence violation */
#define  FWL_PREC_CUTOFF                     15    /* Precedence cut off */


#define FWL_REDIR_NET                        0   /* Redirect network */
#define FWL_REDIR_HOST                       1   /* Redirect host */
#define FWL_REDIR_TOS_NET                    2   
#define FWL_REDIR_TOS_HOST                   3

/*****************************************************************************/
/*                 Macro definitions for String operations                   */
/*****************************************************************************/

#define  FWL_STRCMP   STRCMP 
#define  FWL_STRNCMP  STRNCMP 
#define  FWL_STRCAT   STRCAT 
#define  FWL_STRCPY   STRCPY 
#define  FWL_STRNCPY  STRNCPY 
#define  FWL_STRLEN   STRLEN 
#define  FWL_MEMCPY   MEMCPY 
#define  FWL_STRSTR   STRSTR 

/*****************************************************************************/
/*                 Macro definitions for Memory allocation and Free          */
/*****************************************************************************/

#define  FWL_FREE    MEM_FREE /* DSL_ADD free   */
#define  FWL_MALLOC(size)  MEM_MALLOC(size,void)

/*****************************************************************************/
/*              Values used for Concating nad Parsing the Address and Mask   */
/*****************************************************************************/

#define  FWL_SHIFT_VALUE                     24
#define  FWL_MASK_VALUE              0xff000000
#define  FWL_REMAINING_SHIFT_VALUE            8
#define  FWL_ASCII_VALUE                     48

/*****************************************************************************/
/*      Constants related to TCP Ack and Rst bits.                           */
/*****************************************************************************/

#define  FWL_TOS_ANY               0
#define  FWL_TOS_MAX_VALUE         7

/*****************************************************************************/
/*      Constants related to DSCP and flow id                                */
/*****************************************************************************/

#define  FWL_DSCP_MIN_VALUE        0
#define  FWL_DSCP_MAX_VALUE        63

#define  FWL_FLOW_ID_MIN_VALUE     0
#define  FWL_FLOW_ID_MAX_VALUE    1048575


#endif  /* End for _FWLSNIF_H */

/*****************************************************************************/
/*                 END of FILE fwlsnif.h                                     */
/*****************************************************************************/
