/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlsz.h,v 1.10 2016/02/27 10:04:59 siva Exp $
 *
 * Description: This file contains mempool creation/deletion prototype
 *              declarations in Firewall module.
 *********************************************************************/
#ifndef _FWLSZ_H_
#define _FWLSZ_H_
enum {
    MAX_FWL_ACL_INFO_SIZING_ID,
    MAX_FWL_DMZ_INFO_SIZING_ID,
    MAX_FWL_FILTERS_INFO_SIZING_ID,
    MAX_FWL_INTERFACE_INFO_SIZING_ID,
    MAX_FWL_IN_USER_SESSION_SIZING_ID,
    MAX_FWL_LOG_BUFFER_SIZING_ID,
    MAX_FWL_OUT_USER_SESSION_SIZING_ID,
    MAX_FWL_PARTIAL_LINKS_SIZING_ID,
    MAX_FWL_PARTIAL_LOG_BLOCK_SIZING_ID,
    MAX_FWL_RULES_INFO_SIZING_ID,
    MAX_FWL_STATEFUL_SESSION_SIZING_ID,
    MAX_FWL_TCP_INIT_FLOWS_SIZING_ID,
    MAX_FWL_URL_FILTERS_SIZING_ID,
    MAX_FWL_URL_LENGTH_NUM_SIZING_ID,
    MAX_FWL_WEB_LOG_BUFFER_SIZING_ID,
    MAX_FWL_BLACK_LIST_SIZING_ID,
    MAX_FWL_WHITE_LIST_SIZING_ID,
 MAX_FWL_IPV6_DMZ_INFO_SIZING_ID,
    MAX_FWL_PACKET_INFO_SIZING_ID,
 MAX_FWL_SNMP_OCTETSTRING_SIZING_ID,
 MAX_FWL_CLI_ACCESS_LIST_SIZING_ID,
 MAX_FWL_CLI_FILTER_SIZING_ID,
 MAX_FWL_SNORK_ATTACK_SIZING_ID,
 MAX_FWL_RPF_CHECK_SIZING_ID,
    FIREWALL_MAX_SIZING_ID
};


#ifdef  _FIREWALLSZ_C
tMemPoolId gaFIREWALLMemPoolIds[ FIREWALL_MAX_SIZING_ID];
INT4  FirewallSizingMemCreateMemPools(VOID);
VOID  FirewallSizingMemDeleteMemPools(VOID);
INT4  FirewallSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _FIREWALLSZ_C  */
extern tMemPoolId gaFIREWALLMemPoolIds[ ];
extern INT4  FirewallSizingMemCreateMemPools(VOID);
extern VOID  FirewallSizingMemDeleteMemPools(VOID);
extern INT4  FirewallSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _FIREWALLSZ_C  */


#ifdef  _FIREWALLSZ_C
tFsModSizingParams gaFsFIREWALLSizingParams [] = {
{ "tAclInfo", "MAX_FWL_ACL_INFO", sizeof(tAclInfo),MAX_FWL_ACL_INFO, MAX_FWL_ACL_INFO,0 },
{ "tFwlDmzInfo", "MAX_FWL_DMZ_INFO", sizeof(tFwlDmzInfo),MAX_FWL_DMZ_INFO, MAX_FWL_DMZ_INFO,0 },
{ "tFilterInfo", "MAX_FWL_FILTERS_INFO", sizeof(tFilterInfo),MAX_FWL_FILTERS_INFO, MAX_FWL_FILTERS_INFO,0 },
{ "tIfaceInfo", "MAX_FWL_INTERFACE_INFO", sizeof(tIfaceInfo),MAX_FWL_INTERFACE_INFO, MAX_FWL_INTERFACE_INFO,0 },
{ "tInUserSessionNode", "MAX_FWL_IN_USER_SESSION", sizeof(tInUserSessionNode),MAX_FWL_IN_USER_SESSION, MAX_FWL_IN_USER_SESSION,0 },
{ "tFwlLogBuffer", "MAX_FWL_LOG_BUFFER", sizeof(tFwlLogBuffer),MAX_FWL_LOG_BUFFER, MAX_FWL_LOG_BUFFER,0 },
{ "tOutUserSessionNode", "MAX_FWL_OUT_USER_SESSION", sizeof(tOutUserSessionNode),MAX_FWL_OUT_USER_SESSION, MAX_FWL_OUT_USER_SESSION,0 },
{ "tStatefulSessionNode", "MAX_FWL_PARTIAL_LINKS", sizeof(tStatefulSessionNode),MAX_FWL_PARTIAL_LINKS, MAX_FWL_PARTIAL_LINKS,0 },
{ "UINT1[FWL_PARTIAL_LOG_SIZE]", "MAX_FWL_PARTIAL_LOG_BLOCK", sizeof(UINT1[FWL_PARTIAL_LOG_SIZE]),MAX_FWL_PARTIAL_LOG_BLOCK, MAX_FWL_PARTIAL_LOG_BLOCK,0 },
{ "tRuleInfo", "MAX_FWL_RULES_INFO", sizeof(tRuleInfo),MAX_FWL_RULES_INFO, MAX_FWL_RULES_INFO,0 },
{ "tStatefulSessionNode", "MAX_FWL_STATEFUL_SESSION", sizeof(tStatefulSessionNode),MAX_FWL_STATEFUL_SESSION, MAX_FWL_STATEFUL_SESSION,0 },
{ "tStatefulSessionNode", "MAX_FWL_TCP_INIT_FLOWS", sizeof(tStatefulSessionNode),MAX_FWL_TCP_INIT_FLOWS, MAX_FWL_TCP_INIT_FLOWS,0 },
{ "tFwlUrlFilterInfo", "MAX_FWL_URL_FILTERS", sizeof(tFwlUrlFilterInfo),MAX_FWL_URL_FILTERS, MAX_FWL_URL_FILTERS,0 },
{ "UINT1[2048]", "MAX_FWL_URL_LENGTH_NUM", sizeof(UINT1[2048]),MAX_FWL_URL_LENGTH_NUM, MAX_FWL_URL_LENGTH_NUM,0 },
{ "UINT1[FWL_MAX_LOG_BUFFER]", "MAX_FWL_WEB_LOG_BUFFER", sizeof(UINT1[FWL_MAX_LOG_BUFFER]),MAX_FWL_WEB_LOG_BUFFER, MAX_FWL_WEB_LOG_BUFFER,0 },
{ "tFwlBlackListEntry", "MAX_FWL_BLACK_LIST_ENTRIES", sizeof(tFwlBlackListEntry), MAX_FWL_BLACK_LIST_ENTRIES, MAX_FWL_BLACK_LIST_ENTRIES,0 },
{ "tFwlWhiteListEntry", "MAX_FWL_WHITE_LIST_ENTRIES", sizeof(tFwlWhiteListEntry), MAX_FWL_WHITE_LIST_ENTRIES, MAX_FWL_WHITE_LIST_ENTRIES,0 },
{ "tFwlPacketInfo", "MAX_FWL_FILTERS_INFO", sizeof(tFwlPacketInfo),MAX_FWL_FILTERS_INFO, MAX_FWL_FILTERS_INFO,0 },
{ "tFwlIPv6DmzInfo", "MAX_FWL_IPV6_DMZ_INFO", sizeof(tFwlIPv6DmzInfo), MAX_FWL_IPV6_DMZ_INFO, MAX_FWL_IPV6_DMZ_INFO,0 },
{ "tIp6Addr", "MAX_FWL_IPV6_ADDR_DMZ_BLOCKS", sizeof(tIp6Addr), MAX_FWL_IPV6_ADDR_DMZ_BLOCKS, MAX_FWL_IPV6_ADDR_DMZ_BLOCKS,0},
{ "tFwlAccessList", "MAX_FWL_ACCESS_LIST_BLOCKS", sizeof(tFwlAccessList),MAX_FWL_ACCESS_LIST_BLOCKS, MAX_FWL_ACCESS_LIST_BLOCKS,0},
{ "tFwlFilterEntry","MAX_FWL_FILTERS_BLOCKS", sizeof(tFwlFilterEntry),MAX_FWL_FILTERS_BLOCKS,MAX_FWL_FILTERS_BLOCKS,0}, 
{ "tFwlSnorkInfo","MAX_FWL_SNORK_ATTACK_BLOCKS", sizeof(tFwlSnorkInfo),MAX_FWL_SNORK_ATTACK_BLOCKS,MAX_FWL_SNORK_ATTACK_BLOCKS,0}, 
{ "tFwlRpfEntryInfo","MAX_FWL_RPF_CHECK_BLOCKS", sizeof(tFwlRpfEntryInfo),MAX_FWL_RPF_CHECK_BLOCKS,MAX_FWL_RPF_CHECK_BLOCKS,0}, 
{"\0","\0",0,0,0,0}
};
#else  /*  _FIREWALLSZ_C  */
extern tFsModSizingParams gaFsFIREWALLSizingParams [];
#endif /*  _FIREWALLSZ_C  */

#endif /* _FWLSZ_H_ */
