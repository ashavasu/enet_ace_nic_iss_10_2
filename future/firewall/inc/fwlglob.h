/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlglob.h,v 1.12 2011/10/28 10:40:10 siva Exp $
 * Description: This file contains all extern declarations.
 *
 *******************************************************************/
#ifndef _FWL_GLOB_H
#define _FWL_GLOB_H
#if (defined _FWLINIT_C_) || (defined _FWLUSER_C) || (defined _FWLAPI_C_)
INT4                gi4FwlSysLogId;
tOsixSemId          gFwlSemId;
UINT4               gu4FwlLogSizeThreshold = FWL_DEF_LOG_SIZE_THRESH;
UINT4               gu4FwlMaxLogSize = FWL_LOGFILE_LIMIT_SIZE;
CHR1                gau1FwlLogFile[256]; 
UINT1               gau1FwlNpHwFilterCap[FWL_NUM_ATTACKS];

tRBTree         gFwlBlackList;
tRBTree         gFwlWhiteList;


#else
extern INT4                gi4FwlSysLogId;
extern tOsixSemId          gFwlSemId;
extern tRBTree         gFwlBlackList;
extern tRBTree         gFwlWhiteList;
extern UINT1               gau1FwlNpHwFilterCap[FWL_NUM_ATTACKS];
#endif
extern unFwlUtilCallBackEntry FWL_UTIL_CALL_BACK [FWL_CUST_MAX_CALL_BACK_EVENTS];

#endif /* End of FIREWALL_GLOB */

/*****************************************************************************/
/*                End of file fwlglob.h                                      */
/*****************************************************************************/
