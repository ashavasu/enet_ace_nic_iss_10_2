/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltmrif.h,v 1.1 2010/11/10 14:59:52 prabuc Exp $
 *
 * Description:This file contains the type definitions    
 *             for the timer related structures and about 
 *             the different constants that are used.  
 *
 *******************************************************************/
#ifndef _FWLTMRIF_H
#define _FWLTMRIF_H

/*****************************************************************************/
/*       Constants used for the timer types                                  */
/*****************************************************************************/

#define   FWL_TCP_INTERCEPT_TIMER       1
#define   FWL_IDLE_TIMER                2
#define   FWL_UDP_INTERCEPT_TIMER       3
#define   FWL_ICMP_INTERCEPT_TIMER      4

#define   FWL_IDLE_TIMEOUT             60 /* 1 Min Timer */
#define   FWL_DEFAULT_IDLE_TIMER_VALUE  30 /* Default Idle Timer value */
#define   FWL_MIN_IDLE_TIMER_VALUE      15 /* Min Idle Timer value */

#endif /* for _FWLTMRIF_H */

/*****************************************************************************/
/*       End of the file -- Fwltimerif.h                                     */
/*****************************************************************************/
