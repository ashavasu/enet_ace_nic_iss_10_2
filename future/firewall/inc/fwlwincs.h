/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlwincs.h,v 1.5 2014/11/07 12:13:47 siva Exp $
 *
 * Description:This file contains the include files needed for   
 *             firewall kernel wrapper functions
 *
 *******************************************************************/
#ifndef _FWL_W_INCS_H
#define _FWL_W_INCS_H
#include "fwlinc.h"
#include "fssnmp.h"
#include "cli.h"
#include "firewall.h"
#include "fwlwtdfs.h"
#include "fwlwdefs.h"
#include "secmod.h"
#include "arsec.h"
#include "fsfwllw.h"

#ifdef NPAPI_WANTED
#include "secnp.h"
#endif

int FwlNmhIoctl (unsigned long p);
#endif /* _FWL_W_INCS_H */
