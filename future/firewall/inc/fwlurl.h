/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlurl.h,v 1.1 2010/11/10 14:59:56 prabuc Exp $
 *
 * Description:  This file contains constants, typedefs &
 *               macros related to Buffer interface.
 *
 *******************************************************************/
#ifndef _FWLURL_H
#define _FWLURL_H


/*******************************************************************/
/*                Macros for URL filtering module                  */
/*******************************************************************/

#define FWL_MAX_METHOD_NAME_LEN  20

#endif
/*******************************************************************/
/*                     End Of File fwlurl.h                        */
/*******************************************************************/

