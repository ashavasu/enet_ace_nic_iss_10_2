/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlinc.h,v 1.15 2014/07/02 10:23:41 siva Exp $
 *
 * Description: This file contains the list of include files.
 *
 *******************************************************************/
#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "secmod.h"
#include "snmcdefn.h"
#include "snmccons.h"
#include "snmctdfs.h"
#include "snmcport.h"
#include "firewall.h"
#include "iss.h"
#include "fsvlan.h"
#include "vlannp.h"
#include "ip6util.h"
#include "utilcli.h"
#include "ids.h"
#include "secidscli.h"
#ifndef SECURITY_KERNEL_WANTED
#include "sys/types.h"
#include "sys/stat.h"
#endif
#ifdef NPAPI_WANTED
#include "npapi.h"
#endif
#include "fwlgen.h"

/* This file includes all variables used during porting */ 
#include "fwlport.h"

/* Firewall include files */
#include "fwldefn.h"

/* CHANGE1 : Some configuration variables are moved from fwldefn.h to
 * fwlconfig.h file.
 */
#include "fwlconfig.h"
#include "fwlbufif.h"

/* This include file contains All timer related variable */
#include "fwltmrif.h"

#include "trace.h"
#include "fwldebug.h"

#include "fwlsnif.h"
#include "fwltdfs.h"

#include "fwlif.h"
#include "fwlmacs.h"

#include "fwlextn.h"
#include "fwlproto.h"

#include "fsfwllw.h"
#include "fwlglob.h"
#include "fwlstat.h" 
#include "ip.h"
#include "fwltcp.h" 
#include  "fwlcli.h"
#include  "fwlsz.h"
#include "vcm.h"
#include "ids.h"

#include "trie.h"
#include "rtm6.h"
#include "rtm.h"
/*****************************************************************************/
/*       End of the file - fwlinc.h                                          */
/*****************************************************************************/
