/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlgen.h,v 1.2.4.1 2018/04/27 12:45:12 siva Exp $
 *
 * Description:This file maps the FSAP2 calls to FSAP1   
 *             calls.                                 
 *
 *******************************************************************/
/* The system time will be updated in pValue and *pValue will be assigned to the
 * variable where this macro been used */
#define   FWL_GET_SYS_TIME(pValue)   (OsixGetSysTime(pValue),*pValue)

/*****************************************************************************/
/*              End Of File fwlgen.h                                         */
/*****************************************************************************/
