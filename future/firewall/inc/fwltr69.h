/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwltr69.h,v 1.1 2010/11/10 14:59:54 prabuc Exp $
 *
 * Description:This file contains type definitions
 *             for firewall TR69
 *
 *******************************************************************/
#define TR_FWL_GLOBAL_STATUS_DISABLE 2
#define TR_FWL_GLOBAL_STATUS_ENABLE 1
#define TR_FWL_FALSE   0
#define TR_FWL_TRUE    1             

#define FWLDEFNFILTERTABLE_SRC_PORT FWL_MAX_PORT_LEN
#define FWLDEFNFILTERTABLE_DEST_ADDRESS FWL_MAX_ADDR_LEN
#define FWLDEFNFILTERTABLE_DEST_PORT FWL_MAX_PORT_LEN
#define FWLDEFNFILTERTABLE_FILTER_NAME FWL_MAX_FILTER_NAME_LEN
#define FWLDEFNFILTERTABLE_SRC_ADDRESS FWL_MAX_ADDR_LEN

#define FWLDEFNRULETABLE_RULE_NAME FWL_MAX_RULE_NAME_LEN
#define FWLDEFNRULETABLE_FILTER_SET FWL_MAX_FILTER_SET_LEN

#define FWLDEFNACLTABLE_ACL_NAME FWL_MAX_ACL_NAME_LEN
#define FWLDEFNACLTABLE_ACL_DIRECTION_IN 1
#define FWLDEFNACLTABLE_ACL_DIRECTION_OUT 2
#define FWL_DEF_PRIORITY_MIN 7000

#define TR_POPULATE_IF_ENTRY 1

#define FWLDEFNDMZTABLE_IP_INDEX 16

#define FWLURLFILTERTABLE_STRING FWL_MAX_URL_FILTER_STRING_LEN
/* ValMasks for InternetGatewayDevice.Firewall.fwlDefnFilterTable.0 */
#define FWL_FILTER_SRC_PORT_MASK     (1)
#define FWL_FILTER_DEST_ADDRESS_MASK (1 << 2)
#define FWL_FILTER_DEST_PORT_MASK    (1 << 3)
#define FWL_FILTER_ROW_STATUS_MASK   (1 << 4)
#define FWL_FILTER_RST_BIT_MASK      (1 << 5)
#define FWL_FILTER_TOS_MASK          (1 << 6)
#define FWL_FILTER_FILTER_NAME_MASK  (1 << 7)
#define FWL_FILTER_SRC_ADDRESS_MASK  (1 << 8)
#define FWL_FILTER_ACK_BIT_MASK      (1 << 9)
#define FWL_FILTER_HITS_COUNT_MASK   (1 << 10)
#define FWL_FILTER_PROTOCOL_MASK     (1 << 11)

#define FWL_DEFN_FILTER_TABLE_VAL_MASK FWL_FILTER_FILTER_NAME_MASK

/* ValMasks for InternetGatewayDevice.Firewall.fwlDefnRuleTable.0 */
#define FWL_RULE_RULE_NAME_MASK  (1)
#define FWL_RULE_FILTER_SET_MASK (1 << 2)
#define FWL_RULE_ROW_STATUS_MASK (1 << 3)

#define FWL_DEFN_RULE_TABLE_VAL_MASK FWL_RULE_RULE_NAME_MASK\
                                    | FWL_RULE_FILTER_SET_MASK

/* ValMasks for InternetGatewayDevice.Firewall.fwlDefnAclTable.0 */
#define FWL_ACL_DIRECTION_MASK       (1)
#define FWL_ACL_FRAG_ACTION_MASK     (1 << 2)
#define FWL_ACL_LOG_TRIGGER_MASK     (1 << 3)
#define FWL_ACL_IF_INDEX_MASK        (1 << 4)
#define FWL_ACL_SEQUENCE_NUMBER_MASK (1 << 5)
#define FWL_ACL_ACTION_MASK          (1 << 6)
#define FWL_ACL_ACL_NAME_MASK        (1 << 7)
#define FWL_ACL_ROW_STATUS_MASK      (1 << 8)
#define FWL_ACL_FILTER_COMB_MASK     (1 << 9)

#define FWL_DEFN_ACL_TABLE_VAL_MASK ( FWL_ACL_DIRECTION_MASK \
                                    | FWL_ACL_IF_INDEX_MASK \
                                    | FWL_ACL_ACL_NAME_MASK \
                                    | FWL_ACL_FRAG_ACTION_MASK \
                                    | FWL_ACL_SEQUENCE_NUMBER_MASK \
                                    | FWL_ACL_ACTION_MASK \
                                    | FWL_ACL_FILTER_COMB_MASK )

/* ValMasks for InternetGatewayDevice.Firewall.fwlDefnIfTable.0 */
#define FWL_IF_IF_INDEX_MASK   (1)
#define FWL_IF_ROW_STATUS_MASK (1 << 2)
#define FWL_IF_IF_TYPE_MASK    (1 << 3)

#define FWL_DEFN_IF_TABLE_VAL_MASK FWL_IF_IF_INDEX_MASK

/* ValMasks for InternetGatewayDevice.Firewall.fwlDefnDmzTable.0 */
#define FWL_DMZ_IP_INDEX_MASK   (1)
#define FWL_DMZ_ROW_STATUS_MASK (1 << 2)

#define FWL_DEFN_DMZ_TABLE_VAL_MASK FWL_DMZ_IP_INDEX_MASK

/* ValMasks for InternetGatewayDevice.Firewall.fwlUrlFilterTable.0 */
#define FWL_URL_STRING_MASK            (1)
#define FWL_URL_HIT_COUNT_MASK         (1 << 2)
#define FWL_URL_FILTER_ROW_STATUS_MASK (1 << 3)

#define FWL_URL_FILTER_TABLE_VAL_MASK FWL_URL_STRING_MASK 
                                      
/* ValMasks for InternetGatewayDevice.Firewall.fwlStatIfTable.0 */
#define FWL_STAT_IF_IP_SPOOFED_PACKETS_DENIED_MASK    (1)
#define FWL_STAT_IF_SYN_PACKETS_DENIED_MASK           (1 << 2)
#define FWL_STAT_IF_PACKETS_ACCEPTED_MASK             (1 << 3)
#define FWL_STAT_IF_FRAGMENT_PACKETS_DENIED_MASK      (1 << 4)
#define FWL_STAT_IF_IF_INDEX_MASK                     (1 << 5)
#define FWL_STAT_IF_FILTER_COUNT_MASK                 (1 << 6)
#define FWL_STAT_IF_PACKETS_DENIED_MASK               (1 << 7)
#define FWL_STAT_IF_IP_OPTION_PACKETS_DENIED_MASK     (1 << 8)
#define FWL_STAT_IF_SRC_ROUTE_PACKETS_DENIED_MASK     (1 << 9)
#define FWL_STAT_IF_ICMP_PACKETS_DENIED_MASK          (1 << 10)
#define FWL_STAT_IF_TINY_FRAGMENT_PACKETS_DENIED_MASK (1 << 11)

#define FWL_STAT_IF_TABLE_VAL_MASK FWL_STAT_IF_IF_INDEX_MASK \
                         | FWL_STAT_IF_IP_SPOOFED_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_SYN_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_PACKETS_ACCEPTED_MASK \
                         | FWL_STAT_IF_FRAGMENT_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_FILTER_COUNT_MASK \
                         | FWL_STAT_IF_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_IP_OPTION_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_SRC_ROUTE_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_ICMP_PACKETS_DENIED_MASK \
                         | FWL_STAT_IF_TINY_FRAGMENT_PACKETS_DENIED_MASK

/* InternetGatewayDevice.Firewall */
typedef struct {
    UINT4 u4Trap;
    INT4  i4IpSpoofFiltering;
    INT4  i4ICMPType;
    INT4  i4MasterControlSwitch;
    INT4  i4TcpInterceptTimeout;
    INT4  i4TcpIntercept;
    INT4  i4Fragments;
    INT4  i4MaxFilters;
    INT4  i4NetBiosFiltering;
    INT4  i4UrlFiltering;
    INT4  i4TcpInterceptThreshold;
    INT4  i4IpOptions;
    INT4  i4ICMPControlSwitch;
    INT4  i4MaxRules;
} tFirewall;

/* InternetGatewayDevice.Firewall.fwlDefnFilterTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4NonMandValMask;
    UINT4 u4Visited;

    UINT1 au1SrcPort [FWLDEFNFILTERTABLE_SRC_PORT];
    UINT1 au1DestAddress [FWLDEFNFILTERTABLE_DEST_ADDRESS];
    UINT1 au1DestPort [FWLDEFNFILTERTABLE_DEST_PORT];
    UINT4 u4RowStatus;
    INT4  i4Tos;
    UINT1 au1FilterName [FWLDEFNFILTERTABLE_FILTER_NAME];
    UINT1 au1SrcAddress [FWLDEFNFILTERTABLE_SRC_ADDRESS];
    UINT4 u4HitsCount;
    INT4  i4Protocol;
                                              
} tFwlDefnFilterTable;

/* InternetGatewayDevice.Firewall.fwlDefnRuleTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1 au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT1 au1RuleName [FWLDEFNRULETABLE_RULE_NAME];
    UINT1 au1FilterSet [FWLDEFNRULETABLE_FILTER_SET];
    UINT4 u4RowStatus;
} tFwlDefnRuleTable;

/* InternetGatewayDevice.Firewall.fwlDefnAclTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4NonMandValMask;
    UINT4 u4Visited;
    INT4  i4Direction;
    INT4  i4FragAction;
    INT4  i4LogTrigger;
    INT4  i4IfIndex;
    INT4  i4SequenceNumber;
    INT4  i4Action;
    UINT1 au1AclName [FWLDEFNACLTABLE_ACL_NAME];
    UINT1 au1FilterComb [FWL_MAX_FILTER_SET_LEN];
    UINT4 u4RowStatus;
} tFwlDefnAclTable;

/* InternetGatewayDevice.Firewall.fwlDefnIfTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;
    UINT4 u4NonMandValMask;

    /* INDICES: */
    UINT4 u4Idx0;

    INT4  i4IfIndex;
    UINT4 u4RowStatus;
    INT4  i4IfType;
} tFwlDefnIfTable;

/* InternetGatewayDevice.Firewall.fwlDefnDmzTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    UINT4 u4Idx0;
    UINT4 u4IpIndex;
    UINT1 au1IpIndex [FWLDEFNDMZTABLE_IP_INDEX];
    UINT4 u4RowStatus;
} tFwlDefnDmzTable;

/* InternetGatewayDevice.Firewall.fwlUrlFilterTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1 au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT1 au1String [FWLURLFILTERTABLE_STRING];
    UINT4 u4HitCount;
    UINT4 u4RowStatus;
} tFwlUrlFilterTable;

/* InternetGatewayDevice.Firewall.fwlStatistics */
typedef struct {

    /* Indices not mapped in .inp file */

    UINT4 u4TotalSrcRoutePacketsDenied;
    UINT4 u4TotalPacketsAccepted;
    UINT4 u4TotalIpOptionPacketsDenied;
    UINT4 u4TotalSynPacketsDenied;
    UINT4 u4TotalFragmentedPacketsDenied;
    UINT4 u4InspectedPacketsCount;
    UINT4 u4TotalIcmpPacketsDenied;
    UINT4 u4TotalPacketsDenied;
    UINT4 u4TotalIpSpoofedPacketsDenied;
} tFwlStatistics;

/* InternetGatewayDevice.Firewall.fwlStatIfTable.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    UINT4 u4IfIpSpoofedPacketsDenied;
    UINT4 u4IfSynPacketsDenied;
    UINT4 u4IfPacketsAccepted;
    UINT4 u4IfFragmentPacketsDenied;
    INT4  i4IfIfIndex;
    INT4  i4IfFilterCount;
    UINT4 u4IfPacketsDenied;
    UINT4 u4IfIpOptionPacketsDenied;
    UINT4 u4IfSrcRoutePacketsDenied;
    UINT4 u4IfIcmpPacketsDenied;
    UINT4 u4IfTinyFragmentPacketsDenied;
}tFwlStatIfTable;

/*Prototype*/

INT4 TrScan_FwlDefnFilterTable PROTO ((void));
UINT4 TrScan_FwlDefnRuleTable PROTO ((void));
UINT4 TrScan_FwlDefnIfTable PROTO ((void));
UINT4 TrScan_FwlDefnDmzTable PROTO ((void));
UINT4 TrScan_FwlUrlFilterTable PROTO ((void));
UINT4 TrScan_FwlStatIfTable PROTO ((void));
UINT4 TrScan_FwlDefnAclTable PROTO ((void));
tFwlDefnFilterTable *TrAdd_DefnFilterTable (UINT4 u4TrIdx,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pOctetStrIdx);
tFwlDefnRuleTable  *TrAdd_DefnRuleTable (UINT4 u4TrIdx,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pOctetStrIdx);
tFwlDefnIfTable    *TrAdd_DefnIfTable (UINT4 u4TrIdx, UINT4 u4IfIdx,
                                       BOOL1 bPopulateIfEntry);

tFwlDefnDmzTable   *TrAdd_DefnDmzTable (UINT4 u4TrIdx, UINT4 u4IfIdx);

tFwlUrlFilterTable *TrAdd_UrlFilterTable (UINT4 u4TrIdx,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pOctetStrIdx);
tFwlStatIfTable    *TrAdd_FwlStatIfTable (UINT4 u4TrIdx, UINT4 u4IfIdx);

tFwlDefnAclTable   *TrAdd_FwlDefnAclTable (UINT4 u4TrIdx,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pOctetStrIdx, INT4 i4Idx0,
                                           INT4 i4Idx1);

INT4
TrSetNonMand_InternetGatewayDevice_Firewall (char *name,
                                              ParameterValue * value);
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name,
                                                                 ParameterValue
                                                                 * value,
                                                                 tFwlDefnFilterTable
                                                                 * pNode);
INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name,
                                                               ParameterValue *
                                                               value,
                                                               tFwlDefnRuleTable
                                                               * pNode);

INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnAclTable (char *name,
                                                              ParameterValue *
                                                              value,
                                                              tFwlDefnAclTable *
                                                              pNode);

INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnIfTable (char *name,
                                                             ParameterValue *
                                                             value,
                                                             tFwlDefnIfTable *
                                                             pNode);

INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name,
                                                              ParameterValue *
                                                              value,
                                                              tFwlDefnDmzTable *
                                                              pNode);

INT4
TrSetNonMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name,
                                                                ParameterValue *
                                                                value,
                                                                tFwlUrlFilterTable
                                                                * pNode);
INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name,
                                                              ParameterValue *
                                                              value,
                                                              tFwlDefnFilterTable
                                                              * pNode);

INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name,
                                                            ParameterValue *
                                                            value,
                                                            tFwlDefnRuleTable *
                                                            pNode);

INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnAclTable (char *name,
                                                           ParameterValue *
                                                           value,
                                                           tFwlDefnAclTable *
                                                           pNode);

INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnIfTable (char *name,
                                                          ParameterValue *
                                                          value,
                                                          tFwlDefnIfTable *
                                                          pNode);

INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name,
                                                           ParameterValue *
                                                           value,
                                                           tFwlDefnDmzTable *
                                                           pNode);

INT4
TrSetMand_InternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name,
                                                             ParameterValue *
                                                             value,
                                                             tFwlUrlFilterTable
                                                             * pNode);

INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name,
                                                             tFwlDefnFilterTable
                                                             * pNode,
                                                             ParameterValue *
                                                             value,
                                                             BOOL1
                                                             bTrDoSnmpSet);

INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name,
                                                           tFwlDefnRuleTable *
                                                           pNode,
                                                           ParameterValue *
                                                           value,
                                                           BOOL1 bTrDoSnmpSet);

INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnAclTable (char *name,
                                                          tFwlDefnAclTable *
                                                          pNode,
                                                          ParameterValue *
                                                          value,
                                                          BOOL1 bTrDoSnmpSet);

INT4   
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnIfTable (char
                                                          *name,
                                                          tFwlDefnIfTable
                                                          *
                                                          pNode,
                                                          ParameterValue
                                                          *
                                                          value,
                                                          BOOL1
                                                          bTrDoSnmpSet);

INT4
TrSetAll_InternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name,
                                                          tFwlDefnDmzTable *
                                                          pNode,
                                                          ParameterValue *
                                                          value,
                                                          BOOL1 bTrDoSnmpSet);

INT4
 TrSetAll_InternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name,
                                                            tFwlUrlFilterTable *
                                                            pNode,
                                                            ParameterValue *
                                                            value,
                                                            BOOL1 bTrDoSnmpSet);
INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnAclTable (int idx1);

INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnAclTable (int idx1);

INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnFilterTable (int idx1);

INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnFilterTable (int idx1);

INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnIfTable (int idx1);

INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnIfTable (int idx1);

INT4
TrDeleteInternetGatewayDevice_Firewall_fwlUrlFilterTable (int idx1);

INT4
TrInitInternetGatewayDevice_Firewall_fwlUrlFilterTable (int idx1);

INT4
TrDeleteInternetGatewayDevice_Firewall_fwlDefnDmzTable (int idx1);

INT4
TrInitInternetGatewayDevice_Firewall_fwlDefnDmzTable (int idx1);

int
TrSetInternetGatewayDevice_Firewall (char *name, ParameterValue * value);

int
TrSetInternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name, int idx1,
                                                        ParameterValue * value);

int
TrSetInternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name, int idx1,
                                                      ParameterValue * value);

int
TrSetInternetGatewayDevice_Firewall_fwlDefnAclTable (char *name, int idx1,
                                                     ParameterValue * value);

int
TrSetInternetGatewayDevice_Firewall_fwlDefnIfTable (char *name, int idx1,
                                                    ParameterValue * value);

int
TrSetInternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name, int idx1,
                                                     ParameterValue * value);

int
TrSetInternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name, int idx1,
                                                       ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall (char *name, ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlDefnFilterTable (char *name, int idx1,
                                                        ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlDefnRuleTable (char *name, int idx1,
                                                      ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlDefnAclTable (char *name, int idx1,
                                                     ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlDefnIfTable (char *name, int idx1,
                                                    ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlDefnDmzTable (char *name, int idx1,
                                                     ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlUrlFilterTable (char *name, int idx1,
                                                       ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlStatistics (char *name,
                                                   ParameterValue * value);

int
TrGetInternetGatewayDevice_Firewall_fwlStatIfTable (char *name, int idx1,
                                                    ParameterValue * value);

extern UINT4 FwlGlobalMaxFilters [];
extern UINT4 FwlGlobalTrap [];
extern UINT4 FwlGlobalSrcRouteFiltering [];
extern UINT4 FwlGlobalIpSpoofFiltering [];
extern UINT4 FwlIfICMPType [];
extern UINT4 FwlGlobalMasterControlSwitch [];
extern UINT4 FwlDefnInterceptTimeout [];
extern UINT4 FwlGlobalTcpIntercept [];
extern UINT4 FwlIfFragments [];
extern UINT4 FwlGlobalMaxFilters [];
extern UINT4 FwlGlobalNetBiosFiltering [];
extern UINT4 FwlGlobalUrlFiltering [];
extern UINT4 FwlDefnTcpInterceptThreshold [];
extern UINT4 FwlIfIpOptions [];
extern UINT4 FwlGlobalICMPControlSwitch [];
extern UINT4 FwlGlobalMaxRules [];
extern UINT4 FwlFilterSrcPort [];
extern UINT4 FwlFilterDestAddress [];
extern UINT4 FwlFilterDestPort [];
extern UINT4 FwlFilterRowStatus [];
extern UINT4 FwlFilterRstBit [];
extern UINT4 FwlFilterTos [];
extern UINT4 FwlFilterFilterName [];
extern UINT4 FwlFilterSrcAddress [];
extern UINT4 FwlFilterAckBit [];
extern UINT4 FwlFilterHitsCount [];
extern UINT4 FwlFilterProtocol [];
extern UINT4 FwlRuleRuleName [];
extern UINT4 FwlRuleFilterSet [];
extern UINT4 FwlRuleRowStatus [];
extern UINT4 FwlAclDirection [];
extern UINT4 FwlAclFragAction [];
extern UINT4 FwlAclLogTrigger [];
extern UINT4 FwlAclIfIndex [];
extern UINT4 FwlAclSequenceNumber [];
extern UINT4 FwlAclAction [];
extern UINT4 FwlAclAclName [];
extern UINT4 FwlAclRowStatus [];
extern UINT4 FwlIfIfIndex [];
extern UINT4 FwlIfRowStatus [];
extern UINT4 FwlIfIfType [];
extern UINT4 FwlDmzIpIndex [];
extern UINT4 FwlDmzRowStatus [];
extern UINT4 FwlUrlString [];
extern UINT4 FwlUrlHitCount [];
extern UINT4 FwlUrlFilterRowStatus [];
extern UINT4 FwlStatTotalSrcRoutePacketsDenied [];
extern UINT4 FwlStatTotalPacketsAccepted [];
extern UINT4 FwlStatTotalIpOptionPacketsDenied [];
extern UINT4 FwlStatTotalSynPacketsDenied [];
extern UINT4 FwlStatTotalFragmentedPacketsDenied [];
extern UINT4 FwlStatInspectedPacketsCount [];
extern UINT4 FwlStatTotalIcmpPacketsDenied [];
extern UINT4 FwlStatTotalPacketsDenied [];
extern UINT4 FwlStatTotalIpSpoofedPacketsDenied [];
extern UINT4 FwlStatIfIpSpoofedPacketsDenied [];
extern UINT4 FwlStatIfSynPacketsDenied [];
extern UINT4 FwlStatIfPacketsAccepted [];
extern UINT4 FwlStatIfFragmentPacketsDenied [];
extern UINT4 FwlStatIfIfIndex [];
extern UINT4 FwlStatIfFilterCount [];
extern UINT4 FwlStatIfPacketsDenied [];
extern UINT4 FwlStatIfIpOptionPacketsDenied [];
extern UINT4 FwlStatIfSrcRoutePacketsDenied [];
extern UINT4 FwlStatIfIcmpPacketsDenied [];
extern UINT4 FwlStatIfTinyFragmentPacketsDenied [];
extern UINT1 FwlDefnDmzTableINDEX [];
extern UINT1 FwlDefnRuleTableINDEX [];
extern UINT1 FwlDefnIfTableINDEX [];
extern UINT1 FwlDefnAclTableINDEX [];
extern UINT1 FwlStatIfTableINDEX [];
extern UINT1 FsVpnTableINDEX [];
extern UINT1 FwlDefnAclTableINDEX [];
extern UINT1 FwlDefnFilterTableINDEX [];
extern UINT1 FwlUrlFilterTableINDEX [];
