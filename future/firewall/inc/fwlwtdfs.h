/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlwtdfs.h,v 1.10 2011/09/24 09:15:55 siva Exp $
 *
 * *******************************************************************/
#ifndef _FWLWTDFS_H
#define _FWLWTDFS_H
typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalMasterControlSwitch;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalMasterControlSwitch;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalICMPControlSwitch;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalICMPControlSwitch;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalIpSpoofFiltering;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalIpSpoofFiltering;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalSrcRouteFiltering;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalSrcRouteFiltering;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalTinyFragmentFiltering;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalTinyFragmentFiltering;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalTcpIntercept;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalTcpIntercept;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalTrap;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalTrap;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalTrace;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalTrace;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalDebug;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalDebug;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalMaxFilters;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalMaxFilters;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalMaxRules;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalMaxRules;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalUrlFiltering;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalUrlFiltering;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlNetBiosFiltering;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalNetBiosFiltering;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlNetBiosLan2Wan;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalNetBiosLan2Wan;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlGlobalLogFileSize;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlGlobalLogFileSize;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlGlobalLogSizeThreshold;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlGlobalLogSizeThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlGlobalIdsLogSize;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlGlobalIdsLogSize;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlGlobalIdsLogThreshold;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlGlobalIdsLogThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pi4RetValFwlGlobalIdsStatus;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlGlobalIdsStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlTrapFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlTrapFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlIdsTrapFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlIdsTrapFileName;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalMasterControlSwitch;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalMasterControlSwitch;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalICMPControlSwitch;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalICMPControlSwitch;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalIpSpoofFiltering;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalIpSpoofFiltering;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalSrcRouteFiltering;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalSrcRouteFiltering;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalTinyFragmentFiltering;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalTinyFragmentFiltering;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalTcpIntercept;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalTcpIntercept;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalTrap;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalTrap;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalTrace;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalTrace;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalDebug;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalDebug;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalUrlFiltering;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalUrlFiltering;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalNetBiosFiltering;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalNetBiosFiltering;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalNetBiosLan2Wan;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalNetBiosLan2Wan;

typedef struct {
    int    cmd;
    UINT4  u4SetValFwlGlobalLogFileSize;
    INT4   cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlGlobalLogFileSize;

typedef struct {
    int    cmd;
    UINT4  u4SetValFwlGlobalLogSizeThreshold;
    INT4   cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlGlobalLogSizeThreshold;

typedef struct {
    int    cmd;
    INT4   i4SetValFwlGlobalIdsStatus;
    INT4   cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlGlobalIdsStatus;

typedef struct {
    int    cmd;
    UINT4  u4SetValFwlGlobalIdsLogSize;
    INT4   cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlGlobalIdsLogSize;

typedef struct {
    int    cmd;
    UINT4  u4SetValFwlGlobalIdsLogThreshold;
    INT4   cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlGlobalIdsLogThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalMasterControlSwitch;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalMasterControlSwitch;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalICMPControlSwitch;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalICMPControlSwitch;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalIpSpoofFiltering;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalIpSpoofFiltering;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalSrcRouteFiltering;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalSrcRouteFiltering;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalTinyFragmentFiltering;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalTcpIntercept;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalTcpIntercept;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalTrap;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalTrap;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalTrace;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalTrace;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalDebug;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalDebug;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalUrlFiltering;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalUrlFiltering;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalNetBiosFiltering;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalNetBiosFiltering;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalNetBiosLan2Wan;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    u4TestValFwlGlobalLogFileSize;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalLogFileSize;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    u4TestValFwlGlobalLogSizeThreshold;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalLogSizeThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    u4TestValFwlGlobalIdsLogSize;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalIdsLogSize;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    u4TestValFwlGlobalIdsLogThreshold;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalIdsLogThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    i4TestValFwlGlobalIdsStatus;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalIdsStatus;
typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlDefnTcpInterceptThreshold;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlDefnTcpInterceptThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlDefnInterceptTimeout;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlDefnInterceptTimeout;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlDefnTcpInterceptThreshold;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlDefnTcpInterceptThreshold;

typedef struct {
    int    cmd;
    UINT4  u4SetValFwlDefnInterceptTimeout;
    INT4    cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlDefnInterceptTimeout;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlDefnTcpInterceptThreshold;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlDefnTcpInterceptThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    u4TestValFwlDefnInterceptTimeout;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlDefnInterceptTimeout;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlDefnFilterTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlDefnFilterTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlFilterFilterName;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlDefnFilterTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlFilterSrcAddress;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterSrcAddress;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlFilterDestAddress;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterDestAddress;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterProtocol;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterProtocol;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlFilterSrcPort;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterSrcPort;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlFilterDestPort;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterDestPort;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterAckBit;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterAckBit;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterRstBit;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterRstBit;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterTos;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterTos;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterAccounting;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterAccounting;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterHitClear;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterHitClear;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    UINT4 *                    pu4RetValFwlFilterHitsCount;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterHitsCount;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlFilterSrcAddress;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterSrcAddress;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlFilterDestAddress;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterDestAddress;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterProtocol;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterProtocol;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlFilterSrcPort;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterSrcPort;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlFilterDestPort;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterDestPort;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterAckBit;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterAckBit;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterRstBit;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterRstBit;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterTos;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterTos;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterAccounting;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterAccounting;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterHitClear;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterHitClear;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlFilterSrcAddress;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterSrcAddress;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlFilterDestAddress;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterDestAddress;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterProtocol;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterProtocol;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlFilterSrcPort;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterSrcPort;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlFilterDestPort;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterDestPort;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterAckBit;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterAckBit;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterRstBit;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterRstBit;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterTos;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterTos;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterAccounting;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterAccounting;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterHitClear;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterHitClear;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlDefnRuleTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlDefnRuleTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlRuleRuleName;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlDefnRuleTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlRuleFilterSet;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlRuleFilterSet;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    INT4 *                     pi4RetValFwlRuleRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlRuleRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlRuleFilterSet;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlRuleFilterSet;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    INT4                       i4SetValFwlRuleRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlRuleRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlRuleFilterSet;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlRuleFilterSet;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlRuleRuleName;
    INT4                       i4TestValFwlRuleRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlRuleRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlDefnAclTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4 *                     pi4FwlAclDirection;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlDefnAclTable;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    INT4 *                     pi4NextFwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4NextFwlAclDirection;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlDefnAclTable;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4RetValFwlAclAction;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlAclAction;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4RetValFwlAclSequenceNumber;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlAclSequenceNumber;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4RetValFwlAclAclType;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlAclAclType;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4RetValFwlAclLogTrigger;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlAclLogTrigger;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4RetValFwlAclFragAction;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlAclFragAction;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4 *                     pi4RetValFwlAclRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlAclRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4SetValFwlAclAction;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlAclAction;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4SetValFwlAclSequenceNumber;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlAclSequenceNumber;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4SetValFwlAclLogTrigger;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlAclLogTrigger;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4SetValFwlAclFragAction;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlAclFragAction;

typedef struct {
    int                        cmd;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4SetValFwlAclRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlAclRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4TestValFwlAclAction;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlAclAction;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4TestValFwlAclSequenceNumber;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlAclSequenceNumber;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4TestValFwlAclLogTrigger;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlAclLogTrigger;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4TestValFwlAclFragAction;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlAclFragAction;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlAclIfIndex;
    tSNMP_OCTET_STRING_TYPE *  pFwlAclAclName;
    INT4                       i4FwlAclDirection;
    INT4                       i4TestValFwlAclRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlAclRowStatus;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhValidateIndexInstanceFwlDefnIfTable;

typedef struct {
    int     cmd;
    INT4 *  pi4FwlIfIfIndex;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFirstIndexFwlDefnIfTable;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4NextFwlIfIfIndex;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetNextIndexFwlDefnIfTable;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfIfType;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfIfType;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfIpOptions;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfIpOptions;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfFragments;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfFragments;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    UINT4 * pu4RetValFwlIfFragmentSize;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfFragmentSize;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfICMPType;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfICMPType;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfICMPCode;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfICMPCode;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfRowStatus;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfRowStatus;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfIfType;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfIfType;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfIpOptions;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfIpOptions;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfFragments;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfFragments;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    UINT4  u4SetValFwlIfFragmentSize;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfFragmentSize;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfICMPType;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfICMPType;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfICMPCode;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfICMPCode;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfRowStatus;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfRowStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfIfType;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfIfType;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfIpOptions;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfIpOptions;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfFragments;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfFragments;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    UINT4     u4TestValFwlIfFragmentSize;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfFragmentSize;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfICMPType;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfICMPType;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfICMPCode;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfICMPCode;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfRowStatus;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfRowStatus;

typedef struct {
    int    cmd;
    UINT4  u4FwlDmzIpIndex;
    INT4    cliWebSetError;
    INT1   rval;
} tFwlwnmhValidateIndexInstanceFwlDefnDmzTable;

typedef struct {
    int      cmd;
    UINT4 *  pu4FwlDmzIpIndex;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFirstIndexFwlDefnDmzTable;

typedef struct {
    int      cmd;
    UINT4    u4FwlDmzIpIndex;
    UINT4 *  pu4NextFwlDmzIpIndex;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetNextIndexFwlDefnDmzTable;

typedef struct {
    int     cmd;
    UINT4   u4FwlDmzIpIndex;
    INT4 *  pi4RetValFwlDmzRowStatus;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlDmzRowStatus;

typedef struct {
    int    cmd;
    UINT4  u4FwlDmzIpIndex;
    INT4   i4SetValFwlDmzRowStatus;
    INT4    cliWebSetError;
    INT1   rval;
} tFwlwnmhSetFwlDmzRowStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    UINT4    u4FwlDmzIpIndex;
    INT4     i4TestValFwlDmzRowStatus;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlDmzRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlUrlFilterTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlUrlFilterTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlUrlString;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlUrlFilterTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    UINT4 *                    pu4RetValFwlUrlHitCount;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlUrlHitCount;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    INT4 *                     pi4RetValFwlUrlFilterRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlUrlFilterRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    INT4                       i4SetValFwlUrlFilterRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlUrlFilterRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlUrlString;
    INT4                       i4TestValFwlUrlFilterRowStatus;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlUrlFilterRowStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatInspectedPacketsCount;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatInspectedPacketsCount;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalPacketsAccepted;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalPacketsAccepted;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalIcmpPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalIcmpPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalSynPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalSynPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalIpSpoofedPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalSrcRoutePacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalTinyFragmentPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalFragmentedPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalLargeFragmentPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalIpOptionPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatTotalAttacksPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatTotalAttacksPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatMemoryAllocationFailCount;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatMemoryAllocationFailCount;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlStatClear;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlStatClear;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlTrapThreshold;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlTrapThreshold;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlStatClear;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlStatClear;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlTrapThreshold;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlTrapThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlStatClear;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlStatClear;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlTrapThreshold;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlTrapThreshold;

typedef struct {
    int   cmd;
    INT4  i4FwlStatIfIfIndex;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhValidateIndexInstanceFwlStatIfTable;

typedef struct {
    int     cmd;
    INT4 *  pi4FwlStatIfIfIndex;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFirstIndexFwlStatIfTable;

typedef struct {
    int     cmd;
    INT4    i4FwlStatIfIfIndex;
    INT4 *  pi4NextFwlStatIfIfIndex;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetNextIndexFwlStatIfTable;

typedef struct {
    int     cmd;
    INT4    i4FwlStatIfIfIndex;
    INT4 *  pi4RetValFwlStatIfFilterCount;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlStatIfFilterCount;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfPacketsAccepted;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfPacketsAccepted;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfSynPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfSynPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIcmpPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIcmpPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIpSpoofedPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfSrcRoutePacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfTinyFragmentPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfFragmentPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfFragmentPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIpOptionPacketsDenied;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIpOptionPacketsDenied;

typedef struct {
    int     cmd;
    INT4    i4FwlStatIfIfIndex;
    INT4 *  pi4RetValFwlStatIfClear;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlStatIfClear;

typedef struct {
    int     cmd;
    INT4    i4FwlStatIfIfIndex;
    INT4 *  pi4RetValFwlIfTrapThreshold;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfTrapThreshold;

typedef struct {
    int   cmd;
    INT4  i4FwlStatIfIfIndex;
    INT4  i4SetValFwlStatIfClear;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlStatIfClear;

typedef struct {
    int   cmd;
    INT4  i4FwlStatIfIfIndex;
    INT4  i4SetValFwlIfTrapThreshold;
    INT4   cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfTrapThreshold;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlStatIfIfIndex;
    INT4     i4TestValFwlStatIfClear;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlStatIfClear;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlStatIfIfIndex;
    INT4     i4TestValFwlIfTrapThreshold;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfTrapThreshold;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlTrapMemFailMessage;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlTrapMemFailMessage;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pRetValFwlTrapAttackMessage;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlTrapAttackMessage;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlTrapMemFailMessage;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlTrapMemFailMessage;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pSetValFwlTrapAttackMessage;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlTrapAttackMessage;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlTrapMemFailMessage;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlTrapMemFailMessage;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pTestValFwlTrapAttackMessage;
    INT4                        cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlTrapAttackMessage;

typedef struct {
    int      cmd;
    UINT4    u4Interface;
    UINT1    u1LogTrigger;
    UINT1 *  pu1AclName;
    UINT4    u4Direction;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhSetFwlLogTrigger;

typedef struct {
    int      cmd;
    UINT4    u4Interface;
    UINT1    u1FragAction;
    UINT1 *  pu1AclName;
    UINT4    u4Direction;
    INT4      cliWebSetError;
    INT1     rval;
} tFwlwnmhSetFwlFragAction;

typedef struct {
    int  cmd;
} tFwlwFwlAddDefaultRules;

typedef struct {
    int    cmd;
    UINT4  u4Interface;
    UINT4  u4Status;
    INT1   rval;
} tFwlwFwlHandleInterfaceIndication;

typedef struct {
    int     cmd;
} tFwlwFwlCliCommit;

typedef struct {
    int    cmd;
    UINT4  u4PrintCount;
    UINT4  u4HashIndex;
    tStatefulSessionNode  StatefulEntry;
    UINT1  u1EntryFound;
    INT1   rval;
} tShowFwlStatefulFilters;

typedef struct {
    int     cmd;
    UINT1   au1LogString[FWL_MAX_LOG_BUF_SIZE];
    UINT1   u1Flag;
    UINT1   u1Pad[3];
} tShowFwlKernelLogs;

typedef struct {
    int  cmd;
    INT4  cliWebSetError;
} tNpwFwlCleanAllAppEntry;

#ifdef TR69_WANTED
typedef struct {
    int    cmd;
    UINT2  u2PortNo;
    INT4    cliWebSetError;
} tFwlSetTr69AcsPortNum;
#endif

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4                       i4FwlStateDirection;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlStateTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4FwlStateType;
    INT4 *                     pi4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4 *                     pi4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4 *                     pi4FwlStateLocalPort;
    INT4 *                     pi4FwlStateRemotePort;
    INT4 *                     pi4FwlStateProtocol;
    INT4 *                     pi4FwlStateDirection;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlStateTable;

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4 *                     pi4NextFwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    INT4 *                     pi4NextFwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    INT4 *                     pi4NextFwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4 *                     pi4NextFwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4 *                     pi4NextFwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4 *                     pi4NextFwlStateProtocol;
    INT4                       i4FwlStateDirection;
    INT4 *                     pi4NextFwlStateDirection;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlStateTable;

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4                       i4FwlStateDirection;
    UINT4 *                    pu4RetValFwlStateEstablishedTime;
    INT1                       rval;
} tFwlwnmhGetFwlStateEstablishedTime;

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4                       i4FwlStateDirection;
    INT4 *                     pi4RetValFwlStateLocalState;
    INT1                       rval;
} tFwlwnmhGetFwlStateLocalState;

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4                       i4FwlStateDirection;
    INT4 *                     pi4RetValFwlStateRemoteState;
    INT1                       rval;
} tFwlwnmhGetFwlStateRemoteState;

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4                       i4FwlStateDirection;
    INT4 *                     pi4RetValFwlStateLogLevel;
    INT1                       rval;
} tFwlwnmhGetFwlStateLogLevel;

typedef struct {
    int                        cmd;
    INT4                       i4FwlStateType;
    INT4                       i4FwlStateLocalIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateLocalIpAddress;
    INT4                       i4FwlStateRemoteIpAddrType;
    tSNMP_OCTET_STRING_TYPE *  pFwlStateRemoteIpAddress;
    INT4                       i4FwlStateLocalPort;
    INT4                       i4FwlStateRemotePort;
    INT4                       i4FwlStateProtocol;
    INT4                       i4FwlStateDirection;
    INT4 *                     pi4RetValFwlStateCallStatus;
    INT1                       rval;
} tFwlwnmhGetFwlStateCallStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT1              rval;
} tFwlwnmhDepv2FwlDefnWhiteListTable;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    UINT4                      u4FwlWhiteListIpMask;
    INT4                       i4TestValFwlWhiteListRowStatus;
    INT1                       rval;
} tFwlwnmhTestv2FwlWhiteListRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    UINT4                      u4FwlWhiteListIpMask;
    INT4                       i4SetValFwlWhiteListRowStatus;
    INT1                       rval;
} tFwlwnmhSetFwlWhiteListRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    UINT4                      u4FwlWhiteListIpMask;
    UINT4 *                    pu4RetValFwlWhiteListHitsCount;
    INT1                       rval;
} tFwlwnmhGetFwlWhiteListHitsCount;

typedef struct {
    int                        cmd;
    INT4                       i4FwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    UINT4                      u4FwlWhiteListIpMask;
    INT4 *                     pi4RetValFwlWhiteListRowStatus;
    INT1                       rval;
} tFwlwnmhGetFwlWhiteListRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlWhiteListIpAddressType;
    INT4 *                     pi4NextFwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlWhiteListIpAddress;
    UINT4                      u4FwlWhiteListIpMask;
    UINT4 *                    pu4NextFwlWhiteListIpMask;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlDefnWhiteListTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4FwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    UINT4 *                    pu4FwlWhiteListIpMask;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlDefnWhiteListTable;

typedef struct {
    int                        cmd;
    INT4                       i4FwlWhiteListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlWhiteListIpAddress;
    UINT4                      u4FwlWhiteListIpMask;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT1              rval;
} tFwlwnmhDepv2FwlDefnBlkListTable;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    INT4                       i4TestValFwlBlkListRowStatus;
    INT1                       rval;
} tFwlwnmhTestv2FwlBlkListRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    INT4                       i4SetValFwlBlkListRowStatus;
    INT1                       rval;
} tFwlwnmhSetFwlBlkListRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    INT4 *                     pi4RetValFwlBlkListRowStatus;
    INT1                       rval;
} tFwlwnmhGetFwlBlkListRowStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    INT4 *                     pi4RetValFwlBlkListEntryType;
    INT1                       rval;
} tFwlwnmhGetFwlBlkListEntryType;

typedef struct {
    int                        cmd;
    INT4                       i4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    UINT4 *                    pu4RetValFwlBlkListHitsCount;
    INT1                       rval;
} tFwlwnmhGetFwlBlkListHitsCount;

typedef struct {
    int                        cmd;
    INT4                       i4FwlBlkListIpAddressType;
    INT4 *                     pi4NextFwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    UINT4 *                    pu4NextFwlBlkListIpMask;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlDefnBlkListTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4 *                    pu4FwlBlkListIpMask;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlDefnBlkListTable;

typedef struct {
    int                        cmd;
    INT4                       i4FwlBlkListIpAddressType;
    tSNMP_OCTET_STRING_TYPE *  pFwlBlkListIpAddress;
    UINT4                      u4FwlBlkListIpMask;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable;

typedef struct {
    int      cmd;
    UINT1 *  pu1Buf;
    INT4     rval;
} tFwlwFwlHandleBlackListInfoFromIDS;


typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalICMPv6ControlSwitch;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalICMPv6ControlSwitch;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlGlobalIpv6SpoofFiltering;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlGlobalIpv6SpoofFiltering;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalICMPv6ControlSwitch;
    INT4     cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalICMPv6ControlSwitch;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlGlobalIpv6SpoofFiltering;
    INT4     cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlGlobalIpv6SpoofFiltering;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalICMPv6ControlSwitch;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlGlobalIpv6SpoofFiltering;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4     cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4     cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalLogFileSize;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalLogSizeThreshold;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalIdsLogSize;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalIdsLogThreshold;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlGlobalIdsStatus;
typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterAddrType;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterAddrType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    UINT4 *                    pu4RetValFwlFilterFlowId;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterFlowId;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4 *                     pi4RetValFwlFilterDscp;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlFilterDscp;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterAddrType;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterAddrType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    UINT4                      u4SetValFwlFilterFlowId;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterFlowId;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4SetValFwlFilterDscp;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlFilterDscp;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterAddrType;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterAddrType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    UINT4                      u4TestValFwlFilterFlowId;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterFlowId;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlFilterFilterName;
    INT4                       i4TestValFwlFilterDscp;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlFilterDscp;

typedef struct {
    int     cmd;
    INT4    i4FwlIfIfIndex;
    INT4 *  pi4RetValFwlIfICMPv6MsgType;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlIfICMPv6MsgType;

typedef struct {
    int   cmd;
    INT4  i4FwlIfIfIndex;
    INT4  i4SetValFwlIfICMPv6MsgType;
    INT4     cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlIfICMPv6MsgType;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlIfIfIndex;
    INT4     i4TestValFwlIfICMPv6MsgType;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlIfICMPv6MsgType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    tSNMP_OCTET_STRING_TYPE *  pNextFwlDmzIpv6Index;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4 *                     pi4RetValFwlDmzAddressType;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlDmzAddressType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4 *                     pi4RetValFwlDmzIpv6RowStatus;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhGetFwlDmzIpv6RowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4                       i4SetValFwlDmzAddressType;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlDmzAddressType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4                       i4SetValFwlDmzIpv6RowStatus;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhSetFwlDmzIpv6RowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4                       i4TestValFwlDmzAddressType;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlDmzAddressType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFwlDmzIpv6Index;
    INT4                       i4TestValFwlDmzIpv6RowStatus;
    INT4     cliWebSetError;
    INT1                       rval;
} tFwlwnmhTestv2FwlDmzIpv6RowStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4     cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlDefnIPv6DmzTable;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatIPv6InspectedPacketsCount;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIPv6InspectedPacketsCount;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatIPv6TotalPacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIPv6TotalPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatIPv6TotalPacketsAccepted;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatIPv6TotalIcmpPacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatIPv6TotalSpoofedPacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFwlStatIPv6TotalAttacksPacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFwlStatClearIPv6;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlStatClearIPv6;

typedef struct {
    int   cmd;
    INT4  i4SetValFwlStatClearIPv6;
    INT4     cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlStatClearIPv6;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFwlStatClearIIPv6;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlStatClearIPv6;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4     cliWebSetError;
    INT1              rval;
} tFwlwnmhDepv2FwlStatClearIPv6;

typedef struct {
    int     cmd;
    INT4    i4FwlStatIfIfIndex;
    INT4 *  pi4RetValFwlStatIfClearIPv6;
    INT4     cliWebSetError;
    INT1    rval;
} tFwlwnmhGetFwlStatIfClearIPv6;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIPv6PacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIPv6PacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIPv6PacketsAccepted;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIPv6PacketsAccepted;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIPv6IcmpPacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied;

typedef struct {
    int      cmd;
    INT4     i4FwlStatIfIfIndex;
    UINT4 *  pu4RetValFwlStatIfIPv6SpoofedPacketsDenied;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied;

typedef struct {
    int   cmd;
    INT4  i4FwlStatIfIfIndex;
    INT4  i4SetValFwlStatIfClearIPv6;
    INT4     cliWebSetError;
    INT1  rval;
} tFwlwnmhSetFwlStatIfClearIPv6;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4FwlStatIfIfIndex;
    INT4     i4TestValFwlStatIfClearIPv6;
    INT4     cliWebSetError;
    INT1     rval;
} tFwlwnmhTestv2FwlStatIfClearIPv6;

typedef struct {
    int cmd;
    UINT1* pu1HwFilterCapabilities;
    INT4   rval;
} tFwlwFwlSetHwFilterCapabilities;

union unFwlNmh {
    tFwlwnmhGetFwlGlobalMasterControlSwitch nmhGetFwlGlobalMasterControlSwitch;
    tFwlwnmhGetFwlGlobalICMPControlSwitch nmhGetFwlGlobalICMPControlSwitch;
    tFwlwnmhGetFwlGlobalIpSpoofFiltering nmhGetFwlGlobalIpSpoofFiltering;
    tFwlwnmhGetFwlGlobalSrcRouteFiltering nmhGetFwlGlobalSrcRouteFiltering;
    tFwlwnmhGetFwlGlobalTinyFragmentFiltering nmhGetFwlGlobalTinyFragmentFiltering;
    tFwlwnmhGetFwlGlobalTcpIntercept nmhGetFwlGlobalTcpIntercept;
    tFwlwnmhGetFwlGlobalTrap nmhGetFwlGlobalTrap;
    tFwlwnmhGetFwlGlobalTrace nmhGetFwlGlobalTrace;
    tFwlwnmhGetFwlGlobalDebug nmhGetFwlGlobalDebug;
    tFwlwnmhGetFwlGlobalMaxFilters nmhGetFwlGlobalMaxFilters;
    tFwlwnmhGetFwlGlobalMaxRules nmhGetFwlGlobalMaxRules;
    tFwlwnmhGetFwlGlobalUrlFiltering nmhGetFwlGlobalUrlFiltering;
    tFwlwnmhGetFwlGlobalNetBiosFiltering nmhGetFwlGlobalNetBiosFiltering;
    tFwlwnmhGetFwlGlobalNetBiosLan2Wan nmhGetFwlGlobalNetBiosLan2Wan;
    tFwlwnmhGetFwlGlobalLogFileSize nmhGetFwlGlobalLogFileSize;
    tFwlwnmhGetFwlGlobalLogSizeThreshold nmhGetFwlGlobalLogSizeThreshold;
    tFwlwnmhGetFwlGlobalIdsLogSize nmhGetFwlGlobalIdsLogSize;
    tFwlwnmhGetFwlGlobalIdsLogThreshold nmhGetFwlGlobalIdsLogThreshold;
    tFwlwnmhGetFwlGlobalIdsStatus nmhGetFwlGlobalIdsStatus;
    tFwlwnmhGetFwlTrapFileName nmhGetFwlTrapFileName;
    tFwlwnmhGetFwlIdsTrapFileName nmhGetFwlIdsTrapFileName;
    tFwlwnmhSetFwlGlobalMasterControlSwitch nmhSetFwlGlobalMasterControlSwitch;
    tFwlwnmhSetFwlGlobalICMPControlSwitch nmhSetFwlGlobalICMPControlSwitch;
    tFwlwnmhSetFwlGlobalIpSpoofFiltering nmhSetFwlGlobalIpSpoofFiltering;
    tFwlwnmhSetFwlGlobalSrcRouteFiltering nmhSetFwlGlobalSrcRouteFiltering;
    tFwlwnmhSetFwlGlobalTinyFragmentFiltering nmhSetFwlGlobalTinyFragmentFiltering;
    tFwlwnmhSetFwlGlobalTcpIntercept nmhSetFwlGlobalTcpIntercept;
    tFwlwnmhSetFwlGlobalTrap nmhSetFwlGlobalTrap;
    tFwlwnmhSetFwlGlobalTrace nmhSetFwlGlobalTrace;
    tFwlwnmhSetFwlGlobalDebug nmhSetFwlGlobalDebug;
    tFwlwnmhSetFwlGlobalUrlFiltering nmhSetFwlGlobalUrlFiltering;
    tFwlwnmhSetFwlGlobalNetBiosFiltering nmhSetFwlGlobalNetBiosFiltering;
    tFwlwnmhSetFwlGlobalNetBiosLan2Wan nmhSetFwlGlobalNetBiosLan2Wan;
    tFwlwnmhSetFwlGlobalLogFileSize nmhSetFwlGlobalLogFileSize;
    tFwlwnmhSetFwlGlobalLogSizeThreshold nmhSetFwlGlobalLogSizeThreshold;
    tFwlwnmhSetFwlGlobalIdsLogSize nmhSetFwlGlobalIdsLogSize;
    tFwlwnmhSetFwlGlobalIdsLogThreshold nmhSetFwlGlobalIdsLogThreshold;
    tFwlwnmhSetFwlGlobalIdsStatus nmhSetFwlGlobalIdsStatus;
    tFwlwnmhTestv2FwlGlobalMasterControlSwitch nmhTestv2FwlGlobalMasterControlSwitch;
    tFwlwnmhTestv2FwlGlobalICMPControlSwitch nmhTestv2FwlGlobalICMPControlSwitch;
    tFwlwnmhTestv2FwlGlobalIpSpoofFiltering nmhTestv2FwlGlobalIpSpoofFiltering;
    tFwlwnmhTestv2FwlGlobalSrcRouteFiltering nmhTestv2FwlGlobalSrcRouteFiltering;
    tFwlwnmhTestv2FwlGlobalTinyFragmentFiltering nmhTestv2FwlGlobalTinyFragmentFiltering;
    tFwlwnmhTestv2FwlGlobalTcpIntercept nmhTestv2FwlGlobalTcpIntercept;
    tFwlwnmhTestv2FwlGlobalTrap nmhTestv2FwlGlobalTrap;
    tFwlwnmhTestv2FwlGlobalTrace nmhTestv2FwlGlobalTrace;
    tFwlwnmhTestv2FwlGlobalDebug nmhTestv2FwlGlobalDebug;
    tFwlwnmhTestv2FwlGlobalUrlFiltering nmhTestv2FwlGlobalUrlFiltering;
    tFwlwnmhTestv2FwlGlobalNetBiosFiltering nmhTestv2FwlGlobalNetBiosFiltering;
    tFwlwnmhTestv2FwlGlobalNetBiosLan2Wan nmhTestv2FwlGlobalNetBiosLan2Wan;
    tFwlwnmhTestv2FwlGlobalLogFileSize nmhTestv2FwlGlobalLogFileSize;
    tFwlwnmhTestv2FwlGlobalLogSizeThreshold nmhTestv2FwlGlobalLogSizeThreshold;
    tFwlwnmhTestv2FwlGlobalIdsLogSize nmhTestv2FwlGlobalIdsLogSize;
    tFwlwnmhTestv2FwlGlobalIdsLogThreshold nmhTestv2FwlGlobalIdsLogThreshold;
    tFwlwnmhTestv2FwlGlobalIdsStatus nmhTestv2FwlGlobalIdsStatus;
    tFwlwnmhGetFwlDefnTcpInterceptThreshold nmhGetFwlDefnTcpInterceptThreshold;
    tFwlwnmhGetFwlDefnInterceptTimeout nmhGetFwlDefnInterceptTimeout;
    tFwlwnmhSetFwlDefnTcpInterceptThreshold nmhSetFwlDefnTcpInterceptThreshold;
    tFwlwnmhSetFwlDefnInterceptTimeout nmhSetFwlDefnInterceptTimeout;
    tFwlwnmhTestv2FwlDefnTcpInterceptThreshold nmhTestv2FwlDefnTcpInterceptThreshold;
    tFwlwnmhTestv2FwlDefnInterceptTimeout nmhTestv2FwlDefnInterceptTimeout;
    tFwlwnmhValidateIndexInstanceFwlDefnFilterTable nmhValidateIndexInstanceFwlDefnFilterTable;
    tFwlwnmhGetFirstIndexFwlDefnFilterTable nmhGetFirstIndexFwlDefnFilterTable;
    tFwlwnmhGetNextIndexFwlDefnFilterTable nmhGetNextIndexFwlDefnFilterTable;
    tFwlwnmhGetFwlFilterSrcAddress nmhGetFwlFilterSrcAddress;
    tFwlwnmhGetFwlFilterDestAddress nmhGetFwlFilterDestAddress;
    tFwlwnmhGetFwlFilterProtocol nmhGetFwlFilterProtocol;
    tFwlwnmhGetFwlFilterSrcPort nmhGetFwlFilterSrcPort;
    tFwlwnmhGetFwlFilterDestPort nmhGetFwlFilterDestPort;
    tFwlwnmhGetFwlFilterAckBit nmhGetFwlFilterAckBit;
    tFwlwnmhGetFwlFilterRstBit nmhGetFwlFilterRstBit;
    tFwlwnmhGetFwlFilterTos nmhGetFwlFilterTos;
    tFwlwnmhGetFwlFilterAccounting nmhGetFwlFilterAccounting;
    tFwlwnmhGetFwlFilterHitClear nmhGetFwlFilterHitClear;
    tFwlwnmhGetFwlFilterHitsCount nmhGetFwlFilterHitsCount;
    tFwlwnmhGetFwlFilterRowStatus nmhGetFwlFilterRowStatus;
    tFwlwnmhSetFwlFilterSrcAddress nmhSetFwlFilterSrcAddress;
    tFwlwnmhSetFwlFilterDestAddress nmhSetFwlFilterDestAddress;
    tFwlwnmhSetFwlFilterProtocol nmhSetFwlFilterProtocol;
    tFwlwnmhSetFwlFilterSrcPort nmhSetFwlFilterSrcPort;
    tFwlwnmhSetFwlFilterDestPort nmhSetFwlFilterDestPort;
    tFwlwnmhSetFwlFilterAckBit nmhSetFwlFilterAckBit;
    tFwlwnmhSetFwlFilterRstBit nmhSetFwlFilterRstBit;
    tFwlwnmhSetFwlFilterTos nmhSetFwlFilterTos;
    tFwlwnmhSetFwlFilterAccounting nmhSetFwlFilterAccounting;
    tFwlwnmhSetFwlFilterHitClear nmhSetFwlFilterHitClear;
 tFwlwnmhSetFwlFilterRowStatus nmhSetFwlFilterRowStatus;
    tFwlwnmhTestv2FwlFilterSrcAddress nmhTestv2FwlFilterSrcAddress;
    tFwlwnmhTestv2FwlFilterDestAddress nmhTestv2FwlFilterDestAddress;
    tFwlwnmhTestv2FwlFilterProtocol nmhTestv2FwlFilterProtocol;
    tFwlwnmhTestv2FwlFilterSrcPort nmhTestv2FwlFilterSrcPort;
    tFwlwnmhTestv2FwlFilterDestPort nmhTestv2FwlFilterDestPort;
    tFwlwnmhTestv2FwlFilterAckBit nmhTestv2FwlFilterAckBit;
    tFwlwnmhTestv2FwlFilterRstBit nmhTestv2FwlFilterRstBit;
    tFwlwnmhTestv2FwlFilterTos nmhTestv2FwlFilterTos;
    tFwlwnmhTestv2FwlFilterAccounting nmhTestv2FwlFilterAccounting;
    tFwlwnmhTestv2FwlFilterHitClear nmhTestv2FwlFilterHitClear;
 tFwlwnmhTestv2FwlFilterRowStatus nmhTestv2FwlFilterRowStatus;
    tFwlwnmhValidateIndexInstanceFwlDefnRuleTable nmhValidateIndexInstanceFwlDefnRuleTable;
    tFwlwnmhGetFirstIndexFwlDefnRuleTable nmhGetFirstIndexFwlDefnRuleTable;
    tFwlwnmhGetNextIndexFwlDefnRuleTable nmhGetNextIndexFwlDefnRuleTable;
    tFwlwnmhGetFwlRuleFilterSet nmhGetFwlRuleFilterSet;
    tFwlwnmhGetFwlRuleRowStatus nmhGetFwlRuleRowStatus;
    tFwlwnmhSetFwlRuleFilterSet nmhSetFwlRuleFilterSet;
    tFwlwnmhSetFwlRuleRowStatus nmhSetFwlRuleRowStatus;
    tFwlwnmhTestv2FwlRuleFilterSet nmhTestv2FwlRuleFilterSet;
    tFwlwnmhTestv2FwlRuleRowStatus nmhTestv2FwlRuleRowStatus;
    tFwlwnmhValidateIndexInstanceFwlDefnAclTable nmhValidateIndexInstanceFwlDefnAclTable;
    tFwlwnmhGetFirstIndexFwlDefnAclTable nmhGetFirstIndexFwlDefnAclTable;
    tFwlwnmhGetNextIndexFwlDefnAclTable nmhGetNextIndexFwlDefnAclTable;
    tFwlwnmhGetFwlAclAction nmhGetFwlAclAction;
    tFwlwnmhGetFwlAclSequenceNumber nmhGetFwlAclSequenceNumber;
    tFwlwnmhGetFwlAclAclType nmhGetFwlAclAclType;
    tFwlwnmhGetFwlAclLogTrigger nmhGetFwlAclLogTrigger;
    tFwlwnmhGetFwlAclFragAction nmhGetFwlAclFragAction;
    tFwlwnmhGetFwlAclRowStatus nmhGetFwlAclRowStatus;
    tFwlwnmhSetFwlAclAction nmhSetFwlAclAction;
    tFwlwnmhSetFwlAclSequenceNumber nmhSetFwlAclSequenceNumber;
    tFwlwnmhSetFwlAclLogTrigger nmhSetFwlAclLogTrigger;
    tFwlwnmhSetFwlAclFragAction nmhSetFwlAclFragAction;
    tFwlwnmhSetFwlAclRowStatus nmhSetFwlAclRowStatus;
    tFwlwnmhTestv2FwlAclAction nmhTestv2FwlAclAction;
    tFwlwnmhTestv2FwlAclSequenceNumber nmhTestv2FwlAclSequenceNumber;
    tFwlwnmhTestv2FwlAclLogTrigger nmhTestv2FwlAclLogTrigger;
    tFwlwnmhTestv2FwlAclFragAction nmhTestv2FwlAclFragAction;
    tFwlwnmhTestv2FwlAclRowStatus nmhTestv2FwlAclRowStatus;
    tFwlwnmhValidateIndexInstanceFwlDefnIfTable nmhValidateIndexInstanceFwlDefnIfTable;
    tFwlwnmhGetFirstIndexFwlDefnIfTable nmhGetFirstIndexFwlDefnIfTable;
    tFwlwnmhGetNextIndexFwlDefnIfTable nmhGetNextIndexFwlDefnIfTable;
    tFwlwnmhGetFwlIfIfType nmhGetFwlIfIfType;
    tFwlwnmhGetFwlIfIpOptions nmhGetFwlIfIpOptions;
    tFwlwnmhGetFwlIfFragments nmhGetFwlIfFragments;
    tFwlwnmhGetFwlIfFragmentSize nmhGetFwlIfFragmentSize;
    tFwlwnmhGetFwlIfICMPType nmhGetFwlIfICMPType;
    tFwlwnmhGetFwlIfICMPCode nmhGetFwlIfICMPCode;
    tFwlwnmhGetFwlIfRowStatus nmhGetFwlIfRowStatus;
    tFwlwnmhSetFwlIfIfType nmhSetFwlIfIfType;
    tFwlwnmhSetFwlIfIpOptions nmhSetFwlIfIpOptions;
    tFwlwnmhSetFwlIfFragments nmhSetFwlIfFragments;
    tFwlwnmhSetFwlIfFragmentSize nmhSetFwlIfFragmentSize;
    tFwlwnmhSetFwlIfICMPType nmhSetFwlIfICMPType;
    tFwlwnmhSetFwlIfICMPCode nmhSetFwlIfICMPCode;
    tFwlwnmhSetFwlIfRowStatus nmhSetFwlIfRowStatus;
    tFwlwnmhTestv2FwlIfIfType nmhTestv2FwlIfIfType;
    tFwlwnmhTestv2FwlIfIpOptions nmhTestv2FwlIfIpOptions;
    tFwlwnmhTestv2FwlIfFragments nmhTestv2FwlIfFragments;
    tFwlwnmhTestv2FwlIfFragmentSize nmhTestv2FwlIfFragmentSize;
    tFwlwnmhTestv2FwlIfICMPType nmhTestv2FwlIfICMPType;
    tFwlwnmhTestv2FwlIfICMPCode nmhTestv2FwlIfICMPCode;
    tFwlwnmhTestv2FwlIfRowStatus nmhTestv2FwlIfRowStatus;
    tFwlwnmhValidateIndexInstanceFwlDefnDmzTable nmhValidateIndexInstanceFwlDefnDmzTable;
    tFwlwnmhGetFirstIndexFwlDefnDmzTable nmhGetFirstIndexFwlDefnDmzTable;
    tFwlwnmhGetNextIndexFwlDefnDmzTable nmhGetNextIndexFwlDefnDmzTable;
    tFwlwnmhGetFwlDmzRowStatus nmhGetFwlDmzRowStatus;
    tFwlwnmhSetFwlDmzRowStatus nmhSetFwlDmzRowStatus;
    tFwlwnmhTestv2FwlDmzRowStatus nmhTestv2FwlDmzRowStatus;
    tFwlwnmhValidateIndexInstanceFwlUrlFilterTable nmhValidateIndexInstanceFwlUrlFilterTable;
    tFwlwnmhGetFirstIndexFwlUrlFilterTable nmhGetFirstIndexFwlUrlFilterTable;
    tFwlwnmhGetNextIndexFwlUrlFilterTable nmhGetNextIndexFwlUrlFilterTable;
    tFwlwnmhGetFwlUrlHitCount nmhGetFwlUrlHitCount;
    tFwlwnmhGetFwlUrlFilterRowStatus nmhGetFwlUrlFilterRowStatus;
    tFwlwnmhSetFwlUrlFilterRowStatus nmhSetFwlUrlFilterRowStatus;
    tFwlwnmhTestv2FwlUrlFilterRowStatus nmhTestv2FwlUrlFilterRowStatus;
    tFwlwnmhGetFwlStatInspectedPacketsCount nmhGetFwlStatInspectedPacketsCount;
    tFwlwnmhGetFwlStatTotalPacketsDenied nmhGetFwlStatTotalPacketsDenied;
    tFwlwnmhGetFwlStatTotalPacketsAccepted nmhGetFwlStatTotalPacketsAccepted;
    tFwlwnmhGetFwlStatTotalIcmpPacketsDenied nmhGetFwlStatTotalIcmpPacketsDenied;
    tFwlwnmhGetFwlStatTotalSynPacketsDenied nmhGetFwlStatTotalSynPacketsDenied;
    tFwlwnmhGetFwlStatTotalIpSpoofedPacketsDenied nmhGetFwlStatTotalIpSpoofedPacketsDenied;
    tFwlwnmhGetFwlStatTotalSrcRoutePacketsDenied nmhGetFwlStatTotalSrcRoutePacketsDenied;
    tFwlwnmhGetFwlStatTotalTinyFragmentPacketsDenied nmhGetFwlStatTotalTinyFragmentPacketsDenied;
    tFwlwnmhGetFwlStatTotalFragmentedPacketsDenied nmhGetFwlStatTotalFragmentedPacketsDenied;
    tFwlwnmhGetFwlStatTotalLargeFragmentPacketsDenied nmhGetFwlStatTotalLargeFragmentPacketsDenied;
    tFwlwnmhGetFwlStatTotalIpOptionPacketsDenied nmhGetFwlStatTotalIpOptionPacketsDenied;
    tFwlwnmhGetFwlStatTotalAttacksPacketsDenied nmhGetFwlStatTotalAttacksPacketsDenied;
    tFwlwnmhGetFwlStatMemoryAllocationFailCount nmhGetFwlStatMemoryAllocationFailCount;
    tFwlwnmhGetFwlStatClear nmhGetFwlStatClear;
    tFwlwnmhGetFwlTrapThreshold nmhGetFwlTrapThreshold;
    tFwlwnmhSetFwlStatClear nmhSetFwlStatClear;
    tFwlwnmhSetFwlTrapThreshold nmhSetFwlTrapThreshold;
    tFwlwnmhTestv2FwlStatClear nmhTestv2FwlStatClear;
    tFwlwnmhTestv2FwlTrapThreshold nmhTestv2FwlTrapThreshold;
    tFwlwnmhValidateIndexInstanceFwlStatIfTable nmhValidateIndexInstanceFwlStatIfTable;
    tFwlwnmhGetFirstIndexFwlStatIfTable nmhGetFirstIndexFwlStatIfTable;
    tFwlwnmhGetNextIndexFwlStatIfTable nmhGetNextIndexFwlStatIfTable;
    tFwlwnmhGetFwlStatIfFilterCount nmhGetFwlStatIfFilterCount;
    tFwlwnmhGetFwlStatIfPacketsDenied nmhGetFwlStatIfPacketsDenied;
    tFwlwnmhGetFwlStatIfPacketsAccepted nmhGetFwlStatIfPacketsAccepted;
    tFwlwnmhGetFwlStatIfSynPacketsDenied nmhGetFwlStatIfSynPacketsDenied;
    tFwlwnmhGetFwlStatIfIcmpPacketsDenied nmhGetFwlStatIfIcmpPacketsDenied;
    tFwlwnmhGetFwlStatIfIpSpoofedPacketsDenied nmhGetFwlStatIfIpSpoofedPacketsDenied;
    tFwlwnmhGetFwlStatIfSrcRoutePacketsDenied nmhGetFwlStatIfSrcRoutePacketsDenied;
    tFwlwnmhGetFwlStatIfTinyFragmentPacketsDenied nmhGetFwlStatIfTinyFragmentPacketsDenied;
    tFwlwnmhGetFwlStatIfFragmentPacketsDenied nmhGetFwlStatIfFragmentPacketsDenied;
    tFwlwnmhGetFwlStatIfIpOptionPacketsDenied nmhGetFwlStatIfIpOptionPacketsDenied;
    tFwlwnmhGetFwlStatIfClear nmhGetFwlStatIfClear;
    tFwlwnmhGetFwlIfTrapThreshold nmhGetFwlIfTrapThreshold;
    tFwlwnmhSetFwlStatIfClear nmhSetFwlStatIfClear;
    tFwlwnmhSetFwlIfTrapThreshold nmhSetFwlIfTrapThreshold;
    tFwlwnmhTestv2FwlStatIfClear nmhTestv2FwlStatIfClear;
    tFwlwnmhTestv2FwlIfTrapThreshold nmhTestv2FwlIfTrapThreshold;
    tFwlwnmhGetFwlTrapMemFailMessage nmhGetFwlTrapMemFailMessage;
    tFwlwnmhGetFwlTrapAttackMessage nmhGetFwlTrapAttackMessage;
    tFwlwnmhSetFwlTrapMemFailMessage nmhSetFwlTrapMemFailMessage;
    tFwlwnmhSetFwlTrapAttackMessage nmhSetFwlTrapAttackMessage;
    tFwlwnmhTestv2FwlTrapMemFailMessage nmhTestv2FwlTrapMemFailMessage;
    tFwlwnmhTestv2FwlTrapAttackMessage nmhTestv2FwlTrapAttackMessage;
    tFwlwnmhSetFwlLogTrigger nmhSetFwlLogTrigger;
    tFwlwnmhSetFwlFragAction nmhSetFwlFragAction;
    tFwlwFwlAddDefaultRules FwlAddDefaultRules;
    tFwlwFwlHandleInterfaceIndication FwlHandleInterfaceIndication;
    tFwlwFwlCliCommit FwlCliCommit;
    tShowFwlStatefulFilters FwlShowStatefulTable; 
    tShowFwlStatefulFilters FwlShowInitFlowTable; 
    tShowFwlStatefulFilters FwlCliShowHiddenPartialEntry; 
    tShowFwlKernelLogs      FwlShowKernelLogs;
    tNpwFwlCleanAllAppEntry FwlCleanAllAppEntry;
    tFwlwnmhDepv2FwlDefnWhiteListTable nmhDepv2FwlDefnWhiteListTable;
    tFwlwnmhTestv2FwlWhiteListRowStatus nmhTestv2FwlWhiteListRowStatus;
    tFwlwnmhSetFwlWhiteListRowStatus nmhSetFwlWhiteListRowStatus;
    tFwlwnmhGetFwlWhiteListHitsCount nmhGetFwlWhiteListHitsCount;
    tFwlwnmhGetFwlWhiteListRowStatus nmhGetFwlWhiteListRowStatus;
    tFwlwnmhGetNextIndexFwlDefnWhiteListTable nmhGetNextIndexFwlDefnWhiteListTable;
    tFwlwnmhGetFirstIndexFwlDefnWhiteListTable nmhGetFirstIndexFwlDefnWhiteListTable;
    tFwlwnmhValidateIndexInstanceFwlDefnWhiteListTable nmhValidateIndexInstanceFwlDefnWhiteListTable;
    tFwlwnmhDepv2FwlDefnBlkListTable nmhDepv2FwlDefnBlkListTable;
    tFwlwnmhTestv2FwlBlkListRowStatus nmhTestv2FwlBlkListRowStatus;
    tFwlwnmhSetFwlBlkListRowStatus nmhSetFwlBlkListRowStatus;
    tFwlwnmhGetFwlBlkListRowStatus nmhGetFwlBlkListRowStatus;
    tFwlwnmhGetFwlBlkListEntryType nmhGetFwlBlkListEntryType;
    tFwlwnmhGetFwlBlkListHitsCount nmhGetFwlBlkListHitsCount;
    tFwlwnmhGetNextIndexFwlDefnBlkListTable nmhGetNextIndexFwlDefnBlkListTable;
    tFwlwnmhGetFirstIndexFwlDefnBlkListTable nmhGetFirstIndexFwlDefnBlkListTable;
    tFwlwnmhValidateIndexInstanceFwlDefnBlkListTable nmhValidateIndexInstanceFwlDefnBlkListTable;
    tFwlwFwlHandleBlackListInfoFromIDS FwlHandleBlackListInfoFromIDS;
    tFwlwnmhValidateIndexInstanceFwlStateTable nmhValidateIndexInstanceFwlStateTable;
    tFwlwnmhGetFirstIndexFwlStateTable nmhGetFirstIndexFwlStateTable;
    tFwlwnmhGetNextIndexFwlStateTable nmhGetNextIndexFwlStateTable;
    tFwlwnmhGetFwlStateEstablishedTime nmhGetFwlStateEstablishedTime;
    tFwlwnmhGetFwlStateLocalState nmhGetFwlStateLocalState;
    tFwlwnmhGetFwlStateRemoteState nmhGetFwlStateRemoteState;
    tFwlwnmhGetFwlStateLogLevel nmhGetFwlStateLogLevel;
    tFwlwnmhGetFwlStateCallStatus nmhGetFwlStateCallStatus;

#ifdef TR69_WANTED
    tFwlSetTr69AcsPortNum FwlSetTr69AcsPortNum;
#endif
    tFwlwnmhGetFwlGlobalICMPv6ControlSwitch nmhGetFwlGlobalICMPv6ControlSwitch;
    tFwlwnmhGetFwlGlobalIpv6SpoofFiltering nmhGetFwlGlobalIpv6SpoofFiltering;
    tFwlwnmhSetFwlGlobalICMPv6ControlSwitch nmhSetFwlGlobalICMPv6ControlSwitch;
    tFwlwnmhSetFwlGlobalIpv6SpoofFiltering nmhSetFwlGlobalIpv6SpoofFiltering;
    tFwlwnmhTestv2FwlGlobalICMPv6ControlSwitch nmhTestv2FwlGlobalICMPv6ControlSwitch;
    tFwlwnmhTestv2FwlGlobalIpv6SpoofFiltering nmhTestv2FwlGlobalIpv6SpoofFiltering;
    tFwlwnmhDepv2FwlGlobalICMPv6ControlSwitch nmhDepv2FwlGlobalICMPv6ControlSwitch;
    tFwlwnmhDepv2FwlGlobalIpv6SpoofFiltering nmhDepv2FwlGlobalIpv6SpoofFiltering;
    tFwlwnmhDepv2FwlGlobalLogFileSize nmhDepv2FwlGlobalLogFileSize;
    tFwlwnmhDepv2FwlGlobalLogSizeThreshold nmhDepv2FwlGlobalLogSizeThreshold;
    tFwlwnmhDepv2FwlGlobalIdsLogSize nmhDepv2FwlGlobalIdsLogSize;
    tFwlwnmhDepv2FwlGlobalIdsLogThreshold nmhDepv2FwlGlobalIdsLogThreshold;
    tFwlwnmhGetFwlFilterAddrType nmhGetFwlFilterAddrType;
    tFwlwnmhGetFwlFilterFlowId nmhGetFwlFilterFlowId;
    tFwlwnmhGetFwlFilterDscp nmhGetFwlFilterDscp;
    tFwlwnmhSetFwlFilterAddrType nmhSetFwlFilterAddrType;
    tFwlwnmhSetFwlFilterFlowId nmhSetFwlFilterFlowId;
    tFwlwnmhSetFwlFilterDscp nmhSetFwlFilterDscp;
    tFwlwnmhTestv2FwlFilterAddrType nmhTestv2FwlFilterAddrType;
    tFwlwnmhTestv2FwlFilterFlowId nmhTestv2FwlFilterFlowId;
    tFwlwnmhTestv2FwlFilterDscp nmhTestv2FwlFilterDscp;
    tFwlwnmhGetFwlIfICMPv6MsgType nmhGetFwlIfICMPv6MsgType;
    tFwlwnmhSetFwlIfICMPv6MsgType nmhSetFwlIfICMPv6MsgType;
    tFwlwnmhTestv2FwlIfICMPv6MsgType nmhTestv2FwlIfICMPv6MsgType;
    tFwlwnmhValidateIndexInstanceFwlDefnIPv6DmzTable nmhValidateIndexInstanceFwlDefnIPv6DmzTable;
    tFwlwnmhGetFirstIndexFwlDefnIPv6DmzTable nmhGetFirstIndexFwlDefnIPv6DmzTable;
    tFwlwnmhGetNextIndexFwlDefnIPv6DmzTable nmhGetNextIndexFwlDefnIPv6DmzTable;
    tFwlwnmhGetFwlDmzAddressType nmhGetFwlDmzAddressType;
    tFwlwnmhGetFwlDmzIpv6RowStatus nmhGetFwlDmzIpv6RowStatus;
    tFwlwnmhSetFwlDmzAddressType nmhSetFwlDmzAddressType;
    tFwlwnmhSetFwlDmzIpv6RowStatus nmhSetFwlDmzIpv6RowStatus;
    tFwlwnmhTestv2FwlDmzAddressType nmhTestv2FwlDmzAddressType;
    tFwlwnmhTestv2FwlDmzIpv6RowStatus nmhTestv2FwlDmzIpv6RowStatus;
    tFwlwnmhDepv2FwlDefnIPv6DmzTable nmhDepv2FwlDefnIPv6DmzTable;
    tFwlwnmhGetFwlStatIPv6InspectedPacketsCount nmhGetFwlStatIPv6InspectedPacketsCount;
    tFwlwnmhGetFwlStatIPv6TotalPacketsDenied nmhGetFwlStatIPv6TotalPacketsDenied;
    tFwlwnmhGetFwlStatIPv6TotalPacketsAccepted nmhGetFwlStatIPv6TotalPacketsAccepted;
    tFwlwnmhGetFwlStatIPv6TotalIcmpPacketsDenied nmhGetFwlStatIPv6TotalIcmpPacketsDenied;
    tFwlwnmhGetFwlStatIPv6TotalSpoofedPacketsDenied nmhGetFwlStatIPv6TotalSpoofedPacketsDenied;
    tFwlwnmhGetFwlStatIPv6TotalAttacksPacketsDenied nmhGetFwlStatIPv6TotalAttacksPacketsDenied;
    tFwlwnmhGetFwlStatClearIPv6 nmhGetFwlStatClearIPv6;
    tFwlwnmhSetFwlStatClearIPv6 nmhSetFwlStatClearIPv6;
    tFwlwnmhTestv2FwlStatClearIPv6 nmhTestv2FwlStatClearIPv6;
    tFwlwnmhDepv2FwlStatClearIPv6 nmhDepv2FwlStatClearIPv6;
    tFwlwnmhGetFwlStatIfClearIPv6 nmhGetFwlStatIfClearIPv6;
    tFwlwnmhGetFwlStatIfIPv6PacketsDenied nmhGetFwlStatIfIPv6PacketsDenied;
    tFwlwnmhGetFwlStatIfIPv6PacketsAccepted nmhGetFwlStatIfIPv6PacketsAccepted;
    tFwlwnmhGetFwlStatIfIPv6IcmpPacketsDenied nmhGetFwlStatIfIPv6IcmpPacketsDenied;
    tFwlwnmhGetFwlStatIfIPv6SpoofedPacketsDenied nmhGetFwlStatIfIPv6SpoofedPacketsDenied;
    tFwlwnmhSetFwlStatIfClearIPv6 nmhSetFwlStatIfClearIPv6;
    tFwlwnmhTestv2FwlStatIfClearIPv6 nmhTestv2FwlStatIfClearIPv6;
    tFwlwFwlSetHwFilterCapabilities FwlSetHwFilterCapabilities;

    };
#endif 
