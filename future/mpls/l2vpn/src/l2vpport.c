/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpport.c,v 1.145 2017/09/27 13:45:31 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpport.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains porting related
 *                             external interface routines.
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"
#include "mplslsr.h"
#include "mplsnp.h"
#include "npapi.h"
#include "arp.h"
#include "cfa.h"
#include "ldpext.h"
#include "rtm.h"
#include "mplstdfs.h"
#include "mplsglob.h"
#include "mplcmndb.h"
#include "mplsred.h"
#include  "mplsnpwr.h"
#include  "cfanp.h"
INT4                L2VpnMplsILMHwAddForPopSearch (tInSegment * pInSegment,
                                                   BOOL1 bIsPsnBkpPath,
                                                   tPwVcEntry * pPwVcEntry,
                                                   VOID *pSlotInfo);

/*****************************************************************************/
/* Function Name : L2VpnRegisterWithMplsModule                               */
/* Description   : This function registers the Tunnel (associated with PwVc) */
/*                 with MPLS (TE module) to get notification when the        */
/*                 tunnel's operational status changes                       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1TnlDir - Direction of the associated tunnel             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnRegisterWithMplsModule (tPwVcEntry * pPwVcEntry, UINT1 u1TnlDir)
{
    INT4                i4Status = L2VPN_SUCCESS;
    UINT1               u1AssociateFlag = L2VPN_ONE;

    i4Status = L2VpnUpdateTeTnlAssociation (pPwVcEntry, u1TnlDir,
                                            u1AssociateFlag);

    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Registration/Association with Mpls module failed\r\n");
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnDeRegisterWithMplsModule                             */
/* Description   : This function de-registers the Tunnel (associated with    */
/*                 PwVc) with MPLS (TE module) when notification of the      */
/*                 tunnel's operational status changes are not required      */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1TnlDir - Direction of the associated tunnel             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDeRegisterWithMplsModule (tPwVcEntry * pPwVcEntry, UINT1 u1TnlDir)
{
    INT4                i4Status = L2VPN_SUCCESS;
    UINT1               u1AssociateFlag = L2VPN_ZERO;

    i4Status = L2VpnUpdateTeTnlAssociation (pPwVcEntry, u1TnlDir,
                                            u1AssociateFlag);

    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "De-Registration/Association with Mpls module failed\r\n");
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnGetTeLspStatus                                       */
/* Description   : This function posts a query to MPLS (TE module) to know   */
/*                 the operational status of the tunnel associated with a    */
/*                 PwVc Entry                                                */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1TnlDir - Direction of the associated tunnel             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnGetTeLspStatus (tPwVcEntry * pPwVcEntry, UINT1 u1TnlDir)
{
    INT4                i4OutStatus = L2VPN_SUCCESS;
    INT4                i4InStatus = L2VPN_SUCCESS;
    UINT4               u4MplsTnlLspId = L2VPN_ZERO;
    UINT4               u4TnlIndex = L2VPN_ZERO;
    UINT4               u4PeerLsr = L2VPN_ZERO;
    UINT4               u4LclLsr = L2VPN_ZERO;
    UINT4               u4TnlInst = L2VPN_ZERO;
    UINT4               u4TnlXcIndex = L2VPN_ZERO;
    UINT4               u4TnlIfIndex = L2VPN_ZERO;

    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;

    if ((u1TnlDir & L2VPN_PSN_TNL_OUT) == L2VPN_PSN_TNL_OUT)
    {
        pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                   L2VPN_PWVC_PSN_ENTRY
                                                   (pPwVcEntry));

        u4TnlIndex = pMplsTnlEntry->unTnlInfo.TeInfo.u4TnlIndex;
        CONVERT_TO_INTEGER (pMplsTnlEntry->unTnlInfo.TeInfo.TnlPeerLSR,
                            u4PeerLsr);
        CONVERT_TO_INTEGER (pMplsTnlEntry->unTnlInfo.TeInfo.TnlLclLSR,
                            u4LclLsr);
        u4PeerLsr = OSIX_NTOHL (u4PeerLsr);
        u4LclLsr = OSIX_NTOHL (u4LclLsr);

        if (TeGetActiveTnlInst (u4TnlIndex, &u4TnlInst, u4LclLsr, u4PeerLsr,
                                &u4TnlXcIndex, &u4TnlIfIndex) == TE_SUCCESS)
        {
            pPwVcEntry->u4PwL3Intf = u4TnlIfIndex;
        }

        pMplsTnlEntry->unTnlInfo.TeInfo.u2TnlInstance = (UINT2) u4TnlInst;

        i4OutStatus = L2VpnPostTeEvent (u4TnlIndex, (UINT2) u4TnlInst,
                                        u4LclLsr, u4PeerLsr,
                                        &u4MplsTnlLspId,
                                        L2VPN_ZERO, L2VPN_TE_TNL_STAT_REQ);

        if (i4OutStatus != L2VPN_SUCCESS)
        {
            pPwVcEntry->u1PsnTnlStatus &= (UINT1) (~L2VPN_PSN_TNL_OUT);
        }
        else
        {
            pPwVcEntry->u4LspId = u4MplsTnlLspId;
            pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_OUT;
        }
    }

    if ((u1TnlDir & L2VPN_PSN_TNL_IN) == L2VPN_PSN_TNL_IN)
    {
        pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

        u4TnlIndex = pMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex;

        CONVERT_TO_INTEGER (pMplsInTnlEntry->unInTnlInfo.TeInfo.TnlPeerLSR,
                            u4PeerLsr);
        CONVERT_TO_INTEGER (pMplsInTnlEntry->unInTnlInfo.TeInfo.TnlLclLSR,
                            u4LclLsr);

        u4PeerLsr = OSIX_NTOHL (u4PeerLsr);
        u4LclLsr = OSIX_NTOHL (u4LclLsr);

        u4TnlInst = L2VPN_ZERO;

        if (TeGetActiveTnlInst (u4TnlIndex, &u4TnlInst, u4LclLsr, u4PeerLsr,
                                &u4TnlXcIndex, &u4TnlIfIndex) == TE_FAILURE)
        {
            pPwVcEntry->u1PsnTnlStatus &= (UINT1) (~L2VPN_PSN_TNL_IN);
            return L2VPN_FAILURE;
        }

        pMplsInTnlEntry->u4LsrXcIndex = u4TnlXcIndex;
        pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance = (UINT2) u4TnlInst;

        i4InStatus = L2VpnPostTeEvent (u4TnlIndex, (UINT2) u4TnlInst,
                                       u4LclLsr, u4PeerLsr,
                                       &u4MplsTnlLspId,
                                       L2VPN_ZERO, L2VPN_TE_TNL_STAT_REQ);

        if (i4InStatus != L2VPN_SUCCESS)
        {
            pPwVcEntry->u1PsnTnlStatus &= (UINT1) (~L2VPN_PSN_TNL_IN);
        }
        else
        {
            pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_IN;
        }
    }

    if ((i4InStatus == L2VPN_FAILURE) || (i4OutStatus == L2VPN_FAILURE))
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnUpdateTeTnlAssociation                               */
/* Description   : This function posts an event to MPLS (TE module) for      */
/*                 associating/disassociating the tunnel to receive/not recv */
/*                 oper status changes                                       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1AssociateFlag - associate/disassociate flag             */
/*                 u1TnlDir - Direction of the associated tunnel             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdateTeTnlAssociation (tPwVcEntry * pPwVcEntry, UINT1 u1TnlDir,
                             UINT1 u1AssociateFlag)
{
    INT4                i4Status = L2VPN_FAILURE;
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    UINT4               u4TnlIndex = L2VPN_ZERO;
    UINT4               u4TnlIngress = L2VPN_ZERO;
    UINT4               u4TnlEgress = L2VPN_ZERO;
    UINT2               u2TnlInstance = L2VPN_ZERO;
    UINT4               u4TnlLspId = L2VPN_ZERO;

    if (u1TnlDir == L2VPN_TNL_OUT)
    {
        pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                   L2VPN_PWVC_PSN_ENTRY
                                                   (pPwVcEntry));

        u4TnlIndex = pMplsTnlEntry->unTnlInfo.TeInfo.u4TnlIndex;
        u2TnlInstance = pMplsTnlEntry->unTnlInfo.TeInfo.u2TnlInstance;

        CONVERT_TO_INTEGER ((pMplsTnlEntry->unTnlInfo.TeInfo.TnlLclLSR),
                            u4TnlIngress);
        CONVERT_TO_INTEGER ((pMplsTnlEntry->unTnlInfo.TeInfo.TnlPeerLSR),
                            u4TnlEgress);
    }
    else if (u1TnlDir == L2VPN_TNL_IN)
    {
        pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

        u4TnlIndex = pMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex;
        u2TnlInstance = pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance;

        CONVERT_TO_INTEGER ((pMplsInTnlEntry->unInTnlInfo.TeInfo.TnlLclLSR),
                            u4TnlIngress);
        CONVERT_TO_INTEGER ((pMplsInTnlEntry->unInTnlInfo.TeInfo.TnlPeerLSR),
                            u4TnlEgress);

    }

    i4Status = L2VpnPostTeEvent (u4TnlIndex, u2TnlInstance, u4TnlIngress,
                                 u4TnlEgress, &u4TnlLspId, u1AssociateFlag,
                                 L2VPN_TE_TNL_ASSOCIATE_REQ);

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnPostTeEvent                                          */
/* Description   : This function posts an event to MPLS (TE module)          */
/* Input(s)      : u4TnlIndex - Tunnel Index                                 */
/*                 u2TnlInstance - Tunnel instance                           */
/*                 u4TnlIngressLSRId - Tunnel Ingress addr                   */
/*                 u4TnlEgressLSRId - Tunnel Egress addr                     */
/*                 u1AssociateFlag - associate/disassociate flag             */
/*                 u4Event - Event type (Querry/association stat updation)   */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPostTeEvent (UINT4 u4TnlIndex,
                  UINT2 u2TnlInstance,
                  UINT4 u4TnlIngressLSRId,
                  UINT4 u4TnlEgressLSRId,
                  UINT4 *pMplsLspId, UINT1 u1AssociateFlag, UINT4 u4Event)
{
    tPwVcMplsTeTnlIndex PwVcMplsTeTnlIndex;

    UINT1              *pTmpMsg = NULL;
    INT4                i4Status = L2VPN_FAILURE;

    pTmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);
    if (pTmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Mem allocation failure - Te If pool\r\n");
        return L2VPN_FAILURE;
    }
    MEMSET (&PwVcMplsTeTnlIndex, L2VPN_ZERO, sizeof (tPwVcMplsTeTnlIndex));

    PwVcMplsTeTnlIndex.u4LspId = L2VPN_ZERO;
    PwVcMplsTeTnlIndex.u4TnlIndex = u4TnlIndex;
    PwVcMplsTeTnlIndex.u2TnlInstance = u2TnlInstance;

    MEMCPY ((UINT1 *) &PwVcMplsTeTnlIndex.TnlLclLSR,
            (UINT1 *) &u4TnlIngressLSRId, IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &PwVcMplsTeTnlIndex.TnlPeerLSR,
            (UINT1 *) &u4TnlEgressLSRId, IPV4_ADDR_LENGTH);
    PwVcMplsTeTnlIndex.u1AssociateFlag = u1AssociateFlag;

    MEMCPY (pTmpMsg, (UINT1 *) &PwVcMplsTeTnlIndex,
            sizeof (tPwVcMplsTeTnlIndex));

    i4Status = TeL2VpnEventHandler (pTmpMsg, u4Event);

    i4Status = ((i4Status == TE_SUCCESS) ? L2VPN_SUCCESS : L2VPN_FAILURE);

    if ((i4Status == L2VPN_MPLS_TE_TNL_STAT_UP) &&
        (((tPwVcMplsTeTnlIndex *) (VOID *) pTmpMsg)->u4LspId != L2VPN_ZERO))
    {
        *pMplsLspId = ((tPwVcMplsTeTnlIndex *) (VOID *) pTmpMsg)->u4LspId;
    }

    if ((MemReleaseMemBlock (L2VPN_Q_POOL_ID, (UINT1 *) pTmpMsg))
        == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Mem release failure - Te If pool\r\n");
        return L2VPN_FAILURE;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnGetNonTeLspStatus                                    */
/* Description   : This function posts a query to MPLS module to know        */
/*                 the operational status of the LSP associated with a       */
/*                 PwVc Entry                                                */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1TnlDir - Direction of the associated LSP                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnGetNonTeLspStatus (tPwVcEntry * pPwVcEntry, UINT1 u1TnlDir)
{
    UINT4               u4XcIndex = L2VPN_ZERO;
    UINT4               u4OutIfIndex = L2VPN_ZERO;
    UINT1               u1XcOwner = L2VPN_ZERO;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, 0, sizeof (tGenU4Addr));

    L2VPN_SUPPRESS_WARNING (u1TnlDir);
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "To process L2VpnGetNonTeLspStatus\r\n");

    pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                   L2VPN_PWVC_PSN_ENTRY
                                                   (pPwVcEntry));
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &(GenU4Addr.Addr.u4Addr),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (MplsGetNonTeLspInfoFromPrefix (&GenU4Addr, &u4XcIndex, &u4OutIfIndex,
                                       &u1XcOwner, MPLS_TRUE) == MPLS_SUCCESS)
    {
        L2VPN_PWVC_MPLS_TNL_XC_INDEX (pPwVcMplsTnlEntry) = u4XcIndex;
        return L2VPN_SUCCESS;
    }
#if 0
    printf ("vishalPW: %s : %d \n u4XcIndex=%d u4OutIfIndex=%d", __FUNCTION__,
            __LINE__, u4XcIndex, u4OutIfIndex);
#endif

    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpRegisterApplication                               */
/* Description   : Function to register L2VPN with LDP by providing call     */
/*                 back functions                                            */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpRegisterApplication (VOID)
{
    INT4                i4Status = L2VPN_SUCCESS;
#ifdef MPLS_SIG_WANTED
    UINT4               u4AppId;
    i4Status = LdpRegisterApplication (L2VPN_PWVC_FEC_TYPE, &u4AppId);
    if (i4Status == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Registration of FEC 128 type with LDP failed\r\n");
    }
    i4Status = LdpRegisterApplication (L2VPN_GEN_FEC_TYPE, &u4AppId);
    if (i4Status == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Registration of FEC 129 type with LDP failed\r\n");
    }

#endif
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpDeRegisterApplication                             */
/* Description   : Function to deregister L2VPN with LDP                     */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpDeRegisterApplication (VOID)
{
    INT4                i4Status = L2VPN_SUCCESS;

#ifdef MPLS_SIG_WANTED
    i4Status = LdpDeRegisterApplication (L2VPN_APP_L2VPN);
    if (i4Status == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "De-Registration of FEC 128 and FEC 129 type with LDP failed\r\n");
    }

#endif
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnRegisterWithIfModule                                 */
/* Description   : Function to register L2VPN with CFA                       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnRegisterWithIfModule (tPwVcEntry * pPwVcEntry)
{
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    /* check all ACs associated for UP status */
    pPwVcEnetServSpecEntry = (tPwVcEnetServSpecEntry *)
        L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);
    if (pPwVcEnetServSpecEntry != NULL)
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (RegisterWithIfModule (pPwVcEnetEntry->i4PortIfIndex,
                                      NULL) == L2VPN_FAILURE)
            {
                return L2VPN_FAILURE;
            }
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnDeRegisterWithIfModule                               */
/* Description   : Function to deregister L2VPN with CFA                     */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDeRegisterWithIfModule (tPwVcEntry * pPwVcEntry)
{
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    /* check all ACs associated for UP status */
    pPwVcEnetServSpecEntry = (tPwVcEnetServSpecEntry *)
        L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);
    if (pPwVcEnetServSpecEntry != NULL)
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (DeRegisterWithIfModule (pPwVcEnetEntry->i4PortIfIndex)
                == L2VPN_FAILURE)
            {
                return L2VPN_FAILURE;
            }
        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RegisterWithIfModule                                      */
/* Description   : Function to register L2VPN with CFA                       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
RegisterWithIfModule (INT4 i4PortIfIndex, INT4 (*pIfHandle) (VOID))
{
    L2VPN_SUPPRESS_WARNING (i4PortIfIndex);
    L2VPN_SUPPRESS_WARNING (pIfHandle);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : DeRegisterWithIfModule                                    */
/* Description   : Function to deregister L2VPN with CFA                     */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
DeRegisterWithIfModule (INT4 i4PortIfIndex)
{
    L2VPN_SUPPRESS_WARNING (i4PortIfIndex);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnFmPwVcSet                                            */
/* Description   : Used to set PWVC                                          */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
/* The following functions are used to update FM.  */
/* When entry is already present in the FM, it's modified, 
 * else New Entry created */
INT4
L2VpnFmPwVcSet (tPwVcEntry * pPwVcEntry)
{
    INT4                i4Status = L2VPN_SUCCESS;
    L2VPN_SUPPRESS_WARNING (pPwVcEntry);

    /* contruct PW Entry for FM and call the API function */
    /*i4Status = FM_API_PWVC_SET (PwVcEntry); */

    return i4Status;

}

/*****************************************************************************/
/* Function Name : L2VpnFmUpdatePwVcStatus                                   */
/* Description   : Function to update the status of PwVc with FM             */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1PwOperStatus - Operational status of the PwVc           */
/*                 u1OperApp  - Application which calls this function        */
/*                              Can be CPorMgmt or OAM.                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnFmUpdatePwVcStatus (tPwVcEntry * pPwVcEntry, UINT1 u1PwOperStatus,
                         UINT1 u1OperApp)
{
    return (L2VpnUpdatePwVcStatus (pPwVcEntry, u1PwOperStatus, NULL,
                                   u1OperApp));
}

/*****************************************************************************/
/* Function Name : L2VpnUpdatePwVcStatus                                     */
/* Description   : Function to update the status of PwVc with FM             */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1PwOperStatus - Operational status of the PwVc           */
/*                 pSInfo - Slot Information                                 */
/*                 u1OperApp - Application which calls this function         */
/*                             Can be CPorMgmt or OAM.                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdatePwVcStatus (tPwVcEntry * pPwVcEntry, UINT1 u1PwOperStatus,
                       VOID *pSlotInfo, UINT1 u1OperApp)
{
    INT4                i4Status = L2VPN_SUCCESS;
    BOOL1               bIsPwVcOperLlDown = FALSE;

    if (u1PwOperStatus == L2VPN_PWVC_OPER_LLDOWN)
    {
        bIsPwVcOperLlDown = TRUE;
        u1PwOperStatus = L2VPN_PWVC_DOWN;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "PSN Type is not configured for PW %d before add"
                    "into the HW\n", pPwVcEntry->u4PwVcIndex);
        return L2VPN_FAILURE;
    }

    if (u1OperApp != L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
    {
#ifdef SNMP_3_WANTED
        /* SNMP trap notification is sent when PW status changes to UP/DOWN */
        L2vpnSnmpPwSendTrap (pPwVcEntry->u4PwVcIndex, L2VPN_PW_STATUS_TRAP,
                             u1PwOperStatus);
#endif
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Update in HW is called by application other than "
                   "CPOrMgmt Plane - updation skipped\n");
        return L2VPN_SUCCESS;
    }

    L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                "Pseudowire (%d index): Local status: %x; Remote Status: %x;\n",
                pPwVcEntry->u4PwVcIndex, pPwVcEntry->u1LocalStatus,
                pPwVcEntry->u1RemoteStatus);

    switch (u1PwOperStatus)
    {
        case L2VPN_PWVC_UP:

            if (L2VpnUtilValidateHwStatus (pPwVcEntry, u1PwOperStatus)
                == L2VPN_SUCCESS)
            {
                return L2VPN_SUCCESS;
            }

            /* PWVC creation */
            if (L2VpnPwVcHwAdd (pPwVcEntry, pSlotInfo) == L2VPN_FAILURE)

            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "PWVC Add in HW for PW %d failed\n",
                            pPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }

            /* PWVC ILM creation */
            if (L2VpnPwVcIlmHwAdd (pPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s ILM Add in HW for PW %d failed\n",
                            __func__, pPwVcEntry->u4PwVcIndex);
                if (L2VpnPwVcHwDel (pPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "PWVC Delete in HW for PW %d failed\n",
                                pPwVcEntry->u4PwVcIndex);
                }
                return L2VPN_FAILURE;
            }
            break;
        case L2VPN_PWVC_DOWN:
            /* Intentional fall through */
        case L2VPN_PWVC_DELETE:

            if (L2VpnUtilValidateHwStatus (pPwVcEntry, u1PwOperStatus) ==
                L2VPN_SUCCESS)
            {
                return L2VPN_SUCCESS;
            }

            /* PWVC deletion */
            if (L2VpnPwVcHwDel (pPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "PWVC Delete in HW for PW %d failed\n",
                            pPwVcEntry->u4PwVcIndex);
                i4Status = L2VPN_FAILURE;
            }

            /* PWVC ILM deletion */
            if (pPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                if (L2VpnPwVcIlmHwDel (pPwVcEntry, bIsPwVcOperLlDown,
                                       pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "ILM Delete in HW for PW %d failed\n",
                                pPwVcEntry->u4PwVcIndex);
                    i4Status = L2VPN_FAILURE;
                }
            }

            if (i4Status == L2VPN_FAILURE)
            {
                return L2VPN_FAILURE;
            }

            if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
            {
                pPwVcEntry->u1HwStatus = L2VPN_FALSE;
            }
            break;
        default:
            /* No need to take any action */
            return L2VPN_SUCCESS;
    }

#ifdef SNMP_3_WANTED
    /* SNMP trap notification is sent when PW status changes to UP/DOWN */
    L2vpnSnmpPwSendTrap (pPwVcEntry->u4PwVcIndex, L2VPN_PW_STATUS_TRAP,
                         u1PwOperStatus);
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwVpnAcDel                                       */
/* Description   : Deletes the VPN AC from the hw, applicable for VPLS       */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwVpnAcDel (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListDelKey;
#endif
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4VplsInstance = L2VPN_ZERO;
    UINT4               u4Action;
    UINT4               u4PwTmpVcIndex = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&GenU4Addr, 0, sizeof (tGenU4Addr));
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwVpnAcDel Entry\n");

    if (L2VPN_PWVC_MODE (pPwVcEntry) != L2VPN_VPLS)
    {
        return L2VPN_SUCCESS;
    }
    if (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) == 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPLS index is not associated with the PW\n");
        return L2VPN_FAILURE;
    }
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
    if (pVplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "VPLS entry is NULL\n");
        return L2VPN_FAILURE;
    }
    if (L2VpnCheckAndGetLastOperUpPw
        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry), &u4PwTmpVcIndex) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN check and "
                   "last PW operational UP status get\n");
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
        /* It is not last PW so just set the AC del status */
        MEMSET (&L2VpnPwHwListDelKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
        L2VpnPwHwListDelKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VpnPwHwListDelKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

        (VOID) L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                    L2VPN_PWAC_NPAPI_SUCCESS |
                                    L2VPN_PWAC_NPAPI_CALLED);
#endif
        return L2VPN_SUCCESS;
    }

    /* 1. All the PWs are down in the VPLS instance
     * 2. This is last PW Up for deletion in the VPLS instance */

    if (!((u4PwTmpVcIndex == L2VPN_ZERO) ||
          (pPwVcEntry->u4PwVcIndex == u4PwTmpVcIndex)))
    {
        return L2VPN_SUCCESS;
    }
    u4Action = L2VPN_ZERO;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &(GenU4Addr.Addr.u4Addr),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
    /* Fill up the VPLS info */
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
    if (pVplsEntry != NULL)
    {
        MEMCPY ((UINT1 *) &u4VpnId,
                &(pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
                sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
        u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI =
            L2VPN_VPLS_VSI (pVplsEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VplsInstance =
            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
    }
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPN Id: %d VSI: %d\n", u4VpnId,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI);

    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
    {
        return L2VPN_SUCCESS;
    }
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    MEMSET (&L2VpnPwHwListDelKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
    L2VpnPwHwListDelKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    L2VpnPwHwListDelKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

    (VOID) L2VpnPwHwListDelete (&L2VpnPwHwListDelKey, L2VPN_PWAC_NPAPI_CALLED);
#endif
#endif
    TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                  ((tPwVcEnetServSpecEntry *)
                   L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                  pPwVcEnetEntry, tPwVcEnetEntry *)
    {
        if (pPwVcEnetEntry->u1HwStatus == MPLS_FALSE)
        {
            continue;
        }
        /* To check whether AC Port is Operationally up */
        if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex) ==
            L2VPN_FAILURE)
        {
            continue;
        }

        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan =
            (UINT4) L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort =
            (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i1PwEnetVlanMode =
            L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.u4PwAcId = pPwVcEnetEntry->u4PwAcId;
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4PwEnetPwVlan =
            L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
#ifdef MBSM_WANTED
        if (pSlotInfo != NULL)
        {
            if (MplsFsMplsMbsmHwDeleteVpnAc (u4VplsInstance, u4VpnId,
                                             &MplsHwVplsInfo, u4Action,
                                             pSlotInfo) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "MplsFsMplsMbsmHwDeleteVpnAc: "
                            "PW %d for Peer Ipv4 -%#x IPv6 -  %s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsMbsmHwDeleteVpnAc: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                return L2VPN_FAILURE;
            }
        }
        else
#endif
        {
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
                MplsHwVplsInfo.mplsHwVplsVcInfo.u1AppOwner =
                    L2VpPortGetPortOwnerFromIndex
                    (MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4AcIfIndex);
#ifdef NPAPI_WANTED

                if (MplsFsMplsHwDeleteVpnAc (u4VplsInstance, u4VpnId,
                                             &MplsHwVplsInfo,
                                             u4Action) == FNP_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwDeleteVpnAc: "
                                "PW %d for Peer IPv4- %#x IPv6 - %s failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwDeleteVpnAc: "
                                "PW %d for %#x failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                }
                else
                {
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "L2VpnPwVcHwVpnAcDel - AC of PW %d with "
                                "Port Vlan %d Port If Index %d "
                                "removed from HW\n",
                                pPwVcEnetEntry->pPwVcEntry->u4PwVcIndex,
                                L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry),
                                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry));
                }
#endif
            }
        }
        if (pPwVcEnetEntry->u4PwAcId != L2VPN_ZERO)
        {
            MplsL2VpnRelAcId (pPwVcEnetEntry->u4PwAcId);
            pPwVcEnetEntry->u4PwAcId = L2VPN_ZERO;
        }
        L2VpnUtilSetEnetHwStatus (L2VPN_PWVC_ENET_PORT_IF_INDEX
                                  (pPwVcEnetEntry),
                                  L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry),
                                  MPLS_FALSE, pPwVcEnetEntry->u4PwAcId);
    }
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    L2VpnPwHwListDelete (&L2VpnPwHwListDelKey, L2VPN_PWAC_NPAPI_SUCCESS);
#endif
#endif
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Vpls Instance: %d\n", u4VplsInstance);
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwVpnAcAdd                                       */
/* Description   : Creates the VPN AC from the hw, applicable for VPLS       */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwVpnAcAdd (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListEntry;
#endif
#endif
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4VplsInstance = L2VPN_ZERO;
    UINT4               u4Action;
    UINT4               u4PwAcId = L2VPN_ZERO;
    UINT1               u1PwAcFlag = L2VPN_FALSE;
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
#ifdef MPLS_IPV6_WANTED
    UNUSED_PARAM (u1PwAcFlag);
#endif
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwVpnAcAdd Entry\n");

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    u4Action = L2VPN_ZERO;
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));

    /* Fill up the VPLS info */
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
    if (pVplsEntry != NULL)
    {
        MEMCPY ((UINT1 *) &u4VpnId,
                &(pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
                sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
        u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "VPLS Instance: %d\n", u4VplsInstance);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI =
            L2VPN_VPLS_VSI (pVplsEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VplsInstance =
            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
    }

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPN Id: %d VSI: %d\n", u4VpnId,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI);

    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
    {
        return L2VPN_SUCCESS;
    }
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
    L2VpnPwHwListEntry.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    L2VpnPwHwListEntry.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

    L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_CALLED);

#endif
#endif
    TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                  ((tPwVcEnetServSpecEntry *)
                   L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                  pPwVcEnetEntry, tPwVcEnetEntry *)
    {
        if (pPwVcEnetEntry->u1HwStatus == MPLS_TRUE)
        {
            continue;
        }

        /* To check whether AC Port is Operationally up */
        if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex) ==
            L2VPN_FAILURE)
        {
            continue;
        }
        else
        {
            pPwVcEntry->u1LocalStatus &=
                (UINT1) (~L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT);
            pPwVcEntry->u1LocalStatus &=
                (UINT1) (~L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT);

            if (L2VpnSendLblMapOrNotifMsg (pPwVcEntry) == L2VPN_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Label Map or Notification Msg not sent for the "
                            "PW %d with Peer IPv4- %x IPv6 -%s\n",
                            pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Label Map or Notification Msg not sent for the "
                            "PW %d with Peer %x\n",
                            pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
                return L2VPN_FAILURE;
            }
        }

        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan =
            (UINT4) L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort =
            (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i1PwEnetVlanMode =
            L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
        MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4PwEnetPwVlan =
            L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
        /*Set the Unique identifier for Enet entry */
        if (pPwVcEnetEntry->u4PwAcId == L2VPN_ZERO)
        {
            u4PwAcId = MplsL2VpnGetAcId (void);
            if (MplsL2VpnSetAcId (u4PwAcId) != u4PwAcId)
            {
                return L2VPN_FAILURE;
            }
            pPwVcEnetEntry->u4PwAcId = u4PwAcId;
            MplsHwVplsInfo.mplsHwVplsVcInfo.u4PwAcId = u4PwAcId;
            u1PwAcFlag = L2VPN_TRUE;
        }
        else
        {
            MplsHwVplsInfo.mplsHwVplsVcInfo.u4PwAcId = pPwVcEnetEntry->u4PwAcId;
        }

#ifdef MBSM_WANTED
        if (pSlotInfo != NULL)
        {
            if (MplsFsMplsMbsmHwAddVpnAc (u4VplsInstance, u4VpnId,
                                          &MplsHwVplsInfo, u4Action,
                                          pSlotInfo) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsMbsmHwAddVpnAc: "
                            "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "MplsFsMplsMbsmHwAddVpnAc: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
                if (u1PwAcFlag == L2VPN_TRUE)
                {
                    MplsL2VpnRelAcId (u4PwAcId);
                }
#endif
                return L2VPN_FAILURE;
            }
        }
        else
#endif
        {
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2VpnPwVcHwVpnAcAdd - AC VLAN %d added\n",
                            MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.
                            u4VpnSrcVlan);
                MplsHwVplsInfo.mplsHwVplsVcInfo.u1AppOwner =
                    L2VpPortGetPortOwnerFromIndex
                    (MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4AcIfIndex);
#ifdef NPAPI_WANTED

                if (MplsFsMplsHwAddVpnAc (u4VplsInstance, u4VpnId,
                                          &MplsHwVplsInfo,
                                          u4Action) == FNP_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwAddVpnAc: "
                                "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwAddVpnAc: "
                                "PW %d for %#x failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                    MplsL2VpnRelAcId (u4PwAcId);
                    return L2VPN_FAILURE;
                }
                else
                {
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "L2VpnPwVcHwVpnAcAdd - AC of PW %d with "
                                "Port Vlan %d Port If Index %d added "
                                "in HW\n",
                                pPwVcEnetEntry->pPwVcEntry->u4PwVcIndex,
                                L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry),
                                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry));
                }
#endif
            }

            L2VpnUtilSetEnetHwStatus (L2VPN_PWVC_ENET_PORT_IF_INDEX
                                      (pPwVcEnetEntry),
                                      L2VPN_PWVC_ENET_PORT_VLAN
                                      (pPwVcEnetEntry), MPLS_TRUE, u4PwAcId);
        }
    }
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry) = u4PwAcId;
    L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_SUCCESS);
#endif
#endif
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "VPLS Instance is : %d \r\n",
                u4VplsInstance);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcIlmHwAdd                                         */
/* Description   : Adds the ILM entry into the hw                            */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcIlmHwAdd (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Action = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4LclLsr = 0;
    UINT4               u4PeerLsr = 0;
    UINT4               u4InIntf = 0;
    tVlanIfaceVlanId    VlanId;
    INT4                i4Vsi = 0;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif
    UINT4               u4PeerLsrId = 0;
    UINT4               u4LclLsrId = 0;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    UINT4               u4PrevOutLabel;
    UINT4               u4PrevInLabel;
    tL2VpnPwHwList      L2VpnPwHwListKey;
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tL2VpnPwHwList      L2VpnPwHwListDelKey;
#endif
#endif
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    MEMSET (&L2VpnPwHwListKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
    MEMSET (&L2VpnPwHwListDelKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
#endif
#endif

    /* Init Npapi related data structures */
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);
    pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);

    if ((pMplsInTnlEntry == NULL) || (pMplsOutTnlEntry == NULL))
    {
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "ILM Add for PW %d Peer -IPv4 %x IPv6 -%s started\n",
                pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "ILM Add for PW %d %x started\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }
    /* Fill PSN (PW) Port IF index, AC Port IF Index,
     * PSN (PW) Vlan, AC port vlan */
    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwIlmInfo.u4NewVlanId,
                          &MplsHwIlmInfo.u4OutPort,
                          &MplsHwIlmInfo.u4PwEnetPwVlan,
                          &MplsHwIlmInfo.i1PwEnetVlanMode);

    /* Fill ILM Info in MplsHwIlmInfo */
    MplsHwIlmInfo.OutLabelToLearn.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;
    MplsHwIlmInfo.i1CwStatus = pPwVcEntry->i1CwStatus;

    /* Terminate this ILM Entry and do L2 switching for L2 VPN */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_L2_SWITCH;

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Enet (AC) Information: Port: %d Vlan: %d Change Vlan: %d "
                "Vlan Mode: %d\n",
                MplsHwIlmInfo.u4OutPort, MplsHwIlmInfo.u4NewVlanId,
                MplsHwIlmInfo.u4PwEnetPwVlan, MplsHwIlmInfo.i1PwEnetVlanMode);
    MplsHwIlmInfo.i1PwVcType = L2VPN_PWVC_TYPE (pPwVcEntry);
    /* PwMplsType = TE */
    if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE)
    {
        /* Pw mapped over P2MP tunnel - 
           In tunnel can be zero */
        if (L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pMplsInTnlEntry) == 0)
        {
            return L2VPN_SUCCESS;
        }

        MEMCPY ((UINT1 *) &u4LclLsr,
                (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR
                (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);
        MEMCPY ((UINT1 *) &u4PeerLsr,
                (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR
                (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);

        u4LclLsr = OSIX_NTOHL (u4LclLsr);
        u4PeerLsr = OSIX_NTOHL (u4PeerLsr);

        if (pPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetIfIndexAndLabelFromXcIndex
            (pPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             &u4InIntf, &(pPwVcEntry->u4InOuterLabel)) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetL3Intf (u4InIntf, &(pPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4InIntf);
            return L2VPN_FAILURE;
        }
        MPLS_CMN_LOCK ();

        CONVERT_TO_INTEGER (L2VPN_PWVC_MPLS_IN_TNL_TNL_LCLLSR (pMplsInTnlEntry),
                            u4LclLsrId);
        CONVERT_TO_INTEGER (L2VPN_PWVC_MPLS_IN_TNL_TNL_PEERLSR
                            (pMplsInTnlEntry), u4PeerLsrId);
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Incoming Tunnel in L2VPN is %d %d %x %x\n",
                    L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pMplsInTnlEntry),
                    L2VPN_PWVC_MPLS_IN_TNL_TNL_INST (pMplsInTnlEntry),
                    OSIX_NTOHL (u4LclLsrId), OSIX_NTOHL (u4PeerLsrId));

        if (TeGetTunnelInfoByPrimaryTnlInst
            (L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pMplsInTnlEntry),
             L2VPN_PWVC_MPLS_IN_TNL_TNL_INST (pMplsInTnlEntry),
             OSIX_NTOHL (u4LclLsrId), OSIX_NTOHL (u4PeerLsrId), &pTeTnlInfo)
            == TE_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Incoming tunnel does not exist\n");
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }
        else
        {
            if (pTeTnlInfo->u1TnlOperStatus != TE_OPER_UP)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "Incoming tunnel is not operationally UP\n");
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
            if ((pTeTnlInfo->u4BkpTnlMPXcIndex1 != L2VPN_ZERO) &&
                (pPwVcEntry->u4InOuterMPLabel1 == L2VPN_LDP_INVALID_LABEL))
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Bkp MP tnl XC Index1 %d\n",
                            pTeTnlInfo->u4BkpTnlMPXcIndex1);

                if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                               pTeTnlInfo->u4BkpTnlMPXcIndex1,
                                               pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "ILM Add for merge point1 is failed \n");
                }
            }
            if ((pTeTnlInfo->u4BkpTnlMPXcIndex2 != L2VPN_ZERO) &&
                (pPwVcEntry->u4InOuterMPLabel2 == L2VPN_LDP_INVALID_LABEL))
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Bkp MP tnl XC Index2 %d\n",
                            pTeTnlInfo->u4BkpTnlMPXcIndex2);

                if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                               pTeTnlInfo->u4BkpTnlMPXcIndex2,
                                               pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "ILM Add for merge point2 is failed \n");
                }
            }
        }
        MPLS_CMN_UNLOCK ();

        /* fill ILM info */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4InL3Intf = pPwVcEntry->u4InIfIndex;

        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = pPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Incoming Tunnel Label: %d, In VC Label: %d, "
                    "Incoming L3Intf: %d\n",
                    pPwVcEntry->u4InOuterLabel, pPwVcEntry->u4InVcLabel,
                    pPwVcEntry->u4InIfIndex);
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Outgoing VC Label: %d, Outgoing L3Intf: %d\n",
                    pPwVcEntry->u4OutVcLabel, pPwVcEntry->u4PwL3Intf);
    }
    /* PwMplsType = PwOnly */
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) ==
             L2VPN_MPLS_TYPE_VCONLY)
    {
        MplsHwIlmInfo.u4InL3Intf = u4InIntf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pMplsOutTnlEntry);

        pPwVcEntry->u4InIfIndex = u4InIntf;

        /* fill ILM info */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = pPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4SrcPort = pMplsOutTnlEntry->unTnlInfo.VcInfo.u4IfIndex;
        MplsHwIlmInfo.u4OutL3Intf =
            CfaGetVlanInterfaceIndex ((UINT2) MplsHwIlmInfo.u4NewVlanId);
    }
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) ==
             L2VPN_MPLS_TYPE_NONTE)
    {
#ifndef MPLS_SIG_WANTED
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Pseudo Wire NON-TE is not supported "
                   "on manual outer tunnel \n");
        return L2VPN_FAILURE;
#endif
        if (L2VpnPwVcIlmHwAddForNonTePsn (pPwVcEntry, &MplsHwIlmInfo,
                                          pSlotInfo) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "ILM add for NON TE failed \n");
            return L2VPN_FAILURE;
        }
    }
    u4InIntf = pPwVcEntry->u4InIfIndex;

    if (u4InIntf != 0)
    {
        if (CfaGetVlanId (u4InIntf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d\n", u4InIntf);
            return L2VPN_FAILURE;
        }
        MplsHwIlmInfo.u4InVlanId = VlanId;
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW VC Add Incoming VLAN ID : %d\n",
                    MplsHwIlmInfo.u4InVlanId);
        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (MplsHwIlmInfo.u4InL3Intf, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4SrcPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", MplsHwIlmInfo.u4InL3Intf);
        }

#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
        /* Fetch the Qos Information for teh incoming port */
        QosGetMplsExpProfileForPortAtIngress (MplsHwIlmInfo.u4SrcPort,
                                              &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
        QosGetMplsExpProfileForPortAtEgress (MplsHwIlmInfo.u4OutPort,
                                             &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwPriMapId = i4HwExpMapId;
#endif
#endif

    }

    switch (MPLS_DIFFSERV_TNL_MODEL (MPLS_DEF_INCARN))
    {
        case UNIFORM_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_UNIFORM;
            break;
        case PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_PIPE;
            break;
        case SHORT_PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_SHORTPIPE;
            break;
        default:
            CMNDB_DBG (DEBUG_ERROR_LEVEL, "invalid Tunnel DIFFSRV model \n");
            return L2VPN_FAILURE;
    }

    MplsHwIlmInfo.u4TTLVal = MPLS_TTL_VALUE (MPLS_DEF_INCARN);
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "VPN Id\n", u4VpnId);

#ifdef NPAPI_WANTED
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    if (pSlotInfo == NULL)
    {
        L2VpnPwHwListKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VpnPwHwListKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);
        if (MPLS_FAILURE != L2VpnHwListGet (&L2VpnPwHwListKey,
                                            &L2VpnPwHwListEntry))
        {
            if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                 L2VPN_PWILM_NPAPI_CALLED))
            {
                if ((L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUTER_IN_LABEL (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_IN_IF_INDEX (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_IN_IF_INDEX (pPwVcEntry)))
                {
                    L2VPN_DBG8 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : HwListMismatch for PWIndex = %u \
                        \nL2VPN_PW_HW_LIST_LSP_IN_LABEL = %u \
                        \nL2VPN_PWVC_OUTER_IN_LABEL = %u \
                        \nL2VPN_PW_HW_LIST_IN_LABEL = %u \
                        \nL2VPN_PWVC_INBOUND_VC_LABEL = %u \
                        \nL2VPN_PW_HW_LIST_IN_IF_INDEX = %u \
                        \nL2VPN_PWVC_IN_IF_INDEX = %u ", __func__, L2VPN_PWVC_INDEX (pPwVcEntry), L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListEntry), L2VPN_PWVC_OUTER_IN_LABEL (pPwVcEntry), L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListEntry), L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry), L2VPN_PW_HW_LIST_IN_IF_INDEX (&L2VpnPwHwListEntry), L2VPN_PWVC_IN_IF_INDEX (pPwVcEntry));
                    if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                         L2VPN_PWILM_NPAPI_CALLED) &&
                        (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                         L2VPN_PWILM_NPAPI_SUCCESS))
                    {
                        u4PrevOutLabel =
                            MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel;
                        u4PrevInLabel =
                            MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel;

                        if (L2VPN_ZERO ==
                            L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListEntry))
                        {
                            MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
                                L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListEntry);
                        }
                        else
                        {
                            MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
                                L2VPN_PW_HW_LIST_LSP_IN_LABEL
                                (&L2VpnPwHwListEntry);
                            MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
                                L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListEntry);
                        }
                        u4Action = MPLS_HW_ACTION_L2_SWITCH;
                        MplsHwIlmInfo.u4VpnId = u4VpnId;
                        L2VpnPwHwListDelKey.u4VplsIndex =
                            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
                        L2VpnPwHwListDelKey.u4PwIndex =
                            L2VPN_PWVC_INDEX (pPwVcEntry);

                        L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                             L2VPN_PWILM_NPAPI_CALLED);

                        if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo, u4Action) ==
                            FNP_FAILURE)
                        {
#ifdef MPLS_IPV6_WANTED
                            L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                        "FsMplsHwDeleteILM: "
                                        "PW %d for Peer IPv4 %#x -IPv6 %s failed in HW\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                        Ip6PrintAddr (&
                                                      (GenU4Addr.Addr.
                                                       Ip6Addr)));
#else
                            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                        "FsMplsHwDeleteILM: "
                                        "PW %d for %#x failed in HW\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                            return L2VPN_FAILURE;
                        }
                        L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                             L2VPN_PWILM_NPAPI_SUCCESS);
                        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
                            u4PrevOutLabel;
                        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
                            u4PrevInLabel;
                    }
                }
                else
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                                "%s : Complete match for PWIndex = %u\n",
                                __func__, L2VPN_PWVC_INDEX (pPwVcEntry));

                    L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
                        L2VPN_PW_STATUS_NOT_STALE;
#ifdef VPLS_GR_WANTED
                    L2VPN_NO_OF_BGP_PW_STALE_ENTRIES (gpPwVcGlobalInfo)--;
                    if (L2VPN_ZERO ==
                        L2VPN_NO_OF_BGP_PW_STALE_ENTRIES (gpPwVcGlobalInfo))
                    {
                        if (gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted ==
                            L2VPN_TRUE)
                        {
                            if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                                              &(gpL2VpnGlobalInfo->BgpGrTimer.
                                                AppTimer)) != TMR_SUCCESS)
                            {
                                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                           "Stop of BGP GR timer failed\r\n");
                            }
                            gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted =
                                L2VPN_FALSE;
                        }
                    }
#endif
                    L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_ZERO);
                }
            }
        }
    }
#endif
#endif
#endif
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;
        if (MplsFsMplsMbsmHwCreateILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwCreateILM: "
                        "PW %d for Peer IPv4 %#x -IPv6 %s failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwCreateILM: "
                        "PW %d for %#x failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
            return L2VPN_FAILURE;
        }
    }
    else
#endif
    {
        /* Configure ILM in H/W */
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
            MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

            if (L2VpnPwIlmHwListUpdate (pPwVcEntry, &MplsHwIlmInfo,
                                        &L2VpnPwHwListEntry) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                            "L2VpnPwVcIlmHwAdd: "
                            "PW %d is nor present in Hw-List\n",
                            pPwVcEntry->u4PwVcIndex);

            }

            L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry,
                                    L2VPN_PWILM_NPAPI_CALLED);
#endif
#endif
            if (MplsFsMplsHwCreateILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwCreateILM: "
                            "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwCreateILM: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                return L2VPN_FAILURE;
            }
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
            L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry,
                                    L2VPN_PWILM_NPAPI_SUCCESS);
#endif
#endif
            pPwVcEntry->b1IsIlmEntryPresent = TRUE;

        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "MPLS HW Action:%d\n", u4Action);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDel                                            */
/* Description   : Adds the PWVc entry into the hw                            */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDel (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tMplsHwVcTnlInfo    MplsHwVcInfo;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    UINT4               u4VpnId = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
    INT4                i4RetVal = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    /* Init Npapi related data structures */
    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
#if    defined(LDP_GR_WANTED) || defined(LDP_GR_WANTED)
    if (L2VPN_FALSE == L2VPN_PWVC_NPAPI (pPwVcEntry))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "NPAPI is not called in case of GR\n");
        return L2VPN_SUCCESS;
    }
#endif

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) (GenU4Addr.Addr.Ip6Addr.u1_addr),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC Delete for PW %d Peer IPv4 -%x IPv6 -%s started\n",
                pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC Delete for PW %d %x started\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) = MPLS_ARP_RESOLVE_UNKNOWN;

    L2VPN_PWVC_ROUTE_STATUS (pPwVcEntry) = L2VPN_PWVC_ROUTE_UNKNOWN;

    /* fill up VC info in MplsHwVcInfo */
    MplsHwVcInfo.LabelOpcodes = MPLS_HW_PRESERVE_USER_TAG;
    MplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;
    MplsHwVcInfo.PwOutVcLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    MplsHwVcInfo.u4PwIndex = pPwVcEntry->u4PwVcIndex;
    MplsHwVcInfo.u4PwIfIndex = pPwVcEntry->u4PwIfIndex;
    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                          &MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                          &MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "VPN Id: %d\n", u4VpnId);

    /* PwMplsType = TE */
    if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE)
    {
        i4RetVal = L2VpnPwVcHwDelPwTe (pPwVcEntry, pSlotInfo,
                                       pPwVcMplsEntry, &MplsHwVcInfo,
                                       &MplsHwVplsInfo, u4VpnId);

    }
    /* PwMplsType = PwOnly */
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) ==
             L2VPN_MPLS_TYPE_VCONLY)
    {
        i4RetVal = L2VpnPwVcHwDelVcOnly (pPwVcEntry, pSlotInfo,
                                         pPwVcMplsEntry, &MplsHwVcInfo,
                                         &MplsHwVplsInfo, u4VpnId);

    }
    else
    {
        i4RetVal = L2VpnPwVcHwDelPwNonTe (pPwVcEntry, pSlotInfo,
                                          &MplsHwVcInfo, &MplsHwVplsInfo,
                                          u4VpnId);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwDel: EXIT\n");
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwAddP2MPDest                                    */
/* Description   : Adds the new branch to the PWVC into the hw               */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAddP2MPDest (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tMplsHwVcTnlInfo    MplsHwVcInfo;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    UINT4               u4LclLsr = L2VPN_ZERO;
    UINT4               u4PeerLsr = L2VPN_ZERO;
    UINT4               u4VpnId = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwAddP2MPDest: ENTRY\n");
    /* Init Npapi related data structures */
    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    if (pPwVcMplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnPwVcHwAddP2MPDest: PwVcMplsEntry is NULL");
        return L2VPN_FAILURE;
    }

    /* fill up VC info in MplsHwVcInfo */
    MplsHwVcInfo.LabelOpcodes = MPLS_HW_PRESERVE_USER_TAG;
    MplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;
    MplsHwVcInfo.PwOutVcLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    MplsHwVcInfo.PwInVcLabel.u.MplsShimLabel = pPwVcEntry->u4InVcLabel;
    MplsHwVcInfo.PwInVcLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }
    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                          &MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                          &MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI = i4Vsi;
    MplsHwVcInfo.FecInfo.u2ReqVlanId = pPwVcEntry->u2ReqVlanId;
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPN Id: %d VSI: %d\n", u4VpnId,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI);

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    if (pMplsOutTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Out Tunnel Entry is NULL");
        return L2VPN_FAILURE;
    }

    MEMCPY ((UINT1 *) &u4LclLsr,
            (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR
            (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &u4PeerLsr,
            (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR
            (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);

    u4LclLsr = OSIX_NTOHL (u4LclLsr);
    u4PeerLsr = OSIX_NTOHL (u4PeerLsr);

    /* TE Tunnel L3 I/F index and MAC */
    MplsHwVcInfo.u4PwL3Intf = pPwVcEntry->u4PwL3Intf;
    MEMCPY (MplsHwVcInfo.au1PwDstMac, pPwVcEntry->au1NextHopMac, MAC_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC P2MP Dest Add for Outgoing L3Intf %d "
                "for PW %d Peer IPv4- %x -IPv6- %s started\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC P2MP Dest Add for Outgoing L3Intf %d "
                "for PW %d %x started\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    if (L2VpnPwVcHwAddWrp (pPwVcEntry, pSlotInfo, &MplsHwVcInfo,
                           &MplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC P2MP Dest Add for Outgoing L3Intf %d "
                    "for PW %d Peer IPv4 %x -IPv6 %s failed\n",
                    pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC P2MP Dest Add for Outgoing L3Intf %d "
                    "for PW %d %x failed\n",
                    pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC P2MP Dest Add for Outgoing L3Intf %d "
                "for PW %d Peer IPv4- %x  IPv6- %s successful\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC P2MP Dest Add for Outgoing L3Intf %d "
                "for PW %d %x successful\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwAdd                                            */
/* Description   : Adds the PWVC into the hw                                 */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAdd (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tMplsHwVcTnlInfo    MplsHwVcInfo;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    UINT4               u4VpnId = 0;
    INT4                i4Vsi = L2VPN_ZERO;
    INT4                i4RetVal = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;
    MEMSET (&GenU4Addr, 0, sizeof (tGenU4Addr));
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwAdd: ENTRY\n");
    /* Init Npapi related data structures */
    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    if (pPwVcMplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnPwVcHwAdd: PwVcMplsEntry is NULL");
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC Add for PW %d %x started\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
    /* fill up VC info in MplsHwVcInfo */
    MplsHwVcInfo.LabelOpcodes = MPLS_HW_PRESERVE_USER_TAG;
    MplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;
    MplsHwVcInfo.PwOutVcLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    MplsHwVcInfo.PwInVcLabel.u.MplsShimLabel = pPwVcEntry->u4InVcLabel;
    MplsHwVcInfo.PwInVcLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    MplsHwVcInfo.u4PwIndex = pPwVcEntry->u4PwVcIndex;
    MplsHwVcInfo.u4PwIfIndex = pPwVcEntry->u4PwIfIndex;
    MplsHwVcInfo.u1AppOwner = pPwVcEntry->u1AppOwner;
    MplsHwVcInfo.i1CwStatus = pPwVcEntry->i1CwStatus;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }
    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                          &MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                          &MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI = i4Vsi;
    MplsHwVcInfo.FecInfo.u2ReqVlanId = pPwVcEntry->u2ReqVlanId;
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPN Id: %d VSI: %d\n",
                u4VpnId, MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI);
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outgoing VC Label: %d, Incoming VC Label: %d\n",
                pPwVcEntry->u4OutVcLabel, pPwVcEntry->u4InVcLabel);
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPN SrcVlan: %d VPN Src Port: %d\n",
                MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort);
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG, "VPN EnetPw Vlan: %d Vlan Mode: %d\n",
                MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);

    if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) == L2VPN_MPLS_TYPE_VCONLY)
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            i4RetVal = L2VpnPwVcIpv6HwAddVcOnly (pPwVcEntry, pSlotInfo,
                                                 pPwVcMplsEntry, &MplsHwVcInfo,
                                                 &MplsHwVplsInfo, u4VpnId);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            i4RetVal = L2VpnPwVcHwAddVcOnly (pPwVcEntry, pSlotInfo,
                                             pPwVcMplsEntry, &MplsHwVcInfo,
                                             &MplsHwVplsInfo, u4VpnId);
        }
    }
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE)
    {
        i4RetVal = L2VpnPwVcHwAddPwTe (pPwVcEntry, pSlotInfo,
                                       pPwVcMplsEntry, &MplsHwVcInfo,
                                       &MplsHwVplsInfo, u4VpnId);

    }
    else
    {
        i4RetVal = L2VpnPwVcHwAddPwNonTe (pPwVcEntry, pSlotInfo,
                                          &MplsHwVcInfo, &MplsHwVplsInfo,
                                          u4VpnId);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwAdd: EXIT\n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwAddVcOnly                                      */
/* Description   : Adds the PW of VC only into the hw                        */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/*                 pPwVcMplsEntry - Tunnel Info                              */
/*                 pMplsHwVcInfo - H/W Vc Information                        */
/*                 pMplsHwVplsInfo - H/W Vpls information                    */
/*                 u4VpnId - iVpn Indentifier                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAddVcOnly (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                      tPwVcMplsEntry * pPwVcMplsEntry,
                      tMplsHwVcTnlInfo * pMplsHwVcInfo,
                      tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4IpAddr = 0;
    UINT4               u4PeerAddr = 0;
    UINT1               au1PwDstMac[MAC_ADDR_LEN];
    UINT1               u1EncapType;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (RtQuery));

    MEMCPY ((UINT1 *) &u4PeerAddr, (UINT1 *) &pPwVcEntry->PeerAddr,
            IPV4_ADDR_LENGTH);
    u4IpAddr = OSIX_HTONL (u4PeerAddr);

    RtQuery.u4DestinationIpAddress = u4IpAddr;
    RtQuery.u4DestinationSubnetMask = 0xffffffff;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
        {
            u4IpAddr = NetIpRtInfo.u4NextHop;
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC Add: Next hop %x for the fec %x started\n",
                    u4IpAddr, pPwVcEntry->PeerAddr);

    }
    /* Get the destination MAC for a given Peer address */
    if ((ArpResolve (u4IpAddr, (INT1 *) au1PwDstMac, &u1EncapType)) ==
        ARP_FAILURE)
    {
        /* Since we have not succeeded in getting the NextHop MAC
         * Trigger Arp Module to resolve this IP and after
         * MPLS_ARP_RESOLVE_TIMEOUT we shall check again if this has
         * been resolved,
         * If resolved, then we go ahead in installing PWs which were
         * waiting for this resolve , if not Raise this as FAILURE
         */

        if (MplsTriggerArpResolve (u4IpAddr, (UINT1) MPLS_PW_ARP_TIMER)
            == L2VPN_SUCCESS)
        {
            L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) =
                MPLS_ARP_RESOLVE_WAITING;
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Arp Resolve Timer Started for %x\n", u4IpAddr);
            return L2VPN_FAILURE;
        }

        L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) =
            MPLS_ARP_RESOLVE_FAILED;
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Arp Resolve Queue Posting failed for Next Hop %x\n",
                    u4IpAddr);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "Arp Resolve Succeded for Next Hop %x\n", u4IpAddr);
    L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) = MPLS_ARP_RESOLVED;

    MEMCPY (pMplsHwVcInfo->au1PwDstMac, au1PwDstMac, MAC_ADDR_LEN);
    MEMCPY (pPwVcEntry->au1NextHopMac, au1PwDstMac, MAC_ADDR_LEN);

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    if (pMplsOutTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Out Tunnel Entry is NULL");
        return L2VPN_FAILURE;
    }
    /* Outbound CFA I/F Index */
    pMplsHwVcInfo->u4PwOutPort = pMplsOutTnlEntry->unTnlInfo.VcInfo.u4IfIndex;
#ifdef NPSIM_WANTED
    pMplsHwVcInfo->u4PwL3Intf = L2VPN_PWVC_MPLS_TNL_IF_INDEX (pMplsOutTnlEntry);
#endif
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddVcOnly :Outgoing L3Intf: %d\n",
                pMplsHwVcInfo->u4PwL3Intf);
    /* fill EXP Bits and TTL value for Pw Only case */
    pMplsHwVcInfo->QosInfo[0].u.MplsQosPolicy.u4LabelPri =
        (L2VPN_LOWNIB & pPwVcMplsEntry->i1ExpModeAndBits);
    pMplsHwVcInfo->u4L3Ttl[0] = pPwVcMplsEntry->u1Ttl;

    if (L2VpnPwVcHwAddWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                           pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;

}

#ifdef MPLS_IPV6_WANTED
/*****************************************************************************/
/* Function Name : L2VpnPwVcIpv6HwAddVcOnly                                  */
/* Description   : Adds the PW of VC only into the hw                        */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/*                 pPwVcMplsEntry - Tunnel Info                              */
/*                 pMplsHwVcInfo - H/W Vc Information                        */
/*                 pMplsHwVplsInfo - H/W Vpls information                    */
/*                 u4VpnId - iVpn Indentifier                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcIpv6HwAddVcOnly (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                          tPwVcMplsEntry * pPwVcMplsEntry,
                          tMplsHwVcTnlInfo * pMplsHwVcInfo,
                          tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    UINT1               au1PwDstMac[MAC_ADDR_LEN];
    tIp6Addr            IpAddr;

    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    if (pMplsOutTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Out Tunnel Entry is NULL");
        return L2VPN_FAILURE;
    }

    NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
    NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
    Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &(pPwVcEntry->PeerAddr.Ip6Addr));
    NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

    if (NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                         &NetIpv6RtInfo) == NETIPV6_SUCCESS)
    {
        if (NetIpv6RtInfo.i1Proto != CIDR_LOCAL_ID)
        {
            MEMCPY (&IpAddr, &NetIpv6RtInfo.NextHop, IPV6_ADDR_LENGTH);
        }
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC Add: Next hop %s for the fec %s started\n",
                    Ip6PrintAddr (&IpAddr),
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
    }

    /* Get the destination MAC for a given Peer address */
    if ((NetIpv6Nd6Resolve (pMplsOutTnlEntry->unTnlInfo.VcInfo.u4IfIndex,
                            &(pPwVcEntry->PeerAddr.Ip6Addr), au1PwDstMac)) ==
        NETIPV6_FAILURE)
    {
        /* Since we have not succeeded in getting the NextHop MAC
         * Trigger Arp Module to resolve this IP and after
         * MPLS_ARP_RESOLVE_TIMEOUT we shall check again if this has
         * been resolved,
         * If resolved, then we go ahead in installing PWs which were
         * waiting for this resolve , if not Raise this as FAILURE
         */

        if (MplsStartArpResolveTimer ((UINT1) MPLS_PW_ARP_TIMER)
            == MPLS_SUCCESS)
        {
            L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) =
                MPLS_ARP_RESOLVE_WAITING;
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Arp Resolve Timer Started for %s\n",
                        Ip6PrintAddr (&IpAddr));
            return L2VPN_FAILURE;
        }

        L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) =
            MPLS_ARP_RESOLVE_FAILED;
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Arp Resolve Queue Posting failed for Next Hop %s\n",
                    Ip6PrintAddr (&IpAddr));
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "Arp Resolve Succeded for Next Hop %s\n",
                Ip6PrintAddr (&IpAddr));
    L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) = MPLS_ARP_RESOLVED;

    MEMCPY (pMplsHwVcInfo->au1PwDstMac, au1PwDstMac, MAC_ADDR_LEN);
    MEMCPY (pPwVcEntry->au1NextHopMac, au1PwDstMac, MAC_ADDR_LEN);

    /* Outbound CFA I/F Index */
    pMplsHwVcInfo->u4PwOutPort = pMplsOutTnlEntry->unTnlInfo.VcInfo.u4IfIndex;
#ifdef NPSIM_WANTED
    pMplsHwVcInfo->u4PwL3Intf = L2VPN_PWVC_MPLS_TNL_IF_INDEX (pMplsOutTnlEntry);
#endif
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcIpv6HwAddVcOnly :Outgoing L3Intf: %d\n",
                pMplsHwVcInfo->u4PwL3Intf);
    /* fill EXP Bits and TTL value for Pw Only case */
    pMplsHwVcInfo->QosInfo[0].u.MplsQosPolicy.u4LabelPri =
        (L2VPN_LOWNIB & pPwVcMplsEntry->i1ExpModeAndBits);
    pMplsHwVcInfo->u4L3Ttl[0] = pPwVcMplsEntry->u1Ttl;

    if (L2VpnPwVcHwAddWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                           pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;

}
#endif

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDelVcOnly                                      */
/* Description   : Deletes the PW of VC only from the hw                     */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/*                 pPwVcMplsEntry - Tunnel Info                              */
/*                 pMplsHwVcInfo - H/W Vc Information                        */
/*                 pMplsHwVplsInfo - H/W Vpls information                    */
/*                 u4VpnId - iVpn Indentifier                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDelVcOnly (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                      tPwVcMplsEntry * pPwVcMplsEntry,
                      tMplsHwVcTnlInfo * pMplsHwVcInfo,
                      tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    if (pMplsOutTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Out Tunnel Entry is NULL");
        return L2VPN_FAILURE;
    }
    /* Outbound CFA I/F Index */
    pMplsHwVcInfo->u4PwOutPort = pMplsOutTnlEntry->unTnlInfo.VcInfo.u4IfIndex;
#ifdef NPSIM_WANTED
    pMplsHwVcInfo->u4PwL3Intf = L2VPN_PWVC_MPLS_TNL_IF_INDEX (pMplsOutTnlEntry);
#endif
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwDelVcOnly : Outgoing L3Intf: %d\n",
                pMplsHwVcInfo->u4PwL3Intf);
    /* fill EXP Bits and TTL value for Pw Only case */
    pMplsHwVcInfo->QosInfo[0].u.MplsQosPolicy.u4LabelPri =
        (L2VPN_LOWNIB & pPwVcMplsEntry->i1ExpModeAndBits);
    pMplsHwVcInfo->u4L3Ttl[0] = pPwVcMplsEntry->u1Ttl;

    if (L2VpnPwVcHwDelWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                           pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwAddPwTe                                        */
/* Description   : Adds the PW with underlying Te tunnel into the hw         */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/*                 pPwVcMplsEntry - Tunnel Info                              */
/*                 pMplsHwVcInfo - H/W Vc Information                        */
/*                 pMplsHwVplsInfo - H/W Vpls information                    */
/*                 u4VpnId - iVpn Indentifier                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAddPwTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                    tPwVcMplsEntry * pPwVcMplsEntry,
                    tMplsHwVcTnlInfo * pMplsHwVcInfo,
                    tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4TnlXcIndex;
    UINT4               u4LclLsr = 0;
    UINT4               u4PeerLsr = 0;
    UINT4               u4CurrIfIndex = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4L3Intf = 0;
    UINT1               au1NextHop[MAC_ADDR_LEN];

    MEMSET (au1NextHop, 0, MAC_ADDR_LEN);

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    if (pMplsOutTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Out Tunnel Entry is NULL");
        return L2VPN_FAILURE;
    }
    /* Pw mapped over P2MP tunnel - 
       Out tunnel can be zero */
    if (L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry) == 0)
    {
        return L2VPN_SUCCESS;
    }

    MEMCPY ((UINT1 *) &u4LclLsr,
            (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR
            (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &u4PeerLsr,
            (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR
            (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);

    u4LclLsr = OSIX_NTOHL (u4LclLsr);
    u4PeerLsr = OSIX_NTOHL (u4PeerLsr);

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outgoing Tunnel in L2VPN is %d %d %x %x\n",
                L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry),
                L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsOutTnlEntry),
                u4LclLsr, u4PeerLsr);

    if (pPwVcEntry->u1TnlType == L2VPN_PW_PSN_TNL_TYPE_P2P)
    {
        MPLS_CMN_LOCK ();

        if (TeGetTunnelInfoByPrimaryTnlInst
            (L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry),
             L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsOutTnlEntry),
             u4LclLsr, u4PeerLsr, &pTeTnlInfo) != TE_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }

        u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
        if (u4TnlXcIndex == L2VPN_ZERO)
        {
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }
        if (pTeTnlInfo->u1TnlRole == TE_INGRESS)
        {
            pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex,
                                                  MPLS_DEF_DIRECTION);
            if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL) ||
                (pXcEntry->u1OperStatus != XC_OPER_UP))
            {
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
            pPwVcEntry->u4PwL3Intf = pXcEntry->pOutIndex->u4IfIndex;
            pPwVcEntry->u4OutOuterLabel = pXcEntry->pOutIndex->u4Label;
            pMplsHwVcInfo->OutLspLabel.u.MplsShimLabel =
                pPwVcEntry->u4OutOuterLabel;
        }
        if (pTeTnlInfo->u1TnlRole == TE_EGRESS)
        {
            pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex,
                                                  MPLS_DIRECTION_REVERSE);

            if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL) ||
                (pXcEntry->u1OperStatus != XC_OPER_UP))
            {
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }

            pPwVcEntry->u4PwL3Intf = pXcEntry->pOutIndex->u4IfIndex;
            pPwVcEntry->u4OutOuterLabel = pXcEntry->pOutIndex->u4Label;
            pMplsHwVcInfo->OutLspLabel.u.MplsShimLabel =
                pPwVcEntry->u4OutOuterLabel;
        }

        MEMCPY (au1NextHop, pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);
        MPLS_CMN_UNLOCK ();

        MEMCPY (pMplsHwVcInfo->au1PwDstMac, au1NextHop, MAC_ADDR_LEN);
        /* TE Tunnel L3 I/F index */
        pMplsHwVcInfo->u4PwL3Intf = pPwVcEntry->u4PwL3Intf;

        /* Fetch the L3 interface from the MPLS tunnel interface. */
        if (MplsGetL3Intf (pMplsHwVcInfo->u4PwL3Intf,
                           &u4L3Intf) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcHwAddPwTe :"
                        "Fetching L3 interface failed "
                        "for tunnel interface %d\t\n",
                        pMplsHwVcInfo->u4PwL3Intf);
        }

        /* The VLAN Id will get updated with valid value only 
         * if the L3 interface is IVR interface. */
        if (CfaGetVlanId (u4L3Intf,
                          &(pMplsHwVcInfo->u2OutVlanId)) != CFA_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcHwAddPwTe : "
                        "CfaGetVlanId Failed for L3 interface %d\r\n",
                        u4L3Intf);
        }

        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4L3Intf, (UINT1 *) (&(pMplsHwVcInfo->au1PwDstMac)),
             &(pMplsHwVcInfo->u4PwOutPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcHwAddPwTe : "
                        "Fetching Physical port for the L3 interface : %d "
                        "failed.\r\n", u4L3Intf);
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Outgoing VC Label: %d, Outgoing L3Intf: %d\n",
                    pPwVcEntry->u4OutVcLabel, pPwVcEntry->u4PwL3Intf);
        L2VPN_DBG6 (L2VPN_DBG_LVL_CRT_FLAG,
                    "TE tnl Next Hop Mac address "
                    "%02x:%02x:%02x:%02x:%02x:%02x\n",
                    pMplsHwVcInfo->au1PwDstMac[0],
                    pMplsHwVcInfo->au1PwDstMac[1],
                    pMplsHwVcInfo->au1PwDstMac[2],
                    pMplsHwVcInfo->au1PwDstMac[3],
                    pMplsHwVcInfo->au1PwDstMac[4],
                    pMplsHwVcInfo->au1PwDstMac[5]);

        if (L2VpnPwVcHwAddWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                               pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }
    }
    else
    {
        while (1)
        {
            u4CurrIfIndex = u4NextIfIndex;

            if (TeCmnExtGetNextP2mpBranchInfo
                (L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry),
                 L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsOutTnlEntry), u4LclLsr,
                 u4PeerLsr, u4CurrIfIndex, &u4NextIfIndex,
                 au1NextHop) == TE_FAILURE)
            {
                break;
            }

            MEMCPY (pMplsHwVcInfo->au1PwDstMac, au1NextHop, MAC_ADDR_LEN);
            pMplsHwVcInfo->u4PwL3Intf = u4NextIfIndex;

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "Outgoing VC Label: %d, Outgoing L3Intf: %d\n",
                        pPwVcEntry->u4OutVcLabel, pPwVcEntry->u4PwL3Intf);
            L2VPN_DBG6 (L2VPN_DBG_LVL_CRT_FLAG,
                        "TE tnl Next Hop Mac address "
                        "%02x:%02x:%02x:%02x:%02x:%02x\n",
                        pMplsHwVcInfo->au1PwDstMac[0],
                        pMplsHwVcInfo->au1PwDstMac[1],
                        pMplsHwVcInfo->au1PwDstMac[2],
                        pMplsHwVcInfo->au1PwDstMac[3],
                        pMplsHwVcInfo->au1PwDstMac[4],
                        pMplsHwVcInfo->au1PwDstMac[5]);

            if (L2VpnPwVcHwAddWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                                   pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Failed while adding Outgoing L3Intf: %d\n",
                            pPwVcEntry->u4PwL3Intf);
            }

        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDelPwTe                                        */
/* Description   : Deletes the PW with underlying Te tunnel from the hw      */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/*                 pPwVcMplsEntry - Tunnel Info                              */
/*                 pMplsHwVcInfo - H/W Vc Information                        */
/*                 pMplsHwVplsInfo - H/W Vpls information                    */
/*                 u4VpnId - iVpn Indentifier                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDelPwTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                    tPwVcMplsEntry * pPwVcMplsEntry,
                    tMplsHwVcTnlInfo * pMplsHwVcInfo,
                    tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    UINT4               u4LclLsr = 0;
    UINT4               u4PeerLsr = 0;
    UINT4               u4CurrIfIndex = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4L3Intf = L2VPN_ZERO;
    UINT1               au1NextHop[MAC_ADDR_LEN];

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    if (pMplsOutTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Out Tunnel Entry is NULL");
        return L2VPN_FAILURE;
    }
    /* Pw mapped over P2MP tunnel - 
       Out tunnel can be zero */
    if (L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry) == 0)
    {
        return L2VPN_SUCCESS;
    }

    MEMCPY ((UINT1 *) &u4LclLsr,
            (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR
            (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &u4PeerLsr,
            (UINT1 *) &L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR
            (pMplsOutTnlEntry), IPV4_ADDR_LENGTH);

    u4LclLsr = OSIX_NTOHL (u4LclLsr);
    u4PeerLsr = OSIX_NTOHL (u4PeerLsr);

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outgoing Tunnel in L2VPN is %d %d %x %x\n",
                L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry),
                L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsOutTnlEntry),
                u4LclLsr, u4PeerLsr);

    if (pPwVcEntry->u1TnlType == L2VPN_PW_PSN_TNL_TYPE_P2P)
    {
        /* TE Tunnel L3 I/F index */
        pMplsHwVcInfo->u4PwL3Intf = pPwVcEntry->u4PwL3Intf;

        /* Fetch the L3 interface from the MPLS tunnel interface. */
        if (MplsGetL3Intf (pMplsHwVcInfo->u4PwL3Intf,
                           &u4L3Intf) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcHwAddPwTe :"
                        "Fetching L3 interface failed "
                        "for tunnel interface %d\t\n",
                        pMplsHwVcInfo->u4PwL3Intf);
        }
        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4L3Intf, (UINT1 *) (&(pPwVcEntry->au1NextHopMac)),
             &(pMplsHwVcInfo->u4PwOutPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcHwDelPwTe : "
                        "Fetching Physical port for the L3 interface : %d "
                        "failed.\r\n", u4L3Intf);
        }

        if (L2VpnPwVcHwDelWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                               pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }
    }
    else
    {
        while (1)
        {
            u4CurrIfIndex = u4NextIfIndex;

            if (TeCmnExtGetNextP2mpBranchInfo
                (L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry),
                 L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsOutTnlEntry), u4LclLsr,
                 u4PeerLsr, u4CurrIfIndex, &u4NextIfIndex,
                 au1NextHop) == TE_FAILURE)
            {
                break;
            }

            pMplsHwVcInfo->u4PwL3Intf = u4NextIfIndex;

            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "Outgoing L3Intf: %d\n", pMplsHwVcInfo->u4PwL3Intf);
            if (L2VpnPwVcHwDelWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                                   pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Failed while deleting Outgoing L3Intf: %d\n",
                            pPwVcEntry->u4PwL3Intf);
            }
        }
    }

    return L2VPN_SUCCESS;
}

/**********************************************n*******************************/
/* Function Name : L2VpnPwVcHwAddPwNonTe                                     */
/* Description   : Adds the PW with Non-Te tunnel  into the hw               */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/*                 pMplsHwVcInfo - H/W Vc Information                        */
/*                 pMplsHwVplsInfo - H/W Vpls information                    */
/*                 u4VpnId - iVpn Indentifier                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAddPwNonTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                       tMplsHwVcTnlInfo * pMplsHwVcInfo,
                       tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
#ifndef MPLS_SIG_WANTED
    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
               "Pseudo Wire NON-TE is not supported on manual outer tunnel \n");
    return L2VPN_FAILURE;
#endif
    if (L2VpnPwVcHwAddForNonTePsn (pPwVcEntry, pMplsHwVcInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PWVC Add for NON-TE PSN failed \n");
        return L2VPN_FAILURE;
    }

    if (L2VpnPwVcHwAddWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                           pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;

}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDelPwNonTe                                     */
/* Description   : Deletes the PW with underlying Non TE tunnel into the hw  */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDelPwNonTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                       tMplsHwVcTnlInfo * pMplsHwVcInfo,
                       tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
#ifndef MPLS_SIG_WANTED
    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
               "Pseudo Wire NON-TE is not supported on manual outer tunnel \n");
    return L2VPN_FAILURE;
#endif
    if (L2VpnPwVcHwDelForNonTePsn (pPwVcEntry, pMplsHwVcInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PWVC NON-TE deletion failed \n");
        return L2VPN_FAILURE;
    }
    if (L2VpnPwVcHwDelWrp (pPwVcEntry, pSlotInfo, pMplsHwVcInfo,
                           pMplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwAddWrp                                         */
/* Description   : Adds the PWVC into the hw                                 */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAddWrp (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                   tMplsHwVcTnlInfo * pMplsHwVcInfo,
                   tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4Action;
    UINT4               u4VplsInstance = 0;
    UINT4               u4PwAcId = 0;
    BOOL1               bIsAllPwsDown = FALSE;
    UINT1               u1HwStatus = L2VPN_ZERO;
    UINT1               u1AppOwner = MPLS_APPLICATION_NONE;
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    UINT4               u4OutPort = 0;
    tHwPortInfo         pHwPortInfo;
#ifdef HVPLS_WANTED
    tVplsPwBindEntry   *pVplsPwBindEntry = NULL;
#endif

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListKey;
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tL2VpnPwHwList      L2VpnPwHwListUpdate;
    UINT1               u1IsSamePeerAddress = L2VPN_FALSE;
#endif
#endif
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    u4Action = L2VPN_ZERO;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    MEMSET (&pHwPortInfo, 0, sizeof (tHwPortInfo));
#endif

#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    MEMSET (&L2VpnPwHwListKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
#endif
#endif

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    MEMCPY (pPwVcEntry->au1NextHopMac,
            pMplsHwVcInfo->au1PwDstMac, MAC_ADDR_LEN);

    u1AppOwner = L2VpPortGetPortOwnerFromIndex (pPwVcEntry->u4PwIfIndex);
    pMplsHwVcInfo->u1AppOwner = u1AppOwner;
    pMplsHwVplsInfo->mplsHwVplsVcInfo.u1AppOwner = u1AppOwner;
    L2VPN_PWVC_TYPE (pMplsHwVcInfo) = L2VPN_PWVC_TYPE (pPwVcEntry);
    pMplsHwVplsInfo->mplsHwVplsVcInfo.i1PwVcType = L2VPN_PWVC_TYPE (pPwVcEntry);

    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
    {
        /* Ac update is specified but no vlan and port information is provided
         */
        if (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE)
        {
            if ((pMplsHwVcInfo->FecInfo.u4VpnSrcVlan == L2VPN_ZERO) &&
                (pMplsHwVcInfo->FecInfo.u4VpnSrcPhyPort == L2VPN_ZERO))
            {
                /* For MultiSegment Pseudowire Pseudowire Hardware programming 
                 * should not be done.
                 * This condition will get hit at the Segmented (Cross connected)
                 * router where PW is created through signalling */
                return L2VPN_SUCCESS;
            }
        }

        pMplsHwVcInfo->u1VpwsPwHwAction = pPwVcEntry->u1HwStatus;
        pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

        if (pPwVcMplsEntry == NULL)
        {
            return L2VPN_FAILURE;
        }
#ifdef NPSIM_WANTED
        /* vconly case L3 interface can be gigabitethernet for
         * router port.
         */
        if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) !=
            L2VPN_MPLS_TYPE_VCONLY)
        {
            if (pMplsHwVcInfo->u4PwL3Intf <= BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                return L2VPN_FAILURE;
            }
        }

#endif
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#ifdef LDP_GR_WANTED
        if (pSlotInfo == NULL)
        {
            MEMSET (&L2VpnPwHwListKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
            L2VpnPwHwListKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
            L2VpnPwHwListKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Getting L2VPN hw list Entry for "
                        "VPLS Index: %u, Pw Index: %u\n",
                        L2VpnPwHwListKey.u4VplsIndex,
                        L2VpnPwHwListKey.u4PwIndex);
            /* If entry not found in RBTree then it is assumed as new entry */
            if ((MPLS_FAILURE != L2VpnHwListGet (&L2VpnPwHwListKey,
                                                 &L2VpnPwHwListEntry)) &&
                (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                 L2VPN_PWAC_NPAPI_SUCCESS))
            {
#ifdef MPLS_IPV6_WANTED
                if (L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE (&L2VpnPwHwListEntry) ==
                    pPwVcEntry->i4PeerAddrType)
#endif
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
                    {
                        if (L2VPN_ZERO ==
                            MEMCMP (L2VPN_PW_HW_LIST_PEER_IPV6_ADDRESS
                                    (&L2VpnPwHwListEntry),
                                    &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                                    IPV6_ADDR_LENGTH))
                        {
                            u1IsSamePeerAddress = L2VPN_TRUE;
                        }
                    }
                    else
#endif
                    {
                        if (L2VPN_ZERO ==
                            MEMCMP (L2VPN_PW_HW_LIST_PEER_IP_ADDRESS
                                    (&L2VpnPwHwListEntry),
                                    &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                                    IPV4_ADDR_LENGTH))
                        {
                            u1IsSamePeerAddress = L2VPN_TRUE;
                        }
                        else
                        {
                            L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                                        "%s : L2VPN_PW_HW_LIST_PEER_IP_ADDRESS=%x\n \
                            L2VPN_PWVC_PEER_ADDR=%x", __func__, L2VPN_PW_HW_LIST_PEER_IP_ADDRESS
                                        (&L2VpnPwHwListEntry), &L2VPN_PWVC_PEER_ADDR (pPwVcEntry));
                        }
                    }
                }
                if ((L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_REMOTE_VE_ID (pPwVcEntry)) ||
                    (L2VPN_TRUE != u1IsSamePeerAddress) ||
                    (L2VPN_PW_HW_LIST_OUT_LABEL (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_OUT_IF_INDEX (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUT_IF_INDEX (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_LSP_OUT_LABEL (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUTER_OUT_LABEL (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_OUT_PORT (&L2VpnPwHwListEntry) !=
                     pMplsHwVcInfo->u4PwOutPort))
                {
                    L2VPN_DBG4 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : HwListMismatch for PWVC\n \
                                    u1IsSamePeerAddress = %d\n \
                                    L2VPN_PW_HW_LIST_OUT_LABEL=%d\n \
                                    L2VPN_PWVC_OUTBOUND_VC_LABEL =%d\n", __func__, u1IsSamePeerAddress, L2VPN_PW_HW_LIST_OUT_LABEL (&L2VpnPwHwListEntry), L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry));
                    L2VPN_DBG5 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : L2VPN_PW_HW_LIST_OUT_IF_INDEX=%d\n \
                                    L2VPN_PWVC_OUT_IF_INDEX=%d\n \
                                    L2VPN_PW_HW_LIST_LSP_OUT_LABEL=%d\n \
                                    L2VPN_PWVC_OUTER_OUT_LABEL=%d", __func__, L2VPN_PW_HW_LIST_OUT_IF_INDEX (&L2VpnPwHwListEntry), L2VPN_PWVC_OUT_IF_INDEX (pPwVcEntry), L2VPN_PW_HW_LIST_LSP_OUT_LABEL (&L2VpnPwHwListEntry), L2VPN_PWVC_OUTER_OUT_LABEL (pPwVcEntry));
                    pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel =
                        L2VPN_PW_HW_LIST_OUT_LABEL (&L2VpnPwHwListEntry);
                    pPwVcEntry->u1HwStatus = PW_HW_PW_UPDATE;
                    if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                         L2VPN_PWVC_NPAPI_SUCCESS) &&
                        (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                         L2VPN_PWVC_NPAPI_CALLED))
                    {
                        L2vpnGrDeleteLdpPwEntryOnMisMatch (&L2VpnPwHwListEntry);
                        pPwVcEntry->u1HwStatus = PW_HW_VPN_UPDATE;
                    }
                    else
                    {
                        u4PwAcId = L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry);
                    }
                    pMplsHwVcInfo->u1VpwsPwHwAction = pPwVcEntry->u1HwStatus;
                    pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel =
                        L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
                }
                else if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry)
                          & L2VPN_PWVC_NPAPI_SUCCESS))
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                                "%s : Complete match for PWIndex = %u\n",
                                __func__, L2VPN_PWVC_INDEX (pPwVcEntry));
                    u1HwStatus =
                        (UINT1) ((pPwVcEntry->u1HwStatus & 0x0F) | (pPwVcEntry->
                                                                    u1HwStatus &
                                                                    0xF0) >>
                                 MPLS_FOUR) << MPLS_FOUR;

                    L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ZERO);
                    if (pPwVcEntry->pServSpecEntry != NULL)
                    {
                        pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                            (L2VPN_PWVC_ENET_ENTRY_LIST
                             ((tPwVcEnetServSpecEntry *)
                              L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
                        if (pPwVcEnetEntry != NULL)
                        {
                            pPwVcEnetEntry->u4PwAcId =
                                L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry);
                        }
                    }

                }
            }
        }
#endif
#endif
        /*Get unique number for Enet entry from Index Manager */
        /*Check AC is not already programmed */
        if (!(pPwVcEntry->u1HwStatus & PW_HW_AC_PRESENT))
        {
            /*Set the Unique identifier for Enet entry */
            u4PwAcId = MplsL2VpnGetAcId (void);

            if (MplsL2VpnSetAcId (u4PwAcId) != u4PwAcId)
            {
                return L2VPN_FAILURE;
            }
            pMplsHwVcInfo->u4PwAcId = u4PwAcId;

            if (pPwVcEntry->pServSpecEntry != NULL)
            {
                pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                    (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                 L2VPN_PWVC_ENET_ENTRY
                                                 (pPwVcEntry)));
                if (pPwVcEnetEntry != NULL)
                {
                    pPwVcEnetEntry->u4PwAcId = u4PwAcId;
                    /* To check whether AC Port is Operationally up */
                    if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex)
                        == L2VPN_FAILURE)
                    {
                        pPwVcEntry->u1CPOrMgmtOperStatus =
                            L2VPN_PWVC_CP_DEF_OPER_STATUS;
                        return L2VPN_FAILURE;
                    }
                    else
                    {
                        pPwVcEntry->u1LocalStatus &=
                            (UINT1) (~L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT);
                        pPwVcEntry->u1LocalStatus &=
                            (UINT1) (~L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT);

                    }
                    if (L2VpnSendLblMapOrNotifMsg (pPwVcEntry) == L2VPN_FAILURE)
                    {
#ifdef MPLS_IPV6_WANTED

                        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "Label Map or Notification Msg not sent for the "
                                    "PW %d with Peer IPv4- %x IPv6 -%s\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    GenU4Addr.Addr.u4Addr,
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
                        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "Label Map or Notification Msg not sent for the "
                                    "PW %d with Peer %x\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    GenU4Addr.Addr.u4Addr);
#endif
                        return L2VPN_FAILURE;
                    }
                }

            }
        }
#ifdef MBSM_WANTED
        if (pSlotInfo != NULL)
        {
            /* If HwStatus is UPDATE,It will create New Entry.
             * If HwStatus is PRESENT,Already Existing,No need to create Entry.  
             * If Entry presents in active, Need to send update Action
             * to standby Node.*/
            if (pPwVcEntry->u1HwStatus & PW_HW_AC_PRESENT)
            {
                pMplsHwVcInfo->u1VpwsPwHwAction = PW_HW_AC_UPDATE;
            }
            if (pPwVcEntry->u1HwStatus & PW_HW_PW_PRESENT)
            {
                pMplsHwVcInfo->u1VpwsPwHwAction = PW_HW_PW_UPDATE;
            }
            if (pPwVcEntry->u1HwStatus & PW_HW_VPN_PRESENT)
            {
                pMplsHwVcInfo->u1VpwsPwHwAction = PW_HW_VPN_UPDATE;
            }
#ifdef LDP_GR_WANTED
            L2VpnPwVcHwListCreate (pPwVcEntry, pMplsHwVcInfo,
                                   &L2VpnPwHwListUpdate);
            L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListUpdate) = u4PwAcId;
            L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                    L2VPN_PWAC_NPAPI_CALLED |
                                    L2VPN_PWVC_NPAPI_CALLED);
#endif

            if (MplsFsMplsMbsmHwCreatePwVc
                (u4VpnId, pMplsHwVcInfo, u4Action, pSlotInfo) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsMbsmHwCreatePwVc: "
                            "PW %d for Peer IPv4- %#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "MplsFsMplsMbsmHwCreatePwVc: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                MplsL2VpnRelAcId (u4PwAcId);
                return L2VPN_FAILURE;
            }
#ifdef LDP_GR_WANTED
            L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                    L2VPN_PWAC_NPAPI_SUCCESS |
                                    L2VPN_PWVC_NPAPI_SUCCESS);
#endif
        }
        else
#endif
        {
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED
#ifdef LDP_GR_WANTED
                L2VpnPwVcHwListCreate (pPwVcEntry, pMplsHwVcInfo,
                                       &L2VpnPwHwListUpdate);
                L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListUpdate) = u4PwAcId;
                L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                        L2VPN_PWVC_NPAPI_CALLED);
                if (pMplsHwVcInfo->u1VpwsPwHwAction & PW_HW_AC_UPDATE)
                {
                    L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                            L2VPN_PWAC_NPAPI_CALLED);
                }
#endif
                if (pMplsHwVcInfo->u1VpwsPwHwAction & PW_HW_PW_UPDATE)
                {
                    if (pMplsHwVcInfo->u4PwOutPort == 0)
                    {
                        MEMCPY (pHwPortInfo.au1Mac,
                                pMplsHwVcInfo->au1PwDstMac, MAC_ADDR_LEN);
                        pHwPortInfo.u2OutVlanId = pMplsHwVcInfo->u2OutVlanId;
                        pHwPortInfo.u1MsgType = ISS_NP_GET_PORT_INFO;
                        CfaNpGetHwPortInfo (&pHwPortInfo);
                        u4OutPort = pHwPortInfo.u4OutPort;
                    }
                    else
                    {
                        u4OutPort = pMplsHwVcInfo->u4PwOutPort;
                    }
                    /*Fetch Qos Info for the Outgoing Interface */
#ifdef QOSX_WANTED
                    QosGetMplsExpProfileForPortAtEgress (u4OutPort,
                                                         &i4HwExpMapId);
                    pMplsHwVcInfo->QosInfo[0].u.MplsQosPolicy.i4HwExpMapId =
                        i4HwExpMapId;
                    QosGetMplsExpProfileForPortAtIngress (u4OutPort,
                                                          &i4HwExpMapId);
                    pMplsHwVcInfo->QosInfo[0].u.MplsQosPolicy.i4HwPriMapId =
                        i4HwExpMapId;
#endif

                }
                if (MplsFsMplsHwCreatePwVc (u4VpnId, pMplsHwVcInfo, u4Action)
                    == FNP_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG4 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwCreatePwVc: "
                                "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW when passing "
                                "HwAction = %x \n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                                pMplsHwVcInfo->u1VpwsPwHwAction);
#else
                    L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwCreatePwVc: "
                                "PW %d for %#x failed in HW when passing "
                                "HwAction = %x \n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                pMplsHwVcInfo->u1VpwsPwHwAction);
#endif
                    MplsL2VpnRelAcId (u4PwAcId);
                    return L2VPN_FAILURE;
                }
#ifdef LDP_GR_WANTED
                L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                        L2VPN_PWVC_NPAPI_SUCCESS);
                if (pMplsHwVcInfo->u1VpwsPwHwAction & PW_HW_AC_UPDATE)
                {
                    L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                            L2VPN_PWAC_NPAPI_SUCCESS);
                }
#endif
#endif
            }
        }
        /* Updated the H/w with Passed information. So copy the passed information
         * to current Hardware Status.
         */
        u1HwStatus = (UINT1) ((pPwVcEntry->u1HwStatus & 0x0F) |
                              ((UINT1) (pPwVcEntry->u1HwStatus & 0xF0) >>
                               MPLS_FOUR)) << MPLS_FOUR;

        L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ZERO);
    }
    else if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        if (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) == 0)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "VPLS index is not associated with the PW\n");
            return L2VPN_FAILURE;
        }
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
            (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
        if (pVplsEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "VPLS entry is NULL\n");
            return L2VPN_FAILURE;
        }
        /* Fill up the VPLS info */
        MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]),
                sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
        u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        pMplsHwVcInfo->FecInfo.i4VSI = L2VPN_VPLS_VSI (pVplsEntry);
        pMplsHwVcInfo->FecInfo.u4VplsInstance =
            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#ifdef HVPLS_WANTED
        pVplsPwBindEntry =
            L2vpnGetVplsPwBindEntry (pPwVcEntry->u4VplsInstance,
                                     pMplsHwVcInfo->u4PwIndex);
        if (pVplsPwBindEntry == NULL)
        {
            return L2VPN_FAILURE;
        }
        pMplsHwVcInfo->u4VplsPwBindType =
            L2VPN_VPLS_PW_BIND_BIND_TYPE (pVplsPwBindEntry);
#else
        pMplsHwVcInfo->u4VplsPwBindType = L2VPN_VPLS_PW_MESH;
#endif

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
        if (pSlotInfo == NULL)
        {
            MEMSET (&L2VpnPwHwListKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
            L2VpnPwHwListKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
            L2VpnPwHwListKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Getting L2VPN hw list Entry for "
                        "VPLS Index: %u, Pw Index: %u\n",
                        L2VpnPwHwListKey.u4VplsIndex,
                        L2VpnPwHwListKey.u4PwIndex);
            /* If entry not found in RBTree then it is assumed as new entry */
            if ((MPLS_FAILURE != L2VpnHwListGet (&L2VpnPwHwListKey,
                                                 &L2VpnPwHwListEntry)) &&
                (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                 L2VPN_PWVC_NPAPI_SUCCESS) &&
                (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                 L2VPN_PWVC_NPAPI_CALLED))
            {
#ifdef MPLS_IPV6_WANTED
                if (L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE (&L2VpnPwHwListEntry) ==
                    pPwVcEntry->i4PeerAddrType)
#endif
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
                    {
                        if (L2VPN_ZERO ==
                            MEMCMP (L2VPN_PW_HW_LIST_PEER_IPV6_ADDRESS
                                    (&L2VpnPwHwListEntry),
                                    &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                                    IPV6_ADDR_LENGTH))
                        {
                            u1IsSamePeerAddress = L2VPN_TRUE;
                        }
                    }
                    else
#endif
                    {
                        if (L2VPN_ZERO ==
                            MEMCMP (L2VPN_PW_HW_LIST_PEER_IP_ADDRESS
                                    (&L2VpnPwHwListEntry),
                                    &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                                    IPV4_ADDR_LENGTH))
                        {
                            u1IsSamePeerAddress = L2VPN_TRUE;
                        }
                    }
                }

                if ((L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_REMOTE_VE_ID (pPwVcEntry)) ||
                    (L2VPN_TRUE != u1IsSamePeerAddress) ||
                    (L2VPN_PW_HW_LIST_OUT_LABEL (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_OUT_IF_INDEX (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUT_IF_INDEX (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_LSP_OUT_LABEL (&L2VpnPwHwListEntry) !=
                     L2VPN_PWVC_OUTER_OUT_LABEL (pPwVcEntry)) ||
                    (L2VPN_PW_HW_LIST_OUT_PORT (&L2VpnPwHwListEntry) !=
                     pMplsHwVcInfo->u4PwOutPort))
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : HwListMismatch for PWIndex = %u\n",
                                __func__, L2VPN_PWVC_INDEX (pPwVcEntry));
                    pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel =
                        L2VPN_PW_HW_LIST_OUT_LABEL (&L2VpnPwHwListEntry);
                    MEMCPY (&pMplsHwVplsInfo->mplsHwVplsVcInfo, pMplsHwVcInfo,
                            sizeof (tMplsHwVcTnlInfo));
                    if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
                         L2VPN_PWVC_NPAPI_SUCCESS)
                        && (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry)
                            & L2VPN_PWVC_NPAPI_CALLED))
                    {
#ifdef VPLS_GR_WANTED
                        if (L2VPN_ZERO !=
                            L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
                        {
                            L2VpnBgpDeletePwEntryOnMismatch
                                (&L2VpnPwHwListEntry);
                        }
                        else
#endif
                        {
                            L2vpnGrDeleteLdpPwEntryOnMisMatch
                                (&L2VpnPwHwListEntry);
                        }
                    }
                    pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel =
                        L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
                }
                else
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                                "%s : Complete match for PWIndex = %u\n",
                                __func__, L2VPN_PWVC_INDEX (pPwVcEntry));
                    if (pPwVcEntry->pServSpecEntry != NULL)
                    {
                        pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                            (L2VPN_PWVC_ENET_ENTRY_LIST
                             ((tPwVcEnetServSpecEntry *)
                              L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
                        if (pPwVcEnetEntry != NULL)
                        {
                            pPwVcEnetEntry->u4PwAcId =
                                L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry);
                        }
                    }

#if 0
                    L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
                        L2VPN_PW_STATUS_NOT_STALE;

                    L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_ZERO);
#endif
                    return L2VPN_SUCCESS;
                }
            }
        }
#endif
#endif
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)
        L2VpnPwVcHwListCreate (pPwVcEntry, pMplsHwVcInfo, &L2VpnPwHwListUpdate);
        L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate, L2VPN_ZERO);
#endif
#endif
        L2VpnCheckForAllPwsDown (u4VplsInstance, &bIsAllPwsDown);
        /* Check if all PWs are down, then remove the AC info. from hw */

        if (pSlotInfo == NULL)
        {
            if ((bIsAllPwsDown) ||
                (TMO_SLL_Count (&pVplsEntry->PwList) == L2VPN_ONE))
            {
                if (L2VpnPwVcHwVpnAcAdd (pPwVcEntry, pSlotInfo) ==
                    L2VPN_FAILURE)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "L2VpnPwVcHwVpnAcAdd: "
                                "PW %d for %#x failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
                    return L2VPN_FAILURE;
                }
            }
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)

            else
            {
                /*As this is not the first PW, so Ac is present in this VPLS */
                L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                        L2VPN_PWAC_NPAPI_SUCCESS |
                                        L2VPN_PWAC_NPAPI_CALLED);
            }
#endif
#endif

        }
        else
        {
            if (L2VpnPwVcHwVpnAcAdd (pPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2VpnPwVcHwVpnAcAdd: "
                            "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2VpnPwVcHwVpnAcAdd: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                return L2VPN_FAILURE;
            }
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)
            else
            {
                /*As this is not the first PW, so Ac is present in this VPLS */
                L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                        L2VPN_PWAC_NPAPI_SUCCESS |
                                        L2VPN_PWAC_NPAPI_CALLED);
            }
#endif
#endif

        }
        /* memcpy vcinfo to vpls info */
        if (pPwVcEntry->u1HwStatus & PW_HW_PW_UPDATE)
        {

            MEMCPY (&pMplsHwVplsInfo->mplsHwVplsVcInfo, pMplsHwVcInfo,
                    sizeof (tMplsHwVcTnlInfo));
            pMplsHwVplsInfo->u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
#ifdef MBSM_WANTED
            if (pSlotInfo != NULL)
            {
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)
                L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                        L2VPN_PWVC_NPAPI_CALLED);
#endif
                if (MplsFsMplsMbsmHwVplsAddPwVc
                    (u4VplsInstance, u4VpnId, pMplsHwVplsInfo, u4Action,
                     pSlotInfo) == FNP_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "MplsFsMplsMbsmHwVplsAddPwVc: "
                                "PW %d for Peer IPv4- %#x IPv6 -%s failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "FsMplsMbsmHwVplsAddPwVc: "
                                "PW %d for %#x failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                    return L2VPN_FAILURE;
                }
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)
                L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                        L2VPN_PWVC_NPAPI_SUCCESS);
#endif
            }
            else
#endif
            {
#ifdef L2RED_WANTED
                if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                {
#ifdef NPAPI_WANTED
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)
                    L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                            L2VPN_PWVC_NPAPI_CALLED);
#endif
                    /*Fetch Qos Info for the Outgoing Interface */
                    u4OutPort = pMplsHwVplsInfo->mplsHwVplsVcInfo.u4PwOutPort;
                    if (u4OutPort == 0)
                    {
                        MEMCPY (pHwPortInfo.au1Mac,
                                pMplsHwVplsInfo->mplsHwVplsVcInfo.
                                au1PwDstMac, MAC_ADDR_LEN);
                        pHwPortInfo.u2OutVlanId =
                            pMplsHwVplsInfo->mplsHwVplsVcInfo.u2OutVlanId;
                        pHwPortInfo.u1MsgType = ISS_NP_GET_PORT_INFO;
                        CfaNpGetHwPortInfo (&pHwPortInfo);
                        u4OutPort = pHwPortInfo.u4OutPort;
                    }
#ifdef QOSX_WANTED
                    QosGetMplsExpProfileForPortAtEgress (u4OutPort,
                                                         &i4HwExpMapId);
                    pMplsHwVplsInfo->mplsHwVplsVcInfo.QosInfo[0].u.
                        MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
                    QosGetMplsExpProfileForPortAtIngress (u4OutPort,
                                                          &i4HwExpMapId);
                    pMplsHwVplsInfo->mplsHwVplsVcInfo.QosInfo[0].u.
                        MplsQosPolicy.i4HwPriMapId = i4HwExpMapId;
#endif

                    if (MplsFsMplsHwVplsAddPwVc
                        (u4VplsInstance, u4VpnId, pMplsHwVplsInfo,
                         u4Action) == FNP_FAILURE)
                    {
#ifdef MPLS_IPV6_WANTED

                        L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                    "FsMplsHwVplsAddPwVc: "
                                    "PW %d for Peer IPv4- %#x IPv6 -%s failed in HW\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                        L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                    "MplsFsMplsHwVplsAddPwVc: "
                                    "PW %d for %#x failed in HW\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                        return L2VPN_FAILURE;
                    }
#if defined (VPLS_GR_WANTED) || defined (LDP_GR_WANTED)
                    L2VpnPwHwListAddUpdate (&L2VpnPwHwListUpdate,
                                            L2VPN_PWVC_NPAPI_SUCCESS);
#endif
#endif
                }
            }
        }
        u1HwStatus = (UINT1) ((pPwVcEntry->u1HwStatus & 0x0F) |
                              ((UINT1) (pPwVcEntry->u1HwStatus & 0xF0) >>
                               MPLS_FOUR)) << MPLS_FOUR;

        L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ZERO);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddWrp: "
                "PW %d for Peer IPv4 -%#x IPv6 -%s successfully added in HW\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddWrp: "
                "PW %d for %#x successfully added in HW\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwRedVcHwModify                                      */
/* Description   : Adds the PWVC into the hw                                 */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pRgGrp     - pointer to tL2vpnRedundancyEntry             */
/*                 Slot Info  - Slot information                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnPwRedVcHwModify (tPwVcEntry * pPwVcEntry, tL2vpnRedundancyEntry * pRgGrp,
                      VOID *pSlotInfo)
{

    tMplsHwVcTnlInfo    MplsHwVcInfo;
    tPwVcEntry         *pPwVcOperActEntry = NULL;
    UINT4               u4OperActivePwIdx = 0;
    UINT4               u4OperActVpnId = 0;
    UINT4               u4VpnId = 0;
    INT4                i4Vsi = 0;
    INT4                i4TmpVsi = 0;

    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    if (pRgGrp != NULL)
    {
        if ((pRgGrp->pOperActivePw != NULL) &&
            (pRgGrp->pOperActivePw->b1IsLocal == TRUE) &&
            (pRgGrp->pOperActivePw->pRgParent != NULL) &&
            (((tL2vpnRedundancyPwEntry *) (VOID *)
              (pRgGrp->pOperActivePw->pRgParent))->pPwVcEntry != NULL))
        {
            u4OperActivePwIdx = L2VPN_PWVC_INDEX
                (L2VPNRED_ICCP_PW_PARENT_PW
                 (pRgGrp->pOperActivePw)->pPwVcEntry);
        }
    }

    pPwVcOperActEntry = L2VpnGetPwVcEntryFromIndex (u4OperActivePwIdx);

    if ((pPwVcOperActEntry == NULL) ||
        (L2VpnGetVpnIdAndVsiFromPw (pPwVcOperActEntry,
                                    &u4OperActVpnId, &i4TmpVsi)
         == L2VPN_FAILURE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                          &MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                          &MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);

    L2VpnPwRedVcHwModifyWrp (u4VpnId, u4OperActVpnId, pPwVcEntry,
                             &MplsHwVcInfo, pSlotInfo);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwRedVplsHwModify                                    */
/* Description   : Block/Unblock the VPLS PWVC into the hw                   */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 bIsBlocking     - whether to block the PW or not          */
/*                 Slot Info  - Slot information                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwRedVplsHwModify (tPwVcEntry * pPwVcEntry, BOOL1 bIsBlocking,
                        VOID *pSlotInfo)
{

    tMplsHwVcTnlInfo    MplsHwVcInfo;
    UINT4               u4OperActVpnId = 0;
    UINT4               u4VpnId = 0;
    INT4                i4Vsi = 0;
    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));
    UNUSED_PARAM (pSlotInfo);
    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                          &MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                          &MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);
    MplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;
    if (L2VPN_TRUE == bIsBlocking)
    {
        u4OperActVpnId = u4VpnId;
    }
#ifdef L2RED_WANTED
    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
    {
#ifdef NPAPI_WANTED
        if ((MplsFsMplsHwModifyPwVc
             (u4VpnId, u4OperActVpnId, &MplsHwVcInfo)) == FNP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsHwModifyPwVc: PW %d failed in HW\n",
                        pPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }
#endif
    }
    UNUSED_PARAM (u4OperActVpnId);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwRedVcHwModifyWrp                                   */
/* Description   : Adds the PWVC into the hw                                 */
/* Input(s)      : u4VpnId - VPN ID                                          */
/*                 u4OperActVpnId - Oper Active VPN ID                       */
/*                 pMplsHwVcInfo  - Pointer to H/W VC Info                   */
/*                 pSlotInfo      - pointer to Slot Info                     */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnPwRedVcHwModifyWrp (UINT4 u4VpnId, UINT4 u4OperActVpnId,
                         tPwVcEntry * pPwVcEntry,
                         tMplsHwVcTnlInfo * pMplsHwVcInfo, VOID *pSlotInfo)
{
#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4VpnId);
    UNUSED_PARAM (u4OperActVpnId);
    UNUSED_PARAM (pMplsHwVcInfo);
#endif
    UNUSED_PARAM (pSlotInfo);

    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            if ((MplsFsMplsHwModifyPwVc
                 (u4VpnId, u4OperActVpnId, pMplsHwVcInfo)) == FNP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwModifyPwVc: PW %d failed in HW\n",
                            pPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }
#endif
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDelWrp                                         */
/* Description   : Deletes the PWVC into the hw                              */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDelWrp (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                   tMplsHwVcTnlInfo * pMplsHwVcInfo,
                   tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    UINT1               u1AppOwner = MPLS_APPLICATION_NONE;
    UINT4               u4VplsInstance = 0;
    UINT1               u1HwStatus = L2VPN_ZERO;
    UINT1               u1UpdateHwStatus = pPwVcEntry->u1HwStatus;
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListDelKey;
#endif
#endif
    tGenU4Addr          GenU4Addr;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    u1AppOwner = L2VpPortGetPortOwnerFromIndex (pPwVcEntry->u4PwIfIndex);
    pMplsHwVcInfo->u1AppOwner = u1AppOwner;
    pMplsHwVplsInfo->mplsHwVplsVcInfo.u1AppOwner = u1AppOwner;

#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
    L2VpnPwHwListDelKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    L2VpnPwHwListDelKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);
#endif
#endif
    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
    {
        /* Ac update is specified but no vlan and port information is provided
         */
        if (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE)
        {
            if ((pMplsHwVcInfo->FecInfo.u4VpnSrcVlan == L2VPN_ZERO) &&
                (pMplsHwVcInfo->FecInfo.u4VpnSrcPhyPort == L2VPN_ZERO))
            {
                /* For MultiSegment Pseudowire Pseudowire Hardware programming 
                 * should not be done.
                 * This condition will get hit at the Segmented (Cross connected)
                 * router where PW is created through signalling */
                return L2VPN_SUCCESS;
            }
        }

        if (pPwVcEntry->pServSpecEntry != NULL)
        {
            pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                             L2VPN_PWVC_ENET_ENTRY
                                             (pPwVcEntry)));
        }
        if (pPwVcEnetEntry != NULL)
        {
            pMplsHwVcInfo->u4PwAcId = pPwVcEnetEntry->u4PwAcId;
        }
        pMplsHwVcInfo->u1VpwsPwHwAction = u1UpdateHwStatus;
#ifdef MBSM_WANTED
        if (pSlotInfo != NULL)
        {
#ifdef LDP_GR_WANTED
            L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                 L2VPN_PWAC_NPAPI_CALLED |
                                 L2VPN_PWVC_NPAPI_CALLED);
#endif
            if (MplsFsMplsMbsmHwDeletePwVc
                (u4VpnId, pMplsHwVcInfo, u4Action, pSlotInfo) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsMbsmHwDeletePwVc: "
                            "PW %d for Peer IPv4-  %#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "MplsFsMplsMbsmHwDeletePwVc: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                return L2VPN_FAILURE;
            }
#ifdef LDP_GR_WANTED
            L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                 L2VPN_PWVC_NPAPI_SUCCESS |
                                 L2VPN_PWAC_NPAPI_SUCCESS);
#endif

        }
        else
#endif
        {
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED
#ifdef LDP_GR_WANTED
                if (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE)
                {
                    L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                         L2VPN_PWAC_NPAPI_CALLED);
                }
                L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                     L2VPN_PWVC_NPAPI_CALLED);
#endif
                if (MplsFsMplsHwDeletePwVc (u4VpnId, pMplsHwVcInfo, u4Action) ==
                    FNP_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG4 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwDeletePwVc: "
                                "PW %d for Peer IPv4 -%#x IpV6 -%s failed in HW when passing "
                                "HwAction = %x \n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                                pMplsHwVcInfo->u1VpwsPwHwAction);
#else
                    L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsHwDeletePwVc: "
                                "PW %d for %#x failed in HW when passing "
                                "HwAction = %x \n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                pMplsHwVcInfo->u1VpwsPwHwAction);
#endif
                    return L2VPN_FAILURE;
                }
#ifdef LDP_GR_WANTED
                if (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE)
                {
                    L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                         L2VPN_PWAC_NPAPI_SUCCESS);
                }
                L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                     L2VPN_PWVC_NPAPI_SUCCESS);
#endif
#endif
            }
        }

        if ((pPwVcEnetEntry != NULL) &&
            (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE))
        {
            MplsL2VpnRelAcId (pPwVcEnetEntry->u4PwAcId);
            pPwVcEnetEntry->u4PwAcId = L2VPN_ZERO;
        }
        u1HwStatus = (UINT1) ((pPwVcEntry->u1HwStatus & 0x0F) ^
                              ((UINT1)
                               ((pPwVcEntry->
                                 u1HwStatus & 0xF0) >> MPLS_FOUR))) <<
            MPLS_FOUR;
        L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ZERO);
    }
    else if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        if (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) == 0)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "VPLS index is not associated with the PW\n");
            return L2VPN_FAILURE;
        }
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
            (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
        if (pVplsEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "VPLS entry is NULL\n");
            return L2VPN_FAILURE;
        }

        /* Fill up the VPLS info */
        MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]),
                sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
        u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "VPLS Instance: %d\n", u4VplsInstance);
        pMplsHwVcInfo->FecInfo.i4VSI = L2VPN_VPLS_VSI (pVplsEntry);
        pMplsHwVcInfo->FecInfo.u4VplsInstance =
            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        if (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE)
        {
            if (L2VpnPwVcHwVpnAcDel (pPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2VpnPwVcHwVpnAcDel: "
                            "PW %d for Peer IPv4 - %#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2VpnPwVcHwVpnAcDel: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                return L2VPN_FAILURE;
            }
        }

        if ((pPwVcEntry->u1HwStatus & PW_HW_PW_UPDATE) &&
            (pPwVcEntry->u1HwStatus & PW_HW_PW_PRESENT))
        {
            /* memcpy vcinfo to vpls info */
            MEMCPY (&pMplsHwVplsInfo->mplsHwVplsVcInfo, pMplsHwVcInfo,
                    sizeof (tMplsHwVcTnlInfo));
            pMplsHwVplsInfo->u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
#ifdef MBSM_WANTED
            if (pSlotInfo != NULL)
            {
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
                L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                     L2VPN_PWVC_NPAPI_CALLED);
#endif
                if (MplsFsMplsMbsmHwVplsDeletePwVc (u4VplsInstance, u4VpnId,
                                                    pMplsHwVplsInfo, u4Action,
                                                    pSlotInfo) == FNP_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsMbsmHwVplsDeletePwVc: "
                                "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsMbsmHwVplsDeletePwVc: "
                                "PW %d for %#x failed in HW\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                    return L2VPN_FAILURE;
                }
            }
            else
#endif
            {
#ifdef L2RED_WANTED
                if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                {
#ifdef NPAPI_WANTED
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
                    L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                         L2VPN_PWVC_NPAPI_CALLED);
#endif
                    if (MplsFsMplsHwVplsDeletePwVc (u4VplsInstance, u4VpnId,
                                                    pMplsHwVplsInfo,
                                                    u4Action) == FNP_FAILURE)
                    {
#ifdef MPLS_IPV6_WANTED

                        L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                                    "FsMplsHwVplsDeletePwVc: "
                                    "PW %d for Peer -IPv4 %#x IPv6 -%s failed in HW\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                        L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                    "FsMplsHwVplsDeletePwVc: "
                                    "PW %d for %#x failed in HW\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                        return L2VPN_FAILURE;
                    }
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
                    L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                         L2VPN_PWVC_NPAPI_SUCCESS);
#endif
#endif
                }
            }
        }
        u1HwStatus = (UINT1) ((pPwVcEntry->u1HwStatus & 0x0F) ^
                              ((UINT1)
                               ((pPwVcEntry->
                                 u1HwStatus & 0xF0) >> MPLS_FOUR))) <<
            MPLS_FOUR;
        L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ZERO);

    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcIlmHwDel                                         */
/* Description   : Adds the PWVC ILM into the hw                             */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 bIsPwVcOperLlDown                                         */
/*                    - TRUE -> Lower layer tunnel is down                   */
/*                    - FALSE -> Lower layer tunnel is not down              */
/*                 pSlotInfo -> Slot information                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcIlmHwDel (tPwVcEntry * pPwVcEntry, BOOL1 bIsPwVcOperLlDown,
                   VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    UINT4               u4VpnId = 0;
    UINT4               u4InIntf = 0;
    UINT4               u4Action;
    UINT4               u4InLabel = L2VPN_INVALID_LABEL;
    INT4                i4Vsi = 0;
    tVlanIfaceVlanId    VlanId;
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListDelKey;
#endif
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    u4Action = L2VPN_ZERO;
    /* Init Npapi related data structures */
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));
    MEMSET (&GenU4Addr, 0, sizeof (tGenU4Addr));

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);
    pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);

    if ((pMplsInTnlEntry == NULL) || (pMplsOutTnlEntry == NULL))
    {
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "ILM Delete for PW %d %x started\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr));

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }
    /* Fill PSN (PW) Port IF index, AC Port IF Index,
     * PSN (PW) Vlan, AC port vlan */
    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwIlmInfo.u4NewVlanId,
                          &MplsHwIlmInfo.u4OutPort,
                          &MplsHwIlmInfo.u4PwEnetPwVlan,
                          &MplsHwIlmInfo.i1PwEnetVlanMode);

    /* Fill ILM Info in MplsHwIlmInfo */
    MplsHwIlmInfo.OutLabelToLearn.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;

    /* Terminate this ILM Entry and do L2 switching for L2 VPN */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_L2_SWITCH;

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Enet (AC) Information: Port: %d Vlan: %d Change Vlan: %d "
                "Vlan Mode: %d\n",
                MplsHwIlmInfo.u4OutPort, MplsHwIlmInfo.u4NewVlanId,
                MplsHwIlmInfo.u4PwEnetPwVlan, MplsHwIlmInfo.i1PwEnetVlanMode);
    /* PwMplsType = TE */
    if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE)
    {
        /* Pw mapped over P2MP tunnel - 
           In tunnel can be zero */
        if (L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pMplsInTnlEntry) == 0)
        {
            return L2VPN_SUCCESS;
        }

        if (pPwVcEntry->u4InOuterMPLabel1 != L2VPN_LDP_INVALID_LABEL)
        {
            if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                           pPwVcEntry->u4InOuterMPLabel1,
                                           pPwVcEntry->u4InMPLabel1,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           bIsPwVcOperLlDown) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "FRR Ilm Hw Del For MP label1 Failed\n");
            }
        }
        if (pPwVcEntry->u4InOuterMPLabel2 != L2VPN_LDP_INVALID_LABEL)
        {
            if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                           pPwVcEntry->u4InOuterMPLabel2,
                                           pPwVcEntry->u4InMPLabel2,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           bIsPwVcOperLlDown) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "FRR Ilm Hw Del For MP label2 Failed\n");
            }
        }
        /* fill ILM info */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf = pPwVcEntry->u4InIfIndex;
        u4InLabel = L2VpnPassValidInLabelToHw (pPwVcEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Incoming Tunnel Label: %d, In VC Label: %d, "
                    "Incoming L3Intf: %d\n",
                    pPwVcEntry->u4InOuterLabel, u4InLabel,
                    pPwVcEntry->u4InIfIndex);
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Outgoing VC Label: %d, Outgoing L3Intf: %d\n",
                    pPwVcEntry->u4OutVcLabel, pPwVcEntry->u4PwL3Intf);
    }
    /* PwMplsType = PwOnly */
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) ==
             L2VPN_MPLS_TYPE_VCONLY)
    {
        MplsHwIlmInfo.u4InL3Intf = u4InIntf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pMplsOutTnlEntry);

        pPwVcEntry->u4InIfIndex = u4InIntf;
        u4InLabel = L2VpnPassValidInLabelToHw (pPwVcEntry);
        /* fill ILM info */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4SrcPort = pMplsOutTnlEntry->unTnlInfo.VcInfo.u4IfIndex;
        MplsHwIlmInfo.u4OutL3Intf =
            CfaGetVlanInterfaceIndex ((UINT2) MplsHwIlmInfo.u4NewVlanId);
    }
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) ==
             L2VPN_MPLS_TYPE_NONTE)
    {
#ifndef MPLS_SIG_WANTED
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Pseudo Wire NON-TE is not supported on manual outer tunnel \n");
        return L2VPN_FAILURE;
#endif
        if (L2VpnPwVcIlmHwDelForNonTePsn (pPwVcEntry, &MplsHwIlmInfo,
                                          bIsPwVcOperLlDown, pSlotInfo)
            == L2VPN_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "ILM delete failed for NON-TE PSN\n", u4InIntf);
            return L2VPN_FAILURE;
        }
        /* Reset the XC Index. */
        pMplsInTnlEntry->u4LsrXcIndex = L2VPN_ZERO;
    }
    u4InIntf = pPwVcEntry->u4InIfIndex;
    if (u4InIntf != 0)
    {
        if (CfaGetVlanId (u4InIntf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d\n", u4InIntf);
            return L2VPN_FAILURE;
        }
        MplsHwIlmInfo.u4InVlanId = VlanId;
    }
    else
    {
        MplsHwIlmInfo.u4InVlanId = 0;

    }
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Incoming VLAN ID : %d VPN Id: %d\n",
                MplsHwIlmInfo.u4InVlanId, u4VpnId);

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
        if (L2VPN_FALSE == L2VPN_PWVC_NPAPI (pPwVcEntry))
        {
            L2VPN_PWVC_NPAPI (pPwVcEntry) = L2VPN_TRUE;
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                       "NPAPI is not called in case of GR\n");
            return L2VPN_SUCCESS;
        }

        MEMSET (&L2VpnPwHwListDelKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
        L2VpnPwHwListDelKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VpnPwHwListDelKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

        L2VpnPwHwListDelete (&L2VpnPwHwListDelKey, L2VPN_PWILM_NPAPI_CALLED);
#endif
        if (MplsFsMplsMbsmHwDeleteILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwDeleteILM: "
                        "PW %d for Peer IPv4 %#x IPv6 -%s failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwDeleteILM: "
                        "PW %d for %#x failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
            return L2VPN_FAILURE;
        }
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)

        L2VpnPwHwListDelete (&L2VpnPwHwListDelKey, L2VPN_PWILM_NPAPI_SUCCESS);
#endif

    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
            if (L2VPN_FALSE == L2VPN_PWVC_NPAPI (pPwVcEntry))
            {
                L2VPN_PWVC_NPAPI (pPwVcEntry) = L2VPN_TRUE;
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                           "NPAPI is not called in case of GR\n");
                return L2VPN_SUCCESS;
            }
            L2VpnPwHwListDelKey.u4VplsIndex =
                L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
            L2VpnPwHwListDelKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

            L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                 L2VPN_PWILM_NPAPI_CALLED);
#endif
            if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwDeleteILM: "
                            "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwDeleteILM: "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
                return L2VPN_FAILURE;
            }
#endif
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)

            L2VpnPwHwListDelete (&L2VpnPwHwListDelKey,
                                 L2VPN_PWILM_NPAPI_SUCCESS);
#endif
            pPwVcEntry->b1IsIlmEntryPresent = FALSE;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcFetchAcInfo                                      */
/* Description   : Fetches the AC info. from PW Enet entry                   */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/* Output(s)     : pu4VpnAcVlan - VPN AC vlan                                */
/*                 pu4VpnAcPhyPort - VPN AC physical port                    */
/*                 pu4VpnPwVlan - VPN AC PW vlan used for VLAN change        */
/*                 pi1VpnPwVlanMode - pu4VpnPwVlan is operated based         */
/*                 on this vlan mode                                         */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcFetchAcInfo (tPwVcEntry * pPwVcEntry, UINT4 *pu4VpnAcVlan,
                      UINT4 *pu4VpnAcPhyPort, UINT4 *pu4VpnPwVlan,
                      INT1 *pi1VpnPwVlanMode)
{
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    /* fill up AC info in MplsHwVcInfo */

    /*MS-PW: check AC attached */
    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "AC is not configured for the PWVC\n");
        return L2VPN_FAILURE;
    }

    pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
        (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                     L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
    if (pPwVcEnetEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "AC is not configured for the PWVC\n");
        return L2VPN_FAILURE;
    }
    *pu4VpnAcVlan = (UINT4) L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
    *pu4VpnAcPhyPort = (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
    *pu4VpnPwVlan = (UINT4) L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
    *pi1VpnPwVlanMode = L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnMplsFrrILMHwAddForMP
 *  Description     : Adds the FRR enabled tnl's MP with PWVC label 
 *  Input           : pPwVcEntry - PWVC information
 *                    u4BkpTnlXcIndex - Backup tnl XC index
 *                    pSlotInfo - Slot information
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT4
L2VpnMplsFrrILMHwAddForMP (tPwVcEntry * pPwVcEntry, UINT4 u4BkpTnlXcIndex,
                           VOID *pSlotInfo)
{
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegIndex = NULL;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    tVlanIfaceVlanId    VlanId;
    UINT4               u4InIntf = L2VPN_ZERO;
    UINT4               u4VpnId = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = L2VPN_ZERO;
#endif
#endif
    tGenU4Addr          GenU4Addr;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsFrrILMHwAddForMP: Entry\n");

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_HTONL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    /* fill up AC info in MplsHwVcInfo */
    /* Fill PSN (PW) Port IF index, AC Port IF Index,
     * PSN (PW) Vlan, AC port vlan */
    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwIlmInfo.u4NewVlanId,
                          &MplsHwIlmInfo.u4OutPort,
                          &MplsHwIlmInfo.u4PwEnetPwVlan,
                          &MplsHwIlmInfo.i1PwEnetVlanMode);

    /* Fill ILM Info in MplsHwIlmInfo */
    MplsHwIlmInfo.OutLabelToLearn.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;

    /* Terminate this ILM Entry and do L2 switching for L2 VPN */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_L2_SWITCH;

    pXcEntry = MplsGetXCEntryByDirection (u4BkpTnlXcIndex, MPLS_DEF_DIRECTION);
    if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL) ||
        (pXcEntry->u1OperStatus != XC_OPER_UP))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnMplsFrrILMHwAddForMP: "
                    "XC Index %d do not exist or not oper up\n",
                    u4BkpTnlXcIndex);
        return L2VPN_FAILURE;
    }
    pInSegIndex = pXcEntry->pInIndex;

    u4InIntf = pInSegIndex->u4IfIndex;

    pPwVcEntry->i4NPop = pInSegIndex->i4NPop;

    if (MplsGetL3Intf (u4InIntf, &(pPwVcEntry->u4MPInIfIndex)) == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "ILM ADD (MPLS Tnl if) for "
                   "POP_SEARCH INTMD EXIT: MplsGetL3Intf Failed\n");
        return L2VPN_FAILURE;
    }

    if (pInSegIndex->i4NPop == L2VPN_TWO)
    {
        /* Add the ILM for two MP labels with POP_SEARCH */
        if (L2VpnMplsILMHwAddForPopSearch (pInSegIndex, TRUE, pPwVcEntry,
                                           pSlotInfo) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2VpnMplsFrrILMHwAddForMP: "
                       "POP Search for L2VPN on LDP over RSVP using "
                       " NON-TE creation failed in HW\n");
            return L2VPN_FAILURE;
        }
        /* Don't do the ILM with a VC label, 
         * as it is done during PWVC creation */
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsFrrILMHwAddForMP: "
                   "PWVC ILM with one VC label programming "
                   "need not to be done \n");
        return L2VPN_SUCCESS;
    }
    else
    {
        /* fill ILM info */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = pInSegIndex->u4Label;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = pPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        if (pPwVcEntry->u4InOuterMPLabel1 == L2VPN_LDP_INVALID_LABEL)
        {
            pPwVcEntry->u4InOuterMPLabel1 = pInSegIndex->u4Label;
        }
        else if (pPwVcEntry->u4InOuterMPLabel2 == L2VPN_LDP_INVALID_LABEL)
        {
            pPwVcEntry->u4InOuterMPLabel2 = pInSegIndex->u4Label;
        }
    }

    u4InIntf = pPwVcEntry->u4MPInIfIndex;
    if (CfaGetVlanId (u4InIntf, &VlanId) == CFA_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "L2VpnMplsFrrILMHwAddForMP: "
                    "Unable to get InVlanId from L3Intf %d\n", u4InIntf);
        return L2VPN_FAILURE;
    }
    if (u4InIntf != 0)
    {
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4InIntf, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4SrcPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", u4InIntf);
        }

    }
    /* Fill the incoming L3 interface */
    MplsHwIlmInfo.u4InL3Intf = u4InIntf;
    /* Fill the incoming L3 vlan id */
    MplsHwIlmInfo.u4InVlanId = VlanId;

    L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnMplsFrrILMHwAddForMP:"
                "Incoming Labels - Outer Tunnel: %d VC label: %d L3Intf: %d "
                "Vlan: %d, VPN Id: %d\n",
                MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel,
                MplsHwIlmInfo.u4InL3Intf, MplsHwIlmInfo.u4InVlanId, u4VpnId);

#ifdef NPAPI_WANTED
#ifdef QOSX_WANTED
    /* Fetch the Qos Information for teh incoming port */
    QosGetMplsExpProfileForPortAtIngress (MplsHwIlmInfo.u4SrcPort,
                                          &i4HwExpMapId);
    MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
    QosGetMplsExpProfileForPortAtEgress (MplsHwIlmInfo.u4OutPort,
                                         &i4HwExpMapId);
    MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwPriMapId = i4HwExpMapId;
#endif
    MplsHwIlmInfo.u4VpnId = u4VpnId;
    if (MplsFsMplsHwCreateILM (&MplsHwIlmInfo, MPLS_HW_ACTION_L2_SWITCH)
        == FNP_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                    "L2VpnMplsFrrILMHwAddForMP: "
                    "FsMplsHwCreateILM: PW %d for Peer IPv4- %#x IPv6 -%s failed in HW\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                    "L2VpnMplsFrrILMHwAddForMP: "
                    "FsMplsHwCreateILM: PW %d for %#x failed in HW\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
        return L2VPN_FAILURE;
    }
#endif
    pPwVcEntry->b1IsIlmEntryPresent = TRUE;
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsFrrILMHwAddForMP: Exit\n");
    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnMplsFrrILMHwDelForMP
 *  Description     : Deletes the FRR enabled tnl's MP with PWVC label 
 *  Input           : pPwVcEntry - PWVC information
 *                    u4InOuterMPLabel1 - First FRR incoming MP label
 *                    u4InOuterMPLabel2 - Second FRR incoming MP label
 *                    u4InOuterMPLabel3 - Third FRR incoming MP label
 *                    bIsPwVcOperLlDown - To provide information 
 *                    about lower layer down
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT4
L2VpnMplsFrrILMHwDelForMP (tPwVcEntry * pPwVcEntry, UINT4 u4InOuterMPLabel1,
                           UINT4 u4InOuterMPLabel2, UINT4 u4InOuterMPLabel3,
                           BOOL1 bIsPwVcOperLlDown)
{
    tMplsHwIlmInfo      MplsHwIlmInfo;
    tVlanIfaceVlanId    VlanId;
    UINT4               u4InIntf = L2VPN_ZERO;
    INT4                i4IsIlmDeleted = L2VPN_TRUE;
    UINT4               u4VpnId = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
    UINT4               u4InLabel = L2VPN_INVALID_LABEL;
    tGenU4Addr          GenU4Addr;
    UNUSED_PARAM (u4InOuterMPLabel2);

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_HTONL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsFrrILMHwDelForMP: Entry\n");

    /* Fill PSN (PW) Port IF index, AC Port IF Index,
     * PSN (PW) Vlan, AC port vlan */
    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwIlmInfo.u4NewVlanId,
                          &MplsHwIlmInfo.u4OutPort,
                          &MplsHwIlmInfo.u4PwEnetPwVlan,
                          &MplsHwIlmInfo.i1PwEnetVlanMode);

    /* Fill ILM Info in MplsHwIlmInfo */
    MplsHwIlmInfo.OutLabelToLearn.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;

    /* Terminate this ILM Entry and do L2 switching for L2 VPN */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_L2_SWITCH;

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnMplsFrrILMHwDelForMP: Enet (AC) Information: "
                "Port: %d Vlan: %d Change Vlan: %d "
                "Vlan Mode: %d\n",
                MplsHwIlmInfo.u4OutPort, MplsHwIlmInfo.u4NewVlanId,
                MplsHwIlmInfo.u4PwEnetPwVlan, MplsHwIlmInfo.i1PwEnetVlanMode);

    if (pPwVcEntry->i4NPop == MPLS_TWO)
    {
        /* TODO FRR N:1 label passing needs to be handled properly */
        if (L2VpnMplsILMHwDelForPopSearch (u4InOuterMPLabel1,    /* Tunnel label */
                                           u4InOuterMPLabel3,    /* LDP label */
                                           pPwVcEntry->u4MPInIfIndex,
                                           bIsPwVcOperLlDown,
                                           NULL,
                                           &i4IsIlmDeleted) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "POP Search for L2VPN on LDP over RSVP using "
                       " NON-TE deletion failed in HW\n");
            return L2VPN_FAILURE;
        }
        if (i4IsIlmDeleted == L2VPN_TRUE)
        {
            pPwVcEntry->b1IsIlmEntryPresent = L2VPN_FALSE;
        }

    }
    else
    {
        /* fill ILM info */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4InOuterMPLabel1;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf = pPwVcEntry->u4MPInIfIndex;
        u4InLabel = L2VpnPassValidInLabelToHw (pPwVcEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    }

    /* FRR N:1 bypass path MP handle */
    if (pPwVcEntry->u4InOuterMPLabel1 == u4InOuterMPLabel1)
    {
        pPwVcEntry->u4InOuterMPLabel1 = L2VPN_LDP_INVALID_LABEL;
        pPwVcEntry->u4InMPLabel1 = L2VPN_LDP_INVALID_LABEL;
    }
    else if (pPwVcEntry->u4InOuterMPLabel2 == u4InOuterMPLabel1)
    {
        pPwVcEntry->u4InOuterMPLabel2 = L2VPN_LDP_INVALID_LABEL;
        pPwVcEntry->u4InMPLabel2 = L2VPN_LDP_INVALID_LABEL;
    }

    if (pPwVcEntry->i4NPop == MPLS_TWO)
    {
        /* PwVc ILM Deletion of one VC Label is not required as it 
         * is done during PW VC Deletion */
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnMplsFrrILMHwDelForMP: PwVc ILM Del need not be "
                   "done\n");
        return L2VPN_SUCCESS;
    }
    u4InIntf = pPwVcEntry->u4MPInIfIndex;

    if (MplsGetL3Intf (u4InIntf, &u4InIntf) == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "ILM DEL (MPLS Tnl if) for "
                   "POP_SEARCH INTMD EXIT: MplsGetL3Intf Failed\n");
        return L2VPN_FAILURE;
    }

    if (CfaGetVlanId (u4InIntf, &VlanId) == CFA_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Unable to get InVlanId from L3Intf %d\n", u4InIntf);

        return L2VPN_FAILURE;
    }
    MplsHwIlmInfo.u4InVlanId = VlanId;

    L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnMplsFrrILMHwDelForMP: "
                "Incoming Labels - Outer Tunnel: %d VC: %d Intf: %d "
                "Vlan: %d VPN Id: %d\n",
                MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel,
                MplsHwIlmInfo.u4InL3Intf, MplsHwIlmInfo.u4InVlanId, u4VpnId);

#ifdef NPAPI_WANTED
    MplsHwIlmInfo.u4VpnId = u4VpnId;
    if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo,
                               MPLS_HW_ACTION_L2_SWITCH) == FNP_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                    "FsMplsHwDeleteILM: PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                    "FsMplsHwDeleteILM: PW %d for %#x failed in HW\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
        return L2VPN_FAILURE;
    }
#endif
    pPwVcEntry->b1IsIlmEntryPresent = FALSE;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsFrrILMHwDelForMP: Exit\n");
    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnMplsILMHwAddForPopSearch
 *  Description     : This function deletes the POP_L3_SWITCH
 *                    ILM entry and adds the ILM entry with
 *                    POP_SEARCH label action
 *  Input           : InSegment, PW VC Entry Information, 
 *                    PSN backup path tnl and Slot Information
 *  Output          : None
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT4
L2VpnMplsILMHwAddForPopSearch (tInSegment * pInSegment, BOOL1 bIsPsnBkpPath,
                               tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tMplsHwIlmInfo      MplsHwIlmInfo;
    tXcEntry           *pXcEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    UINT4               u4InL3Intf = L2VPN_ZERO;
    UINT2               u2InVlanId = L2VPN_ZERO;
    UINT1               u1LblStkCnt = L2VPN_ZERO;
    UINT4               u4TmpLabel1 = L2VPN_LDP_INVALID_LABEL;
    UINT4               u4TmpLabel2 = L2VPN_LDP_INVALID_LABEL;
    UINT4               u4TmpLabel3 = L2VPN_LDP_INVALID_LABEL;
    tGenU4Addr          GenU4Addr;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    pXcEntry = pInSegment->pXcIndex;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_HTONL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwAddForPopSearch ENTRY\n");

    if (pXcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "ILM ADD for POP_SEARCH INTMD EXIT: XC NULL\n");
        return L2VPN_FAILURE;
    }

    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));
    if ((pInSegment->u4Label == 0) && (pInSegment->mplsLabelIndex != 0))
    {
        u1LblStkCnt =
            (UINT1) TMO_SLL_Count (&(pInSegment->mplsLabelIndex->LblList));

        if (pInSegment->i4NPop != u1LblStkCnt)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "ILM ADD for POP_SEARCH INTMD EXIT: N-Hop Not 2\n");
            return L2VPN_FAILURE;
        }
        TMO_SLL_Scan (&(pInSegment->mplsLabelIndex->LblList), pLblEntry,
                      tLblEntry *)
        {
            if (u4TmpLabel1 == L2VPN_LDP_INVALID_LABEL)
            {
                u4TmpLabel1 = pLblEntry->u4Label;
            }
            else if (u4TmpLabel2 == L2VPN_LDP_INVALID_LABEL)
            {
                u4TmpLabel2 = pLblEntry->u4Label;
            }
            else
            {
                u4TmpLabel3 = pLblEntry->u4Label;
            }
        }
    }

    if (u4TmpLabel1 != L2VPN_LDP_INVALID_LABEL)
    {
        /* First Label: Tunnel Label or Bypass tunnel label */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4TmpLabel1;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        if (!bIsPsnBkpPath)
        {
            pPwVcEntry->u4InOuterLabel = u4TmpLabel1;
        }
        else if (pPwVcEntry->u4InOuterMPLabel1 == L2VPN_LDP_INVALID_LABEL)
        {
            pPwVcEntry->u4InOuterMPLabel1 = u4TmpLabel1;
        }
        else if (pPwVcEntry->u4InOuterMPLabel2 == L2VPN_LDP_INVALID_LABEL)
        {
            pPwVcEntry->u4InOuterMPLabel2 = u4TmpLabel1;
        }
    }
    if (u4TmpLabel2 != L2VPN_LDP_INVALID_LABEL)
    {
        /* Second Label: LDP Label or protected tunnel label */
        if (u4TmpLabel3 == L2VPN_LDP_INVALID_LABEL)
        {
            pPwVcEntry->u4InOuterLdpLabel = u4TmpLabel2;
            MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4TmpLabel2;
            MplsHwIlmInfo.InLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
        }
        else
        {
            if (pPwVcEntry->u4InMPLabel1 == L2VPN_LDP_INVALID_LABEL)
            {
                pPwVcEntry->u4InMPLabel1 = u4TmpLabel2;
            }
            else if (pPwVcEntry->u4InMPLabel2 == L2VPN_LDP_INVALID_LABEL)
            {
                pPwVcEntry->u4InMPLabel2 = u4TmpLabel2;
            }
        }
    }
    if (u4TmpLabel3 != L2VPN_LDP_INVALID_LABEL)
    {
        pPwVcEntry->u4InOuterLdpLabel = u4TmpLabel3;
    }

    /* TODO FRR N:1 handling */

    /* Label to be swapped */
    if (pXcEntry->pOutIndex != NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "ILM ADD for POP_SEARCH INTMD EXIT: OutIndex is not NULL\n");
        return L2VPN_FAILURE;
    }
    if (bIsPsnBkpPath)
    {
        u4InL3Intf = pPwVcEntry->u4MPInIfIndex;
    }
    else
    {
        u4InL3Intf = pPwVcEntry->u4InIfIndex;
    }

    if (MplsGetL3Intf (u4InL3Intf, &u4InL3Intf) == MPLS_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "ILM ADD (MPLS Tnl if) for "
                    "POP_SEARCH INTMD EXIT: MplsGetL3Intf Failed for %d\n",
                    u4InL3Intf);
        return L2VPN_FAILURE;
    }

    if (CfaGetVlanId ((UINT2) u4InL3Intf, &u2InVlanId) != CFA_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "ILM ADD (MPLS Tnl if) for "
                   "POP_SEARCH INTMD EXIT: CfaGetVlanId Failed\n");
        return L2VPN_FAILURE;
    }

    MplsHwIlmInfo.u4InVlanId = u2InVlanId;

    /* Get the IVR L3 Interface for the Tnl IF */
    MplsHwIlmInfo.u4InL3Intf = u4InL3Intf;
    if (u4InL3Intf != 0)
    {
        if (MplsGetPhyPortFromLogicalIfIndex
            (MplsHwIlmInfo.u4InL3Intf, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4SrcPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", u4InL3Intf);
        }

#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
        /* Fetch the Qos Information for teh incoming port */
        QosGetMplsExpProfileForPortAtIngress (MplsHwIlmInfo.u4SrcPort,
                                              &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
        QosGetMplsExpProfileForPortAtEgress (MplsHwIlmInfo.u4OutPort,
                                             &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwPriMapId = i4HwExpMapId;
#endif
#endif
    }
    if (bIsPsnBkpPath)
    {
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwAddForPopSearch "
                    " Backup path RSVP Label : %d "
                    " LDP Label : %d "
                    " Incoming L3 interface : %d "
                    " Incoming Vlan ID : %d\n",
                    MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                    MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel,
                    MplsHwIlmInfo.u4InL3Intf, MplsHwIlmInfo.u4InVlanId);
    }
    else
    {
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwAddForPopSearch "
                    " RSVP Label : %d "
                    " LDP Label : %d "
                    " Incoming L3 interface : %d "
                    " Incoming Vlan ID : %d\n",
                    MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                    MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel,
                    MplsHwIlmInfo.u4InL3Intf, MplsHwIlmInfo.u4InVlanId);
    }
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        /* Delete the existing ILM entry with POP_L3_SWITCH label 
         * action in the hardware, then program the same ILM entry 
         * with POP_SEARCH action. */
        if (MplsILMHwDel (pInSegment, pSlotInfo) == MPLS_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                        "MplsILMHwDel: "
                        "Pop_L3_Switch label action for "
                        "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        GenU4Addr.Addr.Ip6Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "MplsILMHwDel: "
                        "Pop_L3_Switch label action for "
                        "PW %d for %#x failed in HW\n",
                        pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
        }
        u4Action = MPLS_HW_ACTION_POP_SEARCH;
        pInSegment->u1LblAction = MPLS_HW_ACTION_POP_SEARCH;
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_SEARCH;
#ifdef NPAPI_WANTED
        if (MplsFsMplsMbsmHwCreateILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwCreateILM: "
                        "PopAndSearch for"
                        "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        GenU4Addr.Addr.u4Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwCreateILM: "
                        "PopAndSearch for"
                        "PW %d for %#x failed in HW\n",
                        pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
            return L2VPN_FAILURE;
        }
#endif
    }
    else
#endif
    {
        /* Configure ILM in H/W */
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            /* Delete the existing ILM entry with POP_L3_SWITCH label 
             * action in the hardware, then program the same ILM entry 
             * with POP_SEARCH action. */
            if (MplsILMHwDel (pInSegment, NULL) == MPLS_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "MplsILMHwDel: "
                            "Pop_L3_Switch label action for "
                            "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "MplsILMHwDel: "
                            "Pop_L3_Switch label action for "
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
            }

            u4Action = MPLS_HW_ACTION_POP_SEARCH;
            pInSegment->u1LblAction = MPLS_HW_ACTION_POP_SEARCH;
            MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_SEARCH;
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwCreateILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwCreateILM: "
                            "PopAndSearch for"
                            "PW %d for Peer IPv4 -%#x IPv6 - %s failed in HW\n",
                            pPwVcEntry->u4PwVcIndex,
                            GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwCreateILM: "
                            "PopAndSearch for"
                            "PW %d for %#x failed in HW\n",
                            pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
                return L2VPN_FAILURE;
            }
#endif
            pPwVcEntry->b1IsIlmEntryPresent = TRUE;

        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwAddForPopSearch EXIT \n");

    return L2VPN_SUCCESS;
}

/******************************************************************************
 *  Function Name   : L2VpnMplsILMHwDelForPopSearch
 *  Description     : This function deletes POP_SEARCH ILM entry and adds
 *                    the ILM entry with POP_L3_SWITCH label action
 *  Input           : u4TunnelLabel    - Tunnel Label
 *                    u4LdpLabel       - LDP Label
 *                    u4IfIndex        - L3 Interface Index.
 *                    bIsPwVcOperLlDown - To indicate the status of 
 *                                        lower layer - useful to decide 
 *                                        whether ILM entry with POP_L3_SEARCH
 *                                        created or not.
 *                    pSlotInfo        - Slot Information.
 *                                        
 *  Output          : None
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT4
L2VpnMplsILMHwDelForPopSearch (UINT4 u4TunnelLabel, UINT4 u4LdpLabel,
                               UINT4 u4IfIndex, BOOL1 bIsPwVcOperLlDown,
                               VOID *pSlotInfo, INT4 *i4IsIlmDeleted)
{
    tMplsHwIlmInfo      MplsHwIlmInfo;
    UINT4               u4Action;
    UINT2               u2InVlanId = 0;
    tInSegment         *pInSegment = NULL;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
               "L2VpnMplsILMHwDelForPopSearch ENTRY \n");
#ifndef NPAPI_WANTED
    UNUSED_PARAM (bIsPwVcOperLlDown);
    UNUSED_PARAM (i4IsIlmDeleted);
#endif

    u4Action = L2VPN_ZERO;
    if (CfaGetVlanId ((UINT2) u4IfIndex, &u2InVlanId) != CFA_SUCCESS)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "L2VpnMplsILMHwDelForPopSearch: CfaGetVlanId Failed for"
                    " Interface %d\n", u4IfIndex);
        return L2VPN_FAILURE;
    }

    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    /* First Label: Tunnel Label */
    MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4TunnelLabel;
    MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    /* Second Label: LDP Label */
    MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4LdpLabel;
    MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

    MplsHwIlmInfo.u4InL3Intf = u4IfIndex;

    MplsHwIlmInfo.u4InVlanId = (UINT4) u2InVlanId;

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwDelForPopSearch "
                " RSVP Label : %d "
                " LDP Label : %d "
                " Incoming L3 interface : %d "
                " Incoming Vlan ID : %d\n",
                MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel,
                MplsHwIlmInfo.u4InL3Intf, MplsHwIlmInfo.u4InVlanId);

    MPLS_CMN_LOCK ();
    pInSegment = MplsFwdGetTwoInSegmentLabelEntry (u4TunnelLabel, u4LdpLabel);
    if (pInSegment != NULL)
    {
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwDelForPopSearch "
                    " Table Index : %d "
                    " Interface Index  : %d "
                    " Incomming Label : %d\n",
                    pInSegment->u4Index,
                    pInSegment->u4IfIndex, pInSegment->u4Label);
    }
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (MplsFsMplsMbsmHwDeleteILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_FNC_CRT_FLAG,
                       "FsMplsMbsmHwDeleteILM failed for PopSearch in HW\n");
        }
        if ((!bIsPwVcOperLlDown) && (pInSegment != NULL))
        {
            pInSegment->u1LblAction = MPLS_HW_ACTION_POP_L3_SWITCH;
            if (MplsILMHwAdd (pInSegment, NULL, pSlotInfo) == MPLS_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_FNC_CRT_FLAG,
                           "MplsILMHwAdd PopL3Switch add failed in HW\n");
            }
        }
    }
    else
#endif
    {
        /* Configure ILM in H/W */
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
                *i4IsIlmDeleted = L2VPN_FALSE;
                L2VPN_DBG (L2VPN_DBG_FNC_CRT_FLAG,
                           "FsMplsHwDeleteILM failed for PopSearch in HW\n");
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "LlDownStatus: %x, ILM Hw Add with POP_L3_SWITCH "
                        "to be called\n", bIsPwVcOperLlDown);

            if (pInSegment != NULL)
            {
                pInSegment->u1LblAction = MPLS_HW_ACTION_POP_L3_SWITCH;
                if (MplsILMHwAdd (pInSegment, NULL, NULL) == MPLS_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_FNC_CRT_FLAG,
                               "MplsILMHwAdd failed for PopL3Switch in HW\n");
                }
                else
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "ILM Hw Add with POP_L3_SWITCH success\n");
                }
            }
#endif
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnMplsILMHwDelForPopSearch EXIT \n");

    MPLS_CMN_UNLOCK ();
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnDeletePsnTnlIf                                       */
/* Description   : Function to delete the PSN tnl interface                  */
/* Input(s)      : u4PwL3Intf - L2VPN PSN tnl interface                      */
/*               : pu1NextHopMac - pointer to the PSN tnl Next Hop Mac       */
/*                 u4PsnDelAct - Action to be performed with this tnl IF     */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDeletePsnTnlIf (UINT4 u4PwL3Intf, UINT1 *pu1NextHopMac, UINT4 u4PsnDelAct)
{

    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;
    UINT4               u4L3Intf = L2VPN_ZERO;
#ifdef NPAPI_WANTED
    UINT4               u4VpnId = 0;
#endif

    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));

    if ((u4PsnDelAct == L2VPN_PSN_DEL_L3FTN) ||
        (u4PsnDelAct == L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF))
    {

        MplsHwL3FTNInfo.u4EgrL3Intf = u4PwL3Intf;
        MEMCPY (MplsHwL3FTNInfo.au1DstMac, pu1NextHopMac, MAC_ADDR_LEN);

        /* Fetch the L3 interface from the MPLS tunnel interface. */
        if (MplsGetL3Intf (MplsHwL3FTNInfo.u4EgrL3Intf,
                           &u4L3Intf) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "MplsTnlNPAdd : "
                        "Fetching L3 interface failed for tunnel "
                        "interface %d\t\n", MplsHwL3FTNInfo.u4EgrL3Intf);
        }

        /* The VLAN Id will get updated with valid value only if the 
         * L3 interface is IVR interface. */
        if (CfaGetVlanId (u4L3Intf,
                          &(MplsHwL3FTNInfo.u2OutVlanId)) != CFA_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "MplsTnlNPAdd : "
                        "CfaGetVlanId Failed for L3 interface %d\r\n",
                        u4L3Intf);
        }

        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4L3Intf, (UINT1 *) (&(MplsHwL3FTNInfo.au1DstMac)),
             &(MplsHwL3FTNInfo.u4OutPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "Fetching Physical port "
                        "for the L3 interface : %d failed.\r\n", u4L3Intf);
        }

        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "\rMPLS L3 FTN %d is deleted by L2VPN\n", u4PwL3Intf);
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            if (MplsFsMplsWpHwDeleteL3FTN (u4VpnId, &MplsHwL3FTNInfo) ==
                FNP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "FsMplsWpHwDeleteL3FTN delete failed "
                            "for vpnid %d  \r\n", u4VpnId);
                return L2VPN_FAILURE;
            }
#endif
        }
    }

    if ((u4PsnDelAct == L2VPN_PSN_DEL_MPLS_TNL_IF) ||
        (u4PsnDelAct == L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF))
    {
        /* NON-TE case, we have to delete the Outbound tunnel interface */
        if (CfaUtilGetIfIndexFromMplsTnlIf (u4PwL3Intf, &u4L3Intf,
                                            TRUE) != CFA_FAILURE)
        {
            if (CfaIfmDeleteStackMplsTunnelInterface
                (u4L3Intf, u4PwL3Intf) == CFA_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "MPLS Tunnel IF %d deletion"
                            " failed in L2VPN\n", u4PwL3Intf);
                return L2VPN_FAILURE;
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "MPLS Tunnel IF %d is deleted by "
                        "L2VPN\n", u4PwL3Intf);
        }
        else
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "PW NON-TE outbound tunnel "
                       "interface deletion failed, "
                       "L3 interface get from MPLS tnl if failed \n");
            return L2VPN_FAILURE;
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnFmQueryPwVcStats                                     */
/* Description   : Function to query statistics information from FM for a    */
/*                 PwVc Entry                                                */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnFmQueryPwVcStats (tPwVcEntry * pPwVcEntry)
{
    tPwVcPerfIntervalEntry CurrEntry;
    tPwVcPerfTotalEntry *pPwVcPerfTotalEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
    FS_UINT8            u8Tmp;
    INT1                i1RetStatus = 0;
    tSNMP_COUNTER64_TYPE Counter64Val;

    MEMSET (&CurrEntry, 0, sizeof (tPwVcPerfIntervalEntry));
    MEMSET (&Counter64Val, 0, sizeof (tSNMP_COUNTER64_TYPE));

    pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));

    if (nmhGetPwPerfCurrentInHCPackets (L2VPN_PWVC_INDEX (pPwVcEntry),
                                        &Counter64Val) == SNMP_FAILURE)
    {
        i1RetStatus++;
    }

    CurrEntry.InHCPkts.u4Hi = Counter64Val.msn;
    CurrEntry.InHCPkts.u4Lo = Counter64Val.lsn;

    if (nmhGetPwPerfCurrentInHCBytes (L2VPN_PWVC_INDEX (pPwVcEntry),
                                      &Counter64Val) == SNMP_FAILURE)
    {
        i1RetStatus++;
    }

    CurrEntry.InHCBytes.u4Hi = Counter64Val.msn;
    CurrEntry.InHCBytes.u4Lo = Counter64Val.lsn;

    if (nmhGetPwPerfCurrentOutHCPackets (L2VPN_PWVC_INDEX (pPwVcEntry),
                                         &Counter64Val) == SNMP_FAILURE)
    {
        i1RetStatus++;
    }
    CurrEntry.OutHCPkts.u4Hi = Counter64Val.msn;
    CurrEntry.OutHCPkts.u4Lo = Counter64Val.lsn;

    if (nmhGetPwPerfCurrentOutHCBytes (L2VPN_PWVC_INDEX (pPwVcEntry),
                                       &Counter64Val) == SNMP_FAILURE)
    {
        i1RetStatus++;
    }
    CurrEntry.OutHCBytes.u4Hi = Counter64Val.msn;
    CurrEntry.OutHCBytes.u4Lo = Counter64Val.lsn;

    if (nmhGetPwEnetStatsIllegalVlan
        (L2VPN_PWVC_INDEX (pPwVcEntry), &u8Tmp.u4Lo) == SNMP_FAILURE)
    {
        i1RetStatus++;
    }
    if (nmhGetPwEnetStatsIllegalLength
        (L2VPN_PWVC_INDEX (pPwVcEntry), &u8Tmp.u4Lo) == SNMP_FAILURE)
    {
        i1RetStatus++;
    }

    if (i1RetStatus > 0)
    {
        return L2VPN_FAILURE;
    }

    /* If HW supports elapsed time also then get it from the HW itself */
    L2VPN_PWVC_PERF_INT_TIME_ELAPSED (pPwVcPerfInfo,
                                      L2VPN_PWVC_PERF_CURRENT_INTERVAL) =
        L2VPN_PWVC_PERF_INTV_IN_MTS * 60;

    pPwVcPerfTotalEntry = &(pPwVcPerfInfo->aPwVcPerf1DayIntervalInfo
                            [pPwVcPerfInfo->u1PwVcPerfDayIndex - 1]);

    FSAP_U8_ADD (&u8Tmp, &pPwVcPerfTotalEntry->InHCPkts, &CurrEntry.InHCPkts);
    MEMCPY (&pPwVcPerfTotalEntry->InHCPkts, &u8Tmp, sizeof (FS_UINT8));
    FSAP_U8_ADD (&u8Tmp, &pPwVcPerfTotalEntry->InHCByts, &CurrEntry.InHCBytes);
    MEMCPY (&pPwVcPerfTotalEntry->InHCByts, &u8Tmp, sizeof (FS_UINT8));
    FSAP_U8_ADD (&u8Tmp, &pPwVcPerfTotalEntry->OutHCPkts, &CurrEntry.OutHCPkts);
    MEMCPY (&pPwVcPerfTotalEntry->OutHCPkts, &u8Tmp, sizeof (FS_UINT8));
    FSAP_U8_ADD (&u8Tmp, &pPwVcPerfTotalEntry->OutHCByts,
                 &CurrEntry.OutHCBytes);
    MEMCPY (&pPwVcPerfTotalEntry->OutHCByts, &u8Tmp, sizeof (FS_UINT8));
    pPwVcPerfTotalEntry->u4MoniTime = (pPwVcPerfTotalEntry->u4MoniTime +
                                       (UINT4)
                                       L2VPN_PWVC_PERF_INT_TIME_ELAPSED
                                       (pPwVcPerfInfo,
                                        L2VPN_PWVC_PERF_CURRENT_INTERVAL));

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnFmQueryAllPwVcStats                                  */
/* Description   : Function to query statistics information from FM for all  */
/*                 PwVc Entries                                              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnFmQueryAllPwVcStats (VOID)
{
    INT1                i1NextInterval = L2VPN_ZERO;
    UINT4               u4PwVcIndex;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (L2VPN_INITIALISED != TRUE)
    {
        return;
    }

    /* Restart the timer before collecting the stats itself so that, there
     * wont be any any loss in counting the time. Otherwise over a period
     * of time we will loose the sync with system clock (lagging) since we
     * have taken rest while collecting the stats OOPS!!! i got it :)
     */
    gpL2VpnGlobalInfo->PerfTimer.u4Event = L2VPN_PWVC_PERF_TMR_EXP_EVENT;
    if (TmrStartTimer (L2VPN_TIMER_LIST_ID,
                       &gpL2VpnGlobalInfo->PerfTimer.AppTimer,
                       L2VPN_PWVC_PERF_TIMER_INTV) != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Restart of 15-minute perf timer failed\r\n");
    }

    if (L2VPN_PWVC_PERF_CURRENT_INTERVAL != L2VPN_PWVC_MAX_PERF_INTV)
    {
        i1NextInterval = (INT1) (L2VPN_PWVC_PERF_CURRENT_INTERVAL + 1);
    }
    /* Foreach operationally active PW collect the statistics and reset
     * next day and/or interval stats to zero
     */
    for (u4PwVcIndex = 1;
         u4PwVcIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo);
         u4PwVcIndex++)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwVcIndex);
        pPwVcEntry = pPwVcPerfInfo->pPwVcEntry;
        if (pPwVcEntry == NULL)
        {
            /* No Pseudowire is associated with this */
            continue;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) != L2VPN_PWVC_ACTIVE)
        {
            /* Pseudowire entry is not created fully */
            continue;
        }

        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            /* Query stats from FM. Data will be set invalid if failed */
            if (L2VpnFmQueryPwVcStats (pPwVcEntry) == L2VPN_SUCCESS)
            {
                L2VPN_PWVC_PERF_INT_VALID_DATA (pPwVcPerfInfo,
                                                L2VPN_PWVC_PERF_CURRENT_INTERVAL)
                    = L2VPN_SNMP_TRUE;
                L2VPN_PWVC_VALID_INTERVALS (pPwVcEntry)++;
                L2VPN_PWVC_PERF_1DAY_VALID_DATA (pPwVcPerfInfo,
                                                 pPwVcPerfInfo->
                                                 u1PwVcPerfDayIndex) =
                    L2VPN_SNMP_TRUE;
            }
        }

        if (L2VPN_PWVC_PERF_CURRENT_INTERVAL == L2VPN_PWVC_MAX_PERF_INTV)
        {
            L2VPN_PWVC_VALID_INTERVALS (pPwVcEntry) = 0;
            pPwVcPerfInfo->u1PwVcPerfDayIndex++;
            if (pPwVcPerfInfo->u1PwVcPerfDayIndex > L2VPN_PWVC_MAX_PERF_DAYS)
            {
                pPwVcPerfInfo->u1PwVcPerfDayIndex = L2VPN_PWVC_MIN_PERF_INTV;
            }
            /* Make the newbie (day) ready */
            MEMSET (&pPwVcPerfInfo->aPwVcPerf1DayIntervalInfo
                    [pPwVcPerfInfo->u1PwVcPerfDayIndex - 1], 0,
                    sizeof (tPwVcPerfTotalEntry));
            L2VPN_PWVC_PERF_1DAY_VALID_DATA (pPwVcPerfInfo,
                                             pPwVcPerfInfo->u1PwVcPerfDayIndex)
                = L2VPN_SNMP_FALSE;
            i1NextInterval = L2VPN_PWVC_MIN_PERF_INTV;
        }
        /* Reset the next interval for the current PWVC */
        MEMSET (&pPwVcPerfInfo->aPwVcPerfIntervalInfo[i1NextInterval - 1], 0,
                sizeof (tPwVcPerfIntervalEntry));
        L2VPN_PWVC_PERF_INT_VALID_DATA (pPwVcPerfInfo, i1NextInterval) =
            L2VPN_SNMP_FALSE;
    }
    L2VPN_PWVC_PERF_CURRENT_INTERVAL = i1NextInterval;

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckPortOperStatus                                  */
/* Description   : Function to query operational status of the interface     */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnCheckPortOperStatus (INT4 i4PortIfIndex)
{
    tCfaIfInfo          IfInfo;

    if (L2VPN_ZERO == i4PortIfIndex)
    {
        return L2VPN_SUCCESS;
    }

    if (CfaGetIfInfo ((UINT4) i4PortIfIndex, &IfInfo) == CFA_SUCCESS)
    {
        if (IfInfo.u1IfOperStatus == CFA_IF_UP)
        {
            return L2VPN_SUCCESS;
        }
    }
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPerfCurrentInterval                               */
/* Description   : This routine returns the interval number in a day         */
/*                 for the given period of intervals. Out vars are optional  */
/* Input(s)      : u1Period - time period information in seconds to divide   */
/*                 the current day into slots. For Example consider 15-minute*/
/*                 based interval i.e u1Period = 15 * 60 and current system  */
/*                 time is 18:36:18, o/p slot = 75, remaining time = 522     */
/*                 elapsed time = 378                                        */
/* Output(s)     : slot of the day for the given period                      */
/*                 elapsed time of the current interval                      */
/*                 remaining time in the current interval                    */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnGetPerfCurrentInterval (UINT4 u4Period, INT1 *pi1CurInterval,
                             UINT4 *pu4RemTime, UINT4 *pu4TimeElapsed)
{
    time_t              ti;
    struct tm          *tp;

    time (&ti);
    tp = localtime (&ti);

    /*    tp->tm_year += 1900;
       tp->tm_mon++;
       tp->tm_wday++; */
    if (tp == NULL)
    {
        return L2VPN_FAILURE;
    }

    if (pi1CurInterval != NULL)
    {
        *pi1CurInterval = (INT1)
            (((tp->tm_hour * 60 * 60) + (tp->tm_min * 60) + tp->tm_sec -
              1) / u4Period + 1);
    }
    if (pu4RemTime != NULL)
    {
        *pu4RemTime =
            u4Period - ((tp->tm_hour * 60 * 60) + (tp->tm_min * 60) +
                        tp->tm_sec - 1) % u4Period;
    }
    if (pu4TimeElapsed != NULL)
    {
        *pu4TimeElapsed =
            ((tp->tm_hour * 60 * 60) + (tp->tm_min * 60) + tp->tm_sec -
             1) % u4Period;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcIlmHwDelForNonTePsn                              */
/* Description   : Deletes the ILM entry in the hw for NON-TE PSN            */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pMplsHwIlmInfo - HW ILM information                       */
/*                 bIsPwVcOperLlDown                                         */
/*                    - TRUE -> Lower layer tunnel is down                   */
/*                    - FALSE -> Lower layer tunnel is not down              */
/*                 pSlotInfo -> Slot information                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcIlmHwDelForNonTePsn (tPwVcEntry * pPwVcEntry,
                              tMplsHwIlmInfo * pMplsHwIlmInfo,
                              BOOL1 bIsPwVcOperLlDown, VOID *pSlotInfo)
{
    UINT4               u4InLabel = L2VPN_INVALID_LABEL;
    INT4                i4IsIlmDeleted = L2VPN_TRUE;
    pPwVcEntry->b1IsIlmEntryPresent = L2VPN_TRUE;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcIlmHwDelForNonTePsn: ENTRY\n");

    /* Fill the Pw ILM incoming L3 interface */
    pMplsHwIlmInfo->u4InL3Intf = pPwVcEntry->u4InIfIndex;

    if (pPwVcEntry->i4NPop == MPLS_TWO)
    {
        /* If i4NPop is 2, Deletion for L2VPNoLDPoRSVP needs to be
         * done. */

        /* We have retrieved Outer (Tunnel Label) and Inner (LDP Label)
         * Labels. Now, we need to delete (POP_SEARCH)
         * operation for ILM for L2VPN on LDP over RSVP Scenario. */
        if (L2VpnMplsILMHwDelForPopSearch (pPwVcEntry->u4InOuterLabel,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           pPwVcEntry->u4InIfIndex,
                                           bIsPwVcOperLlDown,
                                           pSlotInfo,
                                           &i4IsIlmDeleted) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "ILM Del POP_Search for L2VPNoLDPoRSVP Non-Te "
                       "failed\n");
            return L2VPN_FAILURE;
        }
        if (i4IsIlmDeleted == L2VPN_TRUE)
        {
            pPwVcEntry->b1IsIlmEntryPresent = L2VPN_FALSE;
        }

        if (pPwVcEntry->u4InOuterMPLabel1 != L2VPN_LDP_INVALID_LABEL)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "In outer MP tnl label1=%d LDP label=%d\n",
                        pPwVcEntry->u4InOuterMPLabel1,
                        pPwVcEntry->u4InOuterLdpLabel);

            /* FRR PWVC incoming outer merge point label exists */
            if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                           pPwVcEntry->u4InOuterMPLabel1,
                                           L2VPN_LDP_INVALID_LABEL,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           bIsPwVcOperLlDown) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "POP Search for L2VPNoLDPoRSVP using "
                           " NON-TE deletion failed for MP label1\n");
            }
        }
        if (pPwVcEntry->u4InOuterMPLabel2 != L2VPN_LDP_INVALID_LABEL)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "In outer MP tnl label2=%d LDP label=%d \n",
                        pPwVcEntry->u4InOuterMPLabel2,
                        pPwVcEntry->u4InOuterLdpLabel);

            /* FRR PWVC incoming outer merge point label exists */
            if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                           pPwVcEntry->u4InOuterMPLabel2,
                                           L2VPN_LDP_INVALID_LABEL,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           bIsPwVcOperLlDown) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "POP Search for L2VPNoLDPoRSVP using "
                           " NON-TE deletion failed for MP label2\n");
            }
        }
        /* Reset the incoming outer labels */
        pPwVcEntry->u4InOuterLabel = L2VPN_LDP_INVALID_LABEL;
        pPwVcEntry->u4InOuterLdpLabel = L2VPN_LDP_INVALID_LABEL;
        pPwVcEntry->i4NPop = L2VPN_ZERO;
        u4InLabel = L2VpnPassValidInLabelToHw (pPwVcEntry);
        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel = u4InLabel;
        pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;

        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "VC label: %d\n", u4InLabel);
    }
    else if (pPwVcEntry->u4InOuterLabel != L2VPN_LDP_INVALID_LABEL)
    {
        /* NPop is not 2, But InOuterLabel is present. It may be 
         *    1. Outer RSVP Tunnel Label (L2VPNoLDPoRSVP Implicit Null) 
         *    2. Outer LDP LSP Label (L2VPNoLDP Normal)
         *
         * So treat this as Normal L2VPNoLDP Non-te Case. */

        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel =
            pPwVcEntry->u4InOuterLabel;
        pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        u4InLabel = L2VpnPassValidInLabelToHw (pPwVcEntry);
        pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel = u4InLabel;
        pMplsHwIlmInfo->InLabelList[1].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnPwVcIlmHwDelForNonTePsn: "
                    "Incoming 1.Outer tnl label=%d"
                    "2.VC label=%d\n", pPwVcEntry->u4InOuterLabel, u4InLabel);
        if (pPwVcEntry->u4InOuterMPLabel1 != L2VPN_LDP_INVALID_LABEL)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "In outer MP tnl label1=%d LDP label=%d\n",
                        pPwVcEntry->u4InOuterMPLabel1,
                        pPwVcEntry->u4InOuterLdpLabel);

            /* FRR PWVC incoming outer merge point label exists */
            if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                           pPwVcEntry->u4InOuterMPLabel1,
                                           L2VPN_LDP_INVALID_LABEL,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           bIsPwVcOperLlDown) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "POP Search for L2VPNoLDPoRSVP using "
                           " NON-TE deletion failed for MP label1\n");
            }
        }

        if (pPwVcEntry->u4InOuterMPLabel2 != L2VPN_LDP_INVALID_LABEL)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "In outer MP tnl label2=%d LDP label=%d \n",
                        pPwVcEntry->u4InOuterMPLabel2,
                        pPwVcEntry->u4InOuterLdpLabel);

            /* FRR PWVC incoming outer merge point label exists */
            if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                           pPwVcEntry->u4InOuterMPLabel2,
                                           L2VPN_LDP_INVALID_LABEL,
                                           pPwVcEntry->u4InOuterLdpLabel,
                                           bIsPwVcOperLlDown) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "POP Search for L2VPNoLDPoRSVP using "
                           " NON-TE deletion failed for MP label2\n");
            }
        }
        pPwVcEntry->u4InOuterLabel = L2VPN_LDP_INVALID_LABEL;
    }
    else
    {
        /* NPop is not 2, OuterLabel is also ZERO.
         * It is L2VPNoLDP Non-Te Implicit Null -> treating it as vconly */
        u4InLabel = L2VpnPassValidInLabelToHw (pPwVcEntry);
        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel = u4InLabel;
        pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;

        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnPwVcIlmHwDelForNonTePsn: "
                    "Incoming PHP implicit Null 1.VC label=%d\n", u4InLabel);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcIlmHwDelForNonTePsn: EXIT\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcIlmHwAddForNonTePsn                              */
/* Description   : Creates the ILM entry in the hw for NON-TE PSN            */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pMplsHwIlmInfo - HW ILM information                       */
/*                 pSlotInfo -> Slot information                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcIlmHwAddForNonTePsn (tPwVcEntry * pPwVcEntry,
                              tMplsHwIlmInfo * pMplsHwIlmInfo, VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegIndex = NULL;
    tL2vpnLabelArgs     L2vpnLabelArgs;
    UINT4               u4LsrXcIndex = L2VPN_ZERO;
    UINT4               u4InIntf = L2VPN_ZERO;
    UINT4               u4TnlLabel = L2VPN_ZERO;
    BOOL1               bIsManualNonTePwVc = FALSE;

    MEMSET (&L2vpnLabelArgs, L2VPN_ZERO, sizeof (tL2vpnLabelArgs));
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcIlmHwAddForNonTePsn: ENTRY\n");

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &L2vpnLabelArgs.PeerAddr.Ip6Addr.u1_addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &L2vpnLabelArgs.PeerAddr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        L2vpnLabelArgs.PeerAddr.u4Addr =
            OSIX_NTOHL (L2vpnLabelArgs.PeerAddr.u4Addr);
    }

    L2vpnLabelArgs.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    L2vpnLabelArgs.u4EntityId =
        pMplsOutTnlEntry->unTnlInfo.NonTeInfo.u4EntityId;

    /* Check for the outer tnl XC index existance */
    if (L2VPN_PWVC_MPLS_TNL_XC_INDEX (pMplsOutTnlEntry) == L2VPN_ZERO)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcIlmHwAddForNonTePsn: "
                   "Non-te Outer Tnl XC is zero");
        return L2VPN_FAILURE;
    }

    /* Check if the PW owner is manual */
    if ((pPwVcEntry->i1PwVcOwner == L2VPN_PWVC_OWNER_MANUAL) ||
        (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE))
    {
        bIsManualNonTePwVc = TRUE;
    }

#ifdef MPLS_SIG_WANTED
    if (0 != pPwVcEntry->GenNonTeTnlSrcAddr.u4Addr)
    {
        if (LdpGetFecInLabelForDestAddr (&L2vpnLabelArgs, bIsManualNonTePwVc) ==
            LDP_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2VpnPwVcIlmHwAddForNonTePsn: "
                       "LdpGetFecInLabelForDestAddr failed\n");
            return L2VPN_FAILURE;
        }
    }
#endif

    /* 4 Cases are possible here.
     *
     * 1. Normal L2VPNoLDPoRSVP.
     * 2. Implicit-Null L2VPNoLDPoRSVP
     * 3. Normal L2VPNoLDP Non-te.
     * 4. Implicit-Null L2VPNoLDP Non-te. */

    if (L2vpnLabelArgs.u4InLabel != MPLS_IMPLICIT_NULL_LABEL &&
        L2vpnLabelArgs.u4InLabel != 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnPwVcIlmHwAddForNonTePsn: "
                   "Incoming Outer Tnl Lbl is not Implicit-Null\n");

        MPLS_CMN_LOCK ();
        if (L2vpnLabelArgs.bIsLspOnTgtSsn)
        {
            /* We have obtained LDP (Targeted Session) Label. Need to get
             * the corresponding Insegment Entry to get Tunnel (Outer)
             * Label. */
            pXcEntry = MplsGetXCEntryByDirection (L2vpnLabelArgs.u4TnlXcIndex,
                                                  MPLS_DEF_DIRECTION);

            if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "L2VpnPwVcIlmHwAddForNonTePsn: "
                           "MplsGetXCEntryByDirection failed\n");
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
            u4TnlLabel = pXcEntry->pInIndex->u4Label;
            pInSegIndex =
                MplsFwdGetTwoInSegmentLabelEntry (u4TnlLabel,
                                                  L2vpnLabelArgs.u4InLabel);
            if (pInSegIndex == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "L2VpnPwVcIlmHwAddForNonTePsn "
                            "MplsGetInSegFromBottomLbl "
                            "for label =%d failed\n", L2vpnLabelArgs.u4InLabel);
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
            /* Check for the incoming merge point existance */
            if (pInSegIndex->u4InMPXcIndex1 != L2VPN_ZERO)
            {
                /* Program the ILM with POP_SEARCH label action */
                if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                               pInSegIndex->u4InMPXcIndex1,
                                               pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "ILM Add for merge point1 is failed \n");
                }

            }
            /* Check for the incoming merge point existance */
            if (pInSegIndex->u4InMPXcIndex2 != L2VPN_ZERO)
            {
                /* Program the ILM with POP_SEARCH label action */
                if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                               pInSegIndex->u4InMPXcIndex2,
                                               pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "ILM Add for merge point1 is failed \n");
                }
            }
            pXcEntry = pInSegIndex->pXcIndex;
        }
        else
        {
            /* Normal L2VPNoLDP Non-te Case handling. */

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2VpnPwVcIlmHwAddForNonTePsn: "
                       "Incoming Outer LSP is over Link ssn\n");

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcIlmHwAddForNonTePsn: "
                        "L2vpnLabelArgs.u4InIfIndex = %u, L2vpnLabelArgs.u4InLabel = %u\n",
                        L2vpnLabelArgs.u4InIfIndex, L2vpnLabelArgs.u4InLabel);

            /* We have obtained LDP (Link Session) label. Need to get
             * the corresponding Insegment Entry. */
            if (MplsGetXcIndexFromInIfIndexAndInLabel
                (L2vpnLabelArgs.u4InIfIndex, L2vpnLabelArgs.u4InLabel,
                 &u4LsrXcIndex) != MPLS_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                            "MplsGetXcIndexFromInIfIndexAndInLabel"
                            "failed for Label %d InIfIndex %d\n",
                            L2vpnLabelArgs.u4InLabel,
                            L2vpnLabelArgs.u4InIfIndex);
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
#if 0
            printf
                ("vishalPw : %s : %d u4InIfIndex=%d u4InLabel=%d u4LsrXcIndex=%d\n",
                 __FUNCTION__, __LINE__, L2vpnLabelArgs.u4InIfIndex,
                 L2vpnLabelArgs.u4InLabel, u4LsrXcIndex);
#endif

            pXcEntry = MplsGetXCEntryByDirection (u4LsrXcIndex,
                                                  MPLS_DEF_DIRECTION);
            if (pXcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "L2VpnPwVcIlmHwAddForNonTePsn: "
                            "XC is NULL for Index %d\n", u4LsrXcIndex);
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
        }

        if (pXcEntry->pInIndex == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcIlmHwAddForNonTePsn: "
                       "InSegment index is NULL\n");
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }

        pInSegIndex = pXcEntry->pInIndex;

        u4InIntf = pInSegIndex->u4IfIndex;
        pPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex = pXcEntry->u4Index;
        pPwVcEntry->i4NPop = pInSegIndex->i4NPop;
        pPwVcEntry->u4InOuterLabel = pInSegIndex->u4Label;

        if (MplsGetL3Intf (u4InIntf, &(pPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            MPLS_CMN_UNLOCK ();
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "L2VpnPwVcIlmHwAddForNonTePsn: "
                        "Interface %d do not exist\n", u4InIntf);
            return L2VPN_FAILURE;
        }

        /* We have retrieved Outer (Tunnel Label) and Inner (LDP Label)
         * Labels. Now, we need to create (POP_SEARCH)
         * operation for ILM for L2VPN on LDP over RSVP Scenario. */
        if (pPwVcEntry->i4NPop == MPLS_TWO)
        {
            /* Normal L2VPNoLDPoRSVP Case handling. */

            if (L2VpnMplsILMHwAddForPopSearch (pInSegIndex, FALSE,
                                               pPwVcEntry, pSlotInfo)
                == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "L2VpnPwVcIlmHwAddForNonTePsn: "
                           "POP Search for L2VPNoLDPoRSVP using "
                           "NON-TE creation failed in HW\n");
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }

            pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel =
                pPwVcEntry->u4InVcLabel;
            pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;

            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnPwVcIlmHwAddForNonTePsn: "
                        "ILM Add for POP_SEARCH done "
                        "and POP_L2SWITCH -> In VC Lbl: %d\n",
                        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel);
        }
        else
        {
            /* Normal L2VPNoLDP Non-te Case handling. */

            pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel =
                pPwVcEntry->u4InOuterLabel;
            pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
            pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel =
                pPwVcEntry->u4InVcLabel;
            pMplsHwIlmInfo->InLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnPwVcIlmHwAddForNonTePsn: "
                        "ILM Add POP_L2SWITCH -> Outer Tnl Lbl: %d, "
                        "In VC Lbl: %d\n",
                        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel,
                        pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel);
        }
        MPLS_CMN_UNLOCK ();
    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnPwVcIlmHwAddForNonTePsn: "
                   "Incoming Outer Tnl Lbl is Implicit-Null\n");
        /* Initialize the number of label POP value to zero */
        pPwVcEntry->i4NPop = L2VPN_ZERO;

        if (L2vpnLabelArgs.bIsLspOnTgtSsn)
        {
            /* Implicit-Null L2VPNoLDPoRSVP Non-te Case handling. */
            u4InIntf = L2vpnLabelArgs.u4InIfIndex;

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2VpnPwVcIlmHwAddForNonTePsn: "
                       "Incoming Outer LSP is over Tgt ssn (LDPoRSVP)\n");

            /* Backup path L2VPNoLDPoRSVP Implicit Null programming */
            if (L2vpnLabelArgs.u4BkpTnlXcIndex1 != L2VPN_ZERO)
            {
                if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                               L2vpnLabelArgs.u4BkpTnlXcIndex1,
                                               pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "ILM Add for merge point1 is failed \n");
                }

            }

            /* Backup path L2VPNoLDPoRSVP Implicit Null programming */
            if (L2vpnLabelArgs.u4BkpTnlXcIndex2 != L2VPN_ZERO)
            {
                if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                               L2vpnLabelArgs.u4BkpTnlXcIndex2,
                                               pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "ILM Add for merge point1 is failed \n");
                }
            }

            if (MplsGetIfIndexAndLabelFromXcIndex (L2vpnLabelArgs.u4TnlXcIndex,
                                                   &L2vpnLabelArgs.u4InIfIndex,
                                                   &u4TnlLabel) == MPLS_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "L2VpnPwVcIlmHwAddForNonTePsn: "
                           "MplsGetIfIndexAndLabelFromXcIndex failed\n");
                return L2VPN_FAILURE;
            }

            pPwVcEntry->u4InOuterLabel = u4TnlLabel;

            pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel =
                pPwVcEntry->u4InOuterLabel;
            pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
            pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel =
                pPwVcEntry->u4InVcLabel;
            pMplsHwIlmInfo->InLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnPwVcIlmHwAddForNonTePsn: "
                        "ILM Add POP_L2SWITCH -> Outer Tnl Lbl: %d, "
                        "In VC Lbl: %d\n",
                        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel,
                        pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel);
        }
        else
        {
            /* Implicit-Null L2VPNoLDP Non-te Case handling. */

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2VpnPwVcIlmHwAddForNonTePsn: "
                       "Incoming Outer LSP is over Link ssn\n");

            u4InIntf = L2vpnLabelArgs.u4InIfIndex;

            pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel =
                pPwVcEntry->u4InVcLabel;
            pMplsHwIlmInfo->InLabelList[0].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;

            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnPwVcIlmHwAddForNonTePsn: "
                        "ILM Add POP_L2SWITCH -> In VC Lbl: %d\n",
                        pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel);
        }
    }

    if (u4InIntf != 0)
    {
        if (MplsGetL3Intf (u4InIntf, &(pPwVcEntry->u4InIfIndex)) ==
            MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "L2VpnPwVcIlmHwAddForNonTePsn"
                        "Interface %d do not exist\n", u4InIntf);
            return L2VPN_FAILURE;
        }

        pMplsHwIlmInfo->u4InL3Intf = pPwVcEntry->u4InIfIndex;
    }
    else
    {
        pMplsHwIlmInfo->u4InL3Intf = 0;
        pPwVcEntry->u4InIfIndex = 0;
    }

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcIlmHwAddForNonTePsn: EXIT\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDelForNonTePsn                                 */
/* Description   : Deletes the PWVC for NON-TE PSN                           */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pMplsHwVcInfo - HW VC information                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDelForNonTePsn (tPwVcEntry * pPwVcEntry,
                           tMplsHwVcTnlInfo * pMplsHwVcInfo)
{
    tGenU4Addr          GenU4Addr;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_HTONL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwDelForNonTePsn: called for PW %d Peer IPv4 -%x"
                " IPv6 -%s\n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwDelForNonTePsn: called for PW %d %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    pMplsHwVcInfo->u4PwL3Intf = pPwVcEntry->u4PwL3Intf;
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwDelForNonTePsn: exit for PW %d Peer IPv4 -%x"
                " IPv6 -%s\n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwDelForNonTePsn: exit for PW %d %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwAddForNonTePsn                                 */
/* Description   : Creates the PWVC for NON-TE PSN                           */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pMplsHwVcInfo - HW VC information                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwAddForNonTePsn (tPwVcEntry * pPwVcEntry,
                           tMplsHwVcTnlInfo * pMplsHwVcInfo)
{
    tPwVcMplsTnlEntry  *pMplsOutTnlEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4L3Intf = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_HTONL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddForNonTePsn: called for PW %d %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);

    pMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY
        ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if (L2VPN_PWVC_MPLS_TNL_XC_INDEX (pMplsOutTnlEntry) == L2VPN_ZERO)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Non-te Outer Tnl XC is zero");
        return L2VPN_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pXcEntry = MplsGetXCEntryByDirection (L2VPN_PWVC_MPLS_TNL_XC_INDEX
                                          (pMplsOutTnlEntry),
                                          MPLS_DEF_DIRECTION);

    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL) ||
        (pXcEntry->u1OperStatus != XC_OPER_UP))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Non-te Outer Tnl XC is not present");
        MPLS_CMN_UNLOCK ();
        return L2VPN_FAILURE;
    }

    pPwVcEntry->u4PwL3Intf = pXcEntry->pOutIndex->u4IfIndex;
    pPwVcEntry->u4OutOuterLabel = pXcEntry->pOutIndex->u4Label;
    pMplsHwVcInfo->u4PwL3Intf = pPwVcEntry->u4PwL3Intf;
    pMplsHwVcInfo->OutLspLabel.u.MplsShimLabel = pPwVcEntry->u4OutOuterLabel;
    MEMCPY (pMplsHwVcInfo->au1PwDstMac,
            pXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);

    /* Lock is released here */
    MPLS_CMN_UNLOCK ();

    if (MplsGetL3Intf (pMplsHwVcInfo->u4PwL3Intf, &u4L3Intf) != MPLS_SUCCESS)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "L2VpnPwVcHwAddForNonTePsn : "
                    "Fetching L3 interface failed for tunnel interface %d\t\n",
                    pMplsHwVcInfo->u4PwL3Intf);
    }

    if (CfaGetVlanId (u4L3Intf, &(pMplsHwVcInfo->u2OutVlanId)) != CFA_SUCCESS)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "L2VpnPwVcHwAddForNonTePsn : "
                    "CfaGetVlanId Failed for L3 interface %d\r\n", u4L3Intf);
    }

    /* Get the physical port for the corresponding Logical L3 interface. */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          (UINT1
                                           *) (&(pMplsHwVcInfo->au1PwDstMac)),
                                          &(pMplsHwVcInfo->u4PwOutPort)) !=
        MPLS_SUCCESS)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "L2VpnPwVcHwAddForNonTePsn : "
                    "Fetching Physical port for the L3 interface : %d "
                    "failed.\r\n", u4L3Intf);
    }

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outgoing MPLS Tunnel Intf: %d, L3 Intf: %d Vlan Id: %d Out Port: %d\n",
                pMplsHwVcInfo->u4PwL3Intf, u4L3Intf, pMplsHwVcInfo->u2OutVlanId,
                pMplsHwVcInfo->u4PwOutPort);

    L2VPN_DBG6 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddForNonTePsn: "
                "NON-TE LSP's Next Hop Mac address "
                "%02x:%02x:%02x:%02x:%02x:%02x\n",
                pMplsHwVcInfo->au1PwDstMac[0], pMplsHwVcInfo->au1PwDstMac[1],
                pMplsHwVcInfo->au1PwDstMac[2], pMplsHwVcInfo->au1PwDstMac[3],
                pMplsHwVcInfo->au1PwDstMac[4], pMplsHwVcInfo->au1PwDstMac[5]);

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddForNonTePsn: exit for PW %d Peer IPv4- %x IPv6 -%s\n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnPwVcHwAddForNonTePsn: exit for PW %d %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcVplsHwVpnAcAdd                                   */
/* Description   : Creates the VPN AC from the hw, applicable for VPLS       */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcVplsHwVpnAcAdd (tPwVcEntry * pPwVcEntry,
                         tPwVcEnetEntry * pPwVcEnetEntry)
{
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tVPLSEntry         *pVplsEntry = NULL;
#ifdef NPAPI_WANTED
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tL2VpnPwHwList      L2VpnPwHwListUpdate;
#endif
#endif
#endif
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4VplsInstance = L2VPN_ZERO;
    UINT4               u4PwAcId = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

#ifdef NPAPI_WANTED
    UINT4               u4Action = L2VPN_ZERO;
#endif

    if (pPwVcEnetEntry->u1HwStatus == MPLS_TRUE)
    {
        return L2VPN_SUCCESS;
    }

    /* To check whether AC Port is Operationally up */
    if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex) ==
        L2VPN_FAILURE)
    {
        return L2VPN_SUCCESS;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
    if (pVplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "VPLS entry is NULL\n");
        return L2VPN_FAILURE;
    }

    /* Fill up the VPLS info */
    MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]), sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);
    u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI = i4Vsi;
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VplsInstance =
        L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);

    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan =
        (UINT4) L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort =
        (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i1PwEnetVlanMode =
        L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4PwEnetPwVlan =
        L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
    MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
#ifdef NPAPI_WANTED
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)

    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
    L2VpnPwHwListEntry.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    L2VpnPwHwListEntry.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

    if (L2VpnHwListGet (&L2VpnPwHwListEntry, &L2VpnPwHwListUpdate) !=
        MPLS_FAILURE)
    {
        if (L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry) != L2VPN_ZERO)
        {
            u4PwAcId = L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry);
        }
        else
        {

#endif
#endif
#endif
            /*Get the unique ID for Enet entry */
            u4PwAcId = MplsL2VpnGetAcId (void);
            if (MplsL2VpnSetAcId (u4PwAcId) != u4PwAcId)
            {
                return L2VPN_FAILURE;
            }
#ifdef NPAPI_WANTED
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
        }
    }
#endif
#endif
#endif
    pPwVcEnetEntry->u4PwAcId = u4PwAcId;
    MplsHwVplsInfo.mplsHwVplsVcInfo.u4PwAcId = u4PwAcId;
    L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPLS Enet (AC) Information: VPN id: %d VSI: %d VplsInstance: %d "
                "Port: %d Vlan: %d addition\n",
                u4VpnId, MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI,
                u4VplsInstance,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan);

#ifdef L2RED_WANTED
    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnPwVcVplsHwVpnAcAdd - AC VLAN %d added\n",
                    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan);
        MplsHwVplsInfo.mplsHwVplsVcInfo.u1AppOwner =
            L2VpPortGetPortOwnerFromIndex (MplsHwVplsInfo.
                                           mplsHwVplsVcInfo.FecInfo.
                                           u4AcIfIndex);
#ifdef NPAPI_WANTED
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)

        MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
        L2VpnPwHwListEntry.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VpnPwHwListEntry.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

        L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_CALLED);
#endif
#endif
        if (MplsFsMplsHwAddVpnAc (u4VplsInstance, u4VpnId,
                                  &MplsHwVplsInfo, u4Action) == FNP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                        "L2VpnPwVcVplsHwVpnAcAdd: "
                        "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                        "L2VpnPwVcVplsHwVpnAcAdd: "
                        "PW %d for %#x failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
            MplsL2VpnRelAcId (u4PwAcId);
            return L2VPN_FAILURE;
        }
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
        L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_SUCCESS);
#endif
#endif
#endif
    }
    L2VpnUtilSetEnetHwStatus (L2VPN_PWVC_ENET_PORT_IF_INDEX
                              (pPwVcEnetEntry),
                              L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry),
                              MPLS_TRUE, pPwVcEnetEntry->u4PwAcId);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcVplsHwVpnAcDel                                   */
/* Description   : Creates the VPN AC from the hw, applicable for VPLS       */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcVplsHwVpnAcDel (tPwVcEntry * pPwVcEntry,
                         tPwVcEnetEntry * pPwVcEnetEntry)
{
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tVPLSEntry         *pVplsEntry = NULL;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListDelKey;
#endif
#endif
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4VplsInstance = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

#ifdef NPAPI_WANTED
    UINT4               u4Action = L2VPN_ZERO;
#endif

    if (pPwVcEnetEntry->u1HwStatus == MPLS_FALSE)
    {
        return L2VPN_SUCCESS;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
    if (pVplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "VPLS entry is NULL\n");
        return L2VPN_FAILURE;
    }
    /* Fill up the VPLS info */
    MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]), sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);
    u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI = i4Vsi;
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VplsInstance =
        L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);

    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan =
        (UINT4) L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort =
        (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i1PwEnetVlanMode =
        L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4PwEnetPwVlan =
        L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
    MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
    L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                "VPLS Enet (AC) Information: VPN id: %d VSI: %d VplsInstance: %d "
                "Port: %d Vlan: %d deletion\n",
                u4VpnId, MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i4VSI,
                u4VplsInstance,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort,
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan);

#ifdef L2RED_WANTED
    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnPwVcVplsHwVpnAcDel - AC VLAN %d deleted\n",
                    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan);
        MplsHwVplsInfo.mplsHwVplsVcInfo.u1AppOwner =
            L2VpPortGetPortOwnerFromIndex (MplsHwVplsInfo.
                                           mplsHwVplsVcInfo.FecInfo.
                                           u4AcIfIndex);
#ifdef NPAPI_WANTED
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)

        MEMSET (&L2VpnPwHwListDelKey, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
        L2VpnPwHwListDelKey.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        L2VpnPwHwListDelKey.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

        L2VpnPwHwListDelete (&L2VpnPwHwListDelKey, L2VPN_PWAC_NPAPI_CALLED);
#endif
#endif

        if (MplsFsMplsHwDeleteVpnAc (u4VplsInstance, u4VpnId,
                                     &MplsHwVplsInfo, u4Action) == FNP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG3 (L2VPN_DBG_FNC_CRT_FLAG,
                        "L2VpnPwVcVplsHwVpnAcDel: "
                        "PW %d for Peer IPv4 -%#x IPv6 -%s failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        GenU4Addr.Addr.u4Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                        "L2VpnPwVcVplsHwVpnAcDel: "
                        "PW %d for %#x failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
            return L2VPN_FAILURE;
        }
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
        L2VpnPwHwListDelete (&L2VpnPwHwListDelKey, L2VPN_PWAC_NPAPI_SUCCESS);
#endif

#endif
    }
    if (pPwVcEnetEntry->u4PwAcId != L2VPN_ZERO)
    {
        MplsL2VpnRelAcId (pPwVcEnetEntry->u4PwAcId);
        pPwVcEnetEntry->u4PwAcId = L2VPN_ZERO;
    }
    pPwVcEnetEntry->u1HwStatus = MPLS_FALSE;
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnPortSnmpSendTrap                                     */
/* Description   : This function sends the trap message tp the fault manager */
/* Input(s)      : pTrapMsg - Notification message                           */
/*                 pc1SysLogMsg - Syslog message                             */
/* Output(s)     : None                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
PUBLIC VOID
L2vpnPortSnmpSendTrap (tSNMP_VAR_BIND * pTrapMsg, CHR1 * pc1SysLogMsg)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = (UINT1 *) pc1SysLogMsg;
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_MPLS_L2VPN;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CR_TRC, "L2VpnPortNotifyFaults: Sending "
                   "Notification to FM failed.\r\n");
        SNMP_free_snmp_vb_list (pTrapMsg);
    }
#else
    UNUSED_PARAM (pTrapMsg);
    UNUSED_PARAM (pc1SysLogMsg);
#endif
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPortGetNodeId
 *
 * DESCRIPTION      : This function is used by L2VPN module to get the
 *                    source/local Global Identifier & Node identifier
 * INPUT            : None
 *
 * OUTPUT           : Pointer to a structure that contains
 *                    Source Global Identifier & Source Node Identifier
 *
 * RETURNS          : OSIX_SUCCESS - If the function is successful
 *                    OSIX_FAILURE - If there are any failures
 *
 **************************************************************************/
PUBLIC INT4
L2VpnPortGetNodeId (tMplsGlobalNodeId * pMplsGlobalNodeId)
{

    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if ((pInMplsApiInfo =
         MemAllocMemBlk (L2VPN_MPLS_API_INPUT_INFO_POOL_ID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pOutMplsApiInfo =
         MemAllocMemBlk (L2VPN_MPLS_API_OUTPUT_INFO_POOL_ID)) == NULL)
    {
        MemReleaseMemBlock (L2VPN_MPLS_API_INPUT_INFO_POOL_ID,
                            (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }

    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pInMplsApiInfo->u4SubReqType = MPLS_GET_GLOBAL_NODE_ID;

    i4RetVal =
        MplsApiHandleExternalRequest (MPLS_GET_NODE_ID, pInMplsApiInfo,
                                      pOutMplsApiInfo);

    pMplsGlobalNodeId->u4GlobalId = pOutMplsApiInfo->OutNodeId.u4GlobalId;
    pMplsGlobalNodeId->u4NodeId = pOutMplsApiInfo->OutNodeId.u4NodeId;

    MemReleaseMemBlock (L2VPN_MPLS_API_INPUT_INFO_POOL_ID,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (L2VPN_MPLS_API_OUTPUT_INFO_POOL_ID,
                        (UINT1 *) pOutMplsApiInfo);

    return (i4RetVal);
}

/*MS-PW */

/*****************************************************************************/
/* Function Name : L2VpnFmUpdateMsPwStatus                                   */
/* Description   : Function to update the status of MSPW with FM             */
/* Input(s)      : pPriPwVcEntry - pointer to primary PwVc Entry             */
/*               : pSecPwVcEntry - pointer to Secodary PwVc Entry            */
/*                 u1PwOperStatus - Operational status of the MSPW           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnFmUpdateMsPwStatus (tPwVcEntry * pPriPwVcEntry, tPwVcEntry * pSecPwVcEntry,
                         UINT1 u1PwOperStatus)
{
    return (L2VpnUpdateMsPwStatus
            (pPriPwVcEntry, pSecPwVcEntry, u1PwOperStatus, NULL));
}

/*****************************************************************************/
/* Function Name : L2VpnUpdateMsPwStatus                                     */
/* Description   : Function to update the status of PwVc with FM for MSPW    */
/* Input(s)      : pPriPwVcEntry - pointer to PwVc Entry                     */
/*               : pSecPwVcEntry - pointer to PwVc Entry                     */
/*                 u1PwOperStatus - Operational status of the PwVc           */
/*                 pSInfo - Slot Information                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdateMsPwStatus (tPwVcEntry * pPriPwVcEntry, tPwVcEntry * pSecPwVcEntry,
                       UINT1 u1PwOperStatus, VOID *pSlotInfo)
{
    if ((L2VPN_PWVC_PSN_ENTRY (pPriPwVcEntry) == NULL) ||
        (L2VPN_PWVC_PSN_ENTRY (pSecPwVcEntry) == NULL))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "PSN Type is not configured for PW %d before add"
                    "into the HW\n", pPriPwVcEntry->u4PwVcIndex);
        return L2VPN_FAILURE;
    }

    if ((((pPriPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_DOWN)
          || (pPriPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_LLDOWN))
         || ((pSecPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_DOWN))
         || (pSecPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_LLDOWN))
        && ((u1PwOperStatus == L2VPN_PWVC_DOWN)
            || (u1PwOperStatus == L2VPN_PWVC_DELETE)))
    {
        return L2VPN_SUCCESS;
    }

    switch (u1PwOperStatus)
    {
        case L2VPN_MSPW_UP:
            L2VpnUtilSetHwStatus (pPriPwVcEntry, PW_HW_PW_UPDATE, L2VPN_ONE);

            if (L2VpnUtilValidateHwStatus (pPriPwVcEntry, L2VPN_PWVC_DELETE) !=
                L2VPN_SUCCESS)
            {
                /* Delete VCInfo for the Pseudowire */
                if (L2VpnPwVcHwDel (pPriPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "PWVC Delete in HW for PW %d failed\n",
                                pPriPwVcEntry->u4PwVcIndex);
                }
            }

            L2VpnUtilSetHwStatus (pSecPwVcEntry, PW_HW_PW_UPDATE, L2VPN_ONE);

            if (L2VpnUtilValidateHwStatus (pSecPwVcEntry, L2VPN_PWVC_DELETE) !=
                L2VPN_SUCCESS)
            {
                /* Delete VCInfo for the Pseudowire */
                if (L2VpnPwVcHwDel (pSecPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "PWVC Delete in HW for PW %d failed\n",
                                pSecPwVcEntry->u4PwVcIndex);
                }
            }

            /* Delete Already Created ILM Entry */
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                if (L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE,
                                       pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "ILM Delete in HW for PW %d failed\n",
                                pPriPwVcEntry->u4PwVcIndex);
                }
            }

            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                /* Delete Already Created ILM Entry */
                if (L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE,
                                       pSlotInfo) == L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "ILM Delete in HW for PW %d failed\n",
                                pSecPwVcEntry->u4PwVcIndex);
                }
            }
            /* MSPW ILM creation in forward direction */
            if (L2VpnMsPwForwardIlmHwAdd
                (pPriPwVcEntry, pSecPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "MSPW  Add in forward dir in HW  %d failed\n",
                            pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }

            /* MSPW ILM creation in reverse direction */
            if (L2VpnMsPwReverseIlmHwAdd
                (pPriPwVcEntry, pSecPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "MSPW  Add in forward Reverse in HW  %d failed\n",
                            pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }

            break;

        case L2VPN_MSPW_DOWN:

            /* Intentional fall through */
        case L2VPN_MSPW_DELETE:

            /* MSPW ILM deletion in forward direction */
            if (L2VpnMsPwForwardIlmHwDel
                (pPriPwVcEntry, pSecPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "MSPW  Del in forward dir in HW  %d failed\n",
                            pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }

            /* MSPW ILM Deletion in reverse direction */
            if (L2VpnMsPwReverseIlmHwDel
                (pPriPwVcEntry, pSecPwVcEntry, pSlotInfo) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "MSPW Deletein forward Reverse in HW  %d failed\n",
                            pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }
            if ((L2VPN_PWVC_MODE (pPriPwVcEntry) == L2VPN_VPWS)
                && (L2VPN_PWVC_MODE (pSecPwVcEntry) == L2VPN_VPWS))
            {
                L2VPN_PWVC_OPER_STATUS (pPriPwVcEntry) = L2VPN_PWVC_OPER_DOWN;
                L2VPN_PWVC_OPER_STATUS (pSecPwVcEntry) = L2VPN_PWVC_OPER_DOWN;
            }
            if ((L2VPN_PWVC_MODE (pPriPwVcEntry) == L2VPN_VPLS))
            {
                /* Create the PW Entry and ILM Entry for the Disassociated Pseudowire */

                if (pPriPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_UP)
                {
                    L2VpnUtilSetHwStatus (pPriPwVcEntry, PW_HW_PW_UPDATE,
                                          L2VPN_ONE);
                    if (L2VpnUtilValidateHwStatus (pPriPwVcEntry, L2VPN_PWVC_UP)
                        == L2VPN_FAILURE)
                    {
                        /* PWVC creation */
                        if (L2VpnPwVcHwAdd (pPriPwVcEntry, pSlotInfo) ==
                            L2VPN_FAILURE)
                        {
                            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                        "PWVC Add in HW for PW %d failed\n",
                                        pPriPwVcEntry->u4PwVcIndex);
                        }
                    }
                    /* PWVC ILM creation */
                    if (L2VpnPwVcIlmHwAdd (pPriPwVcEntry, pSlotInfo) ==
                        L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "ILM Add in HW for PW %d failed\n",
                                    pPriPwVcEntry->u4PwVcIndex);
                        L2VpnUtilSetHwStatus (pPriPwVcEntry, PW_HW_PW_UPDATE,
                                              L2VPN_ONE);
                        if (L2VpnPwVcHwDel (pPriPwVcEntry, pSlotInfo) ==
                            L2VPN_FAILURE)
                        {
                            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                        "PWVC Del in HW for PW %d failed\n",
                                        pPriPwVcEntry->u4PwVcIndex);
                        }

                    }

                }
            }

            if ((L2VPN_PWVC_MODE (pSecPwVcEntry) == L2VPN_VPLS))
            {

                if (pSecPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_UP)
                {
                    L2VpnUtilSetHwStatus (pSecPwVcEntry, PW_HW_PW_UPDATE,
                                          L2VPN_ONE);
                    if (L2VpnUtilValidateHwStatus (pSecPwVcEntry, L2VPN_PWVC_UP)
                        == L2VPN_FAILURE)
                    {
                        /* PWVC creation */
                        if (L2VpnPwVcHwAdd (pSecPwVcEntry, pSlotInfo) ==
                            L2VPN_FAILURE)
                        {
                            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                        "PWVC Add in HW for PW %d failed\n",
                                        pSecPwVcEntry->u4PwVcIndex);
                        }
                    }

                    /* PWVC ILM creation */
                    if (L2VpnPwVcIlmHwAdd (pSecPwVcEntry, pSlotInfo) ==
                        L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "ILM Add in HW for PW %d failed\n",
                                    pSecPwVcEntry->u4PwVcIndex);
                        L2VpnUtilSetHwStatus (pSecPwVcEntry, PW_HW_PW_UPDATE,
                                              L2VPN_ONE);
                        if (L2VpnPwVcHwDel (pSecPwVcEntry, pSlotInfo) ==
                            L2VPN_FAILURE)
                        {
                            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                        "PWVC Del in HW for PW %d failed\n",
                                        pSecPwVcEntry->u4PwVcIndex);
                        }

                    }

                }
            }
            break;

        default:

            /* No need to take any action */
            return L2VPN_SUCCESS;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnMsPwForwardIlmHwAdd                                  */
/* Description   : Adds the ILM entry in the Forward direction for MSPW 
 *                 into the hw                            */
/* Input(s)      : pPriPwVcEntry - Primary PWVC information                  */
/*          : pSecPwVcEntry - Secondary PWVC information                */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnMsPwForwardIlmHwAdd (tPwVcEntry * pPriPwVcEntry,
                          tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pPriMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pPriPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pSecMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pSecPwVcMplsEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    INT4                i4Vsi = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4Intf = 0;
    UINT4               u4OutOuterLabel = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4OutTnlXcIndex = 0;
    tVlanIfaceVlanId    VlanId;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    pPriPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPriPwVcEntry);
    pPriMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPriPwVcMplsEntry);

    pSecPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pSecPwVcEntry);
    pSecMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pSecPwVcMplsEntry);

    if (L2VpnGetVpnIdAndVsiFromPw (pPriPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    /*LSP type need to discuss */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;

    /* PwMplsType = TE */
    if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE) &&
        (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE))
    {

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams (pSecMplsOutTnlEntry, &u4OutTnlXcIndex,
                                    MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            return L2VPN_FAILURE;

        }

        if ((pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO) &&
            (u4OutTnlXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);

            return L2VPN_FAILURE;
        }

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &(pPriPwVcEntry->u4InOuterLabel), L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;

        }

        if (MplsGetL3Intf (u4Intf, &(pPriPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPriPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4InL3Intf = pPriPwVcEntry->u4InIfIndex;

        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
            pPriPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION, &u4Intf, &u4OutOuterLabel,
             L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;

        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        /*Fill ILM Info */

        MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPriPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

        /*Get Dst MAC */
        if (L2VpnGetDstMacForVcOnly (pSecPwVcEntry, MplsHwIlmInfo.au1DstMac)
            != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2VpnGetDstMacForVcOnly failed\n");
            return L2VPN_FAILURE;
        }

    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        if ((pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &(pPriPwVcEntry->u4InOuterLabel), L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;

        }

        if (MplsGetL3Intf (u4Intf, &(pPriPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPriPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf = pPriPwVcEntry->u4InIfIndex;
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
            pPriPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

        /*Get Dst MAC */
        if (L2VpnGetDstMacForVcOnly (pSecPwVcEntry, MplsHwIlmInfo.au1DstMac)
            != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2VpnGetDstMacForVcOnly failed\n");
            return L2VPN_FAILURE;
        }

    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE))
    {
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPriPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams
            (pSecMplsOutTnlEntry, &u4OutTnlXcIndex,
             MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            return L2VPN_FAILURE;

        }

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION, &u4Intf, &u4OutOuterLabel,
             L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;

        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;
    }

    /*ToDo for NON-TE case,since currently it is not supported for Manual mode
     * need to update later */

    if (MplsHwIlmInfo.u4InL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4InL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4InL3Intf);
            return L2VPN_FAILURE;
        }
        MplsHwIlmInfo.u4InVlanId = VlanId;
        if (MplsGetPhyPortFromLogicalIfIndex
            (MplsHwIlmInfo.u4InL3Intf, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4SrcPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", MplsHwIlmInfo.u4InL3Intf);
        }

    }
    if (MplsHwIlmInfo.u4OutL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4OutL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4OutL3Intf);
            return L2VPN_FAILURE;
        }
        if (VlanId != 0)
        {
            MplsHwIlmInfo.u4NewVlanId = VlanId;
            if (MplsGetPhyPortFromLogicalIfIndex
                (MplsHwIlmInfo.u4OutL3Intf,
                 (UINT1 *) (&(MplsHwIlmInfo.au1DstMac)),
                 &(MplsHwIlmInfo.u4OutPort)) != MPLS_SUCCESS)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "Fetching Physical port "
                            "for the L3 interface : %d failed.\r\n",
                            MplsHwIlmInfo.u4OutL3Intf);
            }
        }
        else
        {
            MplsHwIlmInfo.u4OutPort = MplsHwIlmInfo.u4OutL3Intf;
        }
    }

    switch (MPLS_DIFFSERV_TNL_MODEL (MPLS_DEF_INCARN))
    {
        case UNIFORM_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_UNIFORM;
            break;
        case PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_PIPE;
            break;
        case SHORT_PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_SHORTPIPE;
            break;
        default:
            CMNDB_DBG (DEBUG_ERROR_LEVEL, "invalid Tunnel DIFFSRV model \n");
            return L2VPN_FAILURE;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "VPN Id\n", u4VpnId);

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;
        if (MplsFsMplsMbsmHwCreateILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwCreateILM: "
                        "PW %d for  failed in HW\n",
                        pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }
    }
    else
#endif
    {

        /* Configure ILM in H/W */
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwCreateILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwCreateILM: "
                            "PW %d for failed in HW\n",
                            pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }
#endif
            pPriPwVcEntry->b1IsIlmEntryPresent = TRUE;
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnMsPwReverseIlmHwAdd                                  */
/* Description   : Adds the ILM entry in the reverse direction for MSPW
 *                  into the hw                                               */
/* Input(s)      : pPriPwVcEntry - Primary PWVC information                  */
/*          : pSecPwVcEntry - Secondary PWVC information                */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnMsPwReverseIlmHwAdd (tPwVcEntry * pPriPwVcEntry,
                          tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pPriMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pPriPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pSecMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pSecPwVcMplsEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    INT4                i4Vsi = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4Intf = 0;
    UINT4               u4OutOuterLabel = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4OutTnlXcIndex = 0;
#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif
    tVlanIfaceVlanId    VlanId;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    pPriPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPriPwVcEntry);
    pPriMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPriPwVcMplsEntry);

    pSecPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pSecPwVcEntry);
    pSecMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pSecPwVcMplsEntry);

    if (L2VpnGetVpnIdAndVsiFromPw (pPriPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    /*LSP type need to discuss */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;

    /* PwMplsType = TE */
    if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE) &&
        (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE))
    {

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams
            (pPriMplsOutTnlEntry, &u4OutTnlXcIndex,
             MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            return L2VPN_FAILURE;
        }

        if ((pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO) &&
            (u4OutTnlXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &(pSecPwVcEntry->u4InOuterLabel), L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetL3Intf (u4Intf, &(pSecPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pSecPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4InL3Intf = pSecPwVcEntry->u4InIfIndex;

        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
            pSecPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION,
             &u4Intf, &u4OutOuterLabel, L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n", u4OutTnlXcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        /*Fill ILM Info */
        MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pSecPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);

        /*Get Dst MAC */
        if (L2VpnGetDstMacForVcOnly (pPriPwVcEntry, MplsHwIlmInfo.au1DstMac)
            != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2VpnGetDstMacForVcOnly failed\n");
            return L2VPN_FAILURE;
        }
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams
            (pPriMplsOutTnlEntry, &u4OutTnlXcIndex,
             MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            return L2VPN_FAILURE;
        }

        if ((u4OutTnlXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION,
             &u4Intf, &u4OutOuterLabel, L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n", u4OutTnlXcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pSecPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;

    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE))
    {
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &pSecPwVcEntry->u4InOuterLabel, L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetL3Intf (u4Intf, &(pSecPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pSecPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
            pSecPwVcEntry->u4InVcLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf = pSecPwVcEntry->u4InIfIndex;

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);

        /*Get Dst MAC */
        if (L2VpnGetDstMacForVcOnly (pPriPwVcEntry, MplsHwIlmInfo.au1DstMac)
            != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2VpnGetDstMacForVcOnly failed\n");
            return L2VPN_FAILURE;
        }
    }

    if (MplsHwIlmInfo.u4InL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4InL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4InL3Intf);
            return L2VPN_FAILURE;
        }
        MplsHwIlmInfo.u4InVlanId = VlanId;
        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (MplsHwIlmInfo.u4InL3Intf, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4SrcPort)) != MPLS_SUCCESS)
        {
            L2VPN_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", MplsHwIlmInfo.u4InL3Intf);
        }

#if defined(MBSM_WANTED) || defined(NPAPI_WANTED)
#ifdef QOSX_WANTED
        /* Fetch the Qos Information for teh incoming port */
        QosGetMplsExpProfileForPortAtIngress (MplsHwIlmInfo.u4SrcPort,
                                              &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
        QosGetMplsExpProfileForPortAtEgress (MplsHwIlmInfo.u4OutPort,
                                             &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwPriMapId = i4HwExpMapId;
#endif
#endif
    }
    if (MplsHwIlmInfo.u4OutL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4OutL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4OutL3Intf);
            return L2VPN_FAILURE;
        }
        if (VlanId != 0)
        {
            MplsHwIlmInfo.u4NewVlanId = VlanId;
            if (MplsGetPhyPortFromLogicalIfIndex
                (MplsHwIlmInfo.u4OutL3Intf,
                 (UINT1 *) (&(MplsHwIlmInfo.au1DstMac)),
                 &(MplsHwIlmInfo.u4OutPort)) != MPLS_SUCCESS)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "Fetching Physical port "
                            "for the L3 interface : %d failed.\r\n",
                            MplsHwIlmInfo.u4OutL3Intf);
            }
        }
        else
        {
            MplsHwIlmInfo.u4OutPort = MplsHwIlmInfo.u4OutL3Intf;
        }
    }

    switch (MPLS_DIFFSERV_TNL_MODEL (MPLS_DEF_INCARN))
    {
        case UNIFORM_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_UNIFORM;
            break;
        case PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_PIPE;
            break;
        case SHORT_PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_SHORTPIPE;
            break;
        default:
            CMNDB_DBG (DEBUG_ERROR_LEVEL, "invalid Tunnel DIFFSRV model \n");
            return L2VPN_FAILURE;
    }

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;
        if (MplsFsMplsMbsmHwCreateILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwCreateILM: "
                        "PW %d failed in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }
    }
    else
#endif
    {

        /* Configure ILM in H/W */
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwCreateILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwCreateILM: "
                            "PW %d failed in HW\n", pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }
#endif
            pPriPwVcEntry->b1IsIlmEntryPresent = TRUE;

        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnMsPwForwardIlmHwDel                                  */
/* Description   : Deletes the ILM entry in the Forward direction for MSPW 
 *                 from the hw                            */
/* Input(s)      : pPriPwVcEntry - Primary PWVC information                  */
/*          : pSecPwVcEntry - Secondary PWVC information                */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnMsPwForwardIlmHwDel (tPwVcEntry * pPriPwVcEntry,
                          tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pPriMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pPriPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pSecMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pSecPwVcMplsEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    INT4                i4Vsi = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4Intf = 0;
    UINT4               u4OutOuterLabel = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4OutTnlXcIndex = 0;
    UINT4               u4InLabel = L2VPN_INVALID_LABEL;
    UINT4               u4InOuterLabel = 0;
    INT4                i4Status = L2VPN_SUCCESS;
    tVlanIfaceVlanId    VlanId;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    pPriPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPriPwVcEntry);
    pPriMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPriPwVcMplsEntry);

    pSecPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pSecPwVcEntry);
    pSecMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pSecPwVcMplsEntry);

    if (L2VpnGetVpnIdAndVsiFromPw (pPriPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    u4InOuterLabel = pPriPwVcEntry->u4InOuterLabel;

    /*LSP type need to discuss */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;

    /* PwMplsType = TE */
    if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE) &&
        (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE))
    {

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams (pSecMplsOutTnlEntry, &u4OutTnlXcIndex,
                                    MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if ((pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO) &&
            (u4OutTnlXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &(u4InOuterLabel), L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        pPriPwVcEntry->u4InOuterLabel = u4InOuterLabel;

        if (MplsGetL3Intf (u4Intf, &(pPriPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPriPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4InL3Intf = pPriPwVcEntry->u4InIfIndex;
        u4InLabel = L2VpnPassValidInLabelToHw (pPriPwVcEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION, &u4Intf, &u4OutOuterLabel,
             L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched for XC Index %d\n",
                        u4OutTnlXcIndex);
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        /*Fill ILM Info */
        MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP;
        u4InLabel = L2VpnPassValidInLabelToHw (pPriPwVcEntry);
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {
        if ((pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &(pPriPwVcEntry->u4InOuterLabel), L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pPriPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if (MplsGetL3Intf (u4Intf, &(pPriPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pPriPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf = pPriPwVcEntry->u4InIfIndex;
        u4InLabel = L2VpnPassValidInLabelToHw (pPriPwVcEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE))
    {
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;
        u4InLabel = L2VpnPassValidInLabelToHw (pPriPwVcEntry);
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams
            (pSecMplsOutTnlEntry, &u4OutTnlXcIndex,
             MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION, &u4Intf, &u4OutOuterLabel,
             L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pSecPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;
    }

    /*ToDo for NON-TE case,since currently it is not supported for Manual mode
     * need to update later */

    if (MplsHwIlmInfo.u4InL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4InL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4InL3Intf);
            return L2VPN_FAILURE;
        }
        MplsHwIlmInfo.u4InVlanId = VlanId;
    }
    if (MplsHwIlmInfo.u4OutL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4OutL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4OutL3Intf);
            return L2VPN_FAILURE;
        }
        if (VlanId != 0)
        {
            MplsHwIlmInfo.u4NewVlanId = VlanId;
            if (MplsGetPhyPortFromLogicalIfIndex
                (MplsHwIlmInfo.u4OutL3Intf,
                 (UINT1 *) (&(MplsHwIlmInfo.au1DstMac)),
                 &(MplsHwIlmInfo.u4OutPort)) != MPLS_SUCCESS)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "Fetching Physical port "
                            "for the L3 interface : %d failed.\r\n",
                            MplsHwIlmInfo.u4OutL3Intf);
            }
        }
        else
        {
            MplsHwIlmInfo.u4OutPort = MplsHwIlmInfo.u4OutL3Intf;
        }
    }

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;
        if (MplsFsMplsMbsmHwDeleteILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwDeleteILM: "
                        "PW %d  failed in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwDeleteILM:  PW %d failed in HW\n",
                            pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }
#endif
            pPriPwVcEntry->b1IsIlmEntryPresent = FALSE;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnMsPwReverseIlmHwDel                                  */
/* Description   : Deletes the ILM entry in the reverse direction for MSPW
 *                  from the hw                                               */
/* Input(s)      : pPriPwVcEntry - Primary PWVC information                  */
/*          : pSecPwVcEntry - Secondary PWVC information                */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnMsPwReverseIlmHwDel (tPwVcEntry * pPriPwVcEntry,
                          tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo)
{
    tPwVcMplsTnlEntry  *pPriMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pPriPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pSecMplsOutTnlEntry = NULL;
    tPwVcMplsEntry     *pSecPwVcMplsEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    INT4                i4Vsi = 0;
    UINT4               u4VpnId = 0;
    UINT4               u4Intf = 0;
    tVlanIfaceVlanId    VlanId;
    UINT4               u4OutOuterLabel = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4InLabel = L2VPN_INVALID_LABEL;
    UINT4               u4OutTnlXcIndex = 0;
    INT4                i4Status = L2VPN_SUCCESS;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    pPriPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPriPwVcEntry);
    pPriMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPriPwVcMplsEntry);

    pSecPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pSecPwVcEntry);
    pSecMplsOutTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pSecPwVcMplsEntry);

    if (L2VpnGetVpnIdAndVsiFromPw (pPriPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    /*LSP type need to discuss */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;

    /* PwMplsType = TE */
    if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE) &&
        (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE))
    {

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams (pPriMplsOutTnlEntry, &u4OutTnlXcIndex,
                                    MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if ((pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex == L2VPN_ZERO) &&
            (u4OutTnlXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &(pSecPwVcEntry->u4InOuterLabel), L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if (MplsGetL3Intf (u4Intf, &(pSecPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pSecPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        MplsHwIlmInfo.u4InL3Intf = pSecPwVcEntry->u4InIfIndex;
        u4InLabel = L2VpnPassValidInLabelToHw (pSecPwVcEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION,
             &u4Intf, &u4OutOuterLabel, L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n", u4OutTnlXcIndex);
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        /*Fill ILM Info */
        MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP;
        u4InLabel = L2VpnPassValidInLabelToHw (pSecPwVcEntry);
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);
    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY))
    {

        /* Get Out going tunnel XC Index */
        if (MplsGetOutTunnelParams (pPriMplsOutTnlEntry, &u4OutTnlXcIndex,
                                    MplsHwIlmInfo.au1DstMac) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Out going tunnel does not exist\n");
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        if ((u4OutTnlXcIndex == L2VPN_ZERO))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Ingress/Egress Tunnel is not up during PW %d add "
                        "in HW\n", pPriPwVcEntry->u4PwVcIndex);
            if (pPriPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pPriPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }

        }

        /*Get Outgoing Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (u4OutTnlXcIndex, MPLS_DEF_DIRECTION,
             &u4Intf, &u4OutOuterLabel, L2VPN_FALSE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n", u4OutTnlXcIndex);
            if (pSecPwVcEntry->b1IsIlmEntryPresent == TRUE)
            {
                i4Status = L2VpnPwVcIlmHwDel (pSecPwVcEntry, TRUE, pSlotInfo);
                return i4Status;
            }
        }

        if (MplsGetL3Intf (u4Intf, &u4L3Intf) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        /*Update ILM entry for insegment */
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;
        u4InLabel = L2VpnPassValidInLabelToHw (pSecPwVcEntry);
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pSecMplsOutTnlEntry);

        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel = u4OutOuterLabel;
        MplsHwIlmInfo.MplsPushLblList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf = u4L3Intf;

    }
    else if ((L2VPN_PWVC_MPLS_MPLS_TYPE (pPriPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_VCONLY) &&
             (L2VPN_PWVC_MPLS_MPLS_TYPE (pSecPwVcMplsEntry)
              == L2VPN_MPLS_TYPE_TE))
    {
        MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_SWAP_PUSH;

        /*Get Incoming Tunnel information */
        if (MplsGetInorOutIfIndexAndLabelFromXcIndex
            (pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex,
             MPLS_DEF_DIRECTION, &u4Intf,
             &pSecPwVcEntry->u4InOuterLabel, L2VPN_TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Outer label can't be fetched "
                        "for XC Index %d\n",
                        pSecPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex);
            return L2VPN_FAILURE;
        }

        if (MplsGetL3Intf (u4Intf, &(pSecPwVcEntry->u4InIfIndex))
            == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4Intf);
            return L2VPN_FAILURE;
        }

        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            pSecPwVcEntry->u4InOuterLabel;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        u4InLabel = L2VpnPassValidInLabelToHw (pSecPwVcEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = u4InLabel;
        MplsHwIlmInfo.InLabelList[1].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4InL3Intf = pSecPwVcEntry->u4InIfIndex;

        /*update ILM for Out segment */
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            pPriPwVcEntry->u4OutVcLabel;
        MplsHwIlmInfo.MplsSwapLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwIlmInfo.u4OutL3Intf =
            L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPriMplsOutTnlEntry);
    }

    if (MplsHwIlmInfo.u4InL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4InL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4InL3Intf);
            return L2VPN_FAILURE;
        }
        MplsHwIlmInfo.u4InVlanId = VlanId;
    }
    if (MplsHwIlmInfo.u4OutL3Intf != 0)
    {
        if (CfaGetVlanId (MplsHwIlmInfo.u4OutL3Intf, &VlanId) == CFA_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Unable to get InVlanId from L3Intf %d.\n",
                        MplsHwIlmInfo.u4OutL3Intf);
            return L2VPN_FAILURE;
        }
        if (VlanId != 0)
        {
            MplsHwIlmInfo.u4NewVlanId = VlanId;
            if (MplsGetPhyPortFromLogicalIfIndex
                (MplsHwIlmInfo.u4OutL3Intf,
                 (UINT1 *) (&(MplsHwIlmInfo.au1DstMac)),
                 &(MplsHwIlmInfo.u4OutPort)) != MPLS_SUCCESS)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "Fetching Physical port "
                            "for the L3 interface : %d failed.\r\n",
                            MplsHwIlmInfo.u4OutL3Intf);
            }
        }
        else
        {
            MplsHwIlmInfo.u4OutPort = MplsHwIlmInfo.u4OutL3Intf;
        }
    }

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;
        if (MplsFsMplsMbsmHwDeleteILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                        "FsMplsMbsmHwDeleteILM: "
                        "PW %d failed in HW\n", pPriPwVcEntry->u4PwVcIndex);
            return L2VPN_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                            "FsMplsHwDeleteILM: "
                            "PW %d failed in HW\n", pPriPwVcEntry->u4PwVcIndex);
                return L2VPN_FAILURE;
            }
#endif
            pPriPwVcEntry->b1IsIlmEntryPresent = FALSE;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;
}

/************************************************************************
 * Function Name   :MplsGetOutTunnelParams 
 * Description     : Function is used to get the Out Tunnel XCIndex 
 *                This function is an API to be used by Applications like L2VPN. 
 *                   Hence, MPLS_CMN_LOCK is taken here.
 * Input           : pMplsOutTnlEntry - Pointer to Outgoing tunnel

 * Output          : pu4XcIndex - Pointer to XC Index                  
 *                      pu1PwDstMac - PW destination MAC
 *                   
 * Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT4
MplsGetOutTunnelParams (tPwVcMplsTnlEntry * pMplsOutTnlEntry,
                        UINT4 *pu4XcIndex, UINT1 *pu1PwDstMac)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4PeerLsrId = 0;
    UINT4               u4LclLsrId = 0;

    MPLS_CMN_LOCK ();

    CONVERT_TO_INTEGER (L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR (pMplsOutTnlEntry),
                        u4LclLsrId);

    CONVERT_TO_INTEGER (L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR (pMplsOutTnlEntry),
                        u4PeerLsrId);

    pTeTnlInfo =
        TeGetTunnelInfo (L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsOutTnlEntry),
                         L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsOutTnlEntry),
                         OSIX_NTOHL (u4LclLsrId), OSIX_NTOHL (u4PeerLsrId));
    if (pTeTnlInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Out going tunnel does not exist\n");
        MPLS_CMN_UNLOCK ();
        return L2VPN_FAILURE;
    }

    *pu4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    MEMCPY (pu1PwDstMac, pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);

    MPLS_CMN_UNLOCK ();

    return L2VPN_SUCCESS;
}

/************************************************************************
 * Function Name   : L2VpnGetDstMacForVcOnly
 * Description     : Function is used to get the Dst MAC address for VC Only PW
 *                      VC
 * Input           : pPwVcEntry- Pointer to PW VC entry

 * Output          : pu1PwDstMac - PW destination MAC
 *                   
 * Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT4
L2VpnGetDstMacForVcOnly (tPwVcEntry * pPwVcEntry, UINT1 *pu1PwDstMac)
{
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4IpAddr = 0;
    UINT4               u4PeerAddr = 0;
    UINT1               u1EncapType = 0;

    MEMCPY ((UINT1 *) &u4PeerAddr, (UINT1 *) &pPwVcEntry->PeerAddr,
            IPV4_ADDR_LENGTH);

    u4IpAddr = OSIX_HTONL (u4PeerAddr);

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC Add for PW %d %x started\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (u4PeerAddr));

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    MEMSET (&RtQuery, 0, sizeof (RtQuery));

    RtQuery.u4DestinationIpAddress = u4IpAddr;
    RtQuery.u4DestinationSubnetMask = 0xffffffff;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
        {
            u4IpAddr = NetIpRtInfo.u4NextHop;
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC Add: Next hop %x for the fec %x started\n",
                    u4IpAddr, pPwVcEntry->PeerAddr);

    }

    /* Get the destination MAC for a given Peer address */
    if ((ArpResolve (u4IpAddr, (INT1 *) pu1PwDstMac, &u1EncapType)) ==
        ARP_FAILURE)
    {
        /* Since we have not succeeded in getting the NextHop MAC
         * Trigger Arp Module to resolve this IP and after
         * MPLS_ARP_RESOLVE_TIMEOUT we shall check again if this has 
         * been resolved,
         * If resolved, then we go ahead in installing PWs which were
         * waiting for this resolve , if not Raise this as FAILURE
         */

        if (MplsTriggerArpResolve (u4IpAddr, (UINT1) MPLS_PW_ARP_TIMER)
            == L2VPN_SUCCESS)
        {
            L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) =
                MPLS_ARP_RESOLVE_WAITING;
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Arp Resolve Timer Started for %x\n", u4IpAddr);
            return L2VPN_FAILURE;
        }

        L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) =
            MPLS_ARP_RESOLVE_FAILED;
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Arp Resolve Queue Posting failed for Next Hop %x\n",
                    u4IpAddr);
        return L2VPN_FAILURE;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "Arp Resolve Succeded for Next Hop %x\n", u4IpAddr);
    L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) = MPLS_ARP_RESOLVED;

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwDelP2MPDest                                    */
/* Description   : Deletes a P2MP branch in the PWVc entry into the hw       */
/* Input(s)      : pPwVcEntry - PWVC information                             */
/*                 pSlotInfo - Slot information                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwVcHwDelP2MPDest (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo)
{
    tMplsHwVcTnlInfo    MplsHwVcInfo;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    UINT4               u4VpnId = L2VPN_ZERO;
    INT4                i4Vsi = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwVcHwDelP2MPDest: ENTRY\n");
    /* Init Npapi related data structures */
    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC Delete for Outgoing L3Intf %d for PW %d Peer -IPv4 -%x "
                " IPv6 -%s started\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC Delete for Outgoing L3Intf %d for PW %d %x started\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) = MPLS_ARP_RESOLVE_UNKNOWN;

    L2VPN_PWVC_ROUTE_STATUS (pPwVcEntry) = L2VPN_PWVC_ROUTE_UNKNOWN;

    /* fill up VC info in MplsHwVcInfo */
    MplsHwVcInfo.LabelOpcodes = MPLS_HW_PRESERVE_USER_TAG;
    MplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel = pPwVcEntry->u4OutVcLabel;
    MplsHwVcInfo.PwOutVcLabel.MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

    if (L2VpnGetVpnIdAndVsiFromPw (pPwVcEntry, &u4VpnId, &i4Vsi)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "VPN Id not associated with this PW\n");
        return L2VPN_FAILURE;
    }

    L2VpnPwVcFetchAcInfo (pPwVcEntry,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                          &MplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                          &MplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                          &MplsHwVcInfo.FecInfo.i1PwEnetVlanMode);

    /* PwMplsType = TE */
    /* TE Tunnel L3 I/F index */
    MplsHwVcInfo.u4PwL3Intf = pPwVcEntry->u4PwL3Intf;

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outgoing VC Label: %d, Outgoing L3Intf: %d\n",
                pPwVcEntry->u4OutVcLabel, pPwVcEntry->u4PwL3Intf);

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "VPN Id: %d\n", u4VpnId);

    if (L2VpnPwVcHwDelWrp (pPwVcEntry, pSlotInfo, &MplsHwVcInfo,
                           &MplsHwVplsInfo, u4VpnId) == L2VPN_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC P2MP Dest Delete for Outgoing L3Intf %d for PW %d Peer IPv4 -%x"
                    " IPv6 -%s  failed\n",
                    pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PWVC P2MP Dest Delete for Outgoing L3Intf %d for PW %d %x failed\n",
                    pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC P2MP Dest Delete for Outgoing L3Intf %d for PW %d Peer IPv4 -%x"
                " IPv6 -%s  successful\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "PWVC P2MP Dest Delete for Outgoing L3Intf %d for PW %d %x successfull\n",
                pPwVcEntry->u4PwL3Intf, pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif
    return L2VPN_FAILURE;

}

/*MS-PW */

/* VPLS FDB */
/*****************************************************************************/
/* Function Name : L2VpnVlanValidateVlanId                                   */
/* Description   : Checks whether VLAN id is statically present or not. This */
/*                 is For mapping VplsInstance to a L2 FDB Id.               */
/*                 Two important points to be mentioned here are:            */
/*                 1) This will check in static vlan entries alone. For      */
/*                    dynamically learnt entries, this is not applicable.    */
/*                 2) Context ID will be default (0). As MPLS works on SI,if */
/*                    the VLAN id does not present in default context, then  */
/*                    FAILURE will be returned.                              */
/* Input(s)      : u4ContextId - Default context Identifier                  */
/*                 u4FdbId     - static vlan identifier (4096 - 65535)       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnVlanValidateVlanId (UINT4 u4ContextId, UINT2 u2VplsFdbId)
{
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnVlanValidateVlanId: ENTRY\n");

    if (VlanValidateVlanId (u4ContextId, (UINT4) u2VplsFdbId) == VLAN_FAILURE)
    {
        L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG, "L2VpnVlanValidateVlanId: "
                    "Vlan %d for context %d does not exist. \n",
                    u2VplsFdbId, u4ContextId);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnVlanValidateVlanId: EXIT\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnDeleteL3FtnAndPsnTnlIf                               */
/* Description   : This function deletes L3 FTN from HW and / or Tunnel If   */
/*                 for the tunnel down event                                 */
/* Input(s)      : pMplsTeEvtInfo  - Pointer to TE Event Info                */
/*                 u4Event         - Event                                   */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnDeleteL3FtnAndPsnTnlIf (tL2VpnMplsPwVcMplsTeEvtInfo * pMplsTeEvtInfo,
                             UINT4 u4Event)
{
    UINT4               u4PsnDelAct = L2VPN_ZERO;
    UINT4               u4LclLsrId = L2VPN_ZERO;
    UINT4               u4PeerLsrId = L2VPN_ZERO;

    /* For PLR_DEL case also, Tunnel Deletion is required */
    if ((u4Event != L2VPN_MPLS_PWVC_TE_TNL_DOWN) &&
        (u4Event != L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL))
    {
        return;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "Interface %d is to be removed\n",
                pMplsTeEvtInfo->u4TnlIfIndex);

    u4PsnDelAct = pMplsTeEvtInfo->u1DeleteTnlAction;

    CONVERT_TO_INTEGER (pMplsTeEvtInfo->PwVcMplsTnlLclLSR, u4LclLsrId);
    CONVERT_TO_INTEGER (pMplsTeEvtInfo->PwVcMplsTnlPeerLSR, u4PeerLsrId);

    u4LclLsrId = OSIX_NTOHL (u4LclLsrId);
    u4PeerLsrId = OSIX_NTOHL (u4PeerLsrId);

    L2VpnHandlePsnTnlDeletion (pMplsTeEvtInfo->u4PwVcMplsTnlIndex,
                               pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                               u4LclLsrId, u4PeerLsrId,
                               pMplsTeEvtInfo->u4TnlIfIndex,
                               pMplsTeEvtInfo->au1NextHopMac, u4PsnDelAct);
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnHandlePsnTnlDeletion                                 */
/* Description   : This function handles the deletion of Tunnel mappings     */
/*                 associated to the pseudowire                              */
/* Input(s)      : u4TnlIndex    - Tunnel Index                              */
/*                 u2TnlInstance - Tunnel Instance                           */
/*                 u4LclLsr      - Local LSR ID                              */
/*                 u4PeerLsr     - Peer LSR ID                               */
/*                 u4PwL3Intf     - Tunnel IfIndex                           */
/*                 pu1NextHopMac  - NextHopMac                               */
/*                 u4PsnDelAction - Tunnel Deletion action                   */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandlePsnTnlDeletion (UINT4 u4TnlIndex, UINT2 u2TnlInstance,
                           UINT4 u4LclLsr, UINT4 u4PeerLsr, UINT4 u4PwL3Intf,
                           UINT1 *pu1NextHopMac, UINT4 u4PsnDelAction)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT1               u1IsOamEnabled = FALSE;

    if ((u4PsnDelAction == L2VPN_PSN_DEL_L3FTN) ||
        (u4PsnDelAction == L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF))
    {

        MPLS_CMN_LOCK ();
        pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u2TnlInstance, u4LclLsr,
                                      u4PeerLsr);
        if (pTeTnlInfo != NULL)
        {
            if (MplsIsOamMonitoringTnl (pTeTnlInfo) == TRUE)
            {
                u1IsOamEnabled = TRUE;
            }
            if ((u1IsOamEnabled == FALSE) &&
                (MplsTunnelNPDeletion (pTeTnlInfo) == MPLS_FAILURE))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "NP Tnl Delete Failed\n");
                MPLS_CMN_UNLOCK ();
                return;
            }
            /* If u1TnlDelSyncFlag is set, Event has to be posted RSVPTE module
             * to reprograme the tunnel
             * */
            if (pTeTnlInfo->u1TnlDelSyncFlag == MPLS_TRUE)
            {
                RpteL2VPNEventHandler (pTeTnlInfo);
            }
            MPLS_CMN_UNLOCK ();
        }
        else
        {
            MPLS_CMN_UNLOCK ();
            L2VpnDeletePsnTnlIf (u4PwL3Intf, pu1NextHopMac,
                                 L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF);
            return;
        }

        /*To delete Tunnel interface in CFA for L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF alone */
        if (u4PsnDelAction == L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF)
        {
            L2VpnDeletePsnTnlIf (u4PwL3Intf, pu1NextHopMac,
                                 L2VPN_PSN_DEL_MPLS_TNL_IF);
        }
    }
    else
    {
        L2VpnDeletePsnTnlIf (u4PwL3Intf, pu1NextHopMac, u4PsnDelAction);
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpPortGetPortOwnerFromIndex                      */
/* Description   : This function gives the Port owner of the given interface */
/* Input(s)      : u4IfIndex - IfIndex of the port                           */
/* Output(s)     : u1AppOwner  - Port owner for the given interface          */
/* Return(s)     : L2VPN_FAILURE / L2VPN_SUCCESS                             */
/*****************************************************************************/
UINT1
L2VpPortGetPortOwnerFromIndex (UINT4 u4IfIndex)
{
    UINT1               u1AppOwner = MPLS_APPLICATION_NONE;
    UINT1               u1Status = OSIX_DISABLED;

    L2IwfGetPortStateCtrlOwner (u4IfIndex, &u1AppOwner, OSIX_TRUE);
    if (u1AppOwner == STP_MODULE)
    {
        L2IwfGetProtocolEnabledStatusOnPort (u4IfIndex, L2_PROTO_STP,
                                             &u1Status);
        if (u1Status == OSIX_ENABLED)
        {
            u1AppOwner = MPLS_APPLICATION_STP;
        }
        else
        {
            u1AppOwner = MPLS_APPLICATION_NONE;
        }
    }
    else if (u1AppOwner == ERPS_MODULE)
    {
        u1AppOwner = MPLS_APPLICATION_ERPS;
    }
    else
    {
        u1AppOwner = MPLS_APPLICATION_ELPS;
    }
    return u1AppOwner;
}

/*****************************************************************************/
/* Function Name : L2VpnHwVplsFlushMac                                       */
/* Description   : This API flushes the MAC address, of instance passed in   */
/*                 Pw Entry                                                  */
/* Input(s)      : tPwVcEntry - PwEntry                                      */
/*                 tVPLSEntry - Vpls Entry                                   */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS                                             */
/*****************************************************************************/
INT4
L2VpnHwVplsFlushMac (tPwVcEntry * pPwVcEntry, tVPLSEntry * pVplsEntry)
{
    UINT4               u4VpnId;
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tMplsHwVcTnlInfo    MplsHwVcInfo;

    MEMSET (&MplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));

    MplsHwVcInfo.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);
    MplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel =
        L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);

    MEMCPY (&MplsHwVplsInfo.mplsHwVplsVcInfo, &MplsHwVcInfo,
            sizeof (tMplsHwVcTnlInfo));

    MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]), sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);
    MplsHwVplsInfo.u2VplsFdbId = pVplsEntry->u2VplsFdbId;

#ifdef NPAPI_WANTED
    MplsFsMplsHwVplsFlushMac (pVplsEntry->u4VplsInstance, u4VpnId,
                              &MplsHwVplsInfo);
#endif

    return L2VPN_SUCCESS;
}

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
/*****************************************************************************/
/* Function Name : L2VpnDelStalePwVc                                         */
/* Description   : This function Deletes the PWVc from hardware                 */
/* Input(s)      : pL2VpnPwHwListEntry - HW List Entry                       */
/* Output(s)     : None                                                         */
/* Return(s)     : L2VPN_FAILURE / L2VPN_SUCCESS                             */
/*****************************************************************************/
tMplsHwVcTnlInfo    gMplsHwVcInfo;
tMplsHwVcTnlInfo    gNextMplsHwVcInfo;

INT4
L2VpnDelStalePwVc (tL2VpnPwHwList * pL2VpnPwHwListEntry, VOID *pSlotInfo)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tMplsHwVplsInfo     MplsHwVplsInfo;
#ifdef VPLS_GR_WANTED
    tVplsAcMapEntry     VplsAcEntry;
    tVplsAcMapEntry    *pVplsAcEntry = NULL;
#endif
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4Action = L2VPN_ZERO;
    UINT4               u4VpnId;
    UINT4               u4PwTmpVcIndex = L2VPN_ZERO;
    UINT1               u1HwStatus = L2VPN_ZERO;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&gMplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));
    MEMSET (&gNextMplsHwVcInfo, 0, sizeof (tMplsHwVcTnlInfo));
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
#ifdef VPLS_GR_WANTED
    MEMSET (&VplsAcEntry, 0, sizeof (tVplsAcMapEntry));
#endif

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (L2VPN_PW_HW_LIST_VPLS_INDEX
                                            (pL2VpnPwHwListEntry));
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : VPLS Index = %u not exists\n", __func__,
                    L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry));
        return L2VPN_FAILURE;
    }
    /* Fill up the VPLS info */
    MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]), sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);
    gMplsHwVcInfo.FecInfo.u4VplsInstance =
        L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry);
    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VplsInstance =
        L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry);
    MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
    gMplsHwVcInfo.PwOutVcLabel.u.MplsShimLabel =
        L2VPN_PW_HW_LIST_OUT_LABEL (pL2VpnPwHwListEntry);
    gMplsHwVcInfo.u4PwIndex = L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry);
    gMplsHwVcInfo.u4PwL3Intf =
        L2VPN_PW_HW_LIST_OUT_IF_INDEX (pL2VpnPwHwListEntry);

    if (L2VPN_ZERO == L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry))
    {
        pPwVcEntry =
            L2VpnGetPwVcEntryFromIndex (L2VPN_PW_HW_LIST_PW_INDEX
                                        (pL2VpnPwHwListEntry));
        if (NULL == pPwVcEntry)
        {
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWVC_NPAPI_CALLED);
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_CALLED);
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_SUCCESS);
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWVC_NPAPI_SUCCESS);
            return L2VPN_SUCCESS;
        }
        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
        {
            gMplsHwVcInfo.u1VpwsPwHwAction = PW_HW_VPN_UPDATE;
            L2VpnPwVcFetchAcInfo (pPwVcEntry,
                                  &gMplsHwVcInfo.FecInfo.u4VpnSrcVlan,
                                  &gMplsHwVcInfo.FecInfo.u4VpnSrcPhyPort,
                                  &gMplsHwVcInfo.FecInfo.u4PwEnetPwVlan,
                                  &gMplsHwVcInfo.FecInfo.i1PwEnetVlanMode);
            if ((gMplsHwVcInfo.FecInfo.u4VpnSrcVlan == L2VPN_ZERO) &&
                (gMplsHwVcInfo.FecInfo.u4VpnSrcPhyPort == L2VPN_ZERO))
            {
                return L2VPN_SUCCESS;
            }
            if (pPwVcEntry->pServSpecEntry != NULL)
            {
                pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                    (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                 L2VPN_PWVC_ENET_ENTRY
                                                 (pPwVcEntry)));
                if (pPwVcEnetEntry != NULL)
                {
                    pPwVcEnetEntry->u4PwAcId =
                        L2VPN_PW_HW_LIST_AC_ID (pL2VpnPwHwListEntry);
                }
            }
            if (pPwVcEnetEntry != NULL)
            {
                gMplsHwVcInfo.u4PwAcId = pPwVcEnetEntry->u4PwAcId;
            }
#ifdef MBSM_WANTED
            if (pSlotInfo != NULL)
            {
                L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                                     L2VPN_PWAC_NPAPI_CALLED |
                                     L2VPN_PWVC_NPAPI_CALLED);
                if (MplsFsMplsMbsmHwDeletePwVc
                    (u4VpnId, &gMplsHwVcInfo, u4Action,
                     pSlotInfo) == FNP_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_FNC_CRT_FLAG,
                                "FsMplsMbsmHwDeletePwVc: "
                                "PW %d failed in HW\n",
                                pPwVcEntry->u4PwVcIndex);
                    return L2VPN_FAILURE;
                }
                L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                                     L2VPN_PWVC_NPAPI_SUCCESS |
                                     L2VPN_PWAC_NPAPI_SUCCESS);
            }
            else
#endif
            {
#ifdef L2RED_WANTED
                if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                {
#ifdef NPAPI_WANTED
                    L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                                         L2VPN_PWAC_NPAPI_CALLED |
                                         L2VPN_PWVC_NPAPI_CALLED);
                    if (FNP_FAILURE !=
                        MplsFsMplsHwGetPwVc (&gMplsHwVcInfo,
                                             &gNextMplsHwVcInfo))
                    {
                        if (L2VPN_ZERO == gMplsHwVcInfo.FecInfo.u4VpnSrcVlan &&
                            L2VPN_ZERO == gMplsHwVcInfo.FecInfo.u4VpnSrcPhyPort)
                        {

                            gMplsHwVcInfo.u1VpwsPwHwAction |= PW_HW_PW_PRESENT;
                        }
                        else
                        {
                            gMplsHwVcInfo.u1VpwsPwHwAction |= PW_HW_VPN_PRESENT;
                        }
                        if (MplsFsMplsHwDeletePwVc
                            (u4VpnId, &gMplsHwVcInfo, u4Action) == FNP_FAILURE)
                        {
                            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                        "FsMplsHwDeletePwVc: "
                                        "PW %d failed in HW when passing "
                                        "HwAction = %x \n",
                                        pPwVcEntry->u4PwVcIndex,
                                        gMplsHwVcInfo.u1VpwsPwHwAction);
                            return L2VPN_FAILURE;
                        }
                    }
                    else if (L2VPN_PW_HW_LIST_NPAPI_STATUS (pL2VpnPwHwListEntry)
                             & L2VPN_PWAC_NPAPI_SUCCESS)
                    {
                        gMplsHwVcInfo.u1VpwsPwHwAction |= PW_HW_AC_PRESENT;
                        if (MplsFsMplsHwDeletePwVc
                            (u4VpnId, &gMplsHwVcInfo, u4Action) == FNP_FAILURE)
                        {
                            L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                        "FsMplsHwDeletePwVc: "
                                        "PW %d failed in HW when passing "
                                        "HwAction = %x \n",
                                        pPwVcEntry->u4PwVcIndex,
                                        gMplsHwVcInfo.u1VpwsPwHwAction);
                            return L2VPN_FAILURE;
                        }

                    }

                    L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                                         L2VPN_PWVC_NPAPI_SUCCESS |
                                         L2VPN_PWAC_NPAPI_SUCCESS);
#endif
                }
            }
            if (pPwVcEnetEntry != NULL)
            {
                MplsL2VpnRelAcId (pPwVcEnetEntry->u4PwAcId);
                pPwVcEnetEntry->u4PwAcId = L2VPN_ZERO;
            }
            u1HwStatus = (UINT1) ((pPwVcEntry->u1HwStatus & 0x0F) ^
                                  ((UINT1) (pPwVcEntry->u1HwStatus & 0xF0) >>
                                   MPLS_FOUR)) << MPLS_FOUR;
            L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ZERO);

            return L2VPN_SUCCESS;
        }
    }

    if (L2VpnCheckAndGetLastOperUpPw
        (L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry),
         &u4PwTmpVcIndex) == L2VPN_SUCCESS)
    {
        if (L2VPN_ZERO != L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry))
        {
#ifdef VPLS_GR_WANTED
            L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcEntry) =
                L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry);
            L2VPN_VPLSAC_INDEX (&VplsAcEntry) = L2VPN_ZERO;
#ifdef NPAPI_WANTED
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_CALLED);
#endif
            while ((pVplsAcEntry = (tVplsAcMapEntry *)
                    RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                                   &VplsAcEntry, NULL)) != NULL)
            {
                L2VPN_VPLSAC_INDEX (&VplsAcEntry) =
                    L2VPN_VPLSAC_INDEX (pVplsAcEntry);
                if (L2VPN_VPLSAC_VPLS_INSTANCE (pVplsAcEntry) !=
                    L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry))
                {
                    break;
                }
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan =
                    L2VPN_VPLSAC_VLAN_ID (pVplsAcEntry);
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort =
                    L2VPN_VPLSAC_PORT_INDEX (pVplsAcEntry);

                /** The changes are Hardware specific, 
                 *  commenting to remove Coverity warning 
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.i1PwEnetVlanMode =
                    L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry); 
                MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4PwEnetPwVlan = 
                    L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry); **/
                MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
#ifdef MBSM_WANTED
                if (pSlotInfo != NULL)
                {
                    if (FNP_FAILURE !=
                        MplsFsMplsMbsmHwGetVpnAc (L2VPN_VPLSAC_VPLS_INSTANCE
                                                  (&VplsAcEntry), u4VpnId,
                                                  &MplsHwVplsInfo, pSlotInfo))
                    {
                        if (MplsFsMplsMbsmHwDeleteVpnAc
                            (L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcEntry), u4VpnId,
                             &MplsHwVplsInfo, u4Action,
                             pSlotInfo) == FNP_FAILURE)
                        {
                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "%s : Ac Deletion failed for VplsInstance=%u\n",
                                        __func__,
                                        L2VPN_VPLSAC_VPLS_INSTANCE
                                        (&VplsAcEntry));
                        }
                    }
                }
                else
#endif
                {
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
#ifdef NPAPI_WANTED
                        if (FNP_FAILURE !=
                            MplsFsMplsHwGetVpnAc (L2VPN_VPLSAC_VPLS_INSTANCE
                                                  (&VplsAcEntry), u4VpnId,
                                                  &MplsHwVplsInfo))
                        {
                            if (MplsFsMplsHwDeleteVpnAc
                                (L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcEntry),
                                 u4VpnId, &MplsHwVplsInfo,
                                 u4Action) == FNP_FAILURE)
                            {
                                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                            "%s : Ac Deletion failed for VplsInstance=%u\n",
                                            __func__,
                                            L2VPN_VPLSAC_VPLS_INSTANCE
                                            (&VplsAcEntry));
                            }
                        }
#endif
                    }
                }
            }
#ifdef NPAPI_WANTED
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_SUCCESS);
#endif
#endif
        }
        else
        {
#ifdef NPAPI_WANTED
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWAC_NPAPI_CALLED);
#endif
            pPwVcEntry =
                L2VpnGetPwVcEntryFromIndex (L2VPN_PW_HW_LIST_PW_INDEX
                                            (pL2VpnPwHwListEntry));
            if (NULL == pPwVcEntry)
            {
                return L2VPN_SUCCESS;
            }
            if (pPwVcEntry->pServSpecEntry != NULL)
            {

                TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                              pPwVcEnetEntry, tPwVcEnetEntry *)
                {
                    if (pPwVcEnetEntry->u1HwStatus == MPLS_FALSE)
                    {
                        continue;
                    }
                    /* To check whether AC Port is Operationally up */
                    if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex)
                        == L2VPN_FAILURE)
                    {
                        continue;
                    }

                    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcVlan =
                        (UINT4) L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                    MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.u4VpnSrcPhyPort =
                        (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);

                    MplsHwVplsInfo.mplsHwVplsVcInfo.u4PwAcId =
                        pPwVcEnetEntry->u4PwAcId;
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
                        MplsHwVplsInfo.mplsHwVplsVcInfo.u1AppOwner =
                            L2VpPortGetPortOwnerFromIndex
                            (MplsHwVplsInfo.mplsHwVplsVcInfo.FecInfo.
                             u4AcIfIndex);
#ifdef NPAPI_WANTED

                        if (MplsFsMplsHwDeleteVpnAc
                            (L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry),
                             u4VpnId, &MplsHwVplsInfo, u4Action) == FNP_SUCCESS)
                        {
                            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "L2VpnPwVcHwVpnAcDel - AC of PW %d with "
                                        "Port Vlan %d Port If Index %d "
                                        "removed from HW\n",
                                        pPwVcEnetEntry->pPwVcEntry->u4PwVcIndex,
                                        L2VPN_PWVC_ENET_PORT_IF_INDEX
                                        (pPwVcEnetEntry),
                                        L2VPN_PWVC_ENET_PORT_VLAN
                                        (pPwVcEnetEntry));
                        }
#endif
                    }
                    if (pPwVcEnetEntry->u4PwAcId != L2VPN_ZERO)
                    {
                        MplsL2VpnRelAcId (pPwVcEnetEntry->u4PwAcId);
                        pPwVcEnetEntry->u4PwAcId = L2VPN_ZERO;
                    }
                    L2VpnUtilSetEnetHwStatus (L2VPN_PWVC_ENET_PORT_IF_INDEX
                                              (pPwVcEnetEntry),
                                              L2VPN_PWVC_ENET_PORT_VLAN
                                              (pPwVcEnetEntry), MPLS_FALSE,
                                              pPwVcEnetEntry->u4PwAcId);

                }
#ifdef NPAPI_WANTED
                L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                                     L2VPN_PWAC_NPAPI_CALLED);
#endif
            }
        }
    }
#ifdef NPAPI_WANTED
    else
    {
        L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                             L2VPN_PWAC_NPAPI_SUCCESS |
                             L2VPN_PWAC_NPAPI_CALLED);
    }
#endif
    /* memcpy vcinfo to vpls info */
    MEMCPY (&MplsHwVplsInfo.mplsHwVplsVcInfo, &gMplsHwVcInfo,
            sizeof (tMplsHwVcTnlInfo));
    MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWVC_NPAPI_CALLED);
        if (FNP_FAILURE !=
            MplsFsMplsMbsmHwGetPwVc (&gMplsHwVcInfo, &gNextMplsHwVcInfo,
                                     pSlotInfo))
        {
            if (MplsFsMplsMbsmHwVplsDeletePwVc
                (L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry), u4VpnId,
                 &MplsHwVplsInfo, u4Action, pSlotInfo) == FNP_FAILURE)
            {
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "%s : PW %d failed in HW\n", __func__,
                            L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry));
                return L2VPN_FAILURE;
            }
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWVC_NPAPI_SUCCESS);
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWVC_NPAPI_CALLED);
            if (FNP_FAILURE !=
                MplsFsMplsHwGetPwVc (&gMplsHwVcInfo, &gNextMplsHwVcInfo))
            {
                if (MplsFsMplsHwVplsDeletePwVc
                    (L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry), u4VpnId,
                     &MplsHwVplsInfo, u4Action) == FNP_FAILURE)
                {
                    L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                "%s : PW %d failed in HW\n", __func__,
                                L2VPN_PW_HW_LIST_PW_INDEX
                                (pL2VpnPwHwListEntry));
                    return L2VPN_FAILURE;
                }
            }
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWVC_NPAPI_SUCCESS);
#endif
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;

}

/*****************************************************************************/
/* Function Name : L2VpnDeleteBgpPwIlm                                         */
/* Description   : This function Deletes the PWVc from hardware                 */
/* Input(s)      : pL2VpnPwHwListEntry - HW List Entry                       */
/* Output(s)     : None                                                         */
/* Return(s)     : L2VPN_FAILURE / L2VPN_SUCCESS                             */
/*****************************************************************************/
INT4
L2VpnDelStalePwIlm (tL2VpnPwHwList * pL2VpnPwHwListEntry, VOID *pSlotInfo)
{
    tMplsHwVplsInfo     MplsHwVplsInfo;
    tMplsHwIlmInfo      MplsHwIlmInfo;
#ifdef NPAPI_WANTED
    tMplsHwIlmInfo      NextMplsHwIlmInfo;
#endif
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = 0;
    UINT4               u4Action;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    u4Action = L2VPN_ZERO;
    /* Init Npapi related data structures */
    MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (L2VPN_PW_HW_LIST_VPLS_INDEX
                                            (pL2VpnPwHwListEntry));
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : VPLS Index = %u not exists\n", __func__,
                    L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry));
        return L2VPN_FAILURE;
    }
    /* Fill up the VPLS info */
    MEMCPY ((UINT1 *) &u4VpnId, &(pVplsEntry->au1VplsVpnID[3]), sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);

    /* Terminate this ILM Entry and do L2 switching for L2 VPN */
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_L2_SWITCH;
    MplsHwIlmInfo.OutLabelToLearn.u.MplsShimLabel =
        L2VPN_PW_HW_LIST_OUT_LABEL (pL2VpnPwHwListEntry);
    MplsHwIlmInfo.u4InL3Intf =
        L2VPN_PW_HW_LIST_IN_IF_INDEX (pL2VpnPwHwListEntry);
    if (L2VPN_ZERO == L2VPN_PW_HW_LIST_LSP_IN_LABEL (pL2VpnPwHwListEntry))
    {
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            L2VPN_PW_HW_LIST_IN_LABEL (pL2VpnPwHwListEntry);
    }
    else
    {
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel =
            L2VPN_PW_HW_LIST_LSP_IN_LABEL (pL2VpnPwHwListEntry);
        MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel =
            L2VPN_PW_HW_LIST_IN_LABEL (pL2VpnPwHwListEntry);
    }
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L2_SWITCH;
        MplsHwIlmInfo.u4VpnId = u4VpnId;
        L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWILM_NPAPI_CALLED);
        if (FNP_FAILURE !=
            MplsFsMplsMbsmHwGetILM (&MplsHwIlmInfo, &NextMplsHwIlmInfo,
                                    pSlotInfo))
        {
            if (MplsFsMplsMbsmHwDeleteILM (&MplsHwIlmInfo, u4Action, pSlotInfo)
                == FNP_FAILURE)
            {
                L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                            "%s :  PW %d failed in HW\n", __func__,
                            L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry));
                return L2VPN_FAILURE;
            }
        }
        L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWILM_NPAPI_SUCCESS);
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L2_SWITCH;
            MplsHwIlmInfo.u4VpnId = u4VpnId;
#ifdef NPAPI_WANTED
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry, L2VPN_PWILM_NPAPI_CALLED);
            if (FNP_FAILURE !=
                MplsFsMplsHwGetILM (&MplsHwIlmInfo, &NextMplsHwIlmInfo))
            {
                if (MplsFsMplsHwDeleteILM (&MplsHwIlmInfo, u4Action) ==
                    FNP_FAILURE)
                {
                    L2VPN_DBG2 (L2VPN_DBG_FNC_CRT_FLAG,
                                "%s :  PW %d failed in HW\n", __func__,
                                L2VPN_PW_HW_LIST_PW_INDEX
                                (pL2VpnPwHwListEntry));
                    return L2VPN_FAILURE;
                }
            }
            L2VpnPwHwListDelete (pL2VpnPwHwListEntry,
                                 L2VPN_PWILM_NPAPI_SUCCESS);
#endif
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "Action performed : %d \r\n", u4Action);
    return L2VPN_SUCCESS;

}

#endif
