/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpnsz.c,v 1.5 2014/12/24 10:58:28 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _L2VPNSZ_C
#include "l2vpincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
L2vpnSizingMemCreateMemPools ()
{
    UINT4                u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < L2VPN_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal =
            MemCreateMemPool (FsL2VPNSizingParams[i4SizingId].u4StructSize,
                              FsL2VPNSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(L2VPNMemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            L2vpnSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
L2vpnSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsL2VPNSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, L2VPNMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
L2vpnSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < L2VPN_MAX_SIZING_ID; i4SizingId++)
    {
        if (L2VPNMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (L2VPNMemPoolIds[i4SizingId]);
            L2VPNMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
