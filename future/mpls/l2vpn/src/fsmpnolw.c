/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpnolw.c,v 1.5 2014/12/24 10:58:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "l2vpincs.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsPwStatusNotifEnable
 Input       :  The Indices

                The Object 
                retValFsMplsPwStatusNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsPwStatusNotifEnable (INT4 *pi4RetValFsMplsPwStatusNotifEnable)
{
    *pi4RetValFsMplsPwStatusNotifEnable = gL2VpnGlobalInfo.b1PwStatusNotif;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsPwOAMStatusNotifEnable
 Input       :  The Indices

                The Object 
                retValFsMplsPwOAMStatusNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsPwOAMStatusNotifEnable (INT4 *pi4RetValFsMplsPwOAMStatusNotifEnable)
{
    *pi4RetValFsMplsPwOAMStatusNotifEnable =
        gL2VpnGlobalInfo.b1PwOamStatusNotif;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsPwStatusNotifEnable
 Input       :  The Indices

                The Object 
                setValFsMplsPwStatusNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsPwStatusNotifEnable (INT4 i4SetValFsMplsPwStatusNotifEnable)
{
    gL2VpnGlobalInfo.b1PwStatusNotif =
        (UINT1) i4SetValFsMplsPwStatusNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsPwOAMStatusNotifEnable
 Input       :  The Indices

                The Object 
                setValFsMplsPwOAMStatusNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsPwOAMStatusNotifEnable (INT4 i4SetValFsMplsPwOAMStatusNotifEnable)
{
    gL2VpnGlobalInfo.b1PwOamStatusNotif =
        (BOOL1) i4SetValFsMplsPwOAMStatusNotifEnable;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsPwStatusNotifEnable
 Input       :  The Indices

                The Object 
                testValFsMplsPwStatusNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsPwStatusNotifEnable (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsMplsPwStatusNotifEnable)
{
    if ((i4TestValFsMplsPwStatusNotifEnable != L2VPN_SNMP_TRUE) &&
        (i4TestValFsMplsPwStatusNotifEnable != L2VPN_SNMP_FALSE))
    {
        /* The variable cannot take this value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsPwOAMStatusNotifEnable
 Input       :  The Indices

                The Object 
                testValFsMplsPwOAMStatusNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsPwOAMStatusNotifEnable (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsMplsPwOAMStatusNotifEnable)
{
    if ((i4TestValFsMplsPwOAMStatusNotifEnable != L2VPN_SNMP_TRUE) &&
        (i4TestValFsMplsPwOAMStatusNotifEnable != L2VPN_SNMP_FALSE))
    {
        /* The variable cannot take this value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsPwStatusNotifEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsPwStatusNotifEnable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsPwOAMStatusNotifEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsPwOAMStatusNotifEnable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
