/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpldp.c,v 1.85 2017/06/08 11:40:30 siva Exp $
 *
 * Description: This file contains the routines to process the events
 *              from the LDP module.
 ********************************************************************/

#include "l2vpincs.h"
#include "ldpext.h"
extern INT4         gi4MplsSimulateFailure;
#ifndef NPAPI_WANTED
extern VOID         VlanVplsDelPwFdbEntries (UINT4 u4PwVcIndex);
#endif
/*****************************************************************************/
/* Function Name : L2VpnProcessLdpNotifEvent                                 */
/* Description   : This routine processes Notification Event                 */
/*                 from LDP module                                           */
/* Input(s)      : pNotifInfo - ptr to Notification message                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpNotifEvent (tPwVcNotifEvtInfo * pNotifInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcDormantVcEntry *pDormantVc = NULL;
    tTMO_SLL_NODE      *pDormantVcPrevNode = NULL;
    tTMO_SLL_NODE      *pDormantVcNextNode = NULL;
    tTMO_SLL_NODE      *pDormantVcCurrNode = NULL;

    UINT1               u1MsgCode;
    INT4                i4Status = L2VPN_SUCCESS;
    BOOL1               bInsertFlag = FALSE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    u1MsgCode = pNotifInfo->u1MsgCode;

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "Recv Notification Event from the LDP with Msg code: %x \r\n",
                u1MsgCode);

    MEMCPY (&(GenU4Addr.Addr), &(pNotifInfo->PeerAddr), sizeof (uGenU4Addr));

    GenU4Addr.u2AddrType = (UINT2) pNotifInfo->u2AddrType;

    if ((u1MsgCode == L2VPN_LDP_PWVC_UP) ||
        (u1MsgCode == L2VPN_LDP_PWVC_DOWN) ||
        (u1MsgCode == L2VPN_LDP_RSRC_UNAVAILABLE))
    {
        /* Get PwVcEntry from Hash Table */
        pPwVcEntry = L2VpnGetPwVcEntry (pNotifInfo->i1PwOwner,
                                        pNotifInfo->u4VcID,
                                        pNotifInfo->u1VcType,
                                        pNotifInfo->au1Agi,
                                        pNotifInfo->au1Saii,
                                        pNotifInfo->au1Taii, GenU4Addr);

        /* if entry not found, Log the Error */
        if (pPwVcEntry == NULL)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PWVC Entry Not found - %x\r\n", u1MsgCode);
            return L2VPN_FAILURE;
        }
    }

    switch (u1MsgCode)
    {
        case L2VPN_LDP_PWVC_UP:

            /* update the PwRec with the label sent to the peer */
            pPwVcEntry->u1MapStatus &= (UINT1) (~L2VPN_REL_WAIT);
            L2VpnPwSetInVcLabel (pPwVcEntry, pNotifInfo->u4Label);
            pPwVcEntry->u4InIfIndex = pNotifInfo->u4IfIndex;

            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_NOT_FORWARDING);

            i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);

            /* Update Pw Oper status */
            i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                            pPwVcEntry->u1LocalStatus,
                            pPwVcEntry->u1RemoteStatus);
            }

            break;

        case L2VPN_LDP_PWVC_DOWN:

            /* Mask the Map sent */
            pPwVcEntry->u1MapStatus &= L2VPN_LOWNIB;
            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                L2VPN_PWVC_STATUS_NOT_FORWARDING;

            /* Update Pw Oper status */
            i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                            pPwVcEntry->u1LocalStatus,
                            pPwVcEntry->u1RemoteStatus);
            }

            /* update the PwRec with the default label value */
            L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);

            break;

        case L2VPN_LDP_RSRC_UNAVAILABLE:
            /* update oper status */
            pPwVcEntry->i1OperStatus = L2VPN_PWVC_OPER_DORMANT;
            pPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_NOT_FORWARDING;

            /* Add the PW VC in the dormant VCs list with */
            /* increasing setup priority                  */

            pDormantVc = (tPwVcDormantVcEntry *)
                MemAllocMemBlk (L2VPN_DORMANT_VCS_POOL_ID);

            if (pDormantVc == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Mem allocation failure - Dormant Vc\r\n");
                return L2VPN_FAILURE;
            }

            pDormantVc->pPwVcEntry = pPwVcEntry;
            pDormantVcCurrNode = &(pDormantVc->NextNode);

            pDormantVcPrevNode = TMO_SLL_First (L2VPN_DORMANT_PWVC_LIST
                                                (gpPwVcGlobalInfo));

            if (pDormantVcPrevNode == NULL)
            {
                TMO_SLL_Add (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo),
                             pDormantVcCurrNode);
                break;
            }

            TMO_SLL_Scan (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo),
                          pDormantVcNextNode, tTMO_SLL_NODE *)
            {
                if (pPwVcEntry->i1SetUpPriority >
                    ((tPwVcDormantVcEntry *) pDormantVcNextNode)->pPwVcEntry->
                    i1SetUpPriority)
                {
                    bInsertFlag = TRUE;
                    TMO_SLL_Insert_In_Middle
                        (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo),
                         pDormantVcPrevNode,
                         pDormantVcCurrNode, pDormantVcNextNode);
                    break;
                }
                pDormantVcPrevNode = pDormantVcNextNode;
            }

            /* If the node is not inserted before insert it in the end. */
            if (bInsertFlag == FALSE)
            {
                TMO_SLL_Add (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo),
                             pDormantVcCurrNode);
            }

            break;

        case L2VPN_LDP_RSRC_AVAILABLE:
            /* Get the PW VC in the dormant VCs list with high setup priority */
            pDormantVc = (tPwVcDormantVcEntry *) TMO_SLL_First
                (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo));

            if (pDormantVc == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Dormant VC list not available\r\n");
                break;
            }

            pPwVcEntry = pDormantVc->pPwVcEntry;

            /* Change Oper status */
            pPwVcEntry->i1OperStatus = L2VPN_PWVC_OPER_DOWN;
            pPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_NOT_FORWARDING;

            i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);
            break;

        case L2VPN_LDP_PEER_NO_PW_SUPPORT:
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "No PW support @ Peer IPv4 - %x IPv6 - %s\r\n",
                        GenU4Addr.Addr.u4Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "No PW support @ peer %x\r\n", GenU4Addr.Addr.u4Addr);
#endif
            i4Status = L2VPN_FAILURE;
            break;

        case L2VPN_LDP_NOTIFICATION_MSG:
            i4Status = L2VpnProcessLdpNotifMsg (pNotifInfo);
            break;

        default:
            break;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpSsnUpEvent                                 */
/* Description   : This routine processes session up event from LDP          */
/* Input(s)      : pSsnInfo - pointer to session info                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpSsnUpEvent (tPwVcSsnEvtInfo * pSsnInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    VOID               *pPsnEntry = NULL;
    INT4                i4Status;
    VOID               *pDllNode = NULL;
    tPwVcActivePeerSsnEntry PeerSsn;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyNodeEntry *pIccpNode = NULL;
    UINT1               u1Event;

    L2VPN_DBG (L2VPN_DBG_DBG_SSN_FLAG, "L2VpnProcessLdpSsnUpEvent\r\n");

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pSsnInfo->u2AddrType)
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pSsnInfo->u2AddrType)
#endif
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                &(pSsnInfo->PeerAddr.u4Addr), IPV4_ADDR_LENGTH);
    }
    PeerSsn.u1AddrType = (UINT1) pSsnInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);
    if (pPeerSsn == NULL)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                    "Session list with Peer IPv4 - %d.%d.%d.%d IPv6 - %s is not available\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                    "Session list with Peer %d.%d.%d.%d is not available\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
        return L2VPN_FAILURE;
    }

    pPwVcEntry = (tPwVcEntry *) TMO_DLL_First (L2VPN_PWVC_LIST (pPeerSsn));
    pDllNode =
        (tL2vpnRedundancyNodeEntry *)
        TMO_DLL_First (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn));
    if (pDllNode != NULL)
    {
        pIccpNode =
            CONTAINER_OF (pDllNode, tL2vpnRedundancyNodeEntry, SessionNode);
    }

    if ((pPwVcEntry == NULL) &&
        (TMO_DLL_Count (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn)) == 0))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                    "No PW's or ICCP Nodes associated with Peer IPv4 - %d.%d.%d.%d IPv6 - %s\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                    "No PW's or ICCP Nodes associated with Peer %d.%d.%d.%d\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
        return L2VPN_FAILURE;
    }

    if (pPeerSsn->u1PeerSsnLdpRegStatus == L2VPN_FALSE)
    {
        /* If the registration with the LDP Entity is not done earlier,
         * do it now. */
        if (((pPwVcEntry != NULL) &&
             (L2VPN_SESSION_PWVC_REG_REQ (pPwVcEntry) == FALSE)) ||
            ((pIccpNode != NULL) &&
             (L2VPN_SESSION_ICCP_REG_REQ (pIccpNode) == FALSE)))
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                        "Message Posting to register with LDP Entity "
                        "for the peer IPv4 - %d.%d.%d.%d IPv6 -%s failed\n",
                        PeerSsn.PeerAddr.au1Ipv4Addr[0],
                        PeerSsn.PeerAddr.au1Ipv4Addr[1],
                        PeerSsn.PeerAddr.au1Ipv4Addr[2],
                        PeerSsn.PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
            L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                        "Message Posting to register with LDP Entity "
                        "for the peer %d.%d.%d.%d failed\n",
                        PeerSsn.PeerAddr.au1Ipv4Addr[0],
                        PeerSsn.PeerAddr.au1Ipv4Addr[1],
                        PeerSsn.PeerAddr.au1Ipv4Addr[2],
                        PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
        }
        pPeerSsn->u1PeerSsnLdpRegStatus = L2VPN_TRUE;
    }
    else
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                    "Peer IPv4 %d.%d.%d.%d IPv6 %s is"
                    "already registered with LDP Entity\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                    "Peer %d.%d.%d.%d is already registered with LDP Entity\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
    }

    if (pPeerSsn->u4Status == L2VPN_SESSION_UP)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                    "LDP Session with Peer %d.%d.%d.%d is already UP\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else

        L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                    "LDP Session with Peer %d.%d.%d.%d is already UP\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
        return L2VPN_SUCCESS;
    }

    pPeerSsn->u4Status = L2VPN_SESSION_UP;
    pPeerSsn->u4LocalLdpEntityID = pSsnInfo->u4LocalLdpEntityID;
    pPeerSsn->u4LdpEntityIndex = pSsnInfo->u4LdpEntityIndex;
    pPeerSsn->u4PeerLdpId = pSsnInfo->u4PeerLdpId;
    pPeerSsn->b1IsActive = pSsnInfo->b1IsActive;

    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        pPsnEntry = pPwVcEntry->pPSNEntry;
        if (pPsnEntry != NULL)
        {
            pPwVcMplsEntry = (tPwVcMplsEntry *) pPsnEntry;
            pPwVcMplsEntry->u4LocalLdpEntityIndex = pPeerSsn->u4LdpEntityIndex;

            L2VPN_DBG2 (L2VPN_DBG_DBG_SSN_FLAG,
                        "Entity Index of PW %d is %d\n",
                        pPwVcEntry->u4PwVcIndex,
                        pPwVcMplsEntry->u4LocalLdpEntityIndex);
        }

        if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
             L2VPN_PWVC_STATUS_NOT_FORWARDING) !=
            L2VPN_PWVC_STATUS_NOT_FORWARDING)
        {
            continue;
        }

        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Outbound list updation failed\r\n");
            continue;
        }

        i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Failed to send Lbl Map Msg\r\n");
            continue;
        }

        /* Update RG ICCP PW entry */
        pRgPw =
            L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX (pPwVcEntry));
        if (pRgPw != NULL)
        {
            if (L2vpnGetIccpFrmSsnInfo (pRgPw->u4RgIndex, pPeerSsn,
                                        pPwVcEntry) == L2VPN_FAILURE)
            {
                u1Event = (UINT1) L2VPNRED_EVENT_PW_ACTIVATE;
            }
            else
            {
                u1Event = (UINT1) L2VPNRED_EVENT_PW_STATE_CHANGE;
            }

            L2vpnRedundancyPwStateUpdate (pRgPw, u1Event);
        }

        /* Update Pw Oper status */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
        }
        pPwVcEntry->u1GrSyncStatus = L2VPN_GR_FULLY_SYNCHRONIZED;
    }

    TMO_DLL_Scan (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn), pDllNode,
                  tL2vpnRedundancyNodeEntry *)
    {
        pIccpNode =
            CONTAINER_OF (pDllNode, tL2vpnRedundancyNodeEntry, SessionNode);
        L2vpnRedundancyNodeStateUpdate (pIccpNode,
                                        L2VPNRED_EVENT_NODE_SESSION_UP);
    }
    pPeerSsn->u1RecoveryProgress = L2VPN_GR_NOT_STARTED;
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG6 (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessLdpSsnUpEvent: "
                "LDP Session with Peer IPv4 %d.%d.%d.%d IPv6 %s is set to "
                "%d State\r\n",
                pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)),
                pPeerSsn->u1RecoveryProgress);
#else
    L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessLdpSsnUpEvent: "
                "LDP Session with Peer %d.%d.%d.%d is set to "
                "%d State\r\n",
                pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                pPeerSsn->u1RecoveryProgress);
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpSsnDownEvent                               */
/* Description   : This routine processes session down event from LDP        */
/* Input(s)      : pSsnInfo - pointer to session info                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpSsnDownEvent (tPwVcSsnEvtInfo * pSsnInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcEntry         *pPwVcEntry = NULL;
    INT4                i4Status;
    UINT4               u4PwCount = 0;
    VOID               *pDllNode = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyNodeEntry *pIccpNode = NULL;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    L2VPN_DBG ((L2VPN_DBG_DBG_SSN_FLAG | L2VPN_DBG_GRACEFUL_RESTART),
               "L2VpnProcessLdpSsnDownEvent\r\n");

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pSsnInfo->u2AddrType)
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
        MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pSsnInfo->u2AddrType)
#endif
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                &(pSsnInfo->PeerAddr.u4Addr), IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = pSsnInfo->PeerAddr.u4Addr;
    }
    PeerSsn.u1AddrType = (UINT1) pSsnInfo->u2AddrType;
    GenU4Addr.u2AddrType = pSsnInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);
    if (pPeerSsn == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_SSN_FLAG,
                   "Updating of Peer session list failed\r\n");
        return L2VPN_FAILURE;
    }

    if (pPeerSsn->u4Status == L2VPN_SESSION_DOWN)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                    "LDP Session with Peer IPv4 %d.%d.%d.%d"
                    " IPv6 -%s is already DOWN\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                    "LDP Session with Peer %d.%d.%d.%d is already DOWN\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
        return L2VPN_SUCCESS;
    }

    u4PwCount = TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn));
    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        u4PwCount--;

        if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
             L2VPN_PWVC_STATUS_NOT_FORWARDING) !=
            L2VPN_PWVC_STATUS_NOT_FORWARDING)
        {

            /* Send ldp notif event to release label */
            /* TODO Label distributed to Peer should be return to Label 
             * Pool here itself */
            L2VpnSendLdpPwVcNotifEvent (pPwVcEntry);
        }

        /* TODO - pwOutBoundLabel should be released if MplsType = PwOnly */
        /*pPwVcEntry->u4OutVcLabel = L2VPN_INVALID_LABEL; */

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_PSN_BITMASK);

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;
        /* Since the LDP session itself is down, 
         * reset the PSN related  faults. */
        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_PSN_BITMASK);

        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) = L2VPN_ZERO;

        L2VPN_PWVC_LCL_CC_SELECTED (pPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_LCL_CV_SELECTED (pPwVcEntry) = L2VPN_ZERO;

        pPwVcEntry->u1MapStatus = L2VPN_ZERO;

        /* Update Pw Oper status */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
            /* return L2VPN_FAILURE; Need to be check at loaded condition */
        }

        /* Update RG ICCP PW entry */
        pRgPw =
            L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX (pPwVcEntry));
        if (pRgPw != NULL)
        {
            L2vpnRedundancyPwStateUpdate (pRgPw,
                                          L2VPNRED_EVENT_PW_STATE_CHANGE);
        }

        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Outbound list updation failed\r\n");
        }

        /* update the PwRec with the default label value */

        if (pPwVcEntry->bIsStaticPw != TRUE)
        {
            L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
            L2VpnPwSetOutVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
        }

        if (u4PwCount == L2VPN_ZERO)
        {
            if (L2VPN_SESSION_PWVC_DEREG_REQ (pPwVcEntry) == FALSE)
            {
                L2VPN_DBG (DBG_ERR_NCRT, "Deregistration with LDP is failed.");
            }
            pPeerSsn->u1PeerSsnLdpRegStatus = L2VPN_FALSE;
            pPeerSsn->i1SsnCreateReq = L2VPN_FALSE;
        }
    }

    /* Remove all the Label Advertisement Messages learned through
     * the peer since the session with the peer has gone down. */
    if ((pPeerSsn = L2VpnUpdatePeerSsnList (GenU4Addr,
                                            L2VPN_SSN_DELETE, NULL)) != NULL)
    {
        TMO_DLL_Scan (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn), pDllNode,
                      tL2vpnRedundancyNodeEntry *)
        {
            pIccpNode =
                CONTAINER_OF (pDllNode, tL2vpnRedundancyNodeEntry, SessionNode);
            L2vpnRedundancyNodeStateUpdate (pIccpNode,
                                            L2VPNRED_EVENT_NODE_SESSION_DOWN);
        }
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_DBG_SSN_FLAG,
                "LDP Session with Peer IPv4-%d.%d.%d.%d IPv6 %s is made DOWN\n",
                PeerSsn.PeerAddr.au1Ipv4Addr[0],
                PeerSsn.PeerAddr.au1Ipv4Addr[1],
                PeerSsn.PeerAddr.au1Ipv4Addr[2],
                PeerSsn.PeerAddr.au1Ipv4Addr[3],
                Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
    L2VPN_DBG4 (L2VPN_DBG_DBG_SSN_FLAG,
                "LDP Session with Peer %d.%d.%d.%d is made DOWN\n",
                PeerSsn.PeerAddr.au1Ipv4Addr[0],
                PeerSsn.PeerAddr.au1Ipv4Addr[1],
                PeerSsn.PeerAddr.au1Ipv4Addr[2],
                PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpLblWcWdrawMsg                              */
/* Description   : This routine processes Label wcard with msg from LDP      */
/* Input(s)      : pLblInfo - pointer to Label message info                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpLblWcWdrawMsg (tPwVcLblMsgInfo * pLblInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcEntry         *pPwVcEntry = NULL;

    /* We got wildcard withdraw from a particular Peer so,
     * get the List of PwVcEntries associated with that Peer and
     * scan through the for GroupID match */

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pLblInfo->u2AddrType)
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                pLblInfo->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pLblInfo->u2AddrType)
#endif
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                &(pLblInfo->PeerAddr.u4Addr), IPV4_ADDR_LENGTH);
    }
    PeerSsn.u1AddrType = (UINT1) pLblInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);

    if (pPeerSsn == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Peer Session is NULL\r\n");
        return L2VPN_FAILURE;
    }
    /* We are not checking status of the PeerSession because WcWdraw 
     * will come  only If session is in UP state */
    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        if (L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry) == pLblInfo->u4GroupID)
        {
            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                L2VPN_PWVC_STATUS_NOT_FORWARDING;
            L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
                L2VPN_PWVC_STATUS_NOT_FORWARDING;

            /* If the node receives withdraw message without Label TLV, or 
             * with correct label, it has to remove the label.
             * If the received label is incorrect, then no need to
             * remove the label
             * */
            if ((pLblInfo->u4Label != L2VPN_INVALID_LABEL) &&
                (pLblInfo->u4Label != pPwVcEntry->u4OutVcLabel))
            {

                L2VPN_DBG (L2VPN_DBG_ERR_SIG_FLAG,
                           "Label mismatch in received Lbl withdraw Message\r\n");
                return L2VPN_SUCCESS;
            }

            /* Update Pw Oper status */
            if (L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN)
                != L2VPN_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                            pPwVcEntry->u1LocalStatus,
                            pPwVcEntry->u1RemoteStatus);
            }

            /* update out-bound list */
            if (L2VpnUpdatePwVcOutboundList (pPwVcEntry,
                                             L2VPN_PWVC_DOWN) != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Outbound list update error\r\n");
            }

            /* update the PwRec with the default label value */
            L2VpnPwSetOutVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpLblRelMsg                                  */
/* Description   : This routine processes Label release message from LDP     */
/* Input(s)      : pLblInfo - pointer to Label message info                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpLblRelMsg (tPwVcLblMsgInfo * pLblInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label release msg from IPv4- %d.%d.%d.%d IPv6 %s \r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)),
                Ip6PrintAddr (&(pLblInfo->PeerAddr.Ip6Addr)));
#else

    L2VPN_DBG4 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label release msg from %d.%d.%d.%d\r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)));
#endif
    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                pLblInfo->i1PwType,
                pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                pLblInfo->i1StatusCode, pLblInfo->u4Label);

    MEMCPY (&(GenU4Addr.Addr), &(pLblInfo->PeerAddr), sizeof (uGenU4Addr));

    GenU4Addr.u2AddrType = (UINT2) pLblInfo->u2AddrType;

    pPwVcEntry = L2VpnGetPwVcEntry (pLblInfo->i1PwOwner,
                                    pLblInfo->u4PwVcID,
                                    (UINT1) pLblInfo->i1PwType,
                                    pLblInfo->au1Agi, pLblInfo->au1Taii,
                                    pLblInfo->au1Saii, GenU4Addr);
    if (pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    pLblInfo->u4PwVcID, pLblInfo->i1PwType,
                    pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                    pLblInfo->i1StatusCode, pLblInfo->u4Label);

    }

    if (pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    pLblInfo->u4PwVcID, pLblInfo->i1PwType,
                    pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                    pLblInfo->i1StatusCode, pLblInfo->u4Label);

    }

    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_SIG_FLAG,
                   "Failed to find matching PwVc Local Info\r\n");
        return i4Status;
    }
    if (pLblInfo->u4Label != L2VPN_INVALID_LABEL)
    {
        /* Optional label TLV present in the release message.
         * In addition to VCID and VC type matching
         * Label matching is also done.
         * If not matched withdraw message is ignored.
         */
        if ((pLblInfo->u4Label != pPwVcEntry->u4InVcLabel) ||
            ((pPwVcEntry->u1MapStatus & L2VPN_REL_WAIT) == L2VPN_REL_WAIT))
        {

            L2VPN_DBG (L2VPN_DBG_ERR_SIG_FLAG,
                       "Label mismatch in received Lbl Rel Message\r\n");
            L2VpnHandlePendingMsgsToLdp ();
            pPwVcEntry->u4PrevInVcLabel = pLblInfo->u4Label;
            L2VpnSendLdpPwVcNotifEvent (pPwVcEntry);
            pPwVcEntry->u4PrevInVcLabel = L2VPN_INVALID_LABEL;
            return i4Status;
        }
    }

    /* Send ldp notif to release label */
    L2VpnSendLdpPwVcNotifEvent (pPwVcEntry);

    L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &=
        (UINT1) (~L2VPN_PWVC_STATUS_PSN_BITMASK);

    /*Only LOCAL_STATUS should be marked NOT_FORWARDING when Label Release is recieved */
    L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |= L2VPN_PWVC_STATUS_NOT_FORWARDING;

    L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) = L2VPN_ZERO;
    L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) = L2VPN_ZERO;

    L2VPN_PWVC_LCL_CC_SELECTED (pPwVcEntry) = L2VPN_ZERO;
    L2VPN_PWVC_LCL_CV_SELECTED (pPwVcEntry) = L2VPN_ZERO;

    /* Update Pw Oper status */
    i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
    }

    pPwVcEntry->u1MapStatus &= (UINT1) (~(L2VPN_MAP_SENT));
    /* update the PwRec with the default label value */
    L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
    L2VpnHandlePendingMsgsToLdp ();

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpLblWdrawMsg                                */
/* Description   : This routine processes Label withdraw message from LDP    */
/* Input(s)      : pLblInfo - pointer to Label message info                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpLblWdrawMsg (tPwVcLblMsgInfo * pLblInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcLblMsgEntry   *pLblEntry = NULL;
    tPwVcLblMsgInfo    *pLblMsgInfo = NULL;
    INT4                i4Status;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label withdraw msg from IPv4- %d.%d.%d.%d IPv6 %s\r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)),
                Ip6PrintAddr (&(pLblInfo->PeerAddr.Ip6Addr)));
#else
    L2VPN_DBG4 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label withdraw msg from %d.%d.%d.%d\r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)));
#endif
    if (pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    pLblInfo->u4PwVcID, pLblInfo->i1PwType,
                    pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                    pLblInfo->i1StatusCode, pLblInfo->u4Label);
    }
    else if (pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    pLblInfo->i1PwType,
                    pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                    pLblInfo->i1StatusCode, pLblInfo->u4Label);

    }
    MEMCPY (&(GenU4Addr.Addr), &(pLblInfo->PeerAddr), sizeof (uGenU4Addr));

    GenU4Addr.u2AddrType = (UINT2) pLblInfo->u2AddrType;

    pPwVcEntry = L2VpnGetPwVcEntry (pLblInfo->i1PwOwner,
                                    pLblInfo->u4PwVcID,
                                    (UINT1) pLblInfo->i1PwType,
                                    pLblInfo->au1Agi, pLblInfo->au1Taii,
                                    pLblInfo->au1Saii, GenU4Addr);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG2 (L2VPN_DBG_ERR_VC_FLAG,
                    "Failed to find the matching PwVc Entry "
                    "VCID = %d, VCType = %d\r\n", pLblInfo->u4PwVcID,
                    pLblInfo->i1PwType);
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pLblInfo->u2AddrType)
        {
            MEMCPY (&(PeerSsn.PeerAddr),
                    pLblInfo->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pLblInfo->u2AddrType)
#endif
        {
            MEMCPY (&(PeerSsn.PeerAddr),
                    &(pLblInfo->PeerAddr.u4Addr), IPV4_ADDR_LENGTH);
        }
        PeerSsn.u1AddrType = (UINT1) pLblInfo->u2AddrType;

        if (((pPeerSsn =
              RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                         &PeerSsn)) != NULL)
            && (pPeerSsn->u4Status == L2VPN_SESSION_UP))
        {
            i4Status = L2VPN_SUCCESS;
            TMO_DLL_Scan (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                          pLblEntry, tPwVcLblMsgEntry *)
            {
                pLblMsgInfo = (tPwVcLblMsgInfo *) & (pLblEntry->LblInfo);
                if (((pLblMsgInfo->u4PwVcID == pLblInfo->u4PwVcID) ||
                     (MEMCMP (pLblMsgInfo->au1Agi, pLblInfo->au1Agi,
                              pLblMsgInfo->u1AgiLen) == L2VPN_ZERO) ||
                     ((MEMCMP (pLblMsgInfo->au1Saii, pLblInfo->au1Saii,
                               pLblMsgInfo->u1SaiiLen) == L2VPN_ZERO) &&
                      (MEMCMP (pLblMsgInfo->au1Taii, pLblInfo->au1Taii,
                               pLblMsgInfo->u1TaiiLen) == L2VPN_ZERO))) &&
                    (pLblMsgInfo->i1PwType == pLblInfo->i1PwType))
                {
                    /* Stop Clean up  Timer */
                    TmrStopTimer (L2VPN_TIMER_LIST_ID,
                                  &(pLblEntry->CleanupTimer.AppTimer));
                    L2VpnReleaseWaitingRemLblMsg (pLblMsgInfo);
                    /* delete entry from lbl msg list */
                    TMO_DLL_Delete (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                                    &(pLblEntry->NextNode));
                    if (MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID,
                                            (UINT1 *) pLblEntry) == MEM_FAILURE)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                   "Mem release failure - label msg pool\r\n");
                    }
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "L2VpnProcessLdpLblWdrawMsg called\r\n");
                    if ((TMO_DLL_Count (L2VPN_PWVC_LIST
                                        (pPeerSsn)) == L2VPN_ZERO) &&
                        (TMO_DLL_Count (L2VPN_PWVC_LBL_MSG_LIST
                                        (pPeerSsn)) == L2VPN_ZERO))
                    {
                        RBTreeRem (L2VPN_PWVC_PEERADDR_RB_PTR
                                   (gpPwVcGlobalInfo), pPeerSsn);
                        MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID,
                                            (UINT1 *) pPeerSsn);
                        pPeerSsn = NULL;
#ifdef MPLS_IPV6_WANTED

                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "L2VpnProcessLdpLblWdrawMsg Peer IPv4 %x -IPv6 %s Session is deleted\r\n",
                                    PeerSsn.PeerAddr.au1Ipv4Addr,
                                    Ip6PrintAddr (&(PeerSsn.PeerAddr.Ip6Addr)));
#else
                        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "L2VpnProcessLdpLblWdrawMsg Peer %x Session is deleted\r\n",
                                    PeerSsn.PeerAddr.au1Ipv4Addr);
#endif
                    }
                    break;
                }
            }
        }
        return L2VPN_FAILURE;
    }

    if (pLblInfo->i1StatusCode == L2VPN_LDP_STAT_PWVC_WRONG_C_BIT)
    {
        /* As per section 5.1.2 of CP-01,
         * "A Label Withdraw message with the "Wrong c-bit" status
         *  code. Treat as a normal Label Withdraw, but do not respond.
         *  Continue to wait for the next control message for the PW"
         */
        /* Nothing is being done, just SUCCESS is returned */

        /* NOTE :
         * Assumption here is "Wrong c-bit" status code will not
         * be received after the PWVC got established. i.e., after
         * PW_VC_UP event is sent to L2VPN module, this message
         * is not expected.
         *
         * If this message is received after PW_VC_UP event was sent,
         * no PW_VC_DOWN event will be sent to L2VPN module, but the
         * next map message is handled accordingly.
         */
        /* Set Control Word Status */
        L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_RXWD_WWRONGBITERRORCODE;
        return L2VPN_SUCCESS;
    }

    if (pLblInfo->u4Label != L2VPN_INVALID_LABEL)
    {
        /* Optional label TLV present in the withdraw message.
         * In addition to VCID and VC type matching
         * Label matching is also done.
         * If not matched withdraw message is ignored.
         */
        if (pLblInfo->u4Label != pPwVcEntry->u4OutVcLabel)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Label mismatch in received Lbl Withdraw message\r\n");
            return L2VPN_FAILURE;
        }
    }

    pPwVcEntry->u1LocalCcSelected = L2VPN_ZERO;
    pPwVcEntry->u1LocalCvSelected = L2VPN_ZERO;

    pPwVcEntry->u1RemoteCcAdvert = L2VPN_ZERO;
    pPwVcEntry->u1RemoteCvAdvert = L2VPN_ZERO;

    /* Update Pw Oper status */
    L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |= L2VPN_PWVC_STATUS_NOT_FORWARDING;
    i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
    }
    L2VPN_PWVC_PREV_LABEL (pPwVcEntry) =
        L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
    if (L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
    {
        i4Status = L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO);
        if (i4Status != L2VPN_SUCCESS)
        {

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Request to send Label Release Msg failed\r\n");
        }
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpNotifMsg                                   */
/* Description   : This routine processes Notification message from LDP      */
/* Input(s)      : pLblInfo - pointer to Label message info                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpNotifMsg (tPwVcNotifEvtInfo * pNotifInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcLblMsgInfo    *pLblInfo = NULL;
    tPwVcLblMsgEntry   *pLblMsgEntry = NULL;
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;    /*MS-PW */
    tPwVcEntry         *pPwVcAttached = NULL;    /*MS-PW */
    INT4                i4Status = L2VPN_FAILURE;
    UINT1               u1OldRemoteStatus = L2VPN_ZERO;
    UINT1               u1Event = 0;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd Notification msg from IPv4- %d.%d.%d.%d  IPv6 %s for VcId : %d\r\n",
                (*(UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr)) + L2VPN_THREE)),
                Ip6PrintAddr (&(pNotifInfo->PeerAddr.Ip6Addr)),
                pNotifInfo->u4VcID);
#else
    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd Notification msg from %d.%d.%d.%d for VcId : %d\r\n",
                (*(UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pNotifInfo->PeerAddr.u4Addr)) + L2VPN_THREE)),
                pNotifInfo->u4VcID);
#endif
    MEMCPY (&(GenU4Addr.Addr), &(pNotifInfo->PeerAddr), sizeof (uGenU4Addr));

    GenU4Addr.u2AddrType = (UINT2) pNotifInfo->u2AddrType;

    pPwVcEntry = L2VpnGetPwVcEntry (pNotifInfo->i1PwOwner,
                                    pNotifInfo->u4VcID, pNotifInfo->u1VcType,
                                    pNotifInfo->au1Agi, pNotifInfo->au1Taii,
                                    pNotifInfo->au1Saii, GenU4Addr);

    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG2 (L2VPN_DBG_ERR_VC_FLAG,
                    "Failed to find the matching PwVc Entry "
                    "VCID = %d, VCType = %d\r\n", pNotifInfo->u4VcID,
                    pNotifInfo->u1VcType);

        /* If the peer changes the PW local status before 
         * the local PW configuration to happen then update 
         * the status code of label info. stored in the peer session */

        MEMSET ((UINT1 *) &PeerSsn, 0, sizeof (tPwVcActivePeerSsnEntry));
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pNotifInfo->u2AddrType)
        {
            MEMCPY (&(PeerSsn.PeerAddr),
                    pNotifInfo->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pNotifInfo->u2AddrType)
#endif
        {
            MEMCPY (&(PeerSsn.PeerAddr),
                    &(pNotifInfo->PeerAddr.u4Addr), IPV4_ADDR_LENGTH);
        }
        PeerSsn.u1AddrType = (UINT1) pNotifInfo->u2AddrType;

        pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                              &PeerSsn);

        if ((pPeerSsn == NULL) || (pPeerSsn->u4Status != L2VPN_SESSION_UP))
        {
            return L2VPN_FAILURE;
        }

        TMO_DLL_Scan (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                      pLblMsgEntry, tPwVcLblMsgEntry *)
        {
            pLblInfo = &pLblMsgEntry->LblInfo;

            if ((pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG) &&
                (pLblInfo->u4PwVcID == pNotifInfo->u4VcID))
            {
                pLblInfo->u4PwStatusCode = pNotifInfo->u1PwStatusCode;
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PWID:PwOperStatus updation from Notif Message: %x"
                            "\r\n", pLblInfo->u4PwStatusCode);
                break;
            }
            else if ((pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
                     (MEMCMP (pLblInfo->au1Agi, pNotifInfo->au1Agi,
                              pLblInfo->u1AgiLen) == L2VPN_ZERO) &&
                     (MEMCMP (pLblInfo->au1Saii, pNotifInfo->au1Saii,
                              pLblInfo->u1SaiiLen) == L2VPN_ZERO) &&
                     (MEMCMP (pLblInfo->au1Taii, pNotifInfo->au1Taii,
                              pLblInfo->u1TaiiLen) == L2VPN_ZERO))
            {
                pLblInfo->u4PwStatusCode = pNotifInfo->u1PwStatusCode;
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "GENFEC:PwOperStatus updation from Notif Message: %x\r\n",
                            pLblInfo->u4PwStatusCode);
                break;
            }
        }
        return L2VPN_FAILURE;
    }

    if ((pPwVcEntry->u1MapStatus & L2VPN_MAP_RCVD) == L2VPN_ZERO)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_SIG_FLAG,
                   "Mapping msg should be rcvd before processing notif msg\n");
        return L2VPN_FAILURE;
    }

    if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) != L2VPN_PWVC_RMT_STAT_CAPABLE)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_SIG_FLAG,
                   "Notification message from peer is invalid if the status "
                   "TLV is not supported\n");
        return L2VPN_FAILURE;
    }
    u1OldRemoteStatus = pPwVcEntry->u1RemoteStatus;
    pPwVcEntry->u1RemoteStatus = pNotifInfo->u1PwStatusCode;
    if (pPwVcEntry->u1RemoteStatus & L2VPN_PWVC_STATUS_PSN_BITMASK)
    {
        if (pPwVcEntry->i1OperStatus == L2VPN_PWVC_UP)
        {
            /* If fourth bit (PSN facing Rx Fault) or fifth bit (PSN Facing 
             * Tx fault) is set, Change the PW Oper Status to LOWER LAYER DOWN.
             * Otherwise, change it to DOWN. 
             */
            if ((pPwVcEntry->u1RemoteStatus &
                 L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT) ||
                (pPwVcEntry->u1RemoteStatus &
                 L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT))
            {
                u1Event = L2VPN_PWVC_PSN_TNL_DOWN;
            }
            else
            {
                u1Event = L2VPN_PWVC_DOWN;
            }

            /* Update PW Oper Status */
            i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, u1Event);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation: PwLocalStatus %x "
                            "PwRemoteStatus %x\r\n",
                            pPwVcEntry->u1LocalStatus,
                            pPwVcEntry->u1RemoteStatus);
            }
        }
        if (u1OldRemoteStatus != pPwVcEntry->u1RemoteStatus)
        {
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));

            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PwLocalStatus %x PwRemoteStatus %x\r\n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
    }
    else if ((L2VPN_PWVC_STATUS_IS_UP (pPwVcEntry->u1RemoteStatus))
             || ((L2VPN_PWVC_IS_ONLY_AC_FAULT (pPwVcEntry)) &&
                 (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)))
    {
        /* If all the bits are cleared, Change the PW Oper status to UP */
        if ((pPwVcEntry->i1OperStatus == L2VPN_PWVC_DOWN) ||
            (pPwVcEntry->i1OperStatus == L2VPN_PWVC_OPER_LLDOWN))
        {
            /* Update PW Oper Status */
            i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation: PwLocalStatus %x "
                            "PwRemoteStatus %x\r\n",
                            pPwVcEntry->u1LocalStatus,
                            pPwVcEntry->u1RemoteStatus);
            }
        }
        else if (u1OldRemoteStatus & L2VPN_PWVC_STATUS_AC_BITMASK)
        {
            /* Program the PW in the hw */
            if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) == L2VPN_ZERO)
            {
                /*When remote AC UP is recieved, in VPWS, PW & AC both should be created and
                 * PWVC_OPER_STATUS should be marked as UP */
                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_AC_UPDATE,
                                          L2VPN_ONE);
                }
                else
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_ONE);
                }

                i4Status = L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                                    L2VPN_PWVC_OPER_APP_CP_OR_MGMT);

                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_AC_UPDATE,
                                          L2VPN_TWO);
                }
                else
                {
                    if (i4Status == L2VPN_SUCCESS &&
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                        L2VPN_PWVC_OPER_DOWN)
                    {
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_UP;
                    }
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_TWO);
                }

                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "AC up handling: PwLocalStatus %x "
                                "PwRemoteStatus %x failed\r\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }
            }
            else
            {
                /*Get Attached PW VC entry */
                pPwVcAttached =
                    L2VpnGetPwVcEntryFromIndex
                    (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                if (pPwVcAttached != NULL &&
                    (L2VPN_PWVC_LOCAL_STATUS (pPwVcAttached)
                     == L2VPN_PWVC_STATUS_FORWARDING) &&
                    (L2VPN_PWVC_REMOTE_STATUS (pPwVcAttached)
                     == L2VPN_PWVC_STATUS_FORWARDING))
                {

                    /* Get MS PW Entry */
                    pL2vpnFsMsPwConfigTable =
                        L2VpnGetMsPwEntry (L2VPN_PWVC_INDEX (pPwVcEntry),
                                           L2VPN_PWVC_ATTACHED_PW_INDEX
                                           (pPwVcEntry));

                    if (pL2vpnFsMsPwConfigTable == NULL)
                    {
                        pL2vpnFsMsPwConfigTable =
                            L2VpnGetMsPwEntry (L2VPN_PWVC_ATTACHED_PW_INDEX
                                               (pPwVcEntry),
                                               L2VPN_PWVC_INDEX (pPwVcEntry));
                    }

                    if (pL2vpnFsMsPwConfigTable == NULL)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Attached PW Index %d not found for %d\n",
                                    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry),
                                    L2VPN_PWVC_INDEX (pPwVcEntry));
                    }

                    i4Status =
                        L2VpnUpdateMsPwOperStatus (pL2vpnFsMsPwConfigTable,
                                                   L2VPN_MSPW_UP);

                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "MSPW Crossconnect UP programming failed "
                                    "for PW %d %d\n",
                                    L2VPN_PWVC_INDEX (pPwVcEntry),
                                    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));
                    }
                }
            }

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);

            if (u1OldRemoteStatus != pPwVcEntry->u1RemoteStatus)
            {
                pRgPw =
                    L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                        (pPwVcEntry));

                if (pRgPw != NULL)
                {
                    L2vpnRedundancyPwStateUpdate (pRgPw,
                                                  L2VPNRED_EVENT_PW_STATE_CHANGE);
                }
            }
        }
        else if (u1OldRemoteStatus != pPwVcEntry->u1RemoteStatus)
        {
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));

            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }

            /* Forward PW-RED status between segments, on change */
            if (L2VPN_RED_STATUS_FORWARD_ENABLE (&gL2vpnMsPwGlobals) &&
                L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != 0 &&
                L2VPN_PWVC_STATUS_RED_STATUS (u1OldRemoteStatus) !=
                L2VPN_PWVC_STATUS_RED_STATUS (L2VPN_PWVC_REMOTE_STATUS
                                              (pPwVcEntry)))
            {
                pPwVcAttached =
                    L2VpnGetPwVcEntryFromIndex (L2VPN_PWVC_ATTACHED_PW_INDEX
                                                (pPwVcEntry));
            }

            if (pPwVcAttached != NULL &&
                L2VPN_PWVC_OWNER (pPwVcEntry) ==
                L2VPN_PWVC_OWNER (pPwVcAttached))
            {
                L2VPN_PWVC_LOCAL_STATUS (pPwVcAttached) |=
                    (UINT1) (L2VPN_PWVC_STATUS_RED_STATUS
                             (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry)));

                if (L2VpnSendLdpPwVcNotifMsg (pPwVcAttached) != L2VPN_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "PW-RED: PW-STATUS forwarding failed\n");
                }
            }
        }
        else if (pPwVcEntry->
                 u1LocalStatus & L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST)
        {
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));

            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }

        }
        if (pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_STANDBY)
        {
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));

            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }
        }
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PwLocalStatus %x PwRemoteStatus %x\r\n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
    }
    else if ((pPwVcEntry->u1RemoteStatus & L2VPN_PWVC_STATUS_AC_BITMASK) &&
             (!(u1OldRemoteStatus & L2VPN_PWVC_STATUS_AC_BITMASK)))
    {
        /* Update PW Oper Status */
        if (pPwVcEntry->i1OperStatus == L2VPN_PWVC_UP)
        {
            if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) == L2VPN_ZERO)
            {
                /*When remote AC Down is recieved, in VPWS, PW & AC both should be deleted and
                 * PWVC_OPER_STATUS should be marked as DOWN */
                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_AC_UPDATE,
                                          L2VPN_ONE);
                }
                else
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_ONE);
                }

                /* Program the PW in the hw */
                i4Status = L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_DOWN,
                                                    L2VPN_PWVC_OPER_APP_CP_OR_MGMT);

                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_AC_UPDATE,
                                          L2VPN_TWO);
                }
                else
                {
                    if (i4Status == L2VPN_SUCCESS &&
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                        L2VPN_PWVC_OPER_UP)
                    {
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_DOWN;
                    }
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_TWO);
                }

                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "AC down handling: PwLocalStatus %x "
                                "PwRemoteStatus %x failed\r\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }
            }
            else
            {
                /*Get Attached PW VC entry */
                pPwVcAttached =
                    L2VpnGetPwVcEntryFromIndex
                    (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                if (pPwVcAttached != NULL)
                {

                    /* Get MS PW Entry */
                    pL2vpnFsMsPwConfigTable =
                        L2VpnGetMsPwEntry (L2VPN_PWVC_INDEX (pPwVcEntry),
                                           L2VPN_PWVC_ATTACHED_PW_INDEX
                                           (pPwVcEntry));

                    if (pL2vpnFsMsPwConfigTable == NULL)
                    {
                        pL2vpnFsMsPwConfigTable =
                            L2VpnGetMsPwEntry (L2VPN_PWVC_ATTACHED_PW_INDEX
                                               (pPwVcEntry),
                                               L2VPN_PWVC_INDEX (pPwVcEntry));
                    }

                    if (pL2vpnFsMsPwConfigTable == NULL)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Attached PW Index %d not found for %d\n",
                                    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry),
                                    L2VPN_PWVC_INDEX (pPwVcEntry));
                    }

                    i4Status =
                        L2VpnUpdateMsPwOperStatus (pL2vpnFsMsPwConfigTable,
                                                   L2VPN_MSPW_DOWN);

                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "MSPW Crossconnect UP programming failed "
                                    "for PW %d %d\n",
                                    L2VPN_PWVC_INDEX (pPwVcEntry),
                                    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));
                    }
                }
            }

        }
        if (u1OldRemoteStatus != pPwVcEntry->u1RemoteStatus)
        {
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));

            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }
        }
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPwVcLblMapMsg                                 */
/* Description   : This routine posts message to LDP queue requesting for    */
/*                 sending a label Map message                               */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
#ifdef LDP_GR_WANTED
tL2VpnPwHwList      gL2VpnPwHwListEntry;
#endif

INT4
L2VpnSendLdpPwVcLblMapMsg (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    INT1               *pi1TmpString = NULL;
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;
#ifdef LDP_GR_WANTED
    MEMSET (&gL2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
#endif

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    LDP_L2VPN_EVT_LBL_STALE_LBLVAL (&TmpPwVcEvtInfo) = L2VPN_INVALID_LABEL;
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4StaticLabel = L2VPN_INVALID_LABEL;

    TmpPwVcEvtInfo.unEvtInfo.LblInfo.pi1LocalIfString = pi1TmpString;
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_MSG_EVT;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_MAP_MSG;
    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {

        pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
        LDP_L2VPN_EVT_LBL_PW_MPLS_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry));
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_LBL_PEER_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u2AddrType =
        (UINT2) pPwVcEntry->i4PeerAddrType;
    LDP_L2VPN_EVT_LBL_LBLVAL (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_PREV_LABEL (pPwVcEntry);

    /* Dont send label Mapping Message */
    if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_DONT_SEND_LBL_MAP)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "gi4MplsSimulateFailure is set, Don't send Label Map Message \n");
        return L2VPN_SUCCESS;
    }

    /* Added for GR Purpose */
    if (pPwVcEntry->u1GrSyncStatus != L2VPN_GR_FULLY_SYNCHRONIZED)
    {
#ifdef LDP_GR_WANTED
        gL2VpnPwHwListEntry.u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        gL2VpnPwHwListEntry.u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);
        if (L2VpnHwListGet (&gL2VpnPwHwListEntry, &gL2VpnPwHwListEntry)
            == MPLS_SUCCESS)
        {
            if (L2VPN_PW_HW_LIST_STALE_STATUS (&gL2VpnPwHwListEntry) ==
                L2VPN_PW_STATUS_STALE)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "%s : RESTARTING NODE: Fetch In-Label(%d) "
                            "from HW List and send label map message to peer\n",
                            __func__,
                            L2VPN_PW_HW_LIST_IN_LABEL (&gL2VpnPwHwListEntry));
                LDP_L2VPN_EVT_LBL_STALE_LBLVAL (&TmpPwVcEvtInfo) =
                    L2VPN_PW_HW_LIST_IN_LABEL (&gL2VpnPwHwListEntry);
            }
            else
            {

                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "HELPER NODE: Fetch In-Label(%d) from VC Entry"
                            "and send label map message to peer\n",
                            pPwVcEntry->u4InVcLabel);
                LDP_L2VPN_EVT_LBL_STALE_LBLVAL (&TmpPwVcEvtInfo) =
                    L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);

            }
        }
#endif
    }
    else
    {
        TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4StaticLabel =
            pPwVcEntry->u4InVcLabel;
    }
    /* Group ID is common for both FEC 128 or FEC 129 type */
    LDP_L2VPN_EVT_LBL_GRP_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);

    LDP_L2VPN_EVT_LBL_REQ_MSGID (&TmpPwVcEvtInfo)
        = L2VPN_PWVC_LABEL_REQID (pPwVcEntry);

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_LBL_PWVC_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_ID (pPwVcEntry);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_LBL_AGI_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry);
        if (L2VPN_PWVC_AGI_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_AGI (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_AGI (pPwVcEntry),
                    L2VPN_PWVC_AGI_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_AGI_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_SAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_SAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_SAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_SAII (pPwVcEntry),
                    L2VPN_PWVC_SAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_SAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_TAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_TAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_TAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_TAII (pPwVcEntry),
                    L2VPN_PWVC_TAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_TAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }
    }
    LDP_L2VPN_EVT_LBL_RMT_STATUS_CAPABLE (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_RMT_STAT_CAPABLE;
    LDP_L2VPN_EVT_LBL_IF_MTU (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry);

    if (L2VPN_PWVC_CW_STATUS (pPwVcEntry) == L2VPN_PWVC_CWPRESENT)
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_TRUE;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_FALSE;
    }

    LDP_L2VPN_EVT_LBL_PW_TYPE (&TmpPwVcEvtInfo) = L2VPN_PWVC_TYPE (pPwVcEntry);

    if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_VCCV_CAPABLE)
    {
        LDP_L2VPN_EVT_LBL_LCL_CC_ADVERT (&TmpPwVcEvtInfo) =
            L2VpnVccvReverseValue (L2VPN_PWVC_LCL_CC_ADVERT (pPwVcEntry));

        LDP_L2VPN_EVT_LBL_LCL_CV_ADVERT (&TmpPwVcEvtInfo) =
            L2VpnVccvReverseValue (L2VPN_PWVC_LCL_CV_ADVERT (pPwVcEntry));
        /* Status Indication also to be added */
        LDP_L2VPN_EVT_LBL_LCL_ADVERT (&TmpPwVcEvtInfo) |= L2VPN_PW_VCCV_CAPABLE;
    }
    if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)
    {
        LDP_L2VPN_EVT_LBL_LCL_ADVERT (&TmpPwVcEvtInfo)
            |= L2VPN_PW_STATUS_INDICATION;
    }
    /* Currently status code is always zero, if local AC up/down is 
     * indication to peer supported then below status code 
     * should be updated accordingly. */
    LDP_L2VPN_EVT_LBL_STAT_CODE (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_CBIT_STATUS_CODE_NONE;

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {
        LDP_L2VPN_EVT_LBL_LCL_ENTID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_MPLS_LCL_LDP_ENTID
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
    }

    if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)
    {
        /*Update the local status */
        LDP_L2VPN_EVT_LBL_PW_STATUS_CODE (&TmpPwVcEvtInfo)
            = pPwVcEntry->u1LocalStatus;
        /* When label mapping message is sent to the peer, not forwarding bit
         * is removed from local status so that peer can program the hardware
         * if required criteria is satisfied.
         *
         * If notification message like resource unavailability is received from
         * LDP, it will be informed to the peer throught LDP Notification Message */
        LDP_L2VPN_EVT_LBL_PW_STATUS_CODE (&TmpPwVcEvtInfo)
            &= ~(L2VPN_PWVC_STATUS_NOT_FORWARDING);

        pPwVcEntry->u1PrevLocalStatus =
            (UINT1) (LDP_L2VPN_EVT_LBL_PW_STATUS_CODE (&TmpPwVcEvtInfo));
    }

    if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_LDP_QUEUE_FULL)
    {
        L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "Queue full Error "
                   "in sending Lbl Mapping Message has simulated\r\n");
        return L2VPN_FAILURE;
    }
    /*if (L2VpnHandleSendMsgToLdp (pPwVcEntry, L2VPN_LDP_LBL_MAP_MSG)
       == L2VPN_NOT_OK)
       {
       return L2VPN_FAILURE;
       } */

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Posting to LDP module to send Lbl Mapping Msg "
                    "failed for PW %d\r\n", pPwVcEntry->u4PwVcIndex);
        return L2VPN_FAILURE;
    }
    /* Mark as Map sent */
    pPwVcEntry->u1MapStatus |= L2VPN_MAP_SENT;
    pPwVcEntry->u1MapStatus |= L2VPN_REL_WAIT;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG2 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label mapping msg to %s for PW %d\r\n",
                Ip6PrintAddr (&
                              (TmpPwVcEvtInfo.unEvtInfo.LblInfo.PeerAddr.
                               Ip6Addr)), pPwVcEntry->u4PwVcIndex);
#else
    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label mapping msg to %d.%d.%d.%d for PW %d\r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)), pPwVcEntry->u4PwVcIndex);
#endif
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPwVcLblRelMsg                                 */
/* Description   : This routine posts message to LDP queue requesting to     */
/*                 send a label release message                              */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : i1Status - Label info status code                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpPwVcLblRelMsg (tPwVcEntry * pPwVcEntry, INT1 i1Status)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;
    UINT4               u4MsgType = 0;

    if (!(pPwVcEntry->u1MapStatus & L2VPN_MAP_RCVD))
    {
        return L2VPN_SUCCESS;
    }

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.pi1LocalIfString = NULL;
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_MSG_EVT;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_REL_MSG;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_LBL_PEER_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u2AddrType =
        (UINT2) pPwVcEntry->i4PeerAddrType;
    if (L2VPN_PWVC_PREV_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
    {
        LDP_L2VPN_EVT_LBL_LBLVAL (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_PREV_LABEL (pPwVcEntry);
        L2VPN_PWVC_PREV_LABEL (pPwVcEntry) = L2VPN_INVALID_LABEL;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_LBLVAL (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
    }

    /* Group ID is common for both FEC 128 or FEC 129 type */
    LDP_L2VPN_EVT_LBL_GRP_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_LBL_PWVC_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_ID (pPwVcEntry);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_LBL_AGI_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_AGI_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_AGI (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_AGI (pPwVcEntry),
                    L2VPN_PWVC_AGI_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_AGI_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_SAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_SAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_SAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_SAII (pPwVcEntry),
                    L2VPN_PWVC_SAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_SAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_TAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry);
        if (L2VPN_PWVC_TAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_TAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_TAII (pPwVcEntry),
                    L2VPN_PWVC_TAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_TAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }
    }
    LDP_L2VPN_EVT_LBL_IF_MTU (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry);

    if (L2VPN_PWVC_CW_STATUS (pPwVcEntry) == L2VPN_PWVC_CWPRESENT)
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_TRUE;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_FALSE;
    }
    LDP_L2VPN_EVT_LBL_PW_TYPE (&TmpPwVcEvtInfo) = L2VPN_PWVC_TYPE (pPwVcEntry);
    LDP_L2VPN_EVT_LBL_STAT_CODE (&TmpPwVcEvtInfo) = i1Status;
    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {
        LDP_L2VPN_EVT_LBL_LCL_ENTID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_MPLS_LCL_LDP_ENTID
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
    }

/*    if ((L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN) ||
        (pPwVcEntry->i1RowStatus == DESTROY) ||
        (pPwVcEntry->i1RowStatus == NOT_IN_SERVICE) ||
        (pPwVcEntry->i1AdminStatus == L2VPN_PWVC_ADMIN_DOWN))
    {
        u4MsgType = L2VPN_LDP_LBL_WDRAW_MSG;
    }
    else*/

    u4MsgType = L2VPN_LDP_LBL_REL_MSG;

    if (L2VpnHandleSendMsgToLdp (pPwVcEntry, u4MsgType) == L2VPN_NOT_OK)
    {
        return L2VPN_FAILURE;
    }

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Post event failed\r\n");
        return L2VPN_FAILURE;
    }

    /* Mask the Map received */
    pPwVcEntry->u1MapStatus &= (UINT1) (~L2VPN_MAP_RCVD);
    /* Remove the Element from Outlabel based RBTree before releasing 
     * the Labels And set the value of outVcLabel as Invalid */

    if (pPwVcEntry->bIsStaticPw != TRUE)
    {
        L2VpnPwSetOutVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
    }

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label release msg to IPv4- %d.%d.%d.%d  IPv6 %s \r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)),
                Ip6PrintAddr (&
                              (TmpPwVcEvtInfo.unEvtInfo.LblInfo.PeerAddr.
                               Ip6Addr)));

#else
    L2VPN_DBG4 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label release msg to %d.%d.%d.%d\r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)));
#endif
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }

    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPwVcLblWdrawMsg                               */
/* Description   : This routine posts message to LDP queue requesting to     */
/*                 send a label withdraw message                             */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : i1Status - Label info status code                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpPwVcLblWdrawMsg (tPwVcEntry * pPwVcEntry, INT1 i1Status)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    if (gi4MplsSimulateFailure == LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC)
    {
        return (L2VpnSendLdpPwVcLblWcWdrawMsg (pPwVcEntry, (UINT1) i1Status));
    }

    if (!(pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT))
    {
        return L2VPN_SUCCESS;
    }

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.pi1LocalIfString = NULL;
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_MSG_EVT;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_WDRAW_MSG;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_LBL_PEER_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u2AddrType =
        (UINT2) pPwVcEntry->i4PeerAddrType;
    LDP_L2VPN_EVT_LBL_LBLVAL (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);

    /* Group ID is common for both FEC 128 or FEC 129 type */
    LDP_L2VPN_EVT_LBL_GRP_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_LBL_PWVC_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_ID (pPwVcEntry);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_LBL_AGI_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_AGI_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_AGI (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_AGI (pPwVcEntry),
                    L2VPN_PWVC_AGI_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_AGI_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_SAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_SAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_SAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_SAII (pPwVcEntry),
                    L2VPN_PWVC_SAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_SAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_TAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_TAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_TAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_TAII (pPwVcEntry),
                    L2VPN_PWVC_TAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_TAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }
    }
    LDP_L2VPN_EVT_LBL_IF_MTU (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry);

    if (L2VPN_PWVC_CW_STATUS (pPwVcEntry) == L2VPN_PWVC_CWPRESENT)
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_TRUE;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_FALSE;
    }
    LDP_L2VPN_EVT_LBL_PW_TYPE (&TmpPwVcEvtInfo) = L2VPN_PWVC_TYPE (pPwVcEntry);
    LDP_L2VPN_EVT_LBL_STAT_CODE (&TmpPwVcEvtInfo) = i1Status;
    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {
        LDP_L2VPN_EVT_LBL_LCL_ENTID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_MPLS_LCL_LDP_ENTID
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
    }

    if (L2VpnHandleSendMsgToLdp (pPwVcEntry, L2VPN_LDP_LBL_WDRAW_MSG)
        == L2VPN_NOT_OK)
    {
        return L2VPN_FAILURE;
    }

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Posting to LDP module to send Lbl Withdraw Msg "
                    "failed for PW %d\r\n", pPwVcEntry->u4PwVcIndex);
        return L2VPN_FAILURE;
    }

    if (L2VPN_LDP_STAT_PWVC_WRONG_C_BIT != i1Status)
    {
        /* Mask the Map sent */
        pPwVcEntry->u1MapStatus &= L2VPN_LOWNIB;
    }

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label withdraw msg to IPv4- %d.%d.%d.%d IPv6 %s for PW %d\r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)),
                Ip6PrintAddr (&
                              (TmpPwVcEvtInfo.unEvtInfo.LblInfo.PeerAddr.
                               Ip6Addr)), pPwVcEntry->u4PwVcIndex);
#else

    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label withdraw msg to %d.%d.%d.%d for PW %d\r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)), pPwVcEntry->u4PwVcIndex);
#endif
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }

    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPwVcNotifEvent                                */
/* Description   : This routine posts message to LDP queue for sending       */
/*                 notification messages                                     */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : i1Status - Label info status code                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpPwVcNotifEvent (tPwVcEntry * pPwVcEntry)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;
    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_NOTIF_EVT;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.NotifInfo.u2AddrType =
        (UINT2) pPwVcEntry->i4PeerAddrType;
    LDP_L2VPN_EVT_NOTIF_LABEL (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_PREV_LABEL (pPwVcEntry);

    LDP_L2VPN_EVT_NOTIF_ENTITY_INDEX (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX
        ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_NOTIF_VC_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_ID (pPwVcEntry);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_NOTIF_AGI_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_AGI_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_AGI (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_AGI (pPwVcEntry),
                    L2VPN_PWVC_AGI_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_AGI_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_NOTIF_SAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_SAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_SAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_SAII (pPwVcEntry),
                    L2VPN_PWVC_SAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_SAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_NOTIF_TAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_TAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_TAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_TAII (pPwVcEntry),
                    L2VPN_PWVC_TAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_TAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }
    }
    LDP_L2VPN_EVT_NOTIF_VC_TYPE (&TmpPwVcEvtInfo) =
        (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry);
    LDP_L2VPN_EVT_NOTIF_MSG_CODE (&TmpPwVcEvtInfo) = L2VPN_LDP_REL_LBL_RES;

    LDP_L2VPN_EVT_NOTIF_ENTITY_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_MPLS_LCL_LDP_ENTID
        ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Post event failed\r\n");
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPwVcNotifMsg                                  */
/* Description   : This routine posts message to LDP queue for sending       */
/*                 notification messages                                     */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : i1Status - Label info status code                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpPwVcNotifMsg (tPwVcEntry * pPwVcEntry)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_NOTIF_EVT;
    LDP_L2VPN_EVT_NOTIF_MSG_CODE (&TmpPwVcEvtInfo) = L2VPN_LDP_NOTIF_MSG_EVT;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.NotifInfo.u2AddrType =
        (UINT2) pPwVcEntry->i4PeerAddrType;
    LDP_L2VPN_EVT_NOTIF_LABEL (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
    LDP_L2VPN_EVT_NOTIF_VC_TYPE (&TmpPwVcEvtInfo) =
        (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry);

    if ((L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_MANDATORY) ||
        (L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_PREFERRED))
    {
        LDP_L2VPN_EVT_LBL_NOTIF_CW (&TmpPwVcEvtInfo) = L2VPN_TRUE;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_NOTIF_CW (&TmpPwVcEvtInfo) = L2VPN_FALSE;
    }
    LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_NOTIF_VC_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_ID (pPwVcEntry);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_NOTIF_AGI_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_AGI_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_AGI (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_AGI (pPwVcEntry),
                    L2VPN_PWVC_AGI_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_AGI_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_NOTIF_SAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_SAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_SAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_SAII (pPwVcEntry),
                    L2VPN_PWVC_SAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_SAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_NOTIF_TAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_TAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_TAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_TAII (pPwVcEntry),
                    L2VPN_PWVC_TAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_TAII_LEN (pPwVcEntry);
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }
    }
    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {

        LDP_L2VPN_EVT_NOTIF_ENTITY_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_MPLS_LCL_LDP_ENTID
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
    }
    if (L2VpnHandleSendMsgToLdp (pPwVcEntry, L2VPN_LDP_NOTIF_MSG_EVT)
        == L2VPN_NOT_OK)
    {
        return L2VPN_FAILURE;
    }

    if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)
    {
        /* Send PW Local Status */
        LDP_L2VPN_EVT_LBL_NOTIF_PW_STATUS_CODE (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry);

        pPwVcEntry->u1PrevLocalStatus = pPwVcEntry->u1LocalStatus;
    }

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Posting to LDP module to send Notification Msg "
                    "failed for PW %d\r\n", pPwVcEntry->u4PwVcIndex);
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG7 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent Notification msg to IPv4- %d.%d.%d.%d IPv6 %s for PW %d with Status "
                "%x\r\n",
                (*(UINT1 *)
                 (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_ONE)),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_TWO)),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_THREE)),
                Ip6PrintAddr (&
                              (TmpPwVcEvtInfo.unEvtInfo.LblInfo.PeerAddr.
                               Ip6Addr)), pPwVcEntry->u4PwVcIndex,
                LDP_L2VPN_EVT_LBL_NOTIF_PW_STATUS_CODE (&TmpPwVcEvtInfo));
#else
    L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent Notification msg to %d.%d.%d.%d for PW %d with Status "
                "%x\r\n",
                (*(UINT1 *)
                 (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_ONE)),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_TWO)),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_THREE)), pPwVcEntry->u4PwVcIndex,
                LDP_L2VPN_EVT_LBL_NOTIF_PW_STATUS_CODE (&TmpPwVcEvtInfo));
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPwVcLblWcWdrawMsg                             */
/* Description   : This routine posts message to LDP queue for sending label */
/*                 withdraw message                                          */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : i1Status - Label info status code                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpPwVcLblWcWdrawMsg (tPwVcEntry * pPwVcEntry, UINT1 i1Status)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_MSG_EVT;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_WC_WDRAW_MSG;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_LBL_PEER_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u2AddrType =
        (UINT2) pPwVcEntry->i4PeerAddrType;

    /* Group ID is common for both FEC 128 or FEC 129 type */
    LDP_L2VPN_EVT_LBL_GRP_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;
    }
    if (L2VPN_PWVC_CW_STATUS (pPwVcEntry) == L2VPN_PWVC_CWPRESENT)
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_TRUE;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = L2VPN_FALSE;
    }
    LDP_L2VPN_EVT_LBL_PW_TYPE (&TmpPwVcEvtInfo) = L2VPN_PWVC_TYPE (pPwVcEntry);
    LDP_L2VPN_EVT_LBL_STAT_CODE (&TmpPwVcEvtInfo) = (INT1) i1Status;
    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {
        LDP_L2VPN_EVT_LBL_LCL_ENTID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_MPLS_LCL_LDP_ENTID
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
    }

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Post event failed\r\n");
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label wildcard withdraw msg to IPV4- %d.%d.%d.%d IPv6 %s \r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)),
                Ip6PrintAddr (&
                              (TmpPwVcEvtInfo.unEvtInfo.LblInfo.PeerAddr.
                               Ip6Addr)));

#else
    L2VPN_DBG4 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent label wildcard withdraw msg to %d.%d.%d.%d  \r\n",
                (*(UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_ONE)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_TWO)),
                (*((UINT1 *) (&(LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo)))
                   + L2VPN_THREE)));
#endif

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4PwVcID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1PwType,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1ControlWord,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4GroupID,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.i1StatusCode,
                    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u4Label);
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLblMapOrNotifMsg                                 */
/* Description   : This routine posts message to LDP queue for sending label */
/*                 mapping or notification message based on remote status    */
/*                 capability.                                               */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLblMapOrNotifMsg (tPwVcEntry * pPwVcEntry)
{
    UINT4               u4PwIndex = pPwVcEntry->u4PwVcIndex;
    UINT1               u1MapToBeSent = FALSE;
    UINT1               u1NotifToBeSent = FALSE;
    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (((pPwVcEntry->i1PwVcOwner == L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        return L2VPN_SUCCESS;
    }

    if (L2VpnUtilIsRemoteStatusNotCapable (pPwVcEntry) == TRUE)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %x is Remote Status Not Capable\n",
                    u4PwIndex, GenU4Addr.Addr.u4Addr);

        /* As per RFC-4447, 5.4.1. Use of Label Mappings Messages
         * If the label withdraw method for PW status communication
         * is selected for the PW, it will result in 
         * the Label Mapping Message being
         * advertised only if the attachment circuit is active. */
        if (!(pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT))
        {
            u1MapToBeSent = TRUE;
        }
        else
        {
            /* Further proceeding should not be done for Remote Status 
             * Not capable if AC FAULT or MAP ALREADY SENT */
            i4Status = L2VPN_SUCCESS;
        }
    }
    else if (L2VpnUtilIsRemoteStatusCapable (pPwVcEntry) == TRUE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %x is Remote Status Capable\n",
                    u4PwIndex, GenU4Addr.Addr.Ip6Addr);
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %x is Remote Status Capable\n",
                    u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
        if (!(pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT))
        {
            u1MapToBeSent = TRUE;
        }
        else if ((!(pPwVcEntry->u1LocalStatus &
                    L2VPN_PWVC_STATUS_NOT_FORWARDING)) &&
                 (pPwVcEntry->u1PrevLocalStatus != pPwVcEntry->u1LocalStatus))
        {
            /* If the local status is changed, i.e status TLV code we 
             * might have sent to LDP protocol, before getting the PW up
             * indication from LDP module, there is a possibility of 
             * local status change. So, check here to update the latest 
             * local status with the peer. 
             *
             * Ex. LMM status - 0x6 (AC facing Rx, TX fault)
             * local status = 0 (AC is up)
             * LDP indicates - LMM is successfully sent to the peer.
             * Now, send the updated local status to the peer.
             * * */

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Send LMM informed to LDP\n");
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "LDP has also sent LMM but Local status "
                        "of PW %d has changed\n",
                        pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Notify the new status to peer\n");

            u1NotifToBeSent = TRUE;
        }
        else
        {
            /* No further action needs to be taken for Remote Status 
             * Capable case if MAP is already sent and no change in local
             * status. */
            i4Status = L2VPN_SUCCESS;

            /* Remove the entry from pending msg to LDP list */

            if (L2VPN_GR_NOT_SYNCHRONIZED != pPwVcEntry->u1GrSyncStatus)
            {
                pPwVcEntry->u1SendToLdpMsgType = 0;
                TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                                &(pPwVcEntry->PendMsgToLdpNode));
            }
        }
    }

    if (u1MapToBeSent == TRUE)
    {
        i4Status = L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_BOTH);

        if (i4Status == L2VPN_SUCCESS)
        {
            pPwVcEntry->u1LocalStatus &=
                (UINT1) (~L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT);

            i4Status = L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry);

        }
        else
        {
            /* Set the 5th bit PSN Facing Tx fault */
            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT;
        }
    }
    else if (u1NotifToBeSent == TRUE)
    {
        i4Status = L2VpnSendLdpPwVcNotifMsg (pPwVcEntry);
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLblWdrawOrNotifMsg                               */
/* Description   : This routine posts message to LDP queue for sending label */
/*                 withdraw or notification message based on remote status   */
/*                 capability.                                               */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLblWdrawOrNotifMsg (tPwVcEntry * pPwVcEntry)
{
    INT4                i4Status = L2VPN_SUCCESS;

    if (((pPwVcEntry->i1PwVcOwner == L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        return L2VPN_SUCCESS;
    }

    if (L2VpnUtilIsRemoteStatusCapable (pPwVcEntry) == TRUE)
    {
        if ((!(pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING)) &&
            (pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT))
        {
            i4Status = L2VpnSendLdpPwVcNotifMsg (pPwVcEntry);
        }
    }
    else if (L2VpnUtilIsRemoteStatusNotCapable (pPwVcEntry) == TRUE)
    {
        i4Status = L2VpnSendLdpPwVcLblWdrawMsg (pPwVcEntry, L2VPN_ZERO);
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnReleaseWaitingRemLblMsg                              */
/* Description   : This routine posts message to LDP queue for sending label */
/*                 release messages                                          */
/* Input(s)      : pLblMsgInfo - pointer to label message info               */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnReleaseWaitingRemLblMsg (tPwVcLblMsgInfo * pLblMsgInfo)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;
    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.pi1LocalIfString = NULL;
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_MSG_EVT;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_LBL_REL_MSG;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pLblMsgInfo->u2AddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_LBL_PEER_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pLblMsgInfo->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pLblMsgInfo->u2AddrType)
#endif
    {
        LDP_L2VPN_EVT_LBL_PEER_IPV4 (&TmpPwVcEvtInfo) =
            pLblMsgInfo->PeerAddr.u4Addr;
    }
    TmpPwVcEvtInfo.unEvtInfo.LblInfo.u2AddrType = pLblMsgInfo->u2AddrType;

    LDP_L2VPN_EVT_LBL_LBLVAL (&TmpPwVcEvtInfo) = pLblMsgInfo->u4Label;
    /* Group ID is common for both FEC 128 or FEC 129 type */
    LDP_L2VPN_EVT_LBL_GRP_ID (&TmpPwVcEvtInfo) = pLblMsgInfo->u4GroupID;
    if (pLblMsgInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_LBL_PWVC_ID (&TmpPwVcEvtInfo) = pLblMsgInfo->u4PwVcID;
    }
    else if (pLblMsgInfo->i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_LBL_AGI_TYPE (&TmpPwVcEvtInfo) = pLblMsgInfo->u1AgiType;
        if (pLblMsgInfo->u1AgiLen > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_AGI (&TmpPwVcEvtInfo),
                    pLblMsgInfo->au1Agi, pLblMsgInfo->u1AgiLen);
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) = pLblMsgInfo->u1AgiLen;
        }
        else
        {
            LDP_L2VPN_EVT_LBL_AGI_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }

        LDP_L2VPN_EVT_LBL_SAII_TYPE (&TmpPwVcEvtInfo) = pLblMsgInfo->u1SaiiType;
        LDP_L2VPN_EVT_LBL_TAII_TYPE (&TmpPwVcEvtInfo) = pLblMsgInfo->u1TaiiType;
        if ((pLblMsgInfo->u1SaiiLen > L2VPN_ZERO) &&
            (pLblMsgInfo->u1TaiiLen > L2VPN_ZERO))
        {
            MEMCPY (LDP_L2VPN_EVT_LBL_SAII (&TmpPwVcEvtInfo),
                    pLblMsgInfo->au1Taii, pLblMsgInfo->u1TaiiLen);
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) =
                pLblMsgInfo->u1TaiiLen;
            MEMCPY (LDP_L2VPN_EVT_LBL_TAII (&TmpPwVcEvtInfo),
                    pLblMsgInfo->au1Saii, pLblMsgInfo->u1SaiiLen);
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) =
                pLblMsgInfo->u1SaiiLen;
        }
        else
        {
            LDP_L2VPN_EVT_LBL_SAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
            LDP_L2VPN_EVT_LBL_TAII_LEN (&TmpPwVcEvtInfo) = L2VPN_ZERO;
        }
    }
    LDP_L2VPN_EVT_LBL_IF_MTU (&TmpPwVcEvtInfo) = pLblMsgInfo->u2IfMtu;
    LDP_L2VPN_EVT_LBL_CNTRL_WORD (&TmpPwVcEvtInfo) = pLblMsgInfo->i1ControlWord;
    LDP_L2VPN_EVT_LBL_PW_TYPE (&TmpPwVcEvtInfo) = pLblMsgInfo->i1PwType;
    LDP_L2VPN_EVT_LBL_STAT_CODE (&TmpPwVcEvtInfo) = pLblMsgInfo->i1StatusCode;
    LDP_L2VPN_EVT_LBL_LCL_ENTID (&TmpPwVcEvtInfo) =
        pLblMsgInfo->u4LocalLdpEntityID;

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Post event failed\r\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpPeerSsnCreateReq                              */
/* Description   : This routine posts message to LDP queue for establishing  */
/*                 a targeted session with remote peer                       */
/* Input(s)      : pPeerAddr - pointer to Peer address                       */
/*               : u4LocalLdpEntityID - Ldp Entity ID                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpPeerSsnCreateReq (uGenAddr * pPeerAddr, INT4 i4PeerAddrType,
                              UINT4 u4LocalLdpEntityID)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_SSN_CREATE_EVT;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) pPeerAddr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == i4PeerAddrType)
#endif
    {
        MEMCPY ((VOID *) &LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo),
                (VOID *) pPeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.SsnInfo.u2AddrType = (UINT2) i4PeerAddrType;

    LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&TmpPwVcEvtInfo) = u4LocalLdpEntityID;

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Post event failed\r\n");
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendLdpDeRegReq                                      */
/* Description   : This routine posts message to LDP queue to                */
/*                 deregister with the LDP                                   */
/* Input(s)      : pPeerAddr - pointer to Peer address                       */
/*               : u4LocalLdpEntityID - Ldp Entity ID                        */
/*                 u4LocalLdpEntityIndex - Ldp Entity Index                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendLdpDeRegReq (uGenAddr * pPeerAddr, INT4 i4PeerAddrType,
                      UINT4 u4LocalLdpEntityID, UINT4 u4LocalLdpEntityIndex)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_DEREG_WITH_LDP;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) pPeerAddr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == i4PeerAddrType)
#endif
    {
        MEMCPY ((VOID *) &LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo),
                (VOID *) pPeerAddr, IPV4_ADDR_LENGTH);
    }
    TmpPwVcEvtInfo.unEvtInfo.SsnInfo.u2AddrType = (UINT2) i4PeerAddrType;

    LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&TmpPwVcEvtInfo) = u4LocalLdpEntityID;
    LDP_L2VPN_EVT_SSN_LCL_ENT_INDEX (&TmpPwVcEvtInfo) = u4LocalLdpEntityIndex;

#ifdef MPLS_SIG_WANTED
    if (LdpProcessTgtEntityDeRegReqEvent
        (&LDP_L2VPN_EINFO_NOTIFINFO ((&TmpPwVcEvtInfo))) == LDP_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VPN: Deregistration with LDP failed\r\n");
    }
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPeerSession                                       */
/* Description   : This routine is to get the active LDP Peer session entry  */
/*                 deregister with the LDP                                   */
/* Input(s)      : u1AddrType - Peer Address type (IPv4/IPv6)                */
/*                 pAddr      - Peer Address                                 */
/* Output(s)     : ppSession - Peer session entry                            */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnGetPeerSession (UINT1 u1AddrType, uGenAddr * pAddr,
                     tPwVcActivePeerSsnEntry ** ppSession)
{
    tPwVcActivePeerSsnEntry *pSessionRb = NULL;
    tPwVcActivePeerSsnEntry *pTmpSession = NULL;

    pTmpSession = (VOID *) MemAllocMemBlk (L2VPN_PEER_SSN_POOL_ID);
    if (pTmpSession == NULL)
    {
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpSession, 0, sizeof (*pTmpSession));
    MEMCPY ((VOID *) &pTmpSession->PeerAddr, (VOID *) pAddr,
            sizeof (pTmpSession->PeerAddr));
    pTmpSession->u1AddrType = u1AddrType;
    pSessionRb =
        RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo), pTmpSession);

    *ppSession = pSessionRb;

    MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID, (VOID *) pTmpSession);
    return pSessionRb == NULL ? L2VPN_FAILURE : L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpPostEvent                                         */
/* Description   : This routine posts events to LDP module                   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpPostEvent (tL2VpnLdpPwVcEvtInfo * pPwVcEvtInfo)
{
    if (pPwVcEvtInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Null pointer received to post event\r\n");
        return L2VPN_FAILURE;
    }

#ifdef MPLS_SIG_WANTED
    if (LdpL2VpnEventHandler (pPwVcEvtInfo, L2VPN_LDP_EVENT) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Failed to send event to LDP\r\n");
        return L2VPN_FAILURE;
    }
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnAddRemVcToPeerSsnList                                */
/* Description   : This routine process label map message                    */
/* Input(s)      : pLblMsg - pointer to label message                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
L2VpnAddRemVcToPeerSsnList (tPwVcLblMsgEntry * pLblMsg)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pLblMsg->LblInfo.u2AddrType)
    {
        MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                pLblMsg->LblInfo.PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pLblMsg->LblInfo.u2AddrType)
#endif
    {
        GenU4Addr.Addr.u4Addr = pLblMsg->LblInfo.PeerAddr.u4Addr;
    }
    GenU4Addr.u2AddrType = (UINT2) (pLblMsg->LblInfo.u2AddrType);

    /* If session created before PwVc configured in l2vpn module then
     * create a new session and add the label info into it */
    if ((pPeerSsn = L2VpnUpdatePeerSsnList (GenU4Addr,
                                            L2VPN_SSN_ADD, NULL)) != NULL)
    {
        /* Start Clean up Timer  */
        /* Use u4Data to Store DLL List head. Useful while delete the
         * pLblMsg in timer expiry handler */
        pLblMsg->CleanupTimer.u4Event = L2VPN_CLEANUP_TMR_EXP_EVENT;
        pLblMsg->CleanupTimer.AppTimer.u4Data =
            (FS_ULONG) L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn);
        if (TmrStartTimer
            (L2VPN_TIMER_LIST_ID, &(pLblMsg->CleanupTimer.AppTimer),
             L2VPN_MAX_PWVC_CLEANUP_INT (gpPwVcGlobalInfo)) != TMR_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Start of Cleanup timer failed\r\n");
            return L2VPN_FAILURE;
        }

        TMO_DLL_Add (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn), &(pLblMsg->NextNode));

        /* We have received LDP mapping message from the peer which means
         * LDP session with that peer is UP. So, copy Entity Index over which
         * mapping message is received to PeerSsn and make the status as UP. */
        pPeerSsn->u4LdpEntityIndex = pLblMsg->LblInfo.u4LdpEntityIndex;
        pPeerSsn->u4Status = L2VPN_SESSION_UP;
        return L2VPN_SUCCESS;
    }
    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
               "Failed to find the session entry with peer\r\n");
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpLblMapMsg                                  */
/* Description   : This routine process label map message                    */
/* Input(s)      : pLblInfo - pointer to label info                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpLblMapMsg (tPwVcLblMsgInfo * pLblInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcLblMsgEntry   *pLblMsg = NULL;
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    INT4                i4Status = L2VPN_SUCCESS;
    INT1                i1CwMandate;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label mapping msg from IPv4- %d.%d.%d.%d %s IPv6 \r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)),
                Ip6PrintAddr (&(pLblInfo->PeerAddr.Ip6Addr)));

#else

    L2VPN_DBG4 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label mapping msg from %d.%d.%d.%d\r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)));

#endif
    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                pLblInfo->i1PwType,
                pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                pLblInfo->i1StatusCode, pLblInfo->u4Label);

    MEMCPY (&(GenU4Addr.Addr), &(pLblInfo->PeerAddr), sizeof (uGenU4Addr));

    GenU4Addr.u2AddrType = (UINT2) pLblInfo->u2AddrType;

    pPwVcEntry = L2VpnGetPwVcEntry (pLblInfo->i1PwOwner,
                                    pLblInfo->u4PwVcID,
                                    (UINT1) pLblInfo->i1PwType,
                                    pLblInfo->au1Agi, pLblInfo->au1Taii,
                                    pLblInfo->au1Saii, GenU4Addr);
    if (pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, label %d\r\n",
                    pLblInfo->u4PwVcID, pLblInfo->i1PwType,
                    pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                    pLblInfo->i1StatusCode, pLblInfo->u4Label);
    }

    /* if entry not found, wait for local configuration to happen */
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_SIG_FLAG,
                   "PW Not created for the received Label Mapping info\n");
        pLblMsg = (tPwVcLblMsgEntry *) MemAllocMemBlk (L2VPN_LBL_MSG_POOL_ID);

        if (pLblMsg == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Mem allocation failure - LblMsgEntry\r\n");
            return L2VPN_FAILURE;
        }

        MEMCPY ((UINT1 *) &pLblMsg->LblInfo,
                (UINT1 *) pLblInfo, sizeof (tPwVcLblMsgInfo));

        /* If received TAII is not matched, Label Release message
         * has to be sent with Unassigned/Unrecognized TAI status code
         * */

        if ((pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (L2VpnMatchPwVcEntryWithTai (pLblInfo->au1Agi, pLblInfo->au1Taii)
             == L2VPN_FAILURE))
        {
            pLblInfo->i1StatusCode = L2VPN_LDP_STAT_PWVC_UNASSIGNED_TAI;
            L2VpnReleaseWaitingRemLblMsg (pLblInfo);
            MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID, (UINT1 *) pLblMsg);
            return L2VPN_SUCCESS;
        }

        i4Status = L2VpnAddRemVcToPeerSsnList (pLblMsg);

        if (i4Status == L2VPN_FAILURE)
        {
            MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID, (UINT1 *) pLblMsg);

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "New Ssn addition failure\r\n");
            return L2VPN_FAILURE;
        }
        KW_FALSEPOSITIVE_FIX (pLblMsg);
        return L2VPN_SUCCESS;
    }

    /* Check PwType */
    if (L2VPN_PWVC_TYPE (pPwVcEntry) != pLblInfo->i1PwType)
    {
        /* Ignoring the map message - Action to be done */
        pPwVcEntry->u4OutVcLabel = pLblInfo->u4Label;
        pPwVcEntry->u1MapStatus |= L2VPN_MAP_RCVD;
        L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry,
                                   L2VPN_LDP_STAT_PWVC_GEN_MISCONF_ERR);
        return L2VPN_SUCCESS;
    }

    /* Mark as Map Rcvd */
    pPwVcEntry->u1MapStatus |= L2VPN_MAP_RCVD;

    /* Check MTU */
    if (L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry) != pLblInfo->u2IfMtu)
    {
        L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry,
                                   L2VPN_LDP_STAT_PWVC_GEN_MISCONF_ERR);
        return L2VPN_FAILURE;
    }

    if ((gi4MplsSimulateFailure == L2VPN_SIM_FAILURE_WDRAW_METHOD_NOT_SUPPORT)
        && (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)
        && (pLblInfo->u1RmtStatusCapable == L2VPN_PWVC_RMT_STAT_NOT_CAPABLE))
    {
        L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry,
                                   L2VPN_LDP_STAT_PWVC_LBL_WR_NOTSUPP);
        return L2VPN_SUCCESS;
    }

    /* Update the rcvd VCCV negotiation parameters */
    L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) =
        L2VpnVccvReverseValue (pLblInfo->u1LocalCCAdvert);
    L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) =
        L2VpnVccvReverseValue (pLblInfo->u1LocalCVAdvert);

    /* Check the local AGI, SAII and TAII types with the 
     * remote AGI, SAII and TAII types, if type doesn't match ignore the LMM.
     * This is to be done.
     * */
    /* Update PwVcEntry */

    L2VPN_PWVC_REMOTE_GRP_ID (pPwVcEntry) = pLblInfo->u4GroupID;
    L2VPN_PWVC_REQ_VLAN_ID (pPwVcEntry) = pLblInfo->u2ReqVlanId;

    L2VPN_PWVC_REMOTE_IFACE_MTU (pPwVcEntry) = pLblInfo->u2IfMtu;

    /*If PW is static 128 or 129, Remote and local lables must be same
     * else misconfigured label must be released*/
    if ((pPwVcEntry->bIsStaticPw == TRUE)
        &&
        (L2VPN_PWVC_OWNER_MANUAL != pPwVcEntry->i1PwVcOwner)
        && (pPwVcEntry->u4OutVcLabel != pLblInfo->u4Label))
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG, "Label Map recieved with incorrect"
                    "Remote Label %d while Configured Remote Label is %d\r\n",
                    pLblInfo->u4Label, pPwVcEntry->u4OutVcLabel);

        /* release Label recieved */
        pPwVcEntry->u4OutVcLabel = pLblInfo->u4Label;
        pPwVcEntry->u1MapStatus |= L2VPN_MAP_RCVD;
        L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry,
                                   L2VPN_LDP_STAT_PWVC_GEN_MISCONF_ERR);
        return L2VPN_FAILURE;
    }

    if (L2VpnPwSetOutVcLabel (pPwVcEntry, pLblInfo->u4Label) == L2VPN_FAILURE)
    {
        /*This return statement is commented, as on receiving same pwvc label from different peers,
         * pw configuration is skipped from here, as RB TREE ADDITION gets failed for already added label */

        /* return L2VPN_FAILURE; */
    }
    /*
       L2VPN_PWVC_OUTBOUND_VC_LABEL_REQ_ID(pPwVcEntry) = u4LblReqId;
       L2VPN_PWVC_REMOTE_IFACE_STRING(pPwVcEntry,x)\
     */

    L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) = (UINT1) (pLblInfo->u4PwStatusCode);

    /* Update Remote status capable */
    if (pLblInfo->u1RmtStatusCapable == L2VPN_PWVC_RMT_STAT_CAPABLE)
    {
        L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) = L2VPN_PWVC_RMT_STAT_CAPABLE;
    }
    else
    {
        L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) = L2VPN_PWVC_RMT_STAT_NOT_CAPABLE;
    }

    i1CwMandate = L2VpnGetControlWordMandate (L2VPN_PWVC_TYPE (pPwVcEntry));
    /* Control Word Negotiation Procedure */
    /* As per RFC 4447 section 6.1, When a label mapping message received with
     * C bit 0 for a pw type which requires control word mandatory, LSR
     * should send label release message wtih Illegal C bit status code
     * */

    if ((pLblInfo->i1ControlWord == L2VPN_FALSE) &&
        (i1CwMandate == L2VPN_PWVC_CTRLW_MANDATORY))
    {
        L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry,
                                   L2VPN_LDP_STAT_PWVC_ILLEGAL_C_BIT);
        /* No VC Possible  */
        return L2VPN_FAILURE;
    }

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);

    L2VPN_DBG4 (L2VPN_DBG_LVL_DBG_FLAG,
                "%s : MapStatus = %d, Control Word = %d, CWStatus = %d\n",
                __func__, pPwVcEntry->u1MapStatus,
                L2VPN_PWVC_CNTRL_WORD (pPwVcEntry), pPwVcEntry->i1CwStatus);

    /*-----------------------------------------------------------
     *         LblMap->C-bit | PW_CNTRL_WORD | "New" CW_STATUS
     *
     * Case 1:      TRUE     | Not_Preferred | WAITINGFORNEXTMSG
     * Case 2:      FALSE    | Preferred     | NOT_Present
     * Case 3:      FALSE    | Not_Preferred | NOT_Present
     * Case 4:      TRUE     | Preferred     | Present
     *------------------------------------------------------- */

    /* Case 1: Locally configured PW NOT PREFERRED to use the control word */
    if ((pLblInfo->i1ControlWord == L2VPN_TRUE) &&
        (L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_NOT_PREFERRED))
    {
        /* Ignoring the map message */
        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;
        /* Set Control Word Status */
        L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_WAITINGFORNEXTMSG;
        /* Signalling is used to setup PW VC, use respective sig method */
        if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
             (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)) &&
            (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
        {

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "%s : %d\r\n", __FUNCTION__, __LINE__);
            /* Check Ssn if already exits,
             * update tPwVcEntry if Remote already sent Lbl Msg */
            MEMCPY (&(PeerSsn.PeerAddr), &(pPwVcEntry->PeerAddr),
                    IPV4_ADDR_LENGTH);
            if (((pPeerSsn =
                  RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                             &PeerSsn)) != NULL)
                && (pPeerSsn->u4Status == L2VPN_SESSION_UP))
            {
                if ((pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT) == L2VPN_ZERO)
                {
                    i4Status =
                        L2VpnGetMplsEntryStatus (pPwVcEntry,
                                                 L2VPN_PSN_TNL_BOTH);

                    if (i4Status == L2VPN_SUCCESS)
                    {
                        pPwVcEntry->u1LocalStatus &=
                            (UINT1) (~L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT);

                        i4Status = L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry);
                    }
                    else
                    {
                        /* Set the 5th bit PSN Facing Tx fault */
                        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                            L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT;
                    }
                }
            }
        }
        return L2VPN_SUCCESS;
    }

    /* Case 2: Locally configured PW PREFERRED to use the control word */
    if ((pLblInfo->i1ControlWord == L2VPN_FALSE) &&
        (L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_PREFERRED))
    {
        if ((pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT) != L2VPN_ZERO)
        {
            /* Send a Lable Wdraw message with Status code -
             * "Wrong C Bit" */
            i4Status = L2VpnSendLdpPwVcLblWdrawMsg (pPwVcEntry,
                                                    L2VPN_LDP_STAT_PWVC_WRONG_C_BIT);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Withdraw msg failed\r\n");
            }

            /* Control word is not present for this PW: C Bit is reset
             * and agreed between the peers */
            L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_CWNOTPRESENT;

            /* update out-bound list */
            i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_UP);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Outbound list update Error\r\n");
            }
            else if ((pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT) == L2VPN_ZERO)
            {
                L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_CWNOTPRESENT;
            }

            i4Status = L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_BOTH);

            if (i4Status == L2VPN_SUCCESS)
            {
                i4Status = L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "Failed to send Lbl Map Msg\r\n");
                    /* return L2VPN_FAILURE; Need to be check at loaded condition  */
                }
                L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &= (UINT1)
                    (~L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT);
            }
            else
            {
                /* Set the 5th bit PSN Facing Tx fault */
                L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                    L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT;

                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "PSN tunnel is down, Lbl Map Msg cannot be sent\r\n");
                return L2VPN_FAILURE;
            }
        }
    }

    /* Case 3: Locally configured PW NOT_PREFERRED to use the control word and C-Bit recieved
     * is False, Update Control Word Status */
    if ((pLblInfo->i1ControlWord == L2VPN_FALSE) &&
        (L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_NOT_PREFERRED))
    {
        /*Case 3.1: If Label Map Already sent to peer B with C-bit = 1 & Label is
         * now recieved with C-Bit =0 from peer B as a result of Case 1 (Discard 
         * label MAP recieved from Peer A).
         * As Label Map with C-Bit = 1 is discarded at peer B, new Label Map must
         * be sent to the peer B by this peer A, hence resetting MapStatus*/

        if (L2VPN_PWVC_CW_STATUS (pPwVcEntry) == L2VPN_PWVC_CWPRESENT)
        {
            pPwVcEntry->u1MapStatus &= (UINT1) (~(L2VPN_MAP_SENT));
        }

        /* Control word is not present for this PW: C Bit is reset
         * and agreed between the peers */
        L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_CWNOTPRESENT;

    }

    /* Case 4: Locally configured PW PREFERRED to use the control word and C-Bit recieved
     * is True, Update Control Word Status */
    if ((pLblInfo->i1ControlWord == L2VPN_TRUE) &&
        (L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_PREFERRED))
    {
        /* Control word is present for this PW: C Bit is set
         * and agreed between the peers */
        L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_CWPRESENT;
    }

    /* Skip the further processing if the PW is not active */
    if ((L2VPN_PWVC_ROW_STATUS (pPwVcEntry) != L2VPN_PWVC_ACTIVE) ||
        (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) != L2VPN_PWVC_ADMIN_UP))
    {
        return L2VPN_SUCCESS;
    }

    /* Selecting Matching capabilities between local and remote */
    if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_VCCV_CAPABLE)
    {
        L2VpnVccvSelectMatchingCCTypes (pPwVcEntry);
        L2VpnVccvSelectMatchingCVTypes (pPwVcEntry);
    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "VCCV is not enabled\r\n");
    }

    /* Signalling is used to setup PW VC, use respective sig method */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)) &&
        (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "%s : %d\r\n", __FUNCTION__, __LINE__);
        /* Check Ssn if already exits, 
         * update tPwVcEntry if Remote already sent Lbl Msg */
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY (&(PeerSsn.PeerAddr), &(pPwVcEntry->PeerAddr),
                    IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY (&(PeerSsn.PeerAddr), &(pPwVcEntry->PeerAddr),
                    IPV4_ADDR_LENGTH);
        }
        PeerSsn.u1AddrType = (UINT1) pPwVcEntry->i4PeerAddrType;

        if (((pPeerSsn =
              RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                         &PeerSsn)) != NULL)
            && (pPeerSsn->u4Status == L2VPN_SESSION_UP))
        {
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "%s : %d\r\n", __FUNCTION__, __LINE__);
                i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);
                if (pPwVcEntry->u1GrSyncStatus == L2VPN_GR_LOCAL_SYNCHRONIZED)
                {
                    pPwVcEntry->u1GrSyncStatus = L2VPN_GR_FULLY_SYNCHRONIZED;
                    pPeerSsn->u4NoOfStalePw--;
                }
            }

            if (i4Status == L2VPN_FAILURE)
            {
                return L2VPN_FAILURE;
            }
        }
    }

    if ((pPeerSsn != NULL) &&
        (pPeerSsn->u1RecoveryProgress == L2VPN_GR_RECOVERY_IN_PROGRESS) &&
        (pPeerSsn->u4NoOfStalePw == L2VPN_ZERO))
    {
        pPeerSsn->u1RecoveryProgress = L2VPN_GR_COMPLETED;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "pPwVcEntry->u1MapStatus %x\r\n", pPwVcEntry->u1MapStatus);

    if ((pPwVcEntry->u1MapStatus & L2VPN_REL_WAIT) != L2VPN_REL_WAIT)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Going to call L2VpnUpdatePwVcOperStatus\r\n");

        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
        }
    }
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG, "%s : %d\r\n", __FUNCTION__, __LINE__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpLblMapReqMsg                               */
/* Description   : This routine process label map request message            */
/* Input(s)      : pLblInfo - pointer to label info                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpLblMapReqMsg (tPwVcLblMsgInfo * pLblInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    INT4                i4Status;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label mapping request msg from IPv4-%d.%d.%d.%d IPv6 %s \r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)),
                Ip6PrintAddr (&(pLblInfo->PeerAddr.Ip6Addr)));

#else
    L2VPN_DBG4 (L2VPN_DBG_DBG_SIG_FLAG,
                "Rcvd label mapping request msg from %d.%d.%d.%d\r\n",
                (*(UINT1 *) (&(pLblInfo->PeerAddr.u4Addr))),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_ONE)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_TWO)),
                (*((UINT1 *) (&(pLblInfo->PeerAddr.u4Addr)) + L2VPN_THREE)));
#endif

    MEMCPY (&(GenU4Addr.Addr), &(pLblInfo->PeerAddr), sizeof (uGenU4Addr));

    GenU4Addr.u2AddrType = (UINT2) pLblInfo->u2AddrType;

    pPwVcEntry = L2VpnGetPwVcEntry (pLblInfo->i1PwOwner,
                                    pLblInfo->u4PwVcID,
                                    (UINT1) pLblInfo->i1PwType,
                                    pLblInfo->au1Agi, pLblInfo->au1Taii,
                                    pLblInfo->au1Saii, GenU4Addr);
    if (pLblInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        L2VPN_DBG6 (L2VPN_DBG_DBG_SIG_FLAG,
                    "vc id %d, vc type %d, cbit %d, group id %d, status %d, mtu %d\r\n",
                    pLblInfo->u4PwVcID, pLblInfo->i1PwType,
                    pLblInfo->i1ControlWord, pLblInfo->u4GroupID,
                    pLblInfo->i1StatusCode, pLblInfo->u2IfMtu);
    }

    /* if entry not found, wait for local configuration to happen */
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "PwVc Entry not present\r\n");
        return L2VPN_FAILURE;
    }

    /* Update PwVcEntry */
    L2VPN_PWVC_REMOTE_GRP_ID (pPwVcEntry) = pLblInfo->u4GroupID;
    L2VPN_PWVC_REMOTE_IFACE_MTU (pPwVcEntry) = pLblInfo->u2IfMtu;

    L2VpnPwSetInVcLabel (pPwVcEntry, pLblInfo->u4Label);

    /* Update the rcvd VCCV negotiation parameters */
    L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) =
        L2VpnVccvReverseValue (pLblInfo->u1LocalCCAdvert);
    L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) =
        L2VpnVccvReverseValue (pLblInfo->u1LocalCVAdvert);

    L2VPN_PWVC_LABEL_REQID (pPwVcEntry) = pLblInfo->u4LblReqId;

    /* Send the Lbl Map msg */
    if ((pPwVcEntry->u1MapStatus & L2VPN_MAP_SENT) == L2VPN_ZERO)
    {
        /* update out-bound list */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Outbound list update Error\r\n");
        }

        i4Status = L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_BOTH);
        if (i4Status == L2VPN_SUCCESS)
        {
            i4Status = L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Failed to send Lbl Map Msg\r\n");
                /* return L2VPN_FAILURE; Need to be check at loaded condition  */
            }
            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT);
        }
        else
        {
            /* Set the 5th bit PSN Facing Tx fault */
            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT;

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "PSN tunnel is down, Lbl Map Msg cannot be sent\r\n");
            return L2VPN_FAILURE;
        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnEnqueueMsgToL2VpnQ                                   */
/* Description   : Does internal posting of message received from LDP and    */
/*                 triggers an internal event                                */
/* Input(s)      : u1EvtType - Event type                                    */
/*                 pL2VpnQMsg - Message to be enqueued to L2VPN task         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT1
L2VpnEnqueueMsgToL2VpnQ (UINT1 u1EvtType, tL2VpnQMsg * pL2VpnQMsg)
{

    UINT1              *pu1TmpMsg = NULL;
    INT1               *pi1IfDes = NULL;

    if (L2VPN_INITIALISED != TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VPN module is in shutdown state\r\n");
        return L2VPN_FAILURE;
    }

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Mem allocation failure - post internal event\r\n");
        return L2VPN_FAILURE;
    }

    if (pL2VpnQMsg->L2VpnEvtInfo.PwVcSigEvtInfo.unEvtInfo.LdpEvtInfo.
        u4EvtType == L2VPN_LDP_LBL_MSG_EVT)
    {
        if (pL2VpnQMsg->L2VpnEvtInfo.PwVcSigEvtInfo.unEvtInfo.LdpEvtInfo.
            unEvtInfo.LblInfo.pi1LocalIfString != NULL)
        {
            pi1IfDes = (INT1 *) MemAllocMemBlk (L2VPN_IF_DESC_POOL_ID);

            if (pi1IfDes == NULL)
            {
                MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);

                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Mem allocation failure - If descr string\r\n");
                return L2VPN_FAILURE;
            }
            MEMCPY (pi1IfDes,
                    pL2VpnQMsg->L2VpnEvtInfo.PwVcSigEvtInfo.unEvtInfo.
                    LdpEvtInfo.unEvtInfo.LblInfo.pi1LocalIfString,
                    STRLEN (pL2VpnQMsg->L2VpnEvtInfo.PwVcSigEvtInfo.unEvtInfo.
                            LdpEvtInfo.unEvtInfo.LblInfo.pi1LocalIfString));
            pL2VpnQMsg->L2VpnEvtInfo.PwVcSigEvtInfo.unEvtInfo.LdpEvtInfo.
                unEvtInfo.LblInfo.pi1LocalIfString = pi1IfDes;
        }
    }
    MEMCPY (pu1TmpMsg, (UINT1 *) pL2VpnQMsg, sizeof (tL2VpnQMsg));

    if (OsixQueSend (L2VPN_QID, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Failed to EnQ msg to L2VpnQ\r\n");

        MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);

        if (pi1IfDes != NULL)
        {
            MemReleaseMemBlock (L2VPN_IF_DESC_POOL_ID,
                                (UINT1 *) (VOID *) pi1IfDes);
        }

        return L2VPN_FAILURE;
    }

    if (OsixEvtSend (L2VPN_TSK_ID, u1EvtType) != OSIX_SUCCESS)

    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);

        if (pi1IfDes != NULL)
        {
            MemReleaseMemBlock (L2VPN_IF_DESC_POOL_ID,
                                (UINT1 *) (VOID *) pi1IfDes);
        }

        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Failed to send event to L2Vpn\r\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpEventHandler                                      */
/* Description   : Handles messages received from signalling module (LDP)    */
/*                 and enqueues received message to L2VPN queue              */
/* Input(s)      : pMsg - Pointer to message received from signalling module */
/*                 u4Event - Event received from signalling module           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpEventHandler (VOID *pMsg, UINT4 u4Event)
{
    tL2VpnQMsg          L2VpnQMsg;

    L2VPN_SUPPRESS_WARNING (u4Event);

    if (L2VPN_ADMIN_STATUS != L2VPN_ADMIN_UP)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2Vpn in Admin down state\r\n");
        return L2VPN_FAILURE;
    }

    L2VPN_QMSG_TYPE = L2VPN_PWVC_SIG_EVENT;
    L2VpnQMsg.L2VpnEvtInfo.PwVcSigEvtInfo.u4SigType = L2VPN_SIG_LDP;
    MEMCPY (&(L2VpnQMsg.L2VpnEvtInfo.PwVcSigEvtInfo.unEvtInfo.
              LdpEvtInfo), pMsg, sizeof (tL2VpnLdpPwVcEvtInfo));

    if (L2VpnEnqueueMsgToL2VpnQ (L2VPN_MSG_EVENT, &L2VpnQMsg) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Failed to EnQ Signalling event to L2VPN Task\r\n");
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpGetEventPwFec                                     */
/* Description   : This function allocates the memory for PW Fec event       */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to tPwRedDataEvtPwFec                             */
/*****************************************************************************/
tPwRedDataEvtPwFec *
L2VpnLdpGetEventPwFec (VOID)
{
    tPwRedDataEvtPwFec *pEvtPwFec = NULL;

    if (L2VPN_ADMIN_STATUS != L2VPN_ADMIN_UP)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN in ADMIN.DOWN state\n");
        return L2VPN_FAILURE;
    }

    pEvtPwFec = (VOID *) MemAllocMemBlk (L2VPN_ICCP_PWVC_REQUESTS_POOL_ID);
    if (pEvtPwFec == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "PRCS: Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwFec\n");
        return NULL;
    }

    return pEvtPwFec;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpCleanEventPwFec                                   */
/* Description   : This function releases the memory allocated for PW Fec    */
/* Input(s)      : None                                                      */
/* Output(s)     : ppEvtPwFec - Pointer to the Event PW Fec                  */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpCleanEventPwFec (VOID *pEvtPwFec)
{
    MemReleaseMemBlock (L2VPN_ICCP_PWVC_REQUESTS_POOL_ID, pEvtPwFec);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnGetEventPwData                                    */
/* Description   : This function allocates memory for PW Data event          */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : pointer to tPwRedDataEvtPwData                            */
/*****************************************************************************/
tPwRedDataEvtPwData *
L2VpnLdpGetEventPwData (VOID)
{
    tPwRedDataEvtPwData *pEvtPwData = NULL;

    if (L2VPN_ADMIN_STATUS != L2VPN_ADMIN_UP)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN in ADMIN.DOWN state\n");
        return L2VPN_FAILURE;
    }

    pEvtPwData = (VOID *) MemAllocMemBlk (L2VPN_ICCP_PWVC_DATA_ENTRIES_POOL_ID);
    if (pEvtPwData == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "PRCS: Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwData\n");
        return L2VPN_FAILURE;
    }

    return pEvtPwData;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpCleanEventPwData                                  */
/* Description   : This function releases the memory allocated for PW Data   */
/* Input(s)      : None                                                      */
/* Output(s)     : ppEvtPwFec - Pointer to the Event PW Fec                  */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpCleanEventPwData (VOID *pEvtPwData)
{
    MemReleaseMemBlock (L2VPN_ICCP_PWVC_DATA_ENTRIES_POOL_ID, pEvtPwData);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnUpdatePeerSsnList                                    */
/* Description   : This routine updates the peer session list                */
/* Input(s)      : pL2vpInEntry - pointer to PwVc Entry/Redundancy node entry*/
/*               : u1Action - Action to be performed                         */
/*               : u4PeerAddr - Peer Address                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : tPwVcActivePeerSsnEntry pointer on success and NULL on    */
/*                 failure                                                   */
/*****************************************************************************/
tPwVcActivePeerSsnEntry *
L2VpnUpdatePeerSsnList (tGenU4Addr GenU4Addr, UINT1 u1Action,
                        VOID *pL2vpInEntry)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcLblMsgEntry   *pLblEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    VOID               *pPsnEntry = NULL;
    tPwVcEntry         *pPwVcInEntry = NULL;
    tL2vpnRedundancyNodeEntry *pNodeEntry = NULL;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                GenU4Addr.Addr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == GenU4Addr.u2AddrType)
#endif
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                &(GenU4Addr.Addr.u4Addr), IPV4_ADDR_LENGTH);
    }
    PeerSsn.u1AddrType = (UINT1) GenU4Addr.u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);
    switch (u1Action)
    {
        case L2VPN_SSN_CREATE:
        case L2VPN_SSN_ADD:
            if (pPeerSsn != NULL)
            {
                L2VPN_DBG (DBG_ERR_NCRT,
                           "Session already exists and in up state\r\n");
                return pPeerSsn;
            }

            pPeerSsn = (tPwVcActivePeerSsnEntry *)
                MemAllocMemBlk (L2VPN_PEER_SSN_POOL_ID);

            if (pPeerSsn == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Mem allocation failure - PeerSsnEntry\r\n");
                return NULL;
            }

            MEMSET (pPeerSsn, 0, sizeof (tPwVcActivePeerSsnEntry));
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
            {
                MEMCPY (&(pPeerSsn->PeerAddr),
                        GenU4Addr.Addr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            }
            else if (MPLS_IPV4_ADDR_TYPE == GenU4Addr.u2AddrType)
#endif
            {
                MEMCPY (&(pPeerSsn->PeerAddr),
                        &(GenU4Addr.Addr.u4Addr), IPV4_ADDR_LENGTH);
            }
            pPeerSsn->u1AddrType = (UINT1) GenU4Addr.u2AddrType;

            /* Initialise Label msg list */
            if (TMO_DLL_Count (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn)) == 0)
            {
                TMO_DLL_Init (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
            }

            /* Initialise Pw List */
            if (TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn)) == 0)
            {
                TMO_DLL_Init (L2VPN_PWVC_LIST (pPeerSsn));
            }
            /* Initialise ICCP Node list */
            if (u1Action == L2VPN_SSN_CREATE)
            {
                TMO_DLL_Init (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn));
            }

            /* Add SSN to RBTree */
            if (RBTreeAdd (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                           pPeerSsn) == RB_FAILURE)
            {
                MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID, (UINT1 *) pPeerSsn);
                return NULL;
            }
            pPeerSsn->u4Status = (u1Action == L2VPN_SSN_CREATE) ?
                L2VPN_SESSION_DOWN : L2VPN_SESSION_UP;

            pPeerSsn->i1SsnCreateReq = L2VPN_FALSE;
            pPeerSsn->u1PeerSsnLdpRegStatus = L2VPN_FALSE;

#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG6 (L2VPN_DBG_LVL_CRT_FLAG,
                        "Peer IPv4- %d.%d.%d.%d IPv6 %s created and added to the "
                        "Peer List. Status is %d\r\n",
                        pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)),
                        pPeerSsn->u4Status);
#else

            L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                        "Peer %d.%d.%d.%d created and added to the "
                        "Peer List. Status is %d\r\n",
                        pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[3], pPeerSsn->u4Status);
#endif
            pPeerSsn->u1RecoveryProgress = L2VPN_GR_NOT_STARTED;

#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG6 (L2VPN_DBG_GRACEFUL_RESTART,
                        "LDP Session with Peer IPv4- %d.%d.%d.%d"
                        "IPv6 %s is set to "
                        "%d Recovery Progress State\r\n",
                        pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)),
                        pPeerSsn->u1RecoveryProgress);

#else
            L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                        "LDP Session with Peer %d.%d.%d.%d is set to "
                        "%d Recovery Progress State\r\n",
                        pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                        pPeerSsn->u1RecoveryProgress);
#endif
            break;

        case L2VPN_SSN_DELETE:
            if (pPeerSsn == NULL)
            {
                L2VPN_DBG (DBG_ERR_NCRT, "No Session in peer ssn list.");
                return NULL;
            }
            pLblEntry = (tPwVcLblMsgEntry *) TMO_DLL_First
                (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
            while (pLblEntry != NULL)
            {
                /* Stop Clean up Timer */
                TmrStopTimer (L2VPN_TIMER_LIST_ID,
                              &(pLblEntry->CleanupTimer.AppTimer));
                TMO_DLL_Delete (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                                &(pLblEntry->NextNode));
                MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID, (UINT1 *) pLblEntry);
                pLblEntry = (tPwVcLblMsgEntry *) TMO_DLL_First
                    (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
            }
            if (TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn)) == 0)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Peer IPv4 - %d.%d.%d.%d IPv6 %s"
                            "to be deleted from peer list "
                            "and to be freed\r\n",
                            pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                            Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)));

#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Peer %d.%d.%d.%d to be deleted from peer list "
                            "and to be freed\r\n",
                            pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[3]);
#endif
                RBTreeRem (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                           pPeerSsn);
                MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID, (UINT1 *) pPeerSsn);
                pPeerSsn = NULL;
            }
            else
            {
                pPeerSsn->u4Status = L2VPN_SESSION_DOWN;
                pPeerSsn->i1SsnCreateReq = L2VPN_FALSE;
                pPeerSsn->u1PeerSsnLdpRegStatus = L2VPN_FALSE;
            }
            break;
        case L2VPN_PWVC_ADD_IN_SESSION:
            if (pPeerSsn != NULL)
            {
                pPwVcInEntry = (tPwVcEntry *) pL2vpInEntry;

                /* TODO Add PWID Order for better snmp walk */
                if (TMO_DLL_Is_Node_In_List (&(pPwVcInEntry->SsnNode)) == 0)
                {
                    TMO_DLL_Add (L2VPN_PWVC_LIST (pPeerSsn),
                                 &(pPwVcInEntry->SsnNode));
#ifdef MPLS_IPV6_WANTED

                    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                                "PW %d added in LDP Session list of "
                                "Peer IPv4-  %d.%d.%d.%d IPv6 %s \r\n",
                                pPwVcInEntry->u4PwVcIndex,
                                pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                                pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                                pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                                pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                                Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)));

#else
                    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG,
                                "PW %d added in LDP Session list of "
                                "Peer %d.%d.%d.%d\r\n",
                                pPwVcInEntry->u4PwVcIndex,
                                pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                                pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                                pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                                pPeerSsn->PeerAddr.au1Ipv4Addr[3]);
#endif
                }

                pPsnEntry = pPwVcInEntry->pPSNEntry;
                if (pPsnEntry != NULL)
                {
                    pPwVcMplsEntry = (tPwVcMplsEntry *) pPsnEntry;
                    pPwVcMplsEntry->u4LocalLdpEntityIndex =
                        pPeerSsn->u4LdpEntityIndex;

                    L2VPN_DBG2 (L2VPN_DBG_DBG_SSN_FLAG,
                                "Entity Index of PW %d is %d\n",
                                pPwVcInEntry->u4PwVcIndex,
                                pPwVcMplsEntry->u4LocalLdpEntityIndex);
                }
            }
            break;
        case L2VPN_PWVC_DELETE_IN_SESSION:
        {
            pPwVcInEntry = (tPwVcEntry *) pL2vpInEntry;

            if (pPeerSsn == NULL)
            {
                break;
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "PW[%u]\n",
                        L2VPN_PWVC_INDEX (pPwVcInEntry));

            TMO_DLL_Delete (L2VPN_PWVC_LIST (pPeerSsn),
                            &(pPwVcInEntry->SsnNode));

#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG6 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PW %d deleted from LDP Session list of "
                        "Peer IPv4- %d.%d.%d.%d IPv6 %s \r\n",
                        pPwVcInEntry->u4PwVcIndex,
                        pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)));
#else

            L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PW %d deleted from LDP Session list of "
                        "Peer %d.%d.%d.%d\r\n",
                        pPwVcInEntry->u4PwVcIndex,
                        pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                        pPeerSsn->PeerAddr.au1Ipv4Addr[3]);
#endif
            if (TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn)) == 0 &&
                TMO_DLL_Count (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn)) == 0)
            {
                if (L2VPN_SESSION_PWVC_DEREG_REQ (pPwVcInEntry) == FALSE)
                {
                    L2VPN_DBG (DBG_ERR_NCRT,
                               "Deregistration with LDP is failed.");
                }
            }

            if ((TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn)) == 0) &&
                (TMO_DLL_Count (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn)) == 0)
                && (TMO_DLL_Count (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn)) == 0))
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Peer %d.%d.%d.%d to be deleted from Peer list "
                            "and to be freed\r\n",
                            pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                            Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)));

#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Peer %d.%d.%d.%d to be deleted from Peer list "
                            "and to be freed\r\n",
                            pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                            pPeerSsn->PeerAddr.au1Ipv4Addr[3]);
#endif
                RBTreeRem (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                           pPeerSsn);
                MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID, (UINT1 *) pPeerSsn);
                pPeerSsn = NULL;
            }
            break;
        }

        case L2VPN_ICCP_ADD_IN_SESSION:
        {
            pNodeEntry = (tL2vpnRedundancyNodeEntry *) pL2vpInEntry;

            if (pPeerSsn == NULL)
            {
                break;
            }

            if (TMO_DLL_Is_Node_In_List (&pNodeEntry->SessionNode) == 0)
            {
                TMO_DLL_Add (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn),
                             &pNodeEntry->SessionNode);
            }
            break;
        }
        case L2VPN_ICCP_DELETE_IN_SESSION:
        {
            pNodeEntry = (tL2vpnRedundancyNodeEntry *) pL2vpInEntry;

            if (pPeerSsn == NULL)
            {
                break;
            }

            TMO_DLL_Delete (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn),
                            &pNodeEntry->SessionNode);

            if (TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn)) == 0 &&
                TMO_DLL_Count (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn)) == 0)
            {
                if (L2VPN_SESSION_ICCP_DEREG_REQ (pNodeEntry) == FALSE)
                {
                    L2VPN_DBG (DBG_ERR_NCRT,
                               "Deregistration with LDP is failed.");
                }
            }

            if ((TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn)) == 0) &&
                (TMO_DLL_Count (L2VPN_SESSION_ICCP_NODE_LIST (pPeerSsn)) == 0)
                && (TMO_DLL_Count (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn)) == 0))
            {
                RBTreeRem (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                           pPeerSsn);
                MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID, (UINT1 *) pPeerSsn);
                pPeerSsn = NULL;
            }
            break;
        }

        default:
            break;
    }

    return pPeerSsn;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckSsnFromPeerSsnList                              */
/* Description   : This routine checks if session exists in peer sess list   */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 b1IsIccpSession - To differentiate ICCP session and others*/
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnCheckSsnFromPeerSsnList (VOID *pL2vpnEntry, BOOL1 b1IsIccpSession)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcLblMsgEntry   *pLblEntry = NULL;
    tPwVcLblMsgInfo    *pLblMsgInfo = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    BOOL1               bLblInfoFound = L2VPN_FALSE;
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyNodeEntry *pNodeEntry = NULL;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    if (b1IsIccpSession)
    {
        pNodeEntry = (tL2vpnRedundancyNodeEntry *) pL2vpnEntry;
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pNodeEntry->u1AddrType)
        {
            MEMCPY (&(PeerSsn.PeerAddr), &(pNodeEntry->Addr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pNodeEntry->u1AddrType)
#endif
        {
            MEMCPY (&(PeerSsn.PeerAddr), &(pNodeEntry->Addr), IPV4_ADDR_LENGTH);
        }
        PeerSsn.u1AddrType = pNodeEntry->u1AddrType;
    }
    else
    {
        pPwVcEntry = (tPwVcEntry *) pL2vpnEntry;
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY (&(PeerSsn.PeerAddr), &(pPwVcEntry->PeerAddr),
                    IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY (&(PeerSsn.PeerAddr), &(pPwVcEntry->PeerAddr),
                    IPV4_ADDR_LENGTH);
        }
        PeerSsn.u1AddrType = (UINT1) pPwVcEntry->i4PeerAddrType;
    }

    if (((pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                                &PeerSsn)) != NULL) &&
        (pPeerSsn->u4Status == L2VPN_SESSION_UP) && (pPwVcEntry != NULL))
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (pPwVcEntry->PeerAddr.Ip6Addr.u1_addr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                                GenU4Addr.Addr.u4Addr);
            GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Session Present for the peer %x and session is up\n",
                    GenU4Addr.Addr.u4Addr);

        /* T-LDP Session is found and UP. So, update the outbound list. */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Outbound List failed\r\n");
            return L2VPN_FAILURE;
        }

        i4Status = L2VPN_SUCCESS;
        TMO_DLL_Scan (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                      pLblEntry, tPwVcLblMsgEntry *)
        {
            pLblMsgInfo = (tPwVcLblMsgInfo *) & (pLblEntry->LblInfo);
            if ((pPwVcEntry->i1PwVcOwner != pLblMsgInfo->i1PwOwner) ||
                (pLblMsgInfo->i1PwType != pPwVcEntry->i1PwVcType))
            {
                continue;
            }

            if ((pLblMsgInfo->i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG) &&
                (pLblMsgInfo->u4PwVcID == pPwVcEntry->u4PwVcID))
            {
                bLblInfoFound = L2VPN_TRUE;
            }
            else if ((pLblMsgInfo->i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
                     (MEMCMP (pLblMsgInfo->au1Agi, pPwVcEntry->au1Agi,
                              pLblMsgInfo->u1AgiLen) == L2VPN_ZERO) &&
                     (MEMCMP (pLblMsgInfo->au1Saii, pPwVcEntry->au1Taii,
                              pLblMsgInfo->u1SaiiLen) == L2VPN_ZERO) &&
                     (MEMCMP (pLblMsgInfo->au1Taii, pPwVcEntry->au1Saii,
                              pLblMsgInfo->u1TaiiLen) == L2VPN_ZERO))
            {
                bLblInfoFound = L2VPN_TRUE;
            }
            else
            {
                bLblInfoFound = L2VPN_FALSE;
            }

            if (bLblInfoFound == L2VPN_TRUE)
            {
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG1 (L2VPN_DBG_DBG_SIG_FLAG,
                            "Label mapping info found for the PW with "
                            "Peer %s\n",
                            Ip6PrintAddr (&GenU4Addr.Addr.Ip6Addr));
#else
                L2VPN_DBG1 (L2VPN_DBG_DBG_SIG_FLAG,
                            "Label mapping info found for the PW with "
                            "Peer %x\n", GenU4Addr.Addr.u4Addr);
#endif

                /* Label msg exists, peer already sent label, update PW */
                i4Status = L2VpnProcessLdpLblMapMsg (pLblMsgInfo);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "Label map message processing failed\r\n");
                }
                /* Stop Clean up  Timer */
                TmrStopTimer (L2VPN_TIMER_LIST_ID,
                              &(pLblEntry->CleanupTimer.AppTimer));
                /* delete entry from lbl msg list */
                TMO_DLL_Delete (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                                &(pLblEntry->NextNode));

                if (MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID,
                                        (UINT1 *) pLblEntry) == MEM_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "Mem release failure - label msg pool\r\n");
                }
                break;
            }
        }
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == PeerSsn.u1AddrType)
    {
        MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                &(PeerSsn.PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == PeerSsn.u1AddrType)
#endif
    {
        MEMCPY (&(GenU4Addr.Addr.u4Addr), &(PeerSsn.PeerAddr),
                IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) PeerSsn.u1AddrType;

    if (pPeerSsn == NULL)
    {
        if ((pPeerSsn = L2VpnUpdatePeerSsnList (GenU4Addr, L2VPN_SSN_CREATE,
                                                NULL)) == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Session Create Failed \r\n");
        }
    }

    if (pPeerSsn != NULL)
    {
        if (pPeerSsn->i1SsnCreateReq == L2VPN_FALSE)
        {
            if (pNodeEntry != NULL)
            {
                pNodeEntry->pSession = pPeerSsn;
            }

            if ((pPwVcEntry != NULL) &&
                (L2VPN_SESSION_PWVC_REG_REQ (pPwVcEntry) == FALSE))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Session create request failed for PW\r\n");
                i4Status = L2VPN_FAILURE;
            }
            else if ((pNodeEntry != NULL) &&
                     (L2VPN_SESSION_ICCP_REG_REQ (pNodeEntry) == FALSE))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Session create request failed for ICCP\r\n");
                i4Status = L2VPN_FAILURE;
            }
            pPeerSsn->i1SsnCreateReq = L2VPN_TRUE;
        }
        /* update Peer Ssn List */
        if ((pPwVcEntry != NULL) &&
            (L2VpnUpdatePeerSsnList (GenU4Addr, L2VPN_PWVC_ADD_IN_SESSION,
                                     pPwVcEntry) == NULL))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Add Pw to Sesssion List Failed \r\n");
        }
        else if ((pNodeEntry != NULL) &&
                 (L2VpnUpdatePeerSsnList (GenU4Addr, L2VPN_ICCP_ADD_IN_SESSION,
                                          pNodeEntry) == NULL))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Add Node to Sesssion List Failed \r\n");
        }

        if (pPeerSsn->u4Status == L2VPN_SESSION_DOWN)
        {
            /* Since LDP Session is DOWN, Label advertisement messages
             * cannot be sent.
             * 
             * When LDP indicates SSN UP Event or label mapping message is received
             * from a peer, LDP Session with the peer is considered as UP and
             * label advertisement messages can be sent. */
            i4Status = L2VPN_FAILURE;
        }
    }
    return i4Status;
}

VOID
L2vpUpdateLdpUpStatus ()
{
    if (L2VPN_INITIALISED != TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VPN module is in shutdown state\r\n");
        return;
    }

    if (OsixEvtSend (L2VPN_TSK_ID, L2VPN_LDP_UP_EVT) != OSIX_SUCCESS)

    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Failed to  send LDP up event to L2Vpn\r\n");
    }
}

/*****************************************************************************/
/* Function Name : L2VpnHandlePendingMsgsToLdp                               */
/* Description   : This function processes the PW Entries in Pending Message */
/*                 sends Label Mapping Message or Label Withdrawl message or */
/*                 Label Release message or Notification Message based on    */
/*                 the last message send type set.                           */
/*                                                                           */
/*                 Then the PW Oper Status is Updated. If the PW to be       */
/*                 destroyed, destroy is initiated                           */
/*                                                                           */
/*                 Pending Message List is count is decremented, Only 100    */
/*                 PW Entries are processed in this flow. After              */
/*                 processing all PW Entries or 100 entries, if pending list */
/*                 count is zero and L2VPN Module Down is initiated, L2VPN   */
/*                 Module is disabled. If there are some more entries to be  */
/*                 processed Pending Message List Timer is started again.    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandlePendingMsgsToLdp ()
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pTmpPwVcEntry = NULL;
    UINT4               u4MsgType = 0;
    INT4                i4Status = L2VPN_FAILURE;
    UINT1               u1Count = 0;
    UINT1               u1Event = 0;

    gpL2VpnGlobalInfo->b1IsSendToLdpTmrStarted = FALSE;
    gpL2VpnGlobalInfo->b1IsSendToLdpTmrExpHandled = TRUE;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
               "L2VpnHandlePendingMsgsToLdp: ENTRY\r\n");

    UTL_DLL_OFFSET_SCAN (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                         pPwVcEntry, pTmpPwVcEntry, tPwVcEntry *)
    {
        if (u1Count >= L2VPN_MAX_MSG_TO_LDP_COUNT)
        {
            break;
        }

        if (gpL2VpnGlobalInfo->u1SendToLdpMsgSpacingCount > 0)
        {
            gpL2VpnGlobalInfo->u1SendToLdpMsgSpacingCount--;
        }

        u4MsgType = pPwVcEntry->u1SendToLdpMsgType;

        if (u4MsgType == L2VPN_LDP_LBL_MAP_MSG)
        {
            pPwVcEntry->u1MapStatus &= (UINT1) (~L2VPN_REL_WAIT);
            i4Status = L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry);
            u1Event = L2VPN_PWVC_UP;
        }
        else if (u4MsgType == L2VPN_LDP_LBL_WDRAW_MSG)
        {
            if (L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
            {
                i4Status = L2VpnSendLdpPwVcLblWdrawMsg (pPwVcEntry, L2VPN_ZERO);
            }

            if (((L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN) ||
                 (pPwVcEntry->i1RowStatus == DESTROY) ||
                 (pPwVcEntry->i1RowStatus == NOT_IN_SERVICE) ||
                 (pPwVcEntry->i1AdminStatus == L2VPN_PWVC_ADMIN_DOWN)) &&
                (L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry)
                 != L2VPN_INVALID_LABEL))
            {
                i4Status = L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO);
            }

            u1Event = L2VPN_PWVC_DOWN;
        }
        else if (u4MsgType == L2VPN_LDP_NOTIF_MSG_EVT)
        {
            i4Status = L2VpnSendLdpPwVcNotifMsg (pPwVcEntry);

            if (pPwVcEntry->u1LocalStatus == L2VPN_PWVC_STATUS_FORWARDING)
            {
                u1Event = L2VPN_PWVC_UP;
            }
            else if ((L2VPN_IS_PSN_FACE_TX_FAULT (pPwVcEntry)) ||
                     (L2VPN_IS_PSN_FACE_RX_FAULT (pPwVcEntry)))
            {
                u1Event = L2VPN_PWVC_PSN_TNL_DOWN;
            }
            else
            {
                u1Event = L2VPN_PWVC_DOWN;
            }
        }
        else if ((u4MsgType == L2VPN_LDP_LBL_REL_MSG) &&
                 (L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry)
                  != L2VPN_INVALID_LABEL))
        {
            i4Status = L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO);

            u1Event = L2VPN_PWVC_DOWN;
        }

        if (i4Status == L2VPN_FAILURE)
        {
            L2VPN_DBG2 (L2VPN_DBG_FNC_SIG_FLAG,
                        "Sending Message %d to LDP for PW %d Failed\r\n",
                        u4MsgType, pPwVcEntry->u4PwVcIndex);
        }
        else
        {
            L2VPN_DBG2 (L2VPN_DBG_FNC_SIG_FLAG,
                        "Sending Message %d to LDP for PW %d succeded "
                        "Entry removed from Pending list\r\n",
                        u4MsgType, pPwVcEntry->u4PwVcIndex);

            TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                            &(pPwVcEntry->PendMsgToLdpNode));

            if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_DESTROY)
            {
                u1Event = L2VPN_PWVC_DELETE;
            }

            i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, u1Event);

            if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_DESTROY)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d to be destroyed, Destroy initiated\r\n",
                            pPwVcEntry->u4PwVcIndex);

                L2VpnDeletePwVc (pPwVcEntry);
            }
        }

        u1Count++;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "PW count in Pending Message list is %d\r\n",
                TMO_DLL_Count (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo)));

    if (TMO_DLL_Count (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo)) == 0)
    {
        gpL2VpnGlobalInfo->u1SendToLdpSpacingTime = 0;
        gpL2VpnGlobalInfo->u1SendToLdpMsgSpacingCount = 0;

        if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
        {
            gpL2VpnGlobalInfo->b1IsSendToLdpTmrExpHandled = FALSE;

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2VPN Module Disable is required, Module Disable "
                       "Initiated\r\n");

            L2VpnDisableL2VpnService (L2VPN_ADMIN_DOWN);

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2VpnHandlePendingMsgsToLdp: EXIT\r\n");

            return;
        }

    }
    else
    {
        L2VpnStartPendingMsgListTimer ();
    }

    gpL2VpnGlobalInfo->b1IsSendToLdpTmrExpHandled = FALSE;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnHandlePendingMsgsToLdp: EXIT\r\n");

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleSendMsgToLdp                                   */
/* Description   : This function handles if the message (Label Map, Label    */
/*                 Withdraw, Label Release, Notification) can be sent to LDP.*/
/*                 Only 100 messages are sent to LDP at a time. If count     */
/*                 exceeds 100, the remaining PW's are added to Pending      */
/*                 message list with corresponding message send type set and */
/*                 Pending Message List Timer is started if not started      */
/*                 already.                                                  */
/* Input(s)      : pPwVcEntry          - Pointer to PW VC Entry              */
/*                 u4SendToLdpMsgType  - Message Type to sent to LDP         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
L2VpnHandleSendMsgToLdp (tPwVcEntry * pPwVcEntry, UINT4 u4SendToLdpMsgType)
{
    if ((pPwVcEntry->u1MapStatus & L2VPN_REL_WAIT) &&
        (u4SendToLdpMsgType == L2VPN_LDP_LBL_MAP_MSG))
    {
        pPwVcEntry->u1SendToLdpMsgType = (UINT1) u4SendToLdpMsgType;

        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VPN to LDP message Rel Awaited %d "
                    "when sending message %d for PW %d\r\n",
                    gpL2VpnGlobalInfo->u1SendToLdpMsgSpacingCount,
                    u4SendToLdpMsgType, pPwVcEntry->u4PwVcIndex);

        if (gpL2VpnGlobalInfo->b1IsSendToLdpTmrExpHandled == FALSE)
        {
            TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                            &(pPwVcEntry->PendMsgToLdpNode));

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PW %d deleted from Pending Message List for message "
                        "%d\r\n", pPwVcEntry->u4PwVcIndex, u4SendToLdpMsgType);
        }

        if (!(TMO_DLL_Is_Node_In_List (&(pPwVcEntry->PendMsgToLdpNode))))
        {
            TMO_DLL_Add (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                         &(pPwVcEntry->PendMsgToLdpNode));

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PW %d added to Pending Message List for message "
                        "%d\r\n", pPwVcEntry->u4PwVcIndex, u4SendToLdpMsgType);
        }

        if (gpL2VpnGlobalInfo->b1IsSendToLdpTmrStarted == FALSE)
        {
            L2VpnStartPendingMsgListTimer ();
        }

        return L2VPN_NOT_OK;
    }

    return L2VPN_OK;
}

/*****************************************************************************/
/* Function Name : L2VpnStartPendingMsgListTimer                             */
/* Description   : This function starts the pending message send list timer  */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnStartPendingMsgListTimer ()
{
    gpL2VpnGlobalInfo->u1SendToLdpSpacingTime = 3;

    gpL2VpnGlobalInfo->PendMsgToLdpTimer.u4Event
        = L2VPN_PEND_MSG_TO_LDP_TMR_EVENT;

    if (TmrStartTimer (L2VPN_TIMER_LIST_ID,
                       &gpL2VpnGlobalInfo->PendMsgToLdpTimer.AppTimer,
                       (gpL2VpnGlobalInfo->u1SendToLdpSpacingTime *
                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Pending Message List Timer start of %d seconds failed\r\n",
                    gpL2VpnGlobalInfo->u1SendToLdpSpacingTime);

        gpL2VpnGlobalInfo->b1IsSendToLdpTmrStarted = FALSE;
    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Pending Message List Timer started for %d seconds\r\n"
                    "Remaining messages will be sent on this expiry\r\n",
                    gpL2VpnGlobalInfo->u1SendToLdpSpacingTime);

        gpL2VpnGlobalInfo->b1IsSendToLdpTmrStarted = TRUE;
    }

    return;
}

#ifdef LDP_GR_WANTED
/*****************************************************************************/
/* Function Name : L2VpnSendReadyEventToLdp                                     */
/* Description   : This function is used to send ready event to LDP,         */
/*                    This event will send to LDP after GR and switchover       */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
L2VpnSendReadyEventToLdp ()
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_READY_EVENT;

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Posting to LDP module to send Ready Event "
                   "failed for PW %d\r\n");
        return;
    }
}
#endif
#ifdef HVPLS_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name : L2VpnSendLdpMacWdrlMsg                                    */
/* Description   : This function is used to post L2VPN_LDP_ADDR_WDRAW_EVT    */
/*           event to LDP                                            */
/*                                                                           */
/* Input(s)      : pPwVcEntry - PW VC Entry                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
L2VpnSendLdpMacWdrlMsg (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "L2VpnSendLdpMacWdrlMsg: ENTRY\r\n");
    if ((!(L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
           L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST)) &&
        ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) != L2VPN_ZERO) ||
         (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) != L2VPN_ZERO)))
    {
        L2VPN_DBG1 (L2VPN_DBG_ALL_FLAG,
                    "L2VpnSendLdpMacWdrlMsg:PW is not in forwarding state."
                    "So MAC Wdrl is not sent on PW(%d)",
                    L2VPN_PWVC_INDEX (pPwVcEntry));
        return L2VPN_FAILURE;
    }
    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_ADDR_WDRAW_EVT;
    LDP_L2VPN_EVT_NOTIF_ADDR_TYPE (&TmpPwVcEvtInfo) =
        (UINT2) L2VPN_PWVC_PEER_ADDR_TYPE (pPwVcEntry);
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV6 (&TmpPwVcEvtInfo),
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo),
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    LDP_L2VPN_EVT_NOTIF_ENTITY_INDEX (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX (pPwVcMplsEntry);
    LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_PWID_FEC_SIG;
        LDP_L2VPN_EVT_NOTIF_VC_ID (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_ID (pPwVcEntry);
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&TmpPwVcEvtInfo) =
            L2VPN_PWVC_OWNER_GEN_FEC_SIG;

        LDP_L2VPN_EVT_NOTIF_AGI_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry);
        if (L2VPN_PWVC_AGI_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_AGI (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_AGI (pPwVcEntry),
                    L2VPN_PWVC_AGI_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_AGI_LEN (pPwVcEntry);
        }

        LDP_L2VPN_EVT_NOTIF_SAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_SAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_SAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_SAII (pPwVcEntry),
                    L2VPN_PWVC_SAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_SAII_LEN (pPwVcEntry);
        }

        LDP_L2VPN_EVT_NOTIF_TAII_TYPE (&TmpPwVcEvtInfo) = (UINT1)
            (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry));
        if (L2VPN_PWVC_TAII_LEN (pPwVcEntry) > L2VPN_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_TAII (&TmpPwVcEvtInfo),
                    L2VPN_PWVC_TAII (pPwVcEntry),
                    L2VPN_PWVC_TAII_LEN (pPwVcEntry));
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&TmpPwVcEvtInfo) =
                L2VPN_PWVC_TAII_LEN (pPwVcEntry);
        }
    }

    if ((L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_MANDATORY) ||
        (L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) == L2VPN_PWVC_CTRLW_PREFERRED))
    {
        LDP_L2VPN_EVT_LBL_NOTIF_CW (&TmpPwVcEvtInfo) = L2VPN_TRUE;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_NOTIF_CW (&TmpPwVcEvtInfo) = L2VPN_FALSE;
    }

    LDP_L2VPN_EVT_NOTIF_VC_TYPE (&TmpPwVcEvtInfo) =
        (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry);

    LDP_L2VPN_EVT_NOTIF_ENTITY_ID (&TmpPwVcEvtInfo) =
        L2VPN_PWVC_MPLS_LCL_LDP_ENTID (pPwVcMplsEntry);
    L2VPN_DBG1 (L2VPN_DBG_ALL_FLAG,
                "Entity Index "
                " %d\r\n", L2VPN_PWVC_MPLS_LCL_LDP_ENTID (pPwVcMplsEntry));

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_ALL_FLAG,
                    "Posting to LDP module to send Mac Addr Withdraw Msg "
                    "failed for PW %d\r\n", pPwVcEntry->u4PwVcIndex);
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG2 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent Mac Addr Withdraw msg to %s for PW %d\r\n",
                Ip6PrintAddr (&
                              (TmpPwVcEvtInfo.unEvtInfo.LblInfo.PeerAddr.
                               Ip6Addr)), pPwVcEntry->u4PwVcIndex);
#else
    L2VPN_DBG5 (L2VPN_DBG_DBG_SIG_FLAG,
                "Sent Mac Addr Withdraw to %d.%d.%d.%d for PW %d\r\n",
                (*(UINT1 *)
                 (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_ONE)),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_TWO)),
                (*
                 ((UINT1
                   *) (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4 (&TmpPwVcEvtInfo)))
                  + L2VPN_THREE)), pPwVcEntry->u4PwVcIndex);
#endif

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name : L2VpnProcessLdpAddrWdrEvent                               */
/* Description   : This function is to process L2VPN_LDP_PWVC_ADDR_WDRAW_MSG */
/*                 Event from LDP                                          */
/*                                                                           */
/* Input(s)      : pNotifInfo - Notification received from LDP               */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpAddrWdrEvent (tPwVcNotifEvtInfo * pNotifInfo)
{
    UINT4               u4PwID = L2VPN_ZERO;
    UINT4               u4VplsInstance = L2VPN_ZERO;
    UINT1               u1PwType = L2VPN_ZERO;
    INT1                i1PwOwner = L2VPN_ZERO;
    tGenU4Addr          PeerAddr;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    MEMSET (&PeerAddr, L2VPN_ZERO, sizeof (tGenU4Addr));
    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "L2VpnProcessLdpAddrWdrEvent: ENTRY\r\n");
    u4PwID = pNotifInfo->u4VcID;
    u1PwType = pNotifInfo->u1VcType;
    i1PwOwner = pNotifInfo->i1PwOwner;
    MEMCPY (&PeerAddr, &(pNotifInfo->PeerAddr), sizeof (tGenU4Addr));
    PeerAddr.u2AddrType = pNotifInfo->u2AddrType;

    /*Fetch the PW info */
    pPwVcEntry =
        L2VpnGetPwVcEntry (i1PwOwner, u4PwID, u1PwType, pNotifInfo->au1Agi,
                           pNotifInfo->au1Taii, pNotifInfo->au1Saii, PeerAddr);
    if (pPwVcEntry != NULL)
    {
        /*Fetch vpls index from PW info and VPLS Entry from vpls Index */
        u4VplsInstance = pPwVcEntry->u4VplsInstance;
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsInstance);
        if (pVplsEntry != NULL)
        {
            L2VpnHwVplsFlushMac (pPwVcEntry, pVplsEntry);
            /*Fetch PWLIST maintained in VPLS entry */
            L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
            {
                pPwEntry =
                    L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
                if (pPwEntry != NULL)
                {
                    if (pPwVcEntry->u4PwVcIndex != pPwEntry->u4PwVcIndex)
                    {
#ifndef NPAPI_WANTED
                        VlanVplsDelPwFdbEntries (pPwEntry->u4PwVcIndex);
#endif
                        if (L2VpnIsSpokePw (pPwVcEntry->u4PwVcIndex) ==
                            L2VPN_FALSE)
                        {
                            L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                                       "Incoming Pw is Mesh Pw, MAC Addr Wdraw Event not sent to Ldp\r\n");
                            continue;
                        }

                        if (L2VpnSendLdpMacWdrlMsg (pPwEntry) == L2VPN_FAILURE)
                        {
                            L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                                       "Failed to post PwVc MAC Addr Wdraw Event to Ldp\r\n");
                            return L2VPN_FAILURE;
                        }
                    }
                }
            }
            return L2VPN_SUCCESS;
        }
    }
    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "PwVc MAC Addr Wdraw Event not posted to Ldp\r\n");
    return L2VPN_FAILURE;
}
#endif
