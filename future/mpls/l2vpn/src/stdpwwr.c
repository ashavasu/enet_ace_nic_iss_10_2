
 /* $Id: stdpwwr.c,v 1.6 2011/10/25 09:29:28 siva Exp $ */
# include  "lr.h"
# include  "mpls.h"
# include  "fssnmp.h"
# include  "stdpwwr.h"
#include   "stdpwdb.h"
# include  "l2vpincs.h"
VOID
RegisterSTDPW ()
{
    SNMPRegisterMibWithLock (&stdpwOID, &stdpwEntry, L2vpnLock, L2vpnUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdpwOID, (const UINT1 *) "stdpw");
}

INT4
PwIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPwIndexNext (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexPwTable (tSnmpIndex * pFirstMultiIndex,
                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwTable (pFirstMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pNextMultiIndex->pIndex[0].
                                      u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
PwTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwType (pMultiIndex->pIndex[0].u4_ULongValue,
                          &(pMultiData->i4_SLongValue)));

}

INT4
PwOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwOwner (pMultiIndex->pIndex[0].u4_ULongValue,
                           &(pMultiData->i4_SLongValue)));

}

INT4
PwPsnTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPsnType (pMultiIndex->pIndex[0].u4_ULongValue,
                             &(pMultiData->i4_SLongValue)));

}

INT4
PwSetUpPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwSetUpPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
PwHoldingPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwHoldingPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
PwPeerAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPeerAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
PwPeerAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPeerAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->pOctetStrValue));

}

INT4
PwAttachedPwIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwAttachedPwIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
PwIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                             &(pMultiData->i4_SLongValue)));

}

INT4
PwIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwID (pMultiIndex->pIndex[0].u4_ULongValue,
                        &(pMultiData->u4_ULongValue)));

}

INT4
PwLocalGroupIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLocalGroupID (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
PwGroupAttachmentIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwGroupAttachmentID (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
PwLocalAttachmentIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLocalAttachmentID (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
PwPeerAttachmentIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPeerAttachmentID (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
PwCwPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwCwPreference (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
PwLocalIfMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLocalIfMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
PwLocalIfStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLocalIfString (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
PwLocalCapabAdvertGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLocalCapabAdvert (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
PwRemoteGroupIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRemoteGroupID (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
PwCwStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwCwStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                              &(pMultiData->i4_SLongValue)));

}

INT4
PwRemoteIfMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRemoteIfMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
PwRemoteIfStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRemoteIfString (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->pOctetStrValue));

}

INT4
PwRemoteCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRemoteCapabilities (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
PwFragmentCfgSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwFragmentCfgSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
PwRmtFragCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRmtFragCapability (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
PwFcsRetentioncfgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwFcsRetentioncfg (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
PwFcsRetentionStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwFcsRetentionStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
PwOutboundLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwOutboundLabel (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
PwInboundLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwInboundLabel (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
PwNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwName (pMultiIndex->pIndex[0].u4_ULongValue,
                          pMultiData->pOctetStrValue));

}

INT4
PwDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwDescr (pMultiIndex->pIndex[0].u4_ULongValue,
                           pMultiData->pOctetStrValue));

}

INT4
PwCreateTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwCreateTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
PwUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwUpTime (pMultiIndex->pIndex[0].u4_ULongValue,
                            &(pMultiData->u4_ULongValue)));

}

INT4
PwLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLastChange (pMultiIndex->pIndex[0].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
PwAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwAdminStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
PwOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwOperStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
PwLocalStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwLocalStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
PwRemoteStatusCapableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRemoteStatusCapable (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
PwRemoteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRemoteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
PwTimeElapsedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwTimeElapsed (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
PwValidIntervalsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwValidIntervals (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
PwRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
PwStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwTable (pMultiIndex->pIndex[0].u4_ULongValue)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
PwTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwType (pMultiIndex->pIndex[0].u4_ULongValue,
                          pMultiData->i4_SLongValue));

}

INT4
PwOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwOwner (pMultiIndex->pIndex[0].u4_ULongValue,
                           pMultiData->i4_SLongValue));

}

INT4
PwPsnTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwPsnType (pMultiIndex->pIndex[0].u4_ULongValue,
                             pMultiData->i4_SLongValue));

}

INT4
PwSetUpPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwSetUpPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
PwHoldingPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwHoldingPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwPeerAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwPeerAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
PwPeerAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwPeerAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->pOctetStrValue));

}

INT4
PwAttachedPwIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwAttachedPwIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
PwIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                             pMultiData->i4_SLongValue));

}

INT4
PwIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwID (pMultiIndex->pIndex[0].u4_ULongValue,
                        pMultiData->u4_ULongValue));

}

INT4
PwLocalGroupIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwLocalGroupID (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
PwGroupAttachmentIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwGroupAttachmentID (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
PwLocalAttachmentIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwLocalAttachmentID (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
PwPeerAttachmentIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwPeerAttachmentID (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
PwCwPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwCwPreference (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
PwLocalIfMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwLocalIfMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiData->u4_ULongValue));

}

INT4
PwLocalIfStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwLocalIfString (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
PwLocalCapabAdvertSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwLocalCapabAdvert (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
PwFragmentCfgSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwFragmentCfgSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
PwFcsRetentioncfgSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwFcsRetentioncfg (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwOutboundLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwOutboundLabel (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
PwInboundLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwInboundLabel (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
PwNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwName (pMultiIndex->pIndex[0].u4_ULongValue,
                          pMultiData->pOctetStrValue));

}

INT4
PwDescrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwDescr (pMultiIndex->pIndex[0].u4_ULongValue,
                           pMultiData->pOctetStrValue));

}

INT4
PwAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwAdminStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
PwRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->i4_SLongValue));

}

INT4
PwStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
PwTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwType (pu4Error,
                             pMultiIndex->pIndex[0].u4_ULongValue,
                             pMultiData->i4_SLongValue));

}

INT4
PwOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwOwner (pu4Error,
                              pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->i4_SLongValue));

}

INT4
PwPsnTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwPsnType (pu4Error,
                                pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiData->i4_SLongValue));

}

INT4
PwSetUpPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2PwSetUpPriority (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
PwHoldingPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2PwHoldingPriority (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
PwPeerAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2PwPeerAddrType (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwPeerAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwPeerAddr (pu4Error,
                                 pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->pOctetStrValue));

}

INT4
PwAttachedPwIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2PwAttachedPwIndex (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
PwIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwIfIndex (pu4Error,
                                pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiData->i4_SLongValue));

}

INT4
PwIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwID (pu4Error,
                           pMultiIndex->pIndex[0].u4_ULongValue,
                           pMultiData->u4_ULongValue));

}

INT4
PwLocalGroupIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2PwLocalGroupID (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
PwGroupAttachmentIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2PwGroupAttachmentID (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
PwLocalAttachmentIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2PwLocalAttachmentID (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
PwPeerAttachmentIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2PwPeerAttachmentID (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
PwCwPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2PwCwPreference (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwLocalIfMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2PwLocalIfMtu (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
PwLocalIfStringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2PwLocalIfString (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
PwLocalCapabAdvertTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2PwLocalCapabAdvert (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
PwFragmentCfgSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2PwFragmentCfgSize (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
PwFcsRetentioncfgTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2PwFcsRetentioncfg (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
PwOutboundLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2PwOutboundLabel (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
PwInboundLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2PwInboundLabel (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
PwNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwName (pu4Error,
                             pMultiIndex->pIndex[0].u4_ULongValue,
                             pMultiData->pOctetStrValue));

}

INT4
PwDescrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PwDescr (pu4Error,
                              pMultiIndex->pIndex[0].u4_ULongValue,
                              pMultiData->pOctetStrValue));

}

INT4
PwAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2PwAdminStatus (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
PwRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2PwRowStatus (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
PwStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2PwStorageType (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
PwTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PwTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexPwPerfCurrentTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwPerfCurrentTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwPerfCurrentTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwPerfCurrentInHCPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentInHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfCurrentInHCBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentInHCBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfCurrentOutHCPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentOutHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfCurrentOutHCBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentOutHCBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfCurrentInPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentInPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
PwPerfCurrentInBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentInBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
PwPerfCurrentOutPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentOutPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
PwPerfCurrentOutBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfCurrentOutBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexPwPerfIntervalTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwPerfIntervalTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwPerfIntervalTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwPerfIntervalNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
PwPerfIntervalValidDataGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalValidData (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
PwPerfIntervalTimeElapsedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalTimeElapsed
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PwPerfIntervalInHCPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalInHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfIntervalInHCBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalInHCBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfIntervalOutHCPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalOutHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfIntervalOutHCBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalOutHCBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfIntervalInPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalInPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
PwPerfIntervalInBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalInBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
PwPerfIntervalOutPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalOutPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
PwPerfIntervalOutBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerfIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerfIntervalOutBytes (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexPwPerf1DayIntervalTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwPerf1DayIntervalTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwPerf1DayIntervalTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwPerf1DayIntervalNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
PwPerf1DayIntervalValidDataGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerf1DayIntervalValidData
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PwPerf1DayIntervalMoniSecsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerf1DayIntervalMoniSecs
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PwPerf1DayIntervalInHCPacketsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerf1DayIntervalInHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerf1DayIntervalInHCBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerf1DayIntervalInHCBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerf1DayIntervalOutHCPacketsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerf1DayIntervalOutHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerf1DayIntervalOutHCBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPerf1DayIntervalTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPerf1DayIntervalOutHCBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
PwPerfTotalErrorPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPwPerfTotalErrorPackets (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexPwIndexMappingTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwIndexMappingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwIndexMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwIndexMappingPwTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwIndexMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
PwIndexMappingPwIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwIndexMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
PwIndexMappingPeerAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwIndexMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
PwIndexMappingPeerAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwIndexMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[3].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[3].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[3].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
PwIndexMappingPwIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwIndexMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwIndexMappingPwIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexPwPeerMappingTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwPeerMappingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwPeerMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwPeerMappingPeerAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPeerMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
PwPeerMappingPeerAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPeerMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
PwPeerMappingPwTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPeerMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
PwPeerMappingPwIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPeerMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
PwPeerMappingPwIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwPeerMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwPeerMappingPwIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
PwUpDownNotifEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPwUpDownNotifEnable (&(pMultiData->i4_SLongValue)));
}

INT4
PwDeletedNotifEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPwDeletedNotifEnable (&(pMultiData->i4_SLongValue)));
}

INT4
PwNotifRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPwNotifRate (&(pMultiData->u4_ULongValue)));
}

INT4
PwUpDownNotifEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPwUpDownNotifEnable (pMultiData->i4_SLongValue));
}

INT4
PwDeletedNotifEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPwDeletedNotifEnable (pMultiData->i4_SLongValue));
}

INT4
PwNotifRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPwNotifRate (pMultiData->u4_ULongValue));
}

INT4
PwUpDownNotifEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PwUpDownNotifEnable (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PwDeletedNotifEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PwDeletedNotifEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PwNotifRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PwNotifRate (pu4Error, pMultiData->u4_ULongValue));
}

INT4
PwUpDownNotifEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PwUpDownNotifEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PwDeletedNotifEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PwDeletedNotifEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PwNotifRateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PwNotifRate (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
