/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vpst.c,v 1.52 2016/07/28 07:47:34 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vpst.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the low level SET routines
 *                             for the MIB objects present in the fsl2vpn.mib.
 *---------------------------------------------------------------------------*/

# include  "l2vpincs.h"
# include  "mplsnp.h"
# include  "mplsred.h"
#include "mplscli.h"
#include  "mplsnpwr.h"
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnTrcFlag
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnTrcFlag (INT4 i4SetValFsMplsL2VpnTrcFlag)
{
    L2VPN_DBG_FLAG = (UINT4) i4SetValFsMplsL2VpnTrcFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnCleanupInterval
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnCleanupInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnCleanupInterval (INT4 i4SetValFsMplsL2VpnCleanupInterval)
{
    L2VPN_MAX_PWVC_CLEANUP_INT (gpPwVcGlobalInfo)
        = (UINT4) i4SetValFsMplsL2VpnCleanupInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnAdminStatus
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnAdminStatus (INT4 i4SetValFsMplsL2VpnAdminStatus)
{
    UINT4               u4Event = 0;

    if (L2VPN_ADMIN_STATUS == i4SetValFsMplsL2VpnAdminStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValFsMplsL2VpnAdminStatus)
    {
        case L2VPN_ADMIN_UP:
            u4Event = L2VPN_ADMIN_UP_EVENT;
            break;

        case L2VPN_ADMIN_DOWN:
            u4Event = L2VPN_ADMIN_DOWN_EVENT;
            break;
        default:
            return SNMP_FAILURE;
    }

    if (OsixEvtSend (L2VPN_TSK_ID, u4Event) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnTrcFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnTrcFlag (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnCleanupInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnCleanupInterval (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnAdminStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnMaxPwVcEntries
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnMaxPwVcEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnMaxPwVcEntries (UINT4 u4SetValFsMplsL2VpnMaxPwVcEntries)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }

    if ((u4SetValFsMplsL2VpnMaxPwVcEntries >= L2VPN_MIN_PWVC_ENTRIES) &&
        (u4SetValFsMplsL2VpnMaxPwVcEntries <= L2VPN_MAXIMUM_PWVC_ENTRIES))
    {
        return SNMP_FAILURE;
    }

    L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo) =
        u4SetValFsMplsL2VpnMaxPwVcEntries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnMaxPwVcMplsEntries
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnMaxPwVcMplsEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnMaxPwVcMplsEntries (UINT4
                                     u4SetValFsMplsL2VpnMaxPwVcMplsEntries)
{
    L2VPN_MAX_MPLS_ENTRIES (gpPwVcMplsGlobalInfo)
        = u4SetValFsMplsL2VpnMaxPwVcMplsEntries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnMaxPwVcMplsInOutEntries
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnMaxPwVcMplsInOutEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnMaxPwVcMplsInOutEntries (UINT4
                                          u4SetValFsMplsL2VpnMaxPwVcMplsInOutEntries)
{
    L2VPN_CFG_MAX_MPLS_IN_OUT_ENTRIES (gpPwVcMplsGlobalInfo)
        = u4SetValFsMplsL2VpnMaxPwVcMplsInOutEntries;
    L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo)
        = u4SetValFsMplsL2VpnMaxPwVcMplsInOutEntries;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnMaxEthernetPwVcs
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnMaxEthernetPwVcs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnMaxEthernetPwVcs (UINT4 u4SetValFsMplsL2VpnMaxEthernetPwVcs)
{
    L2VPN_MAX_ENET_SERV_ENTRIES (gpPwVcEnetGlobalInfo)
        = u4SetValFsMplsL2VpnMaxEthernetPwVcs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnMaxPwVcEnetEntries
 Input       :  The Indices

                The Object 
                setValFsMplsL2VpnMaxPwVcEnetEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnMaxPwVcEnetEntries (UINT4
                                     u4SetValFsMplsL2VpnMaxPwVcEnetEntries)
{
    L2VPN_MAX_ENET_ENTRIES (gpPwVcEnetGlobalInfo)
        = u4SetValFsMplsL2VpnMaxPwVcEnetEntries;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwMode
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwMode (UINT4 u4PwIndex, INT4 i4SetValFsMplsL2VpnPwMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_MODE (pPwVcEntry) = (UINT1) i4SetValFsMplsL2VpnPwMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnVplsIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnVplsIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnVplsIndex (UINT4 u4PwIndex, UINT4 u4SetValFsMplsL2VpnVplsIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = u4SetValFsMplsL2VpnVplsIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwLocalCapabAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwLocalCapabAdvert
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwLocalCapabAdvert (UINT4 u4PwIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsMplsL2VpnPwLocalCapabAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) =
            pSetValFsMplsL2VpnPwLocalCapabAdvert->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwLocalCCAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwLocalCCAdvert
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwLocalCCAdvert (UINT4 u4PwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsMplsL2VpnPwLocalCCAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_LCL_CC_ADVERT (pPwVcEntry) =
            pSetValFsMplsL2VpnPwLocalCCAdvert->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwLocalCVAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwLocalCVAdvert
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwLocalCVAdvert (UINT4 u4PwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsMplsL2VpnPwLocalCVAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_LCL_CV_ADVERT (pPwVcEntry) =
            pSetValFsMplsL2VpnPwLocalCVAdvert->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwRemoteCCAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwRemoteCCAdvert
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwRemoteCCAdvert (UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMplsL2VpnPwRemoteCCAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) =
            pSetValFsMplsL2VpnPwRemoteCCAdvert->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwRemoteCVAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwRemoteCVAdvert
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwRemoteCVAdvert (UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMplsL2VpnPwRemoteCVAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) =
            pSetValFsMplsL2VpnPwRemoteCVAdvert->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwOamEnable
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwOamEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwOamEnable (UINT4 u4PwIndex,
                              INT4 i4SetValFsMplsL2VpnPwOamEnable)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_OAM_ENABLE (pPwVcEntry) =
            (BOOL1) i4SetValFsMplsL2VpnPwOamEnable;

#ifdef SNMP_3_WANTED
        L2vpnSnmpPwSendTrap (u4PwIndex, L2VPN_OAM_STATUS_TRAP,
                             (UINT1) i4SetValFsMplsL2VpnPwOamEnable);
#endif

        if (L2VPN_PWVC_OAM_ENABLE (pPwVcEntry) == L2VPN_PW_OAM_DISABLE)
        {
            pPwVcEntry->bOamEnable = FALSE;
            pPwVcEntry->u4ProactiveSessionIndex = 0;
            if ((L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_DOWN) &&
                (pPwVcEntry->u1CPOrMgmtOperStatus == L2VPN_PWVC_OPER_UP))
            {
                if (L2VpnUpdateOperStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                           L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
                    == L2VPN_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
        }
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwGenAGIType
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwGenAGIType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwGenAGIType (UINT4 u4PwIndex,
                               UINT4 u4SetValFsMplsL2VpnPwGenAGIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry) = u4SetValFsMplsL2VpnPwGenAGIType;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwGenLocalAIIType
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwGenLocalAIIType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwGenLocalAIIType (UINT4 u4PwIndex,
                                    UINT4 u4SetValFsMplsL2VpnPwGenLocalAIIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry) =
            u4SetValFsMplsL2VpnPwGenLocalAIIType;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsL2VpnPwGenRemoteAIIType
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsMplsL2VpnPwGenRemoteAIIType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwGenRemoteAIIType (UINT4 u4PwIndex,
                                     UINT4
                                     u4SetValFsMplsL2VpnPwGenRemoteAIIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry) =
            u4SetValFsMplsL2VpnPwGenRemoteAIIType;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************                                      Function    :  nmhSetFsMplsL2VpnPwAIIFormat
 Input       :  The Indices
                PwIndex

                The Object
                setValFsMplsL2VpnPwAIIFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsL2VpnPwAIIFormat (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE
                              * pSetValFsMplsL2VpnPwAIIFormat)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->u1AiiFormat =
            pSetValFsMplsL2VpnPwAIIFormat->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLocalCCTypesCapabilities
 Input       :  The Indices

                The Object 
                setValFsMplsLocalCCTypesCapabilities
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLocalCCTypesCapabilities (tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsMplsLocalCCTypesCapabilities)
{
     
    CPY_FROM_SNMP (&gL2VpnGlobalInfo.u1LocalCcTypeCapabilities,
                   pSetValFsMplsLocalCCTypesCapabilities, sizeof (UINT1));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLocalCVTypesCapabilities
 Input       :  The Indices

                The Object 
                setValFsMplsLocalCVTypesCapabilities
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLocalCVTypesCapabilities (tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsMplsLocalCVTypesCapabilities)
{
    CPY_FROM_SNMP (&gL2VpnGlobalInfo.u1LocalCvTypeCapabilities,
                   pSetValFsMplsLocalCVTypesCapabilities, sizeof (UINT1));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLocalCCTypesCapabilities
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLocalCCTypesCapabilities (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLocalCVTypesCapabilities
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLocalCVTypesCapabilities (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnPwTable
 Input       :  The Indices
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnPwTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for fsMplsVplsConfigTable */

/****************************************************************************
 Function    :  nmhSetFsMplsVplsVsi
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsVsi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsVsi (UINT4 u4FsMplsVplsInstanceIndex,
                     INT4 i4SetValFsMplsVplsVsi)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_VSI (pVplsEntry) = i4SetValFsMplsVplsVsi;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsVpnId
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsVpnId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsVpnId (UINT4 u4FsMplsVplsInstanceIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsMplsVplsVpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = L2VPN_ZERO;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFsMplsVplsVpnId->i4_Length != L2VPN_ZERO)
    {
        MEMCPY (&u4VpnId,
                &(pSetValFsMplsVplsVpnId->
                  pu1_OctetList[STRLEN (MPLS_OUI_VPN_ID)]), sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);

        if (MplsL2VpnCheckVpnId (u4VpnId) != u4VpnId)
        {
            return SNMP_FAILURE;
        }

        if (MplsL2VpnSetVpnId (u4VpnId) != u4VpnId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        MEMCPY (&u4VpnId, &(pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
                sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
	if (u4VpnId != L2VPN_ZERO)
	{
		MplsL2VpnRelVpnId (u4VpnId);
	}
    }

    MEMSET (L2VPN_VPLS_VPN_ID (pVplsEntry), 0,
            sizeof (L2VPN_VPLS_VPN_ID (pVplsEntry)));
    CPY_FROM_SNMP (&L2VPN_VPLS_VPN_ID (pVplsEntry), pSetValFsMplsVplsVpnId,
                   pSetValFsMplsVplsVpnId->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsName
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsName (UINT4 u4FsMplsVplsInstanceIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsMplsVplsName)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        MEMSET (L2VPN_VPLS_NAME (pVplsEntry), 0,
                sizeof (L2VPN_VPLS_NAME (pVplsEntry)));
        CPY_FROM_SNMP (&L2VPN_VPLS_NAME (pVplsEntry), pSetValFsMplsVplsName,
                       pSetValFsMplsVplsName->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsDescr
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsDescr (UINT4 u4FsMplsVplsInstanceIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsMplsVplsDescr)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        MEMSET (L2VPN_VPLS_DESCR (pVplsEntry), 0,
                sizeof (L2VPN_VPLS_DESCR (pVplsEntry)));
        CPY_FROM_SNMP (&L2VPN_VPLS_DESCR (pVplsEntry), pSetValFsMplsVplsDescr,
                       pSetValFsMplsVplsDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsFdbHighWatermark
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsFdbHighWatermark
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsFdbHighWatermark (UINT4 u4FsMplsVplsInstanceIndex,
                                  UINT4 u4SetValFsMplsVplsFdbHighWatermark)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_FDB_HIGH_THRESHOLD (pVplsEntry) =
            u4SetValFsMplsVplsFdbHighWatermark;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsFdbLowWatermark
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsFdbLowWatermark
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsFdbLowWatermark (UINT4 u4FsMplsVplsInstanceIndex,
                                 UINT4 u4SetValFsMplsVplsFdbLowWatermark)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_FDB_LOW_THRESHOLD (pVplsEntry) =
            u4SetValFsMplsVplsFdbLowWatermark;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsRowStatus
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsMplsVplsRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsRowStatus (UINT4 u4FsMplsVplsInstanceIndex,
                           INT4 i4SetValFsMplsVplsRowStatus)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    UINT4               u4VpnId = 0;

#ifdef NPAPI_WANTED
#ifdef HVPLS_WANTED
    tMplsNpWrFsMplsRegisterFwdAlarm MplsNpWrFsMplsRegisterFwdAlarm;
    tMplsHwVplsInfo   MplsHwVplsVpnInfo;  
#endif
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
    tL2VpnVplsHwList    L2VpnVplsHwList;
#endif
	tMplsHwVplsInfo     MplsHwVplsInfo;
#else
   UNUSED_PARAM (u4VpnId);
#endif
      
#ifdef NPAPI_WANTED
#ifdef HVPLS_WANTED
    MEMSET(&MplsNpWrFsMplsRegisterFwdAlarm ,0, sizeof(tMplsNpWrFsMplsRegisterFwdAlarm));
    /*MEMSET(&,0,sizeof(tMplsHwVplsWaterMarkInfo));*/
#endif
#endif
    
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex); 

     switch (i4SetValFsMplsVplsRowStatus)
     {     
        case CREATE_AND_WAIT:
            pVplsEntry = L2vpnCreateVplsEntry (u4FsMplsVplsInstanceIndex);
            if (pVplsEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:
            if (pVplsEntry != NULL)
            {
#ifdef HVPLS_WANTED
                if(L2VPN_VPLS_NOTIF_ENABLE_STATUS == TRUE)
                {
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
#ifdef NPAPI_WANTED
                        MEMCPY (&u4VpnId, &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                                sizeof (UINT4));

                        u4VpnId = OSIX_HTONL (u4VpnId);

                        MplsFsMplsDeRegisterFwdAlarm(u4VpnId);
#endif

                    }
                }
#endif

                if (NOT_IN_SERVICE == L2VPN_VPLS_ROW_STATUS (pVplsEntry))
                {
                    return SNMP_SUCCESS;
                }
#ifdef VPLSADS_WANTED
                if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
                {
                    L2VpnSendVplsDownAdminEvent (u4FsMplsVplsInstanceIndex);
                    L2VpnSendVplsDeleteAdminEvent (u4FsMplsVplsInstanceIndex);
                }
#ifdef HVPLS_WANTED
                else
                {
                    L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_DOWN;
                    L2VpnVplsOperStatusTrap (pVplsEntry); 
                }
#endif
#endif
                L2VPN_VPLS_ROW_STATUS (pVplsEntry) = NOT_IN_SERVICE;
                if (L2vpnVplsVfiDelete (pVplsEntry) != L2VPN_FAILURE)
                {
                    MEMCPY (&u4VpnId,
                            &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                            sizeof (UINT4));
                    u4VpnId = OSIX_HTONL (u4VpnId);
#ifdef NPAPI_WANTED
                    /* Call for updating the NP */
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
                        /* VPLS FDB . This flow will be set for unmapping */
                        MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
                        MplsHwVplsInfo.u2VplsFdbId =
                            L2VPN_VPLS_FDB_ID (pVplsEntry);
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
                        
						MEMSET (&L2VpnVplsHwList, 0, sizeof (tL2VpnVplsHwList));
						L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwList) = 
								u4FsMplsVplsInstanceIndex;
						L2VpnVplsHwListRemove (&L2VpnVplsHwList, L2VPN_VPLS_NPAPI_SUCCESS);
#endif	
						MplsFsMplsHwDeleteVplsVpn (L2VPN_VPLS_INDEX
                                                   (pVplsEntry), u4VpnId,
                                                   &MplsHwVplsInfo);
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
						L2VpnVplsHwListRemove (&L2VpnVplsHwList, L2VPN_VPLS_NPAPI_CALLED);	
#endif
                    }
#endif
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;

        case ACTIVE:
            if (pVplsEntry != NULL)
            {
#ifdef HVPLS_WANTED
                if(L2VPN_VPLS_NOTIF_ENABLE_STATUS == TRUE)
                {
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
#ifdef NPAPI_WANTED
                        MEMCPY (&u4VpnId, &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                                sizeof (UINT4));

                        u4VpnId = OSIX_HTONL (u4VpnId);




                        MplsHwVplsVpnInfo.u4VplsHighWaterMark =  L2VPN_VPLS_FDB_HIGH_THRESHOLD(pVplsEntry);
                        MplsHwVplsVpnInfo.u4VplsLowWaterMark  =  L2VPN_VPLS_FDB_LOW_THRESHOLD(pVplsEntry);
                        MplsNpWrFsMplsRegisterFwdAlarm.pMplsHwVplsVpnInfo = &MplsHwVplsVpnInfo;           
                        MplsFsMplsRegisterFwdAlarm(u4VpnId,
                                MplsNpWrFsMplsRegisterFwdAlarm.pMplsHwVplsVpnInfo);
#endif
                    }

                }
#endif
                if (L2vpnVplsVfiAdd (pVplsEntry) != L2VPN_FAILURE)
                {
                    MEMCPY (&u4VpnId,
                            &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                            sizeof (UINT4));
                    u4VpnId = OSIX_HTONL (u4VpnId);
#ifdef NPAPI_WANTED
                    /* Call for updating the NP */
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
                        /* VPLS FDB */
                        MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
                        MplsHwVplsInfo.u2VplsFdbId =
                            L2VPN_VPLS_FDB_ID (pVplsEntry);
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
						MEMSET (&L2VpnVplsHwList, 0, sizeof (tL2VpnVplsHwList));
						L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwList) = 
								u4FsMplsVplsInstanceIndex;	
						L2VPN_VPLS_HW_LIST_VPNID (&L2VpnVplsHwList) = u4VpnId;
						L2VPN_VPLS_HW_LIST_FDBID (&L2VpnVplsHwList) = 
								L2VPN_VPLS_FDB_ID (pVplsEntry);
						L2VpnVplsHwListAddUpdate (&L2VpnVplsHwList,													L2VPN_VPLS_NPAPI_CALLED);
#endif
                        if ( MplsFsMplsHwCreateVplsVpn (L2VPN_VPLS_INDEX
                                                   (pVplsEntry), u4VpnId,
                                                   &MplsHwVplsInfo) == FNP_FAILURE)
                        {   
                            L2vpnVplsVfiDelete(pVplsEntry); 
                            return SNMP_FAILURE;
                        }
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
						L2VpnVplsHwListAddUpdate (&L2VpnVplsHwList,
													L2VPN_VPLS_NPAPI_SUCCESS);
#endif
                    }
#endif 
                    L2VPN_VPLS_ROW_STATUS (pVplsEntry) = ACTIVE;
#ifdef VPLSADS_WANTED
                    if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
                    {
                        L2VpnSendBgpVplsCreateEvent (u4FsMplsVplsInstanceIndex);
                        L2VpnSendVplsUpAdminEvent (u4FsMplsVplsInstanceIndex);
                    }
#ifdef HVPLS_WANTED
                    else
                    {
                        L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_UP; 
                        L2VpnVplsOperStatusTrap (pVplsEntry); 
                    }
#endif
#endif
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;

        case DESTROY:
            if (pVplsEntry != NULL)
            {
#ifdef HVPLS_WANTED
                if(L2VPN_VPLS_NOTIF_ENABLE_STATUS == TRUE)
                {
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
#ifdef NPAPI_WANTED
                        MEMCPY (&u4VpnId, &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                                sizeof (UINT4));

                        u4VpnId = OSIX_HTONL (u4VpnId);

                        MplsFsMplsDeRegisterFwdAlarm(u4VpnId);
#endif
                    }

                }
#endif

#ifdef VPLSADS_WANTED
                if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
                {
                    L2VpnSendVplsDownAdminEvent (u4FsMplsVplsInstanceIndex);
                    L2VpnSendVplsDeleteAdminEvent (u4FsMplsVplsInstanceIndex);
                }
#endif
                /* Get all the Pw Entries associated to this Vpls Instance and
                 * make OPER_DOWN */
                L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
                {
                    pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry,
                                                   VplsNode, pPwVplsNode);
                    L2VPN_PWVC_OPER_STATUS (pPwEntry) = L2VPN_PWVC_OPER_DOWN;
                    L2VpnUpdateGlobalStats (pPwEntry, L2VPN_PWVC_OPER_DOWN);
                }
                MEMCPY (&u4VpnId,
                        &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                        sizeof (UINT4));
                u4VpnId = OSIX_HTONL (u4VpnId);

                if (u4VpnId != L2VPN_ZERO)
                {
                    MplsL2VpnRelVpnId (u4VpnId);
                }
#ifdef NPAPI_WANTED
				MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
                MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
#endif
#ifdef VPLSADS_WANTED
                if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
                {
                    L2VpnSendVplsRowDeleteAdminEvent
                        (u4FsMplsVplsInstanceIndex);
                }
                else
#endif
                {
                    if (L2vpnDeleteVplsEntry (u4FsMplsVplsInstanceIndex) ==
                        L2VPN_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                }
 
#ifdef NPAPI_WANTED
                /* Call for updating the NP */
#ifdef L2RED_WANTED
                if ((MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) 
                    && (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == ACTIVE))
#endif
                {
                    /* VPLS FDB */
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
                   	MEMSET (&L2VpnVplsHwList, 0, sizeof (tL2VpnVplsHwList));
					L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwList) = 
							u4FsMplsVplsInstanceIndex;
					L2VpnVplsHwListRemove (&L2VpnVplsHwList, L2VPN_VPLS_NPAPI_SUCCESS);	
#endif
                    MplsFsMplsHwDeleteVplsVpn (L2VPN_VPLS_INDEX (pVplsEntry),
                                               u4VpnId, &MplsHwVplsInfo);
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
					L2VpnVplsHwListRemove (&L2VpnVplsHwList, L2VPN_VPLS_NPAPI_CALLED);	
#endif
                }
#endif
          		return SNMP_SUCCESS; 
			}
            return SNMP_FAILURE;

        case CREATE_AND_GO:
        case NOT_READY:
            /* Fall Through */
        default:
            return SNMP_FAILURE;
    }
}

/* VPLS FDB */
/****************************************************************************
 Function    :  nmhSetFsMplsVplsL2MapFdbId
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object
                setValFsMplsVplsL2MapFdbId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsL2MapFdbId (UINT4 u4FsMplsVplsInstanceIndex,
                            INT4 i4SetValFsMplsVplsL2MapFdbId)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_FDB_ID (pVplsEntry) = (UINT2) i4SetValFsMplsVplsL2MapFdbId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsmplsVplsMtu
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsmplsVplsMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmplsVplsMtu (UINT4 u4FsMplsVplsInstanceIndex,
                     UINT4 u4SetValFsmplsVplsMtu)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_MTU (pVplsEntry) = u4SetValFsmplsVplsMtu;
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (u4SetValFsmplsVplsMtu);
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsmplsVplsStorageType
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsmplsVplsStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmplsVplsStorageType (UINT4 u4FsMplsVplsInstanceIndex,
                             INT4 i4SetValFsmplsVplsStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_STORAGE_TYPE (pVplsEntry) =
            (UINT1) i4SetValFsmplsVplsStorageType;
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (i4SetValFsmplsVplsStorageType);
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsmplsVplsSignalingType
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsmplsVplsSignalingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmplsVplsSignalingType (UINT4 u4FsMplsVplsInstanceIndex,
                               INT4 i4SetValFsmplsVplsSignalingType)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_SIG_TYPE (pVplsEntry) =
            (UINT4) i4SetValFsmplsVplsSignalingType;
        L2VPN_VPLS_VE_RANGE_SIZE (pVplsEntry) =
            L2VPN_VPLS_DEFAULT_NUMBER_OF_SITES;
        L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_DOWN;
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (i4SetValFsmplsVplsSignalingType);
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsmplsVplsControlWord
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                setValFsmplsVplsControlWord
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmplsVplsControlWord (UINT4 u4FsMplsVplsInstanceIndex,
                             INT4 i4SetValFsmplsVplsControlWord)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_VPLS_CONTROL_WORD (pVplsEntry) =
            (BOOL1) i4SetValFsmplsVplsControlWord;
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (i4SetValFsmplsVplsControlWord);
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPwMplsInboundTunnelIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsPwMplsInboundTunnelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPwMplsInboundTunnelIndex (UINT4 u4PwIndex,
                                  UINT4 u4SetValFsPwMplsInboundTunnelIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if ((pPwVcEntry == NULL) || (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex =
        u4SetValFsPwMplsInboundTunnelIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPwMplsInboundTunnelEgressLSR
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsPwMplsInboundTunnelEgressLSR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPwMplsInboundTunnelEgressLSR (UINT4 u4PwIndex,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pSetValFsPwMplsInboundTunnelEgressLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if ((pPwVcEntry == NULL) || (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    CPY_FROM_SNMP (pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.TnlPeerLSR,
                   pSetValFsPwMplsInboundTunnelEgressLSR, ROUTER_ID_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPwMplsInboundTunnelIngressLSR
 Input       :  The Indices
                PwIndex

                The Object 
                setValFsPwMplsInboundTunnelIngressLSR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPwMplsInboundTunnelIngressLSR (UINT4 u4PwIndex,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValFsPwMplsInboundTunnelIngressLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if ((pPwVcEntry == NULL) || (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    CPY_FROM_SNMP (pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.TnlLclLSR,
                   pSetValFsPwMplsInboundTunnelIngressLSR, ROUTER_ID_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsVplsConfigTable
 Input       :  The Indices
                FsMplsVplsInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsVplsConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPwMplsInboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPwMplsInboundTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnMaxPwVcEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnMaxPwVcEntries (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnMaxPwVcMplsEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnMaxPwVcMplsEntries (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnMaxPwVcMplsInOutEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnMaxPwVcMplsInOutEntries (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnMaxEthernetPwVcs
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnMaxEthernetPwVcs (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsL2VpnMaxPwVcEnetEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsL2VpnMaxPwVcEnetEntries (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsPortBundleStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsMplsPortBundleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsPortBundleStatus (INT4 i4IfIndex,
                              INT4 i4SetValFsMplsPortBundleStatus)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo->u1BundleStatus = (UINT1) i4SetValFsMplsPortBundleStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMplsPortMultiplexStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsMplsPortMultiplexStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsPortMultiplexStatus (INT4 i4IfIndex,
                                 INT4 i4SetValFsMplsPortMultiplexStatus)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo->u1MultiplexStatus =
        (UINT1) i4SetValFsMplsPortMultiplexStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsPortAllToOneBundleStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsMplsPortAllToOneBundleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsPortAllToOneBundleStatus (INT4 i4IfIndex,
                                      INT4
                                      i4SetValFsMplsPortAllToOneBundleStatus)
{
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo->u1AllToOneBundleStatus =
        (UINT1) i4SetValFsMplsPortAllToOneBundleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsPortRowStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsMplsPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsPortRowStatus (INT4 i4IfIndex, INT4 i4SetValFsMplsPortRowStatus)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    switch (i4SetValFsMplsPortRowStatus)
    {
        case MPLS_STATUS_CREATE_AND_WAIT:
        case MPLS_STATUS_CREATE_AND_GO:

            if (pMplsPortEntryInfo != NULL)
            {
                return SNMP_FAILURE;
            }

            pMplsPortEntryInfo =
                (tMplsPortEntryInfo *)
                MemAllocMemBlk (L2VPN_PORT_ENTRY_INFO_POOL_ID);

            if (pMplsPortEntryInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            pMplsPortEntryInfo->u4IfIndex = (UINT4) i4IfIndex;
            pMplsPortEntryInfo->u1BundleStatus = MPLS_ENABLE;
            pMplsPortEntryInfo->u1MultiplexStatus = MPLS_ENABLE;
            pMplsPortEntryInfo->u1AllToOneBundleStatus = MPLS_DISABLE;
            pMplsPortEntryInfo->i1RowStatus = MPLS_STATUS_NOT_READY;

            if (RBTreeAdd (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE, pMplsPortEntryInfo)
                != RB_SUCCESS)
            {
                MemReleaseMemBlock (L2VPN_PORT_ENTRY_INFO_POOL_ID,
                                    (UINT1 *) pMplsPortEntryInfo);
                return SNMP_FAILURE;
            }

            if (pMplsPortEntryInfo->i1RowStatus == MPLS_STATUS_CREATE_AND_GO)
            {
                pMplsPortEntryInfo->i1RowStatus = MPLS_STATUS_ACTIVE;
            }
            break;

        case MPLS_STATUS_ACTIVE:
        case MPLS_STATUS_NOT_INSERVICE:

            if (pMplsPortEntryInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            pMplsPortEntryInfo->i1RowStatus =
                (INT1) i4SetValFsMplsPortRowStatus;
            break;

        case MPLS_STATUS_DESTROY:

            if (pMplsPortEntryInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            if (RBTreeRemove
                (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE,
                 pMplsPortEntryInfo) != RB_FAILURE)
            {
                MemReleaseMemBlock (L2VPN_PORT_ENTRY_INFO_POOL_ID,
                                    (UINT1 *) pMplsPortEntryInfo);
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsPortTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsAcMapPortIfIndex
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                setValFsMplsVplsAcMapPortIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsAcMapPortIfIndex (UINT4 u4FsMplsVplsAcMapVplsIndex,
                                  UINT4 u4FsMplsVplsAcMapAcIndex,
                                  UINT4 u4SetValFsMplsVplsAcMapPortIfIndex)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry     vplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapEntry;
    MEMSET (&vplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE (&vplsAcMapEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX (&vplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGet (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsAcMapEntry);

    if (pVplsAcMapEntry == NULL)
    {
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        return SNMP_FAILURE;
    }
    L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) =
        u4SetValFsMplsVplsAcMapPortIfIndex;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM (u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM (u4SetValFsMplsVplsAcMapPortIfIndex);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsAcMapPortVlan
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                setValFsMplsVplsAcMapPortVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsAcMapPortVlan (UINT4 u4FsMplsVplsAcMapVplsIndex,
                               UINT4 u4FsMplsVplsAcMapAcIndex,
                               UINT4 u4SetValFsMplsVplsAcMapPortVlan)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry     vplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapEntry;
    MEMSET (&vplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE (&vplsAcMapEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX (&vplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGet (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsAcMapEntry);

    if (pVplsAcMapEntry == NULL)
    {
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        return SNMP_FAILURE;
    }
    L2VPN_VPLSAC_VLAN_ID (pVplsAcMapEntry) = u4SetValFsMplsVplsAcMapPortVlan;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM (u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM (u4SetValFsMplsVplsAcMapPortVlan);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsMplsVplsAcMapRowStatus
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                setValFsMplsVplsAcMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsVplsAcMapRowStatus (UINT4 u4FsMplsVplsAcMapVplsIndex,
                                UINT4 u4FsMplsVplsAcMapAcIndex,
                                INT4 i4SetValFsMplsVplsAcMapRowStatus)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry     vplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapEntry;
    tVplsInfo           VplsInfo;
    UINT1               u1IsAcPresent = L2VPN_FALSE;
    UINT1               u1OperStatus = L2VPN_ZERO;

    MEMSET (&vplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));
    MEMSET(&VplsInfo, 0, sizeof(tVplsInfo));

    L2VPN_VPLSAC_VPLS_INSTANCE (&vplsAcMapEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX (&vplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGet (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsAcMapEntry);

    if (pVplsAcMapEntry == NULL && CREATE_AND_WAIT !=
        i4SetValFsMplsVplsAcMapRowStatus)
    {
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if (pVplsAcMapEntry != NULL && L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry)
        == i4SetValFsMplsVplsAcMapRowStatus)
    {
        return SNMP_SUCCESS;
    }
    switch (i4SetValFsMplsVplsAcMapRowStatus)
    {
        case CREATE_AND_WAIT:
            pVplsAcMapEntry =
                (tVplsAcMapEntry *) MemAllocMemBlk (L2VPN_VPLS_AC_POOL_ID);
            if (NULL == pVplsAcMapEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetFsMplsVplsAcMapRowStatus :"
                           " Memory Allocate Failed \t\n");
                return SNMP_FAILURE;
            }

            MEMSET (pVplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));
            L2VPN_VPLSAC_VPLS_INSTANCE (pVplsAcMapEntry) =
                u4FsMplsVplsAcMapVplsIndex;
            L2VPN_VPLSAC_INDEX (pVplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;
            L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) = NOT_READY;
            L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) =
                L2VPN_VPLSAC_INVALID_PORT_INDEX;
            L2VPN_VPLSAC_VLAN_ID (pVplsAcMapEntry) =
                L2VPN_VPLSAC_INVALID_VLAN_ID;
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry) =
                L2VPN_VPLS_AC_OPER_STATUS_DOWN;
            if (RBTreeAdd (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           (tRBElem *) pVplsAcMapEntry) == RB_FAILURE)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetFsMplsVplsAcMapRowStatus :"
                           "VPLS RD RBTree Add Failed \t\n");
                return SNMP_FAILURE;
            }
            L2VpnSetAcIndexForVplsAc (u4FsMplsVplsAcMapVplsIndex,
                                      u4FsMplsVplsAcMapAcIndex);
            break;
        case ACTIVE:
            L2VpnIsActiveAcPresent (L2VPN_VPLSAC_VPLS_INSTANCE
                                    (pVplsAcMapEntry), &u1IsAcPresent);
            L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) =
                (INT1) i4SetValFsMplsVplsAcMapRowStatus;

            VplsInfo.u4VplsIndex = L2VPN_VPLS_INDEX(pVplsAcMapEntry);
            VplsInfo.u4IfIndex =
                  L2VPN_VPLSAC_PORT_INDEX(pVplsAcMapEntry);
            VplsInfo.VlanId =
                  (tVlanIfaceVlanId)L2VPN_VPLSAC_VLAN_ID(pVplsAcMapEntry);
            if (VplsInfo.VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
            {
                VplsInfo.u2MplsServiceType = VLAN_L2VPN_PORT_BASED;
            }
            else
            {
                VplsInfo.u2MplsServiceType = VLAN_L2VPN_VLAN_BASED;
            }

            if (VlanL2VpnAddL2VpnInfo (&VplsInfo) == VLAN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "-E- Unable to add VPLS info - "
                           "S/W forwading can not be done\n");
            }

            if (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) != L2VPN_ZERO)
            {
                CfaGetIfOperStatus (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry),
                                    &u1OperStatus);
            }

            if ((u1OperStatus == L2VPN_PWVC_OPER_UP) ||
                (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) == L2VPN_ZERO))
            {
                L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry) =
                    L2VPN_VPLS_AC_OPER_STATUS_UP;
                /* If it is first AC then VPLS should be up */
                if (L2VPN_FALSE == u1IsAcPresent)
                {
                    L2VpnSendVplsUpAdminEvent (L2VPN_VPLSAC_VPLS_INSTANCE
                                               (pVplsAcMapEntry));
                }
            }
            break;
        case NOT_IN_SERVICE:

            if ( ACTIVE == L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) )
            {
                VplsInfo.u4VplsIndex = L2VPN_VPLS_INDEX(pVplsAcMapEntry);
                VplsInfo.u4IfIndex =
                      L2VPN_VPLSAC_PORT_INDEX(pVplsAcMapEntry);
                VplsInfo.VlanId =
                      (tVlanIfaceVlanId)L2VPN_VPLSAC_VLAN_ID(pVplsAcMapEntry);
                if (VplsInfo.VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                {
                    VplsInfo.u2MplsServiceType = VLAN_L2VPN_PORT_BASED;
                }
                else
                {
                    VplsInfo.u2MplsServiceType = VLAN_L2VPN_VLAN_BASED;
                }

                if (VlanL2VpnDelL2VpnInfo(&VplsInfo) == VLAN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "-E- Can not de-register the VPLS info from vlan\n");
                }
            }
            L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) =
                (INT1) i4SetValFsMplsVplsAcMapRowStatus;
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry) =
                L2VPN_VPLS_AC_OPER_STATUS_DOWN;
            L2VpnIsActiveAcPresent (L2VPN_VPLSAC_VPLS_INSTANCE
                                    (pVplsAcMapEntry), &u1IsAcPresent);
            /*If it is last AC then VPLS should be down */
            if (L2VPN_FALSE == u1IsAcPresent)
            {
                L2VpnSendVplsDownAdminEvent (L2VPN_VPLSAC_VPLS_INSTANCE
                                             (pVplsAcMapEntry));
            }
            break;
        case DESTROY:
            if ( ACTIVE == L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) )
            {
                VplsInfo.u4VplsIndex = L2VPN_VPLS_INDEX(pVplsAcMapEntry);
                VplsInfo.u4IfIndex =
                      L2VPN_VPLSAC_PORT_INDEX(pVplsAcMapEntry);
                VplsInfo.VlanId =
                      (tVlanIfaceVlanId)L2VPN_VPLSAC_VLAN_ID(pVplsAcMapEntry);
                if (VplsInfo.VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                {
                    VplsInfo.u2MplsServiceType = VLAN_L2VPN_PORT_BASED;
                }
                else
                {
                    VplsInfo.u2MplsServiceType = VLAN_L2VPN_VLAN_BASED;
                }

                if (VlanL2VpnDelL2VpnInfo(&VplsInfo) == VLAN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "-E- Can not de-register the VPLS info from vlan\n");
                }
            }
            L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) = NOT_IN_SERVICE;
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry) =
                L2VPN_VPLS_AC_OPER_STATUS_DOWN;
            L2VpnIsActiveAcPresent (L2VPN_VPLSAC_VPLS_INSTANCE
                                    (pVplsAcMapEntry), &u1IsAcPresent);
            /*If it is last AC then VPLS should be down */
            if (L2VPN_FALSE == u1IsAcPresent)
            {
                L2VpnSendVplsDownAdminEvent (L2VPN_VPLSAC_VPLS_INSTANCE
                                             (pVplsAcMapEntry));
            }
            pVplsAcMapEntry = (tVplsAcMapEntry *)
                RBTreeRem (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           (tRBElem *) pVplsAcMapEntry);
            if (NULL == pVplsAcMapEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DELETE_FAILED);
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS AC RBTree delete Failed \t\n", __func__);
                return SNMP_FAILURE;
            }

            if (MEM_FAILURE == MemReleaseMemBlock (L2VPN_VPLS_AC_POOL_ID,
                                                   (UINT1 *) pVplsAcMapEntry))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DELETE_FAILED);
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : "
                            "MemRelease failed for L2VPN_VPLS_AC_POOL_ID \t\n",
                            __func__);
                return SNMP_FAILURE;
            }
            L2VpnReleaseAcIndexForVplsAc (u4FsMplsVplsAcMapVplsIndex,
                                          u4FsMplsVplsAcMapAcIndex);
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM (u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM (i4SetValFsMplsVplsAcMapRowStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsVplsAcMapTable
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsVplsAcMapTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
