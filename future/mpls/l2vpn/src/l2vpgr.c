/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpgr.c,v 1.13 2014/12/31 10:51:05 siva Exp $
 *
 * Description: This file contains the routines to process the events
 *              from the LDP module.
 ********************************************************************/

#include "l2vpincs.h"
extern INT4         gi4MplsSimulateFailure;
/*****************************************************************************/
/* Function Name : L2vpnGrProcessLdpGrShutEvent                              */
/* Description   : This routine processes GR Shut down event from LDP.       */
/*                 This routing marks all the Pseudowire associated with this*/
/*                 session as stale.                                         */
/* Input(s)      : pSsnInfo - Pointer to Session Entry                       */
/*                 u4Event  - Event                                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnGrProcessLdpGrShutEvent (tPwVcSsnEvtInfo * pSsnInfo, UINT4 u4Event)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcEntry         *pPwVcEntry = NULL;

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent\r\n");

#ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pSsnInfo->u2AddrType )
    {
        MEMCPY( &(PeerSsn.PeerAddr),
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH );
    }
    else if ( MPLS_IPV4_ADDR_TYPE == pSsnInfo->u2AddrType )
#endif
    {
        MEMCPY ( &(PeerSsn.PeerAddr),
                 &(pSsnInfo->PeerAddr.u4Addr),
                 IPV4_ADDR_LENGTH );
    }
    PeerSsn.u1AddrType = (UINT1)pSsnInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);

    if (pPeerSsn == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
                   "L2vpnGrProcessLdpGrShutEvent: No corresponding PeerSsn Found\r\n");
        return L2VPN_FAILURE;
    }

    /* Already PeerSsn is Down. So no need to mark the entry as
     * Stale */
    if (pPeerSsn->u4Status == L2VPN_SESSION_DOWN)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
                    "LDP Session with Peer IPv4- %d.%d.%d.%d  IPv6 -%s " 
                    " is already DOWN\r\n",
                    pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
                    "LDP Session with Peer %d.%d.%d.%d is already DOWN\r\n",
                    pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[3]);
#endif     
        return L2VPN_SUCCESS;
    }

    if ((u4Event != L2VPN_LDP_PWVC_GR_SHUTDOWN)&&
		(pPeerSsn->u1RecoveryProgress != L2VPN_GR_COMPLETED) &&
        (pPeerSsn->u1RecoveryProgress != L2VPN_GR_NOT_STARTED))
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG6 (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
                    "LDP Session with Peer IPv4 -%d.%d.%d.%d "
                     "IPv6 - %s is already in" 
                    "%d State. Cannot process the Event\r\n",
                    pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)),
                    pPeerSsn->u1RecoveryProgress);

#else
        L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
                    "LDP Session with Peer %d.%d.%d.%d is already in "
                    "%d State. Cannot process the Event\r\n",
                    pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                    pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                    pPeerSsn->u1RecoveryProgress);
#endif     
       return L2VPN_FAILURE;
    }
    if (u4Event == L2VPN_LDP_PWVC_GR_SHUTDOWN)
    {
        L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
                   "Received LDP GR Shutdown Event.\r\n");
#ifdef LDP_GR_WANTED
		if(L2VpnFlushPwVcDynamicEntries(pPeerSsn)== L2VPN_FAILURE)
		{
			L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
					"Unable to flush PwVc dynamic data structures\r\n");
			return L2VPN_FAILURE;
		}
#endif
		pPeerSsn->u1RecoveryProgress = L2VPN_GR_SHUT_DOWN_IN_PROGRESS;
		L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
				"PW Dynamic entries are flushed and made PW VC oper status as down\r\n");
    }
    else
    {
		L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
				"Received Helper Mark Stale Event.\r\n");
		pPeerSsn->u4NoOfStalePw = L2VPN_ZERO;
		TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
		{
			TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
					&(pPwVcEntry->PendMsgToLdpNode));

			L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrProcessLdpGrShutEvent: "
				"Marking PW(%d) as Not Synchronized. \r\n",L2VPN_PWVC_INDEX (pPwVcEntry));
			pPwVcEntry->u1GrSyncStatus = L2VPN_GR_NOT_SYNCHRONIZED;
			pPeerSsn->u4NoOfStalePw++;
		}

		pPeerSsn->u1RecoveryProgress = L2VPN_GR_RECONNECT_IN_PROGRESS;
	}
	return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnGrProcessLdpDelStaleEntryEvent                       */
/* Description   : This routine makes the Pseudowire operdown that are       */
/*                 marked statle.                                            */
/* Input(s)      : pSsnInfo - Pointer to Session Entry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnGrProcessLdpDelStaleEntryEvent (tPwVcSsnEvtInfo * pSsnInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcLblMsgEntry   *pLblEntry = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    UINT4               u4PwCount = L2VPN_ZERO;
    UINT1               u1Flag = L2VPN_TRUE;

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
               "L2vpnGrProcessLdpDelStaleEntryEvent: Entry\r\n");

#ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pSsnInfo->u2AddrType )
    {
        MEMCPY( &(PeerSsn.PeerAddr),
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH );
    }
    else if ( MPLS_IPV4_ADDR_TYPE == pSsnInfo->u2AddrType )
#endif
    {
        MEMCPY ( &(PeerSsn.PeerAddr),
                 &(pSsnInfo->PeerAddr.u4Addr),
                 IPV4_ADDR_LENGTH );
    }
    PeerSsn.u1AddrType = (UINT1)pSsnInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);

    if (pPeerSsn == NULL)
    {
        return L2VPN_FAILURE;
    }

    if (pPeerSsn->u4Status == L2VPN_SESSION_DOWN)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                    "LDP Session with Peer IPv4- %d.%d.%d.%d IPv6 -%s "
                    "is already DOWN\r\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                    "LDP Session with Peer %d.%d.%d.%d is already DOWN\r\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
        return L2VPN_SUCCESS;
    }

    u4PwCount = TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn));
    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                        &(pPwVcEntry->PendMsgToLdpNode));

        if (pPwVcEntry->u1GrSyncStatus == L2VPN_GR_FULLY_SYNCHRONIZED)
        {
            L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
                        "L2vpnGrProcessLdpDelStaleEntryEvent: Pseudowire with "
                        "Index %d is already synchronised. Do nothing. \r\n",
                        pPwVcEntry->u4PwVcIndex);
            continue;
        }

        L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
                    "L2vpnGrProcessLdpDelStaleEntryEvent: Pseudowire with "
                    "Index %d is not synchronised. Marking the Entry Down\r\n",
                    pPwVcEntry->u4PwVcIndex);

        u1Flag = L2VPN_FALSE;
        pPwVcEntry->u1GrSyncStatus = L2VPN_GR_NOT_SYNCHRONIZED;
        u4PwCount--;

        if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
             L2VPN_PWVC_STATUS_NOT_FORWARDING) !=
            L2VPN_PWVC_STATUS_NOT_FORWARDING)
        {
            pPwVcEntry->u4PrevInVcLabel = pPwVcEntry->u4InVcLabel;

            /* Send ldp notif event to release label */
            /* TODO Label distributed to Peer should be return to Label 
             * Pool here itself */
            L2VpnSendLdpPwVcNotifEvent (pPwVcEntry);
        }

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_PSN_BITMASK);

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;
        /* Since the LDP session itself is down, 
         * reset the PSN related  faults. */
        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_PSN_BITMASK);

        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) = L2VPN_ZERO;

        L2VPN_PWVC_LCL_CC_SELECTED (pPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_LCL_CV_SELECTED (pPwVcEntry) = L2VPN_ZERO;

        pPwVcEntry->u1MapStatus = L2VPN_ZERO;

        /* Update Pw Oper status */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 ((L2VPN_DBG_LVL_CRT_FLAG | L2VPN_DBG_GRACEFUL_RESTART),
                        "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
            /* return L2VPN_FAILURE; Need to be check at loaded condition */
        }

        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG ((L2VPN_DBG_LVL_CRT_FLAG | L2VPN_DBG_GRACEFUL_RESTART),
                       "Outbound list updation failed\r\n");
        }

        /* update the PwRec with the default label value */
        L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
        L2VpnPwSetOutVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);

        if (u4PwCount == L2VPN_ZERO)
        {
            if (L2VPN_SESSION_PWVC_DEREG_REQ (pPwVcEntry) == FALSE)
            {
                L2VPN_DBG ((DBG_ERR_NCRT | L2VPN_DBG_GRACEFUL_RESTART),
                           "Deregistration with LDP is failed.\r\n");
            }
            pPeerSsn->u1PeerSsnLdpRegStatus = L2VPN_FALSE;
            pPeerSsn->i1SsnCreateReq = L2VPN_FALSE;
        }
    }

    if (u1Flag == L2VPN_FALSE)
    {
        L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
                   "L2vpnGrProcessLdpDelStaleEntryEvent: Graceful Restart "
                   "procedure is aborted\r\n");
        pPeerSsn->u1RecoveryProgress = L2VPN_GR_ABORTED;
    }
    if (u4PwCount != L2VPN_ZERO)
    {
        return L2VPN_SUCCESS;
    }

    /* Remove all the Label Advertisement Messages learned through
     * the peer since the session with the peer has gone down. */
    pLblEntry = (tPwVcLblMsgEntry *)
        TMO_DLL_First (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
    while (pLblEntry != NULL)
    {
        /* Stop Clean up Timer */
        TmrStopTimer (L2VPN_TIMER_LIST_ID, &(pLblEntry->CleanupTimer.AppTimer));
        L2VpnReleaseWaitingRemLblMsg (&(pLblEntry->LblInfo));
        TMO_DLL_Delete (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                        &(pLblEntry->NextNode));
        MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID, (UINT1 *) pLblEntry);
        pLblEntry = (tPwVcLblMsgEntry *)
            TMO_DLL_First (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
    }

    pPeerSsn->u4Status = L2VPN_SESSION_DOWN;
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                "L2vpnGrProcessLdpDelStaleEntryEvent: "
                "LDP Session with Peer IPv4- %d.%d.%d.%d"
                "IPv6 -%s is made DOWN\r\n",
                PeerSsn.PeerAddr.au1Ipv4Addr[0],
                PeerSsn.PeerAddr.au1Ipv4Addr[1],
                PeerSsn.PeerAddr.au1Ipv4Addr[2],
                PeerSsn.PeerAddr.au1Ipv4Addr[3],
                Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)));

#else
    L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                "L2vpnGrProcessLdpDelStaleEntryEvent: "
                "LDP Session with Peer %d.%d.%d.%d is made DOWN\r\n",
                PeerSsn.PeerAddr.au1Ipv4Addr[0],
                PeerSsn.PeerAddr.au1Ipv4Addr[1],
                PeerSsn.PeerAddr.au1Ipv4Addr[2],
                PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2vpnGrProcessLdpDelAllStaleEntries                       */
/* Description   : This routine will delete all stale PW Entry(VC & ILM) from*/
/*                 hardware and HW-List                                      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnGrProcessLdpDelAllStaleEntries ()
{
#ifdef LDP_GR_WANTED
	tL2VpnPwHwList   L2VpnPwHwListKey;
	tL2VpnPwHwList  L2VpnPwHwListEntry;

	tPwVcEntry         *pPwVcEntry = NULL;
	tPwVcActivePeerSsnEntry *pPeerSsn = NULL;

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
              "L2vpnGrProcessLdpDelAllStaleEntries: Entry\r\n");

	pPeerSsn = RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
	
	do
	{
		if (pPeerSsn == NULL)
		{
			L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
          		"L2vpnGrProcessLdpDelAllStaleEntries: Peer Session is NULL\r\n");
			break;
		}
		TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
		{
			if(pPwVcEntry == NULL)
			{
				L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
						"L2vpnGrProcessLdpDelAllStaleEntries: PwVc Entry is NULL\r\n");
				continue;
			}

			L2VPN_PW_HW_LIST_VPLS_INDEX(&L2VpnPwHwListKey)=
				L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
			L2VPN_PW_HW_LIST_PW_INDEX(&L2VpnPwHwListKey)=
				L2VPN_PWVC_INDEX (pPwVcEntry);	
			
			if(L2VpnHwListGet(&L2VpnPwHwListKey, &L2VpnPwHwListEntry)
					== MPLS_FAILURE)
			{
				L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,"Fetch from HW List"
						"failed\n");
				continue;
			}

			if(L2VPN_PW_HW_LIST_STALE_STATUS(&L2VpnPwHwListEntry)
					==	L2VPN_PW_STATUS_STALE)
			{
				pPeerSsn->u1RecoveryProgress = L2VPN_GR_ABORTED;	
				L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG, "%s : u1RecoveryProgress(%d)\n",
						__func__,pPeerSsn->u1RecoveryProgress);
			}	
		}		

	}while (pPeerSsn ==
       RBTreeGetNext (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                      (tRBElem *) pPeerSsn, NULL));
	if (L2VpnHwListGetFirst(&L2VpnPwHwListEntry) == MPLS_FAILURE)
	{
		L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : No node in HwList\n",
				__func__);
		return L2VPN_FAILURE;
	}
	do
	{
		if ((L2VPN_ZERO == L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
					&& (L2VPN_PW_HW_LIST_STALE_STATUS(&L2VpnPwHwListEntry)
						== L2VPN_PW_STATUS_STALE))
		{
			if(L2VpnLdpSendLblRelEvent(&L2VpnPwHwListEntry) == L2VPN_FAILURE)
			{
				L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,"L2vpnGrProcessLdpDelAllStaleEntries:"
						"Unable to send label release event to LDP\n");
				return L2VPN_FAILURE;

			}
			
			if(L2VpnDelStalePwVc(&L2VpnPwHwListEntry,NULL) == L2VPN_FAILURE)
			{
				L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
           				"L2vpnGrProcessLdpDelAllStaleEntries: NPAPI is failed for"
						 "PW VC Entry(%d) \r\n",L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
				return L2VPN_FAILURE;

			}
			if(L2VpnDelStalePwIlm(&L2VpnPwHwListEntry,NULL) == L2VPN_FAILURE)
			{
				L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
           				"L2vpnGrProcessLdpDelAllStaleEntries: NPAPI is failed for"
						"PW ILM Entry(%d)\r\n",L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
				return L2VPN_FAILURE;
			}
		}
	}
	while (L2VpnHwListGetNext(&L2VpnPwHwListEntry,&L2VpnPwHwListEntry)
			== MPLS_SUCCESS);   	

#endif

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
               "L2vpnGrProcessLdpDelAllStaleEntries: Exit\r\n");
	return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnGrSetGrRecoveryTime                                  */
/* Description   : This routine Set the value for GR Recovery time.          */
/* Input(s)      : pSsnInfo - Pointer to Session Entry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnGrSetGrRecoveryTime (tPwVcSsnEvtInfo * pSsnInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;

#ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pSsnInfo->u2AddrType )
    {
        MEMCPY( &(PeerSsn.PeerAddr),
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH );
    }
    else if ( MPLS_IPV4_ADDR_TYPE == pSsnInfo->u2AddrType )
#endif
    {
        MEMCPY ( &(PeerSsn.PeerAddr),
                 &(pSsnInfo->PeerAddr.u4Addr),
                 IPV4_ADDR_LENGTH );
    }
    PeerSsn.u1AddrType = (UINT1)pSsnInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);

    if (pPeerSsn == NULL)
    {
        return L2VPN_FAILURE;
    }

    pPeerSsn->u2RecoveryTime = pSsnInfo->u2RecoveryTime;
    L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART, "L2vpnGrSetGrRecoveryTime: "
                "Recovery Time value received from LDP as %d seconds.\r\n",
                pPeerSsn->u2RecoveryTime);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnGrProcessRecoveryTmrExpiry                           */
/* Description   : This routine is to send the Label Mapping Messages for the*/
/*                 Stale Entries                                             */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
L2VpnGrProcessRecoveryTmrExpiry (tTmrAppTimer * pAppTimer)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    BOOL1               b1IsRecovered = FALSE;
    UINT4               u4RemainingTime = L2VPN_ZERO;

    if (L2VPN_INITIALISED != TRUE)
    {
        return L2VPN_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnGrProcessRecoveryTmrExpiry: "
               "Recovery Timer Expired.\r\n");

    pPeerSsn = (tPwVcActivePeerSsnEntry *) pAppTimer->u4Data;

    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                        &(pPwVcEntry->PendMsgToLdpNode));

        if (pPwVcEntry->u1GrSyncStatus == L2VPN_GR_FULLY_SYNCHRONIZED)
        {
            continue;
        }

        L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
                    "L2VpnGrProcessRecoveryTmrExpiry: Pseudowire with "
                    "Index %d is not synchronised. Send Label Mapping Msg \r\n",
                    pPwVcEntry->u4PwVcIndex);

        if (L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry) == L2VPN_FAILURE)
        {
            if ((pPeerSsn->u2RecoveryTime != L2VPN_ZERO) &&
                (TmrGetRemainingTime (L2VPN_TIMER_LIST_ID,
                                      &(pPeerSsn->RecoveryTimer.AppTimer),
                                      &u4RemainingTime) == TMR_SUCCESS))
            {
                if (u4RemainingTime != L2VPN_ZERO)
                {
                    TmrStopTimer (L2VPN_TIMER_LIST_ID,
                                  &(pPeerSsn->RecoveryTimer.AppTimer));
                }
                pPeerSsn->RecoveryTimer.u4Event = L2VPN_RECOVERY_TMR_EXP_EVENT;
                pPeerSsn->RecoveryTimer.AppTimer.u4Data =
                    (FS_ULONG) (VOID *) (pPeerSsn);
                if (TmrStartTimer (L2VPN_TIMER_LIST_ID,
                                   &(pPeerSsn->RecoveryTimer.AppTimer),
                                   (pPeerSsn->u2RecoveryTime / 10)) ==
                    TMR_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
                                "Failed to start the timer in function %s \r\n",
                                __func__);
                }
                gi4MplsSimulateFailure = L2VPN_ZERO;
                b1IsRecovered = FALSE;
                break;
            }
        }
        else
        {
            b1IsRecovered = TRUE;
            pPwVcEntry->u1GrSyncStatus = L2VPN_GR_LOCAL_SYNCHRONIZED;
            pPeerSsn->u1RecoveryProgress = L2VPN_GR_RECOVERY_IN_PROGRESS;
        }
    }
    if (b1IsRecovered == TRUE)
    {
        if ((TmrGetRemainingTime (L2VPN_TIMER_LIST_ID,
                                  &(pPeerSsn->RecoveryTimer.AppTimer),
                                  &u4RemainingTime) == TMR_SUCCESS) &&
            (u4RemainingTime != L2VPN_ZERO))
        {
            TmrStopTimer (L2VPN_TIMER_LIST_ID,
                          &(pPeerSsn->RecoveryTimer.AppTimer));
        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessLdpGrSsnUpEvent                               */
/* Description   : This routine processes session up event from              */
/*                 LDP for Gr purpose                                        */
/* Input(s)      : pSsnInfo - pointer to session info                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessLdpGrSsnUpEvent (tPwVcSsnEvtInfo * pSsnInfo)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    VOID               *pPsnEntry = NULL;
    UINT4               u4RemainingTime = L2VPN_ZERO;
    BOOL1               b1IsRecovered = FALSE;
    tPwVcActivePeerSsnEntry PeerSsn;

    L2VPN_DBG (L2VPN_DBG_DBG_SSN_FLAG, "L2VpnProcessLdpGrSsnUpEvent:Entry\r\n");
    MEMSET(&PeerSsn, L2VPN_ZERO, sizeof(tPwVcActivePeerSsnEntry));

#ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pSsnInfo->u2AddrType )
    {
        MEMCPY( &(PeerSsn.PeerAddr),
                pSsnInfo->PeerAddr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH );
    }
    else if ( MPLS_IPV4_ADDR_TYPE == pSsnInfo->u2AddrType )
#endif
    {
        MEMCPY ( &(PeerSsn.PeerAddr),
                 &(pSsnInfo->PeerAddr.u4Addr),
                 IPV4_ADDR_LENGTH );
    }
    PeerSsn.u1AddrType = (UINT1)pSsnInfo->u2AddrType;

    pPeerSsn = RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                          &PeerSsn);
    if (pPeerSsn == NULL)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                    "L2VpnProcessLdpGrSsnUpEvent: "
                    "Session list with Peer IPv4- %d.%d.%d.%d"
                     "IPv6 -%s  is not available\r\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr(&(PeerSsn.PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                    "L2VpnProcessLdpGrSsnUpEvent: "
                    "Session list with Peer %d.%d.%d.%d is not available\r\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif     
       return L2VPN_FAILURE;
    }

    if (pPeerSsn->u1RecoveryProgress == L2VPN_GR_ABORTED)
    {
        return (L2VpnProcessLdpSsnUpEvent (pSsnInfo));
    }
    pPwVcEntry = (tPwVcEntry *) TMO_DLL_First (L2VPN_PWVC_LIST (pPeerSsn));

    if (pPwVcEntry == NULL)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                    "L2VpnProcessLdpGrSsnUpEvent: "
                    "No PW's associated with Peer IPv4- %d.%d.%d.%d"
                    " IPv6 -%s \r\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                    "L2VpnProcessLdpGrSsnUpEvent: "
                    "No PW's associated with Peer %d.%d.%d.%d\r\n",
                    PeerSsn.PeerAddr.au1Ipv4Addr[0],
                    PeerSsn.PeerAddr.au1Ipv4Addr[1],
                    PeerSsn.PeerAddr.au1Ipv4Addr[2],
                    PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif     
        return L2VPN_FAILURE;
    }

	L2VPN_DBG2 (L2VPN_DBG_GRACEFUL_RESTART,
			"L2VpnProcessLdpGrSsnUpEvent: u1RecoveryProgress(%d) "
			"Session Status(%d)\n",
			pPeerSsn->u1RecoveryProgress,
			pPeerSsn->u4Status);
	if ((pPeerSsn->u1RecoveryProgress == L2VPN_GR_SHUT_DOWN_IN_PROGRESS) ||
		(pPeerSsn->u1RecoveryProgress == L2VPN_GR_NOT_STARTED))
	{
		pPeerSsn->u1RecoveryProgress = L2VPN_GR_RECOVERY_IN_PROGRESS;
	}
    /* For Helper Node, pPeerSsn->u1RecoveryProgress is set to 
     * L2VPN_GR_RECONNECT_IN_PROGRESS in the function 
     * L2vpnGrProcessLdpGrShutEvent.
     * Only for the Helper Node should process SsnUp Event and start
     * sending Lbl Mapping Msg to Restarting Peer.
     */
    if ((pPeerSsn->u4Status == L2VPN_SESSION_DOWN) &&
        (pPeerSsn->u1RecoveryProgress != L2VPN_GR_RECONNECT_IN_PROGRESS))
    {
        L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
                   "L2VpnProcessLdpGrSsnUpEvent: Process Session Up event\n");
        return (L2VpnProcessLdpSsnUpEvent (pSsnInfo));
    }
    pPeerSsn->u4Status = L2VPN_SESSION_UP;
    pPeerSsn->u4LdpEntityIndex = pSsnInfo->u4LdpEntityIndex;

    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        pPsnEntry = pPwVcEntry->pPSNEntry;
        if (pPsnEntry != NULL)
        {
            pPwVcMplsEntry = (tPwVcMplsEntry *) pPsnEntry;
            pPwVcMplsEntry->u4LocalLdpEntityIndex = pPeerSsn->u4LdpEntityIndex;

            L2VPN_DBG2 (L2VPN_DBG_DBG_SSN_FLAG,
                        "Entity Index of PW %d is %d\r\n",
                        pPwVcEntry->u4PwVcIndex,
                        pPwVcMplsEntry->u4LocalLdpEntityIndex);
        }

        /* Added for GR Purpose.
         * If the Pseudowire entry is stale, Node currently Acting as 
         * helper should send Label Mapping Message
         */
        if (pPwVcEntry->u1GrSyncStatus == L2VPN_GR_NOT_SYNCHRONIZED)
        {
            if (L2VpnSendLdpPwVcLblMapMsg (pPwVcEntry) == L2VPN_FAILURE)
            {
                if ((pPeerSsn->u2RecoveryTime != L2VPN_ZERO) &&
                    (TmrGetRemainingTime (L2VPN_TIMER_LIST_ID,
                                          &(pPeerSsn->RecoveryTimer.AppTimer),
                                          &u4RemainingTime) == TMR_SUCCESS))
                {
                    if (u4RemainingTime != L2VPN_ZERO)
                    {
                        if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                                          &(pPeerSsn->RecoveryTimer.
                                            AppTimer)) == TMR_FAILURE)
                        {
                            L2VPN_DBG2 (L2VPN_DBG_DBG_SSN_FLAG,
                                        "Error while stopping the timer at line no. %d in function %s\r\n",
                                        __LINE__, __func__);

                        }
                    }
                    pPeerSsn->RecoveryTimer.u4Event =
                        L2VPN_RECOVERY_TMR_EXP_EVENT;
                    pPeerSsn->RecoveryTimer.AppTimer.u4Data =
                        (FS_ULONG) (VOID *) (pPeerSsn);
                    if (TmrStartTimer (L2VPN_TIMER_LIST_ID,
                                       &(pPeerSsn->RecoveryTimer.AppTimer),
                                       (pPeerSsn->u2RecoveryTime / 10)) ==
                        TMR_FAILURE)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_DBG_SSN_FLAG,
                                    "Failed to start the timer at line no. %d in function %s\r\n",
                                    __LINE__, __func__);
                    }
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                                "L2VpnProcessLdpGrSsnUpEvent: "
                                "Lbl Mapping Msg Sending Failed for the Peer.\r\n",
                                PeerSsn.PeerAddr.au1Ipv4Addr[0],
                                PeerSsn.PeerAddr.au1Ipv4Addr[1],
                                PeerSsn.PeerAddr.au1Ipv4Addr[2],
                                PeerSsn.PeerAddr.au1Ipv4Addr[3],
                                Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)));
#else

                    L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                                "L2VpnProcessLdpGrSsnUpEvent: "
                                "Lbl Mapping Msg Sending Failed for the Peer.\r\n",
                                PeerSsn.PeerAddr.au1Ipv4Addr[0],
                                PeerSsn.PeerAddr.au1Ipv4Addr[1],
                                PeerSsn.PeerAddr.au1Ipv4Addr[2],
                                PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
                    L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
                                "L2VpnProcessLdpGrSsnUpEvent: "
                                "Starting Recovery Timer for %d seconds.\r\n",
                                pPeerSsn->u2RecoveryTime / 10);
                    b1IsRecovered = FALSE;
                }
               	/*break;*/
            }
            else
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                            "L2VpnProcessLdpGrSsnUpEvent: "
                            "Lbl Mapping Msg Sent to the Peer.\r\n",
                            PeerSsn.PeerAddr.au1Ipv4Addr[0],
                            PeerSsn.PeerAddr.au1Ipv4Addr[1],
                            PeerSsn.PeerAddr.au1Ipv4Addr[2],
                            PeerSsn.PeerAddr.au1Ipv4Addr[3],
                            Ip6PrintAddr(&(pPeerSsn->PeerAddr.Ip6Addr)));
#else
           
                L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                            "L2VpnProcessLdpGrSsnUpEvent: "
                            "Lbl Mapping Msg Sent to the Peer.\r\n",
                            PeerSsn.PeerAddr.au1Ipv4Addr[0],
                            PeerSsn.PeerAddr.au1Ipv4Addr[1],
                            PeerSsn.PeerAddr.au1Ipv4Addr[2],
                            PeerSsn.PeerAddr.au1Ipv4Addr[3]);
#endif
                pPwVcEntry->u1GrSyncStatus = L2VPN_GR_LOCAL_SYNCHRONIZED;
                pPeerSsn->u1RecoveryProgress = L2VPN_GR_RECOVERY_IN_PROGRESS;
                b1IsRecovered = TRUE;
            }
            continue;
        }
    }

    if (b1IsRecovered == TRUE)
    {
        if ((TmrGetRemainingTime (L2VPN_TIMER_LIST_ID,
                                  &(pPeerSsn->RecoveryTimer.AppTimer),
                                  &u4RemainingTime) == TMR_SUCCESS) &&
            (u4RemainingTime != L2VPN_ZERO))
        {
            TmrStopTimer (L2VPN_TIMER_LIST_ID,
                          &(pPeerSsn->RecoveryTimer.AppTimer));
        }
    }

    L2VPN_DBG (L2VPN_DBG_DBG_SSN_FLAG, "L2VpnProcessLdpGrSsnUpEvent:Exit\r\n");
    return L2VPN_SUCCESS;
}
#ifdef LDP_GR_WANTED
/*****************************************************************************/
/* Function Name : L2vpnGrDeleteLdpPwEntryOnMisMatch                       	 */
/* Description   : This routine will delete the PW Entry(VC & ILM) from      */
/*                 hardware as well as hw-list When any mismatch happens	 */
/*                 after GR     											 */
/* Input(s)      : pL2VpnPwHwListEntry                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2vpnGrDeleteLdpPwEntryOnMisMatch(tL2VpnPwHwList  *pL2VpnPwHwListEntry)
{

	L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
			"L2vpnGrDeleteLdpPwEntryOnMisMatch: Entry\r\n");

	if(L2VpnLdpSendLblRelEvent(pL2VpnPwHwListEntry) == L2VPN_FAILURE)
	{
		L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,"L2vpnGrDeleteLdpPwEntryOnMisMatch:"
				"Unable to send label release event to LDP\n");
		return L2VPN_FAILURE;

	}
	if(L2VpnDelStalePwVc(pL2VpnPwHwListEntry,NULL) == L2VPN_FAILURE)
	{
		L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
				"L2vpnGrDeleteLdpPwEntryOnMisMatch: NPAPI is failed for"
				"PW VC Entry(%d) \r\n",L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry));
		return L2VPN_FAILURE;

	}
	if(L2VpnDelStalePwIlm(pL2VpnPwHwListEntry,NULL) == L2VPN_FAILURE)
	{
		L2VPN_DBG1 (L2VPN_DBG_GRACEFUL_RESTART,
				"L2vpnGrDeleteLdpPwEntryOnMisMatch: NPAPI is failed for"
				"PW ILM Entry(%d)\r\n",L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry));
		return L2VPN_FAILURE;
	}		
    
	L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
               "L2vpnGrDeleteLdpPwEntryOnMisMatch: Exit\r\n");
	return L2VPN_SUCCESS;
}

VOID
L2VpnSetPwAdminStatusAtGoStandby(INT4 i4SetValPwAdminStatus)
{
	UINT4	u4PwVcIndex;
	
	L2VpnProcessBgpGREvent ();	
	for (u4PwVcIndex = 1; u4PwVcIndex <=
			L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwVcIndex++)
	{
		L2VpnSetPwAdminStatus(u4PwVcIndex,i4SetValPwAdminStatus);
	}
}

#endif
