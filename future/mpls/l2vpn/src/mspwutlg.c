/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mspwutlg.c,v 1.5 2014/12/24 10:58:28 siva Exp $
 *
 * Description: This file contains utility functions used by protocol 
 *********************************************************************/

#include "l2vpincs.h"
#include "mspwclig.h"

PUBLIC INT4
L2vpnFsMsPwConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL2vpnFsMsPwConfigTable, FsMsPwConfigTableNode);

    if ((gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsMsPwConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
FsMsPwConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL2vpnFsMsPwConfigTable *pFsMsPwConfigTable1 =
        (tL2vpnFsMsPwConfigTable *) pRBElem1;
    tL2vpnFsMsPwConfigTable *pFsMsPwConfigTable2 =
        (tL2vpnFsMsPwConfigTable *) pRBElem2;

    if (pFsMsPwConfigTable1->u4FsMsPwIndex1 >
        pFsMsPwConfigTable2->u4FsMsPwIndex1)
    {
        return 1;
    }
    else if (pFsMsPwConfigTable1->u4FsMsPwIndex1 <
             pFsMsPwConfigTable2->u4FsMsPwIndex1)
    {
        return -1;
    }

    if (pFsMsPwConfigTable1->u4FsMsPwIndex2 >
        pFsMsPwConfigTable2->u4FsMsPwIndex2)
    {
        return 1;
    }
    else if (pFsMsPwConfigTable1->u4FsMsPwIndex2 <
             pFsMsPwConfigTable2->u4FsMsPwIndex2)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
Function    :  L2vpnSetAllFsMsPwConfigTableTrigger
Input       :  The Indices
pL2vpnSetFsMsPwConfigTable
pL2vpnIsSetFsMsPwConfigTable
Output      :  This Routine is used to send 
MSR and RM indication
Returns     :  OSIX_TRUE or OSIX_FALSE
 ****************************************************************************/
INT4
L2vpnSetAllFsMsPwConfigTableTrigger (tL2vpnFsMsPwConfigTable *
                                     pL2vpnSetFsMsPwConfigTable,
                                     tL2vpnIsSetFsMsPwConfigTable *
                                     pL2vpnIsSetFsMsPwConfigTable,
                                     INT4 i4SetOption)
{
    if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMsPwRowStatus, 13, L2vpnLock,
                      L2vpnUnLock, 0, 1, 2, i4SetOption, "%u %u %i",
                      pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1,
                      pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2,
                      pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  FsMsPwConfigTableFilterInputs
Input       :  The Indices
pL2vpnFsMsPwConfigTable
pL2vpnSetFsMsPwConfigTable
pL2vpnIsSetFsMsPwConfigTable
Output      :  This Routine checks set value 
with that of the value in database
to find whether the same input is given
Returns     :  OSIX_TRUE or OSIX_FALSE
 ****************************************************************************/
INT4
FsMsPwConfigTableFilterInputs (tL2vpnFsMsPwConfigTable *
                               pL2vpnFsMsPwConfigTable,
                               tL2vpnFsMsPwConfigTable *
                               pL2vpnSetFsMsPwConfigTable,
                               tL2vpnIsSetFsMsPwConfigTable *
                               pL2vpnIsSetFsMsPwConfigTable)
{
    if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 == OSIX_TRUE)
    {
        if (pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1 ==
            pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1)
            pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 = OSIX_FALSE;
    }
    if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex2 == OSIX_TRUE)
    {
        if (pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2 ==
            pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2)
            pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex2 = OSIX_FALSE;
    }
    if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus == OSIX_TRUE)
    {
        if (pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus ==
            pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus)
            pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus = OSIX_FALSE;
    }

    if ((pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 == OSIX_FALSE)
        && (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 == OSIX_FALSE)
        && (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex2 == OSIX_FALSE)
        && (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 * Function    :  L2vpnUtilUpdateFsMsPwConfigTable
 * Input       :  The Indices
 pL2vpnFsMsPwConfigTable
 pL2vpnSetFsMsPwConfigTable
 * Output      :  This Routine checks set value 
 with that of the value in database
 and do the necessary protocol operation
 * Returns     :  OSIX_TRUE or OSIX_FALSE
 ****************************************************************************/
INT4
L2vpnUtilUpdateFsMsPwConfigTable (tL2vpnFsMsPwConfigTable *
                                  pL2vpnOldFsMsPwConfigTable,
                                  tL2vpnFsMsPwConfigTable *
                                  pL2vpnFsMsPwConfigTable)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    UINT4               u4RetStatus = SNMP_SUCCESS;

    switch (pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus)
    {
        case ACTIVE:
            L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4EvtType
                = L2VPN_MSPW_ACTIVE_EVENT;
            L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4PrimPwVcIndex
                = pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1;
            L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4ScndPwVcIndex
                = pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2;
            u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                  L2VPN_MSPW_EVENT);

            if (u4RetStatus == L2VPN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case DESTROY:
            L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4EvtType
                = L2VPN_MSPW_DESTROY_EVENT;
            L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4PrimPwVcIndex
                = pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1;
            L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4ScndPwVcIndex
                = pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2;
            u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                  L2VPN_MSPW_EVENT);

            if (u4RetStatus == L2VPN_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case NOT_READY:
            if (pL2vpnOldFsMsPwConfigTable == NULL)
            {
                return SNMP_SUCCESS;
            }
            break;
	default:
	    break;
    }

    UNUSED_PARAM (pL2vpnFsMsPwConfigTable);
    return SNMP_FAILURE;
}

PUBLIC INT4
L2vpnUtlCreatMemPool ()
{
    if (MemCreateMemPool
        (sizeof (tL2vpnFsMsPwConfigTable),
         L2VPN_MAX_MSPW_ENTRIES (gL2vpnMsPwGlobals),
         MEM_DEFAULT_MEMORY_TYPE, &(L2VPN_MSPW_POOL_ID)) == MEM_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
               "Memory allocation success for MSPW\r\n");

    return OSIX_SUCCESS;
}

PUBLIC INT4
L2vpnUtlCreateRBTree ()
{
    if (L2vpnFsMsPwConfigTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "RB Tree creation success for MSPW\r\n");
    return OSIX_SUCCESS;
}

PUBLIC INT4
L2vpnUtlDeleteRBTree ()
{

    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    tPwVcEntry         *pPriPwVcEntry = NULL;    /*Primary PW VC */
    tPwVcEntry         *pSecPwVcEntry = NULL;    /*Secondary PW VC */

    pL2vpnFsMsPwConfigTable =
        (tL2vpnFsMsPwConfigTable *) RBTreeGetFirst (gL2vpnMsPwGlobals.
                                                    L2vpnMsPwGlbMib.
                                                    FsMsPwConfigTable);

    while (pL2vpnFsMsPwConfigTable != NULL)
    {

        /*Get primay PW VC entry */
        pPriPwVcEntry =
            L2VpnGetPwVcEntryFromIndex (pL2vpnFsMsPwConfigTable->
                                        u4FsMsPwIndex1);
        if (pPriPwVcEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "pri PW VC entry not found\r\n");
            return L2VPN_FAILURE;
        }
        /*Get Secondary PW VC entry */
        pSecPwVcEntry =
            L2VpnGetPwVcEntryFromIndex (pL2vpnFsMsPwConfigTable->
                                        u4FsMsPwIndex2);
        if (pSecPwVcEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Sec PW VC entry not found\r\n");
            return L2VPN_FAILURE;
        }
        /* Delete MSPW Entry */
        i4Status = L2VpnFmUpdateMsPwStatus (pPriPwVcEntry, pSecPwVcEntry,
                                            L2VPN_MSPW_DELETE);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "MSPW NP programing failed\r\n ");
            return L2VPN_FAILURE;
        }

        L2VPN_PWVC_OPER_STATUS (pPriPwVcEntry) = L2VPN_PWVC_OPER_NOTPRES;
        L2VPN_PWVC_OPER_STATUS (pSecPwVcEntry) = L2VPN_PWVC_OPER_NOTPRES;
        L2VPN_PWVC_UP_TIME (pPriPwVcEntry) = 0;
        L2VPN_PWVC_UP_TIME (pSecPwVcEntry) = 0;
        L2VpnUpdateGlobalStats (pPriPwVcEntry, L2VPN_PWVC_OPER_DOWN);
        L2VpnPrintPwVc (pPriPwVcEntry);
        L2VpnUpdateGlobalStats (pSecPwVcEntry, L2VPN_PWVC_OPER_DOWN);
        L2VpnPrintPwVc (pSecPwVcEntry);

        RBTreeRem (gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable,
                   pL2vpnFsMsPwConfigTable);

        MemReleaseMemBlock (L2VPN_MSPW_POOL_ID,
                            (UINT1 *) pL2vpnFsMsPwConfigTable);

        /* update the attached PW VC Indexes */
        L2VPN_PWVC_ATTACHED_PW_INDEX (pPriPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_ATTACHED_PW_INDEX (pSecPwVcEntry) = L2VPN_ZERO;

        pL2vpnFsMsPwConfigTable =
            (tL2vpnFsMsPwConfigTable *) RBTreeGetFirst (gL2vpnMsPwGlobals.
                                                        L2vpnMsPwGlbMib.
                                                        FsMsPwConfigTable);
    }

    RBTreeDelete (gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable);
    gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable = NULL;

    return L2VPN_SUCCESS;
}

/****************************************************************************
Function    : L2VpnGetMsPwEntry 
Description : This function is used to get a MS PW entry
Input       :  The Indices
PwIndex

Output      : NONE
Returns     :  MS PW entry or NULL
 ****************************************************************************/

tL2vpnFsMsPwConfigTable *
L2VpnGetMsPwEntry (UINT4 u4PriPwVcIndex, UINT4 u4SecPwVcIndex)
{
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;
    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (L2vpnFsMsPwConfigTable));

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = u4PriPwVcIndex;
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = u4SecPwVcIndex;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "L2VpnGetMsPwEntry :Enter\r\n");

    pL2vpnFsMsPwConfigTable =
        RBTreeGet (gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable,
                   (tRBElem *) & L2vpnFsMsPwConfigTable);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "L2VpnGetMsPwEntry :Exit\r\n");

    return pL2vpnFsMsPwConfigTable;
}
