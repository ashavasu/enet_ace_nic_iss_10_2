/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vpext.c,v 1.41 2017/11/09 13:22:04 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vpext.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains entry point function to
 *                             L2VPN and its supporting routines.
 *
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"
#include "mplsapi.h"

extern UINT4        PwType[13];
PRIVATE INT4
 
 
 
 L2vpnExtFillPwInfo (tPwVcEntry * pPwVcEntry,
                     tMplsApiOutInfo * pOutMplsApiInfo);

/*****************************************************************************/
/* Function Name : L2VpnExtChkAndUpdatePwOperStatus                          */
/* Description   : This routine processes PwVc operational status events     */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1Event - type of PwVc event (up/down/delete)             */
/*                 u1OperApp - Application (control plane/managment/         */
/*                             oam application) which is notifying           */
/*                             the operational status.                       */
/*                 pbIsOperChged - Operation status changed - TRUE/FALSE     */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
VOID
L2VpnExtChkAndUpdatePwOperStatus (tPwVcEntry * pPwVcEntry, UINT1 u1Event,
                                  UINT1 u1OperApp, BOOL1 * pbIsOperChged)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    *pbIsOperChged = FALSE;

    switch (u1Event)
    {
        case L2VPN_PWVC_UP:
            if ((u1OperApp == L2VPN_PWVC_OPER_APP_OAM) &&
                (pPwVcEntry->bOamEnable == TRUE))
            {
                if ((pPwVcEntry->u1OamOperStatus
                     == L2VPN_PWVC_OPER_UP) &&
                    (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP))
                {
                    *pbIsOperChged = FALSE;
                    break;
                }
                else
                {
                    pPwVcEntry->u1OamOperStatus = L2VPN_PWVC_OPER_UP;
                }
            }
            else if (u1OperApp == L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
            {
                if ((pPwVcEntry->u1CPOrMgmtOperStatus
                     == L2VPN_PWVC_OPER_UP) &&
                    (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP))
                {
                    *pbIsOperChged = FALSE;
                    break;
                }
                else
                {
                    pPwVcEntry->u1CPOrMgmtOperStatus = L2VPN_PWVC_OPER_UP;
                }
            }

            if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
            {
                *pbIsOperChged = FALSE;
                break;
            }
            if (pPwVcEntry->bOamEnable == TRUE)
            {
                if ((pPwVcEntry->u1OamOperStatus == L2VPN_PWVC_OPER_UP) &&
                    (pPwVcEntry->u1CPOrMgmtOperStatus == L2VPN_PWVC_OPER_UP))
                {
                    L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_UP;
                    *pbIsOperChged = TRUE;
                }
                else
                {
                    /* CPorMgmt/OAM of the oper. status is down */
                    *pbIsOperChged = FALSE;
                }
            }
            else if (pPwVcEntry->u1CPOrMgmtOperStatus == L2VPN_PWVC_OPER_UP)
            {
                L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_UP;
                *pbIsOperChged = TRUE;
            }
            else
            {
                /* Invalid operational status up handling */
                *pbIsOperChged = FALSE;
            }
            if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
            {
                L2vpnExtNotifyPwStatusToApp (pPwVcEntry, u1OperApp);
            }
            break;

        case L2VPN_PWVC_DOWN:
            if (u1OperApp == L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
            {
                /* Check both the operational status,
                 * the below check will be useful when pseudowire protection
                 * moved from PROT_IN_USE to PROT_NOT_AVAILABLE*/
                if ((pPwVcEntry->u1CPOrMgmtOperStatus == L2VPN_PWVC_OPER_DOWN)
                    && (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                        L2VPN_PWVC_OPER_DOWN))
                {
                    /* Tunnel is already operationally DOWN */
                    *pbIsOperChged = FALSE;
                    break;
                }
                else
                {
                    pPwVcEntry->u1CPOrMgmtOperStatus = L2VPN_PWVC_OPER_DOWN;
                }
            }
            else if ((u1OperApp == L2VPN_PWVC_OPER_APP_OAM) &&
                     (pPwVcEntry->bOamEnable == TRUE))
            {
                if ((pPwVcEntry->u1OamOperStatus == L2VPN_PWVC_OPER_DOWN) &&
                    (L2VPN_PWVC_OPER_STATUS (pPwVcEntry)
                     == L2VPN_PWVC_OPER_DOWN))
                {
                    /* Tunnel is already operationally DOWN */
                    *pbIsOperChged = FALSE;
                    break;
                }
                else
                {
                    pPwVcEntry->u1OamOperStatus = L2VPN_PWVC_OPER_DOWN;
                }
            }
            else
            {
                /* Invalid operational status down notification */
                *pbIsOperChged = FALSE;
                break;
            }
            if ((pPwVcEntry->u1ProtStatus == LOCAL_PROT_AVAIL) ||
                (pPwVcEntry->u1ProtStatus == LOCAL_PROT_IN_USE))
            {
                /* Don't notify pseudowire down to applications when we
                 * have protection path.
                 * */
                *pbIsOperChged = FALSE;
                break;
            }
            if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_DOWN)
            {
                /* Pseudowire is already operationally DOWN */
                *pbIsOperChged = FALSE;
                break;
            }
            *pbIsOperChged = TRUE;
            break;
        default:
            break;
    }

    if (*pbIsOperChged)
    {
        if (L2VPN_PWRED_STATUS != L2VPN_PWRED_DISABLED)
        {
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));
            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2vpnExtNotifyPwStatusToApp                               */
/* Description   : This routine notifies the PW status to applications       */
/*                 pPwVcEntry - Pseudowire entry                             */
/*                 u1OperApp - Application (control plane/managment/         */
/*                             oam application) which is notifying           */
/*                             the operational status.                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnExtNotifyPwStatusToApp (tPwVcEntry * pPwVcEntry, UINT1 u1OperApp)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tMplsEventNotif     MplsEventNotif;
    UINT4               u4DestModId = 0;
    UINT1               u1OperStatus = 0;
#ifdef HVPLS_WANTED
    UINT1               u1AdminStatus = 0;
#endif
    /*Sending OperStatus to CFA */
    if (L2VPN_PWVC_IF_INDEX (pPwVcEntry) != L2VPN_ZERO)
    {
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            u1OperStatus = CFA_IF_UP;
        }
        else
        {
            u1OperStatus = CFA_IF_DOWN;
        }
        CfaInterfaceStatusChangeIndication (L2VPN_PWVC_IF_INDEX (pPwVcEntry),
                                            u1OperStatus);
    }

    if (u1OperApp != L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
    {
#ifdef RFC6374_WANTED
        if (pPwVcEntry->u4PwVcID != 0)
        {
            /* Send Notification for RFC6374 to Registered 
             * PW for UP/DOWN Event */
            /* if APP Type is set to RFC6374 send Event to RFC6374 */
            if (pPwVcEntry->u1MplsPmAppType == MPLS_PM_TYPE_RFC6374)
            {
                u1OperStatus = (UINT1) L2VPN_PWVC_OPER_STATUS (pPwVcEntry);
                if (u1OperStatus == L2VPN_PWVC_OPER_UP)
                {
                    MplsEventNotif.u2Event = MPLS_PW_UP_EVENT;
                }
                if (u1OperStatus == L2VPN_PWVC_OPER_DOWN)
                {
                    MplsEventNotif.u2Event = MPLS_PW_DOWN_EVENT;
                }

                if (MplsEventNotif.u2Event != 0)
                {
                    MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                    MplsEventNotif.PathId.u4PathType = MPLS_PATH_TYPE_PW;
                    MplsEventNotif.PathId.PwId.u4VcId = pPwVcEntry->u4PwVcID;
                    u4DestModId |= MPLS_APPLICATION_RFC6374;

                    if (MplsPortEventNotification (u4DestModId, &MplsEventNotif)
                        == OSIX_FAILURE)
                    {
                        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                                   "L2vpnExtNotifyPwStatusToApp: "
                                   "Failed to post path status change notification"
                                   "to RFC6374 module \r\n");
                        return L2VPN_FAILURE;
                    }
                }
            }
        }
#endif
#ifdef HVPLS_WANTED
        /* CASE VPLS RES: BFD will also notify for pseudo wire status up/ down 
           which shall be passed to ERPS */
        if ((pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS) &&
            (pPwVcEntry->u4PwIfIndex != 0)
            && (pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex != 0))
        {
            MEMSET (&MplsEventNotif, 0, sizeof (tMplsEventNotif));
            CfaApiGetIfAdminStatus (pPwVcEntry->u4PwIfIndex, &u1AdminStatus);
            if ((L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                && (CFA_IF_UP == u1AdminStatus)
                && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                    MPLS_PW_UP_EVENT)
                && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                    MPLS_PW_IF_UP))
            {
                MplsEventNotif.u2Event = MPLS_PW_UP_EVENT;
            }
            else if ((L2VPN_PWVC_OPER_STATUS (pPwVcEntry) != L2VPN_PWVC_OPER_UP)
                     && (pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex != 0)
                     && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                         MPLS_PW_DOWN_EVENT)
                     && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                         MPLS_PW_IF_DOWN))
            {
                MplsEventNotif.u2Event = MPLS_PW_DOWN_EVENT;
            }
            else
            {
                L2VPN_DBG2 (L2VPN_DBG_DBG_SIG_FLAG,
                            "L2vpnExtNotifyPwStatusToApp: "
                            "LastPWStatusSend to ERPS:%d is similar as CurrentPwStatus:%d\r\n",
                            pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend,
                            L2VPN_PWVC_OPER_STATUS (pPwVcEntry));
            }
            if (MplsEventNotif.u2Event != 0)
            {
                MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                MplsEventNotif.PathId.u4PathType = MPLS_PATH_TYPE_PW;
                MplsEventNotif.PathId.PwId.u4VcId = pPwVcEntry->u4PwVcID;
                MplsEventNotif.u4InIfIndex = pPwVcEntry->u4PwIfIndex;
                u4DestModId |= MPLS_APPLICATION_ERPS;
                MplsEventNotif.PathId.PwId.u4MegIndex =
                    pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex;
                MplsEventNotif.PathId.PwId.u4MeIndex =
                    pPwVcEntry->ErpsMegInfo.u4ErpsMeIndex;
                MplsEventNotif.PathId.PwId.u4MpIndex =
                    pPwVcEntry->ErpsMegInfo.u4ErpsMepIndex;
                MplsEventNotif.PathId.PwId.u4PwIfIndex =
                    pPwVcEntry->u4PwIfIndex;
                if (MplsPortEventNotification (u4DestModId, &MplsEventNotif) ==
                    OSIX_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                               "L2vpnExtNotifyPwStatusToApp: "
                               "Failed to post path status change notification"
                               "to ERPS module \r\n");
                    return L2VPN_FAILURE;
                }
                L2VPN_DBG3 (L2VPN_DBG_DBG_SIG_FLAG,
                            "L2vpnExtNotifyPwStatusToApp: CP_OR_MGMT disabled "
                            "LastStatusSend:%x Notifying Application ERPS with Event:%x for PW VCID:%d\r\n",
                            pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend,
                            MplsEventNotif.u2Event, pPwVcEntry->u4PwVcIndex);
                pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend =
                    MplsEventNotif.u2Event;
            }
        }
#endif
        return L2VPN_SUCCESS;
    }
#ifdef RFC6374_WANTED
    if ((pPwVcEntry->u4ProactiveSessionIndex == 0) &&
        (pPwVcEntry->u1MplsOamAppType == MPLS_OAM_TYPE_NONE) &&
        (pPwVcEntry->u1MplsPmAppType == MPLS_OAM_TYPE_NONE))
#else
    if ((pPwVcEntry->u4ProactiveSessionIndex == 0) &&
        (pPwVcEntry->u1MplsOamAppType == MPLS_OAM_TYPE_NONE))
#endif
    {
        /* As of now, for MPLS-TP MEG will only
         * monitor for Tnl/PW and notify to Tnl/PW to
         * update the OAM status, OAM status should not be
         * notified to MEG module as MEG is notifying
         * the OAM status now. */
        if ((pMplsApiInInfo =
             MemAllocMemBlk (L2VPN_MPLS_API_INPUT_INFO_POOL_ID)) == NULL)
        {
            return L2VPN_FAILURE;
        }
        if ((pMplsApiOutInfo =
             MemAllocMemBlk (L2VPN_MPLS_API_OUTPUT_INFO_POOL_ID)) == NULL)
        {
            MemReleaseMemBlock (L2VPN_MPLS_API_INPUT_INFO_POOL_ID,
                                (UINT1 *) pMplsApiInInfo);
            return L2VPN_FAILURE;
        }

        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

        pMplsApiInInfo->u4SrcModId = L2VPN_MODULE;
        pMplsApiInInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pMplsApiInInfo->InPathId.MegId.u4MegIndex = pPwVcEntry->u4MegIndex;
        pMplsApiInInfo->InPathId.MegId.u4MeIndex = pPwVcEntry->u4MeIndex;
        pMplsApiInInfo->InPathId.MegId.u4MpIndex = pPwVcEntry->u4MpIndex;

        pMplsApiInInfo->InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
        pMplsApiInInfo->InServicePathId.PwId.u4PwIndex =
            pPwVcEntry->u4PwVcIndex;

        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            pMplsApiInInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
        }
        else
        {
            pMplsApiInInfo->InOamPathStatus.u1PathStatus =
                MPLS_PATH_STATUS_DOWN;
        }
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

        if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                          pMplsApiInInfo, pMplsApiOutInfo)
            == OSIX_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtNotifyPwStatusToApp: "
                       "Failed to post path status change notification"
                       "to OAM module \r\n");
            MemReleaseMemBlock (L2VPN_MPLS_API_INPUT_INFO_POOL_ID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (L2VPN_MPLS_API_OUTPUT_INFO_POOL_ID,
                                (UINT1 *) pMplsApiOutInfo);
            return L2VPN_FAILURE;
        }
        if ((pPwVcEntry->u4MegIndex == 0) &&
            (pPwVcEntry->u4MeIndex == 0) && (pPwVcEntry->u4MpIndex == 0))
        {
            pPwVcEntry->u4MegIndex =
                pMplsApiOutInfo->OutPathId.MegId.u4MegIndex;
            pPwVcEntry->u4MeIndex = pMplsApiOutInfo->OutPathId.MegId.u4MeIndex;
            pPwVcEntry->u4MpIndex = pMplsApiOutInfo->OutPathId.MegId.u4MpIndex;
        }

        MemReleaseMemBlock (L2VPN_MPLS_API_INPUT_INFO_POOL_ID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (L2VPN_MPLS_API_OUTPUT_INFO_POOL_ID,
                            (UINT1 *) pMplsApiOutInfo);
    }
    else
    {
        MEMSET (&MplsEventNotif, 0, sizeof (tMplsEventNotif));

        MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;

        MplsEventNotif.PathId.u4PathType = MPLS_PATH_TYPE_PW;
        MplsEventNotif.PathId.PwId.u4VcId = pPwVcEntry->u4PwVcID;

        if (pPwVcEntry->u1MplsOamAppType == MPLS_OAM_TYPE_Y1731)
        {
            u4DestModId = MPLS_APPLICATION_Y1731;
        }
        else                    /* Notification to BFD */
        {
            MplsEventNotif.u4ProactiveSessIndex =
                pPwVcEntry->u4ProactiveSessionIndex;
            u4DestModId = MPLS_APPLICATION_BFD;
#ifdef HVPLS_WANTED
            /*Notifying to ERPS Module If PW APP OWNER IS ERPS */
            if ((pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS) &&
                (pPwVcEntry->u4PwIfIndex != 0)
                && (pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex != 0))
            {
                CfaApiGetIfAdminStatus (pPwVcEntry->u4PwIfIndex,
                                        &u1AdminStatus);
                if ((L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                    && (CFA_IF_UP == u1AdminStatus)
                    && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                        MPLS_PW_UP_EVENT)
                    && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                        MPLS_PW_IF_UP))
                {
                    MplsEventNotif.u2Event = MPLS_PW_UP_EVENT;
                }
                else if ((L2VPN_PWVC_OPER_STATUS (pPwVcEntry) !=
                          L2VPN_PWVC_OPER_UP)
                         && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                             MPLS_PW_DOWN_EVENT)
                         && (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend !=
                             MPLS_PW_IF_DOWN))
                {
                    MplsEventNotif.u2Event = MPLS_PW_DOWN_EVENT;
                }
                else
                {
                    L2VPN_DBG2 (L2VPN_DBG_DBG_SIG_FLAG,
                                "L2vpnExtNotifyPwStatusToApp: "
                                "LastPWStatusSend to ERPS:%d is similar as CurrentPwStatus:%d\r\n",
                                pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend,
                                L2VPN_PWVC_OPER_STATUS (pPwVcEntry));
                }
                if (MplsEventNotif.u2Event != 0)
                {
                    MplsEventNotif.u4InIfIndex = pPwVcEntry->u4PwIfIndex;
                    MplsEventNotif.PathId.PwId.u4MegIndex =
                        pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex;
                    MplsEventNotif.PathId.PwId.u4MeIndex =
                        pPwVcEntry->ErpsMegInfo.u4ErpsMeIndex;
                    MplsEventNotif.PathId.PwId.u4MpIndex =
                        pPwVcEntry->ErpsMegInfo.u4ErpsMepIndex;
                    MplsEventNotif.PathId.PwId.u4PwIfIndex =
                        pPwVcEntry->u4PwIfIndex;
                    u4DestModId |= MPLS_APPLICATION_ERPS;
                }
            }
#endif
        }
#ifdef RFC6374_WANTED
        if (pPwVcEntry->u1MplsPmAppType == MPLS_PM_TYPE_RFC6374)    /* Notify to RFC6374 */
        {
            u4DestModId |= MPLS_APPLICATION_RFC6374;
        }
#endif
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            MplsEventNotif.u2Event = MPLS_PW_UP_EVENT;
        }
        else
        {
            MplsEventNotif.u2Event = MPLS_PW_DOWN_EVENT;
        }
        if (MplsPortEventNotification (u4DestModId, &MplsEventNotif)
            == OSIX_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtNotifyPwStatusToApp: "
                       "Failed to post path status change notification"
                       "to OAM/ERPS module \r\n");
            return L2VPN_FAILURE;
        }
#ifdef HVPLS_WANTED
        if ((pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS) &&
            (pPwVcEntry->u4PwIfIndex != 0)
            && (pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex != 0))
        {
            L2VPN_DBG3 (L2VPN_DBG_DBG_SIG_FLAG,
                        "L2vpnExtNotifyPwStatusToApp:  CP_OR_MGMT enabled"
                        "LastStatusSend:%x Notifying Application ERPS with Event:%x for PW VCID:%d\r\n",
                        pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend,
                        MplsEventNotif.u2Event, pPwVcEntry->u4PwVcIndex);
            pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend = MplsEventNotif.u2Event;
        }
#endif
    }
    return L2VPN_SUCCESS;
}

#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name : L2vpnExtNotifyPwIfStatusToApp                               */
/* Description   : This routine notifies the PW Interface status to applications       */
/*                 pPwVcEntry - Pseudowire entry                             */
/*                 u1OperStatus - Operational Status of Pw Interface         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnExtNotifyPwIfStatusToApp (tPwVcEntry * pPwVcEntry, UINT1 u1OperStatus)
{
    tMplsEventNotif     MplsEventNotif;
    UINT4               u4DestModId = 0;

    MEMSET (&MplsEventNotif, 0, sizeof (tMplsEventNotif));
    MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
    MplsEventNotif.PathId.u4PathType = MPLS_PATH_TYPE_PW;
    MplsEventNotif.PathId.PwId.u4VcId = pPwVcEntry->u4PwVcID;
    MplsEventNotif.u4InIfIndex = pPwVcEntry->u4PwIfIndex;
    u4DestModId |= MPLS_APPLICATION_ERPS;
    MplsEventNotif.PathId.PwId.u4MegIndex =
        pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex;
    MplsEventNotif.PathId.PwId.u4MeIndex =
        pPwVcEntry->ErpsMegInfo.u4ErpsMeIndex;
    MplsEventNotif.PathId.PwId.u4MpIndex =
        pPwVcEntry->ErpsMegInfo.u4ErpsMepIndex;
    MplsEventNotif.PathId.PwId.u4PwIfIndex = pPwVcEntry->u4PwIfIndex;
    if ((u1OperStatus == CFA_IF_DOWN) &&
        (pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex != 0) &&
        (pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS) &&
        (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend != MPLS_PW_IF_DOWN) &&
        (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend != MPLS_PW_DOWN_EVENT))
    {
        MplsEventNotif.u2Event = MPLS_PW_IF_DOWN;
    }
    else if ((u1OperStatus == CFA_IF_UP) &&
             (pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex != 0) &&
             (pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS) &&
             (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend != MPLS_PW_IF_UP) &&
             (pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend != MPLS_PW_UP_EVENT))
    {
        MplsEventNotif.u2Event = MPLS_PW_IF_UP;
    }
    if (MplsEventNotif.u2Event != 0)
    {
        if (MplsPortEventNotification (u4DestModId, &MplsEventNotif)
            == OSIX_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtNotifyPwIfStatusToApp: "
                       "Failed to post Pseudo-wire interface change notification"
                       "to ERPS Module \r\n");
            return L2VPN_FAILURE;
        }
        L2VPN_DBG3 (L2VPN_DBG_DBG_SIG_FLAG,
                    "L2vpnExtNotifyPwIfStatusToApp: "
                    "LastStatusSend:%x Notifying Application ERPS with Event:%x for PW VCID:%d\r\n",
                    pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend,
                    MplsEventNotif.u2Event, pPwVcEntry->u4PwVcIndex);
        pPwVcEntry->ErpsMegInfo.u2LastPwStatusSend = MplsEventNotif.u2Event;
    }
    return L2VPN_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function     : L2vpnExtGetPwInfo                                          */
/*                                                                           */
/* Description  : This function provides the PW information based on         */
/*                multiple inputs (Pw index, Vc-index, 128 FEC, 129 FECs     */
/*                informations.                                              */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtGetPwInfo (tMplsApiInInfo * pInMplsApiInfo,
                   tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               au1Saii[L2VPN_MAX_AI_LEN];
    UINT1               au1Taii[L2VPN_MAX_AI_LEN];
    tGenU4Addr          GenU4Addr;
    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));
    if (pOutMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtGetPwInfo: "
                   "OUT API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (au1Saii, 0, L2VPN_MAX_AI_LEN);
    MEMSET (au1Taii, 0, L2VPN_MAX_AI_LEN);

    switch (pInMplsApiInfo->u4SubReqType)
    {
        case MPLS_GET_PW_INFO_FROM_PW_INDEX:
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pInMplsApiInfo->InPathId.
                                                     PwId.u4PwIndex);
            break;
        case MPLS_GET_PW_INFO_FROM_VCID:
            pPwVcEntry = L2VpnGetPwVcEntryFromVcId
                (pInMplsApiInfo->InPathId.PwId.u4VcId);
            break;
        case MPLS_GET_PW_INFO_FROM_GENTYPE2:
            pInMplsApiInfo->InPathId.PwId.SrcNodeId.MplsGlobalNodeId.u4GlobalId
                = OSIX_HTONL (pInMplsApiInfo->InPathId.PwId.SrcNodeId.
                              MplsGlobalNodeId.u4GlobalId);
            pInMplsApiInfo->InPathId.PwId.SrcNodeId.MplsGlobalNodeId.u4NodeId =
                OSIX_HTONL (pInMplsApiInfo->InPathId.PwId.SrcNodeId.
                            MplsGlobalNodeId.u4NodeId);
            pInMplsApiInfo->InPathId.PwId.u4SrcAcId =
                OSIX_HTONL (pInMplsApiInfo->InPathId.PwId.u4SrcAcId);
            /* Fill the PW identifiers from SAII */
            MEMCPY (&au1Saii[L2VPN_GEN_FEC_GLOBAL_ID_OFFSET],
                    &pInMplsApiInfo->InPathId.PwId.SrcNodeId.
                    MplsGlobalNodeId.u4GlobalId, sizeof (UINT4));
            MEMCPY (&au1Saii[L2VPN_GEN_FEC_NODE_ID_OFFSET],
                    &pInMplsApiInfo->InPathId.PwId.SrcNodeId.
                    MplsGlobalNodeId.u4NodeId, sizeof (UINT4));
            MEMCPY (&au1Saii[L2VPN_GEN_FEC_AC_ID_OFFSET],
                    &pInMplsApiInfo->InPathId.PwId.u4SrcAcId, sizeof (UINT4));

            pInMplsApiInfo->InPathId.PwId.DstNodeId.MplsGlobalNodeId.
                u4GlobalId =
                OSIX_HTONL (pInMplsApiInfo->InPathId.PwId.DstNodeId.
                            MplsGlobalNodeId.u4GlobalId);
            pInMplsApiInfo->InPathId.PwId.DstNodeId.MplsGlobalNodeId.
                u4NodeId =
                OSIX_HTONL (pInMplsApiInfo->InPathId.PwId.DstNodeId.
                            MplsGlobalNodeId.u4NodeId);
            pInMplsApiInfo->InPathId.PwId.u4DstAcId =
                OSIX_HTONL (pInMplsApiInfo->InPathId.PwId.u4DstAcId);

            /* Fill the PW identifiers from SAII */
            MEMCPY (&au1Taii[L2VPN_GEN_FEC_GLOBAL_ID_OFFSET],
                    &pInMplsApiInfo->InPathId.PwId.DstNodeId.
                    MplsGlobalNodeId.u4GlobalId, sizeof (UINT4));
            MEMCPY (&au1Taii[L2VPN_GEN_FEC_NODE_ID_OFFSET],
                    &pInMplsApiInfo->InPathId.PwId.DstNodeId.
                    MplsGlobalNodeId.u4NodeId, sizeof (UINT4));
            MEMCPY (&au1Taii[L2VPN_GEN_FEC_AC_ID_OFFSET],
                    &pInMplsApiInfo->InPathId.PwId.u4DstAcId, sizeof (UINT4));

            if ((pInMplsApiInfo->InPathId.PwId.
                 SrcNodeId.MplsGlobalNodeId.u4GlobalId != 0) &&
                (pInMplsApiInfo->InPathId.PwId.SrcNodeId.
                 MplsGlobalNodeId.u4NodeId != 0) &&
                (pInMplsApiInfo->InPathId.PwId.u4SrcAcId != 0))
            {
                GenU4Addr.Addr.u4Addr = pInMplsApiInfo->InPathId.PwId.
                    DstNodeId.MplsGlobalNodeId.u4NodeId;
                GenU4Addr.u2AddrType = MPLS_IPV4_ADDR_TYPE;

                pPwVcEntry = L2VpnGetPwVcEntry (L2VPN_PWVC_OWNER_GEN_FEC_SIG,
                                                0, 0, pInMplsApiInfo->InPathId.
                                                PwId.au1Agi,
                                                au1Saii, au1Taii, GenU4Addr);
            }
            else
            {
                pPwVcEntry = L2VpnGetPwVcEntryFromAgiAndTaii
                    (pInMplsApiInfo->InPathId.PwId.au1Agi, au1Taii);
            }
            break;

        case MPLS_GET_PW_INFO_FROM_GENFEC:
            GenU4Addr.Addr.u4Addr = pInMplsApiInfo->InPathId.PwId.
                DstNodeId.MplsRouterId.u4_addr[0];
            GenU4Addr.u2AddrType = MPLS_IPV4_ADDR_TYPE;
            pPwVcEntry = L2VpnGetPwVcEntry (L2VPN_PWVC_OWNER_GEN_FEC_SIG,
                                            0, 0, pInMplsApiInfo->InPathId.
                                            PwId.au1Agi,
                                            pInMplsApiInfo->InPathId.PwId.
                                            au1Saii,
                                            pInMplsApiInfo->InPathId.PwId.
                                            au1Taii, GenU4Addr);
            break;
        case MPLS_GET_PW_INFO_FROM_PWIDFEC:
            GenU4Addr.Addr.u4Addr = pInMplsApiInfo->InPathId.PwId.
                DstNodeId.MplsRouterId.u4_addr[0];
            GenU4Addr.u2AddrType = MPLS_IPV4_ADDR_TYPE;
            pPwVcEntry = L2VpnGetPwVcEntry (L2VPN_PWVC_OWNER_PWID_FEC_SIG,
                                            pInMplsApiInfo->InPathId.PwId.
                                            u4VcId,
                                            (UINT1) pInMplsApiInfo->InPathId.
                                            PwId.u2PwVcType, NULL, NULL, NULL,
                                            GenU4Addr);
            break;
        default:
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtGetPwInfo: " "Invalid sub request type\r\n");
            return OSIX_FAILURE;
    }

    if (L2vpnExtFillPwInfo (pPwVcEntry, pOutMplsApiInfo) == OSIX_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtGetPwInfo: "
                   "Failed to fill the PW information\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtPwUpdateSessionParams                              */
/*                                                                           */
/* Description  : This function updates the session params for PW            */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtPwUpdateSessionParams (tMplsApiInInfo * pInMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    /* Get the PW entry from VC-ID */
    pPwVcEntry = L2VpnGetPwVcEntryFromVcId
        (pInMplsApiInfo->InPathId.PwId.u4VcId);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtPwUpdateSessionParams: "
                    "Failed to find the matching PwVc Entry "
                    "VCID = %d\r\n", pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    if (pPwVcEntry->bPwIntOamEnable == L2VPN_PW_OAM_DISABLE)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtHandlePathStatusChgForPw: "
                    "OAM is disabled on a Pseudowire"
                    "VCID = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    pPwVcEntry->u4ProactiveSessionIndex =
        pInMplsApiInfo->InOamSessionParams.u4ProactiveSessIndex;
    pPwVcEntry->u1OamOperStatus = L2VPN_PWVC_OPER_UNKWN;

    if (pPwVcEntry->u4ProactiveSessionIndex != 0)
    {
        pPwVcEntry->bOamEnable = TRUE;
    }
    else
    {
        pPwVcEntry->bOamEnable = FALSE;
        if ((pPwVcEntry->u1CPOrMgmtOperStatus == L2VPN_PWVC_UP) &&
            (pPwVcEntry->i1OperStatus == L2VPN_PWVC_DOWN))
        {
            if (L2VpnUpdateOperStatus (pPwVcEntry,
                                       pPwVcEntry->u1CPOrMgmtOperStatus,
                                       L2VPN_PWVC_OPER_APP_OAM)
                == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                            "L2vpnExtPwUpdateSessionParams: "
                            "Failed to update the PW OAM oper. status "
                            "VCID = %d\r\n",
                            pInMplsApiInfo->InPathId.PwId.u4VcId);
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtGetPwServicePointerOid                             */
/*                                                                           */
/* Description  : This function provides the first accessible column         */
/*                OID of the table with/without indices.                     */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtGetPwServicePointerOid (tMplsApiInInfo * pInMplsApiInfo,
                                tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4OidLen = 0;

    if (pOutMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtGetPwServicePointerOid: "
                   "OUT API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
            PwType, sizeof (PwType));
    switch (pInMplsApiInfo->u4SubReqType)
    {
        case MPLS_GET_BASE_OID_FROM_VCID:
            pPwVcEntry = L2VpnGetPwVcEntryFromVcId
                (pInMplsApiInfo->InPathId.PwId.u4VcId);
            if (pPwVcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                            "L2vpnExtGetPwServicePointerOid: "
                            "Failed to find the matching PwVc Entry "
                            "VCID = %d: INTMD-EXIT\r\n",
                            pInMplsApiInfo->InPathId.PwId.u4VcId);
                return OSIX_FAILURE;
            }
            u4OidLen = sizeof (PwType) / sizeof (UINT4);
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pPwVcEntry->u4PwVcIndex;
            u4OidLen = u4OidLen + 1;
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            break;
        case MPLS_GET_BASE_PW_OID:
            u4OidLen = sizeof (PwType) / sizeof (UINT4);
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtHandlePathStatusChgForPw                           */
/*                                                                           */
/* Description  : This function handles the path status change for PW        */
/*                from external OAM modules.                                 */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
L2vpnExtHandlePathStatusChgForPw (tMplsApiInInfo * pInMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1Event = 0;

    /* Get the PW entry from VC ID */
    pPwVcEntry = L2VpnGetPwVcEntryFromVcId
        (pInMplsApiInfo->InPathId.PwId.u4VcId);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtHandlePathStatusChgForPw: "
                    "Failed to find the matching PwVc Entry "
                    "VCID = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    if (pPwVcEntry->bPwIntOamEnable == L2VPN_PW_OAM_DISABLE)
    {
        /* Admin has not enabled the OAM for this PW */
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtHandlePathStatusChgForPw: "
                    "OAM is disabled on a Pseudowire"
                    "VCID = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    if ((pInMplsApiInfo->u4SrcModId == BFD_MODULE) &&
        (pPwVcEntry->bOamEnable == FALSE))
    {
        /* Proactive session params should have been updated 
         * before notifying the OAM status for PW */
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtHandlePathStatusChgForPw "
                   "OAM is not enabled on the pseudowire: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    /* Update the PW operational status here. */
    if (pInMplsApiInfo->InOamPathStatus.u1PathStatus == MPLS_PATH_STATUS_UP)
    {
        u1Event = L2VPN_PWVC_UP;
    }
    else
    {
        u1Event = L2VPN_PWVC_DOWN;
    }
    if (L2VpnUpdateOperStatus (pPwVcEntry, u1Event,
                               L2VPN_PWVC_OPER_APP_OAM) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtHandlePathStatusChgForPw: "
                    "Failed to update PW OAM operational status "
                    "VCID = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtGetPwInfoFromInLblInfo                             */
/*                                                                           */
/* Description  : This function provides the PW entry if exists based on     */
/*                Incoming VC label.                                         */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtGetPwInfoFromInLblInfo (tMplsApiInInfo * pInMplsApiInfo,
                                tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (pOutMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtGetPwInfoFromInLblInfo: "
                   "OUT API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromLabel (pInMplsApiInfo->InInLblInfo.
                                             u4Inlabel,
                                             pInMplsApiInfo->InInLblInfo.
                                             u4InIf);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtGetPwInfoFromInLblInfo: "
                   "PW does not exist: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    if ((pPwVcEntry->u4MegIndex != 0) &&
        (pPwVcEntry->u4MeIndex != 0) && (pPwVcEntry->u4MpIndex != 0))
    {
        pInMplsApiInfo->u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
        pInMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pInMplsApiInfo->InPathId.MegId.u4MegIndex = pPwVcEntry->u4MegIndex;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex = pPwVcEntry->u4MeIndex;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex = pPwVcEntry->u4MpIndex;

        if (MplsApiHandleExternalRequest (MPLS_OAM_GET_MEG_INFO,
                                          pInMplsApiInfo, pOutMplsApiInfo)
            == OSIX_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtGetPwInfoFromInLblInfo: "
                       "Filling MEG information failed:" "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    if (L2vpnExtFillPwInfo (pPwVcEntry, pOutMplsApiInfo) == OSIX_FAILURE)
    {
        L2VPN_DBG2 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtGetPwInfoFromInLblInfo: "
                    "Failed to find the matching PwVc Entry for "
                    "InIntf =%d InVcLabel = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InInLblInfo.u4InIf,
                    pInMplsApiInfo->InInLblInfo.u4Inlabel);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtFillPwInfo                                         */
/*                                                                           */
/* Description  : This function fills the PW information from PW entry       */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
L2vpnExtFillPwInfo (tPwVcEntry * pPwVcEntry, tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tTeTnlIndices       ProtTnlInfo;
    tTeTnlIndices       BkpTnlInfo;
    UINT4               u4NodeId = 0;
    UINT4               u4L3Intf = 0;

    MEMSET (&ProtTnlInfo, 0, sizeof (tTeTnlIndices));
    MEMSET (&BkpTnlInfo, 0, sizeof (tTeTnlIndices));

    /* If PW is not ready for operation, return failure from here 
     *
     * AC Information is not required for OAM modules like BFD, LSP-Ping and 
     * ELPS. So, AC Information need not be present. If AC Information is
     * present fill that information to the caller. Hence, the pServSpecEntry 
     * is not checked for NULL here. */
    if ((pPwVcEntry == NULL) || (pPwVcEntry->pPSNEntry == NULL))
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtFillPwInfo: "
                   "PW entry/PW PSN info. doesnot exist\r\n");
        return OSIX_FAILURE;
    }
    if (pPwVcEntry->u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_2)
    {
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.SrcNodeId.u4NodeType =
            MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

        /* Fill the GEN FEC Type-2 identifiers */
        MEMCPY (&pOutMplsApiInfo->OutPwInfo.MplsPwPathId.SrcNodeId.
                MplsGlobalNodeId.u4GlobalId,
                &pPwVcEntry->au1Saii[L2VPN_GEN_FEC_GLOBAL_ID_OFFSET],
                sizeof (UINT4));
        MEMCPY (&pOutMplsApiInfo->OutPwInfo.MplsPwPathId.SrcNodeId.
                MplsGlobalNodeId.u4NodeId,
                &pPwVcEntry->au1Saii[L2VPN_GEN_FEC_NODE_ID_OFFSET],
                sizeof (UINT4));
        MEMCPY (&pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4SrcAcId,
                &pPwVcEntry->au1Saii[L2VPN_GEN_FEC_AC_ID_OFFSET],
                sizeof (UINT4));
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.SrcNodeId.MplsGlobalNodeId.
            u4GlobalId = OSIX_NTOHL (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                     SrcNodeId.MplsGlobalNodeId.u4GlobalId);
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.SrcNodeId.MplsGlobalNodeId.
            u4NodeId = OSIX_NTOHL (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                   SrcNodeId.MplsGlobalNodeId.u4NodeId);
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4SrcAcId =
            OSIX_NTOHL (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4SrcAcId);

        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

        /* Fill the GEN FEC Type-2 identifiers */
        MEMCPY (&pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                MplsGlobalNodeId.u4GlobalId,
                &pPwVcEntry->au1Taii[L2VPN_GEN_FEC_GLOBAL_ID_OFFSET],
                sizeof (UINT4));
        MEMCPY (&pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                MplsGlobalNodeId.u4NodeId,
                &pPwVcEntry->au1Taii[L2VPN_GEN_FEC_NODE_ID_OFFSET],
                sizeof (UINT4));
        MEMCPY (&pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4DstAcId,
                &pPwVcEntry->au1Taii[L2VPN_GEN_FEC_AC_ID_OFFSET],
                sizeof (UINT4));
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.MplsGlobalNodeId.
            u4GlobalId = OSIX_NTOHL (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                     DstNodeId.MplsGlobalNodeId.u4GlobalId);
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.MplsGlobalNodeId.
            u4NodeId = OSIX_NTOHL (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4DstAcId =
            OSIX_NTOHL (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4DstAcId);
    }
    else
    {
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;
        MEMCPY (&u4NodeId, &pPwVcEntry->PeerAddr, sizeof (UINT4));
        u4NodeId = OSIX_NTOHL (u4NodeId);
        pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
            MplsRouterId.u4_addr[0] = u4NodeId;
    }

    pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
    pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_PW;

    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId = pPwVcEntry->u4PwVcID;
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4PwIndex = pPwVcEntry->u4PwVcIndex;

    MEMCPY (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.au1Agi,
            pPwVcEntry->au1Agi, pPwVcEntry->u1AgiLen);
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1AgiLen = pPwVcEntry->u1AgiLen;

    MEMCPY (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.au1Saii,
            pPwVcEntry->au1Saii, pPwVcEntry->u1SaiiLen);
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1SaiiLen = pPwVcEntry->u1SaiiLen;

    MEMCPY (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.au1Taii,
            pPwVcEntry->au1Taii, pPwVcEntry->u1TaiiLen);
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1TaiiLen = pPwVcEntry->u1TaiiLen;

    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u2PwVcType =
        (UINT2) pPwVcEntry->i1PwVcType;
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1PwVcOwner =
        (UINT1) pPwVcEntry->i1PwVcOwner;

    /*  Fill the AII types */
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1AgiType =
        (UINT1) pPwVcEntry->u4GenAgiType;
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1LocalAIIType =
        (UINT1) pPwVcEntry->u4LocalAiiType;
    pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u1RemoteAIIType =
        (UINT1) pPwVcEntry->u4RemoteAiiType;

    pOutMplsApiInfo->OutPwInfo.MplsMegId.u4MegIndex = pPwVcEntry->u4MegIndex;
    pOutMplsApiInfo->OutPwInfo.MplsMegId.u4MeIndex = pPwVcEntry->u4MeIndex;
    pOutMplsApiInfo->OutPwInfo.MplsMegId.u4MpIndex = pPwVcEntry->u4MpIndex;

    pOutMplsApiInfo->OutPwInfo.u4OutVcLabel = pPwVcEntry->u4OutVcLabel;
    pOutMplsApiInfo->OutPwInfo.u4InVcLabel = pPwVcEntry->u4InVcLabel;
    pOutMplsApiInfo->OutPwInfo.u4LocalGroupID = pPwVcEntry->u4LocalGroupID;
    pOutMplsApiInfo->OutPwInfo.u4RemoteGroupID = pPwVcEntry->u4RemoteGroupID;
    pOutMplsApiInfo->OutPwInfo.u4ProactiveSessIndex =
        pPwVcEntry->u4ProactiveSessionIndex;

    pOutMplsApiInfo->OutPwInfo.u4OutIf = pPwVcEntry->u4PwL3Intf;

    /* To get the L3 interface from Tunnel Interface Index  */
    if (MplsGetL3Intf (pOutMplsApiInfo->OutPwInfo.u4OutIf, &(u4L3Intf)) ==
        MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2vpnExtFillPwInfo : "
                   "getting L3 interface from Tunnel Interface Index Failed");
    }

    /* To get thei VLAN ID of the L3 interface associated to 
     * MPLS tunnel interface from L3 interface index  */
    if (CfaGetVlanId (u4L3Intf, &(pOutMplsApiInfo->OutPwInfo.u2VlanId)) ==
        CFA_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2vpnExtFillPwInfo : "
                   "getting VLAN ID Failed");
    }

    MEMCPY (pOutMplsApiInfo->OutPwInfo.au1PwVcName,
            pPwVcEntry->au1PwVcName, L2VPN_PWVC_MAX_NAME_LEN);

    pOutMplsApiInfo->OutPwInfo.u1CcSelected = pPwVcEntry->u1LocalCcSelected;
    pOutMplsApiInfo->OutPwInfo.u1CvSelected = pPwVcEntry->u1LocalCvSelected;

    pOutMplsApiInfo->OutPwInfo.u1PwVcMode = pPwVcEntry->u1PwVcMode;
    pOutMplsApiInfo->OutPwInfo.u1IpVersion = IP_VERSION_4;

    /* NextHop Mac address pwOnly to be added in the PwVcEntry */
    MEMCPY (&pOutMplsApiInfo->OutPwInfo.au1NextHopMac,
            pPwVcEntry->au1NextHopMac, MAC_ADDR_LEN);

    pOutMplsApiInfo->OutPwInfo.u4PwIfIndex = pPwVcEntry->u4PwIfIndex;
    pOutMplsApiInfo->OutPwInfo.u4PwVcIndex = pPwVcEntry->u4PwVcIndex;

    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    MEMCPY ((UINT1 *) &pOutMplsApiInfo->OutPwInfo.u4VpnId,
            &(pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
            sizeof (UINT4));

    pOutMplsApiInfo->OutPwInfo.u4VpnId =
        OSIX_HTONL (pOutMplsApiInfo->OutPwInfo.u4VpnId);

    pOutMplsApiInfo->OutPwInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);

    pOutMplsApiInfo->OutPwInfo.bOamEnable = pPwVcEntry->bOamEnable;

    pOutMplsApiInfo->OutPwInfo.i1OperStatus =
        (INT1) pPwVcEntry->u1CPOrMgmtOperStatus;
    pOutMplsApiInfo->OutPwInfo.u1PwVcMode = pPwVcEntry->u1PwVcMode;

    /* To get the physical port */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          pOutMplsApiInfo->OutPwInfo.
                                          au1NextHopMac,
                                          &(pOutMplsApiInfo->OutPwInfo.
                                            u4PhyPort)) != MPLS_SUCCESS)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "L2VpnPwVcHwAddPwTe : "
                    "Fetching Physical port for the L3 interface : %d "
                    "failed.\r\n", u4L3Intf);
    }

    /* AC information filling */

    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            /* VPWS and VPLS (with only one associated attachment circuit) 
             * are only taken care here..i.e 
             * it is expected to have only one Enet entry associated 
             * with the PW */

            /****VPLS with more than one associated attachment circuits is 
             * not considered here****/

            pOutMplsApiInfo->OutPwInfo.u4PortVlan = pPwVcEnetEntry->u2PortVlan;
            pOutMplsApiInfo->OutPwInfo.i4PortIfIndex =
                pPwVcEnetEntry->i4PortIfIndex;
            pOutMplsApiInfo->OutPwInfo.i1VlanMode = pPwVcEnetEntry->i1VlanMode;
        }
    }

    /* PSN information filling */
    pPwVcMplsEntry = pPwVcEntry->pPSNEntry;
    pOutMplsApiInfo->OutPwInfo.u1MplsType = pPwVcMplsEntry->u1MplsType;
    pOutMplsApiInfo->OutPwInfo.u1Ttl = pPwVcMplsEntry->u1Ttl;

    if (pPwVcMplsEntry->PwVcMplsOutTnl.u1TnlType == L2VPN_MPLS_TYPE_TE)
    {
        /* TE as outbound tunnel */
        ProtTnlInfo.u4TnlIndex =
            pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.u4TnlIndex;
        ProtTnlInfo.u4TnlInstance =
            pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.u2TnlInstance;
        MEMCPY (ProtTnlInfo.TnlIngressLsrId,
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.
                TnlLclLSR, sizeof (UINT4));
        MEMCPY (ProtTnlInfo.TnlEgressLsrId,
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.
                TnlPeerLSR, sizeof (UINT4));

        pOutMplsApiInfo->OutPwInfo.TeTnlId.SrcNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;
        pOutMplsApiInfo->OutPwInfo.TeTnlId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;

        if (TeCmnExtGetBkpTnlFrmInUseProtTnl (&ProtTnlInfo, &BkpTnlInfo)
            == TE_SUCCESS)
        {
            pOutMplsApiInfo->OutPwInfo.TeTnlId.u4SrcTnlNum =
                BkpTnlInfo.u4TnlIndex;
            pOutMplsApiInfo->OutPwInfo.TeTnlId.u4LspNum =
                BkpTnlInfo.u4TnlInstance;

            MEMCPY (&u4NodeId, BkpTnlInfo.TnlIngressLsrId, sizeof (UINT4));
            u4NodeId = OSIX_NTOHL (u4NodeId);
            pOutMplsApiInfo->OutPwInfo.TeTnlId.SrcNodeId.
                MplsRouterId.u4_addr[0] = u4NodeId;

            MEMCPY (&u4NodeId, BkpTnlInfo.TnlEgressLsrId, sizeof (UINT4));
            u4NodeId = OSIX_NTOHL (u4NodeId);
            pOutMplsApiInfo->OutPwInfo.TeTnlId.DstNodeId.
                MplsRouterId.u4_addr[0] = u4NodeId;
        }
        else
        {
            pOutMplsApiInfo->OutPwInfo.TeTnlId.u4SrcTnlNum =
                ProtTnlInfo.u4TnlIndex;
            pOutMplsApiInfo->OutPwInfo.TeTnlId.u4LspNum =
                ProtTnlInfo.u4TnlInstance;

            MEMCPY (&u4NodeId, ProtTnlInfo.TnlIngressLsrId, sizeof (UINT4));
            u4NodeId = OSIX_NTOHL (u4NodeId);
            pOutMplsApiInfo->OutPwInfo.TeTnlId.SrcNodeId.
                MplsRouterId.u4_addr[0] = u4NodeId;

            MEMCPY (&u4NodeId, ProtTnlInfo.TnlEgressLsrId, sizeof (UINT4));
            u4NodeId = OSIX_NTOHL (u4NodeId);
            pOutMplsApiInfo->OutPwInfo.TeTnlId.DstNodeId.
                MplsRouterId.u4_addr[0] = u4NodeId;
        }
    }
    else if (pPwVcMplsEntry->PwVcMplsOutTnl.u1TnlType == L2VPN_MPLS_TYPE_NONTE)
    {
        /* NON-TE as outbound tunnel */
        pOutMplsApiInfo->OutPwInfo.NonTeTnlId =
            pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.NonTeInfo.u4LsrXcIndex;
    }
    else if (pPwVcMplsEntry->PwVcMplsOutTnl.u1TnlType == L2VPN_MPLS_TYPE_VCONLY)
    {
        /* No outbound tunnel */
        pOutMplsApiInfo->OutPwInfo.PwOnlyIfIndex =
            pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.VcInfo.u4IfIndex;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtUpdatePwLpsStatus                                  */
/*                                                                           */
/* Description  : This function updates the session params for PW            */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtUpdatePwLpsStatus (tMplsApiInInfo * pInMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pBkpPwVcEntry = NULL;
    UINT4               u4BkpPwVcID = 0;
    UINT1               u1PrevProtStatus = 0;
    UINT1               u1OperApp = 0;

    pPwVcEntry = L2VpnGetPwVcEntryFromVcId
        (pInMplsApiInfo->InPathId.PwId.u4VcId);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtUpdatePwLpsStatus: "
                    "Failed to find the matching PwVc Entry "
                    "VCID = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);

        return OSIX_FAILURE;
    }

    u4BkpPwVcID = pPwVcEntry->u4BkpPwVcID;

    switch (pInMplsApiInfo->u4SubReqType)
    {
        case MPLS_LPS_PROT_AVAILABLE:
            pBkpPwVcEntry = L2VpnGetPwVcEntryFromVcId
                (pInMplsApiInfo->InServicePathId.PwId.u4VcId);

            if (pBkpPwVcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Protection PW VC Entry %d do not exist\n",
                            pInMplsApiInfo->InServicePathId.PwId.u4VcId);
                return OSIX_FAILURE;
            }

            /* Map the Enet Entry list of Working PW to Protection PW. */
            pBkpPwVcEntry->pServSpecEntry = pPwVcEntry->pServSpecEntry;

            /* Handle AC UP Event for Protection PW */
            L2VpnHandleAcUp (pBkpPwVcEntry, L2VPN_PWVC_IF_EVENT);

            pPwVcEntry->u4BkpPwVcID =
                pInMplsApiInfo->InServicePathId.PwId.u4VcId;
            pPwVcEntry->u1PwPathType = L2VPN_PW_WORKING_PATH;
            pPwVcEntry->u1ProtStatus = LOCAL_PROT_AVAIL;
            pBkpPwVcEntry->u4BkpPwVcID = pInMplsApiInfo->InPathId.PwId.u4VcId;
            pBkpPwVcEntry->u1PwPathType = L2VPN_PW_PROTECTION_PATH;
            pPwVcEntry->u1ElpsMode = pInMplsApiInfo->InElpsMode;
            pBkpPwVcEntry->u1ElpsMode = pInMplsApiInfo->InElpsMode;
            break;

        case MPLS_LPS_PROT_NOT_APPLICABLE:
        case MPLS_LPS_PROT_NOT_AVAILABLE:
            u1PrevProtStatus = pPwVcEntry->u1ProtStatus;
            if (pInMplsApiInfo->u4SubReqType == MPLS_LPS_PROT_NOT_AVAILABLE)
            {
                pPwVcEntry->u1ProtStatus = LOCAL_PROT_NOT_AVAIL;
            }
            else if (pInMplsApiInfo->u4SubReqType ==
                     MPLS_LPS_PROT_NOT_APPLICABLE)
            {
                pPwVcEntry->u4BkpPwVcID = 0;
                pPwVcEntry->u1ProtStatus = LOCAL_PROT_NOT_APPLICABLE;
            }

            pBkpPwVcEntry = L2VpnGetPwVcEntryFromVcId (u4BkpPwVcID);

            if (pBkpPwVcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Protection PW VC Entry %d do not exist\n",
                            pInMplsApiInfo->InServicePathId.PwId.u4VcId);
                return OSIX_FAILURE;
            }

            /* Handle AC Down Event for Protection PW */
            L2VpnHandleAcDown (pBkpPwVcEntry, L2VPN_PWVC_IF_EVENT);

            /* Unmap AC Enet Entry list of working PW from protection PW */
            pBkpPwVcEntry->pServSpecEntry = NULL;

            pPwVcEntry->u1PwPathType = L2VPN_PW_WORKING_PATH;
            pBkpPwVcEntry->u1PwPathType = L2VPN_PW_WORKING_PATH;

            /* Indicate the actual pseudowire status to applications */
            if (u1PrevProtStatus == LOCAL_PROT_IN_USE)
            {
                if (pPwVcEntry->i1OperStatus == L2VPN_PWVC_UP)
                {
                    break;
                }
                u1OperApp = L2VPN_PWVC_OPER_APP_CP_OR_MGMT;
                if (pPwVcEntry->bOamEnable == TRUE)
                {
                    if (!((pPwVcEntry->u1OamOperStatus == L2VPN_PWVC_OPER_UP) &&
                          (pPwVcEntry->u1CPOrMgmtOperStatus ==
                           L2VPN_PWVC_OPER_UP)))
                    {
                        break;
                    }
                }
                else if (pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_OPER_UP)
                {
                    break;
                }

                if (L2VpnUpdateOperStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                           u1OperApp) == OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
            break;

        case MPLS_LPS_PROT_IN_USE:
            if (pPwVcEntry->u1ProtStatus != LOCAL_PROT_AVAIL)
            {
                L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                            "L2vpnExtUpdatePwLpsStatus: "
                            "Invalid protection status "
                            "VCID = %d: INTMD-EXIT\r\n",
                            pInMplsApiInfo->InPathId.PwId.u4VcId);
                return OSIX_FAILURE;
            }
            pPwVcEntry->u1ProtStatus = LOCAL_PROT_IN_USE;
            break;
        default:
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnExtHdlMegAssocAndOamStatus                           */
/* Description   : This function associates the MEG with the PW and          */
/*                 notifies the proactive session status through MEG         */
/*                                                                           */
/* Input(s)      : u4MainReqType - Main request type                         */
/*                 pInMplsApiInfo - In API information                       */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS/OSIX_FAILURE                                 */
/*****************************************************************************/
INT4
L2vpnExtHdlMegAssocAndOamStatus (UINT4 u4MainReqType,
                                 tMplsApiInInfo * pInMplsApiInfo)
{
    tL2VpnQMsg          L2VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\rL2vpnExtHdlMegAssocAndOamStatus: Function Entry\n");

    MEMSET (&L2VpnQMsg, 0, sizeof (tL2VpnQMsg));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\rL2VPN is Down\n");
        return OSIX_FAILURE;
    }

    L2VPN_QMSG_TYPE = L2VPN_PWVC_OAM_MEG_EVENT;

    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u4MegIndex =
        pInMplsApiInfo->InPathId.MegId.u4MegIndex;
    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u4MeIndex =
        pInMplsApiInfo->InPathId.MegId.u4MeIndex;
    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u4MpIndex =
        pInMplsApiInfo->InPathId.MegId.u4MpIndex;
    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u4ContextId = pInMplsApiInfo->u4ContextId;
    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u4PwIndex =
        pInMplsApiInfo->InServicePathId.PwId.u4PwIndex;
    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u1PathStatus =
        pInMplsApiInfo->InOamPathStatus.u1PathStatus;
    L2VpnQMsg.L2VpnEvtInfo.PwOamInfo.u1CmdType = (UINT1) u4MainReqType;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\rMemory allocation failed "
                   "for L2VPN Q\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    if (L2VpnInternalEventHandler (pu1TmpMsg) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\r OAM Event not sent to L2VPN\n");
        return OSIX_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\rL2vpnExtHdlMegAssocAndOamStatus: Function Exit\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtUpdateOamAppStatus                                 */
/*                                                                           */
/* Description  : This function updates the OAM applications status          */
/*                i.e., OAM application enabled/disabled for this PW         */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtUpdateOamAppStatus (tMplsApiInInfo * pInMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    pPwVcEntry = L2VpnGetPwVcEntryFromVcId
        (pInMplsApiInfo->InPathId.PwId.u4VcId);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtUpdateOamAppStatus: "
                    "Failed to find the matching PwVc Entry "
                    "VCID = %d: INTMD-EXIT\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    if (pInMplsApiInfo->u4SrcModId == Y1731_MODULE)
    {
        if (pInMplsApiInfo->InY1731OamStatus == TRUE)
        {
            pPwVcEntry->u1MplsOamAppType = MPLS_OAM_TYPE_Y1731;
        }
        else
        {
            pPwVcEntry->u1MplsOamAppType = MPLS_OAM_TYPE_NONE;
        }
    }
#ifdef RFC6374_WANTED
    if (pInMplsApiInfo->u4SrcModId == RFC6374_MODULE)
    {
        if (pInMplsApiInfo->InRFC6374Status == TRUE)
        {
            pPwVcEntry->u1MplsPmAppType = MPLS_PM_TYPE_RFC6374;
        }
        else
        {
            pPwVcEntry->u1MplsPmAppType = MPLS_OAM_TYPE_NONE;
        }
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtGetPktCnt                                          */
/*                                                                           */
/* Description  : This function provides the  Transmitted and Received MPLS  */
/*                Packet Count                                               */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtGetPktCnt (tMplsApiInInfo * pInMplsApiInfo,
                   tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
    UINT4               u4CurTotCnt = MPLS_ZERO;
    UINT4               u4PrevDayIndex = MPLS_ZERO;
    INT1                i1IntervalIndex = MPLS_ZERO;

    if (pInMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtGetPktCnt: "
                   "OUT API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    pPwVcEntry =
        L2VpnGetPwVcEntryFromVcId (pInMplsApiInfo->unIntInfo.MplsPktCntInfo.
                                   u4PwId);

    if (pPwVcEntry != NULL)
    {
        pPwVcPerfInfo =
            L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
        i1IntervalIndex =
            (INT1) ((INT1)
                    (pInMplsApiInfo->unIntInfo.MplsPktCntInfo.u4LmmInterval /
                     (L2VPN_PWVC_PERF_INTV_IN_MS)) + 1);
        /* If the day counter has expired between last and current Lm query */
        if (L2VPN_PWVC_PERF_CURRENT_INTERVAL < i1IntervalIndex)
        {
            u4PrevDayIndex = (UINT4) (pPwVcPerfInfo->u1PwVcPerfDayIndex - 1);
            if (u4PrevDayIndex <= MPLS_ZERO)
            {
                u4PrevDayIndex = L2VPN_PWVC_MAX_PERF_DAYS;
            }

            /* Update the Mpls Total day count with Last day info */
            pInMplsApiInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4TxCnt +=
                pPwVcPerfInfo->aPwVcPerf1DayIntervalInfo
                [u4PrevDayIndex - 1].OutHCPkts.u4Lo;
            pInMplsApiInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4RxCnt +=
                pPwVcPerfInfo->aPwVcPerf1DayIntervalInfo
                [u4PrevDayIndex - 1].InHCPkts.u4Lo;
        }

        /* Get current interval count */
        if (nmhGetPwPerfCurrentOutPackets
            (pPwVcEntry->u4PwVcIndex, &u4CurTotCnt) == SNMP_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtGetPktCnt: "
                       "reading Pw CurrentOutPackets Failed\r\n");
        }

        /* Add with Current 1 day Interval count */
        u4CurTotCnt += pPwVcPerfInfo->aPwVcPerf1DayIntervalInfo
            [pPwVcPerfInfo->u1PwVcPerfDayIndex - 1].OutHCPkts.u4Lo;

        /* Add with Total day count */
        u4CurTotCnt +=
            pInMplsApiInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4TxCnt;
        pOutMplsApiInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4TxCnt =
            u4CurTotCnt;

        u4CurTotCnt = MPLS_ZERO;

        /* Get current interval count */
        if (nmhGetPwPerfCurrentInPackets (pPwVcEntry->u4PwVcIndex, &u4CurTotCnt)
            == SNMP_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtGetPktCnt: "
                       "reading Pw CurrentInPackets Failed\r\n");
        }
        /* Add with Current 1 day Interval count */
        u4CurTotCnt += pPwVcPerfInfo->aPwVcPerf1DayIntervalInfo
            [pPwVcPerfInfo->u1PwVcPerfDayIndex - 1].InHCPkts.u4Lo;

        /* Add with Total day count */
        u4CurTotCnt +=
            pInMplsApiInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4RxCnt;
        pOutMplsApiInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4RxCnt =
            u4CurTotCnt;

        /* Update the Out Info with Total Day count */
        pOutMplsApiInfo->unOutInfo.MplsPktCntRes.MplsTotDayCnt.u4TxCnt =
            pInMplsApiInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4TxCnt;
        pOutMplsApiInfo->unOutInfo.MplsPktCntRes.MplsTotDayCnt.u4RxCnt =
            pInMplsApiInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.u4RxCnt;
    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtIncrOutPktCnt: "
                   "PW does not exist: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtIncrOutPktCnt                                      */
/*                                                                           */
/* Description  : This function increments the  Transmitted MPLS             */
/*                Packet Count                                               */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtIncrOutPktCnt (tMplsApiInInfo * pInMplsApiInfo,
                       tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    UNUSED_PARAM (pOutMplsApiInfo);

    if (pInMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtIncrOutPktCnt: "
                   "OUT API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    pPwVcEntry =
        L2VpnGetPwVcEntryFromVcId (pInMplsApiInfo->unIntInfo.MplsPktCntInfo.
                                   u4PwId);
    if (pPwVcEntry != NULL)
    {
        pPwVcPerfInfo =
            L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
        FSAP_U8_INC (&pPwVcPerfInfo->aPwVcPerfIntervalInfo
                     [L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1].OutHCPkts);
    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtIncrOutPktCnt: "
                   "PW does not exist: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtIncrInPktCnt                                       */
/*                                                                           */
/* Description  : This function increments the  Receiveded MPLS              */
/*                Packet Count                                               */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtIncrInPktCnt (tMplsApiInInfo * pInMplsApiInfo,
                      tMplsApiOutInfo * pOutMplsApiInfo)
{

    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    UNUSED_PARAM (pOutMplsApiInfo);

    if (pInMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtIncrInPktCnt: "
                   "OUT API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromLabel (pInMplsApiInfo->InInLblInfo.
                                             u4Inlabel,
                                             pInMplsApiInfo->InInLblInfo.
                                             u4InIf);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtIncrInPktCnt: "
                   "PW does not exist: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
    FSAP_U8_INC (&pPwVcPerfInfo->aPwVcPerfIntervalInfo
                 [L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1].InHCPkts);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : L2vpnExtUpdatePwApp                                        */
/*                                                                           */
/* Description  : This function updates the application running over the PW  */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtUpdatePwApp (tMplsApiInInfo * pInMplsApiInfo,
                     tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1AppOwner = 0;

    UNUSED_PARAM (pOutMplsApiInfo);

    if (pInMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtUpdatePwApp: "
                   "IN API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    switch (pInMplsApiInfo->u4SrcModId)
    {
        case ELPS_MODULE:
            u1AppOwner = (UINT1) MPLS_APPLICATION_ELPS;
            break;
        case ERPS_MODULE_APP:
            u1AppOwner = (UINT1) MPLS_APPLICATION_ERPS;
            break;
        default:
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG, "L2vpnExtUpdatePwApp: "
                       "Invalid module\r\n");
            return OSIX_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pInMplsApiInfo->InPathId.
                                             PwId.u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtUpdatePwApp: " "PW does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pInMplsApiInfo->u4SubReqType == MPLS_APP_REGISTER)
    {
        pPwVcEntry->u1AppOwner |= u1AppOwner;
    }
    else
    {
        pPwVcEntry->u1AppOwner &= (UINT1) (~(u1AppOwner));
#ifdef HVPLS_WANTED
        if (u1AppOwner == (UINT1) MPLS_APPLICATION_ERPS)
        {
            /* Set value of ERPSMegInfo to 0 which were filled during the registration */
            pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex = 0;
            pPwVcEntry->ErpsMegInfo.u4ErpsMeIndex = 0;
            pPwVcEntry->ErpsMegInfo.u4ErpsMepIndex = 0;
        }
#endif
    }

    return OSIX_SUCCESS;
}

#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function     : L2vpnExtRegisterPwForNotif                                        */
/*                                                                           */
/* Description  : This function registers the PW for notification  */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnExtRegisterPwForNotif (tMplsApiInInfo * pInMplsApiInfo,
                            tMplsApiOutInfo * pOutMplsApiInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               i4RetStatus = 0;
    UINT1               u1OperStatus = 0;
    UNUSED_PARAM (pOutMplsApiInfo);

    if (pInMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtRegisterPwForNotif: "
                   "IN API information should not be NULL\r\n");
        return OSIX_FAILURE;
    }

    /* Check if the module that wants to register is
       a valid module */
    if (pInMplsApiInfo->InRegParams.u4ModId >= MPLS_MAX_APP_ID)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2vpnExtRegisterPwForNotif: "
                   "Invalid Module ID : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pInMplsApiInfo->InPathId.
                                            PwId.u4VcId);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_ERR_VC_FLAG,
                    "L2vpnExtRegisterPwForNotif: "
                    "Registration for PW notification fails pwVcEntry does not exist for VC ID: %d\r\n",
                    pInMplsApiInfo->InPathId.PwId.u4VcId);
        return OSIX_FAILURE;
    }
    L2VPN_DBG3 (L2VPN_DBG_ERR_VC_FLAG,
                "L2vpnExtRegisterPwForNotif: "
                "Registration with MPLS module from AppId=%d "
                "Events=%x Request type=%d\n",
                pInMplsApiInfo->InRegParams.u4ModId,
                pInMplsApiInfo->InRegParams.u4Events,
                pInMplsApiInfo->u4SubReqType);
    if (pInMplsApiInfo->u4SubReqType == MPLS_APP_REGISTER)
    {
        MPLS_CMN_LOCK ();
        i4RetStatus = MplsApiFillRegInfo (pInMplsApiInfo);
        MPLS_CMN_UNLOCK ();
        if (i4RetStatus == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2vpnExtRegisterPwForNotif: Registration Failed");
            return OSIX_FAILURE;
        }
        pPwVcEntry->ErpsMegInfo.u4ErpsMegIndex =
            pInMplsApiInfo->InPathId.PwId.u4MegIndex;
        pPwVcEntry->ErpsMegInfo.u4ErpsMeIndex =
            pInMplsApiInfo->InPathId.PwId.u4MeIndex;
        pPwVcEntry->ErpsMegInfo.u4ErpsMepIndex =
            pInMplsApiInfo->InPathId.PwId.u4MpIndex;
        /*Check the status of PWINTERFACE and notify the EPRS */
        if (CfaGetIfOperStatus (pPwVcEntry->u4PwIfIndex, &u1OperStatus) ==
            CFA_SUCCESS)
        {
            if (L2vpnExtNotifyPwIfStatusToApp (pPwVcEntry, u1OperStatus) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "\r L2vpnExtRegisterPwForNotif: PW IF status notification to "
                           "applciation(ERPS) failed\n");
                return OSIX_FAILURE;
            }
        }
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 *  FUNCTION NAME    : L2vpnHandleCfmPacket
 * 
 *  DESCRIPTION      : This function handles the CFM packet
 *
 *  INPUT            : pBuf - pointer to buffer received
 *                     u4PwIfIndex - pseudo-wire interface index
 *                     u4PktSize- size of packet received
 * 
 *  OUTPUT           : None
 *
 * RETURNS          : L2VPN_SUCCESS/ L2VPN_FAILURE
 * 
 ***************************************************************************/
INT4
L2vpnHandleCfmPacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT4 u4PwIfIndex, UINT4 u4PktSize)
{
    tMplsEventNotif     MplsEventNotif;
    MEMSET (&MplsEventNotif, 0, sizeof (tMplsEventNotif));

    L2VPN_DBG2 (L2VPN_DBG_FNC_SIG_FLAG, "L2vpnHandleCfmPacket: "
                "CFM Packet received over pwIfIndex:%d with packte size:%d\n",
                u4PwIfIndex, u4PktSize);

    MplsEventNotif.pBuf = pBuf;
    MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
    MplsEventNotif.u4InIfIndex = u4PwIfIndex;
    MplsEventNotif.u1PayloadOffset = (UINT1) ((MPLS_DOUBLE_HEADER_LEN));
    MplsEventNotif.u1ServiceType = 0;
    MplsEventNotif.u2Event = MPLS_ECFM_PACKET;
    if (MplsPortEventNotification (MPLS_APPLICATION_ECFM, &MplsEventNotif) ==
        OSIX_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Error received in sending CFM packet\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}
#endif
