/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwelw.c,v 1.44 2014/12/24 10:58:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fssnmp.h"
# include  "l2vpincs.h"
# include  "mplsnp.h"
# include  "fsvlan.h"
# include  "l2iwf.h"
#include  "mplsnpwr.h"

/* LOW LEVEL Routines for Table : PwEnetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwEnetTable
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwEnetTable (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance)
{
    if ((u4PwIndex < L2VPN_MIN_PWVC_ENET_ENTRIES) ||
        (u4PwIndex > L2VPN_MAX_PWVC_ENET_ENTRIES) ||
        (u4PwEnetPwInstance < L2VPN_MIN_PWVC_ENET_ENTRIES) ||
        (u4PwEnetPwInstance > L2VPN_MAX_PWVC_ENET_ENTRIES))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwEnetTable
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwEnetTable (UINT4 *pu4PwIndex, UINT4 *pu4PwEnetPwInstance)
{
    UINT1               u1EnetFound = L2VPN_FALSE;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (nmhGetFirstIndexPwTable (pu4PwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    do
    {
        pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (*pu4PwIndex);

        if (pPwVcEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (u1EnetFound == L2VPN_FALSE)
                {
                    *pu4PwEnetPwInstance =
                        L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry);
                    u1EnetFound = L2VPN_TRUE;
                }
                else if (*pu4PwEnetPwInstance >
                         L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry))
                {
                    *pu4PwEnetPwInstance =
                        L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry);
                }
            }
        }
    }
    while ((u1EnetFound == L2VPN_FALSE) &&
           (nmhGetNextIndexPwTable (*pu4PwIndex, pu4PwIndex) == SNMP_SUCCESS));
    if (u1EnetFound == L2VPN_TRUE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwEnetTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
                PwEnetPwInstance
                nextPwEnetPwInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwEnetTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex,
                            UINT4 u4PwEnetPwInstance,
                            UINT4 *pu4NextPwEnetPwInstance)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT1               u1PwEnetFound = L2VPN_FALSE;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
         || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {

            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) >
                u4PwEnetPwInstance)
            {
                if (u1PwEnetFound == L2VPN_FALSE)
                {
                    *pu4NextPwEnetPwInstance =
                        L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry);
                    u1PwEnetFound = L2VPN_TRUE;
                    continue;
                }
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) <
                    *pu4NextPwEnetPwInstance)
                {
                    *pu4NextPwEnetPwInstance =
                        L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry);
                }
            }
        }
    }
    if (u1PwEnetFound == L2VPN_TRUE)
    {
        *pu4NextPwIndex = u4PwIndex;
        return SNMP_SUCCESS;
    }
    if (nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    do
    {
        pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);
        if (pPwVcEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                          ((tPwVcEnetServSpecEntry *)
                           L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (u1PwEnetFound == L2VPN_FALSE)
                {
                    *pu4NextPwEnetPwInstance =
                        L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry);
                    u1PwEnetFound = L2VPN_TRUE;
                    continue;
                }
                if (*pu4NextPwEnetPwInstance >
                    L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry))
                {
                    *pu4NextPwEnetPwInstance =
                        L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry);
                }
            }
        }
    }
    while ((u1PwEnetFound == L2VPN_FALSE) &&
           (nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex) == SNMP_SUCCESS));
    if (u1PwEnetFound == L2VPN_TRUE)
    {
        *pu4NextPwIndex = u4PwIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwEnetPwVlan
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetPwVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetPwVlan (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                    UINT4 *pu4RetValPwEnetPwVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pu4RetValPwEnetPwVlan =
                        L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetVlanMode
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetVlanMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetVlanMode (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                      INT4 *pi4RetValPwEnetVlanMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pi4RetValPwEnetVlanMode =
                        L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetPortVlan
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetPortVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetPortVlan (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                      UINT4 *pu4RetValPwEnetPortVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pu4RetValPwEnetPortVlan =
                        L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetPortIfIndex
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetPortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetPortIfIndex (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                         INT4 *pi4RetValPwEnetPortIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pi4RetValPwEnetPortIfIndex =
                        L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetVcIfIndex
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetVcIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetVcIfIndex (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                       INT4 *pi4RetValPwEnetVcIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pi4RetValPwEnetVcIfIndex =
                        L2VPN_PWVC_ENET_VC_IF_INDEX (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetRowStatus
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetRowStatus (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                       INT4 *pi4RetValPwEnetRowStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                          ((tPwVcEnetServSpecEntry *)
                           L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pi4RetValPwEnetRowStatus =
                        L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetStorageType
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                retValPwEnetStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetStorageType (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                         INT4 *pi4RetValPwEnetStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    *pi4RetValPwEnetStorageType =
                        L2VPN_PWVC_ENET_STORAGE_TYPE (pPwVcEnetEntry);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPwEnetPwVlan
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetPwVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetPwVlan (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                    UINT4 u4SetValPwEnetPwVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry)
                        = (UINT2) u4SetValPwEnetPwVlan;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetVlanMode
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetVlanMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetVlanMode (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                      INT4 i4SetValPwEnetVlanMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry)
                        = (INT1) i4SetValPwEnetVlanMode;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetPortVlan
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetPortVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetPortVlan (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                      UINT4 u4SetValPwEnetPortVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry)
                        = (UINT2) u4SetValPwEnetPortVlan;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetPortIfIndex
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetPortIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetPortIfIndex (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                         INT4 i4SetValPwEnetPortIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)
                        = i4SetValPwEnetPortIfIndex;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetVcIfIndex
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetVcIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetVcIfIndex (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                       INT4 i4SetValPwEnetVcIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    L2VPN_PWVC_ENET_VC_IF_INDEX (pPwVcEnetEntry) =
                        i4SetValPwEnetVcIfIndex;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetRowStatus
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetRowStatus (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                       INT4 i4SetValPwEnetRowStatus)
{
    if (L2VpnSetPwEnetRowStatus (u4PwIndex,
                                 u4PwEnetPwInstance,
                                 i4SetValPwEnetRowStatus) == L2VPN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetPwEnetStorageType
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetStorageType (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                         INT4 i4SetValPwEnetStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                          ((tPwVcEnetServSpecEntry *)
                           L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)), pPwVcEnetEntry,
                          tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    L2VPN_PWVC_ENET_STORAGE_TYPE (pPwVcEnetEntry)
                        = (INT1) i4SetValPwEnetStorageType;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PwEnetPwVlan
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetPwVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetPwVlan (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                       UINT4 u4PwEnetPwInstance, UINT4 u4TestValPwEnetPwVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValPwEnetPwVlan > L2VPN_VLANCFG)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Now scan the list to get the ENet entry using */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)), pPwVcEnetEntry,
                      tPwVcEnetEntry *)
        {
            /* when the node is got with the matching index chk for null */
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                break;
            }
        }
        if (pPwVcEnetEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetVlanMode
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetVlanMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetVlanMode (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         UINT4 u4PwEnetPwInstance, INT4 i4TestValPwEnetVlanMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwEnetVlanMode)
    {
        case L2VPN_VLANMODE_OTHER:
        case L2VPN_VLANMODE_PORTBASED:
        case L2VPN_VLANMODE_NOCHANGE:
        case L2VPN_VLANMODE_CHANGEVLAN:
        case L2VPN_VLANMODE_ADDVLAN:
        case L2VPN_VLANMODE_REMOVEVLAN:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Now scan the list to get the ENet entry using */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)), pPwVcEnetEntry,
                      tPwVcEnetEntry *)
        {
            /* when the node is got with the matching index chk for null */
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                break;
            }
        }
        if (pPwVcEnetEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetPortVlan
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetPortVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetPortVlan (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         UINT4 u4PwEnetPwInstance,
                         UINT4 u4TestValPwEnetPortVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    BOOL1               bElanFlag;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4BridgeMode = 0;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValPwEnetPortVlan > L2VPN_VLANCFG)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Now scan the list to get the ENet entry using */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)), pPwVcEnetEntry,
                      tPwVcEnetEntry *)
        {
            /* when the node is got with the matching index chk for null */
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                break;
            }
        }
        if (pPwVcEnetEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (u4TestValPwEnetPortVlan == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
        {
            return SNMP_SUCCESS;
        }
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
            (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
        if (pVplsEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
#ifdef MI_WANTED
        if (L2IwfGetBridgeMode ((UINT4)(L2VPN_VPLS_VSI (pVplsEntry)), &u4BridgeMode) !=
            L2IWF_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
#else
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) !=
            L2IWF_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
#endif
        if ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE))
        {
#ifdef MI_WANTED
            bElanFlag = L2IwfIsVlanElan ((UINT4) L2VPN_VPLS_VSI (pVplsEntry),
                                         (UINT2) u4TestValPwEnetPortVlan);
#else
            bElanFlag =
                L2IwfIsVlanElan (L2IWF_DEFAULT_CONTEXT,
                                 (UINT2) u4TestValPwEnetPortVlan);
#endif
            if (!(((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                   && (bElanFlag == OSIX_TRUE))
                  || ((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
                      && (bElanFlag == OSIX_FALSE))))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetPortIfIndex
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetPortIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetPortIfIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            UINT4 u4PwEnetPwInstance,
                            INT4 i4TestValPwEnetPortIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValPwEnetPortIfIndex < L2VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Now scan the list to get the ENet entry using */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)), pPwVcEnetEntry,
                      tPwVcEnetEntry *)
        {
            /* when the node is got with the matching index chk for null */
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                break;
            }
        }
        if (pPwVcEnetEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetVcIfIndex
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetVcIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetVcIfIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                          UINT4 u4PwEnetPwInstance,
                          INT4 i4TestValPwEnetVcIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValPwEnetVcIfIndex < L2VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Now scan the list to get the ENet entry using */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)), pPwVcEnetEntry,
                      tPwVcEnetEntry *)
        {
            /* when the node is got with the matching index chk for null */
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                break;
            }
        }
        if (pPwVcEnetEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetRowStatus
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetRowStatus (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                          UINT4 u4PwEnetPwInstance,
                          INT4 i4TestValPwEnetRowStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT1               u1Found = L2VPN_FALSE;
    UINT4               u4Instance = L2VPN_ZERO;
#ifdef VPLSADS_WANTED
    UINT4               u4AcIndex = L2VPN_ZERO;
#endif

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* MS-PW: Attached Index should be zero */
        if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != L2VPN_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /*MS-PW: check for equal to NULL check is not required as memory 
         * won't be created during PW VC
         * creation*/

        /*MS-PW: skip if AC is not attached */
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
        {
            /* Now scan the list to get the ENet entry using */
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                /* when the node is got with the matching index chk for null */
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4PwEnetPwInstance)
                {
                    u1Found = L2VPN_TRUE;
                    break;
                }
            }
        }                        /*MS-PW */
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    switch (i4TestValPwEnetRowStatus)
    {
        case L2VPN_PWVC_ACTIVE:
            if (u1Found == L2VPN_TRUE)
            {
                if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                    L2VPN_PWVC_ACTIVE)
                {
                    break;
                }
                if ((L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) !=
                     L2VPN_PWVC_NOTINSERVICE) &&
                    (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) !=
                     L2VPN_PWVC_NOTREADY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

#ifdef VPLSADS_WANTED
                L2VpnGetAcIndexFromIfandVlan ((UINT4)
                                              L2VPN_PWVC_ENET_PORT_IF_INDEX
                                              (pPwVcEnetEntry),
                                              L2VPN_PWVC_ENET_PORT_VLAN
                                              (pPwVcEnetEntry), &u4AcIndex);
                if (u4AcIndex != L2VPN_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : Ac is mapped to autodiscovered vpls\t\n",
                                __func__);
                    return SNMP_FAILURE;
                }
#endif
            }

        case L2VPN_PWVC_NOTINSERVICE:
        case L2VPN_PWVC_DESTROY:
            if (u1Found == L2VPN_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        case L2VPN_PWVC_CREATEANDWAIT:
            if (u1Found == L2VPN_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /*MS-PW: skip if AC is not attached */
            if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)    /*MS-PW */
            {

                u4Instance = TMO_SLL_Count (L2VPN_PWVC_ENET_ENTRY_LIST
                                            ((tPwVcEnetServSpecEntry *)
                                             L2VPN_PWVC_ENET_ENTRY
                                             (pPwVcEntry)));
                if ((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
                    && (u4Instance == L2VPN_ONE))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

            }                    /*MS-PW */

            break;
        case L2VPN_PWVC_CREATEANDGO:
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetStorageType
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                testValPwEnetStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetStorageType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            UINT4 u4PwEnetPwInstance,
                            INT4 i4TestValPwEnetStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValPwEnetStorageType)
    {
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Now scan the list to get the ENet entry using */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)), pPwVcEnetEntry,
                      tPwVcEnetEntry *)
        {
            /* when the node is got with the matching index chk for null */
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                break;
            }
        }
        if (pPwVcEnetEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PwEnetTable
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwEnetTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PwEnetMplsPriMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwEnetMplsPriMappingTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwEnetMplsPriMappingTable (UINT4 u4PwIndex)
{
    if (!((u4PwIndex >= L2VPN_PWVC_INDEX_MINVAL) &&
          (u4PwIndex <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwEnetMplsPriMappingTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwEnetMplsPriMappingTable (UINT4 *pu4PwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    if (nmhGetFirstIndexPwTable (pu4PwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (*pu4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if ((((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
              || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
             && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
             && ((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
             != NULL))
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwEnetMplsPriMappingTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwEnetMplsPriMappingTable (UINT4 u4PwIndex,
                                          UINT4 *pu4NextPwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (*pu4NextPwIndex);
    if (pPwVcEntry != NULL)
    {
        if ((((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
              || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
             && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
             && ((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
             != NULL))
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwEnetMplsPriMapping
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwEnetMplsPriMapping
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetMplsPriMapping (UINT4 u4PwIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPwEnetMplsPriMapping)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT2               u2PriMapping = L2VPN_ZERO;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
              || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
             && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
             && ((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
             != NULL))
        {
            u2PriMapping = L2VPN_ENET_MPLS_PRI_MAPPING
                ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY (pPwVcEntry));
            CPY_TO_SNMP (pRetValPwEnetMplsPriMapping, &u2PriMapping, L2VPN_TWO);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetMplsPriMappingRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwEnetMplsPriMappingRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetMplsPriMappingRowStatus (UINT4 u4PwIndex,
                                     INT4
                                     *pi4RetValPwEnetMplsPriMappingRowStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
            && (((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
                != NULL))
        {
            *pi4RetValPwEnetMplsPriMappingRowStatus =
                L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY (pPwVcEntry));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetMplsPriMappingStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwEnetMplsPriMappingStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetMplsPriMappingStorageType (UINT4 u4PwIndex,
                                       INT4
                                       *pi4RetValPwEnetMplsPriMappingStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            *pi4RetValPwEnetMplsPriMappingStorageType =
                L2VPN_ENET_MPLS_PRI_MAP_STORAGE
                ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY (pPwVcEntry));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPwEnetMplsPriMapping
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwEnetMplsPriMapping
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetMplsPriMapping (UINT4 u4PwIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValPwEnetMplsPriMapping)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT2               u2PriMapping = L2VPN_ZERO;

    CPY_FROM_SNMP (&u2PriMapping, pSetValPwEnetMplsPriMapping, L2VPN_TWO);

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
              || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
             && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
            && (((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
                != NULL))
        {
            L2VPN_ENET_MPLS_PRI_MAPPING ((tPwVcEnetServSpecEntry *)
                                         L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)) =
                u2PriMapping;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetMplsPriMappingRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwEnetMplsPriMappingRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetMplsPriMappingRowStatus (UINT4 u4PwIndex,
                                     INT4 i4SetValPwEnetMplsPriMappingRowStatus)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tPwVcEntry         *pPwVcEntry = NULL;

    tPwVcEnetMplsPriMappingEntry *pPwVcEnetMplsPriMappingEntry = NULL;

    UINT1               u1Found = L2VPN_FALSE;
    UINT4               u4RetStatus;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            if (((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
                != NULL)
            {
                u1Found = L2VPN_TRUE;
            }
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValPwEnetMplsPriMappingRowStatus)
    {
        case L2VPN_PWVC_ACTIVE:
            if (u1Found == L2VPN_TRUE)
            {
                if ((L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) == L2VPN_PWVC_NOTINSERVICE) ||
                    (L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) == L2VPN_PWVC_NOTREADY))
                {
                    L2VPN_ADMIN_EVT_PWVC_SSTYPE = L2VPN_PWVC_SERV_ETH;
                    L2VPN_ADMIN_EVT_SSEVT_TYPE
                        = L2VPN_PWVC_ENET_MPLS_PRI_MAP_ACTIVE;
                    L2VPN_ADMIN_EVT_SSENET_VCINDEX = u4PwIndex;
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_SERV_ADMIN_EVENT);
                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }

                    L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                        ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                         (pPwVcEntry)) = L2VPN_PWVC_ACTIVE;
                    return SNMP_SUCCESS;
                }
            }
            break;

        case L2VPN_PWVC_CREATEANDWAIT:
            if (u1Found == L2VPN_FALSE)
            {
                pPwVcEnetMplsPriMappingEntry =
                    (tPwVcEnetMplsPriMappingEntry *)
                    MemAllocMemBlk (L2VPN_MPLS_PRIMAP_POOL_ID);

                if (pPwVcEnetMplsPriMappingEntry == NULL)
                {
                    return SNMP_FAILURE;
                }

                MEMSET (pPwVcEnetMplsPriMappingEntry, L2VPN_ZERO,
                        sizeof (tPwVcEnetMplsPriMappingEntry));
                ((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry =
pPwVcEnetMplsPriMappingEntry;
                L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS ((tPwVcEnetServSpecEntry *)
                                                   L2VPN_PWVC_ENET_ENTRY
                                                   (pPwVcEntry)) =
                    L2VPN_PWVC_NOTREADY;
                return SNMP_SUCCESS;
            }
            break;

        case L2VPN_PWVC_NOTINSERVICE:
            if (u1Found == L2VPN_TRUE)
            {
                if (L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                    ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                     (pPwVcEntry)) == L2VPN_PWVC_ACTIVE)
                {
                    L2VPN_ADMIN_EVT_PWVC_SSTYPE = L2VPN_PWVC_SERV_ETH;
                    L2VPN_ADMIN_EVT_SSEVT_TYPE
                        = L2VPN_PWVC_ENET_MPLS_PRI_MAP_NIS;
                    L2VPN_ADMIN_EVT_SSENET_VCINDEX = u4PwIndex;
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_SERV_ADMIN_EVENT);
                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }

                    L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                        ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                         (pPwVcEntry)) = L2VPN_PWVC_NOTINSERVICE;
                    return SNMP_SUCCESS;
                }

                if ((L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) == L2VPN_PWVC_NOTINSERVICE) ||
                    (L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) == L2VPN_PWVC_NOTREADY))
                {
                    L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                        ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                         (pPwVcEntry)) = L2VPN_PWVC_NOTINSERVICE;
                    return SNMP_SUCCESS;
                }
            }
            break;

        case L2VPN_PWVC_DESTROY:
            if (u1Found == L2VPN_TRUE)
            {
                if ((L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) == L2VPN_PWVC_NOTINSERVICE) ||
                    (L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) == L2VPN_PWVC_NOTREADY))
                {
                    if (MemReleaseMemBlock
                        (L2VPN_MPLS_PRIMAP_POOL_ID,
                         (UINT1 *) ((tPwVcEnetServSpecEntry *)
                                    L2VPN_PWVC_ENET_ENTRY
                                    (pPwVcEntry))->pMplsPriMappingEntry) !=
                        MEM_FAILURE)
                    {
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        /* - Debug msg for mem rel failure */
                    }
                }
                if (L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                    ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                     (pPwVcEntry)) == L2VPN_PWVC_ACTIVE)
                {
                    L2VPN_ADMIN_EVT_PWVC_SSTYPE = L2VPN_PWVC_SERV_ETH;
                    L2VPN_ADMIN_EVT_SSEVT_TYPE
                        = L2VPN_PWVC_ENET_MPLS_PRI_MAP_DESTROY;
                    L2VPN_ADMIN_EVT_SSENET_VCINDEX = u4PwIndex;
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_SERV_ADMIN_EVENT);
                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }

                    if (MemReleaseMemBlock
                        (L2VPN_MPLS_PRIMAP_POOL_ID,
                         (UINT1 *) ((tPwVcEnetServSpecEntry *)
                                    L2VPN_PWVC_ENET_ENTRY
                                    (pPwVcEntry))->pMplsPriMappingEntry) !=
                        MEM_FAILURE)
                    {
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        /* - Debug msg for mem rel failure */
                    }

                }
            }
            break;

        default:
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwEnetMplsPriMappingStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwEnetMplsPriMappingStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwEnetMplsPriMappingStorageType (UINT4 u4PwIndex,
                                       INT4
                                       i4SetValPwEnetMplsPriMappingStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            L2VPN_ENET_MPLS_PRI_MAP_STORAGE ((tPwVcEnetServSpecEntry *)
                                             L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))
                = (INT1) i4SetValPwEnetMplsPriMappingStorageType;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PwEnetMplsPriMapping
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwEnetMplsPriMapping
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetMplsPriMapping (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValPwEnetMplsPriMapping)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT2               u2Temp;

    MEMCPY (&u2Temp, pTestValPwEnetMplsPriMapping->pu1_OctetList,
            pTestValPwEnetMplsPriMapping->i4_Length);

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2Temp & PRI_MAPPING_MASK) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetMplsPriMappingRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwEnetMplsPriMappingRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetMplsPriMappingRowStatus (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                        INT4
                                        i4TestValPwEnetMplsPriMappingRowStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1Found = L2VPN_FALSE;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            if (((tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))->pMplsPriMappingEntry
                != NULL)
            {
                u1Found = L2VPN_TRUE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwEnetMplsPriMappingRowStatus)
    {
        case L2VPN_PWVC_ACTIVE:
            if (u1Found == L2VPN_TRUE)
            {
                if ((L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) != L2VPN_PWVC_NOTINSERVICE) &&
                    (L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS
                     ((tPwVcEnetServSpecEntry *) L2VPN_PWVC_ENET_ENTRY
                      (pPwVcEntry)) != L2VPN_PWVC_NOTREADY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        case L2VPN_PWVC_NOTINSERVICE:
        case L2VPN_PWVC_DESTROY:
        case L2VPN_PWVC_CREATEANDWAIT:
        case L2VPN_PWVC_CREATEANDGO:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwEnetMplsPriMappingStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwEnetMplsPriMappingStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwEnetMplsPriMappingStorageType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                          INT4
                                          i4TestValPwEnetMplsPriMappingStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValPwEnetMplsPriMappingStorageType)
    {
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH)
            && (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PwEnetMplsPriMappingTable
 Input       :  The Indices
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwEnetMplsPriMappingTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PwEnetStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwEnetStatsTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwEnetStatsTable (UINT4 u4PwIndex)
{
    if (!((u4PwIndex >= L2VPN_PWVC_INDEX_MINVAL) &&
          (u4PwIndex <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwEnetStatsTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwEnetStatsTable (UINT4 *pu4PwIndex)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwEnetStatsTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwEnetStatsTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwEnetStatsIllegalVlan
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwEnetStatsIllegalVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetStatsIllegalVlan (UINT4 u4PwIndex,
                              UINT4 *pu4RetValPwEnetStatsIllegalVlan)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* This object is applicable only for pwType = Ethernet Tagged */
        if (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN)
        {
#ifdef NPAPI_WANTED
            {
                tMplsInputParams    MplsStatsInput;
                tStatsInfo          StatsInfo;

                MplsStatsInput.i1NpReset = TRUE;
                MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_VC_ILLEGAL_VLAN,
                                          &StatsInfo);
                *pu4RetValPwEnetStatsIllegalVlan = StatsInfo.u8Value.u4Lo;
                L2VPN_PWVC_PERF_TOTAL_ERROR_PKTS (gpPwVcGlobalInfo) +=
                    StatsInfo.u8Value.u4Lo;
            }
#else
            *pu4RetValPwEnetStatsIllegalVlan = 0;
#endif
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwEnetStatsIllegalLength
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwEnetStatsIllegalLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwEnetStatsIllegalLength (UINT4 u4PwIndex,
                                UINT4 *pu4RetValPwEnetStatsIllegalLength)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
            || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        {
#ifdef NPAPI_WANTED
            {
                tMplsInputParams    MplsStatsInput;
                tStatsInfo          StatsInfo;

                MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
                StatsInfo.u8Value.u4Hi = 0;
                StatsInfo.u8Value.u4Lo = 0;

                MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
                StatsInfo.u8Value.u4Hi = 0;
                StatsInfo.u8Value.u4Lo = 0;

                MplsStatsInput.i1NpReset = TRUE;
                MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_VC_ILLEGAL_VLAN,
                                          &StatsInfo);
                *pu4RetValPwEnetStatsIllegalLength = StatsInfo.u8Value.u4Lo;
                L2VPN_PWVC_PERF_TOTAL_ERROR_PKTS (gpPwVcGlobalInfo) +=
                    StatsInfo.u8Value.u4Lo;
            }
#else
            *pu4RetValPwEnetStatsIllegalLength = 0;
#endif
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
