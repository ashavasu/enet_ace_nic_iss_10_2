
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vprm.c,v 1.4 2014/11/28 02:13:12 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/


#include "l2vpincs.h"

VOID L2VpnRmHandleRmMessageEvent (tL2VpnRmMsg *pRmL2VpnMsg);
VOID L2VpnRmHandleGoActiveEvent (VOID);
VOID L2VpnRmHandleGoStandbyEvent (VOID);
VOID L2VpnRmHandleStandbyUpEvent (tRmNodeInfo * pRmNode);
VOID L2VpnRmHandleStandbyDownEvent (tRmNodeInfo * pRmNode);
VOID L2VpnRmSendBulkReq (VOID);
VOID L2VpnRmHandleConfRestoreCompEvt(VOID); 
VOID L2VpnRmInitBulkUpdateFlags (VOID);
VOID L2VpnRmProcessBulkReq (VOID); 
VOID L2VpnRmProcessBulkTail (VOID);
VOID L2VpnRmSendBulkTail (VOID);
/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnProcessRmEvent                                        */
/*                                                                           */
/* Description  : This function receives the RM events and RM message.       */
/*                Based on the events it will call the corresponding         */
/*                functions to process the events.                           */
/*                                                                           */
/* Input        : pRmL2VpnMsg - RM message.                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID L2VpnProcessRmEvent(tL2VpnRmMsg  *pRmL2VpnMsg)
{

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,"ENTRY:L2VpnProcessRmEvent\n");

    switch (pRmL2VpnMsg->u1Event)
    {

        case RM_MESSAGE:

            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received RM_MESSAGE event.\n");     
            L2VpnRmHandleRmMessageEvent(pRmL2VpnMsg);
            break;

        case GO_ACTIVE:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received GO_ACTIVE event.\n");
            L2VpnRmHandleGoActiveEvent ();
            break;

        case GO_STANDBY:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received GO_STANDBY event.\n");
            L2VpnRmHandleGoStandbyEvent();
            break;

        case RM_PEER_UP:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received RM_PEER_UP event.\n");
            L2VpnRmHandleStandbyUpEvent ((tRmNodeInfo *)
                    (pRmL2VpnMsg->pBuff));
            break;
        case RM_PEER_DOWN:

            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"Received RM_PEER_DOWN event.\n");
            L2VpnRmHandleStandbyDownEvent ((tRmNodeInfo *)
                    (pRmL2VpnMsg->pBuff));

            break;

        case L2_INITIATE_BULK_UPDATES:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"Received L2_INITIATE_BULK_UPDATES event.\n");
            L2VpnRmSendBulkReq ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received RM_CONFIG_RESTORE_COMPLETE event.\n");
            L2VpnRmHandleConfRestoreCompEvt();
            break;

        default:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received Invalid event\n");
            break;


    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmHandleRmMessageEvent                                */
/*                                                                           */
/* Description  : This function process the messages set by the peer.        */
/*                This function is invoked whenever L2VPN module receive     */
/*                dynamic sync up/bulk update/bulk request message.          */
/*                This function parses the messages received and updates     */
/*                its database. Following are the messages and related       */
/*                message process functions                                  */
/*                                                                           */
/*                                                                           */
/* Input        : pRmL2VpnMsg - RM Message                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/


VOID L2VpnRmHandleRmMessageEvent (tL2VpnRmMsg *pRmL2VpnMsg)
{
    UINT4               u4SeqNum = 0;
    UINT4               u4L2VpnNodeState = gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;


    /* Get the sequence number for RM Message*/
    RM_PKT_GET_SEQNUM (pRmL2VpnMsg->pBuff, &u4SeqNum);
    RM_STRIP_OFF_RM_HDR (pRmL2VpnMsg->pBuff, pRmL2VpnMsg->u2Length);

    /* Get the message Type and Message length */
    L2VPN_RM_GET_1_BYTE (pRmL2VpnMsg->pBuff, u4Offset, u1MsgType);
    L2VPN_RM_GET_2_BYTE (pRmL2VpnMsg->pBuff, u4Offset, u2MsgLen);

    /* check the message length after removing the RM header*/
    if (pRmL2VpnMsg->u2Length != u2MsgLen)
    {

        RM_FREE (pRmL2VpnMsg->pBuff);
        L2VpnRmApiSendProtoAckToRM(u4SeqNum);
        return;
    }
    if ((u4L2VpnNodeState != L2VPN_RM_ACTIVE_STANDBY_UP) &&
            (u1MsgType == L2VPN_RM_BULK_UPDT_REQ_MSG))
    {
        /* This may happen when the RM channel is disturbed */
        RM_FREE (pRmL2VpnMsg->pBuff);
        L2VpnRmApiSendProtoAckToRM(u4SeqNum);
        return;
    }

    switch (u1MsgType)
    {

        case L2VPN_RM_BULK_UPDT_REQ_MSG:

            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Received L2VPN_RM_BULK_UPDT_REQ_MSG message\n");               
            if (L2VPN_IS_STANDBY_UP () == L2VPN_RM_FALSE)
            {

                gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd = L2VPN_RM_TRUE;
                break;
            }
            /* process bulk request message */
            L2VpnRmInitBulkUpdateFlags();
            gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd = L2VPN_RM_FALSE;
            gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_INPROGRESS ;
            L2VpnRmProcessBulkReq();
            break;

        case L2VPN_RM_BULK_UPDATE_MSG:

            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Received L2VPN_RM_BULK_UPDATE_MSG message\n");
            /* sync bulk update received from Active to standby */
            L2VpnRmProcessBulkUpdateMsg (pRmL2VpnMsg->pBuff, &u4Offset);
            break;

        case L2VPN_RM_BULK_UPDT_TAIL_MSG:

            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Received L2VPN_RM_BULK_UPDT_TAIL_MSG message\n");
            /* send Bulk update completed to RM */
            L2VpnRmProcessBulkTail ();
            break;

        case L2VPN_RM_SYNC_HWLIST_MSG:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Received L2VPN_RM_SYNC_HWLIST_MSG message\n");
            /* Dynamic sync at standby */
            L2VpnRmProcessSyncHwListEntry (pRmL2VpnMsg->pBuff, &u4Offset);
            break;
		case L2VPN_RM_SYNC_VPLS_HWLIST_MSG:
			L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Received L2VPN_RM_SYNC_VPLS_HWLIST_MSG message\n");
			L2VpnRmProcessSyncVplsHwListEntry (pRmL2VpnMsg->pBuff, &u4Offset);
			break;
        default:
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Received Invalid message\n");
            break;
    }

    RM_FREE (pRmL2VpnMsg->pBuff);
    L2VpnRmApiSendProtoAckToRM (u4SeqNum);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmInitBulkUpdateFlags                                 */
/*                                                                           */
/* Description  : This function initialize the flags used to bulk update.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/



VOID L2VpnRmInitBulkUpdateFlags()
{
    gL2VpnGlobalInfo.l2vpRmInfo.u1BulkUpdModuleStatus  = L2VPN_RM_GBL_MOD ;
    gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus  =L2VPN_RM_BLKUPDT_NOT_STARTED;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmProcessBulkReq                                      */
/*                                                                           */
/* Description  : This function initialize the flags used to bulk update.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/



VOID L2VpnRmProcessBulkReq ()
{

    if(L2VPN_IS_NODE_ACTIVE() != L2VPN_RM_TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Node is not in active State\n");
        return;
    }

    if (L2VPN_IS_STANDBY_UP() == L2VPN_RM_FALSE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "No standby Node is existing\n");
        return;
    }
    /* construct dynamic bulk update at active and send to standby*/ 
    L2VpnRmSendBulkPwVcInfo();
    gL2VpnGlobalInfo.l2vpRmInfo.u1BulkUpdModuleStatus = L2VPN_RM_MOD_COMPLETED;
    gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus  = L2VPN_RM_BLKUPDT_COMPLETED;
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Send Bulk tail info to RM\n");
    L2VpnRmSendBulkTail();

}
/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmSendBulkTail                                        */
/*                                                                           */
/* Description  : This function is called when active node has completed     */
/*                sending all the bulk updates or it has no bulk updates     */
/*                to send to the standby. This function constucts and        */
/*                sends the bulk tail message.                               */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID L2VpnRmSendBulkTail ()
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = L2VPN_FAILURE;

    L2VpnRmSetBulkUpdateStatus();
    pRmMsg = L2VpnRmAllocForRmMsg (L2VPN_RM_BULK_TAIL_MSG_LEN);
    if (pRmMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n"); 
        return;
    }

    /* Fill message type and message length */
    L2VPN_RM_PUT_1_BYTE (pRmMsg, u4Offset, (UINT1) L2VPN_RM_BULK_UPDT_TAIL_MSG);
    L2VPN_RM_PUT_2_BYTE (pRmMsg, u4Offset, (UINT2) L2VPN_RM_BULK_TAIL_MSG_LEN);

    i4RetVal = L2VpnRmSendMsgToRm (pRmMsg, (UINT2) L2VPN_RM_BULK_TAIL_MSG_LEN);

    if (i4RetVal != L2VPN_SUCCESS)
    {

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Sending bulk tail to RM is failed\n");
    }

    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Sending bulk tail to RM is Success\n");
    }


}


/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmProcessBulkTail                                     */
/*                                                                           */
/* Description  : This function sends the bulk update complete messages      */
/*                to RM. Sends the RM_PROTOCOL_BULK_UPDT_COMPLETION event    */
/*                to the RM.                                                 */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*                                                                           */
/*****************************************************************************/




VOID L2VpnRmProcessBulkTail()
{

    tRmProtoEvt         ProtoEvt;
    ProtoEvt.u4AppId = RM_L2VPN_APP_ID;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
    ProtoEvt.u4Error = RM_NONE;

    (VOID)L2VpnRmSendEventToRm (&ProtoEvt); 
    gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus =L2VPN_RM_BLKUPDT_COMPLETED;

} 

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmHandleGoActiveEvent                                   */
/*                                                                           */
/* Description  : This function handles the GO_ACTIVE event from RM.         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID L2VpnRmHandleGoActiveEvent ()
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4NodeState = gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState; 

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    /* if the node count is greater than one standby is up*/
    gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = L2VpnRmGetStandbyNodeCount ();
    if (gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount > 0)
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
    }
    else
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState  = L2VPN_RM_ACTIVE_STANDBY_DOWN;
    }

    /*check the node state to process the event */ 
    if (u4NodeState == L2VPN_RM_INIT)
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4L2vpnRmGoActiveReason = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;

    }
    else if (u4NodeState == L2VPN_RM_STANDBY)
    {
        L2VpnRmInitBulkUpdateFlags ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
        gL2VpnGlobalInfo.l2vpRmInfo.u4L2vpnRmGoActiveReason = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    } 

    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,"Received GO_ACTIVE event while the node is neither in INIT nor STANDBY state\n");
        return;
    }

    /* send event to RM */
    ProtoEvt.u4AppId = RM_L2VPN_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    (VOID)L2VpnRmSendEventToRm (&ProtoEvt);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "Send event %u to RM Success\n",ProtoEvt.u4Event);


}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmHandleGoStandbyEvent                                */
/*                                                                           */
/* Description  : This function handles the GO_STANDBY event from RM         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/



VOID L2VpnRmHandleGoStandbyEvent ()
{

    tRmProtoEvt         ProtoEvt;
    UINT4               u4L2VpnNodeState = gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if ((u4L2VpnNodeState == L2VPN_RM_STANDBY) || (u4L2VpnNodeState == L2VPN_RM_INIT))
    {

        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,"Received GO_STANDBY event when the node is :%s state\n",u4L2VpnNodeState);
        return;
    }

    /* check node state as standby */ 
    else if ((u4L2VpnNodeState == L2VPN_RM_ACTIVE_STANDBY_UP) ||
            (u4L2VpnNodeState == L2VPN_RM_ACTIVE_STANDBY_DOWN))
    {
        L2VpnRmInitBulkUpdateFlags();
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
		L2VpnSetPwAdminStatusAtGoStandby(L2VPN_PWVC_ADMIN_DOWN);
		L2VpnSetPwAdminStatusAtGoStandby(L2VPN_PWVC_ADMIN_UP);
        gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = 0;
        ProtoEvt.u4AppId = RM_L2VPN_APP_ID;
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        ProtoEvt.u4Error = RM_NONE;
        (VOID)L2VpnRmSendEventToRm (&ProtoEvt);
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,"Sending event RM_STANDBY_EVT_PROCESSED to RM Success \n");
    }
}



/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmHandleStandbyUpEvent                                */
/*                                                                           */
/* Description  : This function handles the Standby up event.                */
/*                                                                           */
/* Input        : pRmNode - info contains number of Stanby nodes             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/


VOID L2VpnRmHandleStandbyUpEvent (tRmNodeInfo * pRmNode)
{

    UINT4               u4NodeStatus = gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState;

    if (u4NodeStatus == L2VPN_RM_STANDBY) 
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Got the Standby Up event at standby.\n");                
        L2VpnRmRelRmMsgMem ((UINT1 *) pRmNode);
        return;
    }
    /* If node count is 1 then set standby up*/
    gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = pRmNode->u1NumStandby;
    if (pRmNode->u1NumStandby > 0) 
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState  = L2VPN_RM_ACTIVE_STANDBY_UP;
    }
    else
    {
       gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN;
    }
    L2VpnRmRelRmMsgMem ((UINT1 *) pRmNode);

    /* check the flag for bulk request received */
    
    if (gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd == L2VPN_RM_TRUE)
    {
        gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd = L2VPN_RM_FALSE;
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Update the flags \n");
        L2VpnRmInitBulkUpdateFlags ();
        gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus =L2VPN_RM_BLKUPDT_INPROGRESS;    
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "send Bulk update message\n");
        L2VpnRmProcessBulkReq ();
    }

    return;
}


/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmHandleStandByDownEvent                                */
/*                                                                           */
/* Description  : This function handles the Standby down event               */
/*                                                                           */
/* Input        : pRmNode - info contains number of Stanby nodes             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID L2VpnRmHandleStandbyDownEvent (tRmNodeInfo * pRmNode)
{
    gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = pRmNode->u1NumStandby;

    if(gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState == L2VPN_RM_STANDBY)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Got the Standby Down event at standby\n");
        L2VpnRmRelRmMsgMem ((UINT1 *) pRmNode);
        return;
    }
    if(pRmNode->u1NumStandby > 0)
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
    }
    else
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN ;
    }

    L2VpnRmRelRmMsgMem ((UINT1 *) pRmNode);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmSendBulkReq                                           */
/*                                                                           */
/* Description  : This function send the bulk update request.                */
/*                                                                           */
/* Input        : None                                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/


VOID L2VpnRmSendBulkReq ()
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = L2VPN_FAILURE;
    

    pRmMsg = L2VpnRmAllocForRmMsg (L2VPN_RM_BULK_UPD_REQ_MSG_LEN);
    if (pRmMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation failed\n");

        return;
    }

    /* Fill the message type and message length */
    L2VPN_RM_PUT_1_BYTE (pRmMsg, u4Offset, (UINT1) L2VPN_RM_BULK_UPDT_REQ_MSG);
    L2VPN_RM_PUT_2_BYTE (pRmMsg, u4Offset, (UINT2) L2VPN_RM_BULK_UPD_REQ_MSG_LEN);
    i4RetVal = L2VpnRmSendMsgToRm (pRmMsg, (UINT2) L2VPN_RM_BULK_UPD_REQ_MSG_LEN);
    if (i4RetVal != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "sending bulk request to RM is failed\n");

    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Sending bulk request to RM is Success\n");
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmHandleConfRestoreCompEvt                            */
/*                                                                           */
/* Description  : This function handle the event  RM_CONFIG_RESTORE_COMPLETE */
/*                received from the RM.Move the node to STANDBY state from   */
/*                INIT.                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID L2VpnRmHandleConfRestoreCompEvt( )
{
    
    if ((gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState == L2VPN_RM_INIT) && (L2VpnRmGetRmNodeState() == RM_STANDBY))
    {
        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmSendBulkAbort                                       */
/*                                                                           */
/* Description  : This API send the bulk update abort event to RM with the   */
/*                the given error code                                       */
/*                                                                           */
/* Input        : u4ErrCode - Error code.                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/


VOID L2VpnRmSendBulkAbort(UINT4 u4ErrCode)
{
    tRmProtoEvt         ProtoEvt;
    gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_ABORTED;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_L2VPN_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4Error = u4ErrCode;
    (VOID)L2VpnRmSendEventToRm (&ProtoEvt);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "Sending event RM_BULK_UPDT_ABORT with error code:%d success\n",u4ErrCode);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmInit                                              */
/*                                                                           */
/* Description  : This function initialize the L2VPN RM related objects.   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID L2VpnRmInit ()
{

    MEMSET (&gL2VpnGlobalInfo.l2vpRmInfo, 0, sizeof (tL2VpnRMInfo));

    /* Initialize the RM variables */
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT;
    gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = 0;
    gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd  = L2VPN_RM_FALSE;
    gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_NOT_STARTED;
    gL2VpnGlobalInfo.l2vpRmInfo.u1AdminState = L2VPN_DISABLED;
}
