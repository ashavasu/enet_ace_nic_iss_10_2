/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mspwlwg.c,v 1.2 2012/07/28 09:17:27 siva Exp $
 *
 * Description: This file contains the routines for the MSPW the module  
 *********************************************************************/


#include "l2vpincs.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsPwMaxEntries
 Input       :  The Indices

                The Object 
                retValFsMsPwMaxEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsPwMaxEntries (UINT4 *pu4RetValFsMsPwMaxEntries)
{
    L2vpnGetFsMsPwMaxEntries (pu4RetValFsMsPwMaxEntries);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsPwMaxEntries
 Input       :  The Indices

                The Object 
                setValFsMsPwMaxEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsPwMaxEntries (UINT4 u4SetValFsMsPwMaxEntries)
{
    L2vpnSetFsMsPwMaxEntries (u4SetValFsMsPwMaxEntries);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsPwMaxEntries
 Input       :  The Indices

                The Object 
                testValFsMsPwMaxEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsPwMaxEntries (UINT4 *pu4ErrorCode, UINT4 u4TestValFsMsPwMaxEntries)
{
    if (L2vpnTestFsMsPwMaxEntries (pu4ErrorCode, u4TestValFsMsPwMaxEntries) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMsPwMaxEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsPwMaxEntries (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMsPwConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMsPwConfigTable
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMsPwConfigTable (UINT4 u4FsMsPwIndex1,
                                           UINT4 u4FsMsPwIndex2)
{
    if(!((u4FsMsPwIndex1>= L2VPN_PWVC_INDEX_MINVAL) &&
		(u4FsMsPwIndex1 <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
	return SNMP_FAILURE;
    }

    if(!((u4FsMsPwIndex2>= L2VPN_PWVC_INDEX_MINVAL) &&
		(u4FsMsPwIndex2 <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
	return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMsPwConfigTable
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMsPwConfigTable (UINT4 *pu4FsMsPwIndex1,
                                   UINT4 *pu4FsMsPwIndex2)
{
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;

    pL2vpnFsMsPwConfigTable = L2vpnGetFirstFsMsPwConfigTable ();

    if (pL2vpnFsMsPwConfigTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMsPwIndex1 = pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1;

    *pu4FsMsPwIndex2 = pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMsPwConfigTable
 Input       :  The Indices
                FsMsPwIndex1
                nextFsMsPwIndex1
                FsMsPwIndex2
                nextFsMsPwIndex2
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMsPwConfigTable (UINT4 u4FsMsPwIndex1,
                                  UINT4 *pu4NextFsMsPwIndex1,
                                  UINT4 u4FsMsPwIndex2,
                                  UINT4 *pu4NextFsMsPwIndex2)
{
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;
    tL2vpnFsMsPwConfigTable *pNextL2vpnFsMsPwConfigTable = NULL;

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));

    /* Assign the index */
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = u4FsMsPwIndex1;

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = u4FsMsPwIndex2;

    pNextL2vpnFsMsPwConfigTable =
        L2vpnGetNextFsMsPwConfigTable (&L2vpnFsMsPwConfigTable);

    if (pNextL2vpnFsMsPwConfigTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMsPwIndex1 = pNextL2vpnFsMsPwConfigTable->u4FsMsPwIndex1;

    *pu4NextFsMsPwIndex2 = pNextL2vpnFsMsPwConfigTable->u4FsMsPwIndex2;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMsPwOperStatus
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2

                The Object 
                retValFsMsPwOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsPwOperStatus (UINT4 u4FsMsPwIndex1, UINT4 u4FsMsPwIndex2,
                        INT4 *pi4RetValFsMsPwOperStatus)
{
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));

    /* Assign the index */
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = u4FsMsPwIndex1;

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = u4FsMsPwIndex2;

    if (L2vpnGetAllFsMsPwConfigTable (&L2vpnFsMsPwConfigTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsPwOperStatus = L2vpnFsMsPwConfigTable.i4FsMsPwOperStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMsPwRowStatus
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2

                The Object 
                retValFsMsPwRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMsPwRowStatus (UINT4 u4FsMsPwIndex1, UINT4 u4FsMsPwIndex2,
                       INT4 *pi4RetValFsMsPwRowStatus)
{
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));

    /* Assign the index */
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = u4FsMsPwIndex1;

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = u4FsMsPwIndex2;

    if (L2vpnGetAllFsMsPwConfigTable (&L2vpnFsMsPwConfigTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMsPwRowStatus = L2vpnFsMsPwConfigTable.i4FsMsPwRowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMsPwRowStatus
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2

                The Object 
                setValFsMsPwRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMsPwRowStatus (UINT4 u4FsMsPwIndex1, UINT4 u4FsMsPwIndex2,
                       INT4 i4SetValFsMsPwRowStatus)
{
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;
    tL2vpnIsSetFsMsPwConfigTable L2vpnIsSetFsMsPwConfigTable;

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));
    MEMSET (&L2vpnIsSetFsMsPwConfigTable, 0,
            sizeof (tL2vpnIsSetFsMsPwConfigTable));

    /* Assign the index */
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = u4FsMsPwIndex1;
    L2vpnIsSetFsMsPwConfigTable.bFsMsPwIndex1 = OSIX_TRUE;

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = u4FsMsPwIndex2;
    L2vpnIsSetFsMsPwConfigTable.bFsMsPwIndex2 = OSIX_TRUE;

    /* Assign the value */
    L2vpnFsMsPwConfigTable.i4FsMsPwRowStatus = i4SetValFsMsPwRowStatus;
    L2vpnIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;

    if (L2vpnSetAllFsMsPwConfigTable
        (&L2vpnFsMsPwConfigTable, &L2vpnIsSetFsMsPwConfigTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMsPwRowStatus
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2

                The Object 
                testValFsMsPwRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMsPwRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMsPwIndex1,
                          UINT4 u4FsMsPwIndex2, INT4 i4TestValFsMsPwRowStatus)
{
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;
    tL2vpnIsSetFsMsPwConfigTable L2vpnIsSetFsMsPwConfigTable;

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));
    MEMSET (&L2vpnIsSetFsMsPwConfigTable, 0,
            sizeof (tL2vpnIsSetFsMsPwConfigTable));

    /* Assign the index */
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = u4FsMsPwIndex1;
    L2vpnIsSetFsMsPwConfigTable.bFsMsPwIndex1 = OSIX_TRUE;

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = u4FsMsPwIndex2;
    L2vpnIsSetFsMsPwConfigTable.bFsMsPwIndex2 = OSIX_TRUE;

    /* Assign the value */
    L2vpnFsMsPwConfigTable.i4FsMsPwRowStatus = i4TestValFsMsPwRowStatus;
    L2vpnIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;

    if (L2vpnTestAllFsMsPwConfigTable (pu4ErrorCode, &L2vpnFsMsPwConfigTable,
                                       &L2vpnIsSetFsMsPwConfigTable, OSIX_FALSE,
                                       OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMsPwConfigTable
 Input       :  The Indices
                FsMsPwIndex1
                FsMsPwIndex2
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMsPwConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
