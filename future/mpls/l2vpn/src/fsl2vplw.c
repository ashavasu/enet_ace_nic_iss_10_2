/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsl2vplw.c,v 1.21 2016/04/05 13:22:19 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "l2vpincs.h"
# include  "mplscli.h"
# include  "mplsprot.h"
extern tL2vpnPwRedTest gL2vpnPwRedTest;
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsConfigIndexNext
 Input       :  The Indices

                The Object 
                retValVplsConfigIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigIndexNext (UINT4 *pu4RetValVplsConfigIndexNext)
{
    UNUSED_PARAM (pu4RetValVplsConfigIndexNext);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VplsConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVplsConfigTable (UINT4 u4VplsConfigIndex)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVplsConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVplsConfigTable (UINT4 *pu4VplsConfigIndex)
{
    UNUSED_PARAM (pu4VplsConfigIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexVplsConfigTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVplsConfigTable (UINT4 u4VplsConfigIndex,
                                UINT4 *pu4NextVplsConfigIndex)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4NextVplsConfigIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsConfigName
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigName (UINT4 u4VplsConfigIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValVplsConfigName)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pRetValVplsConfigName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigDescr
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigDescr (UINT4 u4VplsConfigIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValVplsConfigDescr)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pRetValVplsConfigDescr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigAdminStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigAdminStatus (UINT4 u4VplsConfigIndex,
                             INT4 *pi4RetValVplsConfigAdminStatus)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigAdminStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigMacLearning
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigMacLearning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigMacLearning (UINT4 u4VplsConfigIndex,
                             INT4 *pi4RetValVplsConfigMacLearning)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigMacLearning);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigDiscardUnknownDest
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigDiscardUnknownDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigDiscardUnknownDest (UINT4 u4VplsConfigIndex,
                                    INT4 *pi4RetValVplsConfigDiscardUnknownDest)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigDiscardUnknownDest);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigMacAging
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigMacAging
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigMacAging (UINT4 u4VplsConfigIndex,
                          INT4 *pi4RetValVplsConfigMacAging)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigMacAging);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigFwdFullHighWatermark
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigFwdFullHighWatermark
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigFwdFullHighWatermark (UINT4 u4VplsConfigIndex,
                                      UINT4
                                      *pu4RetValVplsConfigFwdFullHighWatermark)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4RetValVplsConfigFwdFullHighWatermark);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigFwdFullLowWatermark
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigFwdFullLowWatermark
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigFwdFullLowWatermark (UINT4 u4VplsConfigIndex,
                                     UINT4
                                     *pu4RetValVplsConfigFwdFullLowWatermark)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4RetValVplsConfigFwdFullLowWatermark);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigRowStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigRowStatus (UINT4 u4VplsConfigIndex,
                           INT4 *pi4RetValVplsConfigRowStatus)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigMtu
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigMtu (UINT4 u4VplsConfigIndex, UINT4 *pu4RetValVplsConfigMtu)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4RetValVplsConfigMtu);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigVpnId
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigVpnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigVpnId (UINT4 u4VplsConfigIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValVplsConfigVpnId)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pRetValVplsConfigVpnId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigStorageType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigStorageType (UINT4 u4VplsConfigIndex,
                             INT4 *pi4RetValVplsConfigStorageType)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsConfigSignalingType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsConfigSignalingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsConfigSignalingType (UINT4 u4VplsConfigIndex,
                               INT4 *pi4RetValVplsConfigSignalingType)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsConfigSignalingType);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsConfigName
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigName (UINT4 u4VplsConfigIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValVplsConfigName)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pSetValVplsConfigName);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigDescr
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigDescr (UINT4 u4VplsConfigIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValVplsConfigDescr)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pSetValVplsConfigDescr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigAdminStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigAdminStatus (UINT4 u4VplsConfigIndex,
                             INT4 i4SetValVplsConfigAdminStatus)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigAdminStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigMacLearning
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigMacLearning
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigMacLearning (UINT4 u4VplsConfigIndex,
                             INT4 i4SetValVplsConfigMacLearning)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigMacLearning);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigDiscardUnknownDest
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigDiscardUnknownDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigDiscardUnknownDest (UINT4 u4VplsConfigIndex,
                                    INT4 i4SetValVplsConfigDiscardUnknownDest)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigDiscardUnknownDest);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigMacAging
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigMacAging
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigMacAging (UINT4 u4VplsConfigIndex,
                          INT4 i4SetValVplsConfigMacAging)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigMacAging);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigFwdFullHighWatermark
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigFwdFullHighWatermark
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigFwdFullHighWatermark (UINT4 u4VplsConfigIndex,
                                      UINT4
                                      u4SetValVplsConfigFwdFullHighWatermark)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4SetValVplsConfigFwdFullHighWatermark);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigFwdFullLowWatermark
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigFwdFullLowWatermark
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigFwdFullLowWatermark (UINT4 u4VplsConfigIndex,
                                     UINT4
                                     u4SetValVplsConfigFwdFullLowWatermark)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4SetValVplsConfigFwdFullLowWatermark);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigRowStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigRowStatus (UINT4 u4VplsConfigIndex,
                           INT4 i4SetValVplsConfigRowStatus)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigRowStatus);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigMtu
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigMtu (UINT4 u4VplsConfigIndex, UINT4 u4SetValVplsConfigMtu)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4SetValVplsConfigMtu);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigStorageType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigStorageType (UINT4 u4VplsConfigIndex,
                             INT4 i4SetValVplsConfigStorageType)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigStorageType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsConfigSignalingType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsConfigSignalingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsConfigSignalingType (UINT4 u4VplsConfigIndex,
                               INT4 i4SetValVplsConfigSignalingType)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsConfigSignalingType);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsConfigName
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigName (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValVplsConfigName)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pTestValVplsConfigName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigDescr
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigDescr (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValVplsConfigDescr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pTestValVplsConfigDescr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigAdminStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigAdminStatus (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                INT4 i4TestValVplsConfigAdminStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigAdminStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigMacLearning
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigMacLearning
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigMacLearning (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                INT4 i4TestValVplsConfigMacLearning)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigMacLearning);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigDiscardUnknownDest
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigDiscardUnknownDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigDiscardUnknownDest (UINT4 *pu4ErrorCode,
                                       UINT4 u4VplsConfigIndex,
                                       INT4
                                       i4TestValVplsConfigDiscardUnknownDest)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigDiscardUnknownDest);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigMacAging
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigMacAging
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigMacAging (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                             INT4 i4TestValVplsConfigMacAging)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigMacAging);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigFwdFullHighWatermark
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigFwdFullHighWatermark
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigFwdFullHighWatermark (UINT4 *pu4ErrorCode,
                                         UINT4 u4VplsConfigIndex,
                                         UINT4
                                         u4TestValVplsConfigFwdFullHighWatermark)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4TestValVplsConfigFwdFullHighWatermark);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigFwdFullLowWatermark
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigFwdFullLowWatermark
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigFwdFullLowWatermark (UINT4 *pu4ErrorCode,
                                        UINT4 u4VplsConfigIndex,
                                        UINT4
                                        u4TestValVplsConfigFwdFullLowWatermark)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4TestValVplsConfigFwdFullLowWatermark);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigRowStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigRowStatus (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                              INT4 i4TestValVplsConfigRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigMtu
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigMtu (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                        UINT4 u4TestValVplsConfigMtu)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4TestValVplsConfigMtu);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigStorageType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigStorageType (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                INT4 i4TestValVplsConfigStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsConfigSignalingType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsConfigSignalingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsConfigSignalingType (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                  INT4 i4TestValVplsConfigSignalingType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsConfigSignalingType);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VplsConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VplsStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsStatusTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVplsStatusTable (UINT4 u4VplsConfigIndex)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVplsStatusTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVplsStatusTable (UINT4 *pu4VplsConfigIndex)
{
    UNUSED_PARAM (pu4VplsConfigIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexVplsStatusTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVplsStatusTable (UINT4 u4VplsConfigIndex,
                                UINT4 *pu4NextVplsConfigIndex)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4NextVplsConfigIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsStatusOperStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsStatusOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsStatusOperStatus (UINT4 u4VplsConfigIndex,
                            INT4 *pi4RetValVplsStatusOperStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
    if (NULL == pVplsEntry)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValVplsStatusOperStatus = (INT4) L2VPN_VPLS_OPER_STATUS (pVplsEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsStatusOperStatus);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsStatusPeerCount
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsStatusPeerCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsStatusPeerCount (UINT4 u4VplsConfigIndex,
                           UINT4 *pu4RetValVplsStatusPeerCount)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
    if (NULL == pVplsEntry)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValVplsStatusPeerCount = L2VPN_VPLS_PEER_COUNT (pVplsEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4RetValVplsStatusPeerCount);
#endif

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VplsPwBindTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVplsPwBindTable (UINT4 u4VplsConfigIndex,
                                         UINT4 u4PwIndex)
{

    if (!((u4VplsConfigIndex >= L2VPN_ONE) &&
          (u4VplsConfigIndex <= L2VPN_MAX_VPLS_ENTRIES)))
    {
        return SNMP_FAILURE;
    }

    if (!((u4PwIndex >= L2VPN_PWVC_INDEX_MINVAL) &&
          (u4PwIndex <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVplsPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVplsPwBindTable (UINT4 *pu4VplsConfigIndex, UINT4 *pu4PwIndex)
{
#ifdef HVPLS_WANTED
       tVplsPwBindEntry          *pVplsPwBindEntry=NULL;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
                   RBTreeGetFirst(L2VPN_VPLS_PW_BIND_RB_PTR(gpPwVcGlobalInfo));
    if ( NULL == pVplsPwBindEntry )
    {
        
        return SNMP_FAILURE;
    }
        
    *pu4VplsConfigIndex  =  L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (pVplsPwBindEntry);
    *pu4PwIndex          =   L2VPN_VPLS_PW_BIND_PWINDEX (pVplsPwBindEntry) ;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM(pu4VplsConfigIndex);
    UNUSED_PARAM(pu4PwIndex);
    return SNMP_FAILURE;
#endif
    
}

/****************************************************************************
 Function    :  nmhGetNextIndexVplsPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVplsPwBindTable (UINT4 u4VplsConfigIndex,
                                UINT4 *pu4NextVplsConfigIndex,
                                UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
#ifdef HVPLS_WANTED
    tVplsPwBindEntry          VplsPwBindEntry;
    tVplsPwBindEntry          *pVplsPwBindEntry=NULL;

    MEMSET(&VplsPwBindEntry, L2VPN_ZERO, sizeof(tVplsPwBindEntry));

    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&VplsPwBindEntry) = u4VplsConfigIndex;
   L2VPN_VPLS_PW_BIND_PWINDEX (&VplsPwBindEntry) = u4PwIndex;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
                    RBTreeGetNext(L2VPN_VPLS_PW_BIND_RB_PTR(gpPwVcGlobalInfo),
                                  &VplsPwBindEntry,
                                  NULL);
    if ( NULL == pVplsPwBindEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4NextVplsConfigIndex  =  L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (pVplsPwBindEntry);
    *pu4NextPwIndex          =   L2VPN_VPLS_PW_BIND_PWINDEX (pVplsPwBindEntry) ;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM(u4VplsConfigIndex);
    UNUSED_PARAM(pu4NextVplsConfigIndex);
    UNUSED_PARAM(u4PwIndex);
    UNUSED_PARAM(pu4NextPwIndex);
    return SNMP_FAILURE;
#endif

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsPwBindConfigType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                retValVplsPwBindConfigType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsPwBindConfigType (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                            INT4 *pi4RetValVplsPwBindConfigType)
{
#ifdef HVPLS_WANTED
    tVplsPwBindEntry          vplsPwBindEntry;
    tVplsPwBindEntry          *pVplsPwBindEntry = NULL;
    MEMSET(&vplsPwBindEntry,0,sizeof(tVplsPwBindEntry));
    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
    L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;
    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                (tRBElem *) & vplsPwBindEntry);
    if ( NULL == pVplsPwBindEntry )
    {
        return SNMP_FAILURE;
    }

    *pi4RetValVplsPwBindConfigType = L2VPN_VPLS_PW_BIND_CONFIG_TYPE(pVplsPwBindEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM ((INT4)u4PwIndex);
    UNUSED_PARAM (pi4RetValVplsPwBindConfigType);    
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsPwBindType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                retValVplsPwBindType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsPwBindType (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                      INT4 *pi4RetValVplsPwBindType)
{
#ifdef HVPLS_WANTED
    tVplsPwBindEntry          vplsPwBindEntry;
    tVplsPwBindEntry          *pVplsPwBindEntry = NULL;
    MEMSET(&vplsPwBindEntry,0,sizeof(tVplsPwBindEntry));
    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
    L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;
    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                (tRBElem *) & vplsPwBindEntry);
    if ( NULL == pVplsPwBindEntry )
    {
        return SNMP_FAILURE;
    }

    *pi4RetValVplsPwBindType = L2VPN_VPLS_PW_BIND_BIND_TYPE(pVplsPwBindEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM ((INT4)u4PwIndex);
    UNUSED_PARAM (pi4RetValVplsPwBindType);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsPwBindRowStatus
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                retValVplsPwBindRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsPwBindRowStatus (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                           INT4 *pi4RetValVplsPwBindRowStatus)
{
#ifdef HVPLS_WANTED
     tVplsPwBindEntry          vplsPwBindEntry;
    tVplsPwBindEntry          *pVplsPwBindEntry = NULL;
    MEMSET(&vplsPwBindEntry,0,sizeof(tVplsPwBindEntry));
    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
    L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;
    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                (tRBElem *) & vplsPwBindEntry);
    if ( NULL == pVplsPwBindEntry )
    {
        return SNMP_FAILURE;
    }

    *pi4RetValVplsPwBindRowStatus = L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4PwIndex);
    UNUSED_PARAM (pi4RetValVplsPwBindRowStatus);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsPwBindStorageType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                retValVplsPwBindStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsPwBindStorageType (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                             INT4 *pi4RetValVplsPwBindStorageType)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4PwIndex);
    UNUSED_PARAM (pi4RetValVplsPwBindStorageType);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsPwBindConfigType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                setValVplsPwBindConfigType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsPwBindConfigType (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                            INT4 i4SetValVplsPwBindConfigType)
{
#ifdef HVPLS_WANTED
   tVplsPwBindEntry          vplsPwBindEntry;
   tVplsPwBindEntry          *pVplsPwBindEntry = NULL;
    MEMSET (&vplsPwBindEntry, 0, sizeof (tVplsPwBindEntry));

    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
   L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsPwBindEntry);

    if (pVplsPwBindEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    L2VPN_VPLS_PW_BIND_CONFIG_TYPE (pVplsPwBindEntry) =
                       i4SetValVplsPwBindConfigType;
    return SNMP_SUCCESS;
#else
UNUSED_PARAM ((INT4)u4VplsConfigIndex);
UNUSED_PARAM (u4PwIndex);
UNUSED_PARAM (i4SetValVplsPwBindConfigType);
    return SNMP_SUCCESS;
#endif    
}

/****************************************************************************
 Function    :  nmhSetVplsPwBindType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                setValVplsPwBindType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsPwBindType (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                      INT4 i4SetValVplsPwBindType)
{
#ifdef HVPLS_WANTED
    tVplsPwBindEntry          vplsPwBindEntry;
    tVplsPwBindEntry          *pVplsPwBindEntry=NULL;
    MEMSET (&vplsPwBindEntry, 0, sizeof (tVplsPwBindEntry));

    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
    L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                (tRBElem *) & vplsPwBindEntry);

    if (pVplsPwBindEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    L2VPN_VPLS_PW_BIND_BIND_TYPE  (pVplsPwBindEntry) =
        i4SetValVplsPwBindType;
    return SNMP_SUCCESS;
#else
UNUSED_PARAM ((INT4)u4VplsConfigIndex);
UNUSED_PARAM (u4PwIndex);
UNUSED_PARAM (i4SetValVplsPwBindType);
   return SNMP_SUCCESS;
#endif    
}

/****************************************************************************
 Function    :  nmhSetVplsPwBindRowStatus
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                setValVplsPwBindRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsPwBindRowStatus (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                           INT4 i4SetValVplsPwBindRowStatus)
{
#ifdef HVPLS_WANTED
    tVplsPwBindEntry          vplsPwBindEntry;
    tVplsPwBindEntry          *pVplsPwBindEntry;
    MEMSET (&vplsPwBindEntry, 0, sizeof (tVplsPwBindEntry));

    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
    L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                (tRBElem *) & vplsPwBindEntry);

    if (pVplsPwBindEntry == NULL && CREATE_AND_WAIT !=
            i4SetValVplsPwBindRowStatus)
    {
        /*CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);*/
        return SNMP_FAILURE;
    }

    if (pVplsPwBindEntry != NULL && L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry)
            == i4SetValVplsPwBindRowStatus)
    {
        return SNMP_SUCCESS;
    }
    switch (i4SetValVplsPwBindRowStatus)
    {
        case CREATE_AND_WAIT:
            pVplsPwBindEntry =
                (tVplsPwBindEntry *) MemAllocMemBlk (L2VPN_VPLS_BIND_POOL_ID);
            if (NULL == pVplsPwBindEntry)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                        "nmhSetVplsPwBindRowStatus :"
                        " Memory Allocate Failed \t\n");
                return SNMP_FAILURE;
            }

            MEMSET (pVplsPwBindEntry, 0, sizeof (tVplsPwBindEntry));
            L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (pVplsPwBindEntry) = u4VplsConfigIndex;
            L2VPN_VPLS_PW_BIND_PWINDEX (pVplsPwBindEntry) = u4PwIndex;
            L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry) = NOT_IN_SERVICE;
            L2VPN_VPLS_PW_BIND_CONFIG_TYPE (pVplsPwBindEntry) =
                L2VPN_VPLS_PW_BIND_INVALID_CONFIG_TYPE;
            L2VPN_VPLS_PW_BIND_BIND_TYPE (pVplsPwBindEntry) =
                L2VPN_VPLS_PW_BIND_INVALID_BIND_TYPE;
            if (RBTreeAdd (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                        (tRBElem *) pVplsPwBindEntry) == RB_FAILURE)
            {

                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                        "nmhSetVplsPwBindRowStatus :"
                        "VPLS RD RBTree Add Failed \t\n");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            if (L2VPN_VPLS_PW_BIND_CONFIG_TYPE (pVplsPwBindEntry) ==
                    L2VPN_VPLS_PW_BIND_INVALID_CONFIG_TYPE ||
                    L2VPN_VPLS_PW_BIND_BIND_TYPE (pVplsPwBindEntry) ==
                    L2VPN_VPLS_PW_BIND_INVALID_BIND_TYPE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Value is not assigned\t\n", __func__);
                return SNMP_FAILURE;
            }

            L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry) =
                (INT1) i4SetValVplsPwBindRowStatus;

            break;
        case NOT_IN_SERVICE:
            if ((L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry) == ACTIVE))
            {

                L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry) = NOT_IN_SERVICE;
            }
            break;
        case DESTROY:


            pVplsPwBindEntry = (tVplsPwBindEntry *)
                RBTreeRem (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                        (tRBElem *) pVplsPwBindEntry);
            if (NULL == pVplsPwBindEntry)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : VPLS BIND RBTree delete Failed \t\n", __func__);
                return SNMP_FAILURE;
            }

            if (MEM_FAILURE == MemReleaseMemBlock (L2VPN_VPLS_BIND_POOL_ID,
                        (UINT1 *) pVplsPwBindEntry))
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : "
                        "MemRelease failed for L2VPN_VPLS_BIND_POOL_ID \t\n",
                        __func__);
                return SNMP_FAILURE;
            }

            break;
        default:
            return SNMP_FAILURE;
    } 
    return SNMP_SUCCESS;
#else
UNUSED_PARAM (u4VplsConfigIndex);
UNUSED_PARAM (u4PwIndex);
UNUSED_PARAM (i4SetValVplsPwBindRowStatus);
return SNMP_SUCCESS;
#endif
}

/**************************************************************************** 
 Function    :  nmhSetVplsPwBindStorageType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                setValVplsPwBindStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsPwBindStorageType (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex,
                             INT4 i4SetValVplsPwBindStorageType)
{
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4PwIndex);
    UNUSED_PARAM (i4SetValVplsPwBindStorageType);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsPwBindConfigType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                testValVplsPwBindConfigType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsPwBindConfigType (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                               UINT4 u4PwIndex,
                               INT4 i4TestValVplsPwBindConfigType)
{
#ifdef HVPLS_WANTED
   tVplsPwBindEntry          vplsPwBindEntry;
   tVplsPwBindEntry          *pVplsPwBindEntry=NULL;
   MEMSET(&vplsPwBindEntry,0,sizeof(tVplsPwBindEntry)); 
   if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValVplsPwBindConfigType > L2VPN_VPLS_PW_BIND_SIG_LDP) &&
        (i4TestValVplsPwBindConfigType > L2VPN_VPLS_PW_BIND_SIG_BGP ))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
   L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsPwBindEntry);
    if (pVplsPwBindEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }


       return SNMP_SUCCESS;
#else
UNUSED_PARAM (pu4ErrorCode);
UNUSED_PARAM (u4VplsConfigIndex);
UNUSED_PARAM (u4PwIndex);
UNUSED_PARAM (i4TestValVplsPwBindConfigType);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2VplsPwBindType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                testValVplsPwBindType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsPwBindType (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                         UINT4 u4PwIndex, INT4 i4TestValVplsPwBindType)
{
#ifdef HVPLS_WANTED
    tVplsPwBindEntry          vplsPwBindEntry;
   tVplsPwBindEntry          *pVplsPwBindEntry=NULL;
   MEMSET(&vplsPwBindEntry,0,sizeof(tVplsPwBindEntry)); 
   if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
 
    if ((i4TestValVplsPwBindType <= L2VPN_VPLS_TYPE_MIN) ||
        (i4TestValVplsPwBindType >= L2VPN_VPLS_TYPE_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }


       L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
   L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;

    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsPwBindEntry);
    if (pVplsPwBindEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (L2VPN_VPLS_PW_BIND_ROW_STATUS (pVplsPwBindEntry) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        /*CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);*/
        return SNMP_FAILURE;
    }

    
   

    return SNMP_SUCCESS;
#else
UNUSED_PARAM (pu4ErrorCode);
UNUSED_PARAM (u4VplsConfigIndex);
UNUSED_PARAM (u4PwIndex);
UNUSED_PARAM (i4TestValVplsPwBindType);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2VplsPwBindRowStatus
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                testValVplsPwBindRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsPwBindRowStatus (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                              UINT4 u4PwIndex,
                              INT4 i4TestValVplsPwBindRowStatus)
{
#ifdef HVPLS_WANTED
   tVplsPwBindEntry          vplsPwBindEntry;
   tVplsPwBindEntry          *pVplsPwBindEntry=NULL;
    MEMSET(&vplsPwBindEntry,0,sizeof(tVplsPwBindEntry));
   if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4PwIndex > L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo) || u4PwIndex < 1)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   if ((u4VplsConfigIndex == 0) ||
        (u4VplsConfigIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
   L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
   L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;
    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsPwBindEntry);
     if (pVplsPwBindEntry == NULL && CREATE_AND_WAIT !=
        i4TestValVplsPwBindRowStatus)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        /*CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);*/
        return SNMP_FAILURE;
    }
    switch (i4TestValVplsPwBindRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pVplsPwBindEntry != NULL)
            {
               /* CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);*/
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Row Already Exists\t\n", __func__);
                return SNMP_FAILURE;
            }
            break;
           case ACTIVE:
            if (L2VPN_VPLS_PW_BIND_CONFIG_TYPE (pVplsPwBindEntry) ==
                L2VPN_VPLS_PW_BIND_INVALID_CONFIG_TYPE ||
                L2VPN_VPLS_PW_BIND_BIND_TYPE (pVplsPwBindEntry) ==
                L2VPN_VPLS_PW_BIND_INVALID_BIND_TYPE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Value is not assigned\t\n", __func__);
                return SNMP_FAILURE;
            }

        case NOT_IN_SERVICE:
        case DESTROY:
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

#else   
UNUSED_PARAM (pu4ErrorCode);
UNUSED_PARAM (u4VplsConfigIndex);
UNUSED_PARAM (u4PwIndex);
UNUSED_PARAM (i4TestValVplsPwBindRowStatus);
    return SNMP_SUCCESS; 
#endif
}

/****************************************************************************
 Function    :  nmhTestv2VplsPwBindStorageType
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                testValVplsPwBindStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
		****************************************************************************/
INT1
nmhTestv2VplsPwBindStorageType (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                UINT4 u4PwIndex,
                                INT4 i4TestValVplsPwBindStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4PwIndex);
    UNUSED_PARAM (i4TestValVplsPwBindStorageType);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VplsPwBindTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VplsBgpADConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsBgpADConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVplsBgpADConfigTable (UINT4 u4VplsConfigIndex)
{
#ifdef VPLSADS_WANTED
    if ((u4VplsConfigIndex == 0) ||
        (u4VplsConfigIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (u4VplsConfigIndex);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVplsBgpADConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVplsBgpADConfigTable (UINT4 *pu4VplsConfigIndex)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGetFirst (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
    if (NULL == pVplsRdEntry)
    {
        return SNMP_FAILURE;
    }

    *pu4VplsConfigIndex = L2VPN_VPLSRD_VPLS_INSTANCE (pVplsRdEntry);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4VplsConfigIndex);

    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexVplsBgpADConfigTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVplsBgpADConfigTable (UINT4 u4VplsConfigIndex,
                                     UINT4 *pu4NextVplsConfigIndex)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, 0, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGetNext (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                       &VplsRdEntry, NULL);
    if (NULL == pVplsRdEntry)
    {
        return SNMP_FAILURE;
    }

    *pu4NextVplsConfigIndex = L2VPN_VPLSRD_VPLS_INSTANCE (pVplsRdEntry);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4NextVplsConfigIndex);

    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsBgpADConfigRouteDistinguisher
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsBgpADConfigRouteDistinguisher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpADConfigRouteDistinguisher (UINT4 u4VplsConfigIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValVplsBgpADConfigRouteDistinguisher)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpADConfigRouteDistinguisher :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    pRetValVplsBgpADConfigRouteDistinguisher->i4_Length = L2VPN_MAX_VPLS_RD_LEN;

    MEMCPY (pRetValVplsBgpADConfigRouteDistinguisher->pu1_OctetList,
            L2VPN_VPLSRD_ROUTE_DISTINGUISHER (pVplsRdEntry),
            pRetValVplsBgpADConfigRouteDistinguisher->i4_Length);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pRetValVplsBgpADConfigRouteDistinguisher);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpADConfigPrefix
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsBgpADConfigPrefix
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpADConfigPrefix (UINT4 u4VplsConfigIndex,
                             UINT4 *pu4RetValVplsBgpADConfigPrefix)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpADConfigRouteDistinguisher :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pu4RetValVplsBgpADConfigPrefix = L2VPN_VPLSRD_CONFIG_PREFIX (pVplsRdEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4RetValVplsBgpADConfigPrefix);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpADConfigVplsId
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsBgpADConfigVplsId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpADConfigVplsId (UINT4 u4VplsConfigIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValVplsBgpADConfigVplsId)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpADConfigRouteDistinguisher :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    pRetValVplsBgpADConfigVplsId->i4_Length =
        (INT4) STRLEN (L2VPN_VPLSRD_VPLS_ID (pVplsRdEntry));

    MEMCPY (pRetValVplsBgpADConfigVplsId->pu1_OctetList,
            L2VPN_VPLSRD_VPLS_ID (pVplsRdEntry),
            pRetValVplsBgpADConfigVplsId->i4_Length);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pRetValVplsBgpADConfigVplsId);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpADConfigRowStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsBgpADConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpADConfigRowStatus (UINT4 u4VplsConfigIndex,
                                INT4 *pi4RetValVplsBgpADConfigRowStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpADConfigRowStatus :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpADConfigRowStatus = L2VPN_VPLSRD_ROW_STATUS (pVplsRdEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsBgpADConfigRowStatus);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpADConfigStorageType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsBgpADConfigStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpADConfigStorageType (UINT4 u4VplsConfigIndex,
                                  INT4 *pi4RetValVplsBgpADConfigStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpADConfigStorageType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpADConfigStorageType =
        L2VPN_VPLSRD_STORAGE_TYPE (pVplsRdEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pi4RetValVplsBgpADConfigStorageType);
#endif

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsBgpADConfigRouteDistinguisher
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsBgpADConfigRouteDistinguisher
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpADConfigRouteDistinguisher (UINT4 u4VplsConfigIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValVplsBgpADConfigRouteDistinguisher)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpADConfigRouteDistinguisher :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    MEMCPY (L2VPN_VPLSRD_ROUTE_DISTINGUISHER (pVplsRdEntry),
            pSetValVplsBgpADConfigRouteDistinguisher->pu1_OctetList,
            pSetValVplsBgpADConfigRouteDistinguisher->i4_Length);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pSetValVplsBgpADConfigRouteDistinguisher);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpADConfigPrefix
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsBgpADConfigPrefix
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpADConfigPrefix (UINT4 u4VplsConfigIndex,
                             UINT4 u4SetValVplsBgpADConfigPrefix)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpADConfigPrefix :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_CONFIG_PREFIX (pVplsRdEntry) = u4SetValVplsBgpADConfigPrefix;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4SetValVplsBgpADConfigPrefix);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpADConfigVplsId
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsBgpADConfigVplsId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpADConfigVplsId (UINT4 u4VplsConfigIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValVplsBgpADConfigVplsId)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpADConfigVplsId :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    MEMCPY (L2VPN_VPLSRD_VPLS_ID (pVplsRdEntry),
            pSetValVplsBgpADConfigVplsId->pu1_OctetList,
            pSetValVplsBgpADConfigVplsId->i4_Length);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pSetValVplsBgpADConfigVplsId);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpADConfigRowStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsBgpADConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpADConfigRowStatus (UINT4 u4VplsConfigIndex,
                                INT4 i4SetValVplsBgpADConfigRowStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry &&
        CREATE_AND_WAIT != i4SetValVplsBgpADConfigRowStatus)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpADConfigRowStatus :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    switch (i4SetValVplsBgpADConfigRowStatus)
    {
        case CREATE_AND_WAIT:
            pVplsRdEntry =
                (tVPLSRdEntry *) MemAllocMemBlk (L2VPN_VPLS_RD_POOL_ID);

            if (NULL == pVplsRdEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RD_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpADConfigRowStatus :"
                           " Memory Allocate Failed \t\n");
                return SNMP_FAILURE;
            }

            MEMSET (pVplsRdEntry, 0, sizeof (tVPLSRdEntry));
            L2VPN_VPLSRD_VPLS_INSTANCE (pVplsRdEntry) = u4VplsConfigIndex;
            L2VPN_VPLSRD_ROW_STATUS (pVplsRdEntry) = NOT_READY;
            L2VPN_VPLSRD_STORAGE_TYPE (pVplsRdEntry) =
                L2VPN_STORAGE_NONVOLATILE;
            if (RBTreeAdd
                (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                 (tRBElem *) pVplsRdEntry) == RB_FAILURE)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RD_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpADConfigRowStatus :"
                           "VPLS RD RBTree Add Failed \t\n");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
        case NOT_IN_SERVICE:
            L2VPN_VPLSRD_ROW_STATUS (pVplsRdEntry) =
                (INT1) i4SetValVplsBgpADConfigRowStatus;
            break;
        case DESTROY:
            pVplsRdEntry = (tVPLSRdEntry *)
                RBTreeRem (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                           (tRBElem *) pVplsRdEntry);
            if (NULL == pVplsRdEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RD_DELETE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpADConfigRowStatus :"
                           "VPLS RD RBTree delete Failed \t\n");
                return SNMP_FAILURE;
            }

            if (MEM_FAILURE == MemReleaseMemBlock (L2VPN_VPLS_RD_POOL_ID,
                                                   (UINT1 *) pVplsRdEntry))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RD_DELETE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpADConfigRowStatus :"
                           "MemRelease failed for L2VPN_VPLS_RD_POOL_ID \t\n");
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsBgpADConfigRowStatus);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpADConfigStorageType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsBgpADConfigStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpADConfigStorageType (UINT4 u4VplsConfigIndex,
                                  INT4 i4SetValVplsBgpADConfigStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpADConfigStorageType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_STORAGE_TYPE (pVplsRdEntry) =
        (UINT1) i4SetValVplsBgpADConfigStorageType;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4SetValVplsBgpADConfigStorageType);
    return SNMP_FAILURE;
#endif

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsBgpADConfigRouteDistinguisher
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsBgpADConfigRouteDistinguisher
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpADConfigRouteDistinguisher (UINT4 *pu4ErrorCode,
                                            UINT4 u4VplsConfigIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValVplsBgpADConfigRouteDistinguisher)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigRouteDistinguisher :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    if (ACTIVE == L2VPN_VPLSRD_ROW_STATUS (pVplsRdEntry))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigRouteDistinguisher :"
                   "Cannot modify object if RowStatus Active \t\n");
        return SNMP_FAILURE;
    }

    if (L2VPN_MAX_VPLS_RD_LEN !=
        pTestValVplsBgpADConfigRouteDistinguisher->i4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if ((L2VPN_VPLS_RD_TYPE_MAX <
         pTestValVplsBgpADConfigRouteDistinguisher->pu1_OctetList[0]) ||
        (L2VPN_VPLS_RD_SUBTYPE !=
         pTestValVplsBgpADConfigRouteDistinguisher->pu1_OctetList[1]))
    {
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_INVALID_RD_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pTestValVplsBgpADConfigRouteDistinguisher);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpADConfigPrefix
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsBgpADConfigPrefix
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpADConfigPrefix (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                UINT4 u4TestValVplsBgpADConfigPrefix)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigPrefix :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    if (ACTIVE == L2VPN_VPLSRD_ROW_STATUS (pVplsRdEntry))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigRouteDistinguisher :"
                   "Cannot modify object if RowStatus Active \t\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
#endif

    UNUSED_PARAM (u4TestValVplsBgpADConfigPrefix);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpADConfigVplsId
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsBgpADConfigVplsId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpADConfigVplsId (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValVplsBgpADConfigVplsId)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigVplsId :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    if (ACTIVE == L2VPN_VPLSRD_ROW_STATUS (pVplsRdEntry))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigRouteDistinguisher :"
                   "Cannot modify object if RowStatus Active \t\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
#endif

    UNUSED_PARAM (pTestValVplsBgpADConfigVplsId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpADConfigRowStatus
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsBgpADConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpADConfigRowStatus (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                   INT4 i4TestValVplsBgpADConfigRowStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsIndex = L2VPN_ZERO;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhValidateIndexInstanceVplsBgpADConfigTable (u4VplsConfigIndex))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigRowStatus :"
                   "u4VplsConfigIndex not valid\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry &&
        CREATE_AND_WAIT != i4TestValVplsBgpADConfigRowStatus)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigRowStatus :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }
    switch (i4TestValVplsBgpADConfigRowStatus)
    {
        case CREATE_AND_WAIT:
            if (NULL != pVplsRdEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RD_ALREADY_ASSOCIATED);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpADConfigRowStatus :"
                           "Row already exists\t\n");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            if (MPLS_SUCCESS !=
                MplsValidateRdRtWithAsn (L2VPN_VPLSRD_ROUTE_DISTINGUISHER
                                         (pVplsRdEntry)))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_INVALID_RD_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpADConfigRowStatus :"
                           "RD value is not matching with ASN configured in BGP\t\n");
                return SNMP_FAILURE;
            }

            if (L2VPN_SUCCESS ==
                L2VpnGetVplsIndexFromRd (L2VPN_VPLSRD_ROUTE_DISTINGUISHER
                                         (pVplsRdEntry), &u4VplsIndex))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_DUPLICATE_RD_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpADConfigRowStatus :"
                           "RD already in use by some other VPLS\t\n");
                return SNMP_FAILURE;
            }
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
            if (pVplsEntry != NULL)
            {
                if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case NOT_IN_SERVICE:
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
            if (pVplsEntry != NULL)
            {
                if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case DESTROY:
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
            if (pVplsEntry != NULL)
            {
                if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsBgpADConfigRowStatus);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpADConfigStorageType
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsBgpADConfigStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpADConfigStorageType (UINT4 *pu4ErrorCode,
                                     UINT4 u4VplsConfigIndex,
                                     INT4 i4TestValVplsBgpADConfigStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSRdEntry        VplsRdEntry;
    tVPLSRdEntry       *pVplsRdEntry = NULL;

    MEMSET (&VplsRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRD_VPLS_INSTANCE (&VplsRdEntry) = u4VplsConfigIndex;

    pVplsRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRdEntry);
    if (NULL == pVplsRdEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpADConfigStorageType :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    switch (i4TestValVplsBgpADConfigStorageType)
    {
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (i4TestValVplsBgpADConfigStorageType);
#endif

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsBgpADConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VplsBgpADConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VplsBgpRteTargetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsBgpRteTargetTable
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVplsBgpRteTargetTable (UINT4 u4VplsConfigIndex,
                                               UINT4 u4VplsBgpRteTargetIndex)
{
#ifdef VPLSADS_WANTED
    if ((u4VplsConfigIndex == 0) ||
        (u4VplsConfigIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (u4VplsConfigIndex);
#endif

    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVplsBgpRteTargetTable
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVplsBgpRteTargetTable (UINT4 *pu4VplsConfigIndex,
                                       UINT4 *pu4VplsBgpRteTargetIndex)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGetFirst (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
    if (NULL == pVplsRtEntry)
    {
        return SNMP_FAILURE;
    }

    *pu4VplsConfigIndex = L2VPN_VPLSRT_VPLS_INSTANCE (pVplsRtEntry);
    *pu4VplsBgpRteTargetIndex = L2VPN_VPLSRT_RT_INDEX (pVplsRtEntry);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4VplsConfigIndex);
    UNUSED_PARAM (pu4VplsBgpRteTargetIndex);

    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexVplsBgpRteTargetTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
                VplsBgpRteTargetIndex
                nextVplsBgpRteTargetIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVplsBgpRteTargetTable (UINT4 u4VplsConfigIndex,
                                      UINT4 *pu4NextVplsConfigIndex,
                                      UINT4 u4VplsBgpRteTargetIndex,
                                      UINT4 *pu4NextVplsBgpRteTargetIndex)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGetNext (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                       &VplsRtEntry, NULL);
    if (NULL == pVplsRtEntry)
    {
        return SNMP_FAILURE;
    }

    *pu4NextVplsConfigIndex = L2VPN_VPLSRT_VPLS_INSTANCE (pVplsRtEntry);
    *pu4NextVplsBgpRteTargetIndex = L2VPN_VPLSRT_RT_INDEX (pVplsRtEntry);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (pu4NextVplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pu4NextVplsBgpRteTargetIndex);

    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsBgpRteTargetRTType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                retValVplsBgpRteTargetRTType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpRteTargetRTType (UINT4 u4VplsConfigIndex,
                              UINT4 u4VplsBgpRteTargetIndex,
                              INT4 *pi4RetValVplsBgpRteTargetRTType)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpRteTargetRTType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpRteTargetRTType = L2VPN_VPLSRT_RT_TYPE (pVplsRtEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pi4RetValVplsBgpRteTargetRTType);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpRteTargetRT
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                retValVplsBgpRteTargetRT
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpRteTargetRT (UINT4 u4VplsConfigIndex,
                          UINT4 u4VplsBgpRteTargetIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValVplsBgpRteTargetRT)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpRteTargetRT :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    pRetValVplsBgpRteTargetRT->i4_Length = L2VPN_MAX_VPLS_RT_LEN;

    MEMCPY (pRetValVplsBgpRteTargetRT->pu1_OctetList,
            L2VPN_VPLSRT_ROUTE_TARGET (pVplsRtEntry),
            pRetValVplsBgpRteTargetRT->i4_Length);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pRetValVplsBgpRteTargetRT);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpRteTargetRTRowStatus
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                retValVplsBgpRteTargetRTRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpRteTargetRTRowStatus (UINT4 u4VplsConfigIndex,
                                   UINT4 u4VplsBgpRteTargetIndex,
                                   INT4 *pi4RetValVplsBgpRteTargetRTRowStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpRteTargetRTRowStatus :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpRteTargetRTRowStatus =
        L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pi4RetValVplsBgpRteTargetRTRowStatus);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsBgpRteTargetStorageType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                retValVplsBgpRteTargetStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsBgpRteTargetStorageType (UINT4 u4VplsConfigIndex,
                                   UINT4 u4VplsBgpRteTargetIndex,
                                   INT4 *pi4RetValVplsBgpRteTargetStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpRteTargetStorageType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpRteTargetStorageType =
        L2VPN_VPLSRT_STORAGE_TYPE (pVplsRtEntry);
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pi4RetValVplsBgpRteTargetStorageType);
#endif

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsBgpRteTargetRTType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                setValVplsBgpRteTargetRTType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpRteTargetRTType (UINT4 u4VplsConfigIndex,
                              UINT4 u4VplsBgpRteTargetIndex,
                              INT4 i4SetValVplsBgpRteTargetRTType)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpRteTargetRTType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRT_RT_TYPE (pVplsRtEntry) =
        (UINT1) i4SetValVplsBgpRteTargetRTType;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (i4SetValVplsBgpRteTargetRTType);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpRteTargetRT
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                setValVplsBgpRteTargetRT
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpRteTargetRT (UINT4 u4VplsConfigIndex,
                          UINT4 u4VplsBgpRteTargetIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValVplsBgpRteTargetRT)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpRteTargetRT :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    MEMCPY (L2VPN_VPLSRT_ROUTE_TARGET (pVplsRtEntry),
            pSetValVplsBgpRteTargetRT->pu1_OctetList,
            pSetValVplsBgpRteTargetRT->i4_Length);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pSetValVplsBgpRteTargetRT);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpRteTargetRTRowStatus
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                setValVplsBgpRteTargetRTRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpRteTargetRTRowStatus (UINT4 u4VplsConfigIndex,
                                   UINT4 u4VplsBgpRteTargetIndex,
                                   INT4 i4SetValVplsBgpRteTargetRTRowStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry &&
        CREATE_AND_WAIT != i4SetValVplsBgpRteTargetRTRowStatus)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpRteTargetRTRowStatus :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValVplsBgpRteTargetRTRowStatus)
    {
        case CREATE_AND_WAIT:
            pVplsRtEntry =
                (tVPLSRtEntry *) MemAllocMemBlk (L2VPN_VPLS_RT_POOL_ID);

            if (NULL == pVplsRtEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RT_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpRteTargetRTRowStatus :"
                           " Memory Allocate Failed \t\n");
                return SNMP_FAILURE;
            }

            MEMSET (pVplsRtEntry, 0, sizeof (tVPLSRtEntry));
            L2VPN_VPLSRT_VPLS_INSTANCE (pVplsRtEntry) = u4VplsConfigIndex;
            L2VPN_VPLSRT_RT_INDEX (pVplsRtEntry) = u4VplsBgpRteTargetIndex;
            L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry) = NOT_READY;
            L2VPN_VPLSRT_STORAGE_TYPE (pVplsRtEntry) = L2VPN_STORAGE_VOLATILE;
            if (RBTreeAdd (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                           (tRBElem *) pVplsRtEntry) == RB_FAILURE)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RT_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpRteTargetRTRowStatus :"
                           "VPLS RT RBTree Add Failed \t\n");
                return SNMP_FAILURE;
            }

            L2VpnSetRtIndexForVpls (u4VplsConfigIndex, u4VplsBgpRteTargetIndex);
            break;
        case ACTIVE:
            L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry) = ACTIVE;
            L2VpnSendRtAddAdminEvent (u4VplsConfigIndex,
                                      L2VPN_VPLSRT_ROUTE_TARGET (pVplsRtEntry),
                                      L2VPN_VPLSRT_RT_TYPE (pVplsRtEntry));
            break;
        case NOT_IN_SERVICE:
            if (ACTIVE == L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry))
            {
                L2VpnSendRtDeleteAdminEvent (u4VplsConfigIndex,
                                             L2VPN_VPLSRT_ROUTE_TARGET
                                             (pVplsRtEntry),
                                             L2VPN_VPLSRT_RT_TYPE
                                             (pVplsRtEntry));
            }
            L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry) = NOT_IN_SERVICE;
            break;
        case DESTROY:
            if (ACTIVE == L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry))
            {
                L2VpnSendRtDeleteAdminEvent (u4VplsConfigIndex,
                                             L2VPN_VPLSRT_ROUTE_TARGET
                                             (pVplsRtEntry),
                                             L2VPN_VPLSRT_RT_TYPE
                                             (pVplsRtEntry));
            }
            pVplsRtEntry = (tVPLSRtEntry *)
                RBTreeRem (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                           (tRBElem *) pVplsRtEntry);
            if (NULL == pVplsRtEntry)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RT_DELETE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpRteTargetRTRowStatus :"
                           "VPLS RT RBTree delete Failed \t\n");
                return SNMP_FAILURE;
            }

            if (MEM_FAILURE == MemReleaseMemBlock (L2VPN_VPLS_RT_POOL_ID,
                                                   (UINT1 *) pVplsRtEntry))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_RT_DELETE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpRteTargetRTRowStatus :"
                           "MemRelease failed for L2VPN_VPLS_RT_POOL_ID \t\n");
                return SNMP_FAILURE;
            }

            L2VpnReleaseRtIndexForVpls (u4VplsConfigIndex,
                                        u4VplsBgpRteTargetIndex);
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (i4SetValVplsBgpRteTargetRTRowStatus);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetVplsBgpRteTargetStorageType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                setValVplsBgpRteTargetStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsBgpRteTargetStorageType (UINT4 u4VplsConfigIndex,
                                   UINT4 u4VplsBgpRteTargetIndex,
                                   INT4 i4SetValVplsBgpRteTargetStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpRteTargetStorageType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRT_STORAGE_TYPE (pVplsRtEntry) =
        (UINT1) i4SetValVplsBgpRteTargetStorageType;

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (i4SetValVplsBgpRteTargetStorageType);
    return SNMP_FAILURE;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsBgpRteTargetRTType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                testValVplsBgpRteTargetRTType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpRteTargetRTType (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                                 UINT4 u4VplsBgpRteTargetIndex,
                                 INT4 i4TestValVplsBgpRteTargetRTType)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetRTType :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    if (ACTIVE == L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetRTType :"
                   "Cannot modify object if RowStatus Active \t\n");
        return SNMP_FAILURE;
    }

    switch (i4TestValVplsBgpRteTargetRTType)
    {
        case L2VPN_VPLS_RT_IMPORT:
        case L2VPN_VPLS_RT_EXPORT:
        case L2VPN_VPLS_RT_BOTH:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (i4TestValVplsBgpRteTargetRTType);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpRteTargetRT
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                testValVplsBgpRteTargetRT
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpRteTargetRT (UINT4 *pu4ErrorCode, UINT4 u4VplsConfigIndex,
                             UINT4 u4VplsBgpRteTargetIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValVplsBgpRteTargetRT)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetRT :" "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    if (ACTIVE == L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetRT :"
                   "Cannot modify object if RowStatus Active \t\n");
        return SNMP_FAILURE;
    }

    if (L2VPN_MAX_VPLS_RT_LEN != pTestValVplsBgpRteTargetRT->i4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if ((L2VPN_VPLS_RT_TYPE_MAX <
         pTestValVplsBgpRteTargetRT->pu1_OctetList[0]) ||
        (L2VPN_VPLS_RT_SUBTYPE != pTestValVplsBgpRteTargetRT->pu1_OctetList[1]))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (pTestValVplsBgpRteTargetRT);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpRteTargetRTRowStatus
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                testValVplsBgpRteTargetRTRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpRteTargetRTRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4VplsConfigIndex,
                                      UINT4 u4VplsBgpRteTargetIndex,
                                      INT4 i4TestValVplsBgpRteTargetRTRowStatus)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4RtIndex = L2VPN_ZERO;
    UINT1               au1RouteTarget[L2VPN_MAX_VPLS_RT_LEN];

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhValidateIndexInstanceVplsBgpRteTargetTable (u4VplsConfigIndex,
                                                       u4VplsBgpRteTargetIndex))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetRTRowStatus :"
                   "u4VplsConfigIndex not valid\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry &&
        CREATE_AND_WAIT != i4TestValVplsBgpRteTargetRTRowStatus)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetRTRowStatus :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }
    switch (i4TestValVplsBgpRteTargetRTRowStatus)
    {
        case CREATE_AND_WAIT:
            if (NULL != pVplsRtEntry)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpRteTargetRTRowStatus :"
                           "Row already exists\t\n");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            if (MPLS_SUCCESS !=
                MplsValidateRdRtWithAsn (L2VPN_VPLSRT_ROUTE_TARGET
                                         (pVplsRtEntry)))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_INVALID_RT_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpADConfigRowStatus :"
                           "RT value is not matching with ASN configured in BGP\t\n");
                return SNMP_FAILURE;
            }

            if (L2VPN_SUCCESS ==
                L2VpnGetVplsAndRtIndexFromRtName (L2VPN_VPLSRT_ROUTE_TARGET
                                                  (pVplsRtEntry), &u4VplsIndex,
                                                  &u4RtIndex))
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_DUPLICATE_RT_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpADConfigRowStatus :"
                           "RT already in use by some other VPLS\t\n");
                return SNMP_FAILURE;
            }
            if (L2VPN_VPLS_RT_EXPORT == L2VPN_VPLSRT_RT_TYPE (pVplsRtEntry) ||
                L2VPN_VPLS_RT_BOTH == L2VPN_VPLSRT_RT_TYPE (pVplsRtEntry))
            {
                if (L2VPN_SUCCESS == L2VpnGetExportRtValue (u4VplsConfigIndex,
                                                            L2VPN_FALSE,
                                                            au1RouteTarget))
                {
                    CLI_SET_ERR (L2VPN_PW_VPLS_CLI_INVALID_RT_TYPE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "nmhTestv2VplsBgpADConfigRowStatus :"
                               "Export RT already exists for this VPLS\t\n");
                    return SNMP_FAILURE;
                }
            }
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
            if (pVplsEntry != NULL)
            {
                if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;
        case NOT_IN_SERVICE:
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
            if (pVplsEntry != NULL)
            {
                if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;
        case DESTROY:
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
            if (pVplsEntry != NULL)
            {
                if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (i4TestValVplsBgpRteTargetRTRowStatus);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VplsBgpRteTargetStorageType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex

                The Object 
                testValVplsBgpRteTargetStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsBgpRteTargetStorageType (UINT4 *pu4ErrorCode,
                                      UINT4 u4VplsConfigIndex,
                                      UINT4 u4VplsBgpRteTargetIndex,
                                      INT4 i4TestValVplsBgpRteTargetStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsConfigIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = u4VplsBgpRteTargetIndex;

    pVplsRtEntry = (tVPLSRtEntry *)
        RBTreeGet (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & VplsRtEntry);
    if (NULL == pVplsRtEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpRteTargetStorageType :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    switch (i4TestValVplsBgpRteTargetStorageType)
    {
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4VplsConfigIndex);
    UNUSED_PARAM (u4VplsBgpRteTargetIndex);
    UNUSED_PARAM (i4TestValVplsBgpRteTargetStorageType);
#endif

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsBgpRteTargetTable
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpRteTargetIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VplsBgpRteTargetTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsStatusNotifEnable
 Input       :  The Indices

                The Object 
                retValVplsStatusNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsStatusNotifEnable (INT4 *pi4RetValVplsStatusNotifEnable)
{
#ifdef HVPLS_WANTED 
    *pi4RetValVplsStatusNotifEnable = L2VPN_VPLS_NOTIF_ENABLE_STATUS; 
#else
    UNUSED_PARAM (pi4RetValVplsStatusNotifEnable);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVplsNotificationMaxRate
 Input       :  The Indices

                The Object 
                retValVplsNotificationMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVplsNotificationMaxRate (UINT4 *pu4RetValVplsNotificationMaxRate)
{
#ifdef HVPLS_WANTED
    *pu4RetValVplsNotificationMaxRate = L2VPN_VPLS_NOTIF_MAX_RATE; 
#else
    UNUSED_PARAM (pu4RetValVplsNotificationMaxRate);
#endif   
   return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsStatusNotifEnable
 Input       :  The Indices

                The Object 
                setValVplsStatusNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsStatusNotifEnable (INT4 i4SetValVplsStatusNotifEnable)
{
#ifdef HVPLS_WANTED
    L2VPN_VPLS_NOTIF_ENABLE_STATUS = i4SetValVplsStatusNotifEnable;
    if( L2VPN_SUCCESS ==  L2vpnHandleVplsNotification( i4SetValVplsStatusNotifEnable))
  
    {
        return SNMP_SUCCESS;  
    }
#else
    UNUSED_PARAM (i4SetValVplsStatusNotifEnable);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetVplsNotificationMaxRate
 Input       :  The Indices

                The Object 
                setValVplsNotificationMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVplsNotificationMaxRate (UINT4 u4SetValVplsNotificationMaxRate)
{
#ifdef HVPLS_WANTED
    L2VPN_VPLS_NOTIF_MAX_RATE = u4SetValVplsNotificationMaxRate;
     return SNMP_SUCCESS;
#else
    UNUSED_PARAM (u4SetValVplsNotificationMaxRate);
    return SNMP_FAILURE;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsStatusNotifEnable
 Input       :  The Indices

                The Object 
                testValVplsStatusNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
    INT1
nmhTestv2VplsStatusNotifEnable (UINT4 *pu4ErrorCode,
        INT4 i4TestValVplsStatusNotifEnable)
{
#ifdef HVPLS_WANTED
    if (( i4TestValVplsStatusNotifEnable  == L2VPN_ONE) ||
            (i4TestValVplsStatusNotifEnable == L2VPN_TWO))
    {
        return SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValVplsStatusNotifEnable);
#endif
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2VplsNotificationMaxRate
 Input       :  The Indices

                The Object 
                testValVplsNotificationMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VplsNotificationMaxRate (UINT4 *pu4ErrorCode,
        UINT4 u4TestValVplsNotificationMaxRate)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValVplsNotificationMaxRate);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsStatusNotifEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VplsStatusNotifEnable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2VplsNotificationMaxRate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VplsNotificationMaxRate (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedundancyStatus
 Input       :  The Indices

                The Object 
                retValFsL2VpnPwRedundancyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedundancyStatus (INT4 *pi4RetValFsL2VpnPwRedundancyStatus)
{
    *pi4RetValFsL2VpnPwRedundancyStatus = L2VPN_PWRED_STATUS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedNegotiationTimeOut
 Input       :  The Indices

                The Object 
                retValFsL2VpnPwRedNegotiationTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedNegotiationTimeOut (UINT4
                                      *pu4RetValFsL2VpnPwRedNegotiationTimeOut)
{
    *pu4RetValFsL2VpnPwRedNegotiationTimeOut =
        gL2vpnRgGlobals.u4NegotiationTimeout;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedundancySyncFailNotifyEnable
 Input       :  The Indices

                The Object 
                retValFsL2VpnPwRedundancySyncFailNotifyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedundancySyncFailNotifyEnable (INT4
                                               *pi4RetValFsL2VpnPwRedundancySyncFailNotifyEnable)
{
    *pi4RetValFsL2VpnPwRedundancySyncFailNotifyEnable =
        gL2vpnRgGlobals.b1SyncFailNotifEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedundancyPwStatusNotifyEnable
 Input       :  The Indices

                The Object 
                retValFsL2VpnPwRedundancyPwStatusNotifyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedundancyPwStatusNotifyEnable (INT4
                                               *pi4RetValFsL2VpnPwRedundancyPwStatusNotifyEnable)
{
    *pi4RetValFsL2VpnPwRedundancyPwStatusNotifyEnable =
        gL2vpnRgGlobals.b1PwStatusNotifEnable;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedundancyStatus
 Input       :  The Indices

                The Object 
                setValFsL2VpnPwRedundancyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedundancyStatus (INT4 i4SetValFsL2VpnPwRedundancyStatus)
{
    if (i4SetValFsL2VpnPwRedundancyStatus == L2VPN_PWRED_STATUS)
    {
        return SNMP_SUCCESS;
    }

    L2VPN_PWRED_STATUS = (UINT1) i4SetValFsL2VpnPwRedundancyStatus;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_ENABLED)
    {
        L2VpnRedundancyEnable ();
    }
    else if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VpnRedundancyDisable ();
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedNegotiationTimeOut
 Input       :  The Indices

                The Object 
                setValFsL2VpnPwRedNegotiationTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedNegotiationTimeOut (UINT4
                                      u4SetValFsL2VpnPwRedNegotiationTimeOut)
{
    gL2vpnRgGlobals.u4NegotiationTimeout =
        u4SetValFsL2VpnPwRedNegotiationTimeOut;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedundancySyncFailNotifyEnable
 Input       :  The Indices

                The Object 
                setValFsL2VpnPwRedundancySyncFailNotifyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedundancySyncFailNotifyEnable (INT4
                                               i4SetValFsL2VpnPwRedundancySyncFailNotifyEnable)
{
    gL2vpnRgGlobals.b1SyncFailNotifEnable = (BOOL1)
        i4SetValFsL2VpnPwRedundancySyncFailNotifyEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedundancyPwStatusNotifyEnable
 Input       :  The Indices

                The Object 
                setValFsL2VpnPwRedundancyPwStatusNotifyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedundancyPwStatusNotifyEnable (INT4
                                               i4SetValFsL2VpnPwRedundancyPwStatusNotifyEnable)
{
    gL2vpnRgGlobals.b1PwStatusNotifEnable = (BOOL1)
        i4SetValFsL2VpnPwRedundancyPwStatusNotifyEnable;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedundancyStatus
 Input       :  The Indices

                The Object 
                testValFsL2VpnPwRedundancyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedundancyStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsL2VpnPwRedundancyStatus)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedundancyStatus != L2VPN_PWRED_DISABLED) &&
        (i4TestValFsL2VpnPwRedundancyStatus != L2VPN_PWRED_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedNegotiationTimeOut
 Input       :  The Indices

                The Object 
                testValFsL2VpnPwRedNegotiationTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedNegotiationTimeOut (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4TestValFsL2VpnPwRedNegotiationTimeOut)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW Redundancy  is Disabled\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsL2VpnPwRedNegotiationTimeOut > 65535)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Value cannot be greater than 65535\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedundancySyncFailNotifyEnable
 Input       :  The Indices

                The Object 
                testValFsL2VpnPwRedundancySyncFailNotifyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedundancySyncFailNotifyEnable (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4TestValFsL2VpnPwRedundancySyncFailNotifyEnable)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW Redundancy  is Disabled\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsL2VpnPwRedundancySyncFailNotifyEnable != TRUE) &&
        (i4TestValFsL2VpnPwRedundancySyncFailNotifyEnable != FALSE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Value is not correct for SyncNotifyEnable\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedundancyPwStatusNotifyEnable
 Input       :  The Indices

                The Object 
                testValFsL2VpnPwRedundancyPwStatusNotifyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedundancyPwStatusNotifyEnable (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4TestValFsL2VpnPwRedundancyPwStatusNotifyEnable)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW Redundancy  is Disabled\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsL2VpnPwRedundancyPwStatusNotifyEnable != TRUE) &&
        (i4TestValFsL2VpnPwRedundancyPwStatusNotifyEnable != FALSE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Value is not correct for SyncNotifyEnable\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedundancyStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedundancyStatus (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedNegotiationTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedNegotiationTimeOut (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedundancySyncFailNotifyEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedundancySyncFailNotifyEnable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedundancyPwStatusNotifyEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedundancyPwStatusNotifyEnable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsL2VpnPwRedGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsL2VpnPwRedGroupTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsL2VpnPwRedGroupTable (UINT4 u4FsL2VpnPwRedGroupIndex)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW Redundancy  is Disabled\r\n");
        return SNMP_FAILURE;
    }

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsL2VpnPwRedGroupTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsL2VpnPwRedGroupTable (UINT4 *pu4FsL2VpnPwRedGroupIndex)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW Redundancy  is Disabled\r\n");
        return SNMP_FAILURE;
    }

    *pu4FsL2VpnPwRedGroupIndex = 0;

    pRgGroup = RBTreeGetFirst (gL2vpnRgGlobals.RgList);

    if (pRgGroup != NULL)
    {
        *pu4FsL2VpnPwRedGroupIndex = pRgGroup->u4Index;
        return (SNMP_SUCCESS);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsL2VpnPwRedGroupTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                nextFsL2VpnPwRedGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsL2VpnPwRedGroupTable (UINT4 u4FsL2VpnPwRedGroupIndex,
                                       UINT4 *pu4NextFsL2VpnPwRedGroupIndex)
{
    tL2vpnRedundancyEntry *pNextRgGroup = NULL;
    tL2vpnRedundancyEntry RgGroup;

    MEMSET (&RgGroup, 0, sizeof (tL2vpnRedundancyEntry));

    RgGroup.u4Index = u4FsL2VpnPwRedGroupIndex;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW Redundancy  is Disabled\r\n");
        return SNMP_FAILURE;
    }

    pNextRgGroup = RBTreeGetNext (gL2vpnRgGlobals.RgList,
                                  (tRBElem *) & RgGroup, NULL);

    if (pNextRgGroup != NULL)
    {
        *pu4NextFsL2VpnPwRedGroupIndex = pNextRgGroup->u4Index;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupProtType
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupProtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupProtType (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 *pi4RetValFsL2VpnPwRedGroupProtType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupProtType = pRgGroup->u1Mode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupReversionType
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupReversionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupReversionType (UINT4 u4FsL2VpnPwRedGroupIndex,
                                      INT4
                                      *pi4RetValFsL2VpnPwRedGroupReversionType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupReversionType = pRgGroup->b1ReversionEnable;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupContentionResolutionMethod
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupContentionResolutionMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupContentionResolutionMethod (UINT4
                                                   u4FsL2VpnPwRedGroupIndex,
                                                   INT4
                                                   *pi4RetValFsL2VpnPwRedGroupContentionResolutionMethod)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupContentionResolutionMethod =
            pRgGroup->u1ContentionResolutionMethod;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupLockoutProtection
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupLockoutProtection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupLockoutProtection (UINT4
                                          u4FsL2VpnPwRedGroupIndex,
                                          INT4
                                          *pi4RetValFsL2VpnPwRedGroupLockoutProtection)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupLockoutProtection =
            pRgGroup->u1LockoutProtect;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupMasterSlaveMode
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupMasterSlaveMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupMasterSlaveMode (UINT4 u4FsL2VpnPwRedGroupIndex,
                                        INT4
                                        *pi4RetValFsL2VpnPwRedGroupMasterSlaveMode)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupMasterSlaveMode =
            pRgGroup->u1MasterSlaveMode;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupDualHomeApps
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupDualHomeApps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupDualHomeApps (UINT4 u4FsL2VpnPwRedGroupIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsL2VpnPwRedGroupDualHomeApps)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    (*pRetValFsL2VpnPwRedGroupDualHomeApps->pu1_OctetList)
        &= L2VPNRED_STATUS_DEFAULT;
    if (pRgGroup != NULL)
    {
        pRetValFsL2VpnPwRedGroupDualHomeApps->i4_Length =
            sizeof (pRgGroup->u1MultiHomingApps);
        pRetValFsL2VpnPwRedGroupDualHomeApps->pu1_OctetList[0] =
            pRgGroup->u1MultiHomingApps;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupName
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupName (UINT4 u4FsL2VpnPwRedGroupIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsL2VpnPwRedGroupName)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        MEMCPY (pRetValFsL2VpnPwRedGroupName->pu1_OctetList,
                pRgGroup->au1RgGrpName, STRLEN (pRgGroup->au1RgGrpName));
        pRetValFsL2VpnPwRedGroupName->i4_Length = (INT4)
            STRLEN (pRgGroup->au1RgGrpName);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsL2VpnPwRedGroupStatus)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRetValFsL2VpnPwRedGroupStatus->i4_Length = 0;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsL2VpnPwRedGroupStatus->i4_Length = 1;

    pRetValFsL2VpnPwRedGroupStatus->pu1_OctetList[0] =
        (UINT1) (pRgGroup->u1Status & L2VPNRED_STATUS_FORWARDING_NEGOTIATON ? 0x80 : 0x00) | 
        (pRgGroup->u1Status & L2VPNRED_STATUS_SWITCHOVER_NEGOTIATON ? 0x40 : 0x00) |
        (pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS ? 0x20 :  0x00) |
        (pRgGroup->u1Status & L2VPNRED_STATUS_WAITING_TO_RESTORE ?  0x08 : 0x00) |
        (pRgGroup->u1Status & (UINT1) L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE ? 0x01 : 0x00);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupOperActivePw
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupOperActivePw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupOperActivePw (UINT4 u4FsL2VpnPwRedGroupIndex,
                                     UINT4
                                     *pu4RetValFsL2VpnPwRedGroupOperActivePw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    *pu4RetValFsL2VpnPwRedGroupOperActivePw = 0;

    if (pRgGroup != NULL)
    {
        if ((pRgGroup->pOperActivePw != NULL) &&
            (pRgGroup->pOperActivePw->pRgParent != NULL) &&
            (((tL2vpnRedundancyPwEntry *) (VOID *)
              (pRgGroup->pOperActivePw->pRgParent))->pPwVcEntry != NULL))
        {
            *pu4RetValFsL2VpnPwRedGroupOperActivePw =
                L2VPN_PWVC_INDEX (L2VPNRED_ICCP_PW_PARENT_PW
                                  (pRgGroup->pOperActivePw)->pPwVcEntry);
        }

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupWtrTimer
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupWtrTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupWtrTimer (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 UINT4 *pu4RetValFsL2VpnPwRedGroupWtrTimer)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pu4RetValFsL2VpnPwRedGroupWtrTimer = pRgGroup->u2WaitToRestoreTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupAdminCmd
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupAdminCmd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupAdminCmd (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 *pi4RetValFsL2VpnPwRedGroupAdminCmd)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupAdminCmd = 222222;    /* To be filled */
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupAdminActivePw
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupAdminActivePw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupAdminActivePw (UINT4 u4FsL2VpnPwRedGroupIndex,
                                      UINT4
                                      *pu4RetValFsL2VpnPwRedGroupAdminActivePw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pu4RetValFsL2VpnPwRedGroupAdminActivePw = pRgGroup->u4AdminActivePw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupAdminCmdStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupAdminCmdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupAdminCmdStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                       INT4
                                       *pi4RetValFsL2VpnPwRedGroupAdminCmdStatus)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupAdminCmdStatus = pRgGroup->u1Status;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedGroupRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                retValFsL2VpnPwRedGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedGroupRowStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                  INT4 *pi4RetValFsL2VpnPwRedGroupRowStatus)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        *pi4RetValFsL2VpnPwRedGroupRowStatus = pRgGroup->u1RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupProtType
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupProtType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupProtType (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 i4SetValFsL2VpnPwRedGroupProtType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u1Mode = (UINT1) i4SetValFsL2VpnPwRedGroupProtType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupReversionType
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupReversionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupReversionType (UINT4 u4FsL2VpnPwRedGroupIndex,
                                      INT4
                                      i4SetValFsL2VpnPwRedGroupReversionType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->b1ReversionEnable =
            (BOOL1) i4SetValFsL2VpnPwRedGroupReversionType;
        if (pRgGroup->u2WaitToRestoreTime == L2VPN_ZERO)
        {
            pRgGroup->u2WaitToRestoreTime = L2VPN_ONE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupContentionResolutionMethod
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupContentionResolutionMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupContentionResolutionMethod (UINT4
                                                   u4FsL2VpnPwRedGroupIndex,
                                                   INT4
                                                   i4SetValFsL2VpnPwRedGroupContentionResolutionMethod)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u1ContentionResolutionMethod = (UINT1)
            i4SetValFsL2VpnPwRedGroupContentionResolutionMethod;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupLockoutProtection
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupLockoutProtection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupLockoutProtection (UINT4 u4FsL2VpnPwRedGroupIndex,
                                          INT4  i4SetValFsL2VpnPwRedGroupLockoutProtection)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u1LockoutProtect = (UINT1)
            i4SetValFsL2VpnPwRedGroupLockoutProtection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}


/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupMasterSlaveMode
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupMasterSlaveMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupMasterSlaveMode (UINT4 u4FsL2VpnPwRedGroupIndex,
                                        INT4
                                        i4SetValFsL2VpnPwRedGroupMasterSlaveMode)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u1MasterSlaveMode =
            (UINT1) i4SetValFsL2VpnPwRedGroupMasterSlaveMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupDualHomeApps
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupDualHomeApps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupDualHomeApps (UINT4 u4FsL2VpnPwRedGroupIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsL2VpnPwRedGroupDualHomeApps)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u1MultiHomingApps &= L2VPNRED_MULTIHOMINGAPP_DEFAULT;
        pRgGroup->u1MultiHomingApps |=
            pSetValFsL2VpnPwRedGroupDualHomeApps->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupName
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupName (UINT4 u4FsL2VpnPwRedGroupIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsL2VpnPwRedGroupName)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        MEMSET (pRgGroup->au1RgGrpName, 0, STRLEN (pRgGroup->au1RgGrpName));
        MEMCPY (pRgGroup->au1RgGrpName,
                pSetValFsL2VpnPwRedGroupName->pu1_OctetList,
                pSetValFsL2VpnPwRedGroupName->i4_Length);
        pRgGroup->au1RgGrpName[pSetValFsL2VpnPwRedGroupName->i4_Length] = '\0';

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupWtrTimer
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupWtrTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupWtrTimer (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 UINT4 u4SetValFsL2VpnPwRedGroupWtrTimer)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u2WaitToRestoreTime =
            (UINT2) u4SetValFsL2VpnPwRedGroupWtrTimer;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupAdminCmd
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupAdminCmd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupAdminCmd (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 i4SetValFsL2VpnPwRedGroupAdminCmd)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup == NULL ||
        pRgGroup->u4AdminActivePw == 0 ||
        i4SetValFsL2VpnPwRedGroupAdminCmd != L2VPN_PWRED_MANUAL_SWITCHOVER_CMD)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (L2VpnAdminEvtInfo));
    L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (&L2VpnAdminEvtInfo) =
        L2VPNRED_EVENT_PW_SWITCHOVER_LOCAL;
    L2VPN_ADMIN_EVT_RED_PW_GROUP (&L2VpnAdminEvtInfo) =
        u4FsL2VpnPwRedGroupIndex;
    L2VPN_ADMIN_EVT_RED_PW_INDEX (&L2VpnAdminEvtInfo) =
        pRgGroup->u4AdminActivePw;
    L2VpnAdminEventHandler (&L2VpnAdminEvtInfo, L2VPN_RED_PW_ADMIN_EVENT);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupAdminActivePw
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupAdminActivePw
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupAdminActivePw (UINT4 u4FsL2VpnPwRedGroupIndex,
                                      UINT4
                                      u4SetValFsL2VpnPwRedGroupAdminActivePw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup != NULL)
    {
        pRgGroup->u4AdminActivePw = u4SetValFsL2VpnPwRedGroupAdminActivePw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedGroupRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                setValFsL2VpnPwRedGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedGroupRowStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                  INT4 i4SetValFsL2VpnPwRedGroupRowStatus)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (L2VpnAdminEvtInfo));
    L2VPN_ADMIN_EVT_RED_GROUP_INDEX (&L2VpnAdminEvtInfo) =
        u4FsL2VpnPwRedGroupIndex;

    switch (i4SetValFsL2VpnPwRedGroupRowStatus)
    {
        case ACTIVE:
            if (pRgGroup == NULL)
            {
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_RG_ACTIVATE;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_GROUP_ADMIN_EVENT);
            break;

        case NOT_IN_SERVICE:
            if (pRgGroup == NULL)
            {
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_RG_DEACTIVATE;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_GROUP_ADMIN_EVENT);
            break;

        case CREATE_AND_WAIT:
            if (pRgGroup != NULL)
            {
                return SNMP_FAILURE;
            }
            pRgGroup = L2vpnRedundancyGroupCreate (u4FsL2VpnPwRedGroupIndex);
            if (pRgGroup == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pRgGroup == NULL)
            {
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_RG_DESTROY;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_GROUP_ADMIN_EVENT);
            break;

        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupProtType
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupProtType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupProtType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsL2VpnPwRedGroupIndex,
                                    INT4 i4TestValFsL2VpnPwRedGroupProtType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "the redundancy group entry is"
                   "not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedGroupProtType != L2VPNRED_MODE_ONEISTON) &&
        (i4TestValFsL2VpnPwRedGroupProtType != L2VPNRED_MODE_SWITCH))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_PROTECTION_TYPE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "wrong protection type value\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupReversionType
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupReversionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupReversionType (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsL2VpnPwRedGroupIndex,
                                         INT4
                                         i4TestValFsL2VpnPwRedGroupReversionType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Check the RG rowstatus is not ACTIVE */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the "
                   "RG group is active\n");
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedGroupReversionType != L2VPN_PWRED_MODE_REVERTIVE)
        && (i4TestValFsL2VpnPwRedGroupReversionType !=
            L2VPN_PWRED_MODE_NON_REVERTIVE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "wrong reversion type value\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_REVERTIVE_TYPE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupContentionResolutionMethod
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupContentionResolutionMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupContentionResolutionMethod (UINT4 *pu4ErrorCode,
                                                      UINT4
                                                      u4FsL2VpnPwRedGroupIndex,
                                                      INT4
                                                      i4TestValFsL2VpnPwRedGroupContentionResolutionMethod)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "the redundancy group entry is not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedGroupContentionResolutionMethod !=
         L2VPNRED_CONTENTION_INDEPENDENT) &&
        (i4TestValFsL2VpnPwRedGroupContentionResolutionMethod !=
         L2VPNRED_CONTENTION_MASTERSLAVE))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_CONTENTION_RES_METHOD);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "wrong contention resolution method."
                   "It can be either independent or master-slave\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupLockoutProtection
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupLockoutProtection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupLockoutProtection (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4FsL2VpnPwRedGroupIndex,
                                             INT4
                                             i4TestValFsL2VpnPwRedGroupLockoutProtection)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "the redundancy group entry is not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedGroupLockoutProtection !=
         L2VPNRED_LOCKOUT_ENABLE) &&
        (i4TestValFsL2VpnPwRedGroupLockoutProtection !=
         L2VPNRED_LOCKOUT_DISABLE))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_LOCKOUT_PROTECTION_FAILURE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "wrong lockout protection value"
                   "It can either be enabled or disabled\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupMasterSlaveMode
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupMasterSlaveMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupMasterSlaveMode (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsL2VpnPwRedGroupIndex,
                                           INT4
                                           i4TestValFsL2VpnPwRedGroupMasterSlaveMode)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "the redundancy group entry is not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedGroupMasterSlaveMode !=
         L2VPNRED_MASTERSLAVE_MASTER)
        && (i4TestValFsL2VpnPwRedGroupMasterSlaveMode !=
            L2VPNRED_MASTERSLAVE_SLAVE))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_MASTER_SLAVE_MODE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mode can be either master or slave\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupDualHomeApps
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupDualHomeApps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupDualHomeApps (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsL2VpnPwRedGroupIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsL2VpnPwRedGroupDualHomeApps)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "the redundancy group entry is not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsL2VpnPwRedGroupDualHomeApps->i4_Length != 0 &&
        pTestValFsL2VpnPwRedGroupDualHomeApps->pu1_OctetList[0] &
        ~L2VPNRED_MULTIHOMINGAPP_LAGG)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_DUAL_HOMS_VALUE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Currently only LAGG is supported\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupName
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupName (UINT4 *pu4ErrorCode,
                                UINT4 u4FsL2VpnPwRedGroupIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsL2VpnPwRedGroupName)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    UINT4               u4PwRgIndex = 0;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "the redundancy group entry"
                   "is not created\n");
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsL2VpnPwRedGroupName->i4_Length < 0) ||
        (pTestValFsL2VpnPwRedGroupName->i4_Length >
         L2VPN_PWRED_GRP_NAME_LEN - 1))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_RG_NAME_LEN);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "The length of name cannot be more"
                   "than 32 characters\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u4PwRgIndex = L2VpnUtilGetRedGrpIdFromRedGrpName
        (pTestValFsL2VpnPwRedGroupName->pu1_OctetList);

    /* Check to see if the RG name is already assoicated with a RG or not */
    if ((u4PwRgIndex != 0) && (u4PwRgIndex != u4FsL2VpnPwRedGroupIndex))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NAME_IN_USE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy group name is already in use\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupWtrTimer
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupWtrTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupWtrTimer (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsL2VpnPwRedGroupIndex,
                                    UINT4 u4TestValFsL2VpnPwRedGroupWtrTimer)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    /* check if the RG group exists or not */
    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsL2VpnPwRedGroupWtrTimer < L2VPN_PWRED_GRP_MIN_WTR_VAL) ||
        (u4TestValFsL2VpnPwRedGroupWtrTimer > L2VPN_PWRED_GRP_MAX_WTR_VAL))
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_WTR_VALUE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pRgGroup->b1ReversionEnable != L2VPN_PWRED_MODE_REVERTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_NON_REVERTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupAdminCmd
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupAdminCmd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupAdminCmd (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsL2VpnPwRedGroupIndex,
                                    INT4 i4TestValFsL2VpnPwRedGroupAdminCmd)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    /* check if the RG group exists or not */
    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The redundancy group entry is not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus != ACTIVE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check Admin command */
    if (i4TestValFsL2VpnPwRedGroupAdminCmd != L2VPN_PWRED_MANUAL_SWITCHOVER_CMD)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Wrong RG Admin command(%d)\n",
                    i4TestValFsL2VpnPwRedGroupAdminCmd);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupAdminActivePw
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupAdminActivePw
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupAdminActivePw (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsL2VpnPwRedGroupIndex,
                                         UINT4
                                         u4TestValFsL2VpnPwRedGroupAdminActivePw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyPwEntry *pRgPwEntry = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    /* check if the RG group exists or not */
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The redundancy group entry is not created\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check the RG rowstatus is ACTIVE or not */
    if (pRgGroup->u1RowStatus != ACTIVE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_STATUS_ACTIVE);
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire cannot be made up as the RG group is not active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if the pseudowire is associated with this RG group or not */
    pRgPwEntry = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                   u4TestValFsL2VpnPwRedGroupAdminActivePw);

    if (pRgPwEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire is not associated with the redundant group\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check if the pseudowire is associated with the ICCP Entry or not */
    if (pRgPwEntry->pIccpPwEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "The ICCP entry is NULL \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* The pseudowire can be a local or remote entry. But the adminactive
       can be issued only to the local pseudowire */
    if (pRgPwEntry->pIccpPwEntry->b1IsLocal != TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The pseudowire is not a local pseudowire\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedGroupRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex

                The Object 
                testValFsL2VpnPwRedGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedGroupRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsL2VpnPwRedGroupIndex,
                                     INT4 i4TestValFsL2VpnPwRedGroupRowStatus)
{
    UINT1               u1AddrType = 0;
    UINT4               u4PwIndex = 0;
    uGenAddr            ipAddr;
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    MEMSET (&ipAddr, 0, sizeof (uGenAddr));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWRED_STATUS != L2VPN_PWRED_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Pseudowire redundancy is not gobally enabled\n");
        return SNMP_FAILURE;
    }

    /* check if the RG group exists or not */
    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if ((pRgGroup == NULL) &&
        ((i4TestValFsL2VpnPwRedGroupRowStatus == ACTIVE) ||
         (i4TestValFsL2VpnPwRedGroupRowStatus == DESTROY) ||
         (i4TestValFsL2VpnPwRedGroupRowStatus == NOT_IN_SERVICE)))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "The proctection group %u is not created\n",
                    u4FsL2VpnPwRedGroupIndex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4FsL2VpnPwRedGroupIndex == 0)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Invalid protection group Index %u\r\n",
                    u4FsL2VpnPwRedGroupIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsL2VpnPwRedGroupRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pRgGroup != NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "The proctection group %u already exists\n",
                            u4FsL2VpnPwRedGroupIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
        case NOT_READY:
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "This Row status is not supported\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            if (NULL != L2VpnUtilGetNextRedundancyPwEntryByIndex
                (u4FsL2VpnPwRedGroupIndex, u4PwIndex))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "The pseudowire entry associated with this group is still not deleted\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (NULL != L2VpnUtilGetNextRedundancyNodeEntryByIndex
                (u4FsL2VpnPwRedGroupIndex, u1AddrType, ipAddr))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "The Node entry associated with this group is still not deleted\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case ACTIVE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedGroupTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedGroupTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsL2VpnPwRedNodeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsL2VpnPwRedNodeTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsL2VpnPwRedNodeTable (UINT4 u4FsL2VpnPwRedGroupIndex,
                                               INT4 i4FsL2VpnPwRedNodeAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsL2VpnPwRedNodeAddr)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    uGenAddr            Addr;

    MEMSET (&Addr, 0, sizeof (uGenAddr));

    MEMCPY (&Addr, pFsL2VpnPwRedNodeAddr->pu1_OctetList, 4);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "The RG Entry is not created \n");
        return SNMP_FAILURE;
    }

/* TODO */
    pRgNode = L2VpnGetRedundancyNodeEntryByIndex
        (u4FsL2VpnPwRedGroupIndex, (UINT1) i4FsL2VpnPwRedNodeAddrType, &Addr);
    if (pRgNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "The RG Entry is not created \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsL2VpnPwRedNodeTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsL2VpnPwRedNodeTable (UINT4 *pu4FsL2VpnPwRedGroupIndex,
                                       INT4 *pi4FsL2VpnPwRedNodeAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsL2VpnPwRedNodeAddr)
{

    tL2vpnRedundancyNodeEntry *pNodeRb = NULL;

    *pu4FsL2VpnPwRedGroupIndex = 0;
    *pi4FsL2VpnPwRedNodeAddrType = 0;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\nSNMP_FAILURE:"
                "Pseudowire-Redundancy is Disabled\n");
        return SNMP_FAILURE;
    }

    pNodeRb = RBTreeGetFirst (gL2vpnRgGlobals.RgNodeList);

    if (pNodeRb != NULL)
    {
        *pi4FsL2VpnPwRedNodeAddrType = pNodeRb->u1AddrType;
        *pu4FsL2VpnPwRedGroupIndex = pNodeRb->u4RgIndex;
        pFsL2VpnPwRedNodeAddr->i4_Length = IPV4_ADDR_LENGTH;
        MEMCPY (pFsL2VpnPwRedNodeAddr->pu1_OctetList, &pNodeRb->Addr,
                pFsL2VpnPwRedNodeAddr->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsL2VpnPwRedNodeTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                nextFsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                nextFsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr
                nextFsL2VpnPwRedNodeAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsL2VpnPwRedNodeTable (UINT4 u4FsL2VpnPwRedGroupIndex,
                                      UINT4 *pu4NextFsL2VpnPwRedGroupIndex,
                                      INT4 i4FsL2VpnPwRedNodeAddrType,
                                      INT4 *pi4NextFsL2VpnPwRedNodeAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsL2VpnPwRedNodeAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFsL2VpnPwRedNodeAddr)
{
    tL2vpnRedundancyNodeEntry *pNodeRb = NULL;
    uGenAddr            IpAddr;

    MEMSET (&IpAddr, 0, sizeof (uGenAddr));
    MEMCPY (&IpAddr, pFsL2VpnPwRedNodeAddr->pu1_OctetList,
            pFsL2VpnPwRedNodeAddr->i4_Length);

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\nSNMP_FAILURE:"
                "Pseudowire-Redundancy is Disabled\n");
        return SNMP_FAILURE;
    }

    pNodeRb =
        L2VpnUtilGetNextRedundancyNodeEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                    (UINT1)
                                                    i4FsL2VpnPwRedNodeAddrType,
                                                    IpAddr);

    if (pNodeRb != NULL)
    {
        *pu4NextFsL2VpnPwRedGroupIndex = pNodeRb->u4RgIndex;
        *pi4NextFsL2VpnPwRedNodeAddrType = pNodeRb->u1AddrType;

        pNextFsL2VpnPwRedNodeAddr->i4_Length = IPV4_ADDR_LENGTH;

        MEMCPY (pNextFsL2VpnPwRedNodeAddr->pu1_OctetList, &pNodeRb->Addr,
                pFsL2VpnPwRedNodeAddr->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedNodeLocalLdpID
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                retValFsL2VpnPwRedNodeLocalLdpID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedNodeLocalLdpID (UINT4 u4FsL2VpnPwRedGroupIndex,
                                  INT4 i4FsL2VpnPwRedNodeAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsL2VpnPwRedNodeAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsL2VpnPwRedNodeLocalLdpID)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    uGenAddr Addr;

    MEMSET(&Addr,0,sizeof(uGenAddr));

    pRetValFsL2VpnPwRedNodeLocalLdpID->i4_Length = 0;
    MEMCPY(&Addr,pFsL2VpnPwRedNodeAddr->pu1_OctetList,sizeof(uGenAddr));

    pRgNode = L2VpnGetRedundancyNodeEntryByIndex
        (u4FsL2VpnPwRedGroupIndex, (UINT1) i4FsL2VpnPwRedNodeAddrType,
         &Addr);

    if (pRgNode == NULL || pRgNode->pSession == NULL)
    {
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (pRgNode->pSession->u4LocalLdpEntityID,
                                 pRetValFsL2VpnPwRedNodeLocalLdpID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedNodeLocalLdpEntityIndex
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                retValFsL2VpnPwRedNodeLocalLdpEntityIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedNodeLocalLdpEntityIndex (UINT4 u4FsL2VpnPwRedGroupIndex,
                                           INT4 i4FsL2VpnPwRedNodeAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsL2VpnPwRedNodeAddr,
                                           UINT4
                                           *pu4RetValFsL2VpnPwRedNodeLocalLdpEntityIndex)
{
	tL2vpnRedundancyNodeEntry *pRgNode = NULL;
	uGenAddr Addr;

	MEMSET(&Addr,0,sizeof(uGenAddr));
        MEMCPY(&Addr,pFsL2VpnPwRedNodeAddr->pu1_OctetList,sizeof(uGenAddr));

	pRgNode = L2VpnGetRedundancyNodeEntryByIndex
		(u4FsL2VpnPwRedGroupIndex, (UINT1) i4FsL2VpnPwRedNodeAddrType,
		 &Addr);

	if (pRgNode == NULL || pRgNode->pSession == NULL)
	{
		return SNMP_FAILURE;
	}

	*pu4RetValFsL2VpnPwRedNodeLocalLdpEntityIndex =
		pRgNode->pSession->u4LdpEntityIndex;

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedNodePeerLdpID
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                retValFsL2VpnPwRedNodePeerLdpID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedNodePeerLdpID (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 i4FsL2VpnPwRedNodeAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsL2VpnPwRedNodeAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsL2VpnPwRedNodePeerLdpID)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    uGenAddr Addr;

    MEMSET(&Addr,0,sizeof(uGenAddr));

    pRetValFsL2VpnPwRedNodePeerLdpID->i4_Length = 0;

    MEMCPY(&Addr, pFsL2VpnPwRedNodeAddr->pu1_OctetList,sizeof(uGenAddr));

    pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                  (UINT1)
                                                  i4FsL2VpnPwRedNodeAddrType,
                                                  &Addr);

    if (pRgNode == NULL || pRgNode->pSession == NULL)
    {
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (pRgNode->pSession->u4PeerLdpId,
                                 pRetValFsL2VpnPwRedNodePeerLdpID);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedNodeStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                retValFsL2VpnPwRedNodeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedNodeStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                              INT4 i4FsL2VpnPwRedNodeAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsL2VpnPwRedNodeAddr,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsL2VpnPwRedNodeStatus)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    uGenAddr            IpAddr;

    MEMSET (&IpAddr, 0, sizeof (uGenAddr));
    MEMCPY (&IpAddr, pFsL2VpnPwRedNodeAddr->pu1_OctetList,
            pFsL2VpnPwRedNodeAddr->i4_Length);
    pRetValFsL2VpnPwRedNodeStatus->i4_Length = 0;

    pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                  (UINT1)
                                                  i4FsL2VpnPwRedNodeAddrType,
                                                  &IpAddr);

    if (NULL == pRgNode)
    {
        return SNMP_FAILURE;
    }

    pRetValFsL2VpnPwRedNodeStatus->i4_Length = 1;

    pRetValFsL2VpnPwRedNodeStatus->pu1_OctetList[0] =
        (UINT1)(pRgNode->u1Status & L2VPNRED_NODE_STATUS_CONNECTED ? 0x80 : 0x00) |
        (UINT1)(pRgNode->u1Status & L2VPNRED_NODE_STATUS_LOCAL_SYNC ? 0x40 : 0x00);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedNodeRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                retValFsL2VpnPwRedNodeRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedNodeRowStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 i4FsL2VpnPwRedNodeAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsL2VpnPwRedNodeAddr,
                                 INT4 *pi4RetValFsL2VpnPwRedNodeRowStatus)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    uGenAddr            IpAddr;

    MEMSET (&IpAddr, 0, sizeof (uGenAddr));
    *pi4RetValFsL2VpnPwRedNodeRowStatus = 0;

    MEMCPY (&IpAddr, pFsL2VpnPwRedNodeAddr->pu1_OctetList,
            pFsL2VpnPwRedNodeAddr->i4_Length);

    pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                  (UINT1)
                                                  i4FsL2VpnPwRedNodeAddrType,
                                                  &IpAddr);

    if (NULL != pRgNode)
    {
        *pi4RetValFsL2VpnPwRedNodeRowStatus = pRgNode->u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedNodeRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                setValFsL2VpnPwRedNodeRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedNodeRowStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 INT4 i4FsL2VpnPwRedNodeAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsL2VpnPwRedNodeAddr,
                                 INT4 i4SetValFsL2VpnPwRedNodeRowStatus)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;

    uGenAddr           Addr;
   
    MEMSET(&Addr,0,sizeof(uGenAddr));
    MEMCPY(&Addr, pFsL2VpnPwRedNodeAddr->pu1_OctetList,sizeof(uGenAddr));

    pRgNode =
	    L2VpnGetRedundancyNodeEntryByIndex (u4FsL2VpnPwRedGroupIndex,
			    (UINT1) i4FsL2VpnPwRedNodeAddrType,
			    &Addr);

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (L2VpnAdminEvtInfo));
    L2VPN_ADMIN_EVT_RED_NODE_GROUP (&L2VpnAdminEvtInfo) = (UINT1)
        u4FsL2VpnPwRedGroupIndex;
    L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE (&L2VpnAdminEvtInfo) = (UINT1)
        i4FsL2VpnPwRedNodeAddrType;
    MEMCPY(L2VPN_ADMIN_EVT_RED_NODE_ADDR (&L2VpnAdminEvtInfo),
		    &Addr,sizeof(uGenAddr));

    switch (i4SetValFsL2VpnPwRedNodeRowStatus)
    {
        case ACTIVE:
            if (pRgNode == NULL)
            {
                CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_NOT_EXIST);
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_NODE_ACTIVATE;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_NODE_ADMIN_EVENT);
            break;

        case NOT_IN_SERVICE:
            if (pRgNode == NULL)
            {
                CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_NOT_EXIST);
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_NODE_DEACTIVATE;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_NODE_ADMIN_EVENT);
            break;

        case CREATE_AND_WAIT:
            if (pRgNode != NULL)
            {
                CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_EXIST);
                return SNMP_FAILURE;
            }
            L2vpnRedundancyNodeCreate
                (u4FsL2VpnPwRedGroupIndex, (UINT1) i4FsL2VpnPwRedNodeAddrType,
                 &Addr);
            break;

        case DESTROY:
            if (pRgNode == NULL)
            {
                CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_NOT_EXIST);
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_NODE_DESTROY;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_NODE_ADMIN_EVENT);
            break;

        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedNodeRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr

                The Object 
                testValFsL2VpnPwRedNodeRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
	    		SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedNodeRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsL2VpnPwRedGroupIndex,
                                    INT4 i4FsL2VpnPwRedNodeAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsL2VpnPwRedNodeAddr,
                                    INT4 i4TestValFsL2VpnPwRedNodeRowStatus)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    uGenAddr            Addr;
    uGenU4Addr         IPAddr;
    MEMSET (&IPAddr, 0, sizeof (uGenU4Addr));


    MEMSET(&Addr,0,sizeof(uGenAddr));
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWRED_STATUS != L2VPN_PWRED_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Pseudowire redundancy is not gobally enabled\n");
        return SNMP_FAILURE;
    }
    MEMCPY(&Addr, pFsL2VpnPwRedNodeAddr->pu1_OctetList,sizeof(uGenAddr));
    pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                  (UINT1)
                                                  i4FsL2VpnPwRedNodeAddrType,
                                                  &Addr);

    if ((pRgNode == NULL) &&
        ((i4TestValFsL2VpnPwRedNodeRowStatus == ACTIVE) ||
         (i4TestValFsL2VpnPwRedNodeRowStatus == DESTROY) ||
         (i4TestValFsL2VpnPwRedNodeRowStatus == NOT_IN_SERVICE)))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "The redundancy node is not created\n");
        CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsL2VpnPwRedNodeRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pRgNode != NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "The redundancy node entry already exists\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_NEIGH_EXIST);
                return SNMP_FAILURE;
            }
            pRgGroup =
                L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

            if (pRgGroup == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "The redundancy group %u doesn't exists\n",
                            u4FsL2VpnPwRedGroupIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((i4FsL2VpnPwRedNodeAddrType != IPV4) &&
                (i4FsL2VpnPwRedNodeAddrType != IPV6))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Invalid address type\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if (i4FsL2VpnPwRedNodeAddrType == IPV6)
            {
                MEMCPY (IPAddr.Ip6Addr.u1_addr,
                        pFsL2VpnPwRedNodeAddr->pu1_OctetList, IPV6_ADDR_LENGTH);
            }
            else if (i4FsL2VpnPwRedNodeAddrType == IPV4)
#endif
            {
                MEMCPY ((UINT1 *)&IPAddr.u4Addr,
                        pFsL2VpnPwRedNodeAddr->pu1_OctetList, IPV4_ADDR_LENGTH);
                IPAddr.u4Addr = OSIX_NTOHL (IPAddr.u4Addr);
            }    
                    if(((IPAddr.u4Addr == L2VPN_BCAST_ADDR)) ||
                    ((IPAddr.u4Addr >= L2VPN_MCAST_NET_ADDR) &&
                     (IPAddr.u4Addr <= L2VPN_MCAST_NET_INV_ADDR)))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

            return SNMP_SUCCESS;
        case CREATE_AND_GO:
        case NOT_READY:
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "This Row status is not supported\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
        case ACTIVE:
        case DESTROY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedNodeTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedNodeAddrType
                FsL2VpnPwRedNodeAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedNodeTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsL2VpnPwRedPwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsL2VpnPwRedPwTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsL2VpnPwRedPwTable (UINT4 u4FsL2VpnPwRedGroupIndex,
                                             UINT4 u4FsL2VpnPwRedPwIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Entry does not exist\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsL2VpnPwRedPwTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsL2VpnPwRedPwTable (UINT4 *pu4FsL2VpnPwRedGroupIndex,
                                     UINT4 *pu4FsL2VpnPwRedPwIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    *pu4FsL2VpnPwRedGroupIndex = 0;
    *pu4FsL2VpnPwRedPwIndex = 0;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\nSNMP_FAILURE:"
                "Pseudowire-Redundancy is Disabled\n");
        return SNMP_FAILURE;
    }

    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);

    if (pRgPw != NULL)
    {
        *pu4FsL2VpnPwRedGroupIndex = pRgPw->u4RgIndex;
        *pu4FsL2VpnPwRedPwIndex = pRgPw->u4PwIndex;
        return (SNMP_SUCCESS);
    }
    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Entry does not exist\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsL2VpnPwRedPwTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                nextFsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex
                nextFsL2VpnPwRedPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsL2VpnPwRedPwTable (UINT4 u4FsL2VpnPwRedGroupIndex,
                                    UINT4 *pu4NextFsL2VpnPwRedGroupIndex,
                                    UINT4 u4FsL2VpnPwRedPwIndex,
                                    UINT4 *pu4NextFsL2VpnPwRedPwIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    *pu4NextFsL2VpnPwRedGroupIndex = 0;
    *pu4NextFsL2VpnPwRedPwIndex = 0;

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\nSNMP_FAILURE:"
                "Pseudowire-Redundancy is Disabled\n");
        return SNMP_FAILURE;
    }

    pRgPw = L2VpnUtilGetNextRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                                      u4FsL2VpnPwRedPwIndex);

    if (NULL != pRgPw)
    {
        *pu4NextFsL2VpnPwRedGroupIndex = pRgPw->u4RgIndex;
        *pu4NextFsL2VpnPwRedPwIndex = pRgPw->u4PwIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedPwPreferance
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                retValFsL2VpnPwRedPwPreferance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedPwPreferance (UINT4 u4FsL2VpnPwRedGroupIndex,
                                UINT4 u4FsL2VpnPwRedPwIndex,
                                INT4 *pi4RetValFsL2VpnPwRedPwPreferance)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    *pi4RetValFsL2VpnPwRedPwPreferance = 0;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsL2VpnPwRedPwPreferance = pRgPw->u1Preference;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedPwLocalStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                retValFsL2VpnPwRedPwLocalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedPwLocalStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                 UINT4 u4FsL2VpnPwRedPwIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsL2VpnPwRedPwLocalStatus)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pRetValFsL2VpnPwRedPwLocalStatus->i4_Length = 0;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        return SNMP_FAILURE;
    }

    if (pRgPw->pIccpPwEntry != NULL)
    {
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] = 0;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING ?
            0x80 : 0x00;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT ?
            0x40 : 0x00;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT ?
            0x20 : 0x00;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT ?
            0x10 : 0x00;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT ?
            0x08 : 0x00;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_STANDBY ? 0x04 :
            0x00;
        pRetValFsL2VpnPwRedPwLocalStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4LocalStatus & L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST ?
            0x02 : 0x00;

        pRetValFsL2VpnPwRedPwLocalStatus->i4_Length = 1;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedPwRemoteStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                retValFsL2VpnPwRedPwRemoteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedPwRemoteStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                  UINT4 u4FsL2VpnPwRedPwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsL2VpnPwRedPwRemoteStatus)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pRetValFsL2VpnPwRedPwRemoteStatus->i4_Length = 0;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        return SNMP_FAILURE;
    }

    if (pRgPw->pIccpPwEntry != NULL)
    {
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] = 0;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING ?
            0x80 : 0x00;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT
            ? 0x40 : 0x00;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT
            ? 0x20 : 0x00;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT ?
            0x10 : 0x00;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT ?
            0x08 : 0x00;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_STANDBY ? 0x04 :
            0x00;
        pRetValFsL2VpnPwRedPwRemoteStatus->pu1_OctetList[0] |=
            (UINT1)pRgPw->
            pIccpPwEntry->u4RemoteStatus & L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST
            ? 0x02 : 0x00;

        pRetValFsL2VpnPwRedPwRemoteStatus->i4_Length = (UINT1)1;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedPwOperStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                retValFsL2VpnPwRedPwOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedPwOperStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                                UINT4 u4FsL2VpnPwRedPwIndex,
                                INT4 *pi4RetValFsL2VpnPwRedPwOperStatus)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL || pRgPw->pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy PW Entry does not exist\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsL2VpnPwRedPwOperStatus =
        L2VPN_PWVC_OPER_STATUS (pRgPw->pPwVcEntry) == L2VPN_PWVC_OPER_UP &&
        ((L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &
          L2VPN_PWVC_STATUS_STANDBY)
         || (L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &
             L2VPN_PWVC_STATUS_STANDBY)) ? L2VPN_PWVC_OPER_DORMANT :
        L2VPN_PWVC_OPER_STATUS (pRgPw->pPwVcEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedPwRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                retValFsL2VpnPwRedPwRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedPwRowStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                               UINT4 u4FsL2VpnPwRedPwIndex,
                               INT4 *pi4RetValFsL2VpnPwRedPwRowStatus)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    *pi4RetValFsL2VpnPwRedPwRowStatus = 0;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsL2VpnPwRedPwRowStatus = pRgPw->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedPwPreferance
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                setValFsL2VpnPwRedPwPreferance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedPwPreferance (UINT4 u4FsL2VpnPwRedGroupIndex,
                                UINT4 u4FsL2VpnPwRedPwIndex,
                                INT4 i4SetValFsL2VpnPwRedPwPreferance)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        return SNMP_FAILURE;
    }

    pRgPw->u1Preference = (UINT1) i4SetValFsL2VpnPwRedPwPreferance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedPwRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                setValFsL2VpnPwRedPwRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedPwRowStatus (UINT4 u4FsL2VpnPwRedGroupIndex,
                               UINT4 u4FsL2VpnPwRedPwIndex,
                               INT4 i4SetValFsL2VpnPwRedPwRowStatus)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4FsL2VpnPwRedPwIndex);

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (L2VpnAdminEvtInfo));
    L2VPN_ADMIN_EVT_RED_PW_GROUP (&L2VpnAdminEvtInfo) =
        u4FsL2VpnPwRedGroupIndex;
    L2VPN_ADMIN_EVT_RED_PW_INDEX (&L2VpnAdminEvtInfo) = u4FsL2VpnPwRedPwIndex;

    switch (i4SetValFsL2VpnPwRedPwRowStatus)
    {
        case ACTIVE:
            if ((pRgPw == NULL) || (pPwVcEntry == NULL))
            {
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_PW_ACTIVATE;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_PW_ADMIN_EVENT);
            break;

        case NOT_IN_SERVICE:
            if (pRgPw == NULL)
            {
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_PW_DEACTIVATE;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_PW_ADMIN_EVENT);
            break;

        case CREATE_AND_WAIT:
            if ((pRgPw != NULL) || (pPwVcEntry == NULL))
            {
                return SNMP_FAILURE;
            }
            pRgPw = L2vpnRedundancyPwCreate (u4FsL2VpnPwRedGroupIndex,
                                             pPwVcEntry);
            if (pRgPw == NULL)
            {
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pRgPw == NULL)
            {
                return SNMP_FAILURE;
            }

            L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (&L2VpnAdminEvtInfo) =
                L2VPNRED_EVENT_PW_DESTROY;
            L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                    L2VPN_RED_PW_ADMIN_EVENT);
            break;

        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedPwPreferance
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                testValFsL2VpnPwRedPwPreferance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedPwPreferance (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsL2VpnPwRedGroupIndex,
                                   UINT4 u4FsL2VpnPwRedPwIndex,
                                   INT4 i4TestValFsL2VpnPwRedPwPreferance)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyEntry *pRgGrp = NULL;
    tL2vpnRedundancyPwEntry *pTmpRgPw = NULL;
    UINT4               u4PwIndex = 0;

    pRgGrp = L2VpnGetRedundancyEntryByIndex (u4FsL2VpnPwRedGroupIndex);

    if (pRgGrp == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy Group Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4FsL2VpnPwRedGroupIndex,
                                              u4FsL2VpnPwRedPwIndex);

    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pRgPw->u1RowStatus == ACTIVE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Preference cannot be changed when the rowstaus is ACTIVE\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pRgGrp->u2NumPw > 0)
    {
        do
        {
            pTmpRgPw =
                L2VpnUtilGetNextRedundancyPwEntryByIndex
                (u4FsL2VpnPwRedGroupIndex, u4PwIndex);
            if (pTmpRgPw == NULL)
            {
                break;
            }
            if ((pTmpRgPw->u4PwIndex != u4FsL2VpnPwRedPwIndex) &&
                (i4TestValFsL2VpnPwRedPwPreferance ==
                 L2VPNRED_PW_PREFERENCE_PRIMARY)
                && (pTmpRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "Primary pseudowire is already available in this group\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            u4PwIndex = pTmpRgPw->u4PwIndex;
        }
        while (pTmpRgPw->u4RgIndex == u4FsL2VpnPwRedGroupIndex);
    }

    if ((i4TestValFsL2VpnPwRedPwPreferance != L2VPNRED_PW_PREFERENCE_PRIMARY) &&
        (i4TestValFsL2VpnPwRedPwPreferance != L2VPNRED_PW_PREFERENCE_SECONDARY))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Invalid Value for preference \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedPwRowStatus
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex

                The Object 
                testValFsL2VpnPwRedPwRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedPwRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsL2VpnPwRedGroupIndex,
                                  UINT4 u4FsL2VpnPwRedPwIndex,
                                  INT4 i4TestValFsL2VpnPwRedPwRowStatus)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2VPN module is SHUTDOWN\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWRED_STATUS != L2VPN_PWRED_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Pseudowire redundancy is not gobally enabled\n");
        return SNMP_FAILURE;
    }

    pRgPw = L2VpnGetRedundancyPwEntryByIndex
        (u4FsL2VpnPwRedGroupIndex, u4FsL2VpnPwRedPwIndex);

    if ((pRgPw == NULL) &&
        ((i4TestValFsL2VpnPwRedPwRowStatus == ACTIVE) ||
         (i4TestValFsL2VpnPwRedPwRowStatus == NOT_IN_SERVICE) ||
         (i4TestValFsL2VpnPwRedPwRowStatus == DESTROY)))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsL2VpnPwRedPwRowStatus == DESTROY) && (pRgPw != NULL) &&
        (pRgPw->u1RowStatus == ACTIVE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Redundancy pseudowire Entry cannot be deleted when it is active\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValFsL2VpnPwRedPwRowStatus)
    {
        case CREATE_AND_WAIT:
            /* Check if the redundant pseudowire entry if already exists */
            if (pRgPw != NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "Redundancy pseudowire Entry already exists\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* Check if redundat group exists */
            pRgGroup = L2VpnGetRedundancyEntryByIndex
                (u4FsL2VpnPwRedGroupIndex);

            if (pRgGroup == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "The redundancy group %u doesn't exists\n",
                            u4FsL2VpnPwRedGroupIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pRgGroup->u1Mode == L2VPNRED_MODE_SWITCH) &&
                (pRgGroup->u2NumPw > L2VPN_PWRED_MAXPW_FOR_SWITCH_MODE))
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "The number of PW that can be created for this mode "
                            "for this RG %u is created already",
                            u4FsL2VpnPwRedGroupIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;

            }
            /* Check if the pseudowire does not belongs to Manual one */
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4FsL2VpnPwRedPwIndex);

            if (pPwVcEntry == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "The pwVcEntry is not created for this pseudowire\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (L2VPN_PWVC_ID (pPwVcEntry) == L2VPN_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "Manual pseudowires cannot be mapped to a RG group\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            /* Check if the pseudowire is assocaited with some other group */
            if (L2VPN_FAILURE ==
                L2VpnUtilCheckPwExistence (u4FsL2VpnPwRedPwIndex,
                                           u4FsL2VpnPwRedGroupIndex))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (L2VPN_FAILURE ==
                L2VpnUtilCheckRedPwPeerAdd (u4FsL2VpnPwRedPwIndex,
                                            u4FsL2VpnPwRedGroupIndex))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case NOT_READY:
        case CREATE_AND_GO:
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "The row status are not supported \n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        case ACTIVE:
            if (pRgPw->pPwVcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "The pseudowire vcentry %u doesn't exist\n",
                            u4FsL2VpnPwRedPwIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (pRgPw->pPwVcEntry->i1RowStatus != ACTIVE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "The pseudowire vcentry is not active\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case DESTROY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedPwTable
 Input       :  The Indices
                FsL2VpnPwRedGroupIndex
                FsL2VpnPwRedPwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedPwTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsL2VpnPwRedIccpPwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsL2VpnPwRedIccpPwTable
 Input       :  The Indices
                FsL2VpnPwRedIccpPwRgIndex
                FsL2VpnPwRedIccpPwHeadLsr
                FsL2VpnPwRedIccpPwFecType
                FsL2VpnPwRedIccpPwTailLsr
                FsL2VpnPwRedIccpPwGroup
                FsL2VpnPwRedIccpPwId
                FsL2VpnPwRedIccpPwAgiType
                FsL2VpnPwRedIccpPwAgi
                FsL2VpnPwRedIccpPwLocalAiiType
                FsL2VpnPwRedIccpPwLocalAii
                FsL2VpnPwRedIccpPwRemoteAiiType
                FsL2VpnPwRedIccpPwRemoteAii
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsL2VpnPwRedIccpPwTable
    (UINT4 u4FsL2VpnPwRedIccpPwRgIndex,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwHeadLsr,
     INT4 i4FsL2VpnPwRedIccpPwFecType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwTailLsr,
     UINT4 u4FsL2VpnPwRedIccpPwGroup,
     UINT4 u4FsL2VpnPwRedIccpPwId,
     INT4 i4FsL2VpnPwRedIccpPwAgiType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwAgi,
     INT4 i4FsL2VpnPwRedIccpPwLocalAiiType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwLocalAii,
     INT4 i4FsL2VpnPwRedIccpPwRemoteAiiType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwRemoteAii)
{

    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;
    
    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\nSNMP_FAILURE:"
                "Pseudowire-Redundancy is Disabled\n");
        return SNMP_FAILURE;
    }
    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4FsL2VpnPwRedIccpPwRgIndex;
    MEMCPY (&pTmpIccpPw->RouterId, pFsL2VpnPwRedIccpPwHeadLsr->pu1_OctetList,
            sizeof (pTmpIccpPw->RouterId));
    pTmpIccpPw->Fec.u1Type = (UINT1) i4FsL2VpnPwRedIccpPwFecType;

    if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
    {
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PeerId,
                pFsL2VpnPwRedIccpPwTailLsr->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec128.au1PeerId));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1GroupId,
                &u4FsL2VpnPwRedIccpPwGroup, sizeof (UINT4));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PwId,
                &u4FsL2VpnPwRedIccpPwId, sizeof (UINT4));
    }
    else if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_GEN_TYPE)
    {
        pTmpIccpPw->Fec.u.Fec129.Agi.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwAgiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1,
                pFsL2VpnPwRedIccpPwAgi->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1));
        pTmpIccpPw->Fec.u.Fec129.SAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwLocalAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwLocalAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2));
        pTmpIccpPw->Fec.u.Fec129.TAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwRemoteAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwRemoteAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2));
    }

    pRgIccpPw =
        RBTreeGet (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pTmpIccpPw);

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);
    if (pRgIccpPw != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsL2VpnPwRedIccpPwTable
 Input       :  The Indices
                FsL2VpnPwRedIccpPwRgIndex
                FsL2VpnPwRedIccpPwHeadLsr
                FsL2VpnPwRedIccpPwFecType
                FsL2VpnPwRedIccpPwTailLsr
                FsL2VpnPwRedIccpPwGroup
                FsL2VpnPwRedIccpPwId
                FsL2VpnPwRedIccpPwAgiType
                FsL2VpnPwRedIccpPwAgi
                FsL2VpnPwRedIccpPwLocalAiiType
                FsL2VpnPwRedIccpPwLocalAii
                FsL2VpnPwRedIccpPwRemoteAiiType
                FsL2VpnPwRedIccpPwRemoteAii
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsL2VpnPwRedIccpPwTable
    (UINT4 *pu4FsL2VpnPwRedIccpPwRgIndex,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwHeadLsr,
     INT4 *pi4FsL2VpnPwRedIccpPwFecType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwTailLsr,
     UINT4 *pu4FsL2VpnPwRedIccpPwGroup,
     UINT4 *pu4FsL2VpnPwRedIccpPwId,
     INT4 *pi4FsL2VpnPwRedIccpPwAgiType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwAgi,
     INT4 *pi4FsL2VpnPwRedIccpPwLocalAiiType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwLocalAii,
     INT4 *pi4FsL2VpnPwRedIccpPwRemoteAiiType,
     tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwRemoteAii)
{
    tSNMP_OCTET_STRING_TYPE DummyOct;
    UINT1               au1DummyBuff[L2VPN_PWVC_AII2_LEN] = { 0 };

    DummyOct.pu1_OctetList = au1DummyBuff;
    DummyOct.i4_Length = L2VPN_PWVC_AII2_LEN;

    return nmhGetNextIndexFsL2VpnPwRedIccpPwTable (0,
                                                   pu4FsL2VpnPwRedIccpPwRgIndex,
                                                   &DummyOct,
                                                   pFsL2VpnPwRedIccpPwHeadLsr,
                                                   0,
                                                   pi4FsL2VpnPwRedIccpPwFecType,
                                                   &DummyOct,
                                                   pFsL2VpnPwRedIccpPwTailLsr,
                                                   0,
                                                   pu4FsL2VpnPwRedIccpPwGroup,
                                                   0, pu4FsL2VpnPwRedIccpPwId,
                                                   0,
                                                   pi4FsL2VpnPwRedIccpPwAgiType,
                                                   &DummyOct,
                                                   pFsL2VpnPwRedIccpPwAgi, 0,
                                                   pi4FsL2VpnPwRedIccpPwLocalAiiType,
                                                   &DummyOct,
                                                   pFsL2VpnPwRedIccpPwLocalAii,
                                                   0,
                                                   pi4FsL2VpnPwRedIccpPwRemoteAiiType,
                                                   &DummyOct,
                                                   pFsL2VpnPwRedIccpPwRemoteAii);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsL2VpnPwRedIccpPwTable
 Input       :  The Indices
                FsL2VpnPwRedIccpPwRgIndex
                nextFsL2VpnPwRedIccpPwRgIndex
                FsL2VpnPwRedIccpPwHeadLsr
                nextFsL2VpnPwRedIccpPwHeadLsr
                FsL2VpnPwRedIccpPwFecType
                nextFsL2VpnPwRedIccpPwFecType
                FsL2VpnPwRedIccpPwTailLsr
                nextFsL2VpnPwRedIccpPwTailLsr
                FsL2VpnPwRedIccpPwGroup
                nextFsL2VpnPwRedIccpPwGroup
                FsL2VpnPwRedIccpPwId
                nextFsL2VpnPwRedIccpPwId
                FsL2VpnPwRedIccpPwAgiType
                nextFsL2VpnPwRedIccpPwAgiType
                FsL2VpnPwRedIccpPwAgi
                nextFsL2VpnPwRedIccpPwAgi
                FsL2VpnPwRedIccpPwLocalAiiType
                nextFsL2VpnPwRedIccpPwLocalAiiType
                FsL2VpnPwRedIccpPwLocalAii
                nextFsL2VpnPwRedIccpPwLocalAii
                FsL2VpnPwRedIccpPwRemoteAiiType
                nextFsL2VpnPwRedIccpPwRemoteAiiType
                FsL2VpnPwRedIccpPwRemoteAii
                nextFsL2VpnPwRedIccpPwRemoteAii
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsL2VpnPwRedIccpPwTable (UINT4 u4FsL2VpnPwRedIccpPwRgIndex,
                                        UINT4 *pu4NextFsL2VpnPwRedIccpPwRgIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsL2VpnPwRedIccpPwHeadLsr,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsL2VpnPwRedIccpPwHeadLsr,
                                        INT4 i4FsL2VpnPwRedIccpPwFecType,
                                        INT4 *pi4NextFsL2VpnPwRedIccpPwFecType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsL2VpnPwRedIccpPwTailLsr,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsL2VpnPwRedIccpPwTailLsr,
                                        UINT4 u4FsL2VpnPwRedIccpPwGroup,
                                        UINT4 *pu4NextFsL2VpnPwRedIccpPwGroup,
                                        UINT4 u4FsL2VpnPwRedIccpPwId,
                                        UINT4 *pu4NextFsL2VpnPwRedIccpPwId,
                                        INT4 i4FsL2VpnPwRedIccpPwAgiType,
                                        INT4 *pi4NextFsL2VpnPwRedIccpPwAgiType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsL2VpnPwRedIccpPwAgi,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsL2VpnPwRedIccpPwAgi,
                                        INT4 i4FsL2VpnPwRedIccpPwLocalAiiType,
                                        INT4
                                        *pi4NextFsL2VpnPwRedIccpPwLocalAiiType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsL2VpnPwRedIccpPwLocalAii,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsL2VpnPwRedIccpPwLocalAii,
                                        INT4 i4FsL2VpnPwRedIccpPwRemoteAiiType,
                                        INT4
                                        *pi4NextFsL2VpnPwRedIccpPwRemoteAiiType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsL2VpnPwRedIccpPwRemoteAii,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsL2VpnPwRedIccpPwRemoteAii)
{
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        return SNMP_FAILURE;
    }

    if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "\nSNMP_FAILURE:"
                "Pseudowire-Redundancy is Disabled\n");
	    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);		
        return SNMP_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4FsL2VpnPwRedIccpPwRgIndex;
    MEMCPY (&pTmpIccpPw->RouterId, pFsL2VpnPwRedIccpPwHeadLsr->pu1_OctetList,
            pFsL2VpnPwRedIccpPwHeadLsr->i4_Length);
    pTmpIccpPw->Fec.u1Type = (UINT1) i4FsL2VpnPwRedIccpPwFecType;
    if (i4FsL2VpnPwRedIccpPwFecType == L2VPN_FEC_PWID_TYPE)
    {
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PeerId,
                pFsL2VpnPwRedIccpPwTailLsr->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec128.au1PeerId));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1GroupId,
                &u4FsL2VpnPwRedIccpPwGroup, sizeof (UINT4));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PwId,
                &u4FsL2VpnPwRedIccpPwId, sizeof (UINT4));
    }
    else if (i4FsL2VpnPwRedIccpPwFecType == L2VPN_FEC_GEN_TYPE)
    {
        pTmpIccpPw->Fec.u.Fec129.Agi.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwAgiType;
        pFsL2VpnPwRedIccpPwAgi->i4_Length =
            pTmpIccpPw->Fec.u.Fec129.Agi.u1Type ==
            L2VPN_GEN_FEC_AGI_TYPE_1 ? L2VPN_PWVC_AGI1_LEN :
            L2VPN_PWVC_AGI1_LEN;
        MEMCPY (&pTmpIccpPw->Fec.u.Fec129.Agi.u,
                pFsL2VpnPwRedIccpPwAgi->pu1_OctetList,
                pFsL2VpnPwRedIccpPwAgi->i4_Length);
        pTmpIccpPw->Fec.u.Fec129.SAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwLocalAiiType;
        pFsL2VpnPwRedIccpPwLocalAii->i4_Length =
            pTmpIccpPw->Fec.u.Fec129.SAii.u1Type ==
            L2VPN_GEN_FEC_AII_TYPE_1 ? L2VPN_PWVC_AII1_LEN :
            L2VPN_PWVC_AII2_LEN;
        MEMCPY (&pTmpIccpPw->Fec.u.Fec129.SAii.u,
                pFsL2VpnPwRedIccpPwLocalAii->pu1_OctetList,
                pFsL2VpnPwRedIccpPwLocalAii->i4_Length);
        pTmpIccpPw->Fec.u.Fec129.TAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwRemoteAiiType;
        pFsL2VpnPwRedIccpPwRemoteAii->i4_Length =
            pTmpIccpPw->Fec.u.Fec129.TAii.u1Type ==
            L2VPN_GEN_FEC_AII_TYPE_1 ? L2VPN_PWVC_AII1_LEN :
            L2VPN_PWVC_AII2_LEN;
        MEMCPY (&pTmpIccpPw->Fec.u.Fec129.TAii.u,
                pFsL2VpnPwRedIccpPwRemoteAii->pu1_OctetList,
                pFsL2VpnPwRedIccpPwRemoteAii->i4_Length);
    }

    pRgIccpPw =
        RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pTmpIccpPw,
                       NULL);

    if (pRgIccpPw != NULL)
    {
        pNextFsL2VpnPwRedIccpPwTailLsr->i4_Length = 0;
        *pu4NextFsL2VpnPwRedIccpPwGroup = 0;
        *pu4NextFsL2VpnPwRedIccpPwId = 0;
        *pi4NextFsL2VpnPwRedIccpPwAgiType = 0;
        pNextFsL2VpnPwRedIccpPwAgi->i4_Length = 0;
        *pi4NextFsL2VpnPwRedIccpPwLocalAiiType = 0;
        pNextFsL2VpnPwRedIccpPwLocalAii->i4_Length = 0;
        *pi4NextFsL2VpnPwRedIccpPwRemoteAiiType = 0;
        pNextFsL2VpnPwRedIccpPwRemoteAii->i4_Length = 0;

        *pu4NextFsL2VpnPwRedIccpPwRgIndex = pRgIccpPw->u4RgIndex;
        pNextFsL2VpnPwRedIccpPwHeadLsr->i4_Length =
            sizeof (pRgIccpPw->RouterId);
        MEMCPY (pNextFsL2VpnPwRedIccpPwHeadLsr->pu1_OctetList,
                &pRgIccpPw->RouterId,
                pNextFsL2VpnPwRedIccpPwHeadLsr->i4_Length);
        *pi4NextFsL2VpnPwRedIccpPwFecType = pRgIccpPw->Fec.u1Type;

        if (pRgIccpPw->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
        {
            pNextFsL2VpnPwRedIccpPwTailLsr->i4_Length =
                sizeof (pRgIccpPw->Fec.u.Fec128.au1PeerId);
            MEMCPY (pNextFsL2VpnPwRedIccpPwTailLsr->pu1_OctetList,
                    pRgIccpPw->Fec.u.Fec128.au1PeerId,
                    pNextFsL2VpnPwRedIccpPwTailLsr->i4_Length);
            MEMCPY (pu4NextFsL2VpnPwRedIccpPwGroup,
                    pRgIccpPw->Fec.u.Fec128.au1GroupId, sizeof (UINT4));
            MEMCPY (pu4NextFsL2VpnPwRedIccpPwId,
                    pRgIccpPw->Fec.u.Fec128.au1PwId, sizeof (UINT4));
        }
        else if (pRgIccpPw->Fec.u1Type == L2VPN_FEC_GEN_TYPE)
        {
            *pi4NextFsL2VpnPwRedIccpPwAgiType =
                pRgIccpPw->Fec.u.Fec129.Agi.u1Type;
            pNextFsL2VpnPwRedIccpPwAgi->i4_Length =
                *pi4NextFsL2VpnPwRedIccpPwAgiType ==
                L2VPN_GEN_FEC_AGI_TYPE_1 ? L2VPN_PWVC_AGI1_LEN :
                L2VPN_PWVC_AGI1_LEN;
            MEMCPY (pNextFsL2VpnPwRedIccpPwAgi->pu1_OctetList,
                    &pRgIccpPw->Fec.u.Fec129.Agi.u,
                    pNextFsL2VpnPwRedIccpPwAgi->i4_Length);
            *pi4NextFsL2VpnPwRedIccpPwLocalAiiType =
                pRgIccpPw->Fec.u.Fec129.SAii.u1Type;
            pNextFsL2VpnPwRedIccpPwLocalAii->i4_Length =
                *pi4NextFsL2VpnPwRedIccpPwLocalAiiType ==
                L2VPN_GEN_FEC_AII_TYPE_1 ? L2VPN_PWVC_AII1_LEN :
                L2VPN_PWVC_AII2_LEN;
            MEMCPY (pNextFsL2VpnPwRedIccpPwLocalAii->pu1_OctetList,
                    &pRgIccpPw->Fec.u.Fec129.SAii.u,
                    pNextFsL2VpnPwRedIccpPwLocalAii->i4_Length);
            *pi4NextFsL2VpnPwRedIccpPwRemoteAiiType =
                pRgIccpPw->Fec.u.Fec129.TAii.u1Type;
            pNextFsL2VpnPwRedIccpPwRemoteAii->i4_Length =
                *pi4NextFsL2VpnPwRedIccpPwRemoteAiiType ==
                L2VPN_GEN_FEC_AII_TYPE_1 ? L2VPN_PWVC_AII1_LEN :
                L2VPN_PWVC_AII2_LEN;
            MEMCPY (pNextFsL2VpnPwRedIccpPwRemoteAii->pu1_OctetList,
                    &pRgIccpPw->Fec.u.Fec129.TAii.u,
                    pNextFsL2VpnPwRedIccpPwRemoteAii->i4_Length);
        }

        L2VPN_DBG8 (L2VPN_DBG_LVL_DBG_FLAG,
                    "ICCP::PW ( %02X%02X%02X%02X _ %02X%02X%02X%02X )\n",
                    pRgIccpPw->RoId.au1NodeId[0],
                    pRgIccpPw->RoId.au1NodeId[1],
                    pRgIccpPw->RoId.au1NodeId[2],
                    pRgIccpPw->RoId.au1NodeId[3],
                    pRgIccpPw->RoId.au1PwIndex[0],
                    pRgIccpPw->RoId.au1PwIndex[1],
                    pRgIccpPw->RoId.au1PwIndex[2],
                    pRgIccpPw->RoId.au1PwIndex[3]);

    }

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    return pRgIccpPw == NULL ? SNMP_FAILURE : SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedIccpPwRoId
 Input       :  The Indices
                FsL2VpnPwRedIccpPwRgIndex
                FsL2VpnPwRedIccpPwHeadLsr
                FsL2VpnPwRedIccpPwFecType
                FsL2VpnPwRedIccpPwTailLsr
                FsL2VpnPwRedIccpPwGroup
                FsL2VpnPwRedIccpPwId
                FsL2VpnPwRedIccpPwAgiType
                FsL2VpnPwRedIccpPwAgi
                FsL2VpnPwRedIccpPwLocalAiiType
                FsL2VpnPwRedIccpPwLocalAii
                FsL2VpnPwRedIccpPwRemoteAiiType
                FsL2VpnPwRedIccpPwRemoteAii

                The Object 
                retValFsL2VpnPwRedIccpPwRoId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedIccpPwRoId (UINT4 u4FsL2VpnPwRedIccpPwRgIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsL2VpnPwRedIccpPwHeadLsr,
                              INT4 i4FsL2VpnPwRedIccpPwFecType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsL2VpnPwRedIccpPwTailLsr,
                              UINT4 u4FsL2VpnPwRedIccpPwGroup,
                              UINT4 u4FsL2VpnPwRedIccpPwId,
                              INT4 i4FsL2VpnPwRedIccpPwAgiType,
                              tSNMP_OCTET_STRING_TYPE * pFsL2VpnPwRedIccpPwAgi,
                              INT4 i4FsL2VpnPwRedIccpPwLocalAiiType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsL2VpnPwRedIccpPwLocalAii,
                              INT4 i4FsL2VpnPwRedIccpPwRemoteAiiType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsL2VpnPwRedIccpPwRemoteAii,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsL2VpnPwRedIccpPwRoId)
{
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4FsL2VpnPwRedIccpPwRgIndex;
    MEMCPY (&pTmpIccpPw->RouterId, pFsL2VpnPwRedIccpPwHeadLsr->pu1_OctetList,
            sizeof (pTmpIccpPw->RouterId));
    pTmpIccpPw->Fec.u1Type = (UINT1) i4FsL2VpnPwRedIccpPwFecType;

    if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
    {
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PeerId,
                pFsL2VpnPwRedIccpPwTailLsr->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec128.au1PeerId));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1GroupId,
                &u4FsL2VpnPwRedIccpPwGroup, sizeof (UINT4));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PwId,
                &u4FsL2VpnPwRedIccpPwId, sizeof (UINT4));
    }
    else if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_GEN_TYPE)
    {
        pTmpIccpPw->Fec.u.Fec129.Agi.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwAgiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1,
                pFsL2VpnPwRedIccpPwAgi->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1));
        pTmpIccpPw->Fec.u.Fec129.SAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwLocalAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwLocalAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2));
        pTmpIccpPw->Fec.u.Fec129.TAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwRemoteAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwRemoteAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2));
    }

    pRgIccpPw =
        RBTreeGet (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pTmpIccpPw);

    if (pRgIccpPw != NULL)
    {
        pRetValFsL2VpnPwRedIccpPwRoId->i4_Length = sizeof (pRgIccpPw->RoId);
        MEMCPY (pRetValFsL2VpnPwRedIccpPwRoId->pu1_OctetList,
                &pRgIccpPw->RoId, pRetValFsL2VpnPwRedIccpPwRoId->i4_Length);
    }

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    return pRgIccpPw == NULL ? SNMP_FAILURE : SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedIccpPwPriority
 Input       :  The Indices
                FsL2VpnPwRedIccpPwRgIndex
                FsL2VpnPwRedIccpPwHeadLsr
                FsL2VpnPwRedIccpPwFecType
                FsL2VpnPwRedIccpPwTailLsr
                FsL2VpnPwRedIccpPwGroup
                FsL2VpnPwRedIccpPwId
                FsL2VpnPwRedIccpPwAgiType
                FsL2VpnPwRedIccpPwAgi
                FsL2VpnPwRedIccpPwLocalAiiType
                FsL2VpnPwRedIccpPwLocalAii
                FsL2VpnPwRedIccpPwRemoteAiiType
                FsL2VpnPwRedIccpPwRemoteAii

                The Object 
                retValFsL2VpnPwRedIccpPwPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedIccpPwPriority (UINT4 u4FsL2VpnPwRedIccpPwRgIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsL2VpnPwRedIccpPwHeadLsr,
                                  INT4 i4FsL2VpnPwRedIccpPwFecType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsL2VpnPwRedIccpPwTailLsr,
                                  UINT4 u4FsL2VpnPwRedIccpPwGroup,
                                  UINT4 u4FsL2VpnPwRedIccpPwId,
                                  INT4 i4FsL2VpnPwRedIccpPwAgiType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsL2VpnPwRedIccpPwAgi,
                                  INT4 i4FsL2VpnPwRedIccpPwLocalAiiType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsL2VpnPwRedIccpPwLocalAii,
                                  INT4 i4FsL2VpnPwRedIccpPwRemoteAiiType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsL2VpnPwRedIccpPwRemoteAii,
                                  UINT4 *pu4RetValFsL2VpnPwRedIccpPwPriority)
{
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4FsL2VpnPwRedIccpPwRgIndex;
    MEMCPY (&pTmpIccpPw->RouterId, pFsL2VpnPwRedIccpPwHeadLsr->pu1_OctetList,
            sizeof (pTmpIccpPw->RouterId));
    pTmpIccpPw->Fec.u1Type = (UINT1) i4FsL2VpnPwRedIccpPwFecType;

    if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
    {
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PeerId,
                pFsL2VpnPwRedIccpPwTailLsr->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec128.au1PeerId));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1GroupId,
                &u4FsL2VpnPwRedIccpPwGroup, sizeof (UINT4));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PwId,
                &u4FsL2VpnPwRedIccpPwId, sizeof (UINT4));
    }
    else if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_GEN_TYPE)
    {
        pTmpIccpPw->Fec.u.Fec129.Agi.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwAgiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1,
                pFsL2VpnPwRedIccpPwAgi->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1));
        pTmpIccpPw->Fec.u.Fec129.SAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwLocalAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwLocalAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2));
        pTmpIccpPw->Fec.u.Fec129.TAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwRemoteAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwRemoteAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2));
    }

    pRgIccpPw =
        RBTreeGet (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pTmpIccpPw);

    if (pRgIccpPw != NULL)
    {
        *pu4RetValFsL2VpnPwRedIccpPwPriority = pRgIccpPw->u2Priority;
    }

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    return pRgIccpPw == NULL ? SNMP_FAILURE : SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedIccpPwStatus
 Input       :  The Indices
                FsL2VpnPwRedIccpPwRgIndex
                FsL2VpnPwRedIccpPwHeadLsr
                FsL2VpnPwRedIccpPwFecType
                FsL2VpnPwRedIccpPwTailLsr
                FsL2VpnPwRedIccpPwGroup
                FsL2VpnPwRedIccpPwId
                FsL2VpnPwRedIccpPwAgiType
                FsL2VpnPwRedIccpPwAgi
                FsL2VpnPwRedIccpPwLocalAiiType
                FsL2VpnPwRedIccpPwLocalAii
                FsL2VpnPwRedIccpPwRemoteAiiType
                FsL2VpnPwRedIccpPwRemoteAii

                The Object 
                retValFsL2VpnPwRedIccpPwStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedIccpPwStatus (UINT4 u4FsL2VpnPwRedIccpPwRgIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsL2VpnPwRedIccpPwHeadLsr,
                                INT4 i4FsL2VpnPwRedIccpPwFecType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsL2VpnPwRedIccpPwTailLsr,
                                UINT4 u4FsL2VpnPwRedIccpPwGroup,
                                UINT4 u4FsL2VpnPwRedIccpPwId,
                                INT4 i4FsL2VpnPwRedIccpPwAgiType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsL2VpnPwRedIccpPwAgi,
                                INT4 i4FsL2VpnPwRedIccpPwLocalAiiType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsL2VpnPwRedIccpPwLocalAii,
                                INT4 i4FsL2VpnPwRedIccpPwRemoteAiiType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsL2VpnPwRedIccpPwRemoteAii,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsL2VpnPwRedIccpPwStatus)
{
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;

    pRetValFsL2VpnPwRedIccpPwStatus->i4_Length = 0;

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4FsL2VpnPwRedIccpPwRgIndex;
    MEMCPY (&pTmpIccpPw->RouterId, pFsL2VpnPwRedIccpPwHeadLsr->pu1_OctetList,
            sizeof (pTmpIccpPw->RouterId));
    pTmpIccpPw->Fec.u1Type = (UINT1) i4FsL2VpnPwRedIccpPwFecType;

    if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
    {
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PeerId,
                pFsL2VpnPwRedIccpPwTailLsr->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec128.au1PeerId));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1GroupId,
                &u4FsL2VpnPwRedIccpPwGroup, sizeof (UINT4));
        MEMCPY (pTmpIccpPw->Fec.u.Fec128.au1PwId,
                &u4FsL2VpnPwRedIccpPwId, sizeof (UINT4));
    }
    else if (pTmpIccpPw->Fec.u1Type == L2VPN_FEC_GEN_TYPE)
    {
        pTmpIccpPw->Fec.u.Fec129.Agi.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwAgiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1,
                pFsL2VpnPwRedIccpPwAgi->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.Agi.u.au1Agi1));
        pTmpIccpPw->Fec.u.Fec129.SAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwLocalAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwLocalAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.SAii.u.au1Aii2));
        pTmpIccpPw->Fec.u.Fec129.TAii.u1Type =
            (UINT1) i4FsL2VpnPwRedIccpPwRemoteAiiType;
        MEMCPY (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2,
                pFsL2VpnPwRedIccpPwRemoteAii->pu1_OctetList,
                sizeof (pTmpIccpPw->Fec.u.Fec129.TAii.u.au1Aii2));
    }

    pRgIccpPw =
        RBTreeGet (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pTmpIccpPw);

    if (pRgIccpPw != NULL)
    {
        pRetValFsL2VpnPwRedIccpPwStatus->i4_Length = 1;

        pRetValFsL2VpnPwRedIccpPwStatus->pu1_OctetList[0] =
            (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_FORWARD ? 0x80 :
             0x00) | (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_STANDBY ?
                      0x40 : 0x00) | (pRgIccpPw->u2Status &
                                      L2VPNRED_PW_STATUS_LOCAL_SWITCHOVER ? 0x20
                                      : 0x00) | (pRgIccpPw->u2Status &
                                                 L2VPNRED_PW_STATUS_REMOTE_SWITCHOVER
                                                 ? 0x10 : 0x00) |
            (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_REMOTE_AWAITED ? 0x08 :
             0x00) | (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_NODE_SWITCHOVER ?
                      0x02 : 0x00) | (pRgIccpPw->u2Status &
                                      L2VPNRED_PW_STATUS_LOCAL_UPDATED ? 0x01 :
                                      0x00);
    }

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    return pRgIccpPw == NULL ? SNMP_FAILURE : SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedSimulateFailure
 Input       :  The Indices

                The Object 
                retValFsL2VpnPwRedSimulateFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedSimulateFailure (INT4 *pi4RetValFsL2VpnPwRedSimulateFailure)
{
    *pi4RetValFsL2VpnPwRedSimulateFailure = gL2vpnPwRedTest.u1SimulateFailure;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsL2VpnPwRedSimulateFailureForNbr
 Input       :  The Indices

                The Object 
                retValFsL2VpnPwRedSimulateFailureForNbr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsL2VpnPwRedSimulateFailureForNbr (UINT4
                                         *pu4RetValFsL2VpnPwRedSimulateFailureForNbr)
{
    *pu4RetValFsL2VpnPwRedSimulateFailureForNbr = gL2vpnPwRedTest.u4PeerAddr;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedSimulateFailure
 Input       :  The Indices

                The Object 
                setValFsL2VpnPwRedSimulateFailure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedSimulateFailure (INT4 i4SetValFsL2VpnPwRedSimulateFailure)
{
    gL2vpnPwRedTest.u1SimulateFailure =
        (UINT1) i4SetValFsL2VpnPwRedSimulateFailure;
    LdpSendInvalidRgDataPkt ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsL2VpnPwRedSimulateFailureForNbr
 Input       :  The Indices

                The Object 
                setValFsL2VpnPwRedSimulateFailureForNbr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsL2VpnPwRedSimulateFailureForNbr (UINT4
                                         u4SetValFsL2VpnPwRedSimulateFailureForNbr)
{
    gL2vpnPwRedTest.u4PeerAddr = u4SetValFsL2VpnPwRedSimulateFailureForNbr;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedSimulateFailure
 Input       :  The Indices

                The Object 
                testValFsL2VpnPwRedSimulateFailure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedSimulateFailure (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsL2VpnPwRedSimulateFailure)
{
    if ((i4TestValFsL2VpnPwRedSimulateFailure < 1) ||
        (i4TestValFsL2VpnPwRedSimulateFailure > 5))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsL2VpnPwRedSimulateFailureForNbr
 Input       :  The Indices

                The Object 
                testValFsL2VpnPwRedSimulateFailureForNbr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsL2VpnPwRedSimulateFailureForNbr (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4TestValFsL2VpnPwRedSimulateFailureForNbr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsL2VpnPwRedSimulateFailureForNbr);

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedSimulateFailure
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedSimulateFailure (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsL2VpnPwRedSimulateFailureForNbr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsL2VpnPwRedSimulateFailureForNbr (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
