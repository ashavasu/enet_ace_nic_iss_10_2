/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpss.c,v 1.39.2.1 2018/03/21 12:57:59 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vpss.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines to handle service
 *                             specific events, and interface status change  
 *                             events
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"

/*****************************************************************************/
/* Function Name : L2VpnProcessEnetServAdminEvent                            */
/* Description   : This routine processes Ethernet Service Admin Events      */
/* Input(s)      : pPwVcServAdminEvtInfo - ptr to Eth SS Admin event info    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessEnetServAdminEvent (tPwVcEnetAdminEvtInfo * pPwVcEnetAdminEvtInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    UINT4               u4EvtType;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4EnetCount = 0;
    UINT1               u1IsUpdateReq = L2VPN_TRUE;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex
        (pPwVcEnetAdminEvtInfo->EnetEntryIndex.u4PwVcIndex);

    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "PW VC Entry does not present\r\n");
        return L2VPN_FAILURE;
    }
    TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                              L2VPN_PWVC_ENET_ENTRY
                                              (pPwVcEntry)),
                  pPwVcEnetEntry, tPwVcEnetEntry *)
    {
        if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
            (UINT4) pPwVcEnetAdminEvtInfo->EnetEntryIndex.i2PwVcEnetPwVlan)
        {
            break;
        }
    }
    if (pPwVcEnetEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    u4EvtType = pPwVcEnetAdminEvtInfo->u4EvtType;

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Process L2VpnProcessEnetServAdminEvent for"
                "the PW %d and Event %x\r\n", pPwVcEntry->u4PwVcIndex,
                u4EvtType);

    switch (u4EvtType)
    {
        case L2VPN_PWVC_ENET_ACTIVE:
            if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) != L2VPN_PWVC_ADMIN_UP)
            {
                L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_NOTPRES;
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "All configuration components not present - "
                           "Admin is down\r\n");
                return L2VPN_FAILURE;
            }

            L2VpnHandleAcUpEvent (pPwVcEntry, L2VPN_ADMIN_EVENT);
            break;

        case L2VPN_PWVC_ENET_NIS:
        case L2VPN_PWVC_ENET_DOWN:
        case L2VPN_PWVC_ENET_DESTROY:

            u4EnetCount = TMO_SLL_Count (L2VPN_PWVC_ENET_ENTRY_LIST
                                         ((tPwVcEnetServSpecEntry *)
                                          L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));

            if (((pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_AC_BITMASK)
                 == L2VPN_ZERO) && (u4EnetCount > L2VPN_ONE))
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2VpnProcessEnetServAdminEvent"
                            "the PW %d is already AC Fault\r\n",
                            pPwVcEntry->u4PwVcIndex);
                u1IsUpdateReq = L2VPN_FALSE;
            }

            /* If more than one ENET is attached to PW , then PW local status can be
               updated only for the Last Enet */

            if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
            {
                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
                    (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
                if (pVplsEntry == NULL)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                                "L2VpnProcessEnetServAdminEvent"
                                "VPLS entry for the PW %d doesn't exist\r\n",
                                pPwVcEntry->u4PwVcIndex);
                    return L2VPN_FAILURE;
                }

                i4Status = PwEntryHwACExistsCheck (pVplsEntry,
                                                   L2VPN_PWVC_ENET_PORT_VLAN
                                                   (pPwVcEnetEntry),
                                                   L2VPN_PWVC_ENET_PORT_IF_INDEX
                                                   (pPwVcEnetEntry));

                /* In Case of VPLS, only the AC Entry should be removed from 
                 * the Hardware. VC should not be removed from the Hardware,
                 * eventhough there is no AC
                 */
                if (i4Status == L2VPN_SUCCESS)
                {
                    i4Status = L2VpnPwVcVplsHwVpnAcDel (pPwVcEntry,
                                                        pPwVcEnetEntry);
                }
            }
            /* Notification and OperDown can be done only when the ENET count is one */
            if (u1IsUpdateReq == L2VPN_TRUE)
            {
                L2VpnHandleAcDownEvent (pPwVcEntry, L2VPN_ADMIN_EVENT);
            }
            if (u4EvtType == L2VPN_PWVC_ENET_DESTROY)
            {
                i4Status = L2VpnDestryEnetServInstance (pPwVcEntry,
                                                        pPwVcEnetEntry);
            }

            break;

        default:
            break;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnDestryEnetServInstance                               */
/* Description   : This routine deletes the PW ethernet entry                */
/* Input(s)      : pPwVcEntry - PwVc Information, pPwVcEnetEntry - AC info.  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnDestryEnetServInstance (tPwVcEntry * pPwVcEntry,
                             tPwVcEnetEntry * pPwVcEnetEntry)
{
    UINT4               u4HashIndex = L2VPN_ZERO;

    TMO_SLL_Delete (L2VPN_PWVC_ENET_ENTRY_LIST
                    ((tPwVcEnetServSpecEntry *)
                     L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                    (tTMO_SLL_NODE *) & (pPwVcEnetEntry->NextEnetEntry));
    u4HashIndex =
        (UINT4) L2VPN_COMPUTE_HASH_INDEX (pPwVcEnetEntry->i4PortIfIndex);
    TMO_HASH_Delete_Node (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                          &(pPwVcEnetEntry->EnetNode), u4HashIndex);
    pPwVcEnetEntry->pPwVcEntry = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnDestryEnetServInstance : "
                "PW %d wire Enet deleted\r\n", pPwVcEntry->u4PwVcIndex);
    if (MemReleaseMemBlock
        (L2VPN_ENET_POOL_ID, (UINT1 *) (pPwVcEnetEntry)) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnProcessEnetServAdminEvent : "
                   "PW wire Enet Mem Release failed\r\n");
        return L2VPN_FAILURE;
    }
    /*L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) = NULL; */
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessServAdminEvent                                */
/* Description   : This routine processes Ethernet Service Admin Events      */
/* Input(s)      : pPwVcServAdminEvtInfo - ptr to Eth SS Admin event info    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessServAdminEvent (tPwVcServSpecAdminEvtInfo * pPwVcServAdminEvtInfo)
{
    INT4                i4Status = L2VPN_FAILURE;

    if (pPwVcServAdminEvtInfo->u4ServSpecType == L2VPN_PWVC_SERV_ETH)
    {
        i4Status = L2VpnProcessEnetServAdminEvent
            (&pPwVcServAdminEvtInfo->EnetAdminEvtInfo);
        return i4Status;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessIfUpEvent                                     */
/* Description   : This routine processes If up event                        */
/* Input(s)      : u4IfIndex - Index of interface which has come up          */
/*                 u2PortVlan - AC L2 VLAN is down                           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessIfUpEvent (UINT4 u4IfIndex, UINT2 u2PortVlan)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
#ifdef HVPLS_WANTED
    tCfaIfInfo          IfInfo;
    UINT4               u4PwIndex = 0;
#endif
    UINT4               u4HashIndex;
    UINT4               u4PrevPortVlan = L2VPN_ZERO;
    UINT1               u1Flag = FALSE;
    BOOL1               b1IsEntryFound = L2VPN_FALSE;

#ifdef VPLSADS_WANTED
    L2VpnHandleIfUpForVplsAc (u4IfIndex);
#endif
#ifdef HVPLS_WANTED
    MEMSET (&IfInfo, 0, sizeof (IfInfo));
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return L2VPN_FAILURE;
    }
    if (IfInfo.u1IfType == CFA_PSEUDO_WIRE)
    {
        if (L2VPN_SUCCESS ==
            L2VpnGetPwIndexFromPwIfIndex (u4IfIndex, &u4PwIndex))
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
            if ((pPwVcEntry != NULL)
                && (pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS))
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                            "L2VpnProcessIfUpEvent : "
                            "Pw-If Up Event, for PW-IF:%d PW-ID:%d Reporting to APP (ERPS\r\n",
                            u4IfIndex, pPwVcEntry->u4PwVcIndex);
                L2vpnExtNotifyPwIfStatusToApp (pPwVcEntry, CFA_IF_UP);
            }
        }
    }
#endif
    u4HashIndex = L2VPN_COMPUTE_HASH_INDEX (u4IfIndex);
    TMO_HASH_Scan_Bucket (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pPwVcEnetEntry = MPLS_PWVC_FROM_ENETLIST (pNode);

        /* If the port that is made Up is not equal to the port 
         * associated with the EnetEntry, then continue
         */

        if ((b1IsEntryFound == L2VPN_FALSE) &&
            (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) !=
             (INT4) u4IfIndex))
        {
            continue;
        }
        else if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) !=
                 (INT4) u4IfIndex)
        {
            break;
        }
        b1IsEntryFound = L2VPN_TRUE;

        /* Store the portvlan of the EnetEntry in the u4PrevPortVlan. 
         * associated with the EnetEntry.
         * Since the Enet Entries are stored in Sorted Order,
         * If the u4PrevPortVlan is not equal to current vlan, then
         * the H/w programming should be done.
         */

        if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) != u4PrevPortVlan)
        {
            u1Flag = L2VPN_FALSE;
        }

        u4PrevPortVlan = L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);

        /* If u1Flag is L2VPN_TRUE, then it means that the
         * H/w Programming is already done for the AC.
         */
        if (u1Flag == L2VPN_TRUE)
        {
            pPwVcEnetEntry->u1HwStatus = MPLS_TRUE;
        }

        pPwVcEntry = pPwVcEnetEntry->pPwVcEntry;

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnProcessIfUpEvent : "
                    "Process If Up Event for the Pw %d  and Port %d\r\n",
                    pPwVcEntry->u4PwVcIndex, u4IfIndex);

        if (L2VpnUtilIsPwAcMatched (pPwVcEntry, pPwVcEnetEntry,
                                    u4IfIndex, u2PortVlan) == FALSE)
        {
            continue;
        }

        L2vpnCheckAndHandleAcUpEvent (pPwVcEntry, pPwVcEnetEntry, &u1Flag);
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessIfDownEvent                                   */
/* Description   : This routine processes If down event                      */
/* Input(s)      : u4IfIndex - Index of interface which went down            */
/*                 u2PortVlan - AC L2 VLAN is down                           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessIfDownEvent (UINT4 u4IfIndex, UINT2 u2PortVlan)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
#ifdef HVPLS_WANTED
    tCfaIfInfo          IfInfo;
    UINT4               u4PwIndex = 0;
#endif
    UINT4               u4HashIndex;
    UINT1               u1Flag = L2VPN_FALSE;
    UINT4               u4PrevPortVlan = L2VPN_ZERO;
    BOOL1               b1IsEntryFound = L2VPN_FALSE;

#ifdef VPLSADS_WANTED
    L2VpnHandleIfDownForVplsAc (u4IfIndex);
#endif
#ifdef HVPLS_WANTED
    MEMSET (&IfInfo, 0, sizeof (IfInfo));
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return L2VPN_FAILURE;
    }
    if (IfInfo.u1IfType == CFA_PSEUDO_WIRE)
    {
        if (L2VPN_SUCCESS ==
            L2VpnGetPwIndexFromPwIfIndex (u4IfIndex, &u4PwIndex))
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
            if ((pPwVcEntry != NULL)
                && (pPwVcEntry->u1AppOwner == MPLS_APPLICATION_ERPS))
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                            "L2VpnProcessIfDownEvent : "
                            "Pw-If Down Event, for PW-IF:%d PW-ID:%d Reporting to APP (ERPS\r\n",
                            u4IfIndex, pPwVcEntry->u4PwVcIndex);
                L2vpnExtNotifyPwIfStatusToApp (pPwVcEntry, CFA_IF_DOWN);
            }
        }
    }
#endif
    u4HashIndex = L2VPN_COMPUTE_HASH_INDEX (u4IfIndex);
    TMO_HASH_Scan_Bucket (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pPwVcEnetEntry = MPLS_PWVC_FROM_ENETLIST (pNode);

        /* If the port that is made Down is not equal to the port 
         * associated with the EnetEntry, then continue
         */

        if ((b1IsEntryFound == L2VPN_FALSE) &&
            (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) !=
             (INT4) u4IfIndex))
        {
            continue;
        }
        else if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) !=
                 (INT4) u4IfIndex)
        {
            break;
        }

        b1IsEntryFound = L2VPN_TRUE;

        /* Store the portvlan of the EnetEntry in the u4PrevPortVlan. 
         * associated with the EnetEntry.
         * Since the Enet Entries are stored in Sorted Order,
         * If the u4PrevPortVlan is not equal to current vlan, then
         * the H/w programming should be done.
         */

        if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) != u4PrevPortVlan)
        {
            u1Flag = L2VPN_FALSE;
        }

        u4PrevPortVlan = L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);

        /* If u1Flag is L2VPN_TRUE, then it means that the
         * H/w Programming is already done for the AC.
         */
        if (u1Flag == L2VPN_TRUE)
        {
            pPwVcEnetEntry->u1HwStatus = MPLS_FALSE;
        }

        pPwVcEntry = pPwVcEnetEntry->pPwVcEntry;

        if (L2VpnUtilIsPwAcMatched (pPwVcEntry, pPwVcEnetEntry,
                                    u4IfIndex, u2PortVlan) == FALSE)
        {
            continue;
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnProcessIfDownEvent : "
                    "Process If Down Event for the Pw %d  and Port %d\r\n",
                    pPwVcEntry->u4PwVcIndex, u4IfIndex);

        L2vpnCheckAndHandleAcDownEvent (pPwVcEntry, pPwVcEnetEntry,
                                        L2VPN_PWVC_IF_EVENT, &u1Flag);
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessIfEvent                                       */
/* Description   : This routine processes PSN Events                         */
/* Input(s)      : pL2VpnIfEvtInfo - pointer to Interface event info         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessIfEvent (tL2VpnIfEvtInfo * pL2VpnIfEvtInfo)
{
    UINT4               u4EvtType;
    INT4                i4Status = L2VPN_FAILURE;

    u4EvtType = pL2VpnIfEvtInfo->u4EvtType;

    if (u4EvtType == L2VPN_IF_UP)
    {
        i4Status = L2VpnProcessIfUpEvent (pL2VpnIfEvtInfo->u4IfIndex,
                                          pL2VpnIfEvtInfo->u2PortVlan);
    }
    else if (u4EvtType == L2VPN_IF_DOWN)
    {
        i4Status = L2VpnProcessIfDownEvent (pL2VpnIfEvtInfo->u4IfIndex,
                                            pL2VpnIfEvtInfo->u2PortVlan);
    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unknown interface event\r\n");
        return i4Status;
    }

    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Error in processing If event\r\n");
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckIfStatus                                        */
/* Description   : This routine checks if status in PwVc entry               */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnCheckIfStatus (tPwVcEntry * pPwVcEntry)
{
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    /* check all ACs associated for UP status */
    pPwVcEnetServSpecEntry = (tPwVcEnetServSpecEntry *)
        L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);
    TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry),
                  pPwVcEnetEntry, tPwVcEnetEntry *)
    {
        if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex)
            == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckAcUpStatus                                    */
/* Description   : This routine return SUCCESS if any one of the AC attached */
/*                 to an PwVc is UP                                          */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1UpdateLocalStatus - If sets to L2VPN_TRUE updates the   */
/*                                       Local status.                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnCheckAcUpStatus (tPwVcEntry * pPwVcEntry, UINT1 u1UpdateLocalStatus)
{
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    pPwVcEnetServSpecEntry = (tPwVcEnetServSpecEntry *)
        L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);

    /* Handling of AC status for VPWS and VPLS cases 
     *
     * VPWS - Only one AC entry should exist - 
     *        Ethernet mode 
     *          - if port is configured, check the status 
     *            and update local status accordingly
     *          - Else - update the local status as AC fault  
     *        Ethernet Tagged mode
     *          - L2 VLAN operational status handling is not supported
     *          - if port is configured, check the status 
     *            and update local status accordingly
     *
     * VPLS - One or more AC entries can be configured.
     *        Any one of the AC port up can be considered 
     *        as a AC facing no fault.
     * */
    if (NULL != pPwVcEnetServSpecEntry)
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            /* Currently Ethernet and Ethernet Tagged PW VC types are supported */
            if ((pPwVcEntry->i1PwVcType == L2VPN_PWVC_TYPE_ETH_VLAN) &&
                (pPwVcEnetEntry->i4PortIfIndex == 0))
            {
                /* AC status change based on L2 VLAN operational 
                   status is not supported now */
                if (u1UpdateLocalStatus == L2VPN_TRUE)
                {
                    pPwVcEntry->u1LocalStatus &=
                        (UINT1) (~L2VPN_PWVC_STATUS_AC_BITMASK);
                    return L2VPN_SUCCESS;
                }
            }

            if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
            {
                if ((pPwVcEntry->i1PwVcType == L2VPN_PWVC_TYPE_ETH)
                    && (pPwVcEnetEntry->i4PortIfIndex == 0)
                    && (pPwVcEnetEntry->u2PortVlan !=
                        L2VPN_PWVC_ENET_DEF_PORT_VLAN))
                {
                    if (u1UpdateLocalStatus == L2VPN_TRUE)
                    {

                        pPwVcEntry->u1LocalStatus &=
                            (UINT1) (~L2VPN_PWVC_STATUS_AC_BITMASK);
                        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "AC BIT CLEARED for PW index\r\n",
                                    pPwVcEntry->u4PwVcIndex);
                        return L2VPN_SUCCESS;
                    }
                }
            }

            if (pPwVcEnetEntry->i4PortIfIndex == 0)
            {
                /* Valid port is not configured */
                continue;
            }

            /* Check the Operational status of the AC port */
            if (L2VpnCheckPortOperStatus (pPwVcEnetEntry->i4PortIfIndex)
                == L2VPN_SUCCESS)
            {
                if (u1UpdateLocalStatus == L2VPN_TRUE)
                {
                    pPwVcEntry->u1LocalStatus &=
                        (UINT1) (~L2VPN_PWVC_STATUS_AC_BITMASK);
                }
                return L2VPN_SUCCESS;
            }
        }
    }
    if (u1UpdateLocalStatus == L2VPN_TRUE)
    {
        pPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_AC_BITMASK;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "AC Interface does not exist or "
                "Down For Pw %d\r\n", pPwVcEntry->u4PwVcIndex);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleAcUpEvent                                      */
/* Description   : This routine handles AC UP Event for both protection and  */
/*                 and working PW.                                           */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u4Event    - Event that calls this function.              */
/*                              Can be L2VPN_ADMIN_EVENT or                  */
/*                              L2VPN_PWVC_IF_EVENT                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandleAcUpEvent (tPwVcEntry * pPwVcEntry, UINT4 u4Event)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tPwVcEntry         *pRedPwVcEntry = NULL;
    tPwVcEntry         *pBkpPwVcEntry = NULL;
    tGenU4Addr          GenU4Addr;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC UP for working PW %d with Peer IPv4 -%x"
                " IPv6 -%s \n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC UP for working PW %d with Peer %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    L2VpnHandleAcUp (pPwVcEntry, u4Event);

    /* In case of Pseudowire Redundancy, need to update the
     * Pseudowire which shares the same AC
     */
    pRgPw = L2VpnGetRedundancyPwEntryByPwIndex (pPwVcEntry->u4PwVcIndex);
    if (pRgPw != NULL)
    {
        L2vpnRedundancyPwStateUpdate (pRgPw, L2VPNRED_EVENT_PW_STATE_CHANGE);
        pRgPw = L2VpnUtilGetNextRedundancyPwEntryByIndex
            (pRgPw->u4RgIndex, pPwVcEntry->u4PwVcIndex);
        if (pRgPw != NULL)
        {
            pRedPwVcEntry = L2VpnGetPwVcEntryFromIndex (pRgPw->u4PwIndex);
            if (pRedPwVcEntry != NULL)
            {
                L2VpnHandleAcUp (pRedPwVcEntry, u4Event);
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_STATE_CHANGE);
            }
        }
    }

    pBkpPwVcEntry = L2VpnGetPwVcEntryFromVcId (pPwVcEntry->u4BkpPwVcID);

    if (pBkpPwVcEntry == NULL)
    {
        return;
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pBkpPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pBkpPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC UP for working PW %d with Peer IPv4 -%x"
                " IPv6 -%s \n",
                pBkpPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC UP for  PW protection %d with Peer %x\n",
                pBkpPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif

    L2VpnHandleAcUp (pBkpPwVcEntry, u4Event);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleAcUp                                           */
/* Description   : This routine handles AC UP. It informs LDP module         */
/*                 to send Lbl Map or Notif Msg and programs hardware.       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u4Event    - Event that calls this function.              */
/*                              Can be L2VPN_ADMIN_EVENT or                  */
/*                              L2VPN_PWVC_IF_EVENT                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandleAcUp (tPwVcEntry * pPwVcEntry, UINT4 u4Event)
{
    tGenU4Addr          GenU4Addr;
    UINT4               u4TimeTicks;
    UINT1               u1Flag = L2VPN_FALSE;
    BOOL1               bIsOperChged = FALSE;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }

    if (!(L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) & L2VPN_PWVC_STATUS_AC_BITMASK))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "AC UP not handled for PW %d with peer IPv4 -%x "
                    " IPv6  -%s AC BIT "
                    "ALREADY CLEARED\n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "AC UP not handled for PW %d with peer %x - AC BIT "
                    "ALREADY CLEARED\n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr);
#endif
        return;
    }

    if (u4Event == L2VPN_ADMIN_EVENT)
    {
        if (L2VpnCheckAcUpStatus (pPwVcEntry, L2VPN_TRUE) == L2VPN_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                        "AC UP not handled for PW %d with peer IPv4 -%x "
                        " IPv6  -%s AC BIT "
                        "NOT CLEARED\n", pPwVcEntry->u4PwVcIndex,
                        GenU4Addr.Addr.u4Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else

            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "AC UP not handled for PW %d with peer %x - "
                        "AC BIT NOT CLEARED\n",
                        pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
            return;
        }
    }
    else
    {
        /* Clear the Local AC Rx and Tx Fault Bits */
        pPwVcEntry->u1LocalStatus &=
            (UINT1) (~L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT);
        pPwVcEntry->u1LocalStatus &=
            (UINT1) (~L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT);
    }

    /* In case if the Lower layer is down prior to the shutdown of AC, then
     * only AC needs to be reprogrammed.
     */
    if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) & L2VPN_PWVC_STATUS_PSN_BITMASK)
        == L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT)
    {
        u1Flag = L2VPN_TRUE;
    }

    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_AC_UPDATE, L2VPN_ONE);
    }
    else
    {
        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_ONE);
    }
    if (L2VpnSendLblMapOrNotifMsg (pPwVcEntry) == L2VPN_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Label Map or Notification Msg not sent for the "
                    "PW %d with Peer IPv4- %x IPv6 -%s\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Label Map or Notification Msg not sent for the "
                    "PW %d with Peer %x\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
        return;
    }

    if (((!L2VPN_PWVC_STATUS_IS_UP (pPwVcEntry->u1LocalStatus)) ||
         (!L2VPN_PWVC_STATUS_IS_UP (pPwVcEntry->u1RemoteStatus))) &&
        (u1Flag == L2VPN_FALSE))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Local Status %d or Remote Status %d in NOT_FORWARDING "
                    "STATE for PW %d with Peer- IPv4 %x"
                    " IPv6 -%s \n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus,
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
        L2VPN_DBG4 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Local Status %d or Remote Status %d in NOT_FORWARDING "
                    "STATE for PW %d with Peer %x\n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus,
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
        return;
    }

    /* In VPWS, when Tunnel & AC are down, Local status is marked as 0x16.
     * When Tunnel is made UP, Local status is updated to 0x06 (Only AC Fault) &
     * Oper Status is update to Down from LLDown but no PW programming is done (in VPWS)
     * hence u1CPOrMgmtOperStatus is not updated to OPER UP.
     * u1CPOrMgmtOperStatus is updated when AC is made UP and no other Fault exist*/
    if (L2VPN_PWVC_MODE (pPwVcEntry) != L2VPN_VPWS)
    {
        if ((pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_OPER_UP) &&
            (pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_CP_DEF_OPER_STATUS)
            && (u1Flag == L2VPN_FALSE))
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                        "PW %d with Peer %x not operationally UP\n",
                        pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "PW %d with Peer %x not operationally UP\n",
                        pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
            return;
        }
    }
    /* Update Pw Oper status */
    if (L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                 L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
        == L2VPN_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "HW Updation failed for PW %d with Peer IPv4- %x"
                    " IPv6 -%s \n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "HW Updation failed for PW %d with Peer %x\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_DOWN;

        return;
    }
    else
    {
        L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                          L2VPN_PWVC_OPER_APP_CP_OR_MGMT,
                                          &bIsOperChged);
        OsixGetSysTime (&u4TimeTicks);
        L2VPN_PWVC_UP_TIME (pPwVcEntry) = u4TimeTicks;
        L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_OPER_UP);
        L2VpnPrintPwVc (pPwVcEntry);
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "HW Updation successful for PW %d with Peer IPv4- %x"
                " IPv6 -%s \n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "HW Updation successful for PW %d with Peer %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_TWO);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleAcDownEvent                                    */
/* Description   : This routine handles AC DOWN Event for both protection    */
/*                 and and working PW.                                       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u4Event    - Event that calls this function.              */
/*                              Can be L2VPN_ADMIN_EVENT or                  */
/*                              L2VPN_PWVC_IF_EVENT                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandleAcDownEvent (tPwVcEntry * pPwVcEntry, UINT4 u4Event)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tPwVcEntry         *pRedPwVcEntry = NULL;
    tPwVcEntry         *pBkpPwVcEntry = NULL;
    tGenU4Addr          GenU4Addr;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC DOWN for working PW %d with Peer IPv6- %x"
                " IPv4 -%s \n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC DOWN for working PW %d with Peer %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    L2VpnHandleAcDown (pPwVcEntry, u4Event);

    /* In case of Pseudowire Redundancy, need to update the
     * Pseudowire which shares the same AC
     */
    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
    {
        pRgPw = L2VpnGetRedundancyPwEntryByPwIndex (pPwVcEntry->u4PwVcIndex);
        if (pRgPw != NULL)
        {
            L2vpnRedundancyPwStateUpdate (pRgPw,
                                          L2VPNRED_EVENT_PW_STATE_CHANGE);
            pRgPw =
                L2VpnUtilGetNextRedundancyPwEntryByIndex (pRgPw->u4RgIndex,
                                                          pPwVcEntry->
                                                          u4PwVcIndex);
            if (pRgPw != NULL)
            {
                pRedPwVcEntry = L2VpnGetPwVcEntryFromIndex (pRgPw->u4PwIndex);
                if (pRedPwVcEntry != NULL)
                {
                    L2VpnHandleAcDown (pRedPwVcEntry, u4Event);
                    L2vpnRedundancyPwStateUpdate (pRgPw,
                                                  L2VPNRED_EVENT_PW_STATE_CHANGE);
                }
            }
        }
    }
    pBkpPwVcEntry = L2VpnGetPwVcEntryFromVcId (pPwVcEntry->u4BkpPwVcID);

    if (pBkpPwVcEntry == NULL)
    {
        return;
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pBkpPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pBkpPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC UP DOWN protection PW %d with Peer IPv4- %x"
                " IPv6 -%s \n",
                pBkpPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "Handling AC UP DOWN protection PW %d with Peer %x\n",
                pBkpPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.Ip6Addr);
#endif
    L2VpnHandleAcDown (pBkpPwVcEntry, u4Event);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleAcDown                                         */
/* Description   : This routine handles AC DOWN. It informs LDP module       */
/*                 to send Lbl WithDraw or Notif Msg and programs hardware.  */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u4Event    - Event that calls this function.              */
/*                              Can be L2VPN_ADMIN_EVENT or                  */
/*                              L2VPN_PWVC_IF_EVENT                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandleAcDown (tPwVcEntry * pPwVcEntry, UINT4 u4Event)
{
    tGenU4Addr          GenU4Addr;

    UNUSED_PARAM (u4Event);

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }

    if (pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_AC_BITMASK)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "AC DOWN not handled for PW %d with peer IpV4 -%x "
                    " IPv6 -%s - AC BIT "
                    "ALREADY FAULT\n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "AC DOWN not handled for PW %d with peer %x - AC BIT "
                    "ALREADY FAULT\n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr);
#endif
        return;
    }

    pPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT;
    pPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT;

    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_AC_UPDATE, L2VPN_ONE);
    }
    else
    {
        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_ONE);
    }
    if (L2VpnSendLblWdrawOrNotifMsg (pPwVcEntry) == L2VPN_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Lbl Withdraw or Notification Msg not sent for PW %d "
                    "with Peer IPv4- %x IPv6 -%s\n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Lbl Withdraw or Notification Msg not sent for PW %d "
                    "with Peer %x\n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr);

#endif
        return;
    }

    if ((pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
        (pPwVcEntry->u1RemoteStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Local Status %d or Remote Status %d already "
                    "in NOT_FORWARDING STATE for PW %d with Peer IPv4- %x"
                    " IPv6 -%s \n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus,
                    pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
        L2VPN_DBG4 (L2VPN_DBG_LVL_ERR_FLAG,
                    "Local Status %d or Remote Status %d already "
                    "in NOT_FORWARDING STATE for PW %d with Peer %x\n",
                    pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus,
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.Ip6Addr);
#endif
        return;
    }

    /* In case of VPLS, Hardware Programming for VC, should not be 
     * deleted when the the customer faces, Rx or Tx Fault
     */
    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        return;
    }

    if (L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_DOWN,
                                 L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
        == L2VPN_FAILURE)
    {
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "HW Updation failed for PW %d with Peer IPv4-%x"
                    "IPv6 -%s\n",
                    pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "HW Updation failed for PW %d with Peer %x\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.Ip6Addr);
#endif
        return;
    }
    else if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
    {
        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_DOWN;
    }

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "HW Updation successful for PW %d with Peer IPv4- %x"
                " IPv6 -%s \n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "HW Updation successful for PW %d with Peer %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.Ip6Addr);

#endif
    if (L2VpnUtilIsRemoteStatusNotCapable (pPwVcEntry) == TRUE)
    {
        L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_LDP_INVALID_LABEL);
    }

    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_TWO);
    return;
}

/*****************************************************************************/
/* Function Name : L2vpnCheckAndHandleAcDownEvent                            */
/* Description   : This routine processes Delete VC Requests received from   */
/*                 CFA.                                                      */
/*                 Incase of VPLS, deletes only the AC from H/w              */
/*                 Marks the Pseudowire VC status Down only when all the     */
/*                 associated VC is down. Else just Deletes the VC and       */
/*                 Pseudowire remains Operationally up.                      */
/*                                                                           */
/* Input(s)      : pPwVcEntry     - pointer to tPwVcEntry                    */
/*                 pPwVcEnetEntry - pointer to tPwVcEnetEntry                */
/*                 u4Event        - Event.                                   */
/*                                                                           */
/* Output(s)     : u1Flag - Set to L2VPN_TRUE if h/w programming is called.  */
/*                          Set to L2VPN_FALSE if h/w programming is skipped.*/
/*                                                                           */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2vpnCheckAndHandleAcDownEvent (tPwVcEntry * pPwVcEntry,
                                tPwVcEnetEntry * pPwVcEnetEntry,
                                UINT4 u4Event, UINT1 *pu1Flag)
{
    INT4                i4Status = L2VPN_FAILURE;
    UINT1               u1IsAcUp = L2VPN_FALSE;
    tGenU4Addr          GenU4Addr;

    if (NULL == pPwVcEntry)
    {
        return L2VPN_SUCCESS;
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "L2vpnCheckAndHandleAcDownEvent: Handling for PW %d with Peer IPv4- %x"
                " IPv6 -%s\n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "L2vpnCheckAndHandleAcDownEvent: Handling for PW %d with Peer %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    /* Only If all the Attachment Circuit is removed for the Pseudowire,
     * Local Status of the Psudowire should be set to AC Fault.
     */

    if (L2VpnCheckAcUpStatus (pPwVcEntry, L2VPN_FALSE) == L2VPN_SUCCESS)
    {
        u1IsAcUp = L2VPN_TRUE;
    }

    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        i4Status = L2VpnPwVcVplsHwVpnAcDel (pPwVcEntry, pPwVcEnetEntry);
        if (i4Status == L2VPN_SUCCESS)
        {
            *pu1Flag = L2VPN_TRUE;
        }
    }
    else
    {
        *pu1Flag = L2VPN_FALSE;
    }

    /*Notification and OperDown can be done when all the Associated VC Entry is
     * Down.
     */
    if (u1IsAcUp == L2VPN_FALSE)
    {
        L2VpnHandleAcDownEvent (pPwVcEntry, u4Event);
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2vpnCheckAndHandleAcUpEvent                              */
/* Description   : This routine processes Create VC Requests received from   */
/*                 CFA.                                                      */
/*                 Adds the VC to the Hardware if the Pseudowire is VPLS.    */
/*                                                                           */
/* Input(s)      : pPwVcEntry     - pointer to tPwVcEntry                    */
/*                 pPwVcEnetEntry - pointer to tPwVcEnetEntry                */
/*                                                                           */
/* Output(s)     : u1Flag - Set to L2VPN_TRUE if h/w programming is called.  */
/*                          Set to L2VPN_FALSE if h/w programming is skipped.*/
/*                                                                           */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnCheckAndHandleAcUpEvent (tPwVcEntry * pPwVcEntry,
                              tPwVcEnetEntry * pPwVcEnetEntry, UINT1 *pu1Flag)
{

    INT4                i4Status = L2VPN_SUCCESS;
    tGenU4Addr          GenU4Addr;

    if (NULL == pPwVcEntry)
    {
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CONVERT_TO_INTEGER ((pPwVcEntry->PeerAddr.au1Ipv4Addr),
                            GenU4Addr.Addr.u4Addr);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                "L2vpnCheckAndHandleAcUpEvent: Handling for PW %d with Peer IPv4 -%x"
                " IPv6 -%s\n",
                pPwVcEntry->u4PwVcIndex,
                GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                "L2vpnCheckAndHandleAcUpEvent: Handling for PW %d with Peer %x\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
    /* Even if atleast one AC is there, PW that is VC Down.
     * should be made up. Hence L2VpnHandleAcUpEvent is called prior
     * to L2vpnCheckAndHandleAcUpEvent
     */
    L2VpnHandleAcUpEvent (pPwVcEntry, L2VPN_PWVC_IF_EVENT);

    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
    {
        i4Status = L2VpnPwVcVplsHwVpnAcAdd (pPwVcEntry, pPwVcEnetEntry);

        if (i4Status == L2VPN_SUCCESS)
        {
            *pu1Flag = L2VPN_TRUE;
        }
    }
    else
    {
        *pu1Flag = L2VPN_FALSE;
    }

    return i4Status;
}
