/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpcli.c,v 1.197.4.1 2018/04/11 14:17:41 siva Exp $
 *
 * Description: This file contains the code for supporting CLI 
 *              commands to L2VPN Module.
 *******************************************************************/

#ifndef __L2VPCLI_C__
#define __L2VPCLI_C__

#include "l2vpincs.h"
#include "l2vpdefs.h"
#include "rtm.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "temacs.h"
#include "fsmplswr.h"
#include "mplslsr.h"
#include "stdlsrlw.h"
#include "teextrn.h"
#include "tedsmacs.h"
#include "mplcmndb.h"
#include "stdpwmcli.h"
#include "stdpwecli.h"
#include "stdpwcli.h"
#include "fsmplscli.h"
#include "fsmpnocli.h"
#include "mplscli.h"
#include "fsl2vpcli.h"
#include "fsVplscli.h"
#include "l2vpclip.h"

PRIVATE UINT1       L2VpnCliValidateAC (tPwVcEntry * pPwVcEntry,
                                        tL2vpnCliArgs * pL2vpnCliArgs);

PRIVATE UINT4       L2VpnCliValidatePwVcEntry (tL2vpnCliArgs * pL2vpnCliArgs);

PRIVATE INT1        L2vpnCliGetAgi (UINT1 *pu1Agi,
                                    tL2vpnCliArgs * pL2vpnCliArgs);

PRIVATE INT1        L2vpnCliGetSaii (UINT4 *pu4Saii, UINT1 *pu1Saii,
                                     tL2vpnCliArgs * L2vpnCliArgs);

PRIVATE INT1        L2vpnCliGetTaii (UINT4 *pu4Taii, UINT1 *pu1Taii,
                                     tL2vpnCliArgs * L2vpnCliArgs);
PRIVATE VOID        L2VpnCliGetAiiType2Identifiers (UINT4 u4GlobalId,
                                                    UINT4 u4NodeId,
                                                    UINT4 u4AcId,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    Aii);
PRIVATE INT4        L2VpnExtractGenFecType2Identifiers (UINT1 *pu1Aii,
                                                        UINT4 *pu4GlobalId,
                                                        UINT4 *pu4NodeId,
                                                        UINT4 *pu4AcId);

PRIVATE INT4        L2VpnCliVcNotifConfigure (tCliHandle CliHandle,
                                              INT4 i4PwStatusNotif);
PRIVATE INT4        L2VpnCliShowGlobalInfo (tCliHandle CliHandle);
PRIVATE VOID
       L2VpnCliShowCcDetail (tCliHandle CliHandle, UINT1 u1CcAdvert);
PRIVATE VOID
       L2VpnCliShowCvDetail (tCliHandle CliHandle, UINT1 u1CvAdvert);
PRIVATE VOID
 
 
 
 MPLSShowRunningConfigPwOamDisplay (tCliHandle CliHandle, UINT4 u4PwVcId,
                                    UINT1 u1LocalCcType, UINT1 u1LocalCvType,
                                    UINT1 u1RemoteCcType, UINT1 u1RemoteCvType,
                                    INT4 i4CwPreference, BOOL1 bIsStaticPw);
PRIVATE INT4
 
 
 
 L2VpnCliSetPwIfIndex (tCliHandle CliHandle, UINT4 u4PwIndex,
                       UINT4 u4PwIfIndex);
PRIVATE VOID        CliShowAgiValue (tCliHandle CliHandle, UINT1 au1Agi[]);

PRIVATE VOID        CliShowRunAgiValue (tCliHandle CliHandle, UINT1 au1Agi[]);

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_vpws_cmd                               */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the L2VPN Module as   */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_vpws_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pau4Args[L2VPN_CLI_MAX_ARGS];
    INT4                i4Inst = L2VPN_ZERO;
    INT4                i4RetStatus = L2VPN_ZERO;
    INT1                i1ArgNo = L2VPN_ZERO;
    UINT4               u4Flag = L2VPN_ZERO;
    tL2vpnCliArgs       L2vpnCliArgs;
    tMplsGlobalNodeId   NodeId;
    UINT1               au1Agi[L2VPN_PWVC_MAX_AGI_LEN];
    UINT1               au1Saii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];
    UINT1               au1Taii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];
    INT4                i4Args = 0;

    MEMSET (au1Agi, 0, L2VPN_PWVC_MAX_AGI_LEN);
    MEMSET (au1Saii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);
    MEMSET (au1Taii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);

    MEMSET (&NodeId, 0, sizeof (tMplsGlobalNodeId));

    if (L2VPN_INITIALISED != TRUE)
    {
        CliPrintf (CliHandle, "\r %%L2VPN module is shutdown\n");
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* Third arguement is always InterfaceName/Index */

    MEMSET (&L2vpnCliArgs, L2VPN_ZERO, sizeof (tL2vpnCliArgs));

    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        u4Flag = (UINT4) i4Inst;
    }
    while (1)
    {
        pau4Args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == L2VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    CliRegisterLock (CliHandle, L2vpnLock, L2vpnUnLock);
    MPLS_L2VPN_LOCK ();

    switch (u4Command)
    {
        case CLI_MPLS_VPWS_CREATE:
            /* pau4Args[0] - Indicates Vlan Based/Port Based &
             *               Mplstype Te/Non-te/Vconly
             * pau4Args[1] - Peer Address
             * pau4Args[2] - Local Label for Manual PW owner case,
             *               pwid for PWIDFEC PW owner case,
             *               saii for GENFEC PW owner case
             * pau4Args[3] - Remote Label for Manaul PW owner case
             *               groupid for PWIDFEC PW Owner case
             *               taii for GENFEC PW Owner case
             * pau4Args[4] - Outgoing Tunnel Id for mplstype TE case
             * pau4Args[5] - Incoming Tunnel Id for mplstype TE case
             * pau4Args[6] - PW Owner (Manual/PwidFec/GenFec)
             * pau4Args[7] - Vlan Mode (Change Vlan/Add Vlan/Remove Vlan)
             * pau4Args[8] - Vlan Id as mentioned in pau4Args[7]
             * pau4Args[9] - agi for GENFEC PW Owner case
             * pau4Args[10] - Carries the value of ifIndex in case of 
             *                VLAN based L2VPN or Vlan Id in case of
             *                Port based L2VPN.
             * pau4Args[11] -  EntityId
             * pau4Args[12] - VC Id for Manual and GENFEC Pw Owners
             * pau4Args[13] - Control word status Enable/Disable
             * pau4Args[14] - VCCV status Enable/Disable
             * pau4Args[15] - Inactive Flag status - to make the row status 
             *                 active/Not in Servive
             * pau4Args[16] - Destination Global Id for FEC129 AII Type 2 owner
             * pau4Args[17] - Destination Node Id for FEC129 AII Type 2 owner
             * pau4Args[18] - Source Attachment Circuit Identifier for 
             *                FEC129 AII Type 2 owners
             * pau4Args[19] - Destination Attachment Circuit Identifier 
             *                for FEC129 AII Type 2 owner
             * pau4Args[20] - Pw Type (ethtag/eth) for Manual and PwidFec owner
             *                or Local Pseudowire Label for FEC129 AII Type 2 owner
             * pau4Args[21] - P2MP identifier for manual and 
             Remote Pseudowire Label for FEC129 AII Type 2 Owner
             * pau4Args[22] - Destination of Out Tunnel Id
             * pau4Args[23] - Source of Out Tunnel Id
             * pau4Args[24] - Destination of In Tunnel Id
             * pau4Args[25] - Source of In Tunnel Id
             * pau4Args[26] - Pw Type (ethtag/eth) for GenFec owner
             * pau4Args[27] - P2MP identifier for GenFec owner
             * pau4Args[28] - Source AII
             * pau4Args[29] - Target AII
             * pau4Args[30] - Redundancy group Id
             * pau4Args[31] - preference value for the pseudowire
             * pauAArgs[32] - priority of the pseudowire
             */

            L2vpnCliArgs.u4Flag = CLI_PTR_TO_U4 (pau4Args[0]);
            L2vpnCliArgs.u4PwOwner = CLI_PTR_TO_U4 (pau4Args[6]);

            if ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
                == CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH_TAGGED;
                L2vpnCliArgs.u4VlanId = (UINT4) CLI_GET_VLANID ();

                if (CLI_GET_CXT_ID () != MPLS_DEF_CONTEXT_ID)
                {
                    CliPrintf (CliHandle, "\r\n%% Invalid context \r\n");
                    MPLS_L2VPN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }

                if (pau4Args[10] != NULL)
                {
                    L2vpnCliArgs.i4PortIfIndex = CLI_PTR_TO_I4 (pau4Args[10]);
                }
            }
            /*MS-PW check PW VC is going to be created in interface mode */
            else if ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_PORT_BASED_COMMAND) ==
                     CLI_MPLS_VPWS_PORT_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH;
                L2vpnCliArgs.i4PortIfIndex = CLI_GET_IFINDEX ();
                if (pau4Args[10] != NULL)
                {
                    L2vpnCliArgs.u4VlanId = CLI_PTR_TO_U4 (pau4Args[10]);
                    L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH_TAGGED;
                }
                else
                {
                    L2vpnCliArgs.u4VlanId = L2VPN_PWVC_ENET_DEF_PORT_VLAN;
                }

            }
            /*MS-PW check PW VC is going to be created in interface mode */
            else if ((L2vpnCliArgs.
                      u4Flag & CLI_MPLS_VPWS_SWITCH_BASED_COMMAND) ==
                     CLI_MPLS_VPWS_SWITCH_BASED_COMMAND)
            {
                if (L2vpnCliArgs.u4PwOwner == CLI_L2VPN_PW_OWNER_GEN_FEC)
                {

                    L2vpnCliArgs.i4PwType = (INT4) CLI_PTR_TO_U4 (pau4Args[26]);
                }
                else if (L2vpnCliArgs.u4PwOwner == CLI_L2VPN_PW_OWNER_PW_ID_FEC)
                {
                    L2vpnCliArgs.i4PwType = CLI_PTR_TO_I4 (pau4Args[24]);
                }
                else
                {
                    L2vpnCliArgs.i4PwType = CLI_PTR_TO_I4 (pau4Args[20]);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%% Invalid Mode \r\n");
                MPLS_L2VPN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            /*MS-PW end */

            /* Control word status */
            L2vpnCliArgs.i4CtrlWordStatus = CLI_PTR_TO_I4 (pau4Args[13]);
            /* StatusIndication/VCCV Status */
            /*Set CapabAdvert as PwStatusIndication as VCCV is disabled in 
             * mpls l2transport CLI cmd*/
            L2vpnCliArgs.u4LocalCapabAdvert = CLI_PTR_TO_U4 (pau4Args[14]);

            if (pau4Args[15] != NULL)    /* If "inactive" keyword present */
            {
                L2vpnCliArgs.u4InactiveFlag = L2VPN_PW_INACTIVE_FLAG_SET;
            }
            if (L2vpnCliArgs.u4PwOwner == CLI_L2VPN_PW_OWNER_MANUAL)
            {
                /* Get Pseudowire Parameters for pwOwner "manual" */
                L2vpnCliArgs.u4LocalLabel = *pau4Args[2];
                L2vpnCliArgs.u4RemoteLabel = *pau4Args[3];

                if (pau4Args[1] != NULL)
                {

                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[23]);
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) (pau4Args[1]), IPV6_ADDR_LENGTH);
                    }
                    else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[1];

                    }

                }

                else if (pau4Args[21] != NULL)    /* P2MP identifier */
                {
                    L2vpnCliArgs.PeerAddr.u4Addr = CLI_PTR_TO_U4 (pau4Args[21]);

                }
                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[12]);    /* Pwid for PwOwner="manual" */

                /*Out Tunnel Destination and Source */
                L2vpnCliArgs.u4OutDestTunnelId = CLI_PTR_TO_U4 (pau4Args[16]);
                L2vpnCliArgs.u4OutSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[17]);

                /*In Tunnel Destination and Source */
                L2vpnCliArgs.u4InDestTunnelId = CLI_PTR_TO_U4 (pau4Args[18]);
                L2vpnCliArgs.u4InSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[19]);
                if (pau4Args[22] != NULL)
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[22]);
                }
            }
            else if (L2vpnCliArgs.u4PwOwner == CLI_L2VPN_PW_OWNER_PW_ID_FEC)
            {
                /* Get Pseudowire parameters for pwOwner "Pwidfec" */
                L2vpnCliArgs.u4PwId = *pau4Args[2];
                L2vpnCliArgs.u4LocalGroupId = *pau4Args[3];

                L2vpnCliArgs.u4EntityId = CLI_PTR_TO_U4 (pau4Args[11]);

                if (((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
                     == CLI_MPLS_VPWS_VLAN_BASED_COMMAND) ||
                    ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_PORT_BASED_COMMAND)
                     == CLI_MPLS_VPWS_PORT_BASED_COMMAND))
                {
                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[25]);
                }
                else if (((L2vpnCliArgs.
                           u4Flag & CLI_MPLS_VPWS_SWITCH_BASED_COMMAND) ==
                          CLI_MPLS_VPWS_SWITCH_BASED_COMMAND))
                {
                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[26]);
                }
#ifdef MPLS_IPV6_WANTED

                if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                {
                    MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                            (UINT1 *) pau4Args[1], IPV6_ADDR_LENGTH);
                }
                else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                {

                    L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[1];
                }

                /*Out Tunnel Destination and Source */
                L2vpnCliArgs.u4OutDestTunnelId = CLI_PTR_TO_U4 (pau4Args[16]);
                L2vpnCliArgs.u4OutSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[17]);

                /*In Tunnel Destination and Source */
                L2vpnCliArgs.u4InDestTunnelId = CLI_PTR_TO_U4 (pau4Args[18]);
                L2vpnCliArgs.u4InSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[19]);
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_U4 (pau4Args[20]);
                L2vpnCliArgs.u1IsBackupPw =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[21]);
                /* Label can be configured for signalling cases also */
                L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[22]);
                L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[23]);
                if (CLI_PTR_TO_U4 (pau4Args[24]) ==
                    CLI_L2VPN_PW_TYPE_ETH_TAGGED)
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[25]);
                }
                else
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[24]);
                }
            }
            else
            {
                /* Get Pseudowire parameters for pwOwner "Genfec" */

                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[12]);
                L2vpnCliArgs.u4EntityId = CLI_PTR_TO_U4 (pau4Args[11]);
                L2vpnCliArgs.u4GenAgiType = L2VPN_GEN_PWVC_AGI_TYPE_1;
                /* Set Destination NodeId */
                if (pau4Args[27] != NULL)    /* P2MP indentifier */
                {
                    L2vpnCliArgs.u4DestNodeId = *(pau4Args[27]);
                }
                else
                {
                    L2vpnCliArgs.u4DestNodeId = CLI_PTR_TO_U4 (pau4Args[17]);
                }
                if (*(pau4Args[1]) != 0)
                {

                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[33]);
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) pau4Args[1], IPV6_ADDR_LENGTH);
                    }

                    else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[1];
                    }

                }
                else
                {
                    L2vpnCliArgs.i4PeerAddrType = MPLS_IPV4_ADDR_TYPE;
                }

                if (L2vpnCliArgs.u4DestNodeId != 0)
                {
                    L2vpnCliArgs.PeerAddr.u4Addr = L2vpnCliArgs.u4DestNodeId;

                    /* Fetch FEC 129 - AII Type 2 related parameters */
                    L2vpnCliArgs.u4DestGlobalId = CLI_PTR_TO_U4 (pau4Args[16]);

                    if (L2VpnPortGetNodeId (&NodeId) == OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Not able to get source "
                                   "Global Id & Node Id\r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        L2vpnCliArgs.u4SrcGlobalId = NodeId.u4GlobalId;
                        L2vpnCliArgs.u4SrcNodeId = NodeId.u4NodeId;
                    }

                    /* When Node id is given and global id is not given */
                    /* In Case of Pw over P2mp, the Dest Global Id will be zero */
                    if ((pau4Args[27] == NULL) &&
                        (L2vpnCliArgs.u4DestGlobalId == 0))
                    {
                        L2vpnCliArgs.u4DestGlobalId = NodeId.u4GlobalId;
                    }

                    /* Set Source AC Id and Destination AC Id */
                    L2vpnCliArgs.u4SrcAcId = CLI_PTR_TO_U4 (pau4Args[18]);
                    L2vpnCliArgs.u4DestAcId = CLI_PTR_TO_U4 (pau4Args[19]);

                    L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[20]);
                    L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[21]);

                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_2;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_2;

                    /*Store AGI for Genfec AII Type 2 */

                    L2vpnCliArgs.Agi.pu1_OctetList = au1Agi;
                    L2vpnCliArgs.Agi.i4_Length = STRLEN ((UINT1 *) pau4Args[9]);
                    MEMCPY (L2vpnCliArgs.Agi.pu1_OctetList,
                            (UINT1 *) pau4Args[9], L2vpnCliArgs.Agi.i4_Length);
                    /* Store the Source GlobalId, Source NodeId and Source AcId 
                     * in SAII*/
                    L2vpnCliArgs.Saii.pu1_OctetList = au1Saii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4SrcGlobalId,
                                                    L2vpnCliArgs.u4SrcNodeId,
                                                    L2vpnCliArgs.u4SrcAcId,
                                                    &L2vpnCliArgs.Saii);

                    /* Store the Destination GlobalId, Destination NodeId and 
                     * Destination AcId in TAII*/
                    L2vpnCliArgs.Taii.pu1_OctetList = au1Taii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4DestGlobalId,
                                                    L2vpnCliArgs.u4DestNodeId,
                                                    L2vpnCliArgs.u4DestAcId,
                                                    &L2vpnCliArgs.Taii);
                }

                else
                {
                    /* Get Local AII Type and Remote AII Type for GenFec 
                     * AII Type 1 */
                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_1;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_1;

                    if (pau4Args[28] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_SAII_IP;
                    }
                    if (pau4Args[29] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_TAII_IP;
                    }

                    if (L2vpnCliGetSaii ((UINT4 *) pau4Args[28],
                                         (UINT1 *) pau4Args[2],
                                         &L2vpnCliArgs) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                   "parameters \r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }

                    if (L2vpnCliGetTaii ((UINT4 *) pau4Args[29],
                                         (UINT1 *) pau4Args[3],
                                         &L2vpnCliArgs) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                   "parameters \r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }

                    if (L2vpnCliGetAgi ((UINT1 *) pau4Args[9], &L2vpnCliArgs)
                        == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                   "parameters \r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }

                /* Label can be configured for signalling cases also */
                L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[20]);
                L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[21]);
                /*Out Tunnel Destination and Source */
                L2vpnCliArgs.u4OutDestTunnelId = CLI_PTR_TO_U4 (pau4Args[22]);
                L2vpnCliArgs.u4OutSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[23]);

                /*In Tunnel Destination and Source */
                L2vpnCliArgs.u4InDestTunnelId = CLI_PTR_TO_U4 (pau4Args[24]);
                L2vpnCliArgs.u4InSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[25]);
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_U4 (pau4Args[30]);
                L2vpnCliArgs.u1IsBackupPw =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[31]);
                if (pau4Args[32] != NULL)
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[32]);
                }

            }
            if (pau4Args[4] != NULL)    /* Out Tunnel Id for mplstype te */
            {
                L2vpnCliArgs.u4OutTunnelId = CLI_PTR_TO_U4 (pau4Args[4]);
            }

            if (pau4Args[5] != NULL)    /* In Tunnel Id for mplstype te */
            {
                L2vpnCliArgs.u4InTunnelId = CLI_PTR_TO_U4 (pau4Args[5]);
            }

            L2vpnCliArgs.i4VlanMode = CLI_PTR_TO_I4 (pau4Args[7]);

            if (L2vpnCliArgs.i4VlanMode == CLI_L2VPN_VLAN_MODE_NO_CHANGE)
            {
                L2vpnCliArgs.u4PwVlanId = L2vpnCliArgs.u4VlanId;
            }
            else
            {
                L2vpnCliArgs.u4PwVlanId = CLI_PTR_TO_U4 (pau4Args[8]);
            }

            i4RetStatus = L2VpnPwVcCreate (CliHandle, &L2vpnCliArgs);
            break;

        case CLI_MPLS_VPWS_DELETE:
            /* pau4Args[0] - Indicates Vlan Based/Port Based/Switch Based pw type 
             * pau4Args[1] - Pw Owner
             * pau4Args[2] - Peer Address 
             * pau4Args[3] - Attachment Group Identifier 
             * pau4Args[4] - Saii for FEC129 AII Type1 
             * pau4Args[5] - Taii for FEC129 AII Type2 and
             VcID for Manual  
             * pau4Args[6] - VcID for Genfec Pw Owner and
             P2MP identifier for manual
             * pau4Args[7] - Destination Global Id for FEC129 AII Type2 
             * pau4Args[8] - Destination Node Id for FEC129 AII Type2 
             * pau4Args[9] - Source Attachment Circuit Identifier for
             *               FEC129 AII Type 2 
             * pau4Args[10] - Destination Attachment Circuit Identifier
             *               for FEC129 AII Type 2 
             * pau4Args[11] - Local Pseudowire Label for FEC129 AII Type 2 owner
             * pau4Args[12] - Remote Pseudowire Label for FEC129 AII Type 2 owner
             * pau4Args[13] - P2MP identifier for FEC129 AII Type 2 owner
             * pau4Args[14] - Ip Address for Saii for FEC129 AII Type1
             * pau4Args[15] - Ip Address for Taii for FEC129 AII Type1 */

            L2vpnCliArgs.u4Flag = CLI_PTR_TO_U4 (pau4Args[0]);

            if ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
                == CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH_TAGGED;
                L2vpnCliArgs.u4VlanId = (UINT4) CLI_GET_VLANID ();
                if (CLI_GET_CXT_ID () != MPLS_DEF_CONTEXT_ID)
                {
                    CliPrintf (CliHandle, "\r\n%% Invalid context \r\n");
                    MPLS_L2VPN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }
            else if ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_PORT_BASED_COMMAND) ==
                     CLI_MPLS_VPWS_PORT_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH;
                L2vpnCliArgs.i4PortIfIndex = CLI_GET_IFINDEX ();
                L2vpnCliArgs.u4VlanId = L2VPN_PWVC_ENET_DEF_PORT_VLAN;
            }
            else if ((L2vpnCliArgs.
                      u4Flag & CLI_MPLS_VPWS_SWITCH_BASED_COMMAND) ==
                     CLI_MPLS_VPWS_SWITCH_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = 0;
            }
            u4Flag = CLI_PTR_TO_U4 (pau4Args[1]);
            L2vpnCliArgs.u4Flag = u4Flag;
            L2vpnCliArgs.u4PwOwner = u4Flag;
            L2vpnCliArgs.i4PwMode = L2VPN_VPWS;
            if (u4Flag == CLI_L2VPN_PW_OWNER_MANUAL)
            {
                if (pau4Args[2] != NULL)
                {
                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[7]);
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) pau4Args[2], IPV6_ADDR_LENGTH);
                    }

                    if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[2];

                    }
                }

                else if (pau4Args[6] != NULL)
                    MEMCPY (&L2vpnCliArgs.PeerAddr.u4Addr, pau4Args[6],
                            IPV4_ADDR_LENGTH);
                L2vpnCliArgs.u4LocalLabel = *pau4Args[3];    /*Locallabel */
                L2vpnCliArgs.u4RemoteLabel = *pau4Args[4];    /*Remotelabel */
                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[5]);    /* PwId */

            }
            else if (L2vpnCliArgs.u4PwOwner == CLI_L2VPN_PW_OWNER_PW_ID_FEC)
            {

                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[6]);
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                {
                    MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                            (UINT1 *) pau4Args[2], IPV6_ADDR_LENGTH);
                }
                else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                {
                    L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[2];
                }
                L2vpnCliArgs.u4PwId = *pau4Args[3];
                L2vpnCliArgs.u4LocalGroupId = *pau4Args[4];
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_U4 (pau4Args[5]);    /* PW RG Index */
            }
            else if (L2vpnCliArgs.u4PwOwner == CLI_L2VPN_PW_OWNER_GEN_FEC)
            {
                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[17]);
                L2vpnCliArgs.u4GenAgiType = L2VPN_GEN_PWVC_AGI_TYPE_1;
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_U4 (pau4Args[16]);    /* PW RG Index */
                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[6]);
                if (pau4Args[13] != NULL)    /* P2MP indentifier */
                {
                    L2vpnCliArgs.u4DestNodeId = *pau4Args[13];
                }
                else
                {
                    L2vpnCliArgs.u4DestNodeId = CLI_PTR_TO_U4 (pau4Args[8]);
                }

                if (L2vpnCliArgs.u4DestNodeId != 0)
                {

                    L2vpnCliArgs.PeerAddr.u4Addr = L2vpnCliArgs.u4DestNodeId;
                    L2vpnCliArgs.u4DestGlobalId = CLI_PTR_TO_U4 (pau4Args[7]);
                    if (L2VpnPortGetNodeId (&NodeId) == OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Not able to get source "
                                   "Global Id & Node Id\r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        L2vpnCliArgs.u4SrcGlobalId = NodeId.u4GlobalId;
                        L2vpnCliArgs.u4SrcNodeId = NodeId.u4NodeId;
                    }

                    /* When Node id is given and global id is not given */
                    /* In Case of Pw over P2mp, the Dest Global Id will be zero */
                    if ((pau4Args[13] == NULL) &&
                        (L2vpnCliArgs.u4DestGlobalId == 0))
                    {
                        L2vpnCliArgs.u4DestGlobalId = NodeId.u4GlobalId;
                    }

                    L2vpnCliArgs.u4SrcAcId = CLI_PTR_TO_U4 (pau4Args[9]);
                    L2vpnCliArgs.u4DestAcId = CLI_PTR_TO_U4 (pau4Args[10]);
                    L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[11]);
                    L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[12]);

                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_2;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_2;

                    /*Store AGI for Genfec AII Type 2 */
                    L2vpnCliArgs.Agi.pu1_OctetList = au1Agi;
                    L2vpnCliArgs.Agi.i4_Length =
                        (INT4) STRLEN ((UINT1 *) pau4Args[3]);
                    MEMCPY (L2vpnCliArgs.Agi.pu1_OctetList,
                            (UINT1 *) pau4Args[3], L2vpnCliArgs.Agi.i4_Length);

                    /* Store the Source GlobalId, Source NodeId and Source AcId 
                     * in SAII*/
                    L2vpnCliArgs.Saii.pu1_OctetList = au1Saii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4SrcGlobalId,
                                                    L2vpnCliArgs.u4SrcNodeId,
                                                    L2vpnCliArgs.u4SrcAcId,
                                                    &L2vpnCliArgs.Saii);

                    /* Store the Destination GlobalId, Destination NodeId and 
                     * Destination AcId in TAII*/
                    L2vpnCliArgs.Taii.pu1_OctetList = au1Taii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4DestGlobalId,
                                                    L2vpnCliArgs.u4DestNodeId,
                                                    L2vpnCliArgs.u4DestAcId,
                                                    &L2vpnCliArgs.Taii);

                }
                else
                {
                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_1;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_1;

                    if (pau4Args[14] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_SAII_IP;
                    }
                    if (pau4Args[15] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_TAII_IP;
                    }

                    if (L2vpnCliGetSaii ((UINT4 *) pau4Args[14],
                                         (UINT1 *) pau4Args[4],
                                         &L2vpnCliArgs) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                   "parameters \r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }

                    if (L2vpnCliGetTaii ((UINT4 *) pau4Args[15],
                                         (UINT1 *) pau4Args[5],
                                         &L2vpnCliArgs) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                   "parameters \r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }

                    if (L2vpnCliGetAgi ((UINT1 *) pau4Args[3], &L2vpnCliArgs)
                        == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                   "parameters \r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }

#ifdef MPLS_IPV6_WANTED

                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) (pau4Args[2]), IPV6_ADDR_LENGTH);
                    }

                    if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[2];

                    }
                }
            }

            i4RetStatus = L2VpnPwVcDelete (CliHandle, &L2vpnCliArgs);
            break;

        case CLI_MPLS_L2VPN_DBG:

            if (pau4Args[1] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (pau4Args[1]);
                L2VpnCliSetDebugLevel (CliHandle, i4Args);
            }

            i4RetStatus =
                L2VpnCliEnableDebug (CliHandle, CLI_PTR_TO_I4 (pau4Args[0]));
            break;
        case CLI_MPLS_NO_L2VPN_DBG:
            i4RetStatus =
                L2VpnCliDisableDebug (CliHandle, CLI_PTR_TO_I4 (pau4Args[0]));
            break;
        case CLI_MPLS_L2VPN_SHOW:

            L2vpnCliArgs.u4Flag = CLI_PTR_TO_U4 (pau4Args[0]);
            L2vpnCliArgs.u4PwOwner = CLI_PTR_TO_U4 (pau4Args[4]);

            if (pau4Args[1] != NULL)
            {
                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[7]);

#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                {
                    MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                            (UINT1 *) pau4Args[1], IPV6_ADDR_LENGTH);
                }

                else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                {
                    L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[1];

                }
            }
            else if ((pau4Args[2] != NULL) && (pau4Args[3] != NULL))
            {
                L2vpnCliArgs.u4DestGlobalId = *((pau4Args[2]));
                L2vpnCliArgs.u4DestNodeId = *(pau4Args[3]);
            }
            L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[5]);
            L2vpnCliArgs.u4PwIndex = CLI_PTR_TO_U4 (pau4Args[6]);

            i4RetStatus = L2VpnPwVcShow (CliHandle, &L2vpnCliArgs);
            break;

        case CLI_MPLS_RED_PW_PREF_CONFIG:

            if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
            {
                CliPrintf (CliHandle,
                           "\r%% pseudowire-redundancy is disabled\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            L2vpnCliArgs.u1RedPwPreference =
                (UINT1) CLI_PTR_TO_U4 (pau4Args[0]);
            L2vpnCliArgs.u4Flag = CLI_PTR_TO_U4 (pau4Args[3]);
            L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[1]);
            L2vpnCliArgs.u1PwStatus = (UINT1) CLI_PTR_TO_U4 (pau4Args[2]);
            L2vpnCliArgs.i4HoldingPriority = CLI_PTR_TO_I4 (pau4Args[4]);
            L2vpnCliArgs.u4VplsIndex = L2VPN_ZERO;
            if ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
                == CLI_MPLS_VPWS_VLAN_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH_TAGGED;
                L2vpnCliArgs.u4VlanId = (UINT4) CLI_GET_VLANID ();
            }
            else if ((L2vpnCliArgs.u4Flag & CLI_MPLS_VPWS_PORT_BASED_COMMAND) ==
                     CLI_MPLS_VPWS_PORT_BASED_COMMAND)
            {
                L2vpnCliArgs.i4PwType = CLI_L2VPN_PW_TYPE_ETH;
                L2vpnCliArgs.i4PortIfIndex = CLI_GET_IFINDEX ();
                L2vpnCliArgs.u4VlanId = L2VPN_PWVC_ENET_DEF_PORT_VLAN;
            }
            else
            {
                L2vpnCliArgs.u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

            }
            if (L2VpnUtilGetMatchPwRGEntry (L2vpnCliArgs.u4PwId,
                                            &L2vpnCliArgs.u4PwRedGrpId) !=
                L2VPN_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% PW Index not mapped to RG Group\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = L2VpnConfigPwPref (CliHandle, &L2vpnCliArgs);
            break;
#ifdef MPLS_TEST_WANTED
#if defined(VPLS_GR_WANTED)||defined(LDP_GR_WANTED)
        case CLI_MPLS_SHOW_HW_LIST:
            i4RetStatus = L2VpnGrCliShowHwList (CliHandle);
            break;
#endif
#endif
        default:
            CliPrintf (CliHandle, "\r%% Unknown Command \r\n");
            MPLS_L2VPN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CLI_SET_ERR (0);
    }
    MPLS_L2VPN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;

}

/******************************************************************************
* Function Name : L2VpnPwVcCreate
* Description   : This routine will create a pwEntry.
*
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 pL2vpnCliArgs         - A pointer to a structure that contains 
*                                        parameters like pw mpls type, pw type, 
*                                        peer addr, pw mode etc., that should be 
*                                        set while creating a pseudowire.
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnPwVcCreate (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry RgPw;
    UINT4               u4PwIndex = L2VPN_ZERO;
    UINT4               u4OldPwIndex = L2VPN_ZERO;
    UINT4               u4PwEnetPwVlan = L2VPN_ZERO;
    INT4                i4PwEnetVlanMode = L2VPN_ZERO;
    INT4                i4IfIndex = L2VPN_ZERO;
    UINT4               u4PwEnetPortVlan = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    UINT4               u4Flag = L2VPN_FALSE;

    i4PwEnetVlanMode = pL2vpnCliArgs->i4VlanMode;
    u4PwEnetPortVlan = pL2vpnCliArgs->u4VlanId;
    i4IfIndex = pL2vpnCliArgs->i4PortIfIndex;
    u4PwEnetPwVlan = pL2vpnCliArgs->u4PwVlanId;

    u4Flag = L2VpnCliValidatePwVcEntry (pL2vpnCliArgs);
    if (u4Flag == L2VPN_TRUE)
    {
        CliPrintf (CliHandle, "\r%%Entry already Exists.\n");
        return CLI_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pL2vpnCliArgs->u4PwId);
    if (pPwVcEntry != NULL)
    {
        if (pL2vpnCliArgs->u1IsBackupPw == L2VPN_CLI_PW_BACKUP)
        {
            CliPrintf (CliHandle, "\r%%This PW is already created "
                       "and hence this cannot be created as the "
                       "backup peer\r\n");
            return CLI_FAILURE;
        }
        u4PwIndex = pPwVcEntry->u4PwVcIndex;
        if (pL2vpnCliArgs->u4PwRedGrpId != 0)
        {
            /* Check whether the RG group Id associated with this Pseudowire is 
             * already associated with some other Attachment Circuit.
             * If so return failure, since One RG Group can be associated with 
             * only one AC
             */
            RgPw.u4RgIndex = pL2vpnCliArgs->u4PwRedGrpId;
            pRgPw =
                RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) & RgPw,
                               NULL);

            if ((pRgPw != NULL)
                && (pRgPw->u4RgIndex == pL2vpnCliArgs->u4PwRedGrpId))
            {
                CliPrintf (CliHandle,
                           "\r%%This Redundancy Group is already associated "
                           "with other Attachment Circuit\r\n");
                return CLI_FAILURE;
            }

            if (CLI_FAILURE ==
                L2VpCliCreateRedPwEntry (CliHandle, u4PwIndex, pL2vpnCliArgs))
            {
                CliPrintf (CliHandle,
                           "\r %%Cannot associate the PW with a RG \r\n");
                return CLI_FAILURE;
            }
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "\r%%PW is already created \r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if ((pL2vpnCliArgs->u4PwRedGrpId != 0) &&
            ((u4PwEnetPortVlan != 0) ||
             (i4IfIndex != 0) ||
             (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN)))
        {
            /* Check if this pseudowire is created for the AC 
               with which another pseudowire is already associated  
               and the pseudowire is not configured as the backup pw,
               in that case return failure */
            if (CLI_FAILURE ==
                L2VpCliCheckIfPwIsBackup (u4PwEnetPortVlan, i4IfIndex,
                                          &u4OldPwIndex))
            {
                pRgPw = L2VpnGetRedundancyPwEntryByPwIndex (u4OldPwIndex);

                if ((pRgPw != NULL)
                    && (pRgPw->u4RgIndex != pL2vpnCliArgs->u4PwRedGrpId))
                {
                    CliPrintf (CliHandle,
                               "\r%%Only one Redundancy Group can be associated "
                               "with an Attachment Circuit\r\n");
                    return CLI_FAILURE;
                }
                if ((pRgPw == NULL)
                    && (pL2vpnCliArgs->u1IsBackupPw == L2VPN_CLI_PW_BACKUP))
                {
                    CliPrintf (CliHandle,
                               "\r%%Back up Peer can be configured only after "
                               "configuring Primary PW\r\n");
                    return CLI_FAILURE;
                }
                if (pL2vpnCliArgs->u1IsBackupPw == 0)
                {
                    CliPrintf (CliHandle,
                               "\r%%Back-up PW should be created with back-up "
                               "peer command only\r\n");
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (pL2vpnCliArgs->u1IsBackupPw == L2VPN_CLI_PW_BACKUP)
                {
                    CliPrintf (CliHandle,
                               "\r%%Back up Peer can be configured only after "
                               "configuring Primary PW\r\n");
                    return CLI_FAILURE;
                }
                /* Check whether the RG group Id associated with this Pseudowire is 
                 * already associated with some other Attachment Circuit.
                 * If so return failure, since One RG Group can be associated with 
                 * only one AC
                 */
                RgPw.u4RgIndex = pL2vpnCliArgs->u4PwRedGrpId;
                pRgPw =
                    RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) & RgPw,
                                   NULL);

                if ((pRgPw != NULL)
                    && (pRgPw->u4RgIndex == pL2vpnCliArgs->u4PwRedGrpId))
                {
                    CliPrintf (CliHandle,
                               "\r%%This Redundancy Group is already associated "
                               "with other Attachment Circuit\r\n");
                    return CLI_FAILURE;
                }
            }
        }
    }

    /* Get the free available pseudo wire index */
    nmhGetPwIndexNext (&u4PwIndex);
    if (u4PwIndex == L2VPN_ZERO)
    {
        CliPrintf (CliHandle, "\r%%PW Index is unavailable.\r\n");
        return CLI_FAILURE;
    }

    /* Create PW and Populate the Pw Table with pw mode as L2VPN_VPWS */
    pL2vpnCliArgs->i4PwMode = L2VPN_VPWS;
    if (PwTablePopulate (CliHandle, u4PwIndex, pL2vpnCliArgs) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Row Status should be made active only when the token 'inactive' is
     * not present in the CLI command "mpls l2transport". When inactive flag 
     * is set, oam related configurations are yet to be done and hence the
     * RS should not be made active. */
    if (nmhTestv2PwRowStatus
        (&u4ErrorCode, u4PwIndex, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%% Unable to activate pseudo wire. "
                   "Check if all required parameters are configured.\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwRowStatus (u4PwIndex, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%% Unable to activate pseudo wire. "
                   "Check if all required parameters are configured.\n");
        return CLI_FAILURE;
    }

    /* Set the pw Mpls type */
    if (PwMplsTypeSet (CliHandle, u4PwIndex, pL2vpnCliArgs->u4Flag) ==
        CLI_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        return CLI_FAILURE;
    }

    /* Populate the Pw Outbound Table */
    if (PwOutboundTablePopulate
        (CliHandle, u4PwIndex, pL2vpnCliArgs) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% PwOutboundTablePopulate failed\n");
        PwDestroy (CliHandle, u4PwIndex);
        return CLI_FAILURE;
    }

    if (pL2vpnCliArgs->u4PwRedGrpId != 0)
    {
        if (CLI_FAILURE ==
            L2VpCliCreateRedPwEntry (CliHandle, u4PwIndex, pL2vpnCliArgs))
        {
            CliPrintf (CliHandle,
                       "\r %%Cannot associate the PW with a RG \r\n");
            PwDestroy (CliHandle, u4PwIndex);
            return CLI_FAILURE;
        }
    }

    /* MS-PW : Skipping the AC details updates as no AC attachment in MS PW */
    if ((pL2vpnCliArgs->u4Flag & CLI_MPLS_VPWS_SWITCH_BASED_COMMAND) !=
        CLI_MPLS_VPWS_SWITCH_BASED_COMMAND)
    {
        /* Populate the Pw Enet Table */
        if (PwEnetTablePopulate
            (CliHandle, u4PwIndex, u4PwEnetPwVlan, u4PwEnetPortVlan,
             i4PwEnetVlanMode, i4IfIndex) == CLI_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            return CLI_FAILURE;
        }
    }
    /* MS-PW */

    if (pL2vpnCliArgs->u4InactiveFlag != L2VPN_PW_INACTIVE_FLAG_SET)
    {
        /* Admin status should not be made UP only when RowStatus != ACTIVE.
         * When inactive flag is set, RS is not made ACTIVE and hence AdminStatus
         * is also not made UP. */
        if (nmhTestv2PwAdminStatus
            (&u4ErrorCode, u4PwIndex, L2VPN_PWVC_ADMIN_UP) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set the pseudo wire admin "
                       "status as up\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwAdminStatus (u4PwIndex, L2VPN_PWVC_ADMIN_UP) ==
            SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set the pseudo wire admin "
                       "status as up\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnPwVcDelete
* Description   : This routine will delete the vpws entry.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 pL2vpnCliArgs         - A pointer to a structure that contains 
*                                        parameters like pw mpls type, pw type, 
*                                        peer addr, pw mode etc., that should be 
*                                        set while deleting a pseudowire.
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnPwVcDelete (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    UINT4               u4PwIndex = 0;
    BOOL1               bFlag = FALSE;
    UINT4               u4PeerAddr;
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    if ((L2VpnCliValidatePwVcEntry (pL2vpnCliArgs)) == L2VPN_FALSE)
    {
        CliPrintf (CliHandle, "\r%%PwVc entry does not exist\r\n");
        return CLI_FAILURE;
    }

    if (pL2vpnCliArgs->u4PwId != L2VPN_ZERO)
    {
        /* If VcId is given in the CLI command, get PwIndex and destroy  
         * the entry */
        pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pL2vpnCliArgs->u4PwId);
        if (pPwVcEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%%PwVc entry does not exist\r\n");
            return CLI_FAILURE;
        }
        else
        {
#ifdef HVPLS_WANTED
            if (pPwVcEntry->u4PwIfIndex != 0)
            {
                CliPrintf (CliHandle,
                           "\r%%PW with PWID:%d cannot be deleted since it is "
                           "mapped to Pw Interface\r\n", pPwVcEntry->u4PwVcID);
                return CLI_FAILURE;
            }
#endif
            if ((pL2vpnCliArgs->u4VplsIndex != L2VPN_ZERO) &&
                (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) !=
                 pL2vpnCliArgs->u4VplsIndex))
            {
                CliPrintf (CliHandle, "\r%%PW is not present in current"
                           " VFI context\r\n");
                return CLI_FAILURE;
            }

            u4PwIndex = pPwVcEntry->u4PwVcIndex;

            if (L2VPN_PWRED_STATUS != L2VPN_PWRED_DISABLED)
            {

                pRgPw =
                    L2VpnGetRedundancyPwEntryByPwIndex (pPwVcEntry->
                                                        u4PwVcIndex);
                if ((pRgPw != NULL) && (pL2vpnCliArgs->u4PwRedGrpId == 0))
                {
                    CliPrintf (CliHandle,
                               "\r%%PW cannot be deleted since it is "
                               "mapped to Redundancy group\r\n");
                    return CLI_FAILURE;
                }
            }

            if ((L2VpnCliValidateAC (pPwVcEntry, pL2vpnCliArgs) == L2VPN_FALSE)
                && (L2VPN_PWVC_MODE (pPwVcEntry) != L2VPN_VPLS))
            {
                CliPrintf (CliHandle,
                           "\r%%Pw cannot be deleted in different vlan mode\r\n");
                return CLI_FAILURE;
            }

            if ((pPwVcEnetServSpecEntry = (tPwVcEnetServSpecEntry *)
                 L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)) != NULL)
            {

                pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                    (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry));
                /*Deletion of Port+Vlan is facilitated
                 *if there is valid PortVlan in AcEntry*/
                if (pPwVcEnetEntry != NULL)
                {
                    if ((pL2vpnCliArgs->i4PwType != 0) &&
                        (L2VPN_PWVC_TYPE (pPwVcEntry) !=
                         (INT1) pL2vpnCliArgs->i4PwType) &&
                        (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                         L2VPN_PWVC_ENET_DEF_PORT_VLAN))
                    {
                        CliPrintf (CliHandle,
                                   "\r%%Pw cannot be deleted in different mode\r\n");
                        return CLI_FAILURE;
                    }
                }
            }
            if (pPwVcEntry->u4LocalGroupID != pL2vpnCliArgs->u4LocalGroupId)
            {
                CliPrintf (CliHandle,
                           "\r%%Pw cannot be deleted as groupid is wrong\r\n");
                return CLI_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                if (L2VPN_ZERO !=
                    MEMCMP (&pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                            &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr,
                            IPV6_ADDR_LENGTH))
                {
                    CliPrintf (CliHandle,
                               "\r%%Pw cannot be deleted for wrong peer address\r\n");
                    return CLI_FAILURE;
                }
            }
            else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
            {

                MEMCPY ((&u4PeerAddr), &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                        IPV4_ADDR_LENGTH);
                u4PeerAddr = OSIX_HTONL (u4PeerAddr);

                if (u4PeerAddr != pL2vpnCliArgs->PeerAddr.u4Addr)
                {

                    CliPrintf (CliHandle,
                               "\r%%Pw cannot be deleted for wrong peer address\r\n");
                    return CLI_FAILURE;
                }
            }
            if (pL2vpnCliArgs->Agi.pu1_OctetList != NULL)
            {
                if ((STRNCMP (pPwVcEntry->au1Agi,
                              pL2vpnCliArgs->Agi.pu1_OctetList,
                              L2VPN_PWVC_MAX_AGI_LEN)) != 0)
                {
                    CliPrintf (CliHandle,
                               "\r%%Pw Attachment Group Id is wrong\r\n");
                    return CLI_FAILURE;
                }
            }

            if (pL2vpnCliArgs->u4PwRedGrpId != 0)
            {
                if (CLI_FAILURE == L2VpCliDeleteRedPwEntry (CliHandle,
                                                            u4PwIndex,
                                                            pL2vpnCliArgs,
                                                            DESTROY))
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to delete the Redundancy PW entry\r\n");
                    return CLI_FAILURE;
                }
            }
            if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
            {
                if (L2VpnCheckForLastPw (pPwVcEntry) == L2VPN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%%AC is present for last PW\n");
                    return CLI_FAILURE;
                }
            }
            PwDestroy (CliHandle, pPwVcEntry->u4PwVcIndex);
            return CLI_SUCCESS;
        }
    }
    else if ((nmhGetFirstIndexPwTable (&u4PwIndex)) != SNMP_SUCCESS)
    {
        /* VcId is not given in CLI command, scan through all entries and 
         * destory the matching entry. */
        CliPrintf (CliHandle, "\r%%There are no entries in pwTable.\r\n");
        return CLI_FAILURE;
    }
    do
    {
        /*  Check for an entry that matches the PeerAddr ,InboundLabel 
         *  and OutboundLabel given as input. If an entry does not exist, 
         *  return failure. If an entry exists , delete it.
         */
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
        if (pPwVcEntry == NULL)
        {
            continue;
        }
#ifdef HVPLS_WANTED
        if (pPwVcEntry->u4PwIfIndex != 0)
        {
            CliPrintf (CliHandle,
                       "\r%%PW with PWID:%d cannot be deleted since it is "
                       "mapped to Pw Interface\r\n", pPwVcEntry->u4PwVcID);
            continue;
        }
#endif
        if ((L2VpnCliValidateAC (pPwVcEntry, pL2vpnCliArgs) == L2VPN_FALSE) &&
            (L2VPN_PWVC_MODE (pPwVcEntry) != L2VPN_VPLS))
        {
            continue;
        }

        if ((pL2vpnCliArgs->i4PwMode != L2VPN_ZERO) &&
            (pPwVcEntry->u1PwVcMode != (UINT1) pL2vpnCliArgs->i4PwMode))
        {
            continue;
        }

        if ((pL2vpnCliArgs->i4PwType != 0) &&
            (L2VPN_PWVC_TYPE (pPwVcEntry) != (INT1) pL2vpnCliArgs->i4PwType))
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED

        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            if (L2VPN_ZERO != MEMCMP (&pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                                      &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr,
                                      IPV6_ADDR_LENGTH))
            {
                continue;
            }
        }
        if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {

            MEMCPY ((&u4PeerAddr), &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                    IPV4_ADDR_LENGTH);
            u4PeerAddr = OSIX_HTONL (u4PeerAddr);

            /*Check PeerAddr entry in Pw table */
            if (u4PeerAddr != pL2vpnCliArgs->PeerAddr.u4Addr)
            {

                /*Check PeerAddr entry in Pw table */
                continue;
            }
        }

        if ((pL2vpnCliArgs->i4PwMode == L2VPN_VPLS) &&
            (pL2vpnCliArgs->u4VplsIndex != pPwVcEntry->u4VplsInstance))
        {
            continue;
        }
        switch (pL2vpnCliArgs->u4Flag)
        {
            case CLI_L2VPN_PW_OWNER_MANUAL:
                /* PW Manual case, We need to have all the checks given below */
                if ((pPwVcEntry->u4InVcLabel == pL2vpnCliArgs->u4LocalLabel) &&
                    (pPwVcEntry->u4OutVcLabel == pL2vpnCliArgs->u4RemoteLabel))
                {
                    bFlag = TRUE;
                }
                break;
            case CLI_L2VPN_PW_OWNER_PW_ID_FEC:
                /* PW PWId case, We need to have all the checks given below */
                if ((pPwVcEntry->u4PwVcID == pL2vpnCliArgs->u4PwId) &&
                    (pPwVcEntry->u4LocalGroupID ==
                     pL2vpnCliArgs->u4LocalGroupId))
                {
                    bFlag = TRUE;
                }
                break;
            case CLI_L2VPN_PW_OWNER_GEN_FEC:
                /* Gen FEC AII Type 2 PW is a manual mode of PW for MPLS-TP */
                if ((L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE) &&
                    (pL2vpnCliArgs->u4LocalLabel != 0) &&
                    (pL2vpnCliArgs->u4RemoteLabel != 0))
                {
                    if ((pPwVcEntry->u4InVcLabel != pL2vpnCliArgs->u4LocalLabel)
                        || (pPwVcEntry->u4OutVcLabel !=
                            pL2vpnCliArgs->u4RemoteLabel))
                    {
                        break;
                    }
                }

                if ((pL2vpnCliArgs->Agi.i4_Length != pPwVcEntry->u1AgiLen) ||
                    (pL2vpnCliArgs->Saii.i4_Length != pPwVcEntry->u1SaiiLen) ||
                    (pL2vpnCliArgs->Taii.i4_Length != pPwVcEntry->u1TaiiLen))
                {
                    break;
                }
                if ((MEMCMP (pPwVcEntry->au1Agi,
                             pL2vpnCliArgs->Agi.pu1_OctetList,
                             pPwVcEntry->u1AgiLen) == 0) &&
                    (MEMCMP (pPwVcEntry->au1Saii,
                             pL2vpnCliArgs->Saii.pu1_OctetList,
                             pPwVcEntry->u1SaiiLen) == 0) &&
                    (MEMCMP (pPwVcEntry->au1Taii,
                             pL2vpnCliArgs->Taii.pu1_OctetList,
                             pPwVcEntry->u1TaiiLen) == 0))
                {
                    bFlag = TRUE;
                }
                break;
            default:
                break;
        }
        if (TRUE == bFlag)
        {
            if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
            {
                if (L2VpnCheckForLastPw (pPwVcEntry) == L2VPN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%%AC is present for last PW\n");
                    return CLI_FAILURE;
                }
            }
            PwDestroy (CliHandle, u4PwIndex);
        }
    }
    while ((nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex)) == SNMP_SUCCESS);

    if (bFlag == FALSE)
    {
        CliPrintf (CliHandle, "\r%%Pseudowire Entry not found\n");
        return CLI_FAILURE;
    }

    if (bFlag == TRUE)
    {
        if (pL2vpnCliArgs->u4PwRedGrpId != 0)
        {
            if (CLI_FAILURE == L2VpCliDeleteRedPwEntry (CliHandle, u4PwIndex,
                                                        pL2vpnCliArgs, DESTROY))
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCheckForLastPw
* Description   : This routine will check if AC is present when current
*                PW is last PW
* Input(s)      : pPwVcEntry 
* Output(s)     : NONE
* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE
*******************************************************************************/
INT4
L2VpnCheckForLastPw (tPwVcEntry * pPwVcEntry)
{
    UINT4               u4VplsIndex = 0;
    tVPLSEntry         *pVplsEntry = NULL;

    u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        return L2VPN_FAILURE;
    }
    if (TMO_SLL_Count (&pVplsEntry->PwList) == L2VPN_ONE)
    {
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
        {
            if (TMO_SLL_Count (L2VPN_PWVC_ENET_ENTRY_LIST
                               ((tPwVcEnetServSpecEntry *)
                                L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))) >
                L2VPN_ZERO)
            {
                return L2VPN_SUCCESS;
            }
        }
    }
    return L2VPN_FAILURE;
}

/******************************************************************************
* Function Name : L2VpnPwVcShow
* Description   : This routine will display detailed information about the 
*                 vpws entries. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 pL2vpnCliArgs         - Pw information(PeerAddr, GlobalID, 
*                                         Node Id, PwIndex and PwID)                        
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnPwVcShow (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4GlobalId = 0;
    UINT4               u4NodeId = 0;
    UINT4               u4AcId = 0;
    UINT4               u4OperUp = 0;
    UINT4               u4AdminDown = 0;
    UINT4               u4OperDown = 0;
    UINT4               u4OperUnknown = 0;
    UINT4               u4OperDormant = 0;
    UINT4               u4OperLlDown = 0;
    BOOL1               bTitleWanted = TRUE;
    tCliShowPw          CliShowPw;
    tSNMP_OCTET_STRING_TYPE NodeId;
    uGenU4Addr          PeerAddr;
    INT1                ai1NullIpv6Addr[IPV6_ADDR_LENGTH];

    MEMSET (ai1NullIpv6Addr, 0, IPV6_ADDR_LENGTH);
    MEMSET (&CliShowPw, 0, sizeof (tCliShowPw));

    if ((pL2vpnCliArgs->u4PwId != L2VPN_ZERO)
        || (pL2vpnCliArgs->u4PwIndex != L2VPN_ZERO))
    {
        if (pL2vpnCliArgs->u4PwId != L2VPN_ZERO)
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pL2vpnCliArgs->u4PwId);
        }
        else
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pL2vpnCliArgs->u4PwIndex);
        }

        if (pPwVcEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%%PwVc entry does not exist\r\n");
            return CLI_FAILURE;
        }
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY (PeerAddr.Ip6Addr.u1_addr,
                    &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            if ((MEMCMP
                 (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr, ai1NullIpv6Addr,
                  IPV6_ADDR_LENGTH)) != L2VPN_ZERO)

            {
                MEMCPY (CliShowPw.PeerAddr.Ip6Addr.u1_addr,
                        &pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                        IPV6_ADDR_LENGTH);
            }
            else
            {
                MEMCPY (CliShowPw.PeerAddr.Ip6Addr.u1_addr,
                        PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            }
        }

        if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY (&(PeerAddr.u4Addr), &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                    IPV4_ADDR_LENGTH);
            PeerAddr.u4Addr = OSIX_HTONL (PeerAddr.u4Addr);

            if (pL2vpnCliArgs->PeerAddr.u4Addr != L2VPN_ZERO)
            {
                CliShowPw.PeerAddr.u4Addr = pL2vpnCliArgs->PeerAddr.u4Addr;
            }
            else
            {
                CliShowPw.PeerAddr.u4Addr = PeerAddr.u4Addr;
            }
        }
        NodeId.pu1_OctetList = pPwVcEntry->au1Taii;
        L2VpnExtractGenFecType2Identifiers (NodeId.pu1_OctetList, &u4GlobalId,
                                            &u4NodeId, &u4AcId);

        CliShowPw.u4DispStatus = pL2vpnCliArgs->u4Flag;

        CliShowPw.u4PwIndex = pPwVcEntry->u4PwVcIndex;
        CliShowPw.pPwVcEntry = pPwVcEntry;
#ifdef MPLS_IPV6_WANTED

        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {

            if (((MEMCMP (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                          ai1NullIpv6Addr, IPV6_ADDR_LENGTH) != L2VPN_ZERO) &&
                 (MEMCMP
                  (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                   PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH) == L2VPN_ZERO))
                || ((pL2vpnCliArgs->u4DestNodeId != L2VPN_ZERO)
                    && (pL2vpnCliArgs->u4DestNodeId == u4NodeId)))
            {

                L2VpnPwVcShowEnetInfo (CliHandle, &CliShowPw, &bTitleWanted);
            }
            else if (((MEMCMP
                       (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                        ai1NullIpv6Addr, IPV6_ADDR_LENGTH) != L2VPN_ZERO)
                      &&
                      (MEMCMP
                       (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                        PeerAddr.Ip6Addr.u1_addr,
                        IPV6_ADDR_LENGTH) != L2VPN_ZERO))
                     || ((pL2vpnCliArgs->u4DestNodeId != L2VPN_ZERO)
                         && (pL2vpnCliArgs->u4DestNodeId != u4NodeId)))
            {
                return CLI_SUCCESS;
            }
            else
            {
                L2VpnPwVcShowEnetInfo (CliHandle, &CliShowPw, &bTitleWanted);
            }
        }

        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            if (((pL2vpnCliArgs->PeerAddr.u4Addr != L2VPN_ZERO) &&
                 (pL2vpnCliArgs->PeerAddr.u4Addr == PeerAddr.u4Addr)) ||
                ((pL2vpnCliArgs->u4DestNodeId != L2VPN_ZERO)
                 && (pL2vpnCliArgs->u4DestNodeId == u4NodeId)))

            {
                L2VpnPwVcShowEnetInfo (CliHandle, &CliShowPw, &bTitleWanted);
            }
            else if (((pL2vpnCliArgs->PeerAddr.u4Addr != L2VPN_ZERO) &&
                      (pL2vpnCliArgs->PeerAddr.u4Addr != PeerAddr.u4Addr)) ||
                     ((pL2vpnCliArgs->u4DestNodeId != L2VPN_ZERO)
                      && (pL2vpnCliArgs->u4DestNodeId != u4NodeId)))
            {
                return CLI_SUCCESS;
            }
            else
            {
                L2VpnPwVcShowEnetInfo (CliHandle, &CliShowPw, &bTitleWanted);
            }
        }
        /*Pw summary case */
        if ((pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_SUMMARY) ||
            (pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_PEER_SUMMARY))
        {
            CliShowPwSummary (pPwVcEntry->u4PwVcIndex, &u4OperUnknown,
                              &u4OperUp, &u4OperDown, &u4OperDormant,
                              &u4OperLlDown, &u4AdminDown);
        }
        if ((bTitleWanted == FALSE)
            && ((pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_SUMMARY)
                || (pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_PEER_SUMMARY)))
        {
            CliPrintf (CliHandle, "\n Total number of PWs\r\n");
            CliPrintf (CliHandle,
                       " unknown: %u\n up: %u\n down: %u\n dormant:  %u\n"
                       " lowerlayerdown: %u\n admin down: %u\r\n",
                       u4OperUnknown, u4OperUp, u4OperDown, u4OperDormant,
                       u4OperLlDown, u4AdminDown);
        }
        return CLI_SUCCESS;
    }

    while (1)
    {
        /* Get the next entry in the PwTable */
        if ((pL2vpnCliArgs->u4PwIndex != 0) &&
            ((nmhGetNextIndexPwTable (pL2vpnCliArgs->u4PwIndex,
                                      &pL2vpnCliArgs->u4PwIndex)) ==
             SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((pL2vpnCliArgs->u4PwIndex == 0) &&
            ((nmhGetFirstIndexPwTable (&pL2vpnCliArgs->u4PwIndex)) ==
             SNMP_FAILURE))
        {
            return CLI_SUCCESS;
        }

        /* Get the Peudo wire VC Entry */
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pL2vpnCliArgs->u4PwIndex);

        /*MS-PW:commented code will be removed after discussion */
        if ((pPwVcEntry == NULL)
            /*|| (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL) */ )
        {
            continue;
        }

        if ((pL2vpnCliArgs->u4PwOwner != 0) &&
            (L2VPN_PWVC_OWNER (pPwVcEntry) != (INT4) pL2vpnCliArgs->u4PwOwner))
        {
            continue;
        }

        /* Peer matching case 
         *  Check for an entry that matches the PeerAddr  given as input. 
         *  If an entry does not exist , return failure.
         *  If an entry exists , display it.
         */
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {

            MEMCPY (&PeerAddr.Ip6Addr.u1_addr,
                    &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            if ((MEMCMP
                 (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr, ai1NullIpv6Addr,
                  IPV6_ADDR_LENGTH)) != L2VPN_ZERO)

            {
                MEMCPY (CliShowPw.PeerAddr.Ip6Addr.u1_addr,
                        &pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                        IPV6_ADDR_LENGTH);
            }
            else
            {
                MEMCPY (CliShowPw.PeerAddr.Ip6Addr.u1_addr,
                        PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            }
        }

        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {

            MEMCPY (&PeerAddr.u4Addr, &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                    IPV4_ADDR_LENGTH);
            PeerAddr.u4Addr = OSIX_HTONL (PeerAddr.u4Addr);

            if (pL2vpnCliArgs->PeerAddr.u4Addr != L2VPN_ZERO)
            {
                CliShowPw.PeerAddr.u4Addr = pL2vpnCliArgs->PeerAddr.u4Addr;
            }
            else
            {
                CliShowPw.PeerAddr.u4Addr = PeerAddr.u4Addr;
            }
        }

        NodeId.pu1_OctetList = pPwVcEntry->au1Taii;
        L2VpnExtractGenFecType2Identifiers (NodeId.pu1_OctetList, &u4GlobalId,
                                            &u4NodeId, &u4AcId);
        /* Peer not matched */
        if ((pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_PEER_DETAIL) ||
            (pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_PEER_SUMMARY))
        {
#ifdef MPLS_IPV6_WANTED

            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                if (((MEMCMP (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                              ai1NullIpv6Addr, IPV6_ADDR_LENGTH) != L2VPN_ZERO)
                     &&
                     (MEMCMP
                      (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                       PeerAddr.Ip6Addr.u1_addr,
                       IPV6_ADDR_LENGTH) != L2VPN_ZERO))
                    || ((pL2vpnCliArgs->u4DestNodeId != L2VPN_ZERO)
                        && (pL2vpnCliArgs->u4DestNodeId != u4NodeId)))
                {
                    continue;
                }
            }
            else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif

            {
                if (((pL2vpnCliArgs->PeerAddr.u4Addr != L2VPN_ZERO) &&
                     (pL2vpnCliArgs->PeerAddr.u4Addr != PeerAddr.u4Addr)) ||
                    ((pL2vpnCliArgs->u4DestNodeId != L2VPN_ZERO) &&
                     (pL2vpnCliArgs->u4DestNodeId != u4NodeId)))
                {

                    continue;
                }

            }
        }
        /*Get and Display the Enet Info */
        CliShowPw.u4DispStatus = pL2vpnCliArgs->u4Flag;
        CliShowPw.u4VcId = pL2vpnCliArgs->u4PwId;
        CliShowPw.u4PwIndex = pL2vpnCliArgs->u4PwIndex;
        CliShowPw.pPwVcEntry = pPwVcEntry;
        L2VpnPwVcShowEnetInfo (CliHandle, &CliShowPw, &bTitleWanted);
        /*Pw summary case */
        if ((pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_SUMMARY) ||
            (pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_PEER_SUMMARY))
        {
            CliShowPwSummary (pL2vpnCliArgs->u4PwIndex, &u4OperUnknown,
                              &u4OperUp, &u4OperDown, &u4OperDormant,
                              &u4OperLlDown, &u4AdminDown);
        }

        if ((bTitleWanted == FALSE)
            && ((pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_SUMMARY)
                || (pL2vpnCliArgs->u4Flag == CLI_MPLS_L2VPN_PEER_SUMMARY)))
        {
            CliPrintf (CliHandle, "\n Total number of PWs\r\n");
            CliPrintf (CliHandle,
                       " unknown: %u\n up: %u\n down: %u\n dormant:  %u\n"
                       " lowerlayerdown: %u\n admin down: %u\r\n",
                       u4OperUnknown, u4OperUp, u4OperDown, u4OperDormant,
                       u4OperLlDown, u4AdminDown);
        }

    }
    return CLI_SUCCESS;

}

/******************************************************************************
 * Function Name : CliMplsConvertTimetoString
 * Description   : This routine will display the time string in 00:00:00:00
 *                 format.
 * Input(s)      :  u4Time               - Time 
 * Output(s)     :  pi1Time              - Display Time String
 * Return(s)     : True /False
 *******************************************************************************/
VOID
CliMplsConvertTimetoString (UINT4 u4Time, INT1 *pi1Time)
{

    UINT4               u4Hrs;
    UINT4               u4Mins;
    UINT4               u4Secs;

    MEMSET (pi1Time, 0, MAX_CLI_TIME_STRING);

    if (u4Time == 0)
    {

        SNPRINTF ((CHR1 *) pi1Time, MAX_CLI_TIME_STRING, "00:00:00");
        return;
    }

    u4Time = u4Time / 100;
    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;
    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;
    u4Hrs = u4Time % 24;

    SNPRINTF ((CHR1 *) pi1Time, MAX_CLI_TIME_STRING,
              "%02d:%02d:%02d", u4Hrs, u4Mins, u4Secs);
    return;
}

/******************************************************************************
* Function Name : PwTablePopulate
* Description   : This routine will populate the entries in pwTable. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable 
*                 pL2vpnCliArgs         - Contains parameters like pw type,
*                                         pw mode, peer address etc., to set 
*                                         for the PW.
* Output(s)     : If Success -CLI_SUCCESS
*                 or else Proper Error Mesage is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
PwTablePopulate (tCliHandle CliHandle, UINT4 u4PwIndex,
                 tL2vpnCliArgs * pL2vpnCliArgs)
{

    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE LocalCapabAdvert;
    tSNMP_OCTET_STRING_TYPE AIIFormat;
    UINT1               au1AiiFormat[MPLS_ONE] = { 0 };
    UINT1               au1PeerAddr[IPV6_ADDR_LENGTH];
    UINT1               u1LocalCapabAdvert = 0;
    UINT4               u4ErrorCode = 0;

    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerAddr.i4_Length = 0;

    AIIFormat.pu1_OctetList = au1AiiFormat;
    AIIFormat.i4_Length = 0;

    u1LocalCapabAdvert = (UINT1) (pL2vpnCliArgs->u4LocalCapabAdvert);
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
    {
        MEMCPY (PeerAddr.pu1_OctetList,
                pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
        PeerAddr.i4_Length = IPV6_ADDR_LENGTH;
    }
    else if (MPLS_IPV4_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
#endif
    {
        MPLS_INTEGER_TO_OCTETSTRING (pL2vpnCliArgs->PeerAddr.u4Addr,
                                     (&PeerAddr));
        PeerAddr.i4_Length = IPV4_ADDR_LENGTH;
    }

    MEMCPY (AIIFormat.pu1_OctetList, &(pL2vpnCliArgs->u1AiiFormat),
            sizeof (UINT1));
    AIIFormat.i4_Length = sizeof (UINT1);

    /* Set Pw Row Status */
    if (nmhTestv2PwRowStatus
        (&u4ErrorCode, u4PwIndex, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create pseudo wire\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwRowStatus
        (u4PwIndex, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create pseudo wire\r\n");
        return CLI_FAILURE;
    }
    /* Set Pw Type */
    if (nmhTestv2PwType
        (&u4ErrorCode, u4PwIndex, pL2vpnCliArgs->i4PwType) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwType\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwType (u4PwIndex, pL2vpnCliArgs->i4PwType) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwType\r\n");
        return CLI_FAILURE;
    }

    /*    Set Pw Owner */
    if (nmhTestv2PwOwner (&u4ErrorCode, u4PwIndex,
                          (INT4) pL2vpnCliArgs->u4PwOwner) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwOwner\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwOwner (u4PwIndex, (INT4) pL2vpnCliArgs->u4PwOwner)
        == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwOwner\r\n");
        return CLI_FAILURE;
    }

    /* Set Pw Psn Type */

    if (nmhTestv2PwPsnType
        (&u4ErrorCode, u4PwIndex, L2VPN_PWVC_PSN_TYPE_MPLS) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwPsnType\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwPsnType (u4PwIndex, L2VPN_PWVC_PSN_TYPE_MPLS) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwPsnType\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2PwPeerAddrType (&u4ErrorCode, u4PwIndex,
                                 pL2vpnCliArgs->i4PeerAddrType) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PW PeerAddrType\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwPeerAddrType (u4PwIndex, pL2vpnCliArgs->i4PeerAddrType)
        == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PW PeerAddrType\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2PwLocalIfMtu
        (&u4ErrorCode, u4PwIndex, pL2vpnCliArgs->u4Mtu) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r\n%%Unable to set MTU size\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwLocalIfMtu (u4PwIndex, pL2vpnCliArgs->u4Mtu) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r\n%%Unable to set MTU size\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2PwPeerAddr
        (&u4ErrorCode, u4PwIndex, &PeerAddr) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PW PeerAddr\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwPeerAddr (u4PwIndex, &PeerAddr) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PW PeerAddr\r\n");
        return CLI_FAILURE;
    }
    LocalCapabAdvert.pu1_OctetList = &u1LocalCapabAdvert;
    LocalCapabAdvert.i4_Length = sizeof (u1LocalCapabAdvert);

    if (nmhTestv2FsMplsL2VpnPwLocalCapabAdvert
        (&u4ErrorCode, u4PwIndex, &LocalCapabAdvert) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwLocalCapabAdvert\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsL2VpnPwLocalCapabAdvert (u4PwIndex,
                                             &LocalCapabAdvert) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwLocalCapabAdvert\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2PwCwPreference (&u4ErrorCode, u4PwIndex,
                                 pL2vpnCliArgs->i4CtrlWordStatus) ==
        SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%TestPwCwPreference is failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwCwPreference (u4PwIndex,
                              pL2vpnCliArgs->i4CtrlWordStatus) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwCwPreference\r\n");
        return CLI_FAILURE;
    }
    if (pL2vpnCliArgs->u4PwId != L2VPN_ZERO)
    {
        /* Set PwID if configured */
        if (nmhTestv2PwID (&u4ErrorCode, u4PwIndex, pL2vpnCliArgs->u4PwId)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwID\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwID (u4PwIndex, pL2vpnCliArgs->u4PwId) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwID\r\n");
            return CLI_FAILURE;
        }
    }

    if (pL2vpnCliArgs->u4PwOwner == (UINT4) L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {

        if (nmhTestv2PwLocalGroupID (&u4ErrorCode, u4PwIndex,
                                     pL2vpnCliArgs->u4LocalGroupId)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set Local Group Id\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwLocalGroupID (u4PwIndex, pL2vpnCliArgs->u4LocalGroupId)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set Local Group Id\r\n");
            return CLI_FAILURE;
        }
    }
    else if (pL2vpnCliArgs->u4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        if (nmhTestv2FsMplsL2VpnPwGenAGIType
            (&u4ErrorCode, u4PwIndex,
             pL2vpnCliArgs->u4GenAgiType) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenAGIType\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsL2VpnPwGenAGIType (u4PwIndex,
                                           pL2vpnCliArgs->u4GenAgiType) ==
            SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenAGIType\r\n");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsMplsL2VpnPwGenLocalAIIType
            (&u4ErrorCode, u4PwIndex,
             pL2vpnCliArgs->u4GenLocalAiiType) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenLocalAIIType\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex,
                                                pL2vpnCliArgs->
                                                u4GenLocalAiiType) ==
            SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenLocalAIIType\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMplsL2VpnPwGenRemoteAIIType
            (&u4ErrorCode, u4PwIndex,
             pL2vpnCliArgs->u4GenRemoteAiiType) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenRemoteAIIType\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMplsL2VpnPwGenRemoteAIIType (u4PwIndex,
                                                 pL2vpnCliArgs->
                                                 u4GenRemoteAiiType) ==
            SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenRemoteAIIType\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMplsL2VpnPwAIIFormat (&u4ErrorCode, u4PwIndex,
                                             &AIIFormat) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenAIIFormat\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsL2VpnPwAIIFormat (u4PwIndex, &AIIFormat) ==
            SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwGenAIIFormat\r\n");
            return CLI_FAILURE;
        }

        /* This Mode of Signalling is GEN FEC Signalling. So, test and set
         * mib objects like GroupAttachmentId, LocalAttachmentId (SAII),
         * and PeerAttachmentId (TAII) to enable GEN FEC Signalling   */
        if (pL2vpnCliArgs->Agi.pu1_OctetList != NULL)
        {
            if (nmhTestv2PwGroupAttachmentID (&u4ErrorCode, u4PwIndex,
                                              &pL2vpnCliArgs->Agi) ==
                SNMP_FAILURE)
            {
                PwDestroy (CliHandle, u4PwIndex);
                CliPrintf (CliHandle,
                           "\r%%Unable to set Attachment Group Id\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetPwGroupAttachmentID (u4PwIndex, &pL2vpnCliArgs->Agi)
                == SNMP_FAILURE)
            {
                PwDestroy (CliHandle, u4PwIndex);
                CliPrintf (CliHandle,
                           "\r%%Unable to set Attachment Group Id\r\n");
                return CLI_FAILURE;
            }
        }
        if (pL2vpnCliArgs->Saii.pu1_OctetList != NULL)
        {
            if (nmhTestv2PwLocalAttachmentID (&u4ErrorCode, u4PwIndex,
                                              &pL2vpnCliArgs->Saii) ==
                SNMP_FAILURE)
            {
                PwDestroy (CliHandle, u4PwIndex);
                CliPrintf (CliHandle, "\r%%Unable to set SAII\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetPwLocalAttachmentID (u4PwIndex, &pL2vpnCliArgs->Saii)
                == SNMP_FAILURE)
            {
                PwDestroy (CliHandle, u4PwIndex);
                CliPrintf (CliHandle, "\r%%Unable to set SAII\r\n");
                return CLI_FAILURE;
            }

            if (nmhTestv2PwPeerAttachmentID (&u4ErrorCode, u4PwIndex,
                                             &pL2vpnCliArgs->Taii) ==
                SNMP_FAILURE)
            {
                PwDestroy (CliHandle, u4PwIndex);
                CliPrintf (CliHandle, "\r%%Unable to set TAII\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetPwPeerAttachmentID (u4PwIndex, &pL2vpnCliArgs->Taii)
                == SNMP_FAILURE)
            {
                PwDestroy (CliHandle, u4PwIndex);
                CliPrintf (CliHandle, "\r%%Unable to set TAII\r\n");
                return CLI_FAILURE;
            }
        }
    }
    if ((pL2vpnCliArgs->u4PwOwner == L2VPN_PWVC_OWNER_MANUAL) ||
        ((pL2vpnCliArgs->u4LocalLabel != 0) &&
         (pL2vpnCliArgs->u4RemoteLabel != 0)))
    {
        /* Label configuration is allowed only for manual & GenFec-AII Type 2 PW.
         * So there is dependency of setting the following before configuring Labels.
         * Manual PW: PwOwner, 
         * GenFEC-AII Type2: LocalAII & RemoteAII Type */
        if (nmhTestv2PwInboundLabel
            (&u4ErrorCode, u4PwIndex, pL2vpnCliArgs->u4LocalLabel)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwInboundLabel\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetPwInboundLabel (u4PwIndex, pL2vpnCliArgs->u4LocalLabel)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwInboundLabel\r\n");
            return CLI_FAILURE;
        }

        /* Set Pw Out Bound Label */
        if (nmhTestv2PwOutboundLabel
            (&u4ErrorCode, u4PwIndex, pL2vpnCliArgs->u4RemoteLabel)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwOutboundLabel\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetPwOutboundLabel (u4PwIndex, pL2vpnCliArgs->u4RemoteLabel)
            == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle, "\r%%Unable to set PwOutboundLabel\r\n");
            return CLI_FAILURE;
        }
    }

    /* Set PwMode */
    if (SetL2VpnPwMode (CliHandle, u4PwIndex, pL2vpnCliArgs->i4PwMode) ==
        CLI_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%%Unable to set PwMode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef HVPLS_WANTED
/***************************************************************************
 * Function Name  : L2VpnCreateVplsPwBindEntry
 *  Description   : This routine Creates an Bind entry for a VPLS Entry
 *  Input(s)      : u4PwIndex : Pw Index
 *                  u1SplitHorizonStatus - set value to be spoke or mesh
 *                                         accordind to its status 
 *  Output(s)     : None.
 *  Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *      
 *****************************************************************************/
INT4
L2VpnCreateVplsPwBindEntry (UINT4 u4PwIndex, UINT1 u1SplitHorizonStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4VplsConfigIndex = 0;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    u4VplsConfigIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    if (nmhTestv2VplsPwBindRowStatus (&u4ErrorCode,
                                      u4VplsConfigIndex,
                                      u4PwIndex,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsPwBindRowStatus(CREATE_AND_WAIT)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
    if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                   u4PwIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsPwBindRowStatus(CREATE_AND_WAIT)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_OTHER)
    {
        if (nmhTestv2VplsPwBindConfigType (&u4ErrorCode,
                                           u4VplsConfigIndex,
                                           u4PwIndex,
                                           L2VPN_VPLS_PW_BIND_SIG_BGP) ==
            SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2VplsPwBindConfigType Failed\r\n",
                        __func__);
            if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                           u4PwIndex, DESTROY) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS BIND RBTree delete Failed \t\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
        i4RetStatus =
            nmhSetVplsPwBindConfigType (u4VplsConfigIndex,
                                        u4PwIndex, L2VPN_VPLS_PW_BIND_SIG_BGP);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsPwBindConfigType Failed\r\n", __func__);
            if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                           u4PwIndex, DESTROY) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS BIND RBTree delete Failed \t\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2VplsPwBindConfigType (&u4ErrorCode,
                                           u4VplsConfigIndex,
                                           u4PwIndex,
                                           L2VPN_VPLS_PW_BIND_SIG_LDP) ==
            SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2VplsPwBindConfigType Failed\r\n",
                        __func__);
            if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                           u4PwIndex, DESTROY) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS BIND RBTree delete Failed \t\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
        i4RetStatus =
            nmhSetVplsPwBindConfigType (u4VplsConfigIndex,
                                        u4PwIndex, L2VPN_VPLS_PW_BIND_SIG_LDP);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsPwBindConfigType Failed\r\n", __func__);
            if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                           u4PwIndex, DESTROY) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS BIND RBTree delete Failed \t\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
    }
    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_OTHER)
    {
        if (nmhTestv2VplsPwBindType (&u4ErrorCode,
                                     u4VplsConfigIndex,
                                     u4PwIndex,
                                     L2VPN_VPLS_PW_MESH) == SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2VplsPwBindType() Failed\r\n", __func__);
            if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                           u4PwIndex, DESTROY) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS BIND RBTree delete Failed \t\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
        i4RetStatus =
            nmhSetVplsPwBindType (u4VplsConfigIndex,
                                  u4PwIndex, L2VPN_VPLS_PW_MESH);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s :nmhSetVplsPwBindType () Failed\r\n", __func__);
            if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                           u4PwIndex, DESTROY) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : VPLS BIND RBTree delete Failed \t\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        if (u1SplitHorizonStatus == L2VPN_FALSE)
        {
            if (nmhTestv2VplsPwBindType (&u4ErrorCode,
                                         u4VplsConfigIndex,
                                         u4PwIndex,
                                         L2VPN_VPLS_PW_SPOKE) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhTestv2VplsPwBindType() Failed\r\n",
                            __func__);
                if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                               u4PwIndex,
                                               DESTROY) == SNMP_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : VPLS BIND RBTree delete Failed \t\n",
                                __func__);
                }
                return CLI_FAILURE;
            }
            i4RetStatus =
                nmhSetVplsPwBindType (u4VplsConfigIndex,
                                      u4PwIndex, L2VPN_VPLS_PW_SPOKE);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s :nmhSetVplsPwBindType () Failed\r\n", __func__);
                if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                               u4PwIndex,
                                               DESTROY) == SNMP_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : VPLS BIND RBTree delete Failed \t\n",
                                __func__);
                }
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2VplsPwBindType (&u4ErrorCode,
                                         u4VplsConfigIndex,
                                         u4PwIndex,
                                         L2VPN_VPLS_PW_MESH) == SNMP_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhTestv2VplsPwBindType() Failed\r\n",
                            __func__);
                if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                               u4PwIndex,
                                               DESTROY) == SNMP_FAILURE)

                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : VPLS BIND RBTree delete Failed \t\n",
                                __func__);
                }
                return CLI_FAILURE;
            }
            i4RetStatus =
                nmhSetVplsPwBindType (u4VplsConfigIndex,
                                      u4PwIndex, L2VPN_VPLS_PW_MESH);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s :nmhSetVplsPwBindType () Failed\r\n", __func__);
                if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                               u4PwIndex,
                                               DESTROY) == SNMP_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : VPLS BIND RBTree delete Failed \t\n",
                                __func__);
                }
                return CLI_FAILURE;
            }
        }
    }
    if (nmhTestv2VplsPwBindRowStatus (&u4ErrorCode,
                                      u4VplsConfigIndex,
                                      u4PwIndex, ACTIVE) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsPwBindRowStatus(ACTIVE)"
                    "Failed\r\n", __func__);
        if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                       u4PwIndex, DESTROY) == SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : VPLS BIND RBTree delete Failed \t\n", __func__);
        }
        return CLI_FAILURE;
    }
    i4RetStatus =
        nmhSetVplsPwBindRowStatus (u4VplsConfigIndex, u4PwIndex, ACTIVE);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsPwBindRowStatus(ACTIVE)"
                    "Failed\r\n", __func__);
        if (nmhSetVplsPwBindRowStatus (u4VplsConfigIndex,
                                       u4PwIndex, DESTROY) == SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : VPLS BIND RBTree delete Failed \t\n", __func__);
        }
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * Function Name  : L2VpnDelVplsPwBindEntry
 * Description    : This routine Deletes an Bind entry for a VPLS Entry
 *   Input(s)     : u4PwIndex : Pw Index
 *   Output(s)     : None.
 *  Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
L2VpnDelVplsPwBindEntry (UINT4 u4PwIndex)
{

    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4ErrorCode;
    INT4                i4RetStatus = SNMP_FAILURE;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {

        return CLI_FAILURE;
    }

    if (nmhTestv2VplsPwBindRowStatus (&u4ErrorCode,
                                      pPwVcEntry->u4VplsInstance,
                                      u4PwIndex, DESTROY) == SNMP_FAILURE)
    {

        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsPwBindRowStatus(DESTROY)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
    i4RetStatus =
        nmhSetVplsPwBindRowStatus (pPwVcEntry->u4VplsInstance,
                                   u4PwIndex, DESTROY);
    if (SNMP_FAILURE == i4RetStatus)
    {

        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsPwBindRowStatus(DESTROY)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
#endif

/****************************************************************************
* Function    :  L2VpnCliSetDebugLevel 
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L2VpnCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    L2VPN_DBG_LVL = 0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        L2VPN_DBG_LVL =
            L2VPN_DBG_LVL_CRT_FLAG | L2VPN_DBG_LVL_ERR_FLAG |
            L2VPN_DBG_LVL_DBG_FLAG | L2VPN_DBG_LVL_ALL_FLAG |
            L2VPN_DBG_FNC_SIG_FLAG | L2VPN_DBG_FNC_SSN_FLAG |
            L2VPN_DBG_FNC_VC_FLAG | L2VPN_DBG_FNC_CRT_FLAG |
            L2VPN_DBG_FNC_ALL_FLAG | L2VPN_DBG_IN_PKT_FLAG |
            L2VPN_DBG_OUT_PKT_FLAG | L2VPN_DBG_ALL_PKT_FLAG |
            L2VPN_DBG_ERR_SIG_FLAG | L2VPN_DBG_DBG_SIG_FLAG |
            L2VPN_DBG_ERR_SSN_FLAG | L2VPN_DBG_DBG_SSN_FLAG |
            L2VPN_DBG_ERR_VC_FLAG | L2VPN_DBG_DBG_VC_FLAG |
            L2VPN_DBG_VCCV_CR_TRC | L2VPN_DBG_VCCV_MGMT_TRC |
            L2VPN_DBG_VCCV_CAPAB_EXG_TRC | L2VPN_DBG_GRACEFUL_RESTART |
            L2VPN_DBG_ALL_FLAG;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        L2VPN_DBG_LVL = L2VPN_DBG_LVL_CRT_FLAG | L2VPN_DBG_LVL_DBG_FLAG |
            L2VPN_DBG_FNC_SIG_FLAG | L2VPN_DBG_FNC_SSN_FLAG |
            L2VPN_DBG_FNC_VC_FLAG | L2VPN_DBG_FNC_CRT_FLAG |
            L2VPN_DBG_IN_PKT_FLAG | L2VPN_DBG_OUT_PKT_FLAG |
            L2VPN_DBG_DBG_SIG_FLAG | L2VPN_DBG_DBG_SSN_FLAG |
            L2VPN_DBG_DBG_VC_FLAG | L2VPN_DBG_VCCV_CR_TRC |
            L2VPN_DBG_VCCV_MGMT_TRC | L2VPN_DBG_VCCV_CAPAB_EXG_TRC |
            L2VPN_DBG_GRACEFUL_RESTART;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        L2VPN_DBG_LVL = L2VPN_DBG_IN_PKT_FLAG | L2VPN_DBG_OUT_PKT_FLAG |
            L2VPN_DBG_DBG_SIG_FLAG | L2VPN_DBG_DBG_SSN_FLAG |
            L2VPN_DBG_DBG_VC_FLAG | L2VPN_DBG_VCCV_CR_TRC |
            L2VPN_DBG_VCCV_MGMT_TRC | L2VPN_DBG_VCCV_CAPAB_EXG_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        L2VPN_DBG_LVL = L2VPN_DBG_FNC_SIG_FLAG | L2VPN_DBG_FNC_SSN_FLAG |
            L2VPN_DBG_FNC_VC_FLAG | L2VPN_DBG_FNC_CRT_FLAG;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        L2VPN_DBG_LVL =
            L2VPN_DBG_LVL_ERR_FLAG | L2VPN_DBG_ERR_SIG_FLAG |
            L2VPN_DBG_ERR_SSN_FLAG | L2VPN_DBG_ERR_VC_FLAG;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        L2VPN_DBG_LVL =
            L2VPN_DBG_LVL_CRT_FLAG | L2VPN_DBG_FNC_CRT_FLAG |
            L2VPN_DBG_VCCV_CR_TRC | L2VPN_DBG_ERR_SIG_FLAG |
            L2VPN_DBG_ERR_VC_FLAG;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : PwOutboundTablePopulate
* Description   : This routine will populate the entries in pwOutboundTable. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable 
*                 pL2vpnCliArgs         - CLI arguments
* Output(s)     : If Success -CLI_SUCCESS
*                 or else Proper Error Mesage is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
PwOutboundTablePopulate (tCliHandle CliHandle, UINT4 u4PwIndex,
                         tL2vpnCliArgs * pL2vpnCliArgs)
{

    tSNMP_OCTET_STRING_TYPE TunnelLclLSR;
    tSNMP_OCTET_STRING_TYPE TunnelPeerLSR;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
#endif

    UINT4               u4OutTnlMode = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    UINT4               u4ErrorCode = 0;
    UINT4               i4IfIndex = 0;
    UINT4               u4InTnlIndex = 0;
    UINT4               u4Mask = 0xffffffff;
    BOOL1               bIsOutTnlP2MPTnl = OSIX_FALSE;
    BOOL1               bIsOutTnlReqd = TRUE;
    UINT1               au1LocalLsr[IPV4_ADDR_LENGTH];
    UINT1               au1PeerLsr[IPV4_ADDR_LENGTH];
    UINT4               u4LclLsr = 0;
    UINT4               u4PeerLsr = 0;
    INT1                ai1NullIpv6Addr[IPV6_ADDR_LENGTH];
    MEMSET (ai1NullIpv6Addr, 0, IPV6_ADDR_LENGTH);
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (au1LocalLsr, 0, IPV4_ADDR_LENGTH);
    MEMSET (au1PeerLsr, 0, IPV4_ADDR_LENGTH);
#ifdef MPLS_IPV6_WANTED
    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
#endif

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
    {
        NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
        NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &(pL2vpnCliArgs->PeerAddr.Ip6Addr));
        NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;
    }

    else if (MPLS_IPV4_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
#endif
    {
        RtQuery.u4DestinationIpAddress = pL2vpnCliArgs->PeerAddr.u4Addr;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    }

    TunnelLclLSR.pu1_OctetList = au1LocalLsr;
    TunnelLclLSR.i4_Length = IPV4_ADDR_LENGTH;
    TunnelPeerLSR.pu1_OctetList = au1PeerLsr;
    TunnelPeerLSR.i4_Length = IPV4_ADDR_LENGTH;

    /* If PwMpls Type = TE , set OutboundTunnelIndex, LocalLSR and PeerLSR */
    if ((pL2vpnCliArgs->u4Flag & CLI_MPLS_TE_CREATE) == CLI_MPLS_TE_CREATE)
    {
        /* Out Bound Table configuration */
        /* Pw mapped over P2MP tunnel - Out tunnel/ In-Tunnel can be zero */
        if (pL2vpnCliArgs->u4OutTunnelId != 0)
        {
            /* If the Out-destination tunnel and In-destination tunnel are configured,
             * the previous existance of Outgoing tunnel and Incoming tunnel is not required */
            if ((pL2vpnCliArgs->u4OutDestTunnelId != 0)
                && (pL2vpnCliArgs->u4InDestTunnelId != 0))
            {
                bIsOutTnlReqd = FALSE;
            }

            MPLS_CMN_LOCK ();

            if (pL2vpnCliArgs->u4OutSrcTunnelId != 0)
            {
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (pL2vpnCliArgs->u4OutTunnelId, 0,
                                                pL2vpnCliArgs->u4OutSrcTunnelId,
                                                pL2vpnCliArgs->
                                                u4OutDestTunnelId,
                                                TE_TNL_OWNER_SNMP, L2VPN_ZERO);
            }
            else
            {
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (pL2vpnCliArgs->u4OutTunnelId, 0,
                                                0,
                                                pL2vpnCliArgs->
                                                u4OutDestTunnelId,
                                                TE_TNL_OWNER_SNMP, L2VPN_ZERO);
                if (pTeTnlInfo == NULL)
                {
                    pTeTnlInfo =
                        TeGetTnlInfoByOwnerAndRole (pL2vpnCliArgs->
                                                    u4OutTunnelId, 0, 0,
                                                    pL2vpnCliArgs->
                                                    u4OutDestTunnelId,
                                                    TE_TNL_OWNER_RSVP,
                                                    L2VPN_ZERO);
                }
            }

            if (bIsOutTnlReqd == TRUE)
            {

                /* Tunnel should be already created. Copy the local and peer LSR ID 
                 * from the tunnel info */
                if (pTeTnlInfo == NULL)
                {
                    CliPrintf (CliHandle, "\r%%Tunnel Does Not Exist\r\n");
                    MPLS_CMN_UNLOCK ();
                    return CLI_FAILURE;
                }
                if (!
                    ((pTeTnlInfo->u1TnlRole == TE_INGRESS)
                     ||
                     ((pTeTnlInfo->u4TnlMode ==
                       TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                      && (pTeTnlInfo->u1TnlRole == TE_EGRESS))))
                {
                    CliPrintf (CliHandle,
                               "\r%%Invalid Outgoing Tunnel association to the PW\r\n");
                    MPLS_CMN_UNLOCK ();
                    return CLI_FAILURE;
                }
                u4OutTnlMode = pTeTnlInfo->u4TnlMode;

                u4InTnlIndex = pTeTnlInfo->u4DestTnlIndex;
                TunnelLclLSR.pu1_OctetList = pTeTnlInfo->TnlIngressLsrId;

                TunnelLclLSR.i4_Length = IPV4_ADDR_LENGTH;

                TunnelPeerLSR.pu1_OctetList = pTeTnlInfo->TnlEgressLsrId;
                TunnelPeerLSR.i4_Length = IPV4_ADDR_LENGTH;

                if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP) &&
                    (NULL != TE_P2MP_TNL_INFO (pTeTnlInfo)))
                {
                    bIsOutTnlP2MPTnl = OSIX_TRUE;
                }

                MPLS_CMN_UNLOCK ();
            }
            else
            {
                u4LclLsr = OSIX_HTONL (pL2vpnCliArgs->u4OutSrcTunnelId);
                MEMCPY (TunnelLclLSR.pu1_OctetList, &u4LclLsr,
                        IPV4_ADDR_LENGTH);

                u4PeerLsr = OSIX_HTONL (pL2vpnCliArgs->u4OutDestTunnelId);
                MEMCPY (TunnelPeerLSR.pu1_OctetList, &u4PeerLsr,
                        IPV4_ADDR_LENGTH);
                MPLS_CMN_UNLOCK ();
            }

            /* Set Pw Mpls Out Bound Tunnel Index */

            if (nmhTestv2PwMplsOutboundTunnelIndex
                (&u4ErrorCode, u4PwIndex,
                 pL2vpnCliArgs->u4OutTunnelId) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set PwMplsOutboundTunnelIndex\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetPwMplsOutboundTunnelIndex
                (u4PwIndex, pL2vpnCliArgs->u4OutTunnelId) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set PwMplsOutboundTunnelIndex\r\n");
                return CLI_FAILURE;
            }

            /* Set Pw Mpls Out Bound Tunnel Lcl LSR */

            if (nmhTestv2PwMplsOutboundTunnelLclLSR
                (&u4ErrorCode, u4PwIndex, &TunnelLclLSR) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set PwMplsOutboundTunnelLclLSR\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetPwMplsOutboundTunnelLclLSR
                (u4PwIndex, &TunnelLclLSR) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set PwMplsOutboundTunnelLclLSR\r\n");
                return CLI_FAILURE;
            }

            /* Set Pw Mpls Out Bound Tunnel Peer LSR */

            if (nmhTestv2PwMplsOutboundTunnelPeerLSR
                (&u4ErrorCode, u4PwIndex, &TunnelPeerLSR) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set PwMplsOutboundTunnelPeerLSR\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetPwMplsOutboundTunnelPeerLSR
                (u4PwIndex, &TunnelPeerLSR) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set PwMplsOutboundTunnelPeerLSR\r\n");
                return CLI_FAILURE;
            }
        }

        /* PW In Bound Table configuration */
        if (pL2vpnCliArgs->u4InTunnelId == L2VPN_ZERO)
        {
            if (u4OutTnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
            {
                pL2vpnCliArgs->u4InTunnelId = pL2vpnCliArgs->u4OutTunnelId;
            }
            else if ((u4OutTnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL) &&
                     (u4InTnlIndex != 0))
            {
                pL2vpnCliArgs->u4InTunnelId = u4InTnlIndex;
            }
            /* Pw mapped over P2MP tunnel - 
               if Out tunnel is P2MP then In-Tunnel should be zero */
            else if (bIsOutTnlP2MPTnl == OSIX_TRUE)
            {
                return CLI_SUCCESS;
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%%In Tunnel-Id is not configured \r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            /* In-Tunnel is configured */
            if ((bIsOutTnlP2MPTnl == OSIX_TRUE) && (u4OutTnlMode ==
                                                    TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
            {
                CliPrintf (CliHandle,
                           "\r%%Pw mapped over P2MP tunnel should"
                           " be uni-directional\r\n");
                return CLI_FAILURE;
            }
            if ((u4OutTnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
                (pL2vpnCliArgs->u4InTunnelId != pL2vpnCliArgs->u4OutTunnelId))
            {
                CliPrintf (CliHandle,
                           "\r%%In-tunnel Id & Out-tunnel Id are not"
                           "same for co-routed bi-directional tunnel\r\n");
                return CLI_FAILURE;
            }
            else if ((u4OutTnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL) &&
                     (pL2vpnCliArgs->u4InTunnelId != u4InTnlIndex))
            {
                CliPrintf (CliHandle,
                           "\r%%In-tunnel Id & tunnel associated "
                           "reverse tunnel Id are not same for "
                           "associated bi-directional tunnel\r\n");
                return CLI_FAILURE;
            }
        }
        if (bIsOutTnlReqd == FALSE)
        {
            u4LclLsr = OSIX_HTONL (pL2vpnCliArgs->u4InDestTunnelId);
            MEMCPY (TunnelLclLSR.pu1_OctetList, &u4LclLsr, IPV4_ADDR_LENGTH);

            u4PeerLsr = OSIX_HTONL (pL2vpnCliArgs->u4InSrcTunnelId);
            MEMCPY (TunnelPeerLSR.pu1_OctetList, &u4PeerLsr, IPV4_ADDR_LENGTH);

        }
        /* No action is required for co-routed bidirectional tunnel as 
         * ingressId and egressId are same on all traversing nodes */
        else if (u4OutTnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        {
            /* Tunnel mode is associated bi-directional. Ingress and
             * Egress Lsr Id should be swapped. */
            TunnelLclLSR.pu1_OctetList = pTeTnlInfo->TnlEgressLsrId;
            TunnelLclLSR.i4_Length = IPV4_ADDR_LENGTH;
            TunnelPeerLSR.pu1_OctetList = pTeTnlInfo->TnlIngressLsrId;
            TunnelPeerLSR.i4_Length = IPV4_ADDR_LENGTH;
        }
        else if (u4OutTnlMode == TE_TNL_MODE_UNIDIRECTIONAL)
        {
            /* Tunnel mode is unidirectional. Get InTunnel information  */
            MPLS_CMN_LOCK ();
            if (pL2vpnCliArgs->u4InSrcTunnelId != 0)
            {
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (pL2vpnCliArgs->u4InTunnelId, 0,
                                                pL2vpnCliArgs->u4InSrcTunnelId,
                                                pL2vpnCliArgs->u4InDestTunnelId,
                                                L2VPN_ZERO, L2VPN_ZERO);
            }
            else
            {
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (pL2vpnCliArgs->u4InTunnelId, 0,
                                                0, 0, L2VPN_ZERO, TE_EGRESS);
            }
            if (pTeTnlInfo != NULL)
            {
                if (!
                    ((pTeTnlInfo->u1TnlRole == TE_EGRESS)
                     ||
                     ((pTeTnlInfo->u4TnlMode ==
                       TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                      && (pTeTnlInfo->u1TnlRole == TE_INGRESS))))
                {
                    CliPrintf (CliHandle,
                               "\r%%Invalid Incoming Tunnel association to the PW\r\n");
                    MPLS_CMN_UNLOCK ();
                    return CLI_FAILURE;
                }
                TunnelLclLSR.pu1_OctetList = pTeTnlInfo->TnlEgressLsrId;
                TunnelLclLSR.i4_Length = IPV4_ADDR_LENGTH;
                TunnelPeerLSR.pu1_OctetList = pTeTnlInfo->TnlIngressLsrId;
                TunnelPeerLSR.i4_Length = IPV4_ADDR_LENGTH;
            }
            MPLS_CMN_UNLOCK ();
        }

        if (nmhTestv2FsPwMplsInboundTunnelIndex
            (&u4ErrorCode, u4PwIndex,
             pL2vpnCliArgs->u4InTunnelId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Cannot set PwMplsInboundTunnelId\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsPwMplsInboundTunnelIndex
            (u4PwIndex, pL2vpnCliArgs->u4InTunnelId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to set PwMplsInboundTunnelId\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsPwMplsInboundTunnelEgressLSR
            (&u4ErrorCode, u4PwIndex, &TunnelLclLSR) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Cannot set TunnelEgressLSR\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsPwMplsInboundTunnelEgressLSR
            (u4PwIndex, &TunnelLclLSR) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set TunnelEgressLSR\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsPwMplsInboundTunnelIngressLSR
            (&u4ErrorCode, u4PwIndex, &TunnelPeerLSR) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Cannot set TunnelIngressLSR\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsPwMplsInboundTunnelIngressLSR
            (u4PwIndex, &TunnelPeerLSR) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set TunnelIngressLSR\r\n");
            return CLI_FAILURE;
        }
    }

    /* If pwMplstype is of type VConly , set Outbound If Index */
    if ((pL2vpnCliArgs->u4Flag & CLI_MPLS_VCONLY_CREATE) ==
        CLI_MPLS_VCONLY_CREATE)
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
        {
            if (MEMCMP (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                        ai1NullIpv6Addr, IPV6_ADDR_LENGTH) == L2VPN_ZERO)
            {
                return CLI_FAILURE;
            }
            if (NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                                 &NetIpv6RtInfo) != NETIPV6_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Failed in Query'ng Route for Peer\n");
                return CLI_FAILURE;
            }
            else
            {
                if (NetIpv6GetCfaIfIndexFromPort (NetIpv6RtInfo.u4Index,
                                                  &i4IfIndex) ==
                    NETIPV6_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%%Peer is not reachable\r\n");
                    return CLI_FAILURE;
                }
            }
        }

        else if (MPLS_IPV4_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
#endif
        {
            if (pL2vpnCliArgs->PeerAddr.u4Addr == L2VPN_ZERO)
            {
                return CLI_FAILURE;
            }

            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Failed in Query'ng Route for Peer\n");
                return CLI_FAILURE;
            }
            else
            {
                if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
                {
                    CliPrintf (CliHandle, "\r%% Peer Not directly connected\n");
                    return CLI_FAILURE;
                }
                if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                                  &i4IfIndex) ==
                    NETIPV4_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%%Peer is not reachable\r\n");
                    return CLI_FAILURE;
                }
            }
        }
        if (nmhTestv2PwMplsOutboundIfIndex
            (&u4ErrorCode, u4PwIndex, i4IfIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to set PwMplsOutboundIfIndex\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetPwMplsOutboundIfIndex (u4PwIndex, i4IfIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to set PwMplsOutboundIfIndex\r\n");
            return CLI_FAILURE;
        }
    }
    else if ((pL2vpnCliArgs->u4Flag & CLI_MPLS_NON_TE_CREATE) ==
             CLI_MPLS_NON_TE_CREATE)
    {
        if (pL2vpnCliArgs->u4EntityId != L2VPN_ZERO)
        {
            if (L2VpnCliSetPwMplsNonTeEntityId (u4PwIndex,
                                                pL2vpnCliArgs->u4EntityId) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Unable to set NON-TE Entity Id\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : PwEnetTablePopulate
* Description   : This routine will populate the entries in pwEnetTable. 
*                 vpws entries. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwEnetPwVlan          - Defines the VLAN field
*                                          value on the pw.
*                 u4PwEnetPortVlan - Defines the VLAN value on the physical port.
*                 i4PwEnetVlanMode - Indicates the mode of vlan. 
*                 i4PwVlanId - Vlan Id
* Output(s)     : If Success -CLI_SUCCESS
*                 or else Proper Error Mesage is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
PwEnetTablePopulate (tCliHandle CliHandle, UINT4 u4PwIndex,
                     UINT4 u4PwEnetPwVlan, UINT4 u4PwEnetPortVlan,
                     INT4 i4PwEnetVlanMode, INT4 i4IfIndex)
{

    UINT4               u4ErrorCode = 0;
    UINT4               u4PwEnetPwInstance = 1;
    INT4                i4PwEnetPortIfIndex = i4IfIndex;

    /* Set Pw Enet Pw Row Status to create and wait */
    if (nmhTestv2PwEnetRowStatus
        (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
         MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create attachment circuit\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwEnetRowStatus
        (u4PwIndex, u4PwEnetPwInstance,
         MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create attachment circuit\r\n");
        return CLI_FAILURE;
    }

    /* Set Pw Enet Pw Vlan */
    if (u4PwEnetPwVlan != 0)
    {
        if (nmhTestv2PwEnetPwVlan
            (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
             u4PwEnetPwVlan) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwEnetPwVlan\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwEnetPwVlan
            (u4PwIndex, u4PwEnetPwInstance, u4PwEnetPwVlan) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwEnetPwVlan\r\n");
            return CLI_FAILURE;
        }
    }
    /* Set Pw Enet Vlan Mode  */

    if (nmhTestv2PwEnetVlanMode
        (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
         i4PwEnetVlanMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set PwEnetVlanMode\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwEnetVlanMode
        (u4PwIndex, u4PwEnetPwInstance, i4PwEnetVlanMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set PwEnetVlanMode\r\n");
        return CLI_FAILURE;
    }

    /* Set Pw Enet Port Vlan   */
    if (u4PwEnetPortVlan != L2VPN_ZERO)
    {
        if (nmhTestv2PwEnetPortVlan
            (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
             u4PwEnetPortVlan) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwEnetPortVlan\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwEnetPortVlan
            (u4PwIndex, u4PwEnetPwInstance, u4PwEnetPortVlan) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwEnetPortVlan\r\n");
            return CLI_FAILURE;
        }
    }
    /* Set Pw Enet Port If Index   */
    if (i4PwEnetPortIfIndex != L2VPN_ZERO)
    {
        if (nmhTestv2PwEnetPortIfIndex
            (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
             i4PwEnetPortIfIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwEnetPortIfIndex\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwEnetPortIfIndex
            (u4PwIndex, u4PwEnetPwInstance,
             i4PwEnetPortIfIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwEnetPortIfIndex\r\n");
            return CLI_FAILURE;
        }
    }
    /* Set Pw Enet Pw Row Status to active */
    if (nmhTestv2PwEnetRowStatus
        (&u4ErrorCode, u4PwIndex, u4PwEnetPwInstance,
         MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to activate attachment circuit\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwEnetRowStatus
        (u4PwIndex, u4PwEnetPwInstance, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to activate attachment circuit\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : PwMplsTypeSet
* Description   : This routine will set the pw Mpls Type. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable 
*                 u4Flag                - Indicates whether the cli command is 
*                                         in the TE mode or Vconly mode.
* Output(s)     : If Success -CLI_SUCCESS
*                 or else Proper Error Mesage is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
PwMplsTypeSet (tCliHandle CliHandle, UINT4 u4PwIndex, UINT4 u4Flag)
{
    tSNMP_OCTET_STRING_TYPE MplsMplsType;
    static UINT1        au1MplsMplsType[L2VPN_ONE];

    UINT4               u4ErrorCode = 0;

    MplsMplsType.pu1_OctetList = au1MplsMplsType;
    MplsMplsType.i4_Length = 0;

    if ((u4Flag & CLI_MPLS_TE_CREATE) == CLI_MPLS_TE_CREATE)
    {
        au1MplsMplsType[0] = L2VPN_MPLS_TYPE_TE;
    }

    if ((u4Flag & CLI_MPLS_VCONLY_CREATE) == CLI_MPLS_VCONLY_CREATE)
    {
        au1MplsMplsType[0] = L2VPN_MPLS_TYPE_VCONLY;
    }

    if ((u4Flag & CLI_MPLS_NON_TE_CREATE) == CLI_MPLS_NON_TE_CREATE)
    {
        au1MplsMplsType[0] = L2VPN_MPLS_TYPE_NONTE;
    }

    MplsMplsType.pu1_OctetList = au1MplsMplsType;
    MplsMplsType.i4_Length = STRLEN (au1MplsMplsType);

    if (nmhTestv2PwMplsMplsType
        (&u4ErrorCode, u4PwIndex, &MplsMplsType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set PW MplsMplsType\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwMplsMplsType (u4PwIndex, &MplsMplsType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set PW MplsMplsType\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : PwDestroy
* Description   : This routine will destroy the pw Entry. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable 
* Output(s)     : If Success -CLI_SUCCESS
*                 or else Proper Error Mesage is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
PwDestroy (tCliHandle CliHandle, UINT4 u4PwIndex)
{
    UINT4               u4ErrorCode = 0;
#ifdef HVPLS_WANTED
    tPwVcEntry         *pPwVcEntry = NULL;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
        {

            if (L2VpnDelVplsPwBindEntry (u4PwIndex) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Can't Delete VPLS Bind Entry \r\n");
                return CLI_FAILURE;
            }
        }
    }
#endif
    if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, MPLS_STATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Pseudowire is associated with BFD/MS-PW."
                   " Remove the association and then delete the Pseudowire.\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwRowStatus (u4PwIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy the pw Entry\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : CliShowPwVCStatistics
* Description   : This routine will display the Pw VC Statistics. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable 
* Output(s)     : Displays the pw VC statistics on the screen.
* Return(s)     : NONE
*******************************************************************************/
VOID
CliShowPwVCStatistics (tCliHandle CliHandle, UINT4 u4PwIndex)
{
    tSNMP_COUNTER64_TYPE RetValPwPerfCurrentInHCPackets;
    tSNMP_COUNTER64_TYPE RetValPwPerfCurrentOutHCPackets;
    tSNMP_COUNTER64_TYPE RetValPwPerfCurrentOutHCBytes;
    tSNMP_COUNTER64_TYPE RetValPwPerfCurrentInHCBytes;

    UINT1               au1RetValPwPerfCurrentInHCPackets[MAX_U8_STRING_SIZE];
    UINT1               au1RetValPwPerfCurrentOutHCPackets[MAX_U8_STRING_SIZE];
    UINT1               au1RetValPwPerfCurrentInHCBytes[MAX_U8_STRING_SIZE];
    UINT1               au1RetValPwPerfCurrentOutHCBytes[MAX_U8_STRING_SIZE];

    FS_UINT8            u8RetValPwPerfCurrentInHCPackets;
    FS_UINT8            u8RetValPwPerfCurrentOutHCPackets;
    FS_UINT8            u8RetValPwPerfCurrentOutHCBytes;
    FS_UINT8            u8RetValPwPerfCurrentInHCBytes;
    FS_UINT8            u8Zero;

    UINT4               u4RetValPwPerfTotalErrorPackets = 0;

    MEMSET (&RetValPwPerfCurrentInHCPackets, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValPwPerfCurrentOutHCPackets, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValPwPerfCurrentOutHCBytes, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValPwPerfCurrentInHCBytes, 0, sizeof (tSNMP_COUNTER64_TYPE));

    FSAP_U8_CLR (&u8RetValPwPerfCurrentInHCBytes);
    FSAP_U8_CLR (&u8RetValPwPerfCurrentOutHCBytes);
    FSAP_U8_CLR (&u8RetValPwPerfCurrentInHCPackets);
    FSAP_U8_CLR (&u8RetValPwPerfCurrentOutHCPackets);
    FSAP_U8_CLR (&u8Zero);

    /* Get VC Statistics */

    if (nmhGetPwPerfCurrentInHCPackets (u4PwIndex,
                                        &RetValPwPerfCurrentInHCPackets)
        == SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetPwPerfCurrentOutHCPackets (u4PwIndex,
                                         &RetValPwPerfCurrentOutHCPackets)
        == SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetPwPerfCurrentOutHCBytes
        (u4PwIndex, &RetValPwPerfCurrentOutHCBytes) == SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetPwPerfCurrentInHCBytes (u4PwIndex, &RetValPwPerfCurrentInHCBytes)
        == SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetPwPerfTotalErrorPackets (&u4RetValPwPerfTotalErrorPackets)
        == SNMP_FAILURE)
    {
        return;
    }

    FSAP_U8_ASSIGN_HI (&u8RetValPwPerfCurrentInHCPackets,
                       RetValPwPerfCurrentInHCPackets.msn);
    FSAP_U8_ASSIGN_LO (&u8RetValPwPerfCurrentInHCPackets,
                       RetValPwPerfCurrentInHCPackets.lsn);
    FSAP_U8_ASSIGN_HI (&u8RetValPwPerfCurrentOutHCPackets,
                       RetValPwPerfCurrentOutHCPackets.msn);
    FSAP_U8_ASSIGN_LO (&u8RetValPwPerfCurrentOutHCPackets,
                       RetValPwPerfCurrentOutHCPackets.lsn);
    FSAP_U8_ASSIGN_HI (&u8RetValPwPerfCurrentInHCBytes,
                       RetValPwPerfCurrentInHCBytes.msn);
    FSAP_U8_ASSIGN_LO (&u8RetValPwPerfCurrentInHCBytes,
                       RetValPwPerfCurrentInHCBytes.lsn);
    FSAP_U8_ASSIGN_HI (&u8RetValPwPerfCurrentOutHCBytes,
                       RetValPwPerfCurrentOutHCBytes.msn);
    FSAP_U8_ASSIGN_LO (&u8RetValPwPerfCurrentOutHCBytes,
                       RetValPwPerfCurrentOutHCBytes.lsn);

    FSAP_U8_2STR (&u8RetValPwPerfCurrentInHCPackets,
                  (CHR1 *) au1RetValPwPerfCurrentInHCPackets);
    FSAP_U8_2STR (&u8RetValPwPerfCurrentOutHCPackets,
                  (CHR1 *) au1RetValPwPerfCurrentOutHCPackets);
    FSAP_U8_2STR (&u8RetValPwPerfCurrentInHCBytes,
                  (CHR1 *) au1RetValPwPerfCurrentInHCBytes);
    FSAP_U8_2STR (&u8RetValPwPerfCurrentOutHCBytes,
                  (CHR1 *) au1RetValPwPerfCurrentOutHCBytes);

    CliPrintf (CliHandle, " VC statistics:\n");
    if ((FSAP_U8_CMP (&u8RetValPwPerfCurrentOutHCPackets, &u8Zero) == 0)
        && (FSAP_U8_CMP (&u8RetValPwPerfCurrentInHCPackets, &u8Zero) == 0))
    {
        CliPrintf (CliHandle, "    packet totals: receive " "0, send  0\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " packet totals       : receive %s, send %s\r\n",
                   au1RetValPwPerfCurrentInHCPackets,
                   au1RetValPwPerfCurrentOutHCPackets);
    }
    if ((FSAP_U8_CMP (&u8RetValPwPerfCurrentOutHCBytes, &u8Zero) == 0)
        && (FSAP_U8_CMP (&u8RetValPwPerfCurrentInHCBytes, &u8Zero) == 0))
    {
        CliPrintf (CliHandle, "    byte totals:   receive " "0, send  0\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " byte   totals       : receive %s, send %s\r\n",
                   au1RetValPwPerfCurrentInHCBytes,
                   au1RetValPwPerfCurrentOutHCBytes);
    }
    CliPrintf (CliHandle, "    packet drops:  %u \r\n\n",
               u4RetValPwPerfTotalErrorPackets);
}

/******************************************************************************
* Function Name : CliShowPwDetail
* Description   : This routine will display the detailed info of Pw Entries. 
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable 
*                 u4Flag                - Indicates whether detailed or
*                                         summary info is to be displayed.
*                 uGenU4Addr            - Contains value of the peer node 
*                                         address
* Output(s)     : Displays the pw VC statistics on the screen.
* Return(s)     : CLI_SUCCESS/CLI_FAILURE 
*******************************************************************************/
INT4
CliShowPwDetail (tCliHandle CliHandle, UINT4 u4PwIndex,
                 UINT4 u4PwEnetPwInstance, tGenU4Addr GenU4Addr, UINT4 u4Flag)
{
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE PwMplsType;
    tSNMP_OCTET_STRING_TYPE Saii;
    tSNMP_OCTET_STRING_TYPE Taii;
    tSNMP_OCTET_STRING_TYPE Agi;
    tSNMP_OCTET_STRING_TYPE AiiFormat;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
#ifdef MPLS_IPV6_WANTED
    tUtlIn6Addr         Ipv6Addr;
#endif
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1PwMplsType[L2VPN_ONE];
    static UINT1        au1PeerAddr[IPV6_ADDR_LENGTH];

    UINT1               au1LocalIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1OutputIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Saii[L2VPN_PWVC_MAX_AI_TYPE2_LEN + 1];    /*Allocating maximum size for
                                                                       Saii and Taii (i.e) Length of Aii type 2 */
    UINT1               au1Taii[L2VPN_PWVC_MAX_AI_TYPE2_LEN + 1];
    UINT1               au1Agi[L2VPN_PWVC_MAX_AGI_LEN + 1];
    UINT1               au1AiiFormat[MPLS_ONE] = { 0 };
    INT1                ai1PwLastChange[MAX_CLI_TIME_STRING];
    INT1                ai1PwCreateTime[MAX_CLI_TIME_STRING];
    UINT1               u1OperStatus = 0;
    CHR1               *pu1String = NULL;
    INT1               *pi1LocalIfName = NULL;
    INT1               *pi1OutputIfName = NULL;
    INT4                i4PwPeerAddrType = 0;
    INT4                i4PwOutIfIndex = 0;
    UINT4               u4RetValPwMplsOutboundLsrXcIndex = 0;
    UINT4               u4RetValPwRemoteIfMtu = 0;
    UINT4               u4RetValPwLocalIfMtu = 0;
    UINT4               u4RetValPwInboundLabel = 0;
    UINT4               u4RetValPwOutboundLabel = 0;
    UINT4               u4RetValPwLocalGroupID = 0;
    UINT4               u4RetValPwRemoteGroupID = 0;
    UINT4               u4RetValPwLastChange = 0;
    UINT4               u4RetValPwCreateTime = 0;
    UINT4               u4RetValMplsOutSegmentTopLabel = L2VPN_INVALID_LABEL;
    UINT4               u4PsnVlan = 0;
    UINT4               u4PwEnetPortVlan = 0;
    UINT4               u4PwMplsOutboundTunnelIndex = 0;
    UINT4               u4PwMplsOutboundTunnelInst = 0;
    UINT4               u4PwMplsOutboundTunnelIngId = 0;
    UINT4               u4PwMplsOutboundTunnelEgrId = 0;
    UINT1               u1PwMplsType = 0;
    UINT4               u4PwId = 0;
    UINT4               u4StackLabel = 0;
    UINT4               u4GlobalId = 0;
    UINT4               u4NodeId = 0;
    UINT4               u4AcId = 0;
    UINT4               u4LocalAiiType = 0;
    UINT4               u4RemoteAiiType = 0;
    eDirection          Direction = MPLS_DIRECTION_FORWARD;
    UINT4               u4DestId = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4L3Intf = 0;
    INT4                i4PwOperStatus = 0;
    INT4                i4AttchdPwOperStatus = 0;    /* MS-PW */
    INT4                i4RetValPwFcsRetentionStatus = 0;
    INT4                i4PwEnetPortIfIndex = 0;
    INT4                i4PwOwner = 0;
    CHR1                ac1LocalCt[MAX_U8_STRING_SIZE];
    CHR1               *pP2mpDestIp = NULL;
    CHR1               *pAiiString = NULL;
    BOOL1               bStackFlag = FALSE;
    BOOL1               bIsP2MPTnl = FALSE;
    BOOL1               bStatusPrinted = FALSE;
    UINT4               u4SrcAii = 0;
    UINT4               u4TargetAii = 0;
    INT4                i4IsStaticPw = L2VPN_ZERO;
    UINT4               u4CfaIfIndex = 0;
    UINT1               au1CfaIfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE MplsOutboundTunnelLclLSR;
    tSNMP_OCTET_STRING_TYPE MplsOutboundTunnelPeerLSR;
    static UINT1        au1MplsOutboundTunnelLclLSR[IPV4_ADDR_LENGTH];
    static UINT1        au1MplsOutboundTunnelPeerLSR[IPV4_ADDR_LENGTH];
#ifdef VPLSADS_WANTED
    UINT1               u1VpnId = L2VPN_ZERO;
#endif

    MEMSET (ac1LocalCt, 0, MAX_U8_STRING_SIZE);
    MEMSET (au1LocalIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1OutputIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1CfaIfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    MEMSET (au1Saii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN + 1);
    MEMSET (au1Taii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN + 1);
    MEMSET (au1Agi, 0, L2VPN_PWVC_MAX_AGI_LEN + 1);

    MplsOutboundTunnelLclLSR.pu1_OctetList = au1MplsOutboundTunnelLclLSR;
    MplsOutboundTunnelLclLSR.i4_Length = 0;

    MplsOutboundTunnelPeerLSR.pu1_OctetList = au1MplsOutboundTunnelPeerLSR;
    MplsOutboundTunnelPeerLSR.i4_Length = 0;

    MplsOutboundTunnelLclLSR.pu1_OctetList = au1MplsOutboundTunnelLclLSR;
    MplsOutboundTunnelLclLSR.i4_Length = 0;

    pi1LocalIfName = (INT1 *) au1LocalIfName;
    pi1OutputIfName = (INT1 *) au1OutputIfName;

    XCIndex.pu1_OctetList = au1XCIndex;
    PwMplsType.pu1_OctetList = au1PwMplsType;
    Saii.pu1_OctetList = au1Saii;
    Taii.pu1_OctetList = au1Taii;
    Agi.pu1_OctetList = au1Agi;
    AiiFormat.pu1_OctetList = au1AiiFormat;
    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerAddr.i4_Length = IPV4_ADDR_LENGTH;
    Saii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE2_LEN;
    Taii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE2_LEN;
    Agi.i4_Length = L2VPN_PWVC_MAX_AGI_LEN;
    AiiFormat.i4_Length = sizeof (UINT1);

    /* Get Local Interface */
    if (u4PwEnetPwInstance != L2VPN_ZERO)
    {
        if (nmhGetPwEnetPortIfIndex (u4PwIndex, u4PwEnetPwInstance,
                                     &i4PwEnetPortIfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetPwEnetPortVlan
            (u4PwIndex, u4PwEnetPwInstance, &u4PwEnetPortVlan) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
        {                        /* For L2 VLAN Name display */
            SNPRINTF ((CHR1 *) pi1LocalIfName, CFA_MAX_PORT_NAME_LENGTH,
                      "vlan%u", u4PwEnetPortVlan);
        }
        else
        {
            CfaCliGetIfName ((UINT4) i4PwEnetPortIfIndex, pi1LocalIfName);
        }
    }
    else
    {
        STRNCPY ((CHR1 *) pi1LocalIfName, "-", STRLEN ("-"));
        pi1LocalIfName[STRLEN ("-")] = '\0';
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if ((CfaUtilGetIfIndexFromMplsTnlIf (pPwVcEntry->u4PwL3Intf,
                                         &u4PsnVlan, TRUE) == CFA_SUCCESS) &&
        (u4PsnVlan != L2VPN_ZERO))
    {
        CfaCliGetIfName (u4PsnVlan, pi1OutputIfName);
    }
    else
    {
        if (nmhGetPwMplsOutboundIfIndex (u4PwIndex, &i4PwOutIfIndex) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        CfaCliGetIfName ((UINT4) i4PwOutIfIndex, pi1OutputIfName);
    }

    /* Get Line Protocol Status */
    if (i4PwEnetPortIfIndex != 0)
    {
        CfaGetIfOperStatus ((UINT4) i4PwEnetPortIfIndex, &u1OperStatus);
    }

    /* To display Peer Addr in Ip Addr format */

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY (Ipv6Addr.u1addr, GenU4Addr.Addr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH);
    }

    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, GenU4Addr.Addr.u4Addr);
    }

    /* Get Admin Status */
    if (nmhGetPwOperStatus (u4PwIndex, &i4PwOperStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* MS-PW Get attached PW VC oper status */
    if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != L2VPN_ZERO)
    {
        if (nmhGetPwOperStatus (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry),
                                &i4AttchdPwOperStatus) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhGetPwID (u4PwIndex, &u4PwId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetPwPeerAddrType (u4PwIndex, &i4PwPeerAddrType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get the Pseudo wire Owner Type */
    if (nmhGetPwOwner (u4PwIndex, &i4PwOwner) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*Get Local AII Type and Remote AII Type */
    if (nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex, &u4LocalAiiType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsMplsL2VpnPwGenRemoteAIIType (u4PwIndex, &u4RemoteAiiType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get pw Create Time and convert it to string to display 
     * in system time format */
    if (nmhGetPwLastChange (u4PwIndex, &u4RetValPwLastChange) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetPwCreateTime (u4PwIndex, &u4RetValPwCreateTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliMplsConvertTimetoString (u4RetValPwLastChange, ai1PwLastChange);
    CliMplsConvertTimetoString (u4RetValPwCreateTime, ai1PwCreateTime);

    /*  Get Tunnel Signalling Proto
     *  Get the TunnelId,TunnelInstance,Ingress and Egress ID is got using the TunnelName */
    if (nmhGetPwMplsMplsType (u4PwIndex, &PwMplsType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    u1PwMplsType = PwMplsType.pu1_OctetList[0];
    if (u1PwMplsType == L2VPN_MPLS_TYPE_TE)
    {
        /* TE */
        if (nmhGetPwMplsOutboundTunnelIndex (u4PwIndex,
                                             &u4PwMplsOutboundTunnelIndex)
            == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (nmhGetPwMplsOutboundTunnelInstance (u4PwIndex,
                                                &u4PwMplsOutboundTunnelInst)
            == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (nmhGetPwMplsOutboundTunnelLclLSR (u4PwIndex,
                                              &MplsOutboundTunnelLclLSR)
            == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (nmhGetPwMplsOutboundTunnelPeerLSR (u4PwIndex,
                                               &MplsOutboundTunnelPeerLSR)
            == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        MEMCPY (&u4PwMplsOutboundTunnelIngId,
                MplsOutboundTunnelLclLSR.pu1_OctetList, IPV4_ADDR_LENGTH);
        MEMCPY (&u4PwMplsOutboundTunnelEgrId,
                MplsOutboundTunnelPeerLSR.pu1_OctetList, IPV4_ADDR_LENGTH);
        u4PwMplsOutboundTunnelIngId = OSIX_NTOHL (u4PwMplsOutboundTunnelIngId);
        u4PwMplsOutboundTunnelEgrId = OSIX_NTOHL (u4PwMplsOutboundTunnelEgrId);

        MPLS_CMN_LOCK ();
        TeGetTunnelInfoByPrimaryTnlInst (u4PwMplsOutboundTunnelIndex,
                                         u4PwMplsOutboundTunnelInst,
                                         u4PwMplsOutboundTunnelIngId,
                                         u4PwMplsOutboundTunnelEgrId,
                                         &pTeTnlInfo);
        if (pTeTnlInfo != NULL)
        {
            if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
            {
                bIsP2MPTnl = TRUE;
            }
            else
            {
                if ((pTeTnlInfo->u4TnlMode ==
                     TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                    && (pTeTnlInfo->u1TnlRole == TE_EGRESS))
                {
                    Direction = MPLS_DIRECTION_REVERSE;
                }
                pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                      Direction);
            }
            if ((pXcEntry != NULL) && (pXcEntry->pOutIndex != NULL))
            {
                u4RetValMplsOutSegmentTopLabel = pXcEntry->pOutIndex->u4Label;
            }
        }
        MPLS_CMN_UNLOCK ();
    }
    else if (u1PwMplsType == L2VPN_MPLS_TYPE_NONTE)
    {
        /* NON-TE */
        if (nmhGetPwMplsOutboundLsrXcIndex (u4PwIndex, &XCIndex) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex),
                                     u4RetValPwMplsOutboundLsrXcIndex);

        MPLS_CMN_LOCK ();

        pXcEntry = MplsGetXCEntryByDirection (u4RetValPwMplsOutboundLsrXcIndex,
                                              MPLS_DEF_DIRECTION);
        if ((pXcEntry != NULL) && (pXcEntry->pOutIndex != NULL))
        {
            if (pXcEntry->mplsLabelIndex != NULL)
            {
                pLblEntry = (tLblEntry *)
                    TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));

                if (pLblEntry != NULL)
                {
                    u4RetValMplsOutSegmentTopLabel = pLblEntry->u4Label;
                }

                u4StackLabel = pXcEntry->pOutIndex->u4Label;
                bStackFlag = TRUE;
            }
            else
            {
                u4RetValMplsOutSegmentTopLabel = pXcEntry->pOutIndex->u4Label;
            }
        }
        MPLS_CMN_UNLOCK ();
    }
    else
    {
        /* VC-ONLY */
        u4RetValMplsOutSegmentTopLabel = L2VPN_INVALID_LABEL;
    }

    /* Get Inbound and Outbound Label */
    if (nmhGetPwInboundLabel (u4PwIndex, &u4RetValPwInboundLabel) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetPwOutboundLabel (u4PwIndex, &u4RetValPwOutboundLabel) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get Local, Remote Group Ids and PwId related to PWID FEC */
    if (nmhGetPwLocalGroupID (u4PwIndex, &u4RetValPwLocalGroupID) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetPwRemoteGroupID (u4PwIndex, &u4RetValPwRemoteGroupID) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get Saii, Taii and Agi related to GenFEC */
    if (nmhGetPwLocalAttachmentID (u4PwIndex, &Saii) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetPwPeerAttachmentID (u4PwIndex, &Taii) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetPwGroupAttachmentID (u4PwIndex, &Agi) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsMplsL2VpnPwAIIFormat (u4PwIndex, &AiiFormat) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsMplsL2VpnIsStaticPw (u4PwIndex, &i4IsStaticPw) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Get Local and Remote Mtus */
    if (nmhGetPwRemoteIfMtu (u4PwIndex, &u4RetValPwRemoteIfMtu) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetPwLocalIfMtu (u4PwIndex, &u4RetValPwLocalIfMtu) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* PWE3 Frame Check Sequence Retention */
    if (nmhGetPwFcsRetentioncfg (u4PwIndex, &i4RetValPwFcsRetentionStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Display all the information */
    if ((u4Flag == CLI_MPLS_L2VPN_SUMMARY) ||
        (u4Flag == CLI_MPLS_L2VPN_PEER_SUMMARY))
    {
        if (nmhGetPwPeerAddr (u4PwIndex, &PeerAddr) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) (&GenU4Addr.Addr.Ip6Addr), PeerAddr.pu1_OctetList,
                    IPV6_ADDR_LENGTH);
        }
        else
        {
#endif
            MEMCPY ((UINT1 *) (&GenU4Addr.Addr.u4Addr), PeerAddr.pu1_OctetList,
                    IPV4_ADDR_LENGTH);
            GenU4Addr.Addr.u4Addr = OSIX_HTONL (GenU4Addr.Addr.u4Addr);
#ifdef MPLS_IPV6_WANTED
        }
#endif

        if (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
        {
            STRNCPY (ac1LocalCt, "Eth VLAN",
                     MPLS_STRLEN_MIN (ac1LocalCt, "Eth VLAN"));
            ac1LocalCt[MPLS_STRLEN_MIN (ac1LocalCt, "Eth VLAN")] = '\0';
        }
        else
        {
            STRNCPY (ac1LocalCt, "Ethernet",
                     MPLS_STRLEN_MIN (ac1LocalCt, "Ethernet"));
            ac1LocalCt[MPLS_STRLEN_MIN (ac1LocalCt, "Ethernet")] = '\0';
        }

        CliPrintf (CliHandle, " %-11s ", pi1LocalIfName);
        CliPrintf (CliHandle, " %-15s", ac1LocalCt);

        if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_2) &&
            (u4RemoteAiiType == L2VPN_GEN_FEC_AII_TYPE_2))
        {
            CliPrintf (CliHandle, " %-18s ", "-");
        }
        else
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                CliPrintf (CliHandle, " %-18s ",
                           Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
            }
            else
#endif
            {
                CliPrintf (CliHandle, " %-18s ", pu1String);
            }
        }
        CliPrintf (CliHandle, "%-19s ",
                   CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
        if (u4PwId != L2VPN_ZERO)
        {
            CliPrintf (CliHandle, "%-10u \r\n", u4PwId);
        }
        else
        {
            CliPrintf (CliHandle, "-        \r\n");
        }
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "Local interface: %s,", pi1LocalIfName);
    /* TO-DO L2-VLAN operational status get API needs to implemented 
     * AS of now, Vlan based line protocol status will be up always*/
    if (u4PwEnetPwInstance == L2VPN_ZERO)
    {
        CliPrintf (CliHandle, " Line protocol: -, Ethernet up\r\n");
    }
    else if ((u1OperStatus == L2VPN_PWVC_OPER_UP) ||
             (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN))
    {
        CliPrintf (CliHandle, " Line protocol: up, Ethernet up \r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Line protocol: down, Ethernet up \r\n");
    }
    if ((pPwVcEntry->u1LocalStatus == L2VPN_PWVC_STATUS_FORWARDING)
        && (pPwVcEntry->u1RemoteStatus == L2VPN_PWVC_STATUS_FORWARDING) &&
        (i4PwOperStatus == L2VPN_PWVC_OPER_UP) && ((u4PwEnetPwInstance
                                                    != L2VPN_ZERO)
                                                   || (i4AttchdPwOperStatus ==
                                                       L2VPN_PWVC_OPER_UP)))
    {
        if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_2) &&
            (u4RemoteAiiType == L2VPN_GEN_FEC_AII_TYPE_2))
        {

            L2VpnExtractGenFecType2Identifiers (Saii.pu1_OctetList, &u4GlobalId,
                                                &u4NodeId, &u4AcId);
            au1Agi[Agi.i4_Length] = '\0';
            if (u4GlobalId != 0)
            {
                CliPrintf (CliHandle,
                           " Destination : - , PW status: %s, "
                           "VC status: up,\r\n",
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }
            else
            {
                CliPrintf (CliHandle,
                           "  P2MP Dest ID : %u, PW status: %s, "
                           "VC status: up,\r\n",
                           GenU4Addr.Addr.u4Addr,
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }

        }
        else if (i4PwPeerAddrType == INET_ADDR_TYPE_UNKNOWN)
        {
            CliPrintf (CliHandle,
                       "  P2MP Dest ID : %u, PW status: %s, "
                       "VC status: up,\r\n",
                       GenU4Addr.Addr.u4Addr,
                       CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
        }
        else
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                CliPrintf (CliHandle,
                           " Destination : %s, PW status: %s, "
                           "VC status: up,\r\n",
                           Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }
            else
#endif
            {
                CliPrintf (CliHandle,
                           " Destination : %s, PW status: %s, "
                           "VC status: up,\r\n",
                           pu1String,
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }
        }
    }
    else
    {
        if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_2) &&
            (u4RemoteAiiType == L2VPN_GEN_FEC_AII_TYPE_2))
        {
            L2VpnExtractGenFecType2Identifiers (Saii.pu1_OctetList, &u4GlobalId,
                                                &u4NodeId, &u4AcId);
            au1Agi[Agi.i4_Length] = '\0';
            if (u4GlobalId != 0)
            {
                CliPrintf (CliHandle,
                           "  Destination : - , PW status: %s, "
                           "VC status: down\r\n",
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }
            else
            {
                CliPrintf (CliHandle,
                           "  P2MP Dest ID : %u, PW status: %s, "
                           "VC status: down\r\n",
                           GenU4Addr.Addr.u4Addr,
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }

        }
        else if (i4PwPeerAddrType == INET_ADDR_TYPE_UNKNOWN)
        {
            CliPrintf (CliHandle,
                       "  P2MP Dest ID : %u, PW status: %s, "
                       "VC status: down\r\n",
                       GenU4Addr.Addr.u4Addr,
                       CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
        }
        else
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                CliPrintf (CliHandle,
                           "  Destination : %s, PW status: %s, "
                           "VC status: down\r\n",
                           Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }
            else
#endif
            {
                CliPrintf (CliHandle,
                           "  Destination : %s, PW status: %s, "
                           "VC status: down\r\n",
                           pu1String,
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (i4PwOperStatus));
            }
        }

    }
    if (u4PwId == L2VPN_ZERO)
    {
        CliPrintf (CliHandle, "VC ID: -,\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "VC ID: %u,\r\n", u4PwId);
    }
    if (bIsP2MPTnl == FALSE)
    {
        if (u4PsnVlan != L2VPN_ZERO || i4PwOutIfIndex != L2VPN_ZERO)
        {
            CliPrintf (CliHandle, "  Output interface: %s", pi1OutputIfName);
        }
        else
        {
            CliPrintf (CliHandle, "  Output interface: unknown");
        }
        if (bStackFlag == TRUE)
        {
            CliPrintf (CliHandle, ", imposed label stack {%u}\n", u4StackLabel);
        }
        else
        {
            CliPrintf (CliHandle, "\n");
        }
    }
    CliPrintf (CliHandle, " Preferred path      : configured \n");
    CliPrintf (CliHandle, " Default path        : disabled \n");
    CliPrintf (CliHandle, " Pseudowire Index    : %u \n", u4PwIndex);
    if ((nmhGetPwIfIndex (u4PwIndex, (INT4 *) &u4CfaIfIndex) == SNMP_SUCCESS)
        && (u4CfaIfIndex != L2VPN_ZERO))
    {
        CliPrintf (CliHandle, " Cfa Pseudowire Index: %u \n", u4CfaIfIndex);
        CfaCliGetIfName (u4CfaIfIndex, (INT1 *) au1CfaIfName);
        CliPrintf (CliHandle, " Cfa Pseudowire Name : %s \n", au1CfaIfName);
    }
    if (pPwVcEntry->bPwIntOamEnable == L2VPN_PW_OAM_ENABLE)
    {
        switch (pPwVcEntry->u1OamOperStatus)
        {
            case L2VPN_PWVC_OPER_UP:
                CliPrintf (CliHandle, " OAM status          : Enabled, Up\n");
                CliPrintf (CliHandle, " OAM Session Index   : %u\n",
                           pPwVcEntry->u4ProactiveSessionIndex);
                break;
            case L2VPN_PWVC_OPER_DOWN:
                CliPrintf (CliHandle, " OAM status          : Enabled, Down\n");
                CliPrintf (CliHandle, " OAM Session Index   : %u\n",
                           pPwVcEntry->u4ProactiveSessionIndex);
                break;
            case L2VPN_PWVC_OPER_UNKWN:
                CliPrintf (CliHandle,
                           " OAM status          : Enabled, Unknown\n");
                break;
            default:
                CliPrintf (CliHandle, " Unknown\n");
                break;

        }
    }
    else
    {
        CliPrintf (CliHandle, " OAM status          : Disabled\n");
    }
    CliPrintf (CliHandle, " Protection status   :");
    switch (pPwVcEntry->u1ProtStatus)
    {
        case LOCAL_PROT_NOT_APPLICABLE:
            CliPrintf (CliHandle, " Not applicable\n");
            break;
        case LOCAL_PROT_IN_USE:
            CliPrintf (CliHandle, " In Use\n");
            break;
        case LOCAL_PROT_AVAIL:
            CliPrintf (CliHandle, " Available\n");
            break;
        case LOCAL_PROT_NOT_AVAIL:
            CliPrintf (CliHandle, " Not available\n");
            break;
        default:
            CliPrintf (CliHandle, " Unknown\n");
            break;
    }
    if (pPwVcEntry->u1ProtStatus != LOCAL_PROT_NOT_APPLICABLE)
    {
        CliPrintf (CliHandle, " Protection Path VC ID       : %u\n",
                   pPwVcEntry->u4BkpPwVcID);
    }

    if (bIsP2MPTnl == TRUE)
    {
        MPLS_CMN_LOCK ();
        TeGetTunnelInfoByPrimaryTnlInst (u4PwMplsOutboundTunnelIndex,
                                         u4PwMplsOutboundTunnelInst,
                                         u4PwMplsOutboundTunnelIngId,
                                         u4PwMplsOutboundTunnelEgrId,
                                         &pTeTnlInfo);

        CliPrintf (CliHandle, "\r P2MP destination        OutLabel\n");
        /* Check if destination is already present in P2MP destination table */
        TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                      pTeP2mpDestEntry, tP2mpDestEntry *)
        {
            MEMCPY ((UINT1 *) &u4DestId,
                    &(TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)), sizeof (UINT4));
            CLI_CONVERT_IPADDR_TO_STR (pP2mpDestIp, u4DestId);

            u4OutIndex = TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry);
            if (0 != u4OutIndex)
            {
                pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
            }
            if ((0 == u4OutIndex) || (NULL == pOutSegment))
            {
                if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
                {
                    CliPrintf (CliHandle, "\r %u                       - \n",
                               u4DestId);
                }
                else
                {
                    CliPrintf (CliHandle, "\r %s               - \n",
                               pP2mpDestIp);
                }
                continue;
            }

            if ((CfaUtilGetIfIndexFromMplsTnlIf
                 (OUTSEGMENT_IF_INDEX (pOutSegment), &u4L3Intf,
                  TRUE) == CFA_FAILURE) || (u4L3Intf == 0))
            {
                if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
                {
                    CliPrintf (CliHandle, "\r %u                       - \n",
                               u4DestId);
                }
                else
                {
                    CliPrintf (CliHandle, "\r %s               - \n",
                               pP2mpDestIp);
                }
                continue;
            }
            CfaCliGetIfName (u4L3Intf, pi1OutputIfName);
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r %u                       %s, %u\n",
                           u4DestId, pi1OutputIfName,
                           OUTSEGMENT_LABEL (pOutSegment));
            }
            else
            {
                CliPrintf (CliHandle, "\r %s               %s, %u\n",
                           pP2mpDestIp, pi1OutputIfName,
                           OUTSEGMENT_LABEL (pOutSegment));
            }
        }

        MPLS_CMN_UNLOCK ();
    }
    else if (u4RetValMplsOutSegmentTopLabel == MPLS_IPV4_EXPLICIT_NULL_LABEL)
    {
        CliPrintf (CliHandle, " Tunnel label        : explicit-null");
    }
    else if (u4RetValMplsOutSegmentTopLabel == MPLS_IMPLICIT_NULL_LABEL)
    {
        CliPrintf (CliHandle, " Tunnel label        : implicit-null");
    }
    else if (u4RetValMplsOutSegmentTopLabel != L2VPN_INVALID_LABEL)
    {
        CliPrintf (CliHandle, " Tunnel label        : %u",
                   u4RetValMplsOutSegmentTopLabel);
    }
    else
    {
        CliPrintf (CliHandle, " Tunnel label        : unassigned");
    }
    CliPrintf (CliHandle, " \r\n  Create time: %s,"
               " last status change time: %s\n",
               ai1PwCreateTime, ai1PwLastChange);
    if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL)
        || (i4IsStaticPw == TRUE))
    {
#ifdef VPLSADS_WANTED
        if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_OTHER)
        {
            CliPrintf (CliHandle, "  Signalling protocol: BGP \n");
        }
        else
#endif
        {
            CliPrintf (CliHandle, "  Signalling protocol: Manual \n");
        }
    }
    else
    {
        if ((u4RetValPwInboundLabel == L2VPN_INVALID_LABEL) ||
            (u4RetValPwOutboundLabel == L2VPN_INVALID_LABEL))
        {

#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                CliPrintf (CliHandle,
                           "  Signalling protocol: LDP, peer %s:0 down\n",
                           Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
            }
            else
#endif
            {
                CliPrintf (CliHandle,
                           "  Signalling protocol: LDP, peer %s:0 down\n",
                           pu1String);
            }
        }
        else
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                CliPrintf (CliHandle,
                           "  Signalling protocol: LDP, peer %s:0 up\n",
                           Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
            }
            else
#endif
            {
                CliPrintf (CliHandle,
                           "  Signalling protocol: LDP, peer %s:0 up\n",
                           pu1String);
            }
        }
        if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)
        {
            if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) ==
                L2VPN_PWVC_RMT_STAT_CAPABLE)
            {
                CliPrintf (CliHandle,
                           "    Status TLV support (local/remote)   : enabled/supported\n");
            }
            else if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) ==
                     L2VPN_PWVC_RMT_STAT_NOT_CAPABLE)
            {
                CliPrintf (CliHandle, "    Status TLV support (local/remote)   "
                           ": enabled/not supported\n");
            }
            else
            {
                CliPrintf (CliHandle, "    Status TLV support (local/remote)   "
                           ": enabled/not yet known\n");
            }
        }
        else
        {
            if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) ==
                L2VPN_PWVC_RMT_STAT_CAPABLE)
            {
                CliPrintf (CliHandle,
                           "    Status TLV support (local/remote)   : "
                           "not enabled/supported\n");
            }
            else if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) ==
                     L2VPN_PWVC_RMT_STAT_NOT_CAPABLE)
            {
                CliPrintf (CliHandle, "    Status TLV support (local/remote)   "
                           ": not enabled/not supported\n");
            }
            else
            {
                CliPrintf (CliHandle, "    Status TLV support (local/remote)   "
                           ": not enabled/not yet known\n");
            }
        }

        if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)
        {
            if (pPwVcEntry->u1LocalStatus == L2VPN_PWVC_STATUS_FORWARDING)
            {
                CliPrintf (CliHandle,
                           "     Last local LDP TLV status sent: No fault\r\n");
            }
            if (pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING)
            {
                CliPrintf (CliHandle,
                           "     Last local LDP TLV status sent: DOWN(PW-tx-fault)");
                bStatusPrinted = TRUE;
            }
            if (pPwVcEntry->u1LocalStatus & L2VPN_PWVC_STATUS_AC_BITMASK)
            {
                if (bStatusPrinted)
                {
                    CliPrintf (CliHandle, " AC DOWN(rx, tx faults)");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "     Last local LDP TLV status sent: "
                               "AC DOWN(rx, tx faults)");
                    bStatusPrinted = TRUE;
                }
            }
            if ((pPwVcEntry->
                 u1LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT)
                && (pPwVcEntry->
                    u1LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT))
            {
                if (bStatusPrinted)
                {
                    CliPrintf (CliHandle, " TNL DOWN(rx, tx faults)");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "     Last local LDP TLV status sent: "
                               "TNL DOWN(rx, tx faults)");
                    bStatusPrinted = TRUE;
                }
            }
            else if (pPwVcEntry->
                     u1LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT)
            {
                if (bStatusPrinted)
                {
                    CliPrintf (CliHandle, " TNL DOWN(rx fault)");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "     Last local LDP TLV status sent: "
                               "TNL DOWN(rx fault)");
                    bStatusPrinted = TRUE;
                }
            }
            else if (pPwVcEntry->
                     u1LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT)
            {
                if (bStatusPrinted)
                {
                    CliPrintf (CliHandle, " TNL DOWN(tx fault)");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "     Last local LDP TLV status sent: "
                               "TNL DOWN(tx fault)");
                    bStatusPrinted = TRUE;
                }
            }
            if (bStatusPrinted)
            {
                CliPrintf (CliHandle, "\r\n");
            }

            bStatusPrinted = FALSE;
            if ((pPwVcEntry->u1MapStatus & L2VPN_MAP_RCVD) == L2VPN_MAP_RCVD)
            {
                if (pPwVcEntry->u1RemoteStatus == L2VPN_PWVC_STATUS_FORWARDING)
                {
                    CliPrintf (CliHandle,
                               "     Last remote LDP TLV status rcvd: No fault\r\n");
                }
                if (pPwVcEntry->
                    u1RemoteStatus & L2VPN_PWVC_STATUS_NOT_FORWARDING)
                {
                    CliPrintf (CliHandle,
                               "     Last remote LDP TLV status rcvd: DOWN(PW-tx-fault)");
                    bStatusPrinted = TRUE;
                }
                if (pPwVcEntry->u1RemoteStatus & L2VPN_PWVC_STATUS_AC_BITMASK)
                {
                    if (bStatusPrinted)
                    {
                        CliPrintf (CliHandle, " AC DOWN(rx, tx faults)");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "     Last remote LDP TLV status rcvd: "
                                   "AC DOWN(rx, tx faults)");
                        bStatusPrinted = TRUE;
                    }
                }
                if ((pPwVcEntry->
                     u1LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT)
                    && (pPwVcEntry->
                        u1LocalStatus & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT))
                {
                    if (bStatusPrinted)
                    {
                        CliPrintf (CliHandle, " TNL DOWN(rx, tx faults)");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "     Last remote LDP TLV status rcvd: "
                                   "TNL DOWN(rx,tx faults)");
                        bStatusPrinted = TRUE;
                    }
                }
                else if (pPwVcEntry->
                         u1RemoteStatus & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT)
                {
                    if (bStatusPrinted)
                    {
                        CliPrintf (CliHandle, " TNL DOWN(rx fault)");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "     Last remote LDP TLV status rcvd: "
                                   "TNL DOWN(rx fault)");
                        bStatusPrinted = TRUE;
                    }
                }
                else if (pPwVcEntry->
                         u1RemoteStatus & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT)
                {
                    if (bStatusPrinted)
                    {
                        CliPrintf (CliHandle, " TNL DOWN(tx fault)");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "     Last remote LDP TLV status rcvd: "
                                   "TNL DOWN(tx fault)");
                        bStatusPrinted = TRUE;
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "     Last remote LDP TLV status rcvd: -\r\n");
            }
        }
    }
    if (bStatusPrinted)
    {
        CliPrintf (CliHandle, "\r\n");
    }
    if ((u4RetValPwInboundLabel == L2VPN_INVALID_LABEL) &&
        (u4RetValPwOutboundLabel == L2VPN_INVALID_LABEL))
    {
        CliPrintf (CliHandle,
                   "    MPLS VC labels: local unassigned, remote unassigned \r\n");
    }
    else if (u4RetValPwInboundLabel == L2VPN_INVALID_LABEL)
    {
        CliPrintf (CliHandle,
                   "    MPLS VC labels: local unassigned, remote %u \r\n",
                   u4RetValPwOutboundLabel);
    }
    else if (u4RetValPwOutboundLabel == L2VPN_INVALID_LABEL)
    {
        CliPrintf (CliHandle,
                   "    MPLS VC labels: local %u, remote unassigned \r\n",
                   u4RetValPwInboundLabel);
    }
    else
    {
        CliPrintf (CliHandle, "    MPLS VC labels: local %u, remote %u \r\n",
                   u4RetValPwInboundLabel, u4RetValPwOutboundLabel);
    }
    if (i4PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        if (u4RetValPwOutboundLabel != L2VPN_INVALID_LABEL)
        {
            CliPrintf (CliHandle, "    Group ID            : local %u, "
                       "remote %u \r\n", u4RetValPwLocalGroupID,
                       u4RetValPwRemoteGroupID);
        }
        else
        {
            CliPrintf (CliHandle, "    Group ID: local %u, "
                       "remote unknown \r\n", u4RetValPwLocalGroupID);
        }
        CliPrintf (CliHandle, "    PwID                : %u \r\n", u4PwId);
    }

    if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
        (u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_1) &&
        (u4RemoteAiiType == L2VPN_GEN_FEC_AII_TYPE_1))
    {
        au1Saii[Saii.i4_Length] = '\0';
        au1Taii[Taii.i4_Length] = '\0';
        au1Agi[Agi.i4_Length] = '\0';

        if ((AiiFormat.pu1_OctetList[0]) & (L2VPN_PWVC_SAII_IP))
        {
            MEMCPY ((UINT1 *) (&u4SrcAii), Saii.pu1_OctetList,
                    IPV4_ADDR_LENGTH);

            u4SrcAii = OSIX_NTOHL (u4SrcAii);
            CLI_CONVERT_IPADDR_TO_STR (pAiiString, u4SrcAii);

            CliPrintf (CliHandle, "    Saii                : %s \r\n",
                       pAiiString);
        }
        else
        {
            if (au1Saii[0] != L2VPN_ZERO)
            {
                CliPrintf (CliHandle, "    Saii                : %s \r\n",
                           au1Saii);
            }
        }

        if ((AiiFormat.pu1_OctetList[0]) & (L2VPN_PWVC_TAII_IP))
        {
            MEMCPY ((UINT1 *) (&u4TargetAii), Taii.pu1_OctetList,
                    IPV4_ADDR_LENGTH);

            u4TargetAii = OSIX_NTOHL (u4TargetAii);
            CLI_CONVERT_IPADDR_TO_STR (pAiiString, u4TargetAii);

            CliPrintf (CliHandle, "    Taii                : %s \r\n",
                       pAiiString);
        }
        else
        {
            if (au1Taii[0] != L2VPN_ZERO)
            {
                CliPrintf (CliHandle, "    Taii                : %s \r\n",
                           au1Taii);
            }
        }
        if (au1Taii[0] == L2VPN_ZERO)
        {
            CliPrintf (CliHandle, "    Agi                 : %d \r\n",
                       Agi.pu1_OctetList[STRLEN (MPLS_OUI_VPN_ID)]);
        }
        else
        {
            CliPrintf (CliHandle, "    Agi                 : ");

            CliShowAgiValue (CliHandle, au1Agi);
            CliPrintf (CliHandle, "\r\n");
        }
    }

    else if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
             (u4LocalAiiType == L2VPN_ZERO) &&
             (u4RemoteAiiType == L2VPN_ZERO) &&
             (au1Taii[0] == L2VPN_ZERO) && (au1Saii[0] == L2VPN_ZERO))

    {

        CliPrintf (CliHandle, "    Agi                 : %d \r\n",
                   Agi.pu1_OctetList[STRLEN (MPLS_OUI_VPN_ID)]);
    }
    else if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
             (u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_2) &&
             (u4RemoteAiiType == L2VPN_GEN_FEC_AII_TYPE_2))
    {
        CliPrintf (CliHandle, "    Agi                 : ");

        CliShowAgiValue (CliHandle, au1Agi);
        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "    Source Global Id         : %u \r\n",
                   u4GlobalId);
        CliPrintf (CliHandle, "    Source Node Id           : %u \r\n",
                   u4NodeId);
        CliPrintf (CliHandle, "    Source Ac Id             : %u \r\n", u4AcId);

        L2VpnExtractGenFecType2Identifiers (Taii.pu1_OctetList, &u4GlobalId,
                                            &u4NodeId, &u4AcId);
        CliPrintf (CliHandle, "    Destination Global Id    : %u \r\n",
                   u4GlobalId);
        CliPrintf (CliHandle, "    Destination Node Id      : %u \r\n",
                   u4NodeId);
        CliPrintf (CliHandle, "    Destination Ac Id        : %u \r\n", u4AcId);

    }
#ifdef VPLSADS_WANTED
    else if (i4PwOwner == L2VPN_PWVC_OWNER_OTHER)
    {
        MEMCPY (&u1VpnId, au1Agi, sizeof (UINT1));
        CliPrintf (CliHandle, "    Agi                      : %u \r\n",
                   u1VpnId);

        CliPrintf (CliHandle, "    Source Global Id         : %u \r\n",
                   u4GlobalId);
        CliPrintf (CliHandle, "    Source Node Id           : %u \r\n",
                   u4NodeId);
        CliPrintf (CliHandle, "    Source Ac Id             : %u \r\n", u4AcId);

        L2VpnExtractGenFecType2Identifiers (Taii.pu1_OctetList, &u4GlobalId,
                                            &u4NodeId, &u4AcId);
        CliPrintf (CliHandle, "    Destination Global Id    : %u \r\n",
                   u4GlobalId);
        CliPrintf (CliHandle, "    Destination Node Id      : %u \r\n",
                   u4NodeId);
        CliPrintf (CliHandle, "    Destination Ac Id        : %u \r\n", u4AcId);
        CliPrintf (CliHandle, "    Local Ve-id              : %u \r\n",
                   L2VPN_PWVC_LOCAL_VE_ID (pPwVcEntry));
        CliPrintf (CliHandle, "    Remote Ve-id             : %u \r\n",
                   L2VPN_PWVC_REMOTE_VE_ID (pPwVcEntry));

    }
#endif

    if (u4RetValPwOutboundLabel != L2VPN_INVALID_LABEL)
    {
        CliPrintf (CliHandle, "    MTU: local %u, remote %u \r\n",
                   u4RetValPwLocalIfMtu, u4RetValPwRemoteIfMtu);
    }
    else
    {
        CliPrintf (CliHandle, "    MTU: local %u, remote unknown \r\n",
                   u4RetValPwLocalIfMtu);
    }
    CliPrintf (CliHandle, "    Remote interface description:");

    if (i4RetValPwFcsRetentionStatus == L2VPN_PWVC_FCSRETN_DISABLE)
    {
        CliPrintf (CliHandle, "\r\n  Sequencing: receive disabled,"
                   "send disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n  Sequencing: receive enabled,"
                   " send enabled\r\n");
    }
    CliShowPwVCStatistics (CliHandle, u4PwIndex);
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : CliShowPwSummary
* Description   : This routine will display the summarised info of Pw Entries. 
* Input(s)      : u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable
*                 pu4OperUp             - Indicates the no of VC entries with
*                                         Operstatus as up.
*                 pu4OperDown           - Indicates the no of VC entries with 
*                                         OperStatus in down state.
*                 pu4OperUnknown        - Indicates the no of VC entries with 
*                                         OperStatus in Unknown state.
*                 pu4OperDormant        - Indicates the no of VC entries with 
*                                         OperStatus in Dormant state.
*                 pu4OperLldown         - Indicates the no of VC entries with 
*                                         OperStatus in Lower layer down state.
*                 pu4AdminDown          - Indicates the no of VC entries with 
*                                         AdminStatus in Down state.
* Output(s)     : Displays the pw VC statistics on the screen.
* Return(s)     : NONE
*******************************************************************************/
VOID
CliShowPwSummary (UINT4 u4PwIndex, UINT4 *pu4OperUnknown,
                  UINT4 *pu4OperUp, UINT4 *pu4OperDown, UINT4 *pu4OperDormant,
                  UINT4 *pu4OperLlDown, UINT4 *pu4AdminDown)
{
    INT4                i4PwOperStatus = 0;
    INT4                i4PwAdminStatus = 0;

    if (nmhGetPwOperStatus (u4PwIndex, &i4PwOperStatus) == SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetPwAdminStatus (u4PwIndex, &i4PwAdminStatus) == SNMP_FAILURE)
    {
        return;
    }
    if (i4PwOperStatus == L2VPN_PWVC_OPER_UNKWN)
    {
        (*pu4OperUnknown) = (*pu4OperUnknown) + 1;
    }
    else if (i4PwOperStatus == L2VPN_PWVC_OPER_DOWN)
    {
        (*pu4OperDown) = (*pu4OperDown) + 1;
    }
    else if (i4PwOperStatus == L2VPN_PWVC_OPER_UP)
    {
        (*pu4OperUp) = (*pu4OperUp) + 1;
    }
    else if (i4PwOperStatus == L2VPN_PWVC_OPER_DORMANT)
    {
        (*pu4OperDormant) = (*pu4OperDormant) + 1;
    }
    else if (i4PwOperStatus == L2VPN_PWVC_OPER_LLDOWN)
    {
        (*pu4OperLlDown) = (*pu4OperLlDown) + 1;
    }

    if (i4PwAdminStatus == L2VPN_ADMIN_DOWN)
    {
        *(pu4AdminDown) = (*pu4AdminDown) + 1;
    }

}

/******************************************************************************
* Function Name : SetL2VpnPwMode
* Description   : This routine will set the Pw mode.
* Input(s)      : u4PwIndex             - Index for the row identifying a PW 
*                                         in the pwTable
*                 i4Mode                - Indicates the mode of pseudowire.
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
SetL2VpnPwMode (tCliHandle CliHandle, UINT4 u4PwIndex, INT4 i4Mode)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsMplsL2VpnPwMode (&u4ErrorCode, u4PwIndex, i4Mode) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set L2VPn Mode\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsL2VpnPwMode (u4PwIndex, i4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set L2VPn Mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_vpls_cmd                               */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the L2VPN Module as   */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_vpls_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tL2vpnCliArgs       L2vpnCliArgs;
    tMplsGlobalNodeId   NodeId;
    INT4                i4RetStatus = 0;
    UINT4              *pau4Args[L2VPN_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;
    INT4                i4Inst;
    UINT4               u4PwAndMplsType = 0;
    UINT4               u4PwOwner = 0;
    UINT4               u4Flag = 0;
    UINT4               u4VplsFdbId = 0;
#ifdef VPLSADS_WANTED
    UINT4               u4VplsRtType = 0;
    UINT4               u4VplsControlWord = 0;
    UINT4               u4VplsMtu = 0;
    UINT4               u4VplsVeId = 0;
    UINT4               u4ErrCode;
#endif
    INT4                i4PwType = 0;
    UINT1               au1Agi[L2VPN_PWVC_MAX_AGI_LEN];
    UINT1               au1Saii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];
    UINT1               au1Taii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];
    MEMSET (au1Agi, 0, L2VPN_PWVC_MAX_AGI_LEN);
    MEMSET (au1Saii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);
    MEMSET (au1Taii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);

    MEMSET (&NodeId, 0, sizeof (tMplsGlobalNodeId));

    if (L2VPN_INITIALISED != TRUE)
    {
        CliPrintf (CliHandle, "\r %%L2VPN module is shutdown\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    MEMSET (&L2vpnCliArgs, L2VPN_ZERO, sizeof (tL2vpnCliArgs));

    /* Third arguement is always InterfaceName/Index */

    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));
    if (i4Inst != 0)
    {
        i4RetStatus = i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array.
     * Store 16 arguements at the max. This is because l2vpn commands do not
     * take more than fifteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        pau4Args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == L2VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    CliRegisterLock (CliHandle, L2vpnLock, L2vpnUnLock);
    MPLS_L2VPN_LOCK ();
    switch (u4Command)
    {
        case CLI_MPLS_VFI_CREATE:
            i4RetStatus =
                L2VpnCliVfiCreate (CliHandle, (UINT1 *) pau4Args[0],
                                   (pau4Args[1]));
            break;
        case CLI_MPLS_VFI_DELETE:
            i4RetStatus =
                L2VpnCliVfiDelete (CliHandle, (UINT1 *) pau4Args[0],
                                   pau4Args[1]);
            break;
        case CLI_MPLS_VPN_CREATE:
            /* VPLS FDB */
            if (pau4Args[1] != NULL)
            {
                u4VplsFdbId = (*pau4Args[1]);
            }
            else
            {
                u4VplsFdbId = L2VPN_VPLS_FDB_DEF_VAL;
            }
            i4RetStatus = L2VpnCliVpnCreate (CliHandle, *(pau4Args[0]),
                                             u4VplsFdbId);
            break;
        case CLI_MPLS_VPN_DELETE:
            i4RetStatus = L2VpnCliVpnDelete (CliHandle, *(pau4Args[0]));
            break;
        case CLI_MPLS_NEIGHBOR_CREATE:
            /* pau4Args[0] - Peer Address
             * pau4Args[1] - Local Label for Manual PW owner case,
             *               pwid for PWIDFEC PW owner case,
             *               agi for GENFEC PW owner case
             * pau4Args[2] - Remote Label for Manaul PW owner case
             *               groupid for PWIDFEC PW Owner case
             *               taii for GENFEC PW Owner case
             * pau4Args[3] - Outgoing Tunnel Id for mplstype TE case
             * pau4Args[4] - Incoming Tunnel Id for mplstype TE case
             * pau4Args[5] - PW Owner (Manual/PwidFec/GenFec)
             * pau4Args[6] - MplsType (Te/Non-te/Vconly)
             * pau4Args[7] - PwType (Ethernet/Ethernet Tagged)
             * pau4Args[8] - Agi for GENFEC PW Owner case
             * pau4Args[9] - EntityId
             * pau4Args[10] - Vc id for MANUAL/GENFEC Pw Owners
             * pau4Args[11] - Control Word Status (Enable/Disable)
             * pau4Args[12] - VCCV status Enable/Disable
             * pau4Args[13] - Inactive Flag status - to make the row status
             *                                       active/Not in Servive
             * pau4Args[14] - Destination Global Id for FEC129 AII Type 2 owner
             * pau4Args[15] - Destination Node Id for FEC129 AII Type 2 owner
             * pau4Args[16] - Source Attachment Circuit Identifier for
             *                FEC129 AII Type 2 owners
             * pau4Args[17] - Destination Attachment Circuit Identifier
             *                for FEC129 AII Type 2 owner
             * pau4Args[18] - Local Pseudowire Label for FEC129 AII Type 2 owner
             * pau4Args[19] - Remote Pseudowire Label for FEC129 AII Type 2 Owner
             * pau4Args[20] - Destination of Out Tunnel Id
             * pau4Args[21] - Source of Out Tunnel Id
             * pau4Args[22] - Destination of In Tunnel Id
             * pau4Args[23] - Source of In Tunnel Id
             * pau4Args[24] - Ip Address for SAII FEC129 AII Type 1
             * pau4Args[25] - Ip Address for TAII FEC129 AII Type 1
             */

            u4PwOwner = CLI_PTR_TO_U4 (pau4Args[5]);    /*Pw Owner */

            if (u4PwOwner == CLI_L2VPN_PW_OWNER_MANUAL)
            {
                if (pau4Args[10] != NULL)    /* Pwid for PwOwner="manual" */
                {
                    L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[10]);
                }
                /*Locallabel and remotelabel */
                L2vpnCliArgs.u4LocalLabel = *pau4Args[1];
                L2vpnCliArgs.u4RemoteLabel = *pau4Args[2];
#ifdef HVPLS_WANTED
                L2vpnCliArgs.u1SplitHorizonStatus =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[20]);
#endif

                /*PeerAddress Type */
                if (pau4Args[0] != NULL)
                {
                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[19]);

#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) pau4Args[0], IPV6_ADDR_LENGTH);
                    }
                    else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[0];
                    }
                }
                /*Out Tunnel Destination and Source */
                L2vpnCliArgs.u4OutDestTunnelId = CLI_PTR_TO_U4 (pau4Args[14]);
                L2vpnCliArgs.u4OutSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[15]);
                /*In Tunnel Destination and Source */
                L2vpnCliArgs.u4InDestTunnelId = CLI_PTR_TO_U4 (pau4Args[16]);
                L2vpnCliArgs.u4InSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[17]);
                if (pau4Args[18] != NULL)
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[18]);
                }
            }
            else if (u4PwOwner == CLI_L2VPN_PW_OWNER_PW_ID_FEC)
            {
                L2vpnCliArgs.u4PwId = *pau4Args[1];
                L2vpnCliArgs.u4LocalGroupId = *pau4Args[2];
#ifdef HVPLS_WANTED
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_I4 (pau4Args[23]);    /* PW RG Index */
                L2vpnCliArgs.u1IsBackupPw =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[24]);
                L2vpnCliArgs.u1SplitHorizonStatus =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[22]);
#endif
                if (pau4Args[0] != NULL)
                {
                    L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[21]);
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) pau4Args[0], IPV6_ADDR_LENGTH);
                    }
                    else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[0];
                    }

                }
                /*Out Tunnel Destination and Source */
                L2vpnCliArgs.u4OutDestTunnelId = CLI_PTR_TO_U4 (pau4Args[14]);
                L2vpnCliArgs.u4OutSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[15]);
                /*In Tunnel Destination and Source */
                L2vpnCliArgs.u4InDestTunnelId = CLI_PTR_TO_U4 (pau4Args[16]);
                L2vpnCliArgs.u4InSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[17]);
                /* Label can be configured for signalling cases also */
                L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[18]);
                L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[19]);
                if (pau4Args[20] != NULL)
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[20]);
                }
            }
            else
            {
                /* Set Pseudowire parameters for pwOwner "Genfec" */

                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[27]);
#ifdef HVPLS_WANTED
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_I4 (pau4Args[29]);    /* PW RG Index */
                L2vpnCliArgs.u1IsBackupPw =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[30]);
                L2vpnCliArgs.u1SplitHorizonStatus =
                    (UINT1) CLI_PTR_TO_I4 (pau4Args[28]);
#endif
                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[10]);    /*Pwid for pwOwner Genfec */
                L2vpnCliArgs.u4EntityId = CLI_PTR_TO_U4 (pau4Args[9]);
                L2vpnCliArgs.u4GenAgiType = L2VPN_GEN_PWVC_AGI_TYPE_1;

                /* Set Destination NodeId */
                L2vpnCliArgs.u4DestNodeId = CLI_PTR_TO_U4 (pau4Args[15]);

                if (pau4Args[0] != NULL)
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                    {
                        MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                                (UINT1 *) pau4Args[0], IPV6_ADDR_LENGTH);
                    }
                    else if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                    {
                        L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[0];
                    }

                }

                /*Set Pw parameters for FEC129 AII Type 2 */

                if (L2vpnCliArgs.u4DestNodeId != 0)
                {

                    L2vpnCliArgs.PeerAddr.u4Addr = L2vpnCliArgs.u4DestNodeId;
                    /* Fetch FEC 129 - AII Type 2 related parameters */
                    /* Set Destination Global Id if configured */
                    L2vpnCliArgs.u4DestGlobalId = CLI_PTR_TO_U4 (pau4Args[14]);

                    /* DestGlobalId is not configured, so assumed that
                     * DestGlobalId is same as that of SrcGlobalId. Get
                     * the SrcGlobalId and assign it to DestGlobalId */

                    if (L2VpnPortGetNodeId (&NodeId) == OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Not able to get source "
                                   "Global Id & Node Id\r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        L2vpnCliArgs.u4SrcGlobalId = NodeId.u4GlobalId;
                        L2vpnCliArgs.u4SrcNodeId = NodeId.u4NodeId;
                    }

                    if (L2vpnCliArgs.u4DestGlobalId == 0)
                    {
                        L2vpnCliArgs.u4DestGlobalId = NodeId.u4GlobalId;
                    }

                    /* Set Source AC Id and Destination AC Id */
                    L2vpnCliArgs.u4SrcAcId = CLI_PTR_TO_U4 (pau4Args[16]);
                    L2vpnCliArgs.u4DestAcId = CLI_PTR_TO_U4 (pau4Args[17]);

                    L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[18]);
                    L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[19]);

                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_2;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_2;

                    /*Store AGI for Genfec AII Type 2 */
                    L2vpnCliArgs.Agi.pu1_OctetList = au1Agi;
                    L2vpnCliArgs.Agi.i4_Length = STRLEN ((UINT1 *) pau4Args[8]);
                    MEMCPY (L2vpnCliArgs.Agi.pu1_OctetList,
                            (UINT1 *) pau4Args[8], L2vpnCliArgs.Agi.i4_Length);

                    /* Store the Source GlobalId, Source NodeId and Source AcId
                     * in SAII*/
                    L2vpnCliArgs.Saii.pu1_OctetList = au1Saii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4SrcGlobalId,
                                                    L2vpnCliArgs.u4SrcNodeId,
                                                    L2vpnCliArgs.u4SrcAcId,
                                                    &L2vpnCliArgs.Saii);
                    /* Store the Destination GlobalId, Destination NodeId and
                     * Destination AcId in TAII*/
                    L2vpnCliArgs.Taii.pu1_OctetList = au1Taii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4DestGlobalId,
                                                    L2vpnCliArgs.u4DestNodeId,
                                                    L2vpnCliArgs.u4DestAcId,
                                                    &L2vpnCliArgs.Taii);
                }
                else
                {
                    /* Get Local AII Type and Remote AII Type for GenFec 
                     * AII Type 1 */
                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_1;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_1;
#ifdef HVPLS_WANTED
                    L2vpnCliArgs.u1SplitHorizonStatus =
                        (UINT1) CLI_PTR_TO_I4 (pau4Args[28]);
#endif
                    if (pau4Args[24] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_SAII_IP;
                    }
                    if (pau4Args[25] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_TAII_IP;
                    }
                    if (pau4Args[1] != NULL)
                    {
                        if (L2vpnCliGetSaii ((UINT4 *) pau4Args[24],
                                             (UINT1 *) pau4Args[1],
                                             &L2vpnCliArgs) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                       "parameters \r\n");
                            MPLS_L2VPN_UNLOCK ();
                            CliUnRegisterLock (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                    if (pau4Args[2] != NULL)
                    {
                        if (L2vpnCliGetTaii ((UINT4 *) pau4Args[25],
                                             (UINT1 *) pau4Args[2],
                                             &L2vpnCliArgs) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                       "parameters \r\n");
                            MPLS_L2VPN_UNLOCK ();
                            CliUnRegisterLock (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                    if ((*pau4Args[8]) != L2VPN_ZERO)
                    {
                        if (L2vpnCliGetAgi
                            ((UINT1 *) pau4Args[8],
                             &L2vpnCliArgs) == CLI_FAILURE)

                        {
                            CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                       "parameters \r\n");
                            MPLS_L2VPN_UNLOCK ();
                            CliUnRegisterLock (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                }

                /* Label can be configured for signalling cases also */
                L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[18]);
                L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[19]);

                /*Out Tunnel Destination and Source */
                L2vpnCliArgs.u4OutDestTunnelId = CLI_PTR_TO_U4 (pau4Args[20]);
                L2vpnCliArgs.u4OutSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[21]);
                /*In Tunnel Destination and Source */
                L2vpnCliArgs.u4InDestTunnelId = CLI_PTR_TO_U4 (pau4Args[22]);
                L2vpnCliArgs.u4InSrcTunnelId = CLI_PTR_TO_U4 (pau4Args[23]);
                if (pau4Args[26] != NULL)
                {
                    L2vpnCliArgs.u4Mtu = CLI_PTR_TO_U4 (pau4Args[26]);
                }

            }

            L2vpnCliArgs.u4OutTunnelId = CLI_PTR_TO_U4 (pau4Args[3]);
            L2vpnCliArgs.u4InTunnelId = CLI_PTR_TO_U4 (pau4Args[4]);

            u4PwAndMplsType = CLI_PTR_TO_U4 (pau4Args[6]);
            L2vpnCliArgs.i4CtrlWordStatus = CLI_PTR_TO_I4 (pau4Args[11]);
            /*Set CapabAdvert as PwStatusIndication as VCCV is disabled 
             * in mpls l2transport CLI cmd*/
            L2vpnCliArgs.u4LocalCapabAdvert = CLI_PTR_TO_U4 (pau4Args[12]);
            if (pau4Args[13] != NULL)
            {
                L2vpnCliArgs.u4InactiveFlag = L2VPN_PW_INACTIVE_FLAG_SET;
            }
            i4PwType = CLI_PTR_TO_I4 (pau4Args[7]);
            L2vpnCliArgs.u4Flag = u4PwAndMplsType;

            L2vpnCliArgs.u4PwOwner = u4PwOwner;
            L2vpnCliArgs.i4PwType = i4PwType;

            i4RetStatus = L2VpnCliNeighborCreate (CliHandle, &L2vpnCliArgs);
            break;
        case CLI_MPLS_NEIGHBOR_DELETE:
            /* pau4Args[0] - Peer Address
             * pau4Args[1] - PW Owner (Manual/PwidFec/GenFec)
             * pau4Args[2] - Local Label for Manual PW owner case,
             *               pwid for PWIDFEC PW owner case,
             *               agi for GENFEC PW owner case
             * pau4Args[3] - Remote Label for Manaul PW owner case
             *               groupid for PWIDFEC PW Owner case
             *               saii for GENFEC PW Owner case
             * pau4Args[4] - Taii for GENFEC PW Owner case
             * pau4Args[5] - VcID MANUAL and GENFEC Pw Owner
             * pau4Args[6] - Destination Global Id for GENFEC PW AII Type2 case
             * pau4Args[7] - Destination Node Id for GENFEC PW AII Type2 case
             * pau4Args[8] - Source AC Id for GENFEC PW AII Type2 case
             * pau4Args[9] - Destination AC Id for GENFEC PW AII Type2 case
             * pau4Args[10] - Local Pseudowire Label for GENFEC PW AII Type2 case
             * pau4Args[11] - Remote Pseudowire Label for GENFEC PW AII Type2 case
             * pau4Args[12] - Ip Address for saii for 129 FEC Type 1
             * pau4Args[13] - Ip Address for taii for 129 FEC Type 1
             */

            u4Flag = CLI_PTR_TO_U4 (pau4Args[1]);    /*Pw Owner */
            L2vpnCliArgs.u4Flag = u4Flag;
            L2vpnCliArgs.i4PwMode = L2VPN_VPLS;
            if (u4Flag == CLI_L2VPN_PW_OWNER_MANUAL)
            {
                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[5]);
            }
            else if (u4Flag == CLI_L2VPN_PW_OWNER_PW_ID_FEC)
            {
                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[4]);
            }
            else if (u4Flag == CLI_L2VPN_PW_OWNER_GEN_FEC)
            {
                L2vpnCliArgs.i4PeerAddrType = CLI_PTR_TO_I4 (pau4Args[14]);
            }
            if (pau4Args[0] != NULL)
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
                {
                    MEMCPY (L2vpnCliArgs.PeerAddr.Ip6Addr.u1_addr,
                            (UINT1 *) pau4Args[0], IPV6_ADDR_LENGTH);
                }
                if (MPLS_IPV4_ADDR_TYPE == L2vpnCliArgs.i4PeerAddrType)
#endif
                {
                    L2vpnCliArgs.PeerAddr.u4Addr = *pau4Args[0];
                }
            }
            L2vpnCliArgs.u4PwOwner = u4Flag;

            if (u4Flag == CLI_L2VPN_PW_OWNER_MANUAL)
            {
                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[4]);    /*Pwid for Owner "manual" */
                L2vpnCliArgs.u4LocalLabel = *pau4Args[2];    /*locallabel */
                L2vpnCliArgs.u4RemoteLabel = *pau4Args[3];    /*remotelabel */
            }
            else if (u4Flag == CLI_L2VPN_PW_OWNER_PW_ID_FEC)
            {
                L2vpnCliArgs.u4PwId = *pau4Args[2];
                L2vpnCliArgs.u4LocalGroupId = *pau4Args[3];
#ifdef HVPLS_WANTED
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_U4 (pau4Args[5]);    /* PW RG Index */
#endif
            }
            else if (u4Flag == CLI_L2VPN_PW_OWNER_GEN_FEC)
            {
                L2vpnCliArgs.u4PwId = CLI_PTR_TO_U4 (pau4Args[5]);    /*Pw Id for GenFec */
                L2vpnCliArgs.u4DestNodeId = CLI_PTR_TO_U4 (pau4Args[7]);
#ifdef HVPLS_WANTED
                L2vpnCliArgs.u4PwRedGrpId = CLI_PTR_TO_U4 (pau4Args[15]);    /* PW RG Index */
#endif
                if (L2vpnCliArgs.u4DestNodeId != 0)
                {
                    /* Fetch FEC 129 - AII Type 2 related parameters */
                    /* As DestGlobalId is not configured, it should be
                     * same as that of SrcGlobalId. Get Source Global Id
                     * and assign it to DestGlobalId  */

                    L2vpnCliArgs.PeerAddr.u4Addr = L2vpnCliArgs.u4DestNodeId;
                    L2vpnCliArgs.u4DestGlobalId = CLI_PTR_TO_U4 (pau4Args[6]);

                    /* As DestGlobalId is not configured, it should be
                     * same as that of SrcGlobalId. Get Source Global Id
                     * and assign it to DestGlobalId  */
                    if (L2VpnPortGetNodeId (&NodeId) == OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Not able to get source "
                                   "Global Id & Node Id\r\n");
                        MPLS_L2VPN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        L2vpnCliArgs.u4SrcGlobalId = NodeId.u4GlobalId;
                        L2vpnCliArgs.u4SrcNodeId = NodeId.u4NodeId;
                    }
                    if (L2vpnCliArgs.u4DestGlobalId == 0)
                    {
                        L2vpnCliArgs.u4DestGlobalId = NodeId.u4GlobalId;
                    }
                    L2vpnCliArgs.u4SrcAcId = CLI_PTR_TO_U4 (pau4Args[8]);
                    L2vpnCliArgs.u4DestAcId = CLI_PTR_TO_U4 (pau4Args[9]);
                    L2vpnCliArgs.u4LocalLabel = CLI_PTR_TO_U4 (pau4Args[10]);
                    L2vpnCliArgs.u4RemoteLabel = CLI_PTR_TO_U4 (pau4Args[11]);

                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_2;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_2;

                    /*Store AGI for Genfec AII Type 2 */
                    L2vpnCliArgs.Agi.pu1_OctetList = au1Agi;
                    L2vpnCliArgs.Agi.i4_Length = STRLEN ((UINT1 *) pau4Args[2]);
                    MEMCPY (L2vpnCliArgs.Agi.pu1_OctetList,
                            (UINT1 *) pau4Args[2], L2vpnCliArgs.Agi.i4_Length);

                    /* Store the Source GlobalId, Source NodeId and Source AcId
                     * in SAII*/
                    L2vpnCliArgs.Saii.pu1_OctetList = au1Saii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4SrcGlobalId,
                                                    L2vpnCliArgs.u4SrcNodeId,
                                                    L2vpnCliArgs.u4SrcAcId,
                                                    &L2vpnCliArgs.Saii);
                    /* Store the Destination GlobalId, Destination NodeId and
                     * Destination AcId in TAII*/
                    L2vpnCliArgs.Taii.pu1_OctetList = au1Taii;
                    L2VpnCliGetAiiType2Identifiers (L2vpnCliArgs.u4DestGlobalId,
                                                    L2vpnCliArgs.u4DestNodeId,
                                                    L2vpnCliArgs.u4DestAcId,
                                                    &L2vpnCliArgs.Taii);
                }
                else
                {

                    if (pau4Args[12] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_SAII_IP;
                    }
                    if (pau4Args[13] != NULL)
                    {
                        L2vpnCliArgs.u1AiiFormat |= L2VPN_PWVC_TAII_IP;
                    }
                    if (pau4Args[3] != NULL)
                    {
                        if (L2vpnCliGetSaii ((UINT4 *) pau4Args[12],
                                             (UINT1 *) pau4Args[3],
                                             &L2vpnCliArgs) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                       "parameters \r\n");
                            MPLS_L2VPN_UNLOCK ();
                            CliUnRegisterLock (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                    if (pau4Args[4] != NULL)
                    {
                        if (L2vpnCliGetTaii ((UINT4 *) pau4Args[13],
                                             (UINT1 *) pau4Args[4],
                                             &L2vpnCliArgs) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                       "parameters \r\n");
                            MPLS_L2VPN_UNLOCK ();
                            CliUnRegisterLock (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                    if ((*pau4Args[2]) != L2VPN_ZERO)
                    {
                        if (L2vpnCliGetAgi
                            ((UINT1 *) pau4Args[2],
                             &L2vpnCliArgs) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r%% Wrong Gen FEC "
                                       "parameters \r\n");
                            MPLS_L2VPN_UNLOCK ();
                            CliUnRegisterLock (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                    L2vpnCliArgs.u4GenLocalAiiType = L2VPN_GEN_FEC_AII_TYPE_1;
                    L2vpnCliArgs.u4GenRemoteAiiType = L2VPN_GEN_FEC_AII_TYPE_1;
                }
            }
            i4RetStatus = L2VpnCliNeighborDelete (CliHandle, &L2vpnCliArgs);
            break;
        case CLI_MPLS_XCONNECT_CREATE:

            L2vpnCliArgs.u4Flag = CLI_PTR_TO_U4 (pau4Args[1]);
            if (L2vpnCliArgs.u4Flag == L2VPN_CLI_VLAN_MODE)
            {
                L2vpnCliArgs.u4VlanId = (UINT4) CLI_GET_VLANID ();

                if (pau4Args[4] != NULL)
                {
                    L2vpnCliArgs.i4PortIfIndex = CLI_PTR_TO_I4 (pau4Args[4]);
                }
            }
            else
            {
                L2vpnCliArgs.i4PortIfIndex = CLI_GET_IFINDEX ();

                if (pau4Args[4] != NULL)
                {
                    L2vpnCliArgs.u4VlanId = CLI_PTR_TO_U4 (pau4Args[4]);
                }
                else
                {
                    L2vpnCliArgs.u4VlanId = L2VPN_PWVC_ENET_DEF_PORT_VLAN;
                }
            }
            if (L2vpnCliArgs.u4VlanId != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
            {
                L2vpnCliArgs.u4Flag = L2VPN_CLI_VLAN_MODE;
            }
            L2vpnCliArgs.i4VlanMode = (INT4) CLI_PTR_TO_U4 (pau4Args[2]);
            if (L2vpnCliArgs.i4VlanMode == CLI_L2VPN_VLAN_MODE_NO_CHANGE)
            {
                L2vpnCliArgs.u4PwVlanId = L2vpnCliArgs.u4VlanId;
            }
            else
            {
                L2vpnCliArgs.u4PwVlanId = CLI_PTR_TO_U4 (pau4Args[3]);
            }
            i4RetStatus = L2VpnCliXCCreate (CliHandle, (UINT1 *) pau4Args[0],
                                            &L2vpnCliArgs);
            break;
        case CLI_MPLS_XCONNECT_DELETE:
            i4RetStatus = L2VpnCliXCDelete (CliHandle,
                                            (UINT1 *) pau4Args[0],
                                            CLI_PTR_TO_U4 (pau4Args[1]),
                                            CLI_PTR_TO_U4 (pau4Args[2]));
            break;
        case CLI_MPLS_SHOW_VFI:
            i4RetStatus =
                L2VpnCliShowVfiDetails (CliHandle, (UINT1 *) pau4Args[0]);
            break;
        case CLI_MPLS_SHOW_ALL:
            i4RetStatus = L2VpnCliShowVfi (CliHandle);
            break;
#ifdef VPLSADS_WANTED
        case CLI_MPLS_RD_CREATE:
            i4RetStatus = L2VpnCliRdCreate (CliHandle, (UINT1 *) pau4Args[0]);
            break;
        case CLI_MPLS_RD_DELETE:
            i4RetStatus = L2VpnCliRdDelete (CliHandle);
            break;
        case CLI_MPLS_RT_CREATE:
            u4VplsRtType = CLI_PTR_TO_U4 (pau4Args[1]);
            i4RetStatus = L2VpnCliRtCreate (CliHandle,
                                            (UINT1 *) pau4Args[0],
                                            u4VplsRtType);
            break;
        case CLI_MPLS_RT_DELETE:
            i4RetStatus = L2VpnCliRtDelete (CliHandle, (UINT1 *) pau4Args[0]);
            break;
        case CLI_MPLS_SET_CONTROL_WORD:
            u4VplsControlWord = CLI_PTR_TO_U4 (pau4Args[0]);
            i4RetStatus = L2VpnCliSetControlWord (CliHandle, u4VplsControlWord);
            break;
        case CLI_MPLS_SET_MTU:
            u4VplsMtu = CLI_PTR_TO_U4 (pau4Args[0]);
            i4RetStatus = L2VpnCliSetMtu (CliHandle, u4VplsMtu);
            break;
        case CLI_MPLS_VE_CREATE:
            u4VplsVeId = CLI_PTR_TO_U4 (pau4Args[0]);
            i4RetStatus = L2VpnCliVeCreate (CliHandle,
                                            u4VplsVeId, (UINT1 *) pau4Args[1]);
            break;
        case CLI_MPLS_VE_DELETE:
            u4VplsVeId = CLI_PTR_TO_U4 (pau4Args[0]);
            i4RetStatus = L2VpnCliVeDelete (CliHandle, u4VplsVeId);
            break;
#endif
#ifdef MPLS_TEST_WANTED
        case CLI_MPLS_SHOW_HW_LIST:
            L2VpnShowVplsHwList (CliHandle);
            break;
#endif
        default:
            CliPrintf (CliHandle, "\r%% Unknown Command \r\n");
            MPLS_L2VPN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }
    if ((i4RetStatus == CLI_FAILURE)
#ifdef VPLSADS_WANTED
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS)
#endif
        )
    {
#ifdef VPLSADS_WANTED
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS_PWVPLS)
            && (u4ErrCode < L2VPN_PW_VPLS_CLI_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%s",
                       L2VPN_MPLS_CLI_PW_VPLS_ERROR_MSGS
                       [CLI_ERR_OFFSET_MPLS_PW_VPLS (u4ErrCode)]);
            CLI_SET_CMD_STATUS (CLI_FAILURE);
            u4ErrCode = 0;
        }
#endif
        CLI_SET_ERR (0);
    }
    MPLS_L2VPN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_l2vpn_cmd                               */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the OAM related commands for the L2VPN */
/*                        Module (Both VPWS and VPLS) as                     */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_l2vpn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pau4Args[L2VPN_CLI_MAX_ARGS];
    INT4                i4Inst = L2VPN_ZERO;
    INT4                i4RetStatus = L2VPN_FAILURE;
    INT1                i1ArgNo = L2VPN_ZERO;
    tL2vpnCliArgs       L2vpnCliArgs;

    if (L2VPN_INITIALISED != TRUE)
    {
        CliPrintf (CliHandle, "\r %%L2VPN module is shutdown\n");
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* Third argument is NULL */

    MEMSET (&L2vpnCliArgs, L2VPN_ZERO, sizeof (tL2vpnCliArgs));
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));
    if (i4Inst != 0)
    {
        i4RetStatus = i4Inst;
    }

    while (1)
    {
        pau4Args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == L2VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    CliRegisterLock (CliHandle, L2vpnLock, L2vpnUnLock);
    MPLS_L2VPN_LOCK ();

    switch (u4Command)
    {
        case CLI_MPLS_OAM_CONFIG:
            /* pau4Args[0] : Pseudowire Identifier 
             * pau4Args[1] : Local CC (Control channel) Type
             * pau4Args[2] : Local CV (Connection verification) Type
             * pau4Args[3] : Remote CC (Control channel) Type
             * pau4Args[4] : Remote CV (Connection verification) Type */

            L2vpnCliArgs.u4PwId = *pau4Args[0];

            L2vpnCliArgs.u4LocalCCAdvert = CLI_PTR_TO_U4 (pau4Args[1]);
            L2vpnCliArgs.u4LocalCVAdvert = CLI_PTR_TO_U4 (pau4Args[2]);
            L2vpnCliArgs.u4RemoteCCAdvert = CLI_PTR_TO_U4 (pau4Args[3]);
            L2vpnCliArgs.u4RemoteCVAdvert = CLI_PTR_TO_U4 (pau4Args[4]);
            i4RetStatus = L2VpnCliVcOamConfigure (CliHandle, &L2vpnCliArgs);
            break;

        case CLI_CONFIG_VCCV_GLOBAL_CC:
            i4RetStatus = L2VpnCliConfigGlobalCcTypes (CliHandle,
                                                       CLI_PTR_TO_U4 (pau4Args
                                                                      [0]));
            break;
        case CLI_CONFIG_VCCV_GLOBAL_CV:
            i4RetStatus = L2VpnCliConfigGlobalCvTypes (CliHandle,
                                                       CLI_PTR_TO_U4 (pau4Args
                                                                      [0]));
            break;
        case CLI_VCCV_NO_GLOBAL_CC:
            i4RetStatus = L2VpnCliDisableGlobalCcTypes (CliHandle);

            break;
        case CLI_VCCV_NO_GLOBAL_CV:
            i4RetStatus = L2VpnCliDisableGlobalCvTypes (CliHandle);

            break;
        case CLI_MPLS_NOTIF:
            /* pau4Args[0] : Pseudowire Status */
            i4RetStatus = L2VpnCliVcNotifConfigure (CliHandle,
                                                    (INT4)
                                                    CLI_PTR_TO_U4 (pau4Args
                                                                   [0]));
            break;
        case CLI_MPLS_NO_NOTIF:
            /* pau4Args[0] : Pseudowire Status */
            i4RetStatus = L2VpnCliVcNotifConfigure (CliHandle,
                                                    (INT4)
                                                    CLI_PTR_TO_U4 (pau4Args
                                                                   [0]));
            break;
        case CLI_MPLS_SHOW_VC_LABEL_BINDING:
            i4RetStatus = L2VpnCliShowVccvCapabs (CliHandle);
            break;
        case CLI_MPLS_SHOW_HW_CAPABS:
            i4RetStatus = L2VpnCliShowHwCapabs (CliHandle);
            break;
        case CLI_MPLS_SHOW_GBL_INFO:
            i4RetStatus = L2VpnCliShowGlobalInfo (CliHandle);
            break;
        case CLI_MPLS_MAP_PWID:
            i4RetStatus =
                L2VpnCliSetPwIfIndex (CliHandle, CLI_PTR_TO_U4 (pau4Args[0]),
                                      (INT4) CLI_PTR_TO_U4 (pau4Args[1]));
            break;
        case CLI_MPLS_UNMAP_PWID:
            i4RetStatus =
                L2VpnCliSetPwIfIndex (CliHandle, CLI_PTR_TO_U4 (pau4Args[0]),
                                      L2VPN_ZERO);
            break;
        default:
            CliPrintf (CliHandle, "\r%% Unknown Command \r\n");
            MPLS_L2VPN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;

    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CLI_SET_ERR (0);
    }
    MPLS_L2VPN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}

/******************************************************************************
 * Function Name : L2VpnCliVcOamConfigure
 * Description   : This routine configures VCCV related params for a PwVcEntry
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliVcOamConfigure (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    INT4                i4RetStatus = CLI_FAILURE;
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pL2vpnCliArgs->u4PwId);

    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "L2VpnCliVcOamConfigure: "
                    "Failed to find the matching PwVc Entry "
                    "VCID = %u: INTMD-EXIT\r\n", pL2vpnCliArgs->u4PwId);
        CliPrintf (CliHandle, "\r%%PwVc entry does not exist\r\n");
        return CLI_FAILURE;
    }

    i4RetStatus = PwUpdtOamParams (CliHandle, pL2vpnCliArgs, pPwVcEntry);

    return i4RetStatus;
}

/******************************************************************************
 * Function Name : L2VpnCliConfigGlobalCcTypes
 * Description   : This routine configures global control channel capabilities
 * Input(s)      : u4GlobCcType - Global control channel type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/

INT4
L2VpnCliConfigGlobalCcTypes (tCliHandle CliHandle, UINT4 u4GlobCcType)
{
    tSNMP_OCTET_STRING_TYPE GlobalCcType;
    UINT4               u4ErrCode = 0;
    UINT1               u1GlobalCcType = (UINT1) u4GlobCcType;

    GlobalCcType.pu1_OctetList = &u1GlobalCcType;
    GlobalCcType.i4_Length = sizeof (UINT1);

    if (nmhTestv2FsMplsLocalCCTypesCapabilities (&u4ErrCode, &GlobalCcType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Test FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLocalCCTypesCapabilities (&GlobalCcType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%set FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnCliConfigGlobalCvTypes
 * Description   : This routine configures global connectivity verification 
 *                 capabilities
 * Input(s)      : u4GlobCvType - Global connectivity verification type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE.
 *******************************************************************************/

INT4
L2VpnCliConfigGlobalCvTypes (tCliHandle CliHandle, UINT4 u4GlobCvType)
{
    tSNMP_OCTET_STRING_TYPE GlobalCvType;
    UINT4               u4ErrCode = 0;
    UINT1               u1GlobalCvType = (UINT1) u4GlobCvType;

    GlobalCvType.pu1_OctetList = &u1GlobalCvType;
    GlobalCvType.i4_Length = sizeof (UINT1);

    if (nmhTestv2FsMplsLocalCVTypesCapabilities (&u4ErrCode, &GlobalCvType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Test FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLocalCVTypesCapabilities (&GlobalCvType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%set FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
L2VpnCliDisableGlobalCcTypes (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE GlobalCcType;

    UINT4               u4ErrCode = 0;
    UINT1               u1PwCCTypeCapabs =
        (UINT1) (L2VPN_VCCV_CC_ACH + L2VPN_VCCV_CC_RAL + L2VPN_VCCV_CC_TTL_EXP);

    GlobalCcType.pu1_OctetList = &u1PwCCTypeCapabs;
    GlobalCcType.i4_Length = sizeof (UINT1);

    if (nmhTestv2FsMplsLocalCCTypesCapabilities (&u4ErrCode, &GlobalCcType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Test FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLocalCCTypesCapabilities (&GlobalCcType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%set FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

INT4
L2VpnCliDisableGlobalCvTypes (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE GlobalCvType;

    UINT4               u4ErrCode = 0;
    UINT1               u1PwCvTypeCapabs = (UINT1) L2VPN_VCCV_CV_LSPP;

    GlobalCvType.pu1_OctetList = &u1PwCvTypeCapabs;

    GlobalCvType.i4_Length = sizeof (UINT1);

    if (nmhTestv2FsMplsLocalCVTypesCapabilities (&u4ErrCode, &GlobalCvType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Test FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLocalCVTypesCapabilities (&GlobalCvType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%set FsMplsL2VpnCCTypeCapabilities Failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

    /******************************************************************************
* Function Name : MplsGetMplsVfiCfgPrompt
* Description   : This routine is returns the prompt to be displayed. 
* Input(s)      : pi1ModeName - Mode to be configured
* Output(s)     : pi1DispStr  - Prompt to be displayed.
* Return(s)     : TRUE or FALSE.
*******************************************************************************/
INT1
MplsGetMplsVfiCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN] = { };

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_VFI_MODE, STRLEN (MPLS_VFI_MODE))
        != L2VPN_ZERO)
    {
        return FALSE;
    }

    if (VcmGetL2ModeExt () == VCM_MI_MODE)
    {
        SNPRINTF (ac1PromptDisplayName, sizeof (ac1PromptDisplayName),
                  "%s", "(config-switch-vfi)#");
    }
    else
    {
        SNPRINTF (ac1PromptDisplayName, sizeof (ac1PromptDisplayName),
                  "%s", "(config-vfi)#");
    }
    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';
    return TRUE;
}

/******************************************************************************
* Function Name : L2VpnCliVfiCreate
* Description   : This routine will a create a VFI (Vpls Instance).
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliVfiCreate (tCliHandle CliHandle, UINT1 *pu1VfiName, UINT4 *pu4VfiMode)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4VSI = 0;

#ifndef VPLSADS_WANTED
    UNUSED_PARAM (pu4VfiMode);
#endif

    pVplsEntry = L2VpnGetVplsEntryFromVFI (pu1VfiName);
    if (pVplsEntry != NULL)
    {
#ifdef MI_WANTED
        if ((INT4) CLI_GET_CXT_ID () != L2VPN_VPLS_VSI (pVplsEntry))
        {
            CliPrintf (CliHandle,
                       "\r%% Vpls Name already exists in another VSI"
                       " (or) Vpls Entry is temporarly not in service \r\n");
            return CLI_FAILURE;
        }
#else
        CLI_SET_CXT_ID (L2IWF_DEFAULT_CONTEXT);
#endif
        CLI_SET_MPLS_VFI_ID (L2VPN_VPLS_INDEX (pVplsEntry));
        if (L2VpnCliVfiModeChange (CliHandle, pu1VfiName) != CLI_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }

    for (u4VplsIndex = 1; u4VplsIndex <= gL2VpnGlobalInfo.u4MaxVplsEntries;
         u4VplsIndex++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
        if (pVplsEntry == NULL)
        {
            CLI_SET_MPLS_VFI_ID ((INT4) u4VplsIndex);
            break;
        }
    }

    if (u4VplsIndex > gL2VpnGlobalInfo.u4MaxVplsEntries)
    {
        CliPrintf (CliHandle, "\r%% VFI Table is FULL\n");
        return CLI_FAILURE;
    }

    /* Get the VSI in which the VFI is being created */
    if (VcmGetL2ModeExt () == VCM_MI_MODE)
    {
        u4VSI = CLI_GET_CXT_ID ();
    }
    else
    {
        u4VSI = 0;                /* set default context */
    }

    VplsName.pu1_OctetList = pu1VfiName;
    VplsName.i4_Length = STRLEN (pu1VfiName);

    if (nmhTestv2FsMplsVplsRowStatus
        (&u4ErrorCode, u4VplsIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed in while creating Vpls Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsVplsRowStatus (u4VplsIndex,
                                   CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed in while creating Vpls Entry\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMplsVplsVsi (&u4ErrorCode, u4VplsIndex,
                                (INT4) u4VSI) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }

        CliPrintf (CliHandle,
                   "\r%% Failed while mapping VSI to VPLS Instance \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsVplsVsi (u4VplsIndex, (INT4) u4VSI) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }

        CliPrintf (CliHandle,
                   "\r%% Failed while mapping VSI to VPLS Instance \r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMplsVplsName (&u4ErrorCode, u4VplsIndex,
                                 &VplsName) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }

        CliPrintf (CliHandle, "\r%% Invalid VPLS Name \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsVplsName (u4VplsIndex, &VplsName) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }

        CliPrintf (CliHandle, "\r%% Invalid VPLS Name \r\n");
        return CLI_FAILURE;
    }
#ifdef VPLSADS_WANTED
    if (nmhTestv2FsmplsVplsSignalingType (&u4ErrorCode, u4VplsIndex,
                                          (INT4) (*pu4VfiMode)) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }
        CliPrintf (CliHandle, "\r%% Invalid VPLS Signalling Type \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsmplsVplsSignalingType (u4VplsIndex,
                                       (INT4) (*pu4VfiMode)) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }
        CliPrintf (CliHandle, "\r%% Invalid VPLS Signalling Type \r\n");
        return CLI_FAILURE;
    }
#endif

    if (L2VpnCliVfiModeChange (CliHandle, pu1VfiName) == CLI_FAILURE)
    {
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(DESTROY)"
                        "Failed\r\n", __func__);
        }

        CliPrintf (CliHandle, "\r%% Unable to change the mode \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliVfiModeChange
* Description   : This routine will changes the <config-vsi> mode to 
*                 <config-vsi-vfi> mode.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliVfiModeChange (tCliHandle CliHandle, UINT1 *pu1VfiName)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd),
              "%s%s", MPLS_VFI_MODE, pu1VfiName);
    /* ENTER MPLS L2VPN VFI  Mode */
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to Enter into L2 VFI Configuration Mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliVfiDelete
* Description   : This routine will a create a VFI (Vpls Instance).
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliVfiDelete (tCliHandle CliHandle, UINT1 *pu1VfiName, UINT4 *pu4VfiMode)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    UINT4               u4VplsIndex = 0;
    UINT4               u4ErrorCode = 0;
    BOOL1               b1Flag = L2VPN_FALSE;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
#ifdef HVPLS_WANTED
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    UINT4               u4Errorcode = 0;
#endif
    pVplsEntry = L2VpnGetVplsEntryFromVFI (pu1VfiName);
    if (pVplsEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Vpls Name \r\n");
        return CLI_FAILURE;
    }
#ifdef VPLSADS_WANTED
    if (*pu4VfiMode != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        CliPrintf (CliHandle, "\r%% Invalid VPLS Mode \r\n");
        return CLI_FAILURE;
    }
#else
    UNUSED_PARAM (pu4VfiMode);
#endif
    u4VplsIndex = L2VPN_VPLS_INDEX (pVplsEntry);

    /* Scan through the PWs associated to the VPLS Instance and delete them */
    while ((pPwVplsNode = TMO_SLL_First (&pVplsEntry->PwList)) != NULL)
    {

        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
        {

            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                b1Flag = L2VPN_TRUE;
            }
            if (b1Flag == L2VPN_TRUE)
            {
                CliPrintf (CliHandle,
                           "\r%% AC should be removed before removing VFI");
                return CLI_FAILURE;
            }
#ifdef HVPLS_WANTED
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwEntry));
            if (pRgPw != NULL)
            {
                if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4Errorcode,
                                                      pRgPw->u4RgIndex,
                                                      L2VPN_PWVC_INDEX
                                                      (pPwEntry),
                                                      NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to de-activate the Redundancy PW\r");
                    return CLI_FAILURE;
                }
                if (nmhSetFsL2VpnPwRedPwRowStatus (pRgPw->u4RgIndex,
                                                   L2VPN_PWVC_INDEX (pPwEntry),
                                                   NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to de-activate the Redundancy PW\r");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4ErrorCode,
                                                      pRgPw->u4RgIndex,
                                                      L2VPN_PWVC_INDEX
                                                      (pPwEntry),
                                                      DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to delete the Redundancy PW\r");
                    return CLI_FAILURE;
                }
                if (SNMP_FAILURE ==
                    nmhSetFsL2VpnPwRedPwRowStatus
                    (pRgPw->u4RgIndex, L2VPN_PWVC_INDEX (pPwEntry), DESTROY))
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to delete the Redundancy PW\r");
                    return CLI_FAILURE;
                }
            }
#endif
        }
        if (PwDestroy (CliHandle, L2VPN_PWVC_INDEX (pPwEntry)) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% PWs associated with VPLS should be deleted before deleting VFI\r\n");
            return CLI_FAILURE;
        }

    }

    if (nmhTestv2FsMplsVplsRowStatus
        (&u4ErrorCode, u4VplsIndex, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed while deleting Vpls Entry\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed while deleting Vpls Entry \r\n");
        return CLI_FAILURE;
    }
#ifdef VPLSADS_WANTED
    if (*pu4VfiMode == L2VPN_VPLS_SIG_BGP)
    {
        L2VpnSendVplsAssociationDelEvent (u4VplsIndex);
    }
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliVpnCreate
* Description   : This routine will associate a VPN Identifier to the
*                 Vpls Instance.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliVpnCreate (tCliHandle CliHandle, UINT4 u4VpnId, UINT4 u4VplsFdbId)
{
    tSNMP_OCTET_STRING_TYPE VpnId;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN];
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4PwIndex = 0;
    INT4                u4VplsFdbIdPresent = 0;

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();
    /* Get the VSI in which the VFI is being created */
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

    if (pVplsEntry == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% No Vpls Entry to be associated with this VPN \r\n");
        return CLI_FAILURE;
    }
    /* First Three Bytes consist of OUI.Rest 4 bytes has the VPN Id inside
     * that Organization */
    STRNCPY (au1VpnId, MPLS_OUI_VPN_ID, sizeof (MPLS_OUI_VPN_ID));
    u4VpnId = OSIX_NTOHL (u4VpnId);
    if (u4VpnId == L2VPN_ZERO)
    {
        CliPrintf (CliHandle, "\r%%Invalid VPN Id \r\n");
        return CLI_FAILURE;
    }
    MEMCPY (&au1VpnId[STRLEN (MPLS_OUI_VPN_ID)], &u4VpnId, sizeof (UINT4));
    if ((L2VPN_VPLS_VPN_ID (pVplsEntry)[0] != 0) &&
        (MEMCMP (au1VpnId, L2VPN_VPLS_VPN_ID (pVplsEntry),
                 L2VPN_MAX_VPLS_VPNID_LEN) != 0))
    {
        CliPrintf (CliHandle,
                   "\r%% Already VPN is configured in this Instance \r\n");
        return CLI_FAILURE;
    }
    else if ((L2VPN_VPLS_VPN_ID (pVplsEntry)[0] != 0) &&
             (MEMCMP (au1VpnId, L2VPN_VPLS_VPN_ID (pVplsEntry),
                      L2VPN_MAX_VPLS_VPNID_LEN) == 0))
    {

        CliPrintf (CliHandle,
                   "\rWarning : Same VPN Id is configured already in this Instance \r\n");

        if (nmhGetFsMplsVplsL2MapFdbId (u4VplsIndex, &u4VplsFdbIdPresent) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rUnable to fetch u4VplsFdbIdPresent");
            return CLI_FAILURE;

        }
        if (u4VplsFdbId == L2VPN_VPLS_FDB_DEF_VAL)
        {
            return CLI_SUCCESS;
        }
        else if (u4VplsFdbId == (UINT4) u4VplsFdbIdPresent)
        {

            CliPrintf (CliHandle,
                       "\rWarning : Same FDB Id is configured already in this Instance \r\n");
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Error : Invalid FDB identifier \r\n");
            return CLI_SUCCESS;
        }

    }

    VpnId.pu1_OctetList = au1VpnId;
    VpnId.i4_Length = sizeof (au1VpnId);

    if (nmhTestv2FsMplsVplsVpnId (&u4ErrorCode, u4VplsIndex,
                                  &VpnId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VPN Identifier \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsVplsVpnId (u4VplsIndex, &VpnId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VPN Identifier \r\n");
        return CLI_FAILURE;
    }

/* VPLS FDB */
    /* mapping VplsInstance to a L2 FDB Id */
    if (nmhTestv2FsMplsVplsL2MapFdbId (&u4ErrorCode, u4VplsIndex,
                                       u4VplsFdbId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid FDB Identifier \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsVplsL2MapFdbId (u4VplsIndex, u4VplsFdbId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid FDB Identifier \r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsVplsRowStatus
        (&u4ErrorCode, u4VplsIndex, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed while making Up VPLS Entry\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed while making Up VPLS Entry\r\n");
        return CLI_FAILURE;
    }

    /* Scan the PW table to find the associated PW's with this VPLS instance 
     * and make them ACTIVE */
    while (1)
    {
        /* Get the next entry in the PwTable */
        if ((u4PwIndex != 0) &&
            ((nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex)) == SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4PwIndex == 0) &&
            ((nmhGetFirstIndexPwTable (&u4PwIndex)) == SNMP_FAILURE))
        {
            break;
        }

        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

        if (pPwVcEntry != NULL)
        {
            /* if the PwVcEntry belongs to this VPLS and NOT_INSERVICE */
            if ((pPwVcEntry->u4VplsInstance) == (pVplsEntry->u4VplsInstance)
                && (pPwVcEntry->i1RowStatus == NOT_IN_SERVICE))
            {
                nmhSetPwRowStatus (pPwVcEntry->u4PwVcIndex, ACTIVE);
            }
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliVpnDelete
* Description   : This routine will dis-associate a VPN Identifier to the
*                 Vpls Instance.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliVpnDelete (tCliHandle CliHandle, UINT4 u4VpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsIndex = 0;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE VpnId;
    UINT1               au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN];
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;

    MEMSET (au1VpnId, 0, sizeof (au1VpnId));
    VpnId.pu1_OctetList = 0;
    VpnId.i4_Length = 0;
    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (pVplsEntry == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% No Vpls Entry to be associated with this VPN \r\n");
        return CLI_FAILURE;
    }
    STRNCPY (au1VpnId, MPLS_OUI_VPN_ID, sizeof (MPLS_OUI_VPN_ID));
    u4VpnId = OSIX_NTOHL (u4VpnId);
    if (u4VpnId == L2VPN_ZERO)
    {
        CliPrintf (CliHandle, "\r%%Invalid VPN Id \r\n");
        return CLI_FAILURE;
    }

    MEMCPY (&au1VpnId[STRLEN (MPLS_OUI_VPN_ID)], &u4VpnId, sizeof (UINT4));
    if (MEMCMP (au1VpnId, L2VPN_VPLS_VPN_ID (pVplsEntry),
                L2VPN_MAX_VPLS_VPNID_LEN) != 0)
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid VPN Id .Enter VPN Id configured"
                   " in this Instance \r\n");
        return CLI_FAILURE;
    }

    /* Since VPN is a Mandatory object, Vpls Entry is set to NIS */
    if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == ACTIVE)
    {
        /* Marking the PW associated with this VPLS in NOT_IN_SERVICE */
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
            if (pPwEntry != NULL)
            {
                if (pPwEntry->i1RowStatus == ACTIVE)
                {
                    if (L2VPN_PWVC_OWNER (pPwEntry) != L2VPN_PWVC_OWNER_OTHER)
                    {
                        nmhSetPwRowStatus (pPwEntry->u4PwVcIndex,
                                           NOT_IN_SERVICE);
                        pPwVplsNode = NULL;
                    }
                }
            }
        }
        if (nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode, u4VplsIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Failed while making the Vpls Entry to down\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMplsVplsRowStatus (u4VplsIndex,
                                       NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Failed while making the Vpls Entry to down\r\n");
            return CLI_FAILURE;
        }
    }
    /* Resetting the VPN Value */
    if (nmhTestv2FsMplsVplsVpnId (&u4ErrorCode, u4VplsIndex,
                                  &VpnId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% nmhTestv2FsMplsVplsVpnId failed \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsVplsVpnId (u4VplsIndex, &VpnId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% nmhSetFsMplsVplsVpnId failed \r\n");
        return CLI_FAILURE;
    }
/* VPLS FDB */
    /* Unmapping VplsInstance to a L2 FDB Id. Setting the default value */
    if (nmhTestv2FsMplsVplsL2MapFdbId (&u4ErrorCode, u4VplsIndex,
                                       L2VPN_VPLS_FDB_DEF_VAL) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid FDB Identifier \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsVplsL2MapFdbId (u4VplsIndex,
                                    L2VPN_VPLS_FDB_DEF_VAL) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid FDB Identifier \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliNeighborCreate
* Description   : This routine will a create a neighbor by establishing a PW 
*                 between the two endpoints manually.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 pL2vpnCliArgs         - A pointer strucuture that contains 
*                                         parameters like pw type, pw mode,
*                                         mpls type etc.. that are required to 
*                                         create an pseudowire
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliNeighborCreate (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tTeRouterId         TnlIngLsrId;
    tTeRouterId         TnlEgrLsrId;
    UINT4               u4PwIndex = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Instance = 0;
    UINT4               u4Flag = L2VPN_FALSE;
#ifdef HVPLS_WANTED
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    BOOL1               bRdFlag = FALSE;
#endif
    BOOL1               bFlag = FALSE;
    MEMSET (&TnlIngLsrId, 0, sizeof (tTeRouterId));
    MEMSET (&TnlEgrLsrId, 0, sizeof (tTeRouterId));

    u4Flag = L2VpnCliValidatePwVcEntry (pL2vpnCliArgs);
    if (u4Flag == L2VPN_TRUE)
    {
        CliPrintf (CliHandle, "\r%%Entry already Exists.\r\n");
        return CLI_FAILURE;
    }

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if ((pVplsEntry == NULL) || (L2VPN_VPLS_ROW_STATUS (pVplsEntry) != ACTIVE))
    {
        CliPrintf (CliHandle,
                   "\r%% No Vpls Entry to be associated with this VPN \r\n");
        return CLI_FAILURE;
    }
    else
    {
        /* Scan through the PWs associated to the VPLS Instance and 
         * check whether a PW already exists for this Destination */
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
#ifdef HVPLS_WANTED
            pRgPw = L2VpnGetRedundancyPwEntryByPwIndex (pPwEntry->u4PwVcIndex);
            if (pRgPw != NULL
                && pL2vpnCliArgs->u1IsBackupPw == L2VPN_CLI_PW_BACKUP)
            {
                if (pRgPw->u4RgIndex != pL2vpnCliArgs->u4PwRedGrpId)
                {
                    continue;
                }
                else
                {
                    bRdFlag = TRUE;
                }
            }
#endif
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
            {
                if (MEMCMP
                    (pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                     &L2VPN_PWVC_PEER_ADDR (pPwEntry),
                     IPV6_ADDR_LENGTH) == L2VPN_ZERO)

                {
                    CliPrintf (CliHandle,
                               "\r%% A PW is already associated with this Peer"
                               " in this Vpls Instance \r\n");
                    return CLI_FAILURE;
                }
            }
            else if (MPLS_IPV4_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
#endif
            {
                if (pL2vpnCliArgs->PeerAddr.u4Addr ==
                    OSIX_NTOHL (*
                                ((UINT4 *) (VOID *)
                                 &L2VPN_PWVC_PEER_ADDR (pPwEntry))))
                {
                    CliPrintf (CliHandle,
                               "\r%% A PW is already associated with this Peer"
                               " in this Vpls Instance \r\n");
                    return CLI_FAILURE;
                }

            }
        }
    }
#ifdef HVPLS_WANTED
    if (bRdFlag == FALSE && pL2vpnCliArgs->u1IsBackupPw == L2VPN_CLI_PW_BACKUP)
    {
        CliPrintf (CliHandle,
                   "\r%%Back up Peer can be configured only after"
                   " configuring Primary PW\r\n");
        return CLI_FAILURE;
    }
#endif
    /* Getting a  Free Index from the Index Manager */
    nmhGetPwIndexNext (&u4PwIndex);
    if (u4PwIndex == L2VPN_ZERO)
    {
        CliPrintf (CliHandle, "\r%%PW Index is unavailable.\r\n");
        return CLI_FAILURE;
    }

    /* Create pw and populate pw table with the pw mode as L2VPN_VPLS */
    pL2vpnCliArgs->i4PwMode = L2VPN_VPLS;
    if (PwTablePopulate (CliHandle, u4PwIndex, pL2vpnCliArgs) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
#ifdef HVPLS_WANTED
    if (L2VpnCreateVplsPwBindEntry
        (u4PwIndex, pL2vpnCliArgs->u1SplitHorizonStatus) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
#endif
    /* This is VPLS. So, we need to set the vpls index associated with pw */
    if (nmhTestv2FsMplsL2VpnVplsIndex (&u4ErrorCode, u4PwIndex,
                                       u4VplsIndex) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle,
                   "\r%% Failed while associating VPLS Instance to the PW\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsL2VpnVplsIndex (u4PwIndex, u4VplsIndex) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle,
                   "\r%% Failed while associating VPLS Instance to the PW\r\n");
        return CLI_FAILURE;
    }

    /* Row Status should be made active only when the token 'inactive' is
     * not present in the CLI command "mpls l2transport". When inactive flag 
     * is set, oam related configurations are yet to be done and hence the
     * RS should not be made active. */
    if (nmhTestv2PwRowStatus (&u4ErrorCode, u4PwIndex, ACTIVE) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%% Failed while making Up PW Entry\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwRowStatus (u4PwIndex, ACTIVE) == SNMP_FAILURE)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle, "\r%% Failed while making Up PW Entry\r\n");
        return CLI_FAILURE;
    }

    /* Add this Pseudo wire to all the PwEnet Entries present in the same 
     * VPLS Entry */
    pPwVplsNode = (tTMO_SLL_NODE *) TMO_SLL_First (&(pVplsEntry->PwList));
    if (pPwVplsNode != NULL)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                /*getting the free Enet pseuwowire Instance */
                if (nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (u4PwIndex,
                                                               &u4Instance) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Failed in getting Enet PW Instance\r\n");
                    return CLI_FAILURE;
                }
                if (PwCreateEnetEntry (CliHandle, u4PwIndex, u4Instance,
                                       (UINT4) pPwVcEnetEntry->u2PwVlan,
                                       (UINT4) pPwVcEnetEntry->u2PortVlan,
                                       pPwVcEnetEntry->i4PortIfIndex,
                                       pPwVcEnetEntry->i1VlanMode) ==
                    CLI_FAILURE)
                {
                    bFlag = TRUE;
                    break;
                }
            }
        }
    }

    if (bFlag)
    {
        PwDestroy (CliHandle, u4PwIndex);
        CliPrintf (CliHandle,
                   "\r%% Error while attaching this entry to PWs"
                   "associated to this Instance\r\n");
        return CLI_FAILURE;
    }
    /* Set Pw Mpls Mpls Type */
    if (PwMplsTypeSet (CliHandle, u4PwIndex, pL2vpnCliArgs->u4Flag)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Setting MPLS Type failed\n");
        PwDestroy (CliHandle, u4PwIndex);
        return CLI_FAILURE;
    }

    if (PwOutboundTablePopulate
        (CliHandle, u4PwIndex, pL2vpnCliArgs) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% PwOutboundTablePopulate failed\n");
        PwDestroy (CliHandle, u4PwIndex);
        return CLI_FAILURE;
    }
#ifdef HVPLS_WANTED
    if (pL2vpnCliArgs->u4PwRedGrpId != 0)
    {
        if (CLI_FAILURE ==
            L2VpCliCreateRedPwEntry (CliHandle, u4PwIndex, pL2vpnCliArgs))
        {
            CliPrintf (CliHandle,
                       "\r %%Cannot associate the PW with a RG \r\n");
            PwDestroy (CliHandle, u4PwIndex);
            return CLI_FAILURE;
        }
    }
#endif
    if (pL2vpnCliArgs->u4InactiveFlag != L2VPN_PW_INACTIVE_FLAG_SET)
    {
        /* Admin status should not be made UP only when RowStatus != ACTIVE.
         * When inactive flag is set, RS is not made ACTIVE and hence AdminStatus
         * is also not made UP. */
        if (nmhTestv2PwAdminStatus
            (&u4ErrorCode, u4PwIndex, L2VPN_PWVC_ADMIN_UP) == SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle,
                       "\r%% Failed while making the AdminStatus to UP\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetPwAdminStatus (u4PwIndex, L2VPN_PWVC_ADMIN_UP) ==
            SNMP_FAILURE)
        {
            PwDestroy (CliHandle, u4PwIndex);
            CliPrintf (CliHandle,
                       "\r%% Failed while making the AdminStatus to UP\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliNeighborDelete
* Description   : This routine will a delete a neighbor entry 
*                 between the two endpoints manually.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 pL2vpnCliArgs         - Pointer to L2vpn CLI Arguments
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliNeighborDelete (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    pL2vpnCliArgs->u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();
    if (L2VpnPwVcDelete (CliHandle, pL2vpnCliArgs) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliXCCreate
* Description   : This routine will a associate a L2 Port/ Vlan to a PW.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliXCCreate (tCliHandle CliHandle, UINT1 *pu1VfiName,
                  tL2vpnCliArgs * pL2vpnCliArgs)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4PwIndex = 0;
    UINT4               u4Instance = 0;
    UINT4               u4StartInstance = 0;
    UINT4               u4ErrorCode = 0;
    BOOL1               bFlag = FALSE;

    pVplsEntry = L2VpnGetVplsEntryFromVFI (pu1VfiName);
    if ((pVplsEntry == NULL) || (L2VPN_VPLS_ROW_STATUS (pVplsEntry) != ACTIVE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Vpls Name \r\n");
        return CLI_FAILURE;
    }
#ifdef VPLSADS_WANTED
    if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        if (L2VpnCreateVplsACMap (pVplsEntry, pL2vpnCliArgs) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Vpls AC map Entry is not created \
                                    \r\n");
            return CLI_FAILURE;
        }
    }
#endif

    /* fill up AC info in MplsHwVcInfo */
    if ((pPwVplsNode = TMO_SLL_First (&pVplsEntry->PwList)) == NULL)
    {
        if (L2VPN_VPLS_SIG_NONE == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
        {
            CliPrintf (CliHandle, "\r%% No PWs present in this VPLS "
                       "Instance\r\n");
            return CLI_FAILURE;
        }
        else
        {
            return CLI_SUCCESS;
        }
    }

    if (L2VPN_FAILURE == L2VpnCheckforAC (pL2vpnCliArgs))
    {
        CliPrintf (CliHandle, "\r%% Vpls Entry is already associated "
                   "with this Instance/Port\r\n");
        return CLI_FAILURE;

    }

    pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
    u4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);

    if (L2VPN_PWVC_PSN_ENTRY (pPwEntry) == NULL)
    {
        CliPrintf (CliHandle, "\r%% NO PSN Entry associated \r\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (L2VPN_PWVC_INDEX (pPwEntry),
                                                   &u4Instance) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed in getting Enet PW Instance\r\n");
        return CLI_FAILURE;
    }

    /*setting the start index as u4Instance */
    u4StartInstance = u4Instance;

    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        u4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);

        if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
        {
            /* Checking whether any AC is connected to this Vlan */
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                /* If this AC is already attched, do nothing */
                if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                     pL2vpnCliArgs->u4VlanId) &&
                    (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                     pL2vpnCliArgs->i4PortIfIndex))
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Already this AC is "
                               "associated to this Vpls Instance \r\n");
                    return CLI_SUCCESS;
                }
            }
        }

        if (nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (u4PwIndex,
                                                       &u4Instance) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Failed in getting Enet PW Instance\r\n");
            return CLI_FAILURE;
        }

#ifdef VPLSADS_WANTED
        if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
        {
            if (PwCreateDynamicEnetEntry (u4PwIndex, u4Instance,
                                          pL2vpnCliArgs->u4VlanId,
                                          pL2vpnCliArgs->u4VlanId,
                                          (UINT4) (pL2vpnCliArgs->
                                                   i4PortIfIndex),
                                          CLI_L2VPN_VLAN_MODE_NO_CHANGE) ==
                L2VPN_FAILURE)
            {
                bFlag = TRUE;
                break;
            }
        }
        else
#endif
        {
            if (PwCreateEnetEntry (CliHandle, u4PwIndex, u4Instance,
                                   pL2vpnCliArgs->u4PwVlanId,
                                   pL2vpnCliArgs->u4VlanId,
                                   pL2vpnCliArgs->i4PortIfIndex,
                                   pL2vpnCliArgs->i4VlanMode) == CLI_FAILURE)
            {
                bFlag = TRUE;
                break;
            }
        }

    }

    if (bFlag)
    {
        u4Instance = u4StartInstance;
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
            u4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);
#ifdef VPLSADS_WANTED
            if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
            {
                if (L2VPN_FAILURE == L2VpnSetPwEnetRowStatus (u4PwIndex,
                                                              u4Instance,
                                                              DESTROY))
                {
                    CliPrintf (CliHandle,
                               "\r%% Failed to set PwEnetRowStatus\r\n");
                }
            }
            else
#endif
            {
                if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex,
                                              u4Instance,
                                              DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetPwEnetRowStatus (u4PwIndex, u4Instance, DESTROY)
                        == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Failed to set PwEnetRowStatus\r\n");
                    }
                }
            }

            u4Instance++;
        }
        CliPrintf (CliHandle,
                   "\r%% Error while attaching this entry to PWs"
                   "associated to this Instance\r\n");
    }
    else
    {
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
            if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
            {
                /* Updating the Mode of Attachment circuit (Vlan/Ethernet) */
                TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwEntry)),
                              pPwVcEnetEntry, tPwVcEnetEntry *)
                {
                    if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                         pL2vpnCliArgs->u4VlanId) &&
                        (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                         pL2vpnCliArgs->i4PortIfIndex))
                    {
                        pPwVcEnetEntry->u1EnetMode =
                            (UINT1) pL2vpnCliArgs->u4Flag;
                    }
                }
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliXCDelete
* Description   : This routine will a associate a L2 Port/ Vlan to a PW.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
*                 pu1VfiName            - VFI Name
*                 u4Mode                - Mode (Vlan or Port)
*                 u4IfIndexOrVlan       - Interface Index (Vlan or Port)
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliXCDelete (tCliHandle CliHandle, UINT1 *pu1VfiName, UINT4 u4Mode,
                  UINT4 u4IfIndexOrVlan)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tTMO_SLL_NODE      *pPwVcEnetNode = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4PwIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PwEnetPortVlan = 0;
    INT4                i4IfIndex = 0;
    BOOL1               b1IsFound = L2VPN_FALSE;
    BOOL1               b1IsDel = L2VPN_FALSE;

    pVplsEntry = L2VpnGetVplsEntryFromVFI (pu1VfiName);
    if ((pVplsEntry == NULL) || (L2VPN_VPLS_ROW_STATUS (pVplsEntry) != ACTIVE))
    {
        CliPrintf (CliHandle, "\r%% Invalid Vpls Name \r\n");
        return CLI_FAILURE;
    }

    if (u4Mode == L2VPN_CLI_ETH_MODE)
    {
        i4IfIndex = CLI_GET_IFINDEX ();
        u4PwEnetPortVlan = u4IfIndexOrVlan;
    }
    else
    {
        u4PwEnetPortVlan = (UINT4) CLI_GET_VLANID ();
        i4IfIndex = (INT4) u4IfIndexOrVlan;
    }
#ifdef VPLSADS_WANTED
    if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        if (L2VpnDeleteVplsACMap (pVplsEntry, i4IfIndex, u4PwEnetPortVlan)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Can't Delete VPLS AC mapping \r\n");
            return CLI_FAILURE;
        }
    }
#endif
    /* Scan through the PW Enet Entry's associated to the PW's in the VPLS 
     * Instance and delete them */
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        u4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);
        if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
        {
            /* Checking whether any AC is connected to this Vlan */
            TMO_DYN_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwEntry)), pPwVcEnetNode,
                              pTempNode, tTMO_SLL_NODE *)
            {
                pPwVcEnetEntry = (tPwVcEnetEntry *) pPwVcEnetNode;
                b1IsFound = L2VPN_FALSE;
                /* If the user specifies the Port+vlan combination
                 * of AC to Delete
                 */
                if (u4IfIndexOrVlan != L2VPN_ZERO)
                {
                    if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                         u4PwEnetPortVlan) &&
                        (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                         i4IfIndex))
                    {
                        if (b1IsDel == L2VPN_TRUE)
                        {
                            pPwVcEnetEntry->u1HwStatus = MPLS_FALSE;
                        }

                        b1IsFound = L2VPN_TRUE;
                    }
                    else
                    {
                        continue;
                    }
                }
                /* If the user gives the no xconnect vfi alone, in the
                 * eth mode, delete all the Enet Entries that has the
                 * port
                 */
                if ((u4Mode == L2VPN_CLI_ETH_MODE) &&
                    (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                     i4IfIndex))
                {
                    if (b1IsDel == L2VPN_TRUE)
                    {
                        pPwVcEnetEntry->u1HwStatus = MPLS_FALSE;
                    }
                    b1IsFound = L2VPN_TRUE;
                }

                /* If the user gives the no xconnect vfi alone, in the
                 * vlan mode, delete all the Enet Entries that has the
                 * Vlan
                 */
                else if ((u4Mode == L2VPN_CLI_VLAN_MODE) &&
                         (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                          u4PwEnetPortVlan))
                {
                    if (b1IsDel == L2VPN_TRUE)
                    {
                        pPwVcEnetEntry->u1HwStatus = MPLS_FALSE;
                    }

                    b1IsFound = L2VPN_TRUE;
                }
                if (b1IsFound == L2VPN_FALSE)
                {
                    continue;
                }

#ifdef VPLSADS_WANTED
                if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
                {
                    if (L2VpnSetPwEnetRowStatus (u4PwIndex,
                                                 L2VPN_PWVC_ENET_PW_INSTANCE
                                                 (pPwVcEnetEntry), DESTROY)
                        == L2VPN_FAILURE)
                    {
                        return CLI_SUCCESS;
                    }

                    if (u4IfIndexOrVlan != L2VPN_ZERO)
                    {
                        break;
                    }
                }
                else
#endif
                {
                    if (nmhTestv2PwEnetRowStatus
                        (&u4ErrorCode, u4PwIndex,
                         L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry),
                         DESTROY) == SNMP_SUCCESS)
                    {
                        if (nmhSetPwEnetRowStatus (u4PwIndex,
                                                   L2VPN_PWVC_ENET_PW_INSTANCE
                                                   (pPwVcEnetEntry), DESTROY)
                            == SNMP_FAILURE)
                        {
                            return CLI_SUCCESS;
                        }
                        /* In case, if the user does not specifies the port
                         * and vlan, all the entries associated with this
                         * port or vlan should be deleted
                         */
                        b1IsDel = L2VPN_TRUE;
                        if (u4IfIndexOrVlan != L2VPN_ZERO)
                        {
                            break;
                        }
                    }
                }

            }
            /*Check for b1IsDel is added to take care scenario:
             *  where Multiple Ac's are attached with same vpls instance
             *  and deletion of AC is requested without specifying port and vlan.*/
            if (b1IsFound == L2VPN_FALSE && b1IsDel == L2VPN_FALSE)
            {
                CliPrintf (CliHandle,
                           "\r%%No attachment circuit present  \r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r%% No attachment circuit  \r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnCliShowVfiDetails
 * Description   : This routine is used to Display a PwVcs Details
 * Input(s)      : CliHandle  - Context in which the command is processed
 * Output(s)     : If Success - None
 *                 else Proper Error Message is printed
 * Return(s)     : CLI_FAILED or CLI_SUCCEEDED
 *******************************************************************************/
INT1
L2VpnCliShowVfiDetails (tCliHandle CliHandle, UINT1 *pu1VfiName)
{
    tPwVcEntry         *pPwEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    CHR1               *pcIpAddr = NULL;
    UINT4               u4IpAddr = 0;
    UINT4               u4TotalPeers = 0;
    UINT4               u4BridgeMode = 0;
    BOOL1               bTitleFlag = TRUE;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ip6Addr;
#endif

#ifdef MPLS_IPV6_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif
    pVplsEntry = L2VpnGetVplsEntryFromVFI (pu1VfiName);
    if (pVplsEntry == NULL)
    {
        return CLI_FAILURE;
    }

#ifdef VPLSADS_WANTED
    if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        if (ACTIVE != L2VPN_VPLS_ROW_STATUS (pVplsEntry))
        {
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "\r\n VPLS Name : %s ",
                   L2VPN_VPLS_NAME (pVplsEntry));
        L2VpnCliShowAutoDiscoveredVfiDetails (CliHandle, pVplsEntry);
    }
#endif
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == L2VPN_PWVC_PEER_ADDR_TYPE (pPwEntry))
        {
            MEMCPY (Ip6Addr.u1_addr, &L2VPN_PWVC_PEER_ADDR (pPwEntry),
                    IPV6_ADDR_LENGTH);
        }
#endif
        MEMCPY ((UINT1 *) &u4IpAddr, &L2VPN_PWVC_PEER_ADDR (pPwEntry),
                L2VPN_IPV4ADR_LEN);
        u4IpAddr = OSIX_HTONL (u4IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pcIpAddr, u4IpAddr);
        if (pPwEntry->u1PwVcMode != L2VPN_VPLS)
        {
            continue;
        }
        if (bTitleFlag)
        {
#ifdef VPLSADS_WANTED
            if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
#endif
            {
                CliPrintf (CliHandle, "\r\n VPLS Name : %s ",
                           L2VPN_VPLS_NAME (pVplsEntry));
#ifdef MI_WANTED
                if (L2IwfGetBridgeMode (L2VPN_VPLS_VSI (pVplsEntry),
                                        &u4BridgeMode) != L2IWF_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Bridge Mode \n");
                    return CLI_FAILURE;
                }
#else
                if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) !=
                    L2IWF_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Bridge Mode \n");
                    return CLI_FAILURE;
                }
#endif
                CliPrintf (CliHandle,
                           "\t Bridge Domain : %s\r\n",
                           CLI_L2VPN_SHOW_BRIDGE_DOMAIN (u4BridgeMode));
            }
            /* To disp the PwVcs */
            CliPrintf (CliHandle,
                       "\r\n \tNeighbor     Pw Id     Pw Status \r\n");
            CliPrintf (CliHandle, " \t--------     -----     --------- \r\n");
            bTitleFlag = FALSE;
        }
        if ((L2VPN_PWVC_OWNER (pPwEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwEntry) == L2VPN_GEN_FEC_AII_TYPE_2)
            && (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwEntry) ==
                L2VPN_GEN_FEC_AII_TYPE_2))
        {
            CliPrintf (CliHandle,
                       "\r\n \t -      %u         %s\r\n",
                       L2VPN_PWVC_ID (pPwEntry),
                       CLI_L2VPN_SHOW_PW_OPER_STATUS (L2VPN_PWVC_OPER_STATUS
                                                      (pPwEntry)));

        }

        else
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == L2VPN_PWVC_PEER_ADDR_TYPE (pPwEntry))
            {
                CliPrintf (CliHandle,
                           "\r\n \t%s       %u         %s\r\n",
                           Ip6PrintAddr (&Ip6Addr),
                           L2VPN_PWVC_ID (pPwEntry),
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (L2VPN_PWVC_OPER_STATUS
                                                          (pPwEntry)));
            }
            else
#endif
            {
                CliPrintf (CliHandle,
                           "\r\n \t%s       %u         %s\r\n",
                           pcIpAddr,
                           L2VPN_PWVC_ID (pPwEntry),
                           CLI_L2VPN_SHOW_PW_OPER_STATUS (L2VPN_PWVC_OPER_STATUS
                                                          (pPwEntry)));
            }
        }
        u4TotalPeers++;
    }
    if (!bTitleFlag)
    {
        CliPrintf (CliHandle, "\r\n No. of Neighbors in this VSI : %u \n\n",
                   u4TotalPeers);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliShowVfi 
* Description   : This routine is used to Display All PwVcs Details
* Input(s)      : CliHandle  - Context in which the command is processed
* Output(s)     : If Success - None
*                 else Proper Error Message is printed
* Return(s)     : CLI_SUCCESS
*******************************************************************************/
INT1
L2VpnCliShowVfi (tCliHandle CliHandle)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsIndex = 0;

    if (nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    do
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
        if (pVplsEntry != NULL)
        {
            L2VpnCliShowVfiDetails (CliHandle, L2VPN_VPLS_NAME (pVplsEntry));
        }

    }
    while (nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndex, &u4VplsIndex)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : PwCreateEnetEntry
* Description   : This routine will a create a Pw Enet Entry.
* Input(s)      : CliHandle             - Context in which the command is 
*                                         processed
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
PwCreateEnetEntry (tCliHandle CliHandle, UINT4 u4PwIndex, UINT4 u4EnetInstance,
                   UINT4 u4PwEnetPwVlan, UINT4 u4PwEnetPortVlan,
                   UINT4 u4IfIndex, UINT4 u4Mode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                  CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed in while creating PW Enet Entry\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwEnetRowStatus (u4PwIndex, u4EnetInstance, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed in while creating PW Enet Entry\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2PwEnetPortVlan (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                 u4PwEnetPortVlan) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while associating Port Vlan to "
                   "PW Enet Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwEnetPortVlan (u4PwIndex, u4EnetInstance,
                              u4PwEnetPortVlan) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while associating Port Vlan to"
                   "PW Enet Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2PwEnetPwVlan (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                               u4PwEnetPwVlan) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while associating PW Vlan to PW "
                   "Enet Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwEnetPwVlan (u4PwIndex, u4EnetInstance,
                            u4PwEnetPwVlan) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while associating PW Vlan to PW"
                   "Enet Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2PwEnetPortIfIndex (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                    u4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while associating Port If Index to"
                   "PW Enet Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwEnetPortIfIndex (u4PwIndex, u4EnetInstance,
                                 u4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while associating Port If Index to"
                   "PW Enet Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2PwEnetVlanMode (&u4ErrorCode, u4PwIndex,
                                 u4EnetInstance, u4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while setting VLAN Mode to PW Enet"
                   "Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwEnetVlanMode (u4PwIndex, u4EnetInstance,
                              u4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Failed while setting VLAN Mode to PW Enet "
                   "Entry \r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex,
                                  u4EnetInstance, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed while making Up PW Enet Entry\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwEnetRowStatus (u4PwIndex, u4EnetInstance,
                               ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed while making Up PW Enet Entry \r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2vpnCliGetSaii                                    */
/*                                                                           */
/*     DESCRIPTION      : This function takes Saii input either as IP address*/
/*                        or as a string from CLI and returns the OctetString*/
/*                        in L2vpnCliArgs                                    */
/*                        structure                                          */
/*                        pu1Saii         - Pointer to Saii String           */
/*                        pu4Saii         - Pointer to Saii Ip Address       */
/*                        u1AiiFormat     - Bits set for AII Format          */
/*                                                                           */
/*     OUTPUT           : pL2vpnCliArgs - Pointer to the L2vpnCliArgs        */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
L2vpnCliGetSaii (UINT4 *pu4Saii, UINT1 *pu1Saii, tL2vpnCliArgs * pL2vpnCliArgs)
{
    static UINT1        au1Saii[L2VPN_PWVC_MAX_AI_LEN];
    UINT4               u4SrcAii = L2VPN_ZERO;

    MEMSET (au1Saii, 0, L2VPN_PWVC_MAX_AI_LEN);

    pL2vpnCliArgs->Saii.pu1_OctetList = au1Saii;

    switch ((pL2vpnCliArgs->u1AiiFormat) & (L2VPN_PWVC_SAII_IP))
    {
        case L2VPN_PWVC_SAII_IP:
        {
            u4SrcAii = OSIX_HTONL (*pu4Saii);
            pL2vpnCliArgs->Saii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE1_LEN;
            MEMCPY (pL2vpnCliArgs->Saii.pu1_OctetList, &u4SrcAii,
                    sizeof (UINT4));
            break;
        }
        default:
        {
            if (STRLEN (pu1Saii) > L2VPN_PWVC_MAX_AI_LEN)
            {
                return CLI_FAILURE;
            }

            pL2vpnCliArgs->Saii.i4_Length = STRLEN (pu1Saii);
            MEMCPY (pL2vpnCliArgs->Saii.pu1_OctetList, pu1Saii,
                    pL2vpnCliArgs->Saii.i4_Length);

            break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2vpnCliGetTaii                                    */
/*                                                                           */
/*     DESCRIPTION      : This function takes Taii input either as IP address*/
/*                        or as a string from CLI and returns the OctetString*/
/*                        in L2vpnCliArgs                                    */
/*                        structure                                          */
/*                        pu1Taii         - Pointer to Taii String           */
/*                        pu4Taii         - Pointer to Taii Ip Address       */
/*                        u1AiiFormat     - Bits set for AII Format          */
/*                                                                           */
/*     OUTPUT           : pL2vpnCliArgs - Pointer to the L2vpnCliArgs        */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT1
L2vpnCliGetTaii (UINT4 *pu4Taii, UINT1 *pu1Taii, tL2vpnCliArgs * pL2vpnCliArgs)
{
    static UINT1        au1Taii[L2VPN_PWVC_MAX_AI_LEN];
    UINT4               u4TargetAii = L2VPN_ZERO;

    MEMSET (au1Taii, 0, L2VPN_PWVC_MAX_AI_LEN);

    pL2vpnCliArgs->Taii.pu1_OctetList = au1Taii;

    switch ((pL2vpnCliArgs->u1AiiFormat) & (L2VPN_PWVC_TAII_IP))
    {
        case L2VPN_PWVC_TAII_IP:
        {
            u4TargetAii = OSIX_HTONL (*pu4Taii);
            pL2vpnCliArgs->Taii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE1_LEN;
            MEMCPY (pL2vpnCliArgs->Taii.pu1_OctetList, &u4TargetAii,
                    sizeof (UINT4));
            break;
        }
        default:
        {
            if (STRLEN (pu1Taii) > L2VPN_PWVC_MAX_AI_LEN)
            {
                return CLI_FAILURE;
            }
            pL2vpnCliArgs->Taii.i4_Length = STRLEN (pu1Taii);
            MEMCPY (pL2vpnCliArgs->Taii.pu1_OctetList, pu1Taii,
                    pL2vpnCliArgs->Taii.i4_Length);

            break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2vpnCliGetAgi                                     */
/*                                                                           */
/*     DESCRIPTION      : This function takes AGI input from CLI and         */
/*                        returns the Octet string to L2vpnCliArgs structure */
/*                        pu1Agi          - Pointer to AGI string            */
/*                                                                           */
/*     OUTPUT           : pL2vpnCliArgs - Pointer to the L2vpnCliArgs        */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT1
L2vpnCliGetAgi (UINT1 *pu1Agi, tL2vpnCliArgs * pL2vpnCliArgs)
{
    static UINT1        au1Agi[L2VPN_PWVC_MAX_AI_LEN];

    if (pu1Agi[0] != L2VPN_PWVC_AGI_TYPE0 && pu1Agi[0] != L2VPN_PWVC_AGI_TYPE1
        && pu1Agi[0] != L2VPN_PWVC_AGI_TYPE2)
    {
        if (STRLEN (pu1Agi) > L2VPN_PWVC_MAX_AI_LEN)
        {
            return CLI_FAILURE;
        }
    }

    MEMSET (au1Agi, 0, L2VPN_PWVC_MAX_AI_LEN);

    pL2vpnCliArgs->Agi.pu1_OctetList = au1Agi;
    if (pu1Agi[0] != L2VPN_PWVC_AGI_TYPE0 && pu1Agi[0] != L2VPN_PWVC_AGI_TYPE1
        && pu1Agi[0] != L2VPN_PWVC_AGI_TYPE2)
    {
        pL2vpnCliArgs->Agi.i4_Length = STRLEN (pu1Agi);
    }
    else
    {
        pL2vpnCliArgs->Agi.i4_Length = L2VPN_PWVC_MAX_AGI_LEN;
    }

    MEMCPY (pL2vpnCliArgs->Agi.pu1_OctetList, pu1Agi,
            pL2vpnCliArgs->Agi.i4_Length);
    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : MplsVfiShowRunningConfig
 *  Input         : tCliHandle CliHadle                                
 *  Description   : This function shows the Mpls Pw entries and the 
 *                  corresponding neighbors
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
MplsVfiShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LocalCapabAdvert;
    tSNMP_OCTET_STRING_TYPE VplsName;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE VpnId;
    tSNMP_OCTET_STRING_TYPE Saii;
    tSNMP_OCTET_STRING_TYPE Taii;
    tSNMP_OCTET_STRING_TYPE Agi;
    tSNMP_OCTET_STRING_TYPE AiiFormat;
    tSNMP_OCTET_STRING_TYPE LocalCcType;
    tSNMP_OCTET_STRING_TYPE RemoteCcType;
    tSNMP_OCTET_STRING_TYPE LocalCvType;
    tSNMP_OCTET_STRING_TYPE RemoteCvType;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    UINT4               u4RemoteLabel = 0;
    UINT4               u4LocalLabel = 0;
    UINT4               u4VplsIndexPrev = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4TunnelId = 0;
    UINT4               u4InTunnelId = 0;
    UINT4               u4PeerAddr = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4CfaIfIndex = L2VPN_ZERO;
    UINT4               u4VpnId = 0;
    UINT4               u4LocalAiiType = 0;
    UINT4               u4RemoteAiiType = 0;
    UINT4               u4PwId = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4SrcGlobalId = 0;
    UINT4               u4DestGlobalId = 0;
    UINT4               u4SrcNodeId = 0;
    UINT4               u4DestNodeId = 0;
    UINT4               u4SrcAcId = 0;
    UINT4               u4DestAcId = 0;
    UINT1               u1MplsType = 0;
    UINT1               u1LocalCapabAdvert = L2VPN_ZERO;
    UINT1               u1LocalCcType = L2VPN_ZERO;
    UINT1               u1RemoteCcType = L2VPN_ZERO;
    UINT1               u1LocalCvType = L2VPN_ZERO;
    UINT1               u1RemoteCvType = L2VPN_ZERO;
    CHR1               *pc1PeerAddr = NULL;
    CHR1               *pAiiString = NULL;
    BOOL1               bLoop = FALSE;
    BOOL1               bIsVPWS = L2VPN_FALSE;
    BOOL1               bIsStaticPw = FALSE;
    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };
    static UINT1        au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN] = { 0 };
    static UINT1        au1PeerAddr[IPV6_ADDR_LENGTH] = { 0 };

    UINT1               au1Saii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];    /*Allocating maximum size for
                                                                   Saii and Taii (i.e) Length of Aii type 2 */
    UINT1               au1Taii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];
    UINT1               au1Agi[L2VPN_PWVC_MAX_AGI_LEN + 1];
    UINT1               au1AiiFormat[MPLS_ONE] = { 0 };
    UINT4               u4SrcAii = 0;
    UINT4               u4TargetAii = 0;
    INT4                i4IsStaticPw = L2VPN_ZERO;
    INT4                i4VplsFdbId = 0;
    INT4                i4ControlWord = 0;
    UINT4               u4ContextId = CLI_GET_CXT_ID ();
    UINT1               au1ContextName[L2IWF_CONTEXT_ALIAS_LEN + 1];
    INT1                ai1IfAliasName[CFA_MAX_IFALIAS_LENGTH];
    UINT4               u4Mtu = 0;
    INT4                i4PwPeerAddrType = 0;
#ifdef HVPLS_WANTED
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    INT4                u4VplsPwBindType = 0;
    UINT4               u4RedGroupIndex = 0;
    UINT4               u4RedGrpIndex = 0;
    UINT4               u4RedGroupPwIndex = 0;
    INT4                i4HoldingPriority = 0;
    UINT4               u4Flag = FALSE;
    UINT4               u4RdFlag = TRUE;
    INT4                i4Preference = 0;
#endif
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ip6Addr;
#endif

    MEMSET (au1Saii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);
    MEMSET (au1Taii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);
    MEMSET (au1Agi, 0, L2VPN_PWVC_MAX_AGI_LEN + 1);
    MEMSET (au1ContextName, 0, (L2IWF_CONTEXT_ALIAS_LEN + 1));
#ifdef MPLS_IPV6_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif

    LocalCapabAdvert.pu1_OctetList = &u1LocalCapabAdvert;
    LocalCcType.pu1_OctetList = &u1LocalCcType;
    RemoteCcType.pu1_OctetList = &u1RemoteCcType;
    LocalCvType.pu1_OctetList = &u1LocalCvType;
    RemoteCvType.pu1_OctetList = &u1RemoteCvType;

    Saii.pu1_OctetList = au1Saii;
    Taii.pu1_OctetList = au1Taii;
    Agi.pu1_OctetList = au1Agi;
    AiiFormat.pu1_OctetList = au1AiiFormat;

    Saii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE2_LEN;
    Taii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE2_LEN;
    Agi.i4_Length = L2VPN_PWVC_MAX_AGI_LEN;
    AiiFormat.i4_Length = sizeof (UINT1);

    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerAddr.i4_Length = IPV6_ADDR_LENGTH;
    VpnId.pu1_OctetList = au1VpnId;
    VpnId.i4_Length = L2VPN_MAX_VPLS_VPNID_LEN;
    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    VcmGetAliasName (u4ContextId, au1ContextName);

    CliPrintf (CliHandle, "\r\n!\n");
    if (STRLEN (au1ContextName) != 0)
    {
        CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
    }

    while (1)
    {
        /* u4VplsIndex = 0; */
        MPLS_L2VPN_LOCK ();
        /* get next entry */
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)))
        {
            MPLS_L2VPN_UNLOCK ();
            CliPrintf (CliHandle, "\r\n!\r\n");
            return CLI_SUCCESS;
        }
        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex)))
        {
            MPLS_L2VPN_UNLOCK ();
            CliPrintf (CliHandle, "\r\n!\r\n");
            return CLI_SUCCESS;
        }

        bLoop = TRUE;
        u4VplsIndexPrev = u4VplsIndex;
        MEMSET (au1VplsName, L2VPN_ZERO, sizeof (au1VplsName));
        /* get vpls name */
        if (SNMP_FAILURE == nmhGetFsMplsVplsName (u4VplsIndex, &VplsName))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get vpls vpn id */
        if (SNMP_FAILURE == nmhGetFsMplsVplsVpnId (u4VplsIndex, &VpnId))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* VPLS FDB */
        /* get vpls mapped fdb identifier */
        if (SNMP_FAILURE == nmhGetFsMplsVplsL2MapFdbId (u4VplsIndex,
                                                        &i4VplsFdbId))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* if PseudoWire mode is VPLS the display the details */
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

        if (pVplsEntry == NULL)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* VPLS configuration should be printed for VPLS PWs only */
        bIsVPWS = L2VPN_FALSE;
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

            if (L2VPN_VPWS == L2VPN_PWVC_MODE (pPwEntry))
            {
                bIsVPWS = L2VPN_TRUE;
            }
            break;                /* Only One Pw is needed to scan to know the PwVc Mode */
        }
        if (L2VPN_TRUE == bIsVPWS)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

#ifdef VPLSADS_WANTED
        if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
        {
            if (ACTIVE != L2VPN_VPLS_ROW_STATUS (pVplsEntry))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            CliPrintf (CliHandle, "\r\nl2 vfi %s autodiscover\r\n",
                       VplsName.pu1_OctetList);
            L2VpnCliShowRunningAutoDiscoveredVfi (CliHandle, pVplsEntry);
            MEMCPY (&u4VpnId, &au1VpnId[STRLEN (MPLS_OUI_VPN_ID)],
                    sizeof (UINT4));
            u4VpnId = OSIX_HTONL (u4VpnId);
            if (i4VplsFdbId != L2VPN_VPLS_FDB_DEF_VAL)
            {
                CliPrintf (CliHandle, "vpn %u fdb %u", u4VpnId, i4VplsFdbId);
                CliPrintf (CliHandle, "\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "vpn %u", u4VpnId);
                CliPrintf (CliHandle, "\r\n");
            }
        }
#endif
#ifdef VPLSADS_WANTED
        if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
#endif
        {
            CliPrintf (CliHandle, "\r\nl2 vfi %s manual",
                       VplsName.pu1_OctetList);
            if (ACTIVE != L2VPN_VPLS_ROW_STATUS (pVplsEntry))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            MEMCPY (&u4VpnId, &au1VpnId[STRLEN (MPLS_OUI_VPN_ID)],
                    sizeof (UINT4));
            u4VpnId = OSIX_HTONL (u4VpnId);
/* VPLS FDB */
            if (i4VplsFdbId != L2VPN_VPLS_FDB_DEF_VAL)
            {
                CliPrintf (CliHandle, "\r\n vpn %u fdb %u",
                           u4VpnId, i4VplsFdbId);
                CliPrintf (CliHandle, "\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n vpn %u", u4VpnId);
                CliPrintf (CliHandle, "\r\n");
            }
        }

        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

            if (L2VPN_VPLS != L2VPN_PWVC_MODE (pPwEntry))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

#ifdef VPLSADS_WANTED
            if (L2VPN_PWVC_OWNER_OTHER == L2VPN_PWVC_OWNER (pPwEntry))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
#endif
            u4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);
            /* get PseudoWire peer address */
            if (SNMP_FAILURE ==
                nmhGetPwPeerAddrType (u4PwIndex, &i4PwPeerAddrType))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            if (SNMP_FAILURE == nmhGetPwPeerAddr (u4PwIndex, &PeerAddr))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /* get Pw ID */
            if (SNMP_FAILURE == nmhGetPwID (u4PwIndex, &u4PwId))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /* get Pw Group ID */
            if (SNMP_FAILURE == nmhGetPwLocalGroupID (u4PwIndex, &u4GroupId))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /* Get AII Format - String or IP Address */
            if (SNMP_FAILURE ==
                nmhGetFsMplsL2VpnPwAIIFormat (u4PwIndex, &AiiFormat))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /*Get Group Attachment ID */
            if (SNMP_FAILURE == nmhGetPwGroupAttachmentID (u4PwIndex, &Agi))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /*Get Local AttachmentID */
            if (SNMP_FAILURE == nmhGetPwLocalAttachmentID (u4PwIndex, &Saii))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /*Get Remote Attachment ID */
            if (SNMP_FAILURE == nmhGetPwPeerAttachmentID (u4PwIndex, &Taii))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            /*Get Local AII Type */
            if (nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex, &u4LocalAiiType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to get PwGenLocalAIIType\r\n");
            }
#ifdef HVPLS_WANTED
            if (nmhGetVplsPwBindType (u4VplsIndex, u4PwIndex, &u4VplsPwBindType)
                == SNMP_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                           "Unable to get Pw Bind Type\r\n");
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
#endif
            /* Get Remote AII Type */
            if (nmhGetFsMplsL2VpnPwGenRemoteAIIType
                (u4PwIndex, &u4RemoteAiiType) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to get MplsL2VpnPwGenRemoteAIIType\r\n");
            }
            /* get Inbound Label */
            if (nmhGetPwInboundLabel (u4PwIndex, &u4LocalLabel) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Unable to get PwInboundLabel\r\n");
            }

            /* get Outbound Label */
            if (nmhGetPwOutboundLabel (u4PwIndex, &u4RemoteLabel) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Unable to get PwOutboundLabel\r\n");
            }
            /* get Local Capable advert, Local CC Advert, Local CV Advert,  
             * Remote CC Advert, Remote CV Advert */
            nmhGetFsMplsL2VpnPwLocalCapabAdvert (u4PwIndex, &LocalCapabAdvert);
            nmhGetFsMplsL2VpnPwLocalCCAdvert (u4PwIndex, &LocalCcType);
            nmhGetFsMplsL2VpnPwLocalCVAdvert (u4PwIndex, &LocalCvType);
            nmhGetFsMplsL2VpnPwRemoteCCAdvert (u4PwIndex, &RemoteCcType);
            nmhGetFsMplsL2VpnPwRemoteCVAdvert (u4PwIndex, &RemoteCvType);

            /*Get the Control word status */
            nmhGetPwCwPreference (u4PwIndex, &i4ControlWord);

            if (SNMP_FAILURE ==
                nmhGetFsMplsL2VpnIsStaticPw (u4PwIndex, &i4IsStaticPw))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            /*Get Local AII Type */
            if (SNMP_FAILURE ==
                nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex, &u4LocalAiiType))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            /* Get Remote AII Type */
            if (SNMP_FAILURE ==
                nmhGetFsMplsL2VpnPwGenRemoteAIIType (u4PwIndex,
                                                     &u4RemoteAiiType))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
            if (SNMP_FAILURE == nmhGetPwLocalIfMtu (u4PwIndex, &u4Mtu))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            bIsStaticPw = (BOOL1) i4IsStaticPw;

#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
            {
                MEMCPY (Ip6Addr.u1_addr, PeerAddr.pu1_OctetList,
                        IPV6_ADDR_LENGTH);
            }
#endif
            MPLS_OCTETSTRING_TO_INTEGER ((&PeerAddr), u4PeerAddr);
            CLI_CONVERT_IPADDR_TO_STR (pc1PeerAddr, u4PeerAddr);
#ifdef HVPLS_WANTED
            pRgPw = L2VpnGetRedundancyPwEntryByPwIndex (u4PwIndex);
            if (pRgPw != NULL)
            {
                if (nmhGetFirstIndexFsL2VpnPwRedPwTable (&u4RedGroupIndex,
                                                         &u4RedGroupPwIndex)
                    != SNMP_FAILURE)
                {
                    do
                    {
                        if (u4RedGroupPwIndex == u4PwIndex)
                        {
                            u4RedGrpIndex = u4RedGroupIndex;
                            break;
                        }
                    }
                    while (nmhGetNextIndexFsL2VpnPwRedPwTable (u4RedGroupIndex,
                                                               &u4RedGroupIndex,
                                                               u4RedGroupPwIndex,
                                                               &u4RedGroupPwIndex)
                           == SNMP_SUCCESS);
                }
                if (nmhGetFirstIndexFsL2VpnPwRedPwTable (&u4RedGroupIndex,
                                                         &u4RedGroupPwIndex)
                    != SNMP_FAILURE)
                {
                    do
                    {
                        if (u4RedGroupPwIndex < u4PwIndex
                            && u4RedGrpIndex == u4RedGroupIndex)
                        {
                            u4RdFlag = FALSE;
                        }
                        if (u4RedGroupPwIndex >= u4PwIndex)
                        {
                            break;
                        }
                    }
                    while (nmhGetNextIndexFsL2VpnPwRedPwTable (u4RedGroupIndex,
                                                               &u4RedGroupIndex,
                                                               u4RedGroupPwIndex,
                                                               &u4RedGroupPwIndex)
                           == SNMP_SUCCESS);
                }

                if (u4RdFlag == FALSE)
                {
                    CliPrintf (CliHandle, "\r\n backup peer");
                }
            }
#endif
            /* PwOwner - manual/pwidfec/genfec */
            if ((L2VPN_PWVC_OWNER_MANUAL == L2VPN_PWVC_OWNER (pPwEntry)))
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
                {
                    CliPrintf (CliHandle, "\r\n neighbor %s ",
                               Ip6PrintAddr (&Ip6Addr));
                }
                else
#endif
                {
                    CliPrintf (CliHandle, "\r\n neighbor %s ", pc1PeerAddr);
                }

                CliPrintf (CliHandle, "manual ");

                if (L2VPN_ZERO != pPwEntry->u4PwVcID)
                {
                    CliPrintf (CliHandle, "pwid %u ", pPwEntry->u4PwVcID);
                }

                CliPrintf (CliHandle, "locallabel %u ",
                           L2VPN_PWVC_INBOUND_VC_LABEL (pPwEntry));

                CliPrintf (CliHandle, "remotelabel %u ",
                           L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwEntry));
            }
            /* if the PwOwner is not MANUAL then continue */
            else if (L2VPN_PWVC_OWNER_PWID_FEC_SIG ==
                     L2VPN_PWVC_OWNER (pPwEntry))
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
                {
                    CliPrintf (CliHandle, " neighbor %s ",
                               Ip6PrintAddr (&Ip6Addr));
                }
                else
#endif
                {
                    CliPrintf (CliHandle, " neighbor %s ", pc1PeerAddr);
                }

                CliPrintf (CliHandle, "pwidfec ");

                CliPrintf (CliHandle, "pwid %u", u4PwId);

                CliPrintf (CliHandle, "groupid %u", u4GroupId);

                if ((i4IsStaticPw == TRUE)
                    &&
                    ((u4LocalLabel != L2VPN_LDP_INVALID_LABEL) &&
                     (u4RemoteLabel != L2VPN_LDP_INVALID_LABEL)))
                {
                    CliPrintf (CliHandle, "locallabel ");
                    CliPrintf (CliHandle, "%u ", u4LocalLabel);
                    CliPrintf (CliHandle, "remotelabel ");
                    CliPrintf (CliHandle, "%u ", u4RemoteLabel);
                }
            }
            else if (L2VPN_PWVC_OWNER_GEN_FEC_SIG ==
                     L2VPN_PWVC_OWNER (pPwEntry))
            {
                if ((L2VPN_GEN_FEC_AII_TYPE_2 == u4LocalAiiType) &&
                    (L2VPN_GEN_FEC_AII_TYPE_2 == u4RemoteAiiType))
                {

                    L2VpnExtractGenFecType2Identifiers (Saii.pu1_OctetList,
                                                        &u4SrcGlobalId,
                                                        &u4SrcNodeId,
                                                        &u4SrcAcId);
                    L2VpnExtractGenFecType2Identifiers (Taii.pu1_OctetList,
                                                        &u4DestGlobalId,
                                                        &u4DestNodeId,
                                                        &u4DestAcId);

                    CliPrintf (CliHandle, " neighbor ");

                    CliPrintf (CliHandle, "global-id %u ", u4DestGlobalId);

                    CliPrintf (CliHandle, "node-id %u ", u4DestNodeId);

                    CliPrintf (CliHandle, "genfec ");

                    if ((*Saii.pu1_OctetList) != 0)
                    {
                        CliPrintf (CliHandle, "agi ");
                        CliPrintf (CliHandle, "%s ", Agi);
                    }

                    CliPrintf (CliHandle, "src-ac-id %u ", u4SrcAcId);

                    CliPrintf (CliHandle, "dst-ac-id %u ", u4DestAcId);

                    CliPrintf (CliHandle, "pwid ");
                    CliPrintf (CliHandle, "%u ", u4PwId);

                    if ((u4LocalLabel != L2VPN_LDP_INVALID_LABEL) &&
                        (u4RemoteLabel != L2VPN_LDP_INVALID_LABEL))
                    {
                        CliPrintf (CliHandle, "locallabel ");
                        CliPrintf (CliHandle, "%u ", u4LocalLabel);
                        CliPrintf (CliHandle, "remotelabel ");
                        CliPrintf (CliHandle, "%u ", u4RemoteLabel);
                    }
                }
                else
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
                    {
                        CliPrintf (CliHandle, " neighbor %s ",
                                   Ip6PrintAddr (&Ip6Addr));
                    }
                    else
#endif
                    {
                        CliPrintf (CliHandle, " neighbor %s ", pc1PeerAddr);
                    }

                    CliPrintf (CliHandle, "genfec ");
                    au1Agi[Agi.i4_Length] = '\0';

                    if ((*Saii.pu1_OctetList) != 0)
                    {
                        CliShowRunAgiValue (CliHandle, au1Agi);

                        CliPrintf (CliHandle, "saii ");

                        if ((AiiFormat.pu1_OctetList[0]) & (L2VPN_PWVC_SAII_IP))
                        {
                            MEMCPY ((UINT1 *) (&u4SrcAii), Saii.pu1_OctetList,
                                    IPV4_ADDR_LENGTH);

                            u4SrcAii = OSIX_HTONL (u4SrcAii);
                            CLI_CONVERT_IPADDR_TO_STR (pAiiString, u4SrcAii);
                            CliPrintf (CliHandle, "%s ", pAiiString);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%s ", au1Saii);
                        }
                    }
                    if ((*Taii.pu1_OctetList) != 0)
                    {
                        CliPrintf (CliHandle, "taii ");

                        if ((AiiFormat.pu1_OctetList[0]) & (L2VPN_PWVC_TAII_IP))
                        {
                            MEMCPY ((UINT1 *) (&u4TargetAii),
                                    Taii.pu1_OctetList, IPV4_ADDR_LENGTH);

                            u4TargetAii = OSIX_HTONL (u4TargetAii);
                            CLI_CONVERT_IPADDR_TO_STR (pAiiString, u4TargetAii);
                            CliPrintf (CliHandle, "%s ", pAiiString);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%s ", au1Taii);
                        }
                    }
                    if ((i4IsStaticPw == TRUE)
                        &&
                        ((u4LocalLabel != L2VPN_LDP_INVALID_LABEL) &&
                         (u4RemoteLabel != L2VPN_LDP_INVALID_LABEL)))
                    {
                        CliPrintf (CliHandle, "locallabel ");
                        CliPrintf (CliHandle, "%u ", u4LocalLabel);
                        CliPrintf (CliHandle, "remotelabel ");
                        CliPrintf (CliHandle, "%u ", u4RemoteLabel);
                    }

                }
            }

            if (L2VPN_PWVC_CTRLW_PREFERRED == i4ControlWord)
            {
                CliPrintf (CliHandle, "control-word enable ");
            }

            if ((LocalCapabAdvert.pu1_OctetList[0] & L2VPN_PW_VCCV_CAPABLE) !=
                L2VPN_PW_VCCV_CAPABLE)
            {
                CliPrintf (CliHandle, "vccv disable ");
            }
#ifdef HVPLS_WANTED
            if (u4VplsPwBindType == L2VPN_VPLS_PW_MESH)
            {
#endif
                CliPrintf (CliHandle, "encapsulation mpls ");
#ifdef HVPLS_WANTED
            }
            else
            {
                CliPrintf (CliHandle, "encapsulation mpls no-split-horizon ");
            }
#endif
            CliPrintf (CliHandle, "mplstype ");

            /* mplstype/Pwmode - te/non-te/vconly */
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwEntry));
            if (L2VPN_MPLS_TYPE_TE == u1MplsType)
            {
                pPwVcMplsTnlEntry =
                    L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                               L2VPN_PWVC_PSN_ENTRY (pPwEntry));

                pPwVcMplsInTnlEntry =
                    L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                              L2VPN_PWVC_PSN_ENTRY (pPwEntry));

                CliPrintf (CliHandle, "te ");
                u4TunnelId = L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pPwVcMplsTnlEntry);
                u4InTunnelId = L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX
                    (pPwVcMplsInTnlEntry);
                CliPrintf (CliHandle, "%u %u", u4TunnelId, u4InTunnelId);
            }

            else if (L2VPN_MPLS_TYPE_NONTE == u1MplsType)
            {
                CliPrintf (CliHandle, "non-te");
            }
            else if (L2VPN_MPLS_TYPE_VCONLY == u1MplsType)
            {
                CliPrintf (CliHandle, "vconly");
            }
            CliPrintf (CliHandle, " pwtype ");
            /* PwType - ethernet/ethernettagged */
            if (L2VPN_PWVC_TYPE_ETH == L2VPN_PWVC_TYPE (pPwEntry))
            {
                CliPrintf (CliHandle, "ethernet");
            }
            else if (L2VPN_PWVC_TYPE_ETH_VLAN == L2VPN_PWVC_TYPE (pPwEntry))
            {
                CliPrintf (CliHandle, "ethernettagged");
            }
            if (u4Mtu != CFA_ENET_MTU)
            {
                CliPrintf (CliHandle, " mtu %u", u4Mtu);
            }
#ifdef HVPLS_WANTED
            if (nmhGetFirstIndexFsL2VpnPwRedPwTable (&u4RedGroupIndex,
                                                     &u4RedGroupPwIndex) !=
                SNMP_FAILURE)
            {
                do
                {
                    if (u4RedGroupPwIndex == u4PwIndex)
                    {
                        u4Flag = TRUE;
                        u4RdFlag = TRUE;
                        break;
                    }
                }
                while (nmhGetNextIndexFsL2VpnPwRedPwTable (u4RedGroupIndex,
                                                           &u4RedGroupIndex,
                                                           u4RedGroupPwIndex,
                                                           &u4RedGroupPwIndex)
                       == SNMP_SUCCESS);
                if (u4Flag == TRUE)
                {
                    CliPrintf (CliHandle,
                               " pseudowire-redundancy class-id %u\r\n",
                               u4RedGroupIndex);

                    if (nmhGetFsL2VpnPwRedPwPreferance (u4RedGroupIndex,
                                                        u4RedGroupPwIndex,
                                                        &i4Preference) ==
                        SNMP_SUCCESS)
                    {
                        if (i4Preference != L2VPNRED_PW_PREFERENCE_SECONDARY)
                        {
                            CliPrintf (CliHandle, " pwid %u deactivate\r\n",
                                       u4PwId);
                            CliPrintf (CliHandle,
                                       " pwid %u preference primary\r\n",
                                       u4PwId);
                            CliPrintf (CliHandle, " pwid %u activate\r\n",
                                       u4PwId);
                        }
                    }
                    if (nmhGetPwHoldingPriority (u4PwIndex, &i4HoldingPriority)
                        == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, " pwid %u deactivate\r\n",
                                   u4PwId);
                        CliPrintf (CliHandle,
                                   " pwid %u holdingPriority %d \r\n", u4PwId,
                                   i4HoldingPriority);
                        CliPrintf (CliHandle, " pwid %u activate\r\n", u4PwId);
                    }
                }
                u4Flag = FALSE;
            }
#endif
            if ((nmhGetPwIfIndex (u4PwIndex, (INT4 *) &u4CfaIfIndex) ==
                 SNMP_SUCCESS) && (u4CfaIfIndex != L2VPN_ZERO))
            {
                if (CfaCliGetIfName (u4CfaIfIndex, ai1IfAliasName) ==
                    CFA_SUCCESS)
                {
                    CliPrintf (CliHandle, " map pwid %u pw %u \r\n", u4PwIndex,
                               CLI_ATOI (ai1IfAliasName + 2));
                }
            }

            CliPrintf (CliHandle, "\r\n\n");
            MPLSShowRunningConfigPwOamDisplay (CliHandle, u4PwId,
                                               LocalCcType.pu1_OctetList[0],
                                               LocalCvType.pu1_OctetList[0],
                                               RemoteCcType.pu1_OctetList[0],
                                               RemoteCvType.pu1_OctetList[0],
                                               i4ControlWord, bIsStaticPw);

        }
        CliPrintf (CliHandle, "\r\n!\n");
        MPLS_L2VPN_UNLOCK ();
    }
}

/******************************************************************************
 *  
 *  Function Name : MplsVpwsShowShowRunningConfig
 *
 *  Input         : tCliHandle  CliHandle
 *                  INT4        i4TestEnetIfIndex - Interface Index
 *                  INT4        u4TestPortVlanId  - PortVlan Id 
 *                  INT4        i4TestPwType      - PwType
 *                  UINT1       *pu1Display       - Pointer to a flag which 
 *                                                  indicates whether to display
 *                                                  the config or not
 *
 *  Output(s)     : UINT1       *pu1ShowConfig - Pointer to a flag which
 *                                                   is updated to OSIX_TRUE
 *                                                   if mpls related config are
 *                                                   present on the given 
 *                                                   interface
 *
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT4
MplsVpwsShowRunningConfig (tCliHandle CliHandle, INT4 i4TestEnetIfIndex,
                           UINT4 u4TestPortVlanId, INT4 i4TestPwType,
                           UINT4 u4VplsIndex, UINT4 u4ContextId,
                           UINT1 *pu1Display, UINT1 *pu1ShowConfig)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE PwMplsMplsTypes;
    tSNMP_OCTET_STRING_TYPE Saii;
    tSNMP_OCTET_STRING_TYPE Taii;
    tSNMP_OCTET_STRING_TYPE Agi;
    tSNMP_OCTET_STRING_TYPE AiiFormat;
    tSNMP_OCTET_STRING_TYPE LocalCapabAdvert;
    tSNMP_OCTET_STRING_TYPE LocalCcType;
    tSNMP_OCTET_STRING_TYPE RemoteCcType;
    tSNMP_OCTET_STRING_TYPE LocalCvType;
    tSNMP_OCTET_STRING_TYPE RemoteCvType;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4RemoteLabel = 0;
    UINT4               u4LocalLabel = 0;
    UINT4               u4TunnelId = 0;
    UINT4               u4InTunnelId = 0;
    UINT4               u4PeerAddr = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4PwVlanId = 0;
    UINT4               u4CfaIfIndex = L2VPN_ZERO;
    UINT4               u4PortVlanId = 0;
    UINT4               u4LocalAiiType = 0;
    UINT4               u4RemoteAiiType = 0;
    INT4                i4EnetIfIndex = 0;
    INT4                i4HoldingPriority = 0;
    INT4                i4VlanMode = 0;
    INT4                i4PwOwner = 0;
    INT4                i4PwMode = 0;
    INT4                i4PwType = 0;
    INT4                i4OamStatus = 0;
    INT4                i4ControlWord = 0;
    INT4                i4AdminStatus = 0;
    INT1               *pi1IfName = NULL;
    INT4                i4PwPeerAddrType = 0;
    UINT4               u4PwVcId = 0;
    UINT4               u4GroupId = 0;
    UINT4               u4SrcGlobalId = 0;
    UINT4               u4DestGlobalId = 0;
    UINT4               u4SrcNodeId = 0;
    UINT4               u4DestNodeId = 0;
    UINT4               u4SrcAcId = 0;
    UINT4               u4DestAcId = 0;
    UINT1               u1LocalCapabAdvert = L2VPN_ZERO;
    UINT1               u1LocalCcType = L2VPN_ZERO;
    UINT1               u1RemoteCcType = L2VPN_ZERO;
    UINT1               u1LocalCvType = L2VPN_ZERO;
    UINT1               u1RemoteCvType = L2VPN_ZERO;
    CHR1               *pc1PeerAddr = NULL;
    BOOL1               bLoop = FALSE;
    BOOL1               bIsStaticPw = FALSE;
    BOOL1               bRdflag = FALSE;
    BOOL1               bRdBackup = FALSE;
    INT4                i4IsStaticPw = L2VPN_ZERO;
    static UINT1        au1PeerAddr[IPV6_ADDR_LENGTH] = { 0 };
    static UINT1        au1MplsMplsType[1] = { 0 };
    UINT1               au1Saii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];    /*Allocating maximum size for
                                                                   Saii and Taii (i.e) Length of Aii type 2 */
    UINT1               au1Taii[L2VPN_PWVC_MAX_AI_TYPE2_LEN];
    UINT1               au1Agi[L2VPN_PWVC_MAX_AGI_LEN + 1];
    UINT1               au1AiiFormat[MPLS_ONE] = { 0 };
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    CHR1               *pAiiString = NULL;
    UINT4               u4SrcAii = 0;
    UINT4               u4TargetAii = 0;
    UINT4               u4RedGroupIndex = 0;
    UINT4               u4RedGroupPwIndex = 0;
    UINT4               u4Flag = FALSE;
    INT4                i4Preference = 0;
    INT1                ai1IfAliasName[CFA_MAX_IFALIAS_LENGTH];
    UINT4               u4Mtu = 0;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ip6Addr;
#endif

    MEMSET (au1Saii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);
    MEMSET (au1Taii, 0, L2VPN_PWVC_MAX_AI_TYPE2_LEN);
    MEMSET (au1Agi, 0, L2VPN_PWVC_MAX_AGI_LEN + 1);
#ifdef MPLS_IPV6_WANTED
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
#endif

    Saii.pu1_OctetList = au1Saii;
    Taii.pu1_OctetList = au1Taii;
    Agi.pu1_OctetList = au1Agi;
    AiiFormat.pu1_OctetList = au1AiiFormat;

    Saii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE2_LEN;
    Taii.i4_Length = L2VPN_PWVC_MAX_AI_TYPE2_LEN;
    Agi.i4_Length = L2VPN_PWVC_MAX_AGI_LEN;
    AiiFormat.i4_Length = sizeof (UINT1);
    PeerAddr.pu1_OctetList = au1PeerAddr;
    PwMplsMplsTypes.pu1_OctetList = au1MplsMplsType;
    PeerAddr.i4_Length = IPV6_ADDR_LENGTH;
    LocalCapabAdvert.pu1_OctetList = &u1LocalCapabAdvert;
    LocalCcType.pu1_OctetList = &u1LocalCcType;
    RemoteCcType.pu1_OctetList = &u1RemoteCcType;
    LocalCvType.pu1_OctetList = &u1LocalCvType;
    RemoteCvType.pu1_OctetList = &u1RemoteCvType;
    pi1IfName = (INT1 *) &au1IfName[0];
    /* Only default context is supported */
    if (u4ContextId != MPLS_DEF_CONTEXT_ID)
    {
        return CLI_SUCCESS;
    }
    while (1)
    {
        /* u4PwIndex = 0; */
        MPLS_L2VPN_LOCK ();
        /* u4VplsIndex will be a valid value if it is sent from 
         * VLAN module. Else it will be ZERO
         */

        if (u4VplsIndex != L2VPN_ZERO)
        {
            if (bLoop == TRUE && bRdBackup == TRUE)
            {
                MPLS_L2VPN_UNLOCK ();
                return CLI_SUCCESS;
            }
            if (bRdflag == TRUE)
            {
                if (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex) ==
                    SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    return CLI_SUCCESS;
                }
                bRdflag = FALSE;
                bRdBackup = TRUE;

            }
            else
            {
                u4PwIndex = L2VpnGetPwIndexFromVplsIndexForVpws (u4VplsIndex);
            }
            pRgPw = L2VpnGetRedundancyPwEntryByPwIndex (u4PwIndex);
            if (pRgPw != NULL && bRdBackup == FALSE)
            {
                bRdflag = TRUE;
            }
            else
            {
                if (nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex) ==
                    SNMP_FAILURE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    return CLI_SUCCESS;
                }
            }

        }
        else
        {
            /* get first entry  */
            if ((FALSE == bLoop) &&
                (SNMP_FAILURE == nmhGetFirstIndexPwTable (&u4PwIndex)))
            {
                MPLS_L2VPN_UNLOCK ();
                return CLI_SUCCESS;
            }
            /* get next entry  */
            if ((TRUE == bLoop) &&
                (SNMP_FAILURE ==
                 nmhGetNextIndexPwTable (u4PwIndexPrev, &u4PwIndex)))
            {
                MPLS_L2VPN_UNLOCK ();
                return CLI_SUCCESS;
            }
        }

        u4PwIndexPrev = u4PwIndex;
        bLoop = TRUE;
        /* get PwMode */
        if (SNMP_FAILURE == nmhGetFsMplsL2VpnPwMode (u4PwIndex, &i4PwMode))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }
        if (L2VPN_VPWS != i4PwMode)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get PwOwner */
        if (SNMP_FAILURE == nmhGetPwOwner (u4PwIndex, &i4PwOwner))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get PwType */
        if (SNMP_FAILURE == nmhGetPwType (u4PwIndex, &i4PwType))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (SNMP_FAILURE == nmhGetPwLocalIfMtu (u4PwIndex, &u4Mtu))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (i4TestPwType != i4PwType)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get Enet IfIndex, VlanMode and VlanId */
        MPLS_L2VPN_UNLOCK ();
        MplsPwEnetGet (u4PwIndex, &i4EnetIfIndex, &i4VlanMode, &u4PwVlanId,
                       &u4PortVlanId);
        MPLS_L2VPN_LOCK ();
        if (MPLS_PWVC_TYPE_ETH == i4TestPwType)
        {
            /* if IfIndex matches */
            if (i4TestEnetIfIndex != i4EnetIfIndex)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        else
        {                        /* if PortVlanId matches */
            if (u4TestPortVlanId != u4PortVlanId)
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        /* get Pw ID */
        if (SNMP_FAILURE == nmhGetPwID (u4PwIndex, &u4PwVcId))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get Group ID */
        if (SNMP_FAILURE == nmhGetPwLocalGroupID (u4PwIndex, &u4GroupId))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* Get AII Format - String or IP Address */
        if (SNMP_FAILURE ==
            nmhGetFsMplsL2VpnPwAIIFormat (u4PwIndex, &AiiFormat))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /*Get Group Attachment ID */
        if (SNMP_FAILURE == nmhGetPwGroupAttachmentID (u4PwIndex, &Agi))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /*Get Local AttachmentID */
        if (SNMP_FAILURE == nmhGetPwLocalAttachmentID (u4PwIndex, &Saii))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /*Get Remote Attachment ID */
        if (SNMP_FAILURE == nmhGetPwPeerAttachmentID (u4PwIndex, &Taii))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /*Get Local AII Type */
        if (SNMP_FAILURE ==
            nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex, &u4LocalAiiType))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* Get Remote AII Type */
        if (SNMP_FAILURE ==
            nmhGetFsMplsL2VpnPwGenRemoteAIIType (u4PwIndex, &u4RemoteAiiType))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get Inbound Label */
        if (SNMP_FAILURE == nmhGetPwInboundLabel (u4PwIndex, &u4LocalLabel))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get Outbound Label */
        if (SNMP_FAILURE == nmhGetPwOutboundLabel (u4PwIndex, &u4RemoteLabel))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /*Get the Control word status */
        if (SNMP_FAILURE == nmhGetPwCwPreference (u4PwIndex, &i4ControlWord))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /*Get the Admin Status */
        if (SNMP_FAILURE == nmhGetPwAdminStatus (u4PwIndex, &i4AdminStatus))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get Mpls type */
        if (SNMP_FAILURE == nmhGetPwMplsMplsType (u4PwIndex, &PwMplsMplsTypes))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        /* get Outbound tunnel index */
        if (L2VPN_MPLS_TYPE_TE == PwMplsMplsTypes.pu1_OctetList[0])
        {
            if (SNMP_FAILURE ==
                nmhGetPwMplsOutboundTunnelIndex (u4PwIndex, &u4TunnelId))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            if (SNMP_FAILURE ==
                nmhGetFsPwMplsInboundTunnelIndex (u4PwIndex, &u4InTunnelId))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }
        }
        /* get PseudoWire peer address */
        if (SNMP_FAILURE == nmhGetPwPeerAddr (u4PwIndex, &PeerAddr))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (SNMP_FAILURE == nmhGetPwPeerAddrType (u4PwIndex, &i4PwPeerAddrType))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (SNMP_FAILURE ==
            nmhGetFsMplsL2VpnPwOamEnable (u4PwIndex, &i4OamStatus))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        /* get Local Capable advert, Local CC Advert, Local CV Advert,  
         * Remote CC Advert, Remote CV Advert */
        if (nmhGetFsMplsL2VpnPwLocalCapabAdvert (u4PwIndex, &LocalCapabAdvert)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsL2VpnPwLocalCCAdvert (u4PwIndex, &LocalCcType) ==
            SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsL2VpnPwLocalCVAdvert (u4PwIndex, &LocalCvType) ==
            SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsL2VpnPwRemoteCCAdvert (u4PwIndex, &RemoteCcType) ==
            SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if (nmhGetFsMplsL2VpnPwRemoteCVAdvert (u4PwIndex, &RemoteCvType) ==
            SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        /* Find whether the Pseudowire is formed by signalling or static */
        if (nmhGetFsMplsL2VpnIsStaticPw (u4PwIndex, &i4IsStaticPw) ==
            SNMP_SUCCESS)
        {
            bIsStaticPw = (BOOL1) i4IsStaticPw;
        }

        MPLS_L2VPN_UNLOCK ();

        if (*pu1Display == OSIX_FALSE)
        {
            *pu1ShowConfig = OSIX_TRUE;
            return CLI_SUCCESS;
        }

#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
        {
            MEMCPY (Ip6Addr.u1_addr, PeerAddr.pu1_OctetList, IPV6_ADDR_LENGTH);
        }
#endif
        MPLS_OCTETSTRING_TO_INTEGER ((&PeerAddr), u4PeerAddr);
        CLI_CONVERT_IPADDR_TO_STR (pc1PeerAddr, u4PeerAddr);
        CliPrintf (CliHandle, "!\r\n");

        if (L2VPN_ZERO != u4TestPortVlanId)
        {
            CliPrintf (CliHandle, "vlan %u\r\n", u4TestPortVlanId);
        }
        if (bRdBackup == TRUE)
        {
            CliPrintf (CliHandle, "\r\n backup peer mpls l2transport ");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n mpls l2transport ");
        }
        /* PwOwner - manual/pwidfec/genfec */

        if (L2VPN_PWVC_OWNER_MANUAL == i4PwOwner)
        {
            CliPrintf (CliHandle, "manual ");
            if (i4PwPeerAddrType == INET_ADDR_TYPE_UNKNOWN)
            {
                CliPrintf (CliHandle, "point-to-multipoint %u", u4PeerAddr);
            }
#ifdef MPLS_IPV6_WANTED
            else if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
            {
                CliPrintf (CliHandle, "%s ", Ip6PrintAddr (&Ip6Addr));
            }
            else
#endif
            {
                CliPrintf (CliHandle, "%s ", pc1PeerAddr);
            }
            if (u4PwVcId != L2VPN_ZERO)
            {
                CliPrintf (CliHandle, "pwid ");
                CliPrintf (CliHandle, "%u ", u4PwVcId);
            }

            CliPrintf (CliHandle, "locallabel %u ", u4LocalLabel);
            CliPrintf (CliHandle, "remotelabel %u ", u4RemoteLabel);
        }
        else if (L2VPN_PWVC_OWNER_PWID_FEC_SIG == i4PwOwner)
        {
            CliPrintf (CliHandle, "pwidfec ");
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
            {
                CliPrintf (CliHandle, "%s ", Ip6PrintAddr (&Ip6Addr));
            }
            else
#endif
            {
                CliPrintf (CliHandle, "%s ", pc1PeerAddr);
            }
            CliPrintf (CliHandle, "pwid ");
            CliPrintf (CliHandle, "%u ", u4PwVcId);
            CliPrintf (CliHandle, "groupid ");
            CliPrintf (CliHandle, "%u ", u4GroupId);

            if ((i4IsStaticPw == TRUE)
                &&
                ((u4LocalLabel != L2VPN_LDP_INVALID_LABEL) &&
                 (u4RemoteLabel != L2VPN_LDP_INVALID_LABEL)))
            {
                CliPrintf (CliHandle, "locallabel ");
                CliPrintf (CliHandle, "%u ", u4LocalLabel);
                CliPrintf (CliHandle, "remotelabel ");
                CliPrintf (CliHandle, "%u ", u4RemoteLabel);
            }

        }
        else if (L2VPN_PWVC_OWNER_GEN_FEC_SIG == i4PwOwner)
        {
            CliPrintf (CliHandle, "genfec ");

            if ((L2VPN_GEN_FEC_AII_TYPE_1 == u4LocalAiiType) &&
                (L2VPN_GEN_FEC_AII_TYPE_1 == u4RemoteAiiType))
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == i4PwPeerAddrType)
                {
                    CliPrintf (CliHandle, "%s ", Ip6PrintAddr (&Ip6Addr));
                }
                else
#endif
                {
                    CliPrintf (CliHandle, "%s ", pc1PeerAddr);
                }
                au1Agi[Agi.i4_Length] = '\0';
                CliShowRunAgiValue (CliHandle, au1Agi);

                CliPrintf (CliHandle, "saii ");

                if ((AiiFormat.pu1_OctetList[0]) & (L2VPN_PWVC_SAII_IP))
                {
                    MEMCPY ((UINT1 *) (&u4SrcAii), Saii.pu1_OctetList,
                            IPV4_ADDR_LENGTH);

                    u4SrcAii = OSIX_HTONL (u4SrcAii);
                    CLI_CONVERT_IPADDR_TO_STR (pAiiString, u4SrcAii);
                    CliPrintf (CliHandle, "%s ", pAiiString);
                }
                else
                {
                    CliPrintf (CliHandle, "%s ", au1Saii);
                }

                CliPrintf (CliHandle, "taii ");
                if ((AiiFormat.pu1_OctetList[0]) & (L2VPN_PWVC_TAII_IP))
                {
                    MEMCPY ((UINT1 *) (&u4TargetAii), Taii.pu1_OctetList,
                            IPV4_ADDR_LENGTH);

                    u4TargetAii = OSIX_HTONL (u4TargetAii);
                    CLI_CONVERT_IPADDR_TO_STR (pAiiString, u4TargetAii);
                    CliPrintf (CliHandle, "%s ", pAiiString);
                }
                else
                {
                    CliPrintf (CliHandle, "%s ", au1Taii);
                }
                if (u4PwVcId != L2VPN_ZERO)
                {
                    CliPrintf (CliHandle, "pwid ");
                    CliPrintf (CliHandle, "%u ", u4PwVcId);
                }

                if ((i4IsStaticPw == TRUE)
                    &&
                    ((u4LocalLabel != L2VPN_LDP_INVALID_LABEL) &&
                     (u4RemoteLabel != L2VPN_LDP_INVALID_LABEL)))
                {
                    CliPrintf (CliHandle, "locallabel ");
                    CliPrintf (CliHandle, "%u ", u4LocalLabel);
                    CliPrintf (CliHandle, "remotelabel ");
                    CliPrintf (CliHandle, "%u ", u4RemoteLabel);
                }
            }

            else if ((L2VPN_GEN_FEC_AII_TYPE_2 == u4LocalAiiType)
                     && (L2VPN_GEN_FEC_AII_TYPE_2 == u4RemoteAiiType))
            {
                L2VpnExtractGenFecType2Identifiers (Saii.pu1_OctetList,
                                                    &u4SrcGlobalId,
                                                    &u4SrcNodeId, &u4SrcAcId);
                L2VpnExtractGenFecType2Identifiers (Taii.pu1_OctetList,
                                                    &u4DestGlobalId,
                                                    &u4DestNodeId, &u4DestAcId);

                if (u4DestGlobalId == 0)
                {
                    CliPrintf (CliHandle, "point-to-multipoint %u", u4PeerAddr);
                }
                else
                {
                    CliPrintf (CliHandle, "global-id ");
                    CliPrintf (CliHandle, "%u ", u4DestGlobalId);
                    CliPrintf (CliHandle, "node-id ");
                    CliPrintf (CliHandle, "%u ", u4DestNodeId);
                }

                au1Agi[Agi.i4_Length] = '\0';
                CliShowRunAgiValue (CliHandle, au1Agi);

                CliPrintf (CliHandle, "src-ac-id ");
                CliPrintf (CliHandle, "%u ", u4SrcAcId);
                CliPrintf (CliHandle, "dst-ac-id ");
                CliPrintf (CliHandle, "%u ", u4DestAcId);

                if (u4PwVcId != L2VPN_ZERO)
                {
                    CliPrintf (CliHandle, "pwid ");
                    CliPrintf (CliHandle, "%u ", u4PwVcId);
                }

                if ((u4LocalLabel != L2VPN_LDP_INVALID_LABEL) &&
                    (u4RemoteLabel != L2VPN_LDP_INVALID_LABEL))
                {
                    CliPrintf (CliHandle, "locallabel ");
                    CliPrintf (CliHandle, "%u ", u4LocalLabel);
                    CliPrintf (CliHandle, "remotelabel ");
                    CliPrintf (CliHandle, "%u ", u4RemoteLabel);
                }

            }
        }
        if (L2VPN_PWVC_CTRLW_PREFERRED == i4ControlWord)
        {
            CliPrintf (CliHandle, "control-word enable ");
        }

        if ((LocalCapabAdvert.pu1_OctetList[0] & L2VPN_PW_VCCV_CAPABLE) !=
            L2VPN_PW_VCCV_CAPABLE)
        {
            CliPrintf (CliHandle, "vccv disable ");
        }

        CliPrintf (CliHandle, "mplstype ");

        /* mplstype/Pwmode - te/non-te/vconly */
        if (L2VPN_MPLS_TYPE_TE == PwMplsMplsTypes.pu1_OctetList[0])
        {
            CliPrintf (CliHandle, "te ");

            CliPrintf (CliHandle, "%u %u ", u4TunnelId, u4InTunnelId);
        }
        else if (L2VPN_MPLS_TYPE_NONTE == PwMplsMplsTypes.pu1_OctetList[0])
        {
            CliPrintf (CliHandle, "non-te ");
        }
        else if (L2VPN_MPLS_TYPE_VCONLY == PwMplsMplsTypes.pu1_OctetList[0])
        {
            CliPrintf (CliHandle, "vconly ");
        }
        if (i4VlanMode != L2VPN_PWVC_ENET_DEF_VLAN_MODE)
        {
            CliPrintf (CliHandle, "vlanmode ");

            switch (i4VlanMode)
            {
                case L2VPN_VLANMODE_OTHER:
                    CliPrintf (CliHandle, "other");
                    break;
                case L2VPN_VLANMODE_PORTBASED:
                    CliPrintf (CliHandle, "portbased");
                    break;
                case L2VPN_VLANMODE_CHANGEVLAN:
                    CliPrintf (CliHandle, "changevlan %u", u4PwVlanId);
                    break;
                case L2VPN_VLANMODE_ADDVLAN:
                    CliPrintf (CliHandle, "addvlan %u", u4PwVlanId);
                    break;
                case L2VPN_VLANMODE_REMOVEVLAN:
                    CliPrintf (CliHandle, "removevlan %u", u4PwVlanId);
                    break;
                default:
                    break;
            }
        }
        if ((i4TestPwType == L2VPN_PWVC_TYPE_ETH_VLAN) && (i4EnetIfIndex !=
                                                           L2VPN_PWVC_DEF_PW_IF_IDX))
        {
            if (CFA_SUCCESS ==
                CfaCliConfGetIfName ((UINT4) i4EnetIfIndex, pi1IfName))
            {
                CliPrintf (CliHandle, " port-ifindex %s", pi1IfName);
            }
        }
        else if ((i4TestPwType == L2VPN_PWVC_TYPE_ETH) && (u4PortVlanId !=
                                                           L2VPN_PWVC_ENET_DEF_PORT_VLAN))
        {
            CliPrintf (CliHandle, " port-vlan vlan %u", u4PortVlanId);
        }

        if (i4AdminStatus == L2VPN_PWVC_DEF_ADMIN_STATUS)
        {
            CliPrintf (CliHandle, " inactive");
        }
        if (u4Mtu != CFA_ENET_MTU)
        {
            CliPrintf (CliHandle, "mtu %u", u4Mtu);
        }

        if (nmhGetFirstIndexFsL2VpnPwRedPwTable (&u4RedGroupIndex,
                                                 &u4RedGroupPwIndex)
            != SNMP_FAILURE)
        {
            do
            {
                if (u4RedGroupPwIndex == u4PwIndex)
                {
                    u4Flag = TRUE;
                    break;
                }

            }
            while (nmhGetNextIndexFsL2VpnPwRedPwTable (u4RedGroupIndex,
                                                       &u4RedGroupIndex,
                                                       u4RedGroupPwIndex,
                                                       &u4RedGroupPwIndex) ==
                   SNMP_SUCCESS);

            if (u4Flag == TRUE)
            {
                CliPrintf (CliHandle, " pseudowire-redundancy class-id %u\r\n",
                           u4RedGroupIndex);

                if (nmhGetFsL2VpnPwRedPwPreferance (u4RedGroupIndex,
                                                    u4RedGroupPwIndex,
                                                    &i4Preference) ==
                    SNMP_SUCCESS)
                {
                    if (i4Preference != L2VPNRED_PW_PREFERENCE_SECONDARY)
                    {
                        CliPrintf (CliHandle, " pwid %u deactivate\r\n",
                                   u4PwVcId);
                        CliPrintf (CliHandle, " pwid %u preference primary\r\n",
                                   u4PwVcId);
                        CliPrintf (CliHandle, " pwid %u activate\r\n",
                                   u4PwVcId);
                    }
                }
                if (nmhGetPwHoldingPriority (u4PwIndex, &i4HoldingPriority) ==
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, " pwid %u deactivate\r\n", u4PwVcId);
                    CliPrintf (CliHandle, " pwid %u holdingPriority %d \r\n",
                               u4PwVcId, i4HoldingPriority);
                    CliPrintf (CliHandle, " pwid %u activate\r\n", u4PwVcId);

                }

            }
            u4Flag = FALSE;
        }

        CliPrintf (CliHandle, "\r\n");
        if ((nmhGetPwIfIndex (u4PwIndex, (INT4 *) &u4CfaIfIndex) ==
             SNMP_SUCCESS) && (u4CfaIfIndex != L2VPN_ZERO))
        {
            if (CfaCliGetIfName (u4CfaIfIndex, ai1IfAliasName) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, " map pwid %d pw %u \r\n", u4PwVcId,
                           CLI_ATOI (ai1IfAliasName + 2));
            }
        }

        MPLSShowRunningConfigPwOamDisplay (CliHandle, u4PwVcId,
                                           LocalCcType.pu1_OctetList[0],
                                           LocalCvType.pu1_OctetList[0],
                                           RemoteCcType.pu1_OctetList[0],
                                           RemoteCvType.pu1_OctetList[0],
                                           i4ControlWord, bIsStaticPw);
        if (u4VplsIndex != L2VPN_ZERO && bRdflag == FALSE)
        {
            break;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  
 *  Function Name : MplsPwXConnectShowRunningConfig 
 *
 *  Input         : tCliHandle  CliHandle
 *                  INT4        i4TestEnetIfIndex - Interface Index
 *                  INT4        u4TestPortVlanId  - PortVlan Id 
 *                  INT4        i4TestPwType      - PwType
 *                  UINT1       *pu1Display       - Pointer to a flag which 
 *                                                  indicates whether to display
 *                                                  the config or not
 *
 *  Output(s)     : UINT1       *pu1ShowConfig - Pointer to a flag which
 *                                                   is updated to OSIX_TRUE
 *                                                   if mpls related config are
 *                                                   present on the given 
 *                                                   interface
 *
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT4
MplsPwXConnectShowRunningConfig (tCliHandle CliHandle, INT4 i4TestEnetIfIndex,
                                 UINT4 u4TestPortVlanId, INT4 i4TestPwType,
                                 UINT1 *pu1Display, UINT1 *pu1ShowConfig)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4PwEnetPwVlan = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4VplsIndexPrev = 0;
    INT4                i4PwEnetVlanMode = 0;
    BOOL1               bLoop = FALSE;
    BOOL1               bFlag = FALSE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };

    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    while (1)
    {
        if (bFlag == TRUE)
        {
            CliPrintf (CliHandle, "\r\n!\n");
        }
        /* u4PwIndex = 0; */
        MPLS_L2VPN_LOCK ();
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)))
        {
            bFlag = FALSE;
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }
        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex)))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }

        bLoop = TRUE;
        u4VplsIndexPrev = u4VplsIndex;

        /* if PseudoWire mode is VPLS the display the details */
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

        if (pVplsEntry == NULL)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

#ifdef VPLSADS_WANTED
        if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
        {
            if (ACTIVE != L2VPN_VPLS_ROW_STATUS (pVplsEntry))
            {
                MPLS_L2VPN_UNLOCK ();
                continue;
            }

            if (*pu1Display == OSIX_FALSE)
            {
                *pu1ShowConfig = OSIX_TRUE;
                MPLS_L2VPN_UNLOCK ();
                return CLI_SUCCESS;
            }

            L2VpnCliShowRunningVplsAc (CliHandle, u4VplsIndex,
                                       i4TestEnetIfIndex, u4TestPortVlanId);
        }
#endif
        if ((pPwVplsNode = TMO_SLL_First (&pVplsEntry->PwList)) == NULL)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (L2VPN_VPLS != L2VPN_PWVC_MODE (pPwEntry))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (bFlag == TRUE)
        {
            bFlag = FALSE;
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

#ifdef VPLSADS_WANTED
        if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
#endif
        {
            if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
            {
                /* Checking whether any AC is connected to this Vlan */
                TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwEntry)),
                              pPwVcEnetEntry, tPwVcEnetEntry *)
                {
                    if (MPLS_PWVC_TYPE_ETH == i4TestPwType)
                    {
                        if ((i4TestEnetIfIndex !=
                             L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)) ||
                            ((pPwVcEnetEntry->u1EnetMode) !=
                             L2VPN_CLI_ETH_MODE))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        /* Ethernet Tagged case */
                        if ((u4TestPortVlanId !=
                             L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                            || ((pPwVcEnetEntry->u1EnetMode) !=
                                L2VPN_CLI_VLAN_MODE))
                        {
                            continue;
                        }
                    }
                    bFlag = TRUE;
                    /* get Vlan mode */
                    i4PwEnetVlanMode =
                        L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);
                    if ((L2VPN_VLANMODE_CHANGEVLAN == i4PwEnetVlanMode) ||
                        (L2VPN_VLANMODE_ADDVLAN == i4PwEnetVlanMode) ||
                        (L2VPN_VLANMODE_REMOVEVLAN == i4PwEnetVlanMode))
                    {
                        u4PwEnetPwVlan =
                            L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
                    }
                    /* get vpls name */
                    MEMSET (au1VplsName, L2VPN_ZERO, sizeof (au1VplsName));
                    if (nmhGetFsMplsVplsName (u4VplsIndex, &VplsName) ==
                        SNMP_FAILURE)
                    {
                        continue;
                    }

                    if (*pu1Display == OSIX_FALSE)
                    {
                        *pu1ShowConfig = OSIX_TRUE;
                        MPLS_L2VPN_UNLOCK ();
                        return CLI_SUCCESS;
                    }

                    CliPrintf (CliHandle, "\r\n xconnect vfi %s ",
                               VplsName.pu1_OctetList);

                    if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) !=
                        L2VPN_ZERO
                        && L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                        L2VPN_PWVC_ENET_DEF_PW_VLAN)
                    {

                        CliPrintf (CliHandle, "port-ifindex ");
                        CfaCliConfGetIfName ((UINT4)
                                             L2VPN_PWVC_ENET_PORT_IF_INDEX
                                             (pPwVcEnetEntry),
                                             (INT1 *) au1IfName);
                        CliPrintf (CliHandle, "%s ", au1IfName);
                    }

                    if (i4PwEnetVlanMode != L2VPN_PWVC_ENET_DEF_VLAN_MODE)
                    {
                        CliPrintf (CliHandle, "vlanmode ");

                        switch (i4PwEnetVlanMode)
                        {
                            case L2VPN_VLANMODE_OTHER:
                                CliPrintf (CliHandle, "other\r\n");
                                break;
                            case L2VPN_VLANMODE_PORTBASED:
                                CliPrintf (CliHandle, "portbased\r\n");
                                break;
                            case L2VPN_VLANMODE_CHANGEVLAN:
                                CliPrintf (CliHandle, "changevlan %u\r\n",
                                           u4PwEnetPwVlan);
                                break;
                            case L2VPN_VLANMODE_ADDVLAN:
                                CliPrintf (CliHandle, "addvlan %u\r\n",
                                           u4PwEnetPwVlan);
                                break;
                            case L2VPN_VLANMODE_REMOVEVLAN:
                                CliPrintf (CliHandle, "removevlan %u\r\n",
                                           u4PwEnetPwVlan);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        MPLS_L2VPN_UNLOCK ();
    }                            /* While Ends */
    return CLI_SUCCESS;

}

/*******************************************************************************
 *  Function Name : MplsPwGlobalShowRunningConfig
 *  Input         : tCliHandle CliHadle
 *  Description   : This function shows the Mpls Pw global configurations.
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
VOID
MplsPwGlobalShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4PwNotifEnable = 0;

    if (nmhGetFsMplsPwStatusNotifEnable (&i4PwNotifEnable) == SNMP_SUCCESS)
    {
        if (i4PwNotifEnable == L2VPN_TRUE)
        {
            CliPrintf (CliHandle, "\rpseudowire-notification pw-status \r\n");
        }
    }

    /* By default all the cc bits is set */
    if (gL2VpnGlobalInfo.u1LocalCcTypeCapabilities != L2VPN_VCCV_CC_ALL)
    {
        CliPrintf (CliHandle, "\rpw-cc-capability");

        if (gL2VpnGlobalInfo.u1LocalCcTypeCapabilities & L2VPN_VCCV_CC_ACH)
        {
            CliPrintf (CliHandle, " pw-ach");
        }

        if (gL2VpnGlobalInfo.u1LocalCcTypeCapabilities & L2VPN_VCCV_CC_RAL)
        {
            CliPrintf (CliHandle, " router-alert-label");
        }

        if (gL2VpnGlobalInfo.u1LocalCcTypeCapabilities & L2VPN_VCCV_CC_TTL_EXP)
        {
            CliPrintf (CliHandle, " ttl-expiry \n");
        }

        CliPrintf (CliHandle, "\r\n");
    }

    /* be default LSP Ping is enable */
    if (gL2VpnGlobalInfo.u1LocalCvTypeCapabilities != L2VPN_VCCV_CV_LSPP)
    {
        CliPrintf (CliHandle, "\rpw-cv-capability");

        if (gL2VpnGlobalInfo.
            u1LocalCvTypeCapabilities & L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY)
        {
            CliPrintf (CliHandle, " bfd-ip-encap-fault");
        }

        if (gL2VpnGlobalInfo.
            u1LocalCvTypeCapabilities & L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS)
        {
            CliPrintf (CliHandle, " bfd-ip-encap-fault-status-notify");
        }

        if (gL2VpnGlobalInfo.
            u1LocalCvTypeCapabilities & L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY)
        {
            CliPrintf (CliHandle, " bfd-ach-encap-fault");
        }

        if (gL2VpnGlobalInfo.
            u1LocalCvTypeCapabilities & L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS)
        {
            CliPrintf (CliHandle, " bfd-ach-encap-fault-status-notify");
        }

        CliPrintf (CliHandle, "\r\n");
    }
}

/*******************************************************************************
 *  Function Name : MplsPwEnetGet
 *  Input         : tCliHandle CliHadle                                
 *                  UINT4    u4TestPwIndex      - Pw Index
 *                  INT4     *pi4RetEnetIfIndex - Enet Interface Index
 *                  INT4     *pi4RetVlanMode    - Vlan Mode 
 *                  UINT4    *pu4RetPwVlanId    - Pw Vlan Id
 *                  UINT4    *pu4RetPortVlanId  - Port Vlan Id
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
MplsPwEnetGet (UINT4 u4TestPwIndex,
               INT4 *pi4RetEnetIfIndex,
               INT4 *pi4RetVlanMode,
               UINT4 *pu4RetPwVlanId, UINT4 *pu4RetPortVlanId)
{
    UINT4               u4PwEnetPwInstance = 0;
    UINT4               u4PwEnetPwInstancePrev = 0;
    UINT4               u4PwIndexPrev = 0;
    UINT4               u4PwIndex = 0;
    BOOL1               bLoop = FALSE;

    /* u4PwIndex = 0; */
    while (u4TestPwIndex != u4PwIndex)
    {
        MPLS_L2VPN_LOCK ();
        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexPwEnetTable (&u4PwIndex, &u4PwEnetPwInstance)))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }
        /* get next entry */
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexPwEnetTable (u4PwIndexPrev, &u4PwIndex,
                                         u4PwEnetPwInstancePrev,
                                         &u4PwEnetPwInstance)))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }

        MPLS_L2VPN_UNLOCK ();
        bLoop = TRUE;
        u4PwIndexPrev = u4PwIndex;
        u4PwEnetPwInstancePrev = u4PwEnetPwInstance;
    }

    MPLS_L2VPN_LOCK ();
    /* get Vlan mode */
    if (SNMP_FAILURE == nmhGetPwEnetVlanMode (u4PwIndex, u4PwEnetPwInstance,
                                              pi4RetVlanMode))
    {
        MPLS_L2VPN_UNLOCK ();
        return CLI_SUCCESS;
    }
    /* get IfIndex */
    if (SNMP_FAILURE == nmhGetPwEnetPortIfIndex (u4PwIndex, u4PwEnetPwInstance,
                                                 pi4RetEnetIfIndex))
    {
        MPLS_L2VPN_UNLOCK ();
        return CLI_SUCCESS;
    }

    if ((L2VPN_VLANMODE_ADDVLAN == *pi4RetVlanMode) ||
        (L2VPN_VLANMODE_CHANGEVLAN == *pi4RetVlanMode) ||
        (L2VPN_VLANMODE_REMOVEVLAN == *pi4RetVlanMode))
    {
        /* get PwVlan id */
        if (SNMP_FAILURE == nmhGetPwEnetPwVlan (u4PwIndex, u4PwEnetPwInstance,
                                                pu4RetPwVlanId))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }
    }
    /* get PortVlan id */
    if (SNMP_FAILURE == nmhGetPwEnetPortVlan (u4PwIndex, u4PwEnetPwInstance,
                                              pu4RetPortVlanId))
    {
        MPLS_L2VPN_UNLOCK ();
        return CLI_SUCCESS;
    }

    MPLS_L2VPN_UNLOCK ();
    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : L2VpnCliSetPwMplsNonTeEntityId
 *  Input         : tCliHandle CliHadle                                
 *                  u4PwIndex   - PwIndex
 *                  u4EntityId - Entity Id
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliSetPwMplsNonTeEntityId (UINT4 u4PwIndex, UINT4 u4EntityId)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));

            pPwVcMplsTnlEntry->unTnlInfo.NonTeInfo.u4EntityId = u4EntityId;
            return CLI_SUCCESS;
        }
    }
    return CLI_FAILURE;
}

/*******************************************************************************
 *  Function Name : L2VpnPwVcShowEnetInfo
 *  Input         : tCliHandle CliHadle
 *                  pCliShowPw: Pointer to CLI show strcuture
 *                  b1TileWanted: Pointer to the flag that holds the value
 *                  Detailed/Summary display
 *  Output(s)     : None.
 *  Return Values : NONE
 *******************************************************************************/
INT4
L2VpnPwVcShowEnetInfo (tCliHandle CliHandle, tCliShowPw * pCliShowPw,
                       BOOL1 * pb1TitleWanted)
{
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT1               u1EnetCnt = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, 0, sizeof (tGenU4Addr));

    if ((pCliShowPw->u4DispStatus == CLI_MPLS_L2VPN_SUMMARY) ||
        (pCliShowPw->u4DispStatus == CLI_MPLS_L2VPN_PEER_SUMMARY))
    {
        if (*pb1TitleWanted)
        {
            CliPrintf (CliHandle, "\r\n%-11s %-15s %-18s %-19s %-10s \r\n",
                       "Local intf", "Local circuit",
                       "Dest address", "PW Status", "VC ID");
            CliPrintf (CliHandle, "%-11s %-15s %-18s %-19s %-10s \r\n",
                       "----------", "-------------",
                       "-------------", "---------------", "-----------");

            *pb1TitleWanted = FALSE;
        }
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pCliShowPw->pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                pCliShowPw->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pCliShowPw->pPwVcEntry->i4PeerAddrType)
#endif
    {

        GenU4Addr.Addr.u4Addr = pCliShowPw->PeerAddr.u4Addr;
    }
    /* MS-PW: Get No of AC associated */
    if (L2VPN_PWVC_ENET_ENTRY (pCliShowPw->pPwVcEntry) != NULL)
    {
        u1EnetCnt = (UINT1) TMO_SLL_Count (L2VPN_PWVC_ENET_ENTRY_LIST
                                           ((tPwVcEnetServSpecEntry *)
                                            L2VPN_PWVC_ENET_ENTRY
                                            (pCliShowPw->pPwVcEntry)));
    }                            /* MS-PW */
    if (u1EnetCnt == L2VPN_ZERO)
    {
        CliShowPwDetail (CliHandle, pCliShowPw->u4PwIndex, L2VPN_ZERO,
                         GenU4Addr, pCliShowPw->u4DispStatus);
    }
    else
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pCliShowPw->pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            CliShowPwDetail (CliHandle, pCliShowPw->u4PwIndex,
                             L2VPN_PWVC_ENET_PW_INSTANCE
                             (pPwVcEnetEntry), GenU4Addr,
                             pCliShowPw->u4DispStatus);
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : PwUpdtOamParams
 * Description   : This routine will populate the entries in pwFsMplsL2vpnTable
 * Input(s)      : CliHandle             - Context in which the command is
 *                                         processed
 *                 pL2vpnCliArgs         - Contains parameters like local CC,CV
 *                                         remote CC,CV,etc., to set
 *                                         for the PW.
 *                 pPwVcEntry            - Pointer to PW entry                                          
 * Output(s)     : If Success -CLI_SUCCESS
 *                 or else Proper Error Mesage is printed.
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *******************************************************************************/
INT4
PwUpdtOamParams (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs,
                 tPwVcEntry * pPwVcEntry)
{
    tSNMP_OCTET_STRING_TYPE LocalCCAdvert;
    tSNMP_OCTET_STRING_TYPE LocalCVAdvert;
    tSNMP_OCTET_STRING_TYPE RemoteCCAdvert;
    tSNMP_OCTET_STRING_TYPE RemoteCVAdvert;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4LclAIIType = 0;
    UINT4               u4RemAIIType = 0;
    INT4                i4PwOwner = 0;
    UINT1               u1LocalCCAdvert = 0;
    UINT1               u1LocalCVAdvert = 0;
    UINT1               u1RemoteCCAdvert = 0;
    UINT1               u1RemoteCVAdvert = 0;

    u4PwIndex = pPwVcEntry->u4PwVcIndex;

    if (nmhTestv2PwRowStatus
        (&u4ErrorCode, u4PwIndex, MPLS_STATUS_NOT_INSERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% PW bring down validations failed.\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwRowStatus (u4PwIndex, MPLS_STATUS_NOT_INSERVICE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to bring down the PW.\n");
        return CLI_FAILURE;
    }
    if (pL2vpnCliArgs->u4LocalCCAdvert != 0)
    {
        LocalCCAdvert.pu1_OctetList = &u1LocalCCAdvert;
        LocalCCAdvert.pu1_OctetList[0] =
            (UINT1) (pL2vpnCliArgs->u4LocalCCAdvert);
        LocalCCAdvert.i4_Length = sizeof (UINT1);

        if (nmhTestv2FsMplsL2VpnPwLocalCCAdvert
            (&u4ErrorCode, u4PwIndex, &LocalCCAdvert) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Test PwLocalCCAdvert Failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsL2VpnPwLocalCCAdvert (u4PwIndex,
                                              &LocalCCAdvert) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwLocalCCAdvert\r\n");
            return CLI_FAILURE;
        }
    }
    if (pL2vpnCliArgs->u4LocalCVAdvert != 0)
    {
        LocalCVAdvert.pu1_OctetList = &u1LocalCVAdvert;
        LocalCVAdvert.pu1_OctetList[0] =
            (UINT1) (pL2vpnCliArgs->u4LocalCVAdvert);
        LocalCVAdvert.i4_Length = sizeof (UINT1);

        if (nmhTestv2FsMplsL2VpnPwLocalCVAdvert
            (&u4ErrorCode, u4PwIndex, &LocalCVAdvert) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Test PwLocalCVAdvert Failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsL2VpnPwLocalCVAdvert (u4PwIndex,
                                              &LocalCVAdvert) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwLocalCVAdvert\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhGetPwOwner (u4PwIndex, &i4PwOwner) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to get PwOwner\r\n");
        return CLI_FAILURE;
    }
    if (nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex, &u4LclAIIType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to get PwGenLocalAIIType\r\n");
        return CLI_FAILURE;
    }
    if (nmhGetFsMplsL2VpnPwGenRemoteAIIType (u4PwIndex, &u4RemAIIType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to get PwGenRemoteAIIType\r\n");
        return CLI_FAILURE;
    }

    if ((i4PwOwner == L2VPN_PWVC_OWNER_MANUAL) ||
        ((u4LclAIIType == L2VPN_GEN_FEC_AII_TYPE_2)
         && (u4RemAIIType == L2VPN_GEN_FEC_AII_TYPE_2)))
    {
        if (pL2vpnCliArgs->u4RemoteCCAdvert != 0)
        {
            /* Remote CC, CV configurations are allowed only for GenFEC - AII Type 2 */
            RemoteCCAdvert.pu1_OctetList = &u1RemoteCCAdvert;
            RemoteCCAdvert.pu1_OctetList[0] =
                (UINT1) (pL2vpnCliArgs->u4RemoteCCAdvert);
            RemoteCCAdvert.i4_Length = sizeof (UINT1);

            if (nmhTestv2FsMplsL2VpnPwRemoteCCAdvert
                (&u4ErrorCode, u4PwIndex, &RemoteCCAdvert) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Test PwRemoteCCAdvert Failed\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsL2VpnPwRemoteCCAdvert (u4PwIndex,
                                                   &RemoteCCAdvert) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Unable to set PwRemoteCCAdvert\r\n");
                return CLI_FAILURE;
            }
        }
        if (pL2vpnCliArgs->u4RemoteCVAdvert != 0)
        {
            RemoteCVAdvert.pu1_OctetList = &u1RemoteCVAdvert;
            RemoteCVAdvert.pu1_OctetList[0] =
                (UINT1) (pL2vpnCliArgs->u4RemoteCVAdvert);
            RemoteCVAdvert.i4_Length = sizeof (UINT1);

            if (nmhTestv2FsMplsL2VpnPwRemoteCVAdvert
                (&u4ErrorCode, u4PwIndex, &RemoteCVAdvert) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Test PwRemoteCVAdvert Failed\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsL2VpnPwRemoteCVAdvert (u4PwIndex,
                                                   &RemoteCVAdvert) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Unable to set PwRemoteCVAdvert\r\n");
                return CLI_FAILURE;
            }
        }
    }

    if (nmhTestv2PwRowStatus
        (&u4ErrorCode, u4PwIndex, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% PW validation for PW activation failed \n");
        return CLI_FAILURE;
    }
    if (nmhSetPwRowStatus (u4PwIndex, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to activate the PW\n");
        return CLI_FAILURE;
    }

    if (pL2vpnCliArgs->i4OamStatus != 0)
    {
        if (nmhTestv2FsMplsL2VpnPwOamEnable
            (&u4ErrorCode, u4PwIndex,
             pL2vpnCliArgs->i4OamStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Prerequisite for PwOamEnable does not exist\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsL2VpnPwOamEnable (u4PwIndex,
                                          pL2vpnCliArgs->i4OamStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set PwOamEnable\r\n");
            return CLI_FAILURE;
        }
    }

    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
    {
        return CLI_SUCCESS;
    }
    if (nmhTestv2PwAdminStatus
        (&u4ErrorCode, u4PwIndex, L2VPN_PWVC_ADMIN_UP) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% PW bring up validations failed\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwAdminStatus (u4PwIndex, L2VPN_PWVC_ADMIN_UP) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to make the PW admin UP\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnCliVcNotifConfigure
 * Description   : This routine configures the trap/notification for the 
                   pseudowire and OAM for the pseudowire
 * Input(s)      : CliHandle - CLI handle
 *                 i4PwStatusNotif - PW status notification
 *                 i4PwOamStatusNotif - PW OAM status notification
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliVcNotifConfigure (tCliHandle CliHandle, INT4 i4PwStatusNotif)
{
    UINT4               u4ErrorCode = L2VPN_ZERO;

    if (i4PwStatusNotif != 0)
    {
        if (nmhTestv2FsMplsPwStatusNotifEnable
            (&u4ErrorCode, i4PwStatusNotif) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Test PwStatusNotif Failed\r\n");
            return CLI_FAILURE;

        }

        if (nmhSetFsMplsPwStatusNotifEnable (i4PwStatusNotif) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Set PwStatusNotif Failed\r\n");
            return CLI_FAILURE;
        }
    }

    L2VPN_DBG (L2VPN_DBG_VCCV_CR_TRC, "L2VpnCliVcNotifConfigure:"
               "Notification is Set for Pw\r\n");
    return CLI_SUCCESS;

}

/******************************************************************************
 * Function Name : L2VpnCliShowVccvCapabs
 * Description   : This routine displays VCCV capabilities configured for the
                   pseudowire
 * Input(s)      : CliHandle             - Context in which the command is
 *                                         processed
 * Output(s)     : None
 * Return(s)     : TRUE or FALSE.
 *******************************************************************************/

INT4
L2VpnCliShowVccvCapabs (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE LocalCcAdvert;
    tSNMP_OCTET_STRING_TYPE LocalCvAdvert;
    tSNMP_OCTET_STRING_TYPE RemoteCcAdvert;
    tSNMP_OCTET_STRING_TYPE RemoteCvAdvert;
    tSNMP_OCTET_STRING_TYPE LocalCcSelected;
    tSNMP_OCTET_STRING_TYPE LocalCvSelected;
    UINT4               u4PwIndex = 0;
    UINT4               u4PwId = 0;
    UINT4               u4LocalAiiType = 0;
    UINT4               u4RemoteAiiType = 0;
    UINT4               u4PwOutboundLabel = 0;
    UINT4               u4PwInboundLabel = 0;
    INT4                i4CwPreference = 0;
    INT4                i4CwStatus = 0;
    INT4                i4PwOwner = 0;
    INT4                i4PwType = 0;
    UINT1               u1LocalCCAdvert = 0;
    UINT1               u1LocalCVAdvert = 0;
    UINT1               u1RemCCAdvert = 0;
    UINT1               u1RemCVAdvert = 0;
    UINT1               u1LocalCCSelected = 0;
    UINT1               u1LocalCVSelected = 0;
    CHR1               *pu1String = NULL;
    static UINT1        au1PeerAddr[IPV6_ADDR_LENGTH];
    uGenU4Addr          GenU4Addr;

#ifdef MPLS_IPV6_WANTED
    INT4                i4RetValPwPeerAddrType = 0;
    tUtlIn6Addr         Ipv6Addr;
#endif

    MEMSET (&GenU4Addr, 0, sizeof (uGenU4Addr));
    MEMSET (&LocalCcAdvert, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LocalCvAdvert, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RemoteCcAdvert, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RemoteCvAdvert, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LocalCcSelected, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LocalCvSelected, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PeerAddr.pu1_OctetList = au1PeerAddr;

    LocalCcAdvert.pu1_OctetList = &u1LocalCCAdvert;
    LocalCvAdvert.pu1_OctetList = &u1LocalCVAdvert;
    RemoteCcAdvert.pu1_OctetList = &u1RemCCAdvert;
    RemoteCvAdvert.pu1_OctetList = &u1RemCVAdvert;
    LocalCcSelected.pu1_OctetList = &u1LocalCCSelected;
    LocalCvSelected.pu1_OctetList = &u1LocalCVSelected;

    while (1)
    {
        /* Get the next entry in the PwTable */
        if ((u4PwIndex != 0) &&
            ((nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex)) == SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4PwIndex == 0) &&
            ((nmhGetFirstIndexPwTable (&u4PwIndex)) == SNMP_FAILURE))
        {
            return CLI_SUCCESS;
        }

        if (nmhGetPwID (u4PwIndex, &u4PwId) == SNMP_FAILURE)
        {
            continue;
        }

        /* Get the Pseudo wire Owner Type */
        if (nmhGetPwOwner (u4PwIndex, &i4PwOwner) == SNMP_FAILURE)
        {
            continue;
        }

        /*Get the Pseudowire Type */
        if (nmhGetPwType (u4PwIndex, &i4PwType) == SNMP_FAILURE)
        {
            continue;
        }

        /*Get Local AII Type and Remote AII Type */
        if (nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwIndex, &u4LocalAiiType)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetFsMplsL2VpnPwGenRemoteAIIType (u4PwIndex, &u4RemoteAiiType)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetPwPeerAddr (u4PwIndex, &PeerAddr) == SNMP_FAILURE)
        {
            continue;
        }

#ifdef MPLS_IPV6_WANTED
        if (nmhGetPwPeerAddrType (u4PwIndex, &i4RetValPwPeerAddrType) ==
            SNMP_FAILURE)
        {
            continue;
        }
        if (MPLS_IPV6_ADDR_TYPE == i4RetValPwPeerAddrType)
        {
            MEMCPY (Ipv6Addr.u1addr, PeerAddr.pu1_OctetList, IPV6_ADDR_LENGTH);
            PeerAddr.i4_Length = IPV6_ADDR_LENGTH;
            pu1String = INET_NTOA6 (Ipv6Addr);
        }

        else if (MPLS_IPV4_ADDR_TYPE == i4RetValPwPeerAddrType)
#endif
        {
            MEMCPY (&GenU4Addr.u4Addr, PeerAddr.pu1_OctetList,
                    IPV4_ADDR_LENGTH);
            GenU4Addr.u4Addr = OSIX_HTONL (GenU4Addr.u4Addr);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, GenU4Addr.u4Addr);
        }
        if (nmhGetPwCwPreference (u4PwIndex, &i4CwPreference) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetPwCwStatus (u4PwIndex, &i4CwStatus) == SNMP_FAILURE)
        {
            continue;
        }

        /* Get Inbound and Outbound Label */
        if (nmhGetPwInboundLabel (u4PwIndex, &u4PwInboundLabel) == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetPwOutboundLabel (u4PwIndex, &u4PwOutboundLabel) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsL2VpnPwLocalCCAdvert (u4PwIndex, &LocalCcAdvert) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsL2VpnPwLocalCVAdvert (u4PwIndex, &LocalCvAdvert) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsL2VpnPwRemoteCCAdvert (u4PwIndex, &RemoteCcAdvert) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsL2VpnPwRemoteCVAdvert (u4PwIndex, &RemoteCvAdvert) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsL2VpnPwLocalCCSelected (u4PwIndex, &LocalCcSelected) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsL2VpnPwLocalCVSelected (u4PwIndex, &LocalCvSelected) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if ((i4PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (u4LocalAiiType == L2VPN_GEN_FEC_AII_TYPE_2) &&
            (u4RemoteAiiType == L2VPN_GEN_FEC_AII_TYPE_2))
        {
            CliPrintf (CliHandle,
                       "Destination Address : %s   VC ID: %u\r", "-", u4PwId);
        }
        else
        {
            CliPrintf (CliHandle,
                       "Destination Address : %s   VC ID: %u\r",
                       pu1String, u4PwId);
        }

        if (u4PwInboundLabel == MPLS_INVALID_LABEL)
        {
            CliPrintf (CliHandle, "\n   Local Label : - \r");
        }
        else
        {
            CliPrintf (CliHandle, "\n   Local Label : %u \r", u4PwInboundLabel);
        }
        if (i4PwType == L2VPN_PWVC_TYPE_ETH)
        {
            if (i4CwStatus == L2VPN_PWVC_CWPRESENT)
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 1   VC Type : Ethernet\n\r");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 0   VC Type : Ethernet\n\r");
            }

        }
        else if (i4PwType == L2VPN_PWVC_TYPE_ETH_VLAN)
        {
            if (i4CwStatus == L2VPN_PWVC_CWPRESENT)
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 1  VC Type : EthernetVlan\n\r");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 0  VC Type : EthernetVlan\n\r");
            }

        }
        CliPrintf (CliHandle, "    VCCV:");
        CliPrintf (CliHandle, "\n\r     CC Type Advertised : ");

        L2VpnCliShowCcDetail (CliHandle, LocalCcAdvert.pu1_OctetList[0]);

        if (i4CwPreference == MPLS_SNMP_FALSE)
        {
            i4CwPreference = L2VPN_ZERO;
        }

        CliPrintf (CliHandle, "     CV Type Advertised : ");
        L2VpnCliShowCvDetail (CliHandle, LocalCvAdvert.pu1_OctetList[0]);

        CliPrintf (CliHandle, "     CC Type Selected   : ");
        L2VpnCliShowCcDetail (CliHandle, LocalCcSelected.pu1_OctetList[0]);

        CliPrintf (CliHandle, "     CV Type Selected   : ");
        L2VpnCliShowCvDetail (CliHandle, LocalCvSelected.pu1_OctetList[0]);

        if (u4PwOutboundLabel == MPLS_INVALID_LABEL)
        {
            CliPrintf (CliHandle, "\n   Remote Label : - \r");
        }
        else
        {
            CliPrintf (CliHandle, "\n   Remote Label : %u \r",
                       u4PwOutboundLabel);
        }
        if (i4PwType == L2VPN_PWVC_TYPE_ETH)
        {
            if (i4CwStatus == L2VPN_PWVC_CWPRESENT)
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 1   VC Type : Ethernet\n\r");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 0   VC Type : Ethernet\n\r");
            }
        }
        else if (i4PwType == L2VPN_PWVC_TYPE_ETH_VLAN)
        {
            if (i4CwStatus == L2VPN_PWVC_CWPRESENT)
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 1  VC Type : EthernetVlan\n\r");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\n\r    Cbit : 0  VC Type : EthernetVlan\n\r");
            }
        }
        CliPrintf (CliHandle, "    VCCV:");
        CliPrintf (CliHandle, "\n\r     CC Type Advertised : ");
        L2VpnCliShowCcDetail (CliHandle, RemoteCcAdvert.pu1_OctetList[0]);
        CliPrintf (CliHandle, "     CV Type Advertised : ");
        L2VpnCliShowCvDetail (CliHandle, RemoteCvAdvert.pu1_OctetList[0]);
    }

    return L2VPN_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnCliShowHwCapabs
 * Description   : This routine displays global control channel capabilities
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : TRUE or FALSE.
 *******************************************************************************/

INT4
L2VpnCliShowHwCapabs (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE HwCcType;
    UINT1               u1HwCcType = L2VPN_ZERO;

    HwCcType.pu1_OctetList = &u1HwCcType;
    if (nmhGetFsMplsHwCCTypeCapabilities (&HwCcType) == SNMP_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    if (u1HwCcType & L2VPN_VCCV_CC_ACH)
    {
        CliPrintf (CliHandle, "\n\rControl Word Processing supported \n\r");
        CliPrintf (CliHandle,
                   "\n\rSequence number processing not supported \n\r");
    }
    else
    {
        CliPrintf (CliHandle, "\n\rControl Word Processing not supported \n\r");
        CliPrintf (CliHandle,
                   "\n\rSequence number processing not supported \n\r");
    }
    if (u1HwCcType & L2VPN_VCCV_CC_RAL)
    {
        CliPrintf (CliHandle,
                   "\n\rRouter Alert Label Processing supported \n\r");
    }
    if (u1HwCcType & L2VPN_VCCV_CC_TTL_EXP)
    {
        CliPrintf (CliHandle,
                   "\n\rPW Label with TTL = 1 Processing supported \n\r");
    }
    return L2VPN_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnCliShowGlobalInfo
 * Description   : This routine displays L2Vpn Global Information
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : L2VPN_SUCCESS
 *******************************************************************************/
INT4
L2VpnCliShowGlobalInfo (tCliHandle CliHandle)
{
    if (gL2VpnGlobalInfo.b1PwStatusNotif == MPLS_SNMP_TRUE)
    {

        CliPrintf (CliHandle, "\n\rPseudowire Status Notification"
                   "       : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\n\rPseudowire Status Notification"
                   "       : Disabled");
    }
    if (gL2VpnGlobalInfo.b1PwOamStatusNotif == MPLS_SNMP_TRUE)
    {

        CliPrintf (CliHandle, "\n\rPseudowire Oam Status Notification"
                   "   : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\n\rPseudowire Oam Status Notification"
                   "   : Disabled");
    }
    CliPrintf (CliHandle, "\n\rGlobal Control Channel Type          :");
    L2VpnCliShowCcDetail (CliHandle,
                          gL2VpnGlobalInfo.u1LocalCcTypeCapabilities);
    CliPrintf (CliHandle, "Global Connectivity Verification Type:");
    L2VpnCliShowCvDetail (CliHandle,
                          gL2VpnGlobalInfo.u1LocalCvTypeCapabilities);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnCliGetAiiType2Identifiers                            */
/* Description   : This routine combines given Global Id, Node Id & AC Id    */
/*                 in the given pointer                                      */
/* Input(s)      : u4GlobalId - Global Identifier                            */
/*                 u4NodeId - Node Identifier                                */
/*                 u4AcId - Attachment Circuit Identifier                    */
/* Output(s)     : pAii - Combined string of Global Id, Node Id & AC Id       */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnCliGetAiiType2Identifiers (UINT4 u4GlobalId, UINT4 u4NodeId,
                                UINT4 u4AcId, tSNMP_OCTET_STRING_TYPE * pAii)
{
    UINT4               u4AiiStrLen = L2VPN_ZERO;

    u4GlobalId = OSIX_HTONL (u4GlobalId);
    MEMCPY (pAii->pu1_OctetList, &u4GlobalId, sizeof (UINT4));
    u4AiiStrLen = sizeof (UINT4);

    u4NodeId = OSIX_HTONL (u4NodeId);
    MEMCPY ((pAii->pu1_OctetList + L2VPN_GEN_FEC_NODE_ID_OFFSET), &u4NodeId,
            sizeof (UINT4));
    u4AiiStrLen += sizeof (UINT4);

    u4AcId = OSIX_HTONL (u4AcId);
    MEMCPY ((pAii->pu1_OctetList + L2VPN_GEN_FEC_AC_ID_OFFSET), &u4AcId,
            sizeof (UINT4));
    u4AiiStrLen += sizeof (UINT4);

    pAii->i4_Length = (INT4) u4AiiStrLen;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : L2VpnCliEnableDebug
 *
 *     DESCRIPTION      : This function will set the debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - L2vpn Debug flag
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
PUBLIC INT4
L2VpnCliEnableDebug (tCliHandle CliHandle, INT4 i4Val)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4Level = 0;

    if (nmhGetFsMplsL2VpnTrcFlag (&i4Level) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to get MplsL2VpnTrcFlag\r\n");
        return CLI_FAILURE;
    }
    i4Val = i4Val | i4Level;

    if (nmhTestv2FsMplsL2VpnTrcFlag (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enable debug option\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsL2VpnTrcFlag (i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set MplsL2VpnTrcFlag\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 *
 *     FUNCTION NAME    : L2VpnCliDisableDebug
 *
 *     DESCRIPTION      : This function will reset the debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - L2Vpn Debug Level
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
PUBLIC INT4
L2VpnCliDisableDebug (tCliHandle CliHandle, INT4 i4Val)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4Level = 0;

    if (nmhGetFsMplsL2VpnTrcFlag (&i4Level) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to get MplsL2VpnTrcFlag\r\n");
        return CLI_FAILURE;
    }
    i4Val = i4Val & i4Level;

    if (nmhTestv2FsMplsL2VpnTrcFlag (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to disable debug option\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsL2VpnTrcFlag (i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set MplsL2VpnTrcFlag\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnCliExtractAiiType2Identifiers                        */
/* Description   : This routine convert the SAII into source Global Id,      */
/*                 source Node Id and source AC Id and TAII into destination */
/*                 global Id, Destination node id and destination AC id      */
/* Input(s)      : pu1Aii - Pointer to SAII or TAII                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnExtractGenFecType2Identifiers (UINT1 *pu1Aii, UINT4 *pu4GlobalId,
                                    UINT4 *pu4NodeId, UINT4 *pu4AcId)
{
    MEMCPY (pu4GlobalId, pu1Aii + L2VPN_GEN_FEC_GLOBAL_ID_OFFSET,
            sizeof (UINT4));
    *pu4GlobalId = OSIX_NTOHL (*pu4GlobalId);
    MEMCPY (pu4NodeId, pu1Aii + L2VPN_GEN_FEC_NODE_ID_OFFSET, sizeof (UINT4));
    *pu4NodeId = OSIX_NTOHL (*pu4NodeId);
    MEMCPY (pu4AcId, pu1Aii + L2VPN_GEN_FEC_AC_ID_OFFSET, sizeof (UINT4));
    *pu4AcId = OSIX_NTOHL (*pu4AcId);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnCliShowCcDetail                                      */
/* Description   : This routine displays the Control channel Type in string  */
/*                 format                                                    */
/* Input(s)      : tCliHandle - Context in which the command is processed    */
/*                 u1CcAdvert - value of Control channel type                */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnCliShowCcDetail (tCliHandle CliHandle, UINT1 u1CcAdvert)
{
    if (u1CcAdvert & L2VPN_VCCV_CC_ACH)
    {
        CliPrintf (CliHandle, " PW-ACH[1] ");
    }
    if (u1CcAdvert & L2VPN_VCCV_CC_RAL)
    {
        CliPrintf (CliHandle, " RAL[2] ");
    }
    if (u1CcAdvert & L2VPN_VCCV_CC_TTL_EXP)
    {
        CliPrintf (CliHandle, " TTL[3] ");
    }
    if (u1CcAdvert == L2VPN_VCCV_CC_NONE)
    {
        CliPrintf (CliHandle, " None ");
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************/
/* Function Name : L2VpnCliShowCvDetail                                      */
/* Description   : This routine displays the Connectivity verification Type  */
/*                 in string format                                          */
/* Input(s)      : tCliHandle - Context in which the command is processed    */
/*                 u1CvAdvert - value of Connectivity verification type      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnCliShowCvDetail (tCliHandle CliHandle, UINT1 u1CvAdvert)
{
    if (u1CvAdvert & L2VPN_VCCV_CV_LSPP)
    {
        CliPrintf (CliHandle, " LSPV ");
    }
    if (u1CvAdvert & L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY)
    {
        CliPrintf (CliHandle, " BFD CV [1] ");
    }
    if (u1CvAdvert & L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS)
    {
        CliPrintf (CliHandle, " BFD CV [2] ");
    }
    if (u1CvAdvert & L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY)
    {
        CliPrintf (CliHandle, " BFD CV [3] ");
    }
    if (u1CvAdvert & L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS)
    {
        CliPrintf (CliHandle, " BFD CV[4] ");
    }
    if (u1CvAdvert == L2VPN_VCCV_CV_NONE)
    {
        CliPrintf (CliHandle, " None ");
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************/
/* Function Name : MPLSShowRunningConfigPwOamDisplay                         */
/* Description   : This routine displays the pseudowire-OAM                  */
/* Input(s)      : tCliHandle - Context in which the command is processed    */
/*                 u4PwVcId   - Pseudowire Id                                */
/*                 i4OamStatus - Status of OAM                               */
/*                 u1LocalCcType  - Local Cc type                            */
/*                 u1LocalCvType - Local Cv type                             */
/*                 u1RemoteCcType - Remote Cc type                           */
/*                 u1RemoteCvType - Remote Cv type                           */
/*                 u1CvAdvert - value of Connectivity verification type      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MPLSShowRunningConfigPwOamDisplay (tCliHandle CliHandle, UINT4 u4PwVcId,
                                   UINT1 u1LocalCcType, UINT1 u1LocalCvType,
                                   UINT1 u1RemoteCcType, UINT1 u1RemoteCvType,
                                   INT4 i4CwPreference, BOOL1 bIsStaticPw)
{
    if ((((i4CwPreference == L2VPN_PWVC_CTRLW_PREFERRED) &&
          (u1LocalCcType == L2VPN_VCCV_CC_ALL)) ||
         ((i4CwPreference == L2VPN_PWVC_CTRLW_NOT_PREFERRED) &&
          (u1LocalCcType == L2VPN_VCCV_RAL_TTL)) ||
         (u1LocalCcType == L2VPN_VCCV_CC_NONE)) &&
        ((u1LocalCvType == L2VPN_VCCV_CV_LSPP) ||
         (u1LocalCvType == L2VPN_VCCV_CV_NONE) ||
         (u1LocalCvType == gL2VpnGlobalInfo.u1LocalCvTypeCapabilities)) &&
        (u1RemoteCcType == L2VPN_VCCV_CC_NONE) &&
        (u1RemoteCvType == L2VPN_VCCV_CV_NONE))
    {
        return;
    }
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " pseudowire-oam");
    if (u4PwVcId != L2VPN_ZERO)
    {
        CliPrintf (CliHandle, " pwid");
        CliPrintf (CliHandle, " %u", u4PwVcId);
    }
    if ((u1LocalCcType != L2VPN_VCCV_CC_NONE) && (u1LocalCcType !=
                                                  L2VPN_VCCV_CC_ALL))
    {
        CliPrintf (CliHandle, " local-cc-type");
        if (u1LocalCcType & L2VPN_VCCV_CC_ACH)
        {
            CliPrintf (CliHandle, " pw-ach");
        }
        if (u1LocalCcType & L2VPN_VCCV_CC_RAL)
        {
            CliPrintf (CliHandle, " router-alert-label");
        }
        if (u1LocalCcType & L2VPN_VCCV_CC_TTL_EXP)
        {
            CliPrintf (CliHandle, " ttl-expiry ");
            CliPrintf (CliHandle, " \r\n ");

        }
    }
    if ((u1LocalCvType != L2VPN_VCCV_CV_NONE) && (u1LocalCvType !=
                                                  L2VPN_VCCV_CV_LSPP))
    {
        CliPrintf (CliHandle, " local-cv-type");
        if (u1LocalCvType & L2VPN_VCCV_CV_LSPP)
        {
            CliPrintf (CliHandle, " lsp-ping");
        }
        if (u1LocalCvType & L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY)
        {
            CliPrintf (CliHandle, " bfd-ip-encap-fault");
        }
        if (u1LocalCvType & L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS)
        {
            CliPrintf (CliHandle, " bfd-ip-encap-fault-status-notify");
        }
        if (u1LocalCvType & L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY)
        {
            CliPrintf (CliHandle, " bfd-ach-encap-fault");
        }
        if (u1LocalCvType & L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS)
        {
            CliPrintf (CliHandle, " bfd-ach-encap-fault-status-notify");
        }
    }

    if ((u1RemoteCcType != L2VPN_VCCV_CC_NONE) &&
        (u1RemoteCcType != L2VPN_VCCV_CC_ALL) && (bIsStaticPw != L2VPN_FALSE))
    {
        CliPrintf (CliHandle, " remote-cc-type");
        if (u1RemoteCcType & L2VPN_VCCV_CC_ACH)
        {
            CliPrintf (CliHandle, " pw-ach");
        }
        if (u1RemoteCcType & L2VPN_VCCV_CC_RAL)
        {
            CliPrintf (CliHandle, " router-alert-label");
        }
        if (u1RemoteCcType & L2VPN_VCCV_CC_TTL_EXP)
        {
            CliPrintf (CliHandle, " ttl-expiry");
        }
    }
    if ((u1RemoteCvType != L2VPN_VCCV_CV_NONE) &&
        (u1RemoteCvType != L2VPN_VCCV_CV_LSPP) && (bIsStaticPw != L2VPN_FALSE))
    {
        CliPrintf (CliHandle, " remote-cv-type");
        if (u1RemoteCvType & L2VPN_VCCV_CV_LSPP)
        {
            CliPrintf (CliHandle, " lsp-ping");
        }
        if (u1RemoteCvType & L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY)
        {
            CliPrintf (CliHandle, " bfd-ip-encap-fault");
        }

        if (u1RemoteCvType & L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS)
        {
            CliPrintf (CliHandle, " bfd-ip-encap-fault-status-notify");
        }
        if (u1RemoteCvType & L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY)
        {
            CliPrintf (CliHandle, " bfd-ach-encap-fault");
        }
        if (u1RemoteCvType & L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS)
        {
            CliPrintf (CliHandle, " bfd-ach-encap-fault-status-notify");
        }
    }
    CliPrintf (CliHandle, " \r\n ");
}

/******************************************************************************
* Function Name : L2VpnCliSetPwIfIndex
* Description   : This routine will set the PwIfIndex.
* Input(s)      : u4PwID                 - Index for the row identifying a PW
*                                          in the pwTable
*                 u4PwIfIndex            - pseudowire IfIndex.
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpnCliSetPwIfIndex (tCliHandle CliHandle, UINT4 u4PwId, UINT4 u4PwIfIndex)
{
    UINT4               u4ErrorCode = 0;
    tPwVcEntry         *pPwVcEntry = NULL;
#ifdef HVPLS_WANTED
    UINT1               u1AppOwner = MPLS_APPLICATION_NONE;
#endif
    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (u4PwId);
    if (pPwVcEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% PwId is Not Present \r\n");
        return CLI_FAILURE;
    }

#ifdef HVPLS_WANTED
    u1AppOwner = L2VpPortGetPortOwnerFromIndex (pPwVcEntry->u4PwIfIndex);
    if (u1AppOwner == MPLS_APPLICATION_ERPS && u4PwIfIndex == 0)
    {
        CliPrintf (CliHandle,
                   "\r%% Unmap is not allowed as pwIfIndex is being used by ERPS. \r\n");
        return CLI_FAILURE;
    }
#endif
    if (nmhTestv2PwRowStatus (&u4ErrorCode, L2VPN_PWVC_INDEX (pPwVcEntry),
                              MPLS_STATUS_NOT_INSERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed in test of Pseudo wire RowStatus\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwRowStatus (L2VPN_PWVC_INDEX (pPwVcEntry),
                           MPLS_STATUS_NOT_INSERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Not able to change the pseudowire RowStatus"
                   "as not in service Check all the parameters.\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2PwIfIndex (&u4ErrorCode, pPwVcEntry->u4PwVcIndex, u4PwIfIndex)
        == SNMP_FAILURE)
    {
        if (nmhSetPwRowStatus
            (L2VPN_PWVC_INDEX (pPwVcEntry), MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            /*Failure condition not checked */
        }
        CliPrintf (CliHandle, "\r%%Test fail for u4PwIfIndex\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetPwIfIndex (pPwVcEntry->u4PwVcIndex, u4PwIfIndex) == SNMP_FAILURE)
    {
        if (nmhSetPwRowStatus
            (L2VPN_PWVC_INDEX (pPwVcEntry), MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            /*Failure condition not checked */
        }
        CliPrintf (CliHandle, "\r%%Unable to set u4PwIfIndex\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2PwRowStatus (&u4ErrorCode, L2VPN_PWVC_INDEX (pPwVcEntry),
                              MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed in test of Pseudo wire RowStatus\n");
        return CLI_FAILURE;
    }

    if (nmhSetPwRowStatus (L2VPN_PWVC_INDEX (pPwVcEntry),
                           MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Not able to change the pseudowire RowStatus"
                   " as Active Check all the parameters.\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2vpnCliGetCxtId
* Description   : This function returns the context Id
* Input(s)      : None
* Output(s)     : None
* Return(s)     : Context Id
*******************************************************************************/
UINT4
L2vpnCliGetCxtId (VOID)
{
    if (VcmGetL2ModeExt () == VCM_MI_MODE)
    {
        return CLI_GET_CXT_ID ();
    }
    else
    {
        return L2IWF_DEFAULT_CONTEXT;
    }
}

/*******************************************************************************
*  Function Name : MplsXCShowRunningConfigInterface
*  Input         : tCliHandle CliHadle
*  Description   : This function shows the Xconnect configurations for interface mode
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE
*******************************************************************************/
INT4
MplsXCShowRunningConfigInterface (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4PwEnetPwVlan = 0;
    UINT4               u4VplsIndex = 0;
    UINT4               u4VplsIndexPrev = 0;
    INT4                i4PwEnetVlanMode = 0;
    BOOL1               bLoop = FALSE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    static UINT1        au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = { 0 };

    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    while (1)
    {
        /* u4PwIndex = 0; */
        MPLS_L2VPN_LOCK ();
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexFsMplsVplsConfigTable (u4VplsIndexPrev,
                                                   &u4VplsIndex)))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }
        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexFsMplsVplsConfigTable (&u4VplsIndex)))
        {
            MPLS_L2VPN_UNLOCK ();
            return CLI_SUCCESS;
        }

        bLoop = TRUE;
        u4VplsIndexPrev = u4VplsIndex;

        /* if PseudoWire mode is VPLS the display the details */
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

        if (pVplsEntry == NULL)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        if ((pPwVplsNode = TMO_SLL_First (&pVplsEntry->PwList)) == NULL)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (L2VPN_VPLS != L2VPN_PWVC_MODE (pPwEntry))
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }

        if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
        {
            /* Checking whether any AC is connected to this Vlan */
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (pPwVcEnetEntry->u1EnetMode == L2VPN_CLI_VLAN_MODE)
                {
                    MPLS_L2VPN_UNLOCK ();
                    continue;
                }

                /* get Vlan mode */
                i4PwEnetVlanMode = L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry);

                if ((L2VPN_VLANMODE_CHANGEVLAN == i4PwEnetVlanMode) ||
                    (L2VPN_VLANMODE_ADDVLAN == i4PwEnetVlanMode) ||
                    (L2VPN_VLANMODE_REMOVEVLAN == i4PwEnetVlanMode))
                {
                    u4PwEnetPwVlan = L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
                }
                /* get vpls name */
                MEMSET (au1VplsName, L2VPN_ZERO, sizeof (au1VplsName));
                if (SNMP_FAILURE ==
                    nmhGetFsMplsVplsName (u4VplsIndex, &VplsName))
                {
                    continue;
                }

                MPLS_L2VPN_UNLOCK ();

                if (CFA_FAILURE !=
                    CfaCliConfGetIfName ((UINT4)
                                         L2VPN_PWVC_ENET_PORT_IF_INDEX
                                         (pPwVcEnetEntry), (INT1 *) au1IfName))
                {
                    CliPrintf (CliHandle, "interface %s ", au1IfName);

                    CliPrintf (CliHandle, "\r\n xconnect vfi %s ",
                               VplsName.pu1_OctetList);
                    if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                        L2VPN_PWVC_ENET_DEF_PW_VLAN)
                    {
                        CliPrintf (CliHandle, "port-vlan vlan ");
                        CliPrintf (CliHandle, "%d \r\n",
                                   L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry));
                    }
                }

                if (i4PwEnetVlanMode != L2VPN_PWVC_ENET_DEF_VLAN_MODE)
                {
                    CliPrintf (CliHandle, "vlanmode ");

                    switch (i4PwEnetVlanMode)
                    {
                        case L2VPN_VLANMODE_OTHER:
                            CliPrintf (CliHandle, "other\r\n");
                            break;
                        case L2VPN_VLANMODE_PORTBASED:
                            CliPrintf (CliHandle, "portbased\r\n");
                            break;
                        case L2VPN_VLANMODE_CHANGEVLAN:
                            CliPrintf (CliHandle, "changevlan %u\r\n",
                                       u4PwEnetPwVlan);
                            break;
                        case L2VPN_VLANMODE_ADDVLAN:
                            CliPrintf (CliHandle, "addvlan %u\r\n",
                                       u4PwEnetPwVlan);
                            break;
                        case L2VPN_VLANMODE_REMOVEVLAN:
                            CliPrintf (CliHandle, "removevlan %u\r\n",
                                       u4PwEnetPwVlan);
                            break;
                        default:
                            break;
                    }
                }
                CliPrintf (CliHandle, "\r\n!\n");
                MPLS_L2VPN_LOCK ();
            }
        }

        MPLS_L2VPN_UNLOCK ();
    }                            /*While ends */

    return CLI_SUCCESS;
}

 /**************************************************************************/
 /*                                                                        */
 /*     Function Name : IssMplsL2vpnShowDebugging                          */
 /*                                                                        */
 /*     Description   : This function is used to display debug level for   */
 /*                     MPLS L2VPN module                                  */
 /*                                                                        */
 /*     INPUT         : CliHandle                                          */
 /*                                                                        */
 /*     OUTPUT        : None                                               */
 /*                                                                        */
 /*     RETURNS       : None                                               */
 /*                                                                        */
 /**************************************************************************/

VOID
IssMplsL2vpnShowDebugging (tCliHandle CliHandle)
{
    INT4                i4Level = 0;

    CliRegisterLock (CliHandle, L2vpnLock, L2vpnUnLock);
    MPLS_L2VPN_LOCK ();

    nmhGetFsMplsL2VpnTrcFlag (&i4Level);

    MPLS_L2VPN_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    if (i4Level == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rMPLS L2VPN :");
    if ((i4Level & L2VPN_DBG_VCCV_MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS l2vpn vccv-mgmt debugging is on");
    }
    if ((i4Level & L2VPN_DBG_VCCV_CR_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS l2vpn vccv-critical debugging is on");
    }
    if ((i4Level & L2VPN_DBG_VCCV_CAPAB_EXG_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS l2vpn vccv-capability-exchange debugging is on");
    }
    if ((i4Level & L2VPN_DBG_GRACEFUL_RESTART) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS l2vpn graceful restart debugging is on");
    }
    if ((i4Level & L2VPN_DBG_FNC_CRT_FLAG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS l2vpn critical functional debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_pwred_cmd                               */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the pseudowire redundancy related      */
/*                        commands for the L2VPN                            */
/*                        Module (VPWS )                                     */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS or CLI_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_pwred_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pau4Args[L2VPN_CLI_MAX_ARGS];
    UINT4               u4PwRedGrpId = 0;
    UINT4               u4PwRedIccpId = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = L2VPN_FAILURE;
    INT1                i1ArgNo = L2VPN_ZERO;
    UINT1               u1PwRedGrpStatus = L2VPN_ZERO;
    UINT1               u1AddrType = 0;
    tL2vpnCliPwRedArgs  L2vpnCliPwRedArgs;

    if (L2VPN_INITIALISED != TRUE)
    {
        CliPrintf (CliHandle, "\r %%L2VPN module is shutdown\n");
        return CLI_FAILURE;
    }
    if (u4Command != CLI_MPLS_RED_GRP_ENABLE)
    {
        if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
        {
            CliPrintf (CliHandle, "\r%%PW Redundancy Disabled\n");
            return CLI_FAILURE;
        }
    }
    va_start (ap, u4Command);

    MEMSET (&L2vpnCliPwRedArgs, L2VPN_ZERO, sizeof (tL2vpnCliPwRedArgs));

    while (1)
    {
        pau4Args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == L2VPN_PWRED_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);
    CliRegisterLock (CliHandle, L2vpnLock, L2vpnUnLock);
    MPLS_L2VPN_LOCK ();

    switch (u4Command)
    {
        case CLI_MPLS_RED_GRP_ENABLE:

            L2vpnCliPwRedArgs.u1IsPwRedEnabled = L2VPN_PWRED_ENABLED;
            i4RetStatus = L2VpCliSetPwRedStatus (CliHandle, &L2vpnCliPwRedArgs);
            break;

        case CLI_MPLS_RED_GRP_DISABLE:

            L2vpnCliPwRedArgs.u1IsPwRedEnabled = L2VPN_PWRED_DISABLED;
            i4RetStatus = L2VpCliSetPwRedStatus (CliHandle, &L2vpnCliPwRedArgs);
            break;

        case CLI_MPLS_RED_GRP_SYNC_NOTIF_ENABLE:

            L2vpnCliPwRedArgs.u1PwRedSyncStatus = L2VPN_PWRED_SYNC_NOTIF_ENABLE;
            i4RetStatus = L2VpCliSetPwRedSyncNotification (CliHandle,
                                                           &L2vpnCliPwRedArgs);
            break;
        case CLI_MPLS_RED_GRP_SYNC_NOTIF_DISABLE:

            L2vpnCliPwRedArgs.u1PwRedSyncStatus =
                L2VPN_PWRED_SYNC_NOTIF_DISABLE;
            i4RetStatus =
                L2VpCliSetPwRedSyncNotification (CliHandle, &L2vpnCliPwRedArgs);
            break;

        case CLI_MPLS_RED_GRP_PWSTATUS_NOTIF_ENABLE:

            L2vpnCliPwRedArgs.u1PwRedSyncStatus =
                L2VPN_PWRED_PWSTATUS_NOTIF_ENABLE;
            i4RetStatus =
                L2VpCliSetPwRedPwStatusNotification (CliHandle,
                                                     &L2vpnCliPwRedArgs);
            break;
        case CLI_MPLS_RED_GRP_PWSTATUS_NOTIF_DISABLE:

            L2vpnCliPwRedArgs.u1PwRedSyncStatus =
                L2VPN_PWRED_PWSTATUS_NOTIF_DISABLE;
            i4RetStatus =
                L2VpCliSetPwRedPwStatusNotification (CliHandle,
                                                     &L2vpnCliPwRedArgs);
            break;

        case CLI_MPLS_RED_GRP_NEG_TIMEOUT:

            L2vpnCliPwRedArgs.u2PwRedNegTimeOut = *pau4Args[0];
            i4RetStatus = L2VpCliSetPwRedNegTimeOut (CliHandle,
                                                     &L2vpnCliPwRedArgs);
            break;

        case CLI_MPLS_RED_GRP_CREATE:

            L2vpnCliPwRedArgs.u4PwRedGrpId = *pau4Args[0];
            u1PwRedGrpStatus = L2VPN_PWRED_GROUP_CREATE;
            i4RetStatus = L2VpCliConfigPwRedGrp (CliHandle,
                                                 &L2vpnCliPwRedArgs,
                                                 u1PwRedGrpStatus);
            break;

        case CLI_MPLS_RED_GRP_DELETE:

            L2vpnCliPwRedArgs.u4PwRedGrpId = *pau4Args[0];
            u1PwRedGrpStatus = L2VPN_PWRED_GROUP_DELETE;
            i4RetStatus = L2VpCliConfigPwRedGrp (CliHandle,
                                                 &L2vpnCliPwRedArgs,
                                                 u1PwRedGrpStatus);
            break;

        case CLI_MPLS_RED_GRP_ACTIVATE:

            L2vpnCliPwRedArgs.u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            i4RetStatus = L2VpCliConfigPwRedGrpActive (CliHandle,
                                                       &L2vpnCliPwRedArgs,
                                                       CLI_MPLS_RED_GRP_ACTIVATE);
            break;

        case CLI_MPLS_RED_GRP_DEACTIVATE:

            L2vpnCliPwRedArgs.u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            i4RetStatus = L2VpCliConfigPwRedGrpActive (CliHandle,
                                                       &L2vpnCliPwRedArgs,
                                                       CLI_MPLS_RED_GRP_DEACTIVATE);
            break;
        case CLI_MPLS_RED_GRP_REVERSION_ENABLE:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1ReversionStatus = L2VPN_PWRED_REVERT_ENABLE;
            i4RetStatus = L2VpCliSetPwRedGrpRevertion (CliHandle,
                                                       &L2vpnCliPwRedArgs,
                                                       u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_REVERSION_DISABLE:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1ReversionStatus = L2VPN_PWRED_REVERT_DISABLE;
            i4RetStatus = L2VpCliSetPwRedGrpRevertion (CliHandle,
                                                       &L2vpnCliPwRedArgs,
                                                       u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_NAME:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            i4RetStatus = L2VpCliSetPwRedGrpName (CliHandle,
                                                  (UINT1 *) pau4Args[0],
                                                  u4PwRedGrpId);

            break;
        case CLI_MPLS_RED_GRP_PROT_TYPE_CONFIG:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1ProtType = *pau4Args[0];
            i4RetStatus = L2VpCliSetPwRedGrpProtType (CliHandle,
                                                      &L2vpnCliPwRedArgs,
                                                      u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_WTR_VAL:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1PwRedWtrVal = *pau4Args[0];
            i4RetStatus = L2VpCliSetPwRedGrpWtrValue (CliHandle,
                                                      &L2vpnCliPwRedArgs,
                                                      u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_MODE_CONFIG:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1PwRedContResType = *pau4Args[0];
            i4RetStatus = L2VpCliSetPwRedContentionMethod (CliHandle,
                                                           &L2vpnCliPwRedArgs,
                                                           u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_STATUS_CONFIG:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1PwRedStatus = *pau4Args[0];
            i4RetStatus = L2VpCliSetPwRedNodeStatus (CliHandle,
                                                     &L2vpnCliPwRedArgs,
                                                     u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_LOCK_PROTECT:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1PwRedLockProtect = L2VPNRED_LOCKOUT_ENABLE;
            i4RetStatus = L2VpCliSetPwRedLockoutProtect (CliHandle,
                                                         &L2vpnCliPwRedArgs,
                                                         u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_LOCK_PROTECT_DISABLE:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1PwRedLockProtect = L2VPNRED_LOCKOUT_DISABLE;
            i4RetStatus = L2VpCliSetPwRedLockoutProtect (CliHandle,
                                                         &L2vpnCliPwRedArgs,
                                                         u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_FS:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u4PwRedForceActPw = *pau4Args[0];
            i4RetStatus = L2VpCliForcePwRedActivePw (CliHandle,
                                                     &L2vpnCliPwRedArgs,
                                                     u4PwRedGrpId);
            break;

        case CLI_MPLS_RED_GRP_MULTI_HOMING_APPS_CONFIG:

            u4PwRedGrpId = (UINT4) CLI_GET_PW_RG_ID ();
            L2vpnCliPwRedArgs.u1MultiHomingApps = *pau4Args[0];
            i4RetStatus = L2VpCliSetPwRedDualHomApps (CliHandle,
                                                      &L2vpnCliPwRedArgs,
                                                      u4PwRedGrpId);
            break;

        case CLI_MPLS_PW_RED_ICCP_GRP:

            u4PwRedIccpId = *pau4Args[0];
            i4RetStatus = L2VpCliConfigPwRedIccpGrp (CliHandle, u4PwRedIccpId);
            break;

        case CLI_MPLS_RED_GRP_ICCP_NEIGHBOR:
            u4PwRedIccpId = (UINT4) CLI_GET_PW_ICCP_ID ();
            u1AddrType = *pau4Args[1];
            i4RetStatus = L2VpCliConfigPwRedIccpNeighbor (CliHandle,
                                                          u4PwRedIccpId,
                                                          *(INT4 *) (VOID *)
                                                          pau4Args[0],
                                                          u1AddrType);
            break;

        case CLI_MPLS_RED_GRP_NO_ICCP_NEIGHBOR:
            u4PwRedIccpId = CLI_GET_PW_ICCP_ID ();
            u1AddrType = *pau4Args[1];
            i4RetStatus = L2VpCliDeletePwRedIccpNeighbor (CliHandle,
                                                          u4PwRedIccpId,
                                                          *(UINT4 *) (VOID *)
                                                          pau4Args[0],
                                                          u1AddrType);
            break;

        case CLI_MPLS_RED_GRP_ICCP_NEIGHBOR_ACTIVATE:
            u4PwRedIccpId = (UINT4) CLI_GET_PW_ICCP_ID ();
            u1AddrType = *pau4Args[1];
            i4RetStatus = L2VpCliConfigPwRedIccpNeighborActivate (CliHandle,
                                                                  u4PwRedIccpId,
                                                                  *(UINT4
                                                                    *) (VOID *)
                                                                  pau4Args[0],
                                                                  u1AddrType);
            break;

        case CLI_MPLS_PSW_RED_SHOW_GBL_INFO:
            i4RetStatus = L2VpCliShowPwRedGlobalInfo (CliHandle);
            break;
        case CLI_MPLS_PSW_RED_SHOW_GRP_INFO:
            if (pau4Args[0] != NULL)
            {
                u4PwRedGrpId = *pau4Args[0];
            }
            else
            {
                u4PwRedGrpId = 0;
            }
            i4RetStatus = L2VpCliShowPwRedGrpInfo (CliHandle, u4PwRedGrpId);
            break;

        case CLI_MPLS_SHOW_RED_PWS:
            i4RetStatus = L2VpCliShowPwRedPwInfo (CliHandle, pau4Args[0]);
            break;

        case CLI_MPLS_SHOW_RED_NODES:
            i4RetStatus = L2VpCliShowPwRedNbrInfo (CliHandle, pau4Args[0]);
            break;

        case CLI_MPLS_SHOW_ICCP_PWS:
            i4RetStatus = L2VpCliShowPwRedIccpPwInfo (CliHandle, pau4Args[0]);
            break;
        default:
            break;
    }
    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS_PWRED)
            && (u4ErrCode < L2VPN_PWRED_CLI_MAX_PWRED_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%s",
                       L2VPN_MPLS_CLI_PWRED_ERROR_MSGS[CLI_ERR_OFFSET_MPLS_PWRED
                                                       (u4ErrCode)]);
            CLI_SET_CMD_STATUS (CLI_FAILURE);
            u4ErrCode = 0;
        }
        CLI_SET_ERR (0);
    }
    MPLS_L2VPN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedStatus
* Description   : This routine sets the pseudowire redundancy status
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire 
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedStatus (tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsL2VpnPwRedundancyStatus (&u4ErrorCode,
                                            pL2vpnCliPwRedArgs->
                                            u1IsPwRedEnabled) == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_PWRED_STATUS);
        return CLI_FAILURE;
    }

    nmhSetFsL2VpnPwRedundancyStatus (pL2vpnCliPwRedArgs->u1IsPwRedEnabled);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedSyncNotification
* Description   : This routine enables the pseudowire redundancy sync
*                 notification status
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire 
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedSyncNotification (tCliHandle CliHandle,
                                 tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsL2VpnPwRedundancySyncFailNotifyEnable (&u4ErrorCode,
                                                          pL2vpnCliPwRedArgs->
                                                          u1PwRedSyncStatus) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_SYNC_NOTIFY_STATUS);
        return CLI_FAILURE;
    }

    nmhSetFsL2VpnPwRedundancySyncFailNotifyEnable
        (pL2vpnCliPwRedArgs->u1PwRedSyncStatus);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedPwStatusNotification
* Description   : This routine enables the pseudowire redundancy pseudowire status
*                 notification status
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire 
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedPwStatusNotification (tCliHandle CliHandle,
                                     tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedundancyPwStatusNotifyEnable (&u4ErrorCode,
                                                          pL2vpnCliPwRedArgs->
                                                          u1PwRedSyncStatus) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_PWSTATUS_NOTIFY);
        return CLI_FAILURE;
    }

    nmhSetFsL2VpnPwRedundancyPwStatusNotifyEnable
        (pL2vpnCliPwRedArgs->u1PwRedSyncStatus);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedNegTimeOut
* Description   : This routine sets the negotiation timer value
*                
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedNegTimeOut (tCliHandle CliHandle,
                           tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedNegotiationTimeOut (&u4ErrorCode,
                                                 pL2vpnCliPwRedArgs->
                                                 u2PwRedNegTimeOut) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_INVALID_PWNEG_TIMEOUT);
        return CLI_FAILURE;
    }

    if (nmhSetFsL2VpnPwRedNegotiationTimeOut
        (pL2vpnCliPwRedArgs->u2PwRedNegTimeOut) == SNMP_FAILURE)
    {
        /*Failure condition not handled here */
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliConfigPwRedGrp
* Description   : This routine creates or deletes the pseudowire redundancy
*                 class
*                
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliConfigPwRedGrp (tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                       UINT1 u1PwRedGrpStatus)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsL2VpnPwRedGroupRowStatus (pL2vpnCliPwRedArgs->u4PwRedGrpId,
                                          &i4RowStatus) != SNMP_FAILURE)
    {
        if (u1PwRedGrpStatus == L2VPN_PWRED_GROUP_CREATE)
        {
            if (L2VpCliChangePwRedGrpMode (CliHandle,
                                           pL2vpnCliPwRedArgs->u4PwRedGrpId) !=
                CLI_SUCCESS)
            {
                return CLI_FAILURE;
            }
            return CLI_SUCCESS;
        }
    }

    if (L2VPN_PWRED_GROUP_CREATE == u1PwRedGrpStatus)
    {
        if (nmhTestv2FsL2VpnPwRedGroupRowStatus
            (&u4ErrorCode, pL2vpnCliPwRedArgs->u4PwRedGrpId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_CREATE_FAILURE);
            return CLI_FAILURE;
        }

        if (nmhSetFsL2VpnPwRedGroupRowStatus (pL2vpnCliPwRedArgs->u4PwRedGrpId,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (L2VPN_PWRED_CLI_GRP_SET_FAILURE);
            return CLI_FAILURE;
        }
        if (L2VpCliChangePwRedGrpMode (CliHandle,
                                       pL2vpnCliPwRedArgs->u4PwRedGrpId) !=
            CLI_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else if (L2VPN_PWRED_GROUP_DELETE == u1PwRedGrpStatus)
    {
        if (nmhTestv2FsL2VpnPwRedGroupRowStatus
            (&u4ErrorCode, pL2vpnCliPwRedArgs->u4PwRedGrpId, DESTROY)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_DELETE_FAILURE);
            return CLI_FAILURE;
        }

        if (nmhSetFsL2VpnPwRedGroupRowStatus (pL2vpnCliPwRedArgs->u4PwRedGrpId,
                                              DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (L2VPN_PWRED_CLI_GRP_SET_FAILURE);
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliConfigPwRedGrpActive
* Description   : This routine activates the RG group
*      
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliConfigPwRedGrpActive (tCliHandle CliHandle,
                             tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                             UINT1 u1PwRedGrpStatus)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    if (u1PwRedGrpStatus == CLI_MPLS_RED_GRP_ACTIVATE)
    {
        if (nmhTestv2FsL2VpnPwRedGroupRowStatus (&u4ErrorCode,
                                                 pL2vpnCliPwRedArgs->
                                                 u4PwRedGrpId,
                                                 ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_ACTIVATE_FAILURE);
            return CLI_FAILURE;
        }

        if (nmhSetFsL2VpnPwRedGroupRowStatus (pL2vpnCliPwRedArgs->u4PwRedGrpId,
                                              ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (L2VPN_PWRED_CLI_GRP_ACTIVATE_SET_FAILURE);
            return CLI_FAILURE;
        }
    }
    else if (u1PwRedGrpStatus == CLI_MPLS_RED_GRP_DEACTIVATE)
    {
        if (nmhTestv2FsL2VpnPwRedGroupRowStatus (&u4ErrorCode,
                                                 pL2vpnCliPwRedArgs->
                                                 u4PwRedGrpId,
                                                 NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (L2VPN_PWRED_CLI_GRP_ACTIVATE_FAILURE);
            return CLI_FAILURE;
        }

        if (nmhSetFsL2VpnPwRedGroupRowStatus (pL2vpnCliPwRedArgs->u4PwRedGrpId,
                                              NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (L2VPN_PWRED_CLI_GRP_ACTIVATE_SET_FAILURE);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedGrpRevertion
* Description   : This routine sets the revertion mode for the redundancy group
*                 class
*                
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId           - The group fro which the revertion mode has
*                                          to be set
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedGrpRevertion (tCliHandle CliHandle,
                             tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                             UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedGroupReversionType (&u4ErrorCode,
                                                 u4PwRedGrpId,
                                                 pL2vpnCliPwRedArgs->
                                                 u1ReversionStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupReversionType (u4PwRedGrpId,
                                              pL2vpnCliPwRedArgs->
                                              u1ReversionStatus) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_REVERTIVE_SET_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedProtType
* Description   : This routine sets the protection type for the redundancy group
*                 class
*                
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedGrpProtType (tCliHandle CliHandle,
                            tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                            UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedGroupProtType (&u4ErrorCode,
                                            u4PwRedGrpId,
                                            pL2vpnCliPwRedArgs->u1ProtType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupProtType (u4PwRedGrpId,
                                         pL2vpnCliPwRedArgs->u1ProtType) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_PROTECTION_SET_FAILURE);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedGrpName
* Description   : This routine sets the revertion mode for the redundancy group
*                 class
*                
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId           - The group fro which the revertion mode has
*                                          to be set
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedGrpName (tCliHandle CliHandle,
                        UINT1 *pu1PwRedGrpName, UINT4 u4PwRedGrpId)
{

    UINT1               au1PwRedGrpName[32];
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE PwRedGrpName;

    UNUSED_PARAM (CliHandle);
    MEMSET (au1PwRedGrpName, 0, 32);
    PwRedGrpName.pu1_OctetList = au1PwRedGrpName;
    if (pu1PwRedGrpName != NULL)
    {
        PwRedGrpName.i4_Length = STRLEN (pu1PwRedGrpName);
        MEMCPY (PwRedGrpName.pu1_OctetList, pu1PwRedGrpName,
                PwRedGrpName.i4_Length);
    }
    else
    {
        PwRedGrpName.i4_Length = 32;
    }

    if (nmhTestv2FsL2VpnPwRedGroupName (&u4ErrorCode,
                                        u4PwRedGrpId,
                                        &PwRedGrpName) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupName (u4PwRedGrpId,
                                     &PwRedGrpName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_RG_NAME_SET_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedGrpWtrValue
* Description   : This routine sets the wait-to-restore value for the
*                 redundant group
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedGrpWtrValue (tCliHandle CliHandle,
                            tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                            UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedGroupWtrTimer (&u4ErrorCode,
                                            u4PwRedGrpId,
                                            pL2vpnCliPwRedArgs->
                                            u1PwRedWtrVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupWtrTimer (u4PwRedGrpId,
                                         pL2vpnCliPwRedArgs->u1PwRedWtrVal) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_WTR_SET_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name :L2VpCliSetPwRedContentionMethod 
*
* Description   : This routine sets the contention-resolution method for the 
*                 redundant group
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedContentionMethod (tCliHandle CliHandle,
                                 tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                                 UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedGroupContentionResolutionMethod (&u4ErrorCode,
                                                              u4PwRedGrpId,
                                                              pL2vpnCliPwRedArgs->
                                                              u1PwRedContResType)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupContentionResolutionMethod (u4PwRedGrpId,
                                                           pL2vpnCliPwRedArgs->
                                                           u1PwRedContResType)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_CONTENTION_RES_SET_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name :L2VpCliSetPwRedLockoutProtect 
*
* Description   : This routine sets the contention-resolution method for the 
*                 redundant group
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedLockoutProtect (tCliHandle CliHandle,
                               tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                               UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedGroupLockoutProtection (&u4ErrorCode,
                                                     u4PwRedGrpId,
                                                     pL2vpnCliPwRedArgs->
                                                     u1PwRedLockProtect)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupLockoutProtection (u4PwRedGrpId,
                                                  pL2vpnCliPwRedArgs->
                                                  u1PwRedLockProtect)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_LOCKOUT_PROTECTION_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name :L2VpCliSetPwRedNodeStatus
*
* Description   : This routine sets the status of the redundant group as either 
*                 as master/slave
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedNodeStatus (tCliHandle CliHandle,
                           tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                           UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsL2VpnPwRedGroupMasterSlaveMode (&u4ErrorCode,
                                                   u4PwRedGrpId,
                                                   pL2vpnCliPwRedArgs->
                                                   u1PwRedStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedGroupMasterSlaveMode (u4PwRedGrpId,
                                                pL2vpnCliPwRedArgs->
                                                u1PwRedStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_MASTER_SLAVE_SET_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name :L2VpCliForcePwRedActivePw
*
* Description   : This function is used to force any pseudowire within the 
*                 group as active pseudowire
*                 
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliForcePwRedActivePw (tCliHandle CliHandle,
                           tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                           UINT4 u4PwRedGrpId)
{

    UINT4               u4ErrorCode = 0;
    UINT4               u4PwIndex = 0;
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromVcId
        (pL2vpnCliPwRedArgs->u4PwRedForceActPw);

    if (pPwVcEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%%PwVc entry does not exist\r\n");
        return CLI_FAILURE;
    }

    u4PwIndex = pPwVcEntry->u4PwVcIndex;

    if (nmhTestv2FsL2VpnPwRedGroupAdminActivePw (&u4ErrorCode,
                                                 u4PwRedGrpId,
                                                 u4PwIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_FORCE_ACTIVE_PW_FAILURE);
        return CLI_FAILURE;
    }

    if (nmhSetFsL2VpnPwRedGroupAdminActivePw (u4PwRedGrpId,
                                              u4PwIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_FORCE_ACTIVE_PW_SET_FAILURE);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsL2VpnPwRedGroupAdminCmd (&u4ErrorCode,
                                            u4PwRedGrpId,
                                            L2VPN_PWRED_MANUAL_SWITCHOVER_CMD)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_FORCE_ACTIVE_PW_FAILURE);
        return CLI_FAILURE;
    }

    if (nmhSetFsL2VpnPwRedGroupAdminCmd (u4PwRedGrpId,
                                         L2VPN_PWRED_MANUAL_SWITCHOVER_CMD)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_FORCE_ACTIVE_PW_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetPwRedDualHomApps
*
* Description   : This function is used to set the multi-homing apps
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliSetPwRedDualHomApps (tCliHandle CliHandle,
                            tL2vpnCliPwRedArgs * pL2vpnCliPwRedArgs,
                            UINT4 u4PwRedGrpId)
{
    UINT1               au1DualHomeApps[MPLS_ONE] = { 0 };
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE DualHomeApps;

    UNUSED_PARAM (CliHandle);

    DualHomeApps.pu1_OctetList = au1DualHomeApps;
    DualHomeApps.i4_Length = sizeof (UINT1);
    au1DualHomeApps[0] |= pL2vpnCliPwRedArgs->u1MultiHomingApps;

    if (nmhTestv2FsL2VpnPwRedGroupDualHomeApps (&u4ErrorCode,
                                                u4PwRedGrpId,
                                                &DualHomeApps) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsL2VpnPwRedGroupDualHomeApps (u4PwRedGrpId,
                                             &DualHomeApps) == SNMP_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_DUAL_HOME_APPS_SET_FAILURE);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name :L2VpCliConfigPwRedIccpGrp
*
* Description   : This function is used to change the mode to ICCP-GRP
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/

INT4
L2VpCliConfigPwRedIccpGrp (tCliHandle CliHandle, UINT4 u4PwRedIccpId)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4PwRedIccpId);

    if (pRgGroup == NULL)
    {
        CLI_SET_ERR (L2VPN_PWRED_CLI_REDPW_ICCP_GRP_FAILURE);
        return CLI_FAILURE;
    }

    if (CLI_FAILURE == (L2VpCliChangeRedPwNodeMode (CliHandle, u4PwRedIccpId)))
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliConfigPwRedIccpNeighbor
*
* Description   : This function is used to change the mode to ICCP-GRP
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/

INT4
L2VpCliConfigPwRedIccpNeighbor (tCliHandle CliHandle,
                                UINT4 u4PwRedIccpId,
                                UINT4 u4IpAddress, UINT1 u1AddrType)
{
    UINT4               u4ErrorCode = 0;
    UINT1               au1Address[MAX_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE NeighborAddr;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Address, 0, MAX_ADDR_LEN);
    NeighborAddr.pu1_OctetList = au1Address;
#ifdef MPLS_IPV6_WANTED
    if (u1AddrType == L2VPN_CLI_IPV6_ADDR_TYPE)
    {
        NeighborAddr.i4_Length = L2VPN_CLI_IPV6_ADDR_TYPE;
    }

    else if (u1AddrType == L2VPN_CLI_IPV4_ADDR_TYPE)
#endif
    {
        NeighborAddr.i4_Length = L2VPN_CLI_IPV4_ADDR_TYPE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (u4IpAddress, (&NeighborAddr));
    if (SNMP_FAILURE == nmhTestv2FsL2VpnPwRedNodeRowStatus (&u4ErrorCode,
                                                            u4PwRedIccpId,
                                                            (INT4) u1AddrType,
                                                            &NeighborAddr,
                                                            CREATE_AND_WAIT))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFsL2VpnPwRedNodeRowStatus (u4PwRedIccpId,
                                                         (INT4) u1AddrType,
                                                         &NeighborAddr,
                                                         CREATE_AND_WAIT))
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliConfigPwRedIccpNeighborActivate
*
* Description   : This function is used to change the mode to ICCP-GRP
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/

INT4
L2VpCliConfigPwRedIccpNeighborActivate (tCliHandle CliHandle,
                                        UINT4 u4PwRedIccpId,
                                        UINT4 u4IpAddress, UINT1 u1AddrType)
{
    UINT4               u4ErrorCode = 0;
    UINT1               au1Address[MAX_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE NeighborAddr;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Address, 0, MAX_ADDR_LEN);
    NeighborAddr.pu1_OctetList = au1Address;
#ifdef MPLS_IPV6_WANTED
    if (u1AddrType == L2VPN_CLI_IPV6_ADDR_TYPE)
    {
        NeighborAddr.i4_Length = L2VPN_CLI_IPV6_ADDR_TYPE;
    }

    else if (u1AddrType == L2VPN_CLI_IPV4_ADDR_TYPE)
#endif
    {
        NeighborAddr.i4_Length = L2VPN_CLI_IPV4_ADDR_TYPE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (u4IpAddress, (&NeighborAddr));
    if (SNMP_FAILURE == nmhTestv2FsL2VpnPwRedNodeRowStatus (&u4ErrorCode,
                                                            u4PwRedIccpId,
                                                            (INT4) u1AddrType,
                                                            &NeighborAddr,
                                                            ACTIVE))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFsL2VpnPwRedNodeRowStatus (u4PwRedIccpId,
                                                         (INT4) u1AddrType,
                                                         &NeighborAddr, ACTIVE))
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : L2VpCliChangePwRedGrpMode                                  *
 *                                                                           *
 * Description  : Changes the mode to PW-RED Grp configuration mode         *
 *                                                                           *
 * Input        : CliHandle - Index of current CLI context                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
L2VpCliChangePwRedGrpMode (tCliHandle CliHandle, UINT4 u4PwRedGrpIndex)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    MEMSET (au1Cmd, 0, sizeof (au1Cmd));

    SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd), "%s%u", L2VPN_PWRED_CLASS_MODE,
              u4PwRedGrpIndex);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter into config-pw-red-class "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpGetPwRedClassCfgPrompt                           *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Protection group. It is exported to CLI module.  *
 *                    This function will be invoked when the System is     *
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
L2VpGetPwRedClassCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4PwRedGrpId = 0;
    UINT4               u4Len = STRLEN ("pw-red-class");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "pw-red-class", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4PwRedGrpId = (UINT4) CLI_ATOI (pi1ModeName);

    /*
     * No need to take lock here, since it is taken by
     cli_process_pwred_cmd
     * 
     */
    CLI_SET_PW_RG_ID ((INT4) u4PwRedGrpId);

    STRNCPY (pi1DispStr, "(config-pw-red-class)#", (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';

    return TRUE;
}

/*****************************************************************************
 *                                                                           *
 * Function     : L2VpCliChangeRedPwNodeMode                                 *
 *                                                                           *
 * Description  : Changes the mode to BFD Session configuration mode         *
 *                                                                           *
 * Input        : CliHandle - Index of current CLI context                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
L2VpCliChangeRedPwNodeMode (tCliHandle CliHandle, UINT4 u4PwRedGrpIndex)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    MEMSET (au1Cmd, 0, sizeof (au1Cmd));

    SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd), "%s%u", L2VPN_PWRED_NODE_MODE,
              u4PwRedGrpIndex);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter into iccp-grp "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpGetPwRedNodePrompt                           *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Protection group. It is exported to CLI module.  *
 *                    This function will be invoked when the System is     *
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
L2VpGetPwRedNodePrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4PwRedGrpId = 0;
    UINT4               u4Len = STRLEN ("iccp-grp");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "iccp-grp", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4PwRedGrpId = CLI_ATOI (pi1ModeName);

    /*
     * No need to take lock here, since it is taken by
     cli_process_pwred_cmd
     * 
     */
    CLI_SET_PW_ICCP_ID ((INT4) u4PwRedGrpId);

    STRNCPY (pi1DispStr, "(config-iccp-grp)#", (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';

    return TRUE;
}

/******************************************************************************
* Function Name : L2VpCliDeletePwRedIccpNeighbor
*
* Description   : This function is used to change the mode to ICCP-GRP
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliPwRedArgs    - A pointer to a structure that contains
*                                         parameters related to pseudowire
*                                         redundancy related parameters like group_id,
*                                         operation mode,contention-resolution method,
*                                         node redundancy  status and so on.
*                 u4PwRedGrpId          - The redundancy node group id
*
* Output(s)     : None
*
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/

INT4
L2VpCliDeletePwRedIccpNeighbor (tCliHandle CliHandle,
                                UINT4 u4PwRedIccpId,
                                UINT4 u4IpAddress, UINT1 u1AddrType)
{
    UINT4               u4ErrorCode = 0;
    UINT1               au1Address[MAX_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE NeighborAddr;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Address, 0, MAX_ADDR_LEN);
    NeighborAddr.pu1_OctetList = au1Address;
    if (u1AddrType == L2VPN_CLI_IPV4_ADDR_TYPE)
    {
        NeighborAddr.i4_Length = L2VPN_CLI_IPV4_ADDR_TYPE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (u4IpAddress, (&NeighborAddr));

    if (SNMP_FAILURE == nmhTestv2FsL2VpnPwRedNodeRowStatus (&u4ErrorCode,
                                                            u4PwRedIccpId,
                                                            (INT4) u1AddrType,
                                                            &NeighborAddr,
                                                            NOT_IN_SERVICE))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFsL2VpnPwRedNodeRowStatus (u4PwRedIccpId,
                                                         (INT4) u1AddrType,
                                                         &NeighborAddr,
                                                         NOT_IN_SERVICE))
    {
        return CLI_FAILURE;
    }
    if (SNMP_FAILURE == nmhTestv2FsL2VpnPwRedNodeRowStatus (&u4ErrorCode,
                                                            u4PwRedIccpId,
                                                            (INT4) u1AddrType,
                                                            &NeighborAddr,
                                                            DESTROY))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE == nmhSetFsL2VpnPwRedNodeRowStatus (u4PwRedIccpId,
                                                         (INT4) u1AddrType,
                                                         &NeighborAddr,
                                                         DESTROY))
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnShowRunningConfig                               *
 *                                                                         *
 * DESCRIPTION      : This function displays the configuration done in psw *
 *                    redundancy scalars and tables                        *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : tCliHandle                                           *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 **************************************************************************/
INT4
L2VpnShowRunningConfig (tCliHandle CliHandle)
{
    L2VpnPwRedShowRunningConfigScalar (CliHandle);

    L2VpnPwRedShowRunningConfigTable (CliHandle);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwRedShowRunningConfigScalar                    *
 *                                                                         *
 * DESCRIPTION      : This function displays the configuration done in psw *
 *                    redundancy scalars                                   *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : tCliHandle                                           *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 **************************************************************************/
VOID
L2VpnPwRedShowRunningConfigScalar (tCliHandle CliHandle)
{
    INT4                i4PwRedScalar = 0;
    UINT4               u4PwNegTimeout = 0;

    nmhGetFsL2VpnPwRedundancyStatus (&i4PwRedScalar);

    if (i4PwRedScalar != L2VPN_PWRED_ENABLED)
    {
        CliPrintf (CliHandle, "pseudowire-redundancy disable\r\n");
        i4PwRedScalar = 0;
    }
    nmhGetFsL2VpnPwRedNegotiationTimeOut (&u4PwNegTimeout);

    if (u4PwNegTimeout != L2VPN_PWRED_GRP_DEF_NEG_TIMEOUT)
    {
        CliPrintf (CliHandle,
                   "pseudowire-redundancy negotiation time-out %u\r\n",
                   u4PwNegTimeout);
    }

    nmhGetFsL2VpnPwRedundancySyncFailNotifyEnable (&i4PwRedScalar);

    if (i4PwRedScalar != FALSE)
    {
        CliPrintf (CliHandle,
                   "pseudowire-redundancy sync notification enable\r\n");
        i4PwRedScalar = 0;
    }

    nmhGetFsL2VpnPwRedundancyPwStatusNotifyEnable (&i4PwRedScalar);

    if (i4PwRedScalar != FALSE)
    {
        CliPrintf (CliHandle,
                   "pseudowire-redundancy pwstatus notification enable\r\n");
        i4PwRedScalar = 0;
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwRedShowRunningConfigTable                     *
 *                                                                         *
 * DESCRIPTION      : This function displays the configuration done in psw *
 *                    redundancy related tables                            *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : tCliHandle                                           *
 *                                                                         *
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE                            *
 *                                                                         *
 **************************************************************************/
VOID
L2VpnPwRedShowRunningConfigTable (tCliHandle CliHandle)
{
    L2VpnSRCPwRedGrpTable (CliHandle);

    L2VpnSRCPwRedNodeTable (CliHandle);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnSRCPwRedNodeTable                                *
 *                                                                         *
 * DESCRIPTION      : This function displays the configuration done in psw *
 *                    redundancy node table                                *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : tCliHandle                                           *
 *                                                                         *
 * RETURNS          : None                                                 *
 *                                                                         *
 **************************************************************************/
VOID
L2VpnSRCPwRedNodeTable (tCliHandle CliHandle)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    tSNMP_OCTET_STRING_TYPE L2VpnPwRedNodeAddr;
    tSNMP_OCTET_STRING_TYPE L2VpnPwRedNextNodeAddr;
    UINT4               u4PwRedGrpIndex = 0;
    UINT4               u4PwRedNextGrpIndex = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4PwRedNodeAddrType = 0;
    INT4                i4PwRedNodeNextAddrType = 0;
    UINT1               au1PwRedGrpAddr[IPV6_ADDR_LENGTH];
    UINT1               au1PwRedNextGrpAddr[IPV6_ADDR_LENGTH];
    UINT1               au1IpId[IPV6_ADDR_LENGTH];
    CHR1               *pu1String = NULL;
    uGenAddr            Addr;

    pu1String = (CHR1 *) & au1IpId[0];

    MEMSET (au1PwRedGrpAddr, 0, IPV6_ADDR_LENGTH);
    MEMSET (au1PwRedNextGrpAddr, 0, IPV6_ADDR_LENGTH);
    MEMSET (&Addr, 0, sizeof (uGenAddr));

    L2VpnPwRedNodeAddr.pu1_OctetList = au1PwRedGrpAddr;
    L2VpnPwRedNodeAddr.i4_Length = 0;

    L2VpnPwRedNextNodeAddr.pu1_OctetList = au1PwRedNextGrpAddr;
    L2VpnPwRedNextNodeAddr.i4_Length = 0;

    if (nmhGetFirstIndexFsL2VpnPwRedNodeTable (&u4PwRedGrpIndex,
                                               &i4PwRedNodeAddrType,
                                               &L2VpnPwRedNodeAddr) ==
        SNMP_SUCCESS)
    {
        u4PwRedNextGrpIndex = u4PwRedGrpIndex;
        i4PwRedNodeNextAddrType = i4PwRedNodeAddrType;
        MEMCPY (L2VpnPwRedNextNodeAddr.pu1_OctetList,
                L2VpnPwRedNodeAddr.pu1_OctetList, IPV6_ADDR_LENGTH);

        do
        {
            u4PwRedGrpIndex = u4PwRedNextGrpIndex;
            i4PwRedNodeAddrType = i4PwRedNodeNextAddrType;
            MEMCPY (L2VpnPwRedNodeAddr.pu1_OctetList,
                    L2VpnPwRedNextNodeAddr.pu1_OctetList, IPV6_ADDR_LENGTH);
            MEMCPY (&Addr, L2VpnPwRedNextNodeAddr.pu1_OctetList,
                    sizeof (uGenAddr));

            pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4PwRedGrpIndex,
                                                          i4PwRedNodeAddrType,
                                                          &Addr);

            if ((pRgNode != NULL) && (pRgNode->u1RowStatus == ACTIVE))
            {
                CliPrintf (CliHandle,
                           "interchassis group %u\r\n", u4PwRedGrpIndex);

                MEMCPY (&u4IpAddr, &L2VpnPwRedNextNodeAddr.pu1_OctetList,
                        sizeof (UINT4));

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
                CliPrintf (CliHandle, "member ip  %s\r\n", pu1String);
            }

        }
        while (nmhGetNextIndexFsL2VpnPwRedNodeTable
               (u4PwRedGrpIndex, &u4PwRedNextGrpIndex, i4PwRedNodeAddrType,
                &i4PwRedNodeNextAddrType, &L2VpnPwRedNodeAddr,
                &L2VpnPwRedNextNodeAddr) == SNMP_SUCCESS);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnSRCPwRedGrpTable                                *
 *                                                                         *
 * DESCRIPTION      : This function displays the configuration done in psw *
 *                    redundancy class table                               *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 *                                                                         *
 * OUTPUT           : tCliHandle                                           *
 *                                                                         *
 * RETURNS          : None                                                 *
 *                                                                         *
 **************************************************************************/
VOID
L2VpnSRCPwRedGrpTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RgName;
    UINT4               u4PwRedGrpIndex = 0;
    UINT4               u4PwNextRedGrpIndex = 0;
    INT4                i4PwRedGroupObj = 0;
    UINT4               u4PwRedGroupObj = 0;
    INT4                i4PwRedRowStatus = 0;
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    UINT1               au1RgName[L2VPN_PWRED_GRP_NAME_LEN + 1];

    MEMSET (au1RgName, 0, L2VPN_PWRED_GRP_NAME_LEN);

    RgName.pu1_OctetList = au1RgName;
    RgName.i4_Length = 0;
    if (nmhGetFirstIndexFsL2VpnPwRedGroupTable (&u4PwRedGrpIndex)
        == SNMP_SUCCESS)
    {
        u4PwNextRedGrpIndex = u4PwRedGrpIndex;
        do
        {
            u4PwRedGrpIndex = u4PwNextRedGrpIndex;
            pRgGroup = L2VpnGetRedundancyEntryByIndex (u4PwRedGrpIndex);

            if (pRgGroup != NULL)
            {

                CliPrintf (CliHandle,
                           " pseudowire-redundancy class %u\r\n",
                           u4PwRedGrpIndex);

                nmhGetFsL2VpnPwRedGroupProtType (u4PwRedGrpIndex,
                                                 &i4PwRedGroupObj);
                if (i4PwRedGroupObj == 1)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy protection-type one-plus-one\r\n");
                }
                else if (i4PwRedGroupObj == 3)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy protection-type one-isto-N\r\n");
                }

                i4PwRedGroupObj = 0;
                nmhGetFsL2VpnPwRedGroupReversionType (u4PwRedGrpIndex,
                                                      &i4PwRedGroupObj);
                if (i4PwRedGroupObj == L2VPN_PWRED_MODE_REVERTIVE)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy reversion enable\r\n");
                    nmhGetFsL2VpnPwRedGroupWtrTimer (u4PwRedGrpIndex,
                                                     &u4PwRedGroupObj);

                    if (u4PwRedGroupObj != L2VPN_ZERO)
                    {
                        CliPrintf (CliHandle,
                                   "pseudowire-redundancy wait-to-restore %u\r\n",
                                   u4PwRedGroupObj);
                    }

                }
                i4PwRedGroupObj = 0;
                nmhGetFsL2VpnPwRedGroupContentionResolutionMethod
                    (u4PwRedGrpIndex, &i4PwRedGroupObj);

                if (i4PwRedGroupObj == L2VPNRED_CONTENTION_MASTERSLAVE)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy contention-resolution "
                               "method masterslave\r\n");
                }
                i4PwRedGroupObj = 0;
                nmhGetFsL2VpnPwRedGroupLockoutProtection (u4PwRedGrpIndex,
                                                          &i4PwRedGroupObj);

                if (i4PwRedGroupObj == L2VPNRED_LOCKOUT_ENABLE)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy lockout-protection enable\r\n");
                }
                i4PwRedGroupObj = 0;
                nmhGetFsL2VpnPwRedGroupMasterSlaveMode (u4PwRedGrpIndex,
                                                        &i4PwRedGroupObj);

                if (i4PwRedGroupObj == L2VPNRED_MASTERSLAVE_MASTER)
                {
                    CliPrintf (CliHandle, "status redundancy master\r\n");
                }

                nmhGetFsL2VpnPwRedGroupName (u4PwRedGrpIndex, &RgName);
                if (RgName.i4_Length != 0)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy class name %s\r\n",
                               RgName.pu1_OctetList);
                    MEMSET (RgName.pu1_OctetList, 0, L2VPN_PWRED_GRP_NAME_LEN);
                    RgName.i4_Length = 0;
                }

                nmhGetFsL2VpnPwRedGroupRowStatus (u4PwRedGrpIndex,
                                                  &i4PwRedRowStatus);

                if (i4PwRedRowStatus == ACTIVE)
                {
                    CliPrintf (CliHandle,
                               "pseudowire-redundancy class activate \r\n");
                }

            }

        }
        while (nmhGetNextIndexFsL2VpnPwRedGroupTable
               (u4PwRedGrpIndex, &u4PwNextRedGrpIndex) == SNMP_SUCCESS);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpCliShowPwRedGlobalInfo
 *
 * DESCRIPTION      : This function displays the global pseudowire redundancy
 *                    related information
 *
 * INPUT            : CliHandle
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

INT4
L2VpCliShowPwRedGlobalInfo (tCliHandle CliHandle)
{
    INT4                i4ModuleStatus = 0;
    INT4                i4SyncNotifyStatus = 0;
    INT4                i4PwStatus = 0;
    UINT4               u4PwNegTimeout = 0;

    nmhGetFsL2VpnPwRedundancyStatus (&i4ModuleStatus);

    nmhGetFsL2VpnPwRedNegotiationTimeOut (&u4PwNegTimeout);

    nmhGetFsL2VpnPwRedundancySyncFailNotifyEnable (&i4SyncNotifyStatus);

    nmhGetFsL2VpnPwRedundancyPwStatusNotifyEnable (&i4PwStatus);

    CliPrintf (CliHandle, "\r\n PSEUDOWIRE REDUNDANCY Global Info \r\n");
    CliPrintf (CliHandle, "------------------------------------------\r\n");

    if (i4ModuleStatus == L2VPN_PWRED_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n", "PW-Redundancy Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n", "PW-Redundancy Status");
    }

    if (i4SyncNotifyStatus == TRUE)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n",
                   "PW-Redundancy Sync fail Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n",
                   "PW-Redundancy Sync fail Status");
    }

    if (i4PwStatus == TRUE)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n",
                   "PW-Redundancy Status Notification");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n",
                   "PW-Redundancy Status Notification");
    }

    CliPrintf (CliHandle, "%-33s: %u\r\n",
               "PW-Redundancy Negotiation time-out value", u4PwNegTimeout);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : L2VpCliShowPwRedGrpInfo
 *
 * DESCRIPTION      : This function displays the global pseudowire redundancy
 *                    related information
 *
 * INPUT            : CliHandle
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

INT4
L2VpCliShowPwRedGrpInfo (tCliHandle CliHandle, UINT4 u4PwRedGrpId)
{

    UINT4               u4PwRedIndex = 0;
    UINT4               u4NextRgIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1IsShowAll = OSIX_TRUE;
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    if (u4PwRedGrpId != 0)
    {
        pRgGroup = L2VpnGetRedundancyEntryByIndex (u4PwRedGrpId);

        if (pRgGroup == NULL)
        {
            return CLI_FAILURE;
        }
        u1IsShowAll = OSIX_FALSE;
        u4NextRgIndex = u4PwRedGrpId;
    }
    else
    {
        if (nmhGetFirstIndexFsL2VpnPwRedGroupTable (&u4PwRedIndex) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u1IsShowAll = OSIX_TRUE;
        u4NextRgIndex = u4PwRedIndex;
    }
    CliPrintf (CliHandle, "\r\nRedundancy Group Configurations\r\n");
    CliPrintf (CliHandle, "------------------------------------\r\n");

    do
    {
        L2VpnCliShowPwRedConfig (CliHandle, u4NextRgIndex, &u4PagingStatus);
        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }
        if (u4PagingStatus == CLI_FAILURE)
            break;
        u4PwRedIndex = u4NextRgIndex;
    }
    while ((nmhGetNextIndexFsL2VpnPwRedGroupTable (u4PwRedIndex, &u4NextRgIndex)
            == SNMP_SUCCESS));

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnCliShowPwRedConfig
 *
 * DESCRIPTION      : This function displays the configurations of a redundancy
 *                    group.
 *
 * INPUT            : u4RgId - Redundant Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
VOID
L2VpnCliShowPwRedConfig (tCliHandle CliHandle, UINT4 u4RgId,
                         UINT4 *pu4PagingStatus)
{
    tSNMP_OCTET_STRING_TYPE RgName;
    tSNMP_OCTET_STRING_TYPE DualHomeApps;
    INT4                i4PwRedGrpProtType = 0;
    INT4                i4PwRedGrpRevType = 0;
    INT4                i4PwRedContResMethod = 0;
    INT4                i4PwRedMasterSlaveMode = 0;
    INT4                i4PwRedLockoutProtectStatus = 0;
    INT4                i4PswRgRowStatus = 0;
    UINT4               u4OperActPw = 0;
    UINT4               u4PwRedGrpWtrTime = 0;
    UINT1               au1RgNameStr[L2VPN_PWRED_GRP_NAME_LEN + 1];
    UINT1               au1DualHomeApps[MPLS_ONE] = { 0 };

    nmhGetFsL2VpnPwRedGroupRowStatus (u4RgId, &i4PswRgRowStatus);

    nmhGetFsL2VpnPwRedGroupProtType (u4RgId, &i4PwRedGrpProtType);

    nmhGetFsL2VpnPwRedGroupReversionType (u4RgId, &i4PwRedGrpRevType);

    nmhGetFsL2VpnPwRedGroupContentionResolutionMethod (u4RgId,
                                                       &i4PwRedContResMethod);

    nmhGetFsL2VpnPwRedGroupLockoutProtection (u4RgId,
                                              &i4PwRedLockoutProtectStatus);

    nmhGetFsL2VpnPwRedGroupMasterSlaveMode (u4RgId, &i4PwRedMasterSlaveMode);

    RgName.pu1_OctetList = au1RgNameStr;
    RgName.i4_Length = 0;
    MEMSET (au1RgNameStr, 0, L2VPN_PWRED_GRP_NAME_LEN);
    DualHomeApps.pu1_OctetList = au1DualHomeApps;
    DualHomeApps.i4_Length = 0;
    MEMSET (au1DualHomeApps, 0, MPLS_ONE);

    nmhGetFsL2VpnPwRedGroupName (u4RgId, &RgName);
    nmhGetFsL2VpnPwRedGroupDualHomeApps (u4RgId, &DualHomeApps);

    nmhGetFsL2VpnPwRedGroupOperActivePw (u4RgId, &u4OperActPw);

    nmhGetFsL2VpnPwRedGroupWtrTimer (u4RgId, &u4PwRedGrpWtrTime);

    CliPrintf (CliHandle, "\r\nPW RED GROUP ID  : %u \r\n", u4RgId);

    if (RgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "PW RED Group Name : %s\r\n",
                   RgName.pu1_OctetList);
    }

    if (i4PwRedGrpProtType == L2VPNRED_MODE_BRIDGE)
    {
        CliPrintf (CliHandle, "PW Protection Type     : %s\r\n", "1+1");
    }
    else if (i4PwRedGrpProtType == L2VPNRED_MODE_SWITCH)
    {
        CliPrintf (CliHandle, "PW Protection Type     : %s\r\n", "1:1");
    }
    else
    {
        CliPrintf (CliHandle, "PW Protection Type     : %s\r\n", "1:N");
    }

    if (i4PwRedGrpRevType == L2VPN_PWRED_MODE_REVERTIVE)
    {
        CliPrintf (CliHandle, "PW Revertive Mode : %s\r\n", "Enabled");

    }
    else if (i4PwRedGrpRevType == L2VPN_PWRED_MODE_NON_REVERTIVE)
    {
        CliPrintf (CliHandle, "PW Revertive Mode : %s\r\n", "Disabled");
    }
    if (i4PwRedLockoutProtectStatus == L2VPNRED_LOCKOUT_ENABLE)
    {
        CliPrintf (CliHandle, "PW Lockout Mode : %s\r\n", "Enabled");

    }
    else if (i4PwRedLockoutProtectStatus == L2VPNRED_LOCKOUT_DISABLE)
    {
        CliPrintf (CliHandle, "PW Lockout Mode : %s\r\n", "Disabled");
    }

    if (i4PwRedContResMethod == L2VPNRED_CONTENTION_INDEPENDENT)
    {
        CliPrintf (CliHandle, "PW Contention Resolution Method: %s\r\n",
                   "Independent");
    }
    else if (i4PwRedContResMethod == L2VPNRED_CONTENTION_MASTERSLAVE)
    {
        CliPrintf (CliHandle, "PW Contention Resolution Method: %s\r\n",
                   "MasterSlave");
        if (i4PwRedMasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER)
        {
            CliPrintf (CliHandle, "PW Master Slave Mode: %s\r\n", "Master");
        }
        else if (i4PwRedMasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE)
        {
            CliPrintf (CliHandle, "PW Master Slave Mode: %s\r\n", "Slave");
        }
    }
    if (u4OperActPw != 0)
    {
        CliPrintf (CliHandle, "PW Currently Active pseudowire : %u\r\n",
                   u4OperActPw);
    }
    if (u4PwRedGrpWtrTime != 0)
    {
        CliPrintf (CliHandle, "WTR Timer Interval      : %u  minutes\r\n",
                   u4PwRedGrpWtrTime);
    }
    if (DualHomeApps.pu1_OctetList[0] & MPLS_L2VPNRED_MULTIHOMINGAPP_LAGG)
    {
        CliPrintf (CliHandle, "Dual Home Application: DLAG\r\n");
    }
    if (i4PswRgRowStatus != ACTIVE)
    {
        CliPrintf (CliHandle, "Redundancy Group Status : Not ACTIVE");
    }
    else
    {
        CliPrintf (CliHandle, "Redundancy Group Status : ACTIVE");

    }
    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    return;
}

/******************************************************************************
 * * Function Name : L2VpnPwVcDelBasedOnOwner
 * * Description   : This routine will delete the pseudowire based on the owner type
 * * Input(s)      : pL2vpnCliArgs - argument list
 * * Output(s)     : If Success -None
 * *                 or else Proper Error Message is printed.
 * * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
L2VpnPwVcDelBasedOnOwner (tCliHandle CliHandle,
                          tL2vpnCliArgs * pL2vpnCliArgs,
                          tPwVcEntry * pPwVcEntry)
{

    UINT4               u4PwIndex = 0;
    BOOL1               bFlag = FALSE;
    UINT1               u1Found = L2VPN_FALSE;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (pPwVcEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (pL2vpnCliArgs->u4Flag == 0)
    {
        CliPrintf (CliHandle, "\r%%PwVc entry Owner is not specified\r\n");
        return CLI_FAILURE;
    }

    u4PwIndex = pL2vpnCliArgs->u4PwIndex;

    if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
         || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry) ==
                (UINT2) pL2vpnCliArgs->u4VlanId)
            {
                u1Found = L2VPN_TRUE;
                break;
            }
        }
        if (u1Found == L2VPN_FALSE)
        {
            CliPrintf (CliHandle,
                       "\r%%Pw cannot be deleted in different vlan mode\r\n");
            return CLI_FAILURE;
        }
    }

    if ((pL2vpnCliArgs->i4PwType != 0) &&
        (L2VPN_PWVC_TYPE (pPwVcEntry) != (INT1) pL2vpnCliArgs->i4PwType))
    {
        CliPrintf (CliHandle, "\r%%Pw cannot be deleted in different mode\r\n");
        return CLI_FAILURE;
    }

    if (pL2vpnCliArgs->u4PwRedGrpId != 0)
    {
        if (L2VpCliDeleteRedPwEntry (CliHandle, pPwVcEntry->u4PwVcIndex,
                                     pL2vpnCliArgs, DESTROY) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to delete the Redundancy PW entry\r\n");
            return CLI_FAILURE;
        }
    }

    switch (pL2vpnCliArgs->u4Flag)
    {
        case CLI_L2VPN_PW_OWNER_MANUAL:
            /* PW Manual case, We need to have all the checks given below */
            if ((pPwVcEntry->u4InVcLabel == pL2vpnCliArgs->u4LocalLabel) &&
                (pPwVcEntry->u4OutVcLabel == pL2vpnCliArgs->u4RemoteLabel))
            {
                PwDestroy (CliHandle, u4PwIndex);
                bFlag = TRUE;
            }
            break;
        case CLI_L2VPN_PW_OWNER_PW_ID_FEC:
            /* PW PWId case, We need to have all the checks given below */
            if ((pPwVcEntry->u4PwVcID == pL2vpnCliArgs->u4PwId) &&
                (pPwVcEntry->u4LocalGroupID == pL2vpnCliArgs->u4LocalGroupId))
            {
                PwDestroy (CliHandle, u4PwIndex);
                bFlag = TRUE;
            }
            break;
        case CLI_L2VPN_PW_OWNER_GEN_FEC:
            /* Gen FEC AII Type 2 PW is a manual mode of PW for MPLS-TP */
            if ((L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE) &&
                (pL2vpnCliArgs->u4LocalLabel != 0) &&
                (pL2vpnCliArgs->u4RemoteLabel != 0))
            {
                if ((pPwVcEntry->u4InVcLabel != pL2vpnCliArgs->u4LocalLabel)
                    || (pPwVcEntry->u4OutVcLabel !=
                        pL2vpnCliArgs->u4RemoteLabel))
                {
                    break;
                }
            }

            if ((pL2vpnCliArgs->Agi.i4_Length != pPwVcEntry->u1AgiLen) ||
                (pL2vpnCliArgs->Saii.i4_Length != pPwVcEntry->u1SaiiLen) ||
                (pL2vpnCliArgs->Taii.i4_Length != pPwVcEntry->u1TaiiLen))
            {
                break;
            }
            if ((MEMCMP (pPwVcEntry->au1Agi,
                         pL2vpnCliArgs->Agi.pu1_OctetList,
                         pPwVcEntry->u1AgiLen) == 0) &&
                (MEMCMP (pPwVcEntry->au1Saii,
                         pL2vpnCliArgs->Saii.pu1_OctetList,
                         pPwVcEntry->u1SaiiLen) == 0) &&
                (MEMCMP (pPwVcEntry->au1Taii,
                         pL2vpnCliArgs->Taii.pu1_OctetList,
                         pPwVcEntry->u1TaiiLen) == 0))
            {
                PwDestroy (CliHandle, u4PwIndex);
                bFlag = TRUE;
            }
            break;

        default:
            break;
    }
    if (bFlag == FALSE)
    {
        CliPrintf (CliHandle, "\r%%Pseudowire Entry not found\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliCreateRedPwEntry
* Description   : This routine will create the redundancy pseudowire entry
* Input(s)      : u4PwIndex - Index for the row identifying a PW
*                                          in the pwTable
*                 pL2vpnCliArgs - argument list
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliCreateRedPwEntry (tCliHandle CliHandle, UINT4 u4PwIndex,
                         tL2vpnCliArgs * pL2vpnCliArgs)
{
    UINT4               u4Errorcode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4Errorcode,
                                          pL2vpnCliArgs->u4PwRedGrpId,
                                          u4PwIndex,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedPwRowStatus (pL2vpnCliArgs->u4PwRedGrpId, u4PwIndex,
                                       CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4Errorcode,
                                          pL2vpnCliArgs->u4PwRedGrpId,
                                          u4PwIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsL2VpnPwRedPwRowStatus (pL2vpnCliArgs->u4PwRedGrpId,
                                       u4PwIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliDeleteRedPwEntry
* Description   : This routine will delete the redundancy pseudowire entry.
* Input(s)      : u4PwIndex - Index for the row identifying a PW
*                                          in the pwTable
*                 pL2vpnCliArgs - argument list
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliDeleteRedPwEntry (tCliHandle CliHandle, UINT4 u4PwIndex,
                         tL2vpnCliArgs * pL2vpnCliArgs, UINT1 u1RedPwRowStatus)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4ErrorCode,
                                          pL2vpnCliArgs->u4PwRedGrpId,
                                          u4PwIndex,
                                          u1RedPwRowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (SNMP_FAILURE ==
        nmhSetFsL2VpnPwRedPwRowStatus
        (pL2vpnCliArgs->u4PwRedGrpId, u4PwIndex, u1RedPwRowStatus))
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliCheckIfPwIsBackup
* Description   : This routine will check whether the PW is for backup or not
* Input(s)      : u4PwEnetPortVlan - Vlan with which this pseudowire is
*                                     associated
*                 i4IfIndex - Port index
* Output(s)     : pu4PwIndex - pointer to the Pw Index
* Return(s)     : Returns CLI_FAILURE if it is not the Back-Up else CLI_SUCCESS
*******************************************************************************/
INT4
L2VpCliCheckIfPwIsBackup (UINT4 u4PwEnetPortVlan, INT4 i4PortIfIndex,
                          UINT4 *pu4PwIndex)
{
    UINT4               u4PwIndex = L2VPN_ONE;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    for (; u4PwIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
        if ((pPwVcEntry == NULL) ||
            (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL))
        {
            continue;
        }

        *pu4PwIndex = pPwVcEntry->u4PwVcIndex;
        /* Check if a pwEntry already exists for the same vlan /port */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) != L2VPN_PWVC_ACTIVE)
            {
                continue;
            }
            if ((u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN) &&
                (i4PortIfIndex != L2VPN_ZERO))
            {
                if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                     u4PwEnetPortVlan) &&
                    (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                     i4PortIfIndex))
                {
                    return CLI_FAILURE;
                }
            }
            else if (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
            {
                if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                    u4PwEnetPortVlan)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                    i4PortIfIndex)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnConfigPwPref
* Description   : This routine configures the preference for the pseudowire 
*
* Input(s)      : CliHandle             - Context in which the command is
*                                         processed
*                 pL2vpnCliArgs         - A pointer to a structure that contains
*                                        parameters like pw mpls type, pw type,
*                                        peer addr, pw mode etc., that should be
*                                        set while creating a pseudowire.
* Output(s)     : If Success -None
*                 or else Proper Error Message is printed.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/

INT4
L2VpnConfigPwPref (tCliHandle CliHandle, tL2vpnCliArgs * pL2vpnCliArgs)
{
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4Errorcode = 0;
    UINT4               u4PwIndex = 0;
    UINT1               u1Found = L2VPN_FALSE;
    INT4                i4RetStatus;

    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pL2vpnCliArgs->u4PwId);
    if (pPwVcEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%%PwVc entry does not exist\r\n");
        return CLI_FAILURE;
    }
    if ((pL2vpnCliArgs->u4VplsIndex != L2VPN_ZERO) &&
        (pL2vpnCliArgs->u4VplsIndex != L2VPN_PWVC_VPLS_INDEX (pPwVcEntry)))

    {
        CliPrintf (CliHandle,
                   "\r%%PW is not present in current VFI context\r\n");
        return CLI_FAILURE;
    }
    u4PwIndex = pPwVcEntry->u4PwVcIndex;

    if (pL2vpnCliArgs->u1PwStatus != 0)
    {
#ifdef HVPLS_WANTED
        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
        {
#endif
            if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
                 || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
                && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
            {
                TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                              pPwVcEnetEntry, tPwVcEnetEntry *)
                {
                    if ((pL2vpnCliArgs->i4PwType ==
                         CLI_L2VPN_PW_TYPE_ETH_TAGGED)
                        && (L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry) ==
                            (UINT2) pL2vpnCliArgs->u4VlanId))
                    {
                        u1Found = L2VPN_TRUE;
                        break;
                    }

                    else if ((pL2vpnCliArgs->i4PwType == CLI_L2VPN_PW_TYPE_ETH)
                             && (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)
                                 == pL2vpnCliArgs->i4PortIfIndex))
                    {
                        u1Found = L2VPN_TRUE;
                        break;
                    }
                }
                if (u1Found == L2VPN_FALSE)
                {
                    CliPrintf (CliHandle,
                               "\r%%Redundancy PW status cannot be changed in "
                               "different vlan mode\r\n");
                    return CLI_FAILURE;
                }
            }
#ifdef HVPLS_WANTED
        }
#endif
        if (pL2vpnCliArgs->u1PwStatus == L2VPN_CLI_PW_ACTIVATE)
        {
            if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4Errorcode,
                                                  pL2vpnCliArgs->u4PwRedGrpId,
                                                  u4PwIndex,
                                                  ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to activate the Redundancy PW\r");
                return CLI_FAILURE;
            }
            if (nmhSetFsL2VpnPwRedPwRowStatus (pL2vpnCliArgs->u4PwRedGrpId,
                                               u4PwIndex,
                                               ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to activate the Redundancy PW\r");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2FsL2VpnPwRedPwRowStatus (&u4Errorcode,
                                                  pL2vpnCliArgs->u4PwRedGrpId,
                                                  u4PwIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to de-activate the Redundancy PW\r");
                return CLI_FAILURE;
            }
            if (nmhSetFsL2VpnPwRedPwRowStatus (pL2vpnCliArgs->u4PwRedGrpId,
                                               u4PwIndex, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to de-activate the Redundancy PW\r");
                return CLI_FAILURE;
            }
        }
    }
    else if (pL2vpnCliArgs->u1RedPwPreference != 0)
    {
        if (SNMP_FAILURE ==
            nmhTestv2FsL2VpnPwRedPwPreferance (&u4Errorcode,
                                               pL2vpnCliArgs->u4PwRedGrpId,
                                               u4PwIndex,
                                               pL2vpnCliArgs->
                                               u1RedPwPreference))
        {
            CliPrintf (CliHandle, "\r%%Unable to configure preference\r\n");
            return CLI_FAILURE;
        }

        if (SNMP_FAILURE ==
            nmhSetFsL2VpnPwRedPwPreferance
            (pL2vpnCliArgs->u4PwRedGrpId, u4PwIndex,
             pL2vpnCliArgs->u1RedPwPreference))
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (pL2vpnCliArgs->i4HoldingPriority != 0)
    {
        if (SNMP_FAILURE ==
            nmhTestv2PwRowStatus (&u4Errorcode, u4PwIndex, NOT_IN_SERVICE))
        {
            return SNMP_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetPwRowStatus (u4PwIndex, NOT_IN_SERVICE))
        {

            return SNMP_FAILURE;
        }
        i4RetStatus =
            L2VpCliSetHoldingPriority (&u4Errorcode, u4PwIndex,
                                       pL2vpnCliArgs->i4HoldingPriority);
        if (SNMP_FAILURE == i4RetStatus)
        {

            if (u4Errorcode == SNMP_ERR_WRONG_VALUE)
            {
                CliPrintf (CliHandle,
                           "\r%%Wrong value passed for holdingpriority\r\n");
            }
            CliPrintf (CliHandle, "\r%%Unable to configure priority\r\n");
        }
        if (SNMP_FAILURE ==
            nmhTestv2PwRowStatus (&u4Errorcode, u4PwIndex, ACTIVE))
        {
            i4RetStatus = SNMP_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetPwRowStatus (u4PwIndex, ACTIVE))
        {
            i4RetStatus = SNMP_FAILURE;
        }
        if (SNMP_FAILURE == i4RetStatus)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliSetHoldingPriority
* Description   : This routine configures the holding priority
*
* Input(s)      : u4PwIndex         -PW index of PW whose holding priority is to set
*                 i4HoldingPriority -Holding priority value
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/

INT4
L2VpCliSetHoldingPriority (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                           INT4 i4HoldingPriority)
{

    if (SNMP_FAILURE ==
        nmhTestv2PwHoldingPriority (pu4ErrorCode, u4PwIndex, i4HoldingPriority))
    {
        return SNMP_FAILURE;
    }
    if (SNMP_FAILURE == nmhSetPwHoldingPriority (u4PwIndex, i4HoldingPriority))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliShowPwRedPwInfo 
* Description   : This routine displays the redundant Pseudowires information
*                 configured in the RG group
*
* Input(s)      : CliHandle      - Context in which the command is processed
*                 pu4GroupId     - pointer to the group Id
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliShowPwRedPwInfo (tCliHandle CliHandle, UINT4 *pu4GroupId)
{
    tL2vpnRedundancyPwEntry RgPwEntry;
    tL2vpnRedundancyPwEntry *pPwEntry = NULL;

    MEMSET (&RgPwEntry, 0, sizeof (tL2vpnRedundancyPwEntry));

    CliPrintf (CliHandle, "\r\nRedundancy Pseudowire Configurations\r\n");
    CliPrintf (CliHandle, "------------------------------------\r\n");

    if (pu4GroupId != NULL)
    {
        RgPwEntry.u4RgIndex = *pu4GroupId;
        do
        {
            CliPrintf (CliHandle, "\r\n");
            pPwEntry = RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                                      (tRBElem *) & RgPwEntry, NULL);

            if ((pPwEntry == NULL) || (pPwEntry->u4RgIndex != *pu4GroupId))
            {
                break;
            }
            L2VpCliDisplayRedPwInfo (CliHandle, pPwEntry);
            RgPwEntry.u4PwIndex = pPwEntry->u4PwIndex;
        }
        while (1);
    }
    else
    {
        do
        {
            CliPrintf (CliHandle, "\r\n");
            pPwEntry =
                RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                               (tRBElem *) & RgPwEntry, NULL);
            if (pPwEntry == NULL)
            {
                break;
            }
            L2VpCliDisplayRedPwInfo (CliHandle, pPwEntry);
            RgPwEntry.u4PwIndex = pPwEntry->u4PwIndex;
            RgPwEntry.u4RgIndex = pPwEntry->u4RgIndex;
        }
        while (1);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliShowPwRedNbrnfo 
* Description   : This routine displays the neighbor node information
*                 configured in the RG group
*
* Input(s)      : CliHandle      - Context in which the command is processed
*                 pu4GroupId     - pointer to the group Id
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliShowPwRedNbrInfo (tCliHandle CliHandle, UINT4 *pu4GroupId)
{
    tL2vpnRedundancyNodeEntry RgNodeEntry;
    tL2vpnRedundancyNodeEntry *pNodeEntry = NULL;

    MEMSET (&RgNodeEntry, 0, sizeof (tL2vpnRedundancyNodeEntry));

    CliPrintf (CliHandle, "\r\nPW-RED Neighbor Details\r\n");
    CliPrintf (CliHandle, "-----------------------\r\n");

    if (pu4GroupId != NULL)
    {
        RgNodeEntry.u4RgIndex = *pu4GroupId;
        do
        {
            CliPrintf (CliHandle, "\r\n");
            pNodeEntry = RBTreeGetNext (gL2vpnRgGlobals.RgNodeList,
                                        (tRBElem *) & RgNodeEntry, NULL);

            if ((pNodeEntry == NULL) || (pNodeEntry->u4RgIndex != *pu4GroupId))
            {
                break;
            }
            L2VpCliDisplayPwRedNbrInfo (CliHandle, pNodeEntry);
            RgNodeEntry.u1AddrType = pNodeEntry->u1AddrType;
            MEMCPY (&RgNodeEntry.Addr, &pNodeEntry->Addr, 4);
        }
        while (1);
    }
    else
    {
        do
        {
            CliPrintf (CliHandle, "\r\n");
            pNodeEntry =
                RBTreeGetNext (gL2vpnRgGlobals.RgNodeList,
                               (tRBElem *) & RgNodeEntry, NULL);
            if (pNodeEntry == NULL)
            {
                break;
            }
            L2VpCliDisplayPwRedNbrInfo (CliHandle, pNodeEntry);
            RgNodeEntry.u4RgIndex = pNodeEntry->u4RgIndex;
            RgNodeEntry.u1AddrType = pNodeEntry->u1AddrType;
            MEMCPY (&RgNodeEntry.Addr, &pNodeEntry->Addr, 4);
        }
        while (1);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliShowPwRedIccpPwInfo 
* Description   : This routine displays the ICCP pseudowire information
*
* Input(s)      : CliHandle      - Context in which the command is processed
*                 pu4GroupId     - pointer to the group Id
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
L2VpCliShowPwRedIccpPwInfo (tCliHandle CliHandle, UINT4 *pu4GroupId)
{
    tL2vpnIccpPwEntry   IccpPwEntry;
    tL2vpnIccpPwEntry  *pIccpPwEntry = NULL;

    MEMSET (&IccpPwEntry, 0, sizeof (tL2vpnIccpPwEntry));

    CliPrintf (CliHandle, "\r\nICCP Pseudowire Details\r\n");
    CliPrintf (CliHandle, "-----------------------\r\n");

    if (pu4GroupId != NULL)
    {
        IccpPwEntry.u4RgIndex = *pu4GroupId;
        pIccpPwEntry = &IccpPwEntry;
        do
        {
            CliPrintf (CliHandle, "\r\n");
            pIccpPwEntry = RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                                          (tRBElem *) pIccpPwEntry, NULL);

            if ((pIccpPwEntry == NULL)
                || (pIccpPwEntry->u4RgIndex != *pu4GroupId))
            {
                break;
            }
            L2VpCliDisplayIccpPwInfo (CliHandle, pIccpPwEntry);
        }
        while (1);
    }
    else
    {
        pIccpPwEntry = &IccpPwEntry;
        do
        {
            CliPrintf (CliHandle, "\r\n");
            pIccpPwEntry =
                RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                               (tRBElem *) pIccpPwEntry, NULL);
            if (pIccpPwEntry == NULL)
            {
                break;
            }
            L2VpCliDisplayIccpPwInfo (CliHandle, pIccpPwEntry);
        }
        while (1);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpCliDisplayRedPwInfo 
* Description   : This routine displays the Redundant pseudowire information
*
* Input(s)      : CliHandle      - Context in which the command is processed
*                 pRgPwEntry     - pointer to Redundant PW entry to be displayed
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpCliDisplayRedPwInfo (tCliHandle CliHandle,
                         tL2vpnRedundancyPwEntry * pRgPwEntry)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    UINT1               au1Status[90];
    INT4                i4OperStatus = 0;

    MEMSET (au1Status, 0, sizeof (au1Status));
    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgPwEntry->u4RgIndex);
    if (pRgGroup != NULL)
    {
        CliPrintf (CliHandle, "\rRG GROUP ID : %u \n", pRgPwEntry->u4RgIndex);
        CliPrintf (CliHandle, "\rRG PW Index : %u \n", pRgPwEntry->u4PwIndex);

        if (pRgPwEntry->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY)
        {
            if (pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE)
            {
                CliPrintf (CliHandle,
                           "\rRG PW Preference : PRIMARY(LOCKOUT) \n");
            }
            else
            {
                CliPrintf (CliHandle, "\rRG PW Preference : PRIMARY \n");
            }
        }
        else
        {
            if (pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE)
            {
                CliPrintf (CliHandle,
                           "\rRG PW Preference : SECONDARY(LOCKOUT) \n");
            }
            else
            {
                CliPrintf (CliHandle, "\rRG PW Preference : SECONDARY \n");
            }
        }
        if (pRgPwEntry->pIccpPwEntry != NULL)
        {
            if (pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE)
            {
                L2VpCliShowRedPwStatus (pRgPwEntry->pIccpPwEntry->u4LocalStatus,
                                        au1Status);
                CliPrintf (CliHandle, "\rRG PW Local Status : %s(LOCKOUT)  \n",
                           au1Status);

                MEMSET (au1Status, 0, sizeof (au1Status));
                L2VpCliShowRedPwStatus (pRgPwEntry->pIccpPwEntry->
                                        u4RemoteStatus, au1Status);
                CliPrintf (CliHandle, "\rRG PW Remote Status : %s(LOCKOUT) \n",
                           au1Status);
            }
            else
            {
                L2VpCliShowRedPwStatus (pRgPwEntry->pIccpPwEntry->
                                        u4LocalStatus |
                                        (L2VPN_PWVC_STATUS_STANDBY &
                                         pRgPwEntry->pIccpPwEntry->
                                         u4RemoteStatus) | pRgPwEntry->
                                        pPwVcEntry->u1LocalStatus, au1Status);
                CliPrintf (CliHandle, "\rRG PW Local Status : %s \n",
                           au1Status);

                MEMSET (au1Status, 0, sizeof (au1Status));
                L2VpCliShowRedPwStatus ((pRgPwEntry->pIccpPwEntry->
                                         u4LocalStatus &
                                         L2VPN_PWVC_STATUS_STANDBY) |
                                        pRgPwEntry->pIccpPwEntry->
                                        u4RemoteStatus | pRgPwEntry->
                                        pPwVcEntry->u1RemoteStatus, au1Status);
                CliPrintf (CliHandle, "\rRG PW Remote Status : %s \n",
                           au1Status);
            }
        }
        nmhGetFsL2VpnPwRedPwOperStatus (pRgPwEntry->u4RgIndex,
                                        pRgPwEntry->u4PwIndex, &i4OperStatus);

        CliPrintf (CliHandle, "\rRG PW Oper Status : %s \n",
                   CLI_L2VPN_SHOW_PW_OPER_STATUS (i4OperStatus));
    }
}

/******************************************************************************
* Function Name : L2VpCliShowRedPwStatus 
* Description   : This routine to set the corresponding string to be dispalyed 
*                 for PW Local/Remote Status
*
* Input(s)      : u4Status      - Local/remote status
* Output(s)     : pu1Status     - pointer to the array containing the string
* Return(s)     : None
*******************************************************************************/
VOID
L2VpCliShowRedPwStatus (UINT4 u4Status, UINT1 *pu1Status)
{
    if (u4Status & L2VPN_PWVC_STATUS_NOT_FORWARDING)
    {
        STRNCAT (pu1Status, "NOT_FWD", STRLEN ("NOT_FWD"));
    }
    if (u4Status & L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT)
    {
        STRNCAT (pu1Status, "CUST_RX_FAULT/", STRLEN ("CUST_RX_FAULT/"));
    }
    if (u4Status & L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT)
    {
        STRNCAT (pu1Status, "CUST_TX_FAULT/", STRLEN ("CUST_TX_FAULT/"));
    }
    if (u4Status & L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT)
    {
        STRNCAT (pu1Status, "PSN_RX_FAULT/", STRLEN ("PSN_RX_FAULT/"));
    }
    if (u4Status & L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT)
    {
        STRNCAT (pu1Status, "PSN_TX_FAULT/", STRLEN ("PSN_TX_FAULT/"));
    }
    if (u4Status & L2VPN_PWVC_STATUS_STANDBY)
    {
        STRNCAT (pu1Status, "STANDBY", STRLEN ("STANDBY"));
    }
    if (u4Status & L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST)
    {
        STRNCAT (pu1Status, "SWITCHOVER_RQST", STRLEN ("SWITCHOVER_RQST"));
    }
    if (u4Status == L2VPN_ZERO)
    {
        STRNCAT (pu1Status, "UP", STRLEN ("UP"));
    }
}

/******************************************************************************
* Function Name : L2VpCliDisplayPwRedNbrInfo 
* Description   : This routine displays the PW-RED Neighbor information
*
* Input(s)      : CliHandle      - Context in which the command is processed
*                 pRgNodeEntry   - pointer to neighbor entry to be displayed
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpCliDisplayPwRedNbrInfo (tCliHandle CliHandle,
                            tL2vpnRedundancyNodeEntry * pRgNodeEntry)
{
    UINT1              *pu1Addr = NULL;
    UINT1               au1Status[80];
    UINT2               u2BufferLen = 0;

    MEMSET (au1Status, 0, sizeof (au1Status));

    CliPrintf (CliHandle, "\rRG GROUP ID : %u \n", pRgNodeEntry->u4RgIndex);

    pu1Addr = pRgNodeEntry->Addr.au1Ipv4Addr;
    CliPrintf (CliHandle, "\rNeighbor Addr : %d.%d.%d.%d \n", pu1Addr[0],
               pu1Addr[1], pu1Addr[2], pu1Addr[3]);

    if (pRgNodeEntry->pSession != NULL)
    {
        CliPrintf (CliHandle, "\rLocal LDP ID : %u \n",
                   pRgNodeEntry->pSession->u4LocalLdpEntityID);
        CliPrintf (CliHandle, "\rLocal LDP Entity Index : %u \n",
                   pRgNodeEntry->pSession->u4LdpEntityIndex);
    }

    if (pRgNodeEntry->u1Status == 0)
    {
        u2BufferLen = (UINT2) (sizeof (au1Status) - STRLEN (au1Status));
        STRNCAT (au1Status, "DISCONNECTED/",
                 (u2BufferLen <
                  STRLEN ("DISCONNECTED/") ? u2BufferLen :
                  STRLEN ("DISCONNECTED/")));
    }
    if (pRgNodeEntry->u1Status & L2VPNRED_NODE_STATUS_CONNECTED)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "CONNECTED/",
                 (u2BufferLen <
                  STRLEN ("CONNECTED/") ? u2BufferLen : STRLEN ("CONNECTED/")));
    }
    if (pRgNodeEntry->u1Status & L2VPNRED_NODE_STATUS_LOCAL_SYNC)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "LOCAL-SYNC/",
                 (u2BufferLen <
                  STRLEN ("LOCAL-SYNC/") ? u2BufferLen :
                  STRLEN ("LOCAL-SYNC/")));
    }
    if (pRgNodeEntry->u1Status & L2VPNRED_NODE_STATUS_CONFIG_AWAITED)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "CONFIG-AWAITED/",
                 (u2BufferLen <
                  STRLEN ("CONFIG-AWAITED/") ? u2BufferLen :
                  STRLEN ("CONFIG-AWAITED/")));
    }
    if (pRgNodeEntry->u1Status & L2VPNRED_NODE_STATUS_STATE_AWAITED)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "STATE-AWAITED/",
                 (u2BufferLen <
                  STRLEN ("STATE-AWAITED/") ? u2BufferLen :
                  STRLEN ("STATE-AWAITED/")));
    }
    if (pRgNodeEntry->u1Status & L2VPNRED_NODE_STATUS_REMOTE_SYNC)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "REMOTE-SYNC/",
                 (u2BufferLen <
                  STRLEN ("REMOTE-SYNC/") ? u2BufferLen :
                  STRLEN ("REMOTE-SYNC/")));
    }
    CliPrintf (CliHandle, "\rNeighbor Node Status : %s \n", au1Status);
}

/******************************************************************************
* Function Name : L2VpCliDisplayIccpPwInfo 
* Description   : This routine displays the ICCP Pseudowire information
*
* Input(s)      : CliHandle      - Context in which the command is processed
*                 pIccpPwEntry   - pointer to Iccp Pw entry to be displayed
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpCliDisplayIccpPwInfo (tCliHandle CliHandle,
                          tL2vpnIccpPwEntry * pIccpPwEntry)
{
    UINT1              *pu1Addr = NULL;
    UINT1               au1Status[110];
    UINT4               u4Id = 0;
    UINT4               u4PwId = 0;
    UINT2               u2BufferLen = 0;

    MEMSET (au1Status, 0, sizeof (au1Status));

    CliPrintf (CliHandle, "\rRG GROUP ID : %u \n", pIccpPwEntry->u4RgIndex);

    pu1Addr = pIccpPwEntry->RouterId.au1Ipv4Addr;
    CliPrintf (CliHandle, "\rIccp-Pw LSR ID : %d.%d.%d.%d \n", pu1Addr[0],
               pu1Addr[1], pu1Addr[2], pu1Addr[3]);

    if (pIccpPwEntry->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
    {
        pu1Addr = pIccpPwEntry->Fec.u.Fec128.au1PeerId;
        CliPrintf (CliHandle, "\rIccp-Pw Remote LSR ID : %d.%d.%d.%d \n",
                   pu1Addr[0], pu1Addr[1], pu1Addr[2], pu1Addr[3]);

        CliPrintf (CliHandle, "\rIccp-Pw Fec Type : FEC 128 \n");

        MEMCPY (&u4Id, &pIccpPwEntry->Fec.u.Fec128.au1GroupId, 4);
        CliPrintf (CliHandle, "\rIccp-Pw Group Id : %u \n",
                   (OSIX_HTONL (u4Id)));

        MEMCPY (&u4PwId, &pIccpPwEntry->Fec.u.Fec128.au1PwId, 4);
        CliPrintf (CliHandle, "\rIccp-Pw Id : %u \n", (OSIX_HTONL (u4PwId)));
    }
    else if (pIccpPwEntry->Fec.u1Type == L2VPN_FEC_GEN_TYPE)
    {
        CliPrintf (CliHandle, "\rIccp-Pw Fec Type : GEN FEC \n");

        if (pIccpPwEntry->Fec.u.Fec129.Agi.u1Type == L2VPN_GEN_FEC_AGI_TYPE_1)
        {
            CliPrintf (CliHandle, "\rIccp-Pw AGI Type : TYPE 1\n");
            CliPrintf (CliHandle, "\rIccp-Pw AGI value : %s \n",
                       pIccpPwEntry->Fec.u.Fec129.Agi.u.au1Agi1);
        }
        else
        {
            CliPrintf (CliHandle, "\rIccp-Pw AGI Type : TYPE 2\n");
        }

        if (pIccpPwEntry->Fec.u.Fec129.SAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_1)
        {
            CliPrintf (CliHandle, "\rIccp-Pw SAII Type : TYPE 1\n");
            CliPrintf (CliHandle, "\rIccp-Pw SAII Value : %s \n",
                       pIccpPwEntry->Fec.u.Fec129.SAii.u.au1Aii1);
        }
        else if (pIccpPwEntry->Fec.u.Fec129.SAii.u1Type ==
                 L2VPN_GEN_FEC_AII_TYPE_2)
        {
            CliPrintf (CliHandle, "\rIccp-Pw SAII Type : TYPE 2\n");
            CliPrintf (CliHandle, "\rIccp-Pw SAII Value : %s \n",
                       pIccpPwEntry->Fec.u.Fec129.SAii.u.au1Aii2);
        }
        if (pIccpPwEntry->Fec.u.Fec129.TAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_1)
        {
            CliPrintf (CliHandle, "\rIccp-Pw TAII Type : TYPE 1\n");
            CliPrintf (CliHandle, "\rIccp-Pw TAII Value : %s \n",
                       pIccpPwEntry->Fec.u.Fec129.TAii.u.au1Aii1);
        }
        else if (pIccpPwEntry->Fec.u.Fec129.TAii.u1Type ==
                 L2VPN_GEN_FEC_AII_TYPE_2)
        {
            CliPrintf (CliHandle, "\rIccp-Pw TAII Type : TYPE 2\n");
            CliPrintf (CliHandle, "\rIccp-Pw TAII Value : %s \n",
                       pIccpPwEntry->Fec.u.Fec129.TAii.u.au1Aii2);
        }
    }
    CliPrintf (CliHandle, "\rIccp-Pw Priority : %d \n",
               pIccpPwEntry->u2Priority);

    MEMCPY (&u4Id, &pIccpPwEntry->RoId.au1NodeId, 4);
    MEMCPY (&u4PwId, &pIccpPwEntry->RoId.au1PwIndex, 4);
    CliPrintf (CliHandle,
               "\rIccp-Pw ROID : Node-Id - %d.%d.%d.%d Pw-Index - %d\n",
               pIccpPwEntry->RoId.au1NodeId[0], pIccpPwEntry->RoId.au1NodeId[1],
               pIccpPwEntry->RoId.au1NodeId[2], pIccpPwEntry->RoId.au1NodeId[3],
               (OSIX_HTONL (u4PwId)));

    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_LOCAL_FORWARD)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "LOCAL-FWD/",
                 (u2BufferLen <
                  STRLEN ("LOCAL-FWD/") ? u2BufferLen : STRLEN ("LOCAL-FWD/")));
    }
    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_LOCAL_STANDBY)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "LOCAL-STANDBY/",
                 (u2BufferLen <
                  STRLEN ("LOCAL-STANDBY/") ? u2BufferLen :
                  STRLEN ("LOCAL-STANDBY/")));
    }
    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_LOCAL_SWITCHOVER)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "LOCAL-SWITCHOVER/",
                 (u2BufferLen <
                  STRLEN ("LOCAL-SWITCHOVER/") ? u2BufferLen :
                  STRLEN ("LOCAL-SWITCHOVER/")));
    }
    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_REMOTE_SWITCHOVER)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "REMOTE-SWITCHOVER/",
                 (u2BufferLen <
                  STRLEN ("REMOTE-SWITCHOVER/") ? u2BufferLen :
                  STRLEN ("REMOTE-SWITCHOVER/")));
    }
    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_REMOTE_AWAITED)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "REMOTE-AWAITED/",
                 (u2BufferLen <
                  STRLEN ("REMOTE-AWAITED/") ? u2BufferLen :
                  STRLEN ("REMOTE-AWAITED/")));
    }
    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_NODE_SWITCHOVER)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "NODE-SWITCHOVER/",
                 (u2BufferLen <
                  STRLEN ("NODE-SWITCHOVER/") ? u2BufferLen :
                  STRLEN ("NODE-SWITCHOVER/")));
    }
    if (pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED)
    {
        u2BufferLen = sizeof (au1Status) - STRLEN (au1Status);
        STRNCAT (au1Status, "LOCAL-UPDATED/",
                 (u2BufferLen <
                  STRLEN ("LOCAL-UPDATED/") ? u2BufferLen :
                  STRLEN ("LOCAL-UPDATED/")));
    }
    CliPrintf (CliHandle, "\rIccp-Pw Status : %s \n", au1Status);
}

/******************************************************************************
* Function Name : L2VpnCliValidateAC
* Description   : This routine will validate whether the Pseudowire created 
*                 deleted in the same vlan.
*
* Input(s)      :pPwVcEntry            -  pointer to a structure that contains
*                                         parameters of Pseudowire created.
*
*                pL2vpnCliArgs         - A pointer to a structure that contains 
*                                        parameters like pw mpls type, pw type, 
*                                        peer addr, pw mode etc., that should be 
*                                        set while creating a pseudowire.
* Output(s)     : None
* Return(s)     : L2VPN_TRUE  - Pseudowire exists in same VLAN
*                 L2VPN_FALSE - Pseudowire not exist in VLAN configured.
*******************************************************************************/
UINT1
L2VpnCliValidateAC (tPwVcEntry * pPwVcEntry, tL2vpnCliArgs * pL2vpnCliArgs)
{
    UINT1               u1Found = L2VPN_FALSE;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
         || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (((UINT2) pL2vpnCliArgs->u4VlanId ==
                 L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                && (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                    L2VPN_PWVC_ENET_DEF_PORT_VLAN))
            {
                pL2vpnCliArgs->u4VlanId =
                    L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
            }

            if ((L2VPN_PWVC_TYPE (pPwVcEntry)
                 == L2VPN_PWVC_TYPE_ETH_VLAN) &&
                ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                  (UINT2) pL2vpnCliArgs->u4VlanId)
                 || (pL2vpnCliArgs->u4Flag ==
                     CLI_MPLS_VPWS_PORT_BASED_COMMAND)))
            {
                u1Found = L2VPN_TRUE;
                break;
            }
            else if ((L2VPN_PWVC_TYPE (pPwVcEntry)
                      == L2VPN_PWVC_TYPE_ETH) &&
                     (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)
                      == pL2vpnCliArgs->i4PortIfIndex))
            {
                u1Found = L2VPN_TRUE;
                break;
            }
            /* For VPLS Psuedowire check whether the mode is VPLS */
            else if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
            {
                u1Found = L2VPN_TRUE;
                break;
            }
        }
    }
    return u1Found;
}

/******************************************************************************
* Function Name : L2VpnCliValidatePwVcEntry
 Description   : This routine will validate whether the Pseudowire is already
*                 created or not.
*
* Input(s)      : pL2vpnCliArgs         - A pointer to a structure that contains 
*                                        parameters like pw mpls type, pw type, 
*                                        peer addr, pw mode etc., that should be 
*                                        set while creating a pseudowire.
* Output(s)     : None
* Return(s)     : L2VPN_TRUE  - If matching Pseudowire exists.
*                 L2VPN_FALSE - If matching Pseudowire does not exist.
*******************************************************************************/

UINT4
L2VpnCliValidatePwVcEntry (tL2vpnCliArgs * pL2vpnCliArgs)
{
    UINT4               u4PwIndex = L2VPN_ZERO;
    UINT4               u4SrcGlobalId = L2VPN_ZERO;
    UINT4               u4SrcNodeId = L2VPN_ZERO;
    UINT4               u4SrcAcId = L2VPN_ZERO;
    UINT4               u4DestGlobalId = L2VPN_ZERO;
    UINT4               u4DestNodeId = L2VPN_ZERO;
    UINT4               u4DestAcId = L2VPN_ZERO;
    UINT1               u1IsSrcDstSame = L2VPN_FALSE;
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tGenU4Addr          PeerAddr;
    tGenU4Addr          GenU4Addr;
    static UINT1        au1AgiVpn[L2VPN_PWVC_MAX_LEN];

    MEMSET (&PeerAddr, 0, sizeof (tGenU4Addr));
    MEMSET (&GenU4Addr, 0, sizeof (tGenU4Addr));
    MEMSET (au1AgiVpn, L2VPN_ZERO, L2VPN_PWVC_MAX_LEN);
    switch (pL2vpnCliArgs->u4PwOwner)
    {
        case CLI_L2VPN_PW_OWNER_MANUAL:

            pPwVcEntry = L2VpnGetPwVcEntryFromLabel
                (pL2vpnCliArgs->u4LocalLabel, L2VPN_ZERO);

            if (pPwVcEntry != NULL)
            {
#ifdef MPLS_IPV6_WANTED

                if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
                {
                    if (L2VPN_ZERO ==
                        (MEMCMP
                         (&pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                          &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr,
                          IPV6_ADDR_LENGTH)))
                    {
                        if (pL2vpnCliArgs->u4RemoteLabel ==
                            pPwVcEntry->u4OutVcLabel)
                        {
                            return L2VPN_TRUE;
                        }
                    }
                }

                else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
                {
                    MEMCPY ((&PeerAddr.Addr.u4Addr),
                            &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                            IPV4_ADDR_LENGTH);
                    PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
                    if ((PeerAddr.Addr.u4Addr == pL2vpnCliArgs->PeerAddr.u4Addr)
                        && (pL2vpnCliArgs->u4RemoteLabel ==
                            pPwVcEntry->u4OutVcLabel))
                    {
                        return L2VPN_TRUE;
                    }

                }
            }
            else
            {
                return L2VPN_FALSE;
            }

            break;

        case CLI_L2VPN_PW_OWNER_PW_ID_FEC:
            /* If PWID case, We need to check PWID and Group ID */
            pPwVcEntry = L2VpnGetPwVcEntryFromPwId (pL2vpnCliArgs->u4PwId,
                                                    (INT1) pL2vpnCliArgs->
                                                    i4PwType);

            if (pPwVcEntry != NULL)
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
                {
                    if (L2VPN_ZERO ==
                        MEMCMP (&pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                                &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr,
                                IPV6_ADDR_LENGTH))

                    {
                        if ((L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry) ==
                             pL2vpnCliArgs->u4LocalGroupId))
                        {
                            return L2VPN_TRUE;
                        }
                    }
                }

                else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
                {
                    MEMCPY ((&PeerAddr.Addr.u4Addr),
                            &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                            IPV4_ADDR_LENGTH);
                    PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
                    if ((L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry) ==
                         pL2vpnCliArgs->u4LocalGroupId) &&
                        (PeerAddr.Addr.u4Addr ==
                         (pL2vpnCliArgs->PeerAddr.u4Addr)))

                    {
                        return L2VPN_TRUE;
                    }

                }
            }
            else
            {
                return L2VPN_FALSE;
            }
            break;

        case CLI_L2VPN_PW_OWNER_GEN_FEC:

            if ((pL2vpnCliArgs->u4GenLocalAiiType == L2VPN_GEN_FEC_AII_TYPE_1)
                && (pL2vpnCliArgs->u4GenRemoteAiiType ==
                    L2VPN_GEN_FEC_AII_TYPE_1))

            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_IPV6_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
                {

                    MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                            pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                            IPV6_ADDR_LENGTH);

                    GenU4Addr.u2AddrType =
                        (UINT2) pL2vpnCliArgs->i4PeerAddrType;
                }
                else if (MPLS_IPV4_ADDR_TYPE == pL2vpnCliArgs->i4PeerAddrType)
#endif
                {
                    GenU4Addr.Addr.u4Addr =
                        OSIX_NTOHL (pL2vpnCliArgs->PeerAddr.u4Addr);
                    GenU4Addr.u2AddrType =
                        (UINT2) pL2vpnCliArgs->i4PeerAddrType;
                }
                if (pL2vpnCliArgs->Agi.pu1_OctetList == NULL)
                {
                    pL2vpnCliArgs->u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();
                    pVplsEntry =
                        L2VpnGetVplsEntryFromInstanceIndex (pL2vpnCliArgs->
                                                            u4VplsIndex);
                    if (NULL == pVplsEntry)
                    {
                        return L2VPN_FAILURE;
                    }
                    pL2vpnCliArgs->Agi.pu1_OctetList = au1AgiVpn;
                    pL2vpnCliArgs->Agi.i4_Length = L2VPN_PWVC_MAX_AGI_LEN;
                    MEMCPY (pL2vpnCliArgs->Agi.pu1_OctetList,
                            &(pVplsEntry->
                              au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
                            pL2vpnCliArgs->Agi.i4_Length);
                    pPwVcEntry =
                        L2VpnGetPwVcEntry (CLI_L2VPN_PW_OWNER_GEN_FEC,
                                           pL2vpnCliArgs->u4PwId,
                                           (UINT1) pL2vpnCliArgs->i4PwType,
                                           pL2vpnCliArgs->Agi.pu1_OctetList,
                                           pL2vpnCliArgs->Saii.pu1_OctetList,
                                           pL2vpnCliArgs->Taii.pu1_OctetList,
                                           GenU4Addr);

                }
                else
                {
                    pPwVcEntry = L2VpnGetPwVcEntry (CLI_L2VPN_PW_OWNER_GEN_FEC,
                                                    pL2vpnCliArgs->u4PwId,
                                                    (UINT1) pL2vpnCliArgs->
                                                    i4PwType,
                                                    pL2vpnCliArgs->Agi.
                                                    pu1_OctetList,
                                                    pL2vpnCliArgs->Saii.
                                                    pu1_OctetList,
                                                    pL2vpnCliArgs->Taii.
                                                    pu1_OctetList, GenU4Addr);
                }
                if (pPwVcEntry != NULL)
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
                    {
                        if (L2VPN_ZERO ==
                            MEMCMP (&pL2vpnCliArgs->PeerAddr.Ip6Addr.u1_addr,
                                    &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr,
                                    IPV6_ADDR_LENGTH))
                        {
                            return L2VPN_TRUE;
                        }

                    }

                    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
                    {
                        MEMCPY ((&PeerAddr.Addr.u4Addr),
                                &pPwVcEntry->PeerAddr.au1Ipv4Addr,
                                IPV4_ADDR_LENGTH);

                        PeerAddr.Addr.u4Addr =
                            OSIX_NTOHL (PeerAddr.Addr.u4Addr);

                        if (PeerAddr.Addr.u4Addr ==
                            (pL2vpnCliArgs->PeerAddr.u4Addr))
                        {
                            return L2VPN_TRUE;
                        }
                    }
                }
                else
                {
                    return L2VPN_FALSE;
                }

            }
            else                /* Gen Fec Type II */
            {
                while (nmhGetNextIndexPwTable (u4PwIndex, &u4PwIndex) ==
                       SNMP_SUCCESS)
                {
                    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

                    if (pPwVcEntry == NULL)
                    {
                        continue;
                    }
                    L2VpnExtractGenFecType2Identifiers
                        (pPwVcEntry->au1Saii,
                         &u4SrcGlobalId, &u4SrcNodeId, &u4SrcAcId);
                    L2VpnExtractGenFecType2Identifiers
                        (pPwVcEntry->au1Taii,
                         &u4DestGlobalId, &u4DestNodeId, &u4DestAcId);

                    if ((pL2vpnCliArgs->u4DestGlobalId == u4DestGlobalId) &&
                        (pL2vpnCliArgs->u4DestNodeId == u4DestNodeId) &&
                        (pL2vpnCliArgs->u4DestAcId == u4DestAcId) &&
                        (pL2vpnCliArgs->u4SrcGlobalId == u4SrcGlobalId) &&
                        (pL2vpnCliArgs->u4SrcNodeId == u4SrcNodeId) &&
                        (pL2vpnCliArgs->u4SrcAcId == u4SrcAcId))
                    {
                        u1IsSrcDstSame = L2VPN_TRUE;
                    }

                    if ((u1IsSrcDstSame == L2VPN_TRUE) &&
                        (L2VPN_IS_STATIC_PW (pPwVcEntry) == FALSE) &&
                        (pL2vpnCliArgs->u4PwId == L2VPN_ZERO))
                    {
                        return L2VPN_TRUE;
                    }
                    /* Gen FEC AII Type 2 PW is a manual mode
                     * of PW for MPLS-TP */
                    if ((L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE) &&
                        (pL2vpnCliArgs->u4LocalLabel != 0) &&
                        (pL2vpnCliArgs->u4RemoteLabel != 0))
                    {
                        if ((pPwVcEntry->u4InVcLabel ==
                             pL2vpnCliArgs->u4LocalLabel)
                            && (pPwVcEntry->u4OutVcLabel ==
                                pL2vpnCliArgs->u4RemoteLabel))
                        {
                            if (pL2vpnCliArgs->u4PwId == L2VPN_ZERO)
                            {
                                return L2VPN_TRUE;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    if (pL2vpnCliArgs->u4PwId != L2VPN_ZERO)
                    {
                        if ((pL2vpnCliArgs->u4PwId == pPwVcEntry->u4PwVcID) &&
                            (u1IsSrcDstSame == L2VPN_TRUE))
                        {
                            return L2VPN_TRUE;
                        }
                    }
                }
            }
            break;
        default:
            break;
    }
    return L2VPN_FALSE;
}

VOID
CliShowAgiValue (tCliHandle CliHandle, UINT1 au1Agi[])
{
    UINT2               u2ASNValue;
    UINT4               u4ASNValue;
    UINT4               u4AgiIpValue;
    UINT2               u2AgiIdValue;
    UINT4               u4AgiIdValue;
    UINT1               au1ASNValue[L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH];

    MEMSET (au1ASNValue, 0, L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH);

    if (au1Agi[0] == L2VPN_PWVC_AGI_TYPE0)
    {
        MEMCPY (&u2ASNValue, au1Agi + L2VPN_PWVC_AGI_TYPE0_ASN_OFFSET,
                L2VPN_PWVC_AGI_TYPE0_ASN_LENGTH);
        MEMCPY (&u4AgiIdValue, au1Agi + L2VPN_PWVC_AGI_TYPE0_ID_OFFSET,
                L2VPN_PWVC_AGI_TYPE0_ID_LENGTH);
        u2ASNValue = OSIX_NTOHS (u2ASNValue);
        u4AgiIdValue = OSIX_NTOHL (u4AgiIdValue);
        CliPrintf (CliHandle, "%u:%u:%u ",
                   L2VPN_PWVC_AGI_TYPE0, u2ASNValue, u4AgiIdValue);
    }
    else if (au1Agi[0] == L2VPN_PWVC_AGI_TYPE1)
    {
        MEMCPY (&u4AgiIpValue, au1Agi + L2VPN_PWVC_AGI_TYPE1_ASN_OFFSET,
                L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH);
        MEMCPY (&u2AgiIdValue, au1Agi + L2VPN_PWVC_AGI_TYPE1_ID_OFFSET,
                L2VPN_PWVC_AGI_TYPE1_ID_LENGTH);
        u2AgiIdValue = OSIX_NTOHS (u2AgiIdValue);
        MEMCPY (au1ASNValue, &u4AgiIpValue, L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH);
        CliPrintf (CliHandle, "%u:%u.%u.%u.%u:%u ",
                   L2VPN_PWVC_AGI_TYPE1, au1ASNValue[0], au1ASNValue[1],
                   au1ASNValue[2], au1ASNValue[3], u2AgiIdValue);
    }
    else if (au1Agi[0] == L2VPN_PWVC_AGI_TYPE2)
    {
        MEMCPY (&u4ASNValue, au1Agi + L2VPN_PWVC_AGI_TYPE2_ASN_OFFSET,
                L2VPN_PWVC_AGI_TYPE2_ASN_LENGTH);
        MEMCPY (&u2AgiIdValue, au1Agi + L2VPN_PWVC_AGI_TYPE2_ID_OFFSET,
                L2VPN_PWVC_AGI_TYPE2_ID_LENGTH);
        u4ASNValue = OSIX_NTOHL (u4ASNValue);
        u2AgiIdValue = OSIX_NTOHS (u2AgiIdValue);
        CliPrintf (CliHandle, "%u:%u:%u ",
                   L2VPN_PWVC_AGI_TYPE2, u4ASNValue, u2AgiIdValue);
    }
    else
    {
        CliPrintf (CliHandle, "%s ", au1Agi);
    }
}

VOID
CliShowRunAgiValue (tCliHandle CliHandle, UINT1 au1Agi[])
{
    UINT2               u2ASNValue;
    UINT4               u4ASNValue;
    UINT4               u4AgiIpValue;
    UINT2               u2AgiIdValue;
    UINT4               u4AgiIdValue;
    UINT1               au1ASNValue[L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH];

    MEMSET (au1ASNValue, 0, L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH);

    if (au1Agi[0] == L2VPN_PWVC_AGI_TYPE0)
    {
        MEMCPY (&u2ASNValue, au1Agi + L2VPN_PWVC_AGI_TYPE0_ASN_OFFSET,
                L2VPN_PWVC_AGI_TYPE0_ASN_LENGTH);
        MEMCPY (&u4AgiIdValue, au1Agi + L2VPN_PWVC_AGI_TYPE0_ID_OFFSET,
                L2VPN_PWVC_AGI_TYPE0_ID_LENGTH);
        u2ASNValue = OSIX_NTOHS (u2ASNValue);
        u4AgiIdValue = OSIX_NTOHL (u4AgiIdValue);
        CliPrintf (CliHandle, "agitype %u ASN %u id %u ",
                   L2VPN_PWVC_AGI_TYPE0, u2ASNValue, u4AgiIdValue);
    }
    else if (au1Agi[0] == L2VPN_PWVC_AGI_TYPE1)
    {
        MEMCPY (&u4AgiIpValue, au1Agi + L2VPN_PWVC_AGI_TYPE1_ASN_OFFSET,
                L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH);
        MEMCPY (&u2AgiIdValue, au1Agi + L2VPN_PWVC_AGI_TYPE1_ID_OFFSET,
                L2VPN_PWVC_AGI_TYPE1_ID_LENGTH);
        u2AgiIdValue = OSIX_NTOHS (u2AgiIdValue);
        MEMCPY (au1ASNValue, &u4AgiIpValue, L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH);
        CliPrintf (CliHandle, "agitype %u %u.%u.%u.%u id %u ",
                   L2VPN_PWVC_AGI_TYPE1, au1ASNValue[0], au1ASNValue[1],
                   au1ASNValue[2], au1ASNValue[3], u2AgiIdValue);
    }
    else if (au1Agi[0] == L2VPN_PWVC_AGI_TYPE2)
    {
        MEMCPY (&u4ASNValue, au1Agi + L2VPN_PWVC_AGI_TYPE2_ASN_OFFSET,
                L2VPN_PWVC_AGI_TYPE2_ASN_LENGTH);
        MEMCPY (&u2AgiIdValue, au1Agi + L2VPN_PWVC_AGI_TYPE2_ID_OFFSET,
                L2VPN_PWVC_AGI_TYPE2_ID_LENGTH);
        u4ASNValue = OSIX_NTOHL (u4ASNValue);
        u2AgiIdValue = OSIX_NTOHS (u2AgiIdValue);
        CliPrintf (CliHandle, "agitype %u ASN %u id %u ",
                   L2VPN_PWVC_AGI_TYPE2, u4ASNValue, u2AgiIdValue);
    }
    else
    {
        CliPrintf (CliHandle, "agi %s ", au1Agi);
    }
}

#ifdef VPLSADS_WANTED
/******************************************************************************
 * Function Name : L2VpnCliRdCreate
 * Description   : This routine configures RD for a VPLS Entry
 * Input(s)      : pu1VplsRd - Rd value to configure
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliRdCreate (tCliHandle CliHandle, UINT1 *pu1VplsRd)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher;
    UINT1               au1VplsRouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];

    UNUSED_PARAM (CliHandle);

    MEMSET (au1VplsRouteDistinguisher, L2VPN_ZERO, L2VPN_MAX_VPLS_RD_LEN);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    i4RetStatus = nmhTestv2VplsBgpADConfigRowStatus (&u4ErrorCode,
                                                     u4VplsIndex,
                                                     CREATE_AND_WAIT);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpADConfigRowStatus(c&w) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, CREATE_AND_WAIT);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpADConfigRowStatus(c&w) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    MEMCPY (au1VplsRouteDistinguisher, pu1VplsRd, L2VPN_MAX_VPLS_RD_LEN);

    VplsRouteDistinguisher.pu1_OctetList = au1VplsRouteDistinguisher;
    VplsRouteDistinguisher.i4_Length = L2VPN_MAX_VPLS_RD_LEN;

    i4RetStatus = nmhTestv2VplsBgpADConfigRouteDistinguisher (&u4ErrorCode,
                                                              u4VplsIndex,
                                                              &VplsRouteDistinguisher);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpADConfigRouteDistinguisher Failed\r\n",
                    __func__);
        i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                        __func__);
        }
#endif
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpADConfigRouteDistinguisher (u4VplsIndex,
                                                           &VplsRouteDistinguisher);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpADConfigRouteDistinguisher Failed\r\n",
                    __func__);
        i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                        __func__);
        }
        return CLI_FAILURE;
    }
#endif

    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);

    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "nmhGetFsMplsVplsRowStatus returns Failure %d \r\n",
                    i4RetStatus);
        return CLI_FAILURE;
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
#endif

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            return CLI_FAILURE;
        }
#endif
    }

    i4RetStatus = nmhTestv2VplsBgpADConfigRowStatus (&u4ErrorCode,
                                                     u4VplsIndex, ACTIVE);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpADConfigRowStatus(Active) Failed\r\n",
                    __func__);
        i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                        __func__);
        }
#endif
        i4RetVal = CLI_FAILURE;
    }
    else
    {
        i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpADConfigRowStatus (Active) Failed\r\n",
                        __func__);
            i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_SUCCESS == i4RetStatus)
#endif
        {
            i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                            __func__);
                i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex,
                                                              DESTROY);
                if (SNMP_FAILURE == i4RetStatus)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                                __func__);
                }
                i4RetVal = CLI_FAILURE;
            }
#endif
        }
#ifndef MPLS_TEST_WANTED
        else
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCliRdDelete 
 * Description   : This routine deletes RD for a VPLS Entry
 * Input(s)      : pu1VplsRd - Rd value to delete
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliRdDelete (tCliHandle CliHandle)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;

    UNUSED_PARAM (CliHandle);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    i4RetStatus = nmhGetVplsBgpADConfigRowStatus (u4VplsIndex, &i4RowStatus);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhGetVplsBgpADConfigRowStatus Failed\r\n", __func__);
        return CLI_FAILURE;
    }

    i4RowStatus = L2VPN_ZERO;
    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);

    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "nmhGetFsMplsVplsRowStatus returns FAILURE %d \r\n",
                    i4RetStatus);
        return CLI_FAILURE;
    }
    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }

    i4RetStatus = nmhTestv2VplsBgpADConfigRowStatus (&u4ErrorCode,
                                                     u4VplsIndex, DESTROY);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                    __func__);
        i4RetVal = CLI_FAILURE;
    }
    else
#endif
    {
        i4RetStatus = nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpADConfigRowStatus(Destroy) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCliRtCreate
 * Description   : This routine configures RT for a VPLS Entry
 * Input(s)      : pu1VplsRt - Rt value to configure
 *                 u4VplsRtType - RT Type(import/export/both)
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliRtCreate (tCliHandle CliHandle, UINT1 *pu1VplsRt, UINT4 u4VplsRtType)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4VplsRtIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
    UINT1               au1VplsRouteTarget[L2VPN_MAX_VPLS_RT_LEN];

    MEMSET (au1VplsRouteTarget, L2VPN_ZERO, L2VPN_MAX_VPLS_RT_LEN);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    if (L2VPN_FAILURE ==
        L2VpnGetFreeRtIndexForVpls (u4VplsIndex, &u4VplsRtIndex))
    {
        CliPrintf (CliHandle, "\r %% Free Rt Index not available\n");
        return CLI_FAILURE;
    }

    i4RetStatus = nmhTestv2VplsBgpRteTargetRTRowStatus (&u4ErrorCode,
                                                        u4VplsIndex,
                                                        u4VplsRtIndex,
                                                        CREATE_AND_WAIT);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpRteTargetRTRowStatus(c&w) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                     u4VplsRtIndex,
                                                     CREATE_AND_WAIT);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpRteTargetRTRowStatus(c&w) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    MEMCPY (au1VplsRouteTarget, pu1VplsRt, L2VPN_MAX_VPLS_RT_LEN);
    VplsRouteTarget.pu1_OctetList = au1VplsRouteTarget;
    VplsRouteTarget.i4_Length = L2VPN_MAX_VPLS_RT_LEN;

    i4RetStatus = nmhTestv2VplsBgpRteTargetRT (&u4ErrorCode,
                                               u4VplsIndex,
                                               u4VplsRtIndex, &VplsRouteTarget);
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex,
                                                         DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                        __func__);
        }
#endif
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpRteTargetRT Failed\r\n", __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpRteTargetRT (u4VplsIndex,
                                            u4VplsRtIndex, &VplsRouteTarget);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex,
                                                         DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                        __func__);
        }
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpRteTargetRT Failed\r\n", __func__);
        return CLI_FAILURE;
    }
#endif

    i4RetStatus = nmhTestv2VplsBgpRteTargetRTType (&u4ErrorCode,
                                                   u4VplsIndex,
                                                   u4VplsRtIndex,
                                                   (INT4) (u4VplsRtType));
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex,
                                                         DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                        __func__);
        }
#endif
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpRteTargetRTType Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpRteTargetRTType (u4VplsIndex,
                                                u4VplsRtIndex, u4VplsRtType);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex,
                                                         DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpRteTargetRTType Failed\r\n", __func__);
        return CLI_FAILURE;
    }
#endif

    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);

    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "nmhGetFsMplsVplsRowStatus returns FAILURE%d \r\n",
                    i4RetStatus);
        return CLI_FAILURE;
    }
    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                             u4VplsRtIndex,
                                                             DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                             u4VplsRtIndex,
                                                             DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }

    i4RetStatus = nmhTestv2VplsBgpRteTargetRTRowStatus (&u4ErrorCode,
                                                        u4VplsIndex,
                                                        u4VplsRtIndex, ACTIVE);
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex,
                                                         DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                        __func__);
        }
#endif
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpRteTargetRTRowStatus(Active) Failed\r\n",
                    __func__);
        i4RetVal = CLI_FAILURE;
    }
    else
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                             u4VplsRtIndex,
                                                             DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                            __func__);
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Active) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_SUCCESS == i4RetStatus)
#endif
        {
            i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
            if (SNMP_FAILURE == i4RetStatus)
            {
                i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                                 u4VplsRtIndex,
                                                                 DESTROY);
                if (SNMP_FAILURE == i4RetStatus)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destroy) Failed\r\n",
                                __func__);
                }
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                            __func__);
                i4RetVal = CLI_FAILURE;
            }
#endif
        }
#ifndef MPLS_TEST_WANTED
        else
        {
            i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                             u4VplsRtIndex,
                                                             DESTROY);
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destroy) Failed\r\n",
                            __func__);
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCliRtDelete
 * Description   : This routine deletes RT for a VPLS Entry
 * Input(s)      : pu1VplsRt - Rt value to delete
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliRtDelete (tCliHandle CliHandle, UINT1 *pu1VplsRt)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4VplsIndexForRt = L2VPN_ZERO;
    UINT4               u4VplsRtIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    if (L2VPN_FAILURE == L2VpnGetVplsAndRtIndexFromRtName (pu1VplsRt,
                                                           &u4VplsIndexForRt,
                                                           &u4VplsRtIndex))
    {
        CliPrintf (CliHandle, "\r %%Rt Value not exists\n");
        return CLI_FAILURE;
    }

    if (u4VplsIndex != u4VplsIndexForRt)
    {
        CliPrintf (CliHandle, "\r %%Rt is associated with some other VPLS\n");
        return CLI_FAILURE;
    }

    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);

    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "nmhGetFsMplsVplsRowStatus returns FAILURE %d \r\n",
                    i4RetStatus);
        return CLI_FAILURE;
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }

    i4RetStatus = nmhTestv2VplsBgpRteTargetRTRowStatus (&u4ErrorCode,
                                                        u4VplsIndex,
                                                        u4VplsRtIndex, DESTROY);
#ifndef MPLS_TEST_WANTED
    if (SNMP_SUCCESS == i4RetStatus)
#endif
    {
        i4RetStatus = nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4VplsRtIndex,
                                                         DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
    }
#ifndef MPLS_TEST_WANTED
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpRteTargetRTRowStatus(Destry) Failed\r\n",
                    __func__);
        i4RetVal = CLI_FAILURE;
    }
#endif

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_SUCCESS == i4RetStatus)
#endif
        {
            i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                            __func__);
                i4RetVal = CLI_FAILURE;
            }
#endif
        }
#ifndef MPLS_TEST_WANTED
        else
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCliSetControlWord
 * Description   : This routine set Control word flag for a VPLS Entry
 * Input(s)      : u4VplsControlWord - Control word flag to configure
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliSetControlWord (tCliHandle CliHandle, UINT4 u4VplsControlWord)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;

    UNUSED_PARAM (CliHandle);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);

    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "nmhGetFsMplsVplsRowStatus returns FAILURE %d \r\n",
                    i4RetStatus);
        return CLI_FAILURE;
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }

    i4RetStatus = nmhTestv2FsmplsVplsControlWord (&u4ErrorCode,
                                                  u4VplsIndex,
                                                  (INT4) u4VplsControlWord);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsmplsVplsControlWord Failed\r\n", __func__);
        i4RetVal = CLI_FAILURE;
    }
    else
    {

        i4RetStatus =
            nmhSetFsmplsVplsControlWord (u4VplsIndex, u4VplsControlWord);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsmplsVplsControlWord Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_SUCCESS == i4RetStatus)
#endif
        {
            i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                            __func__);
                i4RetVal = CLI_FAILURE;
            }
#endif
        }
#ifndef MPLS_TEST_WANTED
        else
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCliSetMtu
 * Description   : This routine set MTU for a VPLS Entry
 * Input(s)      : u4VplsMtu - MTU value to configure
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliSetMtu (tCliHandle CliHandle, UINT4 u4VplsMtu)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;

    UNUSED_PARAM (CliHandle);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);
    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }

    i4RetStatus = nmhTestv2FsmplsVplsMtu (&u4ErrorCode, u4VplsIndex, u4VplsMtu);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsmplsVplsMtu Failed\r\n", __func__);
        i4RetVal = CLI_FAILURE;
    }
    else
    {
        i4RetStatus = nmhSetFsmplsVplsMtu (u4VplsIndex, u4VplsMtu);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsmplsVplsMtu Failed\r\n", __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_SUCCESS == i4RetStatus)
#endif
        {
            i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
            if (SNMP_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                            __func__);
                i4RetVal = CLI_FAILURE;
            }
#endif
        }
#ifndef MPLS_TEST_WANTED
        else
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
#endif
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCliVeCreate
 * Description   : This routine Configures VEId for a VPLS Entry
 * Input(s)      : u4VplsVeId - VeId value to configure
 *                 pu4VplsVeName - Ve name to configure
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliVeCreate (tCliHandle CliHandle, UINT4 u4VplsVeId, UINT1 *pu4VplsVeName)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE VplsVeName;
    UINT1               au1VplsVeName[L2VPN_MAX_VPLS_NAME_LEN];

    UNUSED_PARAM (CliHandle);

    MEMSET (au1VplsVeName, L2VPN_ZERO, L2VPN_MAX_VPLS_NAME_LEN);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    i4RetStatus = nmhTestv2VplsBgpVERowStatus (&u4ErrorCode,
                                               u4VplsIndex,
                                               u4VplsVeId, CREATE_AND_WAIT);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpVERowStatus(c&w) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                            u4VplsVeId, CREATE_AND_WAIT);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpVERowStatus(c&w) Failed\r\n", __func__);
        return CLI_FAILURE;
    }

    MEMCPY (au1VplsVeName, pu4VplsVeName, STRLEN (pu4VplsVeName));
    VplsVeName.pu1_OctetList = au1VplsVeName;
    VplsVeName.i4_Length = (INT4) STRLEN (pu4VplsVeName);

    i4RetStatus = nmhTestv2VplsBgpVEName (&u4ErrorCode,
                                          u4VplsIndex, u4VplsVeId, &VplsVeName);
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                u4VplsVeId, DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpVERowStatus(Destroy) Failed\r\n",
                        __func__);
        }
#endif

        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpVEName Failed\r\n", __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpVEName (u4VplsIndex, u4VplsVeId, &VplsVeName);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                u4VplsVeId, DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpVERowStatus(Destroy) Failed\r\n",
                        __func__);
        }

        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpVEName Failed\r\n", __func__);
        return CLI_FAILURE;
    }
#endif

    i4RetStatus = nmhTestv2VplsBgpVERowStatus (&u4ErrorCode,
                                               u4VplsIndex, u4VplsVeId, ACTIVE);
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                u4VplsVeId, DESTROY);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpVERowStatus(Destroy) Failed\r\n",
                        __func__);
        }
#endif

        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpVERowStatus(ACTIVE) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }

    i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex, u4VplsVeId, ACTIVE);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                u4VplsVeId, DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpVERowStatus(Destroy) Failed\r\n",
                        __func__);
        }

        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetVplsBgpVERowStatus(ACTIVE) Failed\r\n",
                    __func__);
        return CLI_FAILURE;
    }
#endif
    /* when this API is called from CLI, RowStatus is already NOT_IN_SERVICE. 
     * Hence making it ACTIVE here.*/
    /* Since VPLS can be made ACTIVE only when all the mandatory parameters 
     * are set, it is possible that making ACTIVE fails here. 
     * Still we do not return failure, as VE is set successfully.*/

    i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                u4VplsIndex, ACTIVE);
    if (SNMP_SUCCESS == i4RetStatus)
    {
        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                        __func__);
        }
#endif
    }
#ifndef MPLS_TEST_WANTED
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsRowStatus(ACTIVE) Failed\r\n",
                    __func__);
    }
#endif

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnCliVeDelete
 * Description   : This routine deletes VEId for a VPLS Entry
 * Input(s)      : u4VplsVeId - VeId value to configure
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCliVeDelete (tCliHandle CliHandle, UINT4 u4VplsVeId)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4ErrorCode = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4VeId = 0;

    UNUSED_PARAM (CliHandle);

    u4VplsIndex = (UINT4) CLI_GET_MPLS_VFI_ID ();

    if ((L2VpnGetLocalVeIdFromVPLSIndex (u4VplsIndex, &u4VeId) ==
         L2VPN_FAILURE) || u4VeId != u4VplsVeId)
    {
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_VE_NOT_EXISTS);
        return CLI_FAILURE;
    }
    i4RetStatus = nmhGetFsMplsVplsRowStatus (u4VplsIndex, &i4RowStatus);

    if (ACTIVE == i4RowStatus)
    {
        i4RetStatus = nmhTestv2FsMplsVplsRowStatus (&u4ErrorCode,
                                                    u4VplsIndex,
                                                    NOT_IN_SERVICE);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhTestv2FsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }

        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, NOT_IN_SERVICE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
            return CLI_FAILURE;
        }
#endif
    }
    i4RetStatus = nmhTestv2VplsBgpVERowStatus (&u4ErrorCode,
                                               u4VplsIndex,
                                               u4VplsVeId, DESTROY);
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2VplsBgpVERowStatus(Destroy) Failed\r\n",
                    __func__);
        i4RetVal = CLI_FAILURE;
    }
    else
    {
        i4RetStatus = nmhSetVplsBgpVERowStatus (u4VplsIndex,
                                                u4VplsVeId, DESTROY);
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetVplsBgpVERowStatus(Destroy) Failed\r\n",
                        __func__);
            i4RetVal = CLI_FAILURE;
        }
    }

    /* if VE is DESTRYed, RowStatus cannot be made ACTIVE, as VE is mandatory
     * parameter. Hence making ACTIVE only if VE DESTROY fails.*/
    if (ACTIVE == i4RowStatus && CLI_FAILURE == i4RetVal)
    {
        i4RetStatus = nmhSetFsMplsVplsRowStatus (u4VplsIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
        if (SNMP_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : nmhSetFsMplsVplsRowStatus(NIS) Failed\r\n",
                        __func__);
        }
#endif
    }
    return i4RetVal;
}

/******************************************************************************
 * Function Name : L2VpnCreateVplsACMap
 * Description   : This routine Creates an AC entry for a VPLS Entry
 * Input(s)      : tVPLSEntry - Pointer to Vpls Entry
 *                    tL2vpnCliArgs - Port and VLAN are part of CLI arguments
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnCreateVplsACMap (tVPLSEntry * pVplsEntry, tL2vpnCliArgs * pL2vpnCliArgs)
{
    UINT4               u4VplsAcIndex;
    UINT4               u4ErrorCode;
    INT4                i4RetStatus = SNMP_FAILURE;

    if (L2VpnGetFreeAcIndexForVplsAc (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                      &u4VplsAcIndex) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Free Index for Ac is not found\r\n", __func__);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMplsVplsAcMapRowStatus (&u4ErrorCode,
                                           L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                           u4VplsAcIndex,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcRowStatus(CREATE_AND_WAIT)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcRowStatus(CREATE_AND_WAIT)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMplsVplsAcMapPortIfIndex (&u4ErrorCode,
                                             L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                             u4VplsAcIndex,
                                             (UINT4) (pL2vpnCliArgs->
                                                      i4PortIfIndex)) ==
        SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcMapPortIfIndex() Failed\r\n",
                    __func__);
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
        return CLI_FAILURE;
    }
    i4RetStatus =
        nmhSetFsMplsVplsAcMapPortIfIndex (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                          u4VplsAcIndex,
                                          pL2vpnCliArgs->i4PortIfIndex);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetFsMplsVplsAcMapPortIfIndex() Failed\r\n",
                    __func__);
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
        return CLI_FAILURE;
    }
#endif
    if (nmhTestv2FsMplsVplsAcMapPortVlan (&u4ErrorCode,
                                          L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                          u4VplsAcIndex,
                                          pL2vpnCliArgs->u4VlanId) ==
        SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcMapPortVlan() Failed\r\n",
                    __func__);
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
        return CLI_FAILURE;
    }
    i4RetStatus =
        nmhSetFsMplsVplsAcMapPortVlan (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                       u4VplsAcIndex, pL2vpnCliArgs->u4VlanId);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhSetFsMplsVplsAcMapPortVlan() Failed\r\n",
                    __func__);
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
        return CLI_FAILURE;
    }
#endif
    if (nmhTestv2FsMplsVplsAcMapRowStatus (&u4ErrorCode,
                                           L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                           u4VplsAcIndex,
                                           ACTIVE) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcRowStatus(ACTIVE)"
                    "Failed\r\n", __func__);
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
        return CLI_FAILURE;
    }
    i4RetStatus =
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, ACTIVE);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcRowStatus(ACTIVE)"
                    "Failed\r\n", __func__);
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
        return CLI_FAILURE;
    }
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnDeleteVplsACMap
 * Description   : This routine deletes an AC entry for a VPLS Entry
 * Input(s)      : tVPLSEntry - Pointer to Vpls Entry
 *                    i4IfIndex - Port If Index
 *                    u4VlanId - Vlan Id
 * Output(s)     : None.
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *******************************************************************************/
INT4
L2VpnDeleteVplsACMap (tVPLSEntry * pVplsEntry, INT4 i4IfIndex, UINT4 u4VlanId)
{
    UINT4               u4VplsAcIndex;
    UINT4               u4ErrorCode;
    INT4                i4RetStatus = SNMP_FAILURE;

    if (L2VpnGetAcIndexFromIfandVlan
        ((UINT4) (i4IfIndex), u4VlanId, &u4VplsAcIndex) == L2VPN_FAILURE)
    {
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Ac Index not found\r\n", __func__);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMplsVplsAcMapRowStatus (&u4ErrorCode,
                                           L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                           u4VplsAcIndex,
                                           DESTROY) == SNMP_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcRowStatus(DESTROY)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
    i4RetStatus =
        nmhSetFsMplsVplsAcMapRowStatus (L2VPN_PWVC_VPLS_INDEX (pVplsEntry),
                                        u4VplsAcIndex, DESTROY);
#ifndef MPLS_TEST_WANTED
    if (SNMP_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : nmhTestv2FsMplsVplsAcRowStatus(DESTROY)"
                    "Failed\r\n", __func__);
        return CLI_FAILURE;
    }
#endif

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : L2VpnCliShowAutoDiscoveredVfiDetails
* Description   : This routine is used to Display a Auto Discocvered 
*                 VPLS Details
* Input(s)      : CliHandle  - Context in which the command is processed
*                 pVplsEntry - Vpls Entry pointer
* Output(s)     : None
* Return(s)     : CLI_FAILED or CLI_SUCCEEDED
*******************************************************************************/
VOID
L2VpnCliShowAutoDiscoveredVfiDetails (tCliHandle CliHandle,
                                      tVPLSEntry * pVplsEntry)
{
    tSNMP_OCTET_STRING_TYPE tVplsBgpVEName;
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4VEId = L2VPN_ZERO;
    UINT4               u4ASN = L2VPN_ZERO;
    UINT4               u4AsignedNumber = L2VPN_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT2               u2ASN = L2VPN_ZERO;
    UINT2               u2AsignedNumber = L2VPN_ZERO;
    UINT1               au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1               au1VplsBgpVeName[L2VPN_MAX_VPLS_NAME_LEN];

    MEMSET (au1RouteDistinguisher, L2VPN_ZERO, L2VPN_MAX_VPLS_RD_LEN);
    MEMSET (au1VplsBgpVeName, L2VPN_ZERO, L2VPN_MAX_VPLS_NAME_LEN);
    MEMSET (&tVplsBgpVEName, L2VPN_ZERO, sizeof (tVplsBgpVEName));

    tVplsBgpVEName.pu1_OctetList = au1VplsBgpVeName;

    if (L2VPN_VPLS_OPER_STAT_UP == L2VPN_VPLS_OPER_STATUS (pVplsEntry))
    {
        CliPrintf (CliHandle, ", Oper-state: UP,");
    }
    else
    {
        CliPrintf (CliHandle, ", Oper-state: DOWN,");
    }

    CliPrintf (CliHandle, " Signalling-Type: BGP\r\n");

    MEMCPY (&u4VpnId,
            &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
            sizeof (UINT4));

    u4VpnId = OSIX_HTONL (u4VpnId);

    CliPrintf (CliHandle, " VPN ID: %d,", u4VpnId);
    i4RetStatus = L2VpnGetRdValue (L2VPN_VPLS_INDEX (pVplsEntry),
                                   au1RouteDistinguisher);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        CliPrintf (CliHandle, " RD: INVALID\r\n");
    }
    else
    {
        if (L2VPN_VPLS_RD_TYPE_0 == au1RouteDistinguisher[0])
        {
            MEMCPY (&u2ASN, &au1RouteDistinguisher[2], sizeof (UINT2));
            MEMCPY (&u4AsignedNumber, &au1RouteDistinguisher[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            CliPrintf (CliHandle, " RD: %u:%u\r\n", u2ASN, u4AsignedNumber);
        }
        else if (L2VPN_VPLS_RD_TYPE_1 == au1RouteDistinguisher[0])
        {
            MEMCPY (&u2AsignedNumber, &au1RouteDistinguisher[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, " RD: %u.%u.%u.%u:%u\r\n",
                       au1RouteDistinguisher[2], au1RouteDistinguisher[3],
                       au1RouteDistinguisher[4], au1RouteDistinguisher[5],
                       u2AsignedNumber);
        }
        else
        {
            MEMCPY (&u4ASN, &au1RouteDistinguisher[2], sizeof (UINT4));
            MEMCPY (&u2AsignedNumber, &au1RouteDistinguisher[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, " RD: %u.%u:%u\r\n",
                       (u4ASN & 0xffff0000) >> 16, u4ASN & 0x0000ffff,
                       u2AsignedNumber);
        }
    }

    L2VpnCliShowRtDetails (CliHandle, L2VPN_VPLS_INDEX (pVplsEntry));

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (L2VPN_VPLS_INDEX (pVplsEntry),
                                                  &u4VEId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        CliPrintf (CliHandle, " VE-Id: -,");
        CliPrintf (CliHandle, " VE-Name: -\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " VE-Id: %d,", u4VEId);
        i4RetStatus = nmhGetVplsBgpVEName (L2VPN_VPLS_INDEX (pVplsEntry),
                                           u4VEId, &tVplsBgpVEName);
        if (SNMP_FAILURE == i4RetStatus)
        {
            CliPrintf (CliHandle, " VE-Name: -\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " VE-Name: %s\r\n",
                       tVplsBgpVEName.pu1_OctetList);
        }
    }

    CliPrintf (CliHandle, " MTU: %d,", L2VPN_VPLS_MTU (pVplsEntry));
    if (L2VPN_ENABLED == L2VPN_VPLS_CONTROL_WORD (pVplsEntry))
    {
        CliPrintf (CliHandle, " Control-word: true\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Control-word: false\r\n");
    }

    CliPrintf (CliHandle, " Local attachment circuits:\r\n");
    L2VpnCliShowVplsAcDetails (CliHandle, L2VPN_VPLS_INDEX (pVplsEntry));

    CliPrintf (CliHandle, " Neighbors connected via pseudowires:\r\n");
}

/******************************************************************************
* Function Name : L2VpnCliShowRtDetails
* Description   : This routine is used to Display a Rt Details for a 
*                 particular VPLS 
* Input(s)      : CliHandle  - Context in which the command is processed
*                 u4VplsIndex - Vpls Index
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpnCliShowRtDetails (tCliHandle CliHandle, UINT4 u4VplsIndex)
{
    tSNMP_OCTET_STRING_TYPE tVplsBgpRteTargetRT;
    UINT4               u4RtIndex = L2VPN_ZERO;
    UINT4               u4NextRtIndex = L2VPN_ZERO;
    UINT4               u4NextVplsIndex = L2VPN_ZERO;
    UINT4               u4ASN = L2VPN_ZERO;
    UINT4               u4AsignedNumber = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RTType = L2VPN_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT2               u2ASN = L2VPN_ZERO;
    UINT2               u2AsignedNumber = L2VPN_ZERO;
    UINT1               au1VplsBgpRteTargetRT[L2VPN_MAX_VPLS_RT_LEN];
    UINT1               u1ActiveRtFound = L2VPN_FALSE;

    MEMSET (au1VplsBgpRteTargetRT, L2VPN_ZERO, L2VPN_MAX_VPLS_RT_LEN);
    MEMSET (&tVplsBgpRteTargetRT, L2VPN_ZERO, sizeof (tVplsBgpRteTargetRT));

    tVplsBgpRteTargetRT.pu1_OctetList = au1VplsBgpRteTargetRT;

    i4RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                        &u4NextVplsIndex,
                                                        u4RtIndex,
                                                        &u4NextRtIndex);

    while (SNMP_SUCCESS == i4RetStatus)
    {
        u4RtIndex = u4NextRtIndex;

        if (u4VplsIndex != u4NextVplsIndex)
        {
            break;
        }

        i4RetStatus = nmhGetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4RtIndex,
                                                         &i4RowStatus);
        if (ACTIVE != i4RowStatus)
        {
            i4RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                                &u4NextVplsIndex,
                                                                u4RtIndex,
                                                                &u4NextRtIndex);
            continue;
        }

        i4RetStatus = nmhGetVplsBgpRteTargetRT (u4VplsIndex,
                                                u4RtIndex,
                                                &tVplsBgpRteTargetRT);
        if (L2VPN_VPLS_RT_TYPE_0 == tVplsBgpRteTargetRT.pu1_OctetList[0])
        {
            MEMCPY (&u2ASN, &tVplsBgpRteTargetRT.pu1_OctetList[2],
                    sizeof (UINT2));
            MEMCPY (&u4AsignedNumber, &tVplsBgpRteTargetRT.pu1_OctetList[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            CliPrintf (CliHandle, " RT: %u:%u", u2ASN, u4AsignedNumber);
        }
        else if (L2VPN_VPLS_RT_TYPE_1 == tVplsBgpRteTargetRT.pu1_OctetList[0])
        {
            MEMCPY (&u2AsignedNumber, &tVplsBgpRteTargetRT.pu1_OctetList[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, " RT: %u.%u.%u.%u:%u",
                       tVplsBgpRteTargetRT.pu1_OctetList[2],
                       tVplsBgpRteTargetRT.pu1_OctetList[3],
                       tVplsBgpRteTargetRT.pu1_OctetList[4],
                       tVplsBgpRteTargetRT.pu1_OctetList[5], u2AsignedNumber);
        }
        else
        {
            MEMCPY (&u4ASN, &tVplsBgpRteTargetRT.pu1_OctetList[2],
                    sizeof (UINT4));
            MEMCPY (&u2AsignedNumber, &tVplsBgpRteTargetRT.pu1_OctetList[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, " RT: %u.%u:%u", (u4ASN & 0xffff0000) >> 16,
                       u4ASN & 0x0000ffff, u2AsignedNumber);
        }

        i4RetStatus = nmhGetVplsBgpRteTargetRTType (u4VplsIndex,
                                                    u4RtIndex, &i4RTType);
        if (L2VPN_VPLS_RT_EXPORT == i4RTType)
        {
            CliPrintf (CliHandle, "  POLICY: EXPORT\r\n");
        }
        else if (L2VPN_VPLS_RT_IMPORT == i4RTType)
        {
            CliPrintf (CliHandle, "  POLICY: IMPORT\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  POLICY: BOTH\r\n");
        }
        u1ActiveRtFound = L2VPN_TRUE;

        i4RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                            &u4NextVplsIndex,
                                                            u4RtIndex,
                                                            &u4NextRtIndex);
    }

    if (L2VPN_TRUE != u1ActiveRtFound)
    {
        if (L2VPN_SUCCESS != L2VpnGetDefaultRtValue (u4VplsIndex,
                                                     au1VplsBgpRteTargetRT))
        {
            CliPrintf (CliHandle, " RT: INVALID\r\n");
            return;
        }
        if (L2VPN_VPLS_RT_TYPE_0 == au1VplsBgpRteTargetRT[0])
        {
            MEMCPY (&u2ASN, &au1VplsBgpRteTargetRT[2], sizeof (UINT2));
            MEMCPY (&u4AsignedNumber, &au1VplsBgpRteTargetRT[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            CliPrintf (CliHandle, " RT: %u:%u", u2ASN, u4AsignedNumber);
        }
        else if (L2VPN_VPLS_RT_TYPE_1 == au1VplsBgpRteTargetRT[0])
        {
            MEMCPY (&u2AsignedNumber, &au1VplsBgpRteTargetRT[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, " RT: %u.%u.%u.%u:%u",
                       au1VplsBgpRteTargetRT[2],
                       au1VplsBgpRteTargetRT[3],
                       au1VplsBgpRteTargetRT[4],
                       au1VplsBgpRteTargetRT[5], u2AsignedNumber);
        }
        else
        {
            MEMCPY (&u4ASN, &au1VplsBgpRteTargetRT[2], sizeof (UINT4));
            MEMCPY (&u2AsignedNumber, &au1VplsBgpRteTargetRT[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, " RT: %u.%u:%u", (u4ASN & 0xffff0000) >> 16,
                       u4ASN & 0x0000ffff, u2AsignedNumber);
        }

        CliPrintf (CliHandle, "  POLICY: BOTH\r\n");
    }
}

/******************************************************************************
* Function Name : L2VpnCliShowVplsAcDetails
* Description   : This routine is used to Display a AC Details for a 
*                 particular VPLS 
* Input(s)      : CliHandle  - Context in which the command is processed
*                 u4VplsIndex - Vpls Index
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpnCliShowVplsAcDetails (tCliHandle CliHandle, UINT4 u4VplsIndex)
{
    UINT4               u4AcIndex = L2VPN_ZERO;
    UINT4               u4NextAcIndex = L2VPN_ZERO;
    UINT4               u4NextVplsIndex = L2VPN_ZERO;
    UINT4               u4IfIndex = L2VPN_ZERO;
    UINT4               u4Vlan = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    INT1                ai1OutputIfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (ai1OutputIfName, L2VPN_ZERO, CFA_MAX_PORT_NAME_LENGTH);

    while (1)
    {
        i4RetStatus = nmhGetNextIndexFsMplsVplsAcMapTable (u4VplsIndex,
                                                           &u4NextVplsIndex,
                                                           u4AcIndex,
                                                           &u4NextAcIndex);
        if (SNMP_FAILURE == i4RetStatus)
        {
            break;
        }

        u4AcIndex = u4NextAcIndex;

        if (u4VplsIndex != u4NextVplsIndex)
        {
            break;
        }

        i4RetStatus = nmhGetFsMplsVplsAcMapRowStatus (u4VplsIndex,
                                                      u4AcIndex, &i4RowStatus);
        if (ACTIVE != i4RowStatus)
        {
            continue;
        }

        i4RetStatus = nmhGetFsMplsVplsAcMapPortVlan (u4VplsIndex,
                                                     u4AcIndex, &u4Vlan);

        i4RetStatus = nmhGetFsMplsVplsAcMapPortIfIndex (u4VplsIndex,
                                                        u4AcIndex, &u4IfIndex);

        if (L2VPN_VPLSAC_INVALID_PORT_INDEX != u4IfIndex)
        {
            CfaCliGetIfName (u4IfIndex, ai1OutputIfName);
            CliPrintf (CliHandle, " Ethernet: %s", ai1OutputIfName);
        }
        else
        {
            CliPrintf (CliHandle, " Ethernet: -");
        }

        if (L2VPN_VPLSAC_INVALID_PORT_INDEX != u4Vlan)
        {
            CliPrintf (CliHandle, " vlan: %d\r\n", u4Vlan);
        }
        else
        {
            CliPrintf (CliHandle, " vlan: -\r\n");
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2VpnCliParseAndGenerateRdRt                       */
/*                                                                           */
/*     DESCRIPTION      : This function takes random string as input and     */
/*                        generate valid RD/RT Value                         */
/*     INPUT            : CliHandle - Cli handle                             */
/*                        pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/*     OUTPUT           : pu1RdRt - Generated Rd Value                       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
L2VpnCliParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt)
{
    if (L2VPN_FAILURE ==
        L2VpnUtilParseAndGenerateRdRt (pu1RandomString, pu1RdRt))
    {
        return CLI_FAILURE;
    }
    else
    {
        return CLI_SUCCESS;
    }
}

/******************************************************************************
* Function Name : L2VpnCliShowRunningAutoDiscoveredVfi
* Description   : This routine is used to Display a Auto Discocvered 
*                 VPLS Details
* Input(s)      : CliHandle  - Context in which the command is processed
*                 pVplsEntry - Vpls Entry pointer
* Output(s)     : None
* Return(s)     : CLI_FAILED or CLI_SUCCEEDED
*******************************************************************************/
VOID
L2VpnCliShowRunningAutoDiscoveredVfi (tCliHandle CliHandle,
                                      tVPLSEntry * pVplsEntry)
{
    tSNMP_OCTET_STRING_TYPE tVplsBgpVEName;
    tSNMP_OCTET_STRING_TYPE tVplsRouteDistinguisher;
    UINT4               u4VEId = L2VPN_ZERO;
    UINT4               u4ASN = L2VPN_ZERO;
    UINT4               u4AsignedNumber = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT2               u2ASN = L2VPN_ZERO;
    UINT2               u2AsignedNumber = L2VPN_ZERO;
    UINT1               au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1               au1VplsBgpVeName[L2VPN_MAX_VPLS_NAME_LEN];

    MEMSET (au1RouteDistinguisher, L2VPN_ZERO, L2VPN_MAX_VPLS_RD_LEN);
    MEMSET (au1VplsBgpVeName, L2VPN_ZERO, L2VPN_MAX_VPLS_NAME_LEN);
    MEMSET (&tVplsBgpVEName, L2VPN_ZERO, sizeof (tVplsBgpVEName));
    MEMSET (&tVplsRouteDistinguisher, L2VPN_ZERO,
            sizeof (tVplsRouteDistinguisher));

    tVplsBgpVEName.pu1_OctetList = au1VplsBgpVeName;
    tVplsRouteDistinguisher.pu1_OctetList = au1RouteDistinguisher;

    i4RetStatus = nmhGetVplsBgpADConfigRowStatus (L2VPN_VPLS_INDEX (pVplsEntry),
                                                  &i4RowStatus);
    if (SNMP_SUCCESS == i4RetStatus)
    {
        if (ACTIVE == i4RowStatus)
        {
            i4RetStatus =
                nmhGetVplsBgpADConfigRouteDistinguisher (L2VPN_VPLS_INDEX
                                                         (pVplsEntry),
                                                         &tVplsRouteDistinguisher);
            if (SNMP_SUCCESS == i4RetStatus)
            {
                CliPrintf (CliHandle, "rd ");
                if (L2VPN_VPLS_RD_TYPE_0 == au1RouteDistinguisher[0])
                {
                    MEMCPY (&u2ASN, &au1RouteDistinguisher[2], sizeof (UINT2));
                    MEMCPY (&u4AsignedNumber, &au1RouteDistinguisher[4],
                            sizeof (UINT4));
                    u2ASN = OSIX_NTOHS (u2ASN);
                    u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
                    CliPrintf (CliHandle, "%u:%u\r\n", u2ASN, u4AsignedNumber);
                }
                else if (L2VPN_VPLS_RD_TYPE_1 == au1RouteDistinguisher[0])
                {
                    MEMCPY (&u2AsignedNumber, &au1RouteDistinguisher[6],
                            sizeof (UINT2));
                    u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
                    CliPrintf (CliHandle, "%u.%u.%u.%u:%u\r\n",
                               au1RouteDistinguisher[2],
                               au1RouteDistinguisher[3],
                               au1RouteDistinguisher[4],
                               au1RouteDistinguisher[5], u2AsignedNumber);
                }
                else
                {
                    MEMCPY (&u4ASN, &au1RouteDistinguisher[2], sizeof (UINT4));
                    MEMCPY (&u2AsignedNumber, &au1RouteDistinguisher[6],
                            sizeof (UINT2));
                    u4ASN = OSIX_NTOHL (u4ASN);
                    u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
                    CliPrintf (CliHandle, "%u.%u:%u\r\n",
                               (u4ASN & 0xffff0000) >> 16, u4ASN & 0x0000ffff,
                               u2AsignedNumber);
                }
            }
        }
    }

    L2VpnCliShowRunningRt (CliHandle, L2VPN_VPLS_INDEX (pVplsEntry));

    if (L2VPN_VPLS_DEFAULT_MTU != L2VPN_VPLS_MTU (pVplsEntry))
    {
        CliPrintf (CliHandle, "mtu %d,", L2VPN_VPLS_MTU (pVplsEntry));
    }

    if (L2VPN_ENABLED == L2VPN_VPLS_CONTROL_WORD (pVplsEntry))
    {
        CliPrintf (CliHandle, "control-word enable\r\n");
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (L2VPN_VPLS_INDEX (pVplsEntry),
                                                  &u4VEId);
    if (L2VPN_SUCCESS == i4RetStatus)
    {
        i4RetStatus = nmhGetVplsBgpVEName (L2VPN_VPLS_INDEX (pVplsEntry),
                                           u4VEId, &tVplsBgpVEName);
        if (SNMP_SUCCESS == i4RetStatus)
        {
            if (L2VPN_ZERO != tVplsBgpVEName.i4_Length)
            {
                CliPrintf (CliHandle, "site-identifier %d site-name %s\r\n",
                           u4VEId, tVplsBgpVEName.pu1_OctetList);
            }
            else
            {
                CliPrintf (CliHandle, "site-identifier %d\r\n", u4VEId);
            }
        }
    }
}

/******************************************************************************
* Function Name : L2VpnCliShowRunningRt
* Description   : This routine is used to Display a Rt Details for a 
*                 particular VPLS 
* Input(s)      : CliHandle  - Context in which the command is processed
*                 u4VplsIndex - Vpls Index
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpnCliShowRunningRt (tCliHandle CliHandle, UINT4 u4VplsIndex)
{
    tSNMP_OCTET_STRING_TYPE tVplsBgpRteTargetRT;
    UINT4               u4RtIndex = L2VPN_ZERO;
    UINT4               u4NextRtIndex = L2VPN_ZERO;
    UINT4               u4NextVplsIndex = L2VPN_ZERO;
    UINT4               u4ASN = L2VPN_ZERO;
    UINT4               u4AsignedNumber = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RTType = L2VPN_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT2               u2ASN = L2VPN_ZERO;
    UINT2               u2AsignedNumber = L2VPN_ZERO;
    UINT1               au1VplsBgpRteTargetRT[L2VPN_MAX_VPLS_RT_LEN];

    MEMSET (au1VplsBgpRteTargetRT, L2VPN_ZERO, L2VPN_MAX_VPLS_RT_LEN);
    MEMSET (&tVplsBgpRteTargetRT, L2VPN_ZERO, sizeof (tVplsBgpRteTargetRT));

    tVplsBgpRteTargetRT.pu1_OctetList = au1VplsBgpRteTargetRT;

    i4RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                        &u4NextVplsIndex,
                                                        u4RtIndex,
                                                        &u4NextRtIndex);

    while (SNMP_SUCCESS == i4RetStatus)
    {
        u4RtIndex = u4NextRtIndex;

        if (u4VplsIndex != u4NextVplsIndex)
        {
            break;
        }

        i4RetStatus = nmhGetVplsBgpRteTargetRTRowStatus (u4VplsIndex,
                                                         u4RtIndex,
                                                         &i4RowStatus);
        if (ACTIVE != i4RowStatus)
        {
            i4RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                                &u4NextVplsIndex,
                                                                u4RtIndex,
                                                                &u4NextRtIndex);
            continue;
        }

        i4RetStatus = nmhGetVplsBgpRteTargetRT (u4VplsIndex,
                                                u4RtIndex,
                                                &tVplsBgpRteTargetRT);
        if (L2VPN_VPLS_RT_TYPE_0 == tVplsBgpRteTargetRT.pu1_OctetList[0])
        {
            MEMCPY (&u2ASN, &tVplsBgpRteTargetRT.pu1_OctetList[2],
                    sizeof (UINT2));
            MEMCPY (&u4AsignedNumber, &tVplsBgpRteTargetRT.pu1_OctetList[4],
                    sizeof (UINT4));
            u2ASN = OSIX_NTOHS (u2ASN);
            u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
            CliPrintf (CliHandle, "rt %u:%u", u2ASN, u4AsignedNumber);
        }
        else if (L2VPN_VPLS_RT_TYPE_1 == tVplsBgpRteTargetRT.pu1_OctetList[0])
        {
            MEMCPY (&u2AsignedNumber, &tVplsBgpRteTargetRT.pu1_OctetList[6],
                    sizeof (UINT2));
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, "rt %u.%u.%u.%u:%u",
                       tVplsBgpRteTargetRT.pu1_OctetList[2],
                       tVplsBgpRteTargetRT.pu1_OctetList[3],
                       tVplsBgpRteTargetRT.pu1_OctetList[4],
                       tVplsBgpRteTargetRT.pu1_OctetList[5], u2AsignedNumber);
        }
        else
        {
            MEMCPY (&u4ASN, &tVplsBgpRteTargetRT.pu1_OctetList[2],
                    sizeof (UINT4));
            MEMCPY (&u2AsignedNumber, &tVplsBgpRteTargetRT.pu1_OctetList[6],
                    sizeof (UINT2));
            u4ASN = OSIX_NTOHL (u4ASN);
            u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
            CliPrintf (CliHandle, "rt %u.%u:%u", (u4ASN & 0xffff0000) >> 16,
                       u4ASN & 0x0000ffff, u2AsignedNumber);
        }

        i4RetStatus = nmhGetVplsBgpRteTargetRTType (u4VplsIndex,
                                                    u4RtIndex, &i4RTType);
        if (L2VPN_VPLS_RT_EXPORT == i4RTType)
        {
            CliPrintf (CliHandle, " export\r\n");
        }
        else if (L2VPN_VPLS_RT_IMPORT == i4RTType)
        {
            CliPrintf (CliHandle, " import\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " both\r\n");
        }

        i4RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                            &u4NextVplsIndex,
                                                            u4RtIndex,
                                                            &u4NextRtIndex);
    }
}

/******************************************************************************
* Function Name : L2VpnCliShowRunningVplsAc
* Description   : This routine is used to Display a AC Details for a 
*                 particular VPLS 
* Input(s)      : CliHandle  - Context in which the command is processed
*                 u4VplsIndex - Vpls Index
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
L2VpnCliShowRunningVplsAc (tCliHandle CliHandle, UINT4 u4VplsIndex,
                           INT4 i4TestEnetIfIndex, UINT4 u4TestPortVlanId)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    UINT4               u4AcIndex = L2VPN_ZERO;
    UINT4               u4NextAcIndex = L2VPN_ZERO;
    UINT4               u4NextVplsIndex = L2VPN_ZERO;
    UINT4               u4IfIndex = L2VPN_ZERO;
    UINT4               u4Vlan = L2VPN_ZERO;
    INT4                i4RowStatus = L2VPN_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT1               au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1];

    MEMSET (au1VplsName, L2VPN_ZERO, L2VPN_MAX_VPLS_NAME_LEN + 1);

    VplsName.pu1_OctetList = au1VplsName;
    VplsName.i4_Length = L2VPN_MAX_VPLS_NAME_LEN;

    if (SNMP_FAILURE == nmhGetFsMplsVplsName (u4VplsIndex, &VplsName))
    {
        return;
    }

    while (1)
    {
        i4RetStatus = nmhGetNextIndexFsMplsVplsAcMapTable (u4VplsIndex,
                                                           &u4NextVplsIndex,
                                                           u4AcIndex,
                                                           &u4NextAcIndex);
        if (SNMP_FAILURE == i4RetStatus)
        {
            break;
        }

        u4AcIndex = u4NextAcIndex;

        if (u4VplsIndex != u4NextVplsIndex)
        {
            break;
        }

        i4RetStatus = nmhGetFsMplsVplsAcMapRowStatus (u4VplsIndex,
                                                      u4AcIndex, &i4RowStatus);
        if (ACTIVE != i4RowStatus)
        {
            continue;
        }

        i4RetStatus = nmhGetFsMplsVplsAcMapPortVlan (u4VplsIndex,
                                                     u4AcIndex, &u4Vlan);
        if ((u4TestPortVlanId != u4Vlan) && (u4TestPortVlanId != 0))
        {
            continue;
        }

        i4RetStatus = nmhGetFsMplsVplsAcMapPortIfIndex (u4VplsIndex,
                                                        u4AcIndex, &u4IfIndex);

        if ((UINT4) i4TestEnetIfIndex != u4IfIndex)
        {
            continue;
        }

        CliPrintf (CliHandle, "xconnect vfi %s", VplsName.pu1_OctetList);

        if (L2VPN_VPLSAC_INVALID_PORT_INDEX != u4Vlan
            && u4Vlan != L2VPN_PWVC_ENET_VLAN)
        {
            if (u4IfIndex != L2VPN_ZERO)
            {
                CliPrintf (CliHandle, " port-vlan vlan %d", u4Vlan);
            }
        }

        CliPrintf (CliHandle, " \r\n");
    }
}
#endif
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
INT4
L2VpnShowVplsHwList (tCliHandle CliHandle)
{
    tL2VpnVplsHwList    L2VpnVplsHwListEntry;
    if (L2VpnVplsHwListGetFirst (&L2VpnVplsHwListEntry) == MPLS_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Entry present in HW List\r\n");
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Vpls Index |EnetIndex |NPAPI Status\r\n");
    do
    {
        CliPrintf (CliHandle, "%10d",
                   L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwListEntry));
        CliPrintf (CliHandle, "%10d",
                   L2VPN_VPLS_HW_LIST_ENET_INDEX (&L2VpnVplsHwListEntry));
        CliPrintf (CliHandle, "%10d\n",
                   L2VPN_VPLS_HW_LIST_NPAPI_STATUS (&L2VpnVplsHwListEntry));
    }
    while (L2VpnVplsHwListGetNext (&L2VpnVplsHwListEntry, &L2VpnVplsHwListEntry)
           != MPLS_FAILURE);

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : L2VpnGrCliShowHwList
 * * Description   : This routine is used to Display a PW entries in hw-list
 * *                 
 * * Input(s)      : CliHandle  - Context in which the command is processed
 * *                 
 * * Output(s)     : None
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *******************************************************************************/
INT4
L2VpnGrCliShowHwList (tCliHandle CliHandle)
{
    tL2VpnPwHwList      L2VpnPwHwListEntry;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (L2VpnHwListGetFirst (&L2VpnPwHwListEntry) == MPLS_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Entry present in HW List\r\n");
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle,
               " VPLS  |PW    |LDP-Entity |PW         |PW          |PEER        |Remote |Out      |In       |In-LSP    |Out-LSP    |NPAPI  |Stale | AcId\r\n");
    CliPrintf (CliHandle,
               " Index |Index |Index      |In-VCLabel |Out-VCLabel |Address     |VE-ID  |If-Index |If-Index |Label     |Label      |Status |Status\r\n");
    CliPrintf (CliHandle,
               " ===== |===== |========== |========== |=========== |=========== |====== |======== |======== |========= |========== |====== |======\r\n");
    do
    {
        CliPrintf (CliHandle, " %d",
                   L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "        %d",
                   L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "         %d",
                   L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "         %d",
                   L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "      %d",
                   L2VPN_PW_HW_LIST_OUT_LABEL (&L2VpnPwHwListEntry));
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE ==
            L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE (&L2VpnPwHwListEntry))
        {
            CliPrintf (CliHandle, "     %s",
                       Ip6PrintAddr (&
                                     (L2VPN_PW_HW_LIST_PEER_ADDRESS
                                      (&L2VpnPwHwListEntry).IpAddress.
                                      Ip6Addr)));
        }
        else
#endif
        {
            CliPrintf (CliHandle, "     %d.%d.%d.%d",
                       L2VPN_PW_HW_LIST_PEER_IP_ADDRESS (&L2VpnPwHwListEntry)
                       [0],
                       L2VPN_PW_HW_LIST_PEER_IP_ADDRESS (&L2VpnPwHwListEntry)
                       [1],
                       L2VPN_PW_HW_LIST_PEER_IP_ADDRESS (&L2VpnPwHwListEntry)
                       [2],
                       L2VPN_PW_HW_LIST_PEER_IP_ADDRESS (&L2VpnPwHwListEntry)
                       [3]);
        }
        CliPrintf (CliHandle, "      %d",
                   L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "        %d",
                   L2VPN_PW_HW_LIST_OUT_IF_INDEX (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "       %d",
                   L2VPN_PW_HW_LIST_IN_IF_INDEX (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "      %d",
                   L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "    %d",
                   L2VPN_PW_HW_LIST_LSP_OUT_LABEL (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "       %d",
                   L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "       %d",
                   L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry));
        CliPrintf (CliHandle, "       %d\n",
                   L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntry));
    }
    while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
           != MPLS_FAILURE);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return CLI_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : L2VpnIpValidateAndGenerate                                */
/* Description   : This routine checks validate and generate the ipv4/ipv6   */
/*                 address from random string                                */
/* Input         : pu1RandomString - IP Adress to be checked                 */
/* Output(s)     : pu1addrType  - Address type (ipv4/ipv6)                   */
/*                 pu1IpAddress - IP adress                                  */
/* Return(s)     : L2VPN_SUCCESS or  L2VPN_FAILURE                           */
/*****************************************************************************/
INT4
L2VpnIpValidateAndGenerate (UINT1 *pu1RandomString,
                            UINT1 *pu1addrType, UINT1 *pu1IpAddress)
{
    INT1                i1DotFnd = L2VPN_ZERO;
    tUtlInAddr          u4UtlInAddr;
    i1DotFnd = CliIsDelimit ('.', (CONST CHR1 *) pu1RandomString);    /*finding incoming address string is ipv4 or ipv6 */
    if (0 != i1DotFnd)
    {
        if (0 == UtlInetAton ((CHR1 *) pu1RandomString, &u4UtlInAddr))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "failed in converting incoming ascii string to integer \n");
            return L2VPN_FAILURE;
        }
        u4UtlInAddr.u4Addr = OSIX_NTOHL (u4UtlInAddr.u4Addr);
        MEMCPY (pu1IpAddress, &(u4UtlInAddr.u4Addr), IPV4_ADDR_LENGTH);
        *pu1addrType = MPLS_IPV4_ADDR_TYPE;    /*storing the address type */
        return L2VPN_SUCCESS;
    }

    /*ipv6 address conversion */
#ifdef MPLS_IPV6_WANTED
    else if (0 != CliIsDelimit (':', (CONST CHR1 *) pu1RandomString))    /*finding incoming address string is ipv4 or ipv6 */
    {

        INET_ATON6 (pu1RandomString, pu1IpAddress);    /*converting incoming ascii string to integer */
        *pu1addrType = MPLS_IPV6_ADDR_TYPE;
        return L2VPN_SUCCESS;
    }
#endif
    else
    {

        return L2VPN_FAILURE;
    }
}
#endif

INT4
L2VpnCheckforAC (tL2vpnCliArgs * pL2vpnCliArgs)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pInPwVcEntry = NULL;
    UINT4               u4PwIndex = 0;
    UINT4               u4InIndex = 0;

    for (u4InIndex = 1; u4InIndex < gL2VpnGlobalInfo.u4MaxVplsEntries;
         u4InIndex++)
    {
        /*for (u4PwIndex = 1; u4PwIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwIndex++)
           { */
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4InIndex);
        if (NULL == pVplsEntry)
        {
            continue;
        }
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

            for (u4PwIndex = 1;
                 u4PwIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo);
                 u4PwIndex++)
            {
                pInPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4InIndex);
                pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
                if ((pInPwVcEntry == NULL) || (pPwVcEntry == NULL))
                {
                    continue;
                }

                if (L2VPN_PWVC_VPLS_INDEX (pInPwVcEntry) !=
                    L2VPN_PWVC_VPLS_INDEX (pPwVcEntry))
                {

                    if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
                    {
                        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                                      ((tPwVcEnetServSpecEntry *)
                                       L2VPN_PWVC_ENET_ENTRY (pPwEntry)),
                                      pPwVcEnetEntry, tPwVcEnetEntry *)
                        {
                            if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                                 pL2vpnCliArgs->u4VlanId)
                                &&
                                (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)
                                 == pL2vpnCliArgs->i4PortIfIndex))
                            {
                                return L2VPN_FAILURE;
                            }
                            else
                            {
                                continue;
                            }
                        }

                    }
                }
            }
        }
    }
    return L2VPN_SUCCESS;
}
