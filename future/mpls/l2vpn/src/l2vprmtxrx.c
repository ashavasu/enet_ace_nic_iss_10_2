
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vprmtxrx.c,v 1.3 2015/01/21 11:04:11 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/

#include "l2vpincs.h"

/*******************************************************************
  Function Name   :L2VpnRmSendVplsHwListEntry                     
  Description     :This function will be invoked from             
                   L2VpnVplsHwListAddUpdate and L2VpnVplsHwListDelete 
                   in active node  when it gets PWVC Hwlist entry and
                   send this to  standby calling L2VpnSendMsgToRm          
  Input(s)        :pVplsHwList-The received HwList entry     
                                                             
  Output(s)       :None                                      
********************************************************************/

VOID L2VpnRmSendVplsHwListEntry (tL2VpnVplsHwList *pVplsHwList, UINT1 u1OpType)
{

    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = L2VPN_ZERO;
    UINT2               u2MsgLength = L2VPN_ZERO;
    UINT2               u2RmMsgLength = L2VPN_ZERO;


    /* Check If the node is active if not return */
    if (L2VPN_IS_NODE_ACTIVE () != L2VPN_RM_TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Node is Standby\n");       
        return;
    }
    /* Check If istandby exist if not return */
    if (L2VPN_IS_STANDBY_UP () == L2VPN_RM_FALSE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "No Standby is existing\n");
        return;
    }
    if (NULL == pVplsHwList)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "No Entry in HW List\n");
        return;
    }
    u2MsgLength = (UINT2)sizeof (tL2VpnVplsHwList);
    u2RmMsgLength =(UINT2) ((UINT2)L2VPN_RM_MSG_HDR_SIZE + u2MsgLength +(UINT2) sizeof (UINT1));
    /* Allocate memory for RM message */
    pMsg = CRU_BUF_Allocate_MsgBufChain (u2RmMsgLength, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return;
    }
    /* Fill the Message Type */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, L2VPN_RM_SYNC_VPLS_HWLIST_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    /* Fill the Message  Length */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);
    /* ADD or DELETE type */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1OpType);
    u4Offset = u4Offset + sizeof (UINT1);

    BUF_COPY_OVER_CHAIN(pMsg, pVplsHwList, u4Offset, sizeof(tL2VpnVplsHwList));
    u4Offset = u4Offset + sizeof(tL2VpnVplsHwList);
    /* Call RM Interface to send the Message */
    if (L2VpnRmSendMsgToRm (pMsg, u2RmMsgLength) != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Sending Message to RM is failed\n");
        return;
    }
    return;
}


/*******************************************************************
  Function Name   :L2VpnRmSendPwVcHwListEntry                     
  Description     :This function will be invoked from             
                   L2VpnPwHwListAddUpdate and L2VpnPwHwListDelete 
                   in active node  when it gets PWVC Hwlist entry and
                   send this to  standby calling L2VpnSendMsgToRm          
  Input(s)        :pPwVcHwList-The received HwList entry     
                                                             
  Output(s)       :None                                      
********************************************************************/

VOID L2VpnRmSendPwVcHwListEntry (tL2VpnPwHwList *pPwVcHwList, UINT1 u1OpType)
{

    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = L2VPN_ZERO;
    UINT2               u2MsgLength = L2VPN_ZERO;
    UINT2               u2RmMsgLength = L2VPN_ZERO;


    /* Check If the node is active if not return */
    if (L2VPN_IS_NODE_ACTIVE () != L2VPN_RM_TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Node is Standby\n");       
        return;
    }
    /* Check If istandby exist if not return */
    if (L2VPN_IS_STANDBY_UP () == L2VPN_RM_FALSE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "No Standby is existing\n");
        return;
    }
    if (NULL == pPwVcHwList)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "No Entry in HW List\n");
        return;
    }
    u2MsgLength = (UINT2)sizeof (tL2VpnPwHwList);
    u2RmMsgLength =(UINT2) ((UINT2)L2VPN_RM_MSG_HDR_SIZE + u2MsgLength +(UINT2) sizeof (UINT1));
    /* Allocate memory for RM message */
    pMsg = CRU_BUF_Allocate_MsgBufChain (u2RmMsgLength, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return;
    }
    /* Fill the Message Type */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, L2VPN_RM_SYNC_HWLIST_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    /* Fill the Message  Length */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);
    /* ADD or DELETE type */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1OpType);
    u4Offset = u4Offset + sizeof (UINT1);

    BUF_COPY_OVER_CHAIN(pMsg, pPwVcHwList, u4Offset, sizeof(tL2VpnPwHwList));
    u4Offset = u4Offset + sizeof(tL2VpnPwHwList);
    /* Call RM Interface to send the Message */
    if (L2VpnRmSendMsgToRm (pMsg, u2RmMsgLength) != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Sending Message to RM is failed\n");
        return;
    }
    return;
}
/*************************************************************
 *   Function Name   :L2VpnRmProcessSyncVplsHwListEntry
 *   Description     :This function is invoked at standby node
 *                    to Process Dynamic sync
 *   Input(s)        :pRmMesg - The message received from RM
 *   Output(s)       :None
 *   Returns         :None
 *************************************************************/
INT4 L2VpnRmProcessSyncVplsHwListEntry(tRmMsg *pRmMesg, UINT4 *pu4Offset)
{

    UINT4            u4Offset = *pu4Offset ;
    UINT1            u1OpType = L2VPN_ZERO ;
    tL2VpnVplsHwList   L2VpnVplsHwListEntry;
    tL2VpnVplsHwList	 L2VpnVplsHwListEntryLocal;
    UINT1			 u1HwStatus = 0;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnVplsHwListEntry, L2VPN_ZERO, sizeof(tL2VpnVplsHwList));
    MEMSET (&L2VpnVplsHwListEntryLocal, L2VPN_ZERO, sizeof(tL2VpnVplsHwList));

    /* Check If the node is active then return*/
    if (L2VPN_IS_NODE_ACTIVE () == L2VPN_RM_TRUE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG ,"%s : Received  syn At active \n",__func__);
        return L2VPN_FAILURE;
    }

    /* Get type of operation to be performed */
    L2VPN_RM_GET_1_BYTE (pRmMesg, u4Offset, u1OpType);

    /* Get hw list entry to add into RBTree */ 
    L2VPN_RM_GET_N_BYTE (pRmMesg, &L2VpnVplsHwListEntry, u4Offset, sizeof(tL2VpnVplsHwList));
    u1HwStatus = L2VpnVplsHwListEntry.u1NpapiStatus;
    switch(u1OpType)
    {
        case ADD_VPLS:
            {
                /* Add to RBTree */
                L2VpnVplsHwListAddUpdate(&L2VpnVplsHwListEntry,u1HwStatus);
            }
            break;

        case ADD_UPDATE_VPLS:
            {
                if(L2VpnVplsHwListGet(&L2VpnVplsHwListEntry,&L2VpnVplsHwListEntryLocal)
                        == MPLS_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,"%s : VPLS Entry is not present in HW"
                            "List\n",__func__);
                    return L2VPN_FAILURE;	
                }
                L2VpnVplsHwListAddUpdate(&L2VpnVplsHwListEntryLocal,u1HwStatus);	
            }
            break;

        case DEL_VPLS:
            {

                if(L2VpnVplsHwListGet(&L2VpnVplsHwListEntry,&L2VpnVplsHwListEntryLocal)
                        == MPLS_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,"%s : VPLS Entry is not present in HW"
                            "List\n",__func__);
                    return L2VPN_FAILURE;
                }
                /* Delete the hw entry from RBTree */
                u1HwStatus = (UINT1)(MPLS_L2VPN_VPLS_HW_LIST_NPAPI_STATUS(&L2VpnVplsHwListEntryLocal) & (~u1HwStatus));
                L2VpnVplsHwListRemove(&L2VpnVplsHwListEntryLocal, u1HwStatus);
            }	
            break;	

        default:
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Invalid Message\n");
    }
    return L2VPN_SUCCESS;
}

/*************************************************************
 *   Function Name   :L2VpnRmProcessSyncHwListEntry
 *   Description     :This function is invoked at standby node
 *                    to Process Dynamic sync
 *   Input(s)        :pRmMesg - The message received from RM
 *   Output(s)       :None
 *   Returns         :None
 *************************************************************/
INT4 L2VpnRmProcessSyncHwListEntry(tRmMsg *pRmMesg, UINT4 *pu4Offset)
{

    UINT4            u4Offset = *pu4Offset ;
    UINT1            u1OpType = L2VPN_ZERO ;
    tL2VpnPwHwList   L2VpnPwHwListEntry;
    tL2VpnPwHwList	 L2VpnPwHwListEntryLocal;
    UINT1			 u1HwStatus = 0;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof(tL2VpnPwHwList));
    MEMSET (&L2VpnPwHwListEntryLocal, L2VPN_ZERO, sizeof(tL2VpnPwHwList));

    /* Check If the node is active then return*/
    if (L2VPN_IS_NODE_ACTIVE () == L2VPN_RM_TRUE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG ,"%s : Received  syn At active \n",__func__);
        return L2VPN_FAILURE;
    }

    /* Get type of operation to be performed */
    L2VPN_RM_GET_1_BYTE (pRmMesg, u4Offset, u1OpType);

    /* Get hw list entry to add into RBTree */ 
    L2VPN_RM_GET_N_BYTE (pRmMesg, &L2VpnPwHwListEntry, u4Offset, sizeof(tL2VpnPwHwList));
    u1HwStatus = L2VpnPwHwListEntry.u1NpapiStatus;
    switch(u1OpType)
    {
        case ADD_PW:
            {
                /* Add to RBTree */
                L2VpnPwHwListAddUpdate(&L2VpnPwHwListEntry,u1HwStatus);
            }
            break;

        case ADD_UPDATE_PW:
            {
                if(L2VpnHwListGet(&L2VpnPwHwListEntry,&L2VpnPwHwListEntryLocal)
                        == MPLS_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,"%s : PW Entry is not present in HW"
                            "List\n",__func__);
                    return L2VPN_FAILURE;	
                }

                u1HwStatus = (UINT1) ((u1HwStatus) & (~(MPLS_PW_HW_LIST_NPAPI_STATUS(&L2VpnPwHwListEntryLocal))));
				if (u1HwStatus == MPLS_PWAC_NPAPI_CALLED)
				{
					L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListEntryLocal) =
						L2VpnPwHwListEntry.u4AcId;
				}	
                if(u1HwStatus == MPLS_PWILM_NPAPI_CALLED)
                {
                    L2VPN_PW_HW_LIST_LSP_IN_LABEL(&L2VpnPwHwListEntryLocal) =
                        L2VpnPwHwListEntry.u4LspInLabel;
                    L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntryLocal) =
                        L2VpnPwHwListEntry.u4InLabel;
                    L2VPN_PW_HW_LIST_IN_IF_INDEX(&L2VpnPwHwListEntryLocal) =  
                        L2VpnPwHwListEntry.u4InIfIndex;
					L2VpnHwListUpdate(&L2VpnPwHwListEntryLocal);
                }			
                L2VpnPwHwListAddUpdate(&L2VpnPwHwListEntryLocal,u1HwStatus);	
            }
            break;

        case DEL_PW:
            {

                if(L2VpnHwListGet(&L2VpnPwHwListEntry,&L2VpnPwHwListEntryLocal)
                        == MPLS_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,"%s : PW Entry is not present in HW"
                            "List\n",__func__);
                    return L2VPN_FAILURE;
                }
                u1HwStatus = (UINT1)(MPLS_PW_HW_LIST_NPAPI_STATUS(&L2VpnPwHwListEntryLocal) & (~u1HwStatus));
                /* Delete the hw entry from RBTree */
                L2VpnPwHwListDelete(&L2VpnPwHwListEntryLocal, u1HwStatus);
            }	
            break;	

        default:
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Invalid Message\n");
    }
    return L2VPN_SUCCESS;
}
/*****************************************************************************/
/* Function Name : L2VpnRmSendBulkPwVcInfo                                   */
/* Description   : This function is invoked from RM in active                */
/*                 node when RM gets the bulk update request                 */
/*                 and sends out the bulk update packet for PwVc Entry       */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)	 : None													     */
/*****************************************************************************/
VOID L2VpnRmSendBulkPwVcInfo()
{

    tRmMsg              *pBuf = NULL;
    tL2VpnPwHwList      PwVcHwListEntry;
    UINT4               u4Offset = L2VPN_ZERO;
    UINT2               u2Count = L2VPN_ZERO ;
    UINT2               u2MaxPwVcEntries = L2VPN_ZERO;

    /* Calculate Max no. of hw list entry can be sent to RM
       after dividing total RM message length  by sizeof tL2VpnPwHwList structure */
    u2MaxPwVcEntries = (L2VPN_RM_SYN_MAX_SIZE - (L2VPN_RM_MSG_HDR_SIZE + sizeof(UINT2)))/sizeof(tL2VpnPwHwList);
    pBuf = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_SYN_MAX_SIZE, L2VPN_ZERO);
    if(NULL == pBuf)
    {

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory allocation Failed\n");
        L2VpnRmSendBulkAbort (RM_MEMALLOC_FAIL);
        gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_ABORTED;
        return;
    }

    L2VPN_RM_PUT_1_BYTE(pBuf, u4Offset, L2VPN_RM_BULK_UPDATE_MSG);

    /* Location for Data_Lenght, which will be filled later in the code below
       when data lengnth is available. For now, move the offset*/
    u4Offset = u4Offset + sizeof (UINT2);

    /* Move the offset for count to be fill later*/
    u4Offset = u4Offset + sizeof (UINT2);

    /* Get hw list first entry */
    if (L2VpnHwListGetFirst(&PwVcHwListEntry) == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "HwList is Empty\n");
        RM_FREE (pBuf);
        return;
    }
    else 
    {
        BUF_COPY_OVER_CHAIN(pBuf, &PwVcHwListEntry, u4Offset, sizeof(tL2VpnPwHwList));
        u4Offset = u4Offset + sizeof(tL2VpnPwHwList);
        u2Count++;
    }   
    /* Get next hw list entry from RBTree */ 
    while(L2VpnHwListGetNext(&PwVcHwListEntry, &PwVcHwListEntry) != MPLS_FAILURE)
    {

        BUF_COPY_OVER_CHAIN(pBuf, &PwVcHwListEntry, u4Offset, sizeof(tL2VpnPwHwList)); 
        u4Offset = u4Offset + sizeof(tL2VpnPwHwList);
        u2Count++;
        if(u2Count == u2MaxPwVcEntries)
        {
            RM_DATA_ASSIGN_2_BYTE (pBuf, L2VPN_RM_BULK_LEN_OFFSET, (UINT2)u4Offset);
            RM_DATA_ASSIGN_2_BYTE (pBuf, L2VPN_RM_MSG_HDR_SIZE, u2Count);

             /* pBuf is freed by L2VpnRmSendMsgToRm, if it fails*/
            if (L2VpnRmSendMsgToRm (pBuf,(UINT2) u4Offset) == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"sending Msg to RM failed\n");
                gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_ABORTED;
                return ;
            }
            else /* If RM_Send is success, Allocate new buffer for next set of data*/
            {
                pBuf = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_SYN_MAX_SIZE, L2VPN_ZERO);
                if(NULL == pBuf)
                {

                  L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory allocation Failed\n");
                  L2VpnRmSendBulkAbort (RM_MEMALLOC_FAIL);
                  gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_ABORTED;
                  return;
                }

                u4Offset = L2VPN_ZERO;
                L2VPN_RM_PUT_1_BYTE (pBuf, u4Offset, L2VPN_RM_BULK_UPDATE_MSG);

                /* Msg length to be filled later */
                u4Offset = u4Offset + sizeof (UINT2);

                /* Move the offset to fill no of PwVc entries later*/
                u4Offset = u4Offset + sizeof (UINT2);
                /* Reset the count*/
                u2Count = L2VPN_ZERO;

            }
        }
    }

    /*Need to send Msg if Count has not reached Max, hence not sent already*/
    if ( u2Count > L2VPN_ZERO )
    {
        RM_DATA_ASSIGN_2_BYTE (pBuf, L2VPN_RM_BULK_LEN_OFFSET, (UINT2)u4Offset);
        RM_DATA_ASSIGN_2_BYTE (pBuf, L2VPN_RM_MSG_HDR_SIZE, u2Count);
        /* pBuf is freed by L2VpnRmSendMsgToRm, if it fails */
        if(L2VpnRmSendMsgToRm (pBuf,(UINT2) u4Offset) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"sending Msg to RM failed\n");
            gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_ABORTED;
            return ;
        }
    }
    else /*No Need to send any message, free the buffer*/
    {
        RM_FREE (pBuf);
    }
    return ;
}


/*************************************************************
 *   Function Name   :L2VpnRmProcessBulkUpdateMsg
 *   Description     :This function is invoked at standby node
 *                    to Process bulk update
 *     
 *   Input(s)        :pMsg- The message received from RM
 *                    u2MsgLen - Total Message Length
 *   Output(s)       :None
 *   Returns         :None
 *************************************************************/

VOID L2VpnRmProcessBulkUpdateMsg (tRmMsg* pMsg, UINT4 *pu4Offset)
{

    UINT2                    u2Count = L2VPN_ZERO;
    UINT4                    u4Offset = *pu4Offset ;
    UINT1                    u1Range = L2VPN_ZERO;
    tL2VpnPwHwList           PwVcHwListEntry ;

    gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus = L2VPN_RM_BLKUPDT_INPROGRESS;
    if (L2VPN_IS_NODE_ACTIVE () == L2VPN_RM_TRUE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"Received Bulk syn At active \n");
        return;
    }
    /* Get number of entry to be added to RBTree */
    L2VPN_RM_GET_2_BYTE (pMsg, u4Offset, u2Count);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG , "u2Count = %d\n", u2Count);
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Add to RBTree\n");

    for (u1Range = L2VPN_ZERO; u1Range < u2Count; u1Range++)
    {
#if 0
        if(L2VpnAllocateMemoryForHwList(&pPwVcHwListEntry) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"Memory Allocation failed\n");
            return ;
        }
        MEMSET( pPwVcHwListEntry,0,sizeof(tL2VpnPwHwList));
#endif
        MEMSET(&PwVcHwListEntry, 0, sizeof(tL2VpnPwHwList));
        L2VPN_RM_GET_N_BYTE (pMsg, &PwVcHwListEntry, u4Offset, sizeof(tL2VpnPwHwList));
        L2VpnHwListAdd(&PwVcHwListEntry);        
    }

}

