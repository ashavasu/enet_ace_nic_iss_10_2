/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpmain.c,v 1.134 2017/06/19 12:26:15 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *****************************************************************************
 *    FILE  NAME             : l2vpmain.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains entry point function to
 *                             L2VPN and its supporting routines.
 *
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"
#include  "l2vpgbl.h"
#include "ldpext.h"
#include  "mplslsr.h"
#include  "arp.h"
#include  "fsvlan.h"
#include  "indexmgr.h"
#include  "mplsnp.h"
#include  "rpteext.h"
#include  "fssyslog.h"
#include  "fsmplswr.h"
#include "mplsred.h"
#include "iss.h"
#include "la.h"
#include "mplsincs.h"

PRIVATE INT4        L2VpnFec128RbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
PRIVATE INT4        L2VpnFec129RbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
PRIVATE INT4        L2VpnFec129RbTreeVplsCmpFunc (tRBElem * pRBElem1,
                                                  tRBElem * pRBElem2);
PRIVATE INT4        L2VpnPeerAddrRbTreeCmpFunc (tRBElem * pRBElem1,
                                                tRBElem * pRBElem2);
PRIVATE INT4        L2VpnCreateRBTree (VOID);
PRIVATE VOID        L2VpnDeleteRBTree (VOID);
PRIVATE INT4        L2vpnAddMappingList (tPwVcEntry * pPwVcEntry, UINT1 u1Type);
PRIVATE INT4        L2vpnAddInMappingList (tPwVcEntry * pPwVcEntry,
                                           UINT1 u1Type);
PRIVATE INT4        L2vpnDelMappingList (tPwVcEntry * pPwVcEntry, UINT1 u1Type);
PRIVATE INT4        L2vpnDelInMappingList (tPwVcEntry * pPwVcEntry,
                                           UINT1 u1Type);
PRIVATE INT4        L2VpnTeRbTreeCmpFunc (tRBElem * pRBElem1,
                                          tRBElem * pRBElem2);
PRIVATE INT4        L2VpnTeInRbTreeCmpFunc (tRBElem * pRBElem1,
                                            tRBElem * pRBElem2);
PRIVATE INT4        L2VpnNonTeRbTreeCmpFunc (tRBElem * pRBElem1,
                                             tRBElem * pRBElem2);
PRIVATE INT4        L2VpnNonTeInRbTreeCmpFunc (tRBElem * pRBElem1,
                                               tRBElem * pRBElem2);
PRIVATE INT4        L2VpnVcOnlyRbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
PRIVATE INT4        L2VpnVplsVfiRbTreeCmpFunc (tRBElem * pRBElem1,
                                               tRBElem * pRBElem2);
#ifdef VPLSADS_WANTED
PRIVATE INT4        L2VpnVplsRdRbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
PRIVATE INT4        L2VpnVplsRtRbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
PRIVATE INT4        L2VpnVplsVeRbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
PRIVATE INT4        L2VpnVplsAcRbTreeCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
#endif
#ifdef HVPLS_WANTED
PRIVATE INT4        L2VpnVplsBindRbTreeCmpFunc (tRBElem * pRBElem1,
                                                tRBElem * pRBElem2);
#endif
PRIVATE INT4        L2VpnPwVcInLblRbTreeCmpFunc (tRBElem * pRBElem1,
                                                 tRBElem * pRBElem2);
PRIVATE VOID        L2vpnPwEnetMplsDelete (tPwVcEntry * pPwVcEntry);

PRIVATE INT4        L2VpnMplsPortEntryRbTreeCmpFunc (tRBElem * pRBElem1,
                                                     tRBElem * pRBElem2);
UINT2               gu2BgpVplsLblGroupId;

extern UINT4        FsMplsVplsRowStatus[14];
extern UINT4        FsMplsL2VpnVplsIndex[14];
/*UINT1               MplsFsMplsHwGetPwCtrlChnlCapabilities (UINT1
        *pu1PwCCTypeCapabs);*/
/*****************************************************************************/
/* Function Name : L2VpnDeInit                                               */
/* Description   : This routine does a graceful shut down of L2VPN Service   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnDeInit (VOID)
{
    tL2VpnQMsg         *pL2VpnQMsg = NULL;

    while (OsixQueRecv (L2VPN_QID, (UINT1 *) (&pL2VpnQMsg), OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)

    {
        if (MemReleaseMemBlock (L2VPN_Q_POOL_ID, (UINT1 *) pL2VpnQMsg)
            == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - Queue msg\r\n");
        }
    }
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        L2VpnDisableL2VpnService (L2VPN_ADMIN_DOWN);
    }
    if (L2VPN_TIMER_LIST_ID != L2VPN_ZERO)
    {
        TmrDeleteTimerList (L2VPN_TIMER_LIST_ID);
        L2VPN_TIMER_LIST_ID = L2VPN_ZERO;
    }
    L2VPN_INITIALISED = FALSE;
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "L2VPN Deinitialization successful\r\n");
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnTaskMain                                             */
/* Description   : This is the main routine that handles all the events and  */
/*                 inputs related to Timer, LDP and Q messages.              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnTaskMain (INT1 *pDummy)
{
    UINT4               u4RcvdEvent;
#ifdef L2VPN_HA_WANTED
    INT4                i4RetVal = L2VPN_FAILURE;
#endif
    /* Dummy pointers for system sizing */
    tPwVcMaxIfStrLenSize *pPwVcMaxIfStrLenSize = NULL;
    tPwVcInfoSize      *pPwVcInfoSize = NULL;
    tVPLSInfoSize      *pVPLSInfoSize = NULL;
    tL2vpnDefMTUSize   *pL2vpnDefMTUSize = NULL;

    UNUSED_PARAM (pPwVcMaxIfStrLenSize);
    UNUSED_PARAM (pPwVcInfoSize);
    UNUSED_PARAM (pVPLSInfoSize);
    UNUSED_PARAM (pL2vpnDefMTUSize);
    L2VPN_SUPPRESS_WARNING (pDummy);

    /* Create Message Q for LDP module */
    if (OsixQueCrt ((UINT1 *) L2VPN_QNAME, OSIX_MAX_Q_MSG_LEN,
                    FsL2VPNSizingParams[MAX_L2VPN_Q_MSG_SIZING_ID].
                    u4PreAllocatedUnits, &L2VPN_QID) != OSIX_SUCCESS)

    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN_Q creation failed\r\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Create Message Q for LDP module */
    if (OsixQueCrt
        ((UINT1 *) L2VPN_RESP_QNAME, OSIX_MAX_Q_MSG_LEN, L2VPN_RESP_QDEPTH,
         &L2VPN_RES_QID) != OSIX_SUCCESS)

    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN_RESP_Q creation failed\r\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Create Semaphore for Mutual exclusion on access of common
     * data structure between L2 VPN and Other tasks */
    if ((OsixCreateSem ((const UINT1 *) L2VPN_SEM_NAME,
                        1, 0, (tOsixSemId *) (&L2VPN_SEM_ID))) != OSIX_SUCCESS)

    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Semaphore creation failed\r\n");
        OsixQueDel (L2VPN_QID);
        OsixQueDel (L2VPN_RES_QID);
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Initialise L2-VPN module */
    if (L2VpnInit () != L2VPN_SUCCESS)
    {
        OsixQueDel (L2VPN_QID);
        OsixQueDel (L2VPN_RES_QID);
        OsixSemDel (L2VPN_SEM_ID);
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN initialization failed\r\n");
        return;
    }
#ifdef SYSLOG_WANTED
    L2VPN_SYSLOG_ID = SYS_LOG_REGISTER ((UINT1 *) "L2VPN", SYSLOG_DEBUG_LEVEL);
#endif
    if (OsixTskIdSelf (&L2VPN_TSK_ID) == OSIX_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "MAIN : L2VPN : Getting Task Id Failed\n");
        OsixQueDel (L2VPN_QID);
        OsixQueDel (L2VPN_RES_QID);
        OsixSemDel (L2VPN_SEM_ID);
        L2VpnDeInit ();
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    L2vpnRegisterL2vpnMibs ();

#ifdef L2VPN_HA_WANTED
    /*Register with RM */
    L2VpnRmInit ();
    i4RetVal = L2VpnRmRegisterWithRM ();
    if (i4RetVal != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Register with RM Failed\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif
    MPLS_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv
            (L2VPN_TSK_ID,
             (L2VPN_MSG_EVENT | L2VPN_TMR_EVENT | L2VPN_LDP_UP_EVT |
              L2VPN_ADMIN_UP_EVENT | L2VPN_ADMIN_DOWN_EVENT |
              L2VPNRED_NEG_TMR_EVENT), OSIX_WAIT, &u4RcvdEvent) != OSIX_SUCCESS)
        {
            continue;
        }

        MPLS_L2VPN_LOCK ();

        if ((u4RcvdEvent & L2VPN_ADMIN_UP_EVENT) == L2VPN_ADMIN_UP_EVENT)
        {
            L2VpnEnableL2VpnService ();
        }

        if (((u4RcvdEvent & L2VPN_ADMIN_DOWN_EVENT) ==
             L2VPN_ADMIN_DOWN_EVENT) && (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP))
        {
            L2VpnDisableL2VpnService (L2VPN_ADMIN_DOWN);
        }
        if ((u4RcvdEvent & L2VPN_LDP_UP_EVT) == L2VPN_LDP_UP_EVT)
        {
            L2VpnLdpRegisterApplication ();
        }

        /* L2VPN_MSG_EVENT Rcvd, Process Q Msgs */
        if ((u4RcvdEvent & L2VPN_MSG_EVENT) == L2VPN_MSG_EVENT)
        {
            L2VpnProcessQMsgs ();
        }

        if (u4RcvdEvent & L2VPN_TMR_EVENT)
        {
            L2vpnProcessTimerEvents ();
        }
        if (u4RcvdEvent & L2VPNRED_NEG_TMR_EVENT)
        {
            L2VpnRedundancyTimerEvent ();
        }

        MPLS_L2VPN_UNLOCK ();
    }
}

/*****************************************************************************/
/* Function Name : L2VpnInit                                                 */
/* Description   : This routine intializes configurable parameters           */
/*                 to default values.                                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnInit (VOID)
{
    tKeyInfoStruct      LblRangeInfo;
    UINT1               u1PwCCTypeCapabs =
        (UINT1) (L2VPN_VCCV_CC_ACH + L2VPN_VCCV_CC_RAL + L2VPN_VCCV_CC_TTL_EXP);

    /* Create Timer list for L2 VPN module */
    if (TmrCreateTimerList ((const UINT1 *) L2VPN_TSK_NAME,
                            L2VPN_TMR_EVENT, NULL,
                            &L2VPN_TIMER_LIST_ID) != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Timer list creation failed\r\n");
        return L2VPN_FAILURE;
    }

    /* Initialise Trace Flag */
    L2VPN_DBG_FLAG = L2VPN_DEF_TRACE;
    /*Default Value for Global CV Type is set to zero */

    if (FsL2VPNSizingParams[MAX_L2VPN_PW_VC_INFO_SIZING_ID].u4PreAllocatedUnits
        != MAX_L2VPN_PW_VC_INFO)
    {
        FsL2VPNSizingParams[MAX_L2VPN_PW_VC_INFO_SIZING_ID].u4StructSize
            = (sizeof (tPwVcInfo) *
               FsL2VPNSizingParams[MAX_L2VPN_PW_VC_INFO_SIZING_ID].
               u4PreAllocatedUnits);
        FsL2VPNSizingParams[MAX_L2VPN_PW_VC_INFO_SIZING_ID].u4PreAllocatedUnits
            = MAX_L2VPN_PW_VC_INFO;
    }

    if (FsL2VPNSizingParams[MAX_L2VPN_VPLS_INFO_SIZING_ID].u4PreAllocatedUnits
        != MAX_L2VPN_VPLS_INFO)
    {
        gL2VpnGlobalInfo.u4MaxVplsEntries
            = FsL2VPNSizingParams[MAX_L2VPN_VPLS_INFO_SIZING_ID].
            u4PreAllocatedUnits;
        FsL2VPNSizingParams[MAX_L2VPN_VPLS_INFO_SIZING_ID].u4StructSize
            = (sizeof (tVPLSInfo) * gL2VpnGlobalInfo.u4MaxVplsEntries);
        FsL2VPNSizingParams[MAX_L2VPN_VPLS_INFO_SIZING_ID].u4PreAllocatedUnits
            = MAX_L2VPN_VPLS_INFO;
    }
    else
    {
        gL2VpnGlobalInfo.u4MaxVplsEntries = MAX_L2VPN_VPLS_ENTRIES;
    }

    L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_PW_VC_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_MAX_WAITING_PEER_VCS (gpPwVcGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_PW_VC_LBLMSG_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_MAX_PEER_SSN (gpPwVcGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;

    L2VPN_MAX_DORMANT_PWVC_ENTRIES (gpPwVcGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_PW_VC_DORMANT_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_MAX_PWVC_CLEANUP_INT (gpPwVcGlobalInfo) = L2VPN_DEF_PWVC_CLEANUP_INTV;

    L2VPN_MAX_MPLS_ENTRIES (gpPwVcMplsGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_PW_VC_MPLS_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_CFG_MAX_MPLS_IN_OUT_ENTRIES (gpPwVcMplsGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    L2VPN_MAX_ENET_SERV_ENTRIES (gpPwVcEnetGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_ENET_SERV_SPEC_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_MAX_ENET_ENTRIES (gpPwVcEnetGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_ENET_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    L2VPN_MAX_ENET_MPLS_PRIMAP_ENTRIES (gpPwVcEnetGlobalInfo)
        = FsL2VPNSizingParams[MAX_L2VPN_ENET_PRIMAPPING_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    /*MS-PW */
    /*Update default Maximum MSPW Entries */
    L2VPN_MAX_MSPW_ENTRIES (gL2vpnMsPwGlobals) =
        FsL2VPNSizingParams[MAX_L2VPN_MS_PW_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    /* Initialise Admin Status */
    L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;

#ifdef NPAPI_WANTED
    if (MplsFsMplsHwGetPwCtrlChnlCapabilities (&u1PwCCTypeCapabs) ==
        FNP_FAILURE)
    {
        TmrDeleteTimerList (L2VPN_TIMER_LIST_ID);

        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "FsMplsHwGetPwCtrlChnlCapabilities failed\r\n");
        return L2VPN_FAILURE;
    }
#endif
    gL2VpnGlobalInfo.u1LocalHwCcTypeCapabilities = u1PwCCTypeCapabs;
    gL2VpnGlobalInfo.u1LocalCcTypeCapabilities = u1PwCCTypeCapabs;
    gL2VpnGlobalInfo.u1LocalCvTypeCapabilities = L2VPN_VCCV_CV_LSPP;
    gL2VpnGlobalInfo.b1PwStatusNotif = MPLS_SNMP_FALSE;
    gL2VpnGlobalInfo.b1PwOamStatusNotif = MPLS_SNMP_FALSE;
#ifdef HVPLS_WANTED
    gL2VpnGlobalInfo.i4VplsStatusNotifEnable = MPLS_SNMP_FALSE;
    gL2VpnGlobalInfo.u4VplsNotificationMaxRate = L2VPN_ZERO;
#endif
    L2VpnRedundancyInit ();
    L2VPN_RED_STATUS_FORWARD_ENABLE (&gL2vpnMsPwGlobals) = TRUE;

    L2VpnEnableL2VpnService ();

    LblRangeInfo.u4Key1Min = 0;
    LblRangeInfo.u4Key1Max = 0;
    LblRangeInfo.u4Key2Min = gSystemSize.MplsSystemSize.u4MinBgpVplsLblRange;
    LblRangeInfo.u4Key2Max = gSystemSize.MplsSystemSize.u4MaxBgpVplsLblRange;

#ifdef VPLSADS_WANTED
    if (LblMgrCreateLabelSpaceGroup
        (LBL_ALLOC_BOTH_NUM, &LblRangeInfo, 1,
         BGP_VPLS_LBL_MODULE_ID, PER_PLATFORM_INTERFACE_INDEX,
         &gu2BgpVplsLblGroupId) == LBL_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnCreateLabelSpaceGroup failed\r\n");
        return L2VPN_FAILURE;
    }
#endif

    L2VPN_INITIALISED = TRUE;
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessL2VpnAdminEvent                               */
/* Description   :                                                           */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessL2VpnAdminEvent (tL2VpnAdminEvtInfo * pL2VpnAdminEvtInfo)
{
    tPwVcAdminEvtInfo  *pPwVcAdminEvtInfo = NULL;
    tPwVcServSpecAdminEvtInfo *pPwVcServAdminEvtInfo = NULL;
    tMsPwAdminEvtInfo  *pMsPwAdminEvtInfo = NULL;    /*MS-PW */
    INT4                i4Status = L2VPN_FAILURE;
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
#ifdef VPLSADS_WANTED
    tL2vpnVplsAdminEvtInfo *pVplsAdminEvtInfo = NULL;
#endif

    switch (pL2VpnAdminEvtInfo->u4EvtType)
    {
        case L2VPN_PWVC_ADMIN_EVENT:
            pPwVcAdminEvtInfo = &pL2VpnAdminEvtInfo->unEvtInfo.PwVcAdminEvtInfo;
            i4Status = L2VpnProcessPwVcAdminEvent (pPwVcAdminEvtInfo);
            break;

            /*MS-PW */
        case L2VPN_MSPW_EVENT:
            pMsPwAdminEvtInfo = &pL2VpnAdminEvtInfo->unEvtInfo.MsPwAdminEvtInfo;
            i4Status = L2VpnProcessMsPwAdminEvent (pMsPwAdminEvtInfo);
            break;

        case L2VPN_PWVC_SERV_ADMIN_EVENT:
            pPwVcServAdminEvtInfo =
                &pL2VpnAdminEvtInfo->unEvtInfo.PwVcServAdminEvtInfo;
            i4Status = L2VpnProcessServAdminEvent (pPwVcServAdminEvtInfo);
            break;

            /* PW Redundancy ADMIN events */
        case L2VPN_RED_GROUP_ADMIN_EVENT:
            switch L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE
                (pL2VpnAdminEvtInfo)
            {
                case L2VPNRED_EVENT_RG_ACTIVATE:
                    i4Status = L2vpnRedundancyGroupActivate
                        (L2VPN_ADMIN_EVT_RED_GROUP_INDEX (pL2VpnAdminEvtInfo));
                    break;
                case L2VPNRED_EVENT_RG_DEACTIVATE:
                    i4Status = L2vpnRedundancyGroupDeActivate
                        (L2VPN_ADMIN_EVT_RED_GROUP_INDEX (pL2VpnAdminEvtInfo));
                    break;
                case L2VPNRED_EVENT_RG_DESTROY:
                    pRgGroup = L2VpnGetRedundancyEntryByIndex
                        (L2VPN_ADMIN_EVT_RED_GROUP_INDEX (pL2VpnAdminEvtInfo));
                    if (pRgGroup == NULL)
                    {
                        break;
                    }
                    i4Status = L2vpnRedundancyGroupDelete (pRgGroup);
                    break;
                default:
                    break;
            }
            break;

        case L2VPN_RED_NODE_ADMIN_EVENT:
            switch L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE
                (pL2VpnAdminEvtInfo)
            {
                case L2VPNRED_EVENT_NODE_ACTIVATE:
                    i4Status = L2vpnRedundancyNodeActivate
                        (L2VPN_ADMIN_EVT_RED_NODE_GROUP (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_NODE_ADDR (pL2VpnAdminEvtInfo));
                    break;
                case L2VPNRED_EVENT_NODE_DEACTIVATE:
                    i4Status = L2vpnRedundancyNodeDeActivate
                        (L2VPN_ADMIN_EVT_RED_NODE_GROUP (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_NODE_ADDR (pL2VpnAdminEvtInfo));
                    break;
                case L2VPNRED_EVENT_NODE_DESTROY:
                    pRgNode = L2VpnGetRedundancyNodeEntryByIndex
                        (L2VPN_ADMIN_EVT_RED_NODE_GROUP (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_NODE_ADDR (pL2VpnAdminEvtInfo));
                    if (pRgNode == NULL)
                    {
                        break;
                    }
                    i4Status = L2vpnRedundancyNodeDelete (pRgNode);
                    break;
                default:
                    break;
            }
            break;

        case L2VPN_RED_PW_ADMIN_EVENT:
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex
                (L2VPN_ADMIN_EVT_RED_PW_INDEX (pL2VpnAdminEvtInfo));

            switch L2VPN_ADMIN_EVT_RED_PW_EVTTYPE
                (pL2VpnAdminEvtInfo)
            {
                case L2VPNRED_EVENT_PW_ACTIVATE:
                    i4Status = L2vpnRedundancyPwActivate
                        (L2VPN_ADMIN_EVT_RED_PW_GROUP (pL2VpnAdminEvtInfo),
                         pPwVcEntry);
                    break;
                case L2VPNRED_EVENT_PW_SWITCHOVER_LOCAL:
                    pRgPw = L2VpnGetRedundancyPwEntryByIndex
                        (L2VPN_ADMIN_EVT_RED_PW_GROUP (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_PW_INDEX (pL2VpnAdminEvtInfo));
                    if (pRgPw == NULL)
                    {
                        break;
                    }

                    i4Status = L2vpnRedundancyPwStateUpdate
                        (pRgPw, L2VPNRED_EVENT_PW_SWITCHOVER_LOCAL);
                    break;
                case L2VPNRED_EVENT_PW_DEACTIVATE:
                    i4Status = L2vpnRedundancyPwDeActivate
                        (L2VPN_ADMIN_EVT_RED_PW_GROUP (pL2VpnAdminEvtInfo),
                         pPwVcEntry);
                    break;
                case L2VPNRED_EVENT_PW_DESTROY:
                    pRgPw = L2VpnGetRedundancyPwEntryByIndex
                        (L2VPN_ADMIN_EVT_RED_PW_GROUP (pL2VpnAdminEvtInfo),
                         L2VPN_ADMIN_EVT_RED_PW_INDEX (pL2VpnAdminEvtInfo));
                    if (pRgPw == NULL)
                    {
                        break;
                    }
                    i4Status = L2vpnRedundancyPwDelete (pRgPw);
                    break;
                default:
                    break;
            }
            break;
#ifdef VPLSADS_WANTED
        case L2VPN_VPLS_ADMIN_EVENT:
            pVplsAdminEvtInfo = &pL2VpnAdminEvtInfo->unEvtInfo.VplsAdminEvtInfo;
            i4Status = L2VpnProcessVplsAdminEvent (pVplsAdminEvtInfo);
            break;
#endif
        default:
            break;
    }
    return (i4Status);
}

/*****************************************************************************/
/* Function Name : L2VpnProcessQMsgs                                         */
/* Description   : This function processes Queue Messages                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
VOID
L2VpnProcessQMsgs (VOID)
{
    tL2VpnQMsg         *pL2VpnQMsg = NULL;
    tL2VpnAdminEvtInfo *pL2VpnAdminEvtInfo = NULL;
    tPwVcSigEvtInfo    *pPwVcSigEvtInfo = NULL;
    tPwVcPSNEvtInfo    *pPwVcPSNEvtInfo = NULL;
    tL2VpnIfEvtInfo    *pL2VpnIfEvtInfo = NULL;
    tPwVcFwdPlaneEvtInfo *pPwVcFwdPlaneEvtInfo = NULL;
    tL2VpnPwRedAcStatusInfo *pL2VpnPwRedAcStatusInfo = NULL;
#ifdef VPLSADS_WANTED
    tL2VpnBgpEvtInfo   *pL2VpnBgpEvtInfo = NULL;
#endif

    UINT4               u4RespMsg = L2VPN_ZERO;

    while (OsixQueRecv (L2VPN_QID, (UINT1 *) (&pL2VpnQMsg), OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)

    {
        if ((L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
            && (pL2VpnQMsg->u4MsgType != L2VPN_ADMIN_EVENT))
        {
            /* Don't process events other than admin events */
            if (MemReleaseMemBlock (L2VPN_Q_POOL_ID, (UINT1 *) pL2VpnQMsg)
                == MEM_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "Mem release failure - Queue msg\r\n");
            }

            continue;
        }

        switch (pL2VpnQMsg->u4MsgType)
        {
            case L2VPN_ADMIN_EVENT:
                pL2VpnAdminEvtInfo
                    = (tL2VpnAdminEvtInfo *) & pL2VpnQMsg->L2VpnEvtInfo;
                L2VpnProcessL2VpnAdminEvent (pL2VpnAdminEvtInfo);
                if (pL2VpnAdminEvtInfo->u4WaitForRespFlag
                    == L2VPN_WAIT_FOR_RESP)
                {
                    u4RespMsg = L2VPN_TRUE;
                    if (OsixQueSend
                        (L2VPN_RES_QID, (UINT1 *) (&u4RespMsg),
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
                    {
                        return;
                    }
                }
                break;

            case L2VPN_PWVC_SIG_EVENT:
                pPwVcSigEvtInfo
                    = (tPwVcSigEvtInfo *) & pL2VpnQMsg->L2VpnEvtInfo;
                L2VpnProcessSigEvent (pPwVcSigEvtInfo);
                break;

            case L2VPN_PWVC_PSN_EVENT:
                pPwVcPSNEvtInfo
                    = (tPwVcPSNEvtInfo *) & pL2VpnQMsg->L2VpnEvtInfo;
                L2VpnProcessPsnEvent (pPwVcPSNEvtInfo);
                break;

            case L2VPN_PWVC_IF_EVENT:
                pL2VpnIfEvtInfo
                    = (tL2VpnIfEvtInfo *) & pL2VpnQMsg->L2VpnEvtInfo;
                L2VpnProcessIfEvent (pL2VpnIfEvtInfo);
                break;

            case L2VPN_PWVC_FWD_EVENT:
                pPwVcFwdPlaneEvtInfo
                    = (tPwVcFwdPlaneEvtInfo *) & pL2VpnQMsg->L2VpnEvtInfo;
                L2VpnProcessFwderEvent (pPwVcFwdPlaneEvtInfo);
                break;

            case L2VPN_PWVC_ROUTE_EVENT:
                L2VpnProcessRouteEvent
                    (&(pL2VpnQMsg->L2VpnEvtInfo.PwRtEvtInfo));
                break;

            case L2VPN_PWVC_OAM_MEG_EVENT:
                L2VpnProcessOamMegEvent (&(pL2VpnQMsg->L2VpnEvtInfo.PwOamInfo));
                break;

            case L2VPN_DLAG_AC_STATUS_EVENT:
                pL2VpnPwRedAcStatusInfo =
                    (tL2VpnPwRedAcStatusInfo *) & pL2VpnQMsg->L2VpnEvtInfo;

                L2VpnProcessAcStatusMsg (pL2VpnPwRedAcStatusInfo);
                break;

#ifdef VPLSADS_WANTED
            case L2VPN_BGP_SIG_EVENT:
                pL2VpnBgpEvtInfo =
                    (tL2VpnBgpEvtInfo *) & pL2VpnQMsg->L2VpnEvtInfo;
                if (L2VPN_BGP_VPLS_MSG_TYPE (pL2VpnBgpEvtInfo) !=
                    L2VPN_BGP_ADMIN_UP &&
                    L2VPN_BGP_VPLS_MSG_TYPE (pL2VpnBgpEvtInfo) !=
                    L2VPN_BGP_ADMIN_DOWN)
                {
                    if (L2VPN_RM_ACTIVE_STANDBY_UP !=
                        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState &&
                        L2VPN_RM_ACTIVE_STANDBY_DOWN !=
                        gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState)
                    {
                        break;
                    }
                }
                L2VpnProcessBgpEvent (pL2VpnBgpEvtInfo);
                break;
#endif
#ifdef L2VPN_HA_WANTED
            case L2VPN_RM_EVENT:
                L2VpnProcessRmEvent (&
                                     (pL2VpnQMsg->L2VpnEvtInfo.L2VpnRmEvtInfo));
                break;
#endif
            default:
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Unknown event\r\n");
                break;
        }

        if (MemReleaseMemBlock (L2VPN_Q_POOL_ID, (UINT1 *) pL2VpnQMsg)
            == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - Queue msg\r\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : L2VpnCreateRBTree                                         */
/* Description   : This routine creates RBTree to FEC128 and 129 PwVc Entries*/
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
PRIVATE INT4
L2VpnCreateRBTree ()
{
    /* RBTree for FEC128 */
    if ((L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo),
                       L2VpnFec128RbTreeCmpFunc)) == NULL)
    {
        return L2VPN_FAILURE;
    }

    /* RBTree for FEC129 */
    if ((L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo),
                       L2VpnFec129RbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        return L2VPN_FAILURE;
    }
    if ((L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo),
                       L2VpnFec129RbTreeVplsCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        return L2VPN_FAILURE;
    }

    /* RBTree for Peer Address */
    if ((L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo),
                       L2VpnPeerAddrRbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }
    /* RBTree for  Non TE Ingress Pw Mapping */
    if ((L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo),
                       L2VpnNonTeRbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for  Non TE Egress Pw Mapping */
    if ((L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo),
                       L2VpnNonTeInRbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for TE Pw Mapping */
    if ((L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo),
                       L2VpnTeRbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for TE In Tunnel PW Mapping */
    if ((L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo),
                       L2VpnTeInRbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for VcOnly Pw Mapping */
    if ((L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo) =
         RBTreeCreate (L2VPN_MAX_MPLS_MAP_ENTRIES (gpPwVcMplsGlobalInfo),
                       L2VpnVcOnlyRbTreeCmpFunc)) == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }
    /* RBTree for VPLS VFI based RBTree */
    if ((L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tVPLSEntry, NextEntry),
                               (tRBCompareFn) L2VpnVplsVfiRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

#ifdef VPLSADS_WANTED
    /* RBTree for VPLS RD based RBTree */
    if ((L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tVPLSRdEntry, NextEntry),
                               (tRBCompareFn) L2VpnVplsRdRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for VPLS RT based RBTree */
    if ((L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tVPLSRtEntry, NextEntry),
                               (tRBCompareFn) L2VpnVplsRtRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for VPLS VE based RBTree */
    if ((L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tVPLSVeEntry, NextEntry),
                               (tRBCompareFn) L2VpnVplsVeRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* RBTree for VPLS AC based RBTree */
    if ((L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tVplsAcMapEntry, NextEntry),
                               (tRBCompareFn) L2VpnVplsAcRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }
#endif
#ifdef HVPLS_WANTED
    if ((L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tVplsPwBindEntry, NextEntry),
                               (tRBCompareFn) L2VpnVplsBindRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }
#endif

    /* RBTree for VPLS VFI based RBTree */

    if ((L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo) =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tPwVcEntry, InVcLblRbNode),
                               (tRBCompareFn) L2VpnPwVcInLblRbTreeCmpFunc)) ==
        NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));

#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
#ifdef HVPLS_WANTED
        RBTreeDelete (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo));
#endif
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }

    /* Creating the Global Pseudo Wire RB Tree. */
    if (L2VpnPwIdCreateTable () == OSIX_FAILURE)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
#ifdef HVPLS_WANTED
        RBTreeDelete (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo));
#endif
        RBTreeDelete (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo));
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnTaskMain: Global "
                   "L2VpnPwCreateTable Failed \r\n");
        return L2VPN_FAILURE;
    }
    /*MS-PW */
    /*RB Tree for MSPW */
    if (L2vpnUtlCreateRBTree () != OSIX_SUCCESS)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
#ifdef HVPLS_WANTED
        RBTreeDelete (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo));
#endif
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        return L2VPN_FAILURE;
    }
    /* RB Tree for PwIfIndex */
    if (L2VpnPwIfIndexCreateTable () == OSIX_FAILURE)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
#ifdef HVPLS_WANTED
        RBTreeDelete (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo));
#endif
        RBTreeDelete (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo));
        L2VpnPwIdDeleteTable ();
        L2vpnUtlDeleteRBTree ();
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnTaskMain: Global "
                   "L2VpnPwIfIndexCreateTable  Failed \r\n");
        return L2VPN_FAILURE;
    }

    /* RB Tree for OutLabel */

    if (L2VpnPwCreateOutLabelBasedRBTree () == OSIX_FAILURE)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
#ifdef HVPLS_WANTED
        RBTreeDelete (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo));
#endif
        RBTreeDelete (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo));
        L2VpnPwIdDeleteTable ();
        L2vpnUtlDeleteRBTree ();
        L2VpnPwIfIndexDeleteTable ();
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnTaskMain: Global "
                   "L2VpnPwCreateOutLabelBasedRBTree  Failed \r\n");
        return L2VPN_FAILURE;
    }

    L2VPN_MPLS_PORT_ENTRY_INFO_TABLE = NULL;
    if ((L2VPN_MPLS_PORT_ENTRY_INFO_TABLE =
         RBTreeCreateEmbedded (FSAP_OFFSETOF
                               (tMplsPortEntryInfo, MplsPortRbNode),
                               (tRBCompareFn) L2VpnMplsPortEntryRbTreeCmpFunc))
        == NULL)
    {
        RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
#ifdef VPLSADS_WANTED
        RBTreeDelete (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo));
        RBTreeDelete (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));
#endif
#ifdef HVPLS_WANTED
        RBTreeDelete (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo));
#endif
        RBTreeDelete (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo));
        L2VpnPwIdDeleteTable ();
        L2vpnUtlDeleteRBTree ();
        L2VpnPwIfIndexDeleteTable ();
        L2VpnPwDeleteOutLabelBasedRBTree ();
        L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnTaskMain: Global "
                   "MPLS Port Entry RBTree Creation Failed \r\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnDeleteRBTree                                         */
/* Description   : This routine deletes PwVc Entries* from RBTree            */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
PRIVATE VOID
L2VpnDeleteRBTree ()
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcActivePeerSsnEntry *pActivePeerSsn = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    /* PwVc Pointer is attached to Peer Address Table so. remove it First 
     *  This List may empty If PeerAddress Tree is already freed. Anyway
     *  Keep it for safety */
    pActivePeerSsn =
        RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
    while (pActivePeerSsn != NULL)
    {
        if (TMO_DLL_Count (L2VPN_PWVC_LIST (pActivePeerSsn)) != 0)
            TMO_DLL_Clear (L2VPN_PWVC_LIST (pActivePeerSsn));
        pActivePeerSsn =
            RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
    }

    /* Scan Through all FEC128 the PwVc Entry and Delete them */
    pPwVcEntry = RBTreeGetFirst (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
    while (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) &&
            (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))

        {
            if (L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Release Msg failed\r\n");
            }
        }

        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_ONE);
        if (L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_DELETE,
                                     L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
            == L2VPN_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                        "\r PWID FEC: PW %d for Peer IPv4 -%#x IPv6 -%s "
                        " delete failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)),
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "\r PWID FEC: PW %d for %#x delete failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)));
#endif
        }

        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_TWO);
        RBTreeRem (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo), pPwVcEntry);
        if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                                (UINT1 *) pPwVcEntry) == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - PwVc Entry removal from  List\r\n");
        }
        pPwVcEntry =
            RBTreeGetFirst (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
    }

    /* Scan Through all FEC129 the PwVc Entry and Delete them */
    pPwVcEntry = RBTreeGetFirst (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
    while (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
        {
            if (L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Release Msg failed\r\n");
            }
        }
        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_ONE);
        if (L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_DELETE,
                                     L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
            == L2VPN_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                        "\r GEN FEC: PW %d for IPv4 -%#x "
                        " IPv6 -%s  delete failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)),
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "\r GEN FEC: PW %d for %#x delete failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)));
#endif
        }

        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_TWO);
        RBTreeRem (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo), pPwVcEntry);
        if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                                (UINT1 *) pPwVcEntry) == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - PwVc Entry removal from  List\r\n");
        }
        pPwVcEntry =
            RBTreeGetFirst (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
    }
    /* Scan Through all FEC129 the PwVc Entry and Delete them */
    pPwVcEntry =
        RBTreeGetFirst (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
    while (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
            (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
        {
            if (L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Release Msg failed\r\n");
            }
        }
        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_ONE);
        if (L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_DELETE,
                                     L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
            == L2VPN_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                        "\r GEN FEC: PW %d for IPv4 -%#x "
                        " IPv6 -%s  delete failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)),
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "\r GEN FEC: PW %d for %#x delete failed in HW\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)));
#endif
        }

        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_TWO);
        RBTreeRem (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo),
                   pPwVcEntry);
        if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID, (UINT1 *) pPwVcEntry) ==
            MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - PwVc Entry removal from  List\r\n");
        }
        pPwVcEntry =
            RBTreeGetFirst (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
    }

    RBTreeDelete (L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo));
    L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo) = NULL;

    RBTreeDelete (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo));
    L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo) = NULL;

    RBTreeDelete (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));
    L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo) = NULL;

    /* Scan Through all VPLS Entry and Delete them */
    pVplsEntry = RBTreeGetFirst (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
    while (pVplsEntry != NULL)
    {
        RBTreeRem (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo), pVplsEntry);
        if (MemReleaseMemBlock (L2VPN_VPLS_POOL_ID,
                                (UINT1 *) pVplsEntry) == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - VPLS Entry release failed\r\n");
        }
        pVplsEntry = RBTreeGetFirst (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));
    }

    /* Deleting the Global Pseudo Wire RB Tree. */
    L2VpnPwIdDeleteTable ();
    L2VPN_MPLS_PWID_TABLE = NULL;

    RBTreeDestroy (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo), NULL, 0);
    L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo) = NULL;

    RBTreeDelete (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
    L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo) = NULL;
    RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo));
    L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo) = NULL;
    RBTreeDelete (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
    L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo) = NULL;
    RBTreeDelete (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo));
    L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo) = NULL;
    RBTreeDelete (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo));
    L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo) = NULL;
    RBTreeDelete (L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo));
    L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo) = NULL;
    RBTreeDelete (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo));
    L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo) = NULL;

    /*MS-PW */
    /*MS-PW */

    /* Deleting the Global PwIfIndex RB Tree. */
    L2VpnPwIfIndexDeleteTable ();

    /*Deleting the MPLS port RBTree */
    pMplsPortEntryInfo = RBTreeGetFirst (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE);

    while (pMplsPortEntryInfo != NULL)
    {
        RBTreeRemove (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE, pMplsPortEntryInfo);

        MemReleaseMemBlock (L2VPN_PORT_ENTRY_INFO_POOL_ID,
                            (UINT1 *) pMplsPortEntryInfo);

        pMplsPortEntryInfo = RBTreeGetFirst (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE);
    }

    RBTreeDestroy (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE, NULL, 0);
    L2VPN_MPLS_PORT_ENTRY_INFO_TABLE = NULL;

}

/*****************************************************************************/
/* Function Name : L2VpnDisableL2VpnService                                  */
/* Description   : This routine disables L2VPN Admin status                  */
/* Input(s)      : Desired administrative status                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDisableL2VpnService (UINT4 u4AdminStatus)
{
    tPwVcDormantVcEntry *pDormantVc = NULL;
    tPwVcInfo          *pPwVcInfo = NULL;
    tVPLSInfo          *pVplsInfo = NULL;

    UINT4               u4Interval;
    UINT4               u4PwVcIndex;
    UINT4               u4VplsInstance;
    INT4                i4RetStatus = L2VPN_SUCCESS;

    L2VPN_ADMIN_STATUS = (INT4) u4AdminStatus;

    if ((L2VPN_INITIALISED == TRUE) &&
        (TMO_DLL_Count (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo)) != 0))
    {
        gpL2VpnGlobalInfo->u1AdminStatusProgress = L2VPN_ADMIN_DOWN_IN_PROGRESS;

        return L2VPN_SUCCESS;
    }

    gpL2VpnGlobalInfo->u1AdminStatusProgress = L2VPN_ADMIN_DOWN;

    /* Free the Dormant VCs List */
    pDormantVc = (tPwVcDormantVcEntry *)
        TMO_SLL_First (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo));
    while (pDormantVc != NULL)
    {
        TMO_SLL_Delete (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo),
                        &(pDormantVc->NextNode));
        if (MemReleaseMemBlock (L2VPN_DORMANT_VCS_POOL_ID,
                                (UINT1 *) pDormantVc) == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - Dormant PwVc list removal\r\n");
            i4RetStatus = L2VPN_FAILURE;
        }
        pDormantVc = (tPwVcDormantVcEntry *)
            TMO_SLL_First (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo));
    }
    if (L2vpnUtlDeleteRBTree () != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "MSPW RB tree deletion faile\r\n");
    }

    for (u4PwVcIndex = 1;
         u4PwVcIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo);
         u4PwVcIndex++)
    {
        pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwVcIndex);

        /* Reset all days statistics */
        for (u4Interval = 1;
             u4Interval <= L2VPN_PWVC_MAX_PERF_DAYS; u4Interval++)
        {
            L2VPN_PWVC_PERF_1DAY_VALID_DATA (pPwVcInfo, u4Interval) =
                L2VPN_SNMP_FALSE;
        }

        for (u4Interval = 1; u4Interval <= L2VPN_PWVC_MAX_PERF_INTV;
             u4Interval++)
        {
            L2VPN_PWVC_PERF_INT_VALID_DATA (pPwVcInfo, u4Interval) =
                L2VPN_SNMP_FALSE;
        }

        pPwVcInfo->u1PwVcPerfDayIndex = L2VPN_PWVC_MIN_PERF_INTV;
        if (pPwVcInfo->pPwVcEntry != NULL)
        {
            /* Release the STATIC Label to the Label Manager */
            if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcInfo->pPwVcEntry) !=
                L2VPN_ZERO)
            {
                continue;
            }
            if (((L2VPN_PWVC_OWNER (pPwVcInfo->pPwVcEntry)
                  == L2VPN_PWVC_OWNER_MANUAL) ||
                 (L2VPN_IS_STATIC_PW (pPwVcInfo->pPwVcEntry) == TRUE))
#ifdef VPLSADS_WANTED
                && L2VPN_PWVC_OWNER (pPwVcInfo->pPwVcEntry) !=
                L2VPN_PWVC_OWNER_OTHER
#endif
                )
            {
                if (L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcInfo->pPwVcEntry) !=
                    L2VPN_LDP_INVALID_LABEL)
                {
                    if (MplsReleaseLblToLblGroup (L2VPN_PWVC_INBOUND_VC_LABEL
                                                  (pPwVcInfo->pPwVcEntry)) ==
                        MPLS_FAILURE)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                   "assigning the static label to the Label Space failed\r\n");
                    }
                }
            }

            MplsL2VpnRelPwVcIndex (pPwVcInfo->pPwVcEntry->u4PwVcIndex);

            L2VpnUpdateGlobalStats (pPwVcInfo->pPwVcEntry, L2VPN_PWVC_DESTROY);

            L2VpnDeletePwVc (pPwVcInfo->pPwVcEntry);
        }
        pPwVcInfo->pPwVcEntry = NULL;
    }
    L2VpnDeleteRBTree ();
    /* Re-Set the VPLS info table */
    for (u4VplsInstance = 1;
         u4VplsInstance <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4VplsInstance++)
    {
        pVplsInfo = L2VPN_VPLS_INFO_PTR + u4VplsInstance;
        pVplsInfo->pVplsEntry = NULL;
    }

    TMO_HASH_Delete_Table (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                           L2vpnDelEnetHashNode);
    L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo) = NULL;

    if ((L2VPN_PWRED_STATUS == L2VPN_PWRED_ENABLED) &&
        (L2VpnRedundancyDisable () == L2VPN_FAILURE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Failure in removing resource reservation for"
                   "L2VPN/PW-RED sub-module\r\n");
        return L2VPN_FAILURE;
    }

    /* De-register with LDP Appln */
    /* If LDP Module is already shutdown No need to De-register
     */
    if (IssGetModuleSystemControl (LDP_MODULE_ID) != MODULE_SHUTDOWN)
    {
        L2VpnLdpDeRegisterApplication ();
    }

    /* Release the memory allocated for PW Vc Info and VPLS Info */
    MemReleaseMemBlock (L2VPN_PW_INFOTAB_POOL_ID,
                        (UINT1 *) gpPwVcGlobalInfo->ppPwVcInfoTable);
    MemReleaseMemBlock (L2VPN_VPLS_TAB_POOL_ID,
                        (UINT1 *) gpPwVcGlobalInfo->ppVPLSTable);

    /* Free the memory allocated */
    L2vpnSizingMemDeleteMemPools ();

    /* Stop all timers started by this task */
    if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                      &(gpL2VpnGlobalInfo->PerfTimer.AppTimer)) != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Stop of 15 min. interval timer failed\r\n");
    }

    if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                      &(gpL2VpnGlobalInfo->PendMsgToLdpTimer.AppTimer))
        != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Stop of 2 seconds. pending timer failed\r\n");
    }
#ifdef VPLS_GR_WANTED
    if (gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted == L2VPN_TRUE)
    {
        if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                          &(gpL2VpnGlobalInfo->BgpGrTimer.AppTimer)) !=
            TMR_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Stop of BGP GR timer failed\r\n");
        }
        gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted = L2VPN_FALSE;
    }
#endif
    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name : L2VpnEnableL2VpnService                                   */
/* Description   : This routine enables L2VPN Service                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnEnableL2VpnService (VOID)
{
    tPwVcInfo          *pPwVcInfo = NULL;
    tVPLSInfo          *pVplsInfo = NULL;
    UINT4               u4PwVcIndex;
    UINT4               u4Count;
    UINT4               u4VplsInstance;

    L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;

    gpL2VpnGlobalInfo->u1AdminStatusProgress = L2VPN_ADMIN_UP_IN_PROGRESS;

    /* Create Mempools for L2VPN Module. */
    if (L2vpnSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    /* PW VC Global Params Initialisation */
    /* PW Info table for the PW VC Entries retrieval using PwVcIndex */

    gpPwVcGlobalInfo->ppPwVcInfoTable =
        MemAllocMemBlk (L2VPN_PW_INFOTAB_POOL_ID);

    if (gpPwVcGlobalInfo->ppPwVcInfoTable == NULL)
    {
        L2vpnSizingMemDeleteMemPools ();
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Mem allocation failure - global info table\r\n");
        return L2VPN_FAILURE;
    }

    for (u4PwVcIndex = 1;
         u4PwVcIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo);
         u4PwVcIndex++)
    {
        pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwVcIndex);

        /* Initialise all days statistics */
        for (u4Count = 1; u4Count <= L2VPN_PWVC_MAX_PERF_DAYS; u4Count++)
        {
            MEMSET (&pPwVcInfo->aPwVcPerf1DayIntervalInfo[u4Count - 1], 0,
                    sizeof (tPwVcPerfTotalEntry));
            L2VPN_PWVC_PERF_1DAY_VALID_DATA (pPwVcInfo, u4Count) =
                L2VPN_SNMP_FALSE;
        }

        /* Initialise all 15-minute intervals in the day */
        for (u4Count = 1; u4Count <= L2VPN_PWVC_MAX_PERF_INTV; u4Count++)
        {
            MEMSET (&pPwVcInfo->aPwVcPerfIntervalInfo[u4Count - 1],
                    0, sizeof (tPwVcPerfIntervalEntry));
            L2VPN_PWVC_PERF_INT_VALID_DATA (pPwVcInfo, u4Count) =
                L2VPN_SNMP_FALSE;
        }

        pPwVcInfo->u1PwVcPerfDayIndex = L2VPN_PWVC_MIN_PERF_INTV;
        pPwVcInfo->pPwVcEntry = NULL;
    }

    /* PW VC Global Params Initialisation */
    /* PW Info table for the PW VC Entries retrieval using PwVcIndex */

    gpPwVcGlobalInfo->ppVPLSTable = MemAllocMemBlk (L2VPN_VPLS_TAB_POOL_ID);

    if (gpPwVcGlobalInfo->ppVPLSTable == NULL)
    {
        MemReleaseMemBlock (L2VPN_PW_INFOTAB_POOL_ID,
                            (UINT1 *) gpPwVcGlobalInfo->ppPwVcInfoTable);
        L2vpnSizingMemDeleteMemPools ();
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Memory Allocation Failure - VPLS Table\r\n");
        return L2VPN_FAILURE;
    }

    for (u4VplsInstance = 1;
         u4VplsInstance <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4VplsInstance++)
    {
        pVplsInfo = L2VPN_VPLS_INFO_PTR + u4VplsInstance;
        pVplsInfo->pVplsEntry = NULL;
    }

    /* Hash table creation for storing the PW VC Entries */
    L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo)
        = TMO_HASH_Create_Table (L2VPN_HASH_SIZE, L2VpnUtilHashIfIndexCmp, 0);

    if (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo) == NULL)
    {
        MemReleaseMemBlock (L2VPN_PW_INFOTAB_POOL_ID,
                            (UINT1 *) gpPwVcGlobalInfo->ppPwVcInfoTable);
        MemReleaseMemBlock (L2VPN_VPLS_TAB_POOL_ID,
                            (UINT1 *) gpPwVcGlobalInfo->ppVPLSTable);
        L2vpnSizingMemDeleteMemPools ();
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Mempool creation failure - Hash table for If Sort \r\n");

        return L2VPN_FAILURE;
    }

    if (L2VpnCreateRBTree () == L2VPN_FAILURE)
    {
        TMO_HASH_Delete_Table (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                               NULL);
        MemReleaseMemBlock (L2VPN_PW_INFOTAB_POOL_ID,
                            (UINT1 *) gpPwVcGlobalInfo->ppPwVcInfoTable);
        MemReleaseMemBlock (L2VPN_VPLS_TAB_POOL_ID,
                            (UINT1 *) gpPwVcGlobalInfo->ppVPLSTable);
        L2vpnSizingMemDeleteMemPools ();
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Mempool creation failure - Hash table\r\n");
        return L2VPN_FAILURE;
    }

    /* Initialise Dormant Vcs list */
    TMO_SLL_Init (L2VPN_DORMANT_PWVC_LIST (gpPwVcGlobalInfo));

    UTL_DLL_INIT (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                  MPLS_OFFSET (tPwVcEntry, PendMsgToLdpNode));

    if ((L2VPN_PWRED_STATUS == L2VPN_PWRED_ENABLED) &&
        (L2VpnRedundancyEnable () == L2VPN_FAILURE))
    {
        L2VPN_PWRED_STATUS = L2VPN_PWRED_DISABLED;
        return L2VPN_FAILURE;
    }

    /* MPLS PSN specific initialisation */

    INIT_COUNTER_64 (L2VPN_PWVC_PERF_TOTAL_ERROR_PKTS (gpPwVcGlobalInfo));

    L2VPN_ACTIVE_PWVC_ENTRIES (gpPwVcGlobalInfo) = 0;
    L2VPN_NO_OF_PWVC_ENTRIES_CREATED (gpPwVcGlobalInfo) = 0;
    L2VPN_NO_OF_PWVC_ENTRIES_DESTROYED (gpPwVcGlobalInfo) = 0;
#ifdef VPLS_GR_WANTED
    L2VPN_NO_OF_BGP_PW_STALE_ENTRIES (gpPwVcGlobalInfo) = 0;
#endif
    /* Init PSN specific global stats */
    for (u4Count = 0; u4Count < L2VPN_MAX_PSN_TYPES; u4Count++)
    {
        L2VPN_ACTIVE_PWVC_MPLS_ENTRIES (gpPwVcGlobalInfo, u4Count) = 0;
    }

    /* Init Serv specific global stats */
    for (u4Count = 0; u4Count < L2VPN_MAX_VC_TYPES; u4Count++)
    {
        L2VPN_ACTIVE_PWVC_ENET_ENTRIES (gpPwVcGlobalInfo, u4Count) = 0;
    }

    /* Initialise Admin Status */
    L2VPN_ADMIN_STATUS = L2VPN_ADMIN_UP;

    gpL2VpnGlobalInfo->u1AdminStatusProgress = L2VPN_ADMIN_UP;

    /* Registration with CFA for Interface status handling service */
    /* Currently this is handled in per interface registration basis.
       i.e., if an interface is being used in L2-VPN module (AC),
       only for that interface the status change notification is 
       expected from CFA. */
    L2VpnGetPerfCurrentInterval (L2VPN_PWVC_PERF_INTV_IN_MTS * 60,
                                 &L2VPN_PWVC_PERF_CURRENT_INTERVAL, &u4Count,
                                 NULL);
    /* Initial timer should be started for the time remaining in the current
     * interval period to sync rest of the intervals with system clock
     */
    gpL2VpnGlobalInfo->PerfTimer.u4Event = L2VPN_PWVC_PERF_TMR_EXP_EVENT;
    if (TmrStartTimer (L2VPN_TIMER_LIST_ID,
                       &gpL2VpnGlobalInfo->PerfTimer.AppTimer,
                       u4Count * SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Start of 15 min. interval timer failed\r\n");
    }

    gpL2VpnGlobalInfo->b1IsSendToLdpTmrStarted = FALSE;

#ifdef MPLS_SIG_WANTED
    if (LdpIsLdpInitialised () == L2VPN_SUCCESS)
    {
        L2vpUpdateLdpUpStatus ();
    }
#endif

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessPwVcAdminEvent                                */
/* Description   : This routine processes PwVc Admin Events                  */
/* Input(s)      : pPwVcAdminEvtInfo - pointer to PwVc Admin event info      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessPwVcAdminEvent (tPwVcAdminEvtInfo * pPwVcAdminEvtInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    UINT4               u4EvtType;

    /* pPwVcAdminEvtInfo-> u4EvtType, u4PwVcIndex; */
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pPwVcAdminEvtInfo->u4PwVcIndex);

    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW with Index %d, does not exist\r\n",
                    pPwVcAdminEvtInfo->u4PwVcIndex);
        return L2VPN_FAILURE;
    }

    u4EvtType = pPwVcAdminEvtInfo->u4EvtType;

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW[%u], u4EvtType(%u)\n",
                L2VPN_PWVC_INDEX (pPwVcEntry), u4EvtType);

    switch (u4EvtType)
    {
        case L2VPN_PWVC_ADMIN_UP_EVENT:
            /* Update RG ICCP PW entry */
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));
            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_ACTIVATE);
            }

            /* L2VPN_PWVC_ADMIN_UP event recvd from Admin */
            if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_UP)
            {
                i4Status = L2VpnSetupPwVc (pPwVcEntry);
            }
            break;

        case L2VPN_PWVC_NIS_EVENT:
            i4Status = L2VpnDownPwVc (pPwVcEntry);
            break;

        case L2VPN_PWVC_ADMIN_DOWN_EVENT:
            /* L2VPN_PWVC_ADMIN_DOWN/NS event recvd from Admin */
            if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_DOWN)
            {
                i4Status = L2VpnDownPwVc (pPwVcEntry);
            }

            /* Update RG ICCP PW entry */
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));
            if (pRgPw != NULL)
            {
                L2vpnRedundancyPwStateUpdate (pRgPw,
                                              L2VPNRED_EVENT_PW_DEACTIVATE);
            }
            break;
        default:
            break;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnEnablePwVc                                           */
/* Description   : This routine processes PwVc oper  Events                  */
/* Input(s)      : pPwVcEntry        - pointer to PwVc                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnEnablePwVc (tPwVcEntry * pPwVcEntry)
{
    INT4                i4Status = L2VPN_FAILURE;

    /* support for manual pw vc setup */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        /* update out-bound list */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Outbound List failed\r\n");
            return i4Status;
        }
        return i4Status;
    }

    /* Signalling is used to setup PW VC, */
    /* use respective sig method          */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) ==
          L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         (L2VPN_PWVC_OWNER (pPwVcEntry) ==
          L2VPN_PWVC_OWNER_GEN_FEC_SIG))
        && (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
    {
        /* Check Ssn if already exits,
         * update tPwVcEntry if Remote 
         * already sent Lbl Msg */
        if (L2VpnCheckSsnFromPeerSsnList (pPwVcEntry, FALSE) == L2VPN_SUCCESS)
        {
            i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);
        }
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnDisablePwVc                                          */
/* Description   : This routine processes PwVc oper  Events                  */
/* Input(s)      : pPwVcEntry        - pointer to PwVc                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDisablePwVc (tPwVcEntry * pPwVcEntry)
{
    UINT4               u4InVcLabel = L2VPN_INVALID_LABEL;
    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "PWVC status update error\r\n");
            return i4Status;
        }
        return i4Status;
    }

    /* Signalling is used to setup PW VC, use respective sig method */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) ==
          L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         (L2VPN_PWVC_OWNER (pPwVcEntry) ==
          L2VPN_PWVC_OWNER_GEN_FEC_SIG))
        && (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
    {
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            /* update Peer Ssn List */
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                        (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
            }
            else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
            {
                MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                        (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
            }
            GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

            u4InVcLabel = L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
            i4Status = L2VpnSendLdpPwVcLblWdrawMsg (pPwVcEntry, L2VPN_ZERO);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label withdraw msg failed\r\n");
            }
            else
            {
                pPwVcEntry->u4PrevInVcLabel = u4InVcLabel;
                L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_LDP_INVALID_LABEL);
            }

            if (L2VpnUpdatePeerSsnList (GenU4Addr,
                                        L2VPN_PWVC_DELETE_IN_SESSION,
                                        pPwVcEntry) != NULL)
            {
                L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                           "More PWs are associated with this peer session\r\n");
            }

        }
    }
    /* Add support for other sig methods here */
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unsupported PSN Type\r\n");
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessFwderEvent                                    */
/* Description   : This routine processes Forwarder Events                   */
/* Input(s)      : pL2VpnFwdEvtInfo - pointer to Interface event info        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessFwderEvent (tPwVcFwdPlaneEvtInfo * pL2VpnFwdEvtInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4EvtType;
    pPwVcEntry = L2VpnGetPwVcEntryFromPwId (pL2VpnFwdEvtInfo->u4PwVcID,
                                            pL2VpnFwdEvtInfo->i1PwVcType);

    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW with ID %d, does not exist\r\n",
                    pL2VpnFwdEvtInfo->u4PwVcID);
        return L2VPN_FAILURE;
    }

    u4EvtType = pL2VpnFwdEvtInfo->u4EvtType;
    switch (u4EvtType)
    {
            /* During error conditions like, Packet Loss, Corruption, 
             * Out-Of-Order Delivery, Misconnection and Payload Type Mismatch,
             * down event from FM is expected */
        case L2VPN_FM_PWVC_DOWN_EVENT:
            L2VpnDisablePwVc (pPwVcEntry);
            break;

            /* When FM wants to bring up the PW VC this event is expected */
        case L2VPN_FM_PWVC_UP_EVENT:
            L2VpnEnablePwVc (pPwVcEntry);
            break;

            /* When FM wants to restart PW VC this event is expected, OR it can use
             * both DOWN/UP events to achieve the same. This will be used when
             * sequence number needs to be restarted */
        case L2VPN_FM_PWVC_RESTART_EVENT:
            L2VpnDisablePwVc (pPwVcEntry);
            L2VpnEnablePwVc (pPwVcEntry);
            break;

        default:
            break;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessRouteEvent                                    */
/* Description   : This routine processes Route Change Notifications.        */
/* Input(s)      : pPwRtEvtInfo - pointer to PW Route Change Event Info      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessRouteEvent (tL2VpnRtEvtInfo * pPwRtEvtInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pMplsPsnEntry = NULL;
    UINT4               u4PwVcIndex = L2VPN_ZERO;
    uGenU4Addr          GenU4IpAddr;
    uGenU4Addr          GenU4DestNetAddr;
    UINT1               u1Event = L2VPN_ZERO;

    MEMSET (&GenU4IpAddr, L2VPN_ZERO, sizeof (uGenU4Addr));
    MEMSET (&GenU4DestNetAddr, L2VPN_ZERO, sizeof (uGenU4Addr));

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "L2VpnProcessRouteEvent: Entry\r\n");

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN is Down\r\n");
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwRtEvtInfo->u1AddrType)
    {
        if (pPwRtEvtInfo->u1CmdType == IP6_ROUTE_ADD)
        {
            u1Event = L2VPN_PWVC_UP;
        }
        else if (pPwRtEvtInfo->u1CmdType == IP6_ROUTE_DEL)
        {
            u1Event = L2VPN_PWVC_DOWN;
        }
        else
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Route Change: Invalid command type\r\n");
            return L2VPN_FAILURE;
        }
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwRtEvtInfo->u1AddrType)
#endif
    {
        if (pPwRtEvtInfo->u1CmdType == NETIPV4_ADD_ROUTE)
        {
            u1Event = L2VPN_PWVC_UP;
        }
        else if (pPwRtEvtInfo->u1CmdType == NETIPV4_DELETE_ROUTE)
        {
            u1Event = L2VPN_PWVC_DOWN;
        }
        else
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Route Change: Invalid command type\r\n");
            return L2VPN_FAILURE;
        }
    }

    MplsGetPrefix (&(pPwRtEvtInfo->DestAddr),
                   &(pPwRtEvtInfo->DestMask),
                   (UINT2) (pPwRtEvtInfo->u1AddrType), &GenU4DestNetAddr);

    for (u4PwVcIndex = 1; u4PwVcIndex <=
         L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwVcIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

        if (pPwVcEntry == NULL)
        {
            continue;
        }

        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_DOWN)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Route Change: PW %d IPv4 -%#x IPv6 -%s"
                        "Admin Down "
                        "- HW Programming skipped\r\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)),
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Route Change: PW %d %#x Admin Down "
                        "- HW Programming skipped\r\n",
                        pPwVcEntry->u4PwVcIndex,
                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                    &(pPwVcEntry->PeerAddr)));
#endif
            continue;
        }

        pMplsPsnEntry = L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
        if (pMplsPsnEntry == NULL)
        {
            continue;
        }

        if (L2VPN_PWVC_MPLS_MPLS_TYPE (pMplsPsnEntry) != L2VPN_MPLS_TYPE_VCONLY)
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_ALL_FLAG,
                    "Route Change: HW Programming for PW %d %#x called\r\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_ALL_FLAG,
                    "Route Change: HW Programming for PW %d %#x called\r\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)));

#endif
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwRtEvtInfo->u1AddrType)
        {
            MEMCPY ((GenU4IpAddr.Ip6Addr.u1_addr),
                    pPwVcEntry->PeerAddr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwRtEvtInfo->u1AddrType)
#endif
        {
            CONVERT_TO_INTEGER (pPwVcEntry->PeerAddr.au1Ipv4Addr,
                                GenU4IpAddr.u4Addr);
            GenU4IpAddr.u4Addr = OSIX_NTOHL (GenU4IpAddr.u4Addr);
        }

        MplsGetPrefix (&(GenU4IpAddr),
                       &(pPwRtEvtInfo->DestMask),
                       (UINT2) (pPwRtEvtInfo->u1AddrType), &GenU4IpAddr);

        if (L2VPN_ZERO !=
            MEMCMP (&GenU4IpAddr, &GenU4DestNetAddr, sizeof (uGenU4Addr)))
        {
            continue;
        }

        if (L2VpnUpdatePwVcOperStatus (pPwVcEntry, u1Event) == L2VPN_SUCCESS)
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Route Change %d: PW %d for %#x Oper %d failed "
                    "in HW\r\n", pPwRtEvtInfo->u1CmdType,
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    u1Event, Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Route Change %d: PW %d for %#x Oper %d failed "
                    "in HW\r\n", pPwRtEvtInfo->u1CmdType,
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    u1Event);
#endif
    }

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "L2VpnProcessRouteEvent: Exit\r\n");

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessOamMegEvent                                   */
/* Description   : This routine processes OAM MEG Change Notifications.      */
/* Input(s)      : pOamInfo - pointer to OAM event Info                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessOamMegEvent (tL2VpnOamInfo * pOamInfo)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1Event = 0;

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "\rL2VpnProcessOamMegEvent: Entry\n");

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\r L2VPN is Down\n");
        return L2VPN_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pOamInfo->u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\r PW doesnot exist\n");
        return L2VPN_FAILURE;
    }
    switch (pOamInfo->u1CmdType)
    {
        case MPLS_OAM_MEG_ASSOC_WITH_PW:
            pPwVcEntry->u4MegIndex = pOamInfo->u4MegIndex;
            pPwVcEntry->u4MeIndex = pOamInfo->u4MeIndex;
            pPwVcEntry->u4MpIndex = pOamInfo->u4MpIndex;

            if (L2vpnExtNotifyPwStatusToApp (pPwVcEntry,
                                             L2VPN_PWVC_OPER_APP_CP_OR_MGMT)
                == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "\r PW status notification to "
                           "applciation failed\n");
                return L2VPN_FAILURE;
            }
            break;
        case MPLS_OAM_UPDATE_PW_MEG_OAM_STATUS:
            if (pPwVcEntry->bPwIntOamEnable == L2VPN_PW_OAM_DISABLE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                            "L2vpnExtHandlePathStatusChgForPw: "
                            "OAM is disabled on a Pseudowire"
                            "PW Index = %d: INTMD-EXIT\r\n",
                            pOamInfo->u4PwIndex);
                return L2VPN_FAILURE;
            }
            /* Update the PW operational status here. */
            if (pOamInfo->u1PathStatus == MPLS_PATH_STATUS_UP)
            {
                pPwVcEntry->bOamEnable = TRUE;
                u1Event = L2VPN_PWVC_UP;
            }
            else if (pOamInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN)
            {
                if (pPwVcEntry->bOamEnable != TRUE)
                {
                    /* Proactive session parameters should have been updated
                     * before notifying OAM status */
                    L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                               "L2vpnExtHandlePathStatusChgForPw: "
                               "Failed to update PW OAM operational status \n");
                    return L2VPN_FAILURE;
                }
                u1Event = L2VPN_PWVC_DOWN;
            }
            else
            {
                pPwVcEntry->bOamEnable = FALSE;
                pPwVcEntry->u1OamOperStatus = L2VPN_PWVC_OPER_UNKWN;
                u1Event = L2VPN_PWVC_UP;
            }
            if (L2VpnUpdateOperStatus (pPwVcEntry, u1Event,
                                       L2VPN_PWVC_OPER_APP_OAM) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                           "L2vpnExtHandlePathStatusChgForPw: "
                           "Failed to update PW OAM operational status \n");
                return L2VPN_FAILURE;
            }
            break;
        default:
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "\r Invalid command type from MEG\n");
            return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessPwVcCleanupEvent                              */
/* Description   : This routine does the cleanup of pending remote PW VCs    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
L2VpnProcessPwVcCleanupEvent (tTmrAppTimer * pAppTimer)
{
    tPwVcActivePeerSsnEntry *pPeerSsn = NULL, PeerSsn;
    tPwVcLblMsgEntry   *pLblEntry = NULL;
    UINT4               u4PeerAddr = L2VPN_ZERO;

    if (L2VPN_INITIALISED != TRUE)
    {
        return L2VPN_FAILURE;
    }

    pLblEntry = MPLS_LBL_INFO_FROM_TIMER (pAppTimer);
    L2VpnReleaseWaitingRemLblMsg (&pLblEntry->LblInfo);

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pLblEntry->LblInfo.u2AddrType)
    {
        MEMCPY (&(PeerSsn.PeerAddr),
                &(pLblEntry->LblInfo.PeerAddr.Ip6Addr.u1_addr),
                IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pLblEntry->LblInfo.u2AddrType)
#endif
    {
        MEMCPY (&(PeerSsn.PeerAddr), &(pLblEntry->LblInfo.PeerAddr.u4Addr),
                IPV4_ADDR_LENGTH);
    }
    PeerSsn.u1AddrType = (UINT1) pLblEntry->LblInfo.u2AddrType;

    u4PeerAddr = pLblEntry->LblInfo.PeerAddr.u4Addr;
    if (((pPeerSsn =
          RBTreeGet (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo),
                     &PeerSsn)) != NULL)
        && (pPeerSsn->u4Status == L2VPN_SESSION_UP))
    {
        /* delete entry from lbl msg list */
        TMO_DLL_Delete (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                        &(pLblEntry->NextNode));
        if (MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID,
                                (UINT1 *) pLblEntry) == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Mem release failure - label msg pool\r\n");
        }
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnProcessPwVcCleanupEvent called\r\n");
        if ((TMO_DLL_Count (L2VPN_PWVC_LIST
                            (pPeerSsn)) == L2VPN_ZERO) &&
            (TMO_DLL_Count (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn)) == L2VPN_ZERO))
        {
            RBTreeRem (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo), pPeerSsn);
            MemReleaseMemBlock (L2VPN_PEER_SSN_POOL_ID, (UINT1 *) pPeerSsn);
            pPeerSsn = NULL;
            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnProcessPwVcCleanupEvent Peer %x Session is deleted\r\n",
                        u4PeerAddr);
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnDeletePwEnetSpecEntry                                */
/* Description   : This routine deletes PwVc Enet Entry and releases memory. */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
VOID
L2VpnDeletePwEnetSpecEntry (tPwVcEntry * pPwVcEntry)
{
    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
    {
        if (MemReleaseMemBlock (L2VPN_SERV_SPEC_POOL_ID,
                                (UINT1 *) L2VPN_PWVC_ENET_ENTRY (pPwVcEntry))
            == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "Mem release failure - PwVc Enet Spec Entry\r\n");
        }
        L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnDeletePwVcEntry                                      */
/* Description   : This routine deletes PwVc entry and releases memory       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
VOID
L2VpnDeletePwVcEntry (tPwVcEntry * pPwVcEntry)
{
    pPwVcEntry->b1IsStaticLabel = FALSE;

    TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                    &(pPwVcEntry->PendMsgToLdpNode));

    if (TMO_DLL_Count (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo)) == 0)
    {
        gpL2VpnGlobalInfo->u1SendToLdpMsgSpacingCount = 0;
        gpL2VpnGlobalInfo->u1SendToLdpSpacingTime = 0;
    }

    /* Remove the RBTree Entry for InVCLable */
    L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
    L2VpnPwSetOutVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntry) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "Mem release failure - PwVc Entry\r\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnEstablishPwVc                                        */
/* Description   : This routine Established PwVc                             */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnEstablishPwVc (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    INT4                i4IfStatus = L2VPN_FAILURE;
    UINT4               u4InTnlIndex = L2VPN_ZERO;
    UINT4               u4OutTnlIndex = L2VPN_ZERO;

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "PW to be established for PW VC Index %d\r\n",
                pPwVcEntry->u4PwVcIndex);

    /* Create/update entry in the FM Module */
    i4Status = L2VpnFmPwVcSet (pPwVcEntry);
    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "Creation of PWVC Entry in FM failed\r\n");
        return i4Status;
    }

    pPwVcEntry->u1CPOrMgmtOperStatus = L2VPN_PWVC_CP_DEF_OPER_STATUS;
    pPwVcEntry->i1OperStatus = L2VPN_PWVC_OPER_DOWN;

    /* MS-PW: skip if AC is not attached */
    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
    {
        /* Check AC interface status and set the AC fault 
         * if the AC is down*/
        L2VpnCheckAcUpStatus (pPwVcEntry, L2VPN_TRUE);

        i4IfStatus = L2VpnRegisterWithIfModule (pPwVcEntry);
        if ((i4IfStatus == L2VPN_FAILURE) && (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) == L2VPN_ZERO))    /* MS-PW */
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "AC Interface or Attached PW"
                       "does not exist\r\n");
            return L2VPN_FAILURE;
        }
    }
    else
    {
        if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) == L2VPN_ZERO)
        {
            pPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_AC_BITMASK;
        }
    }

    /* support for manual pw vc setup */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == (UINT1) L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &=
            (UINT1) (~(L2VPN_PWVC_STATUS_NOT_FORWARDING));

        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &=
            (UINT1) ~(L2VPN_PWVC_STATUS_NOT_FORWARDING);

        /* update out-bound list */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_UP);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Outbound List failed\r\n");
            return L2VPN_FAILURE;
        }

        pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

        if (pPwVcMplsEntry == NULL)
        {
            return L2VPN_FAILURE;
        }

        if (pPwVcMplsEntry->u1MplsType == L2VPN_MPLS_TYPE_TE)
        {
            pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                       L2VPN_PWVC_PSN_ENTRY
                                                       (pPwVcEntry));
            if (pMplsTnlEntry == NULL)
            {
                return L2VPN_FAILURE;
            }

            u4OutTnlIndex = pMplsTnlEntry->unTnlInfo.TeInfo.u4TnlIndex;

            pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                        L2VPN_PWVC_PSN_ENTRY
                                                        (pPwVcEntry));
            if (pMplsInTnlEntry == NULL)
            {
                return L2VPN_FAILURE;
            }
            u4InTnlIndex = pMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex;

            /* Pw mapped over P2MP tunnel - Out tunnel/ In-Tunnel can be zero */
            if ((u4OutTnlIndex == 0) && (u4InTnlIndex != 0))
            {
                i4Status =
                    L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_IN);
            }
            else if ((u4OutTnlIndex != 0) && (u4InTnlIndex == 0))
            {
                i4Status =
                    L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_OUT);
            }
            else
            {
                i4Status =
                    L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_BOTH);
            }
        }
        else
        {
            i4Status = L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_BOTH);
        }
        if (i4Status == L2VPN_SUCCESS)
        {
            /* Update Pw Oper status */
            i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                            pPwVcEntry->u1LocalStatus,
                            pPwVcEntry->u1RemoteStatus);
            }
        }

        /* Set to "Not Applicable" for manually setup PW */
        L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) = L2VPN_PWVC_RMT_STAT_NOT_APPL;
        return L2VPN_SUCCESS;
    }

    /* Signalling is used to setup PW VC, use respective sig method */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)) &&
        (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
    {
        /* Check Ssn if already exits, 
         * update tPwVcEntry if Remote already sent Lbl Msg */
        if (L2VpnCheckSsnFromPeerSsnList (pPwVcEntry, FALSE) == L2VPN_SUCCESS)
        {
            i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);
        }
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnAddPwVcToPwVcTable                                   */
/* Description   : This routine adds PwVc Entry to the Hash Table            */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnAddPwVcToPwVcTable (tPwVcEntry * pPwVcEntry)
{
    struct rbtree      *pRbTree = NULL;
    UINT1              *pu1Saii = NULL;
    pu1Saii = pPwVcEntry->au1Saii;
    if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_MANUAL
#ifdef VPLSADS_WANTED
        && L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER
#endif
        )
    {
        pRbTree =
            (L2VPN_PWVC_OWNER (pPwVcEntry) ==
             L2VPN_PWVC_OWNER_PWID_FEC_SIG) ?
            L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo) :
            (((*pu1Saii) != L2VPN_ZERO) ?
             L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo) :
             L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));

        if (RBTreeAdd (pRbTree, pPwVcEntry) == RB_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "PW VC RBTree Add Failed\r\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : L2VpnDeletePwVcFromPwVcTable                              */
/* Description   : This routine deletes PwVc Entry from the Hash Table       */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnDeletePwVcFromPwVcTable (tPwVcEntry * pPwVcEntry)
{
    tPwVcInfo          *pPwVcInfo = NULL;
    tPwVcEntry         *pTmpPwVcEntry = NULL;
    UINT4               u4Interval;
    struct rbtree      *pRbTree = NULL;
    tGenU4Addr          GenU4Addr;
    UINT1              *pu1Saii = NULL;
    pu1Saii = pPwVcEntry->au1Saii;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &pPwVcEntry->PeerAddr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_MANUAL
#ifdef VPLSADS_WANTED
        && L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER
#endif
        )
    {
        pTmpPwVcEntry = L2VpnGetPwVcEntry (L2VPN_PWVC_OWNER (pPwVcEntry),
                                           L2VPN_PWVC_ID (pPwVcEntry),
                                           (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry),
                                           L2VPN_PWVC_AGI (pPwVcEntry),
                                           L2VPN_PWVC_SAII (pPwVcEntry),
                                           L2VPN_PWVC_TAII (pPwVcEntry),
                                           GenU4Addr);

        /* Below check is to avoid deleting the existing PWVC entry when same PWVc 
         * configurations are done multiple times */
        if ((pTmpPwVcEntry != NULL) &&
            (L2VPN_PWVC_INDEX (pPwVcEntry) == L2VPN_PWVC_INDEX (pTmpPwVcEntry)))
        {
            pRbTree =
                (L2VPN_PWVC_OWNER (pPwVcEntry) ==
                 L2VPN_PWVC_OWNER_PWID_FEC_SIG) ?
                L2VPN_PWVC_FEC128_RB_PTR (gpPwVcGlobalInfo) :
                (((*pu1Saii) != L2VPN_ZERO) ?
                 L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo) :
                 L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo));

            RBTreeRem (pRbTree, pPwVcEntry);
        }
    }

    pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));

    /* Reset all days and interval statistics */
    for (u4Interval = 1; u4Interval <= L2VPN_PWVC_MAX_PERF_DAYS; u4Interval++)
    {
        MEMSET (&pPwVcInfo->aPwVcPerf1DayIntervalInfo[u4Interval - 1], 0,
                sizeof (tPwVcPerfTotalEntry));

        L2VPN_PWVC_PERF_1DAY_VALID_DATA (pPwVcInfo, u4Interval) =
            L2VPN_SNMP_FALSE;
    }

    for (u4Interval = 1; u4Interval <= L2VPN_PWVC_MAX_PERF_INTV; u4Interval++)
    {
        MEMSET (&pPwVcInfo->aPwVcPerfIntervalInfo[u4Interval - 1],
                0, sizeof (tPwVcPerfIntervalEntry));

        L2VPN_PWVC_PERF_INT_VALID_DATA (pPwVcInfo, u4Interval) =
            L2VPN_SNMP_FALSE;
    }

    pPwVcInfo->u1PwVcPerfDayIndex = L2VPN_PWVC_MIN_PERF_INTV;
    pPwVcInfo->pPwVcEntry = NULL;

    return;
}

/*****************************************************************************/
/* Function Name : L2vpnVplsVfiAdd                                           */
/* Description   : This function adds a VPLSEntry to RB Tree.VFI Name acts   */
/*                 as the key for this RB Tree.                              */
/* Input(s)      : pVplsEntry - pointer to Vpls entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2vpnVplsVfiAdd (tVPLSEntry * pVplsEntry)
{
    if (L2VPN_VPLS_NAME (pVplsEntry)[0] != 0)
    {
        /* VFI Name acts as the key for the RB Tree */
        if (RBTreeAdd (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo),
                       (tRBElem *) pVplsEntry) == RB_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "L2vpnVplsVfiAdd : VPLS VFI RBTree Add Failed \t\n");
            return L2VPN_FAILURE;
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnVplsVfiDelete                                        */
/* Description   : This function deletes VPLSEntry from the RB Tree.VFI Name */
/*                 acts as the key for this RB Tree                          */
/* Input(s)      : pVplsEntry - pointer to Vpls entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2vpnVplsVfiDelete (tVPLSEntry * pVplsEntry)
{
    if (L2VPN_VPLS_NAME (pVplsEntry)[0] != 0)
    {
        RBTreeRem (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) pVplsEntry);
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSetupPwVc                                            */
/* Description   : This routine processes PwVc Setup event                   */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSetupPwVc (tPwVcEntry * pPwVcEntry)
{
    tPwVcEntry         *pTmpPwVcEntry = NULL;
    INT4                i4Status;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    pTmpPwVcEntry = L2VpnGetPwVcEntry (L2VPN_PWVC_OWNER (pPwVcEntry),
                                       L2VPN_PWVC_ID (pPwVcEntry),
                                       (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry),
                                       L2VPN_PWVC_AGI (pPwVcEntry),
                                       L2VPN_PWVC_SAII (pPwVcEntry),
                                       L2VPN_PWVC_TAII (pPwVcEntry), GenU4Addr);
    if (pTmpPwVcEntry == NULL)
    {
        /* Setting up PwVc for the First Time */
        /* Add entry into Hash table */
        L2VpnAddPwVcToPwVcTable (pPwVcEntry);
    }

    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG) &&
          (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry) ==
           L2VPN_GEN_FEC_AII_TYPE_1))) && (pPwVcEntry->bIsStaticPw == TRUE))
    {
        /* The variable b1IsStaticLabel is set as true if Pw Owner is PwId Fec
         * or Gen Fec Aii Type 1 and Static PW is set as true */
        pPwVcEntry->b1IsStaticLabel = TRUE;
    }

    /* Establish PW VC, IN-LSP */
    i4Status = L2VpnEstablishPwVc (pPwVcEntry);
    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "PwVc establishment failed\r\n");
        return i4Status;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnDeletePwVc                                           */
/* Description   : This routine processes PwVc Delete event                  */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDeletePwVc (tPwVcEntry * pPwVcEntry)
{
    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    if (TMO_DLL_Is_Node_In_List (&(pPwVcEntry->PendMsgToLdpNode)))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG6 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW Peer IPv4 - %d %d.%d.%d.%d IPv6 - %s is present in Pending Message List\r\n"
                    "PW will be deleted when removed from Pending list\r\n",
                    pPwVcEntry->u4PwVcIndex,
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[3],
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d %d.%d.%d.%d is present in Pending Message List\r\n"
                    "PW will be deleted when removed from Pending list\r\n",
                    pPwVcEntry->u4PwVcIndex,
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                    pPwVcEntry->PeerAddr.au1Ipv4Addr[3]);
#endif
        return L2VPN_SUCCESS;
    }

    L2VpnDeRegisterWithIfModule (pPwVcEntry);

    if (L2VPN_PWVC_ID (pPwVcEntry) != L2VPN_ZERO)
    {
        /* Delete the PwId based node from RbTree */
        L2VpnPwIdDelNode (pPwVcEntry);
    }

    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_UP)
        {
            /* Delete PW Entry in FM */
            i4Status = L2VpnUpdatePwVcOperStatus
                (pPwVcEntry, L2VPN_PWVC_DELETE);

            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "PWVC status(delete) update error\r\n");
            }
            /* update Outbound list */
            i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry,
                                                    L2VPN_PWVC_DOWN);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Updation of Outbound List failed\r\n");
            }
        }
        else
        {
            i4Status = L2VPN_SUCCESS;
        }

        /* relinquish and Delete Enet, Priority Mapping, and mpls entries */
        L2vpnPwEnetMplsDelete (pPwVcEntry);
        /* For Both VPLS/VPWS, a VPLS Entry must have been there
         * before destroying it.If not return failure */
        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
        {
            L2vpnDeletePwVplsEntry (pPwVcEntry);
        }
        else if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
        {
            L2vpnDeleteVplsEntry (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));

            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = 0;
        }
        /*Delete RbTree entry for PwIfIndex table */
        L2VpnPwIfIndexDeleteNode (pPwVcEntry);
        /* Delete entry into Hash table */
        L2VpnDeletePwVcFromPwVcTable (pPwVcEntry);

        L2VpnDeletePwEnetSpecEntry (pPwVcEntry);

        /* Delete the rec and release the memory */
        L2VpnDeletePwVcEntry (pPwVcEntry);
        return i4Status;
    }

    /* Signalling is used to setup PW VC, use respective sig method */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) ==
          L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         (L2VPN_PWVC_OWNER (pPwVcEntry) ==
          L2VPN_PWVC_OWNER_GEN_FEC_SIG)) &&
        (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
    {
        /* update Peer Ssn List */
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                    (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

        if (L2VpnUpdatePeerSsnList (GenU4Addr, L2VPN_PWVC_DELETE_IN_SESSION,
                                    pPwVcEntry) != NULL)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "More PWs are associated with this peer session\r\n");
        }
        /* Delete PW Entry in FM */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DELETE);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Oper status failed\r\n");
        }

        /* update Outbound list */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Outbound List failed\r\n");
        }

        if (L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
        {
            i4Status = L2VpnSendLdpPwVcLblWdrawMsg (pPwVcEntry, 0);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Withdraw Msg failed\r\n");
            }
        }

        if (L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
        {
            i4Status = L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Release Msg failed\r\n");
            }
        }

        /* relinquish and Delete Enet, Priority Mapping, and mpls entries */
        L2vpnPwEnetMplsDelete (pPwVcEntry);
        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
        {
            L2vpnDeletePwVplsEntry (pPwVcEntry);
        }
        else if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
        {
            L2vpnDeleteVplsEntry (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
            L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = 0;
        }

        /*Delete RbTree entry for PwIfIndex table */
        L2VpnPwIfIndexDeleteNode (pPwVcEntry);
        /* Delete entry into Hash table */
        L2VpnDeletePwVcFromPwVcTable (pPwVcEntry);

        L2VpnDeletePwEnetSpecEntry (pPwVcEntry);

        /* Delete PW VC Entry */
        L2VpnDeletePwVcEntry (pPwVcEntry);
    }
    /* Add support for other sig methods here */
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unsupported PSN Type\r\n");
        return L2VPN_FAILURE;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnDownPwVc                                             */
/* Description   : This routine processes PwVc Down event                    */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDownPwVc (tPwVcEntry * pPwVcEntry)
{
    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    L2VpnDeRegisterWithIfModule (pPwVcEntry);

    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
         (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE)) &&
        (pPwVcEntry->b1IsStaticLabel == FALSE))
    {
        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        /* Update Pw Oper status */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DELETE);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
        }

        /* update out-bound list */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Outbound List failed\r\n");
        }
        return i4Status;
    }

    /* Signalling is used to setup PW VC, use respective sig method */
    if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
         (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)) &&
        (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS))
    {
        pPwVcEntry->u1RemoteCcAdvert = L2VPN_ZERO;
        pPwVcEntry->u1RemoteCvAdvert = L2VPN_ZERO;
        /* update Peer Ssn List */
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                    (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

        if (L2VpnUpdatePeerSsnList (GenU4Addr,
                                    L2VPN_PWVC_DELETE_IN_SESSION,
                                    pPwVcEntry) != NULL)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "More PWs are associated with this peer session\r\n");
        }

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        /* Update Pw Oper status */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DELETE);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwOperStatus updation: PwLocalStatus %x PwRemoteStatus %x\r\n",
                        pPwVcEntry->u1LocalStatus, pPwVcEntry->u1RemoteStatus);
        }

        /* update Outbound list */
        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Updation of Outbound List failed\r\n");
        }

        if (L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
        {
            i4Status = L2VpnSendLdpPwVcLblWdrawMsg (pPwVcEntry, 0);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Withdraw Msg failed\r\n");
            }
            else
            {
                L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_LDP_INVALID_LABEL);
            }
        }
        if (L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry) != L2VPN_INVALID_LABEL)
        {
            i4Status = L2VpnSendLdpPwVcLblRelMsg (pPwVcEntry, L2VPN_ZERO);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Request to send Label Release Msg failed\r\n");
            }
        }
    }
    /* Add support for other sig methods here */
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unsupported PSN Type\r\n");
        return L2VPN_FAILURE;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnUpdatePwVcOutboundList                              */
/* Description   : This routine updates the Outbound Mapping list           */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : u1Event - type of PwVc event (up/down/delete)             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdatePwVcOutboundList (tPwVcEntry * pPwVcEntry, UINT1 u1Event)
{
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;

    pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    switch (u1Event)
    {
        case L2VPN_PWVC_UP:

            if (L2vpnAddMappingList (pPwVcEntry, pPwVcMplsEntry->u1MplsType) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2vpn Mapping Table add failed\n");
                return L2VPN_FAILURE;
            }

            if (L2vpnAddInMappingList (pPwVcEntry, pPwVcMplsEntry->u1MplsType)
                == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2vpn In Mapping Table add failed\n");
                return L2VPN_FAILURE;
            }

            break;

        case L2VPN_PWVC_DOWN:

            if (L2vpnDelMappingList (pPwVcEntry, pPwVcMplsEntry->u1MplsType) !=
                L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2vpn Mapping Table delete failed \r\n");
                return L2VPN_FAILURE;
            }

            if (L2vpnDelInMappingList (pPwVcEntry, pPwVcMplsEntry->u1MplsType)
                != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2vpn In Mapping Table delete failed\n");
                return L2VPN_FAILURE;
            }

            break;

        default:
            break;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnUpdatePwVcOperStatus                                 */
/* Description   : This routine updates the PwVc Oper status                 */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : u1Event - type of PwVc event (up/down/delete)             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdatePwVcOperStatus (tPwVcEntry * pPwVcEntry, UINT1 u1Event)
{
    return (L2VpnUpdateOperStatus (pPwVcEntry, u1Event,
                                   L2VPN_PWVC_OPER_APP_CP_OR_MGMT));
}

/************************************************************************
 *  Function Name   : L2VpnFec128RbTreeCmpFunc
 *  Description     : RBTree Compare function for Fec128 PW Table 
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnFec128RbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcEntry         *pPwVcEntry1 = (tPwVcEntry *) pRBElem1;
    tPwVcEntry         *pPwVcEntry2 = (tPwVcEntry *) pRBElem2;

    if (pPwVcEntry1->u4PwVcID < pPwVcEntry2->u4PwVcID)
    {
        return -1;
    }
    else if (pPwVcEntry1->u4PwVcID > pPwVcEntry2->u4PwVcID)
    {
        return 1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnPwVcInLblRbTreeCmpFunc
 *  Description     : RBTree Compare function for Pw-InVcLabel 
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnPwVcInLblRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcEntry         *pPwVcEntry1 = (tPwVcEntry *) pRBElem1;
    tPwVcEntry         *pPwVcEntry2 = (tPwVcEntry *) pRBElem2;

    if (pPwVcEntry1->u4InVcLabel < pPwVcEntry2->u4InVcLabel)
    {
        return -1;
    }
    else if (pPwVcEntry1->u4InVcLabel > pPwVcEntry2->u4InVcLabel)
    {
        return 1;
    }

    return 0;
}

/**************************************************************************
 *Function Name   : L2VpnFec129VplsRbTreeCmpFunc
 *Description     : RBTree Compare function for Fec129 PW Table
 * Input          : Two RBTree Nodes to be compared
 * Output         : None
 *  Returns       : 1/(-1)/0
 * ************************************************************************/
PRIVATE INT4
L2VpnFec129RbTreeVplsCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    INT4                i4Return = 0;
    tPwVcEntry         *pPwVcEntry1 = (tPwVcEntry *) pRBElem1;
    tPwVcEntry         *pPwVcEntry2 = (tPwVcEntry *) pRBElem2;

    if ((i4Return = MEMCMP (pPwVcEntry1->au1Agi,
                            pPwVcEntry2->au1Agi, L2VPN_PWVC_MAX_AI_LEN)) != 0)
    {
        return i4Return;
    }
#ifdef MPLS_IPV6_WANTED

    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry1->i4PeerAddrType)
    {
        return (MEMCMP (&(pPwVcEntry1->PeerAddr.Ip6Addr.u1_addr),
                        &(pPwVcEntry2->PeerAddr.Ip6Addr.u1_addr),
                        IPV6_ADDR_LENGTH));
    }
    else
#endif
    {
        return (MEMCMP (&(pPwVcEntry1->PeerAddr.au1Ipv4Addr),
                        &(pPwVcEntry2->PeerAddr.au1Ipv4Addr),
                        IPV4_ADDR_LENGTH));
    }

    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnFec129RbTreeCmpFunc
 *  Description     : RBTree Compare function for Fec129 PW Table 
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnFec129RbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    INT4                i4Return = 0;
    tPwVcEntry         *pPwVcEntry1 = (tPwVcEntry *) pRBElem1;
    tPwVcEntry         *pPwVcEntry2 = (tPwVcEntry *) pRBElem2;
    if ((i4Return = MEMCMP (pPwVcEntry1->au1Agi,
                            pPwVcEntry2->au1Agi, L2VPN_PWVC_MAX_AI_LEN)) != 0)
    {
        return i4Return;
    }
    if ((i4Return = MEMCMP (pPwVcEntry1->au1Taii,
                            pPwVcEntry2->au1Taii, L2VPN_PWVC_MAX_AI_LEN)) != 0)
    {
        return i4Return;
    }

    if ((i4Return = MEMCMP (pPwVcEntry1->au1Saii,
                            pPwVcEntry2->au1Saii, L2VPN_PWVC_MAX_AI_LEN)) != 0)
    {
        return i4Return;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnPeerAddrRbTreeCmpFunc 
 *  Description     : RBTree Compare function for Peer Address PW Table 
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnPeerAddrRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcActivePeerSsnEntry *pPeerSsn1 = (tPwVcActivePeerSsnEntry *) pRBElem1;
    tPwVcActivePeerSsnEntry *pPeerSsn2 = (tPwVcActivePeerSsnEntry *) pRBElem2;

#ifdef MPLS_IPV6_WANTED
    if (pPeerSsn1->u1AddrType < pPeerSsn2->u1AddrType)
    {
        return -1;
    }
    else if (pPeerSsn1->u1AddrType > pPeerSsn2->u1AddrType)
    {
        return 1;
    }

    if (MPLS_IPV6_ADDR_TYPE == pPeerSsn1->u1AddrType)
    {
        return (MEMCMP (&(pPeerSsn1->PeerAddr.Ip6Addr.u1_addr),
                        &(pPeerSsn2->PeerAddr.Ip6Addr.u1_addr),
                        IPV6_ADDR_LENGTH));
    }
    else
#endif
    {
        return (MEMCMP (&(pPeerSsn1->PeerAddr.au1Ipv4Addr),
                        &(pPeerSsn2->PeerAddr.au1Ipv4Addr), IPV4_ADDR_LENGTH));
    }

}

/************************************************************************
 *  Function Name   : L2vpnAddMappingList 
 *  Description     : Function to add a PwVC to an appropriate RBTree
 *                    to search based on Te/NonTe/Vc Info  
 *  Input           : pPwVcEntry - PwVcEntry to be added to the Tree
 *                    u1Type - Tunnel Type (Te/NonTe/VcOnly)
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2vpnAddMappingList (tPwVcEntry * pPwVcEntry, UINT1 u1Type)
{
    struct rbtree      *pRBTree = NULL;
    tPwVcMplsMappingEntry *pPwVcMplsMappingEntry = NULL, PwVcMplsMappingEntry;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    UINT4               u4TnlIndex = L2VPN_ZERO;
    UINT4               u4PeerLsr = L2VPN_ZERO;
    UINT4               u4LclLsr = L2VPN_ZERO;
    UINT4               u4TnlInst = L2VPN_ZERO;
    UINT4               u4TnlXcIndex = L2VPN_ZERO;
    UINT4               u4TnlIfIndex = L2VPN_ZERO;
    UINT4               u4MplsTnlLspId = L2VPN_ZERO;
    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    MEMSET (&PwVcMplsMappingEntry, 0, sizeof (tPwVcMplsMappingEntry));

    pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    switch (u1Type)
    {
        case L2VPN_MPLS_TYPE_TE:
            /* Other than the initial instance of the tunnel will be notified
             * from TE module during tunnel re-establishment */
            u4TnlIndex = pMplsTnlEntry->unTnlInfo.TeInfo.u4TnlIndex;
            if (u4TnlIndex == 0)
            {
                return L2VPN_SUCCESS;
            }
            CONVERT_TO_INTEGER (pMplsTnlEntry->unTnlInfo.TeInfo.TnlPeerLSR,
                                u4PeerLsr);
            CONVERT_TO_INTEGER (pMplsTnlEntry->unTnlInfo.TeInfo.TnlLclLSR,
                                u4LclLsr);
            u4PeerLsr = OSIX_NTOHL (u4PeerLsr);
            u4LclLsr = OSIX_NTOHL (u4LclLsr);

            TeGetActiveTnlInst (u4TnlIndex, &u4TnlInst, u4LclLsr, u4PeerLsr,
                                &u4TnlXcIndex, &u4TnlIfIndex);

            if (u4TnlInst != L2VPN_ZERO)
            {
                pMplsTnlEntry->unTnlInfo.TeInfo.u2TnlInstance
                    = (UINT2) u4TnlInst;
                pPwVcEntry->u4PwL3Intf = u4TnlIfIndex;
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "ActiveTnlInst: %u u4TnlIfIndex: %u\n",
                            u4TnlInst, u4TnlIfIndex);
            }

            PwVcMplsMappingEntry.unTnlInfo.TeInfo =
                pMplsTnlEntry->unTnlInfo.TeInfo;
            pRBTree = L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        case L2VPN_MPLS_TYPE_NONTE:
            MEMCPY (&PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.PeerOrSrcAddr,
                    &(pPwVcEntry->PeerAddr), sizeof (uGenAddr));

            PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.u1AddrType =
                (UINT1) pPwVcEntry->i4PeerAddrType;
            pRBTree = L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        case L2VPN_MPLS_TYPE_VCONLY:
            PwVcMplsMappingEntry.unTnlInfo.VcInfo.u4IfIndex =
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.VcInfo.u4IfIndex;
            pRBTree = L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        default:
            return L2VPN_FAILURE;
    }
    if ((pPwVcMplsMappingEntry =
         RBTreeGet (pRBTree, &PwVcMplsMappingEntry)) == NULL)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer IPv4 -%#x IPv6 -%s has no Mpls Mapping Entry. "
                    " - To be created\r\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %#x has no Mpls Mapping Entry. "
                    " - To be created\r\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)));
#endif
        pPwVcMplsMappingEntry =
            (tPwVcMplsMappingEntry *) MemAllocMemBlk (L2VPN_MPLS_MAP_POOL_ID);

        if (pPwVcMplsMappingEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Mem allocation failure - addition in list\r\n");
            return L2VPN_FAILURE;
        }
        MEMCPY (pPwVcMplsMappingEntry, &PwVcMplsMappingEntry,
                sizeof (tPwVcMplsMappingEntry));
        pPwVcMplsMappingEntry->i4TnlType = (INT4) u1Type;
        TMO_DLL_Init (&pPwVcMplsMappingEntry->PwOutList);
        if (RBTreeAdd (pRBTree, pPwVcMplsMappingEntry) == RB_FAILURE)
        {
            MemReleaseMemBlock (L2VPN_MPLS_IN_MAP_POOL_ID,
                                (UINT1 *) pPwVcMplsMappingEntry);
            return L2VPN_FAILURE;
        }
    }

    pDll = &(pPwVcMplsMappingEntry->PwOutList);
    pNode = &(pPwVcEntry->PwOutNode);
    if (TMO_DLL_Is_Node_In_List (pNode))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer IPv4 - %x IPv6 - %s is already present in the Outbound "
                    "list. - addition skipped\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %#x is already present in the Outbound "
                    "list. - addition skipped\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif

        /* L2VpnUpdatePwVcOutboundList  is called
         * more than once for the same Node addition. 
         * This will happen if there is a PSN up/down
         * or AC up/down so, ignore the second
         * add and return from here */
        return L2VPN_SUCCESS;
    }
    TMO_DLL_Init_Node (pNode);

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outbound List empty. PW %d with Peer IPv4 - %x IPv6 - %s added to it.\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Outbound List empty. PW %d with %#x added to it.\n",
                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif

    /* Register with MPLS module to get status change */
    if (u1Type == L2VPN_MPLS_TYPE_TE)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2vpnAddMappingList: PW TE Type, "
                    "Outbound tunnel update in list for  "
                    "PW %d Peer IPv4 - %x IPv6 - %s \n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2vpnAddMappingList: PW TE Type, "
                    "Outbound tunnel update in list for  "
                    "PW %d peer %#x \n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr);
#endif
        i4Status = L2VpnRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2vpnAddMappingList: "
                       "Registration with TE outer tunnel failed\r\n");
        }

        if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
            (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE))
        {
            i4Status = L2VpnPostTeEvent (u4TnlIndex, (UINT2) u4TnlInst,
                                         u4LclLsr, u4PeerLsr,
                                         &u4MplsTnlLspId,
                                         L2VPN_ZERO, L2VPN_TE_TNL_STAT_REQ);

            if (i4Status == L2VPN_SUCCESS)
            {
                pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_OUT;
                pPwVcEntry->u4LspId = u4MplsTnlLspId;
            }
        }
    }
    else if (u1Type == L2VPN_MPLS_TYPE_NONTE)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2vpnAddMappingList: PW NON-TE Type, "
                    "Outbound tunnel update in list for  "
                    "PW %d peer Peer IPv4 - %x IPv6 - %s \n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2vpnAddMappingList: PW NON-TE Type, "
                    "Outbound tunnel update in list for  "
                    "PW %d peer %#x \n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr);
#endif

        i4Status = L2VpnRegWithNonTeLowerLayer (pPwVcEntry);
        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2vpnAddMappingList: "
                       "Registration with NON-TE outer tunnel failed\r\n");
        }
    }

    TMO_DLL_Add (pDll, pNode);

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Number of PWs in the Peer IPv4 - %x IPv6 - %s Outbound List =%d. "
                "PW %d insertion done in the list\n", GenU4Addr.Addr.u4Addr,
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                TMO_DLL_Count (pDll), pPwVcEntry->u4PwVcIndex);
#else
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "Number of PWs in the peer's %#x Outbound List =%d. "
                "PW %d insertion done in the list\n", GenU4Addr.Addr.u4Addr,
                TMO_DLL_Count (pDll), pPwVcEntry->u4PwVcIndex);
#endif

    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2vpnAddInMappingList 
 *  Description     : Function to add a PwVC to a InTnl RBTree
 *  Input           : pPwVcEntry - PwVcEntry to be added to the Tree
 *                    u1Type - Tunnel Type (Te)
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2vpnAddInMappingList (tPwVcEntry * pPwVcEntry, UINT1 u1Type)
{
    struct rbtree      *pRBTree = NULL;
    tPwVcMplsInMappingEntry *pPwVcMplsInMappingEntry = NULL;
    tPwVcMplsInMappingEntry PwVcMplsInMappingEntry;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    UINT4               u4TnlIndex = L2VPN_ZERO;
    UINT4               u4PeerLsr = L2VPN_ZERO;
    UINT4               u4LclLsr = L2VPN_ZERO;
    UINT4               u4TnlInst = L2VPN_ZERO;
    UINT4               u4TnlXcIndex = L2VPN_ZERO;
    UINT4               u4TnlIfIndex = L2VPN_ZERO;
    UINT4               u4MplsTnlLspId = L2VPN_ZERO;
#ifdef MPLS_SIG_WANTED
    tGenU4Addr          GenU4Prefix;
    uGenU4Addr          TempAddr;
#endif
    INT4                i4Status = L2VPN_FAILURE;
    BOOL1               bIsManualNonTePwVc = FALSE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));
    MEMSET (&GenU4Prefix, L2VPN_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_SIG_WANTED
    MEMSET (&TempAddr, L2VPN_ZERO, sizeof (uGenU4Addr));
#endif

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (u1Type == (UINT1) L2VPN_MPLS_TYPE_VCONLY)
    {
        /* Currently only In Mapping List is not handled only for vconly. */
        return L2VPN_SUCCESS;
    }

    MEMSET (&PwVcMplsInMappingEntry, 0, sizeof (tPwVcMplsInMappingEntry));

    pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);

    switch (u1Type)
    {
        case L2VPN_MPLS_TYPE_TE:
        {
            u4TnlIndex = pMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex;
            if (u4TnlIndex == 0)
            {
                return L2VPN_SUCCESS;
            }
            CONVERT_TO_INTEGER (pMplsInTnlEntry->unInTnlInfo.TeInfo.TnlPeerLSR,
                                u4PeerLsr);
            CONVERT_TO_INTEGER (pMplsInTnlEntry->unInTnlInfo.TeInfo.TnlLclLSR,
                                u4LclLsr);

            u4PeerLsr = OSIX_NTOHL (u4PeerLsr);
            u4LclLsr = OSIX_NTOHL (u4LclLsr);

            /* Get the Active Tunnel Instance and store it in the In Tnl Entry. */
            TeGetActiveTnlInst (u4TnlIndex, &u4TnlInst, u4LclLsr, u4PeerLsr,
                                &u4TnlXcIndex, &u4TnlIfIndex);

            if (u4TnlInst != L2VPN_ZERO)
            {
                pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance =
                    (UINT2) u4TnlInst;
                pMplsInTnlEntry->u4LsrXcIndex = u4TnlXcIndex;
            }

            PwVcMplsInMappingEntry.unInTnlInfo.TeInfo =
                pMplsInTnlEntry->unInTnlInfo.TeInfo;

            pRBTree = L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        }

        case L2VPN_MPLS_TYPE_NONTE:
        {
            if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
                (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE))
            {
                bIsManualNonTePwVc = TRUE;
            }

#ifdef MPLS_SIG_WANTED
            if (LdpGetSrcTransAddrFromDestAddr (&GenU4Addr, &GenU4Prefix,
                                                bIsManualNonTePwVc) ==
                LDP_FAILURE)
            {
                return L2VPN_FAILURE;
            }

#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                MEMCPY (pPwVcEntry->GenNonTeTnlSrcAddr.Ip6Addr.u1_addr,
                        GenU4Prefix.Addr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);

                if (MEMCMP
                    (&pPwVcEntry->GenNonTeTnlSrcAddr, &TempAddr,
                     sizeof (uGenU4Addr)) == 0)
                {
                    return L2VPN_SUCCESS;
                }

                MEMCPY (PwVcMplsInMappingEntry.unInTnlInfo.NonTeInfo.
                        PeerOrSrcAddr.Ip6Addr.u1_addr,
                        GenU4Prefix.Addr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            }
            else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
            {
                pPwVcEntry->GenNonTeTnlSrcAddr.u4Addr = GenU4Prefix.Addr.u4Addr;

                if (MEMCMP
                    (&pPwVcEntry->GenNonTeTnlSrcAddr, &TempAddr,
                     sizeof (uGenU4Addr)) == 0)
                {
                    return L2VPN_SUCCESS;
                }

                GenU4Prefix.Addr.u4Addr = OSIX_HTONL (GenU4Prefix.Addr.u4Addr);
                MEMCPY (&PwVcMplsInMappingEntry.unInTnlInfo.NonTeInfo.
                        PeerOrSrcAddr.au1Ipv4Addr,
                        (UINT1 *) &GenU4Prefix.Addr.u4Addr, IPV4_ADDR_LENGTH);
            }

            PwVcMplsInMappingEntry.unInTnlInfo.NonTeInfo.u1AddrType =
                (UINT1) pPwVcEntry->i4PeerAddrType;
#else
            MEMSET (pPwVcEntry->GenNonTeTnlSrcAddr, 0, sizeof (uGenU4Addr));
            return L2VPN_SUCCESS;
#endif
            pRBTree = L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        default:
            break;
        }
    }

    pPwVcMplsInMappingEntry = RBTreeGet (pRBTree, &PwVcMplsInMappingEntry);

    if (pPwVcMplsInMappingEntry == NULL)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer IPv4 -%#x IPv6 -%s "
                    " has no Mpls In Mapping Entry. "
                    " - To be created\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %#x has no Mpls In Mapping Entry. "
                    " - To be created\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)));
#endif
        pPwVcMplsInMappingEntry =
            (tPwVcMplsInMappingEntry *)
            MemAllocMemBlk (L2VPN_MPLS_IN_MAP_POOL_ID);
        if (pPwVcMplsInMappingEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Mem allocation failure - addition in list\r\n");
            return L2VPN_FAILURE;
        }

        MEMCPY (pPwVcMplsInMappingEntry, &PwVcMplsInMappingEntry,
                sizeof (tPwVcMplsInMappingEntry));
        TMO_DLL_Init (&pPwVcMplsInMappingEntry->PwInList);

        if (RBTreeAdd (pRBTree, pPwVcMplsInMappingEntry) == RB_FAILURE)
        {
            MemReleaseMemBlock (L2VPN_MPLS_IN_MAP_POOL_ID,
                                (UINT1 *) pPwVcMplsInMappingEntry);
            return L2VPN_FAILURE;
        }
    }

    pDll = &(pPwVcMplsInMappingEntry->PwInList);
    pNode = &(pPwVcEntry->PwInNode);

    if (TMO_DLL_Is_Node_In_List (pNode))
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer IPv4 - %x IPv6 - %s is already present in the Inbound TE "
                    "list. - addition skipped\n",
                    pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d for Peer %#x is already present in the Inbound TE "
                    "list. - addition skipped\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif

        /* L2vpnAddInMappingList  is called
         * more than once for the same Node addition. 
         * This will happen if there is a PSN up/down
         * or AC up/down so, ignore the second
         * add and return from here */
        return L2VPN_SUCCESS;
    }
    TMO_DLL_Init_Node (pNode);

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "Inbound TE List empty. PW %d with Peer IPv4 - %x IPv6 - %s added to it.\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2vpnAddInMappingList: PW TE Type, "
                "Inbound tunnel update in list for  "
                "PW %d Peer IPv4 - %x IPv6 - %s \n", pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Inbound TE List empty. PW %d with %#x added to it.\n",
                pPwVcEntry->u4PwVcIndex, OSIX_NTOHL (GenU4Addr.Addr.u4Addr));

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2vpnAddInMappingList: PW TE Type, "
                "Inbound tunnel update in list for  "
                "PW %d peer %#x \n", pPwVcEntry->u4PwVcIndex,
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr));
#endif

    if (u1Type == L2VPN_MPLS_TYPE_TE)
    {
        i4Status = L2VpnRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_IN);

        if (i4Status != L2VPN_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2vpnAddInMappingList: "
                       "Registration with TE Incoming tunnel failed\n");
        }

        if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
            (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE))
        {
            i4Status = L2VpnPostTeEvent (u4TnlIndex, (UINT2) u4TnlInst,
                                         u4LclLsr, u4PeerLsr,
                                         &u4MplsTnlLspId,
                                         L2VPN_ZERO, L2VPN_TE_TNL_STAT_REQ);

            if (i4Status == L2VPN_SUCCESS)
            {
                pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_IN;
                pPwVcEntry->u4LspId = u4MplsTnlLspId;
            }
        }
    }

    TMO_DLL_Add (pDll, pNode);

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                "Number of PWs in the Peer IPv4 - %x IPv6 - %s Inbound TE List = %d. "
                "PW %d insertion done in the list\n",
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr),
                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)), TMO_DLL_Count (pDll),
                pPwVcEntry->u4PwVcIndex);
#else
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "Number of PWs in the peer's %#x Inbound TE List = %d. "
                "PW %d insertion done in the list\n",
                OSIX_NTOHL (GenU4Addr.Addr.u4Addr), TMO_DLL_Count (pDll),
                pPwVcEntry->u4PwVcIndex);
#endif

    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnRegWithNonTeLowerLayer 
 *  Description     : This function used to register the L2VPN with 
 *                    lower layer module
 *  Input           : pPwVcEntry - PwVcEntry to be deleted 
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2VpnRegWithNonTeLowerLayer (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    UINT4               u4OutIfIndex = L2VPN_ZERO;
    UINT4               u4XcIndex = L2VPN_ZERO;
    UINT1               u1Owner = L2VPN_ZERO;
    tGenU4Addr          PeerAddr;

    MEMSET (&PeerAddr, 0, sizeof (tGenU4Addr));

    pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                               L2VPN_PWVC_PSN_ENTRY
                                               (pPwVcEntry));
    if (pMplsTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2vpn Mapping Table Outbound info. not exist \r\n");
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
    }
    PeerAddr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (MplsGetNonTeLspInfoFromPrefix (&PeerAddr,
                                       &u4XcIndex, &u4OutIfIndex,
                                       &u1Owner, MPLS_FALSE) == MPLS_SUCCESS)
    {
        L2VPN_PWVC_MPLS_TNL_XC_INDEX (pMplsTnlEntry) = u4XcIndex;

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnRegWithNonTeLowerLayer: Out List Add, XCIndex=%d "
                    "OutIfIndex %d\r\n", u4XcIndex, u4OutIfIndex);
        pPwVcEntry->u4PwL3Intf = u4OutIfIndex;
        if (u1Owner != MPLS_OWNER_SNMP)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2VpnRegWithNonTeLowerLayer: Signalling NON-TE LSP\n");
            pPwVcEntry->u1IsSigNonTeLsp = L2VPN_TRUE;
            if (LdpRegOrDeRegNonTeLspWithL2Vpn (&PeerAddr,
                                                u4OutIfIndex, TRUE)
                == LDP_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "LdpRegOrDeRegNonTeLspWithL2Vpn failed while "
                           "registering with NON-TE\r\n");
                return L2VPN_FAILURE;
            }
        }
        if (MplsRegOrDeRegNonTeLspWithL2Vpn (&PeerAddr, TRUE) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "MplsRegOrDeRegNonTeLspWithL2Vpn failed while "
                       "registering with NON-TE\r\n");
        }
    }
    else
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2VpnRegWithNonTeLowerLayer: Failed while fetching "
                   "Signalling NON-TE LSP information\n");
    }
    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnDeRegWithNonTeLowerLayer
 *  Description     : This function used to deregister the L2VPN with 
 *                    lower layer module
 *  Input           : pPwVcEntry - PwVcEntry to be deleted 
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2VpnDeRegWithNonTeLowerLayer (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tGenU4Addr          PeerAddr;

    MEMSET (&PeerAddr, 0, sizeof (tGenU4Addr));

    pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                               L2VPN_PWVC_PSN_ENTRY
                                               (pPwVcEntry));
    if (pMplsTnlEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "L2vpn Mapping Table Outbound info. not exist \r\n");
        return L2VPN_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
    }
    PeerAddr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (pPwVcEntry->u1IsSigNonTeLsp == L2VPN_TRUE)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnDeRegWithNonTeLowerLayer: Out List Delete,"
                    "Signalling NON-TE XCIndex=%d OutIfIndex %d\r\n",
                    L2VPN_PWVC_MPLS_TNL_XC_INDEX (pMplsTnlEntry),
                    pPwVcEntry->u4PwL3Intf);
        if (LdpRegOrDeRegNonTeLspWithL2Vpn (&PeerAddr,
                                            pPwVcEntry->u4PwL3Intf,
                                            FALSE) == LDP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Deregistration for the MPLS Tunnel If"
                        " %d for FEC IPv4 - %x IPv6 - %s Failed\n",
                        pPwVcEntry->u4PwL3Intf, PeerAddr.Addr.u4Addr,
                        Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Deregistration for the MPLS Tunnel If"
                        " %d for FEC %x Failed\n",
                        pPwVcEntry->u4PwL3Intf, PeerAddr.Addr.u4Addr);
#endif
            L2VpnDeletePsnTnlIf (pPwVcEntry->u4PwL3Intf,
                                 pPwVcEntry->au1NextHopMac,
                                 L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF);
        }
    }
    else
    {
        if ((pPwVcEntry->u4PwL3Intf != 0) &&
            (MplsGetXCIndexFromPrefix (&PeerAddr) == 0))
        {
            L2VpnDeletePsnTnlIf (pPwVcEntry->u4PwL3Intf,
                                 pPwVcEntry->au1NextHopMac,
                                 L2VPN_PSN_DEL_L3FTN);
        }
    }
    if (MplsRegOrDeRegNonTeLspWithL2Vpn (&PeerAddr, FALSE) == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "MplsRegOrDeRegNonTeLspWithL2Vpn failed while "
                   "deregistering with NON-TE\r\n");
    }

    pPwVcEntry->u1IsSigNonTeLsp = L2VPN_FALSE;
    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnHandleNonTeLowerLayerDown
 *  Description     : This function used to handle lsp down event
 *  Input           : pPwVcEntry - PwVcEntry to be deleted 
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2VpnHandleNonTeLowerLayerDown (tPwVcEntry * pPwVcEntry)
{
    INT4                i4RetStatus = L2VPN_SUCCESS;
    tGenU4Addr          PeerAddr;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
    }
    PeerAddr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    if (pPwVcEntry->u1IsSigNonTeLsp == L2VPN_TRUE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnHandleNonTeLowerLayerDown: Out List Delete,"
                    "Signalling NON-TE OutIfIndex %d\r\n",
                    pPwVcEntry->u4PwL3Intf);
        if (LdpRegOrDeRegNonTeLspWithL2Vpn (&PeerAddr,
                                            pPwVcEntry->u4PwL3Intf,
                                            FALSE) == LDP_FAILURE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Deregistration for the MPLS Tunnel If"
                        " %d for FEC IPv4 - %x IPv6 - %s Failed\n",
                        pPwVcEntry->u4PwL3Intf, PeerAddr.Addr.u4Addr,
                        Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Deregistration for the MPLS Tunnel If"
                        " %d for FEC %x Failed\n",
                        pPwVcEntry->u4PwL3Intf, PeerAddr.Addr.u4Addr);
#endif
            if (pPwVcEntry->u4PwL3Intf != 0)
            {

                if (L2VpnDeletePsnTnlIf (pPwVcEntry->u4PwL3Intf,
                                         pPwVcEntry->au1NextHopMac,
                                         L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF) ==
                    L2VPN_FAILURE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "L2VpnDeletePsnTnlIf failed for MPLS Tunnel If %d \n",
                                pPwVcEntry->u4PwL3Intf);
                    i4RetStatus = L2VPN_FAILURE;
                }
            }
        }
    }
    else
    {
        if (pPwVcEntry->u4PwL3Intf != 0)
        {
            if (L2VpnDeletePsnTnlIf (pPwVcEntry->u4PwL3Intf,
                                     pPwVcEntry->au1NextHopMac,
                                     L2VPN_PSN_DEL_L3FTN) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "L2VpnDeletePsnTnlIf failed for MPLS Tunnel If %d \n",
                            pPwVcEntry->u4PwL3Intf);
                i4RetStatus = L2VPN_FAILURE;
            }
        }
    }
    if (MplsRegOrDeRegNonTeLspWithL2Vpn (&PeerAddr, FALSE) == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "MplsRegOrDeRegNonTeLspWithL2Vpn failed while "
                   "deregistering with NON-TE\r\n");
    }

    pPwVcEntry->u1IsSigNonTeLsp = L2VPN_FALSE;
    return i4RetStatus;
}

/************************************************************************
 *  Function Name   : L2vpnDelMappingList 
 *  Description     : Function to delete a PwVC from an appropriate RBTree
 *  Input           : pPwVcEntry - PwVcEntry to be deleted 
 *                    u1Type - Tunnel Type (Te/NonTe/VcOnly)
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2vpnDelMappingList (tPwVcEntry * pPwVcEntry, UINT1 u1Type)
{
    struct rbtree      *pRBTree = NULL;
    tPwVcMplsMappingEntry *pPwVcMplsMappingEntry = NULL, PwVcMplsMappingEntry;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    INT4                i4Status = L2VPN_SUCCESS;
    UINT4               u4TnlIndex = L2VPN_ZERO;
    UINT2               u2TnlInstance = L2VPN_ZERO;
    UINT4               u4LclLsr = L2VPN_ZERO;
    UINT4               u4PeerLsr = L2VPN_ZERO;
    tGenU4Addr          PeerAddr;

    MEMSET (&PeerAddr, 0, sizeof (tGenU4Addr));

    MEMSET (&PwVcMplsMappingEntry, 0, sizeof (tPwVcMplsMappingEntry));
    pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
    }
    PeerAddr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    switch (u1Type)
    {
        case L2VPN_MPLS_TYPE_TE:
            if (pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.u4TnlIndex == 0)
            {
                return L2VPN_SUCCESS;
            }
            PwVcMplsMappingEntry.unTnlInfo.TeInfo =
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo;

            u4TnlIndex =
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.u4TnlIndex;
            u2TnlInstance =
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.u2TnlInstance;
            CONVERT_TO_INTEGER (pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.
                                TnlLclLSR, u4LclLsr);
            CONVERT_TO_INTEGER (pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.TeInfo.
                                TnlPeerLSR, u4PeerLsr);
            u4LclLsr = OSIX_NTOHL (u4LclLsr);
            u4PeerLsr = OSIX_NTOHL (u4PeerLsr);

            pRBTree = L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        case L2VPN_MPLS_TYPE_NONTE:
            MEMCPY (&PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.PeerOrSrcAddr,
                    (UINT1 *) &pPwVcEntry->PeerAddr, sizeof (uGenAddr));
            PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.u1AddrType =
                (UINT1) pPwVcEntry->i4PeerAddrType;

            pRBTree = L2VPN_PWVC_MPLS_NONTE_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        case L2VPN_MPLS_TYPE_VCONLY:
            PwVcMplsMappingEntry.unTnlInfo.VcInfo.u4IfIndex =
                pPwVcMplsEntry->PwVcMplsOutTnl.unTnlInfo.VcInfo.u4IfIndex;
            pRBTree = L2VPN_PWVC_MPLS_VCONLY_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        default:
            return L2VPN_FAILURE;
    }

    if ((pPwVcMplsMappingEntry =
         RBTreeGet (pRBTree, &PwVcMplsMappingEntry)) == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "No Entry in Mpls Mappling List for the PW\n");
        return L2VPN_SUCCESS;
    }

    pDll = &(pPwVcMplsMappingEntry->PwOutList);
    pNode = &(pPwVcEntry->PwOutNode);

    TMO_DLL_Delete (pDll, pNode);

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "Removed PW %d Peer IPv4 - %x IPv6 - %s from Outbound List\n",
                pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr,
                Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Removed PW %d %x from Outbound List\n",
                pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr);
#endif

    if (TMO_DLL_Count (pDll) == 0)
    {
        /* De-Register with MPLS module to get status change */
        if (u1Type == L2VPN_MPLS_TYPE_TE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2vpnDelMappingList: PW TE Type, "
                        "Outbound tunnel delete in list for  "
                        "PW %d Peer IPv4 - %x IPv6 - %s \n",
                        pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr,
                        Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2vpnDelMappingList: PW TE Type, "
                        "Outbound tunnel delete in list for  "
                        "PW %d peer %#x \n",
                        pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr);
#endif
            i4Status = L2VpnDeRegisterWithMplsModule (pPwVcEntry,
                                                      L2VPN_TNL_OUT);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "DeRegistration with MPLS Module failed\n");
                L2VpnHandlePsnTnlDeletion (u4TnlIndex, u2TnlInstance,
                                           u4LclLsr, u4PeerLsr,
                                           pPwVcEntry->u4PwL3Intf,
                                           pPwVcEntry->au1NextHopMac,
                                           L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF);
            }

            if ((L2VpnGetMplsEntryStatus (pPwVcEntry, L2VPN_PSN_TNL_OUT) !=
                 L2VPN_SUCCESS))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "TE DeRegistration is done, "
                           "check for the tnl oper. status, if down, "
                           "delete the L3FTN from h/w\n");
                L2VpnHandlePsnTnlDeletion (u4TnlIndex, u2TnlInstance,
                                           u4LclLsr, u4PeerLsr,
                                           pPwVcEntry->u4PwL3Intf,
                                           pPwVcEntry->au1NextHopMac,
                                           L2VPN_PSN_DEL_L3FTN);
            }
        }
        else if (u1Type == L2VPN_MPLS_TYPE_NONTE)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2vpnDelMappingList: PW NON-TE Type, "
                        "Outbound tunnel delete in list for  "
                        "PW %d Peer IPv4 - %x IPv6 - %s\n",
                        pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr,
                        Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2vpnDelMappingList: PW NON-TE Type, "
                        "Outbound tunnel delete in list for  "
                        "PW %d peer %#x \n",
                        pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr);
#endif

            i4Status = L2VpnDeRegWithNonTeLowerLayer (pPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2vpnDelMappingList: "
                           "DeRegistration with NON-TE outer tunnel failed\n");
            }
        }

        RBTreeRem (pRBTree, pPwVcMplsMappingEntry);

        if (MemReleaseMemBlock
            (L2VPN_MPLS_MAP_POOL_ID,
             (UINT1 *) (pPwVcMplsMappingEntry)) != MEM_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Mpls Mapping Mem release failure\n");
            return L2VPN_FAILURE;
        }
    }

    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2vpnDelInMappingList 
 *  Description     : Function to delete a PwVC from In Tnl RBTree
 *  Input           : pPwVcEntry - PwVcEntry to be deleted 
 *                    u1Type - Tunnel Type (Te)
 *  Output          : None 
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE 
 ************************************************************************/
INT4
L2vpnDelInMappingList (tPwVcEntry * pPwVcEntry, UINT1 u1Type)
{
    struct rbtree      *pRBTree = NULL;
    tPwVcMplsInMappingEntry *pPwVcMplsInMappingEntry = NULL;
    tPwVcMplsInMappingEntry PwVcMplsInMappingEntry;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tTMO_DLL           *pDll = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    INT4                i4Status = L2VPN_SUCCESS;
    tGenU4Addr          GenU4Prefix;
    tGenU4Addr          PeerAddr;

    MEMSET (&PeerAddr, 0, sizeof (tGenU4Addr));
    MEMSET (&GenU4Prefix, 0, sizeof (tGenU4Addr));

    MEMSET (&PwVcMplsInMappingEntry, 0, sizeof (tPwVcMplsInMappingEntry));
    pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

    if (pPwVcMplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        MEMCPY ((UINT1 *) &PeerAddr.Addr.u4Addr,
                (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
        PeerAddr.Addr.u4Addr = OSIX_NTOHL (PeerAddr.Addr.u4Addr);
    }
    PeerAddr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);
    switch (u1Type)
    {
        case L2VPN_MPLS_TYPE_TE:
        {
            if (pMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex == 0)
            {
                return L2VPN_SUCCESS;
            }
            PwVcMplsInMappingEntry.unInTnlInfo.TeInfo =
                pMplsInTnlEntry->unInTnlInfo.TeInfo;

            pRBTree = L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        }
        case L2VPN_MPLS_TYPE_NONTE:
        {

#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                MEMCPY (GenU4Prefix.Addr.Ip6Addr.u1_addr,
                        pPwVcEntry->GenNonTeTnlSrcAddr.Ip6Addr.u1_addr,
                        IPV6_ADDR_LENGTH);
                MEMCPY (PwVcMplsInMappingEntry.unInTnlInfo.NonTeInfo.
                        PeerOrSrcAddr.Ip6Addr.u1_addr,
                        GenU4Prefix.Addr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
            }
            else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
            {
                GenU4Prefix.Addr.u4Addr = pPwVcEntry->GenNonTeTnlSrcAddr.u4Addr;
                GenU4Prefix.Addr.u4Addr = OSIX_HTONL (GenU4Prefix.Addr.u4Addr);
                MEMCPY (&PwVcMplsInMappingEntry.unInTnlInfo.NonTeInfo.
                        PeerOrSrcAddr.au1Ipv4Addr,
                        (UINT1 *) &GenU4Prefix.Addr.u4Addr, IPV4_ADDR_LENGTH);
            }
            PwVcMplsInMappingEntry.unInTnlInfo.NonTeInfo.u1AddrType =
                (UINT1) pPwVcEntry->i4PeerAddrType;
            pRBTree = L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST (gpPwVcMplsGlobalInfo);
            break;
        }
        default:
        {
            /* vconly case is not supported here. */
            return L2VPN_SUCCESS;
        }
    }

    if ((pPwVcMplsInMappingEntry =
         RBTreeGet (pRBTree, &PwVcMplsInMappingEntry)) == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "No Entry in Mpls Mappling List for the PW\n");
        return L2VPN_SUCCESS;
    }

    pDll = &(pPwVcMplsInMappingEntry->PwInList);
    pNode = &(pPwVcEntry->PwInNode);

    TMO_DLL_Delete (pDll, pNode);

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "Removed PW %d Peer IPv4 - %x IPv6 - %s from Inbound List\n",
                pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr,
                Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Removed PW %d %x from Inbound List\n",
                pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr);
#endif

    if (TMO_DLL_Count (pDll) == 0)
    {
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2vpnDelInMappingList: PW TE Type, "
                    "Inbound tunnel delete in list for  "
                    "PW %d Peer IPv4 - %x IPv6 - %s \n",
                    pPwVcEntry->u4PwVcIndex, PeerAddr.Addr.u4Addr,
                    Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2vpnDelInMappingList: PW TE Type, "
                    "Inbound tunnel delete in list for  "
                    "PW %d peer %#x \n", pPwVcEntry->u4PwVcIndex,
                    PeerAddr.Addr.u4Addr);
#endif

        if (u1Type == L2VPN_MPLS_TYPE_TE)
        {
            i4Status = L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_IN);

            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "DeRegistration with MPLS Module failed\n");
            }
        }

        RBTreeRem (pRBTree, pPwVcMplsInMappingEntry);

        if (MemReleaseMemBlock
            (L2VPN_MPLS_IN_MAP_POOL_ID,
             (UINT1 *) (pPwVcMplsInMappingEntry)) != MEM_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Mpls In Mapping Mem release failure\n");
            return L2VPN_FAILURE;
        }
    }
    return L2VPN_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnTeRbTreeCmpFunc 
 *  Description     : RBTree Compare function for Te PW Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnTeRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcMplsMappingEntry *pEntry1 = (tPwVcMplsMappingEntry *) pRBElem1;
    tPwVcMplsMappingEntry *pEntry2 = (tPwVcMplsMappingEntry *) pRBElem2;
    INT4                i4Return = 0;

    if (pEntry1->unTnlInfo.TeInfo.u4TnlIndex <
        pEntry2->unTnlInfo.TeInfo.u4TnlIndex)
    {
        return -1;
    }
    else if (pEntry1->unTnlInfo.TeInfo.u4TnlIndex >
             pEntry2->unTnlInfo.TeInfo.u4TnlIndex)
    {
        return 1;
    }

    if ((i4Return = MEMCMP (pEntry1->unTnlInfo.TeInfo.TnlLclLSR,
                            pEntry2->unTnlInfo.TeInfo.TnlLclLSR,
                            sizeof (tTeRouterId))) != 0)
    {
        return i4Return;
    }

    if ((i4Return = MEMCMP (pEntry1->unTnlInfo.TeInfo.TnlPeerLSR,
                            pEntry2->unTnlInfo.TeInfo.TnlPeerLSR,
                            sizeof (tTeRouterId))) != 0)
    {
        return i4Return;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnTeInRbTreeCmpFunc 
 *  Description     : RBTree Compare function for Te PW Entries for In Tnl
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnTeInRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcMplsInMappingEntry *pEntry1 = (tPwVcMplsInMappingEntry *) pRBElem1;
    tPwVcMplsInMappingEntry *pEntry2 = (tPwVcMplsInMappingEntry *) pRBElem2;
    INT4                i4Return = 0;

    if (pEntry1->unInTnlInfo.TeInfo.u4TnlIndex <
        pEntry2->unInTnlInfo.TeInfo.u4TnlIndex)
    {
        return -1;
    }
    else if (pEntry1->unInTnlInfo.TeInfo.u4TnlIndex >
             pEntry2->unInTnlInfo.TeInfo.u4TnlIndex)
    {
        return 1;
    }

    if ((i4Return = MEMCMP (pEntry1->unInTnlInfo.TeInfo.TnlLclLSR,
                            pEntry2->unInTnlInfo.TeInfo.TnlLclLSR,
                            sizeof (tTeRouterId))) != 0)
    {
        return i4Return;
    }

    if ((i4Return = MEMCMP (pEntry1->unInTnlInfo.TeInfo.TnlPeerLSR,
                            pEntry2->unInTnlInfo.TeInfo.TnlPeerLSR,
                            sizeof (tTeRouterId))) != 0)
    {
        return i4Return;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnNonTeRbTreeCmpFunc 
 *  Description     : RBTree Compare function for NonTe Ingress PW Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnNonTeRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcMplsMappingEntry *pEntry1 = (tPwVcMplsMappingEntry *) pRBElem1;
    tPwVcMplsMappingEntry *pEntry2 = (tPwVcMplsMappingEntry *) pRBElem2;

#ifdef MPLS_IPV6_WANTED
    if (pEntry1->unTnlInfo.NonTeInfo.u1AddrType <
        pEntry2->unTnlInfo.NonTeInfo.u1AddrType)
    {
        return -1;
    }
    else if (pEntry1->unTnlInfo.NonTeInfo.u1AddrType >
             pEntry2->unTnlInfo.NonTeInfo.u1AddrType)
    {
        return 1;
    }

    if (MPLS_IPV6_ADDR_TYPE == pEntry1->unTnlInfo.NonTeInfo.u1AddrType)
    {
        return (MEMCMP
                (&(pEntry1->unTnlInfo.NonTeInfo.PeerOrSrcAddr.Ip6Addr.u1_addr),
                 &(pEntry2->unTnlInfo.NonTeInfo.PeerOrSrcAddr.Ip6Addr.u1_addr),
                 IPV6_ADDR_LENGTH));
    }
    else
#endif
    {
        return (MEMCMP
                (&(pEntry1->unTnlInfo.NonTeInfo.PeerOrSrcAddr.au1Ipv4Addr),
                 &(pEntry2->unTnlInfo.NonTeInfo.PeerOrSrcAddr.au1Ipv4Addr),
                 IPV4_ADDR_LENGTH));
    }
}

/************************************************************************
 *  Function Name   : L2VpnNonTeInRbTreeCmpFunc 
 *  Description     : RBTree Compare function for NonTe Egress PW Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnNonTeInRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcMplsInMappingEntry *pEntry1 = (tPwVcMplsInMappingEntry *) pRBElem1;
    tPwVcMplsInMappingEntry *pEntry2 = (tPwVcMplsInMappingEntry *) pRBElem2;

#ifdef MPLS_IPV6_WANTED
    if (pEntry1->unInTnlInfo.NonTeInfo.u1AddrType <
        pEntry2->unInTnlInfo.NonTeInfo.u1AddrType)
    {
        return -1;
    }
    else if (pEntry1->unInTnlInfo.NonTeInfo.u1AddrType >
             pEntry2->unInTnlInfo.NonTeInfo.u1AddrType)
    {
        return 1;
    }

    if (MPLS_IPV6_ADDR_TYPE == pEntry1->unInTnlInfo.NonTeInfo.u1AddrType)
    {
        return (MEMCMP
                (&
                 (pEntry1->unInTnlInfo.NonTeInfo.PeerOrSrcAddr.Ip6Addr.u1_addr),
                 &(pEntry2->unInTnlInfo.NonTeInfo.PeerOrSrcAddr.Ip6Addr.
                   u1_addr), IPV6_ADDR_LENGTH));
    }
    else
#endif
    {
        return (MEMCMP
                (&(pEntry1->unInTnlInfo.NonTeInfo.PeerOrSrcAddr.au1Ipv4Addr),
                 &(pEntry2->unInTnlInfo.NonTeInfo.PeerOrSrcAddr.au1Ipv4Addr),
                 IPV4_ADDR_LENGTH));
    }
}

/************************************************************************
 *  Function Name   : L2VpnVcOnlyRbTreeCmpFunc
 *  Description     : RBTree Compare function for VcOnly PW Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnVcOnlyRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcMplsMappingEntry *pEntry1 = (tPwVcMplsMappingEntry *) pRBElem1;
    tPwVcMplsMappingEntry *pEntry2 = (tPwVcMplsMappingEntry *) pRBElem2;

    if (pEntry1->unTnlInfo.VcInfo.u4IfIndex >
        pEntry2->unTnlInfo.VcInfo.u4IfIndex)
    {
        return 1;
    }
    else if (pEntry1->unTnlInfo.VcInfo.u4IfIndex <
             pEntry2->unTnlInfo.VcInfo.u4IfIndex)
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnVplsVfiRbTreeCmpFunc 
 *  Description     : RBTree Compare function for VPLS Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnVplsVfiRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVPLSEntry         *pVplsEntry1 = (tVPLSEntry *) pRBElem1;
    tVPLSEntry         *pVplsEntry2 = (tVPLSEntry *) pRBElem2;
    return (MEMCMP (pVplsEntry1->au1VplsName, pVplsEntry2->au1VplsName,
                    L2VPN_MAX_VPLS_NAME_LEN));
}

#ifdef VPLSADS_WANTED
/************************************************************************
 *  Function Name   : L2VpnVplsRdRbTreeCmpFunc
 *  Description     : RBTree Compare function for VPLS Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnVplsRdRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVPLSRdEntry       *pVplsRdEntry1 = (tVPLSRdEntry *) pRBElem1;
    tVPLSRdEntry       *pVplsRdEntry2 = (tVPLSRdEntry *) pRBElem2;

    if (pVplsRdEntry1->u4VplsInstance > pVplsRdEntry2->u4VplsInstance)
    {
        return 1;
    }

    if (pVplsRdEntry1->u4VplsInstance < pVplsRdEntry2->u4VplsInstance)
    {
        return -1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnVplsRtRbTreeCmpFunc
 *  Description     : RBTree Compare function for VPLS Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnVplsRtRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVPLSRtEntry       *pVplsRtEntry1 = (tVPLSRtEntry *) pRBElem1;
    tVPLSRtEntry       *pVplsRtEntry2 = (tVPLSRtEntry *) pRBElem2;

    if (pVplsRtEntry1->u4VplsInstance > pVplsRtEntry2->u4VplsInstance)
    {
        return 1;
    }

    if (pVplsRtEntry1->u4VplsInstance < pVplsRtEntry2->u4VplsInstance)
    {
        return -1;
    }

    if (pVplsRtEntry1->u4VplsBgpRteTargetIndex >
        pVplsRtEntry2->u4VplsBgpRteTargetIndex)
    {
        return 1;
    }

    if (pVplsRtEntry1->u4VplsBgpRteTargetIndex <
        pVplsRtEntry2->u4VplsBgpRteTargetIndex)
    {
        return -1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnVplsVeRbTreeCmpFunc
 *  Description     : RBTree Compare function for VPLS Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnVplsVeRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVPLSVeEntry       *pVplsVeEntry1 = (tVPLSVeEntry *) pRBElem1;
    tVPLSVeEntry       *pVplsVeEntry2 = (tVPLSVeEntry *) pRBElem2;

    if (pVplsVeEntry1->u4VplsInstance > pVplsVeEntry2->u4VplsInstance)
    {
        return 1;
    }

    if (pVplsVeEntry1->u4VplsInstance < pVplsVeEntry2->u4VplsInstance)
    {
        return -1;
    }

    if (pVplsVeEntry1->u4VplsBgpVEId > pVplsVeEntry2->u4VplsBgpVEId)
    {
        return 1;
    }

    if (pVplsVeEntry1->u4VplsBgpVEId < pVplsVeEntry2->u4VplsBgpVEId)
    {
        return -1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : L2VpnVplsAcRbTreeCmpFunc
 *  Description     : RBTree Compare function for VPLS Entries
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnVplsAcRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVplsAcMapEntry    *pVplsAcEntry1 = (tVplsAcMapEntry *) pRBElem1;
    tVplsAcMapEntry    *pVplsAcEntry2 = (tVplsAcMapEntry *) pRBElem2;

    if (pVplsAcEntry1->u4VplsInstance > pVplsAcEntry2->u4VplsInstance)
    {
        return 1;
    }

    if (pVplsAcEntry1->u4VplsInstance < pVplsAcEntry2->u4VplsInstance)
    {
        return -1;
    }

    if (pVplsAcEntry1->u4AcIndex > pVplsAcEntry2->u4AcIndex)
    {
        return 1;
    }

    if (pVplsAcEntry1->u4AcIndex < pVplsAcEntry2->u4AcIndex)
    {
        return -1;
    }

    return 0;
}
#endif
#ifdef HVPLS_WANTED
/************************************************************************
 *Function Name   : L2VpnVplsBindRbTreeCmpFunc
 *Description     : RBTree Compare function for VPLS Entries
 *Input           : Two RBTree Nodes to be compared
 *Output          : None
 *Returns         : 1/(-1)/0
 ************************************************************************/

PRIVATE INT4
L2VpnVplsBindRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVplsPwBindEntry   *pVplsPwBindEntry1 = (tVplsPwBindEntry *) pRBElem1;
    tVplsPwBindEntry   *pVplsPwBindEntry2 = (tVplsPwBindEntry *) pRBElem2;

    if (pVplsPwBindEntry1->u4VplsInstance > pVplsPwBindEntry2->u4VplsInstance)
    {
        return 1;
    }

    if (pVplsPwBindEntry1->u4VplsInstance < pVplsPwBindEntry2->u4VplsInstance)
    {
        return -1;
    }

    if (pVplsPwBindEntry1->u4PwIndex > pVplsPwBindEntry2->u4PwIndex)
    {
        return 1;
    }

    if (pVplsPwBindEntry1->u4PwIndex < pVplsPwBindEntry2->u4PwIndex)
    {
        return -1;
    }

    return 0;
}
#endif
/************************************************************************
 *  Function Name   : L2VpnMplsPortEntryRbTreeCmpFunc
 *  Description     : RBTree Compare function for Mpls Port Entry
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnMplsPortEntryRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tMplsPortEntryInfo *pMplsPortEntryInfo1 = (tMplsPortEntryInfo *) pRBElem1;
    tMplsPortEntryInfo *pMplsPortEntryInfo2 = (tMplsPortEntryInfo *) pRBElem2;

    if (pMplsPortEntryInfo1->u4IfIndex < pMplsPortEntryInfo2->u4IfIndex)
    {
        return -1;
    }
    else if (pMplsPortEntryInfo1->u4IfIndex > pMplsPortEntryInfo2->u4IfIndex)
    {
        return 1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : L2vpnLock 
 *  Description     : Function used in SNMP context to lock the L2VPN 
 *                    module before accessing L2VPN SNMP APIs 
 *                    (nmh routines)
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE 
 ************************************************************************/
INT4
L2vpnLock (VOID)
{
    if (OsixSemTake (L2VPN_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2vpnUnLock 
 *  Description     : Function used in SNMP context to unlock the L2VPN 
 *                    module after accessing L2VPN SNMP APIs 
 *                    (nmh routines)
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE 
 ************************************************************************/

INT4
L2vpnUnLock (VOID)
{
    if (OsixSemGive (L2VPN_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsHandlePwArpResolveTmrEvent 
 *  Description     : This Function is triggered when the ARP Resolve timer 
 *                    gets out.Scan through all the PwEnties and update the 
 *                    respective status [Resolved/Failed].
 *  Input           : None
 *  Output          : None
 *  Returns         : L2VPN_SUCCESS on Success or else L2VPN_FAILURE
 ************************************************************************/
VOID
MplsHandlePwArpResolveTmrEvent (VOID *pArg)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4PwVcIndex = L2VPN_ZERO;

    UNUSED_PARAM (pArg);

    /* Since this function is called from FM Module, L2VPN_LOCK is taken
     * here */
    MPLS_L2VPN_LOCK ();

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        MPLS_L2VPN_UNLOCK ();
        return;
    }

    for (u4PwVcIndex = 1; u4PwVcIndex <=
         L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwVcIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);
        if (pPwVcEntry == NULL)
        {
            continue;
        }

        if (L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) !=
            MPLS_ARP_RESOLVE_WAITING)
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_ALL_FLAG,
                    "Arp Resolve: HW Programming for PW %d Peer IPv4 %#x IPv6"
                    " -%s called\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
        L2VPN_DBG2 (L2VPN_DBG_ALL_FLAG,
                    "Arp Resolve: HW Programming for PW %d %#x called\n",
                    pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)));
#endif
        if (L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP)
            == L2VPN_SUCCESS)
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED

        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "ARP Resolve: PW %d for Peer Ipv4 - %#x IPv6 - %s "
                    " Oper UP failed "
                    "in HW\n", pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)),
                    Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "ARP Resolve: PW %d for %#x Oper UP failed "
                    "in HW\n", pPwVcEntry->u4PwVcIndex,
                    OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr)));
#endif
    }

    /* L2VPN_LOCK taken is released here. */
    MPLS_L2VPN_UNLOCK ();

    return;
}

/************************************************************************
 *  Function Name   : L2vpnPwEnetMplsDelete 
 *  Description     : This Function is used to clean Enet, Enet Priority 
 *                    mapping and MPLS Entries from Pseudo wire entry 
 *                    before cleaning Pseudo  wire entry
 *  Input           : pPwVcEntry - PW Entry Pointer 
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/
VOID
L2vpnPwEnetMplsDelete (tPwVcEntry * pPwVcEntry)
{
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    UINT4               u4HashIndex = 0;
    tVplsInfo           VplsInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    BOOL1               b1VplsEntryExists = FALSE;

    pPwVcEnetServSpecEntry = (tPwVcEnetServSpecEntry *)
        L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);

    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    if (pPwVcMplsEntry != NULL)
    {
        /* Relinquish PSN from this PW */
        if ((MemReleaseMemBlock
             (L2VPN_MPLS_ENTRY_POOL_ID,
              (UINT1 *) (pPwVcMplsEntry))) != MEM_SUCCESS)
        {

            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "L2vpnPwEnetMplsDelete : "
                       "Enet Priority Mapping Mem Release failed \r\n");

        }
        /* Invalidate PSN reference */
        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) = NULL;
    }

    if (pPwVcEnetServSpecEntry != NULL)
    {
        if (pPwVcEnetServSpecEntry->pMplsPriMappingEntry != NULL)
        {
            /* Relinquish Priority Mapping from this Enet Service Specifics */
            if (MemReleaseMemBlock
                (L2VPN_MPLS_PRIMAP_POOL_ID,
                 (UINT1 *) (pPwVcEnetServSpecEntry->pMplsPriMappingEntry))
                == MEM_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2vpnPwEnetMplsDelete : "
                           "Enet Priority Mapping Mem Release failed \r\n");
            }
            /* Invalidate Priority Mapping reference */
            pPwVcEnetServSpecEntry->pMplsPriMappingEntry = NULL;
        }
        /* Both VPWS and VPLS cases we have a VplsEntry */
        if ((pPwVcEntry->u4VplsInstance != 0) &&
            ((pVplsEntry =
              L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->
                                                  u4VplsInstance)) != NULL))
        {
            b1VplsEntryExists = TRUE;
            /* Register VplsInfo with vlan module when the first PW
             * in this instance is made up. This info will be made
             * use by vlan to give appropriate packets to mpls */
            VplsInfo.u4ContextId = (UINT4) L2VPN_VPLS_VSI (pVplsEntry);
            VplsInfo.u4VplsIndex = L2VPN_VPLS_INDEX (pVplsEntry);

        }
        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
        {
            while ((pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                    (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry))) !=
                   NULL)
            {
                if ((b1VplsEntryExists)
                    && (pPwVcEnetEntry->i1RowStatus == ACTIVE))
                {

                    VplsInfo.u1PwVcMode = VLAN_L2VPN_VPWS;
                    VplsInfo.u4IfIndex =
                        (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
                    VplsInfo.VlanId =
                        L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                    /* Service type of PW is set as port based when 
                     * Ethernet Port is used as AC (Vlan Id value here will be 
                     * 4097. If Vlan Id value is not 4097, service type is set
                     * as Vlan based. */
                    if (VplsInfo.VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                    {
                        VplsInfo.u2MplsServiceType = VLAN_L2VPN_PORT_BASED;
                    }
                    else
                    {
                        VplsInfo.u2MplsServiceType = VLAN_L2VPN_VLAN_BASED;
                    }

                    if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER)
                    {
                        if (VlanL2VpnDelL2VpnInfo (&VplsInfo) == VLAN_FAILURE)
                        {
                            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                       "-E- Can not de-register the VPLS info from vlan\n");
                        }
                    }
                }

                /* Relinquish Enet Entries from EnetService Specific list 1 by 1 */
                TMO_SLL_Delete (L2VPN_PWVC_ENET_ENTRY_LIST
                                (pPwVcEnetServSpecEntry),
                                (tTMO_SLL_NODE *) & (pPwVcEnetEntry->
                                                     NextEnetEntry));

                u4HashIndex = L2VPN_COMPUTE_HASH_INDEX ((UINT4) pPwVcEnetEntry->
                                                        i4PortIfIndex);
                TMO_HASH_Delete_Node (L2VPN_PWVC_INTERFACE_HASH_PTR
                                      (gpPwVcGlobalInfo),
                                      &(pPwVcEnetEntry->EnetNode), u4HashIndex);
                /* Invalidate PW reference */
                pPwVcEnetEntry->pPwVcEntry = NULL;

                /* Relinquish PW Enet Entry */
                if (MemReleaseMemBlock
                    (L2VPN_ENET_POOL_ID,
                     (UINT1 *) (pPwVcEnetEntry)) == MEM_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "L2vpnPwEnetMplsDelete : "
                               "PW wire Enet Mem Release failed\r\n");
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name : L2VpnUpdateOperStatus                           */
/* Description   : This routine updates the PwVc Oper status                 */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*                 u1Event - type of PwVc event (up/down/delete)             */
/*                 u1OperApp - Application (control plane/managment/         */
/*                             oam application) which is notifying           */
/*                             the operational status.                       */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdateOperStatus (tPwVcEntry * pPwVcEntry, UINT1 u1Event, UINT1 u1OperApp)
{
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEntry         *pPwVcAttached = NULL;    /*MS-PW */
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;    /*MS-PW */
    INT4                i4RetStatus = L2VPN_FAILURE;
    UINT4               u4TimeTicks;
    UINT1               u1HwStatus = L2VPN_ZERO;
    BOOL1               bIsOperChged = FALSE;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VPN Oper Status Event %d called for PW %d from %d\r\n",
                u1Event, pPwVcEntry->u4PwVcIndex, u1OperApp);

    /* OAM should notify only operational status up/down */
    if ((u1OperApp == L2VPN_PWVC_OPER_APP_OAM) &&
        ((u1Event != L2VPN_PWVC_UP) && (u1Event != L2VPN_PWVC_DOWN)))
    {
        return L2VPN_FAILURE;
    }
    /* in/out bound operstatus can be only down/testing/lldown/up */
    switch (u1Event)
    {
        case L2VPN_PWVC_DELETE:

            if ((L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL) ||
                ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                  L2VPN_PWVC_STATUS_AC_BITMASK)))
            {
                u1HwStatus |= (UINT1) PW_HW_PW_UPDATE;
            }
            else if ((L2VPN_PWVC_HARDWARE_STATUS (pPwVcEntry) &
                      PW_HW_PW_PRESENT) == L2VPN_ZERO)
            {
                u1HwStatus |= (UINT1) PW_HW_AC_UPDATE;
            }
            else
            {
                u1HwStatus = (UINT1) PW_HW_VPN_UPDATE;
            }

            L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ONE);

            if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_DESTROY)
            {
                u1Event = L2VPN_PWVC_DELETE;
            }
            else
            {
                u1Event = L2VPN_PWVC_DOWN;
            }

            i4RetStatus = L2VpnFmUpdatePwVcStatus (pPwVcEntry, u1Event,
                                                   u1OperApp);

            if (i4RetStatus != L2VPN_SUCCESS)
            {
#ifdef MPLS_IPV6_WANTED

                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "SW Update: PW %d for Peer -IPv4 %#x IPv6 -%s "
                            " delete failed in HW\r\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (*(UINT4 *) (VOID *)
                                        &(pPwVcEntry->PeerAddr)),
                            Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "SW Update: PW %d for %#x delete failed in HW\r\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (*(UINT4 *) (VOID *)
                                        &(pPwVcEntry->PeerAddr)));
#endif
            }

            L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_UNKWN;

            L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN,
                                              u1OperApp, &bIsOperChged);

            pPwVcEntry->u1CPOrMgmtOperStatus = L2VPN_PWVC_OPER_DOWN;

            /* Sending OperStatus to CFA at pseudowire deleation */
            L2vpnExtNotifyPwStatusToApp (pPwVcEntry, u1OperApp);

            break;

        case L2VPN_PWVC_UP:

            if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) ==
                L2VPN_PWVC_RMT_STAT_NOT_CAPABLE)
            {
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d Peer IPv4- %#x IPv6 -%s "
                            " in RMT STAT NOT CAP Mode\r\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (*(UINT4 *) (VOID *)
                                        &(pPwVcEntry->PeerAddr)),
                            Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d %#x in RMT STAT NOT CAP Mode\r\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (*(UINT4 *) (VOID *)
                                        &(pPwVcEntry->PeerAddr)));
#endif
                if ((L2VPN_PWVC_STATUS_IS_UP
                     (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry)))
                    &&
                    (L2VPN_PWVC_STATUS_IS_UP
                     (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry))))
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_ONE);

                    /* MS-PW: Get PWVC Enet entry If AC attached */
                    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
                    {

                        pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                            (L2VPN_PWVC_ENET_ENTRY_LIST
                             ((tPwVcEnetServSpecEntry *)
                              L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
                    }            /* MS-PW */

                    if ((pPwVcEnetEntry != NULL) &&
                        (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                         L2VPN_PWVC_ACTIVE))
                    {
                        i4RetStatus =
                            L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                                     u1OperApp);

                        /* PW Oper Status is made UP in SW only if succeeds 
                         * in HW */
                        if (i4RetStatus != L2VPN_SUCCESS)
                        {
#ifdef MPLS_IPV6_WANTED

                            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "SW Update: RMT STAT NOT CAP: "
                                        "PW %d for Peer IPv4 -%#x IPv6 -%s "
                                        " Oper UP failed in HW\r\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                    &(pPwVcEntry->PeerAddr)),
                                        Ip6PrintAddr (&
                                                      (pPwVcEntry->PeerAddr.
                                                       Ip6Addr)));
#else

                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "SW Update: RMT STAT NOT CAP: "
                                        "PW %d for %#x Oper UP failed in HW\r\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                    &(pPwVcEntry->PeerAddr)));
#endif
                            L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                                L2VPN_PWVC_OPER_DOWN;
                        }
                    }
                    /*MS-PW: check PW-VC attached */
                    else if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry)
                             != L2VPN_ZERO)
                    {

                        /*Get Attached PW VC entry */
                        pPwVcAttached =
                            L2VpnGetPwVcEntryFromIndex
                            (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                        if (pPwVcAttached != NULL)
                        {
                            pPwVcAttached->u1LocalStatus &=
                                (UINT1) (~L2VPN_PWVC_STATUS_AC_BITMASK);

                            i4RetStatus =
                                L2VpnSendLblMapOrNotifMsg (pPwVcAttached);
                            if (i4RetStatus != L2VPN_SUCCESS)
                            {
                                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                           "Attached: label Map or "
                                           "notification sending failed\r\n");
                            }

                        }

                        if (pPwVcAttached != NULL &&
                            (L2VPN_PWVC_STATUS_IS_UP
                             (L2VPN_PWVC_LOCAL_STATUS (pPwVcAttached)))
                            &&
                            (L2VPN_PWVC_STATUS_IS_UP
                             (L2VPN_PWVC_REMOTE_STATUS (pPwVcAttached))))
                        {
                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "Getting MSPW Cross Connect Entry for "
                                        "%d %d\r\n",
                                        L2VPN_PWVC_INDEX (pPwVcEntry),
                                        L2VPN_PWVC_ATTACHED_PW_INDEX
                                        (pPwVcEntry));

                            /* Get MS PW Entry */
                            pL2vpnFsMsPwConfigTable =
                                L2VpnGetMsPwEntry (L2VPN_PWVC_INDEX
                                                   (pPwVcEntry),
                                                   L2VPN_PWVC_ATTACHED_PW_INDEX
                                                   (pPwVcEntry));

                            if (pL2vpnFsMsPwConfigTable == NULL)
                            {
                                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                            "MSPW Cross Connect Entry not "
                                            "found for %d %d\r\n",
                                            L2VPN_PWVC_INDEX (pPwVcEntry),
                                            L2VPN_PWVC_ATTACHED_PW_INDEX
                                            (pPwVcEntry));

                                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                            "Getting MSPW Cross Connect Entry "
                                            "for %d %d\r\n",
                                            L2VPN_PWVC_ATTACHED_PW_INDEX
                                            (pPwVcEntry),
                                            L2VPN_PWVC_INDEX (pPwVcEntry));

                                pL2vpnFsMsPwConfigTable
                                    = L2VpnGetMsPwEntry
                                    (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry),
                                     L2VPN_PWVC_INDEX (pPwVcEntry));
                            }

                            i4RetStatus =
                                L2VpnUpdateMsPwOperStatus
                                (pL2vpnFsMsPwConfigTable, L2VPN_MSPW_UP);

                        }
                    }            /*MS-PW */
                    /*Pseudowire OperStatus should be made up only if HW programming
                     * is done successfully*/
                    if (i4RetStatus == L2VPN_SUCCESS)
                    {
                        L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, u1Event,
                                                          u1OperApp,
                                                          &bIsOperChged);
                        OsixGetSysTime (&u4TimeTicks);
                        L2VPN_PWVC_UP_TIME (pPwVcEntry) = u4TimeTicks;

                        L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_OPER_UP);

                        L2VpnPrintPwVc (pPwVcEntry);

                        if (bIsOperChged == FALSE)
                        {
                            i4RetStatus = L2VPN_SUCCESS;
                            break;
                        }
                    }
                }
                else
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "RMT STAT NOT CAP: "
                                "PW %d for %#x Not in Forwarding Mode\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)),
                                Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "RMT STAT NOT CAP: "
                                "PW %d for %#x Not in Forwarding Mode\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)));
#endif
                }
            }
            else                /* Remote Status Notification capable */
            {
                /*
                 * PW Oper Status is set to UP only if the first bit 
                 * (PW Not Forwarding), the fourth bit (PSN Facing Rx fault) 
                 * and the fifth bit (PSN facing Tx fault) are cleared in
                 * PwLocalStatus and PwRemoteStatus. 
                 */
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d Peer IPv4 -%#x IPv6 -%s "
                            "in Mode other RMT STAT NOT CAP\r\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (*(UINT4 *) (VOID *)
                                        &(pPwVcEntry->PeerAddr)),
                            Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d %#x in Mode other RMT STAT NOT CAP\r\n",
                            pPwVcEntry->u4PwVcIndex,
                            OSIX_NTOHL (*(UINT4 *) (VOID *)
                                        &(pPwVcEntry->PeerAddr)));
#endif
                if ((L2VPN_PWVC_STATUS_IS_UP
                     (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry)))
                    &&
                    (L2VPN_PWVC_STATUS_IS_UP
                     (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry))))
                {
                    /* In case of Tnl/Lsp Up Event, it will be Filled by 
                     * Tnl/Lsp Up Event Handlers.
                     */

                    /* If VPN is already programmed in HW then no action is required
                     * If AC is already programmed in hardware, only
                     * PW_HW_PW_UPDATE should be set as HwAction.
                     * Else PW_HW_VPN_UPDATE should be set as HwAction
                     */

                    if (((pPwVcEntry->u1HwStatus) & (PW_HW_VPN_PRESENT)) ==
                        PW_HW_VPN_PRESENT)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "PW is already Programmed with HWStatus = %x \r\n",
                                    pPwVcEntry->u1HwStatus);

                        i4RetStatus = L2VPN_SUCCESS;
                    }
                    else if (pPwVcEntry->u1HwStatus & PW_HW_AC_PRESENT)
                    {
                        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_PW_UPDATE,
                                              L2VPN_ONE);
                    }
                    else

                    {
                        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                              L2VPN_ONE);
                    }
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PW %d for Peer IPv4 -%#x IPv6 -%s "
                                " in Forwarding state or "
                                "Manual mode\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)),
                                Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PW %d for Peer %#x in Forwarding state or "
                                "Manual mode\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)));
#endif
                    /* MS-PW: Get PWVC Enet entry If AC attached */
                    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
                    {
                        pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                            (L2VPN_PWVC_ENET_ENTRY_LIST
                             ((tPwVcEnetServSpecEntry *)
                              L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
                    }            /* MS-PW */
                    if ((pPwVcEnetEntry != NULL) &&
                        (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                         L2VPN_PWVC_ACTIVE))
                    {
                        i4RetStatus =
                            L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                                     u1OperApp);

                        /* PW Oper Status is made UP in SW only if succeeds 
                         * in HW */
                        if (i4RetStatus != L2VPN_SUCCESS)
                        {
#ifdef MPLS_IPV6_WANTED
                            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "SW Update: RMT STAT CAP: "
                                        "PW %d for %#x Oper UP failed in HW\r\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                    &(pPwVcEntry->PeerAddr)),
                                        Ip6PrintAddr (&
                                                      (pPwVcEntry->PeerAddr.
                                                       Ip6Addr)));
#else
                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "SW Update: RMT STAT CAP: "
                                        "PW %d for %#x Oper UP failed in HW\r\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                    &(pPwVcEntry->PeerAddr)));
#endif

                            L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                                L2VPN_PWVC_OPER_DOWN;
                        }
                    }

                    /*MS-PW: check PW-VC attached */
                    else if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry)
                             != L2VPN_ZERO)
                    {

                        /*Get Attached PW VC entry */
                        pPwVcAttached =
                            L2VpnGetPwVcEntryFromIndex
                            (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                        if (pPwVcAttached != NULL)
                        {
                            L2VPN_PWVC_LOCAL_STATUS (pPwVcAttached) &=
                                (UINT1) (~L2VPN_PWVC_STATUS_AC_BITMASK);

                            /* Forward PW-RED status between segments */
                            if (L2VPN_PWVC_OWNER (pPwVcEntry) ==
                                L2VPN_PWVC_OWNER (pPwVcAttached)
                                && (L2VPN_PWVC_OWNER (pPwVcEntry) ==
                                    L2VPN_PWVC_OWNER_PWID_FEC_SIG
                                    || L2VPN_PWVC_OWNER (pPwVcEntry) ==
                                    L2VPN_PWVC_OWNER_GEN_FEC_SIG)
                                &&
                                L2VPN_RED_STATUS_FORWARD_ENABLE
                                (&gL2vpnMsPwGlobals))
                            {
                                L2VPN_PWVC_LOCAL_STATUS (pPwVcAttached) |=
                                    (UINT1) L2VPN_PWVC_STATUS_RED_STATUS
                                    (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry));
                            }

                            i4RetStatus =
                                L2VpnSendLblMapOrNotifMsg (pPwVcAttached);
                            if (i4RetStatus != L2VPN_SUCCESS)
                            {
                                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                           "Attached: label Map or "
                                           "notification sending failed\r\n");
                            }

                        }

                        if (pPwVcAttached != NULL &&
                            (L2VPN_PWVC_STATUS_IS_UP
                             (L2VPN_PWVC_LOCAL_STATUS (pPwVcAttached)))
                            &&
                            (L2VPN_PWVC_STATUS_IS_UP
                             (L2VPN_PWVC_REMOTE_STATUS (pPwVcAttached))))
                        {
                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "Getting MSPW Cross Connect Entry for "
                                        "%d %d\r\n",
                                        L2VPN_PWVC_INDEX (pPwVcEntry),
                                        L2VPN_PWVC_ATTACHED_PW_INDEX
                                        (pPwVcEntry));

                            /* Get MS PW Entry */
                            pL2vpnFsMsPwConfigTable =
                                L2VpnGetMsPwEntry (L2VPN_PWVC_INDEX
                                                   (pPwVcEntry),
                                                   L2VPN_PWVC_ATTACHED_PW_INDEX
                                                   (pPwVcEntry));

                            if (pL2vpnFsMsPwConfigTable == NULL)
                            {
                                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                            "MSPW Cross Connect Entry not "
                                            "found for %d %d\r\n",
                                            L2VPN_PWVC_INDEX (pPwVcEntry),
                                            L2VPN_PWVC_ATTACHED_PW_INDEX
                                            (pPwVcEntry));

                                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                            "Getting MSPW Cross Connect Entry "
                                            "for %d %d\r\n",
                                            L2VPN_PWVC_ATTACHED_PW_INDEX
                                            (pPwVcEntry),
                                            L2VPN_PWVC_INDEX (pPwVcEntry));

                                pL2vpnFsMsPwConfigTable
                                    = L2VpnGetMsPwEntry
                                    (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry),
                                     L2VPN_PWVC_INDEX (pPwVcEntry));
                            }

                            i4RetStatus =
                                L2VpnUpdateMsPwOperStatus
                                (pL2vpnFsMsPwConfigTable, L2VPN_MSPW_UP);

                        }

                    }            /*MS-PW */
                    else
                    {
                        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                            L2VPN_PWVC_OPER_LLDOWN)
                        {
                            L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                                L2VPN_PWVC_OPER_DOWN;
                        }

#ifdef MPLS_IPV6_WANTED
                        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Enet for PW %d for Peer IPv4-  %#x "
                                    "IPv6 -%s is "
                                    "not present or not in active state\r\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                &(pPwVcEntry->PeerAddr)),
                                    Ip6PrintAddr (&
                                                  (pPwVcEntry->PeerAddr.
                                                   Ip6Addr)));

#else
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Enet for PW %d for Peer %#x is "
                                    "not present or not in active state\r\n",
                                    pPwVcEntry->u4PwVcIndex,
                                    OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                &(pPwVcEntry->PeerAddr)));
#endif
                    }
                    /*PW operStatus is made up only after successful 
                     * HW programming*/
                    if (i4RetStatus == L2VPN_SUCCESS)
                    {
                        L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, u1Event,
                                                          u1OperApp,
                                                          &bIsOperChged);
                        OsixGetSysTime (&u4TimeTicks);
                        L2VPN_PWVC_UP_TIME (pPwVcEntry) = u4TimeTicks;

                        L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_OPER_UP);

                        L2VpnPrintPwVc (pPwVcEntry);

                        if (bIsOperChged == FALSE)
                        {
                            i4RetStatus = L2VPN_SUCCESS;
                            break;
                        }
                    }

                }
                else if (L2VPN_PWVC_IS_ONLY_AC_FAULT (pPwVcEntry) == L2VPN_TRUE)
                {
                    pRgPw =
                        L2VpnGetRedundancyPwEntryByPwIndex (pPwVcEntry->
                                                            u4PwVcIndex);

                    /*Initializing for VPWS pseudowires */
                    i4RetStatus = L2VPN_SUCCESS;

                    if ((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                        || (pRgPw != NULL))
                    {
                        /* If AC is fault, PW_HW_PW_UPDATE should be set as
                         * HwAction
                         * */
                        L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_PW_UPDATE,
                                              L2VPN_ONE);
                        i4RetStatus =
                            L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_UP,
                                                     u1OperApp);

                        /* PW Oper Status is made UP in SW only if succeeds
                         * in HW */
                        if (i4RetStatus != L2VPN_SUCCESS)
                        {
#ifdef MPLS_IPV6_WNATED
                            L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "SW Update: RMT STAT CAP: "
                                        "PW %d for  IPv4 -%#x IPv6 -%s "
                                        "Oper UP failed in HW\r\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                    &(pPwVcEntry->PeerAddr)),
                                        Ip6PrintAddr (&
                                                      (pPwVcEntry->PeerAddr.
                                                       Ip6Addr)));
#else
                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "SW Update: RMT STAT CAP: "
                                        "PW %d for %#x Oper UP failed in HW\r\n",
                                        pPwVcEntry->u4PwVcIndex,
                                        OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                    &(pPwVcEntry->PeerAddr)));
#endif

                            L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                                L2VPN_PWVC_OPER_DOWN;
                        }
                        else
                        {
                            L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry,
                                                              u1Event,
                                                              u1OperApp,
                                                              &bIsOperChged);
                            OsixGetSysTime (&u4TimeTicks);
                            L2VPN_PWVC_UP_TIME (pPwVcEntry) = u4TimeTicks;

                            L2VpnUpdateGlobalStats (pPwVcEntry,
                                                    L2VPN_PWVC_OPER_UP);

                            L2VpnPrintPwVc (pPwVcEntry);
                        }
                    }
                    /*If only AC fault is there, then in VPWS case, PW Oper Status
                     * should be marked as DOWN*/
                    else
                    {
                        L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry,
                                                          L2VPN_PWVC_DOWN,
                                                          u1OperApp,
                                                          &bIsOperChged);

                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_DOWN;
                    }
                }
                else if ((L2VPN_PWVC_STATUS_IS_UP
                          (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry)))
                         &&
                         (!L2VPN_PWVC_STATUS_IS_UP
                          (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry))))
                {

                    /* PSN tunnel is UP before receiving the PSN up from the peer 
                     * sp, change the PWVc status LL_DOWN to DOWN */
                    if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                        L2VPN_PWVC_OPER_LLDOWN)
                    {
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_DOWN;
                    }
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PW %d for Peer IPv4- %#x "
                                " IPv6 -%s Local status in Forwarding Mode and "
                                " Remote status is not forwarding\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)),
                                Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PW %d for %#x Local status in Forwarding Mode and "
                                " Remote status is not forwarding\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)));
#endif
                }
                else
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PW %d for Peer IPv4 - %#x IPv6 -%s "
                                " Not in Forwarding Mode and "
                                " not manual\r\n", pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)),
                                Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PW %d for %#x Not in Forwarding Mode and "
                                " not manual\r\n", pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)));
#endif
                }
            }
            break;

        case L2VPN_PWVC_DOWN:

            if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
            {
                L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, u1Event,
                                                  u1OperApp, &bIsOperChged);
                if (bIsOperChged == FALSE)
                {
                    i4RetStatus = L2VPN_SUCCESS;
                    break;
                }
                /* MS-PW:Update the MS PW oper status */
                if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != L2VPN_ZERO)
                {
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_ONE);
                    pPwVcAttached =
                        L2VpnGetPwVcEntryFromIndex (L2VPN_PWVC_ATTACHED_PW_INDEX
                                                    (pPwVcEntry));

                    if (pPwVcAttached != NULL &&
                        L2VPN_PWVC_OPER_STATUS (pPwVcAttached) ==
                        L2VPN_PWVC_OPER_UP)
                    {
                        pPwVcAttached->u1LocalStatus |=
                            L2VPN_PWVC_STATUS_AC_BITMASK;

                        i4RetStatus =
                            L2VpnSendLblWdrawOrNotifMsg (pPwVcAttached);
                        if (i4RetStatus != L2VPN_SUCCESS)
                        {
                            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                       "Attached: label withdraw or "
                                       "notification sending failed\r\n");
                        }

                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Getting MSPW Cross Connect Entry for "
                                    "%d %d\r\n",
                                    L2VPN_PWVC_INDEX (pPwVcEntry),
                                    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                        /* Get MS PW Entry */
                        pL2vpnFsMsPwConfigTable =
                            L2VpnGetMsPwEntry (L2VPN_PWVC_INDEX (pPwVcEntry),
                                               L2VPN_PWVC_ATTACHED_PW_INDEX
                                               (pPwVcEntry));

                        if (pL2vpnFsMsPwConfigTable == NULL)
                        {
                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "MSPW Cross Connect Entry not found for"
                                        " %d %d\r\n",
                                        L2VPN_PWVC_INDEX (pPwVcEntry),
                                        L2VPN_PWVC_ATTACHED_PW_INDEX
                                        (pPwVcEntry));

                            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                        "Getting MSPW Cross Connect Entry for "
                                        "%d %d\r\n",
                                        L2VPN_PWVC_ATTACHED_PW_INDEX
                                        (pPwVcEntry),
                                        L2VPN_PWVC_INDEX (pPwVcEntry));

                            pL2vpnFsMsPwConfigTable =
                                L2VpnGetMsPwEntry (L2VPN_PWVC_ATTACHED_PW_INDEX
                                                   (pPwVcEntry),
                                                   L2VPN_PWVC_INDEX
                                                   (pPwVcEntry));
                        }

                        i4RetStatus =
                            L2VpnUpdateMsPwOperStatus (pL2vpnFsMsPwConfigTable,
                                                       L2VPN_MSPW_DOWN);

                    }
                    else
                    {
                        i4RetStatus = L2VPN_SUCCESS;
                    }
                    if (L2VpnUtilIsRemoteStatusNotCapable (pPwVcEntry) == TRUE
                        && L2VPN_PWVC_OWNER (pPwVcEntry) !=
                        L2VPN_PWVC_OWNER_OTHER)
                    {
                        L2VpnPwSetInVcLabel (pPwVcEntry,
                                             L2VPN_LDP_INVALID_LABEL);
                    }
                }
                else
                {                /*MS-PW */
                    /* Fault in VC side */
                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_PW_UPDATE,
                                          L2VPN_ONE);
                    i4RetStatus =
                        L2VpnFmUpdatePwVcStatus (pPwVcEntry, L2VPN_PWVC_DOWN,
                                                 u1OperApp);
                }                /*MS-PW */

                if (i4RetStatus != L2VPN_SUCCESS)
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "SW Update: for PW %d Peer IPv4 -%x IPv6 -%s "
                                "Oper DOWN failed "
                                "in HW\r\n", pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)),
                                Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "SW Update: for PW %d %x Oper DOWN failed "
                                "in HW\r\n", pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)));
#endif
                }

                L2VPN_PWVC_UP_TIME (pPwVcEntry) = 0;
                L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_DOWN;
                L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_OPER_DOWN);
                L2VpnPrintPwVc (pPwVcEntry);
                L2vpnExtNotifyPwStatusToApp (pPwVcEntry, u1OperApp);
            }
            else
            {
                i4RetStatus = L2VPN_SUCCESS;
                L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, u1Event,
                                                  u1OperApp, &bIsOperChged);
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                    L2VPN_PWVC_OPER_LLDOWN)
                {
                    L2VPN_PWVC_OPER_STATUS (pPwVcEntry) = L2VPN_PWVC_OPER_DOWN;
                    L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_OPER_DOWN);

                    L2VpnPrintPwVc (pPwVcEntry);
                }
            }
            break;

        case L2VPN_PWVC_PSN_TNL_DOWN:
            if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
            {
                L2VpnExtChkAndUpdatePwOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN,
                                                  u1OperApp, &bIsOperChged);
                if (bIsOperChged == FALSE)
                {
                    i4RetStatus = L2VPN_SUCCESS;
                    break;
                }
                /* Fault in VC side */
                L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_PW_UPDATE, L2VPN_ONE);
                /* MS-PW:Update the MS PW oper status */

                pPwVcAttached =
                    L2VpnGetPwVcEntryFromIndex (L2VPN_PWVC_ATTACHED_PW_INDEX
                                                (pPwVcEntry));

                if ((pPwVcAttached != NULL)
                    && (L2VPN_PWVC_OPER_STATUS (pPwVcAttached) ==
                        L2VPN_PWVC_OPER_UP))
                {
                    pPwVcAttached->u1LocalStatus |=
                        L2VPN_PWVC_STATUS_AC_BITMASK;

                    i4RetStatus = L2VpnSendLblWdrawOrNotifMsg (pPwVcAttached);
                    if (i4RetStatus != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                   "Primary: label withdraw or "
                                   "notification sending failed\r\n");
                    }

                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Getting MSPW Cross Connect Entry for "
                                "%d %d\r\n",
                                L2VPN_PWVC_INDEX (pPwVcEntry),
                                L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                    /* Get MS PW Entry */
                    pL2vpnFsMsPwConfigTable =
                        L2VpnGetMsPwEntry (L2VPN_PWVC_INDEX (pPwVcEntry),
                                           L2VPN_PWVC_ATTACHED_PW_INDEX
                                           (pPwVcEntry));

                    if (pL2vpnFsMsPwConfigTable == NULL)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "MSPW Cross Connect Entry not found for"
                                    " %d %d\r\n",
                                    L2VPN_PWVC_INDEX (pPwVcEntry),
                                    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry));

                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Getting MSPW Cross Connect Entry for "
                                    "%d %d\r\n",
                                    L2VPN_PWVC_ATTACHED_PW_INDEX
                                    (pPwVcEntry),
                                    L2VPN_PWVC_INDEX (pPwVcEntry));

                        pL2vpnFsMsPwConfigTable =
                            L2VpnGetMsPwEntry (L2VPN_PWVC_ATTACHED_PW_INDEX
                                               (pPwVcEntry),
                                               L2VPN_PWVC_INDEX (pPwVcEntry));
                    }

                    i4RetStatus =
                        L2VpnUpdateMsPwOperStatus (pL2vpnFsMsPwConfigTable,
                                                   L2VPN_MSPW_DOWN);

                }
                else
                {                /*MS-PW */
                    i4RetStatus =
                        L2VpnFmUpdatePwVcStatus (pPwVcEntry,
                                                 L2VPN_PWVC_OPER_LLDOWN,
                                                 u1OperApp);
                }                /*MS-PW */
                if (i4RetStatus != L2VPN_SUCCESS)
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "SW Update: LL DOWN: Failed in Hw"
                               /*" PW %d for IPv4 -%#x IPv6 -%s " 
                                  " Oper DOWN failed in HW\r\n",
                                  pPwVcEntry->u4PwVcIndex,
                                  OSIX_NTOHL (*(UINT4 *) (VOID *)
                                  &(pPwVcEntry->PeerAddr)),
                                  Ip6PrintAddr(&(pPwVcEntry->PeerAddr.Ip6Addr)) */
                        );

#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "SW Update: LL DOWN: "
                                " PW %d for %#x Oper DOWN failed in HW\r\n",
                                pPwVcEntry->u4PwVcIndex,
                                OSIX_NTOHL (*(UINT4 *) (VOID *)
                                            &(pPwVcEntry->PeerAddr)));
#endif
                }
                else
                {
                    L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                        L2VPN_PWVC_OPER_LLDOWN;
                    L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_OPER_DOWN);
                    L2VpnPrintPwVc (pPwVcEntry);
                    L2vpnExtNotifyPwStatusToApp (pPwVcEntry,
                                                 L2VPN_PWVC_OPER_APP_CP_OR_MGMT);
                }
            }
            else
            {
                i4RetStatus = L2VPN_SUCCESS;
            }
            break;

        default:
            break;
    }

    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE, L2VPN_TWO);
    return i4RetStatus;
}

/* MS-PW */
/*****************************************************************************/
/* Function Name : L2VpnProcessMsPwAdminEvent                                */
/* Description   : This routine processes MSPW Admin Events                  */
/* Input(s)      : pMsPwAdminEvtInfo - pointer to MSPW Admin event info      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessMsPwAdminEvent (tMsPwAdminEvtInfo * pMsPwAdminEvtInfo)
{
    INT4                i4Status = L2VPN_FAILURE;
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;
    tPwVcEntry         *pPriPwVcEntry = NULL;    /*Primary PW VC */
    tPwVcEntry         *pSecPwVcEntry = NULL;    /*Secondary PW VC */

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (L2vpnFsMsPwConfigTable));

    L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = pMsPwAdminEvtInfo->u4PrimPwVcIndex;
    L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = pMsPwAdminEvtInfo->u4ScndPwVcIndex;

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "Admin Event processed for MSPW Cross Connect Entry %d %d\r\n",
                pMsPwAdminEvtInfo->u4PrimPwVcIndex,
                pMsPwAdminEvtInfo->u4ScndPwVcIndex);

    /*Get MSPW entry */
    pL2vpnFsMsPwConfigTable =
        L2VpnGetMsPwEntry (pMsPwAdminEvtInfo->u4PrimPwVcIndex,
                           pMsPwAdminEvtInfo->u4ScndPwVcIndex);

    if (pL2vpnFsMsPwConfigTable == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "MS PW entry not found\r\n");
        return L2VPN_FAILURE;
    }

    /*Get primay PW VC entry */
    pPriPwVcEntry =
        L2VpnGetPwVcEntryFromIndex (pMsPwAdminEvtInfo->u4PrimPwVcIndex);
    if (pPriPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "pri PW VC entry not found\r\n");
        return L2VPN_FAILURE;
    }

    /*Get Secondary PW VC entry */
    pSecPwVcEntry =
        L2VpnGetPwVcEntryFromIndex (pMsPwAdminEvtInfo->u4ScndPwVcIndex);
    if (pSecPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Sec PW VC entry not found\r\n");
        return L2VPN_FAILURE;
    }

    switch (pMsPwAdminEvtInfo->u4EvtType)
    {
        case L2VPN_MSPW_ACTIVE_EVENT:

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "Admin Create Event processed for MSPW Cross Connect "
                        "Entry %d %d\r\n",
                        pMsPwAdminEvtInfo->u4PrimPwVcIndex,
                        pMsPwAdminEvtInfo->u4ScndPwVcIndex);

            /* MS-PW Status TLV */
            i4Status = L2VpnSendLblMapOrNotifMsg (pPriPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2VpnProcessMsPwAdminEvent : "
                           "primary failed to send Label Map or Notif Msg\n");
            }

            i4Status = L2VpnSendLblMapOrNotifMsg (pSecPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2VpnProcessMsPwAdminEvent : "
                           "Secondary failed to send Label Map or Notif Msg\n");
            }
            if ((!L2VPN_PWVC_STATUS_IS_UP
                 (L2VPN_PWVC_LOCAL_STATUS (pPriPwVcEntry)))
                ||
                (!L2VPN_PWVC_STATUS_IS_UP
                 (L2VPN_PWVC_REMOTE_STATUS (pPriPwVcEntry)))
                ||
                (!L2VPN_PWVC_STATUS_IS_UP
                 (L2VPN_PWVC_LOCAL_STATUS (pSecPwVcEntry)))
                ||
                (!L2VPN_PWVC_STATUS_IS_UP
                 (L2VPN_PWVC_REMOTE_STATUS (pSecPwVcEntry))))
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PriPwOperStatus updation: PwLocalStatus %x "
                            "PwRemoteStatus %x\r\n",
                            pPriPwVcEntry->u1LocalStatus,
                            pPriPwVcEntry->u1RemoteStatus);
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "SecPwOperStatus updation: PwLocalStatus %x "
                            "PwRemoteStatus %x\r\n",
                            pSecPwVcEntry->u1LocalStatus,
                            pSecPwVcEntry->u1RemoteStatus);

                return L2VPN_FAILURE;
            }

            /* Program the MSPW operstatus */
            i4Status = L2VpnUpdateMsPwOperStatus (pL2vpnFsMsPwConfigTable,
                                                  L2VPN_MSPW_UP);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "MSPW oper status set failed\r\n ");

                return L2VPN_FAILURE;
            }
            break;

        case L2VPN_MSPW_DESTROY_EVENT:

            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "Admin Destroy Event processed for MSPW Cross Connect "
                        "Entry %d %d\r\n",
                        pMsPwAdminEvtInfo->u4PrimPwVcIndex,
                        pMsPwAdminEvtInfo->u4ScndPwVcIndex);

            /* Delete MSPW Entry */
            i4Status = L2VpnFmUpdateMsPwStatus (pPriPwVcEntry, pSecPwVcEntry,
                                                L2VPN_MSPW_DELETE);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "MSPW NP programing failed\r\n ");
                return L2VPN_FAILURE;
            }

            /* MS-PW status TLV : set the AC bit mask */
            pPriPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_AC_BITMASK;
            pSecPwVcEntry->u1LocalStatus |= L2VPN_PWVC_STATUS_AC_BITMASK;

            i4Status = L2VpnSendLblWdrawOrNotifMsg (pPriPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2VpnProcessMsPwAdminEvent : "
                           "Primary: label withdraw or "
                           "notification sending failed\r\n");
            }

            i4Status = L2VpnSendLblWdrawOrNotifMsg (pSecPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2VpnProcessMsPwAdminEvent : "
                           "Primary: label withdraw or "
                           "notification sending failed\r\n");
            }

            RBTreeRem (gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable,
                       pL2vpnFsMsPwConfigTable);
            MemReleaseMemBlock (L2VPN_MSPW_POOL_ID,
                                (UINT1 *) pL2vpnFsMsPwConfigTable);

            /*Decrement the No of configured MSPW entries */
            gL2vpnMsPwGlobals.u4NoOfMsPwEntries--;

            /* update the attached PW VC Indexes */
            L2VPN_PWVC_ATTACHED_PW_INDEX (pPriPwVcEntry) = L2VPN_ZERO;
            L2VPN_PWVC_ATTACHED_PW_INDEX (pSecPwVcEntry) = L2VPN_ZERO;

            break;

        default:
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unknown Event\r\n ");
            return L2VPN_FAILURE;
    }
    return (i4Status);
}

/*****************************************************************************/
/* Function Name : L2VpnUpdateMsPwOperStatus                                 */
/* Description   : This routine updates the MSPW Oper status                 */
/* Input(s)      :pL2vpnFsMsPwConfigTable - pointer to MSPW Entry            */
/*               : u1Event - type of MSPW event (up/down/delete)             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnUpdateMsPwOperStatus (tL2vpnFsMsPwConfigTable * pL2vpnFsMsPwConfigTable,
                           UINT1 u1Event)
{
    tPwVcEntry         *pPriPwVcEntry = NULL;    /*Primary PW VC */
    tPwVcEntry         *pSecPwVcEntry = NULL;    /*Secondary PW VC */
    INT4                i4Status = L2VPN_FAILURE;
    INT4                i4PriTnlsStatus = L2VPN_FAILURE;
    INT4                i4SecTnlsStatus = L2VPN_FAILURE;
    UINT4               u4TimeTicks;

    if (pL2vpnFsMsPwConfigTable == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "MS PW entry is NULL\r\n");
        return L2VPN_FAILURE;

    }

    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                "L2VpnUpdateMsPwOperStatus Called for MSPW Cross Connect "
                "Entry %d %d\r\n",
                pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1,
                pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2);

    /*Get primay PW VC entry */
    pPriPwVcEntry =
        L2VpnGetPwVcEntryFromIndex (pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1);
    if (pPriPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "pri PW VC entry not found\r\n");
        return L2VPN_FAILURE;
    }

    /*Get Secondary PW VC entry */
    pSecPwVcEntry =
        L2VpnGetPwVcEntryFromIndex (pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2);
    if (pSecPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Sec PW VC entry not found\r\n");
        return L2VPN_FAILURE;
    }

    switch (u1Event)
    {
        case L2VPN_MSPW_UP:

            /*Get primary PWVC tunnels status */
            i4PriTnlsStatus = L2VpnGetMplsEntryStatus (pPriPwVcEntry,
                                                       L2VPN_PSN_TNL_BOTH);

            /*Get Secondary PWVC tunnels status */
            i4SecTnlsStatus = L2VpnGetMplsEntryStatus (pSecPwVcEntry,
                                                       L2VPN_PSN_TNL_BOTH);

            /*Both PWVC's tunnel status should be UP */
            if ((i4PriTnlsStatus != L2VPN_SUCCESS) ||
                (i4SecTnlsStatus != L2VPN_SUCCESS))
            {
                /*update the oper status */
                pL2vpnFsMsPwConfigTable->i4FsMsPwOperStatus = L2VPN_MSPW_DOWN;
                L2VPN_PWVC_OPER_STATUS (pPriPwVcEntry) = L2VPN_PWVC_OPER_DOWN;
                L2VPN_PWVC_OPER_STATUS (pSecPwVcEntry) = L2VPN_PWVC_OPER_DOWN;

                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "MSPW:Pri tunnel status %d sec tunnel status %d\r\n ",
                            i4PriTnlsStatus, i4SecTnlsStatus);

                return L2VPN_FAILURE;
            }

            i4Status = L2VpnFmUpdateMsPwStatus (pPriPwVcEntry,
                                                pSecPwVcEntry, L2VPN_MSPW_UP);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "MSPW NP programing failed \r\n ");

                return L2VPN_FAILURE;
            }

            /*update the oper status */
            pL2vpnFsMsPwConfigTable->i4FsMsPwOperStatus = L2VPN_MSPW_UP;
            L2VPN_PWVC_OPER_STATUS (pPriPwVcEntry) = L2VPN_PWVC_OPER_UP;
            L2VPN_PWVC_OPER_STATUS (pSecPwVcEntry) = L2VPN_PWVC_OPER_UP;
            OsixGetSysTime (&u4TimeTicks);
            L2VPN_PWVC_UP_TIME (pPriPwVcEntry) = u4TimeTicks;
            L2VPN_PWVC_UP_TIME (pSecPwVcEntry) = u4TimeTicks;
            L2VpnUpdateGlobalStats (pPriPwVcEntry, L2VPN_PWVC_OPER_UP);
            L2VpnPrintPwVc (pPriPwVcEntry);
            L2VpnUpdateGlobalStats (pSecPwVcEntry, L2VPN_PWVC_OPER_UP);
            L2VpnPrintPwVc (pSecPwVcEntry);
            break;

        case L2VPN_MSPW_DOWN:

            if (pL2vpnFsMsPwConfigTable->i4FsMsPwOperStatus == L2VPN_MSPW_UP)
            {
                i4Status = L2VpnFmUpdateMsPwStatus (pPriPwVcEntry,
                                                    pSecPwVcEntry,
                                                    L2VPN_MSPW_DOWN);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "MSPW NP programing failed \r\n ");

                    return L2VPN_FAILURE;
                }

                /*update the oper status */
                pL2vpnFsMsPwConfigTable->i4FsMsPwOperStatus = L2VPN_MSPW_DOWN;
            }
            break;
        default:
            break;

    }

    return L2VPN_SUCCESS;
}

/*MS-PW */
/*****************************************************************************/
/* Function Name : L2vpnRegisterL2vpnMibs                                    */
/* Description   : This function Registers the L2VPN MIBS                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2vpnRegisterL2vpnMibs (VOID)
{
    RegisterSTDPW ();
    RegisterSTDPWM ();
    RegisterSTDPWE ();
    RegisterFSMPNO ();
    RegisterFSMSPW ();            /*MS-PW */
    RegisterFSMPLSL2vpnObj ();
    RegisterFSL2VP ();
#ifdef VPLSADS_WANTED
    RegisterFSVPLS ();
#endif
}

/*****************************************************************************/
/* Function Name : L2vpnProcessTimerEvents                                   */
/* Description   : This function processes timer events                      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
L2vpnProcessTimerEvents ()
{
    tL2vpnTimer        *pL2vpnTimer = NULL;

    pL2vpnTimer = (tL2vpnTimer *) TmrGetNextExpiredTimer (L2VPN_TIMER_LIST_ID);

    while (pL2vpnTimer != NULL)
    {
        switch (pL2vpnTimer->u4Event)
        {
            case L2VPN_CLEANUP_TMR_EXP_EVENT:
                L2VpnProcessPwVcCleanupEvent (&(pL2vpnTimer->AppTimer));
                break;

            case L2VPN_PWVC_PERF_TMR_EXP_EVENT:
                L2VpnFmQueryAllPwVcStats ();
                break;

            case L2VPN_RECOVERY_TMR_EXP_EVENT:
                L2VpnGrProcessRecoveryTmrExpiry (&(pL2vpnTimer->AppTimer));
                break;

            case L2VPN_PEND_MSG_TO_LDP_TMR_EVENT:
                break;
#ifdef VPLS_GR_WANTED
            case L2VPN_BGP_GR_TMR_EXP_EVENT:
                L2VpnBgpDeleteStaleEntries ();
                break;
#endif
#ifdef HVPLS_WANTED
            case L2VPN_NOTIF_RATE_TMR_EXP_EVENT:
                L2VpnProcessNotifRateTmrExpiry ();
                break;
#endif
            default:
                break;
        }
        pL2vpnTimer =
            (tL2vpnTimer *) TmrGetNextExpiredTimer (L2VPN_TIMER_LIST_ID);
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessAcStatusMsg                                   */
/* Description   : This routine process the Ac status message received from  */
/*                           D-LAG                                           */
/* Input(s)      : pL2vpnPwRedAcStatusInfo - pointer to Ac status info Entry */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessAcStatusMsg (tL2VpnPwRedAcStatusInfo * pL2VpnPwRedAcStatusInfo)
{
    UINT1               u1DlagPorts = 0;
    UINT4               u4PortIndex = 0;
    UINT2               u2PortVlan = 0;

    for (; u1DlagPorts < pL2VpnPwRedAcStatusInfo->u1DlagPortCnt; u1DlagPorts++)
    {
        u4PortIndex = pL2VpnPwRedAcStatusInfo->au2DlagPort[u1DlagPorts];
        L2IwfGetVlanPortPvid (u4PortIndex, &u2PortVlan);
        if (pL2VpnPwRedAcStatusInfo->u1AcStatus == LA_AC_OPER_UP)
        {
            L2VpPwRedProcessAcUpEvent (u4PortIndex, u2PortVlan);
            u4PortIndex = 0;
            L2VpPwRedProcessAcUpEvent (u4PortIndex, u2PortVlan);
        }
        else if (pL2VpnPwRedAcStatusInfo->u1AcStatus == LA_AC_OPER_DOWN)
        {
            L2VpPwRedProcessAcDownEvent (u4PortIndex, u2PortVlan);
            u4PortIndex = 0;
            L2VpPwRedProcessAcDownEvent (u4PortIndex, u2PortVlan);
        }
    }
    return L2VPN_SUCCESS;
}

#ifdef VPLSADS_WANTED
/*****************************************************************************/
/* Function Name : L2VpnProcessBgpEvent                                      */
/* Description   : This routine process the message received from BGP        */
/* Input(s)      : pL2vpnPwRedAcStatusInfo - pointer to Ac status info Entry */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo)
{
    switch (L2VPN_BGP_VPLS_MSG_TYPE (pL2VpnBgpEvtInfo))
    {
        case L2VPN_BGP_ADVERTISE_MSG:
            L2VpnProcessBgpAdvEvent (pL2VpnBgpEvtInfo);
            break;
        case L2VPN_BGP_WITHDRAW_MSG:
            L2VpnProcessBgpWithdrawEvent (pL2VpnBgpEvtInfo);
            break;
        case L2VPN_BGP_ADMIN_UP:
            MplsSetBgpAdminState (L2VPN_BGP_ADMIN_UP);
            L2VpnProcessBgpUpEvent ();
            break;
        case L2VPN_BGP_ADMIN_DOWN:
            MplsSetBgpAdminState (L2VPN_BGP_ADMIN_DOWN);
            L2VpnProcessBgpDownEvent ();
            break;
        case L2VPN_BGP_MODULE_DOWN:
            MplsSetBgpAdminState (L2VPN_BGP_ADMIN_DOWN);
            L2VpnProcessBgpDownEvent ();
            break;
#ifdef VPLS_GR_WANTED
        case L2VPN_BGP_GR_START:
            MplsSetBgpAdminState (L2VPN_BGP_ADMIN_DOWN);
            L2VpnProcessBgpGREvent ();
            break;
        case L2VPN_BGP_GR_IN_PROGRESS:
            MplsSetBgpAdminState (L2VPN_BGP_ADMIN_UP);
            L2VpnProcessBgpGRInProgressEvent (pL2VpnBgpEvtInfo);
            break;
#endif
        default:
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "Unknown messageType - %d\r\n",
                        L2VPN_BGP_VPLS_MSG_TYPE (pL2VpnBgpEvtInfo));
            break;
    }
}

/******************************************************************************
 * Function Name : L2VpnVplsPwDestroy
 * Description   : This routine will destroy the pw Entry. 
 * Input(s)      : u4PwIndex             - Index for the row identifying a PW 
 *                                         in the pwTable 
 * Output(s)     : If Success -L2VPN_SUCCESS
 *                 or else Proper Error Mesage is printed.
 * Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE
 *******************************************************************************/
INT4
L2VpnVplsPwDestroy (UINT4 u4PwIndex)
{

    if (L2VpnSetPwRowStatus (u4PwIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "\r%%Unable to destroy the pw Entry\r\n");
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnVplsPwTablePopulate
 * Description   : This routine will populate the entries in pwTable. 
 * Input(s)      : u4VplsIndex           - Vpls config index
 *                 u4PwIndex             - PW Index
 *                 u4PeerAddr            - IP address of peer node
 *                 u4LocalLabel          - local label of PW
 *                 u4RemoteLabel         - remote label of PW
 *                 bControlWordFlag      - control word flag to be set
 * Output(s)     : NONE 
 * Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE
 *******************************************************************************/

INT4
L2VpnVplsPwTablePopulate (UINT4 u4VplsIndex,
                          UINT4 u4PwIndex,
                          tGenU4Addr GenU4Addr,
                          UINT4 u4LocalLabel,
                          UINT4 u4RemoteLabel,
                          UINT4 u4VplsMtu,
                          BOOL1 bControlWordFlag,
                          UINT4 u4VplsLocalVeId, UINT4 u4VplsRemoteVeId)
{

    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcInfo          *pPwVcInfo = NULL;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE LocalCapabAdvert;
    tSNMP_OCTET_STRING_TYPE AIIFormat;
    tSNMP_OCTET_STRING_TYPE AGIValue;
    tSNMP_OCTET_STRING_TYPE SAIIValue;
    tSNMP_OCTET_STRING_TYPE TAIIValue;
    UINT1               au1AiiFormat[MPLS_ONE] = { 0 };
    UINT1               u1LocalCapabAdvert = 0;
    UINT1               u1AGIValue = 0;
    UINT1               u1SAIIValue = 0;
    UINT1               u1TAIIValue = 0;
    UINT1               u1AiiFormat = 0;
    UINT1               au1PeerAddr[IPV6_ADDR_LENGTH];
    u1AGIValue = (UINT1) u4VplsIndex;

    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerAddr.i4_Length = 0;

    AIIFormat.pu1_OctetList = au1AiiFormat;
    AIIFormat.i4_Length = 0;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
    {
        MEMCPY (PeerAddr.pu1_OctetList,
                GenU4Addr.Addr.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);
        PeerAddr.i4_Length = IPV6_ADDR_LENGTH;
    }
    else if (MPLS_IPV4_ADDR_TYPE == GenU4Addr.u2AddrType)
#endif
    {
        MPLS_INTEGER_TO_OCTETSTRING (GenU4Addr.Addr.u4Addr, (&PeerAddr));
        PeerAddr.i4_Length = IPV4_ADDR_LENGTH;
    }

    MEMCPY (AIIFormat.pu1_OctetList, &u1AiiFormat, sizeof (UINT1));
    AIIFormat.i4_Length = sizeof (UINT1);

    /* Set Pw Row Status */
    if (u4PwIndex > L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo) || u4PwIndex < 1)
    {
        MplsL2VpnRelPwVcIndex (u4PwIndex);
        return L2VPN_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        MplsL2VpnRelPwVcIndex (u4PwIndex);
        return L2VPN_FAILURE;
    }

    pPwVcEntry = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntry == NULL)
    {
        MplsL2VpnRelPwVcIndex (u4PwIndex);
        return L2VPN_FAILURE;
    }

    MEMSET (pPwVcEntry, L2VPN_ZERO, sizeof (tPwVcEntry));
    L2vpnInitPwVcEntry (u4PwIndex, pPwVcEntry);

    pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
    pPwVcInfo->pPwVcEntry = pPwVcEntry;
    /* mark row status as not-ready */
    L2VPN_PWVC_ROW_STATUS (pPwVcEntry) = L2VPN_PWVC_NOTREADY;
    L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry) = (UINT2) u4VplsMtu;
    L2VPN_PWVC_REMOTE_IFACE_MTU (pPwVcEntry) = (UINT2) u4VplsMtu;
    L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) = MPLS_SNMP_FALSE;
    L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_CREATE);

    /* Set Pw Type */

    L2VPN_PWVC_TYPE (pPwVcEntry) = (INT1) L2VPN_PWVC_TYPE_ETH_VLAN;

    /*    Set Pw Owner */

    L2VPN_PWVC_OWNER (pPwVcEntry) = (INT1) L2VPN_PWVC_OWNER_OTHER;

    /* Set Pw Psn Type */

    pPwVcEntry->i1PwVcPsnType = (INT1) L2VPN_PWVC_PSN_TYPE_MPLS;

    pPwVcEntry->i4PeerAddrType = GenU4Addr.u2AddrType;

    CPY_FROM_SNMP (&L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                   &PeerAddr, PeerAddr.i4_Length);
    LocalCapabAdvert.pu1_OctetList = &u1LocalCapabAdvert;
    LocalCapabAdvert.i4_Length = sizeof (u1LocalCapabAdvert);

    L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) = LocalCapabAdvert.pu1_OctetList[0];

    L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) = (INT1) bControlWordFlag;
    L2VPN_PWVC_LOCAL_VE_ID (pPwVcEntry) = u4VplsLocalVeId;
    L2VPN_PWVC_REMOTE_VE_ID (pPwVcEntry) = u4VplsRemoteVeId;

    L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry) = L2VPN_ZERO;

    L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry) = L2VPN_ZERO;

    L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry) = L2VPN_ZERO;

    pPwVcEntry->u1AiiFormat = AIIFormat.pu1_OctetList[0];

    AGIValue.pu1_OctetList = &u1AGIValue;
    AGIValue.i4_Length = sizeof (u1LocalCapabAdvert);

    CPY_FROM_SNMP (L2VPN_PWVC_AGI (pPwVcEntry), &AGIValue, AGIValue.i4_Length);
    L2VPN_PWVC_AGI_LEN (pPwVcEntry) = (UINT1) AGIValue.i4_Length;

    SAIIValue.pu1_OctetList = &u1SAIIValue;
    SAIIValue.i4_Length = sizeof (u1LocalCapabAdvert);

    CPY_FROM_SNMP (L2VPN_PWVC_SAII (pPwVcEntry),
                   &SAIIValue, SAIIValue.i4_Length);
    L2VPN_PWVC_SAII_LEN (pPwVcEntry) = (UINT1) SAIIValue.i4_Length;

    TAIIValue.pu1_OctetList = &u1TAIIValue;
    TAIIValue.i4_Length = sizeof (u1LocalCapabAdvert);

    CPY_FROM_SNMP (L2VPN_PWVC_TAII (pPwVcEntry),
                   &TAIIValue, TAIIValue.i4_Length);
    L2VPN_PWVC_TAII_LEN (pPwVcEntry) = (UINT1) TAIIValue.i4_Length;

    if (u4LocalLabel != 0)
    {
        L2VpnPwSetInVcLabel (pPwVcEntry, u4LocalLabel);
    }

    if (u4RemoteLabel != 0)
    {
        L2VpnPwSetOutVcLabel (pPwVcEntry, u4RemoteLabel);

    }

    pPwVcEntry->bIsStaticPw = TRUE;
    /* Set PwMode */
#ifdef VPLS_GR_WANTED
    L2VPN_PWVC_NPAPI (pPwVcEntry) = L2VPN_TRUE;
#endif
    L2VPN_PWVC_MODE (pPwVcEntry) = (UINT1) L2VPN_VPLS;

    L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = u4VplsIndex;

    return L2VPN_SUCCESS;
}

/******************************************************************************
 * Function Name : L2VpnVplsNeighborCreate
 * Description   : This routine will a create a neighbor by establishing a 
 *                 autodiscovered PW between the two endpoints.
 * Input(s)      : u4VplsIndex           - Vpls config index
 *                 u4LocalLabel          - local label of PW
 *                 u4RemoteLabel         - remote label of PW
 *                 u4PeerAddr            - IP address of peer node
 *                 u4VplsLocalVeId       - VE Id of local node
 *                 u4VplsRemoteVeId      - VE Id of remote node
 *                 bControlWordFlag      - control word flag to be set
 * Output(s)     : None
 * Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE
 *******************************************************************************/
INT4
L2VpnVplsNeighborCreate (UINT4 u4VplsIndex,
                         UINT4 u4LocalLabel,
                         UINT4 u4RemoteLabel,
                         tGenU4Addr GenU4Addr,
                         UINT4 u4VplsLocalVeId,
                         UINT4 u4VplsRemoteVeId,
                         UINT4 u4VplsMtu, BOOL1 bControlWordFlag)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    UINT4               u4PwIndex = 0;
    UINT4               u4Instance = 0;
    UINT4               u4NextVplsIndex = 0;
    UINT4               u4AcIndex = 0;
    UINT4               u4NextAcIndex = 0;
    UINT4               u4AcIfIndex = 0;
    UINT4               u4AcVlan = 0;
    INT4                i4AcRowStatus = 0;
    BOOL1               bFlag = FALSE;
    BOOL1               bIsPwAlreadyExists = FALSE;
#ifdef VPLS_GR_WANTED
    tL2VpnPwHwList      L2VpnPwHwListEntry;

    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));
#endif
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if ((pVplsEntry == NULL) || (L2VPN_VPLS_ROW_STATUS (pVplsEntry) != ACTIVE))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "No Vpls Entry to be associated with this VPN \n");
        return L2VPN_FAILURE;
    }
    else
    {
        /* Scan through the PWs associated to the VPLS Instance and 
         * check whether a PW already exists for this Destination */
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
            {
                if (L2VPN_ZERO ==
                    MEMCMP (GenU4Addr.Addr.Ip6Addr.u1_addr,
                            &L2VPN_PWVC_PEER_ADDR (pPwEntry), IPV6_ADDR_LENGTH))
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "A PW is already associated with this Peer"
                               " in this Vpls Instance \n");
                    bIsPwAlreadyExists = TRUE;
                    break;
                }
            }
            else if (MPLS_IPV4_ADDR_TYPE == GenU4Addr.u2AddrType)
#endif
            {

                if (GenU4Addr.Addr.u4Addr ==
                    OSIX_NTOHL (*
                                ((UINT4 *) (VOID *)
                                 &L2VPN_PWVC_PEER_ADDR (pPwEntry))))
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "A PW is already associated with this Peer"
                               " in this Vpls Instance \n");
                    bIsPwAlreadyExists = TRUE;
                    break;
                }
            }
        }
    }

    if (FALSE == bIsPwAlreadyExists)
    {
        /* Getting a  Free Index from the Index Manager */
#ifdef VPLS_GR_WANTED
        L2VpnPwHwListEntry.u4VplsIndex = u4VplsIndex;
        L2VpnPwHwListEntry.u4PwIndex = L2VPN_ZERO;
        while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
               == L2VPN_SUCCESS)
        {
            L2VpnPwHwListEntry.u4PwIndex =
                L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry);
            if (u4VplsIndex !=
                L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry))
            {
                break;
            }
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
            {
                if ((u4VplsRemoteVeId ==
                     L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
                    && (MEMCMP (GenU4Addr.Addr.Ip6Addr.u1_addr,
                                L2VPN_PW_HW_LIST_PEER_IPV6_ADDRESS
                                (&L2VpnPwHwListEntry), IPV6_ADDR_LENGTH) == 0))
                {
                    u4PwIndex = L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry);
                    break;
                }
            }
            else
#endif
            {
                if ((u4VplsRemoteVeId ==
                     L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
                    && (MEMCMP (&(GenU4Addr.Addr.u4Addr),
                                L2VPN_PW_HW_LIST_PEER_IP_ADDRESS
                                (&L2VpnPwHwListEntry), IPV4_ADDR_LENGTH) == 0))
                {
                    u4PwIndex = L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry);
                    break;
                }
            }
        }
#endif
        if (u4PwIndex == L2VPN_ZERO)
        {
            nmhGetPwIndexNext (&u4PwIndex);
            if (u4PwIndex == L2VPN_ZERO)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "PW Index is unavailable.\n");
                return L2VPN_FAILURE;
            }
            if (MplsL2VpnSetPwVcIndex (u4PwIndex) != u4PwIndex)
            {
                return L2VPN_FAILURE;
            }

        }
        /* Create pw and populate pw table with the pw mode as L2VPN_VPLS */
        if (L2VpnVplsPwTablePopulate (u4VplsIndex,
                                      u4PwIndex,
                                      GenU4Addr,
                                      u4LocalLabel,
                                      u4RemoteLabel,
                                      u4VplsMtu,
                                      bControlWordFlag,
                                      u4VplsLocalVeId,
                                      u4VplsRemoteVeId) == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }

        if (L2VpnSetPwRowStatus (u4PwIndex, ACTIVE) == SNMP_FAILURE)
        {
            L2VpnVplsPwDestroy (u4PwIndex);
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Failed while making Up PW Entry\n");
            return L2VPN_FAILURE;
        }

        /* Add this Pseudo wire to all the PwEnet Entries present in the same 
         * VPLS Entry */

        while (SNMP_SUCCESS == nmhGetNextIndexFsMplsVplsAcMapTable (u4VplsIndex,
                                                                    &u4NextVplsIndex,
                                                                    u4AcIndex,
                                                                    &u4NextAcIndex))
        {
            u4AcIndex = u4NextAcIndex;
            if (u4VplsIndex != u4NextVplsIndex)
            {
                break;
            }

            (VOID) nmhGetFsMplsVplsAcMapRowStatus (u4VplsIndex,
                                                   u4AcIndex, &i4AcRowStatus);

            if (ACTIVE != i4AcRowStatus)
            {
                continue;
            }

            bFlag = TRUE;

            (VOID) nmhGetFsMplsVplsAcMapPortVlan (u4VplsIndex,
                                                  u4AcIndex, &u4AcVlan);

            (VOID) nmhGetFsMplsVplsAcMapPortIfIndex (u4VplsIndex,
                                                     u4AcIndex, &u4AcIfIndex);

            (VOID) nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (u4PwIndex,
                                                              &u4Instance);

            if (PwCreateDynamicEnetEntry (u4PwIndex,
                                          u4Instance,
                                          u4AcVlan,
                                          u4AcVlan,
                                          u4AcIfIndex, 2) == L2VPN_FAILURE)
            {
                bFlag = FALSE;
                break;
            }
        }

        if (!bFlag)
        {
            L2VpnVplsPwDestroy (u4PwIndex);
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Error while attaching this entry to PWs"
                       "associated to this Instance\n");
            return L2VPN_FAILURE;
        }
        /* Set Pw Mpls Mpls Type */
    }
    else
    {
        if (u4LocalLabel != 0)
        {
            L2VpnPwSetInVcLabel (pPwEntry, u4LocalLabel);
        }

        if (u4RemoteLabel != 0)
        {
            L2VpnPwSetOutVcLabel (pPwEntry, u4RemoteLabel);

        }

        u4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (NULL == pPwVcEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Pw Vc Entry not found", __func__);
        return L2VPN_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL)
    {
        L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                   L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)) =
            L2VPN_MPLS_TYPE_NONTE;

        pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                       L2VPN_PWVC_PSN_ENTRY
                                                       (pPwVcEntry));
        L2VPN_PWVC_MPLS_TNL_TYPE (pPwVcMplsTnlEntry) =
            L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                       L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    }
    if (L2VPN_INVALID_LABEL != L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry) &&
        L2VPN_INVALID_LABEL != L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry))
    {
        if (L2VpnSetPwAdminStatus (u4PwIndex, L2VPN_PWVC_ADMIN_UP)
            == L2VPN_FAILURE)
        {
            L2VpnVplsPwDestroy (u4PwIndex);
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Failed while making the AdminStatus to UP\n");
            return L2VPN_FAILURE;
        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessVplsAdminEvent                                */
/* Description   : This routine processes VPLS Admin Events                  */
/* Input(s)      : pVplsAdminEvtInfo - pointer to VPLS Admin event info      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessVplsAdminEvent (tL2vpnVplsAdminEvtInfo * pVplsAdminEvtInfo)
{
    UINT4               u4EvtType = L2VPN_ZERO;
    INT4                i4Status = L2VPN_SUCCESS;

    u4EvtType = pVplsAdminEvtInfo->u4EvtType;

    switch (u4EvtType)
    {
        case L2VPN_VPLS_UP_EVENT:
            i4Status = L2VpnProcessVplsUpEvent (pVplsAdminEvtInfo->u4VplsIndex);
            break;

        case L2VPN_VPLS_DOWN_EVENT:
            i4Status =
                L2VpnProcessVplsDownEvent (pVplsAdminEvtInfo->u4VplsIndex,
                                           pVplsAdminEvtInfo->u4Mtu,
                                           pVplsAdminEvtInfo->u4VeId,
                                           (tVPLSLBInfo *) pVplsAdminEvtInfo->
                                           pVplsLBInfoBase,
                                           pVplsAdminEvtInfo->
                                           au1RouteDistinguisher,
                                           pVplsAdminEvtInfo->au1RouteTarget,
                                           pVplsAdminEvtInfo->bControlWordFlag);
            break;

        case L2VPN_VPLS_DELETE_EVENT:
            i4Status =
                L2VpnProcessVplsDeleteEvent (pVplsAdminEvtInfo->u4VplsIndex);
            break;

        case L2VPN_VPLS_ROW_DELETE_EVENT:
            i4Status = L2vpnDeleteVplsEntry (pVplsAdminEvtInfo->u4VplsIndex);
            break;

        case L2VPN_VPLS_ASSOC_DELETE_EVENT:
            i4Status = L2VpnVplsAssocDelete (pVplsAdminEvtInfo->u4VplsIndex);
            break;

        case L2VPN_VPLS_RT_ADD_EVENT:
            i4Status = L2VpnProcessRtAddEvent (pVplsAdminEvtInfo->u4VplsIndex,
                                               pVplsAdminEvtInfo->
                                               au1RouteTarget,
                                               pVplsAdminEvtInfo->u1RTType);
            break;

        case L2VPN_VPLS_RT_DELETE_EVENT:
            i4Status =
                L2VpnProcessRtDeleteEvent (pVplsAdminEvtInfo->u4VplsIndex,
                                           pVplsAdminEvtInfo->au1RouteTarget,
                                           pVplsAdminEvtInfo->u1RTType);
            break;

        default:
            break;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessVplsUpEvent                                   */
/* Description   : This routine processes VPLS UP Events                     */
/* Input(s)      : u4VplsIndex - VPLS index                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessVplsUpEvent (UINT4 u4VplsIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VBO = L2VPN_ZERO;
    INT4                i4Status = L2VPN_FAILURE;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        return i4Status;
    }

    L2VPN_VPLS_LABEL_BLOCK (pVplsEntry) =
        (tVPLSLBInfo *) MemAllocMemBlk (L2VPN_VPLS_LB_POOL_ID);
    if (NULL == L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s :Memory Allocate Failed\n", __func__);
        return L2VPN_FAILURE;
    }

    L2VpnSendBgpVplsUpEvent (u4VplsIndex);

    for (u4VBO = L2VPN_ZERO; u4VBO < L2VPN_VPLS_MAX_LB; u4VBO++)
    {
        L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[u4VBO].u4LabelBlock =
            L2VPN_INVALID_LABEL;
    }

    i4Status = L2VpnSendBgpAdvEvent (u4VplsIndex, L2VPN_ZERO);

    if (L2VPN_SUCCESS == i4Status)
    {
        L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_UP;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessVplsDownEvent                                 */
/* Description   : This routine processes VPLS DOWN Events                   */
/* Input(s)      : u4VplsIndex - VPLS index                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessVplsDownEvent (UINT4 u4VplsIndex,
                           UINT4 u4Mtu,
                           UINT4 u4VeId,
                           tVPLSLBInfo * pVPLSLBInfoBase,
                           UINT1 *pu1RouteDistinguisher,
                           UINT1 *pu1RouteTarget, BOOL1 bControlWordFlag)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    INT4                i4PwOwner = L2VPN_ZERO;
    UINT4               u4VBO = L2VPN_ZERO;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        return L2VPN_FAILURE;
    }

    /* Scan through the PWs associated to the VPLS Instance and delete them */

    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

        if (pPwEntry != NULL)
        {
            nmhGetPwOwner (L2VPN_PWVC_INDEX (pPwEntry), &i4PwOwner);
            if (L2VPN_PWVC_OWNER_OTHER != i4PwOwner)
            {
                continue;
            }
            L2VpnVplsPwDestroy (L2VPN_PWVC_INDEX (pPwEntry));
        }
        pPwVplsNode = NULL;
    }

    for (u4VBO = 0; u4VBO < L2VPN_VPLS_MAX_LB; u4VBO++)
    {
        if (L2VPN_INVALID_LABEL != pVPLSLBInfoBase[u4VBO].u4LabelBlock)
        {
            i4Status = L2VpnSendBgpWithdrawEvent (u4VplsIndex,
                                                  u4VeId,
                                                  u4Mtu,
                                                  u4VBO,
                                                  pVPLSLBInfoBase[u4VBO].
                                                  u4LabelBlock,
                                                  pu1RouteDistinguisher,
                                                  pu1RouteTarget,
                                                  bControlWordFlag);

            i4Status = L2VpnReleaseLb (pVPLSLBInfoBase[u4VBO].u4LabelBlock);
        }
        pPwVplsNode = NULL;
    }

    L2VpnSendBgpVplsDownEvent (u4VplsIndex);

    MEMCPY (L2VPN_VPLS_PREVIOUS_DEFAULT_RD (pVplsEntry),
            L2VPN_VPLS_DEFAULT_RD (pVplsEntry), L2VPN_MAX_VPLS_RD_LEN);

    MEMCPY (L2VPN_VPLS_PREVIOUS_DEFAULT_RT (pVplsEntry),
            L2VPN_VPLS_DEFAULT_RT (pVplsEntry), L2VPN_MAX_VPLS_RD_LEN);

    MEMSET (L2VPN_VPLS_DEFAULT_RD (pVplsEntry),
            L2VPN_ZERO, L2VPN_MAX_VPLS_RD_LEN);

    MEMSET (L2VPN_VPLS_DEFAULT_RT (pVplsEntry),
            L2VPN_ZERO, L2VPN_MAX_VPLS_RT_LEN);

    if (MemReleaseMemBlock (L2VPN_VPLS_LB_POOL_ID,
                            (UINT1 *) L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))
        == MEM_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : MemRelease failed for L2VPN_VPLS_LB_POOL_ID\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessVplsDeleteEvent                               */
/* Description   : This routine processes VPLS Delete Event                  */
/* Input(s)      : u4VplsIndex - VPLS index                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessVplsDeleteEvent (UINT4 u4VplsIndex)
{
    INT4                i4Status = L2VPN_SUCCESS;
    tVPLSEntry         *pVplsEntry = NULL;
    ULONG8              u8RtValue = 0;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        return L2VPN_FAILURE;
    }
    if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        MEMCPY (&u8RtValue,
                L2VPN_VPLS_PREVIOUS_DEFAULT_RT (pVplsEntry),
                L2VPN_MAX_VPLS_RT_LEN);
        if (L2VPN_ZERO != u8RtValue)
        {
            L2VpnSendBgpRtDeleteEvent (u4VplsIndex,
                                       L2VPN_VPLS_PREVIOUS_DEFAULT_RT
                                       (pVplsEntry), L2VPN_VPLS_RT_BOTH);
            MEMSET (L2VPN_VPLS_PREVIOUS_DEFAULT_RT (pVplsEntry), 0,
                    L2VPN_MAX_VPLS_RT_LEN);
        }

    }
    L2VpnSendBgpVplsDeleteEvent (u4VplsIndex);

    return i4Status;
}

/******************************************************************************
 * Function Name : PwCreateDynamicEnetEntry
 * Description   : This routine will a create a Pw Enet Entry.
 * Input(s)      : CliHandle             - Context in which the command is 
 *                                         processed
 * Output(s)     : If Success -None
 *                 or else Proper Error Message is printed.
 * Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE
 *******************************************************************************/
INT4
PwCreateDynamicEnetEntry (UINT4 u4PwIndex, UINT4 u4EnetInstance,
                          UINT4 u4PwEnetPwVlan, UINT4 u4PwEnetPortVlan,
                          UINT4 u4IfIndex, UINT4 u4Mode)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    if (L2VpnSetPwEnetRowStatus (u4PwIndex, u4EnetInstance, CREATE_AND_WAIT)
        == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
             || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                      L2VPN_PWVC_ENET_ENTRY
                                                      (pPwVcEntry)),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                    u4EnetInstance)
                {
                    L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry)
                        = (UINT2) u4PwEnetPortVlan;
                    L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry)
                        = (UINT2) u4PwEnetPwVlan;
                    L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)
                        = (INT4) u4IfIndex;
                    L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) = (INT1) u4Mode;
                }
            }
        }
    }

    if (L2VpnSetPwEnetRowStatus (u4PwIndex, u4EnetInstance, ACTIVE)
        == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessRtAddEvent                                    */
/* Description   : This routine processes Rt Add Event                       */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/*                 pu1RouteTarget - RT value                                 */
/*                 u1RTType - RT Type(both/import/export)                    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessRtAddEvent (UINT4 u4VplsIndex,
                        UINT1 *pu1RouteTarget, UINT1 u1RTType)
{
    L2VpnSendBgpRtAddEvent (u4VplsIndex, pu1RouteTarget, u1RTType);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessRtDeleteEvent                                 */
/* Description   : This routine processes Rt Delete Event                    */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/*                 pu1RouteTarget - RT value                                 */
/*                 u1RTType - RT Type(both/import/export)                    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessRtDeleteEvent (UINT4 u4VplsIndex,
                           UINT1 *pu1RouteTarget, UINT1 u1RTType)
{
    L2VpnSendBgpRtDeleteEvent (u4VplsIndex, pu1RouteTarget, u1RTType);

    return L2VPN_SUCCESS;
}

#endif
