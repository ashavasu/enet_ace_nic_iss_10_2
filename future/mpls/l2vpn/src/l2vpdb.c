/***************************************************************************
 * Copyright (C)2010 Aricent Inc . All Rights Reserved
 * $Id: l2vpdb.c,v 1.17 2015/11/12 12:25:25 siva Exp $
 * Description:  This file contains the utility procedures used by L2Vpn 
 *               to access Pseudo Wire table.
 * ************************************************************************/

#include "l2vpincs.h"

PRIVATE INT4 L2VpnPwIdRBCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4
       L2VpnPwIfIndexRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
PRIVATE INT4
       L2VpnPwVcOutLblRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2);
/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIdCreateTable
 *
 * DESCRIPTION      : This function creats the Pseudo Wire table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
L2VpnPwIdCreateTable (VOID)
{
    L2VPN_MPLS_PWID_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPwVcEntry, PwIdRbNode),
                              L2VpnPwIdRBCmp);
    if (L2VPN_MPLS_PWID_TABLE == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwIdCreateTable: "
                   " RB Tree Creation failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIdDeleteTable
 *
 * DESCRIPTION      : This function Deletes the Pseudo Wire table.
 *                    
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
L2VpnPwIdDeleteTable (VOID)
{
    if (L2VPN_MPLS_PWID_TABLE != NULL)
    {
        RBTreeDestroy (L2VPN_MPLS_PWID_TABLE, NULL, 0);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIdRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the Pseudo Wire 
 *                    table. 
 *                    Indices of this table are - Pseudo Wire Id
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : L2VPN_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    L2VPN_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    L2VPN_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
L2VpnPwIdRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcEntry         *pPwEntryA = NULL;
    tPwVcEntry         *pPwEntryB = NULL;

    pPwEntryA = (tPwVcEntry *) pRBElem1;
    pPwEntryB = (tPwVcEntry *) pRBElem2;

    if (pPwEntryA->u4PwVcID < pPwEntryB->u4PwVcID)
    {
        return L2VPN_RB_LESSER;
    }
    else if (pPwEntryA->u4PwVcID > pPwEntryB->u4PwVcID)
    {
        return L2VPN_RB_GREATER;
    }

    return L2VPN_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIdAddNode
 *
 * DESCRIPTION      : This function add a node to the Pseudo Wire table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pPwVcEntry - pointer to Pseudo Wire information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
L2VpnPwIdAddNode (tPwVcEntry * pPwVcEntry)
{
    if (pPwVcEntry->u4PwVcID == L2VPN_ZERO)
    {
        return OSIX_SUCCESS;
    }

    if (RBTreeAdd (L2VPN_MPLS_PWID_TABLE, pPwVcEntry) != RB_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwIdAddNode: "
                   " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIdDelNode
 *
 * DESCRIPTION      : This function delete a Pseudo Wire node from the
 *                    Pseudo Wire table. It also takes care of updating all 
 *                    the associated datastructure links before deleting 
 *                    the node.
 *
 * INPUT            : pPwVcEntry - pointer to Pseudo Wire information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
L2VpnPwIdDelNode (tPwVcEntry * pPwVcEntry)
{
    if (pPwVcEntry->u4PwVcID == L2VPN_ZERO)
    {
        return;
    }
    RBTreeRem (L2VPN_MPLS_PWID_TABLE, pPwVcEntry);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnGetPwVcEntryFromVcId
 *
 * DESCRIPTION      : This function Gets a Pseudo Wire node from the
 *                    Pseudo Wire table.
 *
 * INPUT            :  u4PwVcId - PW VC identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : pPwVcEntry - pointer to PwVc entry ELSE NULL
 * *
 **************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromVcId (UINT4 u4PwVcId)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pVcEntry = NULL;

    if ((L2VPN_INITIALISED == FALSE) ||
        (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN))
    {
        return NULL;
    }

    pPwVcEntry = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pPwVcEntry, 0, sizeof (tPwVcEntry));

    pPwVcEntry->u4PwVcID = u4PwVcId;

    pVcEntry = (tPwVcEntry *) RBTreeGet (L2VPN_MPLS_PWID_TABLE, pPwVcEntry);

    MemReleaseMemBlock (L2VPN_PWVC_POOL_ID, (UINT1 *) pPwVcEntry);

    return pVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntryFromIndex                                */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 PwVc Index the search is done in the global array         */
/* Input(s)      : u4PwVcIndex - PwVc Index                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromIndex (UINT4 u4PwVcIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcInfo          *pPwVcInfo = NULL;

    if (L2VPN_INITIALISED != TRUE)
    {
        return NULL;
    }
    if (u4PwVcIndex > L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo) ||
        u4PwVcIndex < 1)
    {
        return NULL;
    }
    if (gpPwVcGlobalInfo->ppPwVcInfoTable != NULL)
    {
        pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwVcIndex);
        if (pPwVcInfo != NULL)
        {
            pPwVcEntry = pPwVcInfo->pPwVcEntry;
        }
    }

    return (pPwVcEntry);
}
#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name : L2VpnIsPwVpws                                             */
/* Description   : This function returns L2VPN_TRUE for VPWS and             */
/*                 L2VPN_FALSE for VPLS for the given PwVc Index.            */
/* Input(s)      : u4PwVcIndex - PwVc Index                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_TRUE - In case VPWS                                 */ 
/*                 L2VPN_FALSE - In case of VPLS                             */
/*****************************************************************************/
    INT4
L2VpnIsPwVpws (UINT4 u4PwVcIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

    if (pPwVcEntry != NULL && L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
    {
        return L2VPN_TRUE;
    }
    else
    {
        return L2VPN_FALSE;
    }
}
#endif
/*****************************************************************************/
/* Function Name : L2VpnGetWorkingPwEntryFromIndex                           */
/* Description   : This function returns pointer to PwVc Entry which is      */
/*                 working PW Entry for the given PwVc Index.                */
/* Input(s)      : u4PwVcIndex - PwVc Index                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetWorkingPwEntryFromIndex (UINT4 u4PwVcIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

    /* If there is no match found for the given PwVc Index, return NULL */
    if (pPwVcEntry == NULL)
    {
        return NULL;
    }

    /* If PW Path Type is protection path, return NULL */
    if (pPwVcEntry->u1PwPathType == L2VPN_PW_PROTECTION_PATH)
    {
        return NULL;
    }

    return (pPwVcEntry);
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwSetInVcLabel
 *
 * DESCRIPTION      : This function adds the PW entry into 
 *                    PWVC_INLABEL_RB Tree based on incoming vc label
 *
 * INPUT            : pPwVcEntry - pointer to Pseudo Wire information node
 *                    u4InVcLabel - incoming vc label
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
INT4
L2VpnPwSetInVcLabel (tPwVcEntry * pPwVcEntry, UINT4 u4InVcLabel)
{
    INT4                i4Result = L2VPN_FAILURE;
    UINT4		u4Retval = L2VPN_ZERO; 

    /* If Label is assigned statically, PW In VC label should not be set as 
     * invalid label */
    if ((u4InVcLabel == L2VPN_INVALID_LABEL) &&
        (pPwVcEntry->b1IsStaticLabel == TRUE))
    {
        return L2VPN_SUCCESS;
    }

    if (u4InVcLabel == L2VPN_INVALID_LABEL)
    {
        if (RBTreeGet (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo),
						(tRBElem *) pPwVcEntry))
		RBTreeRem (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) pPwVcEntry);
        pPwVcEntry->u4InVcLabel = L2VPN_INVALID_LABEL;
        return L2VPN_SUCCESS;
    }

    if (pPwVcEntry->u4InVcLabel != L2VPN_INVALID_LABEL)
    {
        RBTreeRem (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) pPwVcEntry);
    }
    pPwVcEntry->u4InVcLabel = u4InVcLabel;

    u4Retval =
        RBTreeAdd (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) pPwVcEntry);
    i4Result = (INT4)u4Retval;
    if (i4Result == RB_SUCCESS)
    {
        i4Result = L2VPN_SUCCESS;
    }
    else
    {
        i4Result = L2VPN_FAILURE;
    }

    return i4Result;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntryFromLabel                                */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 label and interface combination                           */
/* Input(s)      : u4Label, u4IfIndex                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromLabel (UINT4 u4Label, UINT4 u4IfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pVcEntry = NULL;

    /* Use interface index incase of per interface label space */
    UNUSED_PARAM (u4IfIndex);

    if ((L2VPN_INITIALISED == FALSE) ||
        (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN))
    {
        return NULL;
    }

    pPwVcEntry = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntry == NULL)
    {
        return NULL;
    }
    MEMSET (pPwVcEntry, 0, sizeof (tPwVcEntry));

    pPwVcEntry->u4InVcLabel = u4Label;

    pVcEntry = (tPwVcEntry *) RBTreeGet
        (L2VPN_PWVC_INLABEL_RB_PTR (gpPwVcGlobalInfo), pPwVcEntry);

    MemReleaseMemBlock (L2VPN_PWVC_POOL_ID, (UINT1 *) pPwVcEntry);

    return pVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntryFromOutLabel                             */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 out label and interface combination                       */
/* Input(s)      : u4Label, u4IfIndex                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromOutLabel (UINT4 u4Label)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pVcEntry = NULL;

    
    

    if ((L2VPN_INITIALISED == FALSE) ||
        (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN))
    {
        return NULL;
    }

    pPwVcEntry = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntry == NULL)
    {
        return NULL;
    }
    MEMSET (pPwVcEntry, 0, sizeof (tPwVcEntry));

    pPwVcEntry->u4OutVcLabel = u4Label;

    pVcEntry = (tPwVcEntry *) RBTreeGet
        (L2VPN_PWVC_OUTLABEL_RB_TREE, pPwVcEntry);

    MemReleaseMemBlock (L2VPN_PWVC_POOL_ID, (UINT1 *) pPwVcEntry);

    return pVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnValidatePsnEntry                                     */
/* Description   : This function validates the PSN Entry                     */
/* Input(s)      : u4PwIndex                                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : If PSN entry is NULL, it returns SNMP failure             */
/*                 Otherwise, it returns SNMP success                        */
/*****************************************************************************/
INT1
L2VpnValidatePsnEntry (UINT4 u4PwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if ((pPwVcEntry == NULL) || (pPwVcEntry->pPSNEntry == NULL))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIfIndexCreateTable
 *
 * DESCRIPTION      : This function creates the PwIfIndex table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
L2VpnPwIfIndexCreateTable (VOID)
{
    L2VPN_MPLS_PWIFINDEX_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPwIfIndexMap, PwIfIndexRbNode),
                              L2VpnPwIfIndexRBTreeCmp);
    if (L2VPN_MPLS_PWIFINDEX_TABLE == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwIfIndexCreateTable: "
                   " RB Tree Creation failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIfIndexDeleteTable
 *
 * DESCRIPTION      : This function Deletes the PwIfIndex table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
L2VpnPwIfIndexDeleteTable (VOID)
{
    tPwIfIndexMap      *pPwIfIndexMap = NULL;

    pPwIfIndexMap = RBTreeGetFirst (L2VPN_MPLS_PWIFINDEX_TABLE);

    while (pPwIfIndexMap != NULL)
    {
        RBTreeRemove (L2VPN_MPLS_PWIFINDEX_TABLE, pPwIfIndexMap);

        pPwIfIndexMap->pPwVcEntry = NULL;
        MemReleaseMemBlock (L2VPN_PW_IFINDEX_POOL_ID, (UINT1 *) pPwIfIndexMap);

        pPwIfIndexMap = RBTreeGetFirst (L2VPN_MPLS_PWIFINDEX_TABLE);
    }

    RBTreeDestroy (L2VPN_MPLS_PWIFINDEX_TABLE, NULL, 0);
    L2VPN_MPLS_PWIFINDEX_TABLE = NULL;

    return;
}

/************************************************************************
 *  Function Name   : L2VpnPwIfIndexRBTreeCmp
 *  Description     : RBTree Compare function for CFA Index entry.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
PRIVATE INT4
L2VpnPwIfIndexRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tPwIfIndexMap      *pPwIfIndexA = NULL;
    tPwIfIndexMap      *pPwIfIndexB = NULL;

    pPwIfIndexA = (tPwIfIndexMap *) pRBElem1;
    pPwIfIndexB = (tPwIfIndexMap *) pRBElem2;

    if ((pPwIfIndexA->pPwVcEntry->u4PwIfIndex) <
        (pPwIfIndexB->pPwVcEntry->u4PwIfIndex))
    {
        return L2VPN_RB_LESSER;
    }
    else if ((pPwIfIndexA->pPwVcEntry->u4PwIfIndex) >
             (pPwIfIndexB->pPwVcEntry->u4PwIfIndex))
    {
        return L2VPN_RB_GREATER;
    }

    return L2VPN_RB_EQUAL;

}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIfIndexAddNode
 *
 * DESCRIPTION      : This function add a node to the PwIfIndex table,
 *                    and takes care of updating all the required
 *                    datastructure links.
 *
 * INPUT            : pPwVcEntry - pointer to Pseudo Wire information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
L2VpnPwIfIndexAddNode (tPwVcEntry * pPwVcEntry)
{
    tPwIfIndexMap      *pPwIfIndexMap = NULL;

    if (pPwVcEntry->u4PwIfIndex == L2VPN_ZERO)
    {
        return OSIX_FAILURE;
    }

    pPwIfIndexMap = (tPwIfIndexMap *) MemAllocMemBlk (L2VPN_PW_IFINDEX_POOL_ID);

    if (pPwIfIndexMap == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pPwIfIndexMap, 0, sizeof (tPwIfIndexMap));

    pPwIfIndexMap->pPwVcEntry = pPwVcEntry;
    if (RBTreeAdd (L2VPN_MPLS_PWIFINDEX_TABLE, pPwIfIndexMap) != RB_SUCCESS)
    {
        MemReleaseMemBlock (L2VPN_PW_IFINDEX_POOL_ID, (UINT1 *) pPwIfIndexMap);
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwIfIndexAddNode: "
                   " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIfIndexDeleteNode
 *
 * DESCRIPTION      : This function delete a PwIfIndex node from the
 *                    PwIfIndex table. It also takes care of updating all
 *                    the associated datastructure links before deleting
 *                    the node.
 *
 * INPUT            : pPwVcEntry - pointer to Pseudo Wire information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
L2VpnPwIfIndexDeleteNode (tPwVcEntry * pPwVcEntry)
{
    tPwIfIndexMap      *pPwIfIndexMap;

    if (L2VPN_PWVC_IF_INDEX (pPwVcEntry) == L2VPN_ZERO)
    {
        return;
    }

    pPwIfIndexMap = L2VpnPwIfIndexGetNode (L2VPN_PWVC_IF_INDEX (pPwVcEntry));

    if (pPwIfIndexMap == NULL)
    {
        return;
    }

    if (RBTreeRemove (L2VPN_MPLS_PWIFINDEX_TABLE, pPwIfIndexMap) != RB_FAILURE)
    {
        pPwIfIndexMap->pPwVcEntry = NULL;

        MemReleaseMemBlock (L2VPN_PW_IFINDEX_POOL_ID, (UINT1 *) pPwIfIndexMap);
    }
    pPwVcEntry->u4PwIfIndex = L2VPN_ZERO;

    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwIfIndexGetNode
 *
 * DESCRIPTION      : This function Gets a PwIfIndexMap node from the
 *                    PwIfIndex table.
 *
 * INPUT            : u4PwIfIndex - pseudowire IfIndex
 *
 * OUTPUT           : None
 *
 * RETURNS          : tPwIfIndexMap - pointer to pseudowire IfIndex Map  entry 
 *                    ELSE NULL
 **************************************************************************/
tPwIfIndexMap      *
L2VpnPwIfIndexGetNode (UINT4 u4PwIfIndex)
{
    tPwIfIndexMap       PwIfIndexMap;
    tPwIfIndexMap      *pPwIfIndexMap;
    tPwVcEntry         *pPwVcEntry = NULL;

    MEMSET (&PwIfIndexMap, L2VPN_ZERO, sizeof (tPwIfIndexMap));

    pPwVcEntry = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntry == NULL)
    {
        return NULL;
    }

    PwIfIndexMap.pPwVcEntry = pPwVcEntry;
    PwIfIndexMap.pPwVcEntry->u4PwIfIndex = u4PwIfIndex;

    pPwIfIndexMap = RBTreeGet (L2VPN_MPLS_PWIFINDEX_TABLE, &PwIfIndexMap);

    MemReleaseMemBlock (L2VPN_PWVC_POOL_ID, (UINT1 *) pPwVcEntry);

    return (pPwIfIndexMap);
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnGetPwIndexFromPwIfIndex
 *
 * DESCRIPTION      : This function Gets a PwIfIndex and return the 
 *                    PwIndex.
 *
 * INPUT            : u4PwIfIndex - pseudowire IfIndex
 *
 * OUTPUT           : *pu4PwIndex - PW Index
 *
 * RETURNS          : L2VPN_FAILURE / L2VPN_SUCCESS
 * *
 **************************************************************************/
INT4
L2VpnGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex)
{
    tPwIfIndexMap      *pPwIfIndexMap = NULL;

    *pu4PwIndex = L2VPN_ZERO;

    pPwIfIndexMap = L2VpnPwIfIndexGetNode (u4PwIfIndex);

    if (pPwIfIndexMap != NULL)
    {
        *pu4PwIndex = pPwIfIndexMap->pPwVcEntry->u4PwVcIndex;
        return L2VPN_SUCCESS;
    }

    return L2VPN_FAILURE;
}

/******************************************************************************
 *  Function Name : L2VpnMplsPortEntryGetNode
 *  Description   : This function Gets a tMplsPortEntryInfo node from the
 *                  MPLS port table.
 *  Input(s)      : u4IfIndex - CFA IfIndex
 *
 *  Output(s)     : None
 *  Return(s)     : tMplsPortEntryInfo - pointer to MPLS port node entry
 *                  ELSE NULL
 * ******************************************************************************/
tMplsPortEntryInfo *
L2VpnMplsPortEntryGetNode (UINT4 u4IfIndex)
{
    tMplsPortEntryInfo  MplsPortEntryInfo;
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    MEMSET (&MplsPortEntryInfo, MPLS_ZERO, sizeof (tMplsPortEntryInfo));

    MplsPortEntryInfo.u4IfIndex = u4IfIndex;

    pMplsPortEntryInfo =
        RBTreeGet (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE, &MplsPortEntryInfo);

    return (pMplsPortEntryInfo);

}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwOutLabelBasedTable
 *
 * DESCRIPTION      : This function creates the OutLabel Based RBTree
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
L2VpnPwCreateOutLabelBasedRBTree ()
{
    L2VPN_PWVC_OUTLABEL_RB_TREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPwVcEntry, OutVcLblRbNode),
                              (tRBCompareFn) L2VpnPwVcOutLblRbTreeCmpFunc);

    if (L2VPN_PWVC_OUTLABEL_RB_TREE == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwCreateOutLabelBasedRBTree: "
                   " RB Tree Creation failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : L2VpnPwVcOutLblRbTreeCmpFunc
 *  Description     : RBTree Compare function for Pw-OutVcLabel 
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
PRIVATE INT4
L2VpnPwVcOutLblRbTreeCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPwVcEntry         *pPwVcEntry1 = (tPwVcEntry *) pRBElem1;
    tPwVcEntry         *pPwVcEntry2 = (tPwVcEntry *) pRBElem2;

    if (pPwVcEntry1->u4OutVcLabel < pPwVcEntry2->u4OutVcLabel)
    {
        return -1;
    }
    else if (pPwVcEntry1->u4OutVcLabel > pPwVcEntry2->u4OutVcLabel)
    {
        return 1;
    }

    return 0;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwOutLabelDeleteTable
 *
 * DESCRIPTION      : This function Deletes the PwVcOutLabelRBTree
 *                    
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
L2VpnPwDeleteOutLabelBasedRBTree ()
{
    if (L2VPN_PWVC_OUTLABEL_RB_TREE != NULL)
    {
        RBTreeDestroy (L2VPN_PWVC_OUTLABEL_RB_TREE, NULL, 0);
    }
    L2VPN_PWVC_OUTLABEL_RB_TREE = NULL;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwSetOutVcLabel
 *
 * DESCRIPTION      : This function adds the PW entry into 
 *                    PWVC_OUTLABEL_RB Tree based on outgoing vc label
 *
 * INPUT            : pPwVcEntry - pointer to Pseudo Wire information node
 *                    U4OutVcLabel - Outgoing VC Label
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
INT4
L2VpnPwSetOutVcLabel (tPwVcEntry * pPwVcEntry, UINT4 u4OutVcLabel)
{

    if (u4OutVcLabel == L2VPN_INVALID_LABEL)
    {
        RBTreeRem (L2VPN_PWVC_OUTLABEL_RB_TREE, (tRBElem *) pPwVcEntry);
        pPwVcEntry->u4OutVcLabel = L2VPN_INVALID_LABEL;
        return L2VPN_SUCCESS;
    }

    if (pPwVcEntry->u4OutVcLabel != L2VPN_INVALID_LABEL)
    {
        RBTreeRem (L2VPN_PWVC_OUTLABEL_RB_TREE, (tRBElem *) pPwVcEntry);
    }

    pPwVcEntry->u4OutVcLabel = u4OutVcLabel;

    if (RBTreeAdd (L2VPN_PWVC_OUTLABEL_RB_TREE, pPwVcEntry) != RB_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VpnPwSetOutVcLabel: "
                   " RB addtion failure \r\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file l2vpdb.c                        */
/*-----------------------------------------------------------------------*/
