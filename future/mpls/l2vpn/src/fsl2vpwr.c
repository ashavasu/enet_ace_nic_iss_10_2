/* $Id: fsl2vpwr.c,v 1.6 2015/09/15 06:42:59 siva Exp $ */

# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fsl2vplw.h"
# include  "fsl2vpwr.h"
# include  "fsl2vpdb.h"


VOID RegisterFSL2VP ()
{
	SNMPRegisterMibWithContextIdAndLock (&fsl2vpOID, &fsl2vpEntry, NULL, NULL, NULL, NULL, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fsl2vpOID, (const UINT1 *) "fsl2vpn");
}



VOID UnRegisterFSL2VP ()
{
	SNMPUnRegisterMib (&fsl2vpOID, &fsl2vpEntry);
	SNMPDelSysorEntry (&fsl2vpOID, (const UINT1 *) "fsl2vpn");
}

INT4 VplsConfigIndexNextGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVplsConfigIndexNext(&(pMultiData->u4_ULongValue)));
}

INT4 GetNextIndexVplsConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVplsConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVplsConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 VplsConfigNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 VplsConfigDescrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigDescr(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 VplsConfigAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigAdminStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigMacLearningGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigMacLearning(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigDiscardUnknownDestGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigDiscardUnknownDest(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigMacAgingGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigMacAging(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigFwdFullHighWatermarkGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigFwdFullHighWatermark(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 VplsConfigFwdFullLowWatermarkGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigFwdFullLowWatermark(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 VplsConfigRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigMtuGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigMtu(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 VplsConfigVpnIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigVpnId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 VplsConfigStorageTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigSignalingTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsConfigSignalingType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsConfigNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsConfigDescrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigDescr(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsConfigAdminStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigAdminStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigMacLearningSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigMacLearning(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigDiscardUnknownDestSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigDiscardUnknownDest(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigMacAgingSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigMacAging(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigFwdFullHighWatermarkSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigFwdFullHighWatermark(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsConfigFwdFullLowWatermarkSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigFwdFullLowWatermark(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsConfigRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigMtuSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigMtu(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsConfigStorageTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigSignalingTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsConfigSignalingType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigNameTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigName(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsConfigDescrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigDescr(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsConfigAdminStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigAdminStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigMacLearningTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigMacLearning(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigDiscardUnknownDestTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigDiscardUnknownDest(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigMacAgingTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigMacAging(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigFwdFullHighWatermarkTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigFwdFullHighWatermark(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsConfigFwdFullLowWatermarkTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigFwdFullLowWatermark(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsConfigRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigMtuTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigMtu(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsConfigStorageTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigStorageType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigSignalingTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsConfigSignalingType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2VplsConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexVplsStatusTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVplsStatusTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVplsStatusTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 VplsStatusOperStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsStatusTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsStatusOperStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsStatusPeerCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsStatusTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsStatusPeerCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}

INT4 GetNextIndexVplsPwBindTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVplsPwBindTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVplsPwBindTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 VplsPwBindConfigTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsPwBindTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsPwBindConfigType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsPwBindTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsPwBindTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsPwBindType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsPwBindRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsPwBindTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsPwBindRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsPwBindStorageTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsPwBindTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsPwBindStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsPwBindConfigTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsPwBindConfigType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsPwBindType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsPwBindRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindStorageTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsPwBindStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindConfigTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsPwBindConfigType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsPwBindType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsPwBindRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindStorageTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsPwBindStorageType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsPwBindTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2VplsPwBindTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexVplsBgpADConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVplsBgpADConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVplsBgpADConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 VplsBgpADConfigRouteDistinguisherGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpADConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpADConfigRouteDistinguisher(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 VplsBgpADConfigPrefixGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpADConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpADConfigPrefix(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 VplsBgpADConfigVplsIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpADConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpADConfigVplsId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 VplsBgpADConfigRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpADConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpADConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsBgpADConfigStorageTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpADConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpADConfigStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsBgpADConfigRouteDistinguisherSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpADConfigRouteDistinguisher(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsBgpADConfigPrefixSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpADConfigPrefix(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsBgpADConfigVplsIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpADConfigVplsId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsBgpADConfigRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpADConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpADConfigStorageTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpADConfigStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpADConfigRouteDistinguisherTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpADConfigRouteDistinguisher(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsBgpADConfigPrefixTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpADConfigPrefix(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 VplsBgpADConfigVplsIdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpADConfigVplsId(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsBgpADConfigRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpADConfigRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpADConfigStorageTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpADConfigStorageType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpADConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2VplsBgpADConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexVplsBgpRteTargetTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVplsBgpRteTargetTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVplsBgpRteTargetTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 VplsBgpRteTargetRTTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpRteTargetTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpRteTargetRTType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsBgpRteTargetRTGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpRteTargetTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpRteTargetRT(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 VplsBgpRteTargetRTRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpRteTargetTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpRteTargetRTRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsBgpRteTargetStorageTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVplsBgpRteTargetTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVplsBgpRteTargetStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 VplsBgpRteTargetRTTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpRteTargetRTType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpRteTargetRTSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpRteTargetRT(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsBgpRteTargetRTRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpRteTargetRTRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpRteTargetStorageTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVplsBgpRteTargetStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpRteTargetRTTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpRteTargetRTType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpRteTargetRTTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpRteTargetRT(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 VplsBgpRteTargetRTRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpRteTargetRTRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpRteTargetStorageTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2VplsBgpRteTargetStorageType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 VplsBgpRteTargetTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2VplsBgpRteTargetTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 VplsStatusNotifEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVplsStatusNotifEnable(&(pMultiData->i4_SLongValue)));
}
INT4 VplsNotificationMaxRateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVplsNotificationMaxRate(&(pMultiData->u4_ULongValue)));
}
INT4 VplsStatusNotifEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetVplsStatusNotifEnable(pMultiData->i4_SLongValue));
}


INT4 VplsNotificationMaxRateSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetVplsNotificationMaxRate(pMultiData->u4_ULongValue));
}


INT4 VplsStatusNotifEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2VplsStatusNotifEnable(pu4Error, pMultiData->i4_SLongValue));
}


INT4 VplsNotificationMaxRateTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2VplsNotificationMaxRate(pu4Error, pMultiData->u4_ULongValue));
}


INT4 VplsStatusNotifEnableDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2VplsStatusNotifEnable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 VplsNotificationMaxRateDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2VplsNotificationMaxRate(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsL2VpnPwRedundancyStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsL2VpnPwRedundancyStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsL2VpnPwRedNegotiationTimeOutGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsL2VpnPwRedNegotiationTimeOut(&(pMultiData->u4_ULongValue)));
}
INT4 FsL2VpnPwRedundancySyncFailNotifyEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsL2VpnPwRedundancySyncFailNotifyEnable(&(pMultiData->i4_SLongValue)));
}
INT4 FsL2VpnPwRedundancyPwStatusNotifyEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsL2VpnPwRedundancyPwStatusNotifyEnable(&(pMultiData->i4_SLongValue)));
}
INT4 FsL2VpnPwRedundancyStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsL2VpnPwRedundancyStatus(pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedNegotiationTimeOutSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsL2VpnPwRedNegotiationTimeOut(pMultiData->u4_ULongValue));
}


INT4 FsL2VpnPwRedundancySyncFailNotifyEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsL2VpnPwRedundancySyncFailNotifyEnable(pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedundancyPwStatusNotifyEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsL2VpnPwRedundancyPwStatusNotifyEnable(pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedundancyStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsL2VpnPwRedundancyStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedNegotiationTimeOutTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsL2VpnPwRedNegotiationTimeOut(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsL2VpnPwRedundancySyncFailNotifyEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsL2VpnPwRedundancySyncFailNotifyEnable(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedundancyPwStatusNotifyEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsL2VpnPwRedundancyPwStatusNotifyEnable(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedundancyStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedundancyStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsL2VpnPwRedNegotiationTimeOutDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedNegotiationTimeOut(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsL2VpnPwRedundancySyncFailNotifyEnableDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedundancySyncFailNotifyEnable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsL2VpnPwRedundancyPwStatusNotifyEnableDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedundancyPwStatusNotifyEnable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsL2VpnPwRedGroupTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsL2VpnPwRedGroupTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsL2VpnPwRedGroupTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsL2VpnPwRedGroupProtTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupProtType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupReversionTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupReversionType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupContentionResolutionMethodGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupContentionResolutionMethod(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupLockoutProtectionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupLockoutProtection(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupMasterSlaveModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupMasterSlaveMode(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupDualHomeAppsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupDualHomeApps(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedGroupNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedGroupStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedGroupOperActivePwGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupOperActivePw(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsL2VpnPwRedGroupWtrTimerGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupWtrTimer(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsL2VpnPwRedGroupAdminCmdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupAdminCmd(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupAdminActivePwGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupAdminActivePw(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsL2VpnPwRedGroupAdminCmdStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupAdminCmdStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedGroupRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedGroupProtTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupProtType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupReversionTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupReversionType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupContentionResolutionMethodSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupContentionResolutionMethod(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupLockoutProtectionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupLockoutProtection(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupMasterSlaveModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupMasterSlaveMode(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupDualHomeAppsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupDualHomeApps(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsL2VpnPwRedGroupNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsL2VpnPwRedGroupWtrTimerSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupWtrTimer(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsL2VpnPwRedGroupAdminCmdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupAdminCmd(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupAdminActivePwSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupAdminActivePw(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsL2VpnPwRedGroupRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedGroupRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupProtTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupProtType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupReversionTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupReversionType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupContentionResolutionMethodTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupContentionResolutionMethod(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupLockoutProtectionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupLockoutProtection(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupMasterSlaveModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupMasterSlaveMode(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupDualHomeAppsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupDualHomeApps(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsL2VpnPwRedGroupNameTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupName(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsL2VpnPwRedGroupWtrTimerTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupWtrTimer(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsL2VpnPwRedGroupAdminCmdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupAdminCmd(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupAdminActivePwTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupAdminActivePw(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsL2VpnPwRedGroupRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedGroupRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedGroupTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedGroupTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsL2VpnPwRedNodeTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsL2VpnPwRedNodeTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsL2VpnPwRedNodeTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].pOctetStrValue,
			pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsL2VpnPwRedNodeLocalLdpIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedNodeTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedNodeLocalLdpID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedNodeLocalLdpEntityIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedNodeTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedNodeLocalLdpEntityIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsL2VpnPwRedNodePeerLdpIDGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedNodeTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedNodePeerLdpID(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedNodeStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedNodeTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedNodeStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedNodeRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedNodeTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedNodeRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedNodeRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedNodeRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedNodeRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedNodeRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedNodeTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedNodeTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsL2VpnPwRedPwTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsL2VpnPwRedPwTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsL2VpnPwRedPwTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsL2VpnPwRedPwPreferanceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedPwPreferance(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedPwLocalStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedPwLocalStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedPwRemoteStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedPwRemoteStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedPwOperStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedPwOperStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedPwRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedPwRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsL2VpnPwRedPwPreferanceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedPwPreferance(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedPwRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsL2VpnPwRedPwRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedPwPreferanceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedPwPreferance(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedPwRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsL2VpnPwRedPwRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsL2VpnPwRedPwTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedPwTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsL2VpnPwRedIccpPwTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsL2VpnPwRedIccpPwTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pNextMultiIndex->pIndex[1].pOctetStrValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pNextMultiIndex->pIndex[3].pOctetStrValue,
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			&(pNextMultiIndex->pIndex[5].u4_ULongValue),
			&(pNextMultiIndex->pIndex[6].i4_SLongValue),
			pNextMultiIndex->pIndex[7].pOctetStrValue,
			&(pNextMultiIndex->pIndex[8].i4_SLongValue),
			pNextMultiIndex->pIndex[9].pOctetStrValue,
			&(pNextMultiIndex->pIndex[10].i4_SLongValue),
			pNextMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsL2VpnPwRedIccpPwTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].pOctetStrValue,
			pNextMultiIndex->pIndex[1].pOctetStrValue,
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].pOctetStrValue,
			pNextMultiIndex->pIndex[3].pOctetStrValue,
			pFirstMultiIndex->pIndex[4].u4_ULongValue,
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			pFirstMultiIndex->pIndex[5].u4_ULongValue,
			&(pNextMultiIndex->pIndex[5].u4_ULongValue),
			pFirstMultiIndex->pIndex[6].i4_SLongValue,
			&(pNextMultiIndex->pIndex[6].i4_SLongValue),
			pFirstMultiIndex->pIndex[7].pOctetStrValue,
			pNextMultiIndex->pIndex[7].pOctetStrValue,
			pFirstMultiIndex->pIndex[8].i4_SLongValue,
			&(pNextMultiIndex->pIndex[8].i4_SLongValue),
			pFirstMultiIndex->pIndex[9].pOctetStrValue,
			pNextMultiIndex->pIndex[9].pOctetStrValue,
			pFirstMultiIndex->pIndex[10].i4_SLongValue,
			&(pNextMultiIndex->pIndex[10].i4_SLongValue),
			pFirstMultiIndex->pIndex[11].pOctetStrValue,
			pNextMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsL2VpnPwRedIccpPwRoIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedIccpPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		pMultiIndex->pIndex[6].i4_SLongValue,
		pMultiIndex->pIndex[7].pOctetStrValue,
		pMultiIndex->pIndex[8].i4_SLongValue,
		pMultiIndex->pIndex[9].pOctetStrValue,
		pMultiIndex->pIndex[10].i4_SLongValue,
		pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedIccpPwRoId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		pMultiIndex->pIndex[6].i4_SLongValue,
		pMultiIndex->pIndex[7].pOctetStrValue,
		pMultiIndex->pIndex[8].i4_SLongValue,
		pMultiIndex->pIndex[9].pOctetStrValue,
		pMultiIndex->pIndex[10].i4_SLongValue,
		pMultiIndex->pIndex[11].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedIccpPwPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedIccpPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		pMultiIndex->pIndex[6].i4_SLongValue,
		pMultiIndex->pIndex[7].pOctetStrValue,
		pMultiIndex->pIndex[8].i4_SLongValue,
		pMultiIndex->pIndex[9].pOctetStrValue,
		pMultiIndex->pIndex[10].i4_SLongValue,
		pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedIccpPwPriority(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		pMultiIndex->pIndex[6].i4_SLongValue,
		pMultiIndex->pIndex[7].pOctetStrValue,
		pMultiIndex->pIndex[8].i4_SLongValue,
		pMultiIndex->pIndex[9].pOctetStrValue,
		pMultiIndex->pIndex[10].i4_SLongValue,
		pMultiIndex->pIndex[11].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsL2VpnPwRedIccpPwStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsL2VpnPwRedIccpPwTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		pMultiIndex->pIndex[6].i4_SLongValue,
		pMultiIndex->pIndex[7].pOctetStrValue,
		pMultiIndex->pIndex[8].i4_SLongValue,
		pMultiIndex->pIndex[9].pOctetStrValue,
		pMultiIndex->pIndex[10].i4_SLongValue,
		pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsL2VpnPwRedIccpPwStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].u4_ULongValue,
		pMultiIndex->pIndex[6].i4_SLongValue,
		pMultiIndex->pIndex[7].pOctetStrValue,
		pMultiIndex->pIndex[8].i4_SLongValue,
		pMultiIndex->pIndex[9].pOctetStrValue,
		pMultiIndex->pIndex[10].i4_SLongValue,
		pMultiIndex->pIndex[11].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsL2VpnPwRedSimulateFailureGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsL2VpnPwRedSimulateFailure(&(pMultiData->i4_SLongValue)));
}
INT4 FsL2VpnPwRedSimulateFailureForNbrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsL2VpnPwRedSimulateFailureForNbr(&(pMultiData->u4_ULongValue)));
}
INT4 FsL2VpnPwRedSimulateFailureSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsL2VpnPwRedSimulateFailure(pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedSimulateFailureForNbrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsL2VpnPwRedSimulateFailureForNbr(pMultiData->u4_ULongValue));
}


INT4 FsL2VpnPwRedSimulateFailureTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsL2VpnPwRedSimulateFailure(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsL2VpnPwRedSimulateFailureForNbrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsL2VpnPwRedSimulateFailureForNbr(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsL2VpnPwRedSimulateFailureDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedSimulateFailure(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsL2VpnPwRedSimulateFailureForNbrDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsL2VpnPwRedSimulateFailureForNbr(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

