/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vppwred.c,v 1.11 2016/05/06 10:40:10 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vppwred.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines to handle PW
 *                             redundancy procedures.
 *---------------------------------------------------------------------------*/

#define _L2VPPWRED_C

#include "l2vpdefs.h" 
#include "l2vpincs.h"
#include "snmputil.h"

UINT4               gaL2VPN_TRAP_OID[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 72 };

UINT4               gaL2VPN_SNMP_TRAP_OID[] =
    { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

#ifndef NPAPI_WANTED
extern VOID VlanVplsDelPwFdbEntries (UINT4 u4PwVcIndex);
#endif

/*****************************************************************************/
/* Function Name : L2vpnRedundancyIccpPwDataTx                               */
/* Description   : This routine sends ICCP PW RG DATA message to LDP         */
/* Input(s)      : pRgNode - pointer to the Redundancy node entry            */
/*                 pRgIccpPw - pointer to the PW information entry           */
/*                 u2ResponseNum - Response number (Request number) to be    */
/*                                 used by LDP for ICCP message              */
/*                 u1ResponseType - Response type                            */
/*                 pResponsePwList - List contains PW information to be sent */
/*                 u1RequestType   - Request type to be sent in ICCP message */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyIccpPwDataTx (tL2vpnRedundancyNodeEntry * pRgNode,
                             tL2vpnIccpPwEntry * pRgIccpPw, UINT2 u2ResponseNum,
                             UINT1 u1ResponseType, tTMO_SLL * pResponsePwList,
                             UINT1 u1RequestType)
{
    tL2VpnLdpPwVcEvtInfo TmpL2VpnEvtInfo;
    tPwRedDataEvtPwData *pL2vpnEvtPwData = NULL;
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    MEMSET (&TmpL2VpnEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    if ((pRgNode == NULL) || (pRgNode->pSession == NULL) ||
        !(pRgNode->u1Status & L2VPNRED_NODE_STATUS_CONNECTED))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return L2VPN_FAILURE;
    }

    L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "NODE[%u, %02X%02X%02X%02X], "
                "pRgIccpPw(%p), u2ResponseNum(%u), u1ResponseType(%#X), "
                " u1RequestType(%#X)\n",
                pRgNode->u4RgIndex,
                pRgNode->Addr.au1Ipv4Addr[0], pRgNode->Addr.au1Ipv4Addr[1],
                pRgNode->Addr.au1Ipv4Addr[2], pRgNode->Addr.au1Ipv4Addr[3],
                pRgIccpPw, u2ResponseNum, u1ResponseType, u1RequestType);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgNode->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    TmpL2VpnEvtInfo.u4EvtType = L2VPN_LDP_ICCP_RG_EVT;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u2MsgType = L2VPN_LDP_ICCP_RG_DATA_MSG;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u4GroupId = pRgNode->u4RgIndex;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u4LocalLdpEntityId =
        pRgNode->pSession->u4LocalLdpEntityID;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u4LocalLdpEntityIndex =
        pRgNode->pSession->u4LdpEntityIndex;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u4PeerLdpId =
        pRgNode->pSession->u4PeerLdpId;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.PeerAddr = pRgNode->Addr;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u1AddrType = pRgNode->u1AddrType;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.u2ResponseNum = u2ResponseNum;
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.u2ResponseNum = u2ResponseNum;
    TMO_SLL_Init (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.DataList);
    TMO_SLL_Init (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.RequestList);

    if (pRgIccpPw != NULL)
    {
        if (LdpL2VpnGetEventPwData ((VOID *) &pL2vpnEvtPwData) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 4\n");
            return L2VPN_FAILURE;
        }

        pL2vpnEvtPwData->Data = *pRgIccpPw;
        MEMSET (&pL2vpnEvtPwData->Data.RbNode, 0,
                sizeof (pL2vpnEvtPwData->Data.RbNode));
        pL2vpnEvtPwData->Data.pRgParent = NULL;

        pL2vpnEvtPwData->u1Type = u1ResponseType;
        pL2vpnEvtPwData->u1Type |= L2VPNRED_ICCP_DATA_STATE;
        pL2vpnEvtPwData->u1Type |= L2VPNRED_ICCP_DATA_SYNCHRONIZED;

        TMO_SLL_Add (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.DataList,
                     &pL2vpnEvtPwData->ListNode);
    }
    else if ((pResponsePwList != NULL) && (TMO_SLL_Count (pResponsePwList) > 0))
    {
        VOID               *pSllNode = NULL;
        tPwRedDataEvtPwFec *pIccpPwFec = NULL;
        tL2vpnIccpPwEntry  *pIccpPw = NULL;
        UINT4               u4RouterId = 0;

        TMO_SLL_Scan (pResponsePwList, pSllNode, tPwRedDataEvtPwFec *)
        {
            pIccpPwFec = CONTAINER_OF (pSllNode, tPwRedDataEvtPwFec, ListNode);
            u4RouterId = OSIX_HTONL (pRgNode->pSession->u4PeerLdpId);

            if (!(u1RequestType & L2VPNRED_ICCP_REQ_SERVICE_NAME))
            {
                pIccpPw = L2VpnGetIccpPwEntryByIndex (pRgNode->u4RgIndex,
                                                      (tMplsRouterId *) &
                                                      u4RouterId,
                                                      &pIccpPwFec->u.Fec);
            }
            else if (u1RequestType & L2VPNRED_ICCP_REQ_SERVICE_NAME)
            {
                pIccpPw =
                    L2VpnGetIccpPwEntryByRgServiceName (pRgNode->u4RgIndex,
                                                        (tMplsRouterId *) &
                                                        u4RouterId,
                                                        &pIccpPwFec->u.Name);
            }

            if (pIccpPw == NULL)
            {
                continue;
            }

            if (LdpL2VpnGetEventPwData ((VOID *) &pL2vpnEvtPwData) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 5\n");
                return L2VPN_FAILURE;
            }

            pL2vpnEvtPwData->Data = *pIccpPw;
            MEMSET (&pL2vpnEvtPwData->Data.RbNode, 0,
                    sizeof (pL2vpnEvtPwData->Data.RbNode));
            pL2vpnEvtPwData->Data.pRgParent = NULL;

            pL2vpnEvtPwData->u1Type = u1ResponseType;
            pL2vpnEvtPwData->u1Type |= L2VPNRED_ICCP_DATA_STATE;
            pL2vpnEvtPwData->u1Type |= L2VPNRED_ICCP_DATA_SYNCHRONIZED;

            TMO_SLL_Add (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.DataList,
                         &pL2vpnEvtPwData->ListNode);
        }
    }
    else
    {
        tL2vpnRedundancyPwEntry *pTmpPwEntry = NULL;
        tL2vpnRedundancyPwEntry *pPwRb = NULL;

        pTmpPwEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
        if (pTmpPwEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 6\n");
            return L2VPN_FAILURE;
        }

        MEMSET (pTmpPwEntry, 0, sizeof (*pTmpPwEntry));
        pTmpPwEntry->u4RgIndex = pRgNode->u4RgIndex;

        while ((pPwRb = RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                                       (tRBElem *) pTmpPwEntry, NULL)) != NULL)
        {
            if (pPwRb->u4RgIndex != pRgNode->u4RgIndex)
            {
                break;
            }

            if ((pPwRb->pIccpPwEntry == NULL) ||
                (LdpL2VpnGetEventPwData ((VOID *) &pL2vpnEvtPwData) ==
                 L2VPN_FAILURE))
            {
                MEMSET (pTmpPwEntry, 0, sizeof (*pTmpPwEntry));
                pTmpPwEntry->u4RgIndex = pPwRb->u4RgIndex;
                pTmpPwEntry->u4PwIndex = pPwRb->u4PwIndex;
                continue;
            }

            pL2vpnEvtPwData->Data = *pPwRb->pIccpPwEntry;
            MEMSET (&pL2vpnEvtPwData->Data.RbNode, 0,
                    sizeof (pL2vpnEvtPwData->Data.RbNode));
            pL2vpnEvtPwData->Data.pRgParent = NULL;

            pL2vpnEvtPwData->u1Type = u1ResponseType;
            pL2vpnEvtPwData->u1Type |= L2VPNRED_ICCP_DATA_STATE;

            TMO_SLL_Add (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.DataList,
                         &pL2vpnEvtPwData->ListNode);

            MEMSET (pTmpPwEntry, 0, sizeof (*pTmpPwEntry));
            pTmpPwEntry->u4RgIndex = pPwRb->u4RgIndex;
            pTmpPwEntry->u4PwIndex = pPwRb->u4PwIndex;
        }
        MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPwEntry);

        if (pL2vpnEvtPwData != NULL)
        {
            pL2vpnEvtPwData->u1Type |= L2VPNRED_ICCP_DATA_SYNCHRONIZED;
        }
    }
    TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u4MessageId = ++pRgGroup->u4MessageId;

    if (u1RequestType != 0)
    {
        TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.u1RequestType =
            u1RequestType;
        TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.u1RequestType |=
            L2VPNRED_ICCP_REQ_STATE;
        TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.u1RequestType |=
            L2VPNRED_ICCP_REQ_ALL;
        TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.u2RequestNum =
            (UINT2) pRgGroup->u4MessageId;
    }

    if (TMO_SLL_Count (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.DataList)
        == 0 &&
        TMO_SLL_Count (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo.u.PwData.RequestList)
        == 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 8: No data to send\n");
        return L2VPN_SUCCESS;
    }

    L2vpnRedundancyIccpPwDebug (&TmpL2VpnEvtInfo.unEvtInfo.IccpInfo);

    if (L2VpnLdpPostEvent (&TmpL2VpnEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 9\n");
        return L2VPN_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 10\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyIccpPwDebug                                */
/* Description   : This routine displays the ICCP PW RG DATA message         */
/* Input(s)      : pLdpIccpEvtInfo - pointer contains ICCP message info      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyIccpPwDebug (tL2VpnLdpIccpEvtInfo * pLdpIccpEvtInfo)
{
    VOID               *pSllNode = NULL;
    tPwRedDataEvtPwFec *pIccpPwFec = NULL;
    tPwRedDataEvtPwData *pIccpPwData = NULL;
    UINT4               u4PwId = L2VPN_ZERO;
    UINT4               u4GroupId = L2VPN_ZERO;

    if (pLdpIccpEvtInfo == NULL ||
        pLdpIccpEvtInfo->u2MsgType != L2VPN_LDP_ICCP_RG_DATA_MSG)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
               "=================================================\n");

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                "MESSAGE_ID     : %u\n", pLdpIccpEvtInfo->u4MessageId);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                "RESPONSE_TO    : %u\n",
                pLdpIccpEvtInfo->u.PwData.u2ResponseNum);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                "RG_ID          : %u\n", pLdpIccpEvtInfo->u4GroupId);
#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG,
                "PEER_ADDRESS IPv4-  : %02X%02X%02X%02X\n"
                "IPv6 -%s ",
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[0],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[1],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[2],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[3],
                Ip6PrintAddr(&(pLdpIccpEvtInfo->PeerAddr.Ip6Addr)));
#else
    L2VPN_DBG4 (L2VPN_DBG_LVL_DBG_FLAG,
                "PEER_ADDRESS   : %02X%02X%02X%02X\n",
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[0],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[1],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[2],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[3]);
#endif
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTITY         : %08X:0\n",
                pLdpIccpEvtInfo->u4LocalLdpEntityId);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTITY_INDEX   : %u\n",
                pLdpIccpEvtInfo->u4LocalLdpEntityIndex);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                "PEER_ENTITY    : %08X:0\n", pLdpIccpEvtInfo->u4PeerLdpId);

    if (TMO_SLL_Count (&pLdpIccpEvtInfo->u.PwData.DataList) > 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "-[PW-DATA]---------------------------------------\n");
    }
    TMO_SLL_Scan (&pLdpIccpEvtInfo->u.PwData.DataList, pSllNode,
                  tPwRedDataEvtPwData *)
    {
        pIccpPwData = CONTAINER_OF (pSllNode, tPwRedDataEvtPwData, ListNode);

        L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                    "ROID           : %02X%02X%02X%02X _ %02X%02X%02X%02X\n",
                    pIccpPwData->Data.RoId.au1NodeId[0],
                    pIccpPwData->Data.RoId.au1NodeId[1],
                    pIccpPwData->Data.RoId.au1NodeId[2],
                    pIccpPwData->Data.RoId.au1NodeId[3],
                    pIccpPwData->Data.RoId.au1PwIndex[0],
                    pIccpPwData->Data.RoId.au1PwIndex[1],
                    pIccpPwData->Data.RoId.au1PwIndex[2],
                    pIccpPwData->Data.RoId.au1PwIndex[3],
                    pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_PURGED ? ", PURGED"
                    : "");

        if (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_CONFIG)
        {
            if (pIccpPwData->Data.Fec.u1Type == L2VPN_FEC_PWID_TYPE)
            {
                MEMCPY (&u4GroupId, pIccpPwData->Data.Fec.u.Fec128.au1GroupId,
                        sizeof (UINT4));
                MEMCPY (&u4PwId, pIccpPwData->Data.Fec.u.Fec128.au1PwId,
                        sizeof (UINT4));
                L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                            "FEC            : {%u, %u}::%02X%02X%02X%02X\n",
                            u4PwId, u4GroupId,
                            pIccpPwData->Data.Fec.u.Fec128.au1PeerId[0],
                            pIccpPwData->Data.Fec.u.Fec128.au1PeerId[1],
                            pIccpPwData->Data.Fec.u.Fec128.au1PeerId[2],
                            pIccpPwData->Data.Fec.u.Fec128.au1PeerId[3]);
            }
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                        "PRIORITY       : %#X\n", pIccpPwData->Data.u2Priority);
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                        "SERVICE_NAME   : %s\n", pIccpPwData->Data.Name);
        }

        if (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_STATE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                        "LOCAL_STATUS   : %#010X\n",
                        pIccpPwData->Data.u4LocalStatus);
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "REMOTE_STATUS  : %#010X\n",
                        pIccpPwData->Data.u4RemoteStatus);
        }
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "-------------------------------------------------\n");
    }

    if (pLdpIccpEvtInfo->u.PwData.u1RequestType != L2VPNRED_ICCP_REQ_NONE ||
        TMO_SLL_Count (&pLdpIccpEvtInfo->u.PwData.RequestList) > 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "-[REQUEST]---------------------------------------\n");
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                    "REQUEST_NUMBER : %u\n",
                    pLdpIccpEvtInfo->u.PwData.u2RequestNum);
        L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG,
                    "REQUEST_TYPE   :%s%s%s\n",
                    (pLdpIccpEvtInfo->u.PwData.u1RequestType &
                     L2VPNRED_ICCP_REQ_CONFIG ? " CONFIG" : ""),
                    (pLdpIccpEvtInfo->u.PwData.u1RequestType &
                     L2VPNRED_ICCP_REQ_STATE ? " STATUS" : ""),
                    (pLdpIccpEvtInfo->u.PwData.u1RequestType &
                     L2VPNRED_ICCP_REQ_ALL ? " ALL" : ""));
    }
    TMO_SLL_Scan (&pLdpIccpEvtInfo->u.PwData.RequestList, pSllNode,
                  tPwRedDataEvtPwFec *)
    {
        pIccpPwFec = CONTAINER_OF (pSllNode, tPwRedDataEvtPwFec, ListNode);

        if (pLdpIccpEvtInfo->u.PwData.u1RequestType &
            L2VPNRED_ICCP_REQ_PW_SERVICE_NAME)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                        "SERVICE_NAME   : %s\n", pIccpPwFec->u.Name);
        }
        else if (!(pLdpIccpEvtInfo->u.PwData.u1RequestType &
                   L2VPNRED_ICCP_REQ_PW_SERVICE_NAME) &&
                 pIccpPwFec->u.Fec.u1Type == L2VPN_FEC_PWID_TYPE)
        {
            MEMCPY (&u4GroupId, pIccpPwFec->u.Fec.u.Fec128.au1GroupId,
                    sizeof (UINT4));
            MEMCPY (&u4PwId, pIccpPwFec->u.Fec.u.Fec128.au1PwId,
                    sizeof (UINT4));
            L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                        "FEC            : {%u, %u}::%02X%02X%02X%02X\n",
                        u4PwId, u4GroupId,
                        pIccpPwFec->u.Fec.u.Fec128.au1PeerId[0],
                        pIccpPwFec->u.Fec.u.Fec128.au1PeerId[1],
                        pIccpPwFec->u.Fec.u.Fec128.au1PeerId[2],
                        pIccpPwFec->u.Fec.u.Fec128.au1PeerId[3]);

        }

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "-------------------------------------------------\n");
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
               "=================================================\n");

    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeIccpPurge                              */
/* Description   : This routine removes all ICCP dynamic info. related to a  */
/*                 sibling node from the internal DB                         */
/* Input(s)      : pRgNode - pointer to the redundancy node entry            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyNodeIccpPurge (tL2vpnRedundancyNodeEntry * pRgNode)
{
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pIccpPwRb = NULL;

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = pRgNode->u4RgIndex;

    while ((pIccpPwRb =
            RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                           (tRBElem *) pTmpIccpPw, NULL)) != NULL)
    {
        if (pIccpPwRb->u4RgIndex != pRgNode->u4RgIndex)
        {
            break;
        }

        MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
        pTmpIccpPw->u4RgIndex = pIccpPwRb->u4RgIndex;
        pTmpIccpPw->RouterId = pIccpPwRb->RouterId;
        pTmpIccpPw->Fec = pIccpPwRb->Fec;

        L2VpnRedundancyIccpPwDelete (pIccpPwRb);
    }
    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeStateUpdate                            */
/* Description   : This routine handles the state change of a sibling PE node*/
/* Input(s)      : pRgNode - pointer to the redundancy node entry            */
/*                 u1Event - Event type                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyNodeStateUpdate (tL2vpnRedundancyNodeEntry * pRgNode,
                                UINT1 u1Event)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    INT4                i4RetStatus = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

    L2VPN_DBG7 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "NODE[%u, %u, %02X%02X%02X%02X], u1Event(%u)\n",
                pRgNode->u4RgIndex, pRgNode->u1AddrType,
                pRgNode->Addr.au1Ipv4Addr[0], pRgNode->Addr.au1Ipv4Addr[1],
                pRgNode->Addr.au1Ipv4Addr[2], pRgNode->Addr.au1Ipv4Addr[3],
                u1Event);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgNode->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (u1Event == L2VPNRED_EVENT_NODE_ACTIVATE)
    {
        if ((pRgGroup->u1RowStatus == ACTIVE) &&
            (pRgNode->u1RowStatus == ACTIVE) && (pRgNode->u1AddrType == MPLS_IPV4_ADDR_TYPE))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "NODE ACTIVATE\n");

            i4RetStatus = L2VpnCheckSsnFromPeerSsnList (pRgNode, TRUE);

            if (i4RetStatus == L2VPN_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                           "T-LDP session not established for the "
                           "ICCP neighbor\n");
                return L2VPN_SUCCESS;
            }
        }
    }

    if ((u1Event == L2VPNRED_EVENT_NODE_ACTIVATE) ||
        (u1Event == L2VPNRED_EVENT_NODE_SESSION_UP))
    {
        if (pRgNode->pSession == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
            return L2VPN_FAILURE;
        }
        if (pRgNode->u1Status & L2VPNRED_NODE_STATUS_CONNECTED)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4\n");
            return L2VPN_SUCCESS;
        }

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "NODE UP\n");

        if ((pRgGroup->u1RowStatus == ACTIVE)
            && (pRgNode->u1RowStatus == ACTIVE)
            && (pRgNode->pSession->u4Status == L2VPN_SESSION_UP))
        {
            pRgNode->u1Status |= L2VPNRED_NODE_STATUS_CONNECTED;
            L2vpnRedundancyIccpPwDataTx (pRgNode, NULL,
                                         LDPICCP_REQUEST_UNSOLICITED,
                                         L2VPNRED_ICCP_DATA_CONFIG,
                                         NULL,
                                         L2VPNRED_ICCP_REQ_CONFIG |
                                         L2VPNRED_ICCP_REQ_STATE |
                                         L2VPNRED_ICCP_REQ_ALL);

            pRgNode->u1Status |= L2VPNRED_NODE_STATUS_LOCAL_SYNC;
        }
    }

    if ((u1Event == L2VPNRED_EVENT_NODE_SESSION_DOWN) ||
        (u1Event == L2VPNRED_EVENT_NODE_DEACTIVATE))
    {
        if (pRgNode->pSession == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 5\n");
            return L2VPN_SUCCESS;
        }

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "NODE DOWN\n");

        if ((pRgNode->u1RowStatus == ACTIVE) &&
            (pRgNode->u1Status & L2VPNRED_NODE_STATUS_CONNECTED))
        {
            L2vpnRedundancyIccpPwDataTx (pRgNode, NULL,
                                         LDPICCP_REQUEST_UNSOLICITED,
                                         L2VPNRED_ICCP_DATA_CONFIG |
                                         L2VPNRED_ICCP_DATA_PURGED,
                                         NULL, L2VPNRED_ICCP_REQ_NONE);

            L2vpnRedundancyNodeIccpPurge (pRgNode);

            pRgNode->u1Status = L2VPNRED_NODE_STATUS_DISCONNECTED;
        }
    }

    if (u1Event == L2VPNRED_EVENT_NODE_DEACTIVATE)
    {
        if (pRgNode->pSession == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 6\n");
            return L2VPN_SUCCESS;
        }

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "NODE DE-ACTIVATE\n");

        if ((pRgNode->u1RowStatus == ACTIVE) && (pRgNode->u1AddrType == IPV4))
        {
#ifdef MPLS_IPV6_WANTED
            if ( MPLS_IPV6_ADDR_TYPE == pRgNode->u1AddrType )
            {
                MEMCPY( (UINT1 *)GenU4Addr.Addr.Ip6Addr.u1_addr,
                        (UINT1 *)&(pRgNode->Addr),
                        IPV6_ADDR_LENGTH );
            }
            else if ( MPLS_IPV4_ADDR_TYPE == pRgNode->u1AddrType )
#endif
            {
                GenU4Addr.Addr.u4Addr = *(UINT4 *) (VOID *) &pRgNode->Addr;
            }
            GenU4Addr.u2AddrType = (UINT2) pRgNode->u1AddrType;

            L2VpnUpdatePeerSsnList (GenU4Addr,
                                    L2VPN_ICCP_DELETE_IN_SESSION, pRgNode);
        }
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 7\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwStateUpdate                              */
/* Description   : This routine handles the status change for a local PW     */
/* Input(s)      : pRgPw - pointer to the redundancy Pw entry                */
/*                 u1Event - Event type                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyPwStateUpdate (tL2vpnRedundancyPwEntry * pRgPw, UINT1 u1Event)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;
    tPwVcActivePeerSsnEntry *pSession = NULL;
    INT4                i4RetStatus = 0;
    UINT4               u4Minutes = 60;
    BOOL1               b1DataUpdated = FALSE;
    BOOL1               b1ReNegotiate = FALSE;
    BOOL1               b1LocalStandby = FALSE;
    BOOL1               b1LocalSwitchover = FALSE;

    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "pRgPw{.u4RgIndex(%u), .pPwVcEntry{.u4Index(%u), "
                ".u1LocalStatus(%#X), "
                ".u1RemoteStatus(%#X)}}, u1Event(%u)\n",
                pRgPw->u4RgIndex, L2VPN_PWVC_INDEX (pRgPw->pPwVcEntry),
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry),
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry), u1Event);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }
    L2VpnGetPeerSession ((UINT1)pRgPw->pPwVcEntry->i4PeerAddrType, 
                         &L2VPN_PWVC_PEER_ADDR (pRgPw->pPwVcEntry),
                         &pSession);

    pRgIccpPw = pRgPw->pIccpPwEntry;
    /*  If Lockout protection is enabled Switching shall not occur.The following Scenarios shall ignore switching if Lockout is enabled
     1) NNI port over which PW is made is shutdown
     2) Forceful switchover is done
     3) AC port is shut which is connected by means of D-LAG
     If Lockout protection is enabled and Switch received event of PW activation     or deactivation normal PW Redundancy shall work without switching**/
    if ((pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_DISABLE) || 
        ((pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE) && 
        ((u1Event == L2VPNRED_EVENT_PW_ACTIVATE) ||  
        ((u1Event == L2VPNRED_EVENT_PW_DEACTIVATE)))) || 
        ((pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE) && 
        ((u1Event == L2VPNRED_EVENT_PW_STATE_CHANGE) && 
        (pRgIccpPw != NULL) && 
        (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry))) && 
        (L2VPN_PWVC_STATUS_IS_UP(L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry))))))
   
  {    /* PW Mapping */
    if ((u1Event == L2VPNRED_EVENT_PW_ACTIVATE) &&
        (pRgPw->u1RowStatus == ACTIVE) &&
        (L2VPN_PWVC_ROW_STATUS (pRgPw->pPwVcEntry) == ACTIVE) &&
        (pSession != NULL) && (pSession->u4Status == L2VPN_SESSION_UP))
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ICCP PW CREATE. "
                    "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                    pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

        if (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS
                                     (pRgPw->pPwVcEntry)) &&
            L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_REMOTE_STATUS
                                     (pRgPw->pPwVcEntry)))
        {
            b1DataUpdated = TRUE;
        }

        if (pRgIccpPw != NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
        }
        else
        {
            pRgIccpPw = L2VpnRedundancyIccpPwCreate (pRgPw, NULL, NULL);
            if (pRgIccpPw == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
                return L2VPN_FAILURE;
            }

            /*if (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry)) &&
               L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry)))
               {
               b1DataUpdated = TRUE;
               } */
            else
            {
                L2vpnRedundancyIccpNodeNotify (pRgPw->u4RgIndex, pRgIccpPw,
                                               L2VPNRED_ICCP_DATA_CONFIG |
                                               L2VPNRED_ICCP_DATA_STATE,
                                               L2VPNRED_ICCP_REQ_STATE);
            }
           if(L2VPN_PWVC_MODE (pRgPw->pPwVcEntry) == L2VPN_VPWS)
           {
            if ((pRgGroup->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_MASTERSLAVE) &&
                (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
            {
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                    L2VPN_PWVC_STATUS_STANDBY;
                pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
                pRgIccpPw->u4LocalStatus =
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
                pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_REMOTE_AWAITED;
                pRgGroup->u2NumPwAwaiting++;
                pRgGroup->u1Status = L2VPNRED_STATUS_FORWARDING_NEGOTIATON;
                L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
            }
           }
        }
    }

    /* Manual switchover */
    if (u1Event == L2VPNRED_EVENT_PW_SWITCHOVER_LOCAL)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW LOCAL SWITCHOVER. "
                    "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                    pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

        i4RetStatus = L2vpnRedundancySwitchover (pRgPw);

        L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4. "
                    "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u), "
                    " .pNegForwardingPw(%p), "
                    ".pNegSwitchoverPw(%p), .pOperActivePw(%p)}\n",
                    pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting,
                    pRgGroup->pNegForwardingPw,
                    pRgGroup->pNegSwitchoverPw, pRgGroup->pOperActivePw);

        L2VpnPwRedSnmpNotifyForceActive (pRgPw->u4RgIndex,
                                         pRgGroup->pOperActivePw,
                                         pRgGroup->u4AdminActivePw);
        return i4RetStatus;
    }

    /* PW Forwarding negotiation awaiting confirmation */
    if ((u1Event == L2VPNRED_EVENT_PW_STATE_CHANGE) &&
        (pRgIccpPw != NULL) &&
        (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS
                                  (pRgPw->pPwVcEntry))) &&
        (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_REMOTE_STATUS
                                  (pRgPw->pPwVcEntry))) &&
        ((L2VPN_PWVC_STATUS_IS_ACTIVE
          (L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry)))
         != (L2VPN_PWVC_STATUS_IS_ACTIVE (pRgIccpPw->u4RemoteStatus))) &&
        (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_REMOTE_AWAITED))
    {
        L2VpPwRedActionforAwaitingPw (pRgIccpPw, pRgPw, pRgGroup,
                                      &b1DataUpdated);
    }

    if ((u1Event == L2VPNRED_EVENT_PW_STATE_CHANGE) &&
        (pRgIccpPw != NULL) &&
        (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry)))
        &&
        (L2VPN_PWVC_STATUS_IS_SWITCHOVER
         (L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry)))
        && (!(pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER)))
    {
        L2VpPwRedHandleRemoteSWOver (pRgIccpPw, pRgPw, pRgGroup,
                                     &b1DataUpdated);
    }

    if ((u1Event == L2VPNRED_EVENT_PW_STATE_CHANGE) &&
        (pRgIccpPw != NULL) &&
        (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry)))
        &&
        (L2VPN_PWVC_STATUS_IS_UP
         (L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry))))
    {   /*   To avoid switching in case of AC fault with D-LAG when lockout is enabled. Switching shall not occur if the AC port is shut and Lockout is enabled and traffic shall not flow from alternate path*/
        if ((pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE) && 
            (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry)) !=              L2VPN_PWVC_STATUS_IS_ACTIVE (pRgIccpPw->u4RemoteStatus)))
        { return L2VPN_SUCCESS;
        }
        
        else if (L2VPN_PWVC_STATUS_IS_SWITCHOVER (L2VPN_PWVC_REMOTE_STATUS
                                             (pRgPw->pPwVcEntry)) &&
            (!(pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER)))
        {
            L2VpPwRedHandleRemoteSWOver (pRgIccpPw, pRgPw, pRgGroup,
                                         &b1DataUpdated);
        }
        /* Condition to send trigger notification to peer when there is a change 
         * in the pseudowire local status */
        else if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                              (pRgPw->pPwVcEntry)) !=
                 L2VPN_PWVC_STATUS_IS_ACTIVE (pRgIccpPw->u4LocalStatus))
        {
            L2VpPwRedHandleChngInLocalStatus (pRgIccpPw, pRgPw);
        }
        /* Condition to check if the remote status has changed */
        else if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                              (pRgPw->pPwVcEntry)) !=
                 L2VPN_PWVC_STATUS_IS_ACTIVE (pRgIccpPw->u4RemoteStatus))
        {
            L2VpPwRedHandleChngInRemoteStatus (pRgIccpPw, pRgPw, pRgGroup,
                                               &b1ReNegotiate);
        }
        /* If it has sent the Switchover bit to the Peer and
         * peer has responded with clearing the bit the do
         * the following
         */
        else if ((L2VPN_PWVC_STATUS_IS_SWITCHOVER (L2VPN_PWVC_LOCAL_STATUS
                                                   (pRgPw->pPwVcEntry))) &&
                  (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_REMOTE_STATUS
                                            (pRgPw->pPwVcEntry))))
        {
            if (((pRgGroup->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_MASTERSLAVE) &&
                (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER)) ||
                (pRgGroup->u1ContentionResolutionMethod == 
                 L2VPNRED_CONTENTION_INDEPENDENT))
            {
                if ((pRgGroup->pOperActivePw == NULL) ||
                    (pRgGroup->pOperActivePw != pRgIccpPw))
                {
                    pRgGroup->pOperActivePw = pRgIccpPw;
                    pRgGroup->u1Status = L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
                    pRgIccpPw->u4RemoteStatus = 
                        L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                        (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                        (~L2VPN_PWVC_STATUS_STANDBY);
                    pRgIccpPw->u4LocalStatus =
                        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
                    L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
                    L2VpnPwRedVcHwModify (pRgPw->pPwVcEntry, pRgGroup, NULL);
                    return L2VPN_SUCCESS;
                }
            }

        }
        /* PW Remote forwarding change */
        else if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                              (pRgPw->pPwVcEntry)) !=
                 L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                              (pRgPw->pPwVcEntry)))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW FORWARDING CHANGE. "
                        "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                        pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

            /* When remote and local status are not same how master node has to react */
            if ((pRgGroup->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_MASTERSLAVE) &&
                (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER))
            {
                if (!L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                                  (pRgPw->pPwVcEntry)))
                {
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                        (~L2VPN_PWVC_STATUS_STANDBY);
                    pRgIccpPw->u4RemoteStatus =
                        L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                    pRgIccpPw->u2Status &=
                        (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_STANDBY);
                    pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_REMOTE_FORWARD;
                    if (pRgGroup->pOperActivePw == pRgIccpPw)
                    {
                        pRgGroup->pOperActivePw = NULL;
                    }
                    else
                    {
                        b1ReNegotiate = TRUE;
                    }
                    if ((pRgGroup->b1ReversionEnable == L2VPN_PWRED_REVERT_ENABLE)
                        && (pRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY)
                        && (pRgGroup->
                            u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)
                        && (pRgGroup->pOperActivePw != NULL)
                        && (L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw)->
                            u1Preference == L2VPNRED_PW_PREFERENCE_SECONDARY)
                        && (pRgGroup->u2WaitToRestoreTime > 0))
                    {
                        pRgGroup->u1Status |= L2VPNRED_STATUS_WAITING_TO_RESTORE;
                        if (TmrStartTimer (gL2vpnRgGlobals.NegTimerList,
                                           &pRgGroup->NegTimer,
                                           pRgGroup->u2WaitToRestoreTime * u4Minutes *
                                           SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
                            != TMR_SUCCESS)
                        {
                            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "INTMD-EXIT 5. "
                                       "Start of PW-RED WTR timer failed\n");
                            return L2VPN_FAILURE;
                        }
                        b1ReNegotiate = FALSE;
                        b1LocalSwitchover = FALSE;
                        b1LocalStandby = FALSE;
                    }

                }
                L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
            }
            /* When remote and local status are not same how slave node has to 
             * react */
            else if ((pRgGroup->u1ContentionResolutionMethod ==
                      L2VPNRED_CONTENTION_MASTERSLAVE) &&
                     (pRgGroup->u1MasterSlaveMode ==
                      L2VPNRED_MASTERSLAVE_SLAVE))
            {
                if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                                 (pRgPw->pPwVcEntry)))
                {
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                        (~L2VPN_PWVC_STATUS_STANDBY);
                    pRgIccpPw->u4LocalStatus =
                        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
                    pRgIccpPw->u2Status &=
                        (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
                    pRgIccpPw->u4RemoteStatus =
                        L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                    pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_REMOTE_FORWARD;
                    if (pRgGroup->pOperActivePw != pRgIccpPw)
                    {
                        pRgGroup->pOperActivePw = pRgIccpPw;
                    }
                }
                else if (!L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                                       (pRgPw->pPwVcEntry)))
                {
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                        L2VPN_PWVC_STATUS_STANDBY;
                    pRgIccpPw->u4LocalStatus =
                        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
                    pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
                    pRgIccpPw->u4RemoteStatus =
                        L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                    pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_REMOTE_STANDBY;
                    if (pRgGroup->pOperActivePw == pRgIccpPw)
                    {
                        pRgGroup->pOperActivePw = NULL;
                    }
                }

                pRgIccpPw->u2Status &=
                    (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);

                if (pRgGroup->u2NumPwAwaiting)
                {
                    pRgGroup->u2NumPwAwaiting--;
                }

                L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
            }
            /* When remote and local status are not same how an independent node has to react */
            else
            {
                pRgIccpPw->u4LocalStatus =
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
                pRgIccpPw->u4RemoteStatus =
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            }
        }
        /* 1.  Code to see if the remote and local status are same 
           2.  If the OperActivePw is not updated it will update with valid 
           PwINDEX
           3.  Also if the pseudowire for which the notification is received has 
           issued switchover request that bit is cleared */
        else if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                              (pRgPw->pPwVcEntry)) ==
                 L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                              (pRgPw->pPwVcEntry)))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW FORWARDING CHANGE. "
                        "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)} \n",
                        pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

            if (((pRgGroup->pOperActivePw == NULL) &&
                 (pRgGroup->u1ContentionResolutionMethod ==
                  L2VPNRED_CONTENTION_MASTERSLAVE) &&
                 (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
                ||
                ((pRgGroup->pOperActivePw != NULL) &&
                 (pRgGroup->pOperActivePw != pRgIccpPw) &&
                 (L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &
                  L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST)))
            {
                pRgGroup->pOperActivePw = pRgIccpPw;
                pRgGroup->u1Status = L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
                pRgIccpPw->u4RemoteStatus =
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_LOCAL_FORWARD;
            }

            pRgGroup->u1Status &=
                (UINT1) (~L2VPNRED_STATUS_SWITCHOVER_NEGOTIATON);
            pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_REMOTE_FORWARD;
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);

            if (pRgGroup->u2NumPwAwaiting)
            {
                pRgGroup->u2NumPwAwaiting--;
            }

            if (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED))
            {
                L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry,
                                                     pRgGroup);
                pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;
            }
        }
    }

    /* Local PW UP/DOWN Handling */
    if (((u1Event == L2VPNRED_EVENT_PW_STATE_CHANGE) && (pRgIccpPw != NULL) &&
         (L2VPN_PWVC_STATUS_IS_UP (pRgIccpPw->u4LocalStatus) !=
          (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS
                                    (pRgPw->pPwVcEntry)))))
        ||
        ((u1Event == L2VPNRED_EVENT_PW_ACTIVATE) && (pRgIccpPw != NULL)
         && b1DataUpdated)
        || ((u1Event == L2VPNRED_EVENT_PW_DEACTIVATE) && (pRgIccpPw != NULL)))
    {
        if ((L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS
                        (pRgPw->pPwVcEntry))) &&
                (u1Event != L2VPNRED_EVENT_PW_DEACTIVATE))
        {
            if (L2VPN_FAILURE == (L2VpPwRedHandlePwUpEvent (pRgIccpPw, pRgPw,
                            pRgGroup,
                            &b1ReNegotiate,
                            &b1LocalStandby,
                            &b1LocalSwitchover)))
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                        "L2VpPwRedHandlePwUpEvent Failed"
                        "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                        pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);
                return L2VPN_FAILURE;
            }

        }
        else
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW DOWN. "
                    "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                    pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);
            if (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_STANDBY)
            {
                pRgIccpPw->u2Status &=
                    (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &=
                    (UINT1) (~L2VPN_PWVC_STATUS_STANDBY);
                L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
            }
            else
            { /* To prevenT switching when redundancy group is deactivated */
                if (pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE)
                { 
                    return L2VPN_SUCCESS;
                }  
                else
                {
                    pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_NONE;
                }
            }
            if (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED))
            {  /* To preven switching when redundancy group is deactivated */
                if (pRgGroup->u1LockoutProtect == L2VPNRED_LOCKOUT_ENABLE)
                {
                    return L2VPN_SUCCESS;
                }
                else
                {
                    L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry,
                                                         pRgGroup);
                    pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;
                }  
            }
            if ((!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)) &&
                    (pRgGroup->pNegForwardingPw != NULL) &&
                    (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pNegForwardingPw)
                     == L2VPN_RB_GREATER))
            {
                b1ReNegotiate = TRUE;
            }

            if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
                    (pRgGroup->pOperActivePw != NULL) &&
                    (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pOperActivePw)
                     != L2VPN_RB_LESSER))
            {
                b1ReNegotiate = TRUE;
            }

            if (pRgGroup->u2NumUpPw)
            {
                pRgGroup->u2NumUpPw--;
            }
            
            pRgIccpPw->u4LocalStatus =
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            /* The pseudowire which is currently going down is the primary pseudowire
               If the reversion is disabled for this group. Then set the b1ReElectionNotReqd
               flag set. So that when the b1ReElectionNotReqd is set when the pseudowire
               comes up it will not become ACTIVE */
            if ((b1ReNegotiate & TRUE) &&
                    (pRgGroup->b1ReversionEnable & L2VPN_PWRED_REVERT_DISABLE) &&
                    (pRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY))
            {
                pRgGroup->b1ReElectionNotReqd = TRUE;
            }
        }
    }
    /* Remote PW UP/DOWN Handling */
    if ((u1Event == L2VPNRED_EVENT_PW_STATE_CHANGE) && (pRgIccpPw != NULL) &&
        ((L2VPN_PWVC_STATUS_IS_UP (pRgIccpPw->u4RemoteStatus) !=
          (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_REMOTE_STATUS
                                    (pRgPw->pPwVcEntry)))) ||
         (pRgIccpPw->u4RemoteStatus != (L2VPN_PWVC_REMOTE_STATUS
                                        (pRgPw->pPwVcEntry)))))
    {
        /* The remote pseudowire which went down has come up again hence check if 
           re-election has to be done or not */

        if (L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_REMOTE_STATUS
                                     (pRgPw->pPwVcEntry)))
        {
            if ((pRgGroup->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_INDEPENDENT) ||
                (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER))
            {
                pRgIccpPw->u4RemoteStatus =
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                if (!L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                                  (pRgPw->pPwVcEntry)))
                {
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                        (~L2VPN_PWVC_STATUS_STANDBY);
                    pRgIccpPw->u4RemoteStatus =
                        L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
                }
                pRgIccpPw->u4LocalStatus =
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);

                if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)
                    &&
                    (pRgGroup->pOperActivePw != NULL) &&
                    (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pOperActivePw)
                     == L2VPN_RB_GREATER))
                {
                    b1LocalSwitchover = TRUE;
                }
                else if ((!(pRgGroup->u1Status &
                            L2VPNRED_STATUS_NEGOTIATION_SUCCESS)) &&
                         (pRgGroup->pOperActivePw == NULL))
                {
                    b1ReNegotiate = TRUE;
                }
                else if (pRgGroup->pOperActivePw != pRgIccpPw)
                {
                    b1ReNegotiate = TRUE;
                }
                if ((pRgGroup->b1ReversionEnable == L2VPN_PWRED_REVERT_ENABLE)
                    && (pRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY)
                    && (pRgGroup->
                        u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)
                    && (pRgGroup->pOperActivePw != NULL)
                    && (L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw)->
                        u1Preference == L2VPNRED_PW_PREFERENCE_SECONDARY)
                    && (pRgGroup->u2WaitToRestoreTime > 0))
                {
                    pRgGroup->u1Status |= L2VPNRED_STATUS_WAITING_TO_RESTORE;
                    if (TmrStartTimer (gL2vpnRgGlobals.NegTimerList,
                                       &pRgGroup->NegTimer,
                                       pRgGroup->u2WaitToRestoreTime * u4Minutes *
                                       SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
                        != TMR_SUCCESS)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "INTMD-EXIT 5. "
                                   "Start of PW-RED WTR timer failed\n");
                        return L2VPN_FAILURE;
                    }
                    b1ReNegotiate = FALSE;
                    b1LocalSwitchover = FALSE;
                    b1LocalStandby = FALSE;
                }

                if ((b1LocalSwitchover) &&
                    (pRgGroup->b1ReElectionNotReqd & TRUE))
                {
                    b1LocalStandby = TRUE;
                    b1LocalSwitchover = FALSE;
                    pRgGroup->b1ReElectionNotReqd = FALSE;
                }
            }
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        }
        else
        {
            if ((pRgGroup->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_INDEPENDENT) ||
                (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER))
            {
                pRgIccpPw->u4LocalStatus =
                    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
                if (pRgGroup->pOperActivePw == pRgIccpPw)
                {
                    b1ReNegotiate = TRUE;
                }
                /* The pseudowire which is currently going down is the primary pseudowire
                   If the reversion is disabled for this group. Then set the b1ReElectionNotReqd
                   flag set. So that when the b1ReElectionNotReqd is set when the pseudowire
                   comes up it will not become ACTIVE */
                if ((b1ReNegotiate & TRUE) &&
                    (pRgGroup->b1ReversionEnable &
                     L2VPN_PWRED_REVERT_DISABLE) &&
                    (pRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY))
                {
                    pRgGroup->b1ReElectionNotReqd = TRUE;
                }
            }
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        }
    }

    /* PW Un-mapping/de-activate or PW-DOWN */
    if (u1Event == L2VPNRED_EVENT_PW_DEACTIVATE)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ICCP PW DELETE"
                    "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                    pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

        if (pRgIccpPw == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 5\n");
            return L2VPN_SUCCESS;
        }

        L2vpnRedundancyIccpNodeNotify (pRgPw->u4RgIndex, pRgIccpPw,
                                       L2VPNRED_ICCP_DATA_CONFIG |
                                       L2VPNRED_ICCP_DATA_PURGED,
                                       L2VPNRED_ICCP_REQ_STATE);

        if (L2VPN_PWVC_STATUS_IS_UP (pRgIccpPw->u4LocalStatus) &&
            (pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_STANDBY) &&
            (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_REMOTE_AWAITED)))
        {
            pRgGroup->u2NumUpPw--;
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_STANDBY);
            L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
        }
        else
        {
            pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_NONE;
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_STANDBY);
        }

        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);

        L2VpnRedundancyIccpPwDelete (pRgIccpPw);
        pRgGroup->u2NumUpPw = pRgGroup->u2NumIccpPw;

        if (L2VPN_PWVC_OPER_STATUS (pRgPw->pPwVcEntry) != L2VPN_PWVC_DOWN)
        {
            i4RetStatus = L2VpnUpdatePwVcOperStatus (pRgPw->pPwVcEntry,
                    L2VPN_PWVC_DOWN);
            if (i4RetStatus == L2VPN_FAILURE)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "PwOperStatus updation failed: PwLocalStatus "
                        "%x PwRemoteStatus %x\r\n",
                        pRgPw->pPwVcEntry->u1LocalStatus,
                        pRgPw->pPwVcEntry->u1RemoteStatus);
            }
        }

        if (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED))
        {
            L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry, pRgGroup);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;
        }

        if (pRgIccpPw == pRgGroup->pNegForwardingPw)
        {
            pRgGroup->pNegForwardingPw = NULL;
        }
        else if (pRgIccpPw == pRgGroup->pOperActivePw)
        {
            pRgGroup->pOperActivePw = NULL;
            pRgGroup->u1Status = L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE;
        }
        /* As the pseudowire is going not_in_service or its getting deleted 
           we need to go for reelection even if the reversion is disabled */
        if (pRgGroup->b1ReElectionNotReqd == TRUE)
        {
            pRgGroup->b1ReElectionNotReqd = FALSE;
        }

        pRgIccpPw = NULL;
        b1DataUpdated = TRUE;
    }
  }
else
 {  return L2VPN_SUCCESS;
 }
    if (b1LocalSwitchover)
    {
        L2vpnRedundancySwitchover (pRgPw);
    }
    else if (b1ReNegotiate)
    {
        L2vpnRedundancyNegotiate (pRgPw->u4RgIndex, FALSE);
    }
    else if (b1LocalStandby)
    {
        L2vpnRedundancyStandby (pRgPw);
    }
    else if (b1DataUpdated)
    {
        L2vpnRedundancyIccpNodeNotify (pRgPw->u4RgIndex, pRgIccpPw,
                                       L2VPNRED_ICCP_DATA_STATE,
                                       L2VPNRED_ICCP_REQ_STATE |
                                       L2VPNRED_ICCP_REQ_ALL);
    }

    L2VpnPwRedVcHwModify (pRgPw->pPwVcEntry, pRgGroup, NULL);

    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 6. "
                "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u), "
                ".pNegForwardingPw(%p), "
                ".pNegSwitchoverPw(%p), .pOperActivePw(%p)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting,
                pRgGroup->pNegForwardingPw, pRgGroup->pNegSwitchoverPw,
                pRgGroup->pOperActivePw);

    return L2VPN_SUCCESS;
 
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyIccpPwDataRx                               */
/* Description   : This routine validates the RX ICCP PW DATA message against*/
/*                 internal DB and takes appropriate action                  */
/* Input(s)      : pLdpIccpEvtInfo - pointer to the LdpIccpEvent             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyIccpPwDataRx (tL2VpnLdpIccpEvtInfo * pLdpIccpEvtInfo)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pNode = NULL;
    tPwRedDataEvtPwData *pIccpPwData = NULL;
    tL2vpnIccpPwEntry  *pIccpPw = NULL;
    tL2vpnRedundancyPwEntry *pLocalStandbyPw = NULL;
    VOID               *pSllNode = NULL;
    UINT4               u4RouterId = 0;
    UINT1               u1RequestType = 0;
    UINT1               u1ResponseType = 0;
    BOOL1               b1PwForwardingSync = FALSE;
    BOOL1               b1DataUpdated = FALSE;
    BOOL1               b1LocalStandby = FALSE;
    BOOL1               b1ReNegotiate = FALSE;

#ifdef MPLS_IPV6_WANTED

    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "ICCP_EVT{.RG(%u)}, NODE IPv4-{ %02X%02X%02X%02X}"
                 " IPv6 -%s \n",
                pLdpIccpEvtInfo->u4GroupId,
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[0],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[1],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[2],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[3],
                Ip6PrintAddr(&(pLdpIccpEvtInfo->PeerAddr.Ip6Addr)));

#else   
   L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "ICCP_EVT{.RG(%u)}, NODE{ %02X%02X%02X%02X}\n",
                pLdpIccpEvtInfo->u4GroupId,
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[0],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[1],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[2],
                pLdpIccpEvtInfo->PeerAddr.au1Ipv4Addr[3]);
#endif
    /* Re-Align SLLs */
    TMO_SLL_Copy_Adjust (&pLdpIccpEvtInfo->u.PwData.DataList);
    TMO_SLL_Copy_Adjust (&pLdpIccpEvtInfo->u.PwData.RequestList);

    L2vpnRedundancyIccpPwDebug (pLdpIccpEvtInfo);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pLdpIccpEvtInfo->u4GroupId);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1: RG not found\n");
        return L2VPN_FAILURE;
    }

    pNode = L2VpnGetRedundancyNodeEntryByIndex (pLdpIccpEvtInfo->u4GroupId,
                                                pLdpIccpEvtInfo->u1AddrType,
                                                &pLdpIccpEvtInfo->PeerAddr);

    if ((pNode == NULL) || (pNode->u1RowStatus != ACTIVE) ||
        (pNode->u1Status == L2VPNRED_NODE_STATUS_DISCONNECTED))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2: NODE not found\n");
        return L2VPN_FAILURE;
    }

    /* PW Data updates */
    TMO_SLL_Scan (&pLdpIccpEvtInfo->u.PwData.DataList, pSllNode,
                  tPwRedDataEvtPwData *)
    {
        pIccpPwData = CONTAINER_OF (pSllNode, tPwRedDataEvtPwData, ListNode);
        u4RouterId = OSIX_HTONL (pLdpIccpEvtInfo->u4PeerLdpId);

        if (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_CONFIG)
        {
            pIccpPw = L2VpnGetIccpPwEntryByIndex (pLdpIccpEvtInfo->u4GroupId,
                                                  (tMplsRouterId *) &
                                                  u4RouterId,
                                                  &pIccpPwData->Data.Fec);
        }
        else if (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_STATE)
        {
            pIccpPw = L2VpnGetIccpPwEntryByRgRoId (pLdpIccpEvtInfo->u4GroupId,
                                                   (tMplsRouterId *) &
                                                   u4RouterId,
                                                   &pIccpPwData->Data.RoId);
        }

        if ((pIccpPw != NULL) && (pIccpPw->b1IsLocal))
        {
            continue;
        }
        b1DataUpdated = FALSE;
        b1PwForwardingSync = FALSE;

        /* PW removal */
        if ((pIccpPw != NULL)
            && (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_PURGED))
        {
            L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                        "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                        "PURGE <%p>\n",
                        pIccpPwData->Data.RoId.au1NodeId[0],
                        pIccpPwData->Data.RoId.au1NodeId[1],
                        pIccpPwData->Data.RoId.au1NodeId[2],
                        pIccpPwData->Data.RoId.au1NodeId[3],
                        pIccpPwData->Data.RoId.au1PwIndex[0],
                        pIccpPwData->Data.RoId.au1PwIndex[1],
                        pIccpPwData->Data.RoId.au1PwIndex[2],
                        pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);

            if ((!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)) &&
                (pRgGroup->pNegForwardingPw != NULL) &&
                (L2vpnRedundancyPwCmp (pIccpPw, pRgGroup->pNegForwardingPw) ==
                 L2VPN_RB_EQUAL))
            {
                b1ReNegotiate = TRUE;
            }

            if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
                (pRgGroup->pOperActivePw != NULL) &&
                (L2vpnRedundancyPwCmp (pIccpPw, pRgGroup->pOperActivePw) ==
                 L2VPN_RB_EQUAL))
            {
                b1ReNegotiate = TRUE;
            }

            L2VpnRedundancyIccpPwDelete (pIccpPw);
            continue;
        }                        /* PW removal */

        /* PW parameter change */
        if ((pIccpPw != NULL)
            && !(pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_PURGED))
        {
            if ((pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_STATE) &&
                (pIccpPw->u4LocalStatus != pIccpPwData->Data.u4LocalStatus))
            {
                pIccpPw->u4LocalStatus = pIccpPwData->Data.u4LocalStatus;
                b1DataUpdated = TRUE;
            }

            if ((pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_STATE) &&
                (pIccpPw->u4RemoteStatus != pIccpPwData->Data.u4RemoteStatus))
            {
                pIccpPw->u4RemoteStatus = pIccpPwData->Data.u4RemoteStatus;
                b1DataUpdated = TRUE;
            }

            if ((pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_CONFIG) &&
                (pIccpPw->u2Priority != pIccpPwData->Data.u2Priority))
            {
                pIccpPw->u2Priority = pIccpPwData->Data.u2Priority;
                b1DataUpdated = TRUE;
            }

            if (b1DataUpdated)
            {
                L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                            "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                            "DATA CHANGE <%p>\n",
                            pIccpPwData->Data.RoId.au1NodeId[0],
                            pIccpPwData->Data.RoId.au1NodeId[1],
                            pIccpPwData->Data.RoId.au1NodeId[2],
                            pIccpPwData->Data.RoId.au1NodeId[3],
                            pIccpPwData->Data.RoId.au1PwIndex[0],
                            pIccpPwData->Data.RoId.au1PwIndex[1],
                            pIccpPwData->Data.RoId.au1PwIndex[2],
                            pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);
            }
        }                        /* PW parameter change */

        /* New PW configuration */
        if ((pIccpPw == NULL) &&
            !(pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_PURGED) &&
            (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_CONFIG))
        {
            L2VPN_DBG8 (L2VPN_DBG_LVL_DBG_FLAG,
                        "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                        "CREATE\n",
                        pIccpPwData->Data.RoId.au1NodeId[0],
                        pIccpPwData->Data.RoId.au1NodeId[1],
                        pIccpPwData->Data.RoId.au1NodeId[2],
                        pIccpPwData->Data.RoId.au1NodeId[3],
                        pIccpPwData->Data.RoId.au1PwIndex[0],
                        pIccpPwData->Data.RoId.au1PwIndex[1],
                        pIccpPwData->Data.RoId.au1PwIndex[2],
                        pIccpPwData->Data.RoId.au1PwIndex[3]);

            pIccpPw =
                L2VpnRedundancyIccpPwCreate (NULL, pNode, &pIccpPwData->Data);

            if ((pIccpPw == NULL) ||
                !L2VPN_PWVC_STATUS_IS_UP (pIccpPw->u4LocalStatus) ||
                !L2VPN_PWVC_STATUS_IS_UP (pIccpPw->u4RemoteStatus))
            {
                continue;
            }

            b1DataUpdated = TRUE;
        }                        /* New PW configuration */

        if (pIccpPw == NULL)
        {
            continue;
        }

        /* PW forwarding negotiation */
        if (!(pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_PURGED) &&
            (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_STATE) &&
            !L2VPN_PWVC_STATUS_IS_SWITCHOVER (pIccpPw->u4LocalStatus) &&
            !L2VPN_PWVC_STATUS_IS_SWITCHOVER (pIccpPw->u4RemoteStatus) &&
            b1DataUpdated == TRUE)
        {
            if (L2VPN_PWVC_STATUS_IS_UP (pIccpPw->u4LocalStatus) &&
                L2VPN_PWVC_STATUS_IS_UP (pIccpPw->u4RemoteStatus))
            {
                L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                            "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                            "UP <%p>\n",
                            pIccpPwData->Data.RoId.au1NodeId[0],
                            pIccpPwData->Data.RoId.au1NodeId[1],
                            pIccpPwData->Data.RoId.au1NodeId[2],
                            pIccpPwData->Data.RoId.au1NodeId[3],
                            pIccpPwData->Data.RoId.au1PwIndex[0],
                            pIccpPwData->Data.RoId.au1PwIndex[1],
                            pIccpPwData->Data.RoId.au1PwIndex[2],
                            pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);

                if (L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4LocalStatus) &&
                    L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4RemoteStatus))
                {
                    L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                                "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                                "ACTIVE <%p>\n",
                                pIccpPwData->Data.RoId.au1NodeId[0],
                                pIccpPwData->Data.RoId.au1NodeId[1],
                                pIccpPwData->Data.RoId.au1NodeId[2],
                                pIccpPwData->Data.RoId.au1NodeId[3],
                                pIccpPwData->Data.RoId.au1PwIndex[0],
                                pIccpPwData->Data.RoId.au1PwIndex[1],
                                pIccpPwData->Data.RoId.au1PwIndex[2],
                                pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);

                    if (pRgGroup->pNegForwardingPw == pIccpPw)
                    {
                        pRgGroup->pOperActivePw = pIccpPw;
                        pRgGroup->pNegForwardingPw = NULL;
                    }

                    b1PwForwardingSync = TRUE;
                }
                else if (!L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4LocalStatus)
                         &&
                         !L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4RemoteStatus))
                {
                    L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                                "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                                "STANDBY <%p>\n",
                                pIccpPwData->Data.RoId.au1NodeId[0],
                                pIccpPwData->Data.RoId.au1NodeId[1],
                                pIccpPwData->Data.RoId.au1NodeId[2],
                                pIccpPwData->Data.RoId.au1NodeId[3],
                                pIccpPwData->Data.RoId.au1PwIndex[0],
                                pIccpPwData->Data.RoId.au1PwIndex[1],
                                pIccpPwData->Data.RoId.au1PwIndex[2],
                                pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);

                    b1PwForwardingSync = TRUE;
                }
            }
            else
            {
                L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                            "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                            "DOWN <%p>\n",
                            pIccpPwData->Data.RoId.au1NodeId[0],
                            pIccpPwData->Data.RoId.au1NodeId[1],
                            pIccpPwData->Data.RoId.au1NodeId[2],
                            pIccpPwData->Data.RoId.au1NodeId[3],
                            pIccpPwData->Data.RoId.au1PwIndex[0],
                            pIccpPwData->Data.RoId.au1PwIndex[1],
                            pIccpPwData->Data.RoId.au1PwIndex[2],
                            pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);

                if (!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)
                    && (pRgGroup->pNegForwardingPw != NULL)
                    && L2vpnRedundancyPwCmp (pIccpPw,
                                             pRgGroup->pNegForwardingPw) !=
                    L2VPN_RB_LESSER)
                {
                    b1ReNegotiate = TRUE;
                }

                if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)
                    && (pRgGroup->pOperActivePw != NULL)
                    && L2vpnRedundancyPwCmp (pIccpPw,
                                             pRgGroup->pOperActivePw) !=
                    L2VPN_RB_LESSER)
                {
                    b1ReNegotiate = TRUE;

                    if (pRgGroup->pOperActivePw == pIccpPw)
                    {
                        pRgGroup->pOperActivePw = NULL;
                    }
                }
            }
        }                        /* PW forwarding negotiation */

        if (b1PwForwardingSync && (pRgGroup->pOperActivePw != NULL) &&
            (pRgGroup->u2NumPwAwaiting == 0))
        {
            TmrStopTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer);

            pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            pRgGroup->u1Status &=
                (UINT1) (~L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE);
        }

        /* PW forwarding re-selection */
        if (!(pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_PURGED) &&
            (b1DataUpdated == TRUE) &&
            (pIccpPwData->u1Type & L2VPNRED_ICCP_DATA_STATE) &&
            L2VPN_PWVC_STATUS_IS_UP (pIccpPw->u4LocalStatus) &&
            L2VPN_PWVC_STATUS_IS_UP (pIccpPw->u4RemoteStatus))
        {
            L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                        "ICCP PW(%02X%02X%02X%02X _ %02X%02X%02X%02X) "
                        "FORWARDING CHECK <%p>\n",
                        pIccpPwData->Data.RoId.au1NodeId[0],
                        pIccpPwData->Data.RoId.au1NodeId[1],
                        pIccpPwData->Data.RoId.au1NodeId[2],
                        pIccpPwData->Data.RoId.au1NodeId[3],
                        pIccpPwData->Data.RoId.au1PwIndex[0],
                        pIccpPwData->Data.RoId.au1PwIndex[1],
                        pIccpPwData->Data.RoId.au1PwIndex[2],
                        pIccpPwData->Data.RoId.au1PwIndex[3], pIccpPw);

            if (!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
                (pRgGroup->pNegForwardingPw != NULL) &&
                ((L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC
                  (pIccpPw->u4LocalStatus, pIccpPw->u4RemoteStatus) == FALSE)
                 || L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4LocalStatus))
                && L2vpnRedundancyPwCmp (pIccpPw,
                                         pRgGroup->pNegForwardingPw) ==
                L2VPN_RB_GREATER)
            {
                b1ReNegotiate = TRUE;
                b1DataUpdated = TRUE;
            }

            if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
                (pRgGroup->pOperActivePw != NULL) &&
                (pRgGroup->pOperActivePw->b1IsLocal) &&
                ((L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC
                  (pIccpPw->u4LocalStatus, pIccpPw->u4RemoteStatus) == FALSE)
                 || L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4LocalStatus))
                && (L2vpnRedundancyPwCmp (pIccpPw, pRgGroup->pOperActivePw) ==
                    L2VPN_RB_GREATER))
            {
                b1ReNegotiate = TRUE;
                b1DataUpdated = FALSE;
            }

            /* Sibling switchover */
            if (!L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4LocalStatus) &&
                L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC (pIccpPw->u4LocalStatus,
                                                      pIccpPw->u4RemoteStatus)
                && (L2VPN_PWVC_STATUS_IS_SWITCHOVER (pIccpPw->u4LocalStatus)
                    ||
                    L2VPN_PWVC_STATUS_IS_SWITCHOVER (pIccpPw->u4RemoteStatus)))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                           "ICCP PW NODE SWITCHOVER TRIGGER\n");

                pIccpPw->u2Status |= L2VPNRED_PW_STATUS_NODE_SWITCHOVER;
            }
            else if ((pRgGroup->pOperActivePw != NULL) &&
                     (pRgGroup->pOperActivePw->b1IsLocal) &&
                     (pIccpPw->u2Status & L2VPNRED_PW_STATUS_NODE_SWITCHOVER) &&
                     (L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4LocalStatus) ||
                      L2VPN_PWVC_STATUS_IS_ACTIVE (pIccpPw->u4RemoteStatus)))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                           "ICCP PW NODE SWITCHOVER LOCAL STANDBY\n");

                pLocalStandbyPw = L2VPNRED_ICCP_PW_PARENT_PW
                    (pRgGroup->pOperActivePw);
                b1LocalStandby = TRUE;
                b1DataUpdated = TRUE;
            }

            if ((pIccpPw->u2Status & L2VPNRED_PW_STATUS_NODE_SWITCHOVER) &&
                L2VPN_PWVC_STATUS_IS_FORWARDING (pIccpPw->u4LocalStatus,
                                                 pIccpPw->u4RemoteStatus))
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                           "ICCP PW NODE SWITCHOVER COMPLETE\n");

                pRgGroup->pOperActivePw = pIccpPw;
                pIccpPw->u2Status &=
                    (UINT2) (~L2VPNRED_PW_STATUS_NODE_SWITCHOVER);

                if (pRgGroup->u2NumPwAwaiting == 0)
                {
                    TmrStopTimer (gL2vpnRgGlobals.NegTimerList,
                                  &pRgGroup->NegTimer);

                    pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
                    pRgGroup->u1Status &= (UINT1)
                        (~L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE);
                }
                b1DataUpdated = TRUE;
            }
        }                        /* PW forwarding re-selection */
    }                            /* TMO_SLL_Scan (&pLdpIccpEvtInfo->u.PwData.DataList) */

    /* PW Data update processesing */
    if (b1LocalStandby)
    {
        L2vpnRedundancyStandby (pLocalStandbyPw);
    }
    else if (b1ReNegotiate)
    {
        L2vpnRedundancyNegotiate (pRgGroup->u4Index, FALSE);
    }

    /* PW Data Request processesing */
    if ((pLdpIccpEvtInfo->u.PwData.u1RequestType != L2VPNRED_ICCP_REQ_NONE) ||
        TMO_SLL_Count (&pLdpIccpEvtInfo->u.PwData.RequestList) > 0)
    {
        if (pLdpIccpEvtInfo->u.PwData.u1RequestType & L2VPNRED_ICCP_REQ_CONFIG)
        {
            u1ResponseType |= L2VPNRED_ICCP_DATA_CONFIG;
        }
        if (pLdpIccpEvtInfo->u.PwData.u1RequestType & L2VPNRED_ICCP_REQ_STATE)
        {
            u1ResponseType |= L2VPNRED_ICCP_DATA_STATE;
        }

        u1RequestType = b1ReNegotiate || b1LocalStandby ?
            L2VPNRED_ICCP_REQ_STATE | L2VPNRED_ICCP_REQ_ALL :
            L2VPNRED_ICCP_REQ_NONE;

        L2vpnRedundancyIccpPwDataTx (pNode, NULL,
                                     pLdpIccpEvtInfo->u.PwData.u2RequestNum,
                                     u1ResponseType,
                                     &pLdpIccpEvtInfo->u.PwData.RequestList,
                                     u1RequestType);
    }

    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3. "
                "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u), "
                ".pNegForwardingPw(%p), .pNegSwitchoverPw(%p), "
                ".pOperActivePw(%p)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting,
                pRgGroup->pNegForwardingPw,
                pRgGroup->pNegSwitchoverPw, pRgGroup->pOperActivePw);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnFmRedundancyPwForwardingUpdate                       */
/* Description   : This routine updates the PW FORWARDING state in HW        */
/* Input(s)      : pPwVcEntry   - PW for which FORWARDING state has to be    */
/*                              updated                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnFmRedundancyPwForwardingUpdate (tPwVcEntry * pPwVcEntry,
                                     tL2vpnRedundancyEntry * pRgGrp)
{
    VOID               *pSlotInfo = NULL;
    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "PW[%u]{LOCAL_STATUS(%#X), REMOTE_STATUS(%#X)}\n",
                L2VPN_PWVC_INDEX (pPwVcEntry),
                L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry),
                L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry));

    L2VpnPwRedVcHwModify (pPwVcEntry, pRgGrp, pSlotInfo);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 1\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwRemoteNotify                             */
/* Description   : This routine updates PW local status in L2VPN/PW & Notify */
/*                 it to remote peer                                         */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyPwRemoteNotify (UINT4 u4RgIndex)
{
    tL2vpnRedundancyPwEntry *pTmpPw = NULL;
    tL2vpnRedundancyPwEntry *pPwRb = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u)\n", u4RgIndex);

    /* Update PW local status & Notify remote peer */
    pTmpPw = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
    pTmpPw->u4RgIndex = u4RgIndex;

    while ((pPwRb = RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                                   (tRBElem *) pTmpPw, NULL)) != NULL)
    {
        if (pPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if (pPwRb->pIccpPwEntry == NULL)
        {
            MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
            pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
            pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
            continue;
        }

        if ((pPwRb->pIccpPwEntry->u2Status & L2VPNRED_PW_STATUS_LOCAL_FORWARD)
            && (!L2VPN_PWVC_STATUS_IS_ACTIVE
                (pPwRb->pIccpPwEntry->u4LocalStatus)))
        {
            pPwRb->pIccpPwEntry->u2Status &=
                (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_UPDATED);
            L2VPN_PWVC_LOCAL_STATUS (pPwRb->pPwVcEntry) &=
                (UINT1) (~L2VPN_PWVC_STATUS_STANDBY);
        }
        else if ((pPwRb->pIccpPwEntry->u2Status &
                  L2VPNRED_PW_STATUS_LOCAL_STANDBY) &&
                 (L2VPN_PWVC_STATUS_IS_ACTIVE
                  (pPwRb->pIccpPwEntry->u4LocalStatus)))
        {
            pPwRb->pIccpPwEntry->u2Status &=
                (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_UPDATED);
            L2VPN_PWVC_LOCAL_STATUS (pPwRb->pPwVcEntry) |=
                L2VPN_PWVC_STATUS_STANDBY;
        }
        pPwRb->pIccpPwEntry->u4LocalStatus =
            L2VPN_PWVC_LOCAL_STATUS (pPwRb->pPwVcEntry);

        L2VpnSendLdpPwVcNotifMsg (pPwRb->pPwVcEntry);

        MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
        pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
        pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPw);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyIccpNodeNotify                             */
/* Description   : This function Updates the sibling-Node about the local PW */
/*                 status                                                    */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 pRgIccpPw - pointer to the Iccp Pw Entry                  */
/*                 u1ResponseType - Response type                            */
/*                 u1RequestType - Request type                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyIccpNodeNotify (UINT4 u4RgIndex, tL2vpnIccpPwEntry * pRgIccpPw,
                               UINT1 u1ResponseType, UINT1 u1RequestType)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pTmpNode = NULL;
    tL2vpnRedundancyNodeEntry *pNodeRb = NULL;

    L2VPN_DBG4 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "u4RgIndex(%u), pRgIccpPw(%p), u1ResponseType(%#X), "
                "u1RequestType(%#X)\n",
                u4RgIndex, pRgIccpPw, u1ResponseType, u1RequestType);

    if (pRgIccpPw != NULL)
    {
        L2VPN_DBG10 (L2VPN_DBG_LVL_DBG_FLAG,
                     "pRgIccpPw{.RoId((%02X%02X%02X%02X _ "
                     "%02X%02X%02X%02X), u4LocalStatus(%#X), "
                     ".u4RemoteStatus(%#X)}\n",
                     pRgIccpPw->RoId.au1NodeId[0],
                     pRgIccpPw->RoId.au1NodeId[1],
                     pRgIccpPw->RoId.au1NodeId[2],
                     pRgIccpPw->RoId.au1NodeId[3],
                     pRgIccpPw->RoId.au1PwIndex[0],
                     pRgIccpPw->RoId.au1PwIndex[1],
                     pRgIccpPw->RoId.au1PwIndex[2],
                     pRgIccpPw->RoId.au1PwIndex[3],
                     pRgIccpPw->u4LocalStatus, pRgIccpPw->u4RemoteStatus);
    }

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if ((pRgGroup->u2NumNode == 0) || (pRgGroup->u2NumPw == 0))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
        return L2VPN_SUCCESS;
    }

    pTmpNode = MemAllocMemBlk (L2VPN_REDUNDANCY_NODE_POOL_ID);
    if (pTmpNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
    pTmpNode->u4RgIndex = u4RgIndex;

    while ((pNodeRb =
            RBTreeGetNext (gL2vpnRgGlobals.RgNodeList,
                           (tRBElem *) pTmpNode, NULL)) != NULL)
    {
        if (pNodeRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if ((pNodeRb->u1RowStatus != ACTIVE) ||
            !(pNodeRb->u1Status & L2VPNRED_NODE_STATUS_CONNECTED))
        {
            MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
            pTmpNode->u4RgIndex = u4RgIndex;
            pTmpNode->u1AddrType = pNodeRb->u1AddrType;
            pTmpNode->Addr = pNodeRb->Addr;
            continue;
        }

        L2vpnRedundancyIccpPwDataTx (pNodeRb, pRgIccpPw,
                                     LDPICCP_REQUEST_UNSOLICITED,
                                     u1ResponseType, NULL, u1RequestType);
        pNodeRb->u1Status |= L2VPNRED_NODE_STATUS_LOCAL_SYNC;

        MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
        pTmpNode->u4RgIndex = u4RgIndex;
        pTmpNode->u1AddrType = pNodeRb->u1AddrType;
        pTmpNode->Addr = pNodeRb->Addr;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pTmpNode);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyStandby                                    */
/* Description   : This routine trigger local standby for a PW               */
/* Input(s)      : pRgPw - pointer to the redundancy PW entry                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyStandby (tL2vpnRedundancyPwEntry * pRgPw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "pRgPw{.u4RgIndex(%u), .pPwVcEntry{%u}}\n",
                pRgPw->u4RgIndex, L2VPN_PWVC_INDEX (pRgPw->pPwVcEntry));

    if (pRgPw == NULL)
    {
        return L2VPN_FAILURE;
    }
    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if ((!L2VPN_PWVC_STATUS_IS_UP (L2VPN_PWVC_LOCAL_STATUS
                                   (pRgPw->pPwVcEntry))) ||
        (!L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                       (pRgPw->pPwVcEntry))))
    {
        L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2. "
                    "pRgGroup{.u1Status(%#X)}, pPwVcEntry{.u4LocalStatus(%#X), "
                    ".u4RemoteStatus(%#X)}\n",
                    pRgGroup->u1Status, L2VPN_PWVC_LOCAL_STATUS
                    (pRgPw->pPwVcEntry),
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry));
        return L2VPN_FAILURE;
    }

    if (pRgGroup->pOperActivePw == pRgPw->pIccpPwEntry)
    {
        pRgGroup->u1Status &= (UINT1) (~L2VPNRED_STATUS_NEGOTIATION_SUCCESS);
        pRgGroup->u1Status = L2VPNRED_STATUS_FORWARDING_NEGOTIATON;
        pRgGroup->u1Status |= L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE;
    }

    pRgPw->pIccpPwEntry->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |= L2VPN_PWVC_STATUS_STANDBY;
    pRgPw->pIccpPwEntry->u4LocalStatus =
        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);

    if (L2VPN_PWVC_MODE (pRgPw->pPwVcEntry) == L2VPN_VPLS)
    {
        L2VpnPwRedVplsHwModify (pRgPw->pPwVcEntry, L2VPN_TRUE, NULL);
    }
    L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);

    L2vpnRedundancyIccpNodeNotify (pRgPw->u4RgIndex, pRgPw->pIccpPwEntry,
                                   L2VPNRED_ICCP_DATA_STATE,
                                   L2VPNRED_ICCP_REQ_STATE |
                                   L2VPNRED_ICCP_REQ_ALL);

    if (L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC
        (pRgPw->pIccpPwEntry->u4LocalStatus,
         pRgPw->pIccpPwEntry->u4RemoteStatus))
    {
        if (pRgGroup->pOperActivePw == pRgPw->pIccpPwEntry)
        {
            pRgGroup->pOperActivePw = NULL;
        }
        pRgGroup->u2NumPwAwaiting--;

        L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry, pRgGroup);
        pRgPw->pIccpPwEntry->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;

        pRgPw->pIccpPwEntry->u2Status &=
            (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
        if (pRgGroup->u2NumPwAwaiting == 0)
        {
            TmrStopTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer);

            pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
        }
    }
    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3. "
                "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u), "
                ".pNegForwardingPw(%p), .pNegSwitchoverPw(%p), "
                ".pOperActivePw(%p)}, pRgPw{.u1Status(%#X)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting,
                pRgGroup->pNegForwardingPw,
                pRgGroup->pNegSwitchoverPw, pRgGroup->pOperActivePw,
                pRgPw->pIccpPwEntry->u2Status);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancySwitchover                                 */
/* Description   : This routine trigger switchover to the specified local PW */
/* Input(s)      : pRgPw - pointer to the redundancy PW entry                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancySwitchover (tL2vpnRedundancyPwEntry * pRgPw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyPwEntry *pRgPwNeg = NULL;
    UINT1               u1CurrStatus = 0;
#ifdef HVPLS_WANTED
#ifdef NPAPI_WANTED
    tVPLSEntry          *pVplsEntry = NULL;
    UINT4               u4VplsInstance;
#endif
#endif

    L2VPN_DBG4 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. "
                "pRgPw{.u4RgIndex(%u), .pPwVcEntry{.u4Index(%u), "
                ".u4LocalStatus(%#X),  .u4RemoteStatus(%#X)}}\n",
                pRgPw->u4RgIndex, L2VPN_PWVC_INDEX (pRgPw->pPwVcEntry),
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry),
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry));

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
    if (pRgGroup == NULL || pRgPw->pIccpPwEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }
    u1CurrStatus = pRgGroup->u1Status;

    if (!L2VPN_PWVC_STATUS_IS_UP (pRgPw->pIccpPwEntry->u4LocalStatus) ||
        !L2VPN_PWVC_STATUS_IS_UP (pRgPw->pIccpPwEntry->u4RemoteStatus))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return L2VPN_FAILURE;
    }

    if (L2VPN_PWVC_STATUS_IS_SWITCHOVER (pRgPw->pIccpPwEntry->u4LocalStatus))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3\n");
        return L2VPN_SUCCESS;
    }

    /* Override on-going switchover */
    if ((pRgGroup->u1Status & L2VPNRED_STATUS_SWITCHOVER_NEGOTIATON) &&
        !(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS))
    {
        pRgPwNeg = L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pNegSwitchoverPw);

        pRgPwNeg->pIccpPwEntry->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
        L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry) |=
            L2VPN_PWVC_STATUS_STANDBY;
        pRgPwNeg->pIccpPwEntry->u4LocalStatus =
            L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry);

        pRgGroup->pNegSwitchoverPw = NULL;

        L2VpnSendLdpPwVcNotifMsg (pRgPwNeg->pPwVcEntry);
    }

    /* Initiate PW switchover */
    pRgGroup->u1Status = L2VPNRED_STATUS_SWITCHOVER_NEGOTIATON;

    pRgPw->pIccpPwEntry->u2Status = L2VPNRED_PW_STATUS_LOCAL_SWITCHOVER;
    pRgPw->pIccpPwEntry->u2Status |= L2VPNRED_PW_STATUS_REMOTE_AWAITED;
    L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
        L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST;
    pRgPw->pIccpPwEntry->u4LocalStatus =
        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
    pRgGroup->u2NumPwAwaiting++;

    pRgGroup->pNegSwitchoverPw = pRgPw->pIccpPwEntry;

    L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
#ifdef HVPLS_WANTED
    if (L2VPN_PWVC_MODE (pRgPw->pPwVcEntry) == L2VPN_VPLS)
    {
        L2VpnPwRedVplsHwModify (pRgPw->pPwVcEntry, L2VPN_FALSE, NULL);
        L2VpnSendLdpMacWdrlMsg(pRgPw->pPwVcEntry);
    }
#endif

    /* Place current ACTIVE PW as STANDBY */
    if ((u1CurrStatus & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
        (pRgGroup->pOperActivePw != NULL) && pRgGroup->pOperActivePw->b1IsLocal)
    {
        pRgPwNeg = L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw);

        pRgPwNeg->pIccpPwEntry->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
        L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry) |=
            L2VPN_PWVC_STATUS_STANDBY;
        pRgPwNeg->pIccpPwEntry->u4LocalStatus =
            L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry);
        pRgPwNeg->pIccpPwEntry->u4LocalStatus =
            L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry);

        L2VpnSendLdpPwVcNotifMsg (pRgPwNeg->pPwVcEntry);
#ifdef HVPLS_WANTED
    if (L2VPN_PWVC_MODE (pRgPwNeg->pPwVcEntry) == L2VPN_VPLS)
    {
        L2VpnPwRedVplsHwModify (pRgPwNeg->pPwVcEntry, L2VPN_FALSE, NULL);
#ifdef NPAPI_WANTED
        u4VplsInstance = pRgPwNeg->pPwVcEntry->u4VplsInstance;
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex(u4VplsInstance);
        if (pVplsEntry != NULL)
        {
            L2VpnHwVplsFlushMac (pRgPwNeg->pPwVcEntry, pVplsEntry); 
        }
#endif
#ifndef NPAPI_WANTED
        VlanVplsDelPwFdbEntries (pRgPwNeg->pPwVcEntry->u4PwVcIndex);
#endif
   }
#endif

    }

    L2vpnRedundancyIccpNodeNotify (pRgGroup->u4Index, NULL,
                                   L2VPNRED_ICCP_DATA_STATE,
                                   L2VPNRED_ICCP_REQ_STATE |
                                   L2VPNRED_ICCP_REQ_ALL);

    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4. pRgGroup{.u1Status(%#X),"
                ".u2NumPwAwaiting(%u), .pNegForwardingPw(%p), "
                ".pNegSwitchoverPw(%p), .pOperActivePw(%p)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting,
                pRgGroup->pNegForwardingPw, pRgGroup->pNegSwitchoverPw,
                pRgGroup->pOperActivePw);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNegotiate                                  */
/* Description   : This routine re-calculates the best PW for FORWARDING and */
/*                 initiate signaling                                        */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 b1AltPriority - Priority                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyNegotiate (UINT4 u4RgIndex, BOOL1 b1AltPriority)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyPwEntry *pTmpPw = NULL;
    tL2vpnRedundancyPwEntry *pPwRb = NULL;
    tL2vpnIccpPwEntry  *pNegIccpPw = NULL;
    UINT2               u2BestPriority = L2VPNRED_PW_PRIORITY_MIN;
#ifdef HVPLS_WANTED
    tPwVcEntry          *pPwVcEntry = NULL;
#ifdef NPAPI_WANTED
    tVPLSEntry          *pVplsEntry = NULL;
    UINT4               u4VplsInstance;
#endif
    UINT4               u4PwIndex;
    BOOL1               bIsMacWdrlReq = L2VPN_FALSE;
#endif

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u),  "
                "b1AltPriority(%u)\n", u4RgIndex, b1AltPriority);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (b1AltPriority)
    {
        u2BestPriority = pRgGroup->u2NegPriority;
    }
    if (L2vpnRedundancyCalcBestPwPriority (&u2BestPriority, u4RgIndex,
                                           u2BestPriority) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return L2VPN_FAILURE;
    }
    pRgGroup->u2NegPriority = u2BestPriority;
    pRgGroup->u2NumPwAwaiting = 0;
    pRgGroup->u1Status = L2VPNRED_STATUS_FORWARDING_NEGOTIATON;
    if (pRgGroup->pOperActivePw == NULL)
    {
        pRgGroup->u1Status |= L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE;
    }
#ifdef HVPLS_WANTED
    else
    {
        MEMCPY(&u4PwIndex, pRgGroup->pOperActivePw->RoId.au1PwIndex, sizeof(UINT4)); 
        u4PwIndex = OSIX_HTONL(u4PwIndex); 
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
        if (pPwVcEntry != NULL)
        {
            if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
            {   
                L2VpnPwRedVplsHwModify (pPwVcEntry, L2VPN_TRUE, NULL);
                bIsMacWdrlReq = L2VPN_TRUE;
            }
        }
    }    
#endif
    if (L2vpnRedundancyCalcBestPw (u4RgIndex, u2BestPriority, &pNegIccpPw)
        == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
        return L2VPN_FAILURE;
    }
    pRgGroup->pNegForwardingPw = pNegIccpPw;

    /* Update PW local status & Notify remote peer */
    L2vpnRedundancyPwRemoteNotify (u4RgIndex);

    /* Update sibling-Node of the local status info */
    L2vpnRedundancyIccpNodeNotify (u4RgIndex, NULL, L2VPNRED_ICCP_DATA_STATE,
                                   L2VPNRED_ICCP_REQ_STATE);

    /* Check remote status and update HW */
    pTmpPw = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 4\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
    pTmpPw->u4RgIndex = u4RgIndex;

    while ((pPwRb =
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                           (tRBElem *) pTmpPw, NULL)) != NULL)
    {
        if (pPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if (pPwRb->pIccpPwEntry == NULL)
        {
            pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
            pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
            continue;
        }

        if (L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC
            (pPwRb->pIccpPwEntry->u4LocalStatus,
             pPwRb->pIccpPwEntry->u4RemoteStatus))
        {
            if (!
                    (pPwRb->pIccpPwEntry->
                     u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED))
            {

                L2vpnFmRedundancyPwForwardingUpdate (pPwRb->pPwVcEntry,
                        pRgGroup);
                pPwRb->pIccpPwEntry->u2Status |=
                        L2VPNRED_PW_STATUS_LOCAL_UPDATED;

#ifdef HVPLS_WANTED
                if (L2VPN_PWVC_MODE (pPwRb->pPwVcEntry) == L2VPN_VPLS)
                {
#ifdef NPAPI_WANTED
                    u4VplsInstance = pPwRb->pPwVcEntry->u4VplsInstance;
                    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex(u4VplsInstance);
                    if (pVplsEntry != NULL)
                    {
                        L2VpnHwVplsFlushMac (pPwRb->pPwVcEntry, pVplsEntry); 
                    }
#endif
                    L2VpnPwRedVplsHwModify (pPwRb->pPwVcEntry, L2VPN_FALSE, NULL);
                    if (bIsMacWdrlReq == L2VPN_TRUE)
                    {
                        L2VpnSendLdpMacWdrlMsg(pPwRb->pPwVcEntry);
#ifndef NPAPI_WANTED
                        VlanVplsDelPwFdbEntries (pPwVcEntry->u4PwVcIndex);
#endif

                    }
                }
#endif
            }
        }

        MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
        pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
        pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPw);

    pNegIccpPw = pRgGroup->pNegForwardingPw;

    if ((pRgGroup->pOperActivePw == pNegIccpPw) || (pNegIccpPw == NULL))
    {
        pRgGroup->pOperActivePw = NULL;
    }
    if (pRgGroup->u2NumPwAwaiting == 0)
    {
        if ((pNegIccpPw != NULL) &&
            (L2VPN_PWVC_STATUS_IS_FORWARDING (pNegIccpPw->u4LocalStatus,
                                              pNegIccpPw->u4RemoteStatus)))
        {
            pRgGroup->pOperActivePw = pRgGroup->pNegForwardingPw;
            pNegIccpPw->u2Status = L2VPNRED_PW_STATUS_LOCAL_FORWARD;
            pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            pRgGroup->u1Status &=
                (UINT1) (~L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE);
        }

        if ((pRgGroup->pNegForwardingPw != NULL) && (pRgGroup->u2NumUpPw == 1))
        {
            pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            pRgGroup->u1Status &=
                (UINT1) (~L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE);
        }
        pRgGroup->pNegForwardingPw = NULL;
        TmrStopTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer);
    }

    /* Monitor PW-RED ICCP negotiation timeout */
    if ((!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
         (gL2vpnRgGlobals.u4NegotiationTimeout > 0)) &&
        (pRgGroup->b1ReversionEnable == L2VPN_PWRED_REVERT_ENABLE))
    {
        if (TmrStartTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer,
                           gL2vpnRgGlobals.u4NegotiationTimeout * 
                           SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "INTMD-EXIT 5. "
                       "Start of PW-RED ICCP timer failed\n");
            return L2VPN_FAILURE;
        }
    }
    L2VPN_DBG5 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 6. "
                "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u), "
                ".pNegForwardingPw(%p), "
                ".pNegSwitchoverPw(%p), .pOperActivePw(%p)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting,
                pRgGroup->pNegForwardingPw,
                pRgGroup->pNegSwitchoverPw, pRgGroup->pOperActivePw);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyCalcBestPwPriority                         */
/* Description   : This routine finds the highest PW priority from internal  */
/*                 ICCP PW DB                                                */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 u2PrevPriority - Previous priority                        */
/* Output(s)     : pu2BestPriority - pointer to Best priority                */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyCalcBestPwPriority (UINT2 *pu2BestPriority, UINT4 u4RgIndex,
                                   UINT2 u2PrevPriority)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pIccpPwRb = NULL;
    UINT2               u2BestPriority = L2VPNRED_PW_PRIORITY_MIN;

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u), "
                "u2PrevPriority(%#X)\n", u4RgIndex, u2PrevPriority);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (pRgGroup->u2NumIccpPw == 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return L2VPN_FAILURE;
    }

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4RgIndex;

    while ((pIccpPwRb = RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                                       (tRBElem *) pTmpIccpPw, NULL)) != NULL)
    {
        if (pIccpPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }
        if (!L2VPN_PWVC_STATUS_IS_UP (pIccpPwRb->u4LocalStatus) ||
            !L2VPN_PWVC_STATUS_IS_UP (pIccpPwRb->u4RemoteStatus) ||
            (pIccpPwRb->u2Status & L2VPNRED_PW_STATUS_REMOTE_STANDBY) ||
            ((u2PrevPriority != L2VPNRED_PW_PRIORITY_MIN) &&
             (pIccpPwRb->u2Priority <= u2PrevPriority)))
        {
            MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
            pTmpIccpPw->u4RgIndex = pIccpPwRb->u4RgIndex;
            pTmpIccpPw->RouterId = pIccpPwRb->RouterId;
            pTmpIccpPw->Fec = pIccpPwRb->Fec;
            continue;
        }

        if (pIccpPwRb->u2Priority < u2BestPriority)
        {
            u2BestPriority = pIccpPwRb->u2Priority;
        }

        MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
        pTmpIccpPw->u4RgIndex = pIccpPwRb->u4RgIndex;
        pTmpIccpPw->RouterId = pIccpPwRb->RouterId;
        pTmpIccpPw->Fec = pIccpPwRb->Fec;
    }
    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    *pu2BestPriority = u2BestPriority;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4. pu2BestPriority(%#X)\n",
                *pu2BestPriority);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyCalcBestPw                                 */
/* Description   : This routine compares ICCP PWs and finds the most         */
/*                 prefered PW                                               */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 u2Priority - priority                                     */
/* Output(s)     : ppNegIccpPw  - double pointer to the Best PW              */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyCalcBestPw (UINT4 u4RgIndex, UINT2 u2Priority,
                           tL2vpnIccpPwEntry ** ppNegIccpPw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnIccpPwEntry  *pTmpIccpPw = NULL;
    tL2vpnIccpPwEntry  *pIccpPwRb = NULL;
    tL2vpnIccpPwEntry  *pBestIccpPw = NULL;
    BOOL1               b1BetterPw = FALSE;

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u), "
                "u2Priority(%#X)\n", u4RgIndex, u2Priority);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (pRgGroup->u2NumIccpPw == 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
        return L2VPN_SUCCESS;
    }

    pTmpIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
    pTmpIccpPw->u4RgIndex = u4RgIndex;

    while ((pIccpPwRb = RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                                       (tRBElem *) pTmpIccpPw, NULL)) != NULL)
    {
        if (pIccpPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if (!L2VPN_PWVC_STATUS_IS_UP (pIccpPwRb->u4LocalStatus) ||
            !L2VPN_PWVC_STATUS_IS_UP (pIccpPwRb->u4RemoteStatus) ||
            (pIccpPwRb->u2Status & L2VPNRED_PW_STATUS_REMOTE_STANDBY))
        {
            MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
            pTmpIccpPw->u4RgIndex = pIccpPwRb->u4RgIndex;
            pTmpIccpPw->RouterId = pIccpPwRb->RouterId;
            pTmpIccpPw->Fec = pIccpPwRb->Fec;
            continue;
        }

        if (pRgGroup->u1MultiHomingApps & L2VPNRED_MULTIHOMINGAPP_LAGG)
        {
            if ((L2VPN_PWVC_STATUS_IS_ACTIVE 
                ((L2VPNRED_ICCP_PW_PARENT_PW(pIccpPwRb))->pPwVcEntry->
                 u1RemoteStatus)) &&
                (L2VPN_PWVC_STATUS_IS_UP
                 ((L2VPNRED_ICCP_PW_PARENT_PW(pIccpPwRb))->pPwVcEntry->
                  u1LocalStatus)))
            {
                pBestIccpPw = pIccpPwRb;
            }
            MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
            pTmpIccpPw->u4RgIndex = pIccpPwRb->u4RgIndex;
            pTmpIccpPw->RouterId = pIccpPwRb->RouterId;
            pTmpIccpPw->Fec = pIccpPwRb->Fec;
            continue;
        }

        b1BetterPw = FALSE;
        if (pIccpPwRb->u2Priority == u2Priority)
        {
            if (pBestIccpPw == NULL ||
                L2vpnRedundancyPwCmp (pIccpPwRb,
                                      pBestIccpPw) == L2VPN_RB_GREATER)
            {
                b1BetterPw = TRUE;
            }
        }

        if (b1BetterPw)
        {
            if (pIccpPwRb->b1IsLocal)
            {
                pIccpPwRb->u2Status = L2VPNRED_PW_STATUS_LOCAL_FORWARD;
            }

            if (pBestIccpPw != NULL && pBestIccpPw->b1IsLocal)
            {
                pBestIccpPw->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
            }

            pBestIccpPw = pIccpPwRb;
        }
        else if (pIccpPwRb->b1IsLocal)
        {
            pIccpPwRb->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
        }

        MEMSET (pTmpIccpPw, 0, sizeof (*pTmpIccpPw));
        pTmpIccpPw->u4RgIndex = pIccpPwRb->u4RgIndex;
        pTmpIccpPw->RouterId = pIccpPwRb->RouterId;
        pTmpIccpPw->Fec = pIccpPwRb->Fec;
    }
    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPw);

    *ppNegIccpPw = pBestIccpPw;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4. pBestIccpPw(%p)\n",
                pBestIccpPw);
    return L2VPN_SUCCESS;
}

/**
 * PW Redundancy internal DB search routines
 */
/*****************************************************************************/
/* Function Name : L2VpnGetRedundancyEntryByIndex                            */
/* Description   : This routine is to fetch the redundancy group entry from  */
/*                 the data base                                             */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/Redundancy entry if success                 */
/*****************************************************************************/
tL2vpnRedundancyEntry *
L2VpnGetRedundancyEntryByIndex (UINT4 u4RgIndex)
{
    tL2vpnRedundancyEntry *pTmpGroupEntry = NULL;
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    pTmpGroupEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_GROUP_POOL_ID);
    if (pTmpGroupEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpGroupEntry, 0, sizeof (*pTmpGroupEntry));
    pTmpGroupEntry->u4Index = u4RgIndex;

    pRgGroup = RBTreeGet (gL2vpnRgGlobals.RgList, (tRBElem *) pTmpGroupEntry);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_GROUP_POOL_ID,
                        (VOID *) pTmpGroupEntry);
    return pRgGroup;
}

/*****************************************************************************/
/* Function Name : L2VpnGetRedundancyNodeEntryByIndex                        */
/* Description   : This routine is to fetch the redundancy node entry from   */
/*                 the data base                                             */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 u1AddrType - address type of the node                     */
/*                 pAddr      - Node addr                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/Redundancy Node Entry if success            */
/*****************************************************************************/
tL2vpnRedundancyNodeEntry *
L2VpnGetRedundancyNodeEntryByIndex (UINT4 u4RgIndex, UINT1 u1AddrType,
                                    uGenAddr * pAddr)
{
    tL2vpnRedundancyNodeEntry *pTmpNodeEntry = NULL;
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;

    pTmpNodeEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_NODE_POOL_ID);
    if (pTmpNodeEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpNodeEntry, 0, sizeof (*pTmpNodeEntry));
    pTmpNodeEntry->u4RgIndex = u4RgIndex;
    pTmpNodeEntry->u1AddrType = u1AddrType;
    pTmpNodeEntry->Addr = *pAddr;

    pRgNode = RBTreeGet (gL2vpnRgGlobals.RgNodeList, (tRBElem *) pTmpNodeEntry);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pTmpNodeEntry);
    return pRgNode;
}

/*****************************************************************************/
/* Function Name : L2VpnGetRedundancyPwEntryByIndex                          */
/* Description   : This routine is to fetch the redundancy PW entry from     */
/*                 the data base                                             */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 u4PwIndex - PW index                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/Redundancy PW Entry if success              */
/*****************************************************************************/
tL2vpnRedundancyPwEntry *
L2VpnGetRedundancyPwEntryByIndex (UINT4 u4RgIndex, UINT4 u4PwIndex)
{
    tL2vpnRedundancyPwEntry *pTmpPwEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pTmpPwEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPwEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpPwEntry, 0, sizeof (*pTmpPwEntry));
    pTmpPwEntry->u4RgIndex = u4RgIndex;
    pTmpPwEntry->u4PwIndex = u4PwIndex;

    pRgPw = RBTreeGet (gL2vpnRgGlobals.RgPwList, (tRBElem *) pTmpPwEntry);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPwEntry);
    return pRgPw;
}

/*****************************************************************************/
/* Function Name : L2VpnGetRedundancyPwEntryByPwIndex                        */
/* Description   : This routine is to fetch the redundancy PW entry from     */
/*                 the data base                                             */
/* Input(s)      : u4PwIndex - PW index                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/Redundancy PW Entry if success              */
/*****************************************************************************/
tL2vpnRedundancyPwEntry *
L2VpnGetRedundancyPwEntryByPwIndex (UINT4 u4PwIndex)
{
    tL2vpnRedundancyEntry *pTmpGroupEntry = NULL;
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyPwEntry *pTmpPwEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

	if (L2VPN_PWRED_STATUS == L2VPN_PWRED_DISABLED)
	{
		return NULL;

	}

    pTmpPwEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPwEntry == NULL)
    {
        return NULL;
    }

    pTmpGroupEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_GROUP_POOL_ID);
    if (pTmpGroupEntry == NULL)
    {
        MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPwEntry);
        return NULL;
    }

    MEMSET (pTmpGroupEntry, 0, sizeof (*pTmpGroupEntry));

    while ((pRgGroup = RBTreeGetNext (gL2vpnRgGlobals.RgList,
                                      (tRBElem *) pTmpGroupEntry,
                                      NULL)) != NULL)
    {
        MEMSET (pTmpPwEntry, 0, sizeof (*pTmpPwEntry));
        pTmpPwEntry->u4RgIndex = pRgGroup->u4Index;
        pTmpPwEntry->u4PwIndex = u4PwIndex;

        pRgPw = RBTreeGet (gL2vpnRgGlobals.RgPwList, (tRBElem *) pTmpPwEntry);
        if (pRgPw != NULL)
        {
            break;
        }

        MEMSET (pTmpGroupEntry, 0, sizeof (*pTmpGroupEntry));
        pTmpGroupEntry->u4Index = pRgGroup->u4Index;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_GROUP_POOL_ID,
                        (VOID *) pTmpGroupEntry);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPwEntry);
    return pRgPw;
}

/*****************************************************************************/
/* Function Name : L2VpnGetIccpPwEntryByIndex                                */
/* Description   : This routine is to fetch the ICCP PW entry from data base */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 pRouterId - pointer to the router id                      */
/*                 pFec      - pointer to the PW FEC                         */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/ICCP PW entry if success                    */
/*****************************************************************************/
tL2vpnIccpPwEntry  *
L2VpnGetIccpPwEntryByIndex (UINT4 u4RgIndex, tMplsRouterId * pRouterId,
                            tL2vpnPwFec * pFec)
{
    tL2vpnIccpPwEntry  *pTmpIccpPwEntry = NULL;
    tL2vpnIccpPwEntry  *pIccpPw = NULL;

    pTmpIccpPwEntry = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPwEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpIccpPwEntry, 0, sizeof (*pTmpIccpPwEntry));
    pTmpIccpPwEntry->u4RgIndex = u4RgIndex;
    pTmpIccpPwEntry->RouterId = *pRouterId;
    pTmpIccpPwEntry->Fec = *pFec;

    pIccpPw =
        RBTreeGet (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pTmpIccpPwEntry);

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPwEntry);
    return pIccpPw;
}

/*****************************************************************************/
/* Function Name : L2VpnGetIccpPwEntryByRgRoId                               */
/* Description   : This routine is to fetch the ICCP PW entry from data base */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 pRouterId - pointer to the router id                      */
/*                 pRoId     - pointer to the ROIDW FEC                      */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/ICCP PW entry if success                    */
/*****************************************************************************/
tL2vpnIccpPwEntry  *
L2VpnGetIccpPwEntryByRgRoId (UINT4 u4RgIndex, tMplsRouterId * pRouterId,
                             tL2vpnIccpRoId * pRoId)
{
    tL2vpnIccpPwEntry  *pTmpIccpPwEntry = NULL;
    tL2vpnIccpPwEntry  *pIccpPwRb = NULL;
    tL2vpnIccpPwEntry  *pIccpPw = NULL;

    pTmpIccpPwEntry = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPwEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpIccpPwEntry, 0, sizeof (*pTmpIccpPwEntry));
    pTmpIccpPwEntry->u4RgIndex = u4RgIndex;

    while ((pIccpPwRb =
            RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                           (tRBElem *) pTmpIccpPwEntry, NULL)) != NULL)
    {
        if (pIccpPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if ((MEMCMP (&pIccpPwRb->RouterId, pRouterId,
                     sizeof (pIccpPwRb->RouterId)) == 0) &&
            (MEMCMP (&pIccpPwRb->RoId, pRoId, sizeof (pIccpPwRb->RoId)) == 0))
        {
            pIccpPw = pIccpPwRb;
            break;
        }

        MEMSET (pTmpIccpPwEntry, 0, sizeof (*pTmpIccpPwEntry));
        pTmpIccpPwEntry->u4RgIndex = pIccpPwRb->u4RgIndex;
        pTmpIccpPwEntry->RouterId = pIccpPwRb->RouterId;
        pTmpIccpPwEntry->Fec = pIccpPwRb->Fec;
    }
    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPwEntry);

    return pIccpPw;
}

/*****************************************************************************/
/* Function Name : L2VpnGetIccpPwEntryByRgServiceName                        */
/* Description   : This routine is to fetch the ICCP PW entry from data base */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 pRouterId - pointer to the router id                      */
/*                 pName     - pointer to the service name                   */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/ICCP PW entry if success                    */
/*****************************************************************************/
tL2vpnIccpPwEntry  *
L2VpnGetIccpPwEntryByRgServiceName (UINT4 u4RgIndex,
                                    tMplsRouterId * pRouterId,
                                    tL2vpnIccpServiceName * pName)
{
    tL2vpnIccpPwEntry  *pTmpIccpPwEntry = NULL;
    tL2vpnIccpPwEntry  *pIccpPwRb = NULL;
    tL2vpnIccpPwEntry  *pIccpPw = NULL;

    pTmpIccpPwEntry = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pTmpIccpPwEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpIccpPwEntry, 0, sizeof (*pTmpIccpPwEntry));
    pTmpIccpPwEntry->u4RgIndex = u4RgIndex;

    while ((pIccpPwRb =
            RBTreeGetNext (gL2vpnRgGlobals.RgIccpPwList,
                           (tRBElem *) pTmpIccpPwEntry, NULL)) != NULL)
    {
        if (pIccpPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if ((MEMCMP (&pIccpPwRb->RouterId, pRouterId,
                     sizeof (pIccpPwRb->RouterId)) == 0) &&
            (MEMCMP (&pIccpPwRb->Name, pName, sizeof (pIccpPwRb->Name)) == 0))
        {
            pIccpPw = pIccpPwRb;
            break;
        }

        MEMSET (pTmpIccpPwEntry, 0, sizeof (*pTmpIccpPwEntry));
        pTmpIccpPwEntry->u4RgIndex = pIccpPwRb->u4RgIndex;
        pTmpIccpPwEntry->RouterId = pIccpPwRb->RouterId;
        pTmpIccpPwEntry->Fec = pIccpPwRb->Fec;
    }
    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pTmpIccpPwEntry);

    return pIccpPw;
}

/**
 * PW Redundancy internal DB maintainance routines
 */
/*****************************************************************************/
/* Function Name : L2vpnRedundancyEntryRbCmp                                 */
/* Description   : This routine is to do the comparison for the nodes to be  */
/*                 added in Redunancy entry                                  */
/* Input(s)      : pRBElem1 - Pointer to the RB Nodes for comparison         */
/*                 pRBElem2 - pointer to the RB Nodes for comparison         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_RB_GREATER/L2VPN_RB_LESSER/L2VPN_RB_EQUAL           */
/*****************************************************************************/
INT4
L2vpnRedundancyEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tL2vpnRedundancyEntry *pL2vpnRedundancyEntryA =
        (tL2vpnRedundancyEntry *) pRBElem1;
    tL2vpnRedundancyEntry *pL2vpnRedundancyEntryB =
        (tL2vpnRedundancyEntry *) pRBElem2;

    if (pL2vpnRedundancyEntryA->u4Index > pL2vpnRedundancyEntryB->u4Index)
    {
        return L2VPN_RB_GREATER;
    }
    else if (pL2vpnRedundancyEntryA->u4Index < pL2vpnRedundancyEntryB->u4Index)
    {
        return L2VPN_RB_LESSER;
    }

    return L2VPN_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeEntryRbCmp                             */
/* Description   : This routine is to do the comparison for the nodes to be  */
/*                 added in Redunancy Node entry                             */
/* Input(s)      : pRBElem1 - Pointer to the RB Nodes for comparison         */
/*                 pRBElem2 - pointer to the RB Nodes for comparison         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_RB_GREATER/L2VPN_RB_LESSER/L2VPN_RB_EQUAL           */
/*****************************************************************************/
INT4
L2vpnRedundancyNodeEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tL2vpnRedundancyNodeEntry *pL2vpnRedundancyNodeEntryA =
        (tL2vpnRedundancyNodeEntry *) pRBElem1;
    tL2vpnRedundancyNodeEntry *pL2vpnRedundancyNodeEntryB =
        (tL2vpnRedundancyNodeEntry *) pRBElem2;

    if ((pL2vpnRedundancyNodeEntryA->u4RgIndex >
         pL2vpnRedundancyNodeEntryB->u4RgIndex)
        ||
        ((pL2vpnRedundancyNodeEntryA->u4RgIndex ==
          pL2vpnRedundancyNodeEntryB->u4RgIndex)
         && (pL2vpnRedundancyNodeEntryA->u1AddrType >
             pL2vpnRedundancyNodeEntryB->u1AddrType))
        ||
        ((pL2vpnRedundancyNodeEntryA->u4RgIndex ==
          pL2vpnRedundancyNodeEntryB->u4RgIndex)
         && (pL2vpnRedundancyNodeEntryA->u1AddrType ==
             pL2vpnRedundancyNodeEntryB->u1AddrType)
         &&
         (MEMCMP
          (&pL2vpnRedundancyNodeEntryA->Addr, &pL2vpnRedundancyNodeEntryB->Addr,
           IPV4_ADDR_LENGTH)) > 0))
    {
        return L2VPN_RB_GREATER;
    }
    else if ((pL2vpnRedundancyNodeEntryA->u4RgIndex ==
              pL2vpnRedundancyNodeEntryB->u4RgIndex)
             && (pL2vpnRedundancyNodeEntryA->u1AddrType ==
                 pL2vpnRedundancyNodeEntryB->u1AddrType)
             &&
             (MEMCMP
              (&pL2vpnRedundancyNodeEntryA->Addr,
               &pL2vpnRedundancyNodeEntryB->Addr, IPV4_ADDR_LENGTH)) == 0)
    {
        return L2VPN_RB_EQUAL;
    }

    return L2VPN_RB_LESSER;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwEntryRbCmp                               */
/* Description   : This routine is to do the comparison for the nodes to be  */
/*                 added in Redunancy PW entry                               */
/* Input(s)      : pRBElem1 - Pointer to the RB Nodes for comparison         */
/*                 pRBElem2 - pointer to the RB Nodes for comparison         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_RB_GREATER/L2VPN_RB_LESSER/L2VPN_RB_EQUAL           */
/*****************************************************************************/
INT4
L2vpnRedundancyPwEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tL2vpnRedundancyPwEntry *pL2vpnRedundancyPwEntryA =
        (tL2vpnRedundancyPwEntry *) pRBElem1;
    tL2vpnRedundancyPwEntry *pL2vpnRedundancyPwEntryB =
        (tL2vpnRedundancyPwEntry *) pRBElem2;

    if ((pL2vpnRedundancyPwEntryA->u4RgIndex >
         pL2vpnRedundancyPwEntryB->u4RgIndex)
        ||
        ((pL2vpnRedundancyPwEntryA->u4RgIndex ==
          pL2vpnRedundancyPwEntryB->u4RgIndex)
         && (pL2vpnRedundancyPwEntryA->u4PwIndex >
             pL2vpnRedundancyPwEntryB->u4PwIndex)))
    {
        return L2VPN_RB_GREATER;
    }
    else if ((pL2vpnRedundancyPwEntryA->u4RgIndex ==
              pL2vpnRedundancyPwEntryB->u4RgIndex)
             && (pL2vpnRedundancyPwEntryA->u4PwIndex ==
                 pL2vpnRedundancyPwEntryB->u4PwIndex))
    {
        return L2VPN_RB_EQUAL;
    }

    return L2VPN_RB_LESSER;
}

/*****************************************************************************/
/* Function Name : L2vpnIccpPwEntryRbCmp                                     */
/* Description   : This routine is to do the comparison for the nodes to be  */
/*                 added in ICCP PW entry                                    */
/* Input(s)      : pRBElem1 - Pointer to the RB Nodes for comparison         */
/*                 pRBElem2 - pointer to the RB Nodes for comparison         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_RB_GREATER/L2VPN_RB_LESSER/L2VPN_RB_EQUAL           */
/*****************************************************************************/
INT4
L2vpnIccpPwEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tL2vpnIccpPwEntry  *pL2vpnIccpPwEntryA = (tL2vpnIccpPwEntry *) pRBElem1;
    tL2vpnIccpPwEntry  *pL2vpnIccpPwEntryB = (tL2vpnIccpPwEntry *) pRBElem2;

    if ((pL2vpnIccpPwEntryA->u4RgIndex > pL2vpnIccpPwEntryB->u4RgIndex) ||
        ((pL2vpnIccpPwEntryA->u4RgIndex == pL2vpnIccpPwEntryB->u4RgIndex) &&
         (MEMCMP (&pL2vpnIccpPwEntryA->RouterId, &pL2vpnIccpPwEntryB->RouterId,
                  sizeof (pL2vpnIccpPwEntryA->RouterId)) > 0)) ||
        ((pL2vpnIccpPwEntryA->u4RgIndex == pL2vpnIccpPwEntryB->u4RgIndex) &&
         (MEMCMP (&pL2vpnIccpPwEntryA->RouterId, &pL2vpnIccpPwEntryB->RouterId,
                  sizeof (pL2vpnIccpPwEntryA->RouterId)) == 0) &&
         (MEMCMP (&pL2vpnIccpPwEntryA->Fec, &pL2vpnIccpPwEntryB->Fec,
                  sizeof (pL2vpnIccpPwEntryA->Fec)) > 0)))
    {
        return L2VPN_RB_GREATER;
    }
    else if ((pL2vpnIccpPwEntryA->u4RgIndex == pL2vpnIccpPwEntryB->u4RgIndex) &&
             (MEMCMP
              (&pL2vpnIccpPwEntryA->RouterId, &pL2vpnIccpPwEntryB->RouterId,
               sizeof (pL2vpnIccpPwEntryA->RouterId)) == 0)
             &&
             (MEMCMP
              (&pL2vpnIccpPwEntryA->Fec, &pL2vpnIccpPwEntryB->Fec,
               sizeof (pL2vpnIccpPwEntryA->Fec)) == 0))
    {
        return L2VPN_RB_EQUAL;
    }

    return L2VPN_RB_LESSER;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwCmp                                      */
/* Description   : This routine is to do the comparison for the nodes to be  */
/*                 added in ICCP PW entry                                    */
/* Input(s)      : pIccpPw1 - Pointer to the RB Nodes for comparison         */
/*                 pIccpPw2 - pointer to the RB Nodes for comparison         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_RB_GREATER/L2VPN_RB_LESSER/L2VPN_RB_EQUAL           */
/*****************************************************************************/
INT4
L2vpnRedundancyPwCmp (tL2vpnIccpPwEntry * pIccpPw1,
                      tL2vpnIccpPwEntry * pIccpPw2)
{
    tL2vpnPwFec         Fec1 = pIccpPw1->Fec;
    tL2vpnPwFec         Fec2 = pIccpPw2->Fec;
    tPwVcActivePeerSsnEntry *pSession1 = NULL;
    tPwVcActivePeerSsnEntry *pSession2 = NULL;
    INT4                i4CmpVal = 0;

    i4CmpVal = pIccpPw1->u2Priority - pIccpPw2->u2Priority;
    if (i4CmpVal < 0)
    {
        return L2VPN_RB_GREATER;
    }
    else if (i4CmpVal > 0)
    {
        return L2VPN_RB_LESSER;
    }

    i4CmpVal = MEMCMP (&pIccpPw1->RouterId,
                       &pIccpPw2->RouterId, sizeof (pIccpPw1->RouterId));
    if (i4CmpVal < 0)
    {
        return L2VPN_RB_GREATER;
    }
    else if (i4CmpVal > 0)
    {
        return L2VPN_RB_LESSER;
    }

    if (pIccpPw1->Fec.u1Type == L2VPN_FEC_PWID_TYPE)
    {
        i4CmpVal = MEMCMP (pIccpPw1->Fec.u.Fec128.au1PwId,
                           pIccpPw2->Fec.u.Fec128.au1PwId,
                           sizeof (pIccpPw1->Fec.u.Fec128.au1PwId));
    }
    else if (pIccpPw1->Fec.u1Type == L2VPN_FEC_GEN_TYPE &&
             pIccpPw1->b1IsLocal && pIccpPw2->b1IsLocal)
    {
        L2VpnGetPeerSession ((UINT1)L2VPN_PWVC_PEER_ADDR_TYPE
                             (L2VPNRED_ICCP_PW_PARENT_PW
                              (pIccpPw1)->pPwVcEntry),
                             &L2VPN_PWVC_PEER_ADDR
                             (L2VPNRED_ICCP_PW_PARENT_PW
                              (pIccpPw1)->pPwVcEntry), &pSession1);
        L2VpnGetPeerSession ((UINT1)L2VPN_PWVC_PEER_ADDR_TYPE(L2VPNRED_ICCP_PW_PARENT_PW
                                                    (pIccpPw2)->pPwVcEntry),
                             &L2VPN_PWVC_PEER_ADDR (L2VPNRED_ICCP_PW_PARENT_PW
                                                    (pIccpPw2)->pPwVcEntry),
                             &pSession2);

        if (pSession1 != NULL && pSession1->b1IsActive)
        {
            Fec1.u.Fec129.SAii = pIccpPw1->Fec.u.Fec129.TAii;
            Fec1.u.Fec129.TAii = pIccpPw1->Fec.u.Fec129.SAii;
        }
        if (pSession2 != NULL && pSession2->b1IsActive)
        {
            Fec2.u.Fec129.SAii = pIccpPw2->Fec.u.Fec129.TAii;
            Fec2.u.Fec129.TAii = pIccpPw2->Fec.u.Fec129.SAii;
        }

        i4CmpVal = MEMCMP (&Fec1, &Fec2, sizeof (Fec1));
    }

    if (i4CmpVal < 0)
    {
        return L2VPN_RB_GREATER;
    }
    else if (i4CmpVal > 0)
    {
        return L2VPN_RB_LESSER;
    }
    if ((!L2VPN_PWVC_STATUS_IS_UP (pIccpPw2->u4LocalStatus)) ||
        ((pIccpPw2->b1IsLocal == TRUE) && ((tL2vpnRedundancyPwEntry *)
                                           (pIccpPw2->
                                            pRgParent))->u1RowStatus ==
         NOT_IN_SERVICE))
    {
        return L2VPN_RB_GREATER;
    }
    return L2VPN_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name : L2VpnRedundancyTimerEvent                                 */
/* Description   : This routine handles the timeout in PW-RED ICCP           */
/*                 negotiation                                               */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnRedundancyTimerEvent (VOID)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tTmrAppTimer       *pNegTimer = NULL;
    UINT4               u4RgIndex = 0;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY\n");

    if (L2VPN_INITIALISED != TRUE)
    {
        return L2VPN_FAILURE;
    }

    while (TmrGetExpiredTimers (gL2vpnRgGlobals.NegTimerList, &pNegTimer)
           == TMR_SUCCESS)
    {
        u4RgIndex = pNegTimer->u4Data;

        pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
        if (pRgGroup == NULL)
        {
            continue;
        }

        if (pRgGroup->u1Status & L2VPNRED_STATUS_WAITING_TO_RESTORE)
        {
            pRgGroup->u1Status &= (UINT1) (~L2VPNRED_STATUS_WAITING_TO_RESTORE);
        }
        L2VpnPwRedSnmpNotifyNegoFail (pRgGroup->u4Index,
                                      pRgGroup->pOperActivePw,
                                      pRgGroup->u4AdminActivePw);

        L2vpnRedundancyNegotiate (u4RgIndex, FALSE);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 1\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnRedundancyInit                                       */
/* Description   : This routine initializes L2VPN/PW-RED sub-module          */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnRedundancyInit (VOID)
{
    MEMSET (&gL2vpnRgGlobals, 0, sizeof (gL2vpnRgGlobals));

    gL2vpnRgGlobals.NegTimerList = 0;

    gL2vpnRgGlobals.b1MultiHomingEnable = FALSE;
    gL2vpnRgGlobals.u4NegotiationTimeout = 60;
    gL2vpnRgGlobals.b1SyncFailNotifEnable = FALSE;
    gL2vpnRgGlobals.b1PwStatusNotifEnable = FALSE;

    L2VPN_PWRED_STATUS = L2VPN_PWRED_ENABLE_DEFAULT;

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnRedundancyEnable                                     */
/* Description   : This routine reserves resources for L2VPN/PW-RED          */
/*                 sub-module                                                */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnRedundancyEnable (VOID)
{
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY\n");

    gL2vpnRgGlobals.RgList =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tL2vpnRedundancyEntry, RbNode),
                              &L2vpnRedundancyEntryRbCmp);
    if (gL2vpnRgGlobals.RgList == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "INTMD-EXIT 1: "
                   "RG list creation failed\n");
        return L2VPN_FAILURE;
    }
    gL2vpnRgGlobals.RgNodeList =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tL2vpnRedundancyNodeEntry, RbNode),
                              &L2vpnRedundancyNodeEntryRbCmp);
    if (gL2vpnRgGlobals.RgNodeList == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "INTMD-EXIT 2: "
                   "RG node list creation failed\n");
        return L2VPN_FAILURE;
    }
    gL2vpnRgGlobals.RgPwList =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tL2vpnRedundancyPwEntry, RbNode),
                              &L2vpnRedundancyPwEntryRbCmp);
    if (gL2vpnRgGlobals.RgPwList == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "INTMD-EXIT 3: "
                   "RG pw list creation failed\n");
        return L2VPN_FAILURE;
    }
    gL2vpnRgGlobals.RgIccpPwList =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tL2vpnIccpPwEntry, RbNode),
                              &L2vpnIccpPwEntryRbCmp);
    if (gL2vpnRgGlobals.RgIccpPwList == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "INTMD-EXIT 4: "
                   "RG iccp pw list creation failed\n");
        return L2VPN_FAILURE;
    }

    if (TmrCreateTimerList ((const UINT1 *) L2VPN_TSK_NAME,
                            L2VPNRED_NEG_TMR_EVENT, NULL,
                            &gL2vpnRgGlobals.NegTimerList) != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "INTMD-EXIT 5. "
                   "ICCP Timer list creation failed\n");
        return L2VPN_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 6\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnRedundancyDisable                                    */
/* Description   : This routine removes the reservation for resources on     */
/*                 L2VPN/PW-RED sub-module                                   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnRedundancyDisable (VOID)
{
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY\n");

    if (gL2vpnRgGlobals.NegTimerList != 0)
    {
        TmrDeleteTimerList (gL2vpnRgGlobals.NegTimerList);
    }

    if (gL2vpnRgGlobals.RgPwList != NULL)
    {
        RBTreeDestroy (gL2vpnRgGlobals.RgPwList, NULL, 0);
    }
    if (gL2vpnRgGlobals.RgNodeList != NULL)
    {
        RBTreeDestroy (gL2vpnRgGlobals.RgNodeList, NULL, 0);
    }
    if (gL2vpnRgGlobals.RgList != NULL)
    {
        RBTreeDestroy (gL2vpnRgGlobals.RgList, NULL, 0);
    }
    if (gL2vpnRgGlobals.RgIccpPwList != NULL)
    {
        RBTreeDestroy (gL2vpnRgGlobals.RgIccpPwList, NULL, 0);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 1\n");
    return L2VPN_SUCCESS;
}

/**
 * PW Redundancy Group list DB management
 */
/*****************************************************************************/
/* Function Name : L2vpnRedundancyGroupCreate                                */
/* Description   : This routine is to create redundancy group                */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/RgGroupNode if success                      */
/*****************************************************************************/
tL2vpnRedundancyEntry *
L2vpnRedundancyGroupCreate (UINT4 u4RgIndex)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u)\n", u4RgIndex);

    pRgGroup = MemAllocMemBlk (L2VPN_REDUNDANCY_GROUP_POOL_ID);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return NULL;
    }

    MEMSET (pRgGroup, 0, sizeof (*pRgGroup));
    pRgGroup->u4Index = u4RgIndex;
    pRgGroup->u4AdminActivePw = 0;
    pRgGroup->b1ReversionEnable = L2VPN_PWRED_REVERT_DISABLE;
    pRgGroup->u2WaitToRestoreTime = 0;
    pRgGroup->u2HoldOffTime = 0;
    pRgGroup->u1Mode = L2VPNRED_MODE_SWITCH;
    pRgGroup->u1ContentionResolutionMethod = L2VPNRED_CONTENTION_DEFAULT;
    pRgGroup->u1MasterSlaveMode = L2VPNRED_MASTERSLAVE_DEFAULT;
    pRgGroup->u1LockoutProtect = L2VPNRED_LOCKOUT_DEFAULT;
    pRgGroup->u1MultiHomingApps = 0;
    pRgGroup->u4PwGroupId = 0;
    pRgGroup->u1Status = L2VPNRED_STATUS_DEFAULT;
    pRgGroup->u1RowStatus = NOT_IN_SERVICE;

    pRgGroup->u4MessageId = 0;
    pRgGroup->u2NumNode = 0;
    pRgGroup->u2NumPw = 0;
    pRgGroup->u2NumIccpPw = 0;
    pRgGroup->u2NumUpPw = 0;
    pRgGroup->u2NegPriority = L2VPNRED_PW_PRIORITY_MIN;
    pRgGroup->NegTimer.u4Data = u4RgIndex;

    if (RBTreeAdd (gL2vpnRgGlobals.RgList, (tRBElem *) pRgGroup) != RB_SUCCESS)
    {
        MemReleaseMemBlock (L2VPN_REDUNDANCY_GROUP_POOL_ID, (VOID *) pRgGroup);
        return NULL;
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return pRgGroup;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyGroupActivate                              */
/* Description   : This routine is to activate redundancy group              */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyGroupActivate (UINT4 u4RgIndex)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pTmpNode = NULL;
    tL2vpnRedundancyNodeEntry *pNodeRb = NULL;
    tL2vpnRedundancyPwEntry *pTmpPw = NULL;
    tL2vpnRedundancyPwEntry *pPwRb = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u)\n", u4RgIndex);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (pRgGroup->u1RowStatus == ACTIVE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
        return L2VPN_SUCCESS;
    }

    pRgGroup->u1RowStatus = ACTIVE;

    if (pRgGroup->u2NumNode == 0 && pRgGroup->u2NumPw == 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3\n");
        return L2VPN_SUCCESS;
    }

    /* Activate RG PWs */
    pTmpPw = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 4\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
    pTmpPw->u4RgIndex = u4RgIndex;

    while ((pPwRb =
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pTmpPw,
                           NULL)) != NULL)
    {
        if (pPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        L2vpnRedundancyPwStateUpdate (pPwRb, L2VPNRED_EVENT_PW_ACTIVATE);
        MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
        pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
        pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPw);

    /* Activate RG sibling Nodes */
    pTmpNode = MemAllocMemBlk (L2VPN_REDUNDANCY_NODE_POOL_ID);
    if (pTmpNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 5\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
    pTmpNode->u4RgIndex = u4RgIndex;

    while ((pNodeRb = RBTreeGetNext (gL2vpnRgGlobals.RgNodeList,
                                     (tRBElem *) pTmpNode, NULL)) != NULL)
    {
        if (pNodeRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        L2vpnRedundancyNodeStateUpdate (pNodeRb, L2VPNRED_EVENT_NODE_ACTIVATE);
        MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
        pTmpNode->u4RgIndex = u4RgIndex;
        pTmpNode->u1AddrType = pNodeRb->u1AddrType;
        pTmpNode->Addr = pNodeRb->Addr;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pTmpNode);

    /* Negotiate FORWARDING status if there are local PWs */
    if (pRgGroup->u2NumUpPw > 0)
    {
        L2vpnRedundancyNegotiate (u4RgIndex, FALSE);
    }

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 6\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyGroupDeActivate                            */
/* Description   : This routine is to de-activate redundancy group           */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyGroupDeActivate (UINT4 u4RgIndex)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pTmpNode = NULL;
    tL2vpnRedundancyNodeEntry *pNodeRb = NULL;
    tL2vpnRedundancyPwEntry *pTmpPw = NULL;
    tL2vpnRedundancyPwEntry *pPwRb = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u)\n", u4RgIndex);

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (pRgGroup->u1RowStatus != ACTIVE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
        return L2VPN_SUCCESS;
    }

    /* De-activate RG sibling Nodes */
    pTmpNode = MemAllocMemBlk (L2VPN_REDUNDANCY_NODE_POOL_ID);
    if (pTmpNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
    pTmpNode->u4RgIndex = u4RgIndex;

    while ((pNodeRb = RBTreeGetNext (gL2vpnRgGlobals.RgNodeList,
                                     (tRBElem *) pTmpNode, NULL)) != NULL)
    {
        if (pNodeRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        L2vpnRedundancyNodeStateUpdate (pNodeRb,
                                        L2VPNRED_EVENT_NODE_DEACTIVATE);
        MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
        pTmpNode->u4RgIndex = u4RgIndex;
        pTmpNode->u1AddrType = pNodeRb->u1AddrType;
        pTmpNode->Addr = pNodeRb->Addr;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pTmpNode);

    /* De-activate RG PWs */
    pTmpPw = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 4\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
    pTmpPw->u4RgIndex = u4RgIndex;

    while ((pPwRb = RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                                   (tRBElem *) pTmpPw, NULL)) != NULL)
    {
        if (pPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        L2vpnRedundancyPwStateUpdate (pPwRb, L2VPNRED_EVENT_PW_DEACTIVATE);
        MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
        pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
        pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPw);

    pRgGroup->u1RowStatus = NOT_IN_SERVICE;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 5\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyGroupDelete                                */
/* Description   : This routine is to delete redundancy group                */
/* Input(s)      : pRgGroup - pointer to the redundancy group                */
/* Output(s)     : None                                                      */
/* Return(s)     : return l2VPN_SUCCESS/L2VPN_FAILURE s                      */
/*****************************************************************************/
INT4
L2vpnRedundancyGroupDelete (tL2vpnRedundancyEntry * pRgGroup)
{
    UINT4 u4RemainingTime  = L2VPN_ZERO;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. pRgGroup{%u}\n",
                pRgGroup->u4Index);

    if ((pRgGroup->u2NumNode > 0) || (pRgGroup->u2NumPw > 0) ||
        (pRgGroup->u2NumIccpPw > 0))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }
    /* Stop the Timer in RGGroup, if it is running */
    if ((TmrGetRemainingTime (gL2vpnRgGlobals.NegTimerList, 
                              &pRgGroup->NegTimer, &u4RemainingTime)
         == TMR_SUCCESS) && (u4RemainingTime != L2VPN_ZERO))
    {
        TmrStopTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer);
    }
    RBTreeRem (gL2vpnRgGlobals.RgList, (tRBElem *) pRgGroup);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_GROUP_POOL_ID, (VOID *) pRgGroup);
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/**
 * PW Redundancy Node list DB management
 */
/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeCreate                                 */
/* Description   : This routine is to create redundancy Node                 */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/*                 u1AddrType - Address type                                 */
/*                 pAddr      - Node address                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/Redundancy node entry if success            */
/*****************************************************************************/
tL2vpnRedundancyNodeEntry *
L2vpnRedundancyNodeCreate (UINT4 u4RgIndex, UINT1 u1AddrType, uGenAddr * pAddr)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG7 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTRY. NODE IPv4-[%u, %u, %02X%02X%02X%02X]IPv6 -%s  \n",
                u4RgIndex, u1AddrType, pAddr->au1Ipv4Addr[0],
                pAddr->au1Ipv4Addr[1], pAddr->au1Ipv4Addr[2],
                pAddr->au1Ipv4Addr[3], 
                Ip6PrintAddr(&(pAddr->Ip6Addr)));
#else
    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTRY. NODE[%u, %u, %02X%02X%02X%02X]\n",
                u4RgIndex, u1AddrType, pAddr->au1Ipv4Addr[0],
                pAddr->au1Ipv4Addr[1], pAddr->au1Ipv4Addr[2],
                pAddr->au1Ipv4Addr[3]);
#endif
    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return NULL;
    }

    pRgNode = MemAllocMemBlk (L2VPN_REDUNDANCY_NODE_POOL_ID);
    if (pRgNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return NULL;
    }

    MEMSET (pRgNode, 0, sizeof (*pRgNode));
    pRgNode->u4RgIndex = u4RgIndex;
    pRgNode->u1AddrType = u1AddrType;
    pRgNode->Addr = *pAddr;
    pRgNode->u1RowStatus = NOT_IN_SERVICE;

    pRgNode->u2NumIccpPw = 0;
    TMO_DLL_Init_Node (&pRgNode->SessionNode);

    if (RBTreeAdd (gL2vpnRgGlobals.RgNodeList,
                   (tRBElem *) pRgNode) != RB_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3\n");
        MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pRgNode);
        return NULL;
    }
    pRgGroup->u2NumNode++;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 4\n");
    return pRgNode;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeActivate                               */
/* Description   : This routine is to activate redundancy Node               */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/*                 u1AddrType - Address type                                 */
/*                 pAddr      - Node address                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyNodeActivate (UINT4 u4RgIndex, UINT1 u1AddrType,
                             uGenAddr * pAddr)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;

#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG7 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTRY. NODE IPv4-[%u, %u, %02X%02X%02X%02X]IPv6"
                " -%s\n",
                u4RgIndex, u1AddrType, pAddr->au1Ipv4Addr[0],
                pAddr->au1Ipv4Addr[1], pAddr->au1Ipv4Addr[2],
                pAddr->au1Ipv4Addr[3],
                Ip6PrintAddr(&(pAddr->Ip6Addr)));
#else
    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTRY. NODE[%u, %u, %02X%02X%02X%02X]\n",
                u4RgIndex, u1AddrType, pAddr->au1Ipv4Addr[0],
                pAddr->au1Ipv4Addr[1], pAddr->au1Ipv4Addr[2],
                pAddr->au1Ipv4Addr[3]);
#endif
    pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4RgIndex, u1AddrType, pAddr);
    if (pRgNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    pRgNode->u1RowStatus = ACTIVE;
    L2vpnRedundancyNodeStateUpdate (pRgNode, L2VPNRED_EVENT_NODE_ACTIVATE);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeDeActivate                             */
/* Description   : This routine is to de-activate redundancy Node            */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/*                 u1AddrType - Address type                                 */
/*                 pAddr      - Node address                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyNodeDeActivate (UINT4 u4RgIndex, UINT1 u1AddrType,
                               uGenAddr * pAddr)
{
    tL2vpnRedundancyNodeEntry *pRgNode = NULL;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG7 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTRY. NODE IPv4-[%u, %u, %02X%02X%02X%02X]"
                 "IPv6 -[%s] \n",
                u4RgIndex, u1AddrType, pAddr->au1Ipv4Addr[0],
                pAddr->au1Ipv4Addr[1], pAddr->au1Ipv4Addr[2],
                pAddr->au1Ipv4Addr[3],
                Ip6PrintAddr(&(pAddr->Ip6Addr)));

#else
    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                "ENTRY. NODE[%u, %u, %02X%02X%02X%02X]\n",
                u4RgIndex, u1AddrType, pAddr->au1Ipv4Addr[0],
                pAddr->au1Ipv4Addr[1], pAddr->au1Ipv4Addr[2],
                pAddr->au1Ipv4Addr[3]);
#endif
    pRgNode = L2VpnGetRedundancyNodeEntryByIndex (u4RgIndex, u1AddrType, pAddr);
    if (pRgNode == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    L2vpnRedundancyNodeStateUpdate (pRgNode, L2VPNRED_EVENT_NODE_DEACTIVATE);
    pRgNode->u1RowStatus = NOT_IN_SERVICE;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyNodeDelete                                 */
/* Description   : This routine is to de-activate redundancy Node            */
/* Input(s)      : pRgNode - pointer to the redundancy node                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyNodeDelete (tL2vpnRedundancyNodeEntry * pRgNode)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG7 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. NODE IPv4- [%u, %u, "
                " %02X%02X%02X%02X ] IPv6 -%s\n",
                pRgNode->u4RgIndex, pRgNode->u1AddrType,
                pRgNode->Addr.au1Ipv4Addr[0], pRgNode->Addr.au1Ipv4Addr[1],
                pRgNode->Addr.au1Ipv4Addr[2], pRgNode->Addr.au1Ipv4Addr[3],
                Ip6PrintAddr(&(pRgNode->Addr.Ip6Addr)));


#else
    L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. NODE[%u, %u, "
                " %02X%02X%02X%02X \n",
                pRgNode->u4RgIndex, pRgNode->u1AddrType,
                pRgNode->Addr.au1Ipv4Addr[0], pRgNode->Addr.au1Ipv4Addr[1],
                pRgNode->Addr.au1Ipv4Addr[2], pRgNode->Addr.au1Ipv4Addr[3]);
#endif
    if (pRgNode->u1RowStatus == ACTIVE || pRgNode->u2NumIccpPw > 0)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgNode->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return L2VPN_FAILURE;
    }

    pRgGroup->u2NumNode--;
    RBTreeRem (gL2vpnRgGlobals.RgNodeList, (tRBElem *) pRgNode);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pRgNode);
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3\n");
    return L2VPN_SUCCESS;
}

/**
 * PW Redundancy PW list DB management
 */
/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwCreate                                   */
/* Description   : This routine is to create redundancy PW                   */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/*                 pPwVcEntry - pointer to the PW VC Entry                   */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/Redundancy PW entry if success              */
/*****************************************************************************/
tL2vpnRedundancyPwEntry *
L2vpnRedundancyPwCreate (UINT4 u4RgIndex, tPwVcEntry * pPwVcEntry)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    if (pPwVcEntry == NULL)
    {
        return NULL;
    }

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u), "
                "pPwVcEntry{%u}\n", u4RgIndex, L2VPN_PWVC_INDEX (pPwVcEntry));

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return NULL;
    }

    pRgPw = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return NULL;
    }

    MEMSET (pRgPw, 0, sizeof (*pRgPw));
    pRgPw->u4RgIndex = u4RgIndex;
    pRgPw->u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);    /* PW shall be already validated to be present */
    pRgPw->u4PwVcId = L2VPN_PWVC_ID (pPwVcEntry);
    pRgPw->u1Preference = L2VPNRED_PW_PREFERENCE_DEFAULT;
    pRgPw->u1RowStatus = NOT_IN_SERVICE;

    pRgPw->pPwVcEntry = pPwVcEntry;

    if (RBTreeAdd (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw) != RB_SUCCESS)
    {
        MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pRgPw);
        return NULL;
    }
    pRgGroup->u2NumPw++;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3\n");
    return pRgPw;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwActivate                                 */
/* Description   : This routine is to activate redundancy PW                 */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/*                 pPwVcEntry - pointer to the PW VC Entry                   */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyPwActivate (UINT4 u4RgIndex, tPwVcEntry * pPwVcEntry)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u), "
                "pPwVcEntry{%u}\n", u4RgIndex, L2VPN_PWVC_INDEX (pPwVcEntry));

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4RgIndex,
                                              L2VPN_PWVC_INDEX (pPwVcEntry));
    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    pRgPw->u1RowStatus = ACTIVE;
    L2vpnRedundancyPwStateUpdate (pRgPw, L2VPNRED_EVENT_PW_ACTIVATE);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwDeActivate                               */
/* Description   : This routine is to de-activate redundancy PW              */
/* Input(s)      : u4RgIndex - redundancy group Index                        */
/*                 pPwVcEntry - pointer to the PW VC Entry                   */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyPwDeActivate (UINT4 u4RgIndex, tPwVcEntry * pPwVcEntry)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    if (pPwVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u), "
                "u4PwIndex{%u}\n", u4RgIndex, pPwVcEntry->u4PwVcIndex);

    pRgPw = L2VpnGetRedundancyPwEntryByIndex (u4RgIndex,
                                              pPwVcEntry->u4PwVcIndex);
    if (pRgPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    L2vpnRedundancyPwStateUpdate (pRgPw, L2VPNRED_EVENT_PW_DEACTIVATE);
    pRgPw->u1RowStatus = NOT_IN_SERVICE;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyPwDelete                                   */
/* Description   : This routine is to delete redundancy PW                   */
/* Input(s)      : pRgPw - pointer to the Redunancy PW entry                 */
/*                 pPwVcEntry - pointer to the PW VC Entry                   */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2vpnRedundancyPwDelete (tL2vpnRedundancyPwEntry * pRgPw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. pRgPw{.u4RgIndex(%u), "
                ".u4PwIndex(%u)}\n", pRgPw->u4RgIndex, pRgPw->u4PwIndex);

    if (pRgPw->u1RowStatus == ACTIVE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return L2VPN_FAILURE;
    }

    pRgGroup->u2NumPw--;
    RBTreeRem (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pRgPw);
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 3\n");
    return L2VPN_SUCCESS;
}

/**
 * PW Redundancy ICCP PW list DB management
 */
/*****************************************************************************/
/* Function Name : L2vpnRedundancyIccpPwCreate                               */
/* Description   : This routine is to crate ICCP PW                          */
/* Input(s)      : pRgPw - pointer to the Redunancy PW entry                 */
/*                 pRgNode - pointer to the Redundancy node                  */
/*                 pIccpPw - pointer to the ICCP PW entry                    */
/* Output(s)     : None                                                      */
/* Return(s)     : NULL if fails/ICCP Pw entry if success                    */
/*****************************************************************************/
tL2vpnIccpPwEntry  *
L2VpnRedundancyIccpPwCreate (tL2vpnRedundancyPwEntry * pRgPw,
                             tL2vpnRedundancyNodeEntry * pRgNode,
                             tL2vpnIccpPwEntry * pIccpPw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;
    tPwVcActivePeerSsnEntry *pSession = NULL;
    UINT4               u4RgIndex = L2VPN_ZERO;
    UINT4               u4Temp = L2VPN_ZERO;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY\n");

    u4RgIndex = pRgPw != NULL ? pRgPw->u4RgIndex : pRgNode->u4RgIndex;

    pRgGroup = L2VpnGetRedundancyEntryByIndex (u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return NULL;
    }

    pRgIccpPw = MemAllocMemBlk (L2VPN_ICCP_PW_POOL_ID);
    if (pRgIccpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 2\n");
        return NULL;
    }

    MEMSET (pRgIccpPw, 0, sizeof (*pRgIccpPw));

    if (pRgPw != NULL)
    {
        pRgIccpPw->u4RgIndex = u4RgIndex;

        L2VpnGetPeerSession ((UINT1)L2VPN_PWVC_PEER_ADDR_TYPE (pRgPw->pPwVcEntry), 
                             &L2VPN_PWVC_PEER_ADDR (pRgPw->pPwVcEntry),
                             &pSession);
        if (pSession == NULL || pSession->u4Status != L2VPN_SESSION_UP)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 3. "
                       "LDP SESSION not OPER.UP\n");
            MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pRgIccpPw);
            return NULL;
        }

        if (L2VPN_PWVC_OWNER (pRgPw->pPwVcEntry) ==
            L2VPN_PWVC_OWNER_PWID_FEC_SIG)
        {
            pRgIccpPw->Fec.u1Type = L2VPN_FEC_PWID_TYPE;

            u4Temp = OSIX_HTONL (pSession->u4PeerLdpId);
            MEMCPY (pRgIccpPw->Fec.u.Fec128.au1PeerId, &u4Temp, sizeof (UINT4));

            u4Temp = OSIX_HTONL (pRgPw->pPwVcEntry->u4LocalGroupID);
            MEMCPY (pRgIccpPw->Fec.u.Fec128.au1GroupId, &u4Temp,
                    sizeof (UINT4));

            u4Temp = OSIX_HTONL (pRgPw->pPwVcEntry->u4PwVcID);
            MEMCPY (pRgIccpPw->Fec.u.Fec128.au1PwId, &u4Temp, sizeof (UINT4));
        }
        else if (L2VPN_PWVC_OWNER (pRgPw->pPwVcEntry) ==
                 L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            pRgIccpPw->Fec.u1Type = L2VPN_FEC_GEN_TYPE;
            pRgIccpPw->Fec.u.Fec129.Agi.u1Type = (UINT1)
                L2VPN_PWVC_GEN_AGI_TYPE (pRgPw->pPwVcEntry);
            if (pRgIccpPw->Fec.u.Fec129.Agi.u1Type == L2VPN_GEN_FEC_AGI_TYPE_1)
            {
                MEMCPY (&pRgIccpPw->Fec.u.Fec129.Agi.u,
                        pRgPw->pPwVcEntry->au1Agi,
                        sizeof (pRgIccpPw->Fec.u.Fec129.Agi.u.au1Agi1));
            }

            pRgIccpPw->Fec.u.Fec129.SAii.u1Type = (UINT1)
                L2VPN_PWVC_GEN_LCL_AII_TYPE (pRgPw->pPwVcEntry);
            if (pRgIccpPw->Fec.u.Fec129.SAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_1)
            {
                MEMCPY (&pRgIccpPw->Fec.u.Fec129.SAii.u.au1Aii1,
                        pRgPw->pPwVcEntry->au1Saii,
                        sizeof (pRgIccpPw->Fec.u.Fec129.SAii.u.au1Aii1));
            }
            else if (pRgIccpPw->Fec.u.Fec129.SAii.u1Type ==
                     L2VPN_GEN_FEC_AII_TYPE_2)
            {
                MEMCPY (&pRgIccpPw->Fec.u.Fec129.SAii.u.au1Aii2,
                        pRgPw->pPwVcEntry->au1Saii,
                        sizeof (pRgIccpPw->Fec.u.Fec129.SAii.u.au1Aii2));
            }

            pRgIccpPw->Fec.u.Fec129.TAii.u1Type = (UINT1)
                L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pRgPw->pPwVcEntry);
            if (pRgIccpPw->Fec.u.Fec129.TAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_1)
            {
                MEMCPY (&pRgIccpPw->Fec.u.Fec129.TAii.u.au1Aii1,
                        pRgPw->pPwVcEntry->au1Taii,
                        sizeof (pRgIccpPw->Fec.u.Fec129.TAii.u.au1Aii1));
            }
            else if (pRgIccpPw->Fec.u.Fec129.TAii.u1Type ==
                     L2VPN_GEN_FEC_AII_TYPE_2)
            {
                MEMCPY (&pRgIccpPw->Fec.u.Fec129.TAii.u.au1Aii2,
                        pRgPw->pPwVcEntry->au1Taii,
                        sizeof (pRgIccpPw->Fec.u.Fec129.TAii.u.au1Aii2));
            }
        }

        u4Temp = OSIX_HTONL (pSession->u4LocalLdpEntityID);
        MEMCPY (pRgIccpPw->RoId.au1NodeId, &u4Temp, sizeof (UINT4));

        u4Temp = OSIX_HTONL (pRgPw->pPwVcEntry->u4PwVcIndex);
        MEMCPY (pRgIccpPw->RoId.au1PwIndex, &u4Temp, sizeof (UINT4));

        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);

        u4Temp = OSIX_HTONL (pSession->u4LocalLdpEntityID);
        MEMCPY (&pRgIccpPw->RouterId, &u4Temp, sizeof (UINT4));

        pRgIccpPw->b1IsLocal = TRUE;
        pRgIccpPw->pRgParent = pRgPw;
        pRgPw->pIccpPwEntry = pRgIccpPw;

        pRgIccpPw->u2Priority = (UINT2) L2VPNRED_PW_PRIORITY
            (L2VPN_PWVC_HOLDING_PRIORITY (pRgPw->pPwVcEntry),
             pRgPw->u1Preference);
        MEMCPY (pRgIccpPw->Name, pRgPw->pPwVcEntry->au1PwVcName,
                sizeof (pRgIccpPw->Name));
    }
    else if (pRgNode != NULL)
    {
        *pRgIccpPw = *pIccpPw;

        pRgIccpPw->u4RgIndex = u4RgIndex;
        u4Temp = OSIX_HTONL (pRgNode->pSession->u4PeerLdpId);
        MEMCPY (&pRgIccpPw->RouterId, &u4Temp, sizeof (UINT4));

        pRgIccpPw->b1IsLocal = FALSE;
        pRgIccpPw->pRgParent = pRgNode;
        pRgNode->u2NumIccpPw++;
    }

    if (RBTreeAdd (gL2vpnRgGlobals.RgIccpPwList,
                   (tRBElem *) pRgIccpPw) != RB_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 4\n");
        MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pRgIccpPw);
        return NULL;
    }
    pRgGroup->u2NumIccpPw++;

    L2VPN_DBG9 (L2VPN_DBG_LVL_DBG_FLAG,
                "pRgIccpPw(%p){.RoId(%02X%02X%02X%02X _ %02X%02X%02X%02X)",
                pRgIccpPw, pRgIccpPw->RoId.au1NodeId[0],
                pRgIccpPw->RoId.au1NodeId[1],
                pRgIccpPw->RoId.au1NodeId[2],
                pRgIccpPw->RoId.au1NodeId[3],
                pRgIccpPw->RoId.au1PwIndex[0],
                pRgIccpPw->RoId.au1PwIndex[1],
                pRgIccpPw->RoId.au1PwIndex[2], pRgIccpPw->RoId.au1PwIndex[3]);

    if (pRgPw != NULL)
    {
        L2VPN_DBG6 (L2VPN_DBG_LVL_DBG_FLAG,
                    "FEC            : {%u, %u}::%02X%02X%02X%02X. EXIT 5\n",
                    pRgPw->pPwVcEntry->u4PwVcID,
                    pRgPw->pPwVcEntry->u4LocalGroupID,
                    pRgIccpPw->Fec.u.Fec128.au1PeerId[0],
                    pRgIccpPw->Fec.u.Fec128.au1PeerId[1],
                    pRgIccpPw->Fec.u.Fec128.au1PeerId[2],
                    pRgIccpPw->Fec.u.Fec128.au1PeerId[3]);
    }
    return pRgIccpPw;
}

/*****************************************************************************/
/* Function Name : L2vpnRedundancyIccpPwDelete                               */
/* Description   : This routine is to delete ICCP PW                         */
/* Input(s)      : pRgIccpPw - pointer to the ICCP PW entry                  */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnRedundancyIccpPwDelete (tL2vpnIccpPwEntry * pRgIccpPw)
{
    tL2vpnRedundancyEntry *pRgGroup = NULL;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY\n");

    pRgGroup = L2VpnGetRedundancyEntryByIndex (pRgIccpPw->u4RgIndex);
    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    if (pRgIccpPw->b1IsLocal == TRUE)
    {
        L2VPNRED_ICCP_PW_PARENT_PW (pRgIccpPw)->pIccpPwEntry = NULL;
    }
    else
    {
        L2VPNRED_ICCP_PW_PARENT_NODE (pRgIccpPw)->u2NumIccpPw--;
    }
    pRgGroup->u2NumIccpPw--;

    RBTreeRem (gL2vpnRgGlobals.RgIccpPwList, (tRBElem *) pRgIccpPw);

    MemReleaseMemBlock (L2VPN_ICCP_PW_POOL_ID, (VOID *) pRgIccpPw);
    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpPwRedProcessAcUpEvent                                 */
/* Description   : This routine processes If up event                        */
/* Input(s)      : u4IfIndex - Index of interface which has come up          */
/*                 u2PortVlan - AC L2 VLAN is down                           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpPwRedProcessAcUpEvent (UINT4 u4IfIndex, UINT2 u2PortVlan)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyEntry *pRgGrp = NULL;
    UINT4               u4HashIndex;

    u4HashIndex = L2VPN_COMPUTE_HASH_INDEX (u4IfIndex);
    TMO_HASH_Scan_Bucket (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pPwVcEnetEntry = MPLS_PWVC_FROM_ENETLIST (pNode);
        pPwVcEntry = pPwVcEnetEntry->pPwVcEntry;

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnProcessIfUpEvent : "
                    "Process If Up Event for the Pw %d  and Port or Vlan %d\r\n",
                    pPwVcEntry->u4PwVcIndex, u2PortVlan);

        if (L2VpnUtilIsPwAcMatched (pPwVcEntry, pPwVcEnetEntry,
                                    u4IfIndex, u2PortVlan) == FALSE)
        {
            continue;
        }

        /* Update RG ICCP PW entry */

        pRgPw =
            L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX (pPwVcEntry));
        if (pRgPw != NULL)
        {

            pRgGrp = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
            if (L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) ==
                L2VPN_PWVC_STATUS_STANDBY)
            {
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                    (~L2VPN_PWVC_STATUS_STANDBY);
                /* the AC status has changed from standby to active 
                   hence notify it to the SNMP Manager */
                L2VpnPwRedSnmpNotifyRoleChng (pRgPw->u4RgIndex,
                                              pRgPw->u4PwIndex,
                                              L2VPN_TRAP_DLAG_STANDBY_TO_ACTIVE,
                                              L2VPN_PWVC_LOCAL_STATUS
                                              (pRgPw->pPwVcEntry),
                                              L2VPN_PWVC_REMOTE_STATUS
                                              (pRgPw->pPwVcEntry));
            }

            if ((NULL != pRgGrp) &&
                (pRgGrp->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_MASTERSLAVE)
                && (pRgGrp->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
            {
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                    (~L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT);
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                    L2VPN_PWVC_STATUS_STANDBY;

            }
            L2vpnRedundancyPwStateUpdate (pRgPw,
                                          L2VPNRED_EVENT_PW_STATE_CHANGE);
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpPwRedProcessAcDownEvent                               */
/* Description   : This routine processes If up event                        */
/* Input(s)      : u4IfIndex - Index of interface which has come up          */
/*                 u2PortVlan - AC L2 VLAN is down                           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpPwRedProcessAcDownEvent (UINT4 u4IfIndex, UINT2 u2PortVlan)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyEntry *pRgGrp = NULL;
    UINT4               u4HashIndex;

    u4HashIndex = L2VPN_COMPUTE_HASH_INDEX (u4IfIndex);
    TMO_HASH_Scan_Bucket (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                          u4HashIndex, pNode, tTMO_HASH_NODE *)
    {
        pPwVcEnetEntry = MPLS_PWVC_FROM_ENETLIST (pNode);
        pPwVcEntry = pPwVcEnetEntry->pPwVcEntry;

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnProcessIfUpEvent : "
                    "Process If Up Event for the Pw %d  and Port or Vlan %d\r\n",
                    pPwVcEntry->u4PwVcIndex, u2PortVlan);

        if (L2VpnUtilIsPwAcMatched (pPwVcEntry, pPwVcEnetEntry,
                                    u4IfIndex, u2PortVlan) == FALSE)
        {
            continue;
        }

        /* Update RG ICCP PW entry */

        pRgPw =
            L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX (pPwVcEntry));
        if (pRgPw != NULL)
        {
            pRgGrp = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
            if ((NULL != pRgGrp) &&
                (pRgGrp->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_MASTERSLAVE)
                && (pRgGrp->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
            {
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                    L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT;

            }
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                L2VPN_PWVC_STATUS_STANDBY;

            /* the AC status has changed from standby to active 
               hence notify it to the SNMP Manager */
            L2VpnPwRedSnmpNotifyRoleChng (pRgPw->u4RgIndex,
                                          pRgPw->u4PwIndex,
                                          L2VPN_TRAP_DLAG_ACTIVE_TO_STANDBY,
                                          L2VPN_PWVC_LOCAL_STATUS
                                          (pRgPw->pPwVcEntry),
                                          L2VPN_PWVC_REMOTE_STATUS
                                          (pRgPw->pPwVcEntry));
            L2vpnRedundancyPwStateUpdate (pRgPw,
                                          L2VPNRED_EVENT_PW_STATE_CHANGE);
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnRedundancyResetOtherStandbyPw                        */
/* Description   : This routine resets other PW otner than the PW which has 
 *                 receive remote status as standby                          */
/* Input(s)      : u4RgIndex - Redundancy group index                        */
/*                 u4PwIndex - Pseudowire which is the exception             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/

INT4
L2VpnRedundancyResetOtherStandbyPw (UINT4 u4RgIndex, UINT4 u4PwIndex)
{
    tL2vpnRedundancyPwEntry *pTmpPw = NULL;
    tL2vpnRedundancyPwEntry *pPwRb = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "ENTRY. u4RgIndex(%u)\n", u4RgIndex);

    /* Update PW local status & Notify remote peer */
    pTmpPw = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPw == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "INTMD-EXIT 1\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
    pTmpPw->u4RgIndex = u4RgIndex;

    while ((pPwRb = RBTreeGetNext (gL2vpnRgGlobals.RgPwList,
                                   (tRBElem *) pTmpPw, NULL)) != NULL)
    {
        if (pPwRb->u4RgIndex != u4RgIndex)
        {
            break;
        }

        if ((pPwRb->pIccpPwEntry == NULL) || (pPwRb->u4PwIndex == u4PwIndex))
        {
            MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
            pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
            pTmpPw->u4PwIndex = pPwRb->u4PwIndex;
            continue;
        }
        L2VPN_PWVC_LOCAL_STATUS (pPwRb->pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_STANDBY);
        pPwRb->pIccpPwEntry->u4LocalStatus =
            L2VPN_PWVC_LOCAL_STATUS (pPwRb->pPwVcEntry);
        pPwRb->pIccpPwEntry->u2Status &=
            (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
        MEMSET (pTmpPw, 0, sizeof (*pTmpPw));
        pTmpPw->u4RgIndex = pPwRb->u4RgIndex;
        pTmpPw->u4PwIndex = pPwRb->u4PwIndex;

    }
    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPw);

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "EXIT 2\n");
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpPwRedActionforAwaitingPw                              */
/* Description   : This routine takes action when a PW which is in awaiting state
/                  receives some informationm                            *    */
/* Input(s)      : pRgIccpPw                                                  */
/*                 pRgPw                                                     */
/*                 pRgGroup                                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpPwRedActionforAwaitingPw (tL2vpnIccpPwEntry * pRgIccpPw,
                              tL2vpnRedundancyPwEntry * pRgPw,
                              tL2vpnRedundancyEntry * pRgGroup,
                              BOOL1 * b1DataUpdated)
{
    if ((pRgGroup->u1ContentionResolutionMethod ==
         L2VPNRED_CONTENTION_MASTERSLAVE) &&
        (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
    {
        if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                         (pRgPw->pPwVcEntry)))
        {
            pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_REMOTE_FORWARD;
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_FORWARD;
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &=
                (UINT1) (~L2VPN_PWVC_STATUS_STANDBY);
            if (pRgGroup->pOperActivePw != pRgIccpPw)
            {
                pRgGroup->pOperActivePw = pRgIccpPw;
            }
        }
        else
        {
            pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_REMOTE_STANDBY;
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                L2VPN_PWVC_STATUS_STANDBY;
        }

        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
        if (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED))
        {
            L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry, pRgGroup);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;
        }
        *b1DataUpdated = TRUE;
    }
    if ((pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_FORWARD ||
         pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_STANDBY) &&
        L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC (L2VPN_PWVC_LOCAL_STATUS
                                              (pRgPw->pPwVcEntry),
                                              L2VPN_PWVC_REMOTE_STATUS
                                              (pRgPw->pPwVcEntry)))
    {
        if (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_REMOTE_SWITCHOVER))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW STATE SYNC. "
                        "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                        pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);
        }
        else
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG,
                        "PW REMOTE SWITCHOVER COMPLETE. "
                        "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                        pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);
        }
        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
        pRgGroup->u2NumPwAwaiting--;

        if ((pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_FORWARD) &&
            L2VPN_PWVC_STATUS_IS_ACTIVE (pRgIccpPw->u4LocalStatus))
        {
            if ((pRgGroup->pNegForwardingPw == pRgIccpPw) ||
                (pRgGroup->pOperActivePw == NULL))
            {
                pRgGroup->pOperActivePw = pRgIccpPw;
                pRgGroup->pNegForwardingPw = NULL;
                pRgGroup->u1Status = L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            }
        }
        else if ((pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_STANDBY) &&
                 !L2VPN_PWVC_STATUS_IS_ACTIVE (pRgIccpPw->u4LocalStatus))
        {
            if (pRgGroup->pOperActivePw == pRgIccpPw)
            {
                pRgGroup->pOperActivePw = NULL;
                pRgGroup->u1Status |= L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE;
            }
        }

        if (!(pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_UPDATED))
        {
            L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry, pRgGroup);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;
        }

        if (pRgGroup->u2NumPwAwaiting == 0)
        {
            TmrStopTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer);
            if (pRgGroup->pNegForwardingPw != NULL &&
                L2VPN_PWVC_STATUS_IS_FORWARDING (pRgGroup->
                                                 pNegForwardingPw->
                                                 u4LocalStatus,
                                                 pRgGroup->pNegForwardingPw->
                                                 u4RemoteStatus))
            {
                pRgGroup->pOperActivePw = pRgGroup->pNegForwardingPw;
                pRgGroup->pNegForwardingPw = NULL;
            }
            pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            pRgGroup->u1Status &=
                (UINT1) (~L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE);
        }
        *b1DataUpdated = TRUE;
    }
    else if ((pRgIccpPw->u2Status & L2VPNRED_PW_STATUS_LOCAL_SWITCHOVER) &&
             !L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                           (pRgPw->pPwVcEntry))
             &&
             L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                          (pRgPw->pPwVcEntry)))
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW LOCAL SWITCHOVER COMPLETE. "
                    "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                    pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

        /* pRgIccpPw->u1Status &= ~L2VPNRED_PW_STATUS_LOCAL_SWITCHOVER; */
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_UPDATED);
        pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_FORWARD;
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &=
            (UINT1) (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &=
            (UINT1) (~L2VPN_PWVC_STATUS_STANDBY);
        pRgGroup->u2NumPwAwaiting--;

        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);

        pRgGroup->pNegSwitchoverPw = NULL;
        pRgGroup->pOperActivePw = pRgIccpPw;

        L2vpnFmRedundancyPwForwardingUpdate (pRgPw->pPwVcEntry, pRgGroup);
        pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;

        L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);

        if (pRgGroup->u2NumPwAwaiting == 0)
        {
            TmrStopTimer (gL2vpnRgGlobals.NegTimerList, &pRgGroup->NegTimer);
            pRgGroup->u1Status |= L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            pRgGroup->u1Status &=
                (UINT1) (~L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE);
        }
        *b1DataUpdated = TRUE;
    }
}

/*****************************************************************************/
/* Function Name : L2VpPwRedActionforAwaitingPw                              */
/* Description   : This routine handles the actions to be taken whenever     */
/*                 a remote node sents S/W over bit set                      */
/* Input(s)      : pRgIccpPw                                                 */
/*                 pRgPw                                                     */
/*                 pRgGroup                                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpPwRedHandleRemoteSWOver (tL2vpnIccpPwEntry * pRgIccpPw,
                             tL2vpnRedundancyPwEntry * pRgPw,
                             tL2vpnRedundancyEntry * pRgGroup,
                             BOOL1 * b1DataUpdated)
{

    tL2vpnRedundancyPwEntry *pRgPwNeg = NULL;
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW REMOTE SWITCHOVER START. "
                "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

    if (pRgGroup->pOperActivePw != NULL)
    {
        /*send an update status notification message with the
           'Preferential Forwarding' status bit set in the
           previously active PW */
        if ((pRgGroup->pOperActivePw->b1IsLocal) &&
            (pRgGroup->pOperActivePw != pRgIccpPw))
        {
            pRgPwNeg = L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw);
            pRgPwNeg->pIccpPwEntry->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
            L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry) |=
                L2VPN_PWVC_STATUS_STANDBY;
            pRgPwNeg->pIccpPwEntry->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPwNeg->pPwVcEntry);
            pRgPwNeg->pIccpPwEntry->u4LocalStatus =
                L2VPN_PWVC_LOCAL_STATUS (pRgPwNeg->pPwVcEntry);
            L2VpnSendLdpPwVcNotifMsg (pRgPwNeg->pPwVcEntry);
            /*send an update status notification message with the
               'Preferential Forwarding' status bit clear and the
               'request switchover' bit reset on the newly active PW */
            pRgGroup->pOperActivePw = pRgIccpPw;
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &=
                (UINT1) (~L2VPN_PWVC_STATUS_STANDBY);
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &=
                (UINT1) (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_FORWARD;
            pRgIccpPw->u4LocalStatus =
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            pRgGroup->u1Status = L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            L2VpnSendLdpPwVcNotifMsg (pRgPwNeg->pPwVcEntry);
        }
        /* The pseudowire for which the request switchover was sent is already active
           hence send an notification and remain active */
        else if (pRgGroup->pOperActivePw == pRgIccpPw)
        {
            pRgPwNeg = L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw);
            L2VpnSendLdpPwVcNotifMsg (pRgPwNeg->pPwVcEntry);
            pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry); 
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        }
    }
    else
    {
        if (pRgGroup->u1ContentionResolutionMethod ==
            L2VPNRED_CONTENTION_INDEPENDENT)
        {
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_FORWARD;
            pRgIccpPw->u4LocalStatus =
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
            pRgGroup->u1Status = L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
            pRgGroup->pOperActivePw = pRgIccpPw;
            L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
        }
        else
        {
            L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
            pRgIccpPw->u4LocalStatus =
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        }
    }
    *b1DataUpdated = TRUE;
}

/*****************************************************************************/
/* Function Name : L2VpPwRedHandleChngInLocalStatus                          */
/* Description   : This routine is used to handle the procedures to be done  */
/*                 whenever there is a change in the received remote status  */
/* Input(s)      : pRgIccpPw                                                  */
/*                 pRgPw                                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpPwRedHandleChngInLocalStatus (tL2vpnIccpPwEntry * pRgIccpPw,
                                  tL2vpnRedundancyPwEntry * pRgPw)
{
    /*There is a change in the local status initmate it to the neighbor */
    pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);

    if (!L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                      (pRgPw->pPwVcEntry)))
    {
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_FORWARD);
        pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_STANDBY;
    }
    else if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                          (pRgPw->pPwVcEntry)))
    {
        pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
        pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_FORWARD;
    }
    L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);

    L2vpnRedundancyIccpNodeNotify (pRgIccpPw->u4RgIndex, pRgIccpPw,
                                   L2VPNRED_ICCP_DATA_STATE,
                                   L2VPNRED_ICCP_REQ_STATE |
                                   L2VPNRED_ICCP_REQ_ALL);
    return;
}

/*****************************************************************************/
/* Function Name : L2VpPwRedHandleChngInRemoteStatus                         */
/* Description   : This routine is used to handle the procedures to be done  */
/*                 whenever there is a change in the received remote status  */
/* Input(s)      : pRgIccpPw                                                 */
/*                 pRgPw                                                     */
/*                 pRgGroup                                                  */
/* Output(s)     : b1ReNegotiate                                             */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpPwRedHandleChngInRemoteStatus (tL2vpnIccpPwEntry * pRgIccpPw,
                                   tL2vpnRedundancyPwEntry * pRgPw,
                                   tL2vpnRedundancyEntry * pRgGroup,
                                   BOOL1 * b1ReNegotiate)
{
    if ((pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE) ||
        (pRgGroup->u1ContentionResolutionMethod ==
         L2VPNRED_CONTENTION_INDEPENDENT))

    {
        if (!(L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                           (pRgPw->pPwVcEntry))))
        {
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            pRgIccpPw->u2Status = (UINT2) (L2VPNRED_PW_STATUS_REMOTE_STANDBY |
                                           (pRgIccpPw->
                                            u2Status &
                                            ~L2VPNRED_PW_STATUS_REMOTE_FORWARD));
            if (pRgGroup->u1ContentionResolutionMethod ==
                L2VPNRED_CONTENTION_MASTERSLAVE)
            {
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
                    L2VPN_PWVC_STATUS_STANDBY;
                pRgGroup->u1Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
            }

            if ((pRgGroup->pOperActivePw == NULL) ||
                (pRgGroup->pOperActivePw == pRgIccpPw))
            {
                pRgGroup->pOperActivePw = NULL;
                pRgGroup->u1Status = L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE;
                if (L2VPN_PWVC_MODE (pRgPw->pPwVcEntry) == L2VPN_VPLS)
                { 
                    L2VpnPwRedVplsHwModify (pRgPw->pPwVcEntry, L2VPN_TRUE, NULL);
                }
                if ((pRgGroup->u1ContentionResolutionMethod ==
                            L2VPNRED_CONTENTION_INDEPENDENT) &&
                    (pRgGroup->u2NumUpPw > 1))
                {
                    L2VpnRedundancyResetOtherStandbyPw (pRgPw->u4RgIndex,
                                                        pRgPw->u4PwIndex);
                    *b1ReNegotiate = TRUE;
                }
            }
        }
        else if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                              (pRgPw->pPwVcEntry)))
        {
            if ((pRgGroup->u1ContentionResolutionMethod ==
                 L2VPNRED_CONTENTION_MASTERSLAVE) &&
                (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
            {
                L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                    (~L2VPN_PWVC_STATUS_STANDBY);
                pRgIccpPw->u4LocalStatus &= (UINT1) 
                    (~L2VPN_PWVC_STATUS_STANDBY);
                pRgIccpPw->u2Status &=
                    (UINT2) (~L2VPNRED_PW_STATUS_LOCAL_STANDBY);
                pRgIccpPw->u2Status &=
                    (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_AWAITED);
                pRgIccpPw->u2Status =
                    (L2VPNRED_PW_STATUS_LOCAL_FORWARD |
                     L2VPNRED_PW_STATUS_REMOTE_FORWARD);
            }
            pRgIccpPw->u4RemoteStatus =
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            pRgIccpPw->u2Status &= (UINT2) (~L2VPNRED_PW_STATUS_REMOTE_STANDBY);
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_REMOTE_FORWARD;

            if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                             (pRgPw->pPwVcEntry)) &&
                (pRgGroup->pOperActivePw != pRgIccpPw))
            {
                pRgGroup->pOperActivePw = pRgIccpPw;
                pRgGroup->u1Status = L2VPNRED_STATUS_NEGOTIATION_SUCCESS;
                if (L2VPN_PWVC_MODE (pRgPw->pPwVcEntry) == L2VPN_VPLS)
                {   
                    L2VpnPwRedVplsHwModify (pRgPw->pPwVcEntry, L2VPN_FALSE, NULL);
                }
                if (L2VpnFmUpdatePwVcStatus (pRgPw->pPwVcEntry, L2VPN_PWVC_UP,
                            L2VPN_PWVC_OPER_APP_CP_OR_MGMT) ==
                        L2VPN_FAILURE)
                {
                    return;
                }
                pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_LOCAL_UPDATED;
            }
        }
    }
    else
    {
        if ((L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &
             L2VPN_PWVC_STATUS_STANDBY) && (!(pRgIccpPw->u4RemoteStatus
                                              & L2VPN_PWVC_STATUS_STANDBY)))
        {
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                (~L2VPN_PWVC_STATUS_STANDBY);
            L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpPwRedHandlePwUpEvent                                  */
/* Description   : This function handles the pseudowire up event             */
/*                 for master slave and independent mode                     */
/* Input(s)      : pRgIccpPw                                                 */
/*                 pRgPw                                                     */
/*                 pRgGroup                                                  */
/* Output(s)     : b1ReNegotiate                                             */
/*                 b1LocalStandby                                            */
/*                 b1LocalSwitchover                                         */
/* Return(s)     : None                                                      */
/*****************************************************************************/

INT4
L2VpPwRedHandlePwUpEvent (tL2vpnIccpPwEntry * pRgIccpPw,
                          tL2vpnRedundancyPwEntry * pRgPw,
                          tL2vpnRedundancyEntry * pRgGroup,
                          BOOL1 * b1ReNegotiate,
                          BOOL1 * b1LocalStandby, BOOL1 * b1LocalSwitchover)
{
    INT4                i4RetStatus = L2VPN_SUCCESS;
    UINT4               u4Minutes = 60;
    L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "PW UP. "
                "pRgGroup{.u1Status(%#X), .u2NumPwAwaiting(%u)}\n",
                pRgGroup->u1Status, pRgGroup->u2NumPwAwaiting);

    if ((pRgGroup->u1ContentionResolutionMethod ==
         L2VPNRED_CONTENTION_MASTERSLAVE) &&
        (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_SLAVE))
    {
        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
        L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry) |=
            L2VPN_PWVC_STATUS_STANDBY;
        pRgIccpPw->u2Status = L2VPNRED_PW_STATUS_LOCAL_STANDBY;
        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);

        if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
            (pRgGroup->pOperActivePw != NULL) &&
            (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pOperActivePw) ==
             L2VPN_RB_LESSER))
        {
            pRgIccpPw->u2Status |= L2VPNRED_PW_STATUS_REMOTE_AWAITED;
        }
        pRgGroup->u2NumPwAwaiting++;
        pRgGroup->u1Status = L2VPNRED_STATUS_FORWARDING_NEGOTIATON;
        L2VpnSendLdpPwVcNotifMsg (pRgPw->pPwVcEntry);
    }

    else if ((pRgGroup->u1ContentionResolutionMethod ==
              L2VPNRED_CONTENTION_INDEPENDENT) ||
             (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER))
    {
        pRgIccpPw->u4LocalStatus = L2VPN_PWVC_LOCAL_STATUS (pRgPw->pPwVcEntry);

        pRgIccpPw->u4RemoteStatus =
            L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);

        if (pRgGroup->u1MasterSlaveMode == L2VPNRED_MASTERSLAVE_MASTER)
        {
            if (L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_LOCAL_STATUS
                                             (pRgPw->pPwVcEntry)) &&
                (!(L2VPN_PWVC_STATUS_IS_ACTIVE (L2VPN_PWVC_REMOTE_STATUS
                                                (pRgPw->pPwVcEntry)))))
            {
                L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry) &= (UINT1)
                    (~L2VPN_PWVC_STATUS_STANDBY);
                pRgIccpPw->u4RemoteStatus =
                    L2VPN_PWVC_REMOTE_STATUS (pRgPw->pPwVcEntry);
            }
        }
        pRgGroup->u2NumUpPw++;

        if ((!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)) &&
            ((pRgGroup->pNegForwardingPw == NULL) ||
             (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pNegForwardingPw) ==
              L2VPN_RB_GREATER)))
        {
            *b1ReNegotiate = TRUE;
        }
        if ((!(pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS)) &&
            (pRgGroup->pNegForwardingPw != NULL) &&
            (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pNegForwardingPw) ==
             L2VPN_RB_LESSER))
        {
            *b1LocalStandby = TRUE;
        }

        if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
            (pRgGroup->pOperActivePw != NULL) &&
            (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pOperActivePw) ==
             L2VPN_RB_GREATER))
        {
            *b1LocalSwitchover = TRUE;
        }
        if ((pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
            (pRgGroup->pOperActivePw != NULL) &&
            (L2vpnRedundancyPwCmp (pRgIccpPw, pRgGroup->pOperActivePw) ==
             L2VPN_RB_LESSER))
        {
            *b1LocalStandby = TRUE;
        }

        if ((pRgGroup->b1ReversionEnable == L2VPN_PWRED_REVERT_ENABLE) &&
            (pRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_PRIMARY) &&
            (pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
            (pRgGroup->pOperActivePw != NULL) &&
            (L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw)->u1Preference
             == L2VPNRED_PW_PREFERENCE_SECONDARY) &&
            (pRgGroup->u2WaitToRestoreTime > 0))
        {
            pRgGroup->u1Status |= L2VPNRED_STATUS_WAITING_TO_RESTORE;
            if (TmrStartTimer (gL2vpnRgGlobals.NegTimerList,
                               &pRgGroup->NegTimer,
                               pRgGroup->u2WaitToRestoreTime * u4Minutes *
                               SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "INTMD-EXIT 5. "
                           "Start of PW-RED WTR timer failed\n");

                return L2VPN_FAILURE;
            }
            *b1ReNegotiate = FALSE;
            *b1LocalSwitchover = FALSE;
            *b1LocalStandby = FALSE;
        }
#ifdef HVPLS_WANTED
        /*when both the Pw are Secondary*/
        if( (pRgPw->u1Preference == L2VPNRED_PW_PREFERENCE_SECONDARY) &&
                (pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS) &&
                (pRgGroup->pOperActivePw != NULL) &&
                (L2VPNRED_ICCP_PW_PARENT_PW (pRgGroup->pOperActivePw)->u1Preference
                 == L2VPNRED_PW_PREFERENCE_SECONDARY))
        {
            *b1ReNegotiate = FALSE;
            *b1LocalSwitchover = FALSE;
            *b1LocalStandby = TRUE;
        }
#endif

        if (((b1LocalSwitchover) && (pRgGroup->b1ReElectionNotReqd & TRUE)) ||
            ((b1LocalSwitchover) && 
             (pRgGroup->b1ReversionEnable != L2VPN_PWRED_REVERT_ENABLE) &&
             (pRgGroup->pOperActivePw != NULL) &&
             pRgGroup->u1Status & L2VPNRED_STATUS_NEGOTIATION_SUCCESS))
        {
            *b1LocalStandby = TRUE;
            *b1LocalSwitchover = FALSE;
            pRgGroup->b1ReElectionNotReqd = FALSE;
        }

        /* If reversion is disabled do not allow the already configured primary pseudowire
           to come active */

        if (L2VPN_PWVC_OPER_STATUS (pRgPw->pPwVcEntry) != L2VPN_PWVC_UP)
        {
            i4RetStatus = L2VpnUpdatePwVcOperStatus (pRgPw->pPwVcEntry,
                                                     L2VPN_PWVC_UP);
            if (i4RetStatus == L2VPN_FAILURE)
            {
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PwOperStatus updation failed: PwLocalStatus %x "
                            "PwRemoteStatus %x\r\n",
                            pRgPw->pPwVcEntry->u1LocalStatus,
                            pRgPw->pPwVcEntry->u1RemoteStatus);
            }
        }
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2VpnPwRedSnmpNotifyRoleChng                         */
/*                                                                           */
/* Description        : This function is called whenever there is a          */
/*                      role change from active to standby and standby       */
/*                      to active                                            */
/*                                                                           */
/* Input(s)           : u4RgGrpIndex, u4PwIndex and the local status         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
L2VpnPwRedSnmpNotifyRoleChng (UINT4 u4RgIndex, UINT4 u4PwIndex,
                              UINT1 u1TrapType, UINT1 u1LocalStatus,
                              UINT1 u1RemoteStatus)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) || \
    defined (SNMPV3_WANTED)

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { L2VPN_ZERO, L2VPN_ZERO };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    UINT1               u1TrapId = 2;

    MEMSET (&au1Buf, 0, SNMP_MAX_OID_LENGTH);

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\r L2VpnPwRedSnmpNotifyRoleChng : Function Entry\r\n");

    pEnterpriseOid = alloc_oid ((sizeof (gaL2VPN_TRAP_OID) /
                                 sizeof (UINT4)) + L2VPN_TWO);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gaL2VPN_TRAP_OID,
            sizeof (gaL2VPN_TRAP_OID));

    pEnterpriseOid->u4_Length = sizeof (gaL2VPN_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, gaL2VPN_SNMP_TRAP_OID,
            sizeof (gaL2VPN_SNMP_TRAP_OID));

    pSnmpTrapOid->u4_Length = sizeof (gaL2VPN_SNMP_TRAP_OID) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "fsL2VpnPwRedGroupIndex");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32, 0,
                                                 (INT4) u4RgIndex, NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "fsL2VpnPwRedPwIndex");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32,
                                                 0, (INT4) u4PwIndex, NULL,
                                                 NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "fsL2VpnPwRedPwLocalStatus");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }
    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);

    /* Depending upon the Trap Type, send the message to the SNMP manager */
    if (u1TrapType == L2VPN_TRAP_DLAG_STANDBY_TO_ACTIVE)
    {
        SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "Local Status :%0x %s ",
                  u1LocalStatus,
                  "L2VPN Local Status changed from STANDBY TO ACTIVE");
    }
    else if (u1TrapType == L2VPN_TRAP_DLAG_ACTIVE_TO_STANDBY)
    {
        SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "Local Status :%0x %s ",
                  u1LocalStatus,
                  "L2VPN Local Status changed from ACTIVE to STANDBY");
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) au1Buf,
                                         (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0, 0,
                                                 pOstring, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "fsL2VpnPwRedPwRemoteStatus");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }

    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "%0x ", u1RemoteStatus);
    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) au1Buf,
                                         (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0, 0, pOstring, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "\r Trap info given to SNMP agent\r\n");

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\r L2VpnPwRedSnmpNotifyRoleChng : Function Exit\r\n");

#else
    UNUSED_PARAM (u4RgIndex);
    UNUSED_PARAM (u4PwIndex);
    UNUSED_PARAM (u1TrapType);
    UNUSED_PARAM (u1LocalStatus);
    UNUSED_PARAM (u1RemoteStatus);
#endif /* SNMP_2_WANTED */
}

/*****************************************************************************/
/* Function Name      : L2VpnPwRedSnmpNotifyRoleChng                         */
/*                                                                           */
/* Description        : This function is called whenever there is a          */
/*                      role change from active to standby and standby       */
/*                      to active                                            */
/*                                                                           */
/* Input(s)           : u4RgGrpIndex, u4PwIndex and the local status         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
L2VpnPwRedSnmpNotifyNegoFail (UINT4 u4RgIndex,
                              tL2vpnIccpPwEntry * pOperActivePw,
                              UINT4 u4AdminActivePw)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) || \
    defined (SNMPV3_WANTED)
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tL2vpnRedundancyPwEntry *pRedPwEntry = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { L2VPN_ZERO, L2VPN_ZERO };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    UINT1               u1TrapId = 1;

    MEMSET (&au1Buf, 0, SNMP_MAX_OID_LENGTH);

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\r L2VpnPwRedSnmpNotifyNegoFail: Function Entry\r\n");

    pEnterpriseOid = alloc_oid ((sizeof (gaL2VPN_TRAP_OID) /
                                 sizeof (UINT4)) + L2VPN_TWO);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gaL2VPN_TRAP_OID,
            sizeof (gaL2VPN_TRAP_OID));

    pEnterpriseOid->u4_Length = sizeof (gaL2VPN_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, gaL2VPN_SNMP_TRAP_OID,
            sizeof (gaL2VPN_SNMP_TRAP_OID));

    pSnmpTrapOid->u4_Length = sizeof (gaL2VPN_SNMP_TRAP_OID) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;
    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "fsL2VpnPwRedGroupIndex");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32,
                                                 0, ((INT4) u4RgIndex), NULL,
                                                 NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
              "fsL2VpnPwRedGroupAdminActivePw");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32,
                                                 0, ((INT4) u4AdminActivePw),
                                                 NULL, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
              "fsL2VpnPwRedGroupOperActivePw");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }
    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);
    if (pOperActivePw == NULL)
    {
        SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "OperActive PW :NIL %s ",
                  "Pseudowire negotiation to find active pseudowire "
                  "failed within Negotiation timeout");
    }
    else
    {
        if ((pOperActivePw->b1IsLocal == TRUE) &&
            ((pRedPwEntry = L2VPNRED_ICCP_PW_PARENT_PW (pOperActivePw)) !=
             NULL))
        {
            SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
                      "Previous active PW: %u %s", pRedPwEntry->u4PwIndex,
                      "Pseudowire negotiation to find active pseudowire failed");
        }
        else
        {
            SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
                      "OperActive PW :NIL %s ",
                      "Pseudowire negotiation to find active pseudowire failed"
                      "within Negotiation timeout");

        }
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) au1Buf,
                                         (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0, 0, pOstring, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "\r Trap info given to SNMP agent\r\n");

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\r L2VpnPwRedSnmpNotifyRoleChng : Function Exit\r\n");

#else
    UNUSED_PARAM (u4RgIndex);
    UNUSED_PARAM (pOperActivePw);
    UNUSED_PARAM (u4AdminActivePw);
#endif /* SNMP_2_WANTED */
}

/*****************************************************************************/
/* Function Name      : L2VpnPwRedSnmpNotifyForceActive                      */
/*                                                                           */
/* Description        : This function is called whenever the user issues     */
/*                      the force switchover                                 */
/*                                                                           */
/* Input(s)           : u4RgGrpIndex, u4PwIndex and the local status         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
L2VpnPwRedSnmpNotifyForceActive (UINT4 u4RgIndex,
                                 tL2vpnIccpPwEntry * pOperActivePw,
                                 UINT4 u4AdminActivePw)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) || \
    defined (SNMPV3_WANTED)
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { L2VPN_ZERO, L2VPN_ZERO };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT4               u4PwVcIndex = 0;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    UINT1               u1TrapId = 1;

    MEMSET (&au1Buf, 0, SNMP_MAX_OID_LENGTH);

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\r L2VpnPwRedSnmpNotifyForceActive: Function Entry\r\n");

    if (pOperActivePw == NULL)
    {
        return;
    }

    pEnterpriseOid = alloc_oid ((sizeof (gaL2VPN_TRAP_OID) /
                                 sizeof (UINT4)) + L2VPN_TWO);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gaL2VPN_TRAP_OID,
            sizeof (gaL2VPN_TRAP_OID));

    pEnterpriseOid->u4_Length = sizeof (gaL2VPN_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, gaL2VPN_SNMP_TRAP_OID,
            sizeof (gaL2VPN_SNMP_TRAP_OID));

    pSnmpTrapOid->u4_Length = sizeof (gaL2VPN_SNMP_TRAP_OID) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;
    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "fsL2VpnPwRedGroupIndex");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32,
                                                 0, ((INT4) u4RgIndex), NULL,
                                                 NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
              "fsL2VpnPwRedGroupAdminActivePw");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32,
                                                 0, ((INT4) u4AdminActivePw),
                                                 NULL, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
              "fsL2VpnPwRedGroupOperActivePw");
    if ((pOid = L2VpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG, " Event Error Freeing Varlist \r\n");
        return;
    }
    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);

    u4PwVcIndex = L2VPN_PWVC_INDEX (L2VPNRED_ICCP_PW_PARENT_PW
                                    (pOperActivePw)->pPwVcEntry);

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf),
              "Current Active PW: %u %s %u", u4PwVcIndex,
              "Switch over Request to ", u4AdminActivePw);

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) au1Buf,
                                         (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0, 0, pOstring, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   " Event Error Freeing Varlist & pOID \r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "\r Trap info given to SNMP agent\r\n");

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "\r L2VpnPwRedSnmpNotifyForceActive: Function Exit\r\n");

#else
    UNUSED_PARAM (u4RgIndex);
    UNUSED_PARAM (pOperActivePw);
    UNUSED_PARAM (u4AdminActivePw);
#endif /* SNMP_2_WANTED */
}

/*****************************************************************************/
/* Function Name : L2vpnGetIccpFrmSsnInfo                                    */
/* Description   : This routine is to Get ICCP PW based on the RgIndex,      */
/*                 LocalLdpEntityId and VcEntry                              */
/* Input(s)      : u4RgIndex - u4RgIndex of tL2vpnRedundancyPwEntry          */
/*                 pSession - Pointer to tPwVcActivePeerSsnEntry             */
/*                 pPwVcEntry - Pointer to PwVcEntry                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2vpnGetIccpFrmSsnInfo (UINT4 u4RgIndex, tPwVcActivePeerSsnEntry * pSession,
                        tPwVcEntry * pPwVcEntry)
{
    tL2vpnIccpPwEntry   RgIccpPw;
    tL2vpnIccpPwEntry  *pRgIccpPw = NULL;
    UINT4               u4Temp = L2VPN_ZERO;

    MEMSET (&RgIccpPw, L2VPN_ZERO, sizeof (tL2vpnIccpPwEntry));

    RgIccpPw.u4RgIndex = u4RgIndex;
    u4Temp = OSIX_HTONL (pSession->u4LocalLdpEntityID);
    MEMCPY (&RgIccpPw.RouterId, &u4Temp, sizeof (UINT4));

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        RgIccpPw.Fec.u1Type = L2VPN_FEC_PWID_TYPE;

        u4Temp = OSIX_HTONL (pSession->u4PeerLdpId);
        MEMCPY (&RgIccpPw.Fec.u.Fec128.au1PeerId, &u4Temp, sizeof (UINT4));

        u4Temp = OSIX_HTONL (pPwVcEntry->u4LocalGroupID);
        MEMCPY (&RgIccpPw.Fec.u.Fec128.au1GroupId, &u4Temp, sizeof (UINT4));

        u4Temp = OSIX_HTONL (pPwVcEntry->u4PwVcID);
        MEMCPY (&RgIccpPw.Fec.u.Fec128.au1PwId, &u4Temp, sizeof (UINT4));
    }
    else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        RgIccpPw.Fec.u1Type = L2VPN_FEC_GEN_TYPE;
        RgIccpPw.Fec.u.Fec129.Agi.u1Type = (UINT1)
            L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry);
        if (RgIccpPw.Fec.u.Fec129.Agi.u1Type == L2VPN_GEN_FEC_AGI_TYPE_1)
        {
            MEMCPY (&RgIccpPw.Fec.u.Fec129.Agi.u,
                    pPwVcEntry->au1Agi,
                    sizeof (RgIccpPw.Fec.u.Fec129.Agi.u.au1Agi1));
        }

        RgIccpPw.Fec.u.Fec129.SAii.u1Type = (UINT1)
            L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry);
        if (RgIccpPw.Fec.u.Fec129.SAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_1)
        {
            MEMCPY (&RgIccpPw.Fec.u.Fec129.SAii.u.au1Aii1,
                    pPwVcEntry->au1Saii,
                    sizeof (RgIccpPw.Fec.u.Fec129.SAii.u.au1Aii1));
        }
        else if (RgIccpPw.Fec.u.Fec129.SAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_2)
        {
            MEMCPY (&RgIccpPw.Fec.u.Fec129.SAii.u.au1Aii2,
                    pPwVcEntry->au1Saii,
                    sizeof (RgIccpPw.Fec.u.Fec129.SAii.u.au1Aii2));
        }

        RgIccpPw.Fec.u.Fec129.TAii.u1Type = (UINT1)
            L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry);
        if (RgIccpPw.Fec.u.Fec129.TAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_1)
        {
            MEMCPY (&RgIccpPw.Fec.u.Fec129.TAii.u.au1Aii1,
                    pPwVcEntry->au1Taii,
                    sizeof (RgIccpPw.Fec.u.Fec129.TAii.u.au1Aii1));
        }
        else if (RgIccpPw.Fec.u.Fec129.TAii.u1Type == L2VPN_GEN_FEC_AII_TYPE_2)
        {
            MEMCPY (&RgIccpPw.Fec.u.Fec129.TAii.u.au1Aii2,
                    pPwVcEntry->au1Taii,
                    sizeof (RgIccpPw.Fec.u.Fec129.TAii.u.au1Aii2));
        }
    }

    pRgIccpPw = RBTreeGet (gL2vpnRgGlobals.RgIccpPwList,
                           (tRBElem *) & RgIccpPw);

    if (pRgIccpPw == NULL)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file l2vppwred.c                             */
/*---------------------------------------------------------------------------*/
