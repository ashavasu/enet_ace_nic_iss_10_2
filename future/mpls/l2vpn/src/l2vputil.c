/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vputil.c,v 1.128 2017/06/15 13:37:51 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vputil.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains utility routines for L2VPN
 *                             module
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"
#include "snmputil.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "fsl2vpn.h"
#include "l2vpmacs.h"
#include "mplscli.h"
#include "fsVplscli.h"
#ifdef HVPLS_WANTED
#include "mplsnpwr.h"
#include "mplsred.h"
#endif

extern INT4         gi4MplsSimulateFailure;

ULONG8              gu8RtBitmap[MAX_L2VPN_VPLS_ENTRIES];
ULONG8              gu8AcBitmap[MAX_L2VPN_VPLS_ENTRIES];

/*****************************************************************************/
/* Function Name : L2VpnInternalEventHandler                                 */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4Event - Event to be posted to L2VPN module              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnInternalEventHandler (UINT1 *pu1TmpMsg)
{
    if (L2VPN_INITIALISED != TRUE)
    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);
        return L2VPN_FAILURE;
    }

    if (OsixQueSend (L2VPN_QID, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);
        return L2VPN_FAILURE;
    }

    if (OsixEvtSend (L2VPN_TSK_ID, L2VPN_MSG_EVENT) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnAdminEventHandler                                    */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : pi1TmpMsg - pointer to admin event message                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnAdminEventHandler (VOID *pAdminEvtInfo, UINT4 u4Event)
{
    tL2VpnQMsg          L2VpnQMsg;
    UINT4              *pu4RespMsg = NULL;
    UINT1              *pu1TmpMsg = NULL;
    UINT4               u4RetStatus = L2VPN_FAILURE;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Admin Event Handler - "
                   "Memory Allocation failed for L2VPN Q Pool\r\n");
        return L2VPN_FAILURE;
    }

    MEMSET (pu1TmpMsg, 0, sizeof (tL2VpnQMsg));
    MEMSET (&L2VpnQMsg, 0, sizeof (tL2VpnQMsg));

    L2VPN_QMSG_TYPE = L2VPN_ADMIN_EVENT;
    switch (u4Event)
    {
        case L2VPN_PWVC_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;
            L2VPN_ADMIN_EVT_PWVC_EVTTYPE
                =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcAdminEvtInfo.u4EvtType;
            L2VPN_ADMIN_EVT_PWVC_INDEX =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcAdminEvtInfo.u4PwVcIndex;
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;

            /*MS-PW */
        case L2VPN_MSPW_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;
            L2VPN_ADMIN_EVT_MSPW_EVTTYPE
                = ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                MsPwAdminEvtInfo.u4EvtType;
            L2VPN_ADMIN_EVT_MSPW_INDEX.u4PrimPwVcIndex =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                MsPwAdminEvtInfo.u4PrimPwVcIndex;
            L2VPN_ADMIN_EVT_MSPW_INDEX.u4ScndPwVcIndex =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                MsPwAdminEvtInfo.u4ScndPwVcIndex;
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;
        case L2VPN_PWVC_SERV_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;
            L2VPN_QMSG_ADMIN_EVT_PWVC_SSTYPE
                =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcServAdminEvtInfo.u4ServSpecType;
            L2VPN_QMSG_ADMIN_EVT_SSEVT_TYPE =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcServAdminEvtInfo.EnetAdminEvtInfo.u4EvtType;
            L2VPN_QMSG_ADMIN_EVT_SSENET_VCINDEX =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcServAdminEvtInfo.EnetAdminEvtInfo.EnetEntryIndex.
                u4PwVcIndex;
            L2VPN_QMSG_ADMIN_EVT_SSENET_VLANID =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcServAdminEvtInfo.EnetAdminEvtInfo.EnetEntryIndex.
                i2PwVcEnetPwVlan;
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;
        case L2VPN_PWVC_PSN_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;

            L2VPN_QMSG_ADMIN_EVT_PSN_TYPE
                =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcPSNAdminEvtInfo.u4PsnType;
            L2VPN_QMSG_ADMIN_EVT_PSN_MPLS_EVTTYPE =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.u4EvtType;
            L2VPN_QMSG_ADMIN_EVT_PSN_MPLS_VCINDEX =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.unEvtInfo.TnlEntryIndex.
                u4PwVcIndex;
            L2VPN_QMSG_ADMIN_EVT_PSN_MPLS_TNLINDEX =
                ((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->unEvtInfo.
                PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.unEvtInfo.TnlEntryIndex.
                u4TnlInOutIndex;

            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;

            /* PW Redundancy */
        case L2VPN_RED_GROUP_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;
            L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE (&L2VpnQMsg.L2VpnEvtInfo.
                                               L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE (pAdminEvtInfo);
            L2VPN_ADMIN_EVT_RED_GROUP_INDEX (&L2VpnQMsg.L2VpnEvtInfo.
                                             L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_GROUP_INDEX (pAdminEvtInfo);
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;
        case L2VPN_RED_NODE_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;
            L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE (&L2VpnQMsg.L2VpnEvtInfo.
                                              L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE (pAdminEvtInfo);
            L2VPN_ADMIN_EVT_RED_NODE_GROUP (&L2VpnQMsg.L2VpnEvtInfo.
                                            L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_NODE_GROUP (pAdminEvtInfo);
            L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE (&L2VpnQMsg.L2VpnEvtInfo.
                                               L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE (pAdminEvtInfo);
            *L2VPN_ADMIN_EVT_RED_NODE_ADDR (&L2VpnQMsg.L2VpnEvtInfo.
                                            L2VpnAdminEvtInfo) =
                *L2VPN_ADMIN_EVT_RED_NODE_ADDR (pAdminEvtInfo);
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;
        case L2VPN_RED_PW_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;
            if ((L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (pAdminEvtInfo) ==
                 L2VPNRED_EVENT_PW_CREATE) ||
                (L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (pAdminEvtInfo) ==
                 L2VPNRED_EVENT_PW_ACTIVATE) ||
                (L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (pAdminEvtInfo) ==
                 L2VPNRED_EVENT_PW_DEACTIVATE))
            {
                L2VPN_ADMIN_WAIT_FOR_RESP_FLAG (L2VpnQMsg) =
                    L2VPN_WAIT_FOR_RESP;
            }
            L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (&L2VpnQMsg.L2VpnEvtInfo.
                                            L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_PW_EVTTYPE (pAdminEvtInfo);
            L2VPN_ADMIN_EVT_RED_PW_GROUP (&L2VpnQMsg.L2VpnEvtInfo.
                                          L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_PW_GROUP (pAdminEvtInfo);
            L2VPN_ADMIN_EVT_RED_PW_INDEX (&L2VpnQMsg.L2VpnEvtInfo.
                                          L2VpnAdminEvtInfo) =
                L2VPN_ADMIN_EVT_RED_PW_INDEX (pAdminEvtInfo);
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;
#ifdef VPLSADS_WANTED
        case L2VPN_VPLS_ADMIN_EVENT:
            L2VPN_ADMIN_SUBEVT_TYPE = u4Event;

            MEMCPY (&(L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.
                      unEvtInfo.VplsAdminEvtInfo),
                    &(((tL2VpnAdminEvtInfo *) pAdminEvtInfo)->
                      unEvtInfo.VplsAdminEvtInfo),
                    sizeof (tL2vpnVplsAdminEvtInfo));
            MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));
            break;
#endif

        default:
            break;
    }

    u4RetStatus = L2VpnInternalEventHandler (pu1TmpMsg);
    if (L2VPN_ADMIN_WAIT_FOR_RESP_FLAG (L2VpnQMsg) == L2VPN_WAIT_FOR_RESP)
    {
        /* As this function has taken lock it waits indefinitely for the 
         * queue reply hence the lock needs to be released to receive the
         * queue message */
        MPLS_L2VPN_UNLOCK ();
        if (OsixQueRecv
            (L2VPN_RES_QID, (UINT1 *) (&pu4RespMsg), OSIX_DEF_MSG_LEN,
             OSIX_WAIT) == OSIX_SUCCESS)
        {
            if ((pu4RespMsg == NULL) || (pu4RespMsg != (UINT4 *) L2VPN_TRUE))
            {
                MPLS_L2VPN_LOCK ();
                return L2VPN_FAILURE;
            }
        }
        MPLS_L2VPN_LOCK ();
    }
    return u4RetStatus;
}

/*****************************************************************************/
/* Function Name : L2VpnTeEventHandler                                       */
/* Description   : This function is used by TE module to post its events     */
/* Input(s)      : pMsg - pointer to message buffer                          */
/*                 u4Event - Event to be posted to L2VPN module              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnTeEventHandler (UINT1 *pMsg, UINT4 u4EvtType)
{
    tL2VpnQMsg          L2VpnQMsg;
    tPwVcMplsTeTnlIndex *pPwVcMplsTeTnlIndex = NULL;

    UINT4               u4RetStatus;
    UINT1              *pu1TmpMsg = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\rL2VPN Service is Down\n");
        return L2VPN_FAILURE;
    }

    L2VPN_QMSG_TYPE = L2VPN_PWVC_PSN_EVENT;
    L2VPN_PSN_EVT_PSN_TYPE = L2VPN_PWVC_PSN_MPLS;
    L2VPN_PSN_EVT_PSN_MPLSTYPE = L2VPN_PWVC_MPLS_TNL;

    switch (u4EvtType)
    {
        case L2VPN_PWVC_TNL_REESTB:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_TNL_REESTB;
            break;
        case L2VPN_PWVC_TNL_DOWN:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_TNL_DOWN;
            break;
        case L2VPN_PWVC_FRR_TNL_PLR_ADD:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD;
            break;
        case L2VPN_PWVC_FRR_TNL_PLR_DEL:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL;
            break;
        case L2VPN_PWVC_TNL_UP:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_TNL_UP;
            break;
        case L2VPN_PWVC_TE_FRR_MP_ADD:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_FRR_MP_ADD;
            break;
        case L2VPN_PWVC_TE_FRR_MP_DEL:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT = L2VPN_MPLS_PWVC_TE_FRR_MP_DEL;
            break;
        case L2VPN_PWVC_P2MP_LSP_BRANCH_ADD:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT =
                L2VPN_MPLS_PWVC_P2MP_TE_TNL_DEST_ADD;
            break;
        case L2VPN_PWVC_P2MP_LSP_BRANCH_DELETE:
            L2VPN_PSN_EVT_PSN_MPLSTNL_EVT =
                L2VPN_MPLS_PWVC_P2MP_TE_TNL_DEST_DELETE;
            break;
        default:
            /* Invalid event */
            return L2VPN_FAILURE;
    }

    pPwVcMplsTeTnlIndex = (tPwVcMplsTeTnlIndex *) (VOID *) pMsg;

    L2VPN_PSN_EVT_PSN_MPLSTNL_INDEX = pPwVcMplsTeTnlIndex->u4TnlIndex;
    L2VPN_PSN_EVT_PSN_MPLSTNL_INST = pPwVcMplsTeTnlIndex->u2TnlInstance;
    /*Store Backup Tunnel Instance in MplsTeEvtInfo structure */
    L2VPN_PSN_EVT_PSN_MPLSTNL_BKP_INST = pPwVcMplsTeTnlIndex->u2BkpTnlInstance;
    L2VPN_PSN_EVT_PSN_MPLSTNL_SWITCH_TYPE =
        pPwVcMplsTeTnlIndex->u1TnlSwitchingType;

    MEMCPY ((UINT1 *) &L2VPN_PSN_EVT_PSN_MPLSTNL_LCLID,
            (UINT1 *) &pPwVcMplsTeTnlIndex->TnlLclLSR, IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &L2VPN_PSN_EVT_PSN_MPLSTNL_PEERID,
            (UINT1 *) &pPwVcMplsTeTnlIndex->TnlPeerLSR, IPV4_ADDR_LENGTH);

    L2VPN_PSN_EVT_PSN_MPLSTNL_TNLROLE = pPwVcMplsTeTnlIndex->u1TnlRole;
    L2VPN_PSN_EVT_PSN_MPLSTNL_XC_INDEX = pPwVcMplsTeTnlIndex->u4TnlXcIndex;
    L2VPN_PSN_EVT_PSN_MPLSTNL_IFINDEX = pPwVcMplsTeTnlIndex->u4TnlIfIndex;

    L2VPN_PSN_EVT_PSN_MPLSTNL_DEL_TNL_INTF =
        pPwVcMplsTeTnlIndex->u1DeleteTnlAction;
    MEMCPY (L2VPN_PSN_EVT_PSN_MPLSTNL_NEXT_HOP_MAC,
            pPwVcMplsTeTnlIndex->au1NextHop, MAC_ADDR_LEN);
    L2VPN_PSN_EVT_PSN_MPLSTNL_MP_LABEL1 = pPwVcMplsTeTnlIndex->u4TnlBkpMPLabel1;
    L2VPN_PSN_EVT_PSN_MPLSTNL_MP_LABEL2 = pPwVcMplsTeTnlIndex->u4TnlBkpMPLabel2;
    L2VPN_PSN_EVT_PSN_MPLSTNL_MP_LABEL3 = pPwVcMplsTeTnlIndex->u4TnlBkpMPLabel3;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "TE Event Handler - "
                   "Memory Allocation failed for L2VPN Q Pool\r\n");

        return L2VPN_FAILURE;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    u4RetStatus = L2VpnInternalEventHandler (pu1TmpMsg);
    return u4RetStatus;
}

/*****************************************************************************/
/* Function Name : L2VpnNonTeEventHandler                                    */
/* Description   : This function is used by LSR module to post its events    */
/* Input(s)      : u3XcIndex - Cross Connect Index                           */
/*                 u4Event - Event to be posted to L2VPN module              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnNonTeEventHandler (UINT4 u4XcIndex, tGenU4Addr * pPrefix,
                        UINT4 u4TnlBkpMPLabel1, UINT4 u4TnlBkpMPLabel2,
                        UINT4 u4TnlBkpMPLabel3, UINT4 u4EvtType)
{
    tL2VpnQMsg          L2VpnQMsg;
    UINT4               u4RetStatus;
    UINT1              *pu1TmpMsg = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\rL2VPN Service is Down\n");
        return L2VPN_FAILURE;
    }

    L2VPN_QMSG_TYPE = L2VPN_PWVC_PSN_EVENT;
    L2VPN_PSN_EVT_PSN_TYPE = L2VPN_PWVC_PSN_MPLS;
    L2VPN_PSN_EVT_PSN_MPLSTYPE = L2VPN_PWVC_MPLS_LSP;
    L2VPN_PSN_EVT_PSN_MPLSLSP_EVT = u4EvtType;
    L2VPN_PSN_EVT_PSN_MPLSLSP_INDEX = u4XcIndex;
    L2VPN_PSN_EVT_PSN_MPLSLSP_MP_LABEL1 = u4TnlBkpMPLabel1;
    L2VPN_PSN_EVT_PSN_MPLSLSP_MP_LABEL2 = u4TnlBkpMPLabel2;
    L2VPN_PSN_EVT_PSN_MPLSLSP_MP_LABEL3 = u4TnlBkpMPLabel3;

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPrefix->u2AddrType)
    {
        MEMCPY ((UINT1 *) &(L2VPN_PSN_EVT_PSN_MPLSLSP_ADDR),
                &(pPrefix->Addr.Ip6Addr.u1_addr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPrefix->u2AddrType)
#endif
    {
        pPrefix->Addr.u4Addr = OSIX_HTONL (pPrefix->Addr.u4Addr);
        MEMCPY ((UINT1 *) &(L2VPN_PSN_EVT_PSN_MPLSLSP_ADDR),
                &(pPrefix->Addr.u4Addr), IPV4_ADDR_LENGTH);
        pPrefix->Addr.u4Addr = OSIX_NTOHL (pPrefix->Addr.u4Addr);
    }

    L2VPN_PSN_EVT_PSN_MPLSLSP_ADDR_TYPE = (UINT1) pPrefix->u2AddrType;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "NON-TE Event Handler - "
                   "Memory Allocation failed for L2VPN Q Pool\r\n");

        return L2VPN_FAILURE;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    u4RetStatus = L2VpnInternalEventHandler (pu1TmpMsg);
    return u4RetStatus;
}

/*****************************************************************************/
/* Function Name : L2VpnIfEventHandler                                       */
/* Description   : This function is used by CFA module to indicate interface */
/*                 status change                                             */
/* Input(s)      : u2IfIndex - Interface index                               */
/*                 u2PortVlan - AC port vlan                                 */
/*                 u1Status - Status of the interface                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnIfEventHandler (UINT4 u4IfIndex, UINT2 u2PortVlan, UINT1 u1Status)
{
    tL2VpnQMsg          L2VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\rL2VPN Service is Down\n");
        return;
    }

    L2VPN_QMSG_TYPE = L2VPN_PWVC_IF_EVENT;
    if (u1Status == L2VPN_IF_STAT_UP)
    {
        L2VpnQMsg.L2VpnEvtInfo.L2VpnIfEvtInfo.u4EvtType = L2VPN_IF_UP;
    }
    else
    {
        L2VpnQMsg.L2VpnEvtInfo.L2VpnIfEvtInfo.u4EvtType = L2VPN_IF_DOWN;
    }
    L2VpnQMsg.L2VpnEvtInfo.L2VpnIfEvtInfo.u4IfIndex = u4IfIndex;
    L2VpnQMsg.L2VpnEvtInfo.L2VpnIfEvtInfo.u2PortVlan = u2PortVlan;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "If Event Handler - "
                   "Memory Allocation failed for L2VPN Q Pool\r\n");
        return;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    if (L2VpnInternalEventHandler (pu1TmpMsg) == L2VPN_FAILURE)
    {
        return;
    }
}

/*****************************************************************************/
/* Function Name : L2VpnHandleRtChgNotification                              */
/* Description   : This function is used by MPLS FM module to indicate       */
/*                 route change.                                             */
/* Input(s)      : pNetIpv4RtInfo - Pointer to Route Information             */
/*                 u1CmdType      - Command Type Bitmap                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandleRtChgNotification (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType)
{
    tL2VpnQMsg          L2VpnQMsg;
    UINT4               u4RetStatus;
    UINT1              *pu1TmpMsg = NULL;

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "L2VpnHandleRtChgNotification: Function Entry\r\n");

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN is Down\r\n");
        return;
    }

    if (pNetIpv4RtInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Incoming Route Information is NULL\r\n");
        return;
    }

    L2VPN_QMSG_TYPE = L2VPN_PWVC_ROUTE_EVENT;

    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.DestAddr.u4Addr =
        pNetIpv4RtInfo->u4DestNet;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.DestMask.u4Addr =
        pNetIpv4RtInfo->u4DestMask;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.NextHop.u4Addr =
        pNetIpv4RtInfo->u4NextHop;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1AddrType = MPLS_IPV4_ADDR_TYPE;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u4RtIfIndx = pNetIpv4RtInfo->u4RtIfIndx;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u4RtNxtHopAS =
        pNetIpv4RtInfo->u4RtNxtHopAs;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.i4Metric1 = pNetIpv4RtInfo->i4Metric1;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1RowStatus =
        (UINT1) pNetIpv4RtInfo->u4RowStatus;
    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1CmdType = u1CmdType;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Route Event Handler - "
                   "Memory Allocation failed for L2VPN Q Pool\r\n");
        return;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    u4RetStatus = L2VpnInternalEventHandler (pu1TmpMsg);

    if (u4RetStatus == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Route Event not sent to L2VPN\r\n");
    }

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "L2VpnHandleRtChgNotification: Function Exit\r\n");
    return;
}

/*****************************************************************************/
/* Function Name : L2vpnInitPwVcEntry                                        */
/* Description   : This function initializes PwVcEntry to default values     */
/* Input(s)      : u4PwVcIndex - PwVc Index                                  */
/*                 pPwVcEntry - pointer to PwVc entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2vpnInitPwVcEntry (UINT4 u4PwVcIndex, tPwVcEntry * pPwVcEntry)
{
    pPwVcEntry->u4PwVcIndex = u4PwVcIndex;
    pPwVcEntry->u4PwVcID = L2VPN_ZERO;
    pPwVcEntry->i1PwVcType = L2VPN_PWVC_DEF_VC_TYPE;
    pPwVcEntry->u4GenAgiType = L2VPN_ZERO;
    pPwVcEntry->u4LocalAiiType = L2VPN_ZERO;
    pPwVcEntry->u4RemoteAiiType = L2VPN_ZERO;
    pPwVcEntry->u4ProactiveSessionIndex = L2VPN_ZERO;
    pPwVcEntry->u1LocalCcAdvert = L2VPN_VCCV_CC_NONE;
    pPwVcEntry->u1LocalCvAdvert = L2VPN_VCCV_CV_NONE;
    pPwVcEntry->u1RemoteCcAdvert = L2VPN_VCCV_CC_NONE;
    pPwVcEntry->u1RemoteCvAdvert = L2VPN_VCCV_CV_NONE;
    pPwVcEntry->bOamEnable = L2VPN_FALSE;
    pPwVcEntry->bPwIntOamEnable = L2VPN_PW_OAM_ENABLE;
    pPwVcEntry->i1PwVcOwner = L2VPN_PWVC_DEF_VC_OWNER;
    pPwVcEntry->i1PwVcPsnType = L2VPN_PWVC_DEF_PSN_TYPE;
    pPwVcEntry->i1SetUpPriority = L2VPN_PWVC_DEF_SETUP_PRIO;
    pPwVcEntry->i1HoldingPriority = L2VPN_PWVC_DEF_HOLD_PRIO;
    pPwVcEntry->u4OutVcLabelReqId = L2VPN_LDP_INVALID_REQ_ID;
    pPwVcEntry->u4InVcLabelReqId = L2VPN_LDP_INVALID_REQ_ID;
    pPwVcEntry->u4InOuterMPLabel1 = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InOuterMPLabel2 = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InMPLabel1 = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InMPLabel2 = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InOuterLdpLabel = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InVcLabel = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4OutVcLabel = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4PrevInVcLabel = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InOuterLabel = L2VPN_LDP_INVALID_LABEL;
    pPwVcEntry->u4InIfIndex = L2VPN_PWVC_DEF_PW_IF_IDX;
    pPwVcEntry->i1ControlWord = L2VPN_PWVC_DEF_SNMP_FALSE;
    pPwVcEntry->u2LocalIfMtu = L2VPN_PWVC_DEF_LOCAL_IFMTU;
    pPwVcEntry->i1AdminStatus = L2VPN_PWVC_DEF_ADMIN_STATUS;
    pPwVcEntry->i1OperStatus = L2VPN_PWVC_DEF_OPER_STATUS;
    pPwVcEntry->u1OamOperStatus = L2VPN_PWVC_OPER_UNKWN;
    pPwVcEntry->u1CPOrMgmtOperStatus = L2VPN_PWVC_CP_DEF_OPER_STATUS;
    pPwVcEntry->u1LocalStatus = L2VPN_PWVC_DEF_LOCAL_STATUS;
    pPwVcEntry->u1RemoteStatus = L2VPN_PWVC_DEF_REMOTE_STATUS;
    pPwVcEntry->i4TimeElapsed = L2VPN_PWVC_DEF_TIME_ELAPSED;
    pPwVcEntry->i1ValidIntervals = L2VPN_PWVC_DEF_VALID_INTERVAL;
    pPwVcEntry->i1StorageType = L2VPN_PWVC_DEF_STORAGE_TYPE;
    pPwVcEntry->u4PwIfIndex = L2VPN_PWVC_DEF_PW_IF_IDX;
    pPwVcEntry->i4PeerAddrType = MPLS_IPV4_ADDR_TYPE;
    pPwVcEntry->i1LocalIfString = L2VPN_PWVC_DEF_SNMP_FALSE;
    pPwVcEntry->u1LocalCapabAdvert = L2VPN_PW_STATUS_INDICATION;
    pPwVcEntry->i1CwStatus = L2VPN_PWVC_DEF_CW_PRESENT;
    pPwVcEntry->u2RemoteIfMtu = L2VPN_PWVC_DEF_REMOTE_IFMTU;
    pPwVcEntry->u1RemoteCapabilities = L2VPN_PWVC_DEF_RMT_CAP_ADV;
    pPwVcEntry->u4FragmentCfgSize = L2VPN_PWVC_DEF_FRAG_CFG_SIZE;
    pPwVcEntry->u1RmtFragCapability = L2VPN_PWVC_DEF_RMT_FRAG_CAP;
    pPwVcEntry->i1FcsRetentionCfg = L2VPN_PWVC_DEF_FCS_RETN_CFG;
    pPwVcEntry->u1FcsRetentionStatus = L2VPN_PWVC_DEF_FCS_RETN_STAT;
    pPwVcEntry->i1RemoteStatusCapable = L2VPN_PWVC_DEF_RMT_STAT_CAP;
    pPwVcEntry->u1AgiLen = L2VPN_ZERO;
    pPwVcEntry->u1SaiiLen = L2VPN_ZERO;
    pPwVcEntry->u1TaiiLen = L2VPN_ZERO;
    pPwVcEntry->u1PwVcMode = L2VPN_VPWS;
    pPwVcEntry->u4VplsInstance = L2VPN_ZERO;
    pPwVcEntry->u1HwStatus = L2VPN_FALSE;
    pPwVcEntry->u1IsSigNonTeLsp = L2VPN_FALSE;
    pPwVcEntry->u1PwPathType = L2VPN_PW_WORKING_PATH;
    pPwVcEntry->u2NextFreeEnetInstance = L2VPN_ONE;
    L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS (pPwVcEntry) = MPLS_ARP_RESOLVE_UNKNOWN;
    L2VPN_PWVC_ROUTE_STATUS (pPwVcEntry) = L2VPN_PWVC_ROUTE_UNKNOWN;
    pPwVcEntry->b1IsStaticLabel = FALSE;
}

/*****************************************************************************/
/* Function Name : L2vpnInitPwVcMplsEntry                                    */
/* Description   : This function initializes PwVcMplsEntry to default values */
/* Input(s)      : pPwVcMplsEntry - pointer to PwVcMpls entry                */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2vpnInitPwVcMplsEntry (tPwVcMplsEntry * pPwVcMplsEntry)
{
    pPwVcMplsEntry->u1MplsType = L2VPN_MPLS_DEF_MPLS_TYPE_NONTE;
    pPwVcMplsEntry->i1ExpModeAndBits = (L2VPN_MPLS_DEF_EBIT_MD_OUT_TNL
                                        << MPLS_FOUR);
    pPwVcMplsEntry->i1ExpModeAndBits |= L2VPN_MPLS_DEF_EBITS;
    pPwVcMplsEntry->u1Ttl = L2VPN_MPLS_DEF_TTL;
    pPwVcMplsEntry->i1StorageType = L2VPN_PWVC_DEF_STORAGE_TYPE;
    pPwVcMplsEntry->PwVcMplsOutTnl.u1TnlType = L2VPN_MPLS_DEF_MPLS_TYPE_NONTE;
}

/*****************************************************************************/
/* Function Name : L2vpnInitPwVcEnetEntry                                    */
/* Description   : This function initializes PwVcEnetEntry to default values */
/* Input(s)      : pPwVcEnetEntry - pointer to PwVcEnet entry                */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2vpnInitPwVcEnetEntry (tPwVcEnetEntry * pPwVcEnetEntry)
{
    pPwVcEnetEntry->u2PwVlan = L2VPN_PWVC_ENET_DEF_PW_VLAN;
    pPwVcEnetEntry->u2PortVlan = L2VPN_PWVC_ENET_DEF_PORT_VLAN;
    pPwVcEnetEntry->i1VlanMode = L2VPN_PWVC_ENET_DEF_VLAN_MODE;
    pPwVcEnetEntry->i1StorageType = L2VPN_PWVC_DEF_STORAGE_TYPE;
    pPwVcEnetEntry->u1HwStatus = MPLS_FALSE;
    TMO_SLL_Init_Node (&(pPwVcEnetEntry->NextEnetEntry));
}

/*****************************************************************************/
/* Function Name : L2vpnCreateVplsEntry                                      */
/* Description   : This function creates a VplsEntry & assign default values */
/* Input(s)      : u4InstanceIndex  : VplsInstance Index                     */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to the tVPLSEntry or NULL                         */
/*****************************************************************************/
tVPLSEntry         *
L2vpnCreateVplsEntry (UINT4 u4InstanceIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;

    /* Check whether the element already exists */
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4InstanceIndex);
    if (pVplsEntry != NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "L2vpnCreateVplsEntry : Entry present already \t\n");
        return NULL;
    }

    pVplsEntry = (tVPLSEntry *) MemAllocMemBlk (L2VPN_VPLS_POOL_ID);

    if (pVplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "L2vpnCreateVplsEntry : Memory Allocate Failed \t\n");
        return NULL;
    }

    MEMSET (pVplsEntry, 0, sizeof (tVPLSEntry));

    L2VPN_VPLS_INDEX (pVplsEntry) = u4InstanceIndex;

    /* Initializing objects with the default vales */
#ifdef VPLSADS_WANTED
    L2VPN_VPLS_MTU (pVplsEntry) = L2VPN_VPLS_DEFAULT_MTU;
    L2VPN_VPLS_CONTROL_WORD (pVplsEntry) = L2VPN_DISABLED;
#endif
    L2VPN_VPLS_FDB_HIGH_THRESHOLD (pVplsEntry) = L2VPN_VPLS_DEF_HIGH_THRESHOLD;
    L2VPN_VPLS_FDB_LOW_THRESHOLD (pVplsEntry) = L2VPN_VPLS_DEF_LOW_THRESHOLD;
    L2VPN_VPLS_VSI (pVplsEntry) = L2IWF_DEFAULT_CONTEXT;
    L2VPN_VPLS_FDB_ID (pVplsEntry) = L2VPN_VPLS_FDB_DEF_VAL;
    L2VPN_VPLS_ROW_STATUS (pVplsEntry) = NOT_READY;
    pVplsEntry->u1StorageType = L2VPN_STORAGE_NONVOLATILE;
    L2VPN_VPLS_SIG_TYPE (pVplsEntry) = L2VPN_VPLS_SIG_NONE;
    TMO_SLL_Init (&pVplsEntry->PwList);
    /* Associating this Entry to the global pointer */
    (L2VPN_VPLS_INFO_PTR + u4InstanceIndex)->pVplsEntry = pVplsEntry;

    return pVplsEntry;
}

#ifdef VPLSADS_WANTED
/*****************************************************************************/
/* Function Name : L2VpnVplsAssocDelete                                      */
/* Description   : This function deletes a associated RD,RT and VE-ID for     */
/*                     VPLSEntry                                                   */
/* Input(s)      : u4InstanceIndex  : VplsInstance Index                     */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2VpnVplsAssocDelete (UINT4 u4InstanceIndex)
{
    UINT4               u4VeId;
    UINT4               u4VplsIndex = u4InstanceIndex;
    UINT4               u4ErrorCode;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    /*Delete the VE associated to the VPLS */
    if (L2VpnGetLocalVeIdFromVPLSIndex (u4VplsIndex, &u4VeId) != L2VPN_FAILURE)
    {
        if (nmhTestv2VplsBgpVERowStatus (&u4ErrorCode, u4VplsIndex, u4VeId,
                                         DESTROY) == SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : \
                                nmhTestv2VplsBgpVERowStatus failed\n", __func__);
            return L2VPN_FAILURE;
        }
        if (nmhSetVplsBgpVERowStatus (u4VplsIndex, u4VeId, DESTROY) ==
            SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : \
                                nmhSetVplsBgpVERowStatus failed\n", __func__);
            return L2VPN_FAILURE;
        }
    }
    /*Delete the RD associated to the VPLS */
    if (nmhTestv2VplsBgpADConfigRowStatus (&u4ErrorCode, u4VplsIndex,
                                           DESTROY) != SNMP_FAILURE)
    {
        if (nmhSetVplsBgpADConfigRowStatus (u4VplsIndex, DESTROY) ==
            SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : \
                                nmhSetVplsBgpADConfigRowStatus failed\n", __func__);
            return L2VPN_FAILURE;
        }
    }
    /*Delete the RTs associated to the VPLS */
    if (L2VpnDeleteRTs (u4VplsIndex) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : \
                            Failure in RT Deletion\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name : L2vpnDeleteVplsEntry                                      */
/* Description   : This function deletes a VPLSEntry                         */
/* Input(s)      : u4InstanceIndex  : VplsInstance Index                     */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2vpnDeleteVplsEntry (UINT4 u4InstanceIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = L2VPN_ZERO;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    /* Check whether the element exists */
    if ((pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4InstanceIndex))
        == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "L2vpnDeleteVplsEntry : Entry not present  \t\n");
        return L2VPN_FAILURE;
    }

    if (L2vpnVplsVfiDelete (pVplsEntry) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "L2vpnDeleteVplsEntry : RbTree Delete Failed \t\n");
    }

    MEMCPY (&u4VpnId, &(L2VPN_VPLS_VPN_ID (pVplsEntry)
                        [STRLEN (MPLS_OUI_VPN_ID)]), sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);

    if (u4VpnId != L2VPN_ZERO)
    {
        MplsL2VpnRelVpnId (u4VpnId);
    }
    (L2VPN_VPLS_INFO_PTR + u4InstanceIndex)->pVplsEntry = NULL;

    if (MemReleaseMemBlock (L2VPN_VPLS_POOL_ID,
                            (UINT1 *) pVplsEntry) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "L2vpnDeleteVplsEntry : Memory Release Failed \t\n");
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnAddPwVplsEntry                                       */
/* Description   : This function adds a PwEntry to a VPLSEntry               */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2vpnAddPwVplsEntry (tPwVcEntry * pPwVcEntry)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (pPwVcEntry->u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    if (TMO_SLL_Is_Node_In_List ((&pPwVcEntry->VplsNode)))
    {
        return L2VPN_SUCCESS;
    }
    TMO_SLL_Add (&pVplsEntry->PwList, &pPwVcEntry->VplsNode);
    L2VPN_VPLS_PEER_COUNT (pVplsEntry) = TMO_SLL_Count (&pVplsEntry->PwList);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2vpnDeletePwVplsEntry                                    */
/* Description   : This function deletes a PwEntry to a VPLSEntry            */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_FAILURE/L2VPN_SUCCESS                               */
/*****************************************************************************/
INT4
L2vpnDeletePwVplsEntry (tPwVcEntry * pPwVcEntry)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
        (pPwVcEntry->u4VplsInstance);
    if (pVplsEntry != NULL)
    {
        TMO_SLL_Delete (&pVplsEntry->PwList, &pPwVcEntry->VplsNode);
        TMO_SLL_Init_Node (&pPwVcEntry->VplsNode);
        return L2VPN_SUCCESS;
    }
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnIsPwIdOrGenFecExist                                  */
/* Description   : This function is used to check whether the                */
/*                 PWId or GenFec Informations are already                   */
/*                 configured on the different PWs                           */
/* Input(s)      : pPwVcEntry - PwVc information                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_TRUE/L2VPN_FALSE                                    */
/*****************************************************************************/
UINT1
L2VpnIsPwIdOrGenFecExist (tPwVcEntry * pPwVcEntry)
{
    tPwVcEntry         *pTmpPwVcEntry = NULL;
    tPwVcInfo          *pPwVcInfo = NULL;
    UINT4               u4PwVcIndex = L2VPN_ZERO;

    for (u4PwVcIndex = 1;
         u4PwVcIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo);
         u4PwVcIndex++)
    {
        if (u4PwVcIndex == L2VPN_PWVC_INDEX (pPwVcEntry))
        {
            continue;
        }
        pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwVcIndex);
        pTmpPwVcEntry = pPwVcInfo->pPwVcEntry;
        if (pTmpPwVcEntry == NULL)
        {
            continue;
        }
        if (L2VPN_PWVC_OWNER (pTmpPwVcEntry) != L2VPN_PWVC_OWNER (pPwVcEntry))
        {
            continue;
        }
        if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
        {
            if ((L2VPN_PWVC_ID (pTmpPwVcEntry) == L2VPN_PWVC_ID (pPwVcEntry)) &&
                (L2VPN_PWVC_LOCAL_GRP_ID (pTmpPwVcEntry) ==
                 L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry)))
            {
                return L2VPN_TRUE;
            }
        }
        else if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            if (((pPwVcEntry->u1SaiiLen) == L2VPN_ZERO)
                && (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS))
            {
                if (MEMCMP (pTmpPwVcEntry->au1Agi,
                            pPwVcEntry->au1Agi, L2VPN_PWVC_MAX_AI_LEN) == 0)
                {
#ifdef MPLS_IPV6_WANTED

                    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
                    {
                        if (MEMCMP (&(pTmpPwVcEntry->PeerAddr.Ip6Addr.u1_addr),
                                    &(pPwVcEntry->PeerAddr.Ip6Addr.u1_addr),
                                    IPV6_ADDR_LENGTH) == 0)
                        {
                            return L2VPN_TRUE;
                        }

                    }
                    else
#endif
                    {
                        if (MEMCMP (&(pTmpPwVcEntry->PeerAddr.au1Ipv4Addr),
                                    &(pPwVcEntry->PeerAddr.au1Ipv4Addr),
                                    IPV4_ADDR_LENGTH) == 0)
                        {
                            return L2VPN_TRUE;
                        }
                    }
                }
            }
            else
            {

                if ((MEMCMP (pTmpPwVcEntry->au1Agi,
                             pPwVcEntry->au1Agi, L2VPN_PWVC_MAX_AI_LEN) == 0) &&
                    (MEMCMP (pTmpPwVcEntry->au1Saii,
                             pPwVcEntry->au1Saii, L2VPN_PWVC_MAX_AI_LEN) == 0)
                    &&
                    (MEMCMP
                     (pTmpPwVcEntry->au1Taii, pPwVcEntry->au1Taii,
                      L2VPN_PWVC_MAX_AI_LEN) == 0))
                {
                    return L2VPN_TRUE;
                }
            }

        }

    }
    return L2VPN_FALSE;

}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntryFromPwId                                 */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 PwVcID,Type                                               */
/* Input(s)      : u4PwVcID, i1PwVcType                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromPwId (UINT4 u4PwVcID, INT1 i1PwVcType)
{
    tPwVcEntry         *pPwVcEntry = NULL, *pPwVcEntryIn = NULL;

    pPwVcEntryIn = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntryIn == NULL)
    {
        return NULL;
    }
    MEMSET (pPwVcEntryIn, 0, sizeof (tPwVcEntry));

    pPwVcEntryIn->u4PwVcID = u4PwVcID;
    pPwVcEntryIn->i1PwVcType = i1PwVcType;
    pPwVcEntry = (tPwVcEntry *) RBTreeGet (L2VPN_MPLS_PWID_TABLE, pPwVcEntryIn);

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure - PwVc memory removal\r\n");
    }
    return pPwVcEntry;
}

/*********************************************************************************************/
/* Function Name : L2VpnGetVplsPwVcEntryFromPwAgi                                            */
/* Description   : This function returns pointer to PwVc Entry for the given                 */
/*                 Agi,PeerAddr                                                              */
/* Input(s)      : Agi,GenU4Addr                                                             */
/* Output(s)     : None                                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL                              */
/**********************************************************************************************/
tPwVcEntry         *
L2VpnGetVplsPwVcEntryFromPwAgi (UINT1 *pu1Agi, tGenU4Addr GenU4Addr)
{
    tPwVcEntry         *pPwVcEntry = NULL, *pPwVcEntryIn = NULL;

    pPwVcEntryIn = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntryIn == NULL)
    {
        return NULL;
    }

    MEMSET (pPwVcEntryIn, 0, sizeof (tPwVcEntry));
    MEMCPY (pPwVcEntryIn->au1Agi, pu1Agi, L2VPN_PWVC_MAX_AI_LEN);
    /* MEMCPY (&(pPwVcEntryIn->PeerAddr),&(pPwVcEntryGet->PeerAddr),sizeof(tSNMP_OCTET_STRING_TYPE)); */
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
    {
        MEMCPY (&(pPwVcEntryIn->PeerAddr), GenU4Addr.Addr.Ip6Addr.u1_addr,
                IPV6_ADDR_LENGTH);

    }
    else if (MPLS_IPV4_ADDR_TYPE == GenU4Addr.u2AddrType)
#endif
    {
        MEMCPY (&(pPwVcEntryIn->PeerAddr), &GenU4Addr.Addr.u4Addr,
                IPV4_ADDR_LENGTH);

    }
    pPwVcEntry = (tPwVcEntry *)
        RBTreeGet (L2VPN_PWVC_FEC129_VPLS_RB_PTR (gpPwVcGlobalInfo),
                   pPwVcEntryIn);

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure - PwVc memory removal\r\n");
    }
    return pPwVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntryFromPwAgi                                */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 Agi,Saii and Taii                                         */
/* Input(s)      : Agi,Saii and Taii                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromPwAgi (UINT1 *pu1Agi, UINT1 *pu1Saii, UINT1 *pu1Taii)
{
    tPwVcEntry         *pPwVcEntry = NULL, *pPwVcEntryIn = NULL;

    pPwVcEntryIn = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);
    if (pPwVcEntryIn == NULL)
    {
        return NULL;
    }
    MEMSET (pPwVcEntryIn, 0, sizeof (tPwVcEntry));
    MEMCPY (pPwVcEntryIn->au1Agi, pu1Agi, L2VPN_PWVC_MAX_AI_LEN);
    MEMCPY (pPwVcEntryIn->au1Saii, pu1Saii, L2VPN_PWVC_MAX_AI_LEN);
    MEMCPY (pPwVcEntryIn->au1Taii, pu1Taii, L2VPN_PWVC_MAX_AI_LEN);

    pPwVcEntry = (tPwVcEntry *)
        RBTreeGet (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo), pPwVcEntryIn);

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure - PwVc memory removal\r\n");
    }
    return pPwVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntryFromAgiAndTaii                           */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 Agi,Taii                                                  */
/* Input(s)      : Agi,Taii                                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntryFromAgiAndTaii (UINT1 *pu1Agi, UINT1 *pu1Taii)
{
    tPwVcEntry         *pPwVcEntry = NULL, *pPwVcEntryIn = NULL;

    pPwVcEntryIn = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntryIn == NULL)
    {
        return NULL;
    }
    MEMSET (pPwVcEntryIn, 0, sizeof (tPwVcEntry));

    MEMCPY (pPwVcEntryIn->au1Agi, pu1Agi, L2VPN_PWVC_MAX_AI_LEN);
    MEMCPY (pPwVcEntryIn->au1Taii, pu1Taii, L2VPN_PWVC_MAX_AI_LEN);
    pPwVcEntry = (tPwVcEntry *)
        RBTreeGetNext (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo),
                       pPwVcEntryIn, NULL);

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure - PwVc memory removal\r\n");
    }
    return pPwVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwVcEntry                                         */
/* Description   : This function returns pointer to PwVc Entry for the given */
/*                 Agi,Saii and Taii if pwOwner is Fec 129 or for the given  */
/*                 pw-id and pw-type if pwOwner is Fec 128                   */
/* Input(s)      : PwOwner, PwId,PwType,Agi,Saii,Taii and PeerAddr           */
/* Output(s)     : None                                                      */
/* Return(s)     : pPwVcEntry - pointer to PwVc entry ELSE NULL              */
/*****************************************************************************/
tPwVcEntry         *
L2VpnGetPwVcEntry (INT1 i1PwOwner, UINT4 u4PwId, UINT1 u1PwType,
                   UINT1 *pu1Agi, UINT1 *pu1Saii, UINT1 *pu1Taii,
                   tGenU4Addr GenU4Addr)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (i1PwOwner == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromPwId (u4PwId, (INT1) u1PwType);
    }
    else if (i1PwOwner == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
    {
        if (((pu1Saii == NULL) || (pu1Taii == NULL))
            || (*pu1Saii == L2VPN_ZERO))
        {
            pPwVcEntry = L2VpnGetVplsPwVcEntryFromPwAgi (pu1Agi, GenU4Addr);
        }
        else
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromPwAgi (pu1Agi, pu1Saii, pu1Taii);
        }
    }

#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == GenU4Addr.u2AddrType)
    {
        if ((pPwVcEntry == NULL) ||
            (MEMCMP (&(pPwVcEntry->PeerAddr), GenU4Addr.Addr.Ip6Addr.u1_addr,
                     IPV6_ADDR_LENGTH) != L2VPN_ZERO))
        {
            return NULL;
        }
    }
    else if (MPLS_IPV4_ADDR_TYPE == GenU4Addr.u2AddrType)
#endif
    {
        if ((pPwVcEntry == NULL) ||
            (MEMCMP (&(pPwVcEntry->PeerAddr), &GenU4Addr.Addr.u4Addr,
                     IPV4_ADDR_LENGTH) != L2VPN_ZERO))
        {
            return NULL;
        }
    }

    return pPwVcEntry;
}

/*****************************************************************************/
/* Function Name : L2VpnGetControlWordMandate                                */
/* Description   : This function returns control word preference for the     */
/*                    given PW Type                                              */
/* Input(s)      : PwType                                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : INT1                                                       */
/*****************************************************************************/
INT1
L2VpnGetControlWordMandate (INT1 i1PwType)
{
    if (gi4MplsSimulateFailure == L2VPN_SIM_FAILURE_CTRLW_MANDATORY)
    {
        return L2VPN_PWVC_CTRLW_MANDATORY;
    }

    switch (i1PwType)
    {
            /* Use of the Control Word MUST be Mandartory for Ethernet or Ethernet
             * Tagged PW Type; But this function returns NOT Mandatory for these PW
             * Type as the HW is not supporting Control Word */

        case L2VPN_PWVC_TYPE_ETH_VLAN:
        case L2VPN_PWVC_TYPE_ETH:
            return L2VPN_PWVC_CTRLW_NOT_MANDATORY;
        default:
            return L2VPN_PWVC_CTRLW_MANDATORY;
    }
}

/*****************************************************************************/
/* Function Name : L2VpnUpdateGlobalStats                                    */
/* Description   : This routine updates the PwVc global stats                */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/*               : u1Event - type of PwVc event (up/down/delete)             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnUpdateGlobalStats (tPwVcEntry * pPwVcEntry, UINT1 u1Event)
{
    UINT4               u4TimeTicks = 0;
    switch (u1Event)
    {
        case L2VPN_PWVC_CREATE:
            L2VPN_NO_OF_PWVC_ENTRIES_CREATED (gpPwVcGlobalInfo)++;
            break;
        case L2VPN_PWVC_OPER_UP:
            L2VPN_ACTIVE_PWVC_ENTRIES (gpPwVcGlobalInfo)++;
            if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            {
                L2VPN_ACTIVE_PWVC_MPLS_ENTRIES
                    (gpPwVcGlobalInfo, L2VPN_PSN_STAT_MPLS)++;
            }
            if ((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH) ||
                (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            {
                L2VPN_ACTIVE_PWVC_ENET_ENTRIES
                    (gpPwVcGlobalInfo, L2VPN_SERV_STAT_ETH)++;
            }
            OsixGetSysTime (&u4TimeTicks);
            L2VPN_PWVC_LAST_CHANGE (pPwVcEntry) = u4TimeTicks;
            break;
        case L2VPN_PWVC_DESTROY:
            L2VPN_NO_OF_PWVC_ENTRIES_DESTROYED (gpPwVcGlobalInfo)++;
            break;
        case L2VPN_PWVC_OPER_DOWN:
            L2VPN_ACTIVE_PWVC_ENTRIES (gpPwVcGlobalInfo)--;
            if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            {
                L2VPN_ACTIVE_PWVC_MPLS_ENTRIES
                    (gpPwVcGlobalInfo, L2VPN_PSN_STAT_MPLS)--;
            }
            if ((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH) ||
                (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
            {
                L2VPN_ACTIVE_PWVC_ENET_ENTRIES
                    (gpPwVcGlobalInfo, L2VPN_SERV_STAT_ETH)--;
            }
            OsixGetSysTime (&u4TimeTicks);
            L2VPN_PWVC_LAST_CHANGE (pPwVcEntry) = u4TimeTicks;
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnPrintPwVc                                            */
/* Description   : This routine Prints the debug related traces              */
/*                 for PwEntries                                             */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnPrintPwVc (tPwVcEntry * pPwVcEntry)
{
    UINT1               u1Length = 0;
    CHR1                ac1Agi[L2VPN_PWVC_MAX_AI_LEN];
    CHR1                ac1Saii[L2VPN_PWVC_MAX_AI_LEN];
    CHR1                ac1Taii[L2VPN_PWVC_MAX_AI_LEN];

    if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG) ||
        (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL))
    {
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG7 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW id %d, PW Type %d) with peer "
                        " IPv4- %d.%d.%d.%d IPv6 %s is UP,\r\n",
                        pPwVcEntry->u4PwVcID, pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else

            L2VPN_DBG6 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW id %d, PW Type %d) with peer %d.%d.%d.%d is UP,\r\n",
                        pPwVcEntry->u4PwVcID, pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3]);
#endif
        }
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_DOWN)
        {
#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG7 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW id %d, PW Type %d) with peer IPv4- %d.%d.%d.%d IPv6 %s"
                        "is DOWN,\r\n",
                        pPwVcEntry->u4PwVcID, pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));

#else
            L2VPN_DBG6 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW id %d, PW Type %d) with peer %d.%d.%d.%d is DOWN,\r\n",
                        pPwVcEntry->u4PwVcID, pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3]);
#endif
        }
    }

    if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG
#ifdef VPLSADS_WANTED
        || L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_OTHER
#endif
        )
    {
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {

#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG6 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW Type %d) with peer IPv4- %d.%d.%d.%d"
                        "IPv6  %s is UP,",
                        pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
            L2VPN_DBG5 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW Type %d) with peer %d.%d.%d.%d is UP,",
                        pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3]);
#endif
        }
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_DOWN)
        {

#ifdef MPLS_IPV6_WANTED

            L2VPN_DBG6 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW Type %d) with peer IPv4- %d.%d.%d.%d"
                        " IPv6 %s  is DOWN,",
                        pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3],
                        Ip6PrintAddr (&(pPwVcEntry->PeerAddr.Ip6Addr)));
#else
            L2VPN_DBG5 (L2VPN_DBG_DBG_VC_FLAG,
                        "PseudoWire (PW Type %d) with peer %d.%d.%d.%d is DOWN,",
                        pPwVcEntry->i1PwVcType,
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[0],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[1],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[2],
                        pPwVcEntry->PeerAddr.au1Ipv4Addr[3]);
#endif
        }

        if (pPwVcEntry->u1AgiLen > L2VPN_ZERO)
        {
            L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, " AGI ");
            while (u1Length < pPwVcEntry->u1AgiLen)
            {
                SNPRINTF (ac1Agi, sizeof (ac1Agi), "%x",
                          pPwVcEntry->au1Agi[u1Length]);
                L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, ac1Agi);
                L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, ":");
                u1Length++;
            }
            L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, "\r\n");
        }

        if ((pPwVcEntry->u1SaiiLen > L2VPN_ZERO) &&
            (pPwVcEntry->u1TaiiLen > L2VPN_ZERO))
        {
            u1Length = 0;
            L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, " SAII ");
            while (u1Length < pPwVcEntry->u1SaiiLen)
            {
                SNPRINTF (ac1Saii, sizeof (ac1Agi), "%x",
                          pPwVcEntry->au1Saii[u1Length]);
                L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, ac1Saii);
                L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, ":");
                u1Length++;
            }
            L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, " ");
            u1Length = 0;

            L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, "TAII ");
            while (u1Length < pPwVcEntry->u1TaiiLen)
            {
                SNPRINTF (ac1Taii, sizeof (ac1Agi), "%x",
                          pPwVcEntry->au1Taii[u1Length]);
                L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, ac1Taii);
                L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, ":");
                u1Length++;
            }
            L2VPN_DUMP (L2VPN_DBG_DBG_VC_FLAG, "\r\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : L2VpnGetVplsEntryFromInstanceIndex                        */
/* Description   : This function returns pointer to VPLS Entry for the given */
/*                 VPLS Instance.                                            */
/* Input(s)      : u4InstanceIndex - VPLS Instance                           */
/* Output(s)     : None                                                      */
/* Return(s)     : pVPLSEntry - pointer to VPLS entry (or) NULL              */
/*****************************************************************************/
tVPLSEntry         *
L2VpnGetVplsEntryFromInstanceIndex (UINT4 u4InstanceIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tVPLSInfo          *pVplsInfo = NULL;

    if ((u4InstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries)
        || (u4InstanceIndex == 0))
    {
        return NULL;
    }
    if (gpPwVcGlobalInfo->ppVPLSTable != NULL)
    {
        pVplsInfo = L2VPN_VPLS_INFO_PTR + u4InstanceIndex;
        if (pVplsInfo != NULL)
        {
            pVplsEntry = pVplsInfo->pVplsEntry;
        }
    }
    return (pVplsEntry);
}

/*****************************************************************************/
/* Function Name : L2VpnGetVplsEntryFromVFI                                  */
/* Description   : This function returns pointer to VPLS Entry for the given */
/*                 Vpls Name.                                                */
/* Input(s)      : pu1VplsVFI  - VPLS NAME                                   */
/* Output(s)     : None                                                      */
/* Return(s)     : pVPLSEntry - pointer to VPLS entry (or) NULL              */
/*****************************************************************************/
tVPLSEntry         *
L2VpnGetVplsEntryFromVFI (UINT1 *pu1VplsVFI)
{
    UINT4               u4VplsInstance;
    tVPLSInfo          *pVplsInfo = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    BOOL1               bFlag = FALSE;

    for (u4VplsInstance = 1;
         u4VplsInstance <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4VplsInstance++)
    {
        pVplsEntry = NULL;
        pVplsInfo = L2VPN_VPLS_INFO_PTR + u4VplsInstance;
        if ((pVplsInfo != NULL) && (pVplsInfo->pVplsEntry != NULL))
        {
            if (STRCMP (L2VPN_VPLS_NAME (pVplsInfo->pVplsEntry), pu1VplsVFI) ==
                0)
            {
                bFlag = TRUE;
                pVplsEntry = pVplsInfo->pVplsEntry;
                break;
            }
        }
    }
    if (bFlag == FALSE)
    {
        pVplsEntry = NULL;
    }
    return (pVplsEntry);
}

#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name : L2VpnGetVplsEntryFromVpnId                                */
/* Description   : This function returns pointer to VPLS Entry for the given */
/*                 VPLS Instance.                                            */
/* Input(s)      : u4VpnId - Vpn Id                                          */
/* Output(s)     : None                                                      */
/* Return(s)     : pVPLSEntry - pointer to VPLS entry (or) NULL              */
/*****************************************************************************/
tVPLSEntry         *
L2VpnGetVplsEntryFromVpnId (UINT4 u4VpnId)
{
    UINT4               u4VplsInstance;
    tVPLSInfo          *pVplsInfo = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4GetVpnId = L2VPN_ZERO;
    UINT1               au1VplsVpnID[L2VPN_MAX_VPLS_VPNID_LEN + 4];
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (au1VplsVpnID, L2VPN_ZERO, L2VPN_MAX_VPLS_VPNID_LEN + 4);
    STRNCPY (au1VplsVpnID, MPLS_OUI_VPN_ID, sizeof (MPLS_OUI_VPN_ID));
    u4GetVpnId = OSIX_NTOHL (u4VpnId);
    MEMCPY (&au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)], &u4GetVpnId,
            sizeof (UINT4));
    for (u4VplsInstance = 1;
         u4VplsInstance <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4VplsInstance++)
    {
        pVplsInfo = L2VPN_VPLS_INFO_PTR + u4VplsInstance;
        if ((pVplsInfo != NULL) && (pVplsInfo->pVplsEntry != NULL))
        {
            if (MEMCMP
                (L2VPN_VPLS_VPN_ID (pVplsInfo->pVplsEntry), au1VplsVpnID,
                 L2VPN_MAX_VPLS_VPNID_LEN) == 0)
            {
                pVplsEntry = pVplsInfo->pVplsEntry;
                break;
            }
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return (pVplsEntry);
}
#endif
/*****************************************************************************/
/* Function Name : L2VpnGetVpnIdAndVsiFromPw                                 */
/* Description   : This function gets the VPN Id and VSI Id from PW Entry    */
/* Input(s)      : pPwVcEntry    - Pointer to Pw Entry                       */
/* Output(s)     : *pu4VpnId     - Pointer to VPN Id                         */
/*                 *pi4Vsi       - Pointer to VSI                            */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnGetVpnIdAndVsiFromPw (tPwVcEntry * pPwVcEntry, UINT4 *pu4VpnId,
                           INT4 *pi4Vsi)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4VpnId = L2VPN_ZERO;

    u4VplsIndex = pPwVcEntry->u4VplsInstance;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    MEMCPY (&u4VpnId, &(pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
            sizeof (UINT4));

    u4VpnId = OSIX_HTONL (u4VpnId);

    *pu4VpnId = u4VpnId;
    *pi4Vsi = pVplsEntry->i4VplsVSI;
    return L2VPN_SUCCESS;
}

#ifdef HVPLS_WANTED
/************************************************************************
 *  Function Name   : L2VpnVplsOperStatusTrap 
 *  Description     : This function sends Notification for the VPls oper status 
 *                    up and down trap to the Manager
 *  Input           : pVplsEntry : Pointer to Vpls Entry
 *  Output          : NONE
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT1
L2VpnVplsOperStatusTrap (tVPLSEntry * pVplsEntry)
{
#ifdef SNMP_3_WANTED
    UINT4               u4ArrLen;
    UINT4               u4Length;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnIdOctet = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pVpnIdOid = NULL,
        *pFdbHighOid = NULL, *pFdbLowOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4VplsFDBNotify[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 1, 0, 1 };
    UINT4               au4VplsVpnId[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 1, 5, 1, 5, 1, 3, 0 };
    UINT4               au4VplsAdminStatus[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 72, 1, 2, 1, 4 };
    UINT4               au4VplsOperStatus[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 72, 1, 3, 1, 1 };

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    /* Trap OID construction. Telling Manager that you have received
     * trap for fsMplsVplsNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return L2VPN_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4VplsFDBNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return L2VPN_FAILURE;
    }
    MEMCPY (pOidValue->pu4_OidList, au4VplsFDBNotify,
            sizeof (au4VplsFDBNotify));
    pOidValue->u4_Length = sizeof (au4VplsFDBNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return L2VPN_FAILURE;
    }
    pStartVb = pVbList;
    u4ArrLen = sizeof (au4VplsVpnId) / sizeof (UINT4);
    pVpnIdOid = alloc_oid ((INT4) u4ArrLen);
    if (pVpnIdOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }
    au4VplsVpnId[u4ArrLen - 1] = L2VPN_VPLS_INDEX (pVplsEntry);
    MEMCPY (pVpnIdOid->pu4_OidList, au4VplsVpnId, sizeof (au4VplsVpnId));
    pVpnIdOid->u4_Length = u4ArrLen;

    L2VPN_ARRAY_LEN (L2VPN_VPLS_VPN_ID (pVplsEntry), L2VPN_MAX_VPLS_VPNID_LEN,
                     u4Length);
    pVpnIdOctet = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString
        (L2VPN_VPLS_VPN_ID (pVplsEntry), (INT4) u4Length);
    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pVpnIdOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pVpnIdOctet, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4VplsAdminStatus) / sizeof (UINT4);
    pFdbHighOid = alloc_oid ((INT4) u4ArrLen);
    if (pFdbHighOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }
    au4VplsAdminStatus[u4ArrLen - 1] = L2VPN_VPLS_INDEX (pVplsEntry);
    MEMCPY (pFdbHighOid->pu4_OidList, au4VplsAdminStatus,
            sizeof (au4VplsAdminStatus));
    pFdbHighOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pFdbHighOid,
                                                  SNMP_DATA_TYPE_COUNTER32,
                                                  L2VPN_TRUE,
                                                  0, NULL, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        free_oid (pFdbHighOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4VplsOperStatus) / sizeof (UINT4);
    pFdbLowOid = alloc_oid ((INT4) u4ArrLen);
    if (pFdbLowOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        free_oid (pFdbHighOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }
    au4VplsOperStatus[u4ArrLen - 1] = L2VPN_VPLS_INDEX (pVplsEntry);
    MEMCPY (pFdbLowOid->pu4_OidList, au4VplsOperStatus,
            sizeof (au4VplsOperStatus));
    pFdbLowOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pFdbLowOid,
                                                  SNMP_DATA_TYPE_COUNTER32,
                                                  L2VPN_VPLS_OPER_STATUS
                                                  (pVplsEntry), 0, NULL, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        free_oid (pFdbHighOid);
        free_oid (pFdbLowOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
    if (L2VpnCheckNotifMaxRate () == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }
    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pVplsEntry);
#endif
    return L2VPN_SUCCESS;
}

INT4
L2VpnCheckNotifMaxRate ()
{
    if (gpL2VpnGlobalInfo->u4VplsNotificationMaxRate == L2VPN_ZERO)
    {
        return L2VPN_SUCCESS;
    }
    if (gpL2VpnGlobalInfo->b1IsNotifRateTmrStarted == L2VPN_FALSE)
    {
        gpL2VpnGlobalInfo->NotifRateTimer.u4Event =
            L2VPN_NOTIF_RATE_TMR_EXP_EVENT;
        if (TmrStartTimer
            (L2VPN_TIMER_LIST_ID, &gpL2VpnGlobalInfo->NotifRateTimer.AppTimer,
             SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Start of Notif Max Rate timer failed\r\n");
        }
        else
        {
            gpL2VpnGlobalInfo->b1IsNotifRateTmrStarted = L2VPN_TRUE;
            gpL2VpnGlobalInfo->u4VplsNotifCounter = L2VPN_ONE;
        }
    }
    else
    {
        gpL2VpnGlobalInfo->u4VplsNotifCounter++;
        if (gpL2VpnGlobalInfo->u4VplsNotifCounter >
            gpL2VpnGlobalInfo->u4VplsNotificationMaxRate)
        {
            return L2VPN_FAILURE;
        }
    }
    return L2VPN_SUCCESS;
}

VOID
L2VpnProcessNotifRateTmrExpiry ()
{
    if (gpL2VpnGlobalInfo->b1IsNotifRateTmrStarted == L2VPN_TRUE)
    {
        if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                          &(gpL2VpnGlobalInfo->NotifRateTimer.AppTimer)) !=
            TMR_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Stop of Notif Max rate timer failed\r\n");
            return;
        }
        gpL2VpnGlobalInfo->b1IsNotifRateTmrStarted = L2VPN_FALSE;
    }
}
#endif
#ifdef HVPLS_WANTED
/************************************************************************
 *  Function Name   : L2VpnVplsFdbAlarm 
 *  Description     : This function sends Notification for the FDB Clear 
 *                    and FDB Full to the Manager
 *  Input           : pVplsEntry : Pointer to Vpls Entry
 *                    u1State    : State tells whether is the FDB is Full 
 *                    Cleared.
 *  Output          : NONE
 *  Returns         : L2VPN_SUCCESS or L2VPN_FAILURE
 ************************************************************************/
INT1
L2VpnVplsFdbAlarm (tVPLSEntry * pVplsEntry, UINT1 u1State)
{
#ifdef SNMP_3_WANTED
    UINT4               u4ArrLen;
    UINT4               u4Length;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnIdOctet = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pVpnIdOid = NULL,
        *pFdbHighOid = NULL, *pFdbLowOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4VplsFDBNotify[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 1, 0, 0 };
    UINT4               au4VplsVpnId[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 1, 5, 1, 5, 1, 3, 0 };
    UINT4               au4VplsFdbHighWatermark[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 1, 5, 1, 5, 1, 6, 0 };
    UINT4               au4VplsFdbLowWatermark[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 1, 5, 1, 5, 1, 7, 0 };

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    /* Trap OID construction. Telling Manager that you have received
     * trap for fsMplsVplsNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return L2VPN_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4VplsFDBNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return L2VPN_FAILURE;
    }
    /* Fill the State Info. for which the Notification has to be triggered
     * (whether it is a FBD Full Alarm or FDB Clear Alarm)*/
    au4VplsFDBNotify[(sizeof (au4VplsFDBNotify) / sizeof (UINT4)) - 1] =
        u1State;
    MEMCPY (pOidValue->pu4_OidList, au4VplsFDBNotify,
            sizeof (au4VplsFDBNotify));
    pOidValue->u4_Length = sizeof (au4VplsFDBNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return L2VPN_FAILURE;
    }
    pStartVb = pVbList;
    u4ArrLen = sizeof (au4VplsVpnId) / sizeof (UINT4);
    pVpnIdOid = alloc_oid ((INT4) u4ArrLen);
    if (pVpnIdOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }
    au4VplsVpnId[u4ArrLen - 1] = L2VPN_VPLS_INDEX (pVplsEntry);
    MEMCPY (pVpnIdOid->pu4_OidList, au4VplsVpnId, sizeof (au4VplsVpnId));
    pVpnIdOid->u4_Length = u4ArrLen;

    L2VPN_ARRAY_LEN (L2VPN_VPLS_VPN_ID (pVplsEntry), L2VPN_MAX_VPLS_VPNID_LEN,
                     u4Length);
    pVpnIdOctet = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString
        (L2VPN_VPLS_VPN_ID (pVplsEntry), (INT4) u4Length);
    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pVpnIdOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pVpnIdOctet, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4VplsFdbHighWatermark) / sizeof (UINT4);
    pFdbHighOid = alloc_oid ((INT4) u4ArrLen);
    if (pFdbHighOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }
    au4VplsFdbHighWatermark[u4ArrLen - 1] = L2VPN_VPLS_INDEX (pVplsEntry);
    MEMCPY (pFdbHighOid->pu4_OidList, au4VplsFdbHighWatermark,
            sizeof (au4VplsFdbHighWatermark));
    pFdbHighOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pFdbHighOid,
                                                  SNMP_DATA_TYPE_COUNTER32,
                                                  L2VPN_VPLS_FDB_HIGH_THRESHOLD
                                                  (pVplsEntry), 0, NULL, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        free_oid (pFdbHighOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4VplsFdbLowWatermark) / sizeof (UINT4);
    pFdbLowOid = alloc_oid ((INT4) u4ArrLen);
    if (pFdbLowOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        free_oid (pFdbHighOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }
    au4VplsFdbLowWatermark[u4ArrLen - 1] = L2VPN_VPLS_INDEX (pVplsEntry);
    MEMCPY (pFdbLowOid->pu4_OidList, au4VplsFdbLowWatermark,
            sizeof (au4VplsFdbLowWatermark));
    pFdbLowOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pFdbLowOid,
                                                  SNMP_DATA_TYPE_COUNTER32,
                                                  L2VPN_VPLS_FDB_LOW_THRESHOLD
                                                  (pVplsEntry), 0, NULL, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pVpnIdOid);
        free_oid (pFdbHighOid);
        free_oid (pFdbLowOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return L2VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
#ifdef HVPLS_WANTED
    if (L2VpnCheckNotifMaxRate () == L2VPN_FAILURE)
    {
        return L2VPN_FAILURE;
    }
#endif

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pVplsEntry);
    UNUSED_PARAM (u1State);
#endif
    return L2VPN_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name : L2VpnGetPwIndexFromLabel                                  */
/* Description   : This routine gives the Pseudowire index corresponding to  */
/*                 the given label and interface combination.                */
/* Input(s)      : u4Label - MPLS label                                      */
/*                 u4IfIndex - Interface on which the label was configured   */
/* Output(s)     : pu4PwVcId - Pseudowire index corresponding to the label   */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnGetPwIndexFromLabel (UINT4 u4Label, UINT4 *pu4PwVcId)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromOutLabel (u4Label);

    if (pPwVcEntry == NULL)
    {
        *pu4PwVcId = 0;
        return L2VPN_FAILURE;
    }

    *pu4PwVcId = L2VPN_PWVC_INDEX (pPwVcEntry);
    return L2VPN_SUCCESS;
}

#ifdef HVPLS_WANTED
/*****************************************************************************
 *Function Name  :  L2VpnVlanIncFDBCounter                                   */
/* Description   : This routine increment the FDB Counter according          */
/*                 to FDB entry                                              */
/* Input(s)      : u4PwIndex - PwIndex                                       */
/* Return(s)     : Incremented Counter value                                 *
 *****************************************************************************/
INT4
L2VpnVlanIncFDBCounter (UINT4 u4PwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4Counter = 0;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    u4Counter = ++pVplsEntry->u4VplsFDBCounter;
    return ((INT4) u4Counter);

}

/********************************************************************************
 *Function Name  :  L2VpnVlanDecFDBCounter                                     */
/* Description   : This routine decrement the FDB Counter according            */
/*                 to FDB entry                                                */
/* Input(s)      : u4PwIndex - PwIndex                                         */
/* Return(s)     : Decremented Counter value   
  * *****************************************************************************/
INT4
L2VpnVlanDecFDBCounter (UINT4 u4PwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4Counter = 0;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    u4Counter = --pVplsEntry->u4VplsFDBCounter;
    return ((INT4) u4Counter);

}

/*****************************************************************************
 *Function Name  : L2VpnVlanGetFDBHighThreshold                              */
/* Description   : This routine gives High Threshold value of FDB entry      */
/* Input(s)      : u4PwIndex - PwIndex                                       */
/* Output        : pu4VplsFDBHighThreshold - High Threshold value            */
/* Return(s)     : L2VPN_SUCCESS_L2VPN_FAILURE
  ***************************************************************************/
VOID
L2VpnVlanGetFDBHighThreshold (UINT4 u4PwIndex,
                              UINT4 *pu4RetValFsMplsVplsFdbHighWatermark)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return;
    }
    nmhGetFsMplsVplsFdbHighWatermark (pPwVcEntry->u4VplsInstance,
                                      pu4RetValFsMplsVplsFdbHighWatermark);

}

/*****************************************************************************
 * Function Name : L2VpnVlanGetFDBLowThreshold                               */
/* Description   : This routine gives Low Threshold value of FDB entry       */
/* Input(s)      : u4PwIndex - PwIndex                                       */
/* Output        : pu4VplsFDBLowThreshold - Low Threshold value              */
/* Return(s)     : L2VPN_SUCCESS_L2VPN_FAILURE
 *   *************************************************************************/
VOID
L2VpnVlanGetFDBLowThreshold (UINT4 u4PwIndex,
                             UINT4 *pu4RetValFsMplsVplsFdbLowWatermark)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return;
    }
    nmhGetFsMplsVplsFdbLowWatermark (pPwVcEntry->u4VplsInstance,
                                     pu4RetValFsMplsVplsFdbLowWatermark);

}

/*****************************************************************************
 * Function Name : L2VpnVlanGetVpnIdFromPwIndex                                         */
/* Description   : This routine gives Vpn Id  of a Vpls entry                */
/* Input(s)      : u4PwIndex - PwIndex                                       */
/* Output        : pu4VpnId - Vpn Id                                         */
/* Return(s)     : L2VPN_SUCCESS_L2VPN_FAILURE                               *
 * ***************************************************************************/
VOID
L2VpnVlanGetVpnIdFromPwIndex (UINT4 u4PwIndex, UINT4 *pu4VpnId)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = L2VPN_ZERO;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return;
    }
    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return;
    }
    MEMCPY (&u4VpnId, &(pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
            sizeof (UINT4));

    u4VpnId = OSIX_HTONL (u4VpnId);
    *pu4VpnId = u4VpnId;

}

/*****************************************************************************
 * Function Name : L2VpnSendVplsFDBAlarmRaised                               */
/* Description   : This routine gives Vpn calls L2VpnVplsFdbAlarm and raised */
/*                 a Full alarm notification                                 */
/* Input(s)      : u4VpnId - Vpn Id                                          */
/* Return(s)     : L2VPN_SUCCESS_L2VPN_FAILURE                               *
 *  * ************************************************************************/
INT4
L2VpnSendVplsFDBAlarmRaised (UINT4 u4VpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    pVplsEntry = L2VpnGetVplsEntryFromVpnId (u4VpnId);
    if (L2VPN_VPLS_NOTIF_ENABLE_STATUS == L2VPN_FALSE)
    {
        return L2VPN_SUCCESS;
    }
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    if (L2VpnVplsFdbAlarm (pVplsEntry, VPLS_FWD_FULL_ALARM_RAISED) ==
        L2VPN_SUCCESS)
    {
        return L2VPN_SUCCESS;
    }

    return L2VPN_FAILURE;

}

/*****************************************************************************
 * Function Name : L2VpnSendVplsFDBAlarmCleared                              */
/* Description   : This routine gives Vpn calls L2VpnVplsFdbAlarm and clear  */
/*                 a Full alarm notification                                 */
/* Input(s)      : u4VpnId - Vpn Id                                          */
/* Return(s)     : L2VPN_SUCCESS_L2VPN_FAILURE                               *
 *  * ************************************************************************/
INT4
L2VpnSendVplsFDBAlarmCleared (UINT4 u4VpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    if (L2VPN_VPLS_NOTIF_ENABLE_STATUS == L2VPN_FALSE)
    {
        return L2VPN_SUCCESS;
    }
    pVplsEntry = L2VpnGetVplsEntryFromVpnId (u4VpnId);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    if (L2VpnVplsFdbAlarm (pVplsEntry, VPLS_FWD_FULL_ALARM_CLEARED) ==
        L2VPN_SUCCESS)
    {
        return L2VPN_SUCCESS;
    }

    return L2VPN_FAILURE;

}
#endif
/*****************************************************************************/
/* Function Name :  PwEntryHwACExistsCheck                                   */
/* Description   : This routine checks whether the attachment circuit exists  */
/*                 in the hardware                                           */
/* Input(s)      : pVplsEntry -Pointer to vpls entry                          */
/*                 u4PwEnetPortVlan:Vlan on which mapping has been done    */
/*                 i4PortIfIndex - Port index of vpls                       */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
PwEntryHwACExistsCheck (tVPLSEntry * pVplsEntry,
                        UINT4 u4PwEnetPortVlan, INT4 i4PortIfIndex)
{
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (NULL == pPwEntry)
        {
            return L2VPN_FAILURE;
        }
        if (NULL == L2VPN_PWVC_ENET_ENTRY (pPwEntry))
        {
            continue;
        }
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pPwEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                 u4PwEnetPortVlan) &&
                (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                 i4PortIfIndex) && (pPwVcEnetEntry->u1HwStatus == MPLS_TRUE))
            {
                return L2VPN_SUCCESS;
            }
        }

    }
    return L2VPN_FAILURE;

}

/*****************************************************************************/
/* Function Name : PwEntryACExistsCheck                                      */
/* Description   : This routine checks whether this port vlan/port if index  */
/*                 already associated with any of the PW.                    */
/* Input(s)      : i4ContextId - Context id
 *                 u4PwVcIndex - Pseudo wire index                           */
/*                 u4PwEnetPortVlan - Port Vlan                              */
/*                 i4PortIfIndex - Port If Index                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
PwEntryACExistsCheck (INT4 i4ContextId, UINT4 u4PwVcIndex,
                      UINT4 u4PwEnetPortVlan, INT4 i4PortIfIndex,
                      BOOL1 bIsCheckOnSameVpls)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pInPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;
    UINT4               u4PwIndex = L2VPN_ONE;
    UINT4               u4RedGrpIndex = L2VPN_ZERO;
    pInPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4PortIfIndex);

    for (; u4PwIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
        if ((u4PwVcIndex == u4PwIndex)
            || (pPwVcEntry == NULL) ||
            (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL))
        {
            continue;
        }

        pVplsEntry =
            L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->u4VplsInstance);

        if ((pVplsEntry == NULL)
            || (i4ContextId != L2VPN_VPLS_VSI (pVplsEntry)))
        {
            continue;
        }

        if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
        {

            if (pInPwVcEntry == NULL)
            {
                return L2VPN_FAILURE;
            }

            if ((bIsCheckOnSameVpls) &&
                (L2VPN_PWVC_VPLS_INDEX (pInPwVcEntry) !=
                 L2VPN_PWVC_VPLS_INDEX (pPwVcEntry)))
            {
                continue;
            }
            else if ((!bIsCheckOnSameVpls) &&
                     (L2VPN_PWVC_VPLS_INDEX (pInPwVcEntry) ==
                      L2VPN_PWVC_VPLS_INDEX (pPwVcEntry)))
            {
                continue;
            }

            /* Continue if any PWs in the VPLS list is not UP */
            if ((bIsCheckOnSameVpls) &&
                (pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_OPER_UP))
            {
                continue;
            }
        }
        /* Check if a pwEntry already exists for the same vlan /port */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) != L2VPN_PWVC_ACTIVE)
            {
                continue;
            }
            if ((u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN) &&
                (i4PortIfIndex != L2VPN_ZERO))
            {
                if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                     u4PwEnetPortVlan) &&
                    (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                     i4PortIfIndex))
                {
                    if (L2VPN_FAILURE ==
                        L2VpnUtilCheckIfPwIsAssoWithGrp (u4PwIndex,
                                                         &u4RedGrpIndex))
                    {
                        return L2VPN_SUCCESS;
                    }
                    /* Check if this pseudowire has RG group if not return failure */
                    else
                    {
                        if (L2VPN_FAILURE ==
                            L2VpnUtilCheckIfPwIsAssoWithGrp (u4PwVcIndex,
                                                             &u4RedGrpIndex))
                        {
                            return L2VPN_SUCCESS;
                        }
                    }
                    return L2VPN_FAILURE;
                }
            }
            else if (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
            {
                if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                    u4PwEnetPortVlan)
                {

                    if (L2VPN_FAILURE ==
                        L2VpnUtilCheckIfPwIsAssoWithGrp (u4PwIndex,
                                                         &u4RedGrpIndex))
                    {
                        return L2VPN_SUCCESS;
                    }
                    /* Check if this pseudowire has RG group if not return failure */
                    else
                    {
                        if (L2VPN_FAILURE ==
                            L2VpnUtilCheckIfPwIsAssoWithGrp (u4PwVcIndex,
                                                             &u4RedGrpIndex))
                        {
                            return L2VPN_SUCCESS;
                        }
                    }
                    return L2VPN_FAILURE;

                }
            }
            else
            {
                if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                    i4PortIfIndex)
                {
                    if (L2VPN_FAILURE ==
                        L2VpnUtilCheckIfPwIsAssoWithGrp (u4PwIndex,
                                                         &u4RedGrpIndex))
                    {
                        return L2VPN_SUCCESS;
                    }
                    /* Check if this pseudowire has RG group if not return failure */
                    else
                    {
                        if (L2VPN_FAILURE ==
                            L2VpnUtilCheckIfPwIsAssoWithGrp (u4PwVcIndex,
                                                             &u4RedGrpIndex))
                        {
                            return L2VPN_SUCCESS;
                        }
                    }

                    return L2VPN_FAILURE;
                }
            }
        }
    }

    if ((pMplsPortEntryInfo == NULL) ||
        (pMplsPortEntryInfo->i1RowStatus != ACTIVE))
    {
        return L2VPN_FAILURE;
    }
    if (i4PortIfIndex != 0)
    {
        if (L2VpnPortServiceStatus (i4PortIfIndex) == L2VPN_FAILURE)
        {
            return L2VPN_SUCCESS;
        }
    }
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckForAllPwsDown                                   */
/* Description   : This routine checks whether all PWs are DOWN in the       */
/*                 VPLS instance                                             */
/* Input(s)      : u4VplsIndex - VPLS instance                               */
/* Output(s)     : pbIsAllPwsDown                                            */
/*                   - TRUE - All Pws are down                               */
/*                   - FALSE - All Pw are not down                           */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnCheckForAllPwsDown (UINT4 u4VplsIndex, BOOL1 * pbIsAllPwsDown)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;

    *pbIsAllPwsDown = FALSE;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    /* Check for any of the PWs down */
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (L2VPN_PWVC_OPER_STATUS (pPwEntry) == L2VPN_PWVC_OPER_UP)
        {
            return L2VPN_FAILURE;
        }
    }
    *pbIsAllPwsDown = TRUE;
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckAndGetLastOperUpPw                              */
/* Description   : This routine checks whether and returns the last          */
/*                 operationally UP PWs                                      */
/* Input(s)      : u4VplsIndex - VPLS instance                               */
/* Output(s)     : pu4PwVcIndex - Last Operationally UP PW                   */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnCheckAndGetLastOperUpPw (UINT4 u4VplsIndex, UINT4 *pu4PwVcIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    UINT4               u4Count = L2VPN_ZERO;
#endif
    *pu4PwVcIndex = L2VPN_ZERO;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    /* Check for any of the PWs down */
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (L2VPN_PWVC_OPER_STATUS (pPwEntry) == L2VPN_PWVC_OPER_UP)
        {
            if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
            {
                if (TMO_SLL_Count (L2VPN_PWVC_ENET_ENTRY_LIST
                                   ((tPwVcEnetServSpecEntry *)
                                    L2VPN_PWVC_ENET_ENTRY (pPwEntry)))
                    == L2VPN_ZERO)
                {
                    continue;
                }
            }
            if (*pu4PwVcIndex != L2VPN_ZERO)
            {
                *pu4PwVcIndex = L2VPN_ZERO;
                return L2VPN_FAILURE;
            }
            *pu4PwVcIndex = pPwEntry->u4PwVcIndex;
        }
    }
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    if (NULL == pPwVplsNode)
    {
        L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry) = u4VplsIndex;
        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry) = L2VPN_ZERO;
        while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
               != MPLS_FAILURE)
        {
            u4Count++;
            if (u4VplsIndex !=
                L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry))
            {
                return L2VPN_SUCCESS;
            }
            if (u4Count > L2VPN_ONE)
            {
                *pu4PwVcIndex = L2VPN_ZERO;
                return L2VPN_FAILURE;
            }
        }
    }
#endif
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnCheckValidTnlForPw                                   */
/* Description   : This routine checks whether the Tunnel to be              */
/*                 associated with the PW is valid                           */
/* Input(s)      : pPwVcEntry  - Pw entry structure                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnCheckValidTnlForPw (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeInTnlInfo = NULL;
    UINT4               u4OutLclLsr = 0;
    UINT4               u4OutPeerLsr = 0;
    UINT4               u4InLclLsr = 0;
    UINT4               u4InPeerLsr = 0;
    UINT4               u4OutTnlId = 0;
    UINT4               u4InTnlId = 0;
    UINT4               u4OutTnlMode = 0;
    UINT4               u4InTnlMode = 0;
    BOOL1               bIsOutTnlReqd = TRUE;

    /*Get the Outgoing Tunnel Info */
    pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                   L2VPN_PWVC_PSN_ENTRY
                                                   (pPwVcEntry));

    pPwVcMplsInTnlEntry =
        L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                  L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if ((pPwVcMplsTnlEntry == NULL) || (pPwVcMplsInTnlEntry == NULL))
    {
        return L2VPN_FAILURE;
    }

    MEMCPY (&u4OutLclLsr,
            &(L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR (pPwVcMplsTnlEntry)),
            IPV4_ADDR_LENGTH);
    MEMCPY (&u4OutPeerLsr,
            &(L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR (pPwVcMplsTnlEntry)),
            IPV4_ADDR_LENGTH);
    u4OutLclLsr = OSIX_HTONL (u4OutLclLsr);
    u4OutPeerLsr = OSIX_HTONL (u4OutPeerLsr);
    u4OutTnlId = L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pPwVcMplsTnlEntry);
    /* If the Peer and Local LSR of Out going Tunnel is already configured,
     * existance of tunnel is made optional */
    if ((u4OutPeerLsr != 0) && (u4OutLclLsr != 0))
    {
        bIsOutTnlReqd = FALSE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTnlInfoByOwnerAndRole (u4OutTnlId, 0,
                                             u4OutLclLsr, u4OutPeerLsr,
                                             TE_TNL_OWNER_SNMP, L2VPN_ZERO);

    /*Get the Incoming Tunnel Info */
    MEMCPY (&u4InLclLsr, &(L2VPN_PWVC_MPLS_IN_TNL_TNL_LCLLSR
                           (pPwVcMplsInTnlEntry)), IPV4_ADDR_LENGTH);
    MEMCPY (&u4InPeerLsr, &(L2VPN_PWVC_MPLS_IN_TNL_TNL_PEERLSR
                            (pPwVcMplsInTnlEntry)), IPV4_ADDR_LENGTH);
    u4InTnlId = L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pPwVcMplsInTnlEntry);
    u4InLclLsr = OSIX_HTONL (u4InLclLsr);
    u4InPeerLsr = OSIX_HTONL (u4InPeerLsr);
    pTeInTnlInfo = TeGetTnlInfoByOwnerAndRole (u4InTnlId, 0,
                                               u4InLclLsr, u4InPeerLsr,
                                               L2VPN_ZERO, L2VPN_ZERO);

    if (pTeTnlInfo != NULL)
    {
        u4OutTnlMode = pTeTnlInfo->u4TnlMode;
        /* Outgoing Tunnel Role is not Ingress */
        if (!
            ((pTeTnlInfo->u1TnlRole == TE_INGRESS)
             ||
             ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
              && (pTeTnlInfo->u1TnlRole == TE_EGRESS))))
        {
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }
        /* Pw mapped over p2mp - should be unidirectional */
        /* Pw mapped over p2mp - is not supported for dynamic */
        if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP)
        {
            if ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                || (L2VPN_IS_STATIC_PW (pPwVcEntry) == FALSE))
            {
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
        }

        /* PW cannot be associated to S-LSP when it is stitched to an LSP. */
        if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_SLSP)
        {
            if (pTeTnlInfo->pMapTnlInfo != NULL)
            {
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
        }
    }
    else
    {
        /* Skip the verification of existence of tunnel, if the
         * flag is set to FALSE. */
        if ((bIsOutTnlReqd == TRUE) && (u4OutTnlId != 0))
        {
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }
        else
        {
            if ((u4OutTnlId != 0) && (pTeInTnlInfo != NULL) &&
                (pTeInTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP))
            {
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
        }

    }

    /*If Tunnel entry does not exist */
    if (pTeInTnlInfo != NULL)
    {
        u4InTnlMode = pTeInTnlInfo->u4TnlMode;
        /*Check whether Incoming Tunnel Role is not Egress */
        if (!((pTeInTnlInfo->u1TnlRole == TE_EGRESS) ||
              ((pTeInTnlInfo->u4TnlMode
                == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
               (pTeInTnlInfo->u1TnlRole == TE_INGRESS))))
        {
            MPLS_CMN_UNLOCK ();
            return L2VPN_FAILURE;
        }
        /* Pw mapped over p2mp - should be unidirectional */
        /* Pw mapped over p2mp - is not supported for dynamic */
        if (pTeInTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP)
        {
            if ((u4OutTnlId != 0) || (L2VPN_IS_STATIC_PW (pPwVcEntry) == FALSE))
            {
                MPLS_CMN_UNLOCK ();
                return L2VPN_FAILURE;
            }
        }
    }
    else if (u4OutTnlId == 0)
    {
        MPLS_CMN_UNLOCK ();
        return L2VPN_FAILURE;
    }

    if (((u4InTnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) ||
         (u4OutTnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)) &&
        ((u4OutTnlId != u4InTnlId) || (u4OutLclLsr != u4InLclLsr) ||
         (u4OutPeerLsr != u4InPeerLsr)))
    {
        MPLS_CMN_UNLOCK ();
        return L2VPN_FAILURE;
    }

    MPLS_CMN_UNLOCK ();

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnUtilIsRemoteStatusCapable                            */
/* Description   : This routine checks whether self and peer are remote      */
/*                 status capable.                                           */
/* Input(s)      : pPwVcEntry  - Pw entry structure                          */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE  - If Self and Peer are both remote status           */
/*                         capable                                           */
/*                 FALSE - If Self or Peer is remote status capable          */
/*****************************************************************************/
BOOL1
L2VpnUtilIsRemoteStatusCapable (tPwVcEntry * pPwVcEntry)
{
    if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) == L2VPN_PWVC_RMT_STAT_CAPABLE)
    {
        return TRUE;
    }

    if (((pPwVcEntry->u1MapStatus & L2VPN_MAP_RCVD) != L2VPN_MAP_RCVD) &&
        (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION))
    {
        return TRUE;
    }

    return FALSE;
}

/*****************************************************************************/
/* Function Name : L2VpnUtilIsRemoteStatusNotCapable                         */
/* Description   : This routine checks whether self and peer are remote      */
/*                 status not capable.                                       */
/* Input(s)      : pPwVcEntry  - Pw entry structure                          */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE  - If Self and Peer are both remote status           */
/*                         not capable                                       */
/*                 FALSE - If Self or Peer is remote status not              */
/*                         capable                                           */
/*****************************************************************************/
BOOL1
L2VpnUtilIsRemoteStatusNotCapable (tPwVcEntry * pPwVcEntry)
{
    if (L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry) == L2VPN_PWVC_RMT_STAT_NOT_CAPABLE)
    {
        return TRUE;
    }

    if (((pPwVcEntry->u1MapStatus & L2VPN_MAP_RCVD) != L2VPN_MAP_RCVD) &&
        (!(L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) & L2VPN_PW_STATUS_INDICATION)))
    {
        return TRUE;
    }

    return FALSE;
}

/*****************************************************************************/
/* Function Name : L2vpnUtilIsPwAcMatched                                    */
/* Description   : This function checks whether Pw AC is matched for IF UP   */
/*                 or IF DOWN Event.                                         */
/* Input(s)      : pPwVcEntry - Pointer to PW VC Entry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to the tPwVcEnetEntry or NULL                     */
/*****************************************************************************/
BOOL1
L2VpnUtilIsPwAcMatched (tPwVcEntry * pPwVcEntry,
                        tPwVcEnetEntry * pPwVcEnetEntry, UINT4 u4IfIndex,
                        UINT2 u2PortVlan)
{
    if (NULL == pPwVcEntry)
    {
        return TRUE;
    }
    if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) != L2VPN_PWVC_ADMIN_UP)
    {
        return FALSE;
    }

    if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PWVC_PSN_TYPE_MPLS)
    {
        return FALSE;
    }

    if ((u4IfIndex != L2VPN_ZERO) &&
        (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) != (INT4) u4IfIndex))
    {
        return FALSE;
    }

    if ((u2PortVlan != L2VPN_ZERO) &&
        (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
         L2VPN_PWVC_ENET_DEF_PORT_VLAN) &&
        (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) != u2PortVlan))
    {
        return FALSE;
    }

    return TRUE;
}

/*****************************************************************************/
/* Function Name : L2vpnUtilIsPsnTeTnlP2MP                                   */
/* Description   : This function checks whether TE Tnl mapped to VC entry    */
/*                   is P2MP is matched for IF UP                            */
/* Input(s)      : pPwVcEntry - Pointer to PW VC Entry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE or FALSE                                             */
/*****************************************************************************/
BOOL1
L2vpnUtilIsPsnTeTnlP2MP (tPwVcEntry * pPwVcEntry)
{
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeInTnlInfo = NULL;
    UINT4               u4LclLsr = 0;
    UINT4               u4PeerLsr = 0;
    UINT4               u4OutTnlId = 0;
    UINT4               u4InTnlId = 0;

    pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                   L2VPN_PWVC_PSN_ENTRY
                                                   (pPwVcEntry));

    pPwVcMplsInTnlEntry =
        L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                  L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if ((pPwVcMplsTnlEntry == NULL) || (pPwVcMplsInTnlEntry == NULL))
    {
        return L2VPN_FAILURE;
    }

    /*Get the Outgoing Tunnel Info */
    MEMCPY (&u4LclLsr,
            &(L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR (pPwVcMplsTnlEntry)),
            IPV4_ADDR_LENGTH);
    MEMCPY (&u4PeerLsr,
            &(L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR (pPwVcMplsTnlEntry)),
            IPV4_ADDR_LENGTH);
    u4LclLsr = OSIX_HTONL (u4LclLsr);
    u4PeerLsr = OSIX_HTONL (u4PeerLsr);

    u4OutTnlId = L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pPwVcMplsTnlEntry);

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTnlInfoByOwnerAndRole (u4OutTnlId, 0,
                                             u4LclLsr, u4PeerLsr,
                                             TE_TNL_OWNER_SNMP, L2VPN_ZERO);

    if ((pTeTnlInfo != NULL) && (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP))
    {
        MPLS_CMN_UNLOCK ();
        return TRUE;
    }

    /*Get the Incoming Tunnel Info */
    MEMCPY (&u4LclLsr, &(L2VPN_PWVC_MPLS_IN_TNL_TNL_LCLLSR
                         (pPwVcMplsInTnlEntry)), IPV4_ADDR_LENGTH);
    MEMCPY (&u4PeerLsr, &(L2VPN_PWVC_MPLS_IN_TNL_TNL_PEERLSR
                          (pPwVcMplsInTnlEntry)), IPV4_ADDR_LENGTH);
    u4InTnlId = L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pPwVcMplsInTnlEntry);
    u4LclLsr = OSIX_HTONL (u4LclLsr);
    u4PeerLsr = OSIX_HTONL (u4PeerLsr);
    pTeInTnlInfo = TeGetTnlInfoByOwnerAndRole (u4InTnlId, 0,
                                               u4LclLsr, u4PeerLsr,
                                               L2VPN_ZERO, L2VPN_ZERO);

    if ((pTeInTnlInfo != NULL) && (pTeInTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP))
    {
        MPLS_CMN_UNLOCK ();
        return TRUE;
    }

    MPLS_CMN_UNLOCK ();
    return FALSE;
}

/*****************************************************************************/
/* Function Name : L2VpnValidateLenForAIIType                                */
/* Description   : This function checks for the AII Type and validates       */
/* the length for the corresponding AII Type                                 */
/* Input(s)      : pPwVcEntry - Pointer to PW VC Entry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE or FALSE                                             */
/*****************************************************************************/

UINT4
L2VpnValidateLenForAIIType (UINT4 u4PwAiiType, UINT1 u1PwAiiLen)
{

    if (((u4PwAiiType == L2VPN_ZERO) && (u1PwAiiLen != L2VPN_ZERO)) ||
        ((u4PwAiiType != L2VPN_ZERO) && (u1PwAiiLen == L2VPN_ZERO)))
    {
        return L2VPN_SUCCESS;
    }

    if (((u4PwAiiType == L2VPN_GEN_FEC_AII_TYPE_1)
         && (u1PwAiiLen == L2VPN_PWVC_MAX_AI_TYPE1_LEN)) ||
        ((u4PwAiiType == L2VPN_GEN_FEC_AII_TYPE_2)
         && (u1PwAiiLen == L2VPN_PWVC_MAX_AI_TYPE2_LEN)))
    {
        return L2VPN_SUCCESS;
    }

    return L2VPN_FAILURE;
}

/******************************************************************************
 *  Function Name : L2VpnGetMultiplexBundleStatus
 *  Description   : This function used for checking that multiplexing or bundling 
 *                  is done on a given port.
 *  Input(s)      : u4PortIfIndex - Interface Index
 *                  u1PortService - MPLS_MULTIPLEX/MPLS_BUNDLING 
 *
 *  Output(s)     : None
 *  Return(s)     : L2VPN_SUCCESS / L2VPN_FAILURE
 *
 * ******************************************************************************/
INT4
L2VpnGetMultiplexBundleStatus (UINT4 u4PortIfIndex, UINT1 u1PortService)
{

    tPwVcEntry         *pPwEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    UINT4               u4Index = L2VPN_ONE;
    UINT4               u4VlanId = 0;
    UINT4               u4VplsInstance = 0;
    BOOL1               bFlag = TRUE;

    if ((u1PortService != MPLS_MULTIPLEX) && (u1PortService != MPLS_BUNDLE))
    {
        return L2VPN_SUCCESS;
    }

    for (; u4Index <= L2VPN_MAX_VPLS_ENTRIES; u4Index++)
    {

        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry == NULL)
        {
            continue;
        }

        /* This Scan is done for the checking, if multiplexing is disabled then 
         * more than one VPLS cannot mapped to same port.In case of Bundling is 
         * disabled then port can be part of only one Ac.*/

        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
            if (L2VPN_PWVC_ENET_ENTRY (pPwEntry) != NULL)
            {
                TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwEntry)),
                              pPwVcEnetEntry,
                              tPwVcEnetEntry *)
                    if ((L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                         (INT4) u4PortIfIndex))
                {
                    if ((bFlag) && (u1PortService == MPLS_MULTIPLEX))
                    {
                        u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pVplsEntry);
                        bFlag = FALSE;
                    }
                    else if (u4VplsInstance !=
                             L2VPN_PWVC_VPLS_INDEX (pVplsEntry)
                             && (u1PortService == MPLS_MULTIPLEX))
                    {
                        return L2VPN_SUCCESS;
                    }
                    else if ((u4VplsInstance !=
                              L2VPN_PWVC_VPLS_INDEX (pVplsEntry))
                             && (u1PortService == MPLS_BUNDLE))
                    {
                        u4VplsInstance = L2VPN_PWVC_VPLS_INDEX (pVplsEntry);
                        u4VlanId = L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                    }
                    else if ((u4VlanId !=
                              L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                             && (u4VplsInstance ==
                                 L2VPN_PWVC_VPLS_INDEX (pVplsEntry))
                             && (u1PortService == MPLS_BUNDLE))
                    {
                        return L2VPN_SUCCESS;
                    }
                }
            }
        }
    }
    return L2VPN_FAILURE;
}

/******************************************************************************
 *  Function Name : L2VpnPortServiceStatus
 *  Description   : This function used for checking that multiplexing or bundling
 *                  is done on the port or not.
 *  Input(s)      : i4PortIfIndex - Interface Index
 *
 *  Output(s)     : None
 *  Return(s)     : L2VPN_SUCCESS / L2VPN_FAILURE
 *
 * ******************************************************************************/
INT4
L2VpnPortServiceStatus (INT4 i4PortIfIndex)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4PortIfIndex);

    /*This condition won't be fail when its callling for PwEntryACExistsCheck */
    if (pMplsPortEntryInfo == NULL)
    {
        return L2VPN_FAILURE;
    }

    /*if all to one bundling is enable or multiplexing is disable then same 
     * port cann't be the part of different VPLS.*/
    if ((pMplsPortEntryInfo->u1AllToOneBundleStatus == MPLS_ENABLE) ||
        ((pMplsPortEntryInfo->u1MultiplexStatus == MPLS_DISABLE) &&
         (pMplsPortEntryInfo->u1AllToOneBundleStatus == MPLS_DISABLE)))
    {
        if (L2VpnGetMultiplexBundleStatus
            (pMplsPortEntryInfo->u4IfIndex, MPLS_MULTIPLEX) == L2VPN_SUCCESS)
        {
            return L2VPN_FAILURE;
        }
    }

    /*if Bundling is disable then the port can be part of only one AC. */
    if ((pMplsPortEntryInfo->u1BundleStatus == MPLS_DISABLE) &&
        (pMplsPortEntryInfo->u1AllToOneBundleStatus == MPLS_DISABLE))
    {
        if (L2VpnGetMultiplexBundleStatus
            (pMplsPortEntryInfo->u4IfIndex, MPLS_BUNDLE) == L2VPN_SUCCESS)

        {
            return L2VPN_FAILURE;
        }
    }

    return L2VPN_SUCCESS;
}

/******************************************************************************
 *  Function Name : L2VpnValidatePortStatus
 *  Description   : This function Validate the Port status Entry.
 *  Input(s)      : i4PortIfIndex - Interface Index
 *                  i4TestValue - Test Value.
 *
 *  Output(s)     : None
 *  Return(s)     : L2VPN_SUCCESS / L2VPN_FAILURE
 * ******************************************************************************/
INT4
L2VpnValidatePortStatus (INT4 i4PortIfIndex)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4PortIfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return L2VPN_FAILURE;
    }
    if (pMplsPortEntryInfo->i1RowStatus == MPLS_STATUS_ACTIVE)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnUtilHashIfIndexCmp                                   */
/* Description   : This function is the call back function provided          */
/*                 to FSAP2 for inserting the Enet node bucket in the        */
/*                 hash table.                                               */
/*                 This function groups the Enet based on the i4PortIfIndex  */
/*                 and u2PortVlan.                                           */
/* Input(s)      : pCurNode - The Current node in the hash table with        */
/*                            which the Interface Index is to be compared.   */
/*                 ptr      - UINT1 pointer. Pointer to the newly adding     */
/*                              Node.                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : INSERT_PRIORTO or MATCH_NOT_FOUND                          */
/*****************************************************************************/

UINT4
L2VpnUtilHashIfIndexCmp (tTMO_HASH_NODE * pCurNode, UINT1 *ptr)
{
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEnetEntry     *pInPwVCEnet = NULL;

    pInPwVCEnet = (tPwVcEnetEntry *) ((VOID *) ptr);

    pPwVcEnetEntry = MPLS_PWVC_FROM_ENETLIST (pCurNode);

    if ((pPwVcEnetEntry->i4PortIfIndex > pInPwVCEnet->i4PortIfIndex) &&
        (pPwVcEnetEntry->u2PortVlan > pInPwVCEnet->u2PortVlan))
    {
        return INSERT_PRIORTO;
    }
    else if ((pPwVcEnetEntry->i4PortIfIndex == pInPwVCEnet->i4PortIfIndex) &&
             (pPwVcEnetEntry->u2PortVlan > pInPwVCEnet->u2PortVlan))
    {
        return INSERT_PRIORTO;
    }

    return MATCH_NOT_FOUND;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilGetRedGrpIdFromRedGrpName 
 *
 * DESCRIPTION      : This function will get the redundancy group Id
 *                    based on the Protection Group Name
 *
 * INPUT            : pu1RgName - Redundancy Group Name
 *
 * OUTPUT           : u4RgGrpIndex - Redundancy group index
 *
 * RETURNS          : u4PgId- Redundancy Group Id. vlaue 0 will be
 *                            returned in case no valid RG id found
 *
 **************************************************************************/
PUBLIC UINT4
L2VpnUtilGetRedGrpIdFromRedGrpName (UINT1 *pu1RgName)
{

    tL2vpnRedundancyEntry *pRgGroup = NULL;
    tL2vpnRedundancyEntry *pNextRgGroup = NULL;

    pRgGroup =
        (tL2vpnRedundancyEntry *) RBTreeGetFirst (gL2vpnRgGlobals.RgList);

    if (pRgGroup == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "the redundancy group is not available\r\n");
        return L2VPN_FAILURE;
    }

    pNextRgGroup = pRgGroup;

    do
    {
        pRgGroup = pNextRgGroup;

        if (STRNCMP (pRgGroup->au1RgGrpName, pu1RgName,
                     STRLEN (pu1RgName)) == 0)
        {
            return (pRgGroup->u4Index);
        }
    }
    while ((pNextRgGroup = L2VpnUtilGetNextRGNode (pRgGroup)) != NULL);

    return (L2VPN_FAILURE);
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilGetNextRGNode
 *
 * DESCRIPTION      : This function returns the next RG Node Info from 
 *                    the redundancy group table
 *
 * INPUT            : pRgGroup - Current RG Node
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : tL2vpnRedundancyEntry * Returns the pointer to the next 
 *                    RG node in the RG table. if no next entry is available 
 *                    in the RG table then it returns NULL.                    
 **************************************************************************/
PUBLIC tL2vpnRedundancyEntry *
L2VpnUtilGetNextRGNode (tL2vpnRedundancyEntry * pRgGroup)
{
    tL2vpnRedundancyEntry *pNextRgGroup = NULL;

    pNextRgGroup = (tL2vpnRedundancyEntry *)
        RBTreeGetNext (gL2vpnRgGlobals.RgList, (tRBElem *) pRgGroup, NULL);

    return pNextRgGroup;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilGetNextRedundancyPwEntryByIndex
 *
 * DESCRIPTION      : This function returns the next Redundat pseudowire entry 
 *                    Node Info from the redundancy PW List table
 *
 * INPUT            : u4RgIndex - Redundancy group index 
 *                    u4PwIndex - Pw Index
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : tL2vpnRedundancyPwEntry * Returns the pointer to the next 
 *                    RG PW list node. if no next entry is available 
 *                    in the RG table then it returns NULL.                    
 **************************************************************************/
tL2vpnRedundancyPwEntry *
L2VpnUtilGetNextRedundancyPwEntryByIndex (UINT4 u4RgIndex, UINT4 u4PwIndex)
{
    tL2vpnRedundancyPwEntry *pTmpPwEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;

    pTmpPwEntry = MemAllocMemBlk (L2VPN_REDUNDANCY_PW_POOL_ID);
    if (pTmpPwEntry == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpPwEntry, 0, sizeof (*pTmpPwEntry));
    pTmpPwEntry->u4RgIndex = u4RgIndex;
    pTmpPwEntry->u4PwIndex = u4PwIndex;

    pRgPw =
        RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pTmpPwEntry, NULL);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_PW_POOL_ID, (VOID *) pTmpPwEntry);
    return pRgPw;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilGetNextRedundancyNodeEntryByIndex
 *
 * DESCRIPTION      : This function returns the next Redundat pseudowire entry 
 *                    Node Info from the redundancy Node List table
 *
 * INPUT            : u4RgIndex - Redundancy group index 
 *                    u1AddrType- Address type
 *                    Addr      - Node address
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : tL2vpnRedundancyPwEntry * Returns the pointer to the next 
 *                    RG PW list node. if no next entry is available 
 *                    in the RG table then it returns NULL.                    
 **************************************************************************/
tL2vpnRedundancyNodeEntry *
L2VpnUtilGetNextRedundancyNodeEntryByIndex (UINT4 u4RgIndex,
                                            UINT1 u1AddrType, uGenAddr Addr)
{
    tL2vpnRedundancyNodeEntry *pTmpNode = NULL;
    tL2vpnRedundancyNodeEntry *pNodeRb = NULL;

    pTmpNode = MemAllocMemBlk (L2VPN_REDUNDANCY_NODE_POOL_ID);
    if (pTmpNode == NULL)
    {
        return NULL;
    }

    MEMSET (pTmpNode, 0, sizeof (*pTmpNode));
    pTmpNode->u4RgIndex = u4RgIndex;
    pTmpNode->u1AddrType = u1AddrType;
    pTmpNode->Addr = Addr;

    pNodeRb = RBTreeGetNext (gL2vpnRgGlobals.RgNodeList,
                             (tRBElem *) pTmpNode, NULL);

    MemReleaseMemBlock (L2VPN_REDUNDANCY_NODE_POOL_ID, (VOID *) pTmpNode);
    return (pNodeRb);
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilCheckPwExistence
 *
 * DESCRIPTION      : This function checks if the pseudowire is part of another
 *                    redundant group or not 
 *
 * INPUT            : u4PwIndex - Pseudowire indexa
 *                    u4PwRedGrpIndex - Redundancy group index
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : L2VPN_FAILURE = if entry is found
 *                    L2VPN_SUCCESS = if entry is not found
 **************************************************************************/
UINT4
L2VpnUtilCheckPwExistence (UINT4 u4PwIndex, UINT4 u4PwRedGroupIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry *pNextRgPw = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pPwOwnVcEntry = NULL;

    /*check if this pseudowire is part of another group or not if so return failure */
    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);
    while (pRgPw != NULL)
    {
        if ((pRgPw->u4PwIndex == u4PwIndex) &&
            (pRgPw->u4RgIndex != u4PwRedGroupIndex))
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "This pseudowire %u is already associated with another group\r\n",
                        u4PwIndex);
            return L2VPN_FAILURE;
        }

        /* Check if this PW owner matches with the other pseudowire in this group */
        if (pRgPw->u4RgIndex == u4PwRedGroupIndex)
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pRgPw->u4PwIndex);
            pPwOwnVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
            if ((pPwVcEntry != NULL) && (pPwOwnVcEntry != NULL))
            {
                if (L2VPN_PWVC_OWNER (pPwVcEntry) !=
                    L2VPN_PWVC_OWNER (pPwOwnVcEntry))
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "The owner of this PW doesn't match with the other PW"
                               "in this group\r\n");
                    return L2VPN_FAILURE;
                }
            }
        }
        pNextRgPw = (tL2vpnRedundancyPwEntry *)
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw, NULL);
        pRgPw = pNextRgPw;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                "the pseudowire %u is not associated with any RG group\r\n",
                u4PwIndex);
    return L2VPN_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpUtilCheckAcGroupMatch
 *
 * DESCRIPTION      : This function is to check the AC group
 *
 * INPUT            : u4PwIndex - Pseudowire indexa
 *                    u4PwRedGrpIndex - Redundancy group index
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : L2VPN_FAILURE = if entry is found
 *                    L2VPN_SUCCESS = if entry is not found
 **************************************************************************/
UINT4
L2VpUtilCheckAcGroupMatch (UINT4 u4PwIndex, UINT4 u4PwRedGroupIndex)
{

    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry *pNextRgPw = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pPwOwnVcEntry = NULL;
    tPwVcEnetEntry     *pPwOwnVcEnetEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;

    /*check if this pseudowire is part of another group or not if so return failure */
    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);
    while (pRgPw != NULL)
    {

        /* Check if this PW owner matches with the other pseudowire in this group */
        if (pRgPw->u4RgIndex == u4PwRedGroupIndex)
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pRgPw->u4PwIndex);
            pPwOwnVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
            if ((pPwVcEntry != NULL) && (pPwOwnVcEntry != NULL))
            {

                TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                              ((tPwVcEnetServSpecEntry *)
                               L2VPN_PWVC_ENET_ENTRY (pPwOwnVcEntry)),
                              pPwOwnVcEnetEntry, tPwVcEnetEntry *)
                {

                    if (L2VPN_PWVC_ENET_ROWSTATUS (pPwOwnVcEnetEntry) !=
                        L2VPN_PWVC_ACTIVE)
                    {
                        continue;
                    }

                    TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                                  ((tPwVcEnetServSpecEntry *)
                                   L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                                  pPwVcEnetEntry, tPwVcEnetEntry *)
                    {
                        if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) !=
                            L2VPN_PWVC_ACTIVE)
                        {
                            continue;
                        }

                        if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                            (L2VPN_PWVC_ENET_PORT_VLAN (pPwOwnVcEnetEntry)))
                        {
                            return L2VPN_FAILURE;
                        }
                    }
                }
            }
        }
        pNextRgPw = (tL2vpnRedundancyPwEntry *)
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw, NULL);
        pRgPw = pNextRgPw;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                "the pseudowire %u is not associated with any RG group\r\n",
                u4PwIndex);
    return L2VPN_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilCheckIfPwIsAssoWithGrp
 *
 * DESCRIPTION      : This utility is used to check if the pseudowire is 
 *                    associated with any grp or not
 *
 * INPUT            : u4PwIndex - Pseudowire indexa
 *
 * OUTPUT           : pu4GrpIndex - PW redundancy group index
 *
 * RETURNS          : L2VPN_FAILURE = if entry is found
 *                    L2VPN_SUCCESS = if entry is not found
 **************************************************************************/
UINT4
L2VpnUtilCheckIfPwIsAssoWithGrp (UINT4 u4PwIndex, UINT4 *pu4GrpIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry *pNextRgPw = NULL;

    *pu4GrpIndex = 0;

    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);
    while (pRgPw != NULL)
    {
        if (pRgPw->u4PwIndex == u4PwIndex)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "This pseudowire %u is associated with RG group\r\n",
                        u4PwIndex);
            *pu4GrpIndex = pRgPw->u4RgIndex;
            return L2VPN_SUCCESS;
        }

        pNextRgPw = (tL2vpnRedundancyPwEntry *)
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw, NULL);
        pRgPw = pNextRgPw;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                "the pseudowire %u is not associated with any RG group\r\n",
                u4PwIndex);
    return L2VPN_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnPwRedSetVpnId
 *
 * DESCRIPTION      : This utility is used to change the VPLS entry
 *                    of the backup pseudowire to the primary pseudowire
 *
 * INPUT            : u4PwRedPwIndex - Pseudowire index
 *                    u4PwRedGroupIndex - Redundancy group Index
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : L2VPN_FAILURE = if entry is found
 *                    L2VPN_SUCCESS = if entry is not found
 **************************************************************************/
INT4
L2VpnPwRedSetVpnId (UINT4 u4PwRedGroupIndex, UINT4 u4PwRedPwIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry *pNextRgPw = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pPwOwnVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4PwInstance = 0;

    pPwOwnVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwRedPwIndex);

    if (pPwOwnVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);

    while (pRgPw != NULL)
    {
        if ((pRgPw->u4PwIndex != u4PwRedPwIndex) &&
            (pRgPw->u4RgIndex == u4PwRedGroupIndex) &&
            (pRgPw->u1RowStatus == ACTIVE))
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pRgPw->u4PwIndex);

            if (pPwVcEntry == NULL)
            {
                return L2VPN_FAILURE;
            }
            u4PwInstance = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4PwInstance);
            if (pVplsEntry == NULL)
            {
                continue;
            }
            L2VPN_PWVC_VPLS_INDEX (pPwOwnVcEntry) = u4PwInstance;
            L2vpnDeletePwVplsEntry (pPwOwnVcEntry);
            L2vpnAddPwVplsEntry (pPwOwnVcEntry);
            return L2VPN_SUCCESS;

        }
        pNextRgPw = (tL2vpnRedundancyPwEntry *)
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw, NULL);
        pRgPw = pNextRgPw;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                "the pseudowire %u is not associated with any RG group\r\n",
                u4PwRedPwIndex);
    return L2VPN_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilCheckRedPwPeerAdd
 *
 * DESCRIPTION      : This utility is used to check if the peer address of the
 *                    PW within an RG group are not smae
 *
 * INPUT            : u4PwIndex - Pseudowire indexa
 *                    u4PwRedGroupIndex - Redundancy group Index
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : L2VPN_FAILURE = if entry is found
 *                    L2VPN_SUCCESS = if entry is not found
 **************************************************************************/

UINT4
L2VpnUtilCheckRedPwPeerAdd (UINT4 u4PwRedPwIndex, UINT4 u4PwRedGroupIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry *pNextRgPw = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pPwOwnVcEntry = NULL;

    pPwOwnVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwRedPwIndex);
    if (pPwOwnVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }
    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);

    while (pRgPw != NULL)
    {
        if ((pRgPw->u4PwIndex != u4PwRedPwIndex) &&
            (pRgPw->u4RgIndex == u4PwRedGroupIndex))
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (pRgPw->u4PwIndex);

            if (pPwVcEntry == NULL)
            {
                return L2VPN_FAILURE;
            }
            if (pPwVcEntry->i4PeerAddrType != pPwOwnVcEntry->i4PeerAddrType)
            {
                return L2VPN_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
            {
                if (!(MEMCMP (&L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                              &L2VPN_PWVC_PEER_ADDR (pPwOwnVcEntry),
                              IPV6_ADDR_LENGTH)))
                {
                    return L2VPN_FAILURE;
                }
            }
            else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
            {
                if (!(MEMCMP (&L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                              &L2VPN_PWVC_PEER_ADDR (pPwOwnVcEntry),
                              IPV4_ADDR_LENGTH)))
                {
                    return L2VPN_FAILURE;
                }
            }
        }
        pNextRgPw = (tL2vpnRedundancyPwEntry *)
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw, NULL);
        pRgPw = pNextRgPw;
    }
    return L2VPN_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : L2VpnUtilGetMatchPwRGEntry
 *
 * DESCRIPTION      : This utility is used to check if the peer address of the
 *                    PW within an RG group are not smae
 *
 * INPUT            : u4PwIndex - Pseudowire VC Id
 *
 * OUTPUT           : u4PwRedGroupIndex - Redundancy group Index
 *
 * RETURNS          : L2VPN_FAILURE = if entry is found
 *                    L2VPN_SUCCESS = if entry is not found
 **************************************************************************/

UINT4
L2VpnUtilGetMatchPwRGEntry (UINT4 u4PwRedPwId, UINT4 *u4PwRedGroupIndex)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyPwEntry *pNextRgPw = NULL;
    pRgPw = RBTreeGetFirst (gL2vpnRgGlobals.RgPwList);

    while (pRgPw != NULL)
    {
        if (pRgPw->u4PwVcId == u4PwRedPwId)
        {
            *u4PwRedGroupIndex = pRgPw->u4RgIndex;
            return L2VPN_SUCCESS;
        }
        pNextRgPw = (tL2vpnRedundancyPwEntry *)
            RBTreeGetNext (gL2vpnRgGlobals.RgPwList, (tRBElem *) pRgPw, NULL);
        pRgPw = pNextRgPw;
    }
    return L2VPN_FAILURE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : L2VpUtilMakeObjIdFromDotNew                             */
/*                                                                            */
/*  Description     : Gives Oid from the input string.                        */
/*                                                                            */
/*  Input(s)        :  pi1TxtStr                                              */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  pOidPtr or NULL                                        */
/*                                                                            */
/******************************************************************************/
#define   L2VPN_SNMP_TEMP_BUFF   280
INT1                gai4TmpBuffer[L2VPN_SNMP_TEMP_BUFF];
tSNMP_OID_TYPE     *
L2VpUtilMakeObjIdFromDotNew (INT1 *pi1TxtStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL, *pi1DotPtr = NULL;
    UINT2               u2Cnt = L2VPN_ZERO;
    UINT2               u2DotCount = L2VPN_ZERO;
    UINT1              *pu1TmpPtr = NULL;
    UINT2               u2BufferLen = 0;

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TxtStr) != L2VPN_ZERO)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TxtStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TxtStr + STRLEN ((INT1 *) pi1TxtStr);
        }
        pi1TempPtr = pi1TxtStr;

        for (u2Cnt = L2VPN_ZERO;
             ((pi1TempPtr < pi1DotPtr) &&
              (u2Cnt < L2VPN_SNMP_TEMP_BUFF)); u2Cnt++)
        {
            gai4TmpBuffer[u2Cnt] = *pi1TempPtr++;
        }
        if (u2Cnt != L2VPN_SNMP_TEMP_BUFF)
            gai4TmpBuffer[u2Cnt] = '\0';
        else
        {
            L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                       "\rL2VpUtilMakeObjIdFromDotNew: Function Entry\n");
            return (NULL);
        }

        for (u2Cnt = L2VPN_ZERO;
             (u2Cnt < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))
              && (orig_mib_oid_table[u2Cnt].pName != NULL)); u2Cnt++)
        {
            if ((STRCMP (orig_mib_oid_table[u2Cnt].pName,
                         (INT1 *) gai4TmpBuffer) == L2VPN_ZERO)
                && (STRLEN ((INT1 *) gai4TmpBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Cnt].pName)))
            {
                STRNCPY ((INT1 *) gai4TmpBuffer,
                         orig_mib_oid_table[u2Cnt].pNumber,
                         MPLS_STRLEN_MIN (gai4TmpBuffer,
                                          orig_mib_oid_table[u2Cnt].pNumber));

                gai4TmpBuffer[MPLS_STRLEN_MIN (gai4TmpBuffer,
                                               orig_mib_oid_table[u2Cnt].
                                               pNumber)] = '\0';

                break;
            }
        }
        if ((u2Cnt < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
            && (orig_mib_oid_table[u2Cnt].pName == NULL))
        {
            return (NULL);
        }

        if (u2Cnt == (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = (UINT2) (sizeof (gai4TmpBuffer) - STRLEN (gai4TmpBuffer));
        STRNCAT ((INT1 *) gai4TmpBuffer, (INT1 *) pi1DotPtr,
                 (u2BufferLen <
                  STRLEN (pi1DotPtr) ? u2BufferLen : STRLEN (pi1DotPtr)));
    }
    else
    {
        /* is not alpha, so just copy into gai4TmpBuffer */
        STRNCPY ((INT1 *) gai4TmpBuffer, (INT1 *) pi1TxtStr,
                 MPLS_STRLEN_MIN (gai4TmpBuffer, pi1TxtStr));
        gai4TmpBuffer[MPLS_STRLEN_MIN (gai4TmpBuffer, pi1TxtStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = L2VPN_ZERO;
    for (u2Cnt = L2VPN_ZERO;
         ((u2Cnt < L2VPN_SNMP_TEMP_BUFF) && (gai4TmpBuffer[u2Cnt] != '\0'));
         u2Cnt++)
    {
        if (gai4TmpBuffer[u2Cnt] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid ((INT4) (u2DotCount + L2VPN_ONE))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) gai4TmpBuffer;
    for (u2Cnt = L2VPN_ZERO; u2Cnt < u2DotCount + L2VPN_ONE; u2Cnt++)
    {
        if ((pOidPtr->pu4_OidList[u2Cnt] =
             ((UINT4) (L2VpUtilParseSubIdNew (&pu1TmpPtr)))) ==
            (UINT4) L2VPN_INVALID)
        {
            free_oid (pOidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */
    pOidPtr->u4_Length = (UINT4) (u2DotCount + L2VPN_ONE);

    return (pOidPtr);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : L2VpUtilParseSubIdNew                                   */
/*                                                                            */
/*  Description     : returns the numeric value of ppu1TempPtr                */
/*                                                                            */
/*  Input(s)        :  **ppu1TempPtr                                          */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : value of ppu1TempPtr or L2VPN_INVALID                   */
/*                                                                            */
/******************************************************************************/
INT4
L2VpUtilParseSubIdNew (UINT1 **ppu1TempPtr)
{
    INT4                i4Value = L2VPN_ZERO;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        i4Value = (i4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4Value = L2VPN_INVALID;
    }
    *ppu1TempPtr = pu1Tmp;
    return (i4Value);
}

/*****************************************************************************/
/* Function Name : L2VpnUtilValidateHwStatus                                 */
/* Description   : This function Validates the H/W status of the Pseudowire  */
/*                 based on the Pseudowire Type (VPLS / VPWS) and the        */
/*                 PwOperstatus.                                             */
/* Input(s)      : pPwVcEntry -  pointer to PwVc Entry                       */
/*                 u1PwOperStatus - Operational status of the PwVc           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnUtilValidateHwStatus (tPwVcEntry * pPwVcEntry, UINT1 u1PwOperStatus)
{
    UINT1               u1CurHwStatus =
        (pPwVcEntry->u1HwStatus & 0xF0) >> MPLS_FOUR;
    UINT1               u1UpdateHwStatus = (pPwVcEntry->u1HwStatus & 0x0F);

    if (u1PwOperStatus == L2VPN_PWVC_UP)
    {
        /*if ((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS) &&
           (pPwVcEntry->u1HwStatus == L2VPN_TRUE))
           {
           return L2VPN_SUCCESS;
           } */
        if ((u1CurHwStatus == u1UpdateHwStatus))
        {
            return L2VPN_SUCCESS;
        }
    }
    else
    {

        if (((u1CurHwStatus & u1UpdateHwStatus) == L2VPN_ZERO))
        {

            return L2VPN_SUCCESS;
        }
    }

    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnUtilSetHwStatus                                      */
/* Description   : This function Sets H/W status for the Pseudowire          */
/* Input(s)      : pPwVcEntry -  pointer to PwVc Entry                       */
/*                 u1HwStatus -  hardware status of Pseudowire               */
/*                 u1Flag     -  If passed one sets the u1HwStatus           */
/*                               If passed two resets the u1HwStatus         */
/*                               If passed Zero assigns the u1HwStatus       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnUtilSetHwStatus (tPwVcEntry * pPwVcEntry, UINT1 u1HwStatus, UINT1 u1Flag)
{
    /*if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
       {
       return;
       } */

    if (L2VpnUtilIsRemoteStatusCapable (pPwVcEntry) == L2VPN_FALSE)
    {
        u1HwStatus = PW_HW_VPN_UPDATE;
    }
    if (u1Flag == L2VPN_ONE)
    {
        pPwVcEntry->u1HwStatus |= u1HwStatus;
    }
    else if (u1Flag == L2VPN_TWO)
    {
        pPwVcEntry->u1HwStatus &= (UINT1) (~(u1HwStatus));
    }
    else
    {
        pPwVcEntry->u1HwStatus = u1HwStatus;
    }

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnUtilFindHwStatus                                     */
/* Description   : This function Finds H/W status to passed to NPAPI         */
/* Input(s)      : pPwVcEntry -  pointer to PwVc Entry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : u1Flag   - H/W Status                                     */
/*****************************************************************************/
UINT1
L2VpnUtilFindHwStatus (tPwVcEntry * pPwVcEntry)
{
    UINT1               u1Flag = L2VPN_ZERO;

    /* For AddHw Case */
    if (pPwVcEntry->u1HwStatus & PW_HW_AC_UPDATE)
    {
        if (!(pPwVcEntry->u1HwStatus & PW_HW_AC_PRESENT))
        {
            u1Flag |= PW_HW_AC_UPDATE;    /* AC needs to be programmed */
        }
    }

    if (pPwVcEntry->u1HwStatus & PW_HW_PW_UPDATE)
    {
        if (!(pPwVcEntry->u1HwStatus & PW_HW_PW_PRESENT))
        {
            u1Flag |= PW_HW_PW_UPDATE;
        }
    }

    return u1Flag;
}

/*****************************************************************************/
/* Function Name : L2VpnUtilSetEnetHwStatus                                  */
/* Description   : This function sets the HW status of the Enet Entries      */
/*                 corresponding to the passed port / vlan combination       */
/* Input(s)      : i4PortIfIndex - Port If Index                             */
/*                 u2PortVlan    - Port Vlan                                 */
/*                 u1HwStatus    - Hw Status                                 */
/*                 u4PwAcId      - Unique ID for Attachment circuit          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnUtilSetEnetHwStatus (INT4 i4PortIfIndex, UINT2 u2PortVlan,
                          UINT1 u1HwStatus, UINT4 u4PwAcId)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT4               u4HashIndex = L2VPN_ZERO;

    TMO_HASH_Scan_Table (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                         u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (L2VPN_PWVC_INTERFACE_HASH_PTR (gpPwVcGlobalInfo),
                              u4HashIndex, pNode, tTMO_HASH_NODE *)
        {
            pPwVcEnetEntry = MPLS_PWVC_FROM_ENETLIST (pNode);

            if ((L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) !=
                 i4PortIfIndex) ||
                (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) != u2PortVlan))
            {
                continue;
            }

            pPwVcEnetEntry->u1HwStatus = u1HwStatus;
            if (MPLS_FALSE == pPwVcEnetEntry->u1HwStatus)
            {
                pPwVcEnetEntry->u4PwAcId = L2VPN_ZERO;
            }
            else
            {
                pPwVcEnetEntry->u4PwAcId = u4PwAcId;
            }

            L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                        "HW Status of AC of PW %d with Port Vlan %d "
                        "Port If Index %d set to %d\n",
                        pPwVcEnetEntry->pPwVcEntry->u4PwVcIndex,
                        L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry),
                        L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry), u1HwStatus);
        }
    }
}

/*****************************************************************************/
/* Function Name : L2VpnMatchPwVcEntryWithTai                                */
/* Description   : This function maps the TAI value with the psedowires      */
/*                 configured                                                */
/* Input(s)      : AGI, TAII                                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/

INT4
L2VpnMatchPwVcEntryWithTai (UINT1 *pu1Agi, UINT1 *pu1Taii)
{
    tPwVcEntry         *pPwVcEntry = NULL, *pPwVcEntryIn = NULL;
    BOOL1               bIsNextAgi = FALSE;

    pPwVcEntryIn = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntryIn == NULL)
    {
        return L2VPN_SUCCESS;
    }
    MEMSET (pPwVcEntryIn, 0, sizeof (tPwVcEntry));

    MEMCPY (pPwVcEntryIn->au1Agi, pu1Agi, L2VPN_PWVC_MAX_AI_LEN);

    pPwVcEntry = (tPwVcEntry *)
        RBTreeGetNext (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo),
                       pPwVcEntryIn, NULL);

    if (pPwVcEntry == NULL)
    {
        if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                                (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Mem release failure - PwVc memory removal\r\n");
        }
        return L2VPN_FAILURE;
    }

    while (pPwVcEntry != NULL)
    {
        if (MEMCMP (pPwVcEntry->au1Agi, pu1Agi, L2VPN_PWVC_MAX_AI_LEN) != 0)
        {
            bIsNextAgi = TRUE;
            break;
        }

        if (MEMCMP (pPwVcEntry->au1Saii, pu1Taii, L2VPN_PWVC_MAX_AI_LEN) == 0)
        {
            break;
        }

        pPwVcEntry = (tPwVcEntry *)
            RBTreeGetNext (L2VPN_PWVC_FEC129_RB_PTR (gpPwVcGlobalInfo),
                           pPwVcEntry, NULL);
    }

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure - PwVc memory removal\r\n");
    }

    if ((pPwVcEntry == NULL) || (bIsNextAgi == TRUE))
    {
        return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPassValidInLabelToHw                                 */
/* Description   : This function returns the valid label either from the     */
/*                 configured                                                */
/* Input(s)      : pPwVcEntry - pointer to tPwVcEntry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : u4InLabel - Valid label to be sent to hardware            */
/*****************************************************************************/
UINT4
L2VpnPassValidInLabelToHw (tPwVcEntry * pPwVcEntry)
{
    UINT4               u4InLabel = L2VPN_INVALID_LABEL;

    if (pPwVcEntry->u4InVcLabel != L2VPN_INVALID_LABEL)
    {
        u4InLabel = pPwVcEntry->u4InVcLabel;
    }
    else
    {
        u4InLabel = pPwVcEntry->u4PrevInVcLabel;
    }

    return u4InLabel;

}

/*****************************************************************************/
/* Function Name : L2VpnGetPwIndexFromVplsIndexForVpws                       */
/* Description   : This function is to get the Associated PWEntry with the   */
/*                 VPLS structure. This function is applicable only for VPWS */
/*                 pseudowires.                                              */
/* Input(s)      :                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_ZERO / VALID_PW_INDEX                               */
/*****************************************************************************/
UINT4
L2VpnGetPwIndexFromVplsIndexForVpws (UINT4 u4VplsInstance)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return L2VPN_ZERO;
    }

    pPwVplsNode = TMO_SLL_First (&pVplsEntry->PwList);

    if (pPwVplsNode == NULL)
    {
        return L2VPN_ZERO;
    }
    pPwVcEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

    return pPwVcEntry->u4PwVcIndex;
}

/****************************************************************************
 Function    :  L2VpnSetPwAdminStatus
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  L2VPN_SUCCESS or L2VPN_FAILURE
****************************************************************************/
INT1
L2VpnSetPwAdminStatus (UINT4 u4PwIndex, INT4 i4SetValPwAdminStatus)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tPwVcEntry         *pPwVcEntry = NULL;

    UINT4               u4RetStatus;
    UINT4               u4TimeTicks;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    switch (i4SetValPwAdminStatus)
    {
        case L2VPN_PWVC_ADMIN_UP:
            if (pPwVcEntry != NULL)
            {
                if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_UP)
                {
                    /* PW is already Admin UP */
                    return L2VPN_SUCCESS;
                }
                L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) = L2VPN_PWVC_ADMIN_UP;

                if (L2VPN_PWVC_CREATE_TIME (pPwVcEntry) == L2VPN_ZERO)
                {
                    /* Currently system ticks is being assigned           */
                    /* while porting this must be changed to gettimeofday */
                    OsixGetSysTime (&u4TimeTicks);
                    L2VPN_PWVC_CREATE_TIME (pPwVcEntry) = u4TimeTicks;
                }
                if (L2vpnUtilIsPsnTeTnlP2MP (pPwVcEntry) == TRUE)
                {
                    pPwVcEntry->u1TnlType = L2VPN_PW_PSN_TNL_TYPE_P2MP;
                }
                L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4EvtType
                    = L2VPN_PWVC_ADMIN_UP_EVENT;
                L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4PwVcIndex
                    = u4PwIndex;
                u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                      L2VPN_PWVC_ADMIN_EVENT);

                if (u4RetStatus == L2VPN_SUCCESS)
                {
                    return L2VPN_SUCCESS;
                }
            }
            break;
        case L2VPN_PWVC_ADMIN_DOWN:
            if (pPwVcEntry != NULL)
            {
                if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) ==
                    L2VPN_PWVC_ADMIN_DOWN)
                {
                    /* PW is already Admin Down */
                    return L2VPN_SUCCESS;
                }
                L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) = L2VPN_PWVC_ADMIN_DOWN;

                L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4EvtType
                    = L2VPN_PWVC_ADMIN_DOWN_EVENT;
                L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4PwVcIndex
                    = u4PwIndex;
                u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                      L2VPN_PWVC_ADMIN_EVENT);

                if (u4RetStatus == L2VPN_SUCCESS)
                {
                    return L2VPN_SUCCESS;
                }
            }
            break;
        case L2VPN_PWVC_ADMIN_TESTING:
            break;
        default:
            break;
    }

    return L2VPN_FAILURE;
}

#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name : L2vpnGetVplsPwBindEntry                                   */
/* Description   : This function is to get the Associated PWBindEntry with   */
/*                 the pwIndex and VplsIndex                                 */
/* Input(s)      : u4VplsConfigIndex - Vpls Instance                         */
/*                 u4PwIndex         - PwIndex                               */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to the tVplsPwBindEntry or NULL                   */
/*****************************************************************************/
tVplsPwBindEntry   *
L2vpnGetVplsPwBindEntry (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex)
{
    tVplsPwBindEntry    vplsPwBindEntry;
    tVplsPwBindEntry   *pVplsPwBindEntry = NULL;
    L2VPN_VPLS_PW_BIND_VPLS_INSTANCE (&vplsPwBindEntry) = u4VplsConfigIndex;
    L2VPN_VPLS_PW_BIND_PWINDEX (&vplsPwBindEntry) = u4PwIndex;
    pVplsPwBindEntry = (tVplsPwBindEntry *)
        RBTreeGet (L2VPN_VPLS_PW_BIND_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsPwBindEntry);

    if (pVplsPwBindEntry == NULL)
    {
        /*CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND); */
        return L2VPN_FAILURE;
    }
    return pVplsPwBindEntry;
}
#endif
/****************************************************************************
 Function    :  L2VpnSetPwRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  L2VPN_SUCCESS or L2VPN_FAILURE
****************************************************************************/
INT1
L2VpnSetPwRowStatus (UINT4 u4PwIndex, INT4 i4SetValPwRowStatus)
{
    UINT4               u4RetStatus;
    UINT4               u4VplsIndex;
    UINT4               u4VpnId = L2VPN_ZERO;
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcInfo          *pPwVcInfo = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tSNMP_OCTET_STRING_TYPE Agi;
    static UINT1        au1AgiVpn[L2VPN_PWVC_MAX_LEN];

    MEMSET (au1AgiVpn, L2VPN_ZERO, L2VPN_PWVC_MAX_LEN);

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    switch (i4SetValPwRowStatus)
    {
        case L2VPN_PWVC_CREATEANDWAIT:
            if (pPwVcEntry != NULL)
            {
                return L2VPN_FAILURE;
            }
            if (MplsL2VpnSetPwVcIndex (u4PwIndex) != u4PwIndex)
            {
                return L2VPN_FAILURE;
            }
            /* allocate memory */

            pPwVcEntry = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

            if (pPwVcEntry == NULL)
            {
                MplsL2VpnRelPwVcIndex (u4PwIndex);
                return L2VPN_FAILURE;
            }

            MEMSET (pPwVcEntry, L2VPN_ZERO, sizeof (tPwVcEntry));
            L2vpnInitPwVcEntry (u4PwIndex, pPwVcEntry);

            pPwVcInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
            pPwVcInfo->pPwVcEntry = pPwVcEntry;
            /* mark row status as not-ready */
            L2VPN_PWVC_ROW_STATUS (pPwVcEntry) = L2VPN_PWVC_NOTREADY;
            L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry) = CFA_ENET_MTU;
            L2VPN_PWVC_REMOTE_IFACE_MTU (pPwVcEntry) = CFA_ENET_MTU;
            L2VPN_PWVC_CNTRL_WORD (pPwVcEntry) = MPLS_SNMP_FALSE;
            L2VPN_PWVC_REMOTE_VE_ID (pPwVcEntry) = L2VPN_ZERO;
#ifdef VPLS_GR_WANTED
            L2VPN_PWVC_NPAPI (pPwVcEntry) = L2VPN_TRUE;
#endif
            L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_CREATE);

            if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS)
            {
                pPwVcMplsEntry =
                    (tPwVcMplsEntry *)
                    MemAllocMemBlk (L2VPN_MPLS_ENTRY_POOL_ID);

                if (pPwVcMplsEntry == NULL)
                {
                    MplsL2VpnRelVpnId (u4VpnId);
                    return L2VPN_FAILURE;
                }

                MEMSET (pPwVcMplsEntry, L2VPN_ZERO, sizeof (tPwVcMplsEntry));
                pPwVcEntry->pPSNEntry = pPwVcMplsEntry;
                L2vpnInitPwVcMplsEntry (pPwVcMplsEntry);
            }
            break;
        case L2VPN_PWVC_ACTIVE:

            if ((pPwVcEntry != NULL)
                && (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL)
                && ((pPwVcEntry->u4InVcLabel == L2VPN_INVALID_LABEL)
                    || (pPwVcEntry->u4OutVcLabel == L2VPN_INVALID_LABEL)))
            {
                return L2VPN_FAILURE;
            }

            /* CC, CV selection before making the RowStatus ACTIVE */
            if ((pPwVcEntry != NULL)
                && (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) &
                    L2VPN_PW_VCCV_CAPABLE))
            {
                pPwVcEntry->u1LocalCcSelected = L2VPN_ZERO;
                pPwVcEntry->u1LocalCvSelected = L2VPN_ZERO;
                if (pPwVcEntry->u1LocalCcAdvert == L2VPN_VCCV_CC_NONE)
                {
                    if ((pPwVcEntry->i1ControlWord == L2VPN_DISABLED) &&
                        (gL2VpnGlobalInfo.
                         u1LocalCcTypeCapabilities & L2VPN_VCCV_CC_ACH))
                    {
                        pPwVcEntry->u1LocalCcAdvert = (UINT1)
                            (gL2VpnGlobalInfo.u1LocalCcTypeCapabilities &
                             (~(L2VPN_VCCV_CC_ACH)));
                    }
                    else
                    {
                        pPwVcEntry->u1LocalCcAdvert =
                            gL2VpnGlobalInfo.u1LocalCcTypeCapabilities;
                    }
                }
                if (pPwVcEntry->u1LocalCvAdvert == L2VPN_VCCV_CV_NONE)
                {
                    pPwVcEntry->u1LocalCvAdvert =
                        gL2VpnGlobalInfo.u1LocalCvTypeCapabilities;
                }
                if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL)
                    || (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE))
                {
                    L2VpnVccvSelectMatchingCCTypes (pPwVcEntry);
                    L2VpnVccvSelectMatchingCVTypes (pPwVcEntry);
                }
            }

            if ((pPwVcEntry != NULL)
                && ((L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_NOTREADY)
                    || (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) ==
                        L2VPN_PWVC_NOTINSERVICE)))
            {
                /* Check if the PW Entry is already associated to a VPLS
                 * instance. If not create a new VPLS Entry and attach it 
                 * to the PW */
                if ((L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) == 0) &&
                    (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS))
                {
                    /* Get the free VPLS instance */
                    for (u4VplsIndex = 1;
                         u4VplsIndex <= gL2VpnGlobalInfo.u4MaxVplsEntries;
                         u4VplsIndex++)
                    {
                        pVplsEntry =
                            L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
                        if (pVplsEntry == NULL)
                        {
                            break;
                        }
                    }
                    if (u4VplsIndex > gL2VpnGlobalInfo.u4MaxVplsEntries)
                    {
                        return L2VPN_FAILURE;
                    }

                    if ((pVplsEntry = L2vpnCreateVplsEntry (u4VplsIndex))
                        == NULL)
                    {
                        return L2VPN_FAILURE;
                    }

                    L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = u4VplsIndex;

                    STRNCPY (L2VPN_VPLS_VPN_ID (pVplsEntry), MPLS_OUI_VPN_ID,
                             STRLEN (MPLS_OUI_VPN_ID));
                    (L2VPN_VPLS_VPN_ID (pVplsEntry))
                        [STRLEN (MPLS_OUI_VPN_ID)] = '\0';

                    u4VpnId = MplsL2VpnGetVpnId (void);
                    if (MplsL2VpnSetVpnId (u4VpnId) != u4VpnId)
                    {
                        return L2VPN_FAILURE;
                    }

                    u4VpnId = OSIX_HTONL (u4VpnId);

                    MEMCPY (&(L2VPN_VPLS_VPN_ID (pVplsEntry)
                              [STRLEN (MPLS_OUI_VPN_ID)]),
                            (UINT1 *) &u4VpnId, sizeof (UINT4));

                    L2VPN_VPLS_ROW_STATUS (pVplsEntry) = ACTIVE;

                    pVplsEntry->u1StorageType = L2VPN_STORAGE_VOLATILE;
                }
                else            /* VPLS */
                {
                    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
                        (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));
                    if (pVplsEntry == NULL)
                    {
                        return L2VPN_FAILURE;
                    }
                    /*if(L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG) */

                    if ((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS) &&
                        (L2VPN_PWVC_OWNER (pPwVcEntry) ==
                         L2VPN_PWVC_OWNER_GEN_FEC_SIG))
                    {
                        if (((*pPwVcEntry->au1Agi) == L2VPN_ZERO)
                            && ((*pPwVcEntry->au1Saii) == L2VPN_ZERO))
                        {

                            Agi.pu1_OctetList = au1AgiVpn;
                            Agi.i4_Length = L2VPN_PWVC_MAX_AGI_LEN;
                            MEMCPY (Agi.pu1_OctetList,
                                    &(pVplsEntry->
                                      au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)]),
                                    Agi.i4_Length);
                            MEMCPY (L2VPN_PWVC_AGI (pPwVcEntry),
                                    Agi.pu1_OctetList, L2VPN_PWVC_MAX_AGI_LEN);
                            pPwVcEntry->u1AgiLen = L2VPN_PWVC_MAX_AGI_LEN;

                        }

                    }
                }

                if (L2vpnAddPwVplsEntry (pPwVcEntry) == L2VPN_FAILURE)
                {
                    MplsL2VpnRelVpnId (u4VpnId);
                    return L2VPN_FAILURE;
                }
            }

            if ((pPwVcEntry != NULL)
                && (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_NOTREADY))
            {
                /* if status is not-ready */
#ifdef VPLSADS_WANTED

                if (pPwVcEntry->i1PwVcOwner == L2VPN_PWVC_OWNER_OTHER)
                {
                    if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) ==
                        L2VPN_PWVC_PSN_TYPE_MPLS)
                    {
                        pPwVcMplsEntry =
                            (tPwVcMplsEntry *)
                            MemAllocMemBlk (L2VPN_MPLS_ENTRY_POOL_ID);

                        if (pPwVcMplsEntry == NULL)
                        {
                            MplsL2VpnRelVpnId (u4VpnId);
                            return L2VPN_FAILURE;
                        }

                        MEMSET (pPwVcMplsEntry, L2VPN_ZERO,
                                sizeof (tPwVcMplsEntry));
                        pPwVcEntry->pPSNEntry = pPwVcMplsEntry;
                        L2vpnInitPwVcMplsEntry (pPwVcMplsEntry);
                    }
                }
#endif

                /*MS-PW: Memory allocation for Enet code has been moved to 
                 * nmhSetPwEnetRowStatus as memory for Enet is required only when AC
                 * attached to the PW otherwise need not be allocated*/

                L2VPN_PWVC_ROW_STATUS (pPwVcEntry) = L2VPN_PWVC_ACTIVE;
            }
            else if ((pPwVcEntry != NULL)
                     && (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) ==
                         L2VPN_PWVC_NOTINSERVICE))
            {
                /* Select the CC and CV types as there may be a 
                 * possibility of having the remote peer's CC and CV types */
                if (L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry) &
                    L2VPN_PW_VCCV_CAPABLE)
                {
                    L2VpnVccvSelectMatchingCCTypes (pPwVcEntry);
                    L2VpnVccvSelectMatchingCVTypes (pPwVcEntry);
                }
                /* if status is notinservice then mark it as active */
                L2VPN_PWVC_ROW_STATUS (pPwVcEntry) = L2VPN_PWVC_ACTIVE;
                if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_UP)
                {
                    L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4EvtType
                        = L2VPN_PWVC_ADMIN_UP_EVENT;
                    L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4PwVcIndex
                        = u4PwIndex;
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_ADMIN_EVENT);
                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return L2VPN_FAILURE;
                    }
                }
            }
            break;
        case L2VPN_PWVC_NOTINSERVICE:
            if ((pPwVcEntry != NULL) &&
                (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == ACTIVE))
            {
                /* For Both VPLS/VPWS, a VPLS Entry must have been there
                 * before making it as NOTINSERVICE.If not return failure */
                if (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) == 0)
                {
                    return L2VPN_FAILURE;
                }
                if (((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS) ||
                     (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)) &&
                    (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) != 0))
                {

                    if (L2vpnDeletePwVplsEntry (pPwVcEntry) == L2VPN_FAILURE)
                    {
                        return L2VPN_FAILURE;
                    }
                }

                /* mark row status as notinservice */
                L2VPN_PWVC_ROW_STATUS (pPwVcEntry) = L2VPN_PWVC_NOTINSERVICE;

                if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_UP)
                {
                    L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4EvtType
                        = L2VPN_PWVC_NIS_EVENT;
                    L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4PwVcIndex
                        = u4PwIndex;
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_ADMIN_EVENT);
                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return L2VPN_FAILURE;
                    }
                }
            }
            break;
        case L2VPN_PWVC_DESTROY:
            if (pPwVcEntry == NULL)
            {
                return L2VPN_FAILURE;
            }
            /* Release the STATIC Label to the Label Manager */
            if (((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_MANUAL) ||
                 (L2VPN_IS_STATIC_PW (pPwVcEntry) == TRUE))
#ifdef VPLSADS_WANTED
                && (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER)
#endif
                )
            {
                if (L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry) !=
                    L2VPN_LDP_INVALID_LABEL)
                {
                    if (MplsReleaseLblToLblGroup (L2VPN_PWVC_INBOUND_VC_LABEL
                                                  (pPwVcEntry)) != MPLS_SUCCESS)
                    {
                        return L2VPN_FAILURE;
                    }
                }
            }

            L2VPN_PWVC_ROW_STATUS (pPwVcEntry) = L2VPN_PWVC_DESTROY;

            L2VpnDeletePwVc (pPwVcEntry);
            MplsL2VpnRelPwVcIndex (u4PwIndex);

            L2VpnUpdateGlobalStats (pPwVcEntry, L2VPN_PWVC_DESTROY);
            break;
        default:
            return L2VPN_FAILURE;
    }
    return L2VPN_SUCCESS;
}

/****************************************************************************
 Function    :  L2VpnSetPwEnetRowStatus
 Input       :  The Indices
                PwIndex
                PwEnetPwInstance

                The Object 
                setValPwEnetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  L2VPN_SUCCESS or L2VPN_FAILURE
****************************************************************************/
INT1
L2VpnSetPwEnetRowStatus (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
                         INT4 i4SetValPwEnetRowStatus)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEnetServSpecEntry *pServSpecEntry = NULL;    /*MS-PW */
    UINT4               u4RetStatus;
    UINT4               u4HashIndex;
    tVplsInfo           VplsInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT1               u1Found = L2VPN_FALSE;
    UINT1               u1IsAcPresent = L2VPN_FALSE;
    pPwVcEntry = L2VpnGetWorkingPwEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    /*MS-PW :Allocate memory only once for SS PW VC Enet */
    if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL)
    {
        if ((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
            || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            pServSpecEntry = (tPwVcEnetServSpecEntry *)
                MemAllocMemBlk (L2VPN_SERV_SPEC_POOL_ID);
            if (pServSpecEntry == NULL)
            {
                return L2VPN_FAILURE;
            }

            MEMSET (pServSpecEntry, L2VPN_ZERO,
                    sizeof (tPwVcEnetServSpecEntry));
            pPwVcEntry->pServSpecEntry = pServSpecEntry;

            TMO_SLL_Init (L2VPN_PWVC_ENET_ENTRY_LIST (pServSpecEntry));
        }
    }
    /*MS-PW */

    if (((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
         || (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        && (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL))
    {
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                                  L2VPN_PWVC_ENET_ENTRY
                                                  (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry) ==
                u4PwEnetPwInstance)
            {
                u1Found = L2VPN_TRUE;
                break;
            }
        }
    }
    else
    {
        return L2VPN_FAILURE;
    }

    switch (i4SetValPwEnetRowStatus)
    {
        case L2VPN_PWVC_ACTIVE:
            if (u1Found != L2VPN_TRUE)
            {
                return L2VPN_FAILURE;
            }
            if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
            {
                return L2VPN_SUCCESS;
            }
            if (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                L2VPN_VLANMODE_NOCHANGE)
            {
                if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                    L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry))
                {
                    return L2VPN_FAILURE;
                }
            }
            pVplsEntry =
                L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->u4VplsInstance);
            if (pVplsEntry == NULL)
            {
                return L2VPN_FAILURE;
            }
            if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) != L2VPN_ZERO &&
                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                L2VPN_PWVC_ENET_DEF_PORT_VLAN)
            {
                pPwVcEnetEntry->u1EnetMode = L2VPN_CLI_VLAN_MODE;
            }
            else
            {
                pPwVcEnetEntry->u1EnetMode = L2VPN_CLI_ETH_MODE;
            }

            if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) != L2VPN_ZERO) &&
                (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) !=
                 L2VPN_PWVC_ENET_DEF_PORT_VLAN))

            {
                pPwVcEnetEntry->u1EnetMode = L2VPN_CLI_VLAN_MODE;
            }
            else
            {
                pPwVcEnetEntry->u1EnetMode = L2VPN_CLI_ETH_MODE;
            }
            if ((PwEntryACExistsCheck (L2VPN_VPLS_VSI (pVplsEntry),
                                       u4PwIndex,
                                       L2VPN_PWVC_ENET_PORT_VLAN
                                       (pPwVcEnetEntry),
                                       L2VPN_PWVC_ENET_PORT_IF_INDEX
                                       (pPwVcEnetEntry),
                                       FALSE) == L2VPN_SUCCESS))
            {
                return L2VPN_FAILURE;
            }
            if ((L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                 L2VPN_PWVC_NOTINSERVICE) ||
                (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                 L2VPN_PWVC_NOTREADY))
            {
                L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) = L2VPN_PWVC_ACTIVE;

                /* Back Pointer for Interface UP/DOWN Search */
                pPwVcEnetEntry->pPwVcEntry = pPwVcEntry;
                u4HashIndex =
                    (UINT4) L2VPN_COMPUTE_HASH_INDEX (pPwVcEnetEntry->
                                                      i4PortIfIndex);
                TMO_HASH_Add_Node (L2VPN_PWVC_INTERFACE_HASH_PTR
                                   (gpPwVcGlobalInfo),
                                   &(pPwVcEnetEntry->EnetNode), u4HashIndex,
                                   (UINT1 *) (pPwVcEnetEntry));
                L2VPN_ADMIN_EVT_PWVC_SSTYPE = L2VPN_PWVC_SERV_ETH;
                L2VPN_ADMIN_EVT_SSEVT_TYPE = L2VPN_PWVC_ENET_ACTIVE;
                L2VPN_ADMIN_EVT_SSENET_VCINDEX = u4PwIndex;
                L2VPN_ADMIN_EVT_SSENET_VLANID = (INT2) u4PwEnetPwInstance;

                if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_PWVC_ADMIN_UP)
                {
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_SERV_ADMIN_EVENT);

                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return L2VPN_FAILURE;
                    }
                }

                if (pPwVcEntry->u4VplsInstance != 0)
                {
                    pVplsEntry =
                        L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->
                                                            u4VplsInstance);
                    if (pVplsEntry == NULL)
                    {
                        return L2VPN_FAILURE;
                    }
                    /* Register VplsInfo with vlan module when the first PW
                     * in this instance is made up. This info will be made
                     * use by vlan to give appropriate packets to mpls */
                    VplsInfo.u4ContextId =
                        (UINT4) (L2VPN_VPLS_VSI (pVplsEntry));
                    VplsInfo.u4VplsIndex = L2VPN_VPLS_INDEX (pVplsEntry);
                    VplsInfo.u4IfIndex =
                        (UINT4) (L2VPN_PWVC_ENET_PORT_IF_INDEX
                                 (pPwVcEnetEntry));
                    VplsInfo.VlanId =
                        L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                    /* Service type of PW is set as port based when 
                     * Ethernet Port is used as AC (Vlan Id value here will be 
                     * 4097. If Vlan Id value is not 4097, service type is set
                     * as Vlan based. */
                    if (VplsInfo.VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                    {
                        VplsInfo.u2MplsServiceType = VLAN_L2VPN_PORT_BASED;
                    }
                    else
                    {
                        VplsInfo.u2MplsServiceType = VLAN_L2VPN_VLAN_BASED;
                    }
                    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
                    {
                        VplsInfo.u1PwVcMode = VLAN_L2VPN_VPWS;
                    }
                    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                    {
                        VplsInfo.u1PwVcMode = VLAN_L2VPN_VPLS;
                    }

                    if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER)
                    {
                        if (VlanL2VpnAddL2VpnInfo (&VplsInfo) == VLAN_FAILURE)
                        {
                            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                       "-E- Unable to add VPLS info - "
                                       "S/W forwading can not be done\n");
                        }
                    }
                    if ((L2VPN_PWVC_OPER_STATUS (pPwVcEntry) ==
                         L2VPN_PWVC_OPER_UP)
                        && (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS))
                    {
                        if (TMO_SLL_Count (&pVplsEntry->PwList) > L2VPN_ONE)
                        {
                            /* Search for this Enet's existance in the other
                             * PWs of VPLS instance,
                             * if exists ignore the hw programming */

                            if (PwEntryHwACExistsCheck
                                (pVplsEntry,
                                 L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry),
                                 L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry))
                                == L2VPN_SUCCESS)
                            {
                                /* Already H/w Programming is Done for the
                                 * AC 
                                 */
                                pPwVcEnetEntry->u1HwStatus = MPLS_TRUE;
                                return L2VPN_SUCCESS;
                            }
                        }
                        L2VpnPwVcVplsHwVpnAcAdd (pPwVcEntry, pPwVcEnetEntry);
                    }
                }
                return L2VPN_SUCCESS;
            }
            break;

        case L2VPN_PWVC_CREATEANDWAIT:
            if (u1Found == L2VPN_FALSE)
            {
                pPwVcEnetEntry = (tPwVcEnetEntry *)
                    MemAllocMemBlk (L2VPN_ENET_POOL_ID);

                if (pPwVcEnetEntry == NULL)
                {
                    return L2VPN_FAILURE;
                }

                MEMSET (pPwVcEnetEntry, L2VPN_ZERO, sizeof (tPwVcEnetEntry));
                L2VPN_PWVC_ENET_PW_INSTANCE (pPwVcEnetEntry)
                    = u4PwEnetPwInstance;
                L2vpnInitPwVcEnetEntry (pPwVcEnetEntry);
                L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry)
                    = L2VPN_PWVC_NOTREADY;
                /* now add this node on to the SLL */
                TMO_SLL_Add (L2VPN_PWVC_ENET_ENTRY_LIST
                             ((tPwVcEnetServSpecEntry *)
                              L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                             (tTMO_SLL_NODE *)
                             & (pPwVcEnetEntry->NextEnetEntry));

                pPwVcEntry->u2NextFreeEnetInstance =
                    (UINT2) (pPwVcEntry->u2NextFreeEnetInstance + L2VPN_ONE);
                return L2VPN_SUCCESS;
            }
            break;

        case L2VPN_PWVC_NOTINSERVICE:
            if (u1Found == L2VPN_TRUE)
            {
                if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                    L2VPN_PWVC_ACTIVE)
                {
                    L2VPN_ADMIN_EVT_PWVC_SSTYPE = L2VPN_PWVC_SERV_ETH;
                    L2VPN_ADMIN_EVT_SSEVT_TYPE = L2VPN_PWVC_ENET_NIS;
                    L2VPN_ADMIN_EVT_SSENET_VCINDEX = u4PwIndex;
                    L2VPN_ADMIN_EVT_SSENET_VLANID = (INT2) u4PwEnetPwInstance;
                    u4RetStatus = L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                          L2VPN_PWVC_SERV_ADMIN_EVENT);
                    if (u4RetStatus == L2VPN_FAILURE)
                    {
                        return L2VPN_FAILURE;
                    }

                    u4HashIndex =
                        L2VPN_COMPUTE_HASH_INDEX ((UINT4) (pPwVcEnetEntry->
                                                           i4PortIfIndex));
                    TMO_HASH_Delete_Node (L2VPN_PWVC_INTERFACE_HASH_PTR
                                          (gpPwVcGlobalInfo),
                                          &(pPwVcEnetEntry->EnetNode),
                                          u4HashIndex);
                    L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) =
                        L2VPN_PWVC_NOTINSERVICE;
                    return L2VPN_SUCCESS;
                }
                if ((L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                     L2VPN_PWVC_NOTINSERVICE) ||
                    (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) ==
                     L2VPN_PWVC_NOTREADY))
                {
                    u4HashIndex =
                        L2VPN_COMPUTE_HASH_INDEX ((UINT4) (pPwVcEnetEntry->
                                                           i4PortIfIndex));
                    TMO_HASH_Delete_Node (L2VPN_PWVC_INTERFACE_HASH_PTR
                                          (gpPwVcGlobalInfo),
                                          &(pPwVcEnetEntry->EnetNode),
                                          u4HashIndex);
                    L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) =
                        L2VPN_PWVC_NOTINSERVICE;
                    return L2VPN_SUCCESS;
                }
            }
            break;

        case L2VPN_PWVC_DESTROY:
            if (u1Found != L2VPN_TRUE)
            {
                break;
            }

            if (pPwVcEntry->u4VplsInstance != 0)
            {
                pVplsEntry =
                    L2VpnGetVplsEntryFromInstanceIndex (pPwVcEntry->
                                                        u4VplsInstance);
                if (pVplsEntry == NULL)
                {
                    return L2VPN_FAILURE;
                }
                /* Register VplsInfo with vlan module when the first PW
                 * in this instance is made up. This info will be made
                 * use by vlan to give appropriate packets to mpls */
                VplsInfo.u4ContextId = (UINT4) (L2VPN_VPLS_VSI (pVplsEntry));
                VplsInfo.u4VplsIndex = L2VPN_VPLS_INDEX (pVplsEntry);
                VplsInfo.u4IfIndex =
                    (UINT4) (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry));
                VplsInfo.VlanId = L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);

                /* Service type of PW is set as port based when 
                 * Ethernet Port is used as AC (Vlan Id value here will be 
                 * 4097. If Vlan Id value is not 4097, service type is set
                 * as Vlan based. */
                if (VplsInfo.VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                {
                    VplsInfo.u2MplsServiceType = VLAN_L2VPN_PORT_BASED;
                }
                else
                {
                    VplsInfo.u2MplsServiceType = VLAN_L2VPN_VLAN_BASED;
                }
                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
                {
                    VplsInfo.u1PwVcMode = VLAN_L2VPN_VPWS;
                }
                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                {
                    if (VplsInfo.VlanId != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                    {
                        L2VpnIsAcPresent (L2VPN_PWVC_INDEX (pPwVcEntry),
                                          L2VPN_PWVC_ENET_PORT_VLAN
                                          (pPwVcEnetEntry), &u1IsAcPresent);
                    }
                    VplsInfo.u1PwVcMode = VLAN_L2VPN_VPLS;
                }

                if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER)
                {
                    if (u1IsAcPresent != L2VPN_TRUE)
                    {
                        if (VlanL2VpnDelL2VpnInfo (&VplsInfo) == VLAN_FAILURE)
                        {
                            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                       "-E- Can not de-register the VPLS info from vlan\n");
                        }
                    }
                }
            }

            if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) == L2VPN_PWVC_ACTIVE)
            {
                L2VPN_ADMIN_EVT_PWVC_SSTYPE = L2VPN_PWVC_SERV_ETH;
                L2VPN_ADMIN_EVT_SSEVT_TYPE = L2VPN_PWVC_ENET_DESTROY;
                L2VPN_ADMIN_EVT_SSENET_VCINDEX = u4PwIndex;
                L2VPN_ADMIN_EVT_SSENET_VLANID = (INT2) u4PwEnetPwInstance;
                u4RetStatus = L2VpnAdminEventHandler
                    (&L2VpnAdminEvtInfo, L2VPN_PWVC_SERV_ADMIN_EVENT);
                if (u4RetStatus == L2VPN_FAILURE)
                {
                    return L2VPN_FAILURE;
                }

                return L2VPN_SUCCESS;
            }
            TMO_SLL_Delete (L2VPN_PWVC_ENET_ENTRY_LIST
                            ((tPwVcEnetServSpecEntry *)
                             L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                            (tTMO_SLL_NODE *)
                            & (pPwVcEnetEntry->NextEnetEntry));
            u4HashIndex =
                L2VPN_COMPUTE_HASH_INDEX ((UINT4)
                                          (pPwVcEnetEntry->i4PortIfIndex));
            TMO_HASH_Delete_Node (L2VPN_PWVC_INTERFACE_HASH_PTR
                                  (gpPwVcGlobalInfo),
                                  &(pPwVcEnetEntry->EnetNode), u4HashIndex);
            pPwVcEnetEntry->pPwVcEntry = NULL;

            if (MemReleaseMemBlock
                (L2VPN_ENET_POOL_ID, (UINT1 *) (pPwVcEnetEntry)) != MEM_FAILURE)
            {
                return L2VPN_SUCCESS;
            }
            break;
        default:
            break;
    }
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnIsAcPresent                                   */
/* Description   : This function checks if more than one AC is present in vlan*/
/* Input(s)      : u4PwIndex - PW to look into for AC                        */
/*                 u4VlanId  - with which AC to be checked                         */
/* Output(s)     : u1IsAcPresent - Set to TRUE if more than 1 AC is present  */
/*                  in givan vlan                                            */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
VOID
L2VpnIsAcPresent (UINT4 u4PwIndex, UINT2 u2VlanId, UINT1 *u1IsAcPresent)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    UINT2               u2Count = 0;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if ((pPwVcEntry == NULL) || (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL))
    {
        return;
    }
    TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
                                              L2VPN_PWVC_ENET_ENTRY
                                              (pPwVcEntry)), pPwVcEnetEntry,
                  tPwVcEnetEntry *)
    {
        if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) == u2VlanId)
        {
            u2Count++;
            if (u2Count > L2VPN_ONE)
            {
                *u1IsAcPresent = L2VPN_TRUE;
                return;
            }
        }
    }
}

#ifdef VPLSADS_WANTED
/*****************************************************************************/
/* Function Name : L2VpnGetVplsIndexFromRd                                   */
/* Description   : This function returns VplsIndex for a particular RD       */
/* Input(s)      : pu1RouteDistinguisher - RD value                          */
/* Output(s)     : pu4VplsIndex - VPLS Instance associated with given RD     */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetVplsIndexFromRd (UINT1 *pu1RouteDistinguisher, UINT4 *pu4VplsIndex)
{
    tVPLSRdEntry       *pVPLSRdEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT2               u2VpnId = L2VPN_ZERO;
    UINT1               au1VplsVpnID[L2VPN_MAX_VPLS_VPNID_LEN + 1];
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (au1VplsVpnID, L2VPN_ZERO, L2VPN_MAX_VPLS_VPNID_LEN + 1);

    pVPLSRdEntry = (tVPLSRdEntry *)
        RBTreeGetFirst (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo));

    if (pVPLSRdEntry != NULL)
    {
        do
        {
            if (ACTIVE != L2VPN_VPLSRD_ROW_STATUS (pVPLSRdEntry))
            {
                continue;
            }
            if (MEMCMP (L2VPN_VPLSRD_ROUTE_DISTINGUISHER (pVPLSRdEntry),
                        pu1RouteDistinguisher, L2VPN_MAX_VPLS_RD_LEN) == 0)
            {

                *pu4VplsIndex = L2VPN_VPLSRD_VPLS_INSTANCE (pVPLSRdEntry);
                L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : VplsIndex = %d\n",
                            __func__, *pu4VplsIndex);
                return L2VPN_SUCCESS;
            }
        }
        while ((pVPLSRdEntry =
                (tVPLSRdEntry *)
                RBTreeGetNext (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo),
                               pVPLSRdEntry, NULL)) != NULL);
    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : pVPLSRdEntry is NULL\n",
                    __func__);
    }

    if (L2VPN_VPLS_RD_TYPE_0 == pu1RouteDistinguisher[0])
    {
        MEMCPY (&u4VpnId, &pu1RouteDistinguisher[4], sizeof (UINT4));
        u4VpnId = OSIX_NTOHL (u4VpnId);
    }
    else if (L2VPN_VPLS_RD_TYPE_1 == pu1RouteDistinguisher[0])
    {
        MEMCPY (&u2VpnId, &pu1RouteDistinguisher[6], sizeof (UINT2));
        u2VpnId = OSIX_NTOHS (u2VpnId);
        u4VpnId = u2VpnId;
    }
    else if (L2VPN_VPLS_RD_TYPE_2 == pu1RouteDistinguisher[0])
    {
        MEMCPY (&u2VpnId, &pu1RouteDistinguisher[6], sizeof (UINT2));
        u2VpnId = OSIX_NTOHS (u2VpnId);
        u4VpnId = u2VpnId;
    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : Invalid RD Type\n", __func__);
        return L2VPN_FAILURE;
    }

    /* VPN value is a 7 byte feild. First 3 bytes contains OUI 
     * and last 4 bytes contains integer vpn id value */
    STRNCPY (au1VplsVpnID, MPLS_OUI_VPN_ID, sizeof (MPLS_OUI_VPN_ID));
    u4VpnId = OSIX_NTOHL (u4VpnId);
    MEMCPY (&au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)], &u4VpnId, sizeof (UINT4));

    pVplsEntry = (tVPLSEntry *)
        RBTreeGetFirst (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s :VPLS Entry not found\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    do
    {
        if (MEMCMP (L2VPN_VPLS_VPN_ID (pVplsEntry),
                    au1VplsVpnID, L2VPN_MAX_VPLS_VPNID_LEN) == 0)
        {
            *pu4VplsIndex = L2VPN_VPLS_INDEX (pVplsEntry);
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : VplsIndex = %d\n",
                        __func__, *pu4VplsIndex);
            return L2VPN_SUCCESS;
        }
    }
    while ((pVplsEntry = (tVPLSEntry *)
            RBTreeGetNext (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo),
                           pVplsEntry, NULL)) != NULL);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnGetPwIndexFromRemoteVPLSVe                           */
/* Description   : This function returns VplsIndex for a particular RD       */
/* Input(s)      : pu1RouteDistinguisher - RD value                          */
/* Output(s)     : pu4VplsIndex - VPLS Instance associated with given RD     */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetPwIndexFromRemoteVPLSVe (UINT4 u4VplsIndex,
                                 UINT4 u4RemoteVeId, UINT4 *pu4PwIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                    "%s : Vpls Entry is not found\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
        if (pPwEntry != NULL)
        {
            if (L2VPN_PWVC_REMOTE_VE_ID (pPwEntry) == u4RemoteVeId)
            {
                *pu4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);
                L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : \
                            PwIndex = %d\n", __func__, *pu4PwIndex);
                return L2VPN_SUCCESS;
            }
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnGetLocalVeIdFromVPLSIndex                            */
/* Description   : This function returns VplsIndex for a particular RD       */
/* Input(s)      : pu1RouteDistinguisher - RD value                          */
/* Output(s)     : pu4VplsIndex - VPLS Instance associated with given RD     */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetLocalVeIdFromVPLSIndex (UINT4 u4VplsIndex, UINT4 *pu4VeId)
{
    tVPLSVeEntry        localVeEntry;
    tVPLSVeEntry       *pVPLSNextVeEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&localVeEntry, 0, sizeof (tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE (&localVeEntry) = u4VplsIndex;

    pVPLSNextVeEntry = (tVPLSVeEntry *)
        RBTreeGetNext (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                       &localVeEntry, NULL);
    if (NULL != pVPLSNextVeEntry)
    {
        if (u4VplsIndex == L2VPN_VPLSVE_VPLS_INSTANCE (pVPLSNextVeEntry) &&
            ACTIVE == L2VPN_VPLSVE_ROW_STATUS (pVPLSNextVeEntry))
        {
            *pu4VeId = L2VPN_VPLSVE_VE_ID (pVPLSNextVeEntry);
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : \
                            VeId = %d\n", __func__, *pu4VeId);
            return L2VPN_SUCCESS;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnAllocateLb                                           */
/* Description   : This function allocates 10 labels and return fisrt label  */
/* Input(s)      : NONE                                                      */
/* Output(s)     : pu4LabelBase - fisrt label value                          */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnAllocateLb (UINT4 *pu4LabelBase)
{
    UINT4               u4Key1 = L2VPN_ZERO;
    UINT4               u4Key2 = L2VPN_ZERO;
    UINT4               u4Index = L2VPN_ZERO;
    INT4                i4RetVal = L2VPN_SUCCESS;

    *pu4LabelBase = L2VPN_ZERO;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    for (u4Index = L2VPN_ZERO; u4Index < L2VPN_VPLS_VBS_DEFAULT_VALUE;
         u4Index++)
    {
        if (LBL_FAILURE == lblMgrGetLblFromLblGroup (gu2BgpVplsLblGroupId,
                                                     &u4Key1, &u4Key2))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "free label block is not available\r\n");
            i4RetVal = L2VPN_FAILURE;
            break;
        }

        if (L2VPN_ZERO == *pu4LabelBase)
        {
            *pu4LabelBase = u4Key2;
        }
    }

    if (L2VPN_FAILURE == i4RetVal)
    {
        while (u4Index > L2VPN_ZERO)
        {
            u4Index--;
            (VOID) LblMgrRelLblToLblGroup (gu2BgpVplsLblGroupId, 0,
                                           (*pu4LabelBase) + u4Index);
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : L2VpnReleaseLb                                            */
/* Description   : This function  release 10 labels atarting from u4LabelBase*/
/* Input(s)      : u4LabelBase - first label of label block                  */
/* Output(s)     : NONE                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnReleaseLb (UINT4 u4LabelBase)
{
    UINT4               u4Index = L2VPN_ZERO;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    for (u4Index = L2VPN_ZERO; u4Index < L2VPN_VPLS_VBS_DEFAULT_VALUE;
         u4Index++)
    {
        if (LBL_FAILURE == LblMgrRelLblToLblGroup (gu2BgpVplsLblGroupId, 0,
                                                   u4LabelBase + u4Index))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "failed to free label block\r\n");
            return L2VPN_FAILURE;
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnGetExportRtValue                                     */
/* Description   : This function returns RT value configured for a given VPLS*/
/* Input(s)      : pu1RouteDistinguisher - RD value                          */
/* Output(s)     : pu4VplsIndex - VPLS Instance associated with given RD     */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetExportRtValue (UINT4 u4VplsIndex, UINT1 u1IsDefaultRt,
                       UINT1 *pu1RouteTarget)
{
    tVPLSRtEntry        VPLSRtEntry;
    tVPLSRtEntry       *pVPLSNextRtEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&VPLSRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));

    L2VPN_VPLSRT_VPLS_INSTANCE (&VPLSRtEntry) = u4VplsIndex;

    pVPLSNextRtEntry = (tVPLSRtEntry *)
        RBTreeGetNext (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                       &VPLSRtEntry, NULL);
    while (NULL != pVPLSNextRtEntry)
    {
        if (L2VPN_VPLSRT_VPLS_INSTANCE (pVPLSNextRtEntry) == u4VplsIndex)
        {
            if ((L2VPN_VPLS_RT_EXPORT ==
                 L2VPN_VPLSRT_RT_TYPE (pVPLSNextRtEntry) ||
                 L2VPN_VPLS_RT_BOTH ==
                 L2VPN_VPLSRT_RT_TYPE (pVPLSNextRtEntry))
                && ACTIVE == L2VPN_VPLSRT_ROW_STATUS (pVPLSNextRtEntry))
            {
                MEMCPY (pu1RouteTarget,
                        L2VPN_VPLSRT_ROUTE_TARGET (pVPLSNextRtEntry),
                        L2VPN_MAX_VPLS_RT_LEN);
                L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : \
                            L2VPN_VPLSRT_ROUTE_TARGET = %s\n", __func__, pu1RouteTarget);
                return L2VPN_SUCCESS;
            }
            else if (L2VPN_VPLS_RT_IMPORT ==
                     L2VPN_VPLSRT_RT_TYPE (pVPLSNextRtEntry)
                     && ACTIVE == L2VPN_VPLSRT_ROW_STATUS (pVPLSNextRtEntry))
            {
                u1IsDefaultRt = L2VPN_FALSE;
            }
        }
        else
        {
            break;
        }
        pVPLSNextRtEntry = (tVPLSRtEntry *)
            RBTreeGetNext (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                           pVPLSNextRtEntry, NULL);
    }

    if (L2VPN_TRUE == u1IsDefaultRt)
    {
        if (L2VPN_SUCCESS == L2VpnGetDefaultRtValue (u4VplsIndex,
                                                     pu1RouteTarget))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : pu1RouteTarget = %s\n",
                        __func__, pu1RouteTarget);
            return L2VPN_SUCCESS;
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnGetRdValue                                           */
/* Description   : This function returns RD value configured for a given VPLS*/
/* Input(s)      : u4VplsIndex - VPLS Instance                               */
/* Output(s)     : pu1RouteDistinguisher - RD value for given vpls           */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetRdValue (UINT4 u4VplsIndex, UINT1 *pu1RouteDistinguisher)
{
    tVPLSRdEntry        VPLSRdEntry;
    tVPLSRdEntry       *pVPLSNextRdEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&VPLSRdEntry, L2VPN_ZERO, sizeof (tVPLSRdEntry));

    L2VPN_VPLSRD_VPLS_INSTANCE (&VPLSRdEntry) = u4VplsIndex;

    pVPLSNextRdEntry = (tVPLSRdEntry *)
        RBTreeGet (L2VPN_VPLS_RD_RB_PTR (gpPwVcGlobalInfo), &VPLSRdEntry);
    if (NULL != pVPLSNextRdEntry)
    {
        if (ACTIVE == L2VPN_VPLSRD_ROW_STATUS (pVPLSNextRdEntry))
        {
            MEMCPY (pu1RouteDistinguisher,
                    L2VPN_VPLSRD_ROUTE_DISTINGUISHER (pVPLSNextRdEntry),
                    L2VPN_MAX_VPLS_RD_LEN);
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : \
                            L2VPN_VPLSRD_ROUTE_DISTINGUISHER = %s\n", __func__, L2VPN_VPLSRD_ROUTE_DISTINGUISHER (pVPLSNextRdEntry));
            return L2VPN_SUCCESS;
        }
    }

    if (L2VPN_SUCCESS == L2VpnGetDefaultRdValue (u4VplsIndex,
                                                 pu1RouteDistinguisher))
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : pu1RouteDist = %x\n",
                    __func__, pu1RouteDistinguisher);
        return L2VPN_SUCCESS;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnSendVplsDownAdminEvent                               */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendVplsDownAdminEvent (UINT4 u4VplsConfigIndex)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tVPLSEntry         *pVplsEntry;
    INT4                i4RetStatus;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls Entry not found\n",
                    __func__);
        return L2VPN_SUCCESS;
    }

    if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls is manual\n", __func__);
        return L2VPN_SUCCESS;
    }

    if (L2VPN_VPLS_OPER_STAT_UP != L2VPN_VPLS_OPER_STATUS (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls is not up\n", __func__);
        return L2VPN_SUCCESS;
    }

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    i4RetStatus = L2VpnGetRdValue (L2VPN_VPLS_INDEX (pVplsEntry),
                                   L2VPN_ADMIN_EVT_VPLS_RD);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : Failure in \
                        getting RD value\n", __func__);
    }

    i4RetStatus = L2VpnGetExportRtValue (L2VPN_VPLS_INDEX (pVplsEntry),
                                         L2VPN_TRUE, L2VPN_ADMIN_EVT_VPLS_RT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : Failure in \
                        getting RT export value\n", __func__);
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (L2VPN_VPLS_INDEX (pVplsEntry),
                                                  &L2VPN_ADMIN_EVT_VPLS_VEID);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : Failure in \
                        getting VE-Id\n", __func__);
        return L2VPN_FAILURE;
    }

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_DOWN_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = L2VPN_VPLS_INDEX (pVplsEntry);
    L2VPN_ADMIN_EVT_VPLS_MTU = L2VPN_VPLS_MTU (pVplsEntry);
    L2VPN_ADMIN_EVT_VPLS_LB = L2VPN_VPLS_LABEL_BLOCK (pVplsEntry);
    L2VPN_ADMIN_EVT_VPLS_CONTROLWORD = L2VPN_VPLS_CONTROL_WORD (pVplsEntry);

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }

    L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_DOWN;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendVplsUpAdminEvent                                 */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendVplsUpAdminEvent (UINT4 u4VplsConfigIndex)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tVPLSEntry         *pVplsEntry;
    INT4                i4RetStatus = L2VPN_FAILURE;
    UINT1               u1IsAcPresent = L2VPN_FALSE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls Entry not found\n",
                    __func__);
        return L2VPN_SUCCESS;
    }

    if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls is manual\n", __func__);
        return L2VPN_SUCCESS;
    }

    L2VpnIsActiveAcPresent (u4VplsConfigIndex, &u1IsAcPresent);

    if (L2VPN_TRUE != u1IsAcPresent)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : No Active AC is present\n",
                    __func__);
        return L2VPN_SUCCESS;
    }

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_UP_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = L2VPN_VPLS_INDEX (pVplsEntry);

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnDeleteRTs                                             */
/* Description   : This routine Deletes All the RTs associated to VPLS         */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnDeleteRTs (UINT4 u4VplsConfigIndex)
{
    UINT4               u4VplsIndex = u4VplsConfigIndex;
    UINT4               u4RtIndex = L2VPN_ZERO;
    UINT4               u4NextVplsIndex;
    UINT4               u4NextRtIndex;
    INT1                i1RetStatus;
    UINT4               u4ErrorCode;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    i1RetStatus =
        nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex, &u4NextVplsIndex,
                                              u4RtIndex, &u4NextRtIndex);
    while (i1RetStatus == SNMP_SUCCESS)
    {
        u4RtIndex = u4NextRtIndex;
        if (u4VplsIndex != u4NextVplsIndex)
        {
            break;
        }
        if (nmhTestv2VplsBgpRteTargetRTRowStatus (&u4ErrorCode, u4VplsIndex,
                                                  u4RtIndex,
                                                  DESTROY) == SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : \
                            nmhTestv2VplsBgpRteTargetRTRowStatus failed\n", __func__);
            return L2VPN_FAILURE;
        }
        if (nmhSetVplsBgpRteTargetRTRowStatus (u4VplsIndex, u4RtIndex,
                                               DESTROY) == SNMP_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : \
                            nmhSetVplsBgpRteTargetRTRowStatus failed\n", __func__);
            return L2VPN_FAILURE;
        }
        i1RetStatus = nmhGetNextIndexVplsBgpRteTargetTable (u4VplsIndex,
                                                            &u4NextVplsIndex,
                                                            u4RtIndex,
                                                            &u4NextRtIndex);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendVplsAssociationDelEvent                             */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendVplsAssociationDelEvent (UINT4 u4VplsConfigIndex)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    INT4                i4RetStatus = L2VPN_FAILURE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_ASSOC_DELETE_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = u4VplsConfigIndex;

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendVplsRowDeleteAdminEvent                             */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendVplsRowDeleteAdminEvent (UINT4 u4VplsConfigIndex)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    INT4                i4RetStatus = L2VPN_FAILURE;
    tVPLSEntry         *pVplsEntry;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls Entry not found\n",
                    __func__);
        return L2VPN_SUCCESS;
    }

    if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls is manual\n", __func__);
        return L2VPN_SUCCESS;
    }

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_ROW_DELETE_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = L2VPN_VPLS_INDEX (pVplsEntry);

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendVplsDeleteAdminEvent                             */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendVplsDeleteAdminEvent (UINT4 u4VplsConfigIndex)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    tVPLSEntry         *pVplsEntry;
    INT4                i4RetStatus = L2VPN_FAILURE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls Entry not found\n",
                    __func__);
        return L2VPN_SUCCESS;
    }

    if (L2VPN_VPLS_SIG_BGP != L2VPN_VPLS_SIG_TYPE (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Vpls is manual\n", __func__);
        return L2VPN_SUCCESS;
    }

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_DELETE_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = L2VPN_VPLS_INDEX (pVplsEntry);

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnGetVplsAndRtIndexFromRtName                          */
/* Description   : This function returns VplsIndex for a particular RT       */
/* Input(s)      : pu1RouteTarget - RT value                                 */
/* Output(s)     : pu4VplsIndex - VPLS Instance associated with given RT     */
/*               : pu4RtIndex - Rt Instance associated with given RT         */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetVplsAndRtIndexFromRtName (UINT1 *pu1RouteTarget,
                                  UINT4 *pu4VplsIndex, UINT4 *pu4RtIndex)
{
    tVPLSRtEntry       *pVPLSRtEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVPLSRtEntry = (tVPLSRtEntry *)
        RBTreeGetFirst (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo));

    if (pVPLSRtEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : RT Entry not found\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    do
    {
        if (ACTIVE != L2VPN_VPLSRT_ROW_STATUS (pVPLSRtEntry))
        {
            continue;
        }

        if (MEMCMP (L2VPN_VPLSRT_ROUTE_TARGET (pVPLSRtEntry),
                    pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN) == 0)
        {
            *pu4VplsIndex = L2VPN_VPLSRT_VPLS_INSTANCE (pVPLSRtEntry);
            *pu4RtIndex = L2VPN_VPLSRT_RT_INDEX (pVPLSRtEntry);
            L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG, "%s : pu4VplsIndex = %d \
                                pu4RtIndex = %d\n", __func__, *pu4VplsIndex, *pu4RtIndex);
            return L2VPN_SUCCESS;
        }
    }
    while ((pVPLSRtEntry =
            (tVPLSRtEntry *)
            RBTreeGetNext (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                           pVPLSRtEntry, NULL)) != NULL);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnGetFreeRtIndexForVpls                                */
/* Description   : This function gets free RT index from RT bitmap for a VPLS*/
/* Input(s)      : u4VplsIndex - VPLS Index                                  */
/* Output(s)     : pu4RtIndex - Free RT Index                                */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetFreeRtIndexForVpls (UINT4 u4VplsIndex, UINT4 *pu4RtIndex)
{

    UINT4               u4Index;

    if (u4VplsIndex == 0 || u4VplsIndex > MAX_L2VPN_VPLS_ENTRIES)
    {
        return L2VPN_FAILURE;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    for (u4Index = 0; u4Index < MAX_L2VPN_VPLS_ENTRIES; u4Index++)
    {
        if (gu8RtBitmap[u4VplsIndex - 1] == (gu8RtBitmap[u4VplsIndex - 1] &
                                             (~(0x1 << u4Index))))
        {
            *pu4RtIndex = u4Index + 1;
            return L2VPN_SUCCESS;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnSetRtIndexForVpls                                    */
/* Description   : This function sets RT from RT bitmap.                     */
/* Input(s)      : u4VplsIndex - VPLS Index                                  */
/*                 u4RtIndex - RT Index                                      */
/* Output(s)     : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
L2VpnSetRtIndexForVpls (UINT4 u4VplsIndex, UINT4 u4RtIndex)
{
    gu8RtBitmap[u4VplsIndex - 1] = gu8RtBitmap[u4VplsIndex - 1] |
        (0x1 << (u4RtIndex - 1));
}

/*****************************************************************************/
/* Function Name : L2VpnReleaseRtIndexForVpls                                */
/* Description   : This function releases RT from RT bitmap.                 */
/* Input(s)      : u4VplsIndex -  VPLS Index                                 */
/*                 u4RtIndex - RT Index                                      */
/* Output(s)     : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
L2VpnReleaseRtIndexForVpls (UINT4 u4VplsIndex, UINT4 u4RtIndex)
{
    gu8RtBitmap[u4VplsIndex - 1] = gu8RtBitmap[u4VplsIndex - 1] &
        (~(0x1 << (u4RtIndex - 1)));
}

/*****************************************************************************/
/* Function Name : L2VpnGetFreeAcIndexForVplsAc                              */
/* Description   : This function gets free AC index from AC bitmap for a VPLS*/
/* Input(s)      : u4VplsIndex - VPLS Index                                  */
/* Output(s)     : pu4AcIndex - Free AC Index                                */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetFreeAcIndexForVplsAc (UINT4 u4VplsIndex, UINT4 *pu4AcIndex)
{

    UINT4               u4Index;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (u4VplsIndex == 0 || u4VplsIndex > MAX_L2VPN_VPLS_ENTRIES)
    {
        return L2VPN_FAILURE;
    }

    for (u4Index = 0; u4Index < MAX_L2VPN_VPLS_AC_ENTRIES; u4Index++)
    {
        if (gu8AcBitmap[u4VplsIndex - 1] == (gu8AcBitmap[u4VplsIndex - 1] &
                                             (~(0x1 << u4Index))))
        {
            *pu4AcIndex = u4Index + 1;
            return L2VPN_SUCCESS;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnSetAcIndexForVplsAc                                  */
/* Description   : This function sets AC from AC bitmap.                     */
/* Input(s)      : u4VplsIndex - VPLS Index                                  */
/*                 u4AcIndex - AC Index                                      */
/* Output(s)     : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
L2VpnSetAcIndexForVplsAc (UINT4 u4VplsIndex, UINT4 u4AcIndex)
{
    gu8AcBitmap[u4VplsIndex - 1] = gu8AcBitmap[u4VplsIndex - 1] |
        (0x1 << (u4AcIndex - 1));
}

/*****************************************************************************/
/* Function Name : L2VpnReleaseAcIndexForVpls                                */
/* Description   : This function releases AC from AC bitmap.                 */
/* Input(s)      : u4VplsIndex -  VPLS Index                                 */
/*                 u4AcIndex - RT Index                                      */
/* Output(s)     : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
L2VpnReleaseAcIndexForVplsAc (UINT4 u4VplsIndex, UINT4 u4AcIndex)
{
    gu8AcBitmap[u4VplsIndex - 1] = gu8AcBitmap[u4VplsIndex - 1] &
        (~(0x1 << (u4AcIndex - 1)));
}

/*****************************************************************************/
/* Function Name : L2VpnGetAcIndexFromIfandVlan                              */
/* Description   : This function returns Ac Index corresponding to a port If */
/*                 Index and Vlan Id                                         */
/* Input(s)      : u4IfIndex -  Port If Index                                */
/*                 u4VlanId - Vlan Id                                        */
/* Output(s)     : pu4AcIndex - Ac Index for the inputs.                     */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetAcIndexFromIfandVlan (UINT4 u4IfIndex,
                              UINT4 u4VlanId, UINT4 *pu4AcIndex)
{
    tVplsAcMapEntry    *pVplsAcMapEntry = NULL;
    tVplsAcMapEntry    *pVplsAcMapNextEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsAcMapNextEntry = (tVplsAcMapEntry *)
        RBTreeGetFirst (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsAcMapNextEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS AC map entry not \
                                found\n", __func__);
        return L2VPN_FAILURE;
    }

    do
    {
        pVplsAcMapEntry = pVplsAcMapNextEntry;

        if (ACTIVE != L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry))
        {
            continue;
        }
        if (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) == u4IfIndex
            && ((L2VPN_VPLSAC_VLAN_ID (pVplsAcMapEntry) == u4VlanId)
                || (L2VPN_VPLSAC_VLAN_ID (pVplsAcMapEntry) ==
                    L2VPN_PWVC_ENET_DEF_PORT_VLAN)))
        {
            *pu4AcIndex = L2VPN_VPLSAC_INDEX (pVplsAcMapEntry);
            L2VPN_DBG2 (L2VPN_DBG_LVL_DBG_FLAG, "%s : AcIndex = %d\n",
                        __func__, *pu4AcIndex);
            return L2VPN_SUCCESS;
        }
    }

    while ((pVplsAcMapNextEntry =
            (tVplsAcMapEntry *)
            RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           pVplsAcMapEntry, NULL)) != NULL);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_FAILURE;
}

/*****************************************************************************/
/* Function Name : L2VpnIsActiveAcPresent                                    */
/* Description   : This function returns whether Active AC present for the   */
/*                     given VPLS or not                                     */
/* Input(s)      : u4VplsInstance -  Vpls Instance                           */
/* Output(s)     : pu1IsAcPresent - L2VPN_TRUE/L2VPN_FALSE                   */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
L2VpnIsActiveAcPresent (UINT4 u4VplsInstance, UINT1 *pu1IsAcPresent)
{
    tVplsAcMapEntry     VplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapNextEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&VplsAcMapEntry, L2VPN_ZERO, sizeof (tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcMapEntry) = u4VplsInstance;

    pVplsAcMapNextEntry =
        (tVplsAcMapEntry *)
        RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo), &VplsAcMapEntry,
                       NULL);

    while (NULL != pVplsAcMapNextEntry)
    {
        if (u4VplsInstance != L2VPN_VPLSAC_VPLS_INSTANCE (pVplsAcMapNextEntry))
        {
            break;
        }

        if (L2VPN_VPLS_AC_OPER_STATUS_UP ==
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapNextEntry))
        {
            *pu1IsAcPresent = L2VPN_TRUE;
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Active AC is present",
                        __func__);
            return;
        }

        pVplsAcMapNextEntry =
            (tVplsAcMapEntry *)
            RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           pVplsAcMapNextEntry, NULL);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnGetDefaultRdValue                                    */
/* Description   : This function returns Default RD value for a given VPLS   */
/* Input(s)      : u4VplsIndex - VPLS Instance                               */
/* Output(s)     : pu1RouteDistinguisher - RD value for given vpls           */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetDefaultRdValue (UINT4 u4VplsIndex, UINT1 *pu1RouteDistinguisher)
{
    ULONG8              u8RdValue = L2VPN_ZERO;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4AsnValue = L2VPN_ZERO;
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4BgpAdminState = L2VPN_ZERO;
    UINT2               u2AsnValue = L2VPN_ZERO;
    UINT2               u2VpnId = L2VPN_ZERO;
    UINT1               u1AsnType = L2VPN_ZERO;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return L2VPN_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : Vpls Entry not found\n",
                    __func__);
        return L2VPN_FAILURE;
    }
    MEMCPY (&u8RdValue,
            L2VPN_VPLS_DEFAULT_RD (pVplsEntry), L2VPN_MAX_VPLS_RD_LEN);
    if (L2VPN_ZERO != u8RdValue)
    {
        MEMCPY (pu1RouteDistinguisher,
                L2VPN_VPLS_DEFAULT_RD (pVplsEntry), L2VPN_MAX_VPLS_RD_LEN);
        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Default RD is present\n",
                    __func__);
        return L2VPN_SUCCESS;
    }

    MplsGetASN (&u1AsnType, &u4AsnValue);

    MEMCPY (&u4VpnId,
            &L2VPN_VPLS_VPN_ID (pVplsEntry)[STRLEN (MPLS_OUI_VPN_ID)],
            sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);

    if (L2VPN_VPLS_RD_TYPE_0 == u1AsnType)
    {
        u2AsnValue = (UINT2) u4AsnValue;
        pu1RouteDistinguisher[0] = L2VPN_VPLS_RD_TYPE_0;
        pu1RouteDistinguisher[1] = L2VPN_VPLS_RD_SUBTYPE;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4VpnId = OSIX_HTONL (u4VpnId);

        MEMCPY (&pu1RouteDistinguisher[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RouteDistinguisher[4], &u4VpnId, sizeof (UINT4));
    }
    else
    {
        u2VpnId = (UINT2) u4VpnId;
        pu1RouteDistinguisher[0] = L2VPN_VPLS_RD_TYPE_2;
        pu1RouteDistinguisher[1] = L2VPN_VPLS_RD_SUBTYPE;

        u4AsnValue = OSIX_HTONL (u4AsnValue);
        u2VpnId = OSIX_HTONS (u2VpnId);

        MEMCPY (&pu1RouteDistinguisher[2], &u4AsnValue, sizeof (UINT4));
        MEMCPY (&pu1RouteDistinguisher[6], &u2VpnId, sizeof (UINT2));
    }

    MEMCPY (L2VPN_VPLS_DEFAULT_RD (pVplsEntry),
            pu1RouteDistinguisher, L2VPN_MAX_VPLS_RD_LEN);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnGetDefaultRtValue                                    */
/* Description   : This function returns Default Rt value for a given VPLS   */
/* Input(s)      : u4VplsIndex - VPLS Instance                               */
/* Output(s)     : pu1RouteTarget - RT value for given vpls                  */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnGetDefaultRtValue (UINT4 u4VplsIndex, UINT1 *pu1RouteTarget)
{
    ULONG8              u8RtValue = L2VPN_ZERO;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4AsnValue = L2VPN_ZERO;
    UINT4               u4VpnId = L2VPN_ZERO;
    UINT4               u4BgpAdminState = L2VPN_ZERO;
    UINT2               u2AsnValue = L2VPN_ZERO;
    UINT2               u2VpnId = L2VPN_ZERO;
    UINT1               u1AsnType = L2VPN_ZERO;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        return L2VPN_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Entry \
                        is not found\n", __func__);
        return L2VPN_FAILURE;
    }
    MEMCPY (&u8RtValue,
            L2VPN_VPLS_DEFAULT_RT (pVplsEntry), L2VPN_MAX_VPLS_RT_LEN);
    if (L2VPN_ZERO != u8RtValue)
    {
        MEMCPY (pu1RouteTarget,
                L2VPN_VPLS_DEFAULT_RT (pVplsEntry), L2VPN_MAX_VPLS_RT_LEN);

        L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Default RT \
                        is already present\n", __func__);
        return L2VPN_SUCCESS;
    }
    MplsGetASN (&u1AsnType, &u4AsnValue);

    MEMCPY (&u4VpnId,
            &L2VPN_VPLS_VPN_ID (pVplsEntry)[STRLEN (MPLS_OUI_VPN_ID)],
            sizeof (UINT4));
    u4VpnId = OSIX_HTONL (u4VpnId);

    if (L2VPN_VPLS_RT_TYPE_0 == u1AsnType)
    {
        u2AsnValue = (UINT2) u4AsnValue;
        pu1RouteTarget[0] = L2VPN_VPLS_RT_TYPE_0;
        pu1RouteTarget[1] = L2VPN_VPLS_RT_SUBTYPE;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4VpnId = OSIX_HTONL (u4VpnId);

        MEMCPY (&pu1RouteTarget[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RouteTarget[4], &u4VpnId, sizeof (UINT4));
    }
    else
    {
        u2VpnId = (UINT2) u4VpnId;
        pu1RouteTarget[0] = L2VPN_VPLS_RT_TYPE_2;
        pu1RouteTarget[1] = L2VPN_VPLS_RT_SUBTYPE;

        u4AsnValue = OSIX_HTONL (u4AsnValue);
        u2VpnId = OSIX_HTONS (u2VpnId);

        MEMCPY (&pu1RouteTarget[2], &u4AsnValue, sizeof (UINT4));
        MEMCPY (&pu1RouteTarget[6], &u2VpnId, sizeof (UINT2));
    }

    MEMCPY (L2VPN_VPLS_DEFAULT_RT (pVplsEntry),
            pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN);

    if (L2VPN_ZERO != MEMCMP (L2VPN_VPLS_PREVIOUS_DEFAULT_RT (pVplsEntry),
                              L2VPN_VPLS_DEFAULT_RT (pVplsEntry),
                              L2VPN_MAX_VPLS_RD_LEN))
    {
        MEMCPY (&u8RtValue,
                L2VPN_VPLS_PREVIOUS_DEFAULT_RT (pVplsEntry),
                L2VPN_MAX_VPLS_RT_LEN);

        L2VPN_DBG4 (L2VPN_DBG_LVL_DBG_FLAG, "%s : VplsIndex = %d \
                                    L2VPN_VPLS_PREVIOUS_DEFAULT_RT = %s \
                                    L2VPN_VPLS_DEFAULT_RT = %s\n", __func__, u4VplsIndex, L2VPN_VPLS_PREVIOUS_DEFAULT_RT (pVplsEntry), L2VPN_VPLS_DEFAULT_RT (pVplsEntry));
        if (L2VPN_ZERO != u8RtValue)
        {
            L2VpnSendBgpRtDeleteEvent (u4VplsIndex,
                                       L2VPN_VPLS_PREVIOUS_DEFAULT_RT
                                       (pVplsEntry), L2VPN_VPLS_RT_BOTH);
        }

        L2VpnSendBgpRtAddEvent (u4VplsIndex,
                                L2VPN_VPLS_DEFAULT_RT (pVplsEntry),
                                L2VPN_VPLS_RT_BOTH);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnBgpEventHandler                                      */
/* Description   : This function is used by Bgp module to post its events    */
/* Input(s)      : pMsg - pointer to message buffer                          */
/*                 u4Event - Event to be posted to L2VPN module              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
UINT4
L2VpnBgpEventHandler (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo)
{
    tL2VpnQMsg          L2VpnQMsg;
    UINT4               u4RetStatus;
    UINT1              *pu1TmpMsg = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "\rL2VPN Service is Down\n");
        return L2VPN_FAILURE;
    }

    L2VPN_QMSG_TYPE = L2VPN_BGP_SIG_EVENT;

    MEMCPY (&L2VpnQMsg.L2VpnEvtInfo, pL2VpnBgpEvtInfo,
            sizeof (tL2VpnBgpEvtInfo));

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Memory Allocation failed for L2VPN Q Pool\r\n",
                    __func__);

        return L2VPN_FAILURE;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    u4RetStatus = L2VpnInternalEventHandler (pu1TmpMsg);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return u4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L2VpnUtilParseAndGenerateRdRt                       */
/*                                                                           */
/*     DESCRIPTION      : This function takes random string as input and     */
/*                        generate valid RD/RT Value                         */
/*     INPUT            : pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/*     OUTPUT           : pu1RdRt - Generated Rd Value                       */
/*                                                                           */
/*     RETURNS          : L2VPN_SUCCESS/L2VPN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
L2VpnUtilParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt)
{
    UINT4               u4AsnValue = L2VPN_ZERO;
    UINT4               u4nnValue = L2VPN_ZERO;
    UINT4               u4LoopIndex = L2VPN_ZERO;
    UINT4               u4IpIndex = L2VPN_ZERO;
    UINT2               u2AsnValue = L2VPN_ZERO;
    UINT2               u2nnValue = L2VPN_ZERO;
    UINT1               au1InputString[L2VPN_MAX_RD_RT_STRING_LEN + 1];
    UINT4               au4IPValue[IPV4_ADDR_LENGTH];
    UINT1               u1IsColonFound = L2VPN_FALSE;
    UINT1               u1IsDotFound = L2VPN_FALSE;
    UINT1               u1NumberOfDigits = 0;

    MEMSET (au1InputString, L2VPN_ZERO, L2VPN_MAX_RD_RT_STRING_LEN + 1);
    MEMSET (au4IPValue, L2VPN_ZERO, sizeof (au4IPValue));

    if (NULL == pu1RandomString)
    {
        return L2VPN_FAILURE;
    }

    STRNCPY (au1InputString, pu1RandomString, L2VPN_MAX_RD_RT_STRING_LEN);
    au1InputString[L2VPN_MAX_RD_RT_STRING_LEN] = '\0';

    for (u4LoopIndex = L2VPN_ZERO; u4LoopIndex < STRLEN (au1InputString);
         u4LoopIndex++)
    {
        if (au1InputString[u4LoopIndex] == '.')
        {
            if (u1NumberOfDigits > MAX_DIGITS_IN_UINT2)
            {
                return L2VPN_FAILURE;
            }

            u1NumberOfDigits = 0;

            if (L2VPN_TRUE == u1IsColonFound)
            {
                return L2VPN_FAILURE;
            }

            pu1RdRt[0] = L2VPN_VPLS_RT_TYPE_1;
            au4IPValue[u4IpIndex] = (UINT4) ATOI (pu1RandomString);
            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            u1IsDotFound = L2VPN_TRUE;
            u4IpIndex++;

            if (IPV4_ADDR_LENGTH == u4IpIndex)
            {
                return L2VPN_FAILURE;
            }
            continue;
        }

        if (au1InputString[u4LoopIndex] == ':')
        {
            if (L2VPN_TRUE == u1IsColonFound)
            {
                return L2VPN_FAILURE;
            }

            if (L2VPN_FALSE == u1IsDotFound)
            {
                if (u1NumberOfDigits == MAX_DIGITS_IN_UINT4)
                {
                    return L2VPN_FAILURE;
                }

                u1NumberOfDigits = 0;

                errno = 0;
                u4AsnValue = strtoul ((const char *) pu1RandomString, NULL, 0);
                if (errno == ERANGE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                "\n %s: Asn Value out of range\n", __func__);
                    return L2VPN_FAILURE;
                }
                if (UINT2_MAX_VALUE >= u4AsnValue)
                {
                    pu1RdRt[0] = L2VPN_VPLS_RT_TYPE_0;
                }
                else
                {
                    pu1RdRt[0] = L2VPN_VPLS_RT_TYPE_2;
                }
            }
            else
            {
                if (u1NumberOfDigits > MAX_DIGITS_IN_UINT2)
                {
                    return L2VPN_FAILURE;
                }

                u1NumberOfDigits = 0;

                au4IPValue[u4IpIndex] = (UINT4) (ATOI (pu1RandomString));
                pu1RandomString = &au1InputString[u4LoopIndex + 1];
                u4IpIndex++;
                if (IPV4_ADDR_LENGTH != u4IpIndex)
                {
                    if (2 == u4IpIndex)
                    {
                        pu1RdRt[0] = L2VPN_VPLS_RT_TYPE_2;
                    }
                    else
                    {
                        return L2VPN_FAILURE;
                    }
                }
            }

            if (u4LoopIndex == (STRLEN (au1InputString) - 1))
            {
                return L2VPN_FAILURE;
            }

            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            u1IsColonFound = L2VPN_TRUE;
            continue;
        }

        if (!(ISDIGIT (au1InputString[u4LoopIndex])))
        {
            return L2VPN_FAILURE;
        }
        u1NumberOfDigits++;
    }

    if (L2VPN_TRUE != u1IsColonFound)
    {
        return L2VPN_FAILURE;
    }

    if (u1NumberOfDigits > MAX_DIGITS_IN_UINT4)
    {
        return L2VPN_FAILURE;
    }

    errno = 0;
    u4nnValue = strtoul ((const char *) pu1RandomString, NULL, 0);
    if (errno == ERANGE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "\n %s: nn Value out of range\n", __func__);
        return L2VPN_FAILURE;
    }

    if (L2VPN_VPLS_RT_TYPE_0 == pu1RdRt[0])
    {
        u2AsnValue = (UINT2) u4AsnValue;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4nnValue = OSIX_HTONL (u4nnValue);

        MEMCPY (&pu1RdRt[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RdRt[4], &u4nnValue, sizeof (UINT4));
    }

    if (L2VPN_VPLS_RT_TYPE_1 == pu1RdRt[0])
    {
        if (UINT2_MAX_VALUE < u4nnValue)
        {
            return L2VPN_FAILURE;
        }

        u2nnValue = (UINT2) u4nnValue;

        u2nnValue = OSIX_HTONS (u2nnValue);

        if (UINT1_MAX_VALUE < au4IPValue[0] ||
            UINT1_MAX_VALUE < au4IPValue[1] ||
            UINT1_MAX_VALUE < au4IPValue[2] || UINT1_MAX_VALUE < au4IPValue[3])
        {
            return L2VPN_FAILURE;
        }

        pu1RdRt[2] = (UINT1) au4IPValue[0];
        pu1RdRt[3] = (UINT1) au4IPValue[1];
        pu1RdRt[4] = (UINT1) au4IPValue[2];
        pu1RdRt[5] = (UINT1) au4IPValue[3];
        MEMCPY (&pu1RdRt[6], &u2nnValue, sizeof (UINT2));
    }

    if (L2VPN_VPLS_RT_TYPE_2 == pu1RdRt[0])
    {
        if (UINT2_MAX_VALUE < u4nnValue)
        {
            return L2VPN_FAILURE;
        }

        u2nnValue = (UINT2) u4nnValue;

        u4AsnValue = OSIX_HTONL (u4AsnValue);
        u2nnValue = OSIX_HTONS (u2nnValue);

        if (L2VPN_TRUE == u1IsDotFound)
        {
            if (UINT2_MAX_VALUE < au4IPValue[0] ||
                UINT2_MAX_VALUE < au4IPValue[1])
            {
                return L2VPN_FAILURE;
            }

            u2AsnValue = (UINT2) au4IPValue[0];
            u2AsnValue = OSIX_HTONS (u2AsnValue);
            MEMCPY (&pu1RdRt[2], &u2AsnValue, sizeof (UINT2));

            u2AsnValue = (UINT2) au4IPValue[1];
            u2AsnValue = OSIX_HTONS (u2AsnValue);
            MEMCPY (&pu1RdRt[4], &u2AsnValue, sizeof (UINT2));
        }
        else
        {
            MEMCPY (&pu1RdRt[2], &u4AsnValue, sizeof (UINT4));
        }
        MEMCPY (&pu1RdRt[6], &u2nnValue, sizeof (UINT2));
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendRtAddAdminEvent                                  */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/*                 pu1RouteTarget - RT value                                 */
/*                 u1RTType - RT Type(both/import/export)                    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendRtAddAdminEvent (UINT4 u4VplsIndex,
                          UINT1 *pu1RouteTarget, UINT1 u1RTType)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    INT4                i4RetStatus = L2VPN_FAILURE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_RT_ADD_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = u4VplsIndex;
    L2VPN_ADMIN_EVT_VPLS_RTTYPE = u1RTType;
    MEMCPY (L2VPN_ADMIN_EVT_VPLS_RT, pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN);

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendRtDeleteAdminEvent                               */
/* Description   : This routine posts Admin event to L2VPN module, this fn   */
/*                 is called from low level routine                          */
/* Input(s)      : u4VplsConfigIndex - vpls index                            */
/*                 pu1RouteTarget - RT value                                 */
/*                 u1RTType - RT Type(both/import/export)                    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendRtDeleteAdminEvent (UINT4 u4VplsIndex,
                             UINT1 *pu1RouteTarget, UINT1 u1RTType)
{
    tL2VpnAdminEvtInfo  L2VpnAdminEvtInfo;
    INT4                i4RetStatus = L2VPN_FAILURE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnAdminEvtInfo, 0, sizeof (tL2VpnAdminEvtInfo));

    L2VPN_ADMIN_EVT_VPLS_EVTTYPE = L2VPN_VPLS_RT_DELETE_EVENT;
    L2VPN_ADMIN_EVT_VPLS_VPLSINDEX = u4VplsIndex;
    L2VPN_ADMIN_EVT_VPLS_RTTYPE = u1RTType;
    MEMCPY (L2VPN_ADMIN_EVT_VPLS_RT, pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN);

    i4RetStatus = (INT4) L2VpnAdminEventHandler (&L2VpnAdminEvtInfo,
                                                 L2VPN_VPLS_ADMIN_EVENT);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : L2VpnAdminEventHandler \
                                failed\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleIfDownForVplsAc                                */
/* Description   : This function handles if down event for autodiscovered    */
/*                 vpls                                                      */
/* Input(s)      : u4IfIndex -  Port If Index                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
L2VpnHandleIfDownForVplsAc (UINT4 u4IfIndex)
{
    tVplsAcMapEntry    *pVplsAcMapEntry = NULL;
    UINT1               u1IsAcPresent = L2VPN_FALSE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGetFirst (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsAcMapEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS AC map entry not \
                                found\n", __func__);
        return;
    }

    do
    {
        if (L2VPN_VPLS_AC_OPER_STATUS_UP !=
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry))
        {
            continue;
        }
        if (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) == u4IfIndex)
        {
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry) =
                L2VPN_VPLS_AC_OPER_STATUS_DOWN;

            L2VpnIsActiveAcPresent (L2VPN_VPLSAC_VPLS_INSTANCE
                                    (pVplsAcMapEntry), &u1IsAcPresent);
            if (L2VPN_FALSE == u1IsAcPresent)
            {
                L2VpnSendVplsDownAdminEvent (L2VPN_VPLSAC_VPLS_INSTANCE
                                             (pVplsAcMapEntry));
            }
        }
    }
    while ((pVplsAcMapEntry =
            (tVplsAcMapEntry *)
            RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           pVplsAcMapEntry, NULL)) != NULL);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnHandleIfUpForVplsAc                                  */
/* Description   : This function handles if up event for autodiscovered      */
/*                 vpls                                                      */
/* Input(s)      : u4IfIndex -  Port If Index                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
L2VpnHandleIfUpForVplsAc (UINT4 u4IfIndex)
{
    tVplsAcMapEntry    *pVplsAcMapEntry = NULL;
    UINT1               u1IsAcPresent = L2VPN_FALSE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGetFirst (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsAcMapEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS AC map entry not \
                                found\n", __func__);
        return;
    }

    do
    {
        if (ACTIVE != L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry))
        {
            continue;
        }
        if (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) == u4IfIndex)
        {
            L2VpnIsActiveAcPresent (L2VPN_VPLSAC_VPLS_INSTANCE
                                    (pVplsAcMapEntry), &u1IsAcPresent);
            L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapEntry) =
                L2VPN_VPLS_AC_OPER_STATUS_UP;
            if (L2VPN_FALSE == u1IsAcPresent)
            {
                L2VpnSendVplsUpAdminEvent (L2VPN_VPLSAC_VPLS_INSTANCE
                                           (pVplsAcMapEntry));
            }
        }
    }
    while ((pVplsAcMapEntry =
            (tVplsAcMapEntry *)
            RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           pVplsAcMapEntry, NULL)) != NULL);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnACExistsCheck                                        */
/* Description   : This routine checks whether this port vlan/port if index  */
/*                 already associated with any of the PW.                    */
/* Input(s)      : u4PwEnetPortVlan - Port Vlan                              */
/*                 i4PortIfIndex - Port If Index                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnACExistsCheck (UINT4 u4PwEnetPortVlan, INT4 i4PortIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;
    UINT4               u4PwIndex = L2VPN_ONE;

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4PortIfIndex);

    for (; u4PwIndex <= L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
        if ((pPwVcEntry == NULL) ||
            (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) == NULL))
        {
            continue;
        }

        /* Check if a pwEntry already exists for the same vlan /port */
        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                      ((tPwVcEnetServSpecEntry *)
                       L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
                      pPwVcEnetEntry, tPwVcEnetEntry *)
        {
            if (L2VPN_PWVC_ENET_ROWSTATUS (pPwVcEnetEntry) != L2VPN_PWVC_ACTIVE)
            {
                continue;
            }
            if ((u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN) &&
                (i4PortIfIndex != L2VPN_ZERO))
            {
                if ((L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                     u4PwEnetPortVlan) &&
                    (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                     i4PortIfIndex))
                {
                    return L2VPN_SUCCESS;
                }
            }
            else if (u4PwEnetPortVlan != L2VPN_PWVC_ENET_DEF_PORT_VLAN)
            {
                if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                    u4PwEnetPortVlan)
                {

                    return L2VPN_SUCCESS;
                }
            }
            else
            {
                if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                    i4PortIfIndex)
                {
                    return L2VPN_SUCCESS;
                }
            }
        }
    }

    if ((pMplsPortEntryInfo == NULL) ||
        (pMplsPortEntryInfo->i1RowStatus != ACTIVE))
    {
        return L2VPN_FAILURE;
    }
    if (L2VpnPortServiceStatus (i4PortIfIndex) == L2VPN_FAILURE)
    {
        return L2VPN_SUCCESS;
    }

    return L2VPN_FAILURE;
}
#endif
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
/*****************************************************************************/
/* Function Name : L2VpnPwVcHwListCreate                                     */
/* Description   : This routine is used to create and initialize HW List     */
/*                                                                              */
/* Input(s)      : pPwVcEntry                                                 */
/*                 pMplsHwVcInfo                                             */
/*                 ppL2VpnPwHwList                                              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnPwVcHwListCreate (tPwVcEntry * pPwVcEntry,
                       tMplsHwVcTnlInfo * pMplsHwVcInfo,
                       tL2VpnPwHwList * pL2VpnPwHwListEntry)
{
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (pL2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

    L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry) =
        L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry) =
        L2VPN_PWVC_INDEX (pPwVcEntry);
    L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX (pL2VpnPwHwListEntry) =
        L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX ((tPwVcMplsEntry *)
                                           L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
    L2VPN_PW_HW_LIST_IN_LABEL (pL2VpnPwHwListEntry) = L2VPN_ZERO;    /* Updated at ILM call */
    L2VPN_PW_HW_LIST_LSP_OUT_LABEL (pL2VpnPwHwListEntry) =
        pMplsHwVcInfo->OutLspLabel.u.MplsShimLabel;
    L2VPN_PW_HW_LIST_OUT_LABEL (pL2VpnPwHwListEntry) =
        pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY (L2VPN_PW_HW_LIST_PEER_IPV6_ADDRESS (pL2VpnPwHwListEntry),
                &(L2VPN_PWVC_PEER_ADDR (pPwVcEntry)), IPV6_ADDR_LENGTH);
    }
    else
#endif
    {
        MEMCPY (L2VPN_PW_HW_LIST_PEER_IP_ADDRESS (pL2VpnPwHwListEntry),
                &(L2VPN_PWVC_PEER_ADDR (pPwVcEntry)), IPV4_ADDR_LENGTH);
    }
    L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE (pL2VpnPwHwListEntry) =
        pPwVcEntry->i4PeerAddrType;

    L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry) =
        pPwVcEntry->u4VplsBgpPwBindRemoteVEId;
    L2VPN_PW_HW_LIST_OUT_IF_INDEX (pL2VpnPwHwListEntry) =
        pMplsHwVcInfo->u4PwL3Intf;
    L2VPN_PW_HW_LIST_LSP_IN_LABEL (pL2VpnPwHwListEntry) = L2VPN_ZERO;    /*Updated at ILM call */
    L2VPN_PW_HW_LIST_IN_IF_INDEX (pL2VpnPwHwListEntry) = L2VPN_ZERO;    /* Updated at ILM call */
    L2VPN_PW_HW_LIST_NPAPI_STATUS (pL2VpnPwHwListEntry) = L2VPN_ZERO;    /* Updated before and after NPAPI call */
    L2VPN_PW_HW_LIST_STALE_STATUS (pL2VpnPwHwListEntry) =
        L2VPN_PW_STATUS_NOT_STALE;
    L2VPN_PW_HW_LIST_OUT_PORT (pL2VpnPwHwListEntry) =
        pMplsHwVcInfo->u4PwOutPort;
    L2VPN_PW_HW_LIST_AC_ID (pL2VpnPwHwListEntry) = L2VPN_ZERO;

    if (L2VPN_IS_STATIC_PW (pPwVcEntry) &&
        L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_OTHER)
    {
        L2VPN_PW_HW_LIST_IS_STATIC_PW (pL2VpnPwHwListEntry) = L2VPN_TRUE;
    }
    else
    {
        L2VPN_PW_HW_LIST_IS_STATIC_PW (pL2VpnPwHwListEntry) = L2VPN_FALSE;
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwIlmHwListUpdate                                    */
/* Description   : This routine is used to update HW List created for PW     */
/*                                                                           */
/* Input(s)      : pPwVcEntry                                                */
/*                 ppL2VpnPwHwList                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwIlmHwListUpdate (tPwVcEntry * pPwVcEntry,
                        tMplsHwIlmInfo * pMplsHwIlmInfo,
                        tL2VpnPwHwList * pL2VpnPwHwListEntry)
{
    tPwVcMplsEntry     *pMplsPsnEntry = NULL;
    tL2VpnPwHwList      L2VpnPwHwListUpdate;

    MEMSET (&L2VpnPwHwListUpdate, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pL2VpnPwHwListEntry->u4VplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
    pL2VpnPwHwListEntry->u4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);

    if (L2VpnHwListGet (pL2VpnPwHwListEntry, &L2VpnPwHwListUpdate)
        == MPLS_FAILURE)
    {
        return L2VPN_FAILURE;
    }

    pMplsPsnEntry = L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
    if (L2VPN_MPLS_TYPE_VCONLY == L2VPN_PWVC_MPLS_MPLS_TYPE (pMplsPsnEntry) ||
        L2VPN_ZERO == pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel)
    {
        L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListUpdate) =
            pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel;
        L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListUpdate) = L2VPN_ZERO;
    }
    else
    {
        L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListUpdate) =
            pMplsHwIlmInfo->InLabelList[0].u.MplsShimLabel;
        L2VPN_PW_HW_LIST_IN_LABEL (&L2VpnPwHwListUpdate) =
            pMplsHwIlmInfo->InLabelList[1].u.MplsShimLabel;
    }
    L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListUpdate) =
        L2VPN_PW_STATUS_NOT_STALE;
    L2VPN_PW_HW_LIST_IN_IF_INDEX (&L2VpnPwHwListUpdate) =
        pMplsHwIlmInfo->u4InL3Intf;

    /* Updating ILM Info in Hw-List */
    L2VpnHwListUpdate (&L2VpnPwHwListUpdate);

    MEMCPY (pL2VpnPwHwListEntry, &L2VpnPwHwListUpdate, sizeof (tL2VpnPwHwList));

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnMarkLdpPwEntriesStale                                */
/* Description   : This routine is used to scan the PW HW List and mark the  */
/*                     PW as stale                                              */
/* Input(s)      : None                                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnMarkLdpPwEntriesStale ()
{
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tPwVcEntry         *pPwVcEntry = NULL;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (L2VpnHwListGetFirst (&L2VpnPwHwListEntry) == MPLS_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : No node in HwList\n",
                    __func__);
        return L2VPN_FAILURE;
    }
    do
    {
        if (L2VPN_ZERO == L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Mark PW(%d) entries as stale\n", __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));

            L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
                L2VPN_PW_STATUS_STALE;
            L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_ZERO);

            pPwVcEntry =
                L2VpnGetPwVcEntryFromIndex (L2VPN_PW_HW_LIST_PW_INDEX
                                            (&L2VpnPwHwListEntry));
            if (pPwVcEntry != NULL)
            {
                pPwVcEntry->u1GrSyncStatus = L2VPN_GR_NOT_SYNCHRONIZED;
                L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : PW(%d) - u1GrSyncStatus(%d)\n", __func__,
                            L2VPN_PWVC_INDEX (pPwVcEntry),
                            pPwVcEntry->u1GrSyncStatus);
            }
        }
    }
    while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
           == MPLS_SUCCESS);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnFlushPwVcDynamicEntries                              */
/* Description   : This routine is used to flush PW VC dynamic entries       */
/*                    however it will not delete the PW entries from hardware   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnFlushPwVcDynamicEntries (tPwVcActivePeerSsnEntry * pPeerSsn)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcLblMsgEntry   *pLblEntry = NULL;

    INT4                i4Status;
    UINT4               u4PwCount = L2VPN_ZERO;

    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
               "L2VpnFlushPwVcDynamicEntries:" " Entry\r\n");
    UNUSED_PARAM (i4Status);

    if (pPeerSsn == NULL)
    {
        return L2VPN_FAILURE;
    }

    u4PwCount = TMO_DLL_Count (L2VPN_PWVC_LIST (pPeerSsn));
    TMO_DLL_Scan (L2VPN_PWVC_LIST (pPeerSsn), pPwVcEntry, tPwVcEntry *)
    {
        TMO_DLL_Delete (L2VPN_PEND_MSG_TO_LDP_LIST (gpPwVcGlobalInfo),
                        &(pPwVcEntry->PendMsgToLdpNode));

        u4PwCount--;

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_PSN_BITMASK);

        L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;
        /* Since the LDP session itself is down, 
         * reset the PSN related  faults. */
        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &= (UINT1)
            (~L2VPN_PWVC_STATUS_PSN_BITMASK);

        L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) |=
            L2VPN_PWVC_STATUS_NOT_FORWARDING;

        L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry) = L2VPN_ZERO;

        L2VPN_PWVC_LCL_CC_SELECTED (pPwVcEntry) = L2VPN_ZERO;
        L2VPN_PWVC_LCL_CV_SELECTED (pPwVcEntry) = L2VPN_ZERO;

        pPwVcEntry->u1MapStatus = L2VPN_ZERO;

        /* Update the Napai status to FALSE so that PW is not deleted 
         * from Hardware */
        L2VPN_PWVC_NPAPI (pPwVcEntry) = L2VPN_FALSE;

        /* Update Pw Oper status */
        i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }

        i4Status = L2VpnUpdatePwVcOutboundList (pPwVcEntry, L2VPN_PWVC_DOWN);
        if (i4Status == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }

        /* update the PwRec with the default label value */
        L2VpnPwSetInVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);
        L2VpnPwSetOutVcLabel (pPwVcEntry, L2VPN_INVALID_LABEL);

        if (u4PwCount == L2VPN_ZERO)
        {
            if (L2VPN_SESSION_PWVC_DEREG_REQ (pPwVcEntry) == FALSE)
            {
                L2VPN_DBG ((DBG_ERR_NCRT),
                           "Deregistration with LDP is failed.\r\n");
            }
            pPeerSsn->u1PeerSsnLdpRegStatus = L2VPN_FALSE;
            pPeerSsn->i1SsnCreateReq = L2VPN_FALSE;
        }
    }

    /* Remove all the Label Advertisement Messages learned through
     * the peer since the session with the peer has gone down. */
    pLblEntry = (tPwVcLblMsgEntry *)
        TMO_DLL_First (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
    while (pLblEntry != NULL)
    {
        /* Stop Clean up Timer */
        TmrStopTimer (L2VPN_TIMER_LIST_ID, &(pLblEntry->CleanupTimer.AppTimer));
        L2VpnReleaseWaitingRemLblMsg (&(pLblEntry->LblInfo));
        TMO_DLL_Delete (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn),
                        &(pLblEntry->NextNode));
        MemReleaseMemBlock (L2VPN_LBL_MSG_POOL_ID, (UINT1 *) pLblEntry);
        pLblEntry = (tPwVcLblMsgEntry *)
            TMO_DLL_First (L2VPN_PWVC_LBL_MSG_LIST (pPeerSsn));
    }

    pPeerSsn->u4Status = L2VPN_SESSION_DOWN;
#ifdef MPLS_IPV6_WANTED
    L2VPN_DBG5 (L2VPN_DBG_GRACEFUL_RESTART,
                "L2VpnFlushPwVcDynamicEntries: "
                "LDP Session with Peer IPv4- %d.%d.%d.%d IPv6 %s is made DOWN\r\n",
                pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                pPeerSsn->PeerAddr.au1Ipv4Addr[3],
                Ip6PrintAddr (&(pPeerSsn->PeerAddr.Ip6Addr)));
#else
    L2VPN_DBG4 (L2VPN_DBG_GRACEFUL_RESTART,
                "L2VpnFlushPwVcDynamicEntries: "
                "LDP Session with Peer %d.%d.%d.%d is made DOWN\r\n",
                pPeerSsn->PeerAddr.au1Ipv4Addr[0],
                pPeerSsn->PeerAddr.au1Ipv4Addr[1],
                pPeerSsn->PeerAddr.au1Ipv4Addr[2],
                pPeerSsn->PeerAddr.au1Ipv4Addr[3]);
#endif

    return L2VPN_SUCCESS;

}

/*****************************************************************************/
/* Function Name : L2VpnGetLabelFromHwList                                   */
/* Description   : This routine is used to get InVcLabel from HW List         */
/* Input(s)      :     pL2VpnPwHwListEntry                                      */
/*                     ppL2VpnPwHwListEntry                                     */
/*                     u4InLabel                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_VALID or  L2VPN_INVALID                             */
/*****************************************************************************/
INT4
L2VpnGetNextLabelFromHwList (tL2VpnPwHwList * pL2VpnPwHwListEntry,
                             tL2VpnPwHwList * pL2VpnPwHwListNextEntry,
                             UINT4 *pu4InLabel)
{
    MPLS_L2VPN_LOCK ();
    while (L2VpnHwListGetNext (pL2VpnPwHwListEntry, pL2VpnPwHwListNextEntry)
           == MPLS_SUCCESS)
    {
        if ((L2VPN_ZERO ==
             L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListNextEntry)) &&
            (L2VPN_FALSE ==
             L2VPN_PW_HW_LIST_IS_STATIC_PW (pL2VpnPwHwListNextEntry)))
        {
            *pu4InLabel = L2VPN_PW_HW_LIST_IN_LABEL (pL2VpnPwHwListNextEntry);
            MPLS_L2VPN_UNLOCK ();
            return L2VPN_VALID;
        }
    }
    MPLS_L2VPN_UNLOCK ();
    return L2VPN_INVALID;
}

 /*****************************************************************************/
 /* Function Name : L2VpnVplsHwListAddUpdate                                     */
 /* Description   : This routine is used to add or update PW Entry in HW List */
 /*                 or send notification to RM                               */
 /* Input(s)      :  pL2VpnVplsHwListEntry                                       */
 /*                     u1HwStatus                                               */
 /* Output(s)     : None                                                      */
 /* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
 /*****************************************************************************/
INT4
L2VpnVplsHwListAddUpdate (tL2VpnVplsHwList * pL2VpnVplsHwListEntry,
                          UINT1 u1HwStatus)
{
    tL2VpnVplsHwList    L2VpnVplsHwListEntry;
    tL2VpnVplsHwList    L2VpnVplsHwList;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);
    MEMSET (&L2VpnVplsHwListEntry, L2VPN_ZERO, sizeof (tL2VpnVplsHwList));
    L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwListEntry) =
        L2VPN_VPLS_HW_LIST_VPLS_INDEX (pL2VpnVplsHwListEntry);

    if (L2VpnVplsHwListGet (&L2VpnVplsHwListEntry, &L2VpnVplsHwList)
        == MPLS_FAILURE)
    {
        L2VPN_VPLS_HW_LIST_NPAPI_STATUS (pL2VpnVplsHwListEntry) |= u1HwStatus;
        if (L2VpnVplsHwListAdd (pL2VpnVplsHwListEntry) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "HW List Add is failed\n");
            return L2VPN_FAILURE;
        }
#ifdef L2VPN_HA_WANTED
        L2VpnRmSendVplsHwListEntry (pL2VpnVplsHwListEntry, ADD_VPLS);
#endif /* L2VPN_HA_WANTED */
    }
    else
    {
        L2VPN_VPLS_HW_LIST_NPAPI_STATUS (&L2VpnVplsHwList) |= u1HwStatus;
        L2VpnVplsHwListUpdate (&L2VpnVplsHwList);
#ifdef L2VPN_HA_WANTED
        L2VpnRmSendVplsHwListEntry (&L2VpnVplsHwList, ADD_UPDATE_VPLS);
#endif /* L2VPN_HA_WANTED */
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

  /*****************************************************************************/
 /* Function Name : L2VpnVplsHwListRemove                                        */
 /* Description   : This routine is used update/Delete HW List and send          */
 /*                     notification to RM                                       */
 /* Input(s)      :  pPwVcEntry                                              */
 /*                  u1HwStatus                                               */
 /* Output(s)     : None                                                      */
 /* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
 /*****************************************************************************/
INT4
L2VpnVplsHwListRemove (tL2VpnVplsHwList * pL2VpnVplsHwListEntry,
                       UINT1 u1HwStatus)
{
    tL2VpnVplsHwList    L2VpnVplsHwList;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (L2VpnVplsHwListGet (pL2VpnVplsHwListEntry, &L2VpnVplsHwList)
        == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Entry is not present in HW List\n");
        return L2VPN_FAILURE;
    }
    L2VPN_VPLS_HW_LIST_NPAPI_STATUS (&L2VpnVplsHwList) &= (UINT1) (~u1HwStatus);
    L2VpnVplsHwListUpdate (&L2VpnVplsHwList);
#ifdef L2VPN_HA_WANTED
    L2VpnRmSendVplsHwListEntry (&L2VpnVplsHwList, DEL_VPLS);
#endif

    if (L2VPN_VPLS_HW_LIST_NPAPI_STATUS (&L2VpnVplsHwList) == L2VPN_ZERO)
    {
        if (L2VpnVplsHwListDelete (&L2VpnVplsHwList) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Deletion from HW List failed\n");
            return L2VPN_FAILURE;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwVcHwListAddUpdate                                     */
/* Description   : This routine is used to add or update PW Entry in HW List */
/*                    or send notification to RM                                 */
/* Input(s)      :  pL2VpnPwHwListEntry                                         */
/*                     u1HwStatus                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPwHwListAddUpdate (tL2VpnPwHwList * pL2VpnPwHwListEntry, UINT1 u1HwStatus)
{

    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tL2VpnPwHwList      L2VpnPwHwListUpdate;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

    L2VpnPwHwListEntry.u4VplsIndex =
        L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry);
    L2VpnPwHwListEntry.u4PwIndex =
        L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry);

    /*Check if Entry does not exist, in that case add, else update withj Non-Zero Params */
    if (L2VpnHwListGet (&L2VpnPwHwListEntry, &L2VpnPwHwListUpdate)
        == MPLS_FAILURE)
    {
        /*Entry does not exist in List, hence ADD */
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Entry is not present in HW List"
                   "so added\n");
        L2VPN_PW_HW_LIST_NPAPI_STATUS (pL2VpnPwHwListEntry) |= u1HwStatus;

        if (L2VpnHwListAdd (pL2VpnPwHwListEntry) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "HW List Add is failed\n");
            return L2VPN_FAILURE;
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG, "%s : NPAPI Status(%d)\n",
                    __func__,
                    L2VPN_PW_HW_LIST_NPAPI_STATUS (pL2VpnPwHwListEntry));

#ifdef L2VPN_HA_WANTED
        /*Send Notification to RM */
        L2VpnRmSendPwVcHwListEntry (pL2VpnPwHwListEntry, ADD_PW);
#endif

    }
    else
    {
        L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListUpdate) |= u1HwStatus;
        L2VPN_PW_HW_LIST_AC_ID (&L2VpnPwHwListUpdate) =
            L2VPN_PW_HW_LIST_AC_ID (pL2VpnPwHwListEntry);
        if (MPLS_ZERO != L2VPN_PW_HW_LIST_STALE_STATUS (pL2VpnPwHwListEntry))
        {
            L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListUpdate) =
                L2VPN_PW_HW_LIST_STALE_STATUS (pL2VpnPwHwListEntry);
        }

        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG, "%s : NPAPI Status(%d)\n", __func__,
                    L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListUpdate));

        L2VpnHwListUpdate (&L2VpnPwHwListUpdate);

#ifdef L2VPN_HA_WANTED
        /*Send Notification to RM */
        L2VpnRmSendPwVcHwListEntry (&L2VpnPwHwListUpdate, ADD_UPDATE_PW);
#endif

    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPwHwListDelete                                         */
/* Description   : This routine is used update/Delete HW List and send          */
/*                     notification to RM                                         */
/* Input(s)      :  pPwVcEntry                                               */
/*                  u1HwStatus                                               */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnPwHwListDelete (tL2VpnPwHwList * pL2VpnPwHwListKey, UINT1 u1HwStatus)
{
    tL2VpnPwHwList      L2VpnPwHwListUpdate;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&L2VpnPwHwListUpdate, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

    if (L2VpnHwListGet (pL2VpnPwHwListKey, &L2VpnPwHwListUpdate)
        == MPLS_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Entry is not present in HW List\n");
        return L2VPN_FAILURE;
    }

    L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListUpdate) =
        (UINT1) (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListUpdate) &
                 (~u1HwStatus));
    L2VpnHwListUpdate (&L2VpnPwHwListUpdate);
#ifdef L2VPN_HA_WANTED
    /*TODO - Send Notification to RM */
    L2VpnRmSendPwVcHwListEntry (&L2VpnPwHwListUpdate, DEL_PW);
#endif

    if (L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListUpdate) == L2VPN_ZERO)
    {
        if (L2VpnHwListDelete (&L2VpnPwHwListUpdate) == L2VPN_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                       "Deletion from HW List failed\n");
            return L2VPN_FAILURE;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnLdpSendLblRelEvent                                     */
/* Description   : This routine is used send an event to LDP to release      */
/*                  L2VpnLdpSendLblRelEvent                                  */
/* Input(s)      :  pPwVcEntry                                               */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnLdpSendLblRelEvent (tL2VpnPwHwList * pL2VpnPwHwListEntry)
{
    tL2VpnLdpPwVcEvtInfo TmpPwVcEvtInfo;

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
               "L2VpnLdpSendLblRelEvent: Entry\r\n");

    MEMSET (&TmpPwVcEvtInfo, L2VPN_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    LDP_L2VPN_EVT_TYPE (&TmpPwVcEvtInfo) = L2VPN_LDP_NOTIF_EVT;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE ==
        L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE (pL2VpnPwHwListEntry))
    {
        MEMCPY (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV6 (&TmpPwVcEvtInfo)),
                (L2VPN_PW_HW_LIST_PEER_IPV6_ADDRESS (pL2VpnPwHwListEntry)),
                IPV6_ADDR_LENGTH);
    }
    else
#endif
    {
        MEMCPY (&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR (&TmpPwVcEvtInfo)),
                (L2VPN_PW_HW_LIST_PEER_IP_ADDRESS (pL2VpnPwHwListEntry)),
                IPV4_ADDR_LENGTH);
    }
    LDP_L2VPN_EVT_NOTIF_ADDR_TYPE (&TmpPwVcEvtInfo) =
        (UINT2) L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE (pL2VpnPwHwListEntry);

    LDP_L2VPN_EVT_NOTIF_LABEL (&TmpPwVcEvtInfo) =
        L2VPN_PW_HW_LIST_IN_LABEL (pL2VpnPwHwListEntry);
    LDP_L2VPN_EVT_NOTIF_ENTITY_INDEX (&TmpPwVcEvtInfo) =
        L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX (pL2VpnPwHwListEntry);

    LDP_L2VPN_EVT_NOTIF_MSG_CODE (&TmpPwVcEvtInfo) = L2VPN_LDP_REL_LBL_RES;

    if (L2VpnLdpPostEvent (&TmpPwVcEvtInfo) == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Post event failed\r\n");
        return L2VPN_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnLdpSendLblRelEvent: Exit\r\n");

    return L2VPN_SUCCESS;
}
#endif
#ifdef VPLS_GR_WANTED
/*****************************************************************************/
/* Function Name : L2VpnSetLb                                           */
/* Description   : This function allocates 10 labels and return fisrt label  */
/* Input(s)      : NONE                                                      */
/* Output(s)     : pu4LabelBase - fisrt label value                          */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnSetLb (UINT4 u4LabelBase)
{
    UINT4               u4Index = L2VPN_ZERO;
    INT4                i4RetVal = L2VPN_SUCCESS;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    for (u4Index = L2VPN_ZERO; u4Index < L2VPN_VPLS_VBS_DEFAULT_VALUE;
         u4Index++)
    {
        if (LBL_FAILURE == LblMgrAssignLblToLblGroup (gu2BgpVplsLblGroupId,
                                                      0, u4LabelBase + u4Index))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "free label block is not available\r\n");
            i4RetVal = L2VPN_FAILURE;
            break;
        }

    }

    if (L2VPN_FAILURE == i4RetVal)
    {
        while (u4Index > L2VPN_ZERO)
        {
            u4Index--;
            (VOID) LblMgrRelLblToLblGroup (gu2BgpVplsLblGroupId, 0,
                                           u4LabelBase + u4Index);
        }
    }

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return i4RetVal;
}
#endif

#ifdef MPLS_IPV6_WANTED
/*****************************************************************************/
/* Function Name : L2VpnHandleIpv6RtChgNotification                          */
/* Description   : This function is used by MPLS FM module to indicate       */
/*                 IPv6 route change.                                        */
/* Input(s)      : pNetIpv6HlParams - Pointer to Route Information           */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnHandleIpv6RtChgNotification (tNetIpv6HliParams * pNetIpv6HlParams)
{

    tL2VpnQMsg          L2VpnQMsg;
    UINT4               u4RetStatus;
    UINT1              *pu1TmpMsg = NULL;
    uGenAddr            DestMask;

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "L2VpnHandleIpv6RtChgNotification: Function Entry\r\n");

    MEMSET (&DestMask, L2VPN_ZERO, sizeof (uGenAddr));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "L2VPN is Down\r\n");
        return;
    }

    if (pNetIpv6HlParams == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                   "IF: No Route Information passed by IP\n");
        return;
    }
    switch (pNetIpv6HlParams->u4Command)
    {
        case NETIPV6_ROUTE_CHANGE:
            MEMCPY (L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.DestAddr.Ip6Addr.u1_addr,
                    pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.Ip6Dst.
                    u1_addr, IPV6_ADDR_LENGTH);

            MplsGetIPV6Subnetmask (pNetIpv6HlParams->unIpv6HlCmdType.
                                   RouteChange.u1Prefixlen,
                                   DestMask.Ip6Addr.u1_addr);
            MEMCPY (L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.DestMask.Ip6Addr.u1_addr,
                    DestMask.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);

            MEMCPY (L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.NextHop.Ip6Addr.u1_addr,
                    pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.NextHop.
                    u1_addr, IPV6_ADDR_LENGTH);

            L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u4RtIfIndx =
                pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Index;

            L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.i4Metric1 =
                (INT4) pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Metric;

            L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1RowStatus =
                (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.
                u4RowStatus;

            L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1AddrType = MPLS_IPV6_ADDR_TYPE;
            /** Route add **/
            if ((pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit ==
                 IP6_BIT_ALL)
                || (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit ==
                    IP6_BIT_METRIC))
            {
                if (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus ==
                    IP6FWD_ACTIVE)
                {

                    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1CmdType =
                        IP6_ROUTE_ADD;
                    L2VPN_DBG4 (L2VPN_DBG_ALL_FLAG,
                                "IF: Ipv6 Route Add event for Dest Addr=%s PrefixLen=%d PrefixMask=%s NextHop=%s\n",
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.DestAddr.Ip6Addr)),
                                pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.
                                u1Prefixlen,
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.DestMask.Ip6Addr)),
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.NextHop.Ip6Addr)));
                }
                else
                {
                    return;
                }
            }

            /** Route Delete **/
            else if (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit ==
                     IP6_BIT_STATUS)
            {
                if (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus ==
                    IP6FWD_DESTROY)
                {
                    L2VPN_DBG4 (L2VPN_DBG_ALL_FLAG,
                                "IF: Ipv6 Route Delete event for Dest Addr=%s PrefixLen=%d PrefixMask=%s NextHop=%s\n",
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.DestAddr.Ip6Addr)),
                                pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.
                                u1Prefixlen,
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.DestMask.Ip6Addr)),
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.NextHop.Ip6Addr)));
                    L2VpnQMsg.L2VpnEvtInfo.PwRtEvtInfo.u1CmdType =
                        IP6_ROUTE_DEL;
                }
                else
                {
                    return;
                }
            }

            /** route Change **/
            else if ((IPV6_GET_BIT_FROM_CHANGED_PARAM
                      (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit,
                       IP6_BIT_NXTHOP))
                     ||
                     (IPV6_GET_BIT_FROM_CHANGED_PARAM
                      (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit,
                       IP6_BIT_RT_TYPE))
                     ||
                     (IPV6_GET_BIT_FROM_CHANGED_PARAM
                      (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit,
                       IP6_BIT_STATUS)))
            {
                if (pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus ==
                    IP6FWD_ACTIVE)
                {
                    L2VPN_DBG4 (L2VPN_DBG_ALL_FLAG,
                                "IF: Ipv6 Route Delete event for Dest Addr=%s PrefixLen=%d PrefixMask=%s NextHop=%s\n",
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.DestAddr.Ip6Addr)),
                                pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.
                                u1Prefixlen,
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.DestMask.Ip6Addr)),
                                Ip6PrintAddr (&
                                              (L2VpnQMsg.L2VpnEvtInfo.
                                               PwRtEvtInfo.NextHop.Ip6Addr)));
                    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                               "No need to handle Route Change event in L2vpn\n");
                    return;

                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }

            break;
        default:
            L2VPN_DBG1 (L2VPN_DBG_ALL_FLAG,
                        "IF: IPV6 unknown event with default Failure Case %d\n",
                        pNetIpv6HlParams->u4Command);
            return;
            break;
    };

    L2VPN_QMSG_TYPE = L2VPN_PWVC_ROUTE_EVENT;

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Route Event Handler - "
                   "Memory Allocation failed for L2VPN Q Pool\r\n");
        return;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) &L2VpnQMsg, sizeof (tL2VpnQMsg));

    u4RetStatus = L2VpnInternalEventHandler (pu1TmpMsg);

    if (u4RetStatus == L2VPN_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Route Event not sent to L2VPN\r\n");
    }

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "L2VpnHandleIpv6RtChgNotification: Function Exit\r\n");
    return;
}
#endif

VOID
L2vpnDelEnetHashNode (tTMO_HASH_NODE * pEnetHashNode)
{
    tPwVcEnetEntry     *pEnetEntry;

    pEnetEntry = (tPwVcEnetEntry *) pEnetHashNode;

    if (MEM_FAILURE == MemReleaseMemBlock (L2VPN_ENET_POOL_ID,
                                           (UINT1 *) pEnetEntry))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure -pEnetHashNode from hash table  \r\n");
    }
    return;
}

#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name : L2vpnHandleVplsNotification                              */
/* Description   : This function handles the Updation of Vpls status       */
/* Input(s)      : Vpls Notif Status                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2vpnHandleVplsNotification (INT4 i4SetValVplsStatusNotifEnable)
{
    UINT4               u4Index = L2VPN_ZERO;
    UINT4               u4VpnId = L2VPN_ZERO;
    tVPLSEntry         *pVplsEntry = NULL;

#ifdef NPAPI_WANTED
    tMplsNpWrFsMplsRegisterFwdAlarm MplsNpWrFsMplsRegisterFwdAlarm;
    tMplsHwVplsInfo     MplsHwVplsVpnInfo;
    MEMSET (&MplsNpWrFsMplsRegisterFwdAlarm, 0,
            sizeof (tMplsNpWrFsMplsRegisterFwdAlarm));
#endif

    L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
               "L2vpnHandleVplsNotification: Function Entry\r\n");

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, " VplsStatusNotif is set to %d\r \n",
                i4SetValVplsStatusNotifEnable);
    if (L2VPN_TRUE == i4SetValVplsStatusNotifEnable)
    {
        for (u4Index = 1; u4Index <= L2VPN_MAX_VPLS_ENTRIES; u4Index++)
        {
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

            if (pVplsEntry == NULL)
            {
                continue;
            }

            if (L2VPN_PWVC_ACTIVE == L2VPN_VPLS_ROW_STATUS (pVplsEntry))
            {

#ifdef L2RED_WANTED
                if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                {
#ifdef NPAPI_WANTED

                    MEMCPY (&u4VpnId,
                            &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                            sizeof (UINT4));

                    u4VpnId = OSIX_HTONL (u4VpnId);
                    MplsHwVplsVpnInfo.u4VplsHighWaterMark =
                        L2VPN_VPLS_FDB_HIGH_THRESHOLD (pVplsEntry);
                    MplsHwVplsVpnInfo.u4VplsLowWaterMark =
                        L2VPN_VPLS_FDB_LOW_THRESHOLD (pVplsEntry);
                    MplsNpWrFsMplsRegisterFwdAlarm.pMplsHwVplsVpnInfo =
                        &MplsHwVplsVpnInfo;
                    MplsFsMplsRegisterFwdAlarm (u4VpnId,
                                                MplsNpWrFsMplsRegisterFwdAlarm.
                                                pMplsHwVplsVpnInfo);

#endif
                }

            }
        }

        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   "L2vpnHandleVplsNotification: Function Exit\r\n");

        return L2VPN_SUCCESS;
    }
    else
    {
        for (u4Index = 1; u4Index <= L2VPN_MAX_VPLS_ENTRIES; u4Index++)
        {
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

            if (pVplsEntry == NULL)
            {
                continue;
            }

            if (L2VPN_PWVC_ACTIVE == L2VPN_VPLS_ROW_STATUS (pVplsEntry))
            {

#ifdef L2RED_WANTED
                if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                {
#ifdef NPAPI_WANTED
                    MEMCPY (&u4VpnId,
                            &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                            sizeof (UINT4));

                    u4VpnId = OSIX_HTONL (u4VpnId);
                    MplsFsMplsDeRegisterFwdAlarm (u4VpnId);
#endif

                }
            }
        }
        L2VPN_DBG (L2VPN_DBG_ALL_FLAG,
                   "L2vpnHandleVplsNotification: Function Exit\r\n");

        return L2VPN_SUCCESS;

    }
    UNUSED_PARAM (u4VpnId);
}
#endif
#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function Name : L2VpnIsSpokePw                                            */
/* Description   : This function returns wheather the PWIndex passed is of   */
/*                 spoke PW or Mesh PW                                       */
/* Input(s)      : u4PwVcIndex - PW Index                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_TRUE - In case of Spoke PW                          */
/*                 L2VPN_FALSE- In case of Mesh PW                           */
/*****************************************************************************/
INT4
L2VpnIsSpokePw (UINT4 u4PwVcIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tVplsPwBindEntry   *pVplsPwBindEntry = NULL;
    UINT4               u4VplsInstance;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "PW Entry DOESN'T exist for this PwIndex\n");
        return L2VPN_FAILURE;
    }

    u4VplsInstance = pPwVcEntry->u4VplsInstance;
    pVplsPwBindEntry = L2vpnGetVplsPwBindEntry (u4VplsInstance, u4PwVcIndex);
    if (pVplsPwBindEntry == NULL)
    {
        return L2VPN_FALSE;
    }

    if (pVplsPwBindEntry->u4VplsPwBindType == L2VPN_VPLS_PW_MESH)
    {
        return L2VPN_FALSE;
    }
    else
    {
        return L2VPN_TRUE;
    }

}
#endif
