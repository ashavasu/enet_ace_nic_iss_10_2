/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mspwclig.c,v 1.7 2014/12/24 10:58:28 siva Exp $
 *
 * Description: This file contains the  CLI related routines 
 *********************************************************************/

#ifndef __L2VPNCLIG_C__
#define __L2VPNCLIG_C__

#include "l2vpincs.h"
#include "mplscli.h"
/*This will be removed */
tPwVcEntry         *L2VpnGetPwVcEntryUsingPwId (UINT4 u4PwVcID);

/****************************************************************************
 * Function    :  cli_process_L2vpn_cmd
 * Description :  This function is exported to CLI module to handle the
 L2VPN cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
cli_process_MSPW_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pu4args[L2VPN_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    UINT4               u4Flag = L2VPN_ZERO;
    tL2vpnFsMsPwConfigTable L2vpnSetFsMsPwConfigTable;
    tL2vpnIsSetFsMsPwConfigTable L2vpnIsSetFsMsPwConfigTable;
    tPwVcEntry         *pPriPwVcEntry = NULL;    /*Primary PW VC */
    tPwVcEntry         *pSecPwVcEntry = NULL;    /*Secondary PW VC */

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, L2vpnLock, L2vpnUnLock);
    MPLS_L2VPN_LOCK ();

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        i4RetStatus = i4Inst;
    }

    while (1)
    {
        pu4args[argno++] = va_arg (ap, UINT4 *);
        if (argno == L2VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_L2VPN_FSMSPWMAXENTRIES:

            i4RetStatus = L2vpnCliSetFsMsPwMaxEntries (CliHandle, pu4args[0]);
            break;

        case CLI_L2VPN_FSMSPWCONFIGTABLE_CREATE:

            MEMSET (&L2vpnSetFsMsPwConfigTable, 0,
                    sizeof (tL2vpnFsMsPwConfigTable));
            MEMSET (&L2vpnIsSetFsMsPwConfigTable, 0,
                    sizeof (tL2vpnIsSetFsMsPwConfigTable));

            /*Get PW-VC records using pu4argswid */
            pPriPwVcEntry = L2VpnGetPwVcEntryUsingPwId (*pu4args[0]);
            if (pPriPwVcEntry == NULL)
            {
                CLI_SET_ERR (CLI_L2VPN_ERR_PWVC_NOT_FOUND);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pSecPwVcEntry = L2VpnGetPwVcEntryUsingPwId (*pu4args[1]);
            if (pSecPwVcEntry == NULL)
            {
                CLI_SET_ERR (CLI_L2VPN_ERR_PWVC_NOT_FOUND);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            L2VPN_FILL_FSMSPWCONFIGTABLE_ARGS ((&L2vpnSetFsMsPwConfigTable),
                                               (&L2vpnIsSetFsMsPwConfigTable),
                                               pPriPwVcEntry->u4PwVcIndex,
                                               pSecPwVcEntry->u4PwVcIndex,
                                               CREATE_AND_GO)
                i4RetStatus = L2vpnCliSetFsMsPwConfigTable (CliHandle,
                                                            &L2vpnSetFsMsPwConfigTable,
                                                            &L2vpnIsSetFsMsPwConfigTable);
            break;

        case CLI_L2VPN_FSMSPWCONFIGTABLE_DESTROY:

            MEMSET (&L2vpnSetFsMsPwConfigTable, 0,
                    sizeof (tL2vpnFsMsPwConfigTable));
            MEMSET (&L2vpnIsSetFsMsPwConfigTable, 0,
                    sizeof (tL2vpnIsSetFsMsPwConfigTable));

            /*Get PW-VC records using pu4argswid */
            pPriPwVcEntry = L2VpnGetPwVcEntryUsingPwId (*pu4args[0]);
            if (pPriPwVcEntry == NULL)
            {
                CLI_SET_ERR (CLI_L2VPN_ERR_PWVC_NOT_FOUND);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pSecPwVcEntry = L2VpnGetPwVcEntryUsingPwId (*pu4args[1]);
            if (pSecPwVcEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                CLI_SET_ERR (CLI_L2VPN_ERR_PWVC_NOT_FOUND);
                break;
            }

            L2VPN_FILL_FSMSPWCONFIGTABLE_ARGS ((&L2vpnSetFsMsPwConfigTable),
                                               (&L2vpnIsSetFsMsPwConfigTable),
                                               pPriPwVcEntry->u4PwVcIndex,
                                               pSecPwVcEntry->u4PwVcIndex,
                                               DESTROY)
                i4RetStatus = L2vpnCliSetFsMsPwConfigTable (CliHandle,
                                                            &L2vpnSetFsMsPwConfigTable,
                                                            &L2vpnIsSetFsMsPwConfigTable);
            break;

        case CLI_L2VPN_FSMSPWCONFIGTABLE_SHOW:

            u4Flag = CLI_PTR_TO_U4 (pu4args[0]);

            if (u4Flag != TRUE)
            {
                /*Get PW-VC records using pu4argswid */
                pPriPwVcEntry = L2VpnGetPwVcEntryUsingPwId (*pu4args[1]);
                if (pPriPwVcEntry == NULL)
                {
                    CLI_SET_ERR (CLI_L2VPN_ERR_PWVC_NOT_FOUND);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                pSecPwVcEntry = L2VpnGetPwVcEntryUsingPwId (*pu4args[2]);
                if (pSecPwVcEntry == NULL)
                {
                    CLI_SET_ERR (CLI_L2VPN_ERR_PWVC_NOT_FOUND);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            i4RetStatus = L2VpnMsPwShow (CliHandle, u4Flag, pPriPwVcEntry,
                                         pSecPwVcEntry);
            break;

        default:
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_L2VPN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", L2vpnCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4)i4RetStatus);

    CliUnRegisterLock (CliHandle);

    MPLS_L2VPN_UNLOCK ();

    return i4RetStatus;
}

/****************************************************************************
 * Function    :  L2vpnCliSetFsMsPwMaxEntries
 * Description :
 * Input       :  CliHandle 
 *                pFsMsPwMaxEntries
 * Output      :  None 
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
L2vpnCliSetFsMsPwMaxEntries (tCliHandle CliHandle, UINT4 *pFsMsPwMaxEntries)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsMsPwMaxEntries = 0;

    L2VPN_FILL_FSMSPWMAXENTRIES (u4FsMsPwMaxEntries, pFsMsPwMaxEntries);

    if (L2vpnTestFsMsPwMaxEntries (&u4ErrorCode, u4FsMsPwMaxEntries) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L2vpnSetFsMsPwMaxEntries (u4FsMsPwMaxEntries) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  L2vpnCliSetFsMsPwConfigTable
 * Description :
 * Input       :  CliHandle 
 *            pL2vpnSetFsMsPwConfigTable
 *            pL2vpnIsSetFsMsPwConfigTable
 * Output      :  None 
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
L2vpnCliSetFsMsPwConfigTable (tCliHandle CliHandle,
                              tL2vpnFsMsPwConfigTable *
                              pL2vpnSetFsMsPwConfigTable,
                              tL2vpnIsSetFsMsPwConfigTable *
                              pL2vpnIsSetFsMsPwConfigTable)
{
    UINT4               u4ErrorCode;

    if (L2vpnTestAllFsMsPwConfigTable (&u4ErrorCode, pL2vpnSetFsMsPwConfigTable,
                                       pL2vpnIsSetFsMsPwConfigTable, OSIX_TRUE,
                                       OSIX_TRUE) != SNMP_SUCCESS)
    {

        CLI_SET_ERR (CLI_L2VPN_ERR_MSPW_CONFIG);
        return CLI_FAILURE;
    }

    if (L2vpnSetAllFsMsPwConfigTable
        (pL2vpnSetFsMsPwConfigTable, pL2vpnIsSetFsMsPwConfigTable, OSIX_TRUE,
         OSIX_TRUE) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_L2VPN_ERR_MSPW_CONFIG);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : L2VpnMsPwShow 
 * Description :This function displays Pseudo wire cross connection information
 * Input       :  CliHandle 
 *                u4Flag
 *                u4PrmyPwVcIndex
 *                u4ScndyPwVcIndex
 * Output      :  None 
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
L2VpnMsPwShow (tCliHandle CliHandle, UINT4 u4Flag,
               tPwVcEntry * pPriPwVcEntry, tPwVcEntry * pSecPwVcEntry)
{
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;
    tL2vpnFsMsPwConfigTable L2vpnFsMsPwConfigTable;

    MEMSET (&L2vpnFsMsPwConfigTable, 0, sizeof (L2vpnFsMsPwConfigTable));

    CliPrintf (CliHandle, "\r\n %-14s%-14s%-14s%-14s\r\n",
               "Prmry PWID", "Scndy PWID", "OperStatus", "RowStatus");
    CliPrintf (CliHandle, "\r\n %-14s%-14s%-14s%-14s\r\n", "----------",
               "----------", "----------", "---------");

    /*display all the entries in the MSPW Table */
    if (u4Flag == TRUE)
    {
        while (1)
        {
            /*only executed at first time */
            if (pL2vpnFsMsPwConfigTable == NULL)
            {
                pL2vpnFsMsPwConfigTable = L2vpnGetFirstFsMsPwConfigTable ();

                if (pL2vpnFsMsPwConfigTable == NULL)
                {
                    return CLI_SUCCESS;
                }
            }
            else
            {
                pL2vpnFsMsPwConfigTable =
                    L2vpnGetNextFsMsPwConfigTable (pL2vpnFsMsPwConfigTable);

                if (pL2vpnFsMsPwConfigTable == NULL)
                {
                    break;
                }
            }

            pPriPwVcEntry =
                L2VpnGetPwVcEntryFromIndex (pL2vpnFsMsPwConfigTable->
                                            u4FsMsPwIndex1);

            if (pPriPwVcEntry == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "pri PW VC entry not found \r\n");
                return CLI_FAILURE;
            }

            pSecPwVcEntry =
                L2VpnGetPwVcEntryFromIndex (pL2vpnFsMsPwConfigTable->
                                            u4FsMsPwIndex2);
            if (pSecPwVcEntry == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "Sec PW VC entry not found \r\n");
                return CLI_FAILURE;
            }

            CliShowMsPwDetail (CliHandle, pL2vpnFsMsPwConfigTable,
                               pPriPwVcEntry, pSecPwVcEntry);
        }                        /*end of while */
    }                            /*end of if u4Flag.. */
    else                        /*diplay only the requested entry */
    {
        L2vpnFsMsPwConfigTable.u4FsMsPwIndex1 = pPriPwVcEntry->u4PwVcIndex;
        L2vpnFsMsPwConfigTable.u4FsMsPwIndex2 = pSecPwVcEntry->u4PwVcIndex;
        pL2vpnFsMsPwConfigTable = L2VpnGetMsPwEntry (pPriPwVcEntry->u4PwVcIndex,
                                                     pSecPwVcEntry->
                                                     u4PwVcIndex);

        if (pL2vpnFsMsPwConfigTable == NULL)
        {
            return CLI_SUCCESS;
        }

        CliShowMsPwDetail (CliHandle, pL2vpnFsMsPwConfigTable,
                           pPriPwVcEntry, pSecPwVcEntry);
    }                            /*end of else.. */

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :CliShowMsPwDetail 
 * Description :This function displays Pseudo wire cross connection information
 *               on the screen
 * Input       :  CliHandle 
 *                pL2vpnSetFsMsPwConfigTable 
 * Output      :  NONE 
 * Returns     : NONE 
 ****************************************************************************/
VOID
CliShowMsPwDetail (tCliHandle CliHandle, tL2vpnFsMsPwConfigTable
                   * pL2vpnFsMsPwConfigTable, tPwVcEntry * pPriPwVcEntry,
                   tPwVcEntry * pSecPwVcEntry)
{
    CHR1                au1String[L2VPN_DEF_BUF_SIZE] = { 0 };

    MEMSET (au1String, '\0', sizeof (au1String));

    switch (pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus)
    {
        case CREATE_AND_WAIT:
            SNPRINTF (au1String, L2VPN_DEF_BUF_SIZE, "%s", "CREATE_AND_WAIT");
            break;

        case ACTIVE:
            SNPRINTF (au1String, L2VPN_DEF_BUF_SIZE, "%s", "ACTIVE");
            break;

        case NOT_IN_SERVICE:
            SNPRINTF (au1String, L2VPN_DEF_BUF_SIZE, "%s", "NOT_IN_SERVICE");
            break;

        case NOT_READY:
            SNPRINTF (au1String, L2VPN_DEF_BUF_SIZE, "%s", "NOT_READY");
            break;
	default:
	    break;
    }                            /*end of switch */

    CliPrintf (CliHandle, "\r\n%-14d", pPriPwVcEntry->u4PwVcID);
    CliPrintf (CliHandle, "%-14d", pSecPwVcEntry->u4PwVcID);
    CliPrintf (CliHandle, "%-14s",
               (pL2vpnFsMsPwConfigTable->i4FsMsPwOperStatus == L2VPN_PWVC_UP) ?
               "UP" : "DOWN");
    CliPrintf (CliHandle, "%-14s\r\n", au1String);
}

/*This function has been written only for compilation 
 * original function will be given by P&S*/

tPwVcEntry         *
L2VpnGetPwVcEntryUsingPwId (UINT4 u4PwVcID)
{
    tPwVcEntry         *pPwVcEntryIn = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntryIn = (tPwVcEntry *) MemAllocMemBlk (L2VPN_PWVC_POOL_ID);

    if (pPwVcEntryIn == NULL)
    {
        return NULL;
    }
    MEMSET (pPwVcEntryIn, 0, sizeof (tPwVcEntry));

    pPwVcEntryIn->u4PwVcID = u4PwVcID;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "L2VpnGetPwVcEntryUsingPwId:"
                "PWID %d", u4PwVcID);

    pPwVcEntry = (tPwVcEntry *) RBTreeGet (L2VPN_MPLS_PWID_TABLE,
                                           (tRBElem *) pPwVcEntryIn);

    if (MemReleaseMemBlock (L2VPN_PWVC_POOL_ID,
                            (UINT1 *) pPwVcEntryIn) == MEM_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Mem release failure - PwVc memory removal\r\n");
    }

    return pPwVcEntry;
}
#endif
