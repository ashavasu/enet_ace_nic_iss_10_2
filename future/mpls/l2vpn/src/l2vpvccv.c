/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpvccv.c,v 1.8 2010/11/26 19:06:42 siva Exp $
 *
 * Description: This file contain the functions related to vccv capability 
 *              negotiation
 *****************************************************************************/

#include "l2vpincs.h"

/*****************************************************************************/
/* Function Name : L2VpnVccvSelectMatchingCCTypes                            */
/* Description   : This routine selects a single Control channel capability  */
/*                 from more than one capability configured                  */
/* Input(s)      : pPwVcEntry - Pointer to Pseudowire Entry                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnVccvSelectMatchingCCTypes (tPwVcEntry * pPwVcEntry)
{
    /* As per RFC 5085, section 7, When multiple capabilities match, only
       one CC Type MUST be used.
       For cases where multiple CC Types are advertised, the following
       precedence rules apply when choosing the single CC Type to use:
       1.  Type 1: PWE3 Control Word with 0001b as first nibble
       2.  Type 2: MPLS Router Alert Label
       3.  Type 3: MPLS PW Label with TTL == 1
     */

    /* If Local & Remote CC are not configured, return SUCCESS */
    if ((pPwVcEntry->u1LocalCcAdvert == L2VPN_VCCV_CC_NONE) ||
        (pPwVcEntry->u1RemoteCcAdvert == L2VPN_VCCV_CC_NONE))
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Local/Remote CC capability"
                   "is not configured\r\n");
        pPwVcEntry->u1LocalCcSelected = L2VPN_VCCV_CC_NONE;
    }
    else if ((pPwVcEntry->u1LocalCcAdvert & L2VPN_VCCV_CC_ACH) &&
             (pPwVcEntry->u1RemoteCcAdvert & L2VPN_VCCV_CC_ACH))
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CC capability: "
                   "PW-ACH\r\n");
        pPwVcEntry->u1LocalCcSelected = L2VPN_VCCV_CC_ACH;
    }
    else if ((pPwVcEntry->u1LocalCcAdvert & L2VPN_VCCV_CC_RAL) &&
             (pPwVcEntry->u1RemoteCcAdvert & L2VPN_VCCV_CC_RAL))
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CC capability: "
                   "RAL\r\n");
        pPwVcEntry->u1LocalCcSelected = L2VPN_VCCV_CC_RAL;
    }
    else if ((pPwVcEntry->u1LocalCcAdvert & L2VPN_VCCV_CC_TTL_EXP) &&
             (pPwVcEntry->u1RemoteCcAdvert & L2VPN_VCCV_CC_TTL_EXP))
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CC capability: "
                   "PW Label with TTL=1\r\n");
        pPwVcEntry->u1LocalCcSelected = L2VPN_VCCV_CC_TTL_EXP;
    }
    return;
}

/*****************************************************************************/
/* Function Name : L2VpnVccvSelectMatchingCVTypes                            */
/* Description   : This routine selects a single Connectivity Verification   */
/*                 capability from mulitple advertised capabilities          */
/* Input(s)      : pPwVcEntry - Pointer to PW entry                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnVccvSelectMatchingCVTypes (tPwVcEntry * pPwVcEntry)
{
    /* Multiple CV type is allowed. i.e., when both BFD & LSP Ping
       are configured, both are allowed. 
       Among 4 possible BFD CV Types, only one CV Type is allowed.

       As per RFC 5885, 
       If the two ends have more than one BFD CV Type in common, the following 
       list of BFD CV Types is considered in the order of the lowest list 
       number CV Type to the highest list number CV Type, and the CV Type 
       with the lowest list number is used:
       1.  0x20 - BFD PW-ACH-encapsulated (without IP/UDP headers), for PW
       Fault Detection and AC/PW Fault Status Signaling
       2.  0x10 - BFD PW-ACH-encapsulated (without IP/UDP headers), for PW
       Fault Detection only
       3.  0x08 - BFD IP/UDP-encapsulated, for PW Fault Detection and AC/PW
       Fault Status Signaling
       4.  0x04 - BFD IP/UDP-encapsulated, for PW Fault Detection only
     */
    /* Reset the CV type already selected */
    pPwVcEntry->u1LocalCvSelected = L2VPN_ZERO;

    if ((pPwVcEntry->u1LocalCvAdvert == L2VPN_VCCV_CV_NONE) ||
        (pPwVcEntry->u1RemoteCvAdvert == L2VPN_VCCV_CV_NONE))
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Local/Remote CC "
                   "capability is not configured\r\n");
        return;
    }

    if ((pPwVcEntry->u1LocalCvAdvert & L2VPN_VCCV_CV_LSPP) &&
        (pPwVcEntry->u1RemoteCvAdvert & L2VPN_VCCV_CV_LSPP))
    {
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC,
                   "Selected CV capability: LSP Ping\r\n");
        pPwVcEntry->u1LocalCvSelected |= L2VPN_VCCV_CV_LSPP;
    }

    if ((pPwVcEntry->u1LocalCvAdvert & L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS) &&
        (pPwVcEntry->u1RemoteCvAdvert & L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS))
    {
        pPwVcEntry->u1LocalCvSelected |= L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS;
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CV capability: "
                   "BFD ACH-encap: PW Fault Detection, PW/AC Fault Status Signaling\r\n");
    }
    else if ((pPwVcEntry->u1LocalCvAdvert & L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY) &&
             (pPwVcEntry->u1RemoteCvAdvert & L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY))
    {
        pPwVcEntry->u1LocalCvSelected |= L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY;
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CV capability: "
                   "BFD ACH-encap: PW Fault Detection only\r\n");
    }
    else if ((pPwVcEntry->u1LocalCvAdvert & L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS)
             && (pPwVcEntry->
                 u1RemoteCvAdvert & L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS))
    {
        pPwVcEntry->u1LocalCvSelected |= L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS;
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CV capability: "
                   "BFD IP/UDP-encap: PW Fault Detection, PW/AC Fault Status Signaling\r\n");
    }
    else if ((pPwVcEntry->u1LocalCvAdvert & L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY) &&
             (pPwVcEntry->u1RemoteCvAdvert & L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY))
    {
        pPwVcEntry->u1LocalCvSelected |= L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY;
        L2VPN_DBG (L2VPN_DBG_VCCV_CAPAB_EXG_TRC, "Selected CV capability: "
                   "BFD IP/UDP-encap: PW Fault Detection only\r\n");
    }

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnVccvValidateCC                                       */
/* Description   : This routine validate the allowed CC types                */
/* Input(s)      : u1CcType - CC type                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnVccvValidateCC (UINT1 u1CcType)
{
    UINT1               u1AllCcType =
        (UINT1) (L2VPN_VCCV_CC_ACH + L2VPN_VCCV_CC_RAL + L2VPN_VCCV_CC_TTL_EXP);

    /* Allow the zero value also */
    if (u1CcType == L2VPN_VCCV_CC_NONE)
    {
        return L2VPN_SUCCESS;
    }

    /* If none of the expected bits are set, return failure */
    if (u1CcType & ~(u1AllCcType))
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnVccvValidateCV                                       */
/* Description   : This routine validate the allowed CV types                */
/* Input(s)      : u1CvType - CV type                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnVccvValidateCV (UINT1 u1CvType)
{
    UINT1               u1AllCvType =
        (UINT1) (L2VPN_VCCV_CV_ICMPP + L2VPN_VCCV_CV_LSPP +
                 L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY +
                 L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS +
                 L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY +
                 L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS);

    /* Allow the zero value also */
    if (u1CvType == L2VPN_VCCV_CV_NONE)
    {
        return L2VPN_SUCCESS;
    }
    /* If none of the expected bits are set, return failure */
    if (u1CvType & ~(u1AllCvType))
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnVccvValidateCapabAdvert                              */
/* Description   : This routine validate the allowed capability              */
/*                 advertisement                                             */
/* Input(s)      : u1CapabAdvert - capability advertisement type             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS/L2VPN_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnVccvValidateCapabAdvert (UINT1 u1CapabAdvert)
{
    UINT1               u1AllCapabAdvert =
        (UINT1) (L2VPN_PW_STATUS_INDICATION + L2VPN_PW_VCCV_CAPABLE);

    /* Allow the zero value also */
    if (u1CapabAdvert == 0)
    {
        return L2VPN_SUCCESS;
    }

    /* If none of the expected bits are set, return failure */
    if (u1CapabAdvert & ~(u1AllCapabAdvert))
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnVccvReverseValue                                     */
/* Description   : This routine reverses the bits of a byte                  */
/* Input(s)      : u1Value - Value to be reversed                            */
/* Output(s)     : None                                                      */
/* Return(s)     : Reversed Value                                            */
/*****************************************************************************/
UINT1
L2VpnVccvReverseValue (UINT1 u1Value)
{
    UINT1               u1RevValue = 0;
    UINT1               u1Itr = 0;

    for (; u1Itr < BITS_PER_BYTE; u1Itr++)
    {
        u1RevValue = (UINT1) ((u1RevValue << 1) + (u1Value & 1));
        u1Value >>= 1;
    }
    return u1RevValue;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file l2vpvccv.c                      */
/*-----------------------------------------------------------------------*/
