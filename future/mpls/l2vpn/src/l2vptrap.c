/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vptrap.c,v 1.8 2013/07/09 13:05:02 siva Exp $
 *
 * Description: This file contains the functions for sending notifications.
 *****************************************************************************/
#ifndef _L2VPTRAP_C_
#define _L2VPTRAP_C_

#ifdef SNMP_3_WANTED

#include "l2vpincs.h"
#include "fssnmp.h"
#include "snmputil.h"

PRIVATE tSNMP_OID_TYPE *L2VpnMakeObjIdFrmDotNew (INT1 *pi1TextStr);

/******************************************************************************
* Function :   L2VpnMakeObjIdFrmDotNew
*
* Description: This Function retuns the OID  of the given string for the
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/
tSNMP_OID_TYPE     *
L2VpnMakeObjIdFrmDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT2               u2Index = 0;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    INT1                ai1TempBuffer[SNMP_MAX_OID_LENGTH + 1];
    INT1                ai1FsMplsPwIndex[] = "1.3.6.1.4.1.2076.13.10.0.2";
    INT1                ai1FsMplsPwNotifStatusStr[] =
        "1.3.6.1.4.1.2076.13.10.0.1";
    INT1                ai1FsMplsPwNotifications[] = "1.3.6.1.4.1.2076.13.10.0";

    /* see if there is an alpha descriptor at begining */
    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));
    if (!pi1TextStr)
    {
        return NULL;
    }
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');
        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) &&
                           (u2Index < SNMP_MAX_OID_LENGTH)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY (ai1TempBuffer, pi1TextStr,
                 MPLS_STRLEN_MIN (ai1TempBuffer, pi1TextStr));
        ai1TempBuffer[MPLS_STRLEN_MIN (ai1TempBuffer, pi1TextStr)] = '\0';
    }

    if (STRCMP (ai1TempBuffer, "fsMplsPwIndex") == 0)
    {
        pOid = SNMP_AGT_GetOidFromString (ai1FsMplsPwIndex);
    }
    else if (STRCMP (ai1TempBuffer, "fsMplsPwNotifStatusStr") == 0)
    {
        pOid = SNMP_AGT_GetOidFromString (ai1FsMplsPwNotifStatusStr);
    }
    else if (STRCMP (ai1TempBuffer, "fsMplsNotifications") == 0)
    {
        pOid = SNMP_AGT_GetOidFromString (ai1FsMplsPwNotifications);
    }
    return pOid;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2vpnSnmpPwSendTrap                                        */
/*                                                                           */
/* Description  : Routine to send trap to SNMP for PwOamStatus and PwStatus  */
/*                 change.                                                   */
/*                                                                           */
/* Input        : u4MplsPwIndex  - Pseudowire index.                         */
/*                u4TrapType     - Trap type                                 */
/*                u1TrapStatus   - Status Value.                             */
/*                                 if Trap Type is L2VPN_PW_STATUS_TRAP,     */
/*                                 this value is L2VPN_PWVC_UP or            */
/*                                 L2VPN_PWVC_UP                             */
/*                                 if Trap Type is L2VPN_OAM_STATUS_TRAP,    */
/*                                 this value is L2VPN_TRUE or L2VPN_FALSE   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
L2vpnSnmpPwSendTrap (UINT4 u4MplsPwIndex, UINT4 u4TrapType, UINT1 u1TrapStatus)
{
#define PW_SYSLOG_MSG_LEN 256
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pSnmpOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
    CHR1                ac1PwSysLogMsg[PW_SYSLOG_MSG_LEN];
    UINT4               u4OidLen = 0;
    UINT1               au1NotifStr[L2VPN_NOTIF_STR_SIZE];

    MEMSET (au1NotifStr, 0, L2VPN_NOTIF_STR_SIZE);
    MEMSET (au1Buf, 0, sizeof (SNMP_MAX_OID_LENGTH));
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    MEMSET (ac1PwSysLogMsg, 0, sizeof (PW_SYSLOG_MSG_LEN));

    if (gL2VpnGlobalInfo.b1PwStatusNotif == MPLS_SNMP_FALSE)
    {
        return;
    }

    if (u4TrapType == L2VPN_PW_STATUS_TRAP)
    {
        if (u1TrapStatus == L2VPN_PWVC_UP)
        {
            SNPRINTF ((char *) au1NotifStr, sizeof (au1NotifStr),
                      "PseudowireStatusUp");
        }
        else if (u1TrapStatus == L2VPN_PWVC_DOWN)
        {
            SNPRINTF ((char *) au1NotifStr, sizeof (au1NotifStr),
                      "PseudowireStatusDown");
        }
        else
        {
            return;
        }
    }
    else if (u4TrapType == L2VPN_OAM_STATUS_TRAP)
    {
        if (u1TrapStatus == L2VPN_TRUE)
        {
            SNPRINTF ((char *) au1NotifStr, sizeof (au1NotifStr),
                      "PseudowireOAMStatusEnabled");
        }
        else
        {
            SNPRINTF ((char *) au1NotifStr, sizeof (au1NotifStr),
                      "PseudowireOAMStatusDisabled");
        }
    }

    SNPRINTF (ac1PwSysLogMsg, sizeof (ac1PwSysLogMsg),
              "PwIndex: %d;  Status: %s ", u4MplsPwIndex, au1NotifStr);
    u4OidLen = (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    pSnmpOid = alloc_oid ((INT4) u4OidLen);
    if (pSnmpOid == NULL)
    {
        return;
    }
    MEMCPY (pSnmpOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpOid->u4_Length = u4OidLen;

    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsNotifications");
    pEnterpriseOid = L2VpnMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        return;
    }
    /* Defect type 1. PW status (4) 2. OAM status(3) */
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4TrapType;

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpOid);
        return;
    }
    pStartVb = pVbList;

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "%s", "fsMplsPwIndex");
    pOid = L2VpnMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "L2vpnSnmpPwSendTrap: Oid not found\r\n");
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    /* For the SNMP Variable Binding of OID and associated Value
     * for pwIndex */
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_UNSIGNED32,
                                                 u4MplsPwIndex, 0,
                                                 NULL, NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "L2vpnSnmpPwSendTrap: Varbind list is null\r\n");
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    pOstring =
        SNMP_AGT_FormOctetString (au1NotifStr, (INT4) STRLEN (au1NotifStr));

    if (pOstring == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "L2vpnSnmpPwSendTrap: Octet string formation failed\r\n");
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    SNPRINTF ((CHR1 *) au1Buf, sizeof (au1Buf), "%s", "fsMplsPwNotifStatusStr");
    pOid = L2VpnMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "L2vpnSnmpPwSendTrap: Oid not found\r\n");
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    /* For the SNMP Variable Binding of OID and associated Value
     * for pwIndex */
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0, 0, pOstring, NULL,
                                                 SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,
                   "L2vpnSnmpPwSendTrap: Varbind list is null\r\n");
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    L2vpnPortSnmpSendTrap (pStartVb, ac1PwSysLogMsg);

    return;
}

#endif /* SNMP_3_WANTED */
#endif /* _L2VPTRAP_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  l2vptrap.c                     */
/*-----------------------------------------------------------------------*/
