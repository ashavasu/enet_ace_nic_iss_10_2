/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpbgp.c,v 1.9 2017/06/15 13:38:13 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *****************************************************************************
 *    FILE  NAME             : l2vpbgp.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the routines to process the
 *                             events from the BGP module.
 *
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"
/*****************************************************************************/
/* Function Name : L2VpnProcessBgpAdvEvent                                   */
/* Description   : This routine processes Advertisement Event from BGP module*/
/* Input(s)      : pL2VpnBgpEvtInfo - ptr to Adv message                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpAdvEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4VplsVeId = L2VPN_ZERO;
    UINT4               u4PwIndex = L2VPN_ZERO;
    UINT4               u4RemoteLabel = L2VPN_INVALID_LABEL;
    UINT4               u4LocalLabel = L2VPN_INVALID_LABEL;
    UINT4               u4LabelBase = L2VPN_ZERO;
    UINT4               u4VBO = L2VPN_ZERO;
    INT4                i4RetStatus = L2VPN_FAILURE;
    tVPLSEntry         *pVplsEntry = NULL;
    tVPLSLBInfo        *pVPLSLBInfoBase = NULL;
    BOOL1               bIsPwTxExists = L2VPN_FALSE;
    BOOL1               bIsPwRxExists = L2VPN_FALSE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    u4VplsIndex = L2VPN_BGP_VPLS_VPLSINDEX (pL2VpnBgpEvtInfo);

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not exists\n",
                    __func__);
        return;
    }

    if (L2VPN_VPLS_OPER_STAT_UP != L2VPN_VPLS_OPER_STATUS (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not up\n",
                    __func__);
        return;
    }

    if (((UINT2) L2VPN_VPLS_MTU (pVplsEntry) !=
         L2VPN_BGP_VPLS_MTU (pL2VpnBgpEvtInfo)) ||
        (L2VPN_VPLS_CONTROL_WORD (pVplsEntry) !=
         L2VPN_BGP_VPLS_CONTROL_FLAG (pL2VpnBgpEvtInfo)))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Signalling parameter mismatch\n", __func__);
        return;
    }

    i4RetStatus = L2VpnGetPwIndexFromRemoteVPLSVe (u4VplsIndex,
                                                   L2VPN_BGP_VPLS_VE_ID
                                                   (pL2VpnBgpEvtInfo),
                                                   &u4PwIndex);
    if (L2VPN_SUCCESS == i4RetStatus)
    {
        if (SNMP_SUCCESS == nmhGetPwInboundLabel (u4PwIndex, &u4LocalLabel))
        {
            if (L2VPN_INVALID_LABEL != u4LocalLabel)
            {
                bIsPwRxExists = L2VPN_TRUE;
            }
        }
        if (SNMP_SUCCESS == nmhGetPwOutboundLabel (u4PwIndex, &u4RemoteLabel))
        {
            if (L2VPN_INVALID_LABEL != u4RemoteLabel)
            {
                bIsPwTxExists = L2VPN_TRUE;
            }
        }
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (u4VplsIndex, &u4VplsVeId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Local VeId Not cofigured for this VPLS\n", __func__);
        return;
    }

    GenU4Addr.u2AddrType =
        (UINT2) L2VPN_BGP_VPLS_IP_ADDR_TYPE (pL2VpnBgpEvtInfo);
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == L2VPN_BGP_VPLS_IP_ADDR_TYPE (pL2VpnBgpEvtInfo))
    {
        MEMCPY (GenU4Addr.Addr.Ip6Addr.u1_addr,
                L2VPN_BGP_VPLS_IPV6_ADDR (pL2VpnBgpEvtInfo), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE ==
             L2VPN_BGP_VPLS_IP_ADDR_TYPE (pL2VpnBgpEvtInfo))
#endif
    {
        GenU4Addr.Addr.u4Addr = L2VPN_BGP_VPLS_IP_ADDR (pL2VpnBgpEvtInfo);
    }
#ifdef MPLS_IPV6_WANTED
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Invalid address type\n", __func__);
        return;
    }
#endif

    if (L2VPN_FALSE == bIsPwTxExists)
    {
        if (((UINT2) u4VplsVeId >= L2VPN_BGP_VPLS_VBO (pL2VpnBgpEvtInfo)) &&
            ((UINT2) u4VplsVeId <= (L2VPN_BGP_VPLS_VBO (pL2VpnBgpEvtInfo) +
                                    L2VPN_BGP_VPLS_VBS (pL2VpnBgpEvtInfo) - 1)))
        {
            u4RemoteLabel = L2VPN_BGP_VPLS_LB (pL2VpnBgpEvtInfo) +
                (u4VplsVeId - L2VPN_BGP_VPLS_VBO (pL2VpnBgpEvtInfo));
            i4RetStatus = L2VpnVplsNeighborCreate (u4VplsIndex,
                                                   L2VPN_ZERO,
                                                   u4RemoteLabel,
                                                   GenU4Addr,
                                                   u4VplsVeId,
                                                   L2VPN_BGP_VPLS_VE_ID
                                                   (pL2VpnBgpEvtInfo),
                                                   L2VPN_VPLS_MTU (pVplsEntry),
                                                   L2VPN_VPLS_CONTROL_WORD
                                                   (pVplsEntry));
            if (L2VPN_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : PW Creation failed in Tx Direction\n",
                            __func__);
            }
        }
        else
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Local VE-id is not in remote VBO-VBS range\n",
                        __func__);
        }
    }

    if (L2VPN_FALSE == bIsPwRxExists)
    {
        u4VBO = (L2VPN_BGP_VPLS_VE_ID (pL2VpnBgpEvtInfo) /
                 L2VPN_VPLS_VBS_DEFAULT_VALUE);
        if (L2VPN_ZERO == (L2VPN_BGP_VPLS_VE_ID (pL2VpnBgpEvtInfo) %
                           L2VPN_VPLS_VBS_DEFAULT_VALUE))
        {
            u4VBO = u4VBO - 1;
        }

        i4RetStatus = L2VpnSendBgpAdvEvent (u4VplsIndex, u4VBO);
        if (L2VPN_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Send Adv msg failed\n", __func__);
            return;
        }

        pVPLSLBInfoBase = L2VPN_VPLS_LABEL_BLOCK (pVplsEntry);
        if (L2VPN_INVALID_LABEL == u4LocalLabel)
        {
            u4LabelBase = pVPLSLBInfoBase[u4VBO].u4LabelBlock;

            u4LocalLabel =
                u4LabelBase +
                (L2VPN_BGP_VPLS_VE_ID (pL2VpnBgpEvtInfo) %
                 L2VPN_VPLS_VBS_DEFAULT_VALUE) - 1;
            if (L2VPN_ZERO ==
                (L2VPN_BGP_VPLS_VE_ID (pL2VpnBgpEvtInfo) %
                 L2VPN_VPLS_VBS_DEFAULT_VALUE))
            {
                u4LocalLabel = u4LocalLabel + L2VPN_VPLS_VBS_DEFAULT_VALUE;
            }
        }
        i4RetStatus = L2VpnVplsNeighborCreate (u4VplsIndex,
                                               u4LocalLabel,
                                               L2VPN_ZERO,
                                               GenU4Addr,
                                               u4VplsVeId,
                                               L2VPN_BGP_VPLS_VE_ID
                                               (pL2VpnBgpEvtInfo),
                                               L2VPN_VPLS_MTU (pVplsEntry),
                                               L2VPN_VPLS_CONTROL_WORD
                                               (pVplsEntry));
        if (L2VPN_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : PW Creation failed in Rx Direction\n", __func__);
            return;
        }

        pVPLSLBInfoBase[u4VBO].u4ReferenceCount++;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessBgpWithdrawEvent                              */
/* Description   : This routine processes Withdraw Event from BGP module.    */
/* Input(s)      : pL2VpnBgpEvtInfo - ptr to Withdraw message                */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpWithdrawEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo)
{
    UINT4               u4VplsIndex = L2VPN_ZERO;
    UINT4               u4VplsVeId = L2VPN_ZERO;
    UINT4               u4PwIndex = L2VPN_ZERO;
    UINT4               u4VBO = L2VPN_ZERO;
    INT4                i4RetStatus = L2VPN_FAILURE;
    tVPLSEntry         *pVplsEntry = NULL;
    tVPLSLBInfo        *pVPLSLBInfoBase = NULL;
    UINT1               au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1               au1RouteTarget[L2VPN_MAX_VPLS_RT_LEN];
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    u4VplsIndex = L2VPN_BGP_VPLS_VPLSINDEX (pL2VpnBgpEvtInfo);

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not exists\n",
                    __func__);
        return;
    }

    if (L2VPN_VPLS_OPER_STAT_UP != L2VPN_VPLS_OPER_STATUS (pVplsEntry))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not up\n",
                    __func__);
        return;
    }

    i4RetStatus = L2VpnGetPwIndexFromRemoteVPLSVe (u4VplsIndex,
                                                   L2VPN_BGP_VPLS_VE_ID
                                                   (pL2VpnBgpEvtInfo),
                                                   &u4PwIndex);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : No PW exists with this VEId to delete\n", __func__);
        return;
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (u4VplsIndex, &u4VplsVeId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Local VeId Not cofigured for this VPLS\n", __func__);
        return;
    }

    if (((UINT2) u4VplsVeId >= L2VPN_BGP_VPLS_VBO (pL2VpnBgpEvtInfo)) &&
        ((UINT2) u4VplsVeId < (L2VPN_BGP_VPLS_VBO (pL2VpnBgpEvtInfo) +
                               L2VPN_BGP_VPLS_VBS (pL2VpnBgpEvtInfo) - 1)))
    {
        i4RetStatus = L2VpnVplsPwDestroy (u4PwIndex);
        if (L2VPN_FAILURE == i4RetStatus)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : PW Delete failed\n", __func__);
            return;
        }

        u4VBO = (L2VPN_BGP_VPLS_VE_ID (pL2VpnBgpEvtInfo) /
                 L2VPN_VPLS_VBS_DEFAULT_VALUE);
        if (L2VPN_ZERO == (L2VPN_BGP_VPLS_VE_ID (pL2VpnBgpEvtInfo) %
                           L2VPN_VPLS_VBS_DEFAULT_VALUE))
        {
            u4VBO = u4VBO - 1;
        }

        pVPLSLBInfoBase = L2VPN_VPLS_LABEL_BLOCK (pVplsEntry);

        pVPLSLBInfoBase[u4VBO].u4ReferenceCount--;
        if (u4VBO != 0 && pVPLSLBInfoBase[u4VBO].u4ReferenceCount == 0)
        {
            (VOID) L2VpnGetRdValue (u4VplsIndex, au1RouteDistinguisher);

            (VOID) L2VpnGetExportRtValue (u4VplsIndex,
                                          L2VPN_TRUE, au1RouteTarget);

            i4RetStatus = L2VpnSendBgpWithdrawEvent (u4VplsIndex,
                                                     u4VplsVeId,
                                                     L2VPN_VPLS_MTU
                                                     (pVplsEntry), u4VBO,
                                                     pVPLSLBInfoBase[u4VBO].
                                                     u4LabelBlock,
                                                     au1RouteDistinguisher,
                                                     au1RouteTarget,
                                                     L2VPN_VPLS_CONTROL_WORD
                                                     (pVplsEntry));

            i4RetStatus = L2VpnReleaseLb (pVPLSLBInfoBase[u4VBO].u4LabelBlock);
            if (L2VPN_FAILURE == i4RetStatus)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Failed to release label block\n", __func__);
            }
            pVPLSLBInfoBase[u4VBO].u4LabelBlock = L2VPN_INVALID_LABEL;
        }

    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Local VE-id is not in remote VBO-VBS range\n",
                    __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpAdvEvent                                      */
/* Description   : This routine sends Advertisement Event to BGP module.     */
/* Input(s)      : u4VplsInstance - Vpls Index value                         */
/*                 u4VBO - BBO value for which Advt. msg to be sent.         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendBgpAdvEvent (UINT4 u4VplsInstance, UINT4 u4VBO)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    tVPLSLBInfo        *pVPLSLBInfoBase = NULL;
    UINT4               u4LabelBase = L2VPN_ZERO;
    UINT4               u4LocalVeId = L2VPN_ZERO;
    INT4                i4RetStatus;
    UINT1               au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1];
    UINT4               u4BgpAdminState;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    VplsName.pu1_OctetList = au1VplsName;

    if (SNMP_FAILURE == nmhGetFsMplsVplsName (u4VplsInstance, &VplsName))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to get vpls name from vpls index.\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    i4RetStatus = L2VpnGetExportRtValue (u4VplsInstance,
                                         L2VPN_TRUE,
                                         BgpL2VpnTxMsgInfo.au1RouteTarget);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return L2VPN_FAILURE;
    }

    i4RetStatus = L2VpnGetRdValue (u4VplsInstance,
                                   BgpL2VpnTxMsgInfo.au1RouteDistinguisher);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return L2VPN_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsInstance);

    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to get vpls Entry\n", __func__);
        return L2VPN_FAILURE;
    }
    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (u4VplsInstance, &u4LocalVeId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return L2VPN_FAILURE;
    }

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    pVPLSLBInfoBase = L2VPN_VPLS_LABEL_BLOCK (pVplsEntry);

    u4LabelBase = pVPLSLBInfoBase[u4VBO].u4LabelBlock;
    if (L2VPN_INVALID_LABEL == u4LabelBase)
    {
        i4RetStatus = L2VpnAllocateLb (&u4LabelBase);
        if (L2VPN_SUCCESS == i4RetStatus)
        {
            pVPLSLBInfoBase[u4VBO].u4LabelBlock = u4LabelBase;
        }
        else
        {
            return L2VPN_FAILURE;
        }
    }
    else
    {
        return L2VPN_SUCCESS;
    }

    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsInstance;
    MEMCPY (BgpL2VpnTxMsgInfo.au1VplsName,
            VplsName.pu1_OctetList, VplsName.i4_Length);
    BgpL2VpnTxMsgInfo.u4LabelBase = u4LabelBase;
    BgpL2VpnTxMsgInfo.u2VeBaseOffset =
        (UINT2) ((L2VPN_VPLS_VBS_DEFAULT_VALUE * u4VBO) + 1);
    BgpL2VpnTxMsgInfo.u2VeBaseSize = L2VPN_VPLS_VBS_DEFAULT_VALUE;
    BgpL2VpnTxMsgInfo.u2Mtu = (UINT2) L2VPN_VPLS_MTU (pVplsEntry);
    BgpL2VpnTxMsgInfo.u2VeId = (UINT2) u4LocalVeId;
    BgpL2VpnTxMsgInfo.bControlWordFlag = L2VPN_VPLS_CONTROL_WORD (pVplsEntry);

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return L2VPN_FAILURE;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpWithdrawEvent                                 */
/* Description   : This routine sends Withdraw Event to BGP module.          */
/* Input(s)      : u4VplsInstance - Vpls Index value                         */
/*                 u4VplsLocalVEId - local VeId to fill in NLRI              */
/*                 u4VplsMtu - local MTU to fill in NLRI                     */
/*                 u4VBO - VBO value                                         */
/*                 u4LabelBase - LB value                                    */
/*                 pu1RouteDistinguisher - RD to fill in NLRI                */
/*                 pu1RouteTarget - RT to fill in NLRI                       */
/*                 bControlWordFlag - Control Word to fill in NLRI           */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendBgpWithdrawEvent (UINT4 u4VplsInstance,
                           UINT4 u4VplsLocalVEId,
                           UINT4 u4VplsMtu,
                           UINT4 u4VBO,
                           UINT4 u4LabelBase,
                           UINT1 *pu1RouteDistinguisher,
                           UINT1 *pu1RouteTarget, BOOL1 bControlWordFlag)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    UINT1               au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1];
    UINT4               u4BgpAdminState;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    VplsName.pu1_OctetList = au1VplsName;

    if (SNMP_FAILURE == nmhGetFsMplsVplsName (u4VplsInstance, &VplsName))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to get vpls name from vpls index.\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return L2VPN_FAILURE;
    }

    MEMCPY (BgpL2VpnTxMsgInfo.au1RouteTarget,
            pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN);

    MEMCPY (BgpL2VpnTxMsgInfo.au1RouteDistinguisher,
            pu1RouteDistinguisher, L2VPN_MAX_VPLS_RD_LEN);

    BgpL2VpnTxMsgInfo.u2VeId = (UINT2) u4VplsLocalVEId;
    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsInstance;
    MEMCPY (BgpL2VpnTxMsgInfo.au1VplsName,
            VplsName.pu1_OctetList, VplsName.i4_Length);
    BgpL2VpnTxMsgInfo.u4LabelBase = u4LabelBase;
    BgpL2VpnTxMsgInfo.u2VeBaseOffset =
        (UINT2) ((L2VPN_VPLS_VBS_DEFAULT_VALUE * u4VBO) + 1);
    BgpL2VpnTxMsgInfo.u2VeBaseSize = L2VPN_VPLS_VBS_DEFAULT_VALUE;
    BgpL2VpnTxMsgInfo.u2Mtu = (UINT2) u4VplsMtu;
    BgpL2VpnTxMsgInfo.bControlWordFlag = bControlWordFlag;

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpRtAddEvent                                    */
/* Description   : This routine sends RT Add Event to BGP module.            */
/* Input(s)      : u4VplsIndex - Vpls Index value                            */
/*                 pu1RouteTarget - RT to fill in NLRI                       */
/*                 u1RTType - RT Type(Import/Export/Both)                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpRtAddEvent (UINT4 u4VplsIndex,
                        UINT1 *pu1RouteTarget, UINT1 u1RTType)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    UINT4               u4BgpAdminState;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    MEMCPY (BgpL2VpnTxMsgInfo.au1RouteTarget,
            pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN);

    if (L2VPN_VPLS_RT_IMPORT == u1RTType)
    {
        BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    }
    else if (L2VPN_VPLS_RT_EXPORT == u1RTType)
    {
        BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_EXPORT_RT_ADD;
    }
    else if (L2VPN_VPLS_RT_BOTH == u1RTType)
    {
        BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_BOTH_RT_ADD;
    }
    else
    {
        return;
    }

    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsIndex;

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpRtDeleteEvent                                 */
/* Description   : This routine sends RT Delete Event to BGP module.         */
/* Input(s)      : u4VplsIndex - Vpls Index value                            */
/*                 pu1RouteTarget - RT to fill in NLRI                       */
/*                 u1RTType - RT Type(Import/Export/Both)                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpRtDeleteEvent (UINT4 u4VplsIndex,
                           UINT1 *pu1RouteTarget, UINT1 u1RTType)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    UINT4               u4BgpAdminState;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    MEMCPY (BgpL2VpnTxMsgInfo.au1RouteTarget,
            pu1RouteTarget, L2VPN_MAX_VPLS_RT_LEN);

    if (L2VPN_VPLS_RT_IMPORT == u1RTType)
    {
        BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_DELETE;
    }
    else if (L2VPN_VPLS_RT_EXPORT == u1RTType)
    {
        BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_EXPORT_RT_DELETE;
    }
    else if (L2VPN_VPLS_RT_BOTH == u1RTType)
    {
        BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_BOTH_RT_DELETE;
    }
    else
    {
        return;
    }

    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsIndex;

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnProcessBgpUpEvent                                    */
/* Description   : This routine processes BGP up Event from BGP module.      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpUpEvent (VOID)
{
    tVPLSEntry         *pVplsEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsEntry = (tVPLSEntry *)
        RBTreeGetFirst (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsEntry == NULL)
    {
        return;
    }

    do
    {
        if (L2VPN_VPLS_OPER_STAT_UP == L2VPN_VPLS_OPER_STATUS (pVplsEntry))
        {
            continue;
        }

        L2VpnSendVplsUpAdminEvent (L2VPN_VPLS_INDEX (pVplsEntry));
    }
    while ((pVplsEntry = (tVPLSEntry *)
            RBTreeGetNext (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo),
                           pVplsEntry, NULL)) != NULL);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnProcessBgpDownEvent                                  */
/* Description   : This routine processes BGP down Event from BGP module.    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpDownEvent (VOID)
{
    tVPLSEntry         *pVplsEntry = NULL;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsEntry = (tVPLSEntry *)
        RBTreeGetFirst (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsEntry == NULL)
    {
        return;
    }

    do
    {
        if (L2VPN_VPLS_OPER_STAT_UP != L2VPN_VPLS_OPER_STATUS (pVplsEntry))
        {
            continue;
        }

        L2VpnSendVplsDownAdminEvent (L2VPN_VPLS_INDEX (pVplsEntry));
    }
    while ((pVplsEntry = (tVPLSEntry *)
            RBTreeGetNext (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo),
                           pVplsEntry, NULL)) != NULL);
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpVplsCreateEvent                               */
/* Description   : This routine sends Vpls Create Event to BGP module.       */
/* Input(s)      : u4VplsIndex - vpls index which gets created.              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpVplsCreateEvent (UINT4 u4VplsIndex)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VplsVeId = L2VPN_ZERO;
    INT4                i4RetStatus;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not exists\n",
                    __func__);
        return;
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (u4VplsIndex, &u4VplsVeId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Local VeId Not cofigured for this VPLS\n", __func__);
        return;
    }

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsIndex;
    BgpL2VpnTxMsgInfo.u2Mtu = (UINT2) L2VPN_VPLS_MTU (pVplsEntry);
    BgpL2VpnTxMsgInfo.u2VeId = (UINT2) u4VplsVeId;
    STRNCPY (BgpL2VpnTxMsgInfo.au1VplsName,
             L2VPN_VPLS_NAME (pVplsEntry),
             STRNLEN (L2VPN_VPLS_NAME (pVplsEntry),
                      BGP4_VPLS_MAX_VPLS_NAME_SIZE));
    BgpL2VpnTxMsgInfo.bControlWordFlag = L2VPN_VPLS_CONTROL_WORD (pVplsEntry);

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpVplsDeleteEvent                               */
/* Description   : This routine sends Vpls Delete Event to BGP module.       */
/* Input(s)      : u4VplsIndex - vpls index which gets deleted.              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpVplsDeleteEvent (UINT4 u4VplsIndex)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    UINT4               u4BgpAdminState;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_VPLS_DELETE;
    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsIndex;

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpVplsUpEvent                                   */
/* Description   : This routine sends Vpls Up Event to BGP module.           */
/* Input(s)      : u4VplsIndex - vpls index which comes up.                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpVplsUpEvent (UINT4 u4VplsIndex)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4LocalVeId = L2VPN_ZERO;
    INT4                i4RetStatus;
    UINT4               u4BgpAdminState;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    i4RetStatus = L2VpnGetExportRtValue (u4VplsIndex,
                                         L2VPN_TRUE,
                                         BgpL2VpnTxMsgInfo.au1RouteTarget);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return;
    }

    i4RetStatus = L2VpnGetRdValue (u4VplsIndex,
                                   BgpL2VpnTxMsgInfo.au1RouteDistinguisher);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not exists\n",
                    __func__);
        return;
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (u4VplsIndex, &u4LocalVeId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return;
    }

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_VPLS_UP;
    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsIndex;
    STRNCPY (BgpL2VpnTxMsgInfo.au1VplsName,
             L2VPN_VPLS_NAME (pVplsEntry),
             STRNLEN (L2VPN_VPLS_NAME (pVplsEntry),
                      BGP4_VPLS_MAX_VPLS_NAME_SIZE));
    BgpL2VpnTxMsgInfo.u2Mtu = (UINT2) L2VPN_VPLS_MTU (pVplsEntry);
    BgpL2VpnTxMsgInfo.u2VeId = (UINT2) u4LocalVeId;
    BgpL2VpnTxMsgInfo.bControlWordFlag = L2VPN_VPLS_CONTROL_WORD (pVplsEntry);

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpVplsDownEvent                                 */
/* Description   : This routine sends Vpls Down Event to BGP module.         */
/* Input(s)      : u4VplsIndex - vpls index which gets down.                 */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpVplsDownEvent (UINT4 u4VplsIndex)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    UINT4               u4BgpAdminState;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_VPLS_DOWN;
    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsIndex;

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

#ifdef VPLS_GR_WANTED
/*****************************************************************************/
/* Function Name : L2VpnProcessBgpGREvent                                  */
/* Description   : This routine processes BGP_GR_START Event from BGP module.    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpGREvent (VOID)
{
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VBO = L2VPN_ZERO;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (L2VpnHwListGetFirst (&L2VpnPwHwListEntry) == MPLS_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : No node in HwList\n",
                    __func__);
        return;
    }
    do
    {
        if (L2VPN_ZERO == L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
        {
            continue;
        }
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex
            (L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry));
        if (NULL == pVplsEntry)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : VPLS Index=%u not exists\n", __func__,
                        L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry));
            continue;
        }
        if (L2VPN_VPLS_OPER_STAT_UP != L2VPN_VPLS_OPER_STATUS (pVplsEntry))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : VPLS Index=%u not up\n", __func__,
                        L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry));
            continue;
        }
        if ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[L2VPN_ZERO].u4LabelBlock !=
            L2VPN_INVALID_LABEL)
        {
            if (L2VpnReleaseLb ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))
                                [L2VPN_ZERO].u4LabelBlock) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Failed to release label block\n", __func__);
            }
            (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[L2VPN_ZERO].u4LabelBlock =
                L2VPN_INVALID_LABEL;
        }
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex
            (L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        if (NULL == pPwVcEntry)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Pw Index=%u doesn't exists\n", __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX ((&L2VpnPwHwListEntry)));
            continue;
        }
        L2VPN_PWVC_NPAPI (pPwVcEntry) = L2VPN_FALSE;
        /*u1NpApiCall is set to FALSE that means NPAPI call to delete the 
         *pwVcEntry is not required as it is GR case*/
        if (L2VpnDeletePwVc (pPwVcEntry) != L2VPN_SUCCESS)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Failed to delete PwVcEntry for PwIndex=%u from control plane",
                        __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        }
        u4VBO = (L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry) /
                 L2VPN_VPLS_VBS_DEFAULT_VALUE);
        if (L2VPN_ZERO == (L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry) %
                           L2VPN_VPLS_VBS_DEFAULT_VALUE))
        {
            u4VBO = u4VBO - 1;
        }
        (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4ReferenceCount--;
        if (L2VPN_ZERO ==
            (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4ReferenceCount
            && (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock !=
            L2VPN_INVALID_LABEL)
        {
            if (L2VpnReleaseLb ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].
                                u4LabelBlock) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Failed to release label block\n", __func__);
            }

            (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock =
                L2VPN_INVALID_LABEL;
        }
        MplsL2VpnRelPwVcIndex (L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
    }
    while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
           != MPLS_FAILURE);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnProcessBgpGRInProgressEvent                          */
/* Description   : This routine processes BGP_GR_IN_PROGRESS Event from BGP  */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnProcessBgpGRInProgressEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tVplsAcMapEntry     VplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapNextEntry = NULL;
    UINT4               u4VBO;
    UINT1               u1OperStatus = L2VPN_PWVC_OPER_UP;
    tL2VpnPwHwList      L2VpnPwHwListEntry;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&VplsAcMapEntry, L2VPN_ZERO, sizeof (tVplsAcMapEntry));
    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

    if (L2VpnMarkBgpPwEntriesStale () == L2VPN_FAILURE)
    {
        return;
    }

    if (L2VPN_TRUE == gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : GR is already in progress, So Deleting Stale entries\n",
                    __func__);
        L2VpnBgpDeleteStaleEntries ();
        return;
    }
    gpL2VpnGlobalInfo->BgpGrTimer.u4Event = L2VPN_BGP_GR_TMR_EXP_EVENT;
    if (TmrStartTimer (L2VPN_TIMER_LIST_ID,
                       &gpL2VpnGlobalInfo->BgpGrTimer.AppTimer,
                       L2VPN_BGP_GR_TMR_INTERVAL (pL2VpnBgpEvtInfo) *
                       SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Start of BGP GR timer failed\r\n");
    }
    else
    {
        gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted = L2VPN_TRUE;
    }
    pVplsEntry = (tVPLSEntry *)
        RBTreeGetFirst (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo));

    if (pVplsEntry == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : No Vpls is present\n",
                    __func__);
        return;
    }

    do
    {
        L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcMapEntry) =
            L2VPN_VPLS_INDEX (pVplsEntry);
        pVplsAcMapNextEntry = (tVplsAcMapEntry *)
            RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                           &VplsAcMapEntry, NULL);
        while (NULL != pVplsAcMapNextEntry)
        {
            if (L2VPN_VPLSAC_VPLS_INSTANCE (pVplsAcMapNextEntry) !=
                L2VPN_VPLS_INDEX (pVplsEntry))
            {
                break;
            }
            if (L2VPN_ZERO != L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapNextEntry) &&
                L2VPN_VPLSAC_INVALID_PORT_INDEX !=
                L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapNextEntry))
            {
                if (CfaGetIfOperStatus
                    (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapNextEntry),
                     &u1OperStatus) == CFA_FAILURE)
                {
                    pVplsAcMapNextEntry = (tVplsAcMapEntry *)
                        RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                                       pVplsAcMapNextEntry, NULL);
                    continue;
                }
            }
            if (L2VPN_PWVC_OPER_DOWN == u1OperStatus &&
                L2VPN_VPLS_AC_OPER_STATUS_UP ==
                L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapNextEntry))
            {
                (VOID)
                    L2VpnProcessIfDownEvent (L2VPN_VPLSAC_PORT_INDEX
                                             (pVplsAcMapNextEntry),
                                             (UINT2) (L2VPN_VPLSAC_VLAN_ID
                                                      (pVplsAcMapNextEntry)));
            }
            pVplsAcMapNextEntry = (tVplsAcMapEntry *)
                RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                               pVplsAcMapNextEntry, NULL);
        }

        if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry) &&
            L2VPN_VPLS_OPER_STAT_UP == L2VPN_VPLS_OPER_STATUS (pVplsEntry))
        {
            L2VpnSendBgpVplsCreateEvent (L2VPN_VPLS_INDEX (pVplsEntry));
            L2VpnSendBgpRtsAddEvent (L2VPN_VPLS_INDEX (pVplsEntry));
            L2VpnSendBgpVplsUpEvent (L2VPN_VPLS_INDEX (pVplsEntry));
            L2VpnSendBgpAdvEvent (L2VPN_VPLS_INDEX (pVplsEntry), L2VPN_ZERO);
            L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry) =
                L2VPN_VPLS_INDEX (pVplsEntry);
            L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry) = L2VPN_ZERO;
            while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
                   != MPLS_FAILURE)
            {
                if (L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry) !=
                    L2VPN_VPLS_INDEX (pVplsEntry))
                {
                    break;
                }
                u4VBO = L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry) /
                    L2VPN_VPLS_VBS_DEFAULT_VALUE;
                if (L2VPN_ZERO == (L2VPN_PW_HW_LIST_REMOTE_VE_ID
                                   (&L2VpnPwHwListEntry) %
                                   L2VPN_VPLS_VBS_DEFAULT_VALUE))
                {
                    u4VBO = u4VBO - 1;
                }

                L2VpnSendBgpGrAdvEvent (L2VPN_VPLS_INDEX (pVplsEntry), u4VBO);
            }
        }
    }
    while ((pVplsEntry = (tVPLSEntry *)
            RBTreeGetNext (L2VPN_VPLS_VFI_RB_PTR (gpPwVcGlobalInfo),
                           pVplsEntry, NULL)) != NULL);

    L2VpnSendBgpEoVplsEvent ();
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

}

/*****************************************************************************/
/* Function Name : L2VpnMarkBgpPwEntriesStale                                */
/* Description   : This routine marks BGP Pws Entries as stale              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
L2VpnMarkBgpPwEntriesStale (VOID)
{
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    if (L2VpnHwListGetFirst (&L2VpnPwHwListEntry) == MPLS_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : No node in HwList\n",
                    __func__);
        return L2VPN_FAILURE;
    }
    do
    {
        if (L2VPN_ZERO == L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
        {
            continue;
        }
        /*if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (pL2VpnPwHwListEntry) &
           L2VPN_PWALL_NPAPI_SET) != L2VPN_PWALL_NPAPI_SET)
           {
           if (L2VpnDelStalePwVc (pL2VpnPwHwListEntry, NULL) == L2VPN_FAILURE)
           {
           L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
           "%s : PwVcIndex = %u deletion is failed from Hw\n",
           __func__, L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry)); 
           }
           if (L2VpnDelStalePwIlm (pL2VpnPwHwListEntry, NULL) == L2VPN_FAILURE)
           {
           L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
           "%s : PwIlmIndex = %u deletion is failed from Hw\n",
           __func__, L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry)); 
           }
           } */
        /*Reserve Pw Indices */
        if (MplsL2VpnSetPwVcIndex
            (L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry)) !=
            L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry))
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : PwIndex = %u reservation failed\n", __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        }
        /*Reserve Local Label Base */
        if (L2VpnReserveLabelFromHwList (&L2VpnPwHwListEntry) == L2VPN_FAILURE)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Label reserve failed for PwIndex = %u\n",
                        __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        }
        /*Mark PW Entry as stale */
        L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
            L2VPN_PW_STATUS_STALE;
        L2VPN_NO_OF_BGP_PW_STALE_ENTRIES (gpPwVcGlobalInfo)++;

        L2VpnPwHwListAddUpdate (&L2VpnPwHwListEntry, L2VPN_ZERO);
    }
    while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
           != MPLS_FAILURE);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpRtsAddEvent                                   */
/* Description   : This routine marks BGP Pws Entries as stale              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
L2VpnSendBgpRtsAddEvent (UINT4 u4VplsIndex)
{
    tVPLSRtEntry        VplsRtEntry;
    tVPLSRtEntry       *pVplsRtEntry = NULL;
    UINT1               u1ActiveRtFound = L2VPN_FALSE;
    UINT1               au1VplsBgpRteTargetRT[L2VPN_MAX_VPLS_RT_LEN];

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));
    MEMSET (au1VplsBgpRteTargetRT, L2VPN_ZERO, L2VPN_MAX_VPLS_RT_LEN);

    L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = u4VplsIndex;
    L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = L2VPN_ZERO;

    while ((pVplsRtEntry =
            (tVPLSRtEntry *)
            RBTreeGetNext (L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
                           &VplsRtEntry, NULL)) != NULL)
    {
        L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) =
            L2VPN_VPLSRT_RT_INDEX (pVplsRtEntry);
        if (u4VplsIndex != L2VPN_VPLSRT_VPLS_INSTANCE (pVplsRtEntry))
        {
            break;
        }

        if (ACTIVE != L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry))
        {
            continue;
        }
        L2VpnSendBgpRtAddEvent (u4VplsIndex,
                                L2VPN_VPLSRT_ROUTE_TARGET (pVplsRtEntry),
                                L2VPN_VPLSRT_RT_TYPE (pVplsRtEntry));
        u1ActiveRtFound = L2VPN_TRUE;
    }
    if (L2VPN_FALSE == u1ActiveRtFound)
    {
        if (L2VPN_SUCCESS == L2VpnGetDefaultRtValue (u4VplsIndex,
                                                     au1VplsBgpRteTargetRT))
        {
            L2VpnSendBgpRtAddEvent (u4VplsIndex, au1VplsBgpRteTargetRT,
                                    L2VPN_VPLS_RT_BOTH);
        }
        else
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG,
                        "%s : Unable to get Default RT\n", __func__);
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpGrAdvEvent                                    */
/* Description   : This routine sends Advertisement Event to BGP module in GR*/
/* Input(s)      : u4VplsInstance - Vpls Index value                         */
/*                 u4VBO - BBO value for which Advt. msg to be sent.         */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnSendBgpGrAdvEvent (UINT4 u4VplsInstance, UINT4 u4VBO)
{
    tSNMP_OCTET_STRING_TYPE VplsName;
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    tVPLSEntry         *pVplsEntry = NULL;
    tVPLSLBInfo        *pVPLSLBInfoBase = NULL;
    UINT4               u4LabelBase = L2VPN_ZERO;
    UINT4               u4LocalVeId = L2VPN_ZERO;
    INT4                i4RetStatus;
    UINT1               au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 1];
    UINT4               u4BgpAdminState;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    VplsName.pu1_OctetList = au1VplsName;

    if (SNMP_FAILURE == nmhGetFsMplsVplsName (u4VplsInstance, &VplsName))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to get vpls name from vpls index.\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    i4RetStatus = L2VpnGetExportRtValue (u4VplsInstance,
                                         L2VPN_TRUE,
                                         BgpL2VpnTxMsgInfo.au1RouteTarget);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return L2VPN_FAILURE;
    }

    i4RetStatus = L2VpnGetRdValue (u4VplsInstance,
                                   BgpL2VpnTxMsgInfo.au1RouteDistinguisher);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return L2VPN_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        return L2VPN_FAILURE;
    }

    i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex (u4VplsInstance, &u4LocalVeId);
    if (L2VPN_FAILURE == i4RetStatus)
    {
        return L2VPN_FAILURE;
    }

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    pVPLSLBInfoBase = L2VPN_VPLS_LABEL_BLOCK (pVplsEntry);

    u4LabelBase = pVPLSLBInfoBase[u4VBO].u4LabelBlock;

    BgpL2VpnTxMsgInfo.u4VplsIndex = u4VplsInstance;
    MEMCPY (BgpL2VpnTxMsgInfo.au1VplsName,
            VplsName.pu1_OctetList, VplsName.i4_Length);
    BgpL2VpnTxMsgInfo.u4LabelBase = u4LabelBase;
    BgpL2VpnTxMsgInfo.u2VeBaseOffset =
        (UINT2) ((L2VPN_VPLS_VBS_DEFAULT_VALUE * u4VBO) + 1);
    BgpL2VpnTxMsgInfo.u2VeBaseSize = L2VPN_VPLS_VBS_DEFAULT_VALUE;
    BgpL2VpnTxMsgInfo.u2Mtu = (UINT2) L2VPN_VPLS_MTU (pVplsEntry);
    BgpL2VpnTxMsgInfo.u2VeId = (UINT2) u4LocalVeId;
    BgpL2VpnTxMsgInfo.bControlWordFlag = L2VPN_VPLS_CONTROL_WORD (pVplsEntry);

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return L2VPN_FAILURE;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
        return L2VPN_FAILURE;
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnSendBgpEoVplsEvent                                  */
/* Description   : This routine sends End of Vpls Event to BGP module.       */
/* Input(s)      : None                                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnSendBgpEoVplsEvent (VOID)
{
    tBgp4L2VpnEvtInfo   BgpL2VpnTxMsgInfo;
    UINT4               u4BgpAdminState;

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (&BgpL2VpnTxMsgInfo, 0, sizeof (tBgp4L2VpnEvtInfo));

    BgpL2VpnTxMsgInfo.u4MsgType = BGP_L2VPN_EOVPLS;

    MplsGetBgpAdminState (&u4BgpAdminState);
    if (L2VPN_BGP_ADMIN_DOWN == u4BgpAdminState)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : BGP is down\n", __func__);
        return;
    }

    if (BGP4_SUCCESS != Bgp4L2vpnEventHandler (&BgpL2VpnTxMsgInfo))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Failed to post event to BGP\n", __func__);
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}

/*****************************************************************************/
/* Function Name : L2VpnReserveLabelFromHwList                               */
/* Description   : This routine Reserves all the labels in a labelBase       */
/* Input(s)      : pL2VpnPwHwListEntry - Pointer to hardwae list entry      */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCESS/L2VPN_FAILURE                                */
/*****************************************************************************/
INT4
L2VpnReserveLabelFromHwList (tL2VpnPwHwList * pL2VpnPwHwListEntry)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VBO;
    UINT4               u4LocalLabel;
    UINT4               u4LabelBase;
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (L2VPN_PW_HW_LIST_VPLS_INDEX
                                            (pL2VpnPwHwListEntry));
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : VPLS Index not exists\n",
                    __func__);
        return L2VPN_FAILURE;
    }

    u4VBO = L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry) /
        L2VPN_VPLS_VBS_DEFAULT_VALUE;
    if (L2VPN_ZERO == (L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry)
                       % L2VPN_VPLS_VBS_DEFAULT_VALUE))
    {
        u4VBO = u4VBO - 1;
    }
    if ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)) == NULL)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : Label Base is NULL\n",
                    __func__);
        return L2VPN_FAILURE;
    }
    if ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock ==
        L2VPN_INVALID_LABEL)
    {
        u4LocalLabel = L2VPN_PW_HW_LIST_IN_LABEL (pL2VpnPwHwListEntry);
        u4LabelBase = u4LocalLabel + 1 -
            (L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry)
             % L2VPN_VPLS_VBS_DEFAULT_VALUE);
        if (L2VPN_ZERO ==
            (L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry)
             % L2VPN_VPLS_VBS_DEFAULT_VALUE))
        {
            u4LabelBase = u4LabelBase - L2VPN_VPLS_VBS_DEFAULT_VALUE;
        }
        /* This will set the LabelBlock in vplsEntry */
        (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock = u4LabelBase;
        /*This will resrve the labels in a label block to label mgr */
        if (L2VpnSetLb (u4LabelBase) == L2VPN_FAILURE)
        {
            return L2VPN_FAILURE;
        }
    }
    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnBgpDeleteStaleEntries                                */
/* Description   : This routine Deletes stale BGP Pws                       */
/* Input(s)      : None                                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                         */
/*****************************************************************************/
VOID
L2VpnBgpDeleteStaleEntries (VOID)
{
    tL2VpnPwHwList      L2VpnPwHwListEntry;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VBO;
    UINT4               u4PwIndex;
    UINT4               u4LocalVeId;
    UINT1               au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1               au1RouteTarget[L2VPN_MAX_VPLS_RT_LEN];

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    MEMSET (au1RouteDistinguisher, 0, sizeof (L2VPN_MAX_VPLS_RD_LEN));
    MEMSET (au1RouteTarget, 0, sizeof (L2VPN_MAX_VPLS_RT_LEN));

    gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted = L2VPN_FALSE;
    MEMSET (&L2VpnPwHwListEntry, L2VPN_ZERO, sizeof (tL2VpnPwHwList));

    if (L2VpnHwListGetFirst (&L2VpnPwHwListEntry) == MPLS_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG, "%s : No node in HwList\n",
                    __func__);
        return;
    }
    do
    {
        if (L2VPN_ZERO == L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
        {
            continue;
        }

        if (L2VPN_PW_STATUS_NOT_STALE ==
            L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry))
        {
            continue;
        }
        pVplsEntry =
            L2VpnGetVplsEntryFromInstanceIndex (L2VPN_PW_HW_LIST_VPLS_INDEX
                                                (&L2VpnPwHwListEntry));
        if (NULL == pVplsEntry)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : VPLS Index = %u not exists\n", __func__,
                        L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry));
            continue;
        }

        if (L2VpnGetLocalVeIdFromVPLSIndex
            (L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry),
             &u4LocalVeId) == L2VPN_FAILURE)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Unable to get VE-id for VPLSInstance = %u\n",
                        __func__,
                        L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListEntry));
            continue;
        }

        u4VBO = L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry)
            / L2VPN_VPLS_VBS_DEFAULT_VALUE;
        if (L2VPN_ZERO == (L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry)
                           % L2VPN_VPLS_VBS_DEFAULT_VALUE))
        {
            u4VBO = u4VBO - 1;
        }
        if (L2VPN_ZERO ==
            (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4ReferenceCount
            && L2VPN_INVALID_LABEL !=
            (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock)
        {
            (VOID)
                L2VpnGetRdValue (L2VPN_PW_HW_LIST_VPLS_INDEX
                                 (&L2VpnPwHwListEntry), au1RouteDistinguisher);
            (VOID)
                L2VpnGetExportRtValue (L2VPN_PW_HW_LIST_VPLS_INDEX
                                       (&L2VpnPwHwListEntry), L2VPN_TRUE,
                                       au1RouteTarget);
            (VOID)
                L2VpnSendBgpWithdrawEvent (L2VPN_PW_HW_LIST_VPLS_INDEX
                                           (&L2VpnPwHwListEntry), u4LocalVeId,
                                           L2VPN_VPLS_MTU (pVplsEntry), u4VBO,
                                           (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))
                                           [u4VBO].u4LabelBlock,
                                           au1RouteDistinguisher,
                                           au1RouteTarget,
                                           L2VPN_VPLS_CONTROL_WORD
                                           (pVplsEntry));

            if (L2VpnReleaseLb ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].
                                u4LabelBlock) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Failed to release label block\n", __func__);
            }

            (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock =
                L2VPN_INVALID_LABEL;
        }
        u4PwIndex = L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry);
        if (L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL) == L2VPN_FAILURE)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : PwVcIndex = %u deletion is failed from Hw\n",
                        __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        }
        if (L2VpnDelStalePwIlm (&L2VpnPwHwListEntry, NULL) == L2VPN_FAILURE)
        {
            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : PwIlmIndex = %u deletion is failed from Hw\n",
                        __func__,
                        L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListEntry));
        }
        MplsL2VpnRelPwVcIndex (u4PwIndex);
        L2VPN_NO_OF_BGP_PW_STALE_ENTRIES (gpPwVcGlobalInfo)--;
        if (L2VPN_ZERO == L2VPN_NO_OF_BGP_PW_STALE_ENTRIES (gpPwVcGlobalInfo))
        {
            if (gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted == L2VPN_TRUE)
            {
                if (TmrStopTimer (L2VPN_TIMER_LIST_ID,
                                  &(gpL2VpnGlobalInfo->BgpGrTimer.AppTimer)) !=
                    TMR_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "Stop of BGP GR timer failed\r\n");
                }
                gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted = L2VPN_FALSE;
            }
        }
    }
    while (L2VpnHwListGetNext (&L2VpnPwHwListEntry, &L2VpnPwHwListEntry)
           != MPLS_FAILURE);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);

}

/*****************************************************************************/
/* Function Name : L2VpnBgpDeleteEntryOnMismatch                                       */
/* Description   : This routine Deletes stale BGP Pws                       */
/* Input(s)      : None                                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                         */
/*****************************************************************************/
VOID
L2VpnBgpDeletePwEntryOnMismatch (tL2VpnPwHwList * pL2VpnPwHwListEntry)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VBO;
    UINT4               u4PwIndex;
    UINT4               u4LocalVeId;
    UINT1               au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1               au1RouteTarget[L2VPN_MAX_VPLS_RT_LEN];

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Entry\n", __func__);

    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (L2VPN_PW_HW_LIST_VPLS_INDEX
                                            (pL2VpnPwHwListEntry));
    if (NULL == pVplsEntry)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : VPLS Index = %u not exists\n", __func__,
                    L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry));
        return;
    }

    if (L2VpnGetLocalVeIdFromVPLSIndex
        (L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry),
         &u4LocalVeId) == L2VPN_FAILURE)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : Unable to get VE-id for VPLSInstance = %u\n",
                    __func__,
                    L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry));
        return;
    }

    u4VBO = L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry)
        / L2VPN_VPLS_VBS_DEFAULT_VALUE;
    if (L2VPN_ZERO == (L2VPN_PW_HW_LIST_REMOTE_VE_ID (pL2VpnPwHwListEntry)
                       % L2VPN_VPLS_VBS_DEFAULT_VALUE))
    {
        u4VBO = u4VBO - 1;
    }
    if (L2VPN_ZERO ==
        (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4ReferenceCount
        && L2VPN_INVALID_LABEL !=
        (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock)
    {
        (VOID)
            L2VpnGetRdValue (L2VPN_PW_HW_LIST_VPLS_INDEX (pL2VpnPwHwListEntry),
                             au1RouteDistinguisher);
        (VOID)
            L2VpnGetExportRtValue (L2VPN_PW_HW_LIST_VPLS_INDEX
                                   (pL2VpnPwHwListEntry), L2VPN_TRUE,
                                   au1RouteTarget);
        (VOID)
            L2VpnSendBgpWithdrawEvent (L2VPN_PW_HW_LIST_VPLS_INDEX
                                       (pL2VpnPwHwListEntry), u4LocalVeId,
                                       L2VPN_VPLS_MTU (pVplsEntry), u4VBO,
                                       (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))
                                       [u4VBO].u4LabelBlock,
                                       au1RouteDistinguisher, au1RouteTarget,
                                       L2VPN_VPLS_CONTROL_WORD (pVplsEntry));

        if (L2VpnReleaseLb ((L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].
                            u4LabelBlock) == L2VPN_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "%s : Failed to release label block\n", __func__);
        }

        (L2VPN_VPLS_LABEL_BLOCK (pVplsEntry))[u4VBO].u4LabelBlock =
            L2VPN_INVALID_LABEL;
    }
    u4PwIndex = L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry);
    if (L2VpnDelStalePwVc (pL2VpnPwHwListEntry, NULL) == L2VPN_FAILURE)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : PwVcIndex = %u deletion is failed from Hw\n",
                    __func__, L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry));
    }
    if (L2VpnDelStalePwIlm (pL2VpnPwHwListEntry, NULL) == L2VPN_FAILURE)
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                    "%s : PwIlmIndex = %u deletion is failed from Hw\n",
                    __func__, L2VPN_PW_HW_LIST_PW_INDEX (pL2VpnPwHwListEntry));
    }
    MplsL2VpnRelPwVcIndex (u4PwIndex);

    L2VPN_DBG1 (L2VPN_DBG_LVL_DBG_FLAG, "%s : Exit\n", __func__);
}
#endif
