/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwmlw.c,v 1.21 2014/12/24 10:58:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fssnmp.h"
# include  "l2vpincs.h"
/* LOW LEVEL Routines for Table : PwMplsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwMplsTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwMplsTable (UINT4 u4PwIndex)
{
    if (!((u4PwIndex >= L2VPN_PWVC_INDEX_MINVAL) &&
          (u4PwIndex <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwMplsTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwMplsTable (UINT4 *pu4PwIndex)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (L2VpnValidatePsnEntry (*pu4PwIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwMplsTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwMplsTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (L2VpnValidatePsnEntry (*pu4NextPwIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwMplsMplsType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsMplsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsMplsType (UINT4 u4PwIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValPwMplsMplsType)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1MplsType = L2VPN_ZERO;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType =
                L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                           L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            CPY_TO_SNMP (pRetValPwMplsMplsType, &u1MplsType, L2VPN_ONE);

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsExpBitsMode
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsExpBitsMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsExpBitsMode (UINT4 u4PwIndex, INT4 *pi4RetValPwMplsExpBitsMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    INT4                i4MplsExpBitsMode = 0;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            i4MplsExpBitsMode = ((L2VPN_UPNIB) &
                                 (L2VPN_PWVC_MPLS_EXP_BITS_MODE
                                  ((tPwVcMplsEntry
                                    *) (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)))));
            *pi4RetValPwMplsExpBitsMode = (i4MplsExpBitsMode >> 4);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsExpBits
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsExpBits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsExpBits (UINT4 u4PwIndex, UINT4 *pu4RetValPwMplsExpBits)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            *pu4RetValPwMplsExpBits = ((L2VPN_LOWNIB) &
                                       (L2VPN_PWVC_MPLS_EXP_BITS
                                        ((tPwVcMplsEntry *)
                                         L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsTtl
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsTtl (UINT4 u4PwIndex, UINT4 *pu4RetValPwMplsTtl)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            *pu4RetValPwMplsTtl = L2VPN_PWVC_MPLS_TTL ((tPwVcMplsEntry *)
                                                       L2VPN_PWVC_PSN_ENTRY
                                                       (pPwVcEntry));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsLocalLdpID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsLocalLdpID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsLocalLdpID (UINT4 u4PwIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValPwMplsLocalLdpID)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            CPY_TO_SNMP (pRetValPwMplsLocalLdpID,
                         L2VPN_PWVC_MPLS_LCL_LDPID ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry)),
                         L2VPN_MAX_LDPID_LEN);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsLocalLdpEntityID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsLocalLdpEntityID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsLocalLdpEntityID (UINT4 u4PwIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValPwMplsLocalLdpEntityID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            MPLS_INTEGER_TO_OCTETSTRING (L2VPN_PWVC_MPLS_LCL_LDP_ENTID
                                         ((tPwVcMplsEntry *)
                                          L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)),
                                         pRetValPwMplsLocalLdpEntityID);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsPeerLdpID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsPeerLdpID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsPeerLdpID (UINT4 u4PwIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValPwMplsPeerLdpID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            CPY_TO_SNMP (pRetValPwMplsPeerLdpID,
                         L2VPN_PWVC_MPLS_PEER_LDPID ((tPwVcMplsEntry *)
                                                     L2VPN_PWVC_PSN_ENTRY
                                                     (pPwVcEntry)),
                         L2VPN_MAX_LDPID_LEN);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsStorageType (UINT4 u4PwIndex, INT4 *pi4RetValPwMplsStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            *pi4RetValPwMplsStorageType = L2VPN_PWVC_MPLS_STORAGE_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPwMplsMplsType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsMplsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsMplsType (UINT4 u4PwIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValPwMplsMplsType)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType = L2VPN_ZERO;

    CPY_FROM_SNMP (&u1MplsType, pSetValPwMplsMplsType, L2VPN_ONE);

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                       L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)) =
                u1MplsType;

            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            L2VPN_PWVC_MPLS_TNL_TYPE (pPwVcMplsTnlEntry) =
                L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                           L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsExpBitsMode
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsExpBitsMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsExpBitsMode (UINT4 u4PwIndex, INT4 i4SetValPwMplsExpBitsMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            L2VPN_PWVC_MPLS_EXP_BITS_MODE ((tPwVcMplsEntry *)
                                           L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)) =
                (INT1) (i4SetValPwMplsExpBitsMode << MPLS_FOUR);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsExpBits
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsExpBits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsExpBits (UINT4 u4PwIndex, UINT4 u4SetValPwMplsExpBits)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            L2VPN_PWVC_MPLS_EXP_BITS ((tPwVcMplsEntry *)
                                      L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)) |=
                u4SetValPwMplsExpBits;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsTtl
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsTtl (UINT4 u4PwIndex, UINT4 u4SetValPwMplsTtl)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            L2VPN_PWVC_MPLS_TTL ((tPwVcMplsEntry *)
                                 L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)) =
                (UINT1) u4SetValPwMplsTtl;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsLocalLdpID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsLocalLdpID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsLocalLdpID (UINT4 u4PwIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValPwMplsLocalLdpID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            CPY_FROM_SNMP (L2VPN_PWVC_MPLS_LCL_LDPID ((tPwVcMplsEntry *)
                                                      L2VPN_PWVC_PSN_ENTRY
                                                      (pPwVcEntry)),
                           pSetValPwMplsLocalLdpID, L2VPN_MAX_LDPID_LEN);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsLocalLdpEntityID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsLocalLdpEntityID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsLocalLdpEntityID (UINT4 u4PwIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValPwMplsLocalLdpEntityID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            MPLS_OCTETSTRING_TO_INTEGER (pSetValPwMplsLocalLdpEntityID,
                                         L2VPN_PWVC_MPLS_LCL_LDP_ENTID
                                         ((tPwVcMplsEntry *)
                                          L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsStorageType (UINT4 u4PwIndex, INT4 i4SetValPwMplsStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            L2VPN_PWVC_MPLS_STORAGE_TYPE ((tPwVcMplsEntry *)
                                          L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)) =
                (INT1) i4SetValPwMplsStorageType;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PwMplsMplsType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsMplsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsMplsType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValPwMplsMplsType)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1MplsType = L2VPN_ZERO;

    CPY_FROM_SNMP (&u1MplsType, pTestValPwMplsMplsType, L2VPN_ONE);

    switch (u1MplsType)
    {
        case L2VPN_MPLS_TYPE_TE:
        case L2VPN_MPLS_TYPE_NONTE:
        case L2VPN_MPLS_TYPE_VCONLY:
            break;
        case L2VPN_MPLS_TYPE_TENONTE:
        case L2VPN_MPLS_TYPE_TEVCONLY:
        case L2VPN_MPLS_TYPE_NONTEVCONLY:
        case L2VPN_MPLS_TYPE_TENONTEVCONLY:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsExpBitsMode
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsExpBitsMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsExpBitsMode (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            INT4 i4TestValPwMplsExpBitsMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwMplsExpBitsMode)
    {
        case L2VPN_MPLS_EBIT_MD_OUT_TNL:
            break;
        case L2VPN_MPLS_EBIT_MD_SPEC_VAL:
            break;
        case L2VPN_MPLS_EBIT_MD_SER_DEP:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsExpBits
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsExpBits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsExpBits (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                        UINT4 u4TestValPwMplsExpBits)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValPwMplsExpBits > L2VPN_MPLS_EXP_BIT_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsTtl
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsTtl (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                    UINT4 u4TestValPwMplsTtl)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValPwMplsTtl > L2VPN_UINT1_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsLocalLdpID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsLocalLdpID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsLocalLdpID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValPwMplsLocalLdpID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (pTestValPwMplsLocalLdpID->i4_Length != L2VPN_MAX_LDPID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsLocalLdpEntityID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsLocalLdpEntityID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsLocalLdpEntityID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValPwMplsLocalLdpEntityID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (pTestValPwMplsLocalLdpEntityID->i4_Length != ROUTER_ID_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsStorageType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            INT4 i4TestValPwMplsStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwMplsStorageType)
    {
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PwMplsTable
 Input       :  The Indices
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwMplsTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PwMplsOutboundTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwMplsOutboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwMplsOutboundTable (UINT4 u4PwIndex)
{
    if ((u4PwIndex < L2VPN_MIN_PWVC_ENTRIES) ||
        (u4PwIndex > L2VPN_MAXIMUM_PWVC_ENTRIES))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwMplsOutboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwMplsOutboundTable (UINT4 *pu4PwIndex)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (L2VpnValidatePsnEntry (*pu4PwIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwMplsOutboundTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwMplsOutboundTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (L2VpnValidatePsnEntry (*pu4NextPwIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundLsrXcIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundLsrXcIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundLsrXcIndex (UINT4 u4PwIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPwMplsOutboundLsrXcIndex)
{
    UINT1               u1MplsType = L2VPN_ZERO;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_NONTE)
            {
                MPLS_INTEGER_TO_OCTETSTRING (L2VPN_PWVC_MPLS_TNL_XC_INDEX
                                             (pPwVcMplsTnlEntry),
                                             pRetValPwMplsOutboundLsrXcIndex);
                return SNMP_SUCCESS;
            }
            else
            {
                MEMSET (pRetValPwMplsOutboundLsrXcIndex->pu1_OctetList,
                        L2VPN_ZERO, sizeof (UINT1));
                pRetValPwMplsOutboundLsrXcIndex->i4_Length = sizeof (UINT1);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundTunnelIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundTunnelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundTunnelIndex (UINT4 u4PwIndex,
                                 UINT4 *pu4RetValPwMplsOutboundTunnelIndex)
{
    UINT1               u1MplsType = L2VPN_ZERO;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                *pu4RetValPwMplsOutboundTunnelIndex =
                    L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pPwVcMplsTnlEntry);
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4RetValPwMplsOutboundTunnelIndex = L2VPN_ZERO;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundTunnelInstance
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundTunnelInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundTunnelInstance (UINT4 u4PwIndex,
                                    UINT4
                                    *pu4RetValPwMplsOutboundTunnelInstance)
{
    UINT1               u1MplsType = L2VPN_ZERO;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                *pu4RetValPwMplsOutboundTunnelInstance =
                    L2VPN_PWVC_MPLS_TNL_TNL_INST (pPwVcMplsTnlEntry);
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4RetValPwMplsOutboundTunnelInstance = L2VPN_ZERO;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundTunnelLclLSR
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundTunnelLclLSR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundTunnelLclLSR (UINT4 u4PwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValPwMplsOutboundTunnelLclLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType = L2VPN_ZERO;
    UINT1               u1TmpRouterId[4] = { 0, 0, 0, 0 };

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                CPY_TO_SNMP (pRetValPwMplsOutboundTunnelLclLSR,
                             L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR
                             (pPwVcMplsTnlEntry), ROUTER_ID_LENGTH);
                return SNMP_SUCCESS;
            }
            else
            {
                /* Must be Zero for type other than TE        */
                CPY_TO_SNMP (pRetValPwMplsOutboundTunnelLclLSR,
                             u1TmpRouterId, ROUTER_ID_LENGTH);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundTunnelPeerLSR
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundTunnelPeerLSR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundTunnelPeerLSR (UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValPwMplsOutboundTunnelPeerLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType = L2VPN_ZERO;
    UINT1               u1TmpRouterId[4] = { 0, 0, 0, 0 };

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                CPY_TO_SNMP (pRetValPwMplsOutboundTunnelPeerLSR,
                             L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR
                             (pPwVcMplsTnlEntry), ROUTER_ID_LENGTH);
                return SNMP_SUCCESS;
            }
            else
            {
                /* Must be Zero for type other than TE        */
                CPY_TO_SNMP (pRetValPwMplsOutboundTunnelPeerLSR,
                             u1TmpRouterId, ROUTER_ID_LENGTH);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundIfIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundIfIndex (UINT4 u4PwIndex,
                             INT4 *pi4RetValPwMplsOutboundIfIndex)
{
    UINT1               u1MplsType = L2VPN_ZERO;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_VCONLY)
            {
                *pi4RetValPwMplsOutboundIfIndex =
                    (INT4)(L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPwVcMplsTnlEntry));
                return SNMP_SUCCESS;
            }
            else
            {
                *pi4RetValPwMplsOutboundIfIndex = L2VPN_ZERO;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwMplsOutboundTunnelTypeInUse
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsOutboundTunnelTypeInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsOutboundTunnelTypeInUse (UINT4 u4PwIndex,
                                     INT4
                                     *pi4RetValPwMplsOutboundTunnelTypeInUse)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            switch (L2VPN_PWVC_MPLS_TNL_TYPE (pPwVcMplsTnlEntry))
            {
                case L2VPN_MPLS_TYPE_TE:
                    *pi4RetValPwMplsOutboundTunnelTypeInUse =
                        L2VPN_TUNNEL_TYPE_IN_USE_MPLS_TE;
                    break;
                case L2VPN_MPLS_TYPE_NONTE:
                    *pi4RetValPwMplsOutboundTunnelTypeInUse =
                        L2VPN_TUNNEL_TYPE_IN_USE_MPLS_NONTE;
                    break;
                case L2VPN_MPLS_TYPE_VCONLY:
                    *pi4RetValPwMplsOutboundTunnelTypeInUse =
                        L2VPN_TUNNEL_TYPE_IN_USE_VCONLY;
                    break;
                default:
                    *pi4RetValPwMplsOutboundTunnelTypeInUse =
                        L2VPN_TUNNEL_TYPE_IN_USE_NOT_YET_KNOWN;
                    break;
            }
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPwMplsOutboundLsrXcIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsOutboundLsrXcIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsOutboundLsrXcIndex (UINT4 u4PwIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValPwMplsOutboundLsrXcIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            return SNMP_FAILURE;
        }

        u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_NONTE)
            {
                MPLS_OCTETSTRING_TO_INTEGER (pSetValPwMplsOutboundLsrXcIndex,
                                             L2VPN_PWVC_MPLS_TNL_XC_INDEX
                                             (pPwVcMplsTnlEntry));
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsOutboundTunnelIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsOutboundTunnelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsOutboundTunnelIndex (UINT4 u4PwIndex,
                                 UINT4 u4SetValPwMplsOutboundTunnelIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            return SNMP_FAILURE;
        }

        u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pPwVcMplsTnlEntry) =
                    u4SetValPwMplsOutboundTunnelIndex;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsOutboundTunnelLclLSR
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsOutboundTunnelLclLSR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsOutboundTunnelLclLSR (UINT4 u4PwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValPwMplsOutboundTunnelLclLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            return SNMP_FAILURE;
        }
        u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                CPY_FROM_SNMP (L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR
                               (pPwVcMplsTnlEntry),
                               pSetValPwMplsOutboundTunnelLclLSR,
                               ROUTER_ID_LENGTH);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsOutboundTunnelPeerLSR
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsOutboundTunnelPeerLSR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsOutboundTunnelPeerLSR (UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValPwMplsOutboundTunnelPeerLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            return SNMP_FAILURE;
        }
        u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_TE)
            {
                CPY_FROM_SNMP (L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR
                               (pPwVcMplsTnlEntry),
                               pSetValPwMplsOutboundTunnelPeerLSR,
                               ROUTER_ID_LENGTH);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwMplsOutboundIfIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwMplsOutboundIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwMplsOutboundIfIndex (UINT4 u4PwIndex,
                             INT4 i4SetValPwMplsOutboundIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsTnlEntry  *pPwVcMplsTnlEntry = NULL;
    UINT1               u1MplsType;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            return SNMP_FAILURE;
        }

        u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
            ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

        if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
        {
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY ((tPwVcMplsEntry *)
                                                           L2VPN_PWVC_PSN_ENTRY
                                                           (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_VCONLY)
            {
                L2VPN_PWVC_MPLS_TNL_IF_INDEX (pPwVcMplsTnlEntry) =
                    (UINT4)i4SetValPwMplsOutboundIfIndex;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PwMplsOutboundLsrXcIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsOutboundLsrXcIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsOutboundLsrXcIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValPwMplsOutboundLsrXcIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (pTestValPwMplsOutboundLsrXcIndex->i4_Length >
        L2VPN_MAX_LSR_XC_INDEX_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* XC Index must not be given if the PW owner is not Manual */
        if ((L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_MANUAL) &&
            (L2VPN_IS_STATIC_PW (pPwVcEntry) != TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_NONTE) &&
            (L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_TE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsOutboundTunnelIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsOutboundTunnelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsOutboundTunnelIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                    UINT4 u4TestValPwMplsOutboundTunnelIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValPwMplsOutboundTunnelIndex > TE_TNL_INDEX_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_TE) &&
            (L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_NONTE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsOutboundTunnelLclLSR
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsOutboundTunnelLclLSR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsOutboundTunnelLclLSR (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValPwMplsOutboundTunnelLclLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (pTestValPwMplsOutboundTunnelLclLSR->i4_Length != ROUTER_ID_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_TE) &&
            (L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_NONTE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsOutboundTunnelPeerLSR
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsOutboundTunnelPeerLSR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsOutboundTunnelPeerLSR (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValPwMplsOutboundTunnelPeerLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (pTestValPwMplsOutboundTunnelPeerLSR->i4_Length != ROUTER_ID_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_TE) &&
            (L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                        L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
             != L2VPN_MPLS_TYPE_NONTE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwMplsOutboundIfIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwMplsOutboundIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwMplsOutboundIfIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                INT4 i4TestValPwMplsOutboundIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = L2VPN_ZERO;
#endif
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValPwMplsOutboundIfIndex < L2VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifdef CFA_WANTED
    /* See whether OutBound Interface is MPLS Enabled or not */
    if (CfaUtilGetMplsIfFromIfIndex ((UINT4) i4TestValPwMplsOutboundIfIndex,
                                     &u4MplsIfIndex, TRUE) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS) ||
            (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                       L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))
            != L2VPN_MPLS_TYPE_VCONLY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PwMplsOutboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwMplsOutboundTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PwMplsInboundTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwMplsInboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwMplsInboundTable (UINT4 u4PwIndex)
{
    if ((u4PwIndex < L2VPN_MIN_PWVC_ENTRIES) ||
        (u4PwIndex > L2VPN_MAXIMUM_PWVC_ENTRIES))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwMplsInboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwMplsInboundTable (UINT4 *pu4PwIndex)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwMplsInboundTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwMplsInboundTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwMplsInboundXcIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwMplsInboundXcIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwMplsInboundXcIndex (UINT4 u4PwIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPwMplsInboundXcIndex)
{
    UINT1               u1MplsType = L2VPN_ZERO;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if ((L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PSN_TYPE_MPLS)
            && (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) != NULL))
        {
            u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE
                ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));
            pPwVcMplsTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                          L2VPN_PWVC_PSN_ENTRY
                                                          (pPwVcEntry));
            if (u1MplsType == L2VPN_MPLS_TYPE_NONTE)
            {
                MPLS_INTEGER_TO_OCTETSTRING (L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX
                                             (pPwVcMplsTnlEntry),
                                             pRetValPwMplsInboundXcIndex);
                return SNMP_SUCCESS;
            }
            else
            {
                MEMSET (pRetValPwMplsInboundXcIndex->pu1_OctetList,
                        L2VPN_ZERO, sizeof (UINT1));
                pRetValPwMplsInboundXcIndex->i4_Length = sizeof (UINT1);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : PwMplsNonTeMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwMplsNonTeMappingTable
 Input       :  The Indices
                PwMplsNonTeMappingDirection
                PwMplsNonTeMappingXcIndex
                PwMplsNonTeMappingIfIndex
                PwMplsNonTeMappingVcIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwMplsNonTeMappingTable (INT4
                                                 i4PwMplsNonTeMappingDirection,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pPwMplsNonTeMappingXcIndex,
                                                 INT4
                                                 i4PwMplsNonTeMappingIfIndex,
                                                 UINT4
                                                 u4PwMplsNonTeMappingVcIndex)
{
    L2VPN_SUPPRESS_WARNING (pPwMplsNonTeMappingXcIndex);
    L2VPN_SUPPRESS_WARNING (i4PwMplsNonTeMappingIfIndex);

    if (((i4PwMplsNonTeMappingDirection != L2VPN_OUTBOUND_DIR) &&
         (i4PwMplsNonTeMappingDirection != L2VPN_INBOUND_DIR)))
    {
        return SNMP_FAILURE;
    }
    if (((u4PwMplsNonTeMappingVcIndex < L2VPN_PWVC_INDEX_MINVAL) ||
         (u4PwMplsNonTeMappingVcIndex > L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwMplsNonTeMappingTable
 Input       :  The Indices
                PwMplsNonTeMappingDirection
                PwMplsNonTeMappingXcIndex
                PwMplsNonTeMappingIfIndex
                PwMplsNonTeMappingVcIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwMplsNonTeMappingTable (INT4 *pi4PwMplsNonTeMappingDirection,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pPwMplsNonTeMappingXcIndex,
                                         INT4 *pi4PwMplsNonTeMappingIfIndex,
                                         UINT4 *pu4PwMplsNonTeMappingVcIndex)
{
/* TODO */
    UNUSED_PARAM (pi4PwMplsNonTeMappingDirection);
    UNUSED_PARAM (pPwMplsNonTeMappingXcIndex);
    UNUSED_PARAM (pi4PwMplsNonTeMappingIfIndex);
    UNUSED_PARAM (pu4PwMplsNonTeMappingVcIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwMplsNonTeMappingTable
 Input       :  The Indices
                PwMplsNonTeMappingDirection
                nextPwMplsNonTeMappingDirection
                PwMplsNonTeMappingXcIndex
                nextPwMplsNonTeMappingXcIndex
                PwMplsNonTeMappingIfIndex
                nextPwMplsNonTeMappingIfIndex
                PwMplsNonTeMappingVcIndex
                nextPwMplsNonTeMappingVcIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwMplsNonTeMappingTable (INT4 i4PwMplsNonTeMappingDirection,
                                        INT4
                                        *pi4NextPwMplsNonTeMappingDirection,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pPwMplsNonTeMappingXcIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextPwMplsNonTeMappingXcIndex,
                                        INT4 i4PwMplsNonTeMappingIfIndex,
                                        INT4 *pi4NextPwMplsNonTeMappingIfIndex,
                                        UINT4 u4PwMplsNonTeMappingVcIndex,
                                        UINT4 *pu4NextPwMplsNonTeMappingVcIndex)
{
/* TODO */
    UNUSED_PARAM (i4PwMplsNonTeMappingDirection);
    UNUSED_PARAM (pi4NextPwMplsNonTeMappingDirection);
    UNUSED_PARAM (pPwMplsNonTeMappingXcIndex);
    UNUSED_PARAM (pNextPwMplsNonTeMappingXcIndex);
    UNUSED_PARAM (i4PwMplsNonTeMappingIfIndex);
    UNUSED_PARAM (pi4NextPwMplsNonTeMappingIfIndex);
    UNUSED_PARAM (u4PwMplsNonTeMappingVcIndex);
    UNUSED_PARAM (pu4NextPwMplsNonTeMappingVcIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : PwMplsTeMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwMplsTeMappingTable
 Input       :  The Indices
                PwMplsTeMappingTunnelIndex
                PwMplsTeMappingTunnelInstance
                PwMplsTeMappingTunnelPeerLsrID
                PwMplsTeMappingTunnelLocalLsrID
                PwMplsTeMappingVcIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwMplsTeMappingTable (UINT4
                                              u4PwMplsTeMappingTunnelIndex,
                                              UINT4
                                              u4PwMplsTeMappingTunnelInstance,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pPwMplsTeMappingTunnelPeerLsrID,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pPwMplsTeMappingTunnelLocalLsrID,
                                              UINT4 u4PwMplsTeMappingVcIndex)
{
    L2VPN_SUPPRESS_WARNING (u4PwMplsTeMappingTunnelIndex);
    L2VPN_SUPPRESS_WARNING (u4PwMplsTeMappingTunnelInstance);
    L2VPN_SUPPRESS_WARNING (pPwMplsTeMappingTunnelPeerLsrID);
    L2VPN_SUPPRESS_WARNING (pPwMplsTeMappingTunnelLocalLsrID);
    L2VPN_SUPPRESS_WARNING (u4PwMplsTeMappingVcIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwMplsTeMappingTable
 Input       :  The Indices
                PwMplsTeMappingTunnelIndex
                PwMplsTeMappingTunnelInstance
                PwMplsTeMappingTunnelPeerLsrID
                PwMplsTeMappingTunnelLocalLsrID
                PwMplsTeMappingVcIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwMplsTeMappingTable (UINT4 *pu4PwMplsTeMappingTunnelIndex,
                                      UINT4 *pu4PwMplsTeMappingTunnelInstance,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pPwMplsTeMappingTunnelPeerLsrID,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pPwMplsTeMappingTunnelLocalLsrID,
                                      UINT4 *pu4PwMplsTeMappingVcIndex)
{
/* TODO */
    UNUSED_PARAM (pu4PwMplsTeMappingTunnelIndex);
    UNUSED_PARAM (pu4PwMplsTeMappingTunnelInstance);
    UNUSED_PARAM (pPwMplsTeMappingTunnelPeerLsrID);
    UNUSED_PARAM (pPwMplsTeMappingTunnelLocalLsrID);
    UNUSED_PARAM (pu4PwMplsTeMappingVcIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwMplsTeMappingTable
 Input       :  The Indices
                PwMplsTeMappingTunnelIndex
                nextPwMplsTeMappingTunnelIndex
                PwMplsTeMappingTunnelInstance
                nextPwMplsTeMappingTunnelInstance
                PwMplsTeMappingTunnelPeerLsrID
                nextPwMplsTeMappingTunnelPeerLsrID
                PwMplsTeMappingTunnelLocalLsrID
                nextPwMplsTeMappingTunnelLocalLsrID
                PwMplsTeMappingVcIndex
                nextPwMplsTeMappingVcIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwMplsTeMappingTable (UINT4 u4PwMplsTeMappingTunnelIndex,
                                     UINT4 *pu4NextPwMplsTeMappingTunnelIndex,
                                     UINT4 u4PwMplsTeMappingTunnelInstance,
                                     UINT4
                                     *pu4NextPwMplsTeMappingTunnelInstance,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pPwMplsTeMappingTunnelPeerLsrID,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextPwMplsTeMappingTunnelPeerLsrID,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pPwMplsTeMappingTunnelLocalLsrID,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextPwMplsTeMappingTunnelLocalLsrID,
                                     UINT4 u4PwMplsTeMappingVcIndex,
                                     UINT4 *pu4NextPwMplsTeMappingVcIndex)
{
/* TODO */
    UNUSED_PARAM (u4PwMplsTeMappingTunnelIndex);
    UNUSED_PARAM (pu4NextPwMplsTeMappingTunnelIndex);
    UNUSED_PARAM (u4PwMplsTeMappingTunnelInstance);
    UNUSED_PARAM (pu4NextPwMplsTeMappingTunnelInstance);
    UNUSED_PARAM (pPwMplsTeMappingTunnelPeerLsrID);
    UNUSED_PARAM (pNextPwMplsTeMappingTunnelPeerLsrID);
    UNUSED_PARAM (pPwMplsTeMappingTunnelLocalLsrID);
    UNUSED_PARAM (pNextPwMplsTeMappingTunnelLocalLsrID);
    UNUSED_PARAM (u4PwMplsTeMappingVcIndex);
    UNUSED_PARAM (pu4NextPwMplsTeMappingVcIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
