/* $Id: l2vpapi.c,v 1.11 2015/11/26 11:05:07 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *****************************************************************************
 *    FILE  NAME             : l2vpapi.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains utility routines for L2VPN
 *                             module
 *---------------------------------------------------------------------------*/

#include "l2vpincs.h"
#include "la.h"

/*****************************************************************************/
/* Function     : L2VpnApiHandleExternalRequest                              */
/*                                                                           */
/* Description  : This function gets/sets the L2VPN information based on the */
/*                request from MPLS L2VPN external module.                   */
/*                                                                           */
/* Input        : u4MainReqType - Request type                               */
/*                pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
L2VpnApiHandleExternalRequest (UINT4 u4MainReqType,
                               tMplsApiInInfo * pInMplsApiInfo,
                               tMplsApiOutInfo * pOutMplsApiInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if (pInMplsApiInfo == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2VpnApiHandleExternalRequest: "
                   "Provide valid in/out API information\r\n");
        return OSIX_FAILURE;
    }
    if ((u4MainReqType == MPLS_OAM_MEG_ASSOC_WITH_PW) ||
        (u4MainReqType == MPLS_OAM_UPDATE_PW_MEG_OAM_STATUS))
    {
        /* This handling should not be done in L2VPN LOCK as these functions are 
         * getting invoked from MEG */
        if (L2vpnExtHdlMegAssocAndOamStatus (u4MainReqType,
                                             pInMplsApiInfo) == OSIX_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                       "L2VpnApiHandleExternalRequest: "
                       "MEG association or PW Proactive status update from "
                       "MEG failed\r\n");

            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }

    MPLS_L2VPN_LOCK ();
    /* External requests handling */
    switch (u4MainReqType)
    {
        case MPLS_OAM_HANDLE_PATH_STATUS_CHG:
            i4RetVal = L2vpnExtHandlePathStatusChgForPw (pInMplsApiInfo);
            break;
        case MPLS_OAM_UPDATE_SESSION_PARAMS:
            i4RetVal = L2vpnExtPwUpdateSessionParams (pInMplsApiInfo);
            break;
        case MPLS_GET_PW_INFO:
            i4RetVal = L2vpnExtGetPwInfo (pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_GET_PATH_FROM_INLBL_INFO:
            i4RetVal = L2vpnExtGetPwInfoFromInLblInfo (pInMplsApiInfo,
                                                       pOutMplsApiInfo);
            break;
        case MPLS_GET_SERVICE_POINTER_OID:
            i4RetVal = L2vpnExtGetPwServicePointerOid (pInMplsApiInfo,
                                                       pOutMplsApiInfo);
            break;
        case MPLS_OAM_UPDATE_LPS_STATUS:
            i4RetVal = L2vpnExtUpdatePwLpsStatus (pInMplsApiInfo);
            break;
        case MPLS_APP_REG_PATH_FOR_NOTIF:
            i4RetVal = L2vpnExtUpdateOamAppStatus (pInMplsApiInfo);
            break;
        case MPLS_GET_PACKET_COUNT:
            i4RetVal = L2vpnExtGetPktCnt (pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_INCR_OUT_PACKET_COUNT:
            i4RetVal = L2vpnExtIncrOutPktCnt (pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_INCR_IN_PACKET_COUNT:
            i4RetVal = L2vpnExtIncrInPktCnt (pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_UPDATE_APP_OWNER_FOR_PW:
            i4RetVal = L2vpnExtUpdatePwApp (pInMplsApiInfo, pOutMplsApiInfo);
            break;
#ifdef HVPLS_WANTED
	case MPLS_REGISTER_PW_FOR_NOTIF:
	    i4RetVal = L2vpnExtRegisterPwForNotif (pInMplsApiInfo, NULL);
	    break;
#endif
        default:
            break;
    }
    if (i4RetVal == OSIX_FAILURE)
    {
        L2VPN_DBG (L2VPN_DBG_ERR_VC_FLAG,
                   "L2VpnApiHandleExternalRequest: "
                   "PW information get/set failed received from "
                   "external module\r\n");
    }
    MPLS_L2VPN_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : L2VpnApiIsPwStatic                                        */
/* Description   : This function checks whether Pw Created is through static */
/*                 or signalling.                                            */
/* Input(s)      : PwIndex - Index of the PW                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE if Static and FALSE if Signalling or invalid PwIndex */
/*                 is specified                                              */
/*****************************************************************************/
BOOL1
L2VpnApiIsPwStatic (UINT4 u4PwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    BOOL1               bIsStaticPw = FALSE;

    MPLS_L2VPN_LOCK ();
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        bIsStaticPw = L2VPN_IS_STATIC_PW (pPwVcEntry);
    }

    MPLS_L2VPN_UNLOCK ();
    return bIsStaticPw;
}

/*****************************************************************************/
/* Function Name : L2VpnApiGetPwIndexFromPwIfIndex                           */
/* Description   : This function check that whether pseudowire entry is      */
/*                 present for the PW IF INDEX passed and returns PW INDEX   */
/* Input(s)      : u4PwIfIndex - Index of the Pseudowire IfIndex in Iftable  */
/* Output(s)     : pu4PwIndex  - pseudowire index in mpls                    */
/* Return(s)     : L2VPN_FAILURE / L2VPN_SUCCESS                             */
/*****************************************************************************/
INT4
L2VpnApiGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex)
{
    INT4                i4RetVal = 0;

    MPLS_L2VPN_LOCK ();

    i4RetVal = L2VpnGetPwIndexFromPwIfIndex (u4PwIfIndex, pu4PwIndex);

    MPLS_L2VPN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : L2VpnNotifyAcUpDown                                       */
/* Description   : This function notifies the D-lag status of the node to    */
/*                 pseudowire                                                */
/* Input(s)      : tDlagAcStatusMsg                                          */
/* Output(s)     : -                                                         */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/

VOID
L2VpnNotifyAcUpDown (tDlagAcStatusMsg * pDlagAcStatusMsg)
{
    tDlagAcStatusMsg    DlagAcStatusMsg;
    tL2VpnQMsg          L2VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;

    MEMSET (&DlagAcStatusMsg, 0, sizeof (tDlagAcStatusMsg));

    if (CRU_BUF_Copy_FromBufChain
        ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pDlagAcStatusMsg,
         (UINT1 *) &(DlagAcStatusMsg), 0,
         sizeof (tDlagAcStatusMsg)) == CRU_FAILURE)
    {
        return;
    }

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        return;
    }
    MEMSET (pu1TmpMsg, 0, sizeof (tL2VpnQMsg));

    L2VPN_PWRED_AC_STATUS (L2VpnQMsg) = DlagAcStatusMsg.u1DlagPswStatus;

    L2VPN_PWRED_AC_DLAG_PORT_CNT (L2VpnQMsg) = DlagAcStatusMsg.u1NoOfPorts;

    L2VPN_QMSG_TYPE = L2VPN_DLAG_AC_STATUS_EVENT;
    MEMCPY (L2VPN_PWRED_AC_DLAG_PORTS (L2VpnQMsg),
            DlagAcStatusMsg.au2PortIndex, LA_MAX_DLAG_PORTS);

    MEMCPY (pu1TmpMsg, &L2VpnQMsg, sizeof (tL2VpnQMsg));

    if (L2VPN_FAILURE == L2VpnInternalEventHandler (pu1TmpMsg))
    {
        return;
    }

    return;
}

/*****************************************************************************/
/* Function Name : L2VpnApiGetVplsStorageType                                */
/* Description   : This function checks whether vpls config table Created is */
/*                 dynamic or static.                                        */
/* Input(s)      : u4VplsIndex - Index of the VPLS                           */
/*                 pu1StorageType - Storage type                             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
L2VpnApiGetVplsStorageType (UINT4 u4VplsIndex, UINT1 *pu1StorageType)
{
    tVPLSEntry         *pVplsEntry = NULL;

    *pu1StorageType = 0;

    MPLS_L2VPN_LOCK ();

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsIndex);

    if (pVplsEntry != NULL)
    {
        *pu1StorageType = pVplsEntry->u1StorageType;
    }

    MPLS_L2VPN_UNLOCK ();

    return;
}
