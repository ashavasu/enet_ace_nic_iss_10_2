/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpfwd.c,v 1.45 2017/06/08 11:40:30 siva Exp $
 *
 * Description: This file contains the code for forwarding
 *              layer 2 packets.
 *******************************************************************/
#include "l2vpincs.h"
#include "rtm.h"
#include "arp.h"
#include "cfa.h"
#include "mplslsr.h"
#include "temacs.h"
#include "fsvlan.h"
#include "ipv6.h"

/* Max number of mpls header in packet can be 4. Taking it 5. Plus 4 bytes for control word.*/
#define MAX_MPLS_HEADER_LENGTH_IN_PACKET    ((MPLS_HDR_LEN * 5) + 4)

extern INT4         gi4MplsSimulateFailure;

PRIVATE INT4
 
 
   L2VpnAddMplsHdrs (UINT1 *pBuf, tPwVcEntry * pPwVcEntry, tXcEntry * pXcEntry);

PRIVATE INT4
 
 
 
 L2VpnDuplicateCruBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize);

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnMplsL2PktVplsFwd                                      */
/*                                                                           */
/* Description  : Tunnels the given layer2 packet thru all the pseudowires   */
/*                that are operationally up in the given VPLS instance       */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer that was received         */
/*              : u4Vsi - Virtual switch instance on which L2 pkt was rcvd   */
/*                u4IfIndex  - Interface on which the packet has come        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS or L2VPN_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2VpnMplsL2PktVplsFwd (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VplsInstance,
                       UINT4 u4IfIndex)
{
    UINT4               u4PktSize;
    UINT4               u4OperPwIdx = L2VPN_ZERO;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyEntry *pRgGrp = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pMsgBuf = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_VC_FLAG, "No PW is created for this instance");
        return L2VPN_FAILURE;
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwVcEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

        if ((L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS) &&
            (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) != L2VPN_PWVC_OPER_UP))
        {
            pRgPw = L2VpnGetRedundancyPwEntryByPwIndex
                (L2VPN_PWVC_INDEX (pPwVcEntry));

            if (pRgPw != NULL)
            {
                pRgGrp = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
                if (pRgGrp == NULL)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "Pseudowire is not the part of RG Group.\n");
                    continue;
                }

                if ((pRgGrp->pOperActivePw != NULL) &&
                    (pRgGrp->pOperActivePw->b1IsLocal == TRUE))
                {
                    if (pRgGrp->pOperActivePw->pRgParent != NULL)
                    {
                        u4OperPwIdx = ((tL2vpnRedundancyPwEntry *)
                                       pRgGrp->pOperActivePw->pRgParent)->
                            u4PwIndex;
                        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4OperPwIdx);
                    }
                }
                if (pPwVcEntry == NULL)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "PW VC Entry does not exists.");
                    continue;
                }
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) != L2VPN_PWVC_OPER_UP)
                {
                    continue;
                }
            }
            else
            {
                continue;
            }
        }
        else if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
        {
            if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
                (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_STANDBY) ||
                (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
                (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_STANDBY))
            {
                continue;
            }

        }
        if (L2VpnDuplicateCruBuffer (pBuf, &pMsgBuf, u4PktSize) == MPLS_FAILURE)
        {
            return L2VPN_FAILURE;
        }

        if (L2VpnWrEncapAndFwdPkt (pMsgBuf, L2VPN_PWVC_INDEX (pPwVcEntry),
                                   u4IfIndex) == L2VPN_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);
            pMsgBuf = NULL;
        }
    }
    /* Release the original buffer since a new buffer is sent on each PW */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return L2VPN_SUCCESS;
}

#ifdef HVPLS_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnMplsPktVplsFwd                                        */
/*                                                                           */
/* Description  : Scan VPLS Entry of same VPLS Instance pseudowires and      */
/*                forward MPLS packets.                                      */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer that was received         */
/*              : u4VplsInstance - VplsInstance for incoming Packet          */
/*                u4IfIndex  - Interface on which the packet has come        */
/*                u4PwVcIndex - Incoming PW Index                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS or L2VPN_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2VpnMplsPktVplsFwd (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VplsInstance,
                     UINT4 u4IfIndex, UINT4 u4PwVcIndex)
{
    UINT4               u4PktSize;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pMsgBuf = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsInstance);
    if (pVplsEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_VC_FLAG, "No PW is created for this instance");
        return L2VPN_FAILURE;
    }
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
    {
        pPwVcEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

        if (L2VPN_PWVC_INDEX (pPwVcEntry) != u4PwVcIndex)
        {
            /*when the packet is coming from Mesh PW, forwarding to Spoke PW only */
            if (L2VpnIsSpokePw (u4PwVcIndex) == L2VPN_FALSE)
            {
                if (L2VpnIsSpokePw (pPwVcEntry->u4PwVcIndex) != L2VPN_TRUE)
                {
                    L2VPN_DBG1 (L2VPN_DBG_ALL_PKT_FLAG,
                                "PwIndex=%d is a Mesh Pw,Packet not Sent to Mesh Pw\t\n",
                                pPwVcEntry->u4PwVcIndex);
                    continue;
                }
                else
                {
                    if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                         L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
                        (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                         L2VPN_PWVC_STATUS_STANDBY) ||
                        (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &
                         L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
                        (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &
                         L2VPN_PWVC_STATUS_STANDBY))
                    {
                        continue;
                    }
                }
            }
            if ((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
                (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_STANDBY) ||
                (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_NOT_FORWARDING) ||
                (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) &
                 L2VPN_PWVC_STATUS_STANDBY))
            {
                continue;
            }
            if (L2VpnDuplicateCruBuffer (pBuf, &pMsgBuf, u4PktSize) ==
                MPLS_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_DBG_VC_FLAG,
                           "Unable to Create CRU Buffer\n");
                return L2VPN_FAILURE;
            }
            if (L2VpnWrEncapAndFwdPkt (pMsgBuf, L2VPN_PWVC_INDEX (pPwVcEntry),
                                       u4IfIndex) == L2VPN_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pMsgBuf, FALSE);
                pMsgBuf = NULL;
            }
        }
    }
    /* Release the original buffer since a new buffer is sent on each PW */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return L2VPN_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnWrEncapAndFwdPkt                                      */
/*                                                                           */
/* Description  : This is the wrapper routine for L2VpnEncapAndFwdPkt.       */
/*                In case of ELPS 1+1 mode, the traffic should be forwarded  */
/*                into protection path too. This part is handled here.       */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*              : u4PwVcIndex- Index to the pseudowire on which packet to    */
/*                             be transmited out                             */
/*                u4IfIndex  - Interface on which the packet has come        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS or L2VPN_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2VpnWrEncapAndFwdPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PwVcIndex,
                       UINT4 u4IfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tL2vpnRedundancyEntry *pRgGrp = NULL;
    tCRU_BUF_CHAIN_HEADER *pBkpBuf = NULL;
    UINT4               u4PktSize = 0;
    UINT4               u4OperPwIdx = 0;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);
    if (pPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "PW Entry DOESN'T exist for this PwIndex\n");
        return L2VPN_FAILURE;
    }
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    /* Check the Protection Status of the PW.
     * If it is PROT_IN_USE, get the Protection PW VC Entry from Backup VC Id
     * of Working PW VC Entry.
     *
     * If the protection status is in use, the traffic should take the protection
     * path instead of working path. */
    if (pPwVcEntry->u1ElpsMode == ELPS_PG_ARCH_1_PLUS_1)
    {
        /* Copy buffer to tx in protection path too.
         * This duplicate buffer piece is added here because
         * after sending the pBuf into L2VpnEncapAndFwdPkt, the pBuf
         * will be modified.
         */
        if (L2VpnDuplicateCruBuffer (pBuf, &pBkpBuf, u4PktSize) == MPLS_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "Duplicating buffer for Protection path failed.\n");
            return L2VPN_FAILURE;
        }
        /* Send the traffic on working path. Check is added in case of "working 
         * path failure" condition, the traffic should not flow on the working path
         */
        if (pPwVcEntry->u1ProtStatus != LOCAL_PROT_IN_USE)
        {
            if (L2VpnEncapAndFwdPkt (pBuf, pPwVcEntry, u4IfIndex) ==
                L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Working path PW for ELPS 1+1 is not able to fwd the pkt. "
                            "PW VC with PW Index: %d\n", u4PwVcIndex);
                CRU_BUF_Release_MsgBufChain (pBkpBuf, FALSE);
                pBkpBuf = NULL;
                return L2VPN_FAILURE;
            }
        }
        /* Working path is not available. So release the pBuf which is 
         * dedicated for working path and continuing to send the traffic 
         * to protection path.
         */
        else
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = NULL;
        }

        /* Send the traffic on protection path. Check is added in case of
         * "protection path failure" condition, the traffic should not 
         * flow on the protection path.
         */
        if ((pPwVcEntry->u1ProtStatus == LOCAL_PROT_AVAIL) ||
            (pPwVcEntry->u1ProtStatus == LOCAL_PROT_IN_USE))
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pPwVcEntry->u4BkpPwVcID);

            if (pPwVcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Protection PW VC does not exist for the working "
                            "PW VC with PW Index: %d\n", u4PwVcIndex);
                CRU_BUF_Release_MsgBufChain (pBkpBuf, FALSE);
                pBkpBuf = NULL;
                /* Here L2VPN_SUCCESS is returned, so that pkt sent on working
                 * path should not be released.
                 */
                return L2VPN_SUCCESS;
            }

            if (L2VpnEncapAndFwdPkt (pBkpBuf, pPwVcEntry,
                                     u4IfIndex) == L2VPN_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Protection path PW for ELPS 1+1 is not able to "
                            "fwd the pkt. PW VC with PW Index: %d\n",
                            u4PwVcIndex);
                CRU_BUF_Release_MsgBufChain (pBkpBuf, FALSE);
                pBkpBuf = NULL;
                /* Here L2VPN_SUCCESS is returned, so that pkt sent on working
                 * path should not be released.
                 */
                return L2VPN_SUCCESS;
            }
        }
        else
        {
            CRU_BUF_Release_MsgBufChain (pBkpBuf, FALSE);
            pBkpBuf = NULL;
        }
        return L2VPN_SUCCESS;
    }
    else
    {

        if (pPwVcEntry->u1ProtStatus == LOCAL_PROT_IN_USE)
        {
            pPwVcEntry = L2VpnGetPwVcEntryFromVcId (pPwVcEntry->u4BkpPwVcID);

            if (pPwVcEntry == NULL)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "Protection PW VC does not exist for the working "
                            "PW VC with PW Index: %d\n", u4PwVcIndex);
                return L2VPN_FAILURE;
            }
        }
        else
        {
            pRgPw = L2VpnGetRedundancyPwEntryByPwIndex
                (L2VPN_PWVC_INDEX (pPwVcEntry));

            if (pRgPw != NULL)
            {
                pRgGrp = L2VpnGetRedundancyEntryByIndex (pRgPw->u4RgIndex);
                if (pRgGrp == NULL)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "Pseudowire is not the part of RG Group.\n");
                    return L2VPN_FAILURE;
                }

                if ((pRgGrp->pOperActivePw != NULL) &&
                    (pRgGrp->pOperActivePw->b1IsLocal == TRUE))
                {
                    if (pRgGrp->pOperActivePw->pRgParent != NULL)
                    {
                        u4OperPwIdx = ((tL2vpnRedundancyPwEntry *)
                                       pRgGrp->pOperActivePw->pRgParent)->
                            u4PwIndex;
                        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4OperPwIdx);
                    }
                }
                if (pPwVcEntry == NULL)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "PW VC Entry does not exists.");
                    return L2VPN_FAILURE;
                }
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) != L2VPN_PWVC_OPER_UP)
                {
                    return L2VPN_FAILURE;
                }
            }
        }

        return (L2VpnEncapAndFwdPkt (pBuf, pPwVcEntry, u4IfIndex));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnEncapAndFwdPkt                                        */
/*                                                                           */
/* Description  : Adds the MPLS header(s) to the given packet and sends on   */
/*                the destination port(L3 vlan interface/router port) of     */
/*                the pseudowire                                             */
/*                                                                           */
/* Input        : pBuf - Pointer to the CRU buffer of the packet received    */
/*              : pPwVcEntry - Pointer to the pseudowire on which packet to  */
/*                             be transmited out
 *                u4IfIndex  - Interface on which the packet has come        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS or L2VPN_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2VpnEncapAndFwdPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tPwVcEntry * pPwVcEntry,
                     UINT4 u4IfIndex)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcMplsTeTnlIndex *pTeTnlIndex = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
    tArpQMsg            ArpQMsg;
    UINT4               u4IpPort = 0;
    UINT4               u4LclLsr;
    UINT4               u4PeerAddr;
    UINT4               u4DstIntfIdx = 0;
    UINT4               u4Tmp = 0;
    INT4                i4Retval = 0;
    eDirection          Direction = MPLS_DIRECTION_ANY;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;
    UINT2               u2ProtocolId = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1EncapType;
    UINT1               u1IfType = 0;
    UINT1               au1DstMac[MAC_ADDR_LEN];
    UINT1               au1NewPktHdr[MAX_MPLS_HEADER_LENGTH_IN_PACKET];
#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            IpAddr;
#endif

    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
#ifdef MPLS_IPV6_WANTED
    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
#endif

    if (pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_OPER_UP)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "-E- PW is NOT operationally UP\n");
        return L2VPN_FAILURE;
    }
    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
    pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

    /* Obtain destination ip and l3 vlan interface */
    if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) == L2VPN_MPLS_TYPE_TE)
    {
        pTeTnlIndex = &pMplsTnlEntry->unTnlInfo.TeInfo;
        MEMCPY ((UINT1 *) &u4LclLsr,
                (UINT1 *) &pTeTnlIndex->TnlLclLSR, IPV4_ADDR_LENGTH);
        MEMCPY ((UINT1 *) &u4PeerAddr,
                (UINT1 *) &pTeTnlIndex->TnlPeerLSR, IPV4_ADDR_LENGTH);
        if (TeGetTunnelInfoByPrimaryTnlInst (pTeTnlIndex->u4TnlIndex,
                                             pTeTnlIndex->u2TnlInstance,
                                             OSIX_NTOHL (u4LclLsr),
                                             OSIX_NTOHL (u4PeerAddr),
                                             &pTeTnlInfo) == TE_FAILURE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "-E- Egress Tunnel DOESN'T exist\n");
            return L2VPN_FAILURE;
        }
        if (TE_TNL_OPER_STATUS (pTeTnlInfo) != TE_OPER_UP)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "-E- Egress Tunnel DOWN\n");
            return L2VPN_FAILURE;
        }
        /* Take the backup tunnel if the primary tunnel is down
         * and protection in use */
        if (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) == LOCAL_PROT_IN_USE)
        {
            MEMCPY (&u4TnlIngressId, pTeTnlInfo->BkpTnlIngressLsrId,
                    sizeof (UINT4));
            u4TnlIngressId = OSIX_NTOHL (u4TnlIngressId);

            MEMCPY (&u4TnlEgressId, pTeTnlInfo->BkpTnlEgressLsrId,
                    sizeof (UINT4));
            u4TnlEgressId = OSIX_NTOHL (u4TnlEgressId);

            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "find backup for tnl: %u %u\n", pTeTnlInfo->u4TnlIndex,
                        pTeTnlInfo->u4TnlInstance);

            L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                        "u4TnlIngressId: 0x%x 0x%x\n", u4TnlIngressId,
                        u4TnlEgressId);

            pTeTnlInfo = TeGetTunnelInfo
                (pTeTnlInfo->u4BkpTnlIndex, pTeTnlInfo->u4BkpTnlInstance,
                 u4TnlIngressId, u4TnlEgressId);
            if (pTeTnlInfo == NULL)
            {
                /* Backup tunnel is not ready */
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "-E- Protected tunnel is down and "
                           "Backup tunnel is not ready\n");
                return L2VPN_FAILURE;
            }
        }

        TeCmnExtGetDirectionFromTnl (pTeTnlInfo, &Direction);

        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              Direction);
        if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "-E- Outgoing interface Info."
                       "does not exist\n");
            return L2VPN_FAILURE;
        }
        u4PeerAddr = pXcEntry->pOutIndex->NHAddr.u4_addr[0];
        u4DstIntfIdx = pXcEntry->pOutIndex->u4IfIndex;
        if (MplsGetL3Intf (u4DstIntfIdx, &u4Tmp) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4DstIntfIdx);
            return L2VPN_FAILURE;
        }
        u4DstIntfIdx = u4Tmp;
        MEMCPY (au1DstMac, pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);
    }
    else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pPwVcMplsEntry) ==
             L2VPN_MPLS_TYPE_NONTE)
    {
        pXcEntry =
            MplsGetXCEntryByDirection (pMplsTnlEntry->unTnlInfo.NonTeInfo.
                                       u4LsrXcIndex, MPLS_DEF_DIRECTION);
        if (pXcEntry == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "-E- Corresponding LSP XC entry NOT FOUND\n");
            return L2VPN_FAILURE;
        }
        if (pXcEntry->pOutIndex == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "-E- Outgoing interface Info."
                       "does not exist\n");
            return L2VPN_FAILURE;
        }
        u4PeerAddr = pXcEntry->pOutIndex->NHAddr.u4_addr[0];
        u4DstIntfIdx = pXcEntry->pOutIndex->u4IfIndex;
        if (MplsGetL3Intf (u4DstIntfIdx, &u4Tmp) == MPLS_FAILURE)
        {
            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                        "Interface %d do not exist\n", u4DstIntfIdx);
            return L2VPN_FAILURE;
        }

        u4DstIntfIdx = u4Tmp;
        MEMCPY (au1DstMac, pXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);
    }
    else                        /* VC-ONLY */
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
            NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst,
                         &(pPwVcEntry->PeerAddr.Ip6Addr));
            NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

            if (NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                                 &NetIpv6RtInfo) != NETIPV6_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "-E- FAILED to get the route\n");
                return L2VPN_FAILURE;
            }
            if (NetIpv6GetCfaIfIndexFromPort (NetIpv6RtInfo.u4Index,
                                              &u4DstIntfIdx) == NETIPV6_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "-E- FAILED to get the route\n");
                return L2VPN_FAILURE;
            }
            if (NetIpv6RtInfo.i1Proto != CIDR_LOCAL_ID)
            {
                MEMCPY (&IpAddr, &NetIpv6RtInfo.NextHop, IPV6_ADDR_LENGTH);
            }
#ifndef LNXIPV6_WANTED
            if ((NetIpv6Nd6Resolve
                 (u4DstIntfIdx, &(pPwVcEntry->PeerAddr.Ip6Addr),
                  au1DstMac)) == NETIPV6_FAILURE)
            {
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            " -E- Arp Resolve Queue Posting failed for Next Hop %s\n",
                            Ip6PrintAddr (&IpAddr));
                return L2VPN_FAILURE;
            }
#endif
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY ((UINT1 *) &u4PeerAddr, &pPwVcEntry->PeerAddr,
                    IPV4_ADDR_LENGTH);
            u4PeerAddr = OSIX_HTONL (u4PeerAddr);
            RtQuery.u4DestinationIpAddress = u4PeerAddr;
            RtQuery.u4DestinationSubnetMask = 0xffffffff;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "-E- FAILED to get the route\n");
                return L2VPN_FAILURE;
            }
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4DstIntfIdx) == NETIPV4_FAILURE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "-E- FAILED to get the route\n");
                return L2VPN_FAILURE;
            }

            if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
            {
                u4PeerAddr = NetIpRtInfo.u4NextHop;
            }
            /* Get the destination MAC for the given Peer address */
            if (ArpResolve (u4PeerAddr, (INT1 *) au1DstMac, &u1EncapType) ==
                ARP_FAILURE)
            {
                if (NetIpv4GetPortFromIfIndex (u4DstIntfIdx, &u4IpPort) ==
                    NETIPV4_FAILURE)
                {
                    return L2VPN_FAILURE;
                }
                ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
                ArpQMsg.u2Port = (UINT2) u4IpPort;
                ArpQMsg.u4IpAddr = u4PeerAddr;
                /* Trigger ARP module for Resolving NextHopAddress */

                if (ARP_FAILURE == ArpEnqueuePkt (&ArpQMsg))
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "failed at ArpEnqueuePkt\n");
                }
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "-E- Egress Tunnel DOWN\n");
                return L2VPN_FAILURE;
            }
        }
    }

    CfaGetIfaceType (u4IfIndex, &u1IfType);
    if (u1IfType != CFA_PSEUDO_WIRE)
    {
        /* Identify the ENET entry corresponding to the incoming pkt */
        pPwVcEnetServSpecEntry = L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);
        u4Tmp = CFA_VLAN_TAG_OFFSET + CFA_VLAN_PROTOCOL_SIZE;
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanId, u4Tmp, 2) ==
            CRU_FAILURE)
        {
            return L2VPN_FAILURE;
        }
        u2VlanId = OSIX_NTOHS (u2VlanId);
        u2VlanId = 0x0fff & u2VlanId;

        if (pPwVcEnetServSpecEntry != NULL)
        {
            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry),
                          pPwVcEnetEntry, tPwVcEnetEntry *)
            {
                if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                    L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                {
                    if (u4IfIndex ==
                        (UINT4) L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry))
                    {
                        break;
                    }
                }
                else if (L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry) ==
                         L2VPN_ZERO)
                {
                    if (u2VlanId == L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                    {
                        break;
                    }
                }
                else if ((u2VlanId ==
                          L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                         && (u4IfIndex ==
                             (UINT4)
                             L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)))
                {
                    break;
                }
            }

            if (pPwVcEnetEntry == NULL)
            {
                return L2VPN_FAILURE;
            }
        }

        /* Native Service Processing (NSP) AC -> PSN direction */
    /***********************************************************************
      | Pw Type | Enet Vlan Mode | CE - PE link (PSN bound direction) Remarks | 
      | ********************************************************************* |
      | Tagged  | Add Vlan       | Add Vlan possible, only if outer vlan tag  |
      |         |                | is not service delimiting.                 | 
      | --------------------------------------------------------------------- |
      | Raw     |    -           | No Vlan mode is applicable                 | 
     ************************************************************************/
        /* Do PW encapsulation based on the pwType and pwEnetVlanMode */
        if ((pPwVcEnetEntry != NULL) &&
            (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH_VLAN))
        {
            /* Tagged mode */
            if (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                L2VPN_VLANMODE_ADDVLAN)
            {
                if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ProtocolId,
                                               CFA_VLAN_TAG_OFFSET,
                                               CFA_VLAN_PROTOCOL_SIZE) ==
                    CRU_FAILURE)
                {
                    return L2VPN_FAILURE;
                }
                u2ProtocolId = OSIX_NTOHS (u2ProtocolId);
                if (u2ProtocolId != VLAN_PROVIDER_PROTOCOL_ID)
                {
                    u2ProtocolId = L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
                    VlanTagFrame (pBuf, u2ProtocolId, 0);
                }
            }
            /* Outer Vlan Tag is removed if it matches with configured remove 
             * vlan value */
            else if (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                     L2VPN_VLANMODE_REMOVEVLAN)
            {
                if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ProtocolId,
                                               (UINT4) CFA_VLAN_TAG_OFFSET,
                                               (UINT4) CFA_VLAN_PROTOCOL_SIZE)
                    == CRU_FAILURE)
                {
                    return L2VPN_FAILURE;
                }
                u2ProtocolId = OSIX_NTOHS (u2ProtocolId);
                if ((u2ProtocolId == VLAN_PROVIDER_PROTOCOL_ID) ||
                    (u2ProtocolId == VLAN_PROTOCOL_ID))
                {
                    if (u2VlanId == L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry))
                    {
                        VlanUnTagFrame (pBuf);
                    }
                }
            }
        }
        else if ((L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH) &&
                 (pPwVcEnetEntry != NULL))
        {
            /* Raw mode */
            /* Service delimiting tag must not be sent on the PW and hence removed */
            if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ProtocolId,
                                           CFA_VLAN_TAG_OFFSET,
                                           CFA_VLAN_PROTOCOL_SIZE) ==
                CRU_FAILURE)
            {
                return L2VPN_FAILURE;
            }
            u2ProtocolId = OSIX_NTOHS (u2ProtocolId);
            if ((u2ProtocolId == VLAN_PROVIDER_PROTOCOL_ID) ||
                (u2ProtocolId == VLAN_PROTOCOL_ID))
            {
                VlanUnTagFrame (pBuf);
            }
        }
    }

    /* This part of code should not be hit in normal scenario
     * outgoing interface must be normal IVR interface or normal
     * Router ports , it cannot be any other interfaces (including VPLS vlan as IVR interfaces
     * ,Pseudowire as router Ports)*/
    CfaGetIfaceType (u4DstIntfIdx, &u1IfType);
    if (u1IfType == CFA_L3IPVLAN)
    {
        u2VlanId = 0;

        if (CfaGetVlanId (u4DstIntfIdx, &u2VlanId) != CFA_SUCCESS)
        {
            L2VPN_DBG1 (L2VPN_DBG_DBG_VC_FLAG,
                        "cfaGetVlanId Failed for L3"
                        "interface %d\r\n", u4DstIntfIdx);
        }

        if (u2VlanId > VLAN_VFI_MIN_ID)
        {
            L2VPN_DBG (L2VPN_DBG_DBG_VC_FLAG,
                       "Outgoing interface cannot be VPLS VLAN \n");
            return L2VPN_FAILURE;
        }
    }
    else if (u1IfType == CFA_PSEUDO_WIRE)
    {
        L2VPN_DBG (L2VPN_DBG_DBG_VC_FLAG,
                   "Outgoing interface cannot be Pseudowire interface \n");
        return L2VPN_FAILURE;
    }

    /* Add tnl labels and vc lbls */
    i4Retval = L2VpnAddMplsHdrs (au1NewPktHdr, pPwVcEntry, pXcEntry);
    u4Tmp = (UINT4) i4Retval;
    CRU_BUF_Prepend_BufChain (pBuf, au1NewPktHdr, u4Tmp);
    if (u4DstIntfIdx == L2VPN_ZERO)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "-E- Outgoing interface is zero\n");
        return L2VPN_FAILURE;
    }
#ifdef CFA_WANTED
    /* Send the labeled Packet to CFA Task for transmission */

    if (CfaHandlePktFromMpls (pBuf, u4DstIntfIdx, MPLS_TO_CFA,
                              au1DstMac, CFA_LINK_UCAST) == CFA_FAILURE)
    {
        return L2VPN_FAILURE;
    }
#endif
    pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
    if (L2VPN_PWVC_PERF_CURRENT_INTERVAL < L2VPN_PWVC_MIN_PERF_INTV)
    {
        L2VPN_PWVC_PERF_CURRENT_INTERVAL = L2VPN_PWVC_MIN_PERF_INTV;
    }
    FSAP_U8_INC (&pPwVcPerfInfo->aPwVcPerfIntervalInfo
                 [L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1].OutHCPkts);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnAddMplsHdrs                                           */
/*                                                                           */
/* Description  : Identify the number of MPLS header(s) to be added for the  */
/*                given pseudowire outgoing packet and construct,  store     */
/*                them in the given buffer                                   */
/*                                                                           */
/* Input        : pPwVcEntry - Pointer to the pseudowire on which packet to  */
/*                             be transmited out                             */
/*                pXcEntry - Pointer to the cross connect entry if MplsType  */
/*                           is mpslTe/mplsNonTe otherwise it must be NULL   */
/*                                                                           */
/* Output       : pBuf - Pointer to the buffer in which headers to be stored */
/*                                                                           */
/* Returns      : Returns the length of the header(s) stored in pBuf         */
/*                                                                           */
/*****************************************************************************/
INT4
L2VpnAddMplsHdrs (UINT1 *pBuf, tPwVcEntry * pPwVcEntry, tXcEntry * pXcEntry)
{
    UINT2               u2NumHdrs = 0;
    UINT2               u2Len = 0;
    tMplsHdr            MplsHdr;
    tLblEntry          *pLblEntry = NULL;
    tLblStkEntry       *pLblStkEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;

    /* There must be atleast one MPLS header with VC label and the other
     * header(s) with tunnel label is optional
     */
    pPwVcMplsEntry = (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

    if (pXcEntry != NULL)
    {
        /* TE/NON-TE case so put one more MPLS header */
        pLblStkEntry = pXcEntry->mplsLabelIndex;

        if (pXcEntry->pOutIndex->u4Label != MPLS_IMPLICIT_NULL_LABEL)
        {
            u2NumHdrs++;
            /* Insert hdr1 (top) */
            MplsHdr.u4Lbl = pXcEntry->pOutIndex->u4Label;
            MplsHdr.Exp = 0;
            /* ..not the stack bottom since vclbl/lbl stack is present */
            MplsHdr.SI = 0;
            MplsHdr.Ttl = MPLS_DEF_TTL - 1;    /* Since ingress LER */

            MplsPutMplsHdr (pBuf, &MplsHdr);
        }
    }

    /* Add a MPLS header foreach active label stack entry */
    if (pLblStkEntry != NULL)
    {
        TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
        {
            if (pLblEntry->u1RowStatus == ACTIVE)
            {

                if (pLblEntry->u4Label != MPLS_IMPLICIT_NULL_LABEL)
                {
                    u2NumHdrs++;
                    MplsHdr.u4Lbl = pLblEntry->u4Label;
                    MplsHdr.Exp = 0;
                    MplsHdr.SI = 0;
                    MplsHdr.Ttl = MPLS_DEF_TTL;
                    /* Move Offset and write Mpls header accordingly */
                    MplsPutMplsHdr ((pBuf + (u2NumHdrs - 1) * MPLS_HDR_LEN),
                                    &MplsHdr);
                }
            }
        }
    }

    /* Insert VC lbl - hdr2 (last) */
    u2NumHdrs++;

    MplsHdr.u4Lbl = pPwVcEntry->u4OutVcLabel;
    MplsHdr.Exp = (L2VPN_LOWNIB & pPwVcMplsEntry->i1ExpModeAndBits);
    MplsHdr.SI = 1;                /* bottom of stack */
    MplsHdr.Ttl = pPwVcMplsEntry->u1Ttl;
    /* Move Offset and write Mpls header accordingly */
    MplsPutMplsHdr ((pBuf + (u2NumHdrs - 1) * MPLS_HDR_LEN), &MplsHdr);

    u2Len = (UINT2) u2NumHdrs *(UINT2) MPLS_HDR_LEN;
    /* Adding PW Ethernet control word */
    if ((gi4MplsSimulateFailure == L2VPN_SIM_FAILURE_CTRLW_MANDATORY) &&
        (pPwVcEntry->i1CwStatus == L2VPN_PWVC_CWPRESENT))
    {
        pPwVcEntry->u2SeqNumber++;
        /* flags and length */
        PTR_ASSIGN2 ((pBuf + u2Len), 0);
        /* Sequence number */
        PTR_ASSIGN2 ((pBuf + u2Len) + 2, pPwVcEntry->u2SeqNumber);

        u2Len = (UINT2) (u2Len + 4);
    }
    return u2Len;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnDuplicateCruBuffer                                    */
/*                                                                           */
/* Description  : This function makes a copy the CRU buffer by making        */
/*                use of the linear buffer and the FSAP calls.               */
/* Input(s)     : pBuf - Source CRU buffer pointer                           */
/*                pDupBuf - Pointer to the destination CRU pointer           */
/*                u4PktSize - Size of the packet to be copied to the dest    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MPLS_SUCCESS / MPLS_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
L2VpnDuplicateCruBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize)
{
    UINT4               u4MaxNewHdrsLen;
    UINT1              *pu1DataBuf = NULL;

    u4MaxNewHdrsLen =
        CFA_ENET_V2_HEADER_SIZE + 2 * VLAN_TAG_LEN + 12 * MPLS_HDR_LEN;
    /* Allocate the Buffer chain for the Duplicate buffer */
    *pDupBuf = CRU_BUF_Allocate_MsgBufChain ((u4PktSize + u4MaxNewHdrsLen),
                                             u4MaxNewHdrsLen);
    if (*pDupBuf == NULL)
    {
        return MPLS_FAILURE;
    }

    /* Allocate memory for the linear buffer */

    if ((pu1DataBuf = MemAllocMemBlk (L2VPN_DATA_BUFFER)) == NULL)
    {
        CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
        return MPLS_FAILURE;
    }

    MEMSET (pu1DataBuf, 0, u4PktSize);

    /* Copy from the pBuf to the Linear buffer */
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataBuf, 0, u4PktSize)
        == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
        MemReleaseMemBlock (L2VPN_DATA_BUFFER, (UINT1 *) pu1DataBuf);
        return MPLS_FAILURE;
    }

    /* Copy from linear buffer to the CRU buffer */
    if (CRU_BUF_Copy_OverBufChain (*pDupBuf, pu1DataBuf, 0, u4PktSize)
        == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
        MemReleaseMemBlock (L2VPN_DATA_BUFFER, (UINT1 *) pu1DataBuf);
        return MPLS_FAILURE;
    }

    MemReleaseMemBlock (L2VPN_DATA_BUFFER, (UINT1 *) pu1DataBuf);
    return MPLS_SUCCESS;
}
