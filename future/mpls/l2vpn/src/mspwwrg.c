/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mspwwrg.c,v 1.1 2010/11/17 14:03:28 siva Exp $
 *
 * Description: This file contains the  CLI related routines 
 *********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "mspwlwg.h"
#include "mspwwrg.h"
#include "mspwdb.h"

VOID
RegisterFSMSPW ()
{
    SNMPRegisterMib (&fsmspwOID, &fsmspwEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmspwOID, (const UINT1 *) "fsmspw");
}

VOID
UnRegisterFSMSPW ()
{
    SNMPUnRegisterMib (&fsmspwOID, &fsmspwEntry);
    SNMPDelSysorEntry (&fsmspwOID, (const UINT1 *) "fsmspw");
}

INT4
FsMsPwMaxEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMsPwMaxEntries (&(pMultiData->u4_ULongValue)));
}

INT4
FsMsPwMaxEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMsPwMaxEntries (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMsPwMaxEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMsPwMaxEntries (pMultiData->u4_ULongValue));
}

INT4
FsMsPwMaxEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMsPwMaxEntries (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMsPwConfigTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMsPwConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMsPwConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMsPwOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMsPwConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMsPwOperStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsMsPwRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMsPwConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMsPwRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsMsPwRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMsPwRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMsPwRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMsPwRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsMsPwConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMsPwConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
