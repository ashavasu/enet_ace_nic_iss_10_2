/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsVplslw.c,v 1.1 2014/03/09 13:30:20 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "l2vpincs.h"
#include "mplscli.h"

/* LOW LEVEL Routines for Table : VplsBgpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsBgpConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceVplsBgpConfigTable(UINT4 u4VplsConfigIndex)
{
    if ((u4VplsConfigIndex == 0) ||
        (u4VplsConfigIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexVplsBgpConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexVplsBgpConfigTable(UINT4 *pu4VplsConfigIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4Index;

    for (u4Index = 1; u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry != NULL)
        {
            *pu4VplsConfigIndex = L2VPN_VPLS_INDEX (pVplsEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexVplsBgpConfigTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexVplsBgpConfigTable(UINT4 u4VplsConfigIndex ,
                                       UINT4 *pu4NextVplsConfigIndex )
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4Index;

    if (u4VplsConfigIndex >= gL2VpnGlobalInfo.u4MaxVplsEntries)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = u4VplsConfigIndex;
         u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry != NULL)
        {
            if (L2VPN_VPLS_INDEX (pVplsEntry) > u4VplsConfigIndex)
            {
                *pu4NextVplsConfigIndex = L2VPN_VPLS_INDEX (pVplsEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsBgpConfigVERangeSize
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                retValVplsBgpConfigVERangeSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpConfigVERangeSize(UINT4 u4VplsConfigIndex , 
                                    UINT4 *pu4RetValVplsBgpConfigVERangeSize)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4VplsConfigIndex);

    if (pVplsEntry != NULL)
    {
        *pu4RetValVplsBgpConfigVERangeSize = 
                                      L2VPN_VPLS_VE_RANGE_SIZE(pVplsEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsBgpConfigVERangeSize
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                setValVplsBgpConfigVERangeSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetVplsBgpConfigVERangeSize(UINT4 u4VplsConfigIndex , 
                                    UINT4 u4SetValVplsBgpConfigVERangeSize)
{
	UNUSED_PARAM(u4VplsConfigIndex);
	UNUSED_PARAM(u4SetValVplsBgpConfigVERangeSize);
    
    return SNMP_FAILURE;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsBgpConfigVERangeSize
 Input       :  The Indices
                VplsConfigIndex

                The Object 
                testValVplsBgpConfigVERangeSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2VplsBgpConfigVERangeSize(UINT4 *pu4ErrorCode , 
                                       UINT4 u4VplsConfigIndex , 
                                       UINT4 u4TestValVplsBgpConfigVERangeSize)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(u4VplsConfigIndex);
	UNUSED_PARAM(u4TestValVplsBgpConfigVERangeSize);
    
    return SNMP_FAILURE;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsBgpConfigTable
 Input       :  The Indices
                VplsConfigIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2VplsBgpConfigTable(UINT4 *pu4ErrorCode, 
                                tSnmpIndexList *pSnmpIndexList, 
                                tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : VplsBgpVETable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsBgpVETable
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceVplsBgpVETable(UINT4 u4VplsConfigIndex , 
                                            UINT4 u4VplsBgpVEId)
{
    if ( ( u4VplsConfigIndex == 0 ) ||
         ( u4VplsConfigIndex > gL2VpnGlobalInfo.u4MaxVplsEntries ) )
    {
        return SNMP_FAILURE;
    }

    if ( ( u4VplsBgpVEId == 0 ) ||
         ( u4VplsBgpVEId > L2VPN_VPLS_DEFAULT_NUMBER_OF_SITES ) )
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexVplsBgpVETable
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexVplsBgpVETable(UINT4 *pu4VplsConfigIndex , 
                                    UINT4 *pu4VplsBgpVEId)
{
    tVPLSVeEntry *pVplsVeEntry = NULL;

    pVplsVeEntry = (tVPLSVeEntry *) 
                    RBTreeGetFirst(L2VPN_VPLS_VE_RB_PTR(gpPwVcGlobalInfo));
    if ( NULL == pVplsVeEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4VplsConfigIndex = L2VPN_VPLSVE_VPLS_INSTANCE(pVplsVeEntry);
    *pu4VplsBgpVEId = L2VPN_VPLSVE_VE_ID(pVplsVeEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexVplsBgpVETable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
                VplsBgpVEId
                nextVplsBgpVEId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexVplsBgpVETable(UINT4 u4VplsConfigIndex ,
                                   UINT4 *pu4NextVplsConfigIndex  , 
                                   UINT4 u4VplsBgpVEId ,
                                   UINT4 *pu4NextVplsBgpVEId )
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry *) 
                    RBTreeGetNext(L2VPN_VPLS_VE_RB_PTR(gpPwVcGlobalInfo),
                                  &VplsVeEntry,
                                  NULL);
    if ( NULL == pVplsVeEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4NextVplsConfigIndex = L2VPN_VPLSVE_VPLS_INSTANCE(pVplsVeEntry);
    *pu4NextVplsBgpVEId = L2VPN_VPLSVE_VE_ID(pVplsVeEntry);

    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsBgpVEName
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                retValVplsBgpVEName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpVEName(UINT4 u4VplsConfigIndex , 
                         UINT4 u4VplsBgpVEId , 
                         tSNMP_OCTET_STRING_TYPE * pRetValVplsBgpVEName)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpVEName :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    pRetValVplsBgpVEName->i4_Length = 
                       (INT4)STRLEN(L2VPN_VPLSVE_VE_NAME(pVplsVeEntry));

    MEMCPY(pRetValVplsBgpVEName->pu1_OctetList,
           L2VPN_VPLSVE_VE_NAME(pVplsVeEntry),
           pRetValVplsBgpVEName->i4_Length);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetVplsBgpVEPreference
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                retValVplsBgpVEPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpVEPreference(UINT4 u4VplsConfigIndex , 
                               UINT4 u4VplsBgpVEId , 
                               UINT4 *pu4RetValVplsBgpVEPreference)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpVEPreference :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pu4RetValVplsBgpVEPreference = L2VPN_VPLSVE_VE_PREFRENCE(pVplsVeEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetVplsBgpVERowStatus
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                retValVplsBgpVERowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpVERowStatus(UINT4 u4VplsConfigIndex , 
                              UINT4 u4VplsBgpVEId , 
                              INT4 *pi4RetValVplsBgpVERowStatus)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpVERowStatus :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpVERowStatus = L2VPN_VPLSVE_ROW_STATUS(pVplsVeEntry);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetVplsBgpVEStorageType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                retValVplsBgpVEStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpVEStorageType(UINT4 u4VplsConfigIndex , 
                                UINT4 u4VplsBgpVEId , 
                                INT4 *pi4RetValVplsBgpVEStorageType)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhGetVplsBgpVEStorageType :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    *pi4RetValVplsBgpVEStorageType = L2VPN_VPLSVE_STORAGE_TYPE(pVplsVeEntry);

    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVplsBgpVEName
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                setValVplsBgpVEName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetVplsBgpVEName(UINT4 u4VplsConfigIndex , 
                         UINT4 u4VplsBgpVEId , 
                         tSNMP_OCTET_STRING_TYPE *pSetValVplsBgpVEName)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpVEName :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    MEMCPY(L2VPN_VPLSVE_VE_NAME(pVplsVeEntry),
           pSetValVplsBgpVEName->pu1_OctetList,
           pSetValVplsBgpVEName->i4_Length);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetVplsBgpVEPreference
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                setValVplsBgpVEPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetVplsBgpVEPreference(UINT4 u4VplsConfigIndex , 
                               UINT4 u4VplsBgpVEId , 
                               UINT4 u4SetValVplsBgpVEPreference)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpVEPreference :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSVE_VE_PREFRENCE(pVplsVeEntry) = u4SetValVplsBgpVEPreference;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetVplsBgpVERowStatus
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                setValVplsBgpVERowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetVplsBgpVERowStatus(UINT4 u4VplsConfigIndex , 
                              UINT4 u4VplsBgpVEId , 
                              INT4 i4SetValVplsBgpVERowStatus)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL && 
         CREATE_AND_WAIT != i4SetValVplsBgpVERowStatus )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpVERowStatus :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    switch(i4SetValVplsBgpVERowStatus)
    {
        case CREATE_AND_WAIT:
            pVplsVeEntry =
                 (tVPLSVeEntry *) MemAllocMemBlk (L2VPN_VPLS_VE_POOL_ID);

            if (pVplsVeEntry == NULL)
            {
                CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhSetVplsBgpVERowStatus :"
                           " Memory Allocate Failed \t\n");
                return SNMP_FAILURE;
            }

            MEMSET(pVplsVeEntry, 0, sizeof(tVPLSVeEntry));
            L2VPN_VPLSVE_VPLS_INSTANCE(pVplsVeEntry) = u4VplsConfigIndex;
            L2VPN_VPLSVE_VE_ID(pVplsVeEntry) = u4VplsBgpVEId;
            L2VPN_VPLSVE_ROW_STATUS(pVplsVeEntry) = NOT_READY;
            L2VPN_VPLSVE_STORAGE_TYPE(pVplsVeEntry) = L2VPN_STORAGE_VOLATILE;
            if (RBTreeAdd (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                       (tRBElem *) pVplsVeEntry) == RB_FAILURE)
            {
                CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_CREATE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                          "nmhSetVplsBgpVERowStatus :"
                          "VPLS VE RBTree Add Failed \t\n");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            L2VPN_VPLSVE_ROW_STATUS(pVplsVeEntry) = ACTIVE;
            break;
        case NOT_IN_SERVICE:
            L2VPN_VPLSVE_ROW_STATUS(pVplsVeEntry) = NOT_IN_SERVICE;
            break;
        case DESTROY:
            pVplsVeEntry = (tVPLSVeEntry *)
                           RBTreeRem (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                                      (tRBElem *) pVplsVeEntry); 
            if ( NULL == pVplsVeEntry )
            {
                CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_DELETE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                          "nmhSetVplsBgpVERowStatus :"
                          "VPLS RD RBTree delete Failed \t\n");
                return SNMP_FAILURE;
            }
       
            if ( MEM_FAILURE == MemReleaseMemBlock(L2VPN_VPLS_VE_POOL_ID,
                                                   (UINT1 *)pVplsVeEntry) )
            {
                CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_DELETE_FAILED);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                          "nmhSetVplsBgpVERowStatus :"
                          "MemRelease failed for L2VPN_VPLS_VE_POOL_ID \t\n");
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetVplsBgpVEStorageType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                setValVplsBgpVEStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetVplsBgpVEStorageType(UINT4 u4VplsConfigIndex , 
                                UINT4 u4VplsBgpVEId , 
                                INT4 i4SetValVplsBgpVEStorageType)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhSetVplsBgpVEStorageType :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSVE_STORAGE_TYPE(pVplsVeEntry) = 
                                    (UINT1)i4SetValVplsBgpVEStorageType;

    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VplsBgpVEName
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                testValVplsBgpVEName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2VplsBgpVEName(UINT4 *pu4ErrorCode , 
                            UINT4 u4VplsConfigIndex , 
                            UINT4 u4VplsBgpVEId , 
                            tSNMP_OCTET_STRING_TYPE *pTestValVplsBgpVEName)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpVEName :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    if ( L2VPN_MAX_VPLS_NAME_LEN < pTestValVplsBgpVEName->i4_Length )
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpVEName :"
                   "string length is more than max value\t\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2VplsBgpVEPreference
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                testValVplsBgpVEPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2VplsBgpVEPreference(UINT4 *pu4ErrorCode , 
                                  UINT4 u4VplsConfigIndex , 
                                  UINT4 u4VplsBgpVEId , 
                                  UINT4 u4TestValVplsBgpVEPreference)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpVEPreference :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    UNUSED_PARAM(u4TestValVplsBgpVEPreference);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2VplsBgpVERowStatus
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                testValVplsBgpVERowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2VplsBgpVERowStatus(UINT4 *pu4ErrorCode , 
                                 UINT4 u4VplsConfigIndex , 
                                 UINT4 u4VplsBgpVEId , 
                                 INT4 i4TestValVplsBgpVERowStatus)
{
    tVPLSVeEntry  VplsVeEntry;
    tVPLSVeEntry  *pVplsVeEntry = NULL;
    tVPLSEntry    *pVplsEntry = NULL;
    INT4          i4RetStatus = SNMP_FAILURE;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ( SNMP_FAILURE == 
         nmhValidateIndexInstanceVplsBgpVETable(u4VplsConfigIndex, 
                                                u4VplsBgpVEId) 
       )
    {
        CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_ID_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpVERowStatus :"
                   "Key not valid\t\n");
        return SNMP_FAILURE;
    }

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL &&
         CREATE_AND_WAIT != i4TestValVplsBgpVERowStatus )
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpVERowStatus :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    switch(i4TestValVplsBgpVERowStatus)
    {
        case CREATE_AND_WAIT:
            if ( pVplsVeEntry != NULL )
            {
                CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_DUPLICATE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpVERowStatus :"
                           "Row already exists\t\n");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            i4RetStatus = L2VpnGetLocalVeIdFromVPLSIndex(u4VplsConfigIndex, 
                                                         &u4VplsBgpVEId);
            if ( L2VPN_SUCCESS == i4RetStatus )
            {
                CLI_SET_ERR(L2VPN_PW_VPLS_CLI_VE_ALREADY_EXISTS);
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpVERowStatus :"
                           "VPLS already associated with some other VeId\t\n");
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case DESTROY:
            pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex(u4VplsConfigIndex);
            if ( NULL != pVplsEntry )
            {
                if ( ACTIVE == L2VPN_VPLS_ROW_STATUS(pVplsEntry) )
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "nmhTestv2VplsBgpVERowStatus :"
                           "Vpls RowStatus is should not be Active\t\n");
                    return SNMP_FAILURE;
                }
            }
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }


    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2VplsBgpVEStorageType
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId

                The Object 
                testValVplsBgpVEStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2VplsBgpVEStorageType(UINT4 *pu4ErrorCode , 
                                   UINT4 u4VplsConfigIndex , 
                                   UINT4 u4VplsBgpVEId , 
                                   INT4 i4TestValVplsBgpVEStorageType)
{
    tVPLSVeEntry VplsVeEntry;
    tVPLSVeEntry *pVplsVeEntry = NULL;

    MEMSET(&VplsVeEntry, 0, sizeof(tVPLSVeEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSVE_VPLS_INSTANCE(&VplsVeEntry) = u4VplsConfigIndex;
    L2VPN_VPLSVE_VE_ID(&VplsVeEntry) = u4VplsBgpVEId;

    pVplsVeEntry = (tVPLSVeEntry * )
                   RBTreeGet (L2VPN_VPLS_VE_RB_PTR (gpPwVcGlobalInfo),
                              (tRBElem *) &VplsVeEntry);
    if ( pVplsVeEntry == NULL )
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "nmhTestv2VplsBgpVEStorageType :"
                   "Row not exists\t\n");
        return SNMP_FAILURE;
    }

    switch (i4TestValVplsBgpVEStorageType)
    {
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VplsBgpVETable
 Input       :  The Indices
                VplsConfigIndex
                VplsBgpVEId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2VplsBgpVETable(UINT4 *pu4ErrorCode, 
                            tSnmpIndexList *pSnmpIndexList, 
                            tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : VplsBgpPwBindTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVplsBgpPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceVplsBgpPwBindTable(UINT4 u4VplsConfigIndex , 
                                                UINT4 u4PwIndex)
{
    if (!((u4VplsConfigIndex >= L2VPN_ONE) &&
          (u4VplsConfigIndex <= L2VPN_MAX_VPLS_ENTRIES)))
    {
        return SNMP_FAILURE;
    }

    if (!((u4PwIndex >= L2VPN_PWVC_INDEX_MINVAL) &&
          (u4PwIndex <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexVplsBgpPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexVplsBgpPwBindTable(UINT4 *pu4VplsConfigIndex , 
                                        UINT4 *pu4PwIndex)
{
    UINT4               u4Index = L2VPN_ZERO;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;

    for (u4Index = 1; u4Index <= L2VPN_MAX_VPLS_ENTRIES; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry == NULL)
        {
            continue;
        }

        pPwVplsNode = (tTMO_SLL_NODE *)
                      TMO_SLL_First ((tTMO_SLL *) (&(pVplsEntry->PwList)));
        if ( NULL == pPwVplsNode )
        {
            continue;
        }

        pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

        *pu4VplsConfigIndex = L2VPN_VPLS_INDEX (pVplsEntry);
        *pu4PwIndex = L2VPN_PWVC_INDEX (pPwEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexVplsBgpPwBindTable
 Input       :  The Indices
                VplsConfigIndex
                nextVplsConfigIndex
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexVplsBgpPwBindTable(UINT4 u4VplsConfigIndex ,
                                       UINT4 *pu4NextVplsConfigIndex  , 
                                       UINT4 u4PwIndex ,
                                       UINT4 *pu4NextPwIndex )
{
    UINT4               u4Index = L2VPN_ZERO;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;

    if (!((u4VplsConfigIndex >= L2VPN_ONE) &&
          (u4VplsConfigIndex <= L2VPN_MAX_VPLS_ENTRIES)))
    {
        return SNMP_FAILURE;
    }

    for (u4Index = u4VplsConfigIndex;
         u4Index <= L2VPN_MAX_VPLS_ENTRIES; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry == NULL)
        {
            continue;
        }

        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

            if ((pPwEntry != NULL)
                && (L2VPN_VPLS_INDEX (pVplsEntry) == u4VplsConfigIndex))
            {
                if ((L2VPN_PWVC_INDEX (pPwEntry) > u4PwIndex))
                {
                    *pu4NextVplsConfigIndex = L2VPN_VPLS_INDEX (pVplsEntry);
                    *pu4NextPwIndex = L2VPN_PWVC_INDEX (pPwEntry);
                    return SNMP_SUCCESS;
                }
            }

        }

        if (L2VPN_VPLS_INDEX (pVplsEntry) > u4VplsConfigIndex)
        {
            pPwVplsNode = (tTMO_SLL_NODE *)
                          TMO_SLL_First ((tTMO_SLL *) (&pVplsEntry->PwList));
            if (pPwVplsNode == NULL)
            {
                continue;
            }

            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);

            *pu4NextVplsConfigIndex = L2VPN_VPLS_INDEX (pVplsEntry);
            *pu4NextPwIndex = L2VPN_PWVC_INDEX (pPwEntry);

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVplsBgpPwBindLocalVEId
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                retValVplsBgpPwBindLocalVEId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpPwBindLocalVEId(UINT4 u4VplsConfigIndex , 
                                  UINT4 u4PwIndex , 
                                  UINT4 *pu4RetValVplsBgpPwBindLocalVEId)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UNUSED_PARAM(u4VplsConfigIndex);

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValVplsBgpPwBindLocalVEId = 
                         L2VPN_PWVC_LOCAL_VE_ID (pPwVcEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetVplsBgpPwBindRemoteVEId
 Input       :  The Indices
                VplsConfigIndex
                PwIndex

                The Object 
                retValVplsBgpPwBindRemoteVEId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetVplsBgpPwBindRemoteVEId(UINT4 u4VplsConfigIndex , 
                                   UINT4 u4PwIndex , 
                                   UINT4 *pu4RetValVplsBgpPwBindRemoteVEId)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UNUSED_PARAM(u4VplsConfigIndex);

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValVplsBgpPwBindRemoteVEId = 
                                  L2VPN_PWVC_REMOTE_VE_ID(pPwVcEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
