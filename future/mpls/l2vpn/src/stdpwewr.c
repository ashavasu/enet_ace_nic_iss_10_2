
/* $Id: stdpwewr.c,v 1.5 2011/10/25 09:29:27 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdpwewr.h"
# include  "stdpwedb.h"
# include  "l2vpincs.h"

INT4
GetNextIndexPwEnetTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwEnetTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwEnetTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTDPWE ()
{
    SNMPRegisterMibWithLock (&stdpweOID, &stdpweEntry, L2vpnLock, L2vpnUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdpweOID, (const UINT1 *) "stdpwe");
}

INT4
PwEnetPwInstanceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
PwEnetPwVlanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetPwVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
PwEnetVlanModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetVlanMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetPortVlanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetPortVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
PwEnetPortIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetPortIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetVcIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetVcIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetPwVlanSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetPwVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiData->u4_ULongValue));

}

INT4
PwEnetVlanModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetVlanMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
PwEnetPortVlanSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetPortVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
PwEnetPortIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetPortIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwEnetVcIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetVcIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
PwEnetRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
PwEnetStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwEnetPwVlanTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetPwVlan (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
PwEnetVlanModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetVlanMode (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
PwEnetPortVlanTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetPortVlan (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
PwEnetPortIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetPortIfIndex (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
PwEnetVcIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetVcIfIndex (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
PwEnetRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
PwEnetStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetStorageType (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
PwEnetTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PwEnetTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexPwEnetMplsPriMappingTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwEnetMplsPriMappingTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwEnetMplsPriMappingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwEnetMplsPriMappingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetMplsPriMappingTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetMplsPriMapping (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
PwEnetMplsPriMappingRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetMplsPriMappingTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetMplsPriMappingRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetMplsPriMappingStorageTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetMplsPriMappingTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetMplsPriMappingStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PwEnetMplsPriMappingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPwEnetMplsPriMapping (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
PwEnetMplsPriMappingRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetPwEnetMplsPriMappingRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
PwEnetMplsPriMappingStorageTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetPwEnetMplsPriMappingStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
PwEnetMplsPriMappingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetMplsPriMapping (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
PwEnetMplsPriMappingRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetMplsPriMappingRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
PwEnetMplsPriMappingStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2PwEnetMplsPriMappingStorageType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
PwEnetMplsPriMappingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PwEnetMplsPriMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexPwEnetStatsTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPwEnetStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPwEnetStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PwEnetStatsIllegalVlanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetStatsIllegalVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
PwEnetStatsIllegalLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePwEnetStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPwEnetStatsIllegalLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}
