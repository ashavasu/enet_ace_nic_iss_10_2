/****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vppsn.c,v 1.53 2017/06/08 11:40:31 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vppsn.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines to handle events
 *                             from signalling modules
 *---------------------------------------------------------------------------*/
#include "l2vpincs.h"
#include "mplslsr.h"
#include "ldpext.h"
#include "mplsnp.h"
#include "mplsutil.h"

/*****************************************************************************/
/* Function Name : L2VpnProcessSigEvent                                      */
/* Description   : This routine processes Signalling Events                  */
/* Input(s)      : pPwVcSigEvtInfo - pointer to Signalling event info        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessSigEvent (tPwVcSigEvtInfo * pPwVcSigEvtInfo)
{
    tPwVcSsnEvtInfo    *pSsnInfo = NULL;
    tPwVcLblMsgInfo    *pLblInfo = NULL;
    tPwVcNotifEvtInfo  *pNotifInfo = NULL;
    tL2VpnLdpIccpEvtInfo *pLdpIccpEvtInfo = NULL;

    UINT4               u4SigType;
    UINT4               u4EvtType;
    UINT1               u1MsgType;
    INT4                i4Status = L2VPN_FAILURE;

    u4SigType = pPwVcSigEvtInfo->u4SigType;

    if (u4SigType != L2VPN_SIG_LDP)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Invalid Sig type\r\n");
        return L2VPN_FAILURE;
    }

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Sig Event Received\n");

    u4EvtType = pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.u4EvtType;

    switch (u4EvtType)
    {
            /* Session messages from LDP */
        case L2VPN_LDP_PWVC_SSN_UP:
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status = L2VpnProcessLdpSsnUpEvent (pSsnInfo);
            break;

        case L2VPN_LDP_GR_PWVC_SSN_UP:
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status = L2VpnProcessLdpGrSsnUpEvent (pSsnInfo);
            break;

        case L2VPN_LDP_PWVC_SSN_DOWN:
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status = L2VpnProcessLdpSsnDownEvent (pSsnInfo);
            break;

            /* Label (Advertisment) messages from LDP */
        case L2VPN_LDP_PWVC_LBL_MSG:
            pLblInfo =
                (tPwVcLblMsgInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.LblInfo;
            u1MsgType =
                pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.unEvtInfo.LblInfo.
                u1MsgType;
            switch (u1MsgType)
            {
                case L2VPN_LDP_LBL_MAP_MSG:
                    i4Status = L2VpnProcessLdpLblMapMsg (pLblInfo);
                    break;
                case L2VPN_LDP_LBL_WDRAW_MSG:
                    i4Status = L2VpnProcessLdpLblWdrawMsg (pLblInfo);
                    break;
                case L2VPN_LDP_LBL_REL_MSG:
                    i4Status = L2VpnProcessLdpLblRelMsg (pLblInfo);
                    break;
                case L2VPN_LDP_LBL_WC_WDRAW_MSG:
                    i4Status = L2VpnProcessLdpLblWcWdrawMsg (pLblInfo);
                    break;
                case L2VPN_LDP_LBL_MAP_REQ_MSG:
                    i4Status = L2VpnProcessLdpLblMapReqMsg (pLblInfo);
                    break;
                default:
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unknown LDP msg\r\n");
                    break;
            }
            if (pLblInfo->pi1LocalIfString != NULL)
            {
                if (MemReleaseMemBlock (L2VPN_IF_DESC_POOL_ID,
                                        (UINT1 *) pLblInfo->pi1LocalIfString)
                    == MEM_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "Mem release failure - If Desc string\r\n");
                }
                pLblInfo->pi1LocalIfString = NULL;
            }
            break;

            /* Notification messages from LDP */
        case L2VPN_LDP_PWVC_NOTIF_MSG:
            pNotifInfo =
                (tPwVcNotifEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.NotifInfo;
            i4Status = L2VpnProcessLdpNotifEvent (pNotifInfo);
            break;
        case L2VPN_LDP_PWVC_ADDR_WDRAW_MSG:
#ifdef HVPLS_WANTED
            /* Handle Address Withdraw from LDP */
            pNotifInfo =
                (tPwVcNotifEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.NotifInfo;
            L2VPN_DBG (L2VPN_DBG_ALL_FLAG, "L2VpnProcessLdpAddrWdrEvent: "
                       "Received MAC Wdr Event from LDP.\n");
            i4Status = L2VpnProcessLdpAddrWdrEvent (pNotifInfo);
#endif
            break;

        case L2VPN_LDP_PWVC_GR_SHUTDOWN:

            L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessSigEvent: "
                       "Received GR shutdown Event from LDP.\n");
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status = L2vpnGrProcessLdpGrShutEvent (pSsnInfo,
                                                     L2VPN_LDP_PWVC_GR_SHUTDOWN);
            break;

        case L2VPN_LDP_PWVC_GR_HELPER_MARK_STALE:

            L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessSigEvent: "
                       "Received Helper Mark Stale Event from LDP.\n");
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status =
                L2vpnGrProcessLdpGrShutEvent (pSsnInfo,
                                              L2VPN_LDP_PWVC_GR_HELPER_MARK_STALE);
            break;

        case L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES:

            L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessSigEvent: "
                       "Received Delete Stale Entries Event from LDP.\n");
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status = L2vpnGrProcessLdpDelStaleEntryEvent (pSsnInfo);
            break;

        case L2VPN_LDP_PWVC_GR_FWD_HOLD_EXPIRY:

            L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessSigEvent: "
                       "Received Forwarding Holding Expiry Event from LDP.\n");
            i4Status = L2vpnGrProcessLdpDelAllStaleEntries ();
            break;

        case L2VPN_LDP_PWVC_GR_SET_RECOVER_TMR:

            L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART, "L2VpnProcessSigEvent: "
                       "Received Set Recover Timer Event from LDP.\n");
            pSsnInfo =
                (tPwVcSsnEvtInfo *) & pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.
                unEvtInfo.SsnInfo;
            i4Status = L2vpnGrSetGrRecoveryTime (pSsnInfo);
            break;

        case L2VPN_LDP_ICCP_RG_EVT:
            pLdpIccpEvtInfo = (tL2VpnLdpIccpEvtInfo *)
                & (pPwVcSigEvtInfo->unEvtInfo.LdpEvtInfo.unEvtInfo.IccpInfo);

            i4Status = L2vpnRedundancyIccpPwDataRx (pLdpIccpEvtInfo);
            break;
#ifdef LDP_GR_WANTED
        case L2VPN_LDP_PWVC_GR_START:
            if ((L2VpnMarkLdpPwEntriesStale () == L2VPN_SUCCESS))
            {
                i4Status = L2VPN_SUCCESS;
            }
            L2VpnSendReadyEventToLdp ();
#endif
            break;
        default:
            /* Do nothing */
            break;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessMplsTeLspEvent                                */
/* Description   : This routine processes PSN Events (MPLS TE Tnl Up/Down)   */
/* Input(s)      : pMplsTeEvtInfo - pointer to Signalling event info         */
/*               : u4Event - Event (Tunnel Up/Down)                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessMplsTeLspEvent (tL2VpnMplsPwVcMplsTeEvtInfo * pMplsTeEvtInfo,
                            UINT4 u4Event)
{
    INT4                i4Status = L2VPN_SUCCESS;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Psn TE Event received\n");

    /* Based on the role received from TE corresponding event will be
     * processed. */
    if (pMplsTeEvtInfo->u1TnlRole == TE_INGRESS_EGRESS)
    {
        /* This role is possible in case of corouted bidirectional tunnels.
         * In this case both ingress and egress should tunnel events 
         * should be processed. */
        i4Status = L2VpnPrcsIngressTnlEvt (pMplsTeEvtInfo, u4Event);
        i4Status = L2VpnPrcsEgressTnlEvt (pMplsTeEvtInfo, u4Event);
    }
    else if (pMplsTeEvtInfo->u1TnlRole == TE_INGRESS)
    {
        i4Status = L2VpnPrcsIngressTnlEvt (pMplsTeEvtInfo, u4Event);
    }
    else if (pMplsTeEvtInfo->u1TnlRole == TE_EGRESS)
    {
        i4Status = L2VpnPrcsEgressTnlEvt (pMplsTeEvtInfo, u4Event);
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessMplsNonTeLspEvent                             */
/* Description   : This routine processes PSN Event (MPLS Non-TE Tnl Up/Down)*/
/* Input(s)      : pPwVcSigEvtInfo - pointer to Signalling event info        */
/*               : u4Event - Event (LSP Up/Down)                             */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessMplsNonTeLspEvent (tL2VpnMplsPwVcMplsNonTeEvtInfo * pLspEvtInfo,
                               UINT4 u4Event)
{
    INT4                i4Status = L2VPN_FAILURE;

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG, "NonTe LspEvent %x is received\n",
                u4Event);

    switch (u4Event)
    {
        case L2VPN_MPLS_PWVC_LSP_UP:
        case L2VPN_MPLS_PWVC_LSP_DOWN:
        case L2VPN_MPLS_PWVC_LSP_DESTROY:
        case L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD:
        case L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL:
            i4Status = L2VpnPrcsIngressLspEvt (pLspEvtInfo, u4Event);
            break;
        case L2VPN_MPLS_PWVC_TE_FRR_MP_ADD:
        case L2VPN_MPLS_PWVC_TE_FRR_MP_DEL:
            i4Status = L2VpnPrcsEgressLspEvt (pLspEvtInfo, u4Event);
            break;
        default:
            break;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnProcessPsnEvent                                      */
/* Description   : This routine processes PSN Events                         */
/* Input(s)      : pPwVcSigEvtInfo - pointer to Signalling event info        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnProcessPsnEvent (tPwVcPSNEvtInfo * pPwVcPsnEvtInfo)
{
    tL2VpnMplsPwVcMplsTeEvtInfo *pMplsTeEvtInfo = NULL;
    tL2VpnMplsPwVcMplsNonTeEvtInfo *pMplsLspEvtInfo = NULL;

    UINT4               u4PsnType;
    UINT4               u4MplsType;
    UINT4               u4Event;
    INT4                i4Status = L2VPN_FAILURE;

    u4PsnType = pPwVcPsnEvtInfo->u4PsnType;

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Psn Event received\n");

    if (u4PsnType != L2VPN_PWVC_PSN_MPLS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unsupported PSN Type\n");
        return i4Status;
    }

    u4MplsType = pPwVcPsnEvtInfo->unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.u4MplsType;

    switch (u4MplsType)
    {
        case L2VPN_PWVC_MPLS_TNL:
            pMplsTeEvtInfo =
                &pPwVcPsnEvtInfo->unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.
                MplsTeEvtInfo;
            u4Event =
                pPwVcPsnEvtInfo->unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.
                MplsTeEvtInfo.u4EvtType;

            i4Status = L2VpnProcessMplsTeLspEvent (pMplsTeEvtInfo, u4Event);

            break;
        case L2VPN_PWVC_MPLS_LSP:
            pMplsLspEvtInfo =
                &pPwVcPsnEvtInfo->unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.
                MplsNonTeEvtInfo;
            u4Event =
                pPwVcPsnEvtInfo->unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.
                MplsNonTeEvtInfo.u4EvtType;

            i4Status = L2VpnProcessMplsNonTeLspEvent (pMplsLspEvtInfo, u4Event);

            break;
        default:
            L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Unsupported Mpls Type\r\n");
            break;
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnGetMplsEntryStatus                                   */
/* Description   : This routine checks the status of Tnls entry from TE      */
/* Input(s)      : pPwVcEntry - pointer to PwVc Entry                        */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnGetMplsEntryStatus (tPwVcEntry * pPwVcEntry, UINT1 u1TnlDir)
{
    tPwVcMplsEntry     *pMplsPsnEntry = NULL;

    INT4                i4Status = L2VPN_SUCCESS;

    /* Request for Tnl status from TE module */
    if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) == L2VPN_PWVC_PSN_TYPE_MPLS)
    {
        pMplsPsnEntry = L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);
        if (pMplsPsnEntry == NULL)
        {
            return L2VPN_FAILURE;
        }
        if (L2VPN_PWVC_MPLS_MPLS_TYPE (pMplsPsnEntry) == L2VPN_MPLS_TYPE_TE)
        {
            i4Status = L2VpnGetTeLspStatus (pPwVcEntry, u1TnlDir);
        }
        else if (L2VPN_PWVC_MPLS_MPLS_TYPE (pMplsPsnEntry) ==
                 L2VPN_MPLS_TYPE_NONTE)
        {
            i4Status = L2VpnGetNonTeLspStatus (pPwVcEntry, u1TnlDir);
        }
        else
        {
            pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_BOTH;
        }
    }

    if (i4Status != L2VPN_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Error in querying LSP status\r\n");
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnPrcsIngressTnlEvt                                    */
/* Description   : This routine processes PSN Ingress Tnl Events (TnlUp/Down)*/
/* Input(s)      : pMplsTeEvtInfo - pointer to Signalling event info         */
/*               : u4Event - Event (Tunnel Up/Down)                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPrcsIngressTnlEvt (tL2VpnMplsPwVcMplsTeEvtInfo * pMplsTeEvtInfo,
                        UINT4 u4Event)
{
    tPwVcMplsMappingEntry *pPwVcMplsMappingEntry = NULL, PwVcMplsMappingEntry;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    UINT4               u4PwIndex = L2VPN_ZERO;
    INT4                i4Status = L2VPN_FAILURE;
    UINT4               u4LclLsrId = L2VPN_ZERO;
    UINT4               u4PeerLsrId = L2VPN_ZERO;
    UINT4               u4InboundTunnelId = L2VPN_ZERO;
    UINT4               u4OutboundTunnelId = L2VPN_ZERO;
    UINT1               au1NextHopMac[MAC_ADDR_LEN];
    BOOL1               bInstanceChg = FALSE;
    UINT1               u1HwStatus = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    MEMSET (au1NextHopMac, 0, MAC_ADDR_LEN);

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "PW processed for Event %x for Ingress Tunnel\n", u4Event);

    PwVcMplsMappingEntry.unTnlInfo.TeInfo.u4TnlIndex =
        pMplsTeEvtInfo->u4PwVcMplsTnlIndex;
    MEMCPY (PwVcMplsMappingEntry.unTnlInfo.TeInfo.TnlLclLSR,
            pMplsTeEvtInfo->PwVcMplsTnlLclLSR, sizeof (tTeRouterId));
    MEMCPY (PwVcMplsMappingEntry.unTnlInfo.TeInfo.TnlPeerLSR,
            pMplsTeEvtInfo->PwVcMplsTnlPeerLSR, sizeof (tTeRouterId));

    pPwVcMplsMappingEntry =
        RBTreeGet (L2VPN_PWVC_MPLS_TE_MAP_LIST (gpPwVcMplsGlobalInfo),
                   &PwVcMplsMappingEntry);

    if (pPwVcMplsMappingEntry == NULL)
    {
        CONVERT_TO_INTEGER (pMplsTeEvtInfo->PwVcMplsTnlLclLSR, u4LclLsrId);
        CONVERT_TO_INTEGER (pMplsTeEvtInfo->PwVcMplsTnlPeerLSR, u4PeerLsrId);
        L2VPN_DBG3 (L2VPN_DBG_LVL_ERR_FLAG,
                    "No PW Entries found for TE Ingress TNL %d %x %x\n",
                    pMplsTeEvtInfo->u4PwVcMplsTnlIndex,
                    OSIX_NTOHL (u4LclLsrId), OSIX_NTOHL (u4PeerLsrId));

        L2VpnDeleteL3FtnAndPsnTnlIf (pMplsTeEvtInfo, u4Event);

        return L2VPN_FAILURE;
    }
    TMO_DLL_Scan (&pPwVcMplsMappingEntry->PwOutList, pNode, tTMO_DLL_NODE *)
    {
        bInstanceChg = FALSE;
        pPwVcEntry = MPLS_PWVC_FROM_OUTLIST (pNode);

        u4PwIndex = pPwVcEntry->u4PwVcIndex;

#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            GenU4Addr.Addr.u4Addr =
                OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr));
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

        MEMCPY (au1NextHopMac, pPwVcEntry->au1NextHopMac, MAC_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Found PW %d for Peer Ipv4 - %x Ipv6 - %s of type %d "
                    "that is in Outbound list of Ingress Tnl\n",
                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                    L2VPN_PWVC_OWNER (pPwVcEntry));
#else
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Found PW %d for Peer %x of type %d "
                    "that is in Outbound list of Ingress Tnl\n",
                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                    L2VPN_PWVC_OWNER (pPwVcEntry));
#endif

        pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

        switch (u4Event)
        {
            case L2VPN_MPLS_PWVC_TE_TNL_UP:

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);
                pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);

                u4InboundTunnelId =
                    L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pMplsInTnlEntry);
                u4OutboundTunnelId =
                    L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsTnlEntry);

                if ((L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry)
                     == pMplsTeEvtInfo->u2PwVcMplsTnlInstance) &&
                    (pPwVcEntry->u1CPOrMgmtOperStatus == L2VPN_PWVC_OPER_UP))
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Tnl UP: No Change in instance and PW %d Peer Ipv4 - %x Ipv6 - %s is "
                                "already UP\n", u4PwIndex,
                                GenU4Addr.Addr.u4Addr,
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Tnl UP: No Change in instance and PW %d %x is "
                                "already UP\n", u4PwIndex,
                                GenU4Addr.Addr.u4Addr);
#endif
                    continue;
                }

#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl UP: Out Tnl If Index changed from %d to %d for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl UP: Out Tnl If Index changed from %d to %d for "
                            "PW %d %x\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                /* De register the old tunnel instance and 
                 * register the new created instance */
                if (L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry)
                    != pMplsTeEvtInfo->u2PwVcMplsTnlInstance)
                {
                    bInstanceChg = TRUE;

                    L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);

#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                                "OUT Tnl Instance changed from %d to %d for "
                                "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                                L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry),
                                pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                                u4PwIndex, GenU4Addr.Addr.u4Addr,
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                                "OUT Tnl Instance changed from %d to %d for "
                                "PW %d %x\n",
                                L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry),
                                pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                                u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                    L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry) =
                        pMplsTeEvtInfo->u2PwVcMplsTnlInstance;

                    i4Status = L2VpnRegisterWithMplsModule (pPwVcEntry,
                                                            L2VPN_TNL_OUT);

                    if (i4Status != L2VPN_SUCCESS)
                    {
#ifdef MPLS_IPV6_WANTED
                        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Registration with TE failed for PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Registration with TE failed for PW %d %x\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                        continue;
                    }
                }
                /* Register the newly created tunnel instance */
                else if (L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry)
                         == pMplsTeEvtInfo->u2PwVcMplsTnlInstance)
                {
                    i4Status = L2VpnRegisterWithMplsModule (pPwVcEntry,
                                                            L2VPN_TNL_OUT);

                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Registration with TE failed for PW %d %x\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr);
                        continue;
                    }
                }

                pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_OUT;

                /* Pw mapped over P2MP tunnel - 
                   Out tunnel/ In-Tunnel can be zero */
                if ((u4InboundTunnelId != L2VPN_ZERO)
                    && (u4OutboundTunnelId != L2VPN_ZERO))
                {
                    if (pPwVcEntry->u1PsnTnlStatus != L2VPN_PSN_TNL_BOTH)
                    {
#ifdef MPLS_IPV6_WANTED
                        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "In Tnl NOT up for PW %d  Peer Ipv4 - %x Ipv6 - %s "
                                    "Sending Lbl Map/Notif Skipped\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "In Tnl NOT up for PW %d %x. "
                                    "Sending Lbl Map/Notif Skipped\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                        continue;
                    }
                }

                if (bInstanceChg == TRUE)
                {
                    /* Instance of Incoming tunnel has changed. So,
                     * Notify the peer about this and make the PW down and 
                     * then UP in HW to reprogram the labels correctly. */

                    /* Here, Failure checks are skipped. */
                    i4Status =
                        L2VpnHandlePsnTnlUpOrDown (pPwVcEntry,
                                                   L2VPN_MPLS_PWVC_TE_TNL_DOWN);

                    i4Status =
                        L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                   L2VPN_PWVC_PSN_TNL_DOWN);
                }

                pPwVcEntry->u4PwL3Intf = pMplsTeEvtInfo->u4TnlIfIndex;

                i4Status = L2VpnHandlePsnTnlUpOrDown (pPwVcEntry, u4Event);

                if (i4Status != L2VPN_SUCCESS)
                {
                    continue;
                }

                i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                      L2VPN_PWVC_UP);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PwOperStatus updation: PwLocalStatus "
                                "%x PwRemoteStatus %x\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }

                break;
            case L2VPN_MPLS_PWVC_TE_TNL_DOWN:

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

                if (L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry)
                    != pMplsTeEvtInfo->u2PwVcMplsTnlInstance)
                {
                    continue;
                }

                pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl Down: Out Tnl If Index changed from %d to %d for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl Down: Out Tnl If Index changed from %d to %d for "
                            "PW %d %x\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                pPwVcEntry->u4PwL3Intf = pMplsTeEvtInfo->u4TnlIfIndex;
                i4Status = L2VpnHandlePsnTnlUpOrDown (pPwVcEntry, u4Event);

                if (i4Status != L2VPN_SUCCESS)
                {
                    continue;
                }

                i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                      L2VPN_PWVC_PSN_TNL_DOWN);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PwOperStatus updation: "
                                "PwLocalStatus %x PwRemoteStatus %x\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }

                break;
            case L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD:
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl PLR Add: Out Tnl If Index changed from %d to %d for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl PLR Add: Out Tnl If Index changed from %d to %d for "
                            "PW %d %x\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                pPwVcEntry->u4PwL3Intf = pMplsTeEvtInfo->u4TnlIfIndex;
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    u1HwStatus |= (UINT1) PW_HW_PW_UPDATE;
                    L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ONE);

                    /* PWVC Addition */
                    if (L2VpnPwVcHwAdd (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "PWVC for PW=%d HW delete failed \n",
                                    pPwVcEntry->u4PwVcIndex);
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_LLDOWN;
                        L2VpnUpdateGlobalStats (pPwVcEntry,
                                                L2VPN_PWVC_OPER_DOWN);
                        L2VpnPrintPwVc (pPwVcEntry);
                        pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
                        continue;
                    }

                    i4Status =
                        L2VpnRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);

                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                   "Registration with TE failed\n");
                        continue;
                    }
                }
                break;

            case L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL:
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl PLR Del: Out Tnl If Index changed from %d to %d for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tnl PLR Del: Out Tnl If Index changed from %d to %d for "
                            "PW %d %x\n",
                            pPwVcEntry->u4PwL3Intf,
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                pPwVcEntry->u4PwL3Intf = pMplsTeEvtInfo->u4TnlIfIndex;
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    u1HwStatus |= (UINT1) PW_HW_PW_UPDATE;
                    L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ONE);

                    /* PWVC deletion */
                    if (L2VpnPwVcHwDel (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "PWVC for PW=%d HW delete failed \n",
                                    pPwVcEntry->u4PwVcIndex);
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_LLDOWN;
                        L2VpnUpdateGlobalStats (pPwVcEntry,
                                                L2VPN_PWVC_OPER_DOWN);
                        L2VpnPrintPwVc (pPwVcEntry);
                        pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
                        continue;
                    }
                    L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);
                }
                break;

            case L2VPN_MPLS_PWVC_TE_TNL_REESTB:
                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tunnel Instance changed from %d to %d for "
                            "Ingress Tunnel for PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry),
                            pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Tunnel Instance changed from %d to %d for "
                            "Ingress Tunnel for PW %d %x\n",
                            L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry),
                            pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                if ((pMplsTeEvtInfo->u1TnlSwitchingType ==
                     TE_PROTECTION_SWITCH_BACK)
                    && (L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry) ==
                        pMplsTeEvtInfo->u2PwVcMplsTnlInstance))
                {

                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "No need to process switchback REESTB event, as"
                               " instance already updated\n");
                    continue;

                }

                /*Deleting Working Tunnel From Hw */
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    u1HwStatus |= (UINT1) PW_HW_VPN_UPDATE;
                    L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ONE);
                    /* PWVC deletion */
                    if (L2VpnPwVcHwDel (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "PWVC for PW=%d HW delete failed \n",
                                    pPwVcEntry->u4PwVcIndex);
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_LLDOWN;
                        L2VpnUpdateGlobalStats (pPwVcEntry,
                                                L2VPN_PWVC_OPER_DOWN);
                        L2VpnPrintPwVc (pPwVcEntry);
                        pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
                        continue;
                    }
                    L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);

                }

                if (pMplsTeEvtInfo->u1TnlSwitchingType ==
                    TE_PROTECTION_SWITCH_OVER)
                {
                    L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry) =
                        pMplsTeEvtInfo->u2PwVcMplsBkpTnlInstance;
                }
                if (pMplsTeEvtInfo->u1TnlSwitchingType ==
                    TE_PROTECTION_SWITCH_BACK)
                {
                    L2VPN_PWVC_MPLS_TNL_TNL_INST (pMplsTnlEntry) =
                        pMplsTeEvtInfo->u2PwVcMplsTnlInstance;
                }

                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {

                    L2VpnUtilSetHwStatus (pPwVcEntry, PW_HW_VPN_UPDATE,
                                          L2VPN_ONE);
                    /* PWVC Addition */
                    if (L2VpnPwVcHwAdd (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "PWVC for PW=%d HW delete failed \n",
                                    pPwVcEntry->u4PwVcIndex);
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_LLDOWN;
                        L2VpnUpdateGlobalStats (pPwVcEntry,
                                                L2VPN_PWVC_OPER_DOWN);
                        L2VpnPrintPwVc (pPwVcEntry);
                        pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
                        continue;
                    }
                    L2VpnRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);
                }

                break;

            case L2VPN_MPLS_PWVC_P2MP_TE_TNL_DEST_DELETE:
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Out Tnl If Index %d deleted for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Out Tnl If Index %d deleted for "
                            "PW %d %x\n",
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                pPwVcEntry->u4PwL3Intf = pMplsTeEvtInfo->u4TnlIfIndex;
                L2VpnPwVcHwDelP2MPDest (pPwVcEntry, NULL);
                break;

            case L2VPN_MPLS_PWVC_P2MP_TE_TNL_DEST_ADD:

#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Out Tnl If Index %d added for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Out Tnl If Index %d added for "
                            "PW %d %x\n",
                            pMplsTeEvtInfo->u4TnlIfIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                pPwVcEntry->u4PwL3Intf = pMplsTeEvtInfo->u4TnlIfIndex;
                MEMCPY (pPwVcEntry->au1NextHopMac,
                        pMplsTeEvtInfo->au1NextHopMac, MAC_ADDR_LEN);
                L2VpnPwVcHwAddP2MPDest (pPwVcEntry, NULL);
                break;

            default:
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Invalid PSN TE Event\n");
                return L2VPN_FAILURE;
        }
    }

    L2VpnDeleteL3FtnAndPsnTnlIf (pMplsTeEvtInfo, u4Event);

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPrcsEgressTnlEvt                                     */
/* Description   : This routine processes PSN Egress Tnl Events (Tnl Up/Down)*/
/* Input(s)      : pMplsTeEvtInfo - pointer to Signalling event info         */
/*               : u4Event - Event (Tunnel Up/Down)                          */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPrcsEgressTnlEvt (tL2VpnMplsPwVcMplsTeEvtInfo * pMplsTeEvtInfo,
                       UINT4 u4Event)
{
    tPwVcMplsInMappingEntry *pPwVcMplsMappingEntry = NULL;
    tPwVcMplsInMappingEntry PwVcMplsMappingEntry;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcMplsInTnlEntry *pMplsInTnlEntry = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    UINT4               u4PwIndex = L2VPN_ZERO;
    INT4                i4Status = L2VPN_FAILURE;
    BOOL1               bInstanceChg = FALSE;
    UINT4               u4LclLsrId = L2VPN_ZERO;
    UINT4               u4PeerLsrId = L2VPN_ZERO;
    UINT4               u4InboundTunnelId = L2VPN_ZERO;
    UINT4               u4OutboundTunnelId = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                "PW processed for Event %x for Egress Tunnel\n", u4Event);

    PwVcMplsMappingEntry.unInTnlInfo.TeInfo.u4TnlIndex =
        pMplsTeEvtInfo->u4PwVcMplsTnlIndex;
    MEMCPY (PwVcMplsMappingEntry.unInTnlInfo.TeInfo.TnlLclLSR,
            pMplsTeEvtInfo->PwVcMplsTnlLclLSR, sizeof (tTeRouterId));
    MEMCPY (PwVcMplsMappingEntry.unInTnlInfo.TeInfo.TnlPeerLSR,
            pMplsTeEvtInfo->PwVcMplsTnlPeerLSR, sizeof (tTeRouterId));

    pPwVcMplsMappingEntry =
        RBTreeGet (L2VPN_PWVC_MPLS_TE_IN_MAP_LIST (gpPwVcMplsGlobalInfo),
                   &PwVcMplsMappingEntry);

    if (pPwVcMplsMappingEntry == NULL)
    {
        CONVERT_TO_INTEGER (pMplsTeEvtInfo->PwVcMplsTnlLclLSR, u4LclLsrId);
        CONVERT_TO_INTEGER (pMplsTeEvtInfo->PwVcMplsTnlPeerLSR, u4PeerLsrId);
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "No PW Entries found for TE Egress TNL %d %x %x\n",
                    pMplsTeEvtInfo->u4PwVcMplsTnlIndex,
                    OSIX_NTOHL (u4LclLsrId), OSIX_NTOHL (u4PeerLsrId));
        return L2VPN_FAILURE;
    }

    TMO_DLL_Scan (&pPwVcMplsMappingEntry->PwInList, pNode, tTMO_DLL_NODE *)
    {
        bInstanceChg = FALSE;
        pPwVcEntry = MPLS_PWVC_FROM_INLIST (pNode);

        u4PwIndex = pPwVcEntry->u4PwVcIndex;

#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            GenU4Addr.Addr.u4Addr =
                OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr));
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Found PW %d for Peer Ipv4 - %x Ipv6 - %s of type %d "
                    "that is in Inbound list of egress tunnel\n",
                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)),
                    L2VPN_PWVC_OWNER (pPwVcEntry));
#else
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Found PW %d for Peer %x of type %d "
                    "that is in Inbound list of egress tunnel\n",
                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                    L2VPN_PWVC_OWNER (pPwVcEntry));
#endif

        pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

        switch (u4Event)
        {
            case L2VPN_MPLS_PWVC_TE_TNL_UP:

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);
                pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);

                u4InboundTunnelId =
                    L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX (pMplsInTnlEntry);
                u4OutboundTunnelId =
                    L2VPN_PWVC_MPLS_TNL_TNL_INDEX (pMplsTnlEntry);

                if ((pMplsTeEvtInfo->u2PwVcMplsTnlInstance ==
                     pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance) &&
                    (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP))
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Tnl UP: No Change in instance for PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                                u4PwIndex, GenU4Addr.Addr.u4Addr,
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Tnl UP: No Change in instance for PW %d %x\n",
                                u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                    continue;
                }

#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                            "In Tnl XC Index changed from %d to %d for "
                            "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                            L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX (pMplsInTnlEntry),
                            pMplsTeEvtInfo->u4TnlXcIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "In Tnl XC Index changed from %d to %d for "
                            "PW %d %x\n",
                            L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX (pMplsInTnlEntry),
                            pMplsTeEvtInfo->u4TnlXcIndex,
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX (pMplsInTnlEntry) =
                    pMplsTeEvtInfo->u4TnlXcIndex;

                if (pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance
                    != pMplsTeEvtInfo->u2PwVcMplsTnlInstance)
                {
                    bInstanceChg = TRUE;

                    L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_IN);

#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                                "In Tnl Instance changed from %d to %d for "
                                "PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                                pMplsInTnlEntry->unInTnlInfo.TeInfo.
                                u2TnlInstance,
                                pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                                u4PwIndex, GenU4Addr.Addr.u4Addr,
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                                "In Tnl Instance changed from %d to %d for "
                                "PW %d %x\n",
                                pMplsInTnlEntry->unInTnlInfo.TeInfo.
                                u2TnlInstance,
                                pMplsTeEvtInfo->u2PwVcMplsTnlInstance,
                                u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif

                    pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance =
                        pMplsTeEvtInfo->u2PwVcMplsTnlInstance;

                    i4Status = L2VpnRegisterWithMplsModule (pPwVcEntry,
                                                            L2VPN_TNL_IN);

                    if (i4Status != L2VPN_SUCCESS)
                    {
#ifdef MPLS_IPV6_WANTED
                        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Registration with TE failed for PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Registration with TE failed for PW %d %x\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                        continue;
                    }
                }

                pPwVcEntry->u1PsnTnlStatus |= L2VPN_PSN_TNL_IN;

                /* Pw mapped over P2MP tunnel - 
                   Out tunnel/ In-Tunnel can be zero */
                if ((u4InboundTunnelId != L2VPN_ZERO)
                    && (u4OutboundTunnelId != L2VPN_ZERO))
                {
                    if (pPwVcEntry->u1PsnTnlStatus != L2VPN_PSN_TNL_BOTH)
                    {
#ifdef MPLS_IPV6_WANTED
                        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Out Tnl NOT up for PW %d Peer Ipv4 - %x Ipv6 - %s "
                                    "Sending Lbl Map/Notif Msg skipped\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr,
                                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "Out Tnl NOT up for PW %d %x. "
                                    "Sending Lbl Map/Notif Msg skipped\n",
                                    u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                        continue;
                    }
                }

                if (bInstanceChg == TRUE)
                {
                    /* Instance of Incoming tunnel has changed. So,
                     * Notify the peer about this and make the PW down and 
                     * then UP in HW to reprogram the labels correctly. */

                    /* Here, Failure checks are skipped. */
                    i4Status =
                        L2VpnHandlePsnTnlUpOrDown (pPwVcEntry,
                                                   L2VPN_MPLS_PWVC_TE_TNL_DOWN);

                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                   "PwOperStatus L2VPN_MPLS_PWVC_TE_TNL_DOWN "
                                   "updation failed\n");
                    }

                    i4Status =
                        L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                   L2VPN_PWVC_PSN_TNL_DOWN);
                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                   "PwOperStatus L2VPN_PWVC_PSN_TNL_DOWN "
                                   "updation failed\n");
                    }
                }

                i4Status = L2VpnHandlePsnTnlUpOrDown (pPwVcEntry, u4Event);

                if (i4Status != L2VPN_SUCCESS)
                {
                    continue;
                }

                i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                      L2VPN_PWVC_UP);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PwOperStatus updation: PwLocalStatus "
                                "%x PwRemoteStatus %x\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }
                break;
            case L2VPN_MPLS_PWVC_TE_TNL_DOWN:

                pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);

                if (L2VPN_PWVC_MPLS_IN_TNL_TNL_INST (pMplsInTnlEntry)
                    != pMplsTeEvtInfo->u2PwVcMplsTnlInstance)
                {
                    continue;
                }

                pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_IN);

                pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);
                pMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance = L2VPN_ZERO;

                L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX (pMplsInTnlEntry) =
                    pMplsTeEvtInfo->u4TnlXcIndex;
                i4Status = L2VpnHandlePsnTnlUpOrDown (pPwVcEntry, u4Event);

                if (i4Status != L2VPN_SUCCESS)
                {
                    continue;
                }

                i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                      L2VPN_PWVC_PSN_TNL_DOWN);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PwOperStatus updation: "
                                "PwLocalStatus %x PwRemoteStatus %x\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }
                break;
            case L2VPN_MPLS_PWVC_TE_FRR_MP_ADD:
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    if (L2VpnMplsFrrILMHwAddForMP (pPwVcEntry,
                                                   pMplsTeEvtInfo->u4TnlXcIndex,
                                                   NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                   "ILM Add for merge point1 is failed \n");
                    }
                }
                else
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Pw not oper up\n");
                }
                break;

            case L2VPN_MPLS_PWVC_TE_FRR_MP_DEL:
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    if (L2VpnMplsFrrILMHwDelForMP (pPwVcEntry,
                                                   pMplsTeEvtInfo->
                                                   u4TnlBkpMPLabel1,
                                                   pMplsTeEvtInfo->
                                                   u4TnlBkpMPLabel2,
                                                   L2VPN_LDP_INVALID_LABEL,
                                                   FALSE) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                                   "POP Search for L2VPNoLDPoRSVP using "
                                   " NON-TE deletion failed for MP label1\n");
                    }
                }
                else
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "Pw already oper down\n");
                }
                break;

            case L2VPN_MPLS_PWVC_TE_TNL_REESTB:

                pMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY (pPwVcMplsEntry);
                if ((pMplsTeEvtInfo->u1TnlSwitchingType ==
                     TE_PROTECTION_SWITCH_BACK)
                    && (L2VPN_PWVC_MPLS_IN_TNL_TNL_INST (pMplsInTnlEntry) ==
                        pMplsTeEvtInfo->u2PwVcMplsTnlInstance))
                {

                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "No need to process switchback REESTB event, as"
                               " instance already updated\n");
                    continue;

                }
                pPwVcMplsEntry->PwVcMplsInTnl.u4LsrXcIndex =
                    pMplsTeEvtInfo->u4TnlXcIndex;

                /*Deleting Working Tunnel From Hw */
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    if (pPwVcEntry->b1IsIlmEntryPresent == TRUE)
                    {
                        if (L2VpnPwVcIlmHwDel (pPwVcEntry, TRUE,
                                               NULL) == L2VPN_FAILURE)
                        {
                            L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                        "ILM Delete in HW for PW %d failed\n",
                                        pPwVcEntry->u4PwVcIndex);
                        }
                        L2VpnDeRegisterWithMplsModule (pPwVcEntry,
                                                       L2VPN_TNL_IN);
                    }

                }

                if (pMplsTeEvtInfo->u1TnlSwitchingType ==
                    TE_PROTECTION_SWITCH_OVER)
                {
                    L2VPN_PWVC_MPLS_IN_TNL_TNL_INST (pMplsInTnlEntry) =
                        pMplsTeEvtInfo->u2PwVcMplsBkpTnlInstance;
                }
                else if (pMplsTeEvtInfo->u1TnlSwitchingType ==
                         TE_PROTECTION_SWITCH_BACK)
                {
                    L2VPN_PWVC_MPLS_IN_TNL_TNL_INST (pMplsInTnlEntry) =
                        pMplsTeEvtInfo->u2PwVcMplsTnlInstance;
                }

                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {

                    if (L2VpnPwVcIlmHwAdd (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "%s ILM Add in HW for PW %d failed\n",
                                    __func__, pPwVcEntry->u4PwVcIndex);
                        return L2VPN_FAILURE;
                    }
                    L2VpnRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_IN);

                }

                L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX (pMplsInTnlEntry) =
                    pMplsTeEvtInfo->u4TnlXcIndex;
                break;

            default:
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Invalid PSN TE Event\n");
                return L2VPN_FAILURE;
        }
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/* Function Name : L2VpnPrcsIngressLspEvt                                    */
/* Description   : This routine processes Ingress Lsp Events.                */
/*                                                                           */
/* Input(s)      : pLspEvtInfo - Pointer to LSP Event Info                   */
/*               : u4Event     - Event to be processed                       */
/*                               Events can be L2VPN_MPLS_PWVC_LSP_UP,       */
/*                               L2VPN_MPLS_PWVC_LSP_DOWN,                   */
/*                               L2VPN_MPLS_PWVC_LSP_DESTROY,                */
/*                               L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD              */
/*                               L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL              */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPrcsIngressLspEvt (tL2VpnMplsPwVcMplsNonTeEvtInfo * pLspEvtInfo,
                        UINT4 u4Event)
{
    tPwVcMplsMappingEntry *pPwVcMplsMappingEntry = NULL, PwVcMplsMappingEntry;
    UINT1               au1NextHopMac[MAC_ADDR_LEN];
    tPwVcMplsTnlEntry  *pMplsTnlEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    BOOL1               bIsLspDestroy = FALSE;
    BOOL1               bIsLspDown = FALSE;
    UINT4               u4PwL3Intf = L2VPN_ZERO;
    UINT1               u1IsSigNonTeLsp = L2VPN_ZERO;
    UINT1               u1HwStatus = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;
#ifdef LDP_GR_WANTED
    tMplsIpAddress      FtnHwListFec;
    tFTNHwListEntry    *pFTNHwListEntry = NULL;
    UINT1               u1FtnHwListPrefLen = 32;
    UINT4               u4NpBitMask = 0;
    MEMSET (&FtnHwListFec, 0, sizeof (tMplsIpAddress));
#endif

    MEMSET (&PwVcMplsMappingEntry, 0, sizeof (tPwVcMplsMappingEntry));
    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    MEMSET (au1NextHopMac, 0, MAC_ADDR_LEN);

    if (pLspEvtInfo->u1AddrType == MPLS_IPV4_ADDR_TYPE)
    {

        L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                    "NonTe LspEvent is received for the FEC %d.%d.%d.%d, Event: %x\n",
                    pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                    pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                    pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                    pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3], u4Event);
        MEMCPY (&PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.PeerOrSrcAddr,
                &pLspEvtInfo->PeerOrSrcAddr, IPV4_ADDR_LENGTH);
    }
    else
    {
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "NonTe LspEvent is received for the FEC %s, Event: %x\n",
                    Ip6PrintAddr (&pLspEvtInfo->PeerOrSrcAddr.Ip6Addr),
                    u4Event);
        MEMCPY (&PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.PeerOrSrcAddr,
                &pLspEvtInfo->PeerOrSrcAddr, IPV6_ADDR_LENGTH);

    }

#ifdef LDP_GR_WANTED
    FtnHwListFec.i4IpAddrType = MPLS_LSR_ADDR_IPV4;
    MEMCPY (&FtnHwListFec.IpAddress, &pLspEvtInfo->PeerOrSrcAddr,
            IPV4_ADDR_LENGTH);
#endif

    PwVcMplsMappingEntry.unTnlInfo.NonTeInfo.u1AddrType =
        pLspEvtInfo->u1AddrType;
    pPwVcMplsMappingEntry = RBTreeGet (L2VPN_PWVC_MPLS_NONTE_MAP_LIST
                                       (gpPwVcMplsGlobalInfo),
                                       &PwVcMplsMappingEntry);
    if (pPwVcMplsMappingEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "NonTe LSP is not used by L2VPN\n");
        return L2VPN_FAILURE;
    }
    TMO_DLL_Scan (&pPwVcMplsMappingEntry->PwOutList, pNode, tTMO_DLL_NODE *)
    {
        pPwVcEntry = MPLS_PWVC_FROM_OUTLIST (pNode);

        pPwVcMplsEntry = (tPwVcMplsEntry *) pPwVcEntry->pPSNEntry;

        u4PwL3Intf = pPwVcEntry->u4PwL3Intf;
        u1IsSigNonTeLsp = pPwVcEntry->u1IsSigNonTeLsp;
        MEMCPY (au1NextHopMac, pPwVcEntry->au1NextHopMac, MAC_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                    (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
            GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d Peer Ipv4 - %x Ipv6 - %s found for this Non-Te LSP\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "PW %d %x found for this Non-Te LSP\n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif

        switch (u4Event)
        {
            case L2VPN_MPLS_PWVC_LSP_UP:

                if (pLspEvtInfo->u1AddrType == MPLS_IPV4_ADDR_TYPE)
                {

                    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                                "NonTe LspEvent is received for the FEC %d.%d.%d.%d, Event is LSP_UP\n",
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);
                }
                else
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                                "NonTe LspEvent is received for the FEC %s, Event is LSP_UP\n",
                                Ip6PrintAddr (&pLspEvtInfo->PeerOrSrcAddr.
                                              Ip6Addr));
                }

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

                pMplsTnlEntry->unTnlInfo.NonTeInfo.u4LsrXcIndex =
                    pLspEvtInfo->u4PwVcMplsOutLsrXcIndex;
#if 0
                printf
                    ("vishalPw: %s : %d pLspEvtInfo->u4PwVcMplsOutLsrXcIndex=%d\n",
                     __FUNCTION__, __LINE__,
                     pLspEvtInfo->u4PwVcMplsOutLsrXcIndex);
#endif

                i4Status = L2VpnRegWithNonTeLowerLayer (pPwVcEntry);

                if (i4Status != L2VPN_SUCCESS)
                {
#ifdef MPLS_IPV6_WANTED
                    L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Registration with NON-TE LSP "
                                "failed for PW %d Peer Ipv4 - %x Ipv6 - %s\n",
                                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                                Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "Registration with NON-TE LSP "
                                "failed for PW %d %x\n",
                                pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr);
#endif
                    continue;
                }

                i4Status = L2VpnHandlePsnTnlUpOrDown (pPwVcEntry, u4Event);

                if (i4Status != L2VPN_SUCCESS)
                {
                    continue;
                }

                if ((pPwVcEntry->u1MapStatus & L2VPN_REL_WAIT) !=
                    L2VPN_REL_WAIT)
                {
                    i4Status =
                        L2VpnUpdatePwVcOperStatus (pPwVcEntry, L2VPN_PWVC_UP);
                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                    "PwOperStatus updation: "
                                    "PwLocalStatus %x PwRemoteStatus %x\n",
                                    pPwVcEntry->u1LocalStatus,
                                    pPwVcEntry->u1RemoteStatus);
                    }
                }
                break;
            case L2VPN_MPLS_PWVC_LSP_DOWN:
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) != L2VPN_PWVC_OPER_UP)
                {
                    /*Below code is added to handle clean-up of control-block when de-registration
                     * of control block gets skipped on LSP_DOWN event if PW-STATUS is not operup for non-te LSP*/
                    if (pPwVcEntry->u1IsSigNonTeLsp == L2VPN_TRUE)
                    {
                        L2VpnHandleNonTeLowerLayerDown (pPwVcEntry);
                    }
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "pw is already oper down\n");
                    continue;
                }
                bIsLspDown = TRUE;
            case L2VPN_MPLS_PWVC_LSP_DESTROY:

                if (pLspEvtInfo->u1AddrType == MPLS_IPV4_ADDR_TYPE)
                {

                    L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                                "NonTe LspEvent is received for the FEC %d.%d.%d.%d, Event is LSP_DOWN\n",
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                                pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);
                }
                else
                {
                    L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                                "NonTe LspEvent is received for the FEC %s, Event is LSP_DOWN\n",
                                Ip6PrintAddr (&pLspEvtInfo->PeerOrSrcAddr.
                                              Ip6Addr));
                }

                i4Status = L2VpnHandlePsnTnlUpOrDown (pPwVcEntry, u4Event);

                if (i4Status != L2VPN_SUCCESS)
                {
                    continue;
                }

                i4Status = L2VpnUpdatePwVcOperStatus (pPwVcEntry,
                                                      L2VPN_PWVC_PSN_TNL_DOWN);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                                "PwOperStatus updation: "
                                "PwLocalStatus %x PwRemoteStatus %x\n",
                                pPwVcEntry->u1LocalStatus,
                                pPwVcEntry->u1RemoteStatus);
                }

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

                if ((u4Event == L2VPN_MPLS_PWVC_LSP_DESTROY) &&
                    (u1IsSigNonTeLsp == L2VPN_TRUE))
                {
                    /* When LSP Destroy event is received for signalling LSP,
                     * MPLS Tunnel Interface used by multiple PW's are deleted.
                     * So, reset this value in this PW. */
                    pPwVcEntry->u4PwL3Intf = 0;
                }
                pMplsTnlEntry->unTnlInfo.NonTeInfo.u4LsrXcIndex = L2VPN_ZERO;

                break;
            case L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD:
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Event received for FEC %d.%d.%d.%d is PLR ADD "
                            "Event\n",
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

                pMplsTnlEntry->unTnlInfo.NonTeInfo.u4LsrXcIndex =
                    pLspEvtInfo->u4PwVcMplsOutLsrXcIndex;
                i4Status = L2VpnRegWithNonTeLowerLayer (pPwVcEntry);
                if (i4Status != L2VPN_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "L2VpnPrcsIngressLspEvt: "
                               "Registration with NON-TE outer tunnel failed\r\n");
                    bIsLspDestroy = TRUE;
                    break;
                }
                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    u1HwStatus |= (UINT1) PW_HW_PW_UPDATE;
                    L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ONE);
                    /* PWVC Addition */
                    if (L2VpnPwVcHwAdd (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "PWVC for PW=%d HW delete failed \n",
                                    pPwVcEntry->u4PwVcIndex);
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_LLDOWN;
                        L2VpnUpdateGlobalStats (pPwVcEntry,
                                                L2VPN_PWVC_OPER_DOWN);
                        L2VpnPrintPwVc (pPwVcEntry);
                        pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
                        continue;
                    }
                    i4Status =
                        L2VpnRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);

                    if (i4Status != L2VPN_SUCCESS)
                    {
                        L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                                   "Registration with TE failed for PW\n");
                        continue;
                    }
                    L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);
                }
                break;

            case L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL:
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Event received for FEC %d.%d.%d.%d is PLR DEL "
                            "Event\n",
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);

                pMplsTnlEntry = L2VPN_PWVC_MPLS_OUT_ENTRY (pPwVcMplsEntry);

                pMplsTnlEntry->unTnlInfo.NonTeInfo.u4LsrXcIndex =
                    pLspEvtInfo->u4PwVcMplsOutLsrXcIndex;

                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    u1HwStatus |= (UINT1) PW_HW_PW_UPDATE;
                    L2VpnUtilSetHwStatus (pPwVcEntry, u1HwStatus, L2VPN_ONE);

                    /* PWVC deletion */
                    if (L2VpnPwVcHwDel (pPwVcEntry, NULL) == L2VPN_FAILURE)
                    {
                        L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                                    "PWVC for PW=%d HW delete failed \n",
                                    pPwVcEntry->u4PwVcIndex);
                        L2VPN_PWVC_OPER_STATUS (pPwVcEntry) =
                            L2VPN_PWVC_OPER_LLDOWN;
                        L2VpnUpdateGlobalStats (pPwVcEntry,
                                                L2VPN_PWVC_OPER_DOWN);
                        L2VpnPrintPwVc (pPwVcEntry);
                        pPwVcEntry->u1PsnTnlStatus &= ~(L2VPN_PSN_TNL_OUT);
                        continue;
                    }
                    L2VpnDeRegisterWithMplsModule (pPwVcEntry, L2VPN_TNL_OUT);
                }
                break;

            default:
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "Invalid PSN NONTE Event\n");
                return i4Status;
        }
    }
    if (u4Event == L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL)
    {
        L2VpnDeletePsnTnlIf (u4PwL3Intf, au1NextHopMac, L2VPN_PSN_DEL_L3FTN);
    }
    if (u4Event == L2VPN_MPLS_PWVC_LSP_DOWN && bIsLspDown == TRUE)
    {
        i4Status = L2VpnHandleNonTeLowerLayerDown (pPwVcEntry);
        if (i4Status != L2VPN_SUCCESS)
        {
#ifdef MPLS_IPV6_WANTED
            L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnHandleNonTeLowerLayerDown "
                        "failed for Peer IPv4 - %x IPv6 - %s\n",
                        GenU4Addr.Addr.u4Addr,
                        Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
            L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                        "L2VpnHandleNonTeLowerLayerDown "
                        "failed for Peer %x\n", GenU4Addr.Addr.u4Addr);
#endif
        }
    }
    if ((u4Event == L2VPN_MPLS_PWVC_LSP_DESTROY) || (bIsLspDestroy))
    {
        L2VPN_DBG1 (L2VPN_DBG_LVL_CRT_FLAG,
                    "L2VpnPrcsIngressLspEvt: NON-TE LSP DOWN "
                    "OutIfIndex %d\n", u4PwL3Intf);

#ifdef LDP_GR_WANTED
        MPLS_CMN_LOCK ();
        pFTNHwListEntry = MplsHwListGetFTNHwListEntry (FtnHwListFec,
                                                       u1FtnHwListPrefLen);
        if ((pFTNHwListEntry != NULL) &&
            (MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry) == u4PwL3Intf))
        {
            L2VPN_DBG5 (L2VPN_DBG_LVL_CRT_FLAG,
                        "FTN Hw List Entry Found for the FEC %d.%d.%d.%d\n and Tunnel interface = %d\n",
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3],
                        MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry));
        }
        else
        {
            L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                        "FTN Hw List Entry NOT Found for the FEC %d.%d.%d.%d\n",
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                        pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);

        }

        if ((pFTNHwListEntry != NULL) &&
            (MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry) == u4PwL3Intf))
        {
            /* Reset the FTN Called and Programmed Bits */
            u4NpBitMask = MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry);

            MPLS_HW_LIST_RESET_NP_BIT (u4NpBitMask,
                                       (NPAPI_FTN_PROGRAMMED |
                                        NPAPI_FTN_CALLED));

            /* Update the NP Bits in the existing the FTN Hw List Entry */
            MplsHwListAddOrUpdateFTNHwListEntry (pFTNHwListEntry, u4NpBitMask);
        }
#endif
        if (L2VpnDeletePsnTnlIf (u4PwL3Intf, au1NextHopMac, L2VPN_PSN_DEL_L3FTN)
            == L2VPN_FAILURE)
        {
#ifdef LDP_GR_WANTED
            MPLS_CMN_UNLOCK ();
#endif
            return L2VPN_FAILURE;

        }

#ifdef LDP_GR_WANTED

        /* Delete the FTN Hw list entry */
        if ((pFTNHwListEntry != NULL) &&
            (MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry) == u4PwL3Intf))
        {
            if (MplsHwListDelFTNHwListEntry (pFTNHwListEntry) == MPLS_FAILURE)
            {
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "Error in Deleting the Hw List Entry for the FEC %d.%d.%d.%d\n",
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);
            }
            else
            {
                L2VPN_DBG4 (L2VPN_DBG_LVL_CRT_FLAG,
                            "FTN Hw List Entry Deleted successfully for the FEC %d.%d.%d.%d\n",
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[0],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[1],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[2],
                            pLspEvtInfo->PeerOrSrcAddr.au1Ipv4Addr[3]);

            }
        }
        MPLS_CMN_UNLOCK ();
#endif
        if (u1IsSigNonTeLsp == L2VPN_TRUE)
        {
            L2VpnDeletePsnTnlIf (u4PwL3Intf,
                                 au1NextHopMac, L2VPN_PSN_DEL_MPLS_TNL_IF);
        }
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnPrcsEgressLspEvt                                     */
/* Description   : This routine processes Egress Lsp Events.                 */
/*                                                                           */
/* Input(s)      : pLspEvtInfo - Pointer to Lsp Event info                   */
/*               : u4Event     - Event to be processed                       */
/*                               Events can be L2VPN_MPLS_PWVC_TE_FRR_MP_ADD,*/
/*                               L2VPN_MPLS_PWVC_TE_FRR_MP_DEL               */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/
INT4
L2VpnPrcsEgressLspEvt (tL2VpnMplsPwVcMplsNonTeEvtInfo * pLspEvtInfo,
                       UINT4 u4Event)
{
    tPwVcMplsInMappingEntry *pPwVcMplsMappingEntry = NULL;
    tPwVcMplsInMappingEntry PwVcMplsMappingEntry;
    tPwVcEntry         *pPwVcEntry = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    INT4                i4Status = L2VPN_FAILURE;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "NonTe LspEvent is received\n");

    MEMCPY (&PwVcMplsMappingEntry.unInTnlInfo.NonTeInfo.PeerOrSrcAddr,
            &pLspEvtInfo->PeerOrSrcAddr, sizeof (uGenAddr));

    PwVcMplsMappingEntry.unInTnlInfo.NonTeInfo.u1AddrType =
        pLspEvtInfo->u1AddrType;

    pPwVcMplsMappingEntry = RBTreeGet (L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST
                                       (gpPwVcMplsGlobalInfo),
                                       &PwVcMplsMappingEntry);

    if (pPwVcMplsMappingEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "NonTe LSP is not used by L2VPN\n");
        return L2VPN_FAILURE;
    }

    TMO_DLL_Scan (&pPwVcMplsMappingEntry->PwInList, pNode, tTMO_DLL_NODE *)
    {
        pPwVcEntry = MPLS_PWVC_FROM_INLIST (pNode);

#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
        {
            MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                    (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
        }
        else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
        {
            MEMCPY ((UINT1 *) &GenU4Addr.Addr.u4Addr,
                    (UINT1 *) &pPwVcEntry->PeerAddr, IPV4_ADDR_LENGTH);
            GenU4Addr.Addr.u4Addr = OSIX_NTOHL (GenU4Addr.Addr.u4Addr);
        }
        GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

#ifdef MPLS_IPV6_WANTED
        L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Pw Entry found for this NON-TE TNL, "
                    "PW %d Peer Ipv4 - %x Ipv6 - %s \n",
                    pPwVcEntry->u4PwVcIndex, GenU4Addr.Addr.u4Addr,
                    Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
        L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                    "Pw Entry found for this NON-TE TNL, "
                    "PW %d peer %#x \n", pPwVcEntry->u4PwVcIndex,
                    GenU4Addr.Addr.u4Addr);
#endif

        switch (u4Event)
        {
            case L2VPN_MPLS_PWVC_TE_FRR_MP_ADD:
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "NON-TE LSP MP ADD event\n");

                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    i4Status = L2VpnMplsFrrILMHwAddForMP
                        (pPwVcEntry, pLspEvtInfo->u4PwVcMplsOutLsrXcIndex,
                         NULL);
                }
                else
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW not oper up\n");
                }
                break;
            }

            case L2VPN_MPLS_PWVC_TE_FRR_MP_DEL:
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG, "NON-TE LSP MP DEL event\n");

                if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
                {
                    if ((pLspEvtInfo->u4TnlBkpMPLabel3 ==
                         L2VPN_LDP_INVALID_LABEL) &&
                        (pLspEvtInfo->u4TnlBkpMPLabel2 !=
                         L2VPN_LDP_INVALID_LABEL))
                    {
                        pLspEvtInfo->u4TnlBkpMPLabel3 =
                            pLspEvtInfo->u4TnlBkpMPLabel2;
                        pLspEvtInfo->u4TnlBkpMPLabel2 = L2VPN_LDP_INVALID_LABEL;
                    }

                    i4Status = L2VpnMplsFrrILMHwDelForMP
                        (pPwVcEntry, pLspEvtInfo->u4TnlBkpMPLabel1,
                         pLspEvtInfo->u4TnlBkpMPLabel2,
                         pLspEvtInfo->u4TnlBkpMPLabel3, FALSE);
                }
                else
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "PW already oper down\n");
                }
                break;
            }

            default:
                break;
        }
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : L2VpnHandlePsnTnlUpOrDown                                 */
/* Description   : This function handles Psn Tnl Up/Down events for both TE  */
/*                 and NONTE. This function determines whether               */
/*                 Label Mapping/Notification/Withdrawl messages needs to be */
/*                 sent to peer or not. This function expects the events     */
/*                 L2VPN_MPLS_PWVC_TE_TNL_UP/L2VPN_MPLS_PWVC_LSP_UP and      */
/*                 L2VPN_MPLS_PWVC_TE_TNL_DOWN/L2VPN_MPLS_PWVC_LSP_DOWN and  */
/*                 will work only for Signalling L2VPN                       */
/* Input(s)      : pPwVcEntry - Pointer to PW VC Entry                       */
/*               : u4Event    - Events processed in this function are as     */
/*                              as mentioned                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : L2VPN_SUCCESS or L2VPN_FAILURE                            */
/*****************************************************************************/

INT4
L2VpnHandlePsnTnlUpOrDown (tPwVcEntry * pPwVcEntry, UINT4 u4Event)
{
    tL2vpnRedundancyPwEntry *pRgPw = NULL;
    tPwVcActivePeerSsnEntry *pSession = NULL;
    UINT4               u4PwIndex = L2VPN_ZERO;
    INT4                i4Status = L2VPN_FAILURE;
    UINT1               u1Event = L2VPN_ZERO;
    tGenU4Addr          GenU4Addr;

    MEMSET (&GenU4Addr, L2VPN_ZERO, sizeof (tGenU4Addr));

    u4PwIndex = pPwVcEntry->u4PwVcIndex;
#ifdef MPLS_IPV6_WANTED
    if (MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
    {
        MEMCPY ((UINT1 *) GenU4Addr.Addr.Ip6Addr.u1_addr,
                (UINT1 *) &(pPwVcEntry->PeerAddr), IPV6_ADDR_LENGTH);
    }
    else if (MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType)
#endif
    {
        GenU4Addr.Addr.u4Addr =
            OSIX_NTOHL (*(UINT4 *) (VOID *) &(pPwVcEntry->PeerAddr));
    }
    GenU4Addr.u2AddrType = (UINT2) pPwVcEntry->i4PeerAddrType;

    switch (u4Event)
    {
        case L2VPN_MPLS_PWVC_TE_TNL_UP:
        case L2VPN_MPLS_PWVC_LSP_UP:

            if (!L2VPN_IS_PSN_FACE_TX_FAULT (pPwVcEntry))
            {
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d for Peer Ipv4 - %x Ipv6 - %s is already not fault\n",
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d for Peer %x is already not fault\n",
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                return L2VPN_SUCCESS;
            }

            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) &=
                ~(L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT);

            i4Status = L2VpnSendLblMapOrNotifMsg (pPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "L2VpnHandlePsnTnlUpOrDown: "
                           "Failed to send Label Map or Notif Msg\n");
            }

            /* Update RG ICCP PW entry */
            /* If, Tunnel is created after LDP session establishment
             * this part is to be done
             */
            pRgPw =
                L2VpnGetRedundancyPwEntryByPwIndex (L2VPN_PWVC_INDEX
                                                    (pPwVcEntry));
            if (pRgPw != NULL)
            {
                if (L2VpnGetPeerSession
                    (IPV4, &L2VPN_PWVC_PEER_ADDR (pRgPw->pPwVcEntry),
                     &pSession) == L2VPN_FAILURE)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                               "Unable to Get LDP session\n");
                    return L2VPN_SUCCESS;
                }
                if (L2vpnGetIccpFrmSsnInfo (pRgPw->u4RgIndex, pSession,
                                            pPwVcEntry) == L2VPN_FAILURE)
                {
                    u1Event = (UINT1) L2VPNRED_EVENT_PW_ACTIVATE;
                    L2vpnRedundancyPwStateUpdate (pRgPw, u1Event);
                }
            }
            break;
        case L2VPN_MPLS_PWVC_TE_TNL_DOWN:
        case L2VPN_MPLS_PWVC_LSP_DOWN:
        case L2VPN_MPLS_PWVC_LSP_DESTROY:

            if (L2VPN_IS_PSN_FACE_TX_FAULT (pPwVcEntry))
            {
#ifdef MPLS_IPV6_WANTED
                L2VPN_DBG3 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d for Peer Ipv4 - %x Ipv6 - %s is already fault\n",
                            u4PwIndex, GenU4Addr.Addr.u4Addr,
                            Ip6PrintAddr (&(GenU4Addr.Addr.Ip6Addr)));
#else
                L2VPN_DBG2 (L2VPN_DBG_LVL_CRT_FLAG,
                            "PW %d for Peer %x is already fault\n",
                            u4PwIndex, GenU4Addr.Addr.u4Addr);
#endif
                return L2VPN_SUCCESS;
            }

            L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) |=
                L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT;

            i4Status = L2VpnSendLblWdrawOrNotifMsg (pPwVcEntry);
            if (i4Status != L2VPN_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Failed to send Label Wdraw or Notif Msg\n");
            }
            break;
        default:
            return L2VPN_FAILURE;
    }

    return i4Status;
}
