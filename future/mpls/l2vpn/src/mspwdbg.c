/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mspwdbg.c,v 1.6 2014/12/24 10:58:28 siva Exp $
 *
 * Description: This file contains the routines for the MSPW the module  
 *********************************************************************/

#include "l2vpincs.h"

/****************************************************************************
Function    :  L2vpnTestFsMsPwMaxEntries
Input       :  The Indices
pu4ErrorCode
u4FsMsPwMaxEntries
Output      :  This Routine will Test 
the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT4
L2vpnTestFsMsPwMaxEntries (UINT4 *pu4ErrorCode, UINT4 u4FsMsPwMaxEntries)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2 VPN module is Admin down\r\n");

        return SNMP_FAILURE;
    }

    if ((u4FsMsPwMaxEntries > MAX_L2VPN_MS_PW_ENTRIES) ||
        (u4FsMsPwMaxEntries < L2VPN_MIN_MSPW_ENTRIES))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "Beyond the  Max or minimum boundary \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  L2vpnGetAllFsMsPwConfigTable
Input       :  The Indices
pL2vpnFsMsPwConfigTable
Output      :  This Routine Take the Indices &
Gets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT4
L2vpnGetAllFsMsPwConfigTable (tL2vpnFsMsPwConfigTable *
                              pL2vpnGetFsMsPwConfigTable)
{
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;

    /* Check whether the node is already present */
    pL2vpnFsMsPwConfigTable =
        L2VpnGetMsPwEntry (pL2vpnGetFsMsPwConfigTable->u4FsMsPwIndex1,
                           pL2vpnGetFsMsPwConfigTable->u4FsMsPwIndex2);

    if (pL2vpnFsMsPwConfigTable == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "MSPW entry not found \r\n");
        return SNMP_FAILURE;
    }

    /* Assign values from the existing node */
    pL2vpnGetFsMsPwConfigTable->i4FsMsPwOperStatus =
        pL2vpnFsMsPwConfigTable->i4FsMsPwOperStatus;

    pL2vpnGetFsMsPwConfigTable->i4FsMsPwRowStatus =
        pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  L2vpnTestAllFsMsPwConfigTable
Input       :  The Indices
pu4ErrorCode
pL2vpnFsMsPwConfigTable
pL2vpnIsSetFsMsPwConfigTable
Output      :  This Routine Take the Indices &
Test the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT4
L2vpnTestAllFsMsPwConfigTable (UINT4 *pu4ErrorCode,
                               tL2vpnFsMsPwConfigTable *
                               pL2vpnSetFsMsPwConfigTable,
                               tL2vpnIsSetFsMsPwConfigTable *
                               pL2vpnIsSetFsMsPwConfigTable,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tPwVcEntry         *pPriPwVcEntry = NULL;    /*Primary PW VC */
    tPwVcEntry         *pSecPwVcEntry = NULL;    /*Secondary PW VC */
    UINT4               u4PriPwIndex =
        pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;
    UINT4               u4SecPwIndex =
        pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;

    L2VPN_DBG3 (L2VPN_DBG_LVL_DBG_FLAG, "\r\nL2vpnTestAllFsMsPwConfigTable"
                "Pri index=%d,Sec Index=%d,RS=%d\r\n", u4PriPwIndex,
                u4SecPwIndex, pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus);

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "L2 VPN module is Admin down \r\n");
        return SNMP_FAILURE;
    }

    pPriPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PriPwIndex);
    if (pPriPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "pri PW VC entry not found \r\n");
        return L2VPN_FAILURE;
    }

    pSecPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4SecPwIndex);
    if (pSecPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Sec PW VC entry not found \r\n");
        return L2VPN_FAILURE;
    }

    /*Both PW-VC type should be same */
    if (L2VPN_PWVC_TYPE (pPriPwVcEntry) != L2VPN_PWVC_TYPE (pSecPwVcEntry))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW-VC types are different \r\n");
        return SNMP_FAILURE;
    }

    if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus != OSIX_FALSE)
    {
        /* Check whether the node is already present */
        pL2vpnFsMsPwConfigTable =
            L2VpnGetMsPwEntry (u4PriPwIndex, u4SecPwIndex);

        switch (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus)
        {
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:

                /*No of configured MSPW entries should not exceed the maximum limit */
                if (gL2vpnMsPwGlobals.u4NoOfMsPwEntries >=
                    L2VPN_MAX_MSPW_ENTRIES (gL2vpnMsPwGlobals))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "Exceeded maximum limit\r\n");
                    return SNMP_FAILURE;
                }
                if (pL2vpnFsMsPwConfigTable != NULL)
                {

                    /* manager trying to create an existing row */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               " Entry Already Exist \r\n");
                    return SNMP_FAILURE;
                }

                /* PW VC attached Index should be zero */
                if ((L2VPN_PWVC_ATTACHED_PW_INDEX (pPriPwVcEntry) != L2VPN_ZERO)
                    || (L2VPN_PWVC_ATTACHED_PW_INDEX (pSecPwVcEntry) !=
                        L2VPN_ZERO))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "PW VC already attached \r\n");
                    return SNMP_FAILURE;
                }

                /* PW VC should not be attached to AC */
                if (L2VPN_PWVC_ENET_ENTRY (pPriPwVcEntry) != NULL ||
                    L2VPN_PWVC_ENET_ENTRY (pSecPwVcEntry) != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "PW VC already attached to AC\r\n");
                    return SNMP_FAILURE;
                }

                break;

            case NOT_READY:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            case ACTIVE:
            case DESTROY:
                if (pL2vpnFsMsPwConfigTable == NULL)
                {

                    /* manager trying to  access an non existing row */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "MS-PW Entry not exist \r\n");
                    return SNMP_FAILURE;
                }
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "invalid value is given \r\n");

                return SNMP_FAILURE;

        }                        /* end of switch */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  L2vpnSetAllFsMsPwConfigTable
Input       :  The Indices
pL2vpnFsMsPwConfigTable
pL2vpnIsSetFsMsPwConfigTable
i4RowStatusLogic
i4RowCreateOption
Output      :  This Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT4
L2vpnSetAllFsMsPwConfigTable (tL2vpnFsMsPwConfigTable *
                              pL2vpnSetFsMsPwConfigTable,
                              tL2vpnIsSetFsMsPwConfigTable *
                              pL2vpnIsSetFsMsPwConfigTable,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;
    tL2vpnFsMsPwConfigTable L2vpnOldFsMsPwConfigTable;
    tL2vpnFsMsPwConfigTable L2vpnTrgFsMsPwConfigTable;
    tL2vpnIsSetFsMsPwConfigTable L2vpnTrgIsSetFsMsPwConfigTable;
    INT4                i4RowMakeActive = FALSE;
    tPwVcEntry         *pPriPwVcEntry = NULL;    /*Primary PW VC */
    tPwVcEntry         *pSecPwVcEntry = NULL;    /*Secondary PW VC */

    MEMSET (&L2vpnOldFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));
    MEMSET (&L2vpnTrgFsMsPwConfigTable, 0, sizeof (tL2vpnFsMsPwConfigTable));
    MEMSET (&L2vpnTrgIsSetFsMsPwConfigTable, 0,
            sizeof (tL2vpnIsSetFsMsPwConfigTable));

    /*No of configured MSPW entries should not exceed the maximum limit */
    if (gL2vpnMsPwGlobals.u4NoOfMsPwEntries >
        L2VPN_MAX_MSPW_ENTRIES (gL2vpnMsPwGlobals))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Exceeded maximum limit \r\n");
        return SNMP_FAILURE;
    }

    /* Check whether the node is already present */
    pL2vpnFsMsPwConfigTable =
        L2VpnGetMsPwEntry (pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1,
                           pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2);

    pPriPwVcEntry =
        L2VpnGetPwVcEntryFromIndex (pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1);
    if (pPriPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "pri PW VC entry not found \r\n");
        return L2VPN_FAILURE;
    }

    pSecPwVcEntry =
        L2VpnGetPwVcEntryFromIndex (pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2);
    if (pSecPwVcEntry == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "Sec PW VC entry not found \r\n");
        return L2VPN_FAILURE;
    }

    if (pL2vpnFsMsPwConfigTable == NULL)
    {

        /* Create the node if the RowStatus given is CREATE_AND_WAIT
         * or CREATE_AND_GO */
        if ((pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == CREATE_AND_WAIT)
            || (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == CREATE_AND_GO)
            || ((pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == ACTIVE)
                && (i4RowCreateOption == 1)))
        {

            /* Allocate memory for the new node */
            pL2vpnFsMsPwConfigTable =
                (tL2vpnFsMsPwConfigTable *) MemAllocMemBlk (L2VPN_MSPW_POOL_ID);
            if (pL2vpnFsMsPwConfigTable == NULL)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_CRT_FLAG,
                           "Memory Creatin failed \r\n");
                return SNMP_FAILURE;
            }

            /* Assign values for the new node */
            if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 != OSIX_FALSE)
            {
                pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1 =
                    pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;
            }

            if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex2 != OSIX_FALSE)
            {
                pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2 =
                    pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;
            }

            if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus != OSIX_FALSE)
            {
                pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus =
                    pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus;
            }

            if ((pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1) &&
                    (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == ACTIVE)))

            {
                pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus = ACTIVE;
                /* For MSR and RM Trigger */
                L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex1 =
                    pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;
                L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex2 =
                    pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;
                L2vpnTrgFsMsPwConfigTable.i4FsMsPwRowStatus = CREATE_AND_GO;
                L2vpnTrgIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;
                if (L2vpnSetAllFsMsPwConfigTableTrigger
                    (&L2vpnTrgFsMsPwConfigTable,
                     &L2vpnTrgIsSetFsMsPwConfigTable,
                     SNMP_SUCCESS) != SNMP_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "MSR and RM Trigger failed \r\n");

                    return SNMP_FAILURE;
                }
            }
            else if (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus ==
                     CREATE_AND_WAIT)
            {
                /* For MSR and RM Trigger */
                pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus = NOT_READY;

                L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex1 =
                    pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;
                L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex2 =
                    pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;
                L2vpnTrgFsMsPwConfigTable.i4FsMsPwRowStatus = CREATE_AND_WAIT;
                L2vpnTrgIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;
                if (L2vpnSetAllFsMsPwConfigTableTrigger
                    (&L2vpnTrgFsMsPwConfigTable,
                     &L2vpnTrgIsSetFsMsPwConfigTable,
                     SNMP_SUCCESS) != SNMP_SUCCESS)
                {
                    L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                               "MSR and RM Trigger failed \r\n");
                    return SNMP_FAILURE;
                }

            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable,
                 (tRBElem *) pL2vpnFsMsPwConfigTable) != RB_SUCCESS)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                           "MSPW entry adding to RB tree failed \r\n");

                return SNMP_FAILURE;
            }

            /*Increment the No of configure MSPW entries */
            gL2vpnMsPwGlobals.u4NoOfMsPwEntries++;

            /* update the PW VC attached Index information */
            L2VPN_PWVC_ATTACHED_PW_INDEX (pPriPwVcEntry) =
                pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;

            L2VPN_PWVC_ATTACHED_PW_INDEX (pSecPwVcEntry) =
                pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;

            /* MS-PW status TLV: Clear the AC bit mask */
            pPriPwVcEntry->u1LocalStatus &= (UINT1)(~(L2VPN_PWVC_STATUS_AC_BITMASK));
            pSecPwVcEntry->u1LocalStatus &= (UINT1)(~(L2VPN_PWVC_STATUS_AC_BITMASK));

            if (L2vpnUtilUpdateFsMsPwConfigTable (NULL, pL2vpnFsMsPwConfigTable)
                != OSIX_TRUE)
            {
                RBTreeRem (gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.FsMsPwConfigTable,
                           pL2vpnFsMsPwConfigTable);

                /*Decrement the No of configure MSPW entries */
                gL2vpnMsPwGlobals.u4NoOfMsPwEntries--;

                /* update the attached PW VC Indexes */
                L2VPN_PWVC_ATTACHED_PW_INDEX (pPriPwVcEntry) = L2VPN_ZERO;
                L2VPN_PWVC_ATTACHED_PW_INDEX (pSecPwVcEntry) = L2VPN_ZERO;

                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        }
        else
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "MSPW entry is not found and RS is not valid one\r\n");

            return SNMP_FAILURE;
        }
    }
    else if ((pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == CREATE_AND_WAIT)
             || (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus ==
                 CREATE_AND_GO))
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "MSPW entry is found and RS can't be modified \r\n");

        return SNMP_FAILURE;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&L2vpnOldFsMsPwConfigTable, pL2vpnFsMsPwConfigTable,
            sizeof (tL2vpnFsMsPwConfigTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == DESTROY)
    {
        pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus = DESTROY;

        if (L2vpnUtilUpdateFsMsPwConfigTable (&L2vpnOldFsMsPwConfigTable,
                                              pL2vpnFsMsPwConfigTable) !=
            OSIX_TRUE)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "MSPW entry can't be deleted \r\n");
            return SNMP_FAILURE;
        }

        L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex1 =
            pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;
        L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex2 =
            pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;
        L2vpnTrgFsMsPwConfigTable.i4FsMsPwRowStatus = DESTROY;
        L2vpnTrgIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;
        /*MS PW node will be removed from RB tree after deleting from HW */
        if (L2vpnSetAllFsMsPwConfigTableTrigger (&L2vpnTrgFsMsPwConfigTable,
                                                 &L2vpnTrgIsSetFsMsPwConfigTable,
                                                 SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "MSR and RM trigger failed \r\n");

            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMsPwConfigTableFilterInputs
        (pL2vpnFsMsPwConfigTable, pL2vpnSetFsMsPwConfigTable,
         pL2vpnIsSetFsMsPwConfigTable) == OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus == ACTIVE))
    {
        pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        L2vpnTrgFsMsPwConfigTable.i4FsMsPwRowStatus = NOT_IN_SERVICE;
        L2vpnTrgIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;

        if (L2vpnUtilUpdateFsMsPwConfigTable (&L2vpnOldFsMsPwConfigTable,
                                              pL2vpnFsMsPwConfigTable) !=
            OSIX_TRUE)
        {

            /*Restore back with previous values */
            MEMCPY (pL2vpnFsMsPwConfigTable, &L2vpnOldFsMsPwConfigTable,
                    sizeof (tL2vpnFsMsPwConfigTable));

            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, " failed \r\n");

            return SNMP_FAILURE;
        }

        if (L2vpnSetAllFsMsPwConfigTableTrigger (&L2vpnTrgFsMsPwConfigTable,
                                                 &L2vpnTrgIsSetFsMsPwConfigTable,
                                                 SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                       "MSR and RM trigger failed \r\n");

            return SNMP_FAILURE;
        }
    }

    if (pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus != OSIX_FALSE)
    {
        pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus =
            pL2vpnSetFsMsPwConfigTable->i4FsMsPwRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus = ACTIVE;
    }

    if (L2vpnUtilUpdateFsMsPwConfigTable (&L2vpnOldFsMsPwConfigTable,
                                          pL2vpnFsMsPwConfigTable) != OSIX_TRUE)
    {

        /*Restore back with previous values */
        MEMCPY (pL2vpnFsMsPwConfigTable, &L2vpnOldFsMsPwConfigTable,
                sizeof (tL2vpnFsMsPwConfigTable));
        return SNMP_FAILURE;
    }

    L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex1 =
        pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex1;
    L2vpnTrgFsMsPwConfigTable.u4FsMsPwIndex2 =
        pL2vpnSetFsMsPwConfigTable->u4FsMsPwIndex2;
    L2vpnTrgFsMsPwConfigTable.i4FsMsPwRowStatus = ACTIVE;
    L2vpnTrgIsSetFsMsPwConfigTable.bFsMsPwRowStatus = OSIX_TRUE;
    if (L2vpnSetAllFsMsPwConfigTableTrigger (pL2vpnSetFsMsPwConfigTable,
                                             pL2vpnIsSetFsMsPwConfigTable,
                                             SNMP_SUCCESS) != SNMP_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG,
                   "MSPW entry is not found and RS is not valid one \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  L2vpnGetFsMsPwMaxEntries
Input       :  The Indices
pu4FsMsPwMaxEntries
Output      :  This Routine will Get 
the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT4
L2vpnGetFsMsPwMaxEntries (UINT4 *pu4FsMsPwMaxEntries)
{
    *pu4FsMsPwMaxEntries = gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.u4FsMsPwMaxEntries;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  L2vpnSetFsMsPwMaxEntries
Input       :  The Indices
u4FsMsPwMaxEntries
Output      :  This Routine will Set 
the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT4
L2vpnSetFsMsPwMaxEntries (UINT4 u4FsMsPwMaxEntries)
{
    gL2vpnMsPwGlobals.L2vpnMsPwGlbMib.u4FsMsPwMaxEntries = u4FsMsPwMaxEntries;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  L2vpnGetFirstFsMsPwConfigTable
Input       :  The Indices
NONE
Output      :  This Routine is used to take
first index from a table
Returns     :  pL2vpnFsMsPwConfigTable or NULL
 ****************************************************************************/
tL2vpnFsMsPwConfigTable *
L2vpnGetFirstFsMsPwConfigTable ()
{
    tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable = NULL;

    pL2vpnFsMsPwConfigTable =
        (tL2vpnFsMsPwConfigTable *) RBTreeGetFirst (gL2vpnMsPwGlobals.
                                                    L2vpnMsPwGlbMib.
                                                    FsMsPwConfigTable);

    return pL2vpnFsMsPwConfigTable;
}

/****************************************************************************
Function    :  L2vpnGetNextFsMsPwConfigTable
Input       :  The Indices
pCurrentL2vpnFsMsPwConfigTable
Output      :  This Routine is used to take
next index from a table
Returns     :  pNextL2vpnFsMsPwConfigTable or NULL
 ****************************************************************************/
tL2vpnFsMsPwConfigTable *
L2vpnGetNextFsMsPwConfigTable (tL2vpnFsMsPwConfigTable *
                               pCurrentL2vpnFsMsPwConfigTable)
{
    tL2vpnFsMsPwConfigTable *pNextL2vpnFsMsPwConfigTable = NULL;

    pNextL2vpnFsMsPwConfigTable =
        (tL2vpnFsMsPwConfigTable *) RBTreeGetNext (gL2vpnMsPwGlobals.
                                                   L2vpnMsPwGlbMib.
                                                   FsMsPwConfigTable,
                                                   (tRBElem *)
                                                   pCurrentL2vpnFsMsPwConfigTable,
                                                   NULL);

    return pNextL2vpnFsMsPwConfigTable;
}
