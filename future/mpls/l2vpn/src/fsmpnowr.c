/* $Id: fsmpnowr.c,v 1.4 2011/10/25 09:29:27 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmpnowr.h"
# include  "fsmpnodb.h"
# include  "l2vpincs.h"

VOID
RegisterFSMPNO ()
{
    SNMPRegisterMibWithLock (&fsmpnoOID, &fsmpnoEntry,
                             L2vpnLock, L2vpnUnLock, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmpnoOID, (const UINT1 *) "fsmpnotif");
}

VOID
UnRegisterFSMPNO ()
{
    SNMPUnRegisterMib (&fsmpnoOID, &fsmpnoEntry);
    SNMPDelSysorEntry (&fsmpnoOID, (const UINT1 *) "fsmpnotif");
}

INT4
FsMplsPwStatusNotifEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsPwStatusNotifEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsPwOAMStatusNotifEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsPwOAMStatusNotifEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsPwStatusNotifEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsPwStatusNotifEnable (pMultiData->i4_SLongValue));
}

INT4
FsMplsPwOAMStatusNotifEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsPwOAMStatusNotifEnable (pMultiData->i4_SLongValue));
}

INT4
FsMplsPwStatusNotifEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsPwStatusNotifEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsPwOAMStatusNotifEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsPwOAMStatusNotifEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsPwStatusNotifEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsPwStatusNotifEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsPwOAMStatusNotifEnableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsPwOAMStatusNotifEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
