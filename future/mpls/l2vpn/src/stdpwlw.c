/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwlw.c,v 1.70 2016/03/01 10:46:29 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fssnmp.h"
# include  "l2vpincs.h"
# include  "inmgrex.h"
# include  "indexmgr.h"

extern UINT4        FsMplsVplsVpnId[14];
extern UINT4        FsMplsVplsRowStatus[14];
extern UINT4        PwID[13];
extern UINT4        FsMplsL2VpnVplsIndex[14];
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwIndexNext
 Input       :  The Indices

                The Object 
                retValPwIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwIndexNext (UINT4 *pu4RetValPwIndexNext)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4RetValPwIndexNext = L2VPN_ZERO;
        return SNMP_SUCCESS;
    }
    *pu4RetValPwIndexNext = MplsL2VpnGetPwVcIndex (void);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwTable (UINT4 u4PwIndex)
{
    if (!((u4PwIndex >= L2VPN_PWVC_INDEX_MINVAL) &&
          (u4PwIndex <= L2VPN_PWVC_INDEX_MAXVAL)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwTable (UINT4 *pu4PwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4PwVcIndex;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    for (u4PwVcIndex = 1; u4PwVcIndex <=
         L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwVcIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

        if (pPwVcEntry != NULL)
        {
            *pu4PwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);
            return SNMP_SUCCESS;

        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4PwVcIndex;

    for (u4PwVcIndex = u4PwIndex; u4PwVcIndex <=
         L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwVcIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

        if (pPwVcEntry != NULL)
        {
            if (L2VPN_PWVC_INDEX (pPwVcEntry) > u4PwIndex)
            {
                *pu4NextPwIndex = L2VPN_PWVC_INDEX (pPwVcEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwType (UINT4 u4PwIndex, INT4 *pi4RetValPwType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwType = L2VPN_PWVC_TYPE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwOwner
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwOwner (UINT4 u4PwIndex, INT4 *pi4RetValPwOwner)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwOwner = L2VPN_PWVC_OWNER (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPsnType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPsnType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPsnType (UINT4 u4PwIndex, INT4 *pi4RetValPwPsnType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwPsnType = L2VPN_PWVC_PSN_TYPE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwSetUpPriority
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwSetUpPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwSetUpPriority (UINT4 u4PwIndex, INT4 *pi4RetValPwSetUpPriority)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwSetUpPriority = L2VPN_PWVC_SETUP_PRIORITY (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwHoldingPriority
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwHoldingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwHoldingPriority (UINT4 u4PwIndex, INT4 *pi4RetValPwHoldingPriority)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwHoldingPriority = L2VPN_PWVC_HOLDING_PRIORITY (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetPwPeerAddrType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPeerAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPeerAddrType (UINT4 u4PwIndex, INT4 *pi4RetValPwPeerAddrType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwPeerAddrType = pPwVcEntry->i4PeerAddrType;
    }
    else
    {
        *pi4RetValPwPeerAddrType = INET_ADDR_TYPE_UNKNOWN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPwPeerAddr
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPeerAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPeerAddr (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE * pRetValPwPeerAddr)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {

#ifdef MPLS_IPV6_WANTED
        if ( MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType )
        {
            CPY_TO_SNMP (pRetValPwPeerAddr, &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                         IPV6_ADDR_LENGTH);
        }
        else if ( MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType )
#endif
        {
            CPY_TO_SNMP (pRetValPwPeerAddr, &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                         IPV4_ADDR_LENGTH);
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwAttachedPwIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwAttachedPwIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwAttachedPwIndex (UINT4 u4PwIndex, UINT4 *pu4RetValPwAttachedPwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwAttachedPwIndex = L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwIfIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwIfIndex (UINT4 u4PwIndex, INT4 *pi4RetValPwIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwIfIndex = L2VPN_PWVC_IF_INDEX (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwID (UINT4 u4PwIndex, UINT4 *pu4RetValPwID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwID = L2VPN_PWVC_ID (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLocalGroupID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLocalGroupID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLocalGroupID (UINT4 u4PwIndex, UINT4 *pu4RetValPwLocalGroupID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwLocalGroupID = L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwGroupAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwGroupAttachmentID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwGroupAttachmentID (UINT4 u4PwIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValPwGroupAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwGroupAttachmentID,
                     L2VPN_PWVC_AGI (pPwVcEntry),
                     L2VPN_PWVC_AGI_LEN (pPwVcEntry));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLocalAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLocalAttachmentID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLocalAttachmentID (UINT4 u4PwIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValPwLocalAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwLocalAttachmentID,
                     L2VPN_PWVC_SAII (pPwVcEntry),
                     L2VPN_PWVC_SAII_LEN (pPwVcEntry));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPeerAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPeerAttachmentID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPeerAttachmentID (UINT4 u4PwIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValPwPeerAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwPeerAttachmentID,
                     L2VPN_PWVC_TAII (pPwVcEntry),
                     L2VPN_PWVC_TAII_LEN (pPwVcEntry));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwCwPreference
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwCwPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwCwPreference (UINT4 u4PwIndex, INT4 *pi4RetValPwCwPreference)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwCwPreference = L2VPN_PWVC_CNTRL_WORD (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLocalIfMtu
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLocalIfMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLocalIfMtu (UINT4 u4PwIndex, UINT4 *pu4RetValPwLocalIfMtu)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwLocalIfMtu = L2VPN_PWVC_LOCAL_IFACE_MTU (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLocalIfString
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLocalIfString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLocalIfString (UINT4 u4PwIndex, INT4 *pi4RetValPwLocalIfString)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwLocalIfString = L2VPN_PWVC_LOCAL_IFACE_STRING (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLocalCapabAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLocalCapabAdvert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLocalCapabAdvert (UINT4 u4PwIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValPwLocalCapabAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwLocalCapabAdvert,
                     &L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry), L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRemoteGroupID
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRemoteGroupID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRemoteGroupID (UINT4 u4PwIndex, UINT4 *pu4RetValPwRemoteGroupID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwRemoteGroupID = L2VPN_PWVC_REMOTE_GRP_ID (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwCwStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwCwStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwCwStatus (UINT4 u4PwIndex, INT4 *pi4RetValPwCwStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwCwStatus = L2VPN_PWVC_CW_STATUS (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRemoteIfMtu
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRemoteIfMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRemoteIfMtu (UINT4 u4PwIndex, UINT4 *pu4RetValPwRemoteIfMtu)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwRemoteIfMtu = L2VPN_PWVC_REMOTE_IFACE_MTU (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRemoteIfString
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRemoteIfString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRemoteIfString (UINT4 u4PwIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValPwRemoteIfString)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwRemoteIfString,
                     pPwVcEntry->au1RemoteIfString,
                     STRLEN (pPwVcEntry->au1RemoteIfString));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRemoteCapabilities
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRemoteCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRemoteCapabilities (UINT4 u4PwIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPwRemoteCapabilities)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwRemoteCapabilities,
                     &pPwVcEntry->u1RemoteCapabilities, L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwFragmentCfgSize
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwFragmentCfgSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwFragmentCfgSize (UINT4 u4PwIndex, UINT4 *pu4RetValPwFragmentCfgSize)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwFragmentCfgSize = L2VPN_PWVC_FRAG_CFG_SIZE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRmtFragCapability
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRmtFragCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRmtFragCapability (UINT4 u4PwIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValPwRmtFragCapability)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwRmtFragCapability,
                     &pPwVcEntry->u1RmtFragCapability, L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwFcsRetentioncfg
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwFcsRetentioncfg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwFcsRetentioncfg (UINT4 u4PwIndex, INT4 *pi4RetValPwFcsRetentioncfg)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwFcsRetentioncfg = L2VPN_PWVC_FCS_RETN_CFG (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwFcsRetentionStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwFcsRetentionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwFcsRetentionStatus (UINT4 u4PwIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPwFcsRetentionStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwFcsRetentionStatus,
                     &pPwVcEntry->u1FcsRetentionStatus, L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwOutboundLabel
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwOutboundLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwOutboundLabel (UINT4 u4PwIndex, UINT4 *pu4RetValPwOutboundLabel)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwOutboundLabel = L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwInboundLabel
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwInboundLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwInboundLabel (UINT4 u4PwIndex, UINT4 *pu4RetValPwInboundLabel)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwInboundLabel = L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwName
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwName (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE * pRetValPwName)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwName, pPwVcEntry->au1PwVcName,
                     STRLEN (pPwVcEntry->au1PwVcName));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwDescr
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwDescr (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE * pRetValPwDescr)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwDescr, pPwVcEntry->au1PwVcDescr,
                     STRLEN (pPwVcEntry->au1PwVcDescr));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwCreateTime
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwCreateTime (UINT4 u4PwIndex, UINT4 *pu4RetValPwCreateTime)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwCreateTime = L2VPN_PWVC_CREATE_TIME (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}


/****************************************************************************
 Function    :  nmhGetPwUpTime
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwUpTime (UINT4 u4PwIndex, UINT4 *pu4RetValPwUpTime)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4CurrTimeTicks;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwUpTime = L2VPN_ZERO;
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            OsixGetSysTime (&u4CurrTimeTicks);
            *pu4RetValPwUpTime
                = u4CurrTimeTicks - L2VPN_PWVC_UP_TIME (pPwVcEntry);
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLastChange
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLastChange (UINT4 u4PwIndex, UINT4 *pu4RetValPwLastChange)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValPwLastChange = L2VPN_PWVC_LAST_CHANGE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwAdminStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwAdminStatus (UINT4 u4PwIndex, INT4 *pi4RetValPwAdminStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwAdminStatus = L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwOperStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwOperStatus (UINT4 u4PwIndex, INT4 *pi4RetValPwOperStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwOperStatus = L2VPN_PWVC_OPER_STATUS (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwLocalStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwLocalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwLocalStatus (UINT4 u4PwIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValPwLocalStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwLocalStatus,
                     &L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry), L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRemoteStatusCapable
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRemoteStatusCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRemoteStatusCapable (UINT4 u4PwIndex,
                             INT4 *pi4RetValPwRemoteStatusCapable)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwRemoteStatusCapable = L2VPN_PWVC_RMT_STAT_CAP (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRemoteStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRemoteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRemoteStatus (UINT4 u4PwIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValPwRemoteStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValPwRemoteStatus,
                     &L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry), L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwTimeElapsed
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwTimeElapsed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwTimeElapsed (UINT4 u4PwIndex, INT4 *pi4RetValPwTimeElapsed)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwTimeElapsed = L2VPN_PWVC_TIME_ELAPSED (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwValidIntervals
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwValidIntervals
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwValidIntervals (UINT4 u4PwIndex, INT4 *pi4RetValPwValidIntervals)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwValidIntervals = L2VPN_PWVC_VALID_INTERVALS (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwRowStatus (UINT4 u4PwIndex, INT4 *pi4RetValPwRowStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwRowStatus = L2VPN_PWVC_ROW_STATUS (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwStorageType (UINT4 u4PwIndex, INT4 *pi4RetValPwStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValPwStorageType = L2VPN_PWVC_STORAGE_TYPE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPwType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwType (UINT4 u4PwIndex, INT4 i4SetValPwType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_TYPE (pPwVcEntry) = (INT1) i4SetValPwType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwOwner
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwOwner (UINT4 u4PwIndex, INT4 i4SetValPwOwner)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_OWNER (pPwVcEntry) = (INT1) i4SetValPwOwner;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwPsnType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwPsnType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwPsnType (UINT4 u4PwIndex, INT4 i4SetValPwPsnType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1PwVcPsnType = (INT1) i4SetValPwPsnType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwSetUpPriority
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwSetUpPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwSetUpPriority (UINT4 u4PwIndex, INT4 i4SetValPwSetUpPriority)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1SetUpPriority = (INT1) i4SetValPwSetUpPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwHoldingPriority
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwHoldingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwHoldingPriority (UINT4 u4PwIndex, INT4 i4SetValPwHoldingPriority)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1HoldingPriority = (INT1) i4SetValPwHoldingPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwPeerAddrType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwPeerAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwPeerAddrType (UINT4 u4PwIndex, INT4 i4SetValPwPeerAddrType)
{

    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i4PeerAddrType = i4SetValPwPeerAddrType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwPeerAddr
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwPeerAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwPeerAddr (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE * pSetValPwPeerAddr)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef MPLS_IPV6_WANTED
        if ( MPLS_IPV6_ADDR_TYPE == pPwVcEntry->i4PeerAddrType )
        {
            CPY_FROM_SNMP (&L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                           pSetValPwPeerAddr, IPV6_ADDR_LENGTH);
        }
        else if ( MPLS_IPV4_ADDR_TYPE == pPwVcEntry->i4PeerAddrType )
#endif
        {
            CPY_FROM_SNMP (&L2VPN_PWVC_PEER_ADDR (pPwVcEntry),
                           pSetValPwPeerAddr, IPV4_ADDR_LENGTH);
        }
 

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwAttachedPwIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwAttachedPwIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwAttachedPwIndex (UINT4 u4PwIndex, UINT4 u4SetValPwAttachedPwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)        /* MS-PW */
    {
        L2VPN_DBG (L2VPN_DBG_LVL_ERR_FLAG, "PW VC entry not found\r\n");
        return L2VPN_FAILURE;
    }

    /* Attached Index should be zero and PW is being attached must be
     * created before creating PW cross connection */
    if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != L2VPN_ZERO ||
        (L2VpnGetPwVcEntryFromIndex (u4SetValPwAttachedPwIndex) == NULL))
    {
        return SNMP_FAILURE;
    }
    /* MS-PW */
    L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) = u4SetValPwAttachedPwIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPwIfIndex
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwIfIndex (UINT4 u4PwIndex, INT4 i4SetValPwIfIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4OldPwIfIndex = 0;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (L2VPN_PWVC_IF_INDEX (pPwVcEntry) == (UINT4) i4SetValPwIfIndex)
    {
        return SNMP_SUCCESS;
    }
    /* Mapping between CFA PW IfIndex and MPLS PW Entry */
    if (i4SetValPwIfIndex != L2VPN_ZERO)
    {
        u4OldPwIfIndex = L2VPN_PWVC_IF_INDEX (pPwVcEntry);
        L2VPN_PWVC_IF_INDEX (pPwVcEntry) = (UINT4)i4SetValPwIfIndex;

        if (L2VpnPwIfIndexAddNode (pPwVcEntry) == OSIX_FAILURE)
        {
            /* Mapping between CFA PW IfIndex and MPLS PW Entry 
             * is failed, so reset the PwIfIndex in pPwVcEntry */
            L2VPN_PWVC_IF_INDEX (pPwVcEntry) = u4OldPwIfIndex;
            return SNMP_FAILURE;
        }

        /* Mapping between CFA PW IfIndex and MPLS PW Entry is
         * succes, so send the PW operstatus to CFA  */
        CfaInterfaceStatusChangeIndication (L2VPN_PWVC_IF_INDEX (pPwVcEntry),
                                            L2VPN_PWVC_OPER_STATUS
                                            (pPwVcEntry));
    }
    /* Unmapping between CFA PW IfIndex and MPLS PW Entry */
    else
    {
        /* Unmapping between CFA PW IfIndex and MPLS PW Entry is
         * success, so send the PW operstatus to CFA  */
        CfaInterfaceStatusChangeIndication (L2VPN_PWVC_IF_INDEX (pPwVcEntry),
                                            CFA_IF_DOWN);
#ifdef HVPLS_WANTED
	 /*Before Deleting Node, let registered api:ERPS knows about the interface*/
        L2vpnExtNotifyPwIfStatusToApp(pPwVcEntry, CFA_IF_DOWN);
#endif
        L2VpnPwIfIndexDeleteNode (pPwVcEntry);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPwID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwID (UINT4 u4PwIndex, UINT4 u4SetValPwID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pPwVcEntry->u4PwVcID == u4SetValPwID)
    {
        return SNMP_SUCCESS;
    }
    /* Remove the entry from RbTree */
    L2VpnPwIdDelNode (pPwVcEntry);
    /* Set the PWId in PwVcEntry */
    pPwVcEntry->u4PwVcID = u4SetValPwID;

    /* set in PwId based RbTree */
    if (L2VpnPwIdAddNode (pPwVcEntry) != OSIX_SUCCESS)
    {
        pPwVcEntry->u4PwVcID = 0;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPwLocalGroupID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwLocalGroupID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwLocalGroupID (UINT4 u4PwIndex, UINT4 u4SetValPwLocalGroupID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VPN_PWVC_LOCAL_GRP_ID (pPwVcEntry) = u4SetValPwLocalGroupID;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwGroupAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwGroupAttachmentID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwGroupAttachmentID (UINT4 u4PwIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValPwGroupAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_FROM_SNMP (L2VPN_PWVC_AGI (pPwVcEntry),
                       pSetValPwGroupAttachmentID,
                       pSetValPwGroupAttachmentID->i4_Length);
        L2VPN_PWVC_AGI_LEN (pPwVcEntry) =
            (UINT1) pSetValPwGroupAttachmentID->i4_Length;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwLocalAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwLocalAttachmentID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwLocalAttachmentID (UINT4 u4PwIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValPwLocalAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_FROM_SNMP (L2VPN_PWVC_SAII (pPwVcEntry),
                       pSetValPwLocalAttachmentID,
                       pSetValPwLocalAttachmentID->i4_Length);
        L2VPN_PWVC_SAII_LEN (pPwVcEntry) = (UINT1)
            pSetValPwLocalAttachmentID->i4_Length;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwPeerAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwPeerAttachmentID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwPeerAttachmentID (UINT4 u4PwIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValPwPeerAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_FROM_SNMP (L2VPN_PWVC_TAII (pPwVcEntry),
                       pSetValPwPeerAttachmentID,
                       pSetValPwPeerAttachmentID->i4_Length);
        L2VPN_PWVC_TAII_LEN (pPwVcEntry) =
            (UINT1) pSetValPwPeerAttachmentID->i4_Length;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwCwPreference
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwCwPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwCwPreference (UINT4 u4PwIndex, INT4 i4SetValPwCwPreference)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1ControlWord = (INT1) i4SetValPwCwPreference;
        if (MPLS_SNMP_TRUE == i4SetValPwCwPreference)
        {
            L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_CWPRESENT;
        }
        else if (MPLS_SNMP_FALSE == i4SetValPwCwPreference)
        {
            L2VPN_PWVC_CW_STATUS (pPwVcEntry) = L2VPN_PWVC_CWNOTPRESENT;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwLocalIfMtu
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwLocalIfMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwLocalIfMtu (UINT4 u4PwIndex, UINT4 u4SetValPwLocalIfMtu)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->u2LocalIfMtu = (UINT2) u4SetValPwLocalIfMtu;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwLocalIfString
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwLocalIfString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwLocalIfString (UINT4 u4PwIndex, INT4 i4SetValPwLocalIfString)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1LocalIfString = (INT1) i4SetValPwLocalIfString;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwLocalCapabAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwLocalCapabAdvert
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwLocalCapabAdvert (UINT4 u4PwIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValPwLocalCapabAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_FROM_SNMP (&L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry),
                       pSetValPwLocalCapabAdvert, L2VPN_ONE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwFragmentCfgSize
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwFragmentCfgSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwFragmentCfgSize (UINT4 u4PwIndex, UINT4 u4SetValPwFragmentCfgSize)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->u4FragmentCfgSize = u4SetValPwFragmentCfgSize;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwFcsRetentioncfg
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwFcsRetentioncfg
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwFcsRetentioncfg (UINT4 u4PwIndex, INT4 i4SetValPwFcsRetentioncfg)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1FcsRetentionCfg = (INT1) i4SetValPwFcsRetentioncfg;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwOutboundLabel
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwOutboundLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwOutboundLabel (UINT4 u4PwIndex, UINT4 u4SetValPwOutboundLabel)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        L2VpnPwSetOutVcLabel (pPwVcEntry, u4SetValPwOutboundLabel);

        if (u4SetValPwOutboundLabel == L2VPN_INVALID_LABEL)
        {
            pPwVcEntry->bIsStaticPw = FALSE;
        }
        else
        {
            pPwVcEntry->bIsStaticPw = TRUE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}




/****************************************************************************
 Function    :  nmhSetPwInboundLabel
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwInboundLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwInboundLabel (UINT4 u4PwIndex, UINT4 u4SetValPwInboundLabel)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry) != L2VPN_LDP_INVALID_LABEL)
    {
        if (MplsReleaseLblToLblGroup (L2VPN_PWVC_INBOUND_VC_LABEL
                                      (pPwVcEntry)) != MPLS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    if (MplsAssignLblToLblGroup (u4SetValPwInboundLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    L2VpnPwSetInVcLabel (pPwVcEntry, u4SetValPwInboundLabel);

    if (u4SetValPwInboundLabel == L2VPN_INVALID_LABEL)
    {
        pPwVcEntry->bIsStaticPw = FALSE;
    }
    else
    {
        pPwVcEntry->bIsStaticPw = TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPwName
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwName (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE * pSetValPwName)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_FROM_SNMP (pPwVcEntry->au1PwVcName, pSetValPwName,
                       pSetValPwName->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwDescr
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwDescr (UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE * pSetValPwDescr)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_FROM_SNMP (pPwVcEntry->au1PwVcDescr, pSetValPwDescr,
                       pSetValPwDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwAdminStatus
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwAdminStatus (UINT4 u4PwIndex, INT4 i4SetValPwAdminStatus)
{
    INT4                i4RetStatus;

    i4RetStatus = L2VpnSetPwAdminStatus (u4PwIndex, i4SetValPwAdminStatus);

    if ( L2VPN_FAILURE == i4RetStatus )
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPwRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwRowStatus (UINT4 u4PwIndex, INT4 i4SetValPwRowStatus)
{
    INT4                i4RetStatus;

    i4RetStatus = L2VpnSetPwRowStatus(u4PwIndex, i4SetValPwRowStatus);

    if ( L2VPN_FAILURE == i4RetStatus )
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPwStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                setValPwStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwStorageType (UINT4 u4PwIndex, INT4 i4SetValPwStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        pPwVcEntry->i1StorageType = (INT1) i4SetValPwStorageType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PwType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex, INT4 i4TestValPwType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwType)
    {
        case L2VPN_PWVC_TYPE_ETH_VLAN:
        case L2VPN_PWVC_TYPE_ETH:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwOwner
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwOwner (UINT4 *pu4ErrorCode, UINT4 u4PwIndex, INT4 i4TestValPwOwner)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwOwner)
    {
        case L2VPN_PWVC_OWNER_MANUAL:
        case L2VPN_PWVC_OWNER_PWID_FEC_SIG:
        case L2VPN_PWVC_OWNER_GEN_FEC_SIG:
            return SNMP_SUCCESS;
        case L2VPN_PWVC_OWNER_L2TP_PROTOCOL:
        case L2VPN_PWVC_OWNER_OTHER:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwPsnType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwPsnType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwPsnType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                    INT4 i4TestValPwPsnType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwPsnType)
    {
        case L2VPN_PWVC_PSN_TYPE_MPLS:
            return SNMP_SUCCESS;
        case L2VPN_PWVC_PSN_TYPE_L2TP:
        case L2VPN_PWVC_PSN_TYPE_IP:
        case L2VPN_PWVC_PSN_TYPE_MPLSOVERIP:
        case L2VPN_PWVC_PSN_TYPE_GRE:
        case L2VPN_PWVC_PSN_TYPE_OTHER:
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwSetUpPriority
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwSetUpPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwSetUpPriority (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                          INT4 i4TestValPwSetUpPriority)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValPwSetUpPriority < L2VPN_PWVC_SETUP_PRIO_MINVAL) ||
        (i4TestValPwSetUpPriority > L2VPN_PWVC_SETUP_PRIO_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PwHoldingPriority
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwHoldingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwHoldingPriority (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            INT4 i4TestValPwHoldingPriority)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValPwHoldingPriority < L2VPN_PWVC_HOLD_PRIO_MINVAL) ||
        (i4TestValPwHoldingPriority > L2VPN_PWVC_HOLD_PRIO_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PwPeerAddrType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwPeerAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwPeerAddrType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         INT4 i4TestValPwPeerAddrType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValPwPeerAddrType != MPLS_IPV4_ADDR_TYPE) &&
        (i4TestValPwPeerAddrType != MPLS_IPV6_ADDR_TYPE) &&
        (i4TestValPwPeerAddrType != INET_ADDR_TYPE_UNKNOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2PwPeerAddr
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwPeerAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwPeerAddr (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                     tSNMP_OCTET_STRING_TYPE * pTestValPwPeerAddr)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    uGenU4Addr         PeerAddr;
    
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValPwPeerAddr->i4_Length > L2VPN_PWVC_MAX_PEER_ADDR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
#ifdef MPLS_IPV6_WANTED
        if (pPwVcEntry->i4PeerAddrType == MPLS_IPV6_ADDR_TYPE)
        {
            
           if(MplsValidateIPv6Addr (pu4ErrorCode,pTestValPwPeerAddr) != SNMP_SUCCESS)
           {
                 return SNMP_FAILURE;  
           }
        }

        if (pPwVcEntry->i4PeerAddrType == MPLS_IPV4_ADDR_TYPE)
#endif
        {
            CPY_FROM_SNMP ((UINT1 *) &PeerAddr.u4Addr, pTestValPwPeerAddr,
                           IPV4_ADDR_LENGTH);
            PeerAddr.u4Addr = OSIX_NTOHL (PeerAddr.u4Addr);

            if (((PeerAddr.u4Addr == L2VPN_NULL_ADDR) ||
                 (PeerAddr.u4Addr == L2VPN_BCAST_ADDR) ||
                  (PeerAddr.u4Addr == L2VPN_LOOPBACK_ADDR)) ||
                ((PeerAddr.u4Addr >= L2VPN_MCAST_NET_ADDR) &&
                 (PeerAddr.u4Addr <= L2VPN_MCAST_NET_INV_ADDR)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (NetIpv4IfIsOurAddress (PeerAddr.u4Addr) == NETIPV4_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n peer ip is self ip :");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
   } 
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}



/****************************************************************************
 Function    :  nmhTestv2PwAttachedPwIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwAttachedPwIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwAttachedPwIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            UINT4 u4TestValPwAttachedPwIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    L2VPN_SUPPRESS_WARNING (u4TestValPwAttachedPwIndex);

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* MS-PW: AC shouldn't be attached */
        if (L2VPN_PWVC_ENET_ENTRY (pPwVcEntry) != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* Attached Index should be zero and PW is being attached must be
         * created before creating PW cross connection */
        if (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != L2VPN_ZERO ||
            (L2VpnGetPwVcEntryFromIndex (u4TestValPwAttachedPwIndex) == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* MS-PW */
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwIfIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwIfIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                    INT4 i4TestValPwIfIndex)
{
    UINT1               u1IfType = 0;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwIfIndexMap       PwIfIndexMap;
    tPwIfIndexMap      *pPwIfIndexMap = NULL;

    MEMSET (&PwIfIndexMap, L2VPN_ZERO, sizeof (tPwIfIndexMap));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /*if value is non-zero that means we want to set the pwIfIndex */
    if (i4TestValPwIfIndex != L2VPN_ZERO)
    {
        if (CfaValidateIfIndex ((UINT4)i4TestValPwIfIndex) != CFA_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        CfaGetIfaceType (i4TestValPwIfIndex, &u1IfType);

        if (u1IfType != CFA_PSEUDO_WIRE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    /*if value is zero means we are reset the pwIfIndex value as Zero */
    else
    {
        if (L2VPN_PWVC_IF_INDEX (pPwVcEntry) == (UINT4) i4TestValPwIfIndex)
        {
            return SNMP_SUCCESS;
        }
        pPwIfIndexMap = L2VpnPwIfIndexGetNode (pPwVcEntry->u4PwIfIndex);

        if (pPwIfIndexMap == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValPwIfIndex != L2VPN_ZERO) &&
        (pPwVcEntry->u4PwIfIndex != L2VPN_ZERO) &&
        (pPwVcEntry->u4PwIfIndex != (UINT4) i4TestValPwIfIndex))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex, UINT4 u4TestValPwID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (u4TestValPwID == L2VPN_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (u4TestValPwID);

    if ((pPwVcEntry != NULL) && (pPwVcEntry->u4PwVcIndex != u4PwIndex))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwLocalGroupID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwLocalGroupID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwLocalGroupID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         UINT4 u4TestValPwLocalGroupID)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    L2VPN_SUPPRESS_WARNING (u4TestValPwLocalGroupID);

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValPwLocalGroupID == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* LocalGroupId is supported only in PWID FEC */
        if (!(L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_L2TP_PROTOCOL
              || L2VPN_PWVC_OWNER (pPwVcEntry) ==
              L2VPN_PWVC_OWNER_PWID_FEC_SIG))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwGroupAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwGroupAttachmentID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwGroupAttachmentID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValPwGroupAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* As per RFC 4446, Section 3.4.2 AGI Type-1 
         * length should be fixed to 8 bytes 
         * */
        if (L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry) == L2VPN_GEN_PWVC_AGI_TYPE_1)
        {
            if (pTestValPwGroupAttachmentID->i4_Length !=
                L2VPN_PWVC_MAX_AGI_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }
        }
        /* GroupAttachmentId is supported only in GEN FEC */
        if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwLocalAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwLocalAttachmentID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwLocalAttachmentID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValPwLocalAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* Check for AII Type. 
         * As per RFC 4446, Type-1 AII value should be of fixed length (4 bytes)
         * As per RFC 5003, Type-II AII should be of fixed length (12 bytes)
         * */
        if (L2VpnValidateLenForAIIType
            (L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry),
             pTestValPwLocalAttachmentID->i4_Length) != L2VPN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        /* LocalAttachmentId is supported only in GEN FEC */
        if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwPeerAttachmentID
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwPeerAttachmentID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwPeerAttachmentID (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValPwPeerAttachmentID)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* Check for AII Type. 
         * As per RFC 4446, Type-1 AII value should be of fixed length (4 bytes)
         * As per RFC 5003, Type-II AII should be of fixed length (12 bytes)
         */
        if (L2VpnValidateLenForAIIType
            (L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry),
             pTestValPwPeerAttachmentID->i4_Length) != L2VPN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        /* PeerAttachmentId is supported only in GEN FEC */
        if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwCwPreference
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwCwPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwCwPreference (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         INT4 i4TestValPwCwPreference)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwCwPreference)
    {
        case MPLS_SNMP_TRUE:
        case MPLS_SNMP_FALSE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwLocalIfMtu
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwLocalIfMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwLocalIfMtu (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                       UINT4 u4TestValPwLocalIfMtu)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4TestValPwLocalIfMtu > L2VPN_PWVC_MIN_MTU_SIZE) &&
        (u4TestValPwLocalIfMtu <= L2VPN_PWVC_MAX_MTU_SIZE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PwLocalIfString
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwLocalIfString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwLocalIfString (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                          INT4 i4TestValPwLocalIfString)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwLocalIfString)
    {
        case MPLS_SNMP_TRUE:
        case MPLS_SNMP_FALSE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwLocalCapabAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwLocalCapabAdvert
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwLocalCapabAdvert (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValPwLocalCapabAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1LocalCapAdv = 0;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    CPY_FROM_SNMP (&u1LocalCapAdv, pTestValPwLocalCapabAdvert, L2VPN_ONE);

    switch (u1LocalCapAdv)
    {
        case L2VPN_PW_STATUS_INDICATION:
        case L2VPN_ZERO:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwFragmentCfgSize
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwFragmentCfgSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwFragmentCfgSize (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            UINT4 u4TestValPwFragmentCfgSize)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    L2VPN_SUPPRESS_WARNING (u4TestValPwFragmentCfgSize);

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwFcsRetentioncfg
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwFcsRetentioncfg
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwFcsRetentioncfg (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            INT4 i4TestValPwFcsRetentioncfg)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwFcsRetentioncfg)
    {
        case L2VPN_PWVC_FCSRETN_DISABLE:
        case L2VPN_PWVC_FCSRETN_ENABLE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PwOutboundLabel
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwOutboundLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwOutboundLabel (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                          UINT4 u4TestValPwOutboundLabel)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4TestValPwOutboundLabel >=
         GEN_MIN_LBL)
        && (u4TestValPwOutboundLabel <=
            GEN_MAX_LBL))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PwInboundLabel
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwInboundLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwInboundLabel (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                         UINT4 u4TestValPwInboundLabel)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4TestValPwInboundLabel >=
         gSystemSize.MplsSystemSize.u4MinStaticLblRange)
        && (u4TestValPwInboundLabel <=
            gSystemSize.MplsSystemSize.u4MaxStaticLblRange))
    {
        if (MplsCheckLabelInGroup (u4TestValPwInboundLabel) == MPLS_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PwName
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2PwName (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                 tSNMP_OCTET_STRING_TYPE * pTestValPwName)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValPwName->i4_Length > L2VPN_PWVC_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwDescr
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwDescr (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                  tSNMP_OCTET_STRING_TYPE * pTestValPwDescr)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValPwDescr->i4_Length > L2VPN_PWVC_MAX_DSCR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwAdminStatus
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwAdminStatus (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                        INT4 i4TestValPwAdminStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsEntry     *pPwVcMplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) != L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwAdminStatus)
    {
        case L2VPN_PWVC_ADMIN_UP:
            /*Check whether the Tunnel is valid for PW association */
            pPwVcMplsEntry =
                (tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry);

            if ((pPwVcMplsEntry != NULL) && (L2VPN_PWVC_MPLS_MPLS_TYPE
                                             (pPwVcMplsEntry) ==
                                             L2VPN_MPLS_TYPE_TE))
            {
                if (L2VpnCheckValidTnlForPw (pPwVcEntry) == L2VPN_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            return SNMP_SUCCESS;
        case L2VPN_PWVC_ADMIN_DOWN:
        case L2VPN_PWVC_ADMIN_TESTING:
            return SNMP_SUCCESS;
        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 cFunction    :  nmhTestv2PwRowStatus
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwRowStatus (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                      INT4 i4TestValPwRowStatus)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4PwIndex > L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo) || u4PwIndex < 1)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    switch (i4TestValPwRowStatus)
    {
        case L2VPN_PWVC_CREATEANDWAIT:
            if (pPwVcEntry != NULL)
            {
                /* manager trying to create an existing row */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;

        case L2VPN_PWVC_ACTIVE:
            if (pPwVcEntry == NULL)
            {
                /* manager trying to access non-existing row */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if ((L2VPN_PWVC_ID (pPwVcEntry) == L2VPN_ZERO) &&
                (L2VPN_PWVC_OWNER (pPwVcEntry) ==
                 L2VPN_PWVC_OWNER_PWID_FEC_SIG))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            if ((L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_PWID_FEC_SIG)
                || (L2VPN_PWVC_OWNER (pPwVcEntry) ==
                    L2VPN_PWVC_OWNER_GEN_FEC_SIG))
            {
                if (L2VpnIsPwIdOrGenFecExist (pPwVcEntry) == L2VPN_TRUE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
            }
            if (L2VPN_PWVC_OWNER (pPwVcEntry) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)
            {
                /* As per RFC 4446 Section 3.4 the cumulative length of AGI, 
                 * SAII, TAII should not exceed 255. */
                if ((L2VPN_PWVC_AGI_LEN (pPwVcEntry) +
                     L2VPN_PWVC_SAII_LEN (pPwVcEntry) +
                     L2VPN_PWVC_TAII_LEN (pPwVcEntry)) >= L2VPN_PWVC_MAX_AI_LEN)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
              if(L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPWS)
               {
                if ((pPwVcEntry->u4GenAgiType == L2VPN_ZERO) ||
                    (pPwVcEntry->u4LocalAiiType == L2VPN_ZERO) ||
                    (pPwVcEntry->u4RemoteAiiType == L2VPN_ZERO))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
               }
            }
            /*BFD Connectivity Verification Type of Fault Detection and Status
             * Signalling is not allowed when LDP is used for signalling*/
            /*BFD CV Type "Fault Detection Only" is applicable for 1.Static PW
             * 2.Signalling PW*/
            /*BFD CV Type "Fault Detection and Status Signalling" is applicable 
             * only for Static PW*/
            if ((pPwVcEntry->u1LocalCvAdvert &
                 L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS) ||
                (pPwVcEntry->u1LocalCvAdvert &
                 L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS))
            {
                if ((L2VPN_IS_STATIC_PW (pPwVcEntry) != TRUE) &&
                    (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_MANUAL))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                                "nmhTestv2FsMplsL2VpnPwLocalCVAdvert: "
                                "Invalid configuration for Signalling Pseudowire "
                                "PwIndex = %d: INTMD-EXIT\r\n",
                                pPwVcEntry->u4PwVcIndex);
                    return SNMP_FAILURE;
                }
            }
            /*BFD ACH encapsulated CV Type (both Fault Detection Only and 
             * Fault Detection Status Signalling) is not allowed when Control 
             * word is disabled*/
            if ((pPwVcEntry->u1LocalCvAdvert &
                 L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY) ||
                (pPwVcEntry->u1LocalCvAdvert &
                 L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS))
            {
                if (pPwVcEntry->i1ControlWord == L2VPN_DISABLED)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                                "nmhTestv2FsMplsL2VpnPwLocalCVAdvert:"
                                "ACH encapsulated BFD CV Type is supported"
                                "only if control word is enabled"
                                "PwIndex = %d: INTMD-EXIT\r\n",
                                pPwVcEntry->u4PwVcIndex);
                    return SNMP_FAILURE;
                }

            }
            break;
        case L2VPN_PWVC_CREATEANDGO:
        case L2VPN_PWVC_NOTINSERVICE:
        case L2VPN_PWVC_NOTREADY:
            if (pPwVcEntry == NULL)
            {
                /* manager trying to access non-existing row */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case L2VPN_PWVC_DESTROY:

            /* MS-PW: PW VC shouldn't be deleted while attached to another PW
             * VC*/
            if (pPwVcEntry == NULL ||
                (L2VPN_PWVC_ATTACHED_PW_INDEX (pPwVcEntry) != L2VPN_ZERO))
            {
                /* manager trying to access non-existing row */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            /*if BFD session is associated to this PW */
            if (L2VPN_PWVC_OAM_SESSION_INDEX (pPwVcEntry) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PwStorageType
 Input       :  The Indices
                PwIndex

                The Object 
                testValPwStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwStorageType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                        INT4 i4TestValPwStorageType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPwStorageType)
    {
        case L2VPN_PWVC_STORAGE_OTHER:
        case L2VPN_PWVC_STORAGE_VOLATILE:
        case L2VPN_PWVC_STORAGE_NONVOLATILE:
        case L2VPN_PWVC_STORAGE_PERMANENT:
        case L2VPN_PWVC_STORAGE_READONLY:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PwTable
 Input       :  The Indices
                PwIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
