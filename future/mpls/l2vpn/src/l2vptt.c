/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vptt.c,v 1.36 2016/07/28 07:47:34 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vptt.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the low level TEST routines
 *                             for the MIB objects present in the fsl2vpn.mib
 *---------------------------------------------------------------------------*/

# include  "l2vpincs.h"
#include "mplscli.h"

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnTrcFlag
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnTrcFlag (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMplsL2VpnTrcFlag)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMplsL2VpnTrcFlag < (INT4) L2VPN_DBG_NONE_FLAG) ||
        (i4TestValFsMplsL2VpnTrcFlag > (INT4) L2VPN_DBG_ALL_FLAG))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnCleanupInterval
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnCleanupInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnCleanupInterval (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMplsL2VpnCleanupInterval)
{
    UNUSED_PARAM (i4TestValFsMplsL2VpnCleanupInterval);
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnAdminStatus
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnAdminStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsL2VpnAdminStatus)
{
    switch (i4TestValFsMplsL2VpnAdminStatus)
    {
        case L2VPN_ADMIN_UP:
        {
            if (gpL2VpnGlobalInfo->u1AdminStatusProgress ==
                L2VPN_ADMIN_DOWN_IN_PROGRESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                break;
            }

            return SNMP_SUCCESS;
        }

        case L2VPN_ADMIN_DOWN:
        {
            if (gpL2VpnGlobalInfo->u1AdminStatusProgress ==
                L2VPN_ADMIN_UP_IN_PROGRESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                break;
            }

            return SNMP_SUCCESS;
        }

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnMaxPwVcEntries
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnMaxPwVcEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnMaxPwVcEntries (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsMplsL2VpnMaxPwVcEntries)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnMaxPwVcEntries >= L2VPN_MIN_PWVC_ENTRIES) &&
        (u4TestValFsMplsL2VpnMaxPwVcEntries <= L2VPN_MAXIMUM_PWVC_ENTRIES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnMaxPwVcMplsEntries
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnMaxPwVcMplsEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnMaxPwVcMplsEntries (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4TestValFsMplsL2VpnMaxPwVcMplsEntries)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnMaxPwVcMplsEntries >= L2VPN_MIN_PWVC_MPLS_ENTRIES)
        && (u4TestValFsMplsL2VpnMaxPwVcMplsEntries <=
            L2VPN_MAXIMUM_PWVC_MPLS_ENTRIES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnMaxPwVcMplsInOutEntries
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnMaxPwVcMplsInOutEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnMaxPwVcMplsInOutEntries (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4TestValFsMplsL2VpnMaxPwVcMplsInOutEntries)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnMaxPwVcMplsInOutEntries >=
         L2VPN_MIN_PWVC_MPLS_IO_ENTRIES)
        && (u4TestValFsMplsL2VpnMaxPwVcMplsInOutEntries <=
            L2VPN_MAX_PWVC_MPLS_IO_ENTRIES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnMaxEthernetPwVcs
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnMaxEthernetPwVcs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnMaxEthernetPwVcs (UINT4 *pu4ErrorCode,
                                      UINT4
                                      u4TestValFsMplsL2VpnMaxEthernetPwVcs)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnMaxEthernetPwVcs >= L2VPN_MIN_PWVC_ENET_ENTRIES) &&
        (u4TestValFsMplsL2VpnMaxEthernetPwVcs <= L2VPN_MAX_PWVC_ENET_ENTRIES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnMaxPwVcEnetEntries
 Input       :  The Indices

                The Object 
                testValFsMplsL2VpnMaxPwVcEnetEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnMaxPwVcEnetEntries (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4TestValFsMplsL2VpnMaxPwVcEnetEntries)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnMaxPwVcEnetEntries >= L2VPN_MIN_PWVC_ENET_ENTRIES)
        && (u4TestValFsMplsL2VpnMaxPwVcEnetEntries <=
            L2VPN_MAX_PWVC_ENET_ENTRIES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwMode
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwMode (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                            INT4 i4TestValFsMplsL2VpnPwMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsL2VpnPwMode < L2VPN_VPWS)
        || (i4TestValFsMplsL2VpnPwMode > L2VPN_IPLS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if (i4TestValFsMplsL2VpnPwMode == L2VPN_IPLS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnVplsIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnVplsIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnVplsIndex (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                               UINT4 u4TestValFsMplsL2VpnVplsIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnVplsIndex == 0) ||
        (u4TestValFsMplsL2VpnVplsIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pVplsEntry =
        L2VpnGetVplsEntryFromInstanceIndex (u4TestValFsMplsL2VpnVplsIndex);
    if (pVplsEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* This Object can be set only when Pseudo-wire mode is VPLS */
        if (L2VPN_PWVC_MODE (pPwVcEntry) != L2VPN_VPLS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwLocalCapabAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwLocalCapabAdvert
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwLocalCapabAdvert (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsMplsL2VpnPwLocalCapabAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert: "
                    "Admin Status is down "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);

        return SNMP_FAILURE;
    }
    if (pTestValFsMplsL2VpnPwLocalCapabAdvert->i4_Length != sizeof (UINT1))
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert: "
                    "Length validation failed "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert: "
                        "RowStatus is ACTIVE "
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert: "
                    "Row does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VpnVccvValidateCapabAdvert (pTestValFsMplsL2VpnPwLocalCapabAdvert->
                                      pu1_OctetList[0]) == L2VPN_FAILURE)
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert: "
                    "Invalid Value " "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwLocalCCAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwLocalCCAdvert
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwLocalCCAdvert (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsMplsL2VpnPwLocalCCAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert: "
                    "Admin Status is down "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsL2VpnPwLocalCCAdvert->i4_Length != sizeof (UINT1))
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert: "
                    "Length validation failed "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        /* Configuration of Local CC is allowed only during row creation. */
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert: "
                    "PwVcEntry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VpnVccvValidateCC (pTestValFsMplsL2VpnPwLocalCCAdvert->
                             pu1_OctetList[0]) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsL2VpnPwLocalCCAdvert:"
                   "Invalid value " "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsL2VpnPwLocalCCAdvert->pu1_OctetList[0] &
        L2VPN_VCCV_CC_ACH)
    {
        if (pPwVcEntry->i1ControlWord == L2VPN_DISABLED)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwLocalCCAdvert:"
                        "ACH is supported only if control word is enabled"
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }

    }
    if ((pTestValFsMplsL2VpnPwLocalCCAdvert->pu1_OctetList[0] &
         gL2VpnGlobalInfo.u1LocalCcTypeCapabilities) == L2VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert:"
                    "Not supported in Global CC Type "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwLocalCVAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwLocalCVAdvert
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwLocalCVAdvert (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsMplsL2VpnPwLocalCVAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCVAdvert: "
                    "Admin Status is down " "PwIndex = %d: INTMD-EXIT\r\n",
                    u4PwIndex);
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsL2VpnPwLocalCVAdvert->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCVAdvert: "
                    "Length validation failed "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VpnVccvValidateCV (pTestValFsMplsL2VpnPwLocalCVAdvert->
                             pu1_OctetList[0]) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsL2VpnPwLocalCVAdvert: "
                   "Invalid value " "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }
    if ((pTestValFsMplsL2VpnPwLocalCVAdvert->pu1_OctetList[0] &
         gL2VpnGlobalInfo.u1LocalCvTypeCapabilities) == L2VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwLocalCVAdvert:"
                    "Not supported in Global CV Type "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwRemoteCCAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwRemoteCCAdvert
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwRemoteCCAdvert (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMplsL2VpnPwRemoteCCAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwRemoteCCAdvert: "
                    "Admin Status is down " "PwIndex = %d: INTMD-EXIT\r\n",
                    u4PwIndex);
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsL2VpnPwRemoteCCAdvert->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnRemoteCCAdvert: "
                    "Length validation failed "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_MANUAL) &&
            (L2VPN_IS_STATIC_PW (pPwVcEntry) != TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnRemoteCCAdvert: "
                        "Invalid remote CC configuration for"
                        "Pw Signalling"
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }

    }
    else
    {
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnRemoteCCAdvert: "
                    "PwVc Entry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (L2VpnVccvValidateCC (pTestValFsMplsL2VpnPwRemoteCCAdvert->
                             pu1_OctetList[0]) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsL2VpnPwRemoteCCAdvert:"
                   "Invalid value " "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwRemoteCVAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwRemoteCVAdvert
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwRemoteCVAdvert (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMplsL2VpnPwRemoteCVAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwRemoteCVAdvert: "
                    "Admin Status is down " "PwIndex = %d: INTMD-EXIT\r\n",
                    u4PwIndex);
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsL2VpnPwRemoteCVAdvert->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_MANUAL) &&
            (L2VPN_IS_STATIC_PW (pPwVcEntry) != TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnRemoteCVAdvert: "
                        "Invalid remote CV configuration for"
                        "Pw Signalling"
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnRemoteCVAdvert: "
                    "PwVc Entry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    if (L2VpnVccvValidateCV (pTestValFsMplsL2VpnPwRemoteCVAdvert->
                             pu1_OctetList[0]) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsL2VpnPwRemoteCVAdvert: "
                   "Invalid value " "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwOamEnable
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwOamEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwOamEnable (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                 INT4 i4TestValFsMplsL2VpnPwOamEnable)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwOamEnable: "
                    "Admin Status is Down "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsL2VpnPwOamEnable != L2VPN_PW_OAM_ENABLE) &&
        (i4TestValFsMplsL2VpnPwOamEnable != L2VPN_PW_OAM_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwOamEnable: "
                    "Invalid Value " "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwOamEnable: "
                    "PwVc Entry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwGenAGIType
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwGenAGIType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwGenAGIType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                  UINT4 u4TestValFsMplsL2VpnPwGenAGIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenAGIType: "
                    "Admin Status is Down "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);

        return SNMP_FAILURE;
    }
    if (u4TestValFsMplsL2VpnPwGenAGIType != L2VPN_GEN_PWVC_AGI_TYPE_1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenAGIType: "
                    "Invalid Value " "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenAGIType: "
                        "Row Status is Active "
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenAGIType: "
                        "PwOwner is not GenFec Signalling "
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenAGIType: "
                    "PwVc entry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwGenLocalAIIType
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwGenLocalAIIType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwGenLocalAIIType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                       UINT4
                                       u4TestValFsMplsL2VpnPwGenLocalAIIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenLocalAIIType: "
                    "Admin Status is Down "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnPwGenLocalAIIType != L2VPN_GEN_FEC_AII_TYPE_1) &&
        (u4TestValFsMplsL2VpnPwGenLocalAIIType != L2VPN_GEN_FEC_AII_TYPE_2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenLocalAIIType: "
                    "Invalid Value " "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VpnValidateLenForAIIType
            (u4TestValFsMplsL2VpnPwGenLocalAIIType,
             L2VPN_PWVC_SAII_LEN (pPwVcEntry)) != L2VPN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenRemoteAIIType: "
                        "Invalid AII Type for length"
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;

        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenLocalAIIType: "
                        "PwRowStatus is Active "
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
        if (L2VPN_PWVC_OWNER (pPwVcEntry) != L2VPN_PWVC_OWNER_GEN_FEC_SIG)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenAGIType: "
                        "PwOwner is not GenFec Signalling "
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenLocalAIIType: "
                    "PwVc entry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwGenRemoteAIIType
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsMplsL2VpnPwGenRemoteAIIType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwGenRemoteAIIType (UINT4 *pu4ErrorCode, UINT4 u4PwIndex,
                                        UINT4
                                        u4TestValFsMplsL2VpnPwGenRemoteAIIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenRemoteAIIType: "
                    "Admin Status is Down "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);

        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsL2VpnPwGenRemoteAIIType != L2VPN_GEN_FEC_AII_TYPE_1) &&
        (u4TestValFsMplsL2VpnPwGenRemoteAIIType != L2VPN_GEN_FEC_AII_TYPE_2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenRemoteAIIType: "
                    "Invalid Value " "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        if (L2VpnValidateLenForAIIType
            (u4TestValFsMplsL2VpnPwGenRemoteAIIType,
             L2VPN_PWVC_TAII_LEN (pPwVcEntry)) != L2VPN_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenRemoteAIIType: "
                        "Invalid AII Type for length"
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;

        }
        if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                        "nmhTestv2FsMplsL2VpnPwGenRemoteAIIType: "
                        "PwRowStatus is Active "
                        "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        L2VPN_DBG1 (L2VPN_DBG_VCCV_MGMT_TRC,
                    "nmhTestv2FsMplsL2VpnPwGenRemoteAIIType: "
                    "PwVc entry does not exist "
                    "PwIndex = %d: INTMD-EXIT\r\n", u4PwIndex);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsLocalCCTypesCapabilities
 Input       :  The Indices

                The Object 
                testValFsMplsLocalCCTypesCapabilities
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLocalCCTypesCapabilities (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsMplsLocalCCTypesCapabilities)
{
    if (pTestValFsMplsLocalCCTypesCapabilities->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsLocalCCTypesCapabilities: "
                   "Length validation is failed "
                   "PwIndex = %d: INTMD-EXIT\r\n");

        return SNMP_FAILURE;
    }
    if (L2VpnVccvValidateCC
        (pTestValFsMplsLocalCCTypesCapabilities->pu1_OctetList[0]) ==
        L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsLocalCCTypesCapabilities: "
                   "Invalid value " "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }
    if ((pTestValFsMplsLocalCCTypesCapabilities->pu1_OctetList[0] == 0) ||
        (pTestValFsMplsLocalCCTypesCapabilities->pu1_OctetList[0] &
         gL2VpnGlobalInfo.u1LocalHwCcTypeCapabilities))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
               "nmhTestv2FsMplsLocalCCTypesCapabilities: "
               "Hardware support is not available" "INTMD-EXIT\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLocalCVTypesCapabilities
 Input       :  The Indices

                The Object 
                testValFsMplsLocalCVTypesCapabilities
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLocalCVTypesCapabilities (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsMplsLocalCVTypesCapabilities)
{
    if (pTestValFsMplsLocalCVTypesCapabilities->i4_Length !=
        (INT4) (sizeof (UINT1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsLocalCVTypesCapabilities: "
                   "Length validation is failed "
                   "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }

    if (L2VpnVccvValidateCV
        (pTestValFsMplsLocalCVTypesCapabilities->pu1_OctetList[0]) ==
        L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        L2VPN_DBG (L2VPN_DBG_VCCV_MGMT_TRC,
                   "nmhTestv2FsMplsLocalCVTypesCapabilities: "
                   "Invalid value " "PwIndex = %d: INTMD-EXIT\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for fsMplsVplsConfigTable  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsVsi
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsVsi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsVsi (UINT4 *pu4ErrorCode, UINT4 u4FsMplsVplsInstanceIndex,
                        INT4 i4TestValFsMplsVplsVsi)
{
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMplsVplsVsi < 0)
        || i4TestValFsMplsVplsVsi > L2VPN_MAX_VSI_ENTRIES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsVpnId
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsVpnId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsVpnId (UINT4 *pu4ErrorCode, UINT4 u4FsMplsVplsInstanceIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsMplsVplsVpnId)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = L2VPN_ZERO;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == L2VPN_ZERO) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((pTestValFsMplsVplsVpnId->i4_Length < L2VPN_ZERO) ||
        (pTestValFsMplsVplsVpnId->i4_Length > L2VPN_MAX_VPLS_VPNID_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsVplsVpnId->i4_Length != L2VPN_ZERO)
    {
        MEMCPY (&u4VpnId,
                &(pTestValFsMplsVplsVpnId->pu1_OctetList
                  [STRLEN (MPLS_OUI_VPN_ID)]), sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
        if (MplsL2VpnCheckVpnId (u4VpnId) != u4VpnId)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsName
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsName (UINT4 *pu4ErrorCode, UINT4 u4FsMplsVplsInstanceIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMplsVplsName)
{
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pTestValFsMplsVplsName->i4_Length > L2VPN_MAX_VPLS_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsDescr
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsDescr (UINT4 *pu4ErrorCode, UINT4 u4FsMplsVplsInstanceIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsMplsVplsDescr)
{
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pTestValFsMplsVplsDescr->i4_Length > L2VPN_MAX_VPLS_DESCR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsFdbHighWatermark
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsFdbHighWatermark
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsFdbHighWatermark (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMplsVplsInstanceIndex,
                                     UINT4 u4TestValFsMplsVplsFdbHighWatermark)
{
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsMplsVplsFdbHighWatermark > L2VPN_VPLS_MAX_FDB_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsFdbLowWatermark
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsFdbLowWatermark
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsFdbLowWatermark (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMplsVplsInstanceIndex,
                                    UINT4 u4TestValFsMplsVplsFdbLowWatermark)
{
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsMplsVplsFdbLowWatermark > L2VPN_VPLS_MAX_FDB_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsRowStatus
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsMplsVplsRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsRowStatus (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMplsVplsInstanceIndex,
                              INT4 i4TestValFsMplsVplsRowStatus)
{
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tPwVcEntry         *pPwEntry = NULL;
#ifdef VPLSADS_WANTED
    UINT4               u4VeId = L2VPN_ZERO;
#endif

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    switch (i4TestValFsMplsVplsRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pVplsEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case ACTIVE:
            if (pVplsEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if ((L2VPN_VPLS_ROW_STATUS (pVplsEntry) == NOT_IN_SERVICE)
                || (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == NOT_READY))
            {
                /* VPLS NAME, VPN Id should be checked for 
                 * validity before activating the entry */
                if ((L2VPN_VPLS_NAME (pVplsEntry)[0] == 0) ||
                    (L2VPN_VPLS_VPN_ID (pVplsEntry)[0] == 0))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

#ifdef VPLSADS_WANTED
                if (L2VPN_VPLS_SIG_BGP == L2VPN_VPLS_SIG_TYPE (pVplsEntry))
                {
                    if (L2VPN_FAILURE ==
                        L2VpnGetLocalVeIdFromVPLSIndex
                        (u4FsMplsVplsInstanceIndex, &u4VeId))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
#endif
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        case NOT_IN_SERVICE:
            if (pVplsEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
            {
                pPwEntry =
                    L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
                if (pPwEntry != NULL)
                {
                    if (L2VPN_PWVC_OWNER (pPwEntry) != L2VPN_PWVC_OWNER_OTHER)
                    {
                        if (pPwEntry->i1RowStatus == ACTIVE)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
            }

            if ((L2VPN_VPLS_ROW_STATUS (pVplsEntry) == NOT_IN_SERVICE)
                || (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        case DESTROY:
            if (pVplsEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
            {
                pPwEntry =
                    L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
                if (pPwEntry != NULL)
                {
                    if (L2VPN_PWVC_OWNER (pPwEntry) != L2VPN_PWVC_OWNER_OTHER)
                    {
                        if (pPwEntry->i1RowStatus == ACTIVE)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
            }

            return SNMP_SUCCESS;
        case CREATE_AND_GO:
        case NOT_READY:
            if (pVplsEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return SNMP_FAILURE;
}

/* VPLS FDB */
/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsL2MapFdbId
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object
                testValFsMplsVplsL2MapFdbId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsL2MapFdbId (UINT4 *pu4ErrorCode,
                               UINT4 u4FsMplsVplsInstanceIndex,
                               INT4 i4TestValFsMplsVplsL2MapFdbId)
{
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* If the value is non-zero, it means, mapping the vpls instanace to the
     * FDB identifier would happen.
     */
    if (i4TestValFsMplsVplsL2MapFdbId != L2VPN_VPLS_FDB_DEF_VAL)
    {
        if ((i4TestValFsMplsVplsL2MapFdbId < L2VPN_VPLS_FDB_MIN_VAL) ||
            (i4TestValFsMplsVplsL2MapFdbId > L2VPN_VPLS_FDB_MAX_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (L2VpnVlanValidateVlanId (L2IWF_DEFAULT_CONTEXT,
                                     (UINT2) i4TestValFsMplsVplsL2MapFdbId) ==
            L2VPN_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsmplsVplsMtu
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsmplsVplsMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmplsVplsMtu (UINT4 *pu4ErrorCode,
                        UINT4 u4FsMplsVplsInstanceIndex,
                        UINT4 u4TestValFsmplsVplsMtu)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsmplsVplsMtu > L2VPN_PWVC_MAX_MTU_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (u4TestValFsmplsVplsMtu);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsmplsVplsStorageType
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsmplsVplsStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmplsVplsStorageType (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMplsVplsInstanceIndex,
                                INT4 i4TestValFsmplsVplsStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (NULL == pVplsEntry)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsmplsVplsStorageType)
    {
        case L2VPN_STORAGE_OTHER:
        case L2VPN_STORAGE_VOLATILE:
        case L2VPN_STORAGE_NONVOLATILE:
        case L2VPN_STORAGE_PERMANENT:
        case L2VPN_STORAGE_READONLY:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (i4TestValFsmplsVplsStorageType);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsmplsVplsSignalingType
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsmplsVplsSignalingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmplsVplsSignalingType (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMplsVplsInstanceIndex,
                                  INT4 i4TestValFsmplsVplsSignalingType)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsmplsVplsSignalingType <= L2VPN_VPLS_SIG_MIN) ||
        (i4TestValFsmplsVplsSignalingType >= L2VPN_VPLS_SIG_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (i4TestValFsmplsVplsSignalingType);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsmplsVplsControlWord
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                testValFsmplsVplsControlWord
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmplsVplsControlWord (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMplsVplsInstanceIndex,
                                INT4 i4TestValFsmplsVplsControlWord)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* TODO use macro for CW */
    if ((L2VPN_ENABLED != i4TestValFsmplsVplsControlWord) &&
        (L2VPN_DISABLED != i4TestValFsmplsVplsControlWord))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
    if (pVplsEntry != NULL)
    {
        if (L2VPN_VPLS_ROW_STATUS (pVplsEntry) == L2VPN_PWVC_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM (i4TestValFsmplsVplsControlWord);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsPwMplsInboundTunnelIndex
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsPwMplsInboundTunnelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPwMplsInboundTunnelIndex (UINT4 *pu4ErrorCode,
                                     UINT4 u4PwIndex,
                                     UINT4 u4TestValFsPwMplsInboundTunnelIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1MplsType;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsPwMplsInboundTunnelIndex > TE_TNL_INDEX_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                            L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if (u1MplsType != L2VPN_MPLS_TYPE_TE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPwMplsInboundTunnelEgressLSR
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsPwMplsInboundTunnelEgressLSR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPwMplsInboundTunnelEgressLSR (UINT4 *pu4ErrorCode,
                                         UINT4 u4PwIndex,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pTestValFsPwMplsInboundTunnelEgressLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1MplsType;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsPwMplsInboundTunnelEgressLSR->i4_Length != ROUTER_ID_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                            L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if (u1MplsType != L2VPN_MPLS_TYPE_TE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPwMplsInboundTunnelIngressLSR
 Input       :  The Indices
                PwIndex

                The Object 
                testValFsPwMplsInboundTunnelIngressLSR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPwMplsInboundTunnelIngressLSR (UINT4 *pu4ErrorCode,
                                          UINT4 u4PwIndex,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValFsPwMplsInboundTunnelIngressLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    UINT1               u1MplsType;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsPwMplsInboundTunnelIngressLSR->i4_Length != ROUTER_ID_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_ADMIN_STATUS (pPwVcEntry) == L2VPN_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_TYPE (pPwVcEntry) != L2VPN_PSN_TYPE_MPLS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u1MplsType = L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                            L2VPN_PWVC_PSN_ENTRY (pPwVcEntry));

    if (u1MplsType != L2VPN_MPLS_TYPE_TE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL2VpnPwAIIFormat
 Input       :  The Indices
                PwIndex

                The Object
                testValFsMplsL2VpnPwAIIFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL2VpnPwAIIFormat (UINT4 *pu4ErrorCode,
                                 UINT4 u4PwIndex, tSNMP_OCTET_STRING_TYPE
                                 * pTestValFsMplsL2VpnPwAIIFormat)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    switch (pTestValFsMplsL2VpnPwAIIFormat->pu1_OctetList[0])
    {
        case L2VPN_PWVC_SAII_IP:
        case L2VPN_PWVC_TAII_IP:
        case (L2VPN_PWVC_SAII_IP + L2VPN_PWVC_TAII_IP):
        case L2VPN_ZERO:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_ROW_STATUS (pPwVcEntry) == L2VPN_PWVC_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

/* Low Level TEST Routines for All Objects  */

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsPortBundleStatus
 Input       :  The Indices
                IfIndex

                The Object
                testValFsMplsPortBundleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsPortBundleStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4TestValFsMplsPortBundleStatus)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsPortBundleStatus != MPLS_ENABLE) &&
        (i4TestValFsMplsPortBundleStatus != MPLS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (L2VpnValidatePortStatus (i4IfIndex) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsPortMultiplexStatus
 Input       :  The Indices
                IfIndex

                The Object
                testValFsMplsPortMultiplexStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsPortMultiplexStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValFsMplsPortMultiplexStatus)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsPortMultiplexStatus != MPLS_ENABLE) &&
        (i4TestValFsMplsPortMultiplexStatus != MPLS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VpnValidatePortStatus (i4IfIndex) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsPortAllToOneBundleStatus
 Input       :  The Indices
                IfIndex

                The Object
                testValFsMplsPortAllToOneBundleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsPortAllToOneBundleStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         INT4
                                         i4TestValFsMplsPortAllToOneBundleStatus)
{
    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsPortAllToOneBundleStatus != MPLS_ENABLE) &&
        (i4TestValFsMplsPortAllToOneBundleStatus != MPLS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (L2VpnValidatePortStatus (i4IfIndex) == L2VPN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsPortRowStatus
 Input       :  The Indices
                IfIndex

                The Object
                testValFsMplsPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsPortRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFsMplsPortRowStatus)
{

    UINT1               u1IfType = 0;
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CfaValidateIfIndex ((UINT4) i4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfaceType ((UINT4) i4IfIndex, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    switch (i4TestValFsMplsPortRowStatus)
    {
        case MPLS_STATUS_CREATE_AND_WAIT:
        case MPLS_STATUS_CREATE_AND_GO:

            if (pMplsPortEntryInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case MPLS_STATUS_ACTIVE:

            if (pMplsPortEntryInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if ((pMplsPortEntryInfo->u1AllToOneBundleStatus == MPLS_ENABLE) &&
                ((pMplsPortEntryInfo->u1MultiplexStatus == MPLS_ENABLE) ||
                 (pMplsPortEntryInfo->u1BundleStatus == MPLS_ENABLE)))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (L2VpnPortServiceStatus (i4IfIndex) == L2VPN_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case MPLS_STATUS_NOT_INSERVICE:
        case MPLS_STATUS_DESTROY:

            if (pMplsPortEntryInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsAcMapPortIfIndex
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                testValFsMplsVplsAcMapPortIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsAcMapPortIfIndex (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMplsVplsAcMapVplsIndex,
                                     UINT4 u4FsMplsVplsAcMapAcIndex,
                                     UINT4 u4TestValFsMplsVplsAcMapPortIfIndex)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry     vplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapEntry;
    MEMSET (&vplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSAC_VPLS_INSTANCE (&vplsAcMapEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX (&vplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGet (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsAcMapEntry);
    if (pVplsAcMapEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        return SNMP_FAILURE;
    }
    else if (L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4TestValFsMplsVplsAcMapPortIfIndex);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM (u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM (u4TestValFsMplsVplsAcMapPortIfIndex);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsAcMapPortVlan
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                testValFsMplsVplsAcMapPortVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsAcMapPortVlan (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMplsVplsAcMapVplsIndex,
                                  UINT4 u4FsMplsVplsAcMapAcIndex,
                                  UINT4 u4TestValFsMplsVplsAcMapPortVlan)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry     vplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapEntry;
    MEMSET (&vplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSAC_VPLS_INSTANCE (&vplsAcMapEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX (&vplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGet (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsAcMapEntry);
    if (pVplsAcMapEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        return SNMP_FAILURE;
    }
    else if (L2VPN_VPLSAC_ROW_STATUS (pVplsAcMapEntry) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);
        return SNMP_FAILURE;
    }

    if (u4TestValFsMplsVplsAcMapPortVlan > L2VPN_VLANCFG)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* In case of port based AC. vlan will be Set to default(VLANCFG), 
     * so further check is not required.*/
    if (u4TestValFsMplsVplsAcMapPortVlan != L2VPN_VLANCFG)
    {
        if (OSIX_FALSE == L2IwfIsVlanActive ((tVlanId)
                                             u4TestValFsMplsVplsAcMapPortVlan))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_WRONG_VLAN);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM (u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM (u4TestValFsMplsVplsAcMapPortVlan);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsVplsAcMapRowStatus
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                testValFsMplsVplsAcMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsVplsAcMapRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMplsVplsAcMapVplsIndex,
                                   UINT4 u4FsMplsVplsAcMapAcIndex,
                                   INT4 i4TestValFsMplsVplsAcMapRowStatus)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry     vplsAcMapEntry;
    tVplsAcMapEntry    *pVplsAcMapEntry;
    UINT4               u4AcIndex = L2VPN_ZERO;
    INT4                i4RetStatus = L2VPN_FAILURE;

    MEMSET (&vplsAcMapEntry, 0, sizeof (tVplsAcMapEntry));

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhValidateIndexInstanceFsMplsVplsAcMapTable
        (u4FsMplsVplsAcMapVplsIndex, u4FsMplsVplsAcMapAcIndex))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    L2VPN_VPLSAC_VPLS_INSTANCE (&vplsAcMapEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX (&vplsAcMapEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcMapEntry = (tVplsAcMapEntry *)
        RBTreeGet (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo),
                   (tRBElem *) & vplsAcMapEntry);

    if (pVplsAcMapEntry == NULL && CREATE_AND_WAIT !=
        i4TestValFsMplsVplsAcMapRowStatus)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_NOT_FOUND);
        return SNMP_FAILURE;
    }
    switch (i4TestValFsMplsVplsAcMapRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pVplsAcMapEntry != NULL)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Row Already Exists\t\n", __func__);
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            if (L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry) ==
                L2VPN_VPLSAC_INVALID_PORT_INDEX ||
                L2VPN_VPLSAC_VLAN_ID (pVplsAcMapEntry) ==
                L2VPN_VPLSAC_INVALID_VLAN_ID)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Value is not assigned\t\n", __func__);
                return SNMP_FAILURE;
            }
            L2VpnGetAcIndexFromIfandVlan (L2VPN_VPLSAC_PORT_INDEX
                                          (pVplsAcMapEntry),
                                          L2VPN_VPLSAC_VLAN_ID
                                          (pVplsAcMapEntry), &u4AcIndex);
            if (u4AcIndex != L2VPN_ZERO)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Row Already Exists\t\n", __func__);
                return SNMP_FAILURE;
            }

            i4RetStatus =
                L2VpnACExistsCheck (L2VPN_VPLSAC_VLAN_ID (pVplsAcMapEntry),
                                    (INT4)
                                    L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapEntry));
            if (L2VPN_SUCCESS == i4RetStatus)
            {
                CLI_SET_ERR (L2VPN_PW_VPLS_CLI_AC_DUPLICATE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                L2VPN_DBG1 (L2VPN_DBG_LVL_ERR_FLAG,
                            "%s : Ac is mapped to manual pw\t\n", __func__);
                return SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case DESTROY:
            break;
        case CREATE_AND_GO:
        case NOT_READY:
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM (u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM (i4TestValFsMplsVplsAcMapRowStatus);
    return SNMP_FAILURE;
#endif
}
