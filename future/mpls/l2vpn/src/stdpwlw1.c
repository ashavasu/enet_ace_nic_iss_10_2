/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: stdpwlw1.c,v 1.16 2014/07/01 11:51:53 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "l2vpincs.h"
# include  "mplsnp.h"
#include  "mplsnpwr.h"

/* LOW LEVEL Routines for Table : PwPerfCurrentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwPerfCurrentTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwPerfCurrentTable (UINT4 u4PwIndex)
{
    if ((u4PwIndex < L2VPN_MIN_PWVC_ENET_ENTRIES) ||
        (u4PwIndex > L2VPN_MAX_PWVC_ENET_ENTRIES))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwPerfCurrentTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwPerfCurrentTable (UINT4 *pu4PwIndex)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwPerfCurrentTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwPerfCurrentTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentInHCPackets
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentInHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentInHCPackets (UINT4 u4PwIndex,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValPwPerfCurrentInHCPackets)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;
            tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
            tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_INPKTS,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->InHCPkts,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->InHCPkts.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->InHCPkts.u4Lo = u8Tmp.u4Lo;

            pu8RetValPwPerfCurrentInHCPackets->msn = u8Tmp.u4Hi;
            pu8RetValPwPerfCurrentInHCPackets->lsn = u8Tmp.u4Lo;
        }
#else
        pu8RetValPwPerfCurrentInHCPackets->msn = 0;
        pu8RetValPwPerfCurrentInHCPackets->lsn = 0;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentInHCBytes
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentInHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentInHCBytes (UINT4 u4PwIndex,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValPwPerfCurrentInHCBytes)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;
            tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
            tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_INBYTES,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->InHCBytes,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->InHCBytes.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->InHCBytes.u4Lo = u8Tmp.u4Lo;

            pu8RetValPwPerfCurrentInHCBytes->msn = u8Tmp.u4Hi;
            pu8RetValPwPerfCurrentInHCBytes->lsn = u8Tmp.u4Lo;
        }
#else
        pu8RetValPwPerfCurrentInHCBytes->msn = 0;
        pu8RetValPwPerfCurrentInHCBytes->lsn = 0;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentOutHCPackets
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentOutHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentOutHCPackets (UINT4 u4PwIndex,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValPwPerfCurrentOutHCPackets)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;
            tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
            tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_OUTPKTS,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->OutHCPkts,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->OutHCPkts.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->OutHCPkts.u4Lo = u8Tmp.u4Lo;

            pu8RetValPwPerfCurrentOutHCPackets->msn = u8Tmp.u4Hi;
            pu8RetValPwPerfCurrentOutHCPackets->lsn = u8Tmp.u4Lo;
        }
#else
        pu8RetValPwPerfCurrentOutHCPackets->msn = 0;
        pu8RetValPwPerfCurrentOutHCPackets->lsn = 0;
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentOutHCBytes
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentOutHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentOutHCBytes (UINT4 u4PwIndex,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValPwPerfCurrentOutHCBytes)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;
            tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
            tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_OUTBYTES,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->OutHCBytes,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->OutHCBytes.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->OutHCBytes.u4Lo = u8Tmp.u4Lo;

            pu8RetValPwPerfCurrentOutHCBytes->msn = u8Tmp.u4Hi;
            pu8RetValPwPerfCurrentOutHCBytes->lsn = u8Tmp.u4Lo;
        }
#else
        pu8RetValPwPerfCurrentOutHCBytes->msn = 0;
        pu8RetValPwPerfCurrentOutHCBytes->lsn = 0;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentInPackets
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentInPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentInPackets (UINT4 u4PwIndex,
                              UINT4 *pu4RetValPwPerfCurrentInPackets)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_INPKTS,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->InHCPkts,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->InHCPkts.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->InHCPkts.u4Lo = u8Tmp.u4Lo;

            *pu4RetValPwPerfCurrentInPackets = u8Tmp.u4Lo;
        }
#else
        pPwVcPerfInfo =
            L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
        pPwVcPerfIntervalEntry = &(pPwVcPerfInfo->
                                   aPwVcPerfIntervalInfo
                                   [L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);

        *pu4RetValPwPerfCurrentInPackets =
            pPwVcPerfIntervalEntry->InHCPkts.u4Lo;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentInBytes
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentInBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentInBytes (UINT4 u4PwIndex,
                            UINT4 *pu4RetValPwPerfCurrentInBytes)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;
            tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
            tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_INBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_INBYTES,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->InHCBytes,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->InHCBytes.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->InHCBytes.u4Lo = u8Tmp.u4Lo;

            *pu4RetValPwPerfCurrentInBytes = u8Tmp.u4Lo;
        }
#else
        *pu4RetValPwPerfCurrentInBytes = 0;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentOutPackets
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentOutPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentOutPackets (UINT4 u4PwIndex,
                               UINT4 *pu4RetValPwPerfCurrentOutPackets)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_OUTPKTS,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->OutHCPkts,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->OutHCPkts.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->OutHCPkts.u4Lo = u8Tmp.u4Lo;

            *pu4RetValPwPerfCurrentOutPackets = u8Tmp.u4Lo;
        }
#else
        pPwVcPerfInfo =
            L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
        pPwVcPerfIntervalEntry = &(pPwVcPerfInfo->
                                   aPwVcPerfIntervalInfo
                                   [L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);

        *pu4RetValPwPerfCurrentOutPackets =
            pPwVcPerfIntervalEntry->OutHCPkts.u4Lo;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfCurrentOutBytes
 Input       :  The Indices
                PwIndex

                The Object 
                retValPwPerfCurrentOutBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfCurrentOutBytes (UINT4 u4PwIndex,
                             UINT4 *pu4RetValPwPerfCurrentOutBytes)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            FS_UINT8            u8Tmp;
            tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
            tPwVcPerfIntervalEntry *pPwVcPerfIntervalEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;
            u8Tmp.u4Hi = 0;
            u8Tmp.u4Lo = 0;

            MplsStatsInput.InputType = MPLS_GET_VC_LBL_STATS;
            MplsStatsInput.i1NpReset = TRUE;
            MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwVcEntry);
            MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                      MPLS_HW_STAT_VC_CURRENT_OUTBYTES,
                                      &StatsInfo);

            /* Accumulate the statistics since they are reset to 0 in the HW */
            pPwVcPerfInfo =
                L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
            pPwVcPerfIntervalEntry =
                &(pPwVcPerfInfo->
                  aPwVcPerfIntervalInfo[L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1]);
            FSAP_U8_ADD (&u8Tmp, &pPwVcPerfIntervalEntry->OutHCBytes,
                         &StatsInfo.u8Value);
            pPwVcPerfIntervalEntry->OutHCBytes.u4Hi = u8Tmp.u4Hi;
            pPwVcPerfIntervalEntry->OutHCBytes.u4Lo = u8Tmp.u4Lo;

            *pu4RetValPwPerfCurrentOutBytes = u8Tmp.u4Lo;
        }
#else
        *pu4RetValPwPerfCurrentOutBytes = 0;
#endif
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : PwPerfIntervalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwPerfIntervalTable
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwPerfIntervalTable (UINT4 u4PwIndex,
                                             INT4 i4PwPerfIntervalNumber)
{
    if ((u4PwIndex < L2VPN_MIN_PWVC_ENET_ENTRIES) ||
        (u4PwIndex > L2VPN_MAX_PWVC_ENET_ENTRIES))
    {
        return SNMP_FAILURE;
    }

    if ((i4PwPerfIntervalNumber < L2VPN_PWVC_MIN_PERF_INTV) ||
        (i4PwPerfIntervalNumber > L2VPN_PWVC_MAX_PERF_INTV))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwPerfIntervalTable
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexPwPerfIntervalTable (UINT4 *pu4PwIndex,
                                     INT4 *pi4PwPerfIntervalNumber)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4PwPerfIntervalNumber = L2VPN_PWVC_MIN_PERF_INTV;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwPerfIntervalTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
                PwPerfIntervalNumber
                nextPwPerfIntervalNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwPerfIntervalTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex,
                                    INT4 i4PwPerfIntervalNumber,
                                    INT4 *pi4NextPwPerfIntervalNumber)
{
    if (i4PwPerfIntervalNumber >= L2VPN_PWVC_MAX_PERF_INTV)
    {
        if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pi4NextPwPerfIntervalNumber = L2VPN_PWVC_MIN_PERF_INTV;
    }
    else
    {
        *pu4NextPwIndex = u4PwIndex;
        *pi4NextPwPerfIntervalNumber = i4PwPerfIntervalNumber + 1;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalValidData
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalValidData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalValidData (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                               INT4 *pi4RetValPwPerfIntervalValidData)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            *pi4RetValPwPerfIntervalValidData =
                L2VPN_PWVC_PERF_INT_VALID_DATA (pPwVcPerfInfo,
                                                i4PwPerfIntervalNumber);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalTimeElapsed
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalTimeElapsed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalTimeElapsed (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                                 INT4 *pi4RetValPwPerfIntervalTimeElapsed)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            *pi4RetValPwPerfIntervalTimeElapsed =
                L2VPN_PWVC_PERF_INT_TIME_ELAPSED (pPwVcPerfInfo,
                                                  i4PwPerfIntervalNumber);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalInHCPackets
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalInHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalInHCPackets (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValPwPerfIntervalInHCPackets)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            MEMCPY (pu8RetValPwPerfIntervalInHCPackets,
                    L2VPN_PWVC_PERF_INT_IN_HC_PKTS (pPwVcPerfInfo,
                                                    i4PwPerfIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalInHCBytes
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalInHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalInHCBytes (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValPwPerfIntervalInHCBytes)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            MEMCPY (pu8RetValPwPerfIntervalInHCBytes,
                    L2VPN_PWVC_PERF_INT_IN_HC_BYTES (pPwVcPerfInfo,
                                                     i4PwPerfIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalOutHCPackets
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalOutHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalOutHCPackets (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValPwPerfIntervalOutHCPackets)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            MEMCPY (pu8RetValPwPerfIntervalOutHCPackets,
                    L2VPN_PWVC_PERF_INT_OUT_HC_PKTS (pPwVcPerfInfo,
                                                     i4PwPerfIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalOutHCBytes
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalOutHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalOutHCBytes (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValPwPerfIntervalOutHCBytes)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            MEMCPY (pu8RetValPwPerfIntervalOutHCBytes,
                    L2VPN_PWVC_PERF_INT_OUT_HC_BYTES (pPwVcPerfInfo,
                                                      i4PwPerfIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalInPackets
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalInPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalInPackets (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                               UINT4 *pu4RetValPwPerfIntervalInPackets)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            *pu4RetValPwPerfIntervalInPackets =
                (L2VPN_PWVC_PERF_INT_IN_HC_PKTS (pPwVcPerfInfo,
                                                 i4PwPerfIntervalNumber))->u4Lo;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalInBytes
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalInBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalInBytes (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                             UINT4 *pu4RetValPwPerfIntervalInBytes)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            *pu4RetValPwPerfIntervalInBytes =
                (L2VPN_PWVC_PERF_INT_IN_HC_BYTES (pPwVcPerfInfo,
                                                  i4PwPerfIntervalNumber))->
                u4Lo;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalOutPackets
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalOutPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalOutPackets (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                                UINT4 *pu4RetValPwPerfIntervalOutPackets)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            *pu4RetValPwPerfIntervalOutPackets =
                (L2VPN_PWVC_PERF_INT_OUT_HC_PKTS (pPwVcPerfInfo,
                                                  i4PwPerfIntervalNumber))->
                u4Lo;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerfIntervalOutBytes
 Input       :  The Indices
                PwIndex
                PwPerfIntervalNumber

                The Object 
                retValPwPerfIntervalOutBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfIntervalOutBytes (UINT4 u4PwIndex, INT4 i4PwPerfIntervalNumber,
                              UINT4 *pu4RetValPwPerfIntervalOutBytes)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_INTVL2ARRAY_IDX (L2VPN_PWVC_PERF_CURRENT_INTERVAL,
                                             i4PwPerfIntervalNumber);
            *pu4RetValPwPerfIntervalOutBytes =
                (L2VPN_PWVC_PERF_INT_OUT_HC_BYTES (pPwVcPerfInfo,
                                                   i4PwPerfIntervalNumber))->
                u4Lo;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : PwPerf1DayIntervalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwPerf1DayIntervalTable
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstancePwPerf1DayIntervalTable (UINT4 u4PwIndex,
                                                 UINT4
                                                 u4PwPerf1DayIntervalNumber)
{
    if ((u4PwIndex < L2VPN_MIN_PWVC_ENET_ENTRIES) ||
        (u4PwIndex > L2VPN_MAX_PWVC_ENET_ENTRIES))
    {
        return SNMP_FAILURE;
    }

    if ((u4PwPerf1DayIntervalNumber < L2VPN_PWVC_MIN_PERF_INTV) ||
        (u4PwPerf1DayIntervalNumber > L2VPN_PWVC_MAX_PERF_DAYS))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwPerf1DayIntervalTable
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexPwPerf1DayIntervalTable (UINT4 *pu4PwIndex,
                                         UINT4 *pu4PwPerf1DayIntervalNumber)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4PwPerf1DayIntervalNumber = L2VPN_PWVC_MIN_PERF_INTV;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwPerf1DayIntervalTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
                PwPerf1DayIntervalNumber
                nextPwPerf1DayIntervalNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwPerf1DayIntervalTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex,
                                        UINT4 u4PwPerf1DayIntervalNumber,
                                        UINT4 *pu4NextPwPerf1DayIntervalNumber)
{
    if (u4PwPerf1DayIntervalNumber >= L2VPN_PWVC_MAX_PERF_DAYS)
    {
        if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pu4NextPwPerf1DayIntervalNumber = L2VPN_PWVC_MIN_PERF_INTV;
    }
    else
    {
        *pu4NextPwIndex = u4PwIndex;
        *pu4NextPwPerf1DayIntervalNumber = u4PwPerf1DayIntervalNumber + 1;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwPerf1DayIntervalValidData
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber

                The Object 
                retValPwPerf1DayIntervalValidData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerf1DayIntervalValidData (UINT4 u4PwIndex,
                                   UINT4 u4PwPerf1DayIntervalNumber,
                                   INT4 *pi4RetValPwPerf1DayIntervalValidData)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX (pPwVcPerfInfo->
                                                  u1PwVcPerfDayIndex,
                                                  u4PwPerf1DayIntervalNumber);
            *pi4RetValPwPerf1DayIntervalValidData =
                L2VPN_PWVC_PERF_1DAY_VALID_DATA (pPwVcPerfInfo,
                                                 u4PwPerf1DayIntervalNumber);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerf1DayIntervalMoniSecs
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber

                The Object 
                retValPwPerf1DayIntervalMoniSecs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerf1DayIntervalMoniSecs (UINT4 u4PwIndex,
                                  UINT4 u4PwPerf1DayIntervalNumber,
                                  INT4 *pi4RetValPwPerf1DayIntervalMoniSecs)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX (pPwVcPerfInfo->
                                                  u1PwVcPerfDayIndex,
                                                  u4PwPerf1DayIntervalNumber);
            *pi4RetValPwPerf1DayIntervalMoniSecs =
                L2VPN_PWVC_PERF_1DAY_MONI_TIME (pPwVcPerfInfo,
                                                u4PwPerf1DayIntervalNumber);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerf1DayIntervalInHCPackets
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber

                The Object 
                retValPwPerf1DayIntervalInHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerf1DayIntervalInHCPackets (UINT4 u4PwIndex,
                                     UINT4 u4PwPerf1DayIntervalNumber,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValPwPerf1DayIntervalInHCPackets)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX (pPwVcPerfInfo->
                                                  u1PwVcPerfDayIndex,
                                                  u4PwPerf1DayIntervalNumber);
            MEMCPY (pu8RetValPwPerf1DayIntervalInHCPackets,
                    L2VPN_PWVC_PERF_1DAY_IN_HC_PKTS (pPwVcPerfInfo,
                                                     u4PwPerf1DayIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerf1DayIntervalInHCBytes
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber

                The Object 
                retValPwPerf1DayIntervalInHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerf1DayIntervalInHCBytes (UINT4 u4PwIndex,
                                   UINT4 u4PwPerf1DayIntervalNumber,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValPwPerf1DayIntervalInHCBytes)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX (pPwVcPerfInfo->
                                                  u1PwVcPerfDayIndex,
                                                  u4PwPerf1DayIntervalNumber);
            MEMCPY (pu8RetValPwPerf1DayIntervalInHCBytes,
                    L2VPN_PWVC_PERF_1DAY_IN_HC_BYTS (pPwVcPerfInfo,
                                                     u4PwPerf1DayIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerf1DayIntervalOutHCPackets
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber

                The Object 
                retValPwPerf1DayIntervalOutHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerf1DayIntervalOutHCPackets (UINT4 u4PwIndex,
                                      UINT4 u4PwPerf1DayIntervalNumber,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValPwPerf1DayIntervalOutHCPackets)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX (pPwVcPerfInfo->
                                                  u1PwVcPerfDayIndex,
                                                  u4PwPerf1DayIntervalNumber);
            MEMCPY (pu8RetValPwPerf1DayIntervalOutHCPackets,
                    L2VPN_PWVC_PERF_1DAY_OUT_HC_PKTS (pPwVcPerfInfo,
                                                      u4PwPerf1DayIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPwPerf1DayIntervalOutHCBytes
 Input       :  The Indices
                PwIndex
                PwPerf1DayIntervalNumber

                The Object 
                retValPwPerf1DayIntervalOutHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerf1DayIntervalOutHCBytes (UINT4 u4PwIndex,
                                    UINT4 u4PwPerf1DayIntervalNumber,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValPwPerf1DayIntervalOutHCBytes)
{
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;

    if (gPwVcGlobalInfo.ppPwVcInfoTable != NULL)
    {
        pPwVcPerfInfo = L2VPN_PWVC_INFO_TABLE_PTR (u4PwIndex);
        if (pPwVcPerfInfo != NULL)
        {
            L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX (pPwVcPerfInfo->
                                                  u1PwVcPerfDayIndex,
                                                  u4PwPerf1DayIntervalNumber);
            MEMCPY (pu8RetValPwPerf1DayIntervalOutHCBytes,
                    L2VPN_PWVC_PERF_1DAY_OUT_HC_BYTS (pPwVcPerfInfo,
                                                      u4PwPerf1DayIntervalNumber),
                    sizeof (tSNMP_COUNTER64_TYPE));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwPerfTotalErrorPackets
 Input       :  The Indices

                The Object 
                retValPwPerfTotalErrorPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPerfTotalErrorPackets (UINT4 *pu4RetValPwPerfTotalErrorPackets)
{
    *pu4RetValPwPerfTotalErrorPackets =
        L2VPN_PWVC_PERF_TOTAL_ERROR_PKTS (gpPwVcGlobalInfo);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PwIndexMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwIndexMappingTable
 Input       :  The Indices
                PwIndexMappingPwType
                PwIndexMappingPwID
                PwIndexMappingPeerAddrType
                PwIndexMappingPeerAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwIndexMappingTable (INT4 i4PwIndexMappingPwType,
                                             UINT4 u4PwIndexMappingPwID,
                                             INT4 i4PwIndexMappingPeerAddrType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pPwIndexMappingPeerAddr)
{
    UNUSED_PARAM (i4PwIndexMappingPwType);
    UNUSED_PARAM (u4PwIndexMappingPwID);
    UNUSED_PARAM (i4PwIndexMappingPeerAddrType);
    UNUSED_PARAM (pPwIndexMappingPeerAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwIndexMappingTable
 Input       :  The Indices
                PwIndexMappingPwType
                PwIndexMappingPwID
                PwIndexMappingPeerAddrType
                PwIndexMappingPeerAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwIndexMappingTable (INT4 *pi4PwIndexMappingPwType,
                                     UINT4 *pu4PwIndexMappingPwID,
                                     INT4 *pi4PwIndexMappingPeerAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pPwIndexMappingPeerAddr)
{
    UNUSED_PARAM (pi4PwIndexMappingPwType);
    UNUSED_PARAM (pu4PwIndexMappingPwID);
    UNUSED_PARAM (pi4PwIndexMappingPeerAddrType);
    UNUSED_PARAM (pPwIndexMappingPeerAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwIndexMappingTable
 Input       :  The Indices
                PwIndexMappingPwType
                nextPwIndexMappingPwType
                PwIndexMappingPwID
                nextPwIndexMappingPwID
                PwIndexMappingPeerAddrType
                nextPwIndexMappingPeerAddrType
                PwIndexMappingPeerAddr
                nextPwIndexMappingPeerAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwIndexMappingTable (INT4 i4PwIndexMappingPwType,
                                    INT4 *pi4NextPwIndexMappingPwType,
                                    UINT4 u4PwIndexMappingPwID,
                                    UINT4 *pu4NextPwIndexMappingPwID,
                                    INT4 i4PwIndexMappingPeerAddrType,
                                    INT4 *pi4NextPwIndexMappingPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pPwIndexMappingPeerAddr,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextPwIndexMappingPeerAddr)
{
    UNUSED_PARAM (i4PwIndexMappingPwType);
    UNUSED_PARAM (pi4NextPwIndexMappingPwType);
    UNUSED_PARAM (u4PwIndexMappingPwID);
    UNUSED_PARAM (pu4NextPwIndexMappingPwID);
    UNUSED_PARAM (i4PwIndexMappingPeerAddrType);
    UNUSED_PARAM (pi4NextPwIndexMappingPeerAddrType);
    UNUSED_PARAM (pPwIndexMappingPeerAddr);
    UNUSED_PARAM (pNextPwIndexMappingPeerAddr);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwIndexMappingPwIndex
 Input       :  The Indices
                PwIndexMappingPwType
                PwIndexMappingPwID
                PwIndexMappingPeerAddrType
                PwIndexMappingPeerAddr

                The Object 
                retValPwIndexMappingPwIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwIndexMappingPwIndex (INT4 i4PwIndexMappingPwType,
                             UINT4 u4PwIndexMappingPwID,
                             INT4 i4PwIndexMappingPeerAddrType,
                             tSNMP_OCTET_STRING_TYPE * pPwIndexMappingPeerAddr,
                             UINT4 *pu4RetValPwIndexMappingPwIndex)
{
    UNUSED_PARAM (i4PwIndexMappingPwType);
    UNUSED_PARAM (u4PwIndexMappingPwID);
    UNUSED_PARAM (i4PwIndexMappingPeerAddrType);
    UNUSED_PARAM (pPwIndexMappingPeerAddr);
    UNUSED_PARAM (pu4RetValPwIndexMappingPwIndex);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : PwPeerMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePwPeerMappingTable
 Input       :  The Indices
                PwPeerMappingPeerAddrType
                PwPeerMappingPeerAddr
                PwPeerMappingPwType
                PwPeerMappingPwID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePwPeerMappingTable (INT4 i4PwPeerMappingPeerAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pPwPeerMappingPeerAddr,
                                            INT4 i4PwPeerMappingPwType,
                                            UINT4 u4PwPeerMappingPwID)
{
    UNUSED_PARAM (i4PwPeerMappingPeerAddrType);
    UNUSED_PARAM (pPwPeerMappingPeerAddr);
    UNUSED_PARAM (i4PwPeerMappingPwType);
    UNUSED_PARAM (u4PwPeerMappingPwID);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPwPeerMappingTable
 Input       :  The Indices
                PwPeerMappingPeerAddrType
                PwPeerMappingPeerAddr
                PwPeerMappingPwType
                PwPeerMappingPwID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPwPeerMappingTable (INT4 *pi4PwPeerMappingPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pPwPeerMappingPeerAddr,
                                    INT4 *pi4PwPeerMappingPwType,
                                    UINT4 *pu4PwPeerMappingPwID)
{
    UNUSED_PARAM (pi4PwPeerMappingPeerAddrType);
    UNUSED_PARAM (pPwPeerMappingPeerAddr);
    UNUSED_PARAM (pi4PwPeerMappingPwType);
    UNUSED_PARAM (pu4PwPeerMappingPwID);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPwPeerMappingTable
 Input       :  The Indices
                PwPeerMappingPeerAddrType
                nextPwPeerMappingPeerAddrType
                PwPeerMappingPeerAddr
                nextPwPeerMappingPeerAddr
                PwPeerMappingPwType
                nextPwPeerMappingPwType
                PwPeerMappingPwID
                nextPwPeerMappingPwID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPwPeerMappingTable (INT4 i4PwPeerMappingPeerAddrType,
                                   INT4 *pi4NextPwPeerMappingPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pPwPeerMappingPeerAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextPwPeerMappingPeerAddr,
                                   INT4 i4PwPeerMappingPwType,
                                   INT4 *pi4NextPwPeerMappingPwType,
                                   UINT4 u4PwPeerMappingPwID,
                                   UINT4 *pu4NextPwPeerMappingPwID)
{
    UNUSED_PARAM (i4PwPeerMappingPeerAddrType);
    UNUSED_PARAM (pi4NextPwPeerMappingPeerAddrType);
    UNUSED_PARAM (pPwPeerMappingPeerAddr);
    UNUSED_PARAM (pNextPwPeerMappingPeerAddr);
    UNUSED_PARAM (i4PwPeerMappingPwType);
    UNUSED_PARAM (pi4NextPwPeerMappingPwType);
    UNUSED_PARAM (u4PwPeerMappingPwID);
    UNUSED_PARAM (pu4NextPwPeerMappingPwID);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwPeerMappingPwIndex
 Input       :  The Indices
                PwPeerMappingPeerAddrType
                PwPeerMappingPeerAddr
                PwPeerMappingPwType
                PwPeerMappingPwID

                The Object 
                retValPwPeerMappingPwIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwPeerMappingPwIndex (INT4 i4PwPeerMappingPeerAddrType,
                            tSNMP_OCTET_STRING_TYPE * pPwPeerMappingPeerAddr,
                            INT4 i4PwPeerMappingPwType,
                            UINT4 u4PwPeerMappingPwID,
                            UINT4 *pu4RetValPwPeerMappingPwIndex)
{
    UNUSED_PARAM (i4PwPeerMappingPeerAddrType);
    UNUSED_PARAM (pPwPeerMappingPeerAddr);
    UNUSED_PARAM (i4PwPeerMappingPwType);
    UNUSED_PARAM (u4PwPeerMappingPwID);
    UNUSED_PARAM (pu4RetValPwPeerMappingPwIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPwUpDownNotifEnable
 Input       :  The Indices

                The Object 
                retValPwUpDownNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwUpDownNotifEnable (INT4 *pi4RetValPwUpDownNotifEnable)
{
    *pi4RetValPwUpDownNotifEnable = MPLS_SNMP_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPwDeletedNotifEnable
 Input       :  The Indices

                The Object 
                retValPwDeletedNotifEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwDeletedNotifEnable (INT4 *pi4RetValPwDeletedNotifEnable)
{
    *pi4RetValPwDeletedNotifEnable = MPLS_SNMP_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPwNotifRate
 Input       :  The Indices

                The Object 
                retValPwNotifRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPwNotifRate (UINT4 *pu4RetValPwNotifRate)
{
    *pu4RetValPwNotifRate = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPwUpDownNotifEnable
 Input       :  The Indices

                The Object 
                setValPwUpDownNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwUpDownNotifEnable (INT4 i4SetValPwUpDownNotifEnable)
{
    UNUSED_PARAM (i4SetValPwUpDownNotifEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwDeletedNotifEnable
 Input       :  The Indices

                The Object 
                setValPwDeletedNotifEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwDeletedNotifEnable (INT4 i4SetValPwDeletedNotifEnable)
{
    UNUSED_PARAM (i4SetValPwDeletedNotifEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPwNotifRate
 Input       :  The Indices

                The Object 
                setValPwNotifRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPwNotifRate (UINT4 u4SetValPwNotifRate)
{
    UNUSED_PARAM (u4SetValPwNotifRate);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PwUpDownNotifEnable
 Input       :  The Indices

                The Object 
                testValPwUpDownNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwUpDownNotifEnable (UINT4 *pu4ErrorCode,
                              INT4 i4TestValPwUpDownNotifEnable)
{

    switch (i4TestValPwUpDownNotifEnable)
    {
        case L2VPN_SNMP_TRUE:
        case L2VPN_SNMP_FALSE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PwDeletedNotifEnable
 Input       :  The Indices

                The Object 
                testValPwDeletedNotifEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwDeletedNotifEnable (UINT4 *pu4ErrorCode,
                               INT4 i4TestValPwDeletedNotifEnable)
{
    switch (i4TestValPwDeletedNotifEnable)
    {
        case L2VPN_SNMP_TRUE:
        case L2VPN_SNMP_FALSE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2PwNotifRate
 Input       :  The Indices

                The Object 
                testValPwNotifRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PwNotifRate (UINT4 *pu4ErrorCode, UINT4 u4TestValPwNotifRate)
{
    UNUSED_PARAM (u4TestValPwNotifRate);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PwUpDownNotifEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwUpDownNotifEnable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PwDeletedNotifEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwDeletedNotifEnable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PwNotifRate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PwNotifRate (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
