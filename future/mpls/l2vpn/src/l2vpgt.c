/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vpgt.c,v 1.27 2016/04/09 09:54:40 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpgt.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the low level GET routines
 *                             for the MIB objects present in the fsl2vpn.mib.
 *---------------------------------------------------------------------------*/

# include  "l2vpincs.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnTrcFlag
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnTrcFlag (INT4 *pi4RetValFsMplsL2VpnTrcFlag)
{
    *pi4RetValFsMplsL2VpnTrcFlag = (INT4) L2VPN_DBG_FLAG;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnCleanupInterval
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnCleanupInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnCleanupInterval (INT4 *pi4RetValFsMplsL2VpnCleanupInterval)
{
    *pi4RetValFsMplsL2VpnCleanupInterval
        = (INT4) L2VPN_MAX_PWVC_CLEANUP_INT (gpPwVcGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnAdminStatus
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnAdminStatus (INT4 *pi4RetValFsMplsL2VpnAdminStatus)
{
    *pi4RetValFsMplsL2VpnAdminStatus = L2VPN_ADMIN_STATUS;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnMaxPwVcEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnMaxPwVcEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnMaxPwVcEntries (UINT4 *pu4RetValFsMplsL2VpnMaxPwVcEntries)
{
    *pu4RetValFsMplsL2VpnMaxPwVcEntries =
        L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnMaxPwVcMplsEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnMaxPwVcMplsEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnMaxPwVcMplsEntries (UINT4
                                     *pu4RetValFsMplsL2VpnMaxPwVcMplsEntries)
{
    *pu4RetValFsMplsL2VpnMaxPwVcMplsEntries
        = L2VPN_MAX_MPLS_ENTRIES (gpPwVcMplsGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnMaxPwVcMplsInOutEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnMaxPwVcMplsInOutEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnMaxPwVcMplsInOutEntries (UINT4
                                          *pu4RetValFsMplsL2VpnMaxPwVcMplsInOutEntries)
{
    *pu4RetValFsMplsL2VpnMaxPwVcMplsInOutEntries
        = L2VPN_CFG_MAX_MPLS_IN_OUT_ENTRIES (gpPwVcMplsGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnMaxEthernetPwVcs
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnMaxEthernetPwVcs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnMaxEthernetPwVcs (UINT4 *pu4RetValFsMplsL2VpnMaxEthernetPwVcs)
{
    *pu4RetValFsMplsL2VpnMaxEthernetPwVcs
        = L2VPN_MAX_ENET_SERV_ENTRIES (gpPwVcEnetGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnMaxPwVcEnetEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnMaxPwVcEnetEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnMaxPwVcEnetEntries (UINT4
                                     *pu4RetValFsMplsL2VpnMaxPwVcEnetEntries)
{
    *pu4RetValFsMplsL2VpnMaxPwVcEnetEntries
        = L2VPN_MAX_ENET_ENTRIES (gpPwVcEnetGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnActivePwVcEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnActivePwVcEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnActivePwVcEntries (UINT4
                                    *pu4RetValFsMplsL2VpnActivePwVcEntries)
{
    *pu4RetValFsMplsL2VpnActivePwVcEntries
        = L2VPN_ACTIVE_PWVC_ENTRIES (gpPwVcGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnActivePwVcMplsEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnActivePwVcMplsEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnActivePwVcMplsEntries (UINT4
                                        *pu4RetValFsMplsL2VpnActivePwVcMplsEntries)
{
    *pu4RetValFsMplsL2VpnActivePwVcMplsEntries
        = L2VPN_ACTIVE_PWVC_MPLS_ENTRIES (gpPwVcGlobalInfo,
                                          L2VPN_PSN_STAT_MPLS);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnActivePwVcEnetEntries
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnActivePwVcEnetEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnActivePwVcEnetEntries (UINT4
                                        *pu4RetValFsMplsL2VpnActivePwVcEnetEntries)
{
    *pu4RetValFsMplsL2VpnActivePwVcEnetEntries
        = L2VPN_ACTIVE_PWVC_ENET_ENTRIES (gpPwVcGlobalInfo,
                                          L2VPN_SERV_STAT_ETH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnNoOfPwVcEntriesCreated
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnNoOfPwVcEntriesCreated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnNoOfPwVcEntriesCreated (UINT4
                                         *pu4RetValFsMplsL2VpnNoOfPwVcEntriesCreated)
{
    *pu4RetValFsMplsL2VpnNoOfPwVcEntriesCreated
        = L2VPN_NO_OF_PWVC_ENTRIES_CREATED (gpPwVcGlobalInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnNoOfPwVcEntriesDeleted
 Input       :  The Indices

                The Object 
                retValFsMplsL2VpnNoOfPwVcEntriesDeleted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnNoOfPwVcEntriesDeleted (UINT4
                                         *pu4RetValFsMplsL2VpnNoOfPwVcEntriesDeleted)
{
    *pu4RetValFsMplsL2VpnNoOfPwVcEntriesDeleted
        = L2VPN_NO_OF_PWVC_ENTRIES_DESTROYED (gpPwVcGlobalInfo);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsL2VpnPwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsL2VpnPwTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsL2VpnPwTable (UINT4 u4PwIndex)
{
    return (nmhValidateIndexInstancePwTable (u4PwIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsL2VpnPwTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsL2VpnPwTable (UINT4 *pu4PwIndex)
{
    return (nmhGetFirstIndexPwTable (pu4PwIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsL2VpnPwTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsL2VpnPwTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    return (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwMode
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwMode (UINT4 u4PwIndex, INT4 *pi4RetValFsMplsL2VpnPwMode)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValFsMplsL2VpnPwMode = L2VPN_PWVC_MODE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnVplsIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnVplsIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnVplsIndex (UINT4 u4PwIndex,
                            UINT4 *pu4RetValFsMplsL2VpnVplsIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValFsMplsL2VpnVplsIndex = L2VPN_PWVC_VPLS_INDEX (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwLocalCapabAdvert
 Input       :  The Indices
                PwIndex

                The Object
                retValFsMplsL2VpnPwLocalCapabAdvert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsMplsL2VpnPwLocalCapabAdvert (UINT4 u4PwIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMplsL2VpnPwLocalCapabAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwLocalCapabAdvert,
                     &L2VPN_PWVC_LCL_CAP_ADVT (pPwVcEntry), sizeof (UINT1));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwLocalCCSelected
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwLocalCCSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwLocalCCSelected (UINT4 u4PwIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMplsL2VpnPwLocalCCSelected)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwLocalCCSelected,
                     &L2VPN_PWVC_LCL_CC_SELECTED (pPwVcEntry), sizeof (UINT1));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwLocalCVSelected
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwLocalCVSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwLocalCVSelected (UINT4 u4PwIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMplsL2VpnPwLocalCVSelected)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwLocalCVSelected,
                     &L2VPN_PWVC_LCL_CV_SELECTED (pPwVcEntry), sizeof (UINT1));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwLocalCCAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwLocalCCAdvert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwLocalCCAdvert (UINT4 u4PwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsMplsL2VpnPwLocalCCAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwLocalCCAdvert,
                     &L2VPN_PWVC_LCL_CC_ADVERT (pPwVcEntry), sizeof (UINT1));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwLocalCVAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwLocalCVAdvert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwLocalCVAdvert (UINT4 u4PwIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsMplsL2VpnPwLocalCVAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwLocalCVAdvert,
                     &L2VPN_PWVC_LCL_CV_ADVERT (pPwVcEntry), sizeof (UINT1));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwRemoteCCAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwRemoteCCAdvert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwRemoteCCAdvert (UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMplsL2VpnPwRemoteCCAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwRemoteCCAdvert,
                     &L2VPN_PWVC_REMOTE_CC_ADVERT (pPwVcEntry), sizeof (UINT1));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwRemoteCVAdvert
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwRemoteCVAdvert
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwRemoteCVAdvert (UINT4 u4PwIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMplsL2VpnPwRemoteCVAdvert)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        CPY_TO_SNMP (pRetValFsMplsL2VpnPwRemoteCVAdvert,
                     &L2VPN_PWVC_REMOTE_CV_ADVERT (pPwVcEntry), sizeof (UINT1));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwOamEnable
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwOamEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwOamEnable (UINT4 u4PwIndex,
                              INT4 *pi4RetValFsMplsL2VpnPwOamEnable)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValFsMplsL2VpnPwOamEnable = L2VPN_PWVC_OAM_ENABLE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwGenAGIType
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwGenAGIType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwGenAGIType (UINT4 u4PwIndex,
                               UINT4 *pu4RetValFsMplsL2VpnPwGenAGIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValFsMplsL2VpnPwGenAGIType =
            L2VPN_PWVC_GEN_AGI_TYPE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwGenLocalAIIType
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwGenLocalAIIType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwGenLocalAIIType (UINT4 u4PwIndex,
                                    UINT4
                                    *pu4RetValFsMplsL2VpnPwGenLocalAIIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValFsMplsL2VpnPwGenLocalAIIType =
            L2VPN_PWVC_GEN_LCL_AII_TYPE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwGenRemoteAIIType
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwGenRemoteAIIType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwGenRemoteAIIType (UINT4 u4PwIndex,
                                     UINT4
                                     *pu4RetValFsMplsL2VpnPwGenRemoteAIIType)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValFsMplsL2VpnPwGenRemoteAIIType =
            L2VPN_PWVC_GEN_REMOTE_AII_TYPE (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsLocalCCTypesCapabilities
 Input       :  The Indices

                The Object
                retValFsMplsLocalCCTypesCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLocalCCTypesCapabilities (tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMplsLocalCCTypesCapabilities)
{
    CPY_TO_SNMP (pRetValFsMplsLocalCCTypesCapabilities,
                 &gL2VpnGlobalInfo.u1LocalCcTypeCapabilities, sizeof (UINT1));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLocalCVTypesCapabilities
 Input       :  The Indices

                The Object
                retValFsMplsLocalCVTypesCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLocalCVTypesCapabilities (tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMplsLocalCVTypesCapabilities)
{
    CPY_TO_SNMP (pRetValFsMplsLocalCVTypesCapabilities,
                 &gL2VpnGlobalInfo.u1LocalCvTypeCapabilities, sizeof (UINT1));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnProactiveOamSsnIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnProactiveOamSsnIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnProactiveOamSsnIndex (UINT4 u4PwIndex,
                                       UINT4
                                       *pu4RetValFsMplsL2VpnProactiveOamSsnIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pu4RetValFsMplsL2VpnProactiveOamSsnIndex =
            L2VPN_PWVC_OAM_SESSION_INDEX (pPwVcEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwAIIFormat
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwAIIFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwAIIFormat (UINT4 u4PwIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMplsL2VpnPwAIIFormat)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);
    if (pPwVcEntry != NULL)
    {
        pRetValFsMplsL2VpnPwAIIFormat->pu1_OctetList[0] =
            pPwVcEntry->u1AiiFormat;
        pRetValFsMplsL2VpnPwAIIFormat->i4_Length = 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 * Function    :  nmhGetFsMplsL2VpnIsStaticPw 
 * Input       :  The Indices
 *                PwIndex
 *                The Object retValFsMplsL2VpnIsStaticPw
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.   
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnIsStaticPw (UINT4 u4PwIndex,
                             INT4 *pi4RetValFsMplsL2VpnIsStaticPw)
{
    tPwVcEntry         *pPwVcEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry != NULL)
    {
        *pi4RetValFsMplsL2VpnIsStaticPw = pPwVcEntry->bIsStaticPw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnNextFreePwEnetPwInstance
 Input       :  The Indices
                PwIndex

                The Object
                retValFsMplsL2VpnNextFreePwEnetPwInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (UINT4 u4PwIndex,
                                           UINT4
                                           *pu4RetValFsMplsL2VpnNextFreePwEnetPwInstance)
{
    tPwVcEntry         *pPwEntry = NULL;

    pPwEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsL2VpnNextFreePwEnetPwInstance =
        (UINT4) pPwEntry->u2NextFreeEnetInstance;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL2VpnPwSynchronizationStatus
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsMplsL2VpnPwSynchronizationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL2VpnPwSynchronizationStatus (UINT4 u4PwIndex,
                                          INT4
                                          *pi4RetValFsMplsL2VpnPwSynchronizationStatus)
{
    tPwVcEntry         *pPwEntry = NULL;

    pPwEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsL2VpnPwSynchronizationStatus =
        (INT4) pPwEntry->u1GrSyncStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsHwCCTypeCapabilities
 Input       :  The Indices

                The Object
                retValFsMplsHwCCTypeCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsHwCCTypeCapabilities (tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsMplsHwCCTypeCapabilities)
{
    CPY_TO_SNMP (pRetValFsMplsHwCCTypeCapabilities,
                 &gL2VpnGlobalInfo.u1LocalHwCcTypeCapabilities, sizeof (UINT1));

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsVplsConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsVplsConfigTable
 Input       :  The Indices
                FsMplsVplsInstanceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsVplsConfigTable (UINT4 u4FsMplsVplsInstanceIndex)
{
    if ((u4FsMplsVplsInstanceIndex == 0) ||
        (u4FsMplsVplsInstanceIndex > gL2VpnGlobalInfo.u4MaxVplsEntries))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsVplsConfigTable
 Input       :  The Indices
                FsMplsVplsInstanceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsVplsConfigTable (UINT4 *pu4FsMplsVplsInstanceIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4Index;

    for (u4Index = 1; u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry != NULL)
        {
            *pu4FsMplsVplsInstanceIndex = L2VPN_VPLS_INDEX (pVplsEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsVplsConfigTable
 Input       :  The Indices
                FsMplsVplsInstanceIndex
                nextFsMplsVplsInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsVplsConfigTable (UINT4 u4FsMplsVplsInstanceIndex,
                                      UINT4 *pu4NextFsMplsVplsInstanceIndex)
{
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4Index;

    if (u4FsMplsVplsInstanceIndex >= gL2VpnGlobalInfo.u4MaxVplsEntries)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = u4FsMplsVplsInstanceIndex;
         u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);

        if (pVplsEntry != NULL)
        {
            if (L2VPN_VPLS_INDEX (pVplsEntry) > u4FsMplsVplsInstanceIndex)
            {
                *pu4NextFsMplsVplsInstanceIndex = L2VPN_VPLS_INDEX (pVplsEntry);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsVplsVsi
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsVsi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsVsi (UINT4 u4FsMplsVplsInstanceIndex,
                     INT4 *pi4RetValFsMplsVplsVsi)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pi4RetValFsMplsVplsVsi = L2VPN_VPLS_VSI (pVplsEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsVplsVpnId
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsVpnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsVpnId (UINT4 u4FsMplsVplsInstanceIndex,
		tSNMP_OCTET_STRING_TYPE * pRetValFsMplsVplsVpnId)
{
	tVPLSEntry         *pVplsEntry = NULL;
	UINT1               au1VplsVpnID[L2VPN_MAX_VPLS_VPNID_LEN + 4];
	pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);
	MEMSET (au1VplsVpnID, L2VPN_ZERO, L2VPN_MAX_VPLS_VPNID_LEN + 4);
	if (pVplsEntry != NULL)
	{
		if (MEMCMP (au1VplsVpnID,  &L2VPN_VPLS_VPN_ID (pVplsEntry),L2VPN_MAX_VPLS_VPNID_LEN) != L2VPN_ZERO)
		{

			MEMCPY (pRetValFsMplsVplsVpnId->pu1_OctetList,
					&L2VPN_VPLS_VPN_ID (pVplsEntry), L2VPN_MAX_VPLS_VPNID_LEN);
			pRetValFsMplsVplsVpnId->i4_Length = L2VPN_MAX_VPLS_VPNID_LEN;
			return SNMP_SUCCESS;
		}
		return SNMP_SUCCESS;
	}
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsVplsName
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsName (UINT4 u4FsMplsVplsInstanceIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsMplsVplsName)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_ARRAY_LEN (L2VPN_VPLS_NAME (pVplsEntry), L2VPN_MAX_VPLS_NAME_LEN,
                         pRetValFsMplsVplsName->i4_Length);
        MEMCPY (pRetValFsMplsVplsName->pu1_OctetList,
                &L2VPN_VPLS_NAME (pVplsEntry),
                pRetValFsMplsVplsName->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsVplsDescr
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsDescr (UINT4 u4FsMplsVplsInstanceIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMplsVplsDescr)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        L2VPN_ARRAY_LEN (L2VPN_VPLS_DESCR (pVplsEntry),
                         L2VPN_MAX_VPLS_DESCR_LEN,
                         pRetValFsMplsVplsDescr->i4_Length);
        MEMCPY (pRetValFsMplsVplsDescr->pu1_OctetList,
                &L2VPN_VPLS_DESCR (pVplsEntry),
                pRetValFsMplsVplsDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsVplsFdbHighWatermark
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsFdbHighWatermark
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsFdbHighWatermark (UINT4 u4FsMplsVplsInstanceIndex,
                                  UINT4 *pu4RetValFsMplsVplsFdbHighWatermark)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pu4RetValFsMplsVplsFdbHighWatermark =
            L2VPN_VPLS_FDB_HIGH_THRESHOLD (pVplsEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsVplsFdbLowWatermark
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsFdbLowWatermark
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsFdbLowWatermark (UINT4 u4FsMplsVplsInstanceIndex,
                                 UINT4 *pu4RetValFsMplsVplsFdbLowWatermark)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pu4RetValFsMplsVplsFdbLowWatermark =
            L2VPN_VPLS_FDB_LOW_THRESHOLD (pVplsEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsVplsRowStatus
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsMplsVplsRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsRowStatus (UINT4 u4FsMplsVplsInstanceIndex,
                           INT4 *pi4RetValFsMplsVplsRowStatus)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pi4RetValFsMplsVplsRowStatus = L2VPN_VPLS_ROW_STATUS (pVplsEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* VPLS FDB */
/****************************************************************************
 Function    :  nmhGetFsMplsVplsL2MapFdbId
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object
                retValFsMplsVplsL2MapFdbId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsVplsL2MapFdbId (UINT4 u4FsMplsVplsInstanceIndex,
                            INT4 *pi4RetValFsMplsVplsL2MapFdbId)
{
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pi4RetValFsMplsVplsL2MapFdbId = (INT4) L2VPN_VPLS_FDB_ID (pVplsEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsmplsVplsMtu
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsmplsVplsMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsmplsVplsMtu(UINT4 u4FsMplsVplsInstanceIndex , 
                         UINT4 *pu4RetValFsmplsVplsMtu)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pu4RetValFsmplsVplsMtu = L2VPN_VPLS_MTU(pVplsEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM(u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM(pu4RetValFsmplsVplsMtu);
    return SNMP_SUCCESS;
#endif
}
/****************************************************************************
 Function    :  nmhGetFsmplsVplsStorageType
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsmplsVplsStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsmplsVplsStorageType(UINT4 u4FsMplsVplsInstanceIndex , 
                                 INT4 *pi4RetValFsmplsVplsStorageType)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pi4RetValFsmplsVplsStorageType = 
                                 (INT4) L2VPN_VPLS_STORAGE_TYPE(pVplsEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM(u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM(pi4RetValFsmplsVplsStorageType);
    return SNMP_SUCCESS;
#endif
}
/****************************************************************************
 Function    :  nmhGetFsmplsVplsSignalingType
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsmplsVplsSignalingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsmplsVplsSignalingType(UINT4 u4FsMplsVplsInstanceIndex , 
                                   INT4 *pi4RetValFsmplsVplsSignalingType)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pi4RetValFsmplsVplsSignalingType = 
                                 (INT4) L2VPN_VPLS_SIG_TYPE(pVplsEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM(u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM(pi4RetValFsmplsVplsSignalingType);
    return SNMP_SUCCESS;
#endif
}
/****************************************************************************
 Function    :  nmhGetFsmplsVplsControlWord
 Input       :  The Indices
                FsMplsVplsInstanceIndex

                The Object 
                retValFsmplsVplsControlWord
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsmplsVplsControlWord(UINT4 u4FsMplsVplsInstanceIndex , 
                                 INT4 *pi4RetValFsmplsVplsControlWord)
{
#ifdef VPLSADS_WANTED
    tVPLSEntry         *pVplsEntry = NULL;

    pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4FsMplsVplsInstanceIndex);

    if (pVplsEntry != NULL)
    {
        *pi4RetValFsmplsVplsControlWord = 
                                 (INT4) L2VPN_VPLS_CONTROL_WORD(pVplsEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM(u4FsMplsVplsInstanceIndex);
    UNUSED_PARAM(pi4RetValFsmplsVplsControlWord);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPwMplsInboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPwMplsInboundTable (UINT4 u4PwIndex)
{
    if (nmhValidateIndexInstancePwMplsTable (u4PwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPwMplsInboundTable
 Input       :  The Indices
                PwIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexFsPwMplsInboundTable (UINT4 *pu4PwIndex)
{
    if (nmhGetFirstIndexPwTable (pu4PwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (L2VpnValidatePsnEntry (*pu4PwIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPwMplsInboundTable
 Input       :  The Indices
                PwIndex
                nextPwIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPwMplsInboundTable (UINT4 u4PwIndex, UINT4 *pu4NextPwIndex)
{
    if (nmhGetNextIndexPwTable (u4PwIndex, pu4NextPwIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (L2VpnValidatePsnEntry (*pu4NextPwIndex));
}

/****************************************************************************
 Function    :  nmhGetFsPwMplsInboundTunnelIndex
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsPwMplsInboundTunnelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPwMplsInboundTunnelIndex (UINT4 u4PwIndex,
                                  UINT4 *pu4RetValFsPwMplsInboundTunnelIndex)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    *pu4RetValFsPwMplsInboundTunnelIndex =
        pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.u4TnlIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPwMplsInboundTunnelInstance
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsPwMplsInboundTunnelInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPwMplsInboundTunnelInstance (UINT4 u4PwIndex,
                                     UINT4
                                     *pu4RetValFsPwMplsInboundTunnelInstance)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    *pu4RetValFsPwMplsInboundTunnelInstance =
        pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.u2TnlInstance;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPwMplsInboundTunnelEgressLSR
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsPwMplsInboundTunnelEgressLSR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPwMplsInboundTunnelEgressLSR (UINT4 u4PwIndex,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pRetValFsPwMplsInboundTunnelEgressLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    CPY_TO_SNMP (pRetValFsPwMplsInboundTunnelEgressLSR,
                 pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.TnlPeerLSR,
                 ROUTER_ID_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPwMplsInboundTunnelIngressLSR
 Input       :  The Indices
                PwIndex

                The Object 
                retValFsPwMplsInboundTunnelIngressLSR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPwMplsInboundTunnelIngressLSR (UINT4 u4PwIndex,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValFsPwMplsInboundTunnelIngressLSR)
{
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnlEntry = NULL;

    pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwIndex);

    if (pPwVcEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL)
    {
        return SNMP_FAILURE;
    }

    pPwVcMplsInTnlEntry = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                    L2VPN_PWVC_PSN_ENTRY
                                                    (pPwVcEntry));

    CPY_TO_SNMP (pRetValFsPwMplsInboundTunnelIngressLSR,
                 pPwVcMplsInTnlEntry->unInTnlInfo.TeInfo.TnlLclLSR,
                 ROUTER_ID_LENGTH);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsPortTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsPortTable (INT4 i4IfIndex)
{
    UINT1               u1IfType = 0;

    if (CfaValidateIfIndex ((UINT4) i4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfaceType ((UINT4) i4IfIndex, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsPortTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMplsPortTable (INT4 *pi4IfIndex)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = RBTreeGetFirst (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pMplsPortEntryInfo->u4IfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsPortTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsPortTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;
    tMplsPortEntryInfo *pMplsPortEntryInfoIn = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfoIn = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfoIn == NULL)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = (tMplsPortEntryInfo *)
        RBTreeGetNext (L2VPN_MPLS_PORT_ENTRY_INFO_TABLE,
                       pMplsPortEntryInfoIn, NULL);
    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pMplsPortEntryInfo->u4IfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsPortBundleStatus
 Input       :  The Indices
                IfIndex

                The Object
                retValFsMplsPortBundleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsPortBundleStatus (INT4 i4IfIndex,
                              INT4 *pi4RetValFsMplsPortBundleStatus)
{
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsPortBundleStatus =
        (INT4) pMplsPortEntryInfo->u1BundleStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsPortMultiplexStatus
 Input       :  The Indices
                IfIndex

                The Object
                retValFsMplsPortMultiplexStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsPortMultiplexStatus (INT4 i4IfIndex,
                                 INT4 *pi4RetValFsMplsPortMultiplexStatus)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsPortMultiplexStatus =
        (INT4) pMplsPortEntryInfo->u1MultiplexStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsPortAllToOneBundleStatus
 Input       :  The Indices
                IfIndex

                The Object
                retValFsMplsPortAllToOneBundleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsPortAllToOneBundleStatus (INT4 i4IfIndex,
                                      INT4
                                      *pi4RetValFsMplsPortAllToOneBundleStatus)
{
    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsPortAllToOneBundleStatus =
        (INT4) pMplsPortEntryInfo->u1AllToOneBundleStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsPortRowStatus
 Input       :  The Indices
                IfIndex

                The Object
                retValFsMplsPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsPortRowStatus (INT4 i4IfIndex, INT4 *pi4RetValFsMplsPortRowStatus)
{

    tMplsPortEntryInfo *pMplsPortEntryInfo = NULL;

    if (L2VPN_ADMIN_STATUS == L2VPN_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    pMplsPortEntryInfo = L2VpnMplsPortEntryGetNode ((UINT4) i4IfIndex);

    if (pMplsPortEntryInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsPortRowStatus = (INT4) pMplsPortEntryInfo->i1RowStatus;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsVplsAcMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsVplsAcMapTable
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsMplsVplsAcMapTable(
                                          UINT4 u4FsMplsVplsAcMapVplsIndex , 
                                          UINT4 u4FsMplsVplsAcMapAcIndex)
{
#ifdef VPLSADS_WANTED
    if ( ( u4FsMplsVplsAcMapVplsIndex == 0 ) ||
         ( u4FsMplsVplsAcMapVplsIndex > gL2VpnGlobalInfo.u4MaxVplsEntries ) )
    {
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM(u4FsMplsVplsAcMapVplsIndex);
#endif
    UNUSED_PARAM(u4FsMplsVplsAcMapAcIndex);

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsVplsAcMapTable
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsMplsVplsAcMapTable(UINT4 *pu4FsMplsVplsAcMapVplsIndex ,
                                          UINT4 *pu4FsMplsVplsAcMapAcIndex)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry *pVplsAcEntry = NULL;

    pVplsAcEntry = (tVplsAcMapEntry *)
                   RBTreeGetFirst(L2VPN_VPLS_AC_RB_PTR(gpPwVcGlobalInfo));
    if ( NULL == pVplsAcEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4FsMplsVplsAcMapVplsIndex = L2VPN_VPLSAC_VPLS_INSTANCE(pVplsAcEntry);
    *pu4FsMplsVplsAcMapAcIndex = L2VPN_VPLSAC_INDEX(pVplsAcEntry);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM(pu4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM(pu4FsMplsVplsAcMapAcIndex);
    return SNMP_FAILURE;
#endif
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsVplsAcMapTable
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                nextFsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex
                nextFsMplsVplsAcMapAcIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsMplsVplsAcMapTable(UINT4 u4FsMplsVplsAcMapVplsIndex ,
                                         UINT4 *pu4NextFsMplsVplsAcMapVplsIndex,
                                         UINT4 u4FsMplsVplsAcMapAcIndex ,
                                         UINT4 *pu4NextFsMplsVplsAcMapAcIndex )
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry VplsAcEntry;
    tVplsAcMapEntry *pVplsAcEntry = NULL;

    MEMSET(&VplsAcEntry, L2VPN_ZERO, sizeof(tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE(&VplsAcEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX(&VplsAcEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcEntry = (tVplsAcMapEntry *)
                    RBTreeGetNext(L2VPN_VPLS_AC_RB_PTR(gpPwVcGlobalInfo),
                                  &VplsAcEntry,
                                  NULL);
    if ( NULL == pVplsAcEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMplsVplsAcMapVplsIndex = L2VPN_VPLSAC_VPLS_INSTANCE(pVplsAcEntry);
    *pu4NextFsMplsVplsAcMapAcIndex = L2VPN_VPLSAC_INDEX(pVplsAcEntry);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM(u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM(pu4NextFsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM(u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM(pu4NextFsMplsVplsAcMapAcIndex);
    return SNMP_FAILURE;
#endif
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsVplsAcMapPortIfIndex
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                retValFsMplsVplsAcMapPortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsVplsAcMapPortIfIndex(UINT4 u4FsMplsVplsAcMapVplsIndex , 
                                     UINT4 u4FsMplsVplsAcMapAcIndex , 
                                     UINT4 *pu4RetValFsMplsVplsAcMapPortIfIndex)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry VplsAcEntry;
    tVplsAcMapEntry *pVplsAcEntry = NULL;

    MEMSET(&VplsAcEntry, L2VPN_ZERO, sizeof(tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE(&VplsAcEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX(&VplsAcEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcEntry = (tVplsAcMapEntry *)
                    RBTreeGet(L2VPN_VPLS_AC_RB_PTR(gpPwVcGlobalInfo),
                              (tRBElem *) &VplsAcEntry);
    if ( NULL == pVplsAcEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsVplsAcMapPortIfIndex = L2VPN_VPLSAC_PORT_INDEX(pVplsAcEntry);
#else
    UNUSED_PARAM(u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM(u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM(pu4RetValFsMplsVplsAcMapPortIfIndex);
#endif

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMplsVplsAcMapPortVlan
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                retValFsMplsVplsAcMapPortVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsVplsAcMapPortVlan(UINT4 u4FsMplsVplsAcMapVplsIndex , 
                                   UINT4 u4FsMplsVplsAcMapAcIndex , 
                                   UINT4 *pu4RetValFsMplsVplsAcMapPortVlan)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry VplsAcEntry;
    tVplsAcMapEntry *pVplsAcEntry = NULL;

    MEMSET(&VplsAcEntry, L2VPN_ZERO, sizeof(tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE(&VplsAcEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX(&VplsAcEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcEntry = (tVplsAcMapEntry *)
                    RBTreeGet(L2VPN_VPLS_AC_RB_PTR(gpPwVcGlobalInfo),
                              (tRBElem *) &VplsAcEntry);
    if ( NULL == pVplsAcEntry )
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsVplsAcMapPortVlan = L2VPN_VPLSAC_VLAN_ID(pVplsAcEntry);
#else
    UNUSED_PARAM(u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM(u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM(pu4RetValFsMplsVplsAcMapPortVlan);
#endif

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsMplsVplsAcMapRowStatus
 Input       :  The Indices
                FsMplsVplsAcMapVplsIndex
                FsMplsVplsAcMapAcIndex

                The Object 
                retValFsMplsVplsAcMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsVplsAcMapRowStatus(UINT4 u4FsMplsVplsAcMapVplsIndex , 
                                    UINT4 u4FsMplsVplsAcMapAcIndex , 
                                    INT4 *pi4RetValFsMplsVplsAcMapRowStatus)
{
#ifdef VPLSADS_WANTED
    tVplsAcMapEntry VplsAcEntry;
    tVplsAcMapEntry *pVplsAcEntry = NULL;

    MEMSET(&VplsAcEntry, L2VPN_ZERO, sizeof(tVplsAcMapEntry));

    L2VPN_VPLSAC_VPLS_INSTANCE(&VplsAcEntry) = u4FsMplsVplsAcMapVplsIndex;
    L2VPN_VPLSAC_INDEX(&VplsAcEntry) = u4FsMplsVplsAcMapAcIndex;

    pVplsAcEntry = (tVplsAcMapEntry *)
                    RBTreeGet(L2VPN_VPLS_AC_RB_PTR(gpPwVcGlobalInfo),
                              (tRBElem *) &VplsAcEntry);
    if ( NULL == pVplsAcEntry )
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsVplsAcMapRowStatus = L2VPN_VPLSAC_ROW_STATUS(pVplsAcEntry);
#else
    UNUSED_PARAM(u4FsMplsVplsAcMapVplsIndex);
    UNUSED_PARAM(u4FsMplsVplsAcMapAcIndex);
    UNUSED_PARAM(pi4RetValFsMplsVplsAcMapRowStatus);
#endif

    return SNMP_SUCCESS;
}
