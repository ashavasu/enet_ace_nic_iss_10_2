
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vprmport.c,v 1.1 2014/08/25 12:23:30 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/

#include "l2vpincs.h"
#include "l2vpdefs.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmEventHandle                                         */
/*                                                                           */
/* Description  : This API constructs a message containing the               */
/*                given RM event and RM message. And posts it to the         */
/*                L2VPN  queue                                               */
/*                                                                           */
/* Input        : u1Event   - Event type given by RM module                  */
/*              : pData     - RM Message to enqueue                          */
/*              : u2DataLen - Length of the message                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

INT4 L2VpnRmEventHandler(UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tL2VpnQMsg          *pL2VpnMsg = NULL;

    /* check the Event type received from RM */  
    if ((u1Event != RM_MESSAGE) && (u1Event != GO_ACTIVE) &&
            (u1Event != GO_STANDBY) && (u1Event != RM_PEER_UP) &&
            (u1Event != RM_PEER_DOWN) &&
            (u1Event != L2_INITIATE_BULK_UPDATES) &&
            (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"Invalid event received from RM\n");
        return OSIX_FAILURE;
    }

    if ((u1Event == RM_MESSAGE) ||
            (u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
    {
        if (pData == NULL)
        {
            L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "NULL pointer message is received\n");
            return OSIX_FAILURE;
        }

        if (u1Event == RM_MESSAGE)
        {
            if (u2DataLen < L2VPN_RM_MSG_HDR_SIZE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received invalid RM Message\n");
                RM_FREE (pData);
                return OSIX_FAILURE;
            }

        }

        else
        {
            if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Received invalid standby node information \n");
                L2VpnRmRelRmMsgMem ((UINT1 *) pData);
                return OSIX_FAILURE;
            }

        }
    }

    /* Allocate memory from l2vpn queue pool id */
    pL2VpnMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    if(pL2VpnMsg == NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
        {
            L2VpnRmRelRmMsgMem ((UINT1 *) pData);
        }
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Memory allocation failed for message to post L2VPN Queue\n");

        return OSIX_FAILURE;
    }


    MEMSET (pL2VpnMsg, 0, sizeof(tL2VpnQMsg));   
    pL2VpnMsg->u4MsgType = L2VPN_RM_EVENT;
    pL2VpnMsg->L2VpnEvtInfo.L2VpnRmEvtInfo.pBuff = pData ;
    pL2VpnMsg->L2VpnEvtInfo.L2VpnRmEvtInfo.u2Length = u2DataLen;
    pL2VpnMsg->L2VpnEvtInfo.L2VpnRmEvtInfo.u1Event = u1Event;

    /* post the message to l2vpn queue */
    if(L2VpnInternalEventHandler((UINT1 *)pL2VpnMsg)== L2VPN_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
        {
            L2VpnRmRelRmMsgMem ((UINT1 *) pData);
        }
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Sending RM message to L2VPN queue failed\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmRegisterWithRM                                      */
/*                                                                           */
/* Description  : This function calls the Redundancy Module API to register  */
/*                with the redundancy module.                                */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS on Successfull registration.                  */
/*                else returns L2VPN_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT4 L2VpnRmRegisterWithRM()
{

    UINT4        u4RetVal = RM_FAILURE;
    tRmRegParams L2VpnRmRegParams ={RM_L2VPN_APP_ID, L2VpnRmEventHandler};

    /* Register the l2vpn module to RM */
    u4RetVal = RmRegisterProtocols (&L2VpnRmRegParams);

    if (u4RetVal != RM_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "L2VPN registration with RM failed\n");
        return L2VPN_FAILURE;
    }

    gL2VpnGlobalInfo.l2vpRmInfo.u1AdminState = L2VPN_ENABLED;


    L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG ,"L2VPN registration with RM Success\n");
    return L2VPN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmDeRegisterWithRM                                      */
/*                                                                           */
/* Description  : This function calls the Redundancy Module API to register  */
/*                with the redundancy module.                                */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS on Successfull registration.                  */
/*                else returns L2VPN_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT4 L2VpnRmDeRegisterWithRM()
{

    UINT4               u4RetVal = RM_FAILURE;
    u4RetVal = RmDeRegisterProtocols (RM_L2VPN_APP_ID);
    if (u4RetVal != RM_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "L2VPN de-registration with RM failed\n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;

}



/******************************************************************************
 *   Function           : L2VpnRmApiSendProtoAckToRM
 *   Input(s)           : u4SeqNum - Sequence number of the RM message for
 *                        which this ACK is generated.
 *   Output(s)          : None.
 *   Returns            : NONE
 *   Action             : This is the function used by L2VPN  task
 *                        to send acknowledgement to RM after processing the
 *                        sync-up message.
 *********************************************************************************/



VOID L2VpnRmApiSendProtoAckToRM(UINT4 u4SeqNum)
{
    tRmProtoAck         ProtoAck;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    ProtoAck.u4AppId = RM_L2VPN_APP_ID;
    ProtoAck.u4SeqNumber = u4SeqNum;
    RmApiSendProtoAckToRM (&ProtoAck);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmSendEventToRm                                         */
/*                                                                           */
/* Description  : This function calls the RM API to post the event           */
/* Input        : u1Event - Event to be sent to RM. And the event can be     */
/*                any of the following                                       */
/*                RM_PROTOCOL_BULK_UPDT_COMPLETION                           */
/*                RM_BULK_UPDT_ABORT                                         */
/*                RM_STANDBY_TO_ACTIVE_EVT_PROCESSED                         */
/*                RM_IDLE_TO_ACTIVE_EVT_PROCESSED                            */
/*                RM_STANDBY_EVT_PROCESSED                                   */
/*                                                                           */
/*                u1Error - In case the event is RM_BULK_UPDATE_ABORT,       */
/*                the reason for the failure is send in the error. And       */
/*                the error can be any of the following,                     */
/*                RM_MEMALLOC_FAIL                                           */
/*                RM_SENTO_FAIL                                              */
/*                RM_PROCESS_FAIL                                            */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS on Success                                   */
/*                else returns L2VPN_FAILURE.                                */
/*                                                                           */
/*****************************************************************************/


INT4  L2VpnRmSendEventToRm(tRmProtoEvt * pEvt) 
{

    INT4    i4RetVal = RM_FAILURE;
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
    if (i4RetVal != RM_SUCCESS)
    {
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmGetStandbyNodeCount                                 */
/*                                                                           */
/* Description  : This function calls the RM API to get the stan by node     */
/*                count.                                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                None                                                       */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Standby node count                                         */
/*                                                                           */
/*****************************************************************************/


UINT1 L2VpnRmGetStandbyNodeCount ()
{

    UINT1               u1StandbyCount = 0;

    u1StandbyCount = RmGetStandbyNodeCount ();

    return u1StandbyCount;
}


/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmRelRmMsgMem                                           */
/*                                                                           */
/* Description  : This function calls the RM API to release the memory       */
/*                allocated for RM message.                                  */
/*                                                                           */
/* Input        : pu1Block - Memory block to be released                     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS on Successfull release         .               */
/*                else returns L2VPN_FAILURE.                                  */
/*                                                                           */
/*****************************************************************************/


INT4 L2VpnRmRelRmMsgMem (UINT1 *pu1Block)
{


    UINT4   u4RetVal = RM_FAILURE;
    u4RetVal = RmReleaseMemoryForMsg (pu1Block);
    if (u4RetVal != RM_SUCCESS)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG , "Releasing the RM message memory failed \n");
        return L2VPN_FAILURE;
    }

    return L2VPN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmGetRmNodeState                                      */
/*                                                                           */
/* Description  : This function calls the RM API to get the node state.      */
/*                                                                           */
/* Input        : None                                                       */
/*                None                                                       */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Node state.                                                */
/*                                                                           */
/*****************************************************************************/


UINT4 L2VpnRmGetRmNodeState ()
{
    UINT4               u4RmState = RM_ACTIVE;

    u4RmState = RmGetNodeState ();

    return u4RmState;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmAllocForRmMsg                                       */
/*                                                                           */
/* Description  : This function allocates memory for the RM messages.        */
/*                If allocation fails the send the event to RM.              */
/*                                                                           */
/* Input        : u2Size - required memory size in bytes.                    */
/*                                                                           */
/* Output       : pRmMsg - pointer to the allocated memory.                  */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS on Successfull release.                        */
/*                else returns L2VPN_FAILURE.                                  */
/*                                                                           */
/*****************************************************************************/

tRmMsg* L2VpnRmAllocForRmMsg(UINT2 u2Size)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pRmMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
#ifdef L2RED_WANTED
    pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2Size);
#else
    UNUSED_PARAM (u2Size);
#endif
    if (pRmMsg == NULL)
    {

        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG,"Memory allocation failed\n");
        gL2VpnGlobalInfo.l2vpRmInfo.u4DynBulkUpdatStatus =L2VPN_RM_BLKUPDT_ABORTED;
        L2VpnRmSendBulkAbort (RM_MEMALLOC_FAIL);
    }

    return pRmMsg;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L2VpnRmSendMsgToRm                                         */
/*                                                                           */
/* Description  : This function calls the RM API to en-queue the             */
/*                message from L2VPN to RM task.                             */
/*                                                                           */
/* Input        : pRmMsg - Message from L2VPN                                */
/*                u2DataLen - Length of message                              */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : L2VPN_SUCCESS on Successfull en-queue                        */
/*                else returns L2VPN_FAILURE.                                  */
/*                                                                           */
/*****************************************************************************/


INT4 L2VpnRmSendMsgToRm(tRmMsg * pRmMsg, UINT2 u2DataLen)  
{


    UINT4               u4SrcEntId = (UINT4) RM_L2VPN_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_L2VPN_APP_ID;
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
            u4SrcEntId, u4DestEntId);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pRmMsg);
        return L2VPN_FAILURE;
    }



    return L2VPN_SUCCESS;
}

/*****************************************************************************
 *  
 * FUNCTION NAME      : L2VpnRmSetBulkUpdateStatus
 *   
 * DESCRIPTION        : This function sets LDP bulk update status in RM
 *      
 * INPUT              : NONE
 *        
 * OUTPUT             : NONE
 * 
 * RETURNS           : NONE
 * 
 * ****************************************************************************/


VOID L2VpnRmSetBulkUpdateStatus (VOID)
{

    RmSetBulkUpdatesStatus(RM_L2VPN_APP_ID);

}
