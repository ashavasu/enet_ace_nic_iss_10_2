/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsVplslw.h,v 1.1 2014/03/09 13:30:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for VplsBgpConfigTable. */
INT1
nmhValidateIndexInstanceVplsBgpConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsBgpConfigTable  */

INT1
nmhGetFirstIndexVplsBgpConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsBgpConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsBgpConfigVERangeSize ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsBgpConfigVERangeSize ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsBgpConfigVERangeSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsBgpConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VplsBgpVETable. */
INT1
nmhValidateIndexInstanceVplsBgpVETable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsBgpVETable  */

INT1
nmhGetFirstIndexVplsBgpVETable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsBgpVETable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsBgpVEName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsBgpVEPreference ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetVplsBgpVERowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetVplsBgpVEStorageType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsBgpVEName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVplsBgpVEPreference ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetVplsBgpVERowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetVplsBgpVEStorageType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsBgpVEName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VplsBgpVEPreference ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2VplsBgpVERowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2VplsBgpVEStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsBgpVETable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VplsBgpPwBindTable. */
INT1
nmhValidateIndexInstanceVplsBgpPwBindTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsBgpPwBindTable  */

INT1
nmhGetFirstIndexVplsBgpPwBindTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsBgpPwBindTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsBgpPwBindLocalVEId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetVplsBgpPwBindRemoteVEId ARG_LIST((UINT4  , UINT4 ,UINT4 *));
