/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpif.h,v 1.22 2017/06/08 11:40:30 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpif.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the interface structure type
 *                             definitions 
 *---------------------------------------------------------------------------*/
#ifndef _L2VPIF_H 
#define _L2VPIF_H

typedef struct _PwVcAdminEvtInfo
{
    UINT4                        u4EvtType;
    UINT4                        u4PwVcIndex;
}
tPwVcAdminEvtInfo;

/*MS-PW */
/*MS-PW Admin Event*/
typedef struct _MsPwAdminEvtInfo
{
    UINT4                        u4EvtType;
    UINT4                        u4PrimPwVcIndex;
    UINT4                        u4ScndPwVcIndex;
    UINT1                        u1Pad[4];
}
tMsPwAdminEvtInfo;

typedef struct _PwVcMplsTnlEntryIndex
{
    UINT4                       u4PwVcIndex;
    UINT4                       u4TnlInOutIndex;
}
tPwVcMplsTnlEntryIndex;


typedef struct _PwVcMplsAdminEvtInfo
{
    UINT4                        u4EvtType;
    union
    {
        tPwVcMplsTnlEntryIndex   TnlEntryIndex;
    }
    unEvtInfo;
}
tPwVcMplsAdminEvtInfo;

/* PSN type and the info related to the PSN */
typedef struct _PwVcPSNAdminEvtInfo
{
    UINT4                    u4PsnType;
    tPwVcMplsAdminEvtInfo    MplsAdminEvtInfo;
}
tPwVcPSNAdminEvtInfo;


typedef struct _PwVcEnetEntryIndex
{
    UINT4                        u4PwVcIndex;
    INT2                         i2PwVcEnetPwVlan;
    INT2                         i2Pad;
}
tPwVcEnetEntryIndex;


typedef struct _PwVcEnetAdminEvtInfo
{
    UINT4                        u4EvtType;
    tPwVcEnetEntryIndex          EnetEntryIndex;
}
tPwVcEnetAdminEvtInfo;



typedef struct _PwVcServSpecAdminEvtInfo
{
    UINT4                    u4ServSpecType;
    tPwVcEnetAdminEvtInfo    EnetAdminEvtInfo;
}
tPwVcServSpecAdminEvtInfo;


/* PW Redundancy */
typedef struct _L2vpnRedGroupAdminEvtInfo
{
    UINT4       u4EvtType;
    UINT4       u4GroupId;
}tL2vpnRedGroupAdminEvtInfo;

typedef struct _L2vpnRedNodeAdminEvtInfo
{
    uGenAddr    Addr;
    UINT4       u4EvtType;
    UINT4       u4GroupId;
    UINT1       u1AddrType;
    UINT1       au1Pad [3];
}tL2vpnRedNodeAdminEvtInfo;

typedef struct tL2vpnRedPwAdminEvtInfo
{
    UINT4       u4EvtType;
    UINT4       u4GroupId;
    UINT4       u4PwIndex;
}tL2vpnRedPwAdminEvtInfo;

#ifdef VPLSADS_WANTED
typedef struct tL2vpnVplsAdminEvtInfo
{
    UINT4         u4EvtType;
    UINT4         u4VplsIndex;
    UINT4         u4Mtu;
    UINT4         u4VeId;
    VOID          *pVplsLBInfoBase;
    UINT1         au1RouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];
    UINT1         au1RouteTarget[L2VPN_MAX_VPLS_RT_LEN];
    BOOL1         bControlWordFlag;
    UINT1         u1RTType;
    UINT1         au1pad[2];
}tL2vpnVplsAdminEvtInfo;
#endif

/* L2VPN module level admin events */
typedef struct _L2VpnAdminEvtInfo
{
    UINT4       u4EvtType; /* L2VPN pw/psn/MSPW/ss ADMIN EVT */
    UINT4       u4WaitForRespFlag;
    union
    {
      UINT4                       u4Param;
      tPwVcAdminEvtInfo           PwVcAdminEvtInfo;
      tMsPwAdminEvtInfo           MsPwAdminEvtInfo; /*MS-PW */
      tPwVcPSNAdminEvtInfo        PwVcPSNAdminEvtInfo;
      tPwVcServSpecAdminEvtInfo   PwVcServAdminEvtInfo;
      tL2vpnRedGroupAdminEvtInfo  RedGroupAdminEvtInfo;
      tL2vpnRedNodeAdminEvtInfo   RedNodeAdminEvtInfo;
      tL2vpnRedPwAdminEvtInfo     RedPwAdminEvtInfo;
#ifdef VPLSADS_WANTED
      tL2vpnVplsAdminEvtInfo      VplsAdminEvtInfo;
#endif
    }
    unEvtInfo;
}
tL2VpnAdminEvtInfo;

typedef struct _PwVcSigEvtInfo
{
    UINT4                        u4SigType;
    union
    {
        tL2VpnLdpPwVcEvtInfo     LdpEvtInfo;          
        /* tL2VpnL2tpPwVcEvtInfo L2tpEvtInfo*/ 
    }
    unEvtInfo;
}
tPwVcSigEvtInfo;


typedef struct _L2VpnMplsPwVcMplsTeEvtInfo
{
    tTeRouterId       PwVcMplsTnlLclLSR;
    tTeRouterId       PwVcMplsTnlPeerLSR;
    UINT4             u4PwVcMplsTnlIndex;
    UINT4             u4EvtType;
    UINT4             u4TnlXcIndex;
    UINT4             u4TnlIfIndex;
    /* Following three labels carry the information for FRR tnls
     * 1:1 - Bkp tnl label for link/node protection - u4TnlBkpMPLabel1 
     * N:1 - Bypass tnl label for link/node protection - u4TnlBkpMPLabel1 
     *       Prot tnl label for link/node protection - u4TnlBkpMPLabel2
     * LDP over RSVP
     *  1:1 - Bkp tnl label for link/node protection - u4TnlBkpMPLabel1
     *        LDP label - u4TnlBkpMPLabel2
     *  N:1 - Bypass tnl label for link/node protection - u4TnlBkpMPLabel1 
     *        Prot tnl label for link/node protection - u4TnlBkpMPLabel2
     *        LDP label - u4TnlBkpMPLabel3 
     * These labels are used during ILM entry deletion from hw */
    UINT4             u4TnlBkpMPLabel1; 
    UINT4             u4TnlBkpMPLabel2; 
    UINT4             u4TnlBkpMPLabel3;
    UINT1             au1NextHopMac[MAC_ADDR_LEN];
    UINT2             u2PwVcMplsTnlInstance;
    UINT2             u2PwVcMplsBkpTnlInstance;  /*Tunnel Backup Protection Tunnel Instance*/
    UINT1             u1TnlRole;
    UINT1             u1DeleteTnlAction;
    UINT1             u1TnlSwitchingType;
    UINT1             au1Pad[3];
}
tL2VpnMplsPwVcMplsTeEvtInfo; /* To be made visible to other modules */


typedef struct _L2VpnMplsPwVcMplsNonTeEvtInfo
{
   UINT4              u4EvtType;
   UINT4              u4PwVcMplsOutLsrXcIndex;
   UINT4              u4TnlBkpMPLabel1;
   UINT4              u4TnlBkpMPLabel2;
   UINT4              u4TnlBkpMPLabel3;
   uGenAddr           PeerOrSrcAddr;
   UINT1              u1AddrType;
   UINT1              au1pad[3];
}
tL2VpnMplsPwVcMplsNonTeEvtInfo; /* To be made visible to other modules */

typedef struct _L2VpnMplsPwVcMplsVcEvtInfo
{
   UINT4              u4EvtType;
   UINT4              u4PwVcMplsOutIfIndex;
}
tL2VpnMplsPwVcMplsVcEvtInfo; /* To be made visible to other modules */


typedef struct _L2VpnMplsPwVcPSNEvtInfo
{
    UINT4                        u4MplsType;
    union
    {
        tL2VpnMplsPwVcMplsTeEvtInfo     MplsTeEvtInfo;
        tL2VpnMplsPwVcMplsNonTeEvtInfo  MplsNonTeEvtInfo;
        tL2VpnMplsPwVcMplsVcEvtInfo     MplsVcEvtInfo; 
    }
    unEvtInfo;
}
tL2VpnMplsPwVcPSNEvtInfo; /* To be made visible to other modules */ 


typedef struct _PwVcPSNEvtInfo
{
    UINT4                        u4PsnType;
    union
    {
        tL2VpnMplsPwVcPSNEvtInfo L2VpnMplsPwVcPSNEvtInfo;
        /*tL2VpnL2tpPsnEvtInfo   L2VpnL2tpPwVcPSNEvtInfo*/ 
    }
    unEvtInfo;
}
tPwVcPSNEvtInfo;


typedef struct _PwVcFwdPlaneEvtInfo
{
   union {
      tPwVcMplsTnlEntryIndex PsnSpecIndex;
      tPwVcEnetEntryIndex    ServSpecIndex;
   } unEvtInfo;

   UINT4                  u4EvtType;
   UINT4                  u4PwVcID;
   INT1                   i1PwVcType;
   INT1                   i1Pad;
   INT2                   i2Pad;
}
tPwVcFwdPlaneEvtInfo;


typedef struct _L2VpnIfEvtInfo
{
    UINT4                   u4EvtType;
    UINT4                   u4IfIndex;
    UINT2                   u2PortVlan;
    UINT2                   u2Rsvd;
}
tL2VpnIfEvtInfo;

typedef struct _L2VpnRtEvtInfo
{
    uGenU4Addr          DestAddr;      /* Destination Network Address */
    uGenU4Addr          DestMask;      /* Destination Network Mask. */
    uGenU4Addr          NextHop;       /* Nexthop address */
    UINT4               u4RtIfIndx;      /* NetIP interface through which the route is learnt */
    UINT4               u4RtNxtHopAS;    /* Autonomous system number of next hop */
    INT4                i4Metric1;       /* Reachability cost for the destination */
    UINT1               u1RowStatus;     /* Status of the row */
    UINT1               u1CmdType;
    UINT1  u1AddrType;
    UINT1               u2Rsvd;
}
tL2VpnRtEvtInfo;

typedef struct _L2VpnOamInfo
{
    /* MPLS-TP OAM specific information */
    UINT4 u4MegIndex; /* Maintenance Entity Group index */
    UINT4 u4MeIndex; /* Maintenance Entity index */
    UINT4 u4MpIndex; /* Maintenance Point index */ 
    UINT4 u4ContextId;
    UINT4 u4PwIndex;
    UINT1 u1PathStatus; /* Path (TNL/PW) status - UP/DOWN */
    UINT1 u1CmdType;
    UINT1 au1Pad[2]; /* Padding */
}tL2VpnOamInfo;


typedef struct _L2VpnPwRedAcStatusInfo
{
    UINT2   au2DlagPort[8];
    UINT1   u1AcStatus;
    UINT1   u1DlagPortCnt;
    UINT2   u2Pad;
} tL2VpnPwRedAcStatusInfo;

typedef struct _L2VpnRmMsg
{
    tRmMsg         *pBuff;     /* Message given by RM module. */
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved; /* Added for alignment purpose */
}tL2VpnRmMsg;



typedef union _L2VpnEvtInfo
{
   tL2VpnAdminEvtInfo          L2VpnAdminEvtInfo;               
   tL2VpnIfEvtInfo             L2VpnIfEvtInfo;
   tPwVcSigEvtInfo             PwVcSigEvtInfo;
   tPwVcPSNEvtInfo             PwVcPSNEvtInfo;
   tPwVcFwdPlaneEvtInfo        PwVcFwdEvtInfo;
   tL2VpnRtEvtInfo             PwRtEvtInfo;
   tL2VpnOamInfo               PwOamInfo;
   tL2VpnPwRedAcStatusInfo     PwRedAcInfo;
#ifdef VPLSADS_WANTED
   tL2VpnBgpEvtInfo            L2VpnBgpEvtInfo;
#endif
   tL2VpnRmMsg                 L2VpnRmEvtInfo;
}
tuL2VpnEvtInfo;


typedef struct _L2VpnQMsg
{
   UINT4             u4MsgType;
   tuL2VpnEvtInfo    L2VpnEvtInfo;
}
tL2VpnQMsg;

extern VOID
RpteL2VPNEventHandler (tTeTnlInfo * pTeTnlInfo);

/* 
typedef struct _PwRedTest
{
    UINT4 u4PeerAddr;
    UINT1 u1SimulateFailure;
    UINT1 au1Pad [3];
}tL2vpnPwRedTest; 
extern tL2vpnPwRedTest     gL2vpnPwRedTest; */
#endif /*_L2VPIF_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file l2vpif.h                               */
/*---------------------------------------------------------------------------*/
