
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vprm.h,v 1.2 2014/11/28 02:13:12 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/



VOID L2VpnProcessRmEvent(tL2VpnRmMsg  *pRmL2VpnMsg);
VOID L2VpnRmApiSendProtoAckToRM(UINT4 u4SeqNum);
tRmMsg* L2VpnRmAllocForRmMsg(UINT2 u2Size);
INT4 L2VpnRmSendMsgToRm(tRmMsg * pRmMsg, UINT2 u2DataLen);
INT4  L2VpnRmSendEventToRm(tRmProtoEvt * pEvt);
UINT1 L2VpnRmGetStandbyNodeCount (VOID);
INT4 L2VpnRmRelRmMsgMem (UINT1 *pu1Block);
UINT4 L2VpnRmGetRmNodeState (VOID);
VOID L2VpnRmSendBulkAbort(UINT4 u4ErrCode);
VOID L2VpnRmSetBulkUpdateStatus (VOID);
INT4 L2VpnRmEventHandler(UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
INT4 L2VpnRmRegisterWithRM (VOID);
INT4 L2VpnRmDeRegisterWithRM (VOID);
INT4 L2VpnRmProcessSyncHwListEntry (tRmMsg * pRmMesg, UINT4 *pu4Offset);
INT4 L2VpnRmProcessSyncVplsHwListEntry (tRmMsg * pRmMesg, UINT4 *pu4Offset);
VOID L2VpnRmProcessBulkUpdateMsg (tRmMsg * pMsg, UINT4 *pu4Offset);
VOID L2VpnRmSendBulkPwVcInfo(VOID);
VOID L2VpnRmInit (VOID);

#define L2VPN_RM_GET_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define L2VPN_RM_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define L2VPN_RM_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}


#define L2VPN_RM_PUT_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define L2VPN_RM_PUT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define L2VPN_RM_SYN_MAX_SIZE           1500
#define L2VPN_RM_MSG_HDR_SIZE           3
#define L2VPN_RM_BULK_TAIL_MSG_LEN      3
#define L2VPN_RM_BULK_LEN_OFFSET        1
#define L2VPN_RM_TRUE                   1
#define L2VPN_RM_FALSE                  2
#define L2VPN_RM_GBL_MOD                1
#define L2VPN_RM_MOD_COMPLETED          2
#define ADD_UPDATE_PW                   1
#define ADD_PW                         2
#define DEL_PW                          3
#define ADD_UPDATE_VPLS     1
#define ADD_VPLS      2
#define DEL_VPLS      3
#define L2VPN_RM_BULK_UPD_REQ_MSG_LEN   3

#define L2VPN_IS_STANDBY_UP() \
    ((gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState == L2VPN_RM_ACTIVE_STANDBY_UP) \
     ? L2VPN_RM_TRUE : L2VPN_RM_FALSE)



enum {
    L2VPN_RM_BULK_UPDT_REQ_MSG = RM_BULK_UPDT_REQ_MSG,

    L2VPN_RM_BULK_UPDATE_MSG = RM_BULK_UPDATE_MSG,

    L2VPN_RM_BULK_UPDT_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG,
    L2VPN_RM_SYNC_HWLIST_MSG   = 4,
 L2VPN_RM_SYNC_VPLS_HWLIST_MSG
};

#define L2VPN_IS_NODE_ACTIVE()\
    ((gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState == L2VPN_RM_ACTIVE_STANDBY_UP || \
      gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState == L2VPN_RM_ACTIVE_STANDBY_DOWN) \
     ? L2VPN_RM_TRUE : L2VPN_RM_FALSE)


#define BUF_COPY_OVER_CHAIN(pMsgBuf, pu1Data, u4Offset, u4Size) \
    CRU_BUF_Copy_OverBufChain(pMsgBuf, (UINT1 *) pu1Data,u4Offset,u4Size)

#define BUF_COPY_FROM_CHAIN(pMsgBuf, pu1Data, u4Offset, u4Size) \
    CRU_BUF_Copy_FromBufChain(pMsgBuf, (UINT1 *) pu1Data,u4Offset,u4Size)





