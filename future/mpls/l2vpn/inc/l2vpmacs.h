
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2vpmacs.h,v 1.56 2017/06/08 11:40:30 siva Exp $
 * 
 * Description: Contains macros used throughout L2VPN module
 *    
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : l2vpmacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros used throughout L2VPN module
 *---------------------------------------------------------------------------*/

#ifndef _L2VP_MACS_H
#define _L2VP_MACS_H

#define MPLS_PWVC_FROM_INLIST(x) (tPwVcEntry *)(VOID *)((UINT1 *)(x) - MPLS_OFFSET(tPwVcEntry,PwInNode))
#define MPLS_PWVC_FROM_OUTLIST(x) (tPwVcEntry *)(VOID *)((UINT1 *)(x) - MPLS_OFFSET(tPwVcEntry,PwOutNode))
#define MPLS_PWVC_FROM_ENETLIST(x) (tPwVcEnetEntry *)(VOID *)((UINT1 *)(x) - MPLS_OFFSET(tPwVcEnetEntry,EnetNode))
#define MPLS_LBL_INFO_FROM_TIMER(x) (tPwVcLblMsgEntry *)(VOID *)((UINT1 *)(x) - \
                       MPLS_OFFSET(tPwVcLblMsgEntry,CleanupTimer))

/* Macro to suppress warnings */
#define L2VPN_SUPPRESS_WARNING(x) UNUSED_PARAM(x)

/* Useful Macros */
#define L1VPN_MIN(a,b) \
           (((a) < (b)) ? (a) : (b))

#define L2VPN_MAX(a, b) \
           (((a) > (b)) ? (a) : (b))

#define L2VPN_CEIL(x)           (ceil ((double)x))

#define UINT1_MAX_VALUE      0xFF
#define UINT2_MAX_VALUE      0xFFFF
#define UINT4_MAX_VALUE      0xFFFFFFFF
#define MAX_DIGITS_IN_UINT2  5
#define MAX_DIGITS_IN_UINT4  10

    /* Counter64 operations */
#define INIT_COUNTER_64(x)   x=0;
#define INC_COUNTER_64(x)    x++;
#define DEC_COUNTER_64(x)    x--;

/* PW Redundancy */

#define L2VPN_PWVC_STATUS_IS_AC_UP(u4Status)        (((u4Status) & L2VPN_PWVC_STATUS_AC_UP_MASK) == 0)
#define L2VPN_PWVC_STATUS_IS_PSN_UP(u4Status)       (((u4Status) & L2VPN_PWVC_STATUS_PSN_UP_MASK) == 0)
#define L2VPN_PWVC_STATUS_IS_UP(u4Status)           (((u4Status) & L2VPN_PWVC_STATUS_UP_MASK) == 0)
#define L2VPN_PWVC_STATUS_IS_ACTIVE(u4Status)       (((u4Status) & L2VPN_PWVC_STATUS_STANDBY) == 0)
#define L2VPN_PWVC_STATUS_IS_SWITCHOVER(u4Status)   (((u4Status) & L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST) != 0)
#define L2VPN_PWVC_STATUS_RED_STATUS(u4Status)      ((u4Status) & L2VPN_PWVC_STATUS_RED_MASK)
#define L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC(u4LocalStatus, u4RemoteStatus) \
(\
    (L2VPN_PWVC_STATUS_IS_ACTIVE (u4LocalStatus) &&\
     L2VPN_PWVC_STATUS_IS_ACTIVE (u4RemoteStatus)) ||\
    (!L2VPN_PWVC_STATUS_IS_ACTIVE (u4LocalStatus) &&\
     !L2VPN_PWVC_STATUS_IS_ACTIVE (u4RemoteStatus))\
)
#define L2VPN_PWVC_STATUS_IS_FORWARDING(u4LocalStatus, u4RemoteStatus) \
(\
    L2VPN_PWVC_STATUS_IS_UP (u4LocalStatus) &&\
    L2VPN_PWVC_STATUS_IS_UP (u4RemoteStatus) &&\
    L2VPN_PWVC_STATUS_IS_ACTIVE (u4LocalStatus) &&\
    L2VPN_PWVC_STATUS_IS_FORWARDING_SYNC (u4LocalStatus, u4RemoteStatus)\
)


#define L2VPNRED_PW_PRIORITY(u1PwPriority, u1RgPreference) \
(\
    (u1PwPriority) |\
    ((u1RgPreference) == L2VPNRED_PW_PREFERENCE_SECONDARY ? \
        L2VPNRED_PW_PRIORITY_SECONDARY_MASK: L2VPNRED_PW_PRIORITY_MASK)\
)

#define L2VPNRED_ICCP_PW_PARENT_PW(pIccpPw)         ((tL2vpnRedundancyPwEntry*) (pIccpPw)->pRgParent)
#define L2VPNRED_ICCP_PW_PARENT_NODE(pIccpPw)       ((tL2vpnRedundancyNodeEntry*) (pIccpPw)->pRgParent)

#define L2VPN_PWRED_STATUS                          gL2VpnGlobalInfo.u1PwRedStatus


/* Macro for verification of PSN_FACE_TX_FAULT. */
#define L2VPN_IS_PSN_FACE_TX_FAULT(x) \
    (((L2VPN_PWVC_LOCAL_STATUS (x) & \
         L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT) == \
        L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT) ? L2VPN_TRUE : L2VPN_FALSE)

/* Macro for verification of PSN_FACE_RX_FAULT. */
#define L2VPN_IS_PSN_FACE_RX_FAULT(x) \
    (((L2VPN_PWVC_LOCAL_STATUS (x) & \
         L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT) == \
        L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT) ? L2VPN_TRUE : L2VPN_FALSE)

#define L2VPN_IS_STATIC_PW(x) \
     ((x->bIsStaticPw == TRUE) ? TRUE : FALSE)

#define L2VPN_IS_PW_GEN_FEC_TYPE1(x) \
        (((L2VPN_PWVC_OWNER(x) == L2VPN_PWVC_OWNER_GEN_FEC_SIG)&& \
        (L2VPN_PWVC_GEN_LCL_AII_TYPE(x) == L2VPN_GEN_FEC_AII_TYPE_1)&& \
        (L2VPN_PWVC_GEN_REMOTE_AII_TYPE(x) == L2VPN_GEN_FEC_AII_TYPE_1)) \
         ? L2VPN_TRUE : L2VPN_FALSE)

/* L2Vpn PwVc PoolIds */
#define L2VPN_PWVC_POOL_ID            L2VPNMemPoolIds[MAX_L2VPN_PW_VC_ENTRIES_SIZING_ID]
#define L2VPN_DORMANT_VCS_POOL_ID     L2VPNMemPoolIds[MAX_L2VPN_PW_VC_DORMANT_ENTRIES_SIZING_ID] 
#define L2VPN_LBL_MSG_POOL_ID         L2VPNMemPoolIds[MAX_L2VPN_PW_VC_LBLMSG_ENTRIES_SIZING_ID]
#define L2VPN_PEER_SSN_POOL_ID        L2VPNMemPoolIds[MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY_SIZING_ID]
#define L2VPN_PW_INFOTAB_POOL_ID      L2VPNMemPoolIds[MAX_L2VPN_PW_VC_INFO_SIZING_ID]


/* L2Vpn PwVc PSN related PoolIds */
#define L2VPN_MPLS_ENTRY_POOL_ID      L2VPNMemPoolIds[MAX_L2VPN_PW_VC_MPLS_ENTRY_SIZING_ID]
#define L2VPN_MPLS_MAP_POOL_ID        L2VPNMemPoolIds[MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES_SIZING_ID]
#define L2VPN_MPLS_IN_MAP_POOL_ID     L2VPNMemPoolIds[MAX_L2VPN_MPLS_INMAPPING_ENTRIES_SIZING_ID]
/* L2Vpn PwVc SS related PoolIds */
#define L2VPN_SERV_SPEC_POOL_ID       L2VPNMemPoolIds[MAX_L2VPN_ENET_SERV_SPEC_ENTRY_SIZING_ID]
#define L2VPN_ENET_POOL_ID            L2VPNMemPoolIds[MAX_L2VPN_ENET_ENTRIES_SIZING_ID]
#define L2VPN_MPLS_PRIMAP_POOL_ID      L2VPNMemPoolIds[MAX_L2VPN_ENET_PRIMAPPING_ENTRIES_SIZING_ID]

/* PW Redundancy */
#define L2VPN_REDUNDANCY_GROUP_POOL_ID          L2VPNMemPoolIds[MAX_L2VPN_REDUNDANCY_SIZING_ID]
#define L2VPN_REDUNDANCY_NODE_POOL_ID           L2VPNMemPoolIds[MAX_L2VPN_REDUNDANCY_NODE_SIZING_ID]
#define L2VPN_REDUNDANCY_PW_POOL_ID             L2VPNMemPoolIds[MAX_L2VPN_REDUNDANCY_PW_SIZING_ID]
#define L2VPN_ICCP_PW_POOL_ID                   L2VPNMemPoolIds[MAX_L2VPN_ICCP_PW_SIZING_ID]
#define L2VPN_ICCP_PWVC_REQUESTS_POOL_ID        L2VPNMemPoolIds[MAX_L2VPN_ICCP_PWVC_REQUESTS_SIZING_ID]
#define L2VPN_ICCP_PWVC_DATA_ENTRIES_POOL_ID    L2VPNMemPoolIds[MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES_SIZING_ID]

/* Pw Redundancy */

/*MS-PW*/
#define L2VPN_MSPW_POOL_ID    L2VPNMemPoolIds[MAX_L2VPN_PW_VC_ENTRIES_SIZING_ID]

/*CFA PW INDEX MAPPING*/
#define L2VPN_PW_IFINDEX_POOL_ID     L2VPNMemPoolIds[MAX_L2VPN_IFINDEX_ENTRIES_SIZING_ID]

/*MPLS Port Table PoolIds*/
#define L2VPN_PORT_ENTRY_INFO_POOL_ID  L2VPNMemPoolIds[MAX_L2VPN_MPLS_PORT_ENTRY_INFO_SIZING_ID]

/* L2Vpn system Parameters */
#define L2VPN_TSK_ID                  (gL2VpnGlobalInfo.TaskId)
#define L2VPN_SYSLOG_ID               (gL2VpnGlobalInfo.i4SysLogId)
#define L2VPN_SEM_ID                  (gL2VpnGlobalInfo.SemId)
#define L2VPN_QID                     (gL2VpnGlobalInfo.QId)
#define L2VPN_RES_QID                 (gL2VpnGlobalInfo.ResQId)
#define L2VPN_TIMER_LIST_ID           (gL2VpnGlobalInfo.TimerListId)
#define L2VPN_Q_POOL_ID               L2VPNMemPoolIds[MAX_L2VPN_Q_MSG_SIZING_ID]
#define L2VPN_IF_DESC_POOL_ID         L2VPNMemPoolIds[MAX_L2VPN_IF_DESCRIPTOR_SIZING_ID]
#define L2VPN_ADMIN_STATUS            (gL2VpnGlobalInfo.i4AdminStatus)
#define L2VPN_INITIALISED             (gL2VpnGlobalInfo.u1L2VpnInitialised)
#define L2VPN_MPLS_API_INPUT_INFO_POOL_ID   L2VPNMemPoolIds[MAX_L2VPN_MPLS_API_IN_INFO_ENTRIES_SIZING_ID]
#define L2VPN_MPLS_API_OUTPUT_INFO_POOL_ID  L2VPNMemPoolIds[MAX_L2VPN_MPLS_API_OUT_INFO_ENTRIES_SIZING_ID]
#define L2VPN_VPLS_NOTIF_ENABLE_STATUS     (gL2VpnGlobalInfo.i4VplsStatusNotifEnable)
#define L2VPN_VPLS_NOTIF_MAX_RATE          (gL2VpnGlobalInfo.u4VplsNotificationMaxRate )
/* L2Vpn Global PwVc Parameters */
#define L2VPN_DORMANT_PWVC_LIST(x)    &((x)->DormantVcsList)
#define L2VPN_PEND_MSG_TO_LDP_LIST(x) &((x)->PendMsgToLdpList)
#define L2VPN_PWVC_INTERFACE_HASH_PTR(x) ((x)->pPwIfHashPtr)
#define L2VPN_PWVC_FEC128_RB_PTR(x)  ((x)->pPwVcFec128RBTree)
#define L2VPN_PWVC_FEC129_RB_PTR(x)  ((x)->pPwVcFec129RBTree)
#define L2VPN_PWVC_FEC129_VPLS_RB_PTR(x) ((x)->pPwVcFec129VplsRBTree)
#ifdef HVPLS_WANTED
#define L2VPN_VPLS_PW_BIND_RB_PTR(x)  ((x)->pVplsPwBindRBTree)
#endif
#define L2VPN_PWVC_PEERADDR_RB_PTR(x)((x)->pPwVcPeerAddrRBTree)
#define L2VPN_VPLS_VFI_RB_PTR(x)       ((x)->pVplsVFIRBTree)
#ifdef VPLSADS_WANTED
#define L2VPN_VPLS_RD_RB_PTR(x)       ((x)->pVplsRDRBTree)
#define L2VPN_VPLS_RT_RB_PTR(x)       ((x)->pVplsRTRBTree)
#define L2VPN_VPLS_VE_RB_PTR(x)       ((x)->pVplsVERBTree)
#define L2VPN_VPLS_AC_RB_PTR(x)       ((x)->pVplsACRBTree)
#endif
#define L2VPN_PWVC_INFO_TABLE_PTR(x) ((tPwVcInfo *)(&gPwVcGlobalInfo.ppPwVcInfoTable[0])+(x-1))
#define L2VPN_PWVC_PERF_CURRENT_INTERVAL (gPwVcGlobalInfo.i1PwVcPerfCurInterval)
#define L2VPN_PWVC_INLABEL_RB_PTR(x)  ((x)->pPwVcInLabelRBTree)
#define L2VPN_PWVC_OUTLABEL_RB_TREE       gPwVcGlobalInfo.PwVcOutLabelRBTree
#define L2VPN_MPLS_PWID_TABLE            gPwVcGlobalInfo.PwIdTable
#define L2VPN_MPLS_PWIFINDEX_TABLE       gPwVcGlobalInfo.PwIfIndexTable 

/*Error Counter */
#define L2VPN_PWVC_PERF_TOTAL_ERROR_PKTS(x) ((x)->u4PwVcPerfTotalErrorPkts)

#define L2VPN_MAX_VPLSINDEX_ENTRIES (x.u4MaxVplsEntries)
/* L2Vpn PwVc Config Parameters */
#define L2VPN_MAX_PWVC_ENTRIES(x)     ((x)->PwVcCfgParams.u4MaxPwVcEntries)
#define L2VPN_MAX_DORMANT_PWVC_ENTRIES(x)     ((x)->PwVcCfgParams.u4MaxDormantPwVcEntries)
#define L2VPN_MAX_PWVC_CLEANUP_INT(x) ((x)->PwVcCfgParams.u4MaxCleanupInterval)
#define L2VPN_MAX_WAITING_PEER_VCS(x) ((x)->PwVcCfgParams.u4MaxWaitingPeerPwVcEntries)
#define L2VPN_MAX_PEER_SSN(x) ((x)->PwVcCfgParams.u4MaxPeerSessionEntries)

#define L2VPN_NO_OF_PWVC_ENTRIES_DESTROYED(x)\
       ((x)->PwVcGlobalStats.u4NoOfPwVcEntriesDestroyed)

#ifdef VPLS_GR_WANTED
#define L2VPN_NO_OF_BGP_PW_STALE_ENTRIES(x)\
       ((x)->PwVcGlobalStats.u4NoOfBgpPwStaleEntries)
#endif

#define L2VPN_NO_OF_PWVC_ENTRIES_CREATED(x)\
       ((x)->PwVcGlobalStats.u4NoOfPwVcEntriesCreated)

/* L2Vpn PwVc MPLS PSN Config Parameters */
#define L2VPN_MAX_MPLS_ENTRIES(x)\
       ((x)->PwVcMplsCfgParams.u4MaxMplsEntries)

#define L2VPN_MAX_MPLS_TNL_ENTRIES(x)\
       ((x)->PwVcMplsCfgParams.u4MaxMplsTnlEntries)

#define L2VPN_MAX_MPLS_MAP_ENTRIES(x)\
       ((x)->PwVcMplsCfgParams.u4MaxMplsMapEntries)

#define L2VPN_CFG_MAX_MPLS_IN_OUT_ENTRIES(x)\
       ((x)->PwVcMplsCfgParams.u4MaxMplsInOutEntries)

/* L2Vpn PwVc SS Config Parameters */
#define L2VPN_MAX_ENET_SERV_ENTRIES(x)\
      ((x)->PwVcEnetCfgParams.u4MaxServSpecEntries)

#define L2VPN_MAX_ENET_ENTRIES(x)\
      ((x)->PwVcEnetCfgParams.u4MaxEnetEntriesPerPw)

#define L2VPN_MAX_ENET_MPLS_PRIMAP_ENTRIES(x)\
      ((x)->PwVcEnetCfgParams.u4MaxEnetMplsPriMapEntries)

/* L2Vpn PwVc Stats Parameters */
#define L2VPN_ACTIVE_PWVC_ENTRIES(x)\
       ((x)->PwVcGlobalStats.u4ActivePwVcEntries)

#define L2VPN_ACTIVE_PWVC_MPLS_ENTRIES(x,y) \
       ((x)->PwVcGlobalStats.u4ActivePwVcEntriesPerPSN[y])

#define L2VPN_ACTIVE_PWVC_ENET_ENTRIES(x,y) \
       ((x)->PwVcGlobalStats.u4ActivePwVcEntriesPerVCType[y])
   /* Pw - MIB Related */
   /*Pw Vc Entry Table*/

#define L2VPN_PWVC_OFFSET_HTBL(HashNode) \
         ((tPwVcEntry *)((UINT1*)HashNode - sizeof (tTMO_HASH_NODE)))

/* Pw Table */

#define L2VPN_PWVC_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4PwVcIndex)

#define L2VPN_PWVC_ARP_RESOLVE_WAITSTATUS(pPwVcEntry)\
((pPwVcEntry)->u1ArpResolveStatus)

#define L2VPN_PWVC_ROUTE_STATUS(pPwVcEntry)\
((pPwVcEntry)->u1PwRouteStatus)

#define L2VPN_PWVC_TYPE(pPwVcEntry)\
((pPwVcEntry)->i1PwVcType)

#define L2VPN_PWVC_OAM_ENABLE(pPwVcEntry)\
((pPwVcEntry)->bPwIntOamEnable)

#define L2VPN_PWVC_GEN_AGI_TYPE(pPwVcEntry)\
((pPwVcEntry)->u4GenAgiType)

#define L2VPN_PWVC_GEN_LCL_AII_TYPE(pPwVcEntry)\
((pPwVcEntry)->u4LocalAiiType)

#define L2VPN_PWVC_GEN_REMOTE_AII_TYPE(pPwVcEntry)\
((pPwVcEntry)->u4RemoteAiiType)

#define L2VPN_PWVC_OAM_SESSION_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4ProactiveSessionIndex)

#define L2VPN_PWVC_LCL_CC_ADVERT(pPwVcEntry)\
((pPwVcEntry)->u1LocalCcAdvert)

#define L2VPN_PWVC_LCL_CV_ADVERT(pPwVcEntry)\
((pPwVcEntry)->u1LocalCvAdvert)

#define L2VPN_PWVC_LCL_CC_SELECTED(pPwVcEntry)\
((pPwVcEntry)->u1LocalCcSelected)

#define L2VPN_PWVC_LCL_CV_SELECTED(pPwVcEntry)\
((pPwVcEntry)->u1LocalCvSelected)

#define L2VPN_PWVC_MODE(pPwVcEntry)\
((pPwVcEntry)->u1PwVcMode)

#define L2VPN_PWVC_VPLS_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4VplsInstance)

#define L2VPN_PWVC_OWNER(pPwVcEntry)\
((pPwVcEntry)->i1PwVcOwner)

#define L2VPN_PWVC_PSN_ENTRY(pPwVcEntry)\
((pPwVcEntry)->pPSNEntry)

#define L2VPN_PWVC_ENET_ENTRY(pPwVcEntry)\
((pPwVcEntry)->pServSpecEntry)

#define L2VPN_PWVC_PSN_TYPE(pPwVcEntry)\
((pPwVcEntry)->i1PwVcPsnType)

#define L2VPN_PWVC_SETUP_PRIORITY(pPwVcEntry)\
((pPwVcEntry)->i1SetUpPriority)

#define L2VPN_PWVC_HOLDING_PRIORITY(pPwVcEntry)\
((pPwVcEntry)->i1HoldingPriority)

#define L2VPN_PWVC_PEER_ADDR(pPwVcEntry)\
((pPwVcEntry)->PeerAddr)

#define L2VPN_PWVC_PEER_ADDR_TYPE(pPwVcEntry)\
((pPwVcEntry)->i4PeerAddrType)

#define L2VPN_PWVC_ATTACHED_PW_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4AttachedPwIndex)

#define L2VPN_PWVC_IF_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4PwIfIndex)

#define L2VPN_PWVC_ID(pPwVcEntry)\
((pPwVcEntry)->u4PwVcID)

#define L2VPN_PWVC_LOCAL_GRP_ID(pPwVcEntry)\
((pPwVcEntry)->u4LocalGroupID)

#define L2VPN_PWVC_AGI(pPwVcEntry)\
((pPwVcEntry)->au1Agi)

#define L2VPN_PWVC_AGI_LEN(pPwVcEntry)\
((pPwVcEntry)->u1AgiLen)

#define L2VPN_PWVC_SAII(pPwVcEntry)\
((pPwVcEntry)->au1Saii)

#define L2VPN_PWVC_SAII_LEN(pPwVcEntry)\
((pPwVcEntry)->u1SaiiLen)

#define L2VPN_PWVC_TAII(pPwVcEntry)\
((pPwVcEntry)->au1Taii)

#define L2VPN_PWVC_TAII_LEN(pPwVcEntry)\
((pPwVcEntry)->u1TaiiLen)

#define L2VPN_PWVC_CNTRL_WORD(pPwVcEntry)\
((pPwVcEntry)->i1ControlWord)

#define L2VPN_PWVC_LOCAL_IFACE_MTU(pPwVcEntry)\
((pPwVcEntry)->u2LocalIfMtu)

#define L2VPN_PWVC_LOCAL_IFACE_STRING(pPwVcEntry)\
((pPwVcEntry)->i1LocalIfString)

#define L2VPN_PWVC_LCL_CAP_ADVT(pPwVcEntry)\
((pPwVcEntry)->u1LocalCapabAdvert)

#define L2VPN_PWVC_REMOTE_CC_ADVERT(pPwVcEntry)\
((pPwVcEntry)->u1RemoteCcAdvert)

#define L2VPN_PWVC_REMOTE_CV_ADVERT(pPwVcEntry)\
((pPwVcEntry)->u1RemoteCvAdvert)

#define L2VPN_PWVC_REMOTE_GRP_ID(pPwVcEntry)\
((pPwVcEntry)->u4RemoteGroupID)

#define L2VPN_PWVC_REQ_VLAN_ID(pPwVcEntry)\
((pPwVcEntry)->u2ReqVlanId)

#define L2VPN_PWVC_CW_STATUS(pPwVcEntry)\
((pPwVcEntry)->i1CwStatus)

#define L2VPN_PWVC_REMOTE_IFACE_MTU(pPwVcEntry)\
((pPwVcEntry)->u2RemoteIfMtu)

#define L2VPN_PWVC_REMOTE_IFACE_STRING(pPwVcEntry,x)\
((pPwVcEntry)->au1RemoteIfString(x))

#define L2VPN_PWVC_RMT_CAP(pPwVcEntry)\
((pPwVcEntry)->u1RemoteCapabilities)

#define L2VPN_PWVC_FRAG_CFG_SIZE(pPwVcEntry)\
((pPwVcEntry)->u4FragmentCfgSize)

#define L2VPN_PWVC_RMT_FRAG_CAPABLE(pPwVcEntry)\
((pPwVcEntry)->u1RmtFragCapability)

#define L2VPN_PWVC_FCS_RETN_CFG(pPwVcEntry)\
((pPwVcEntry)->i1FcsRetentionCfg)

#define L2VPN_PWVC_FCS_RETN_STATUS(pPwVcEntry)\
((pPwVcEntry)->u1FcsRetentionStatus)

#define L2VPN_PWVC_OUTBOUND_VC_LABEL(pPwVcEntry)\
((pPwVcEntry)->u4OutVcLabel)

#define L2VPN_PWVC_INBOUND_VC_LABEL(pPwVcEntry)\
((pPwVcEntry)->u4InVcLabel)

#define L2VPN_PWVC_PREV_LABEL(pPwVcEntry)\
((pPwVcEntry)->u4PrevInVcLabel)
   
#define L2VPN_PWVC_LABEL_REQID(pPwVcEntry)\
((pPwVcEntry)->u4PrevInVcLabel)

#define L2VPN_PWVC_OUTER_IN_LABEL(pPwVcEntry)\
((pPwVcEntry)->u4InOuterLabel)

#define L2VPN_PWVC_OUTER_OUT_LABEL(pPwVcEntry)\
((pPwVcEntry)->u4OutOuterLabel)

#define L2VPN_PWVC_IN_IF_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4InIfIndex)

#define L2VPN_PWVC_NAME(pPwVcEntry,x)\
((pPwVcEntry)->au1PwVcName[x])

#define L2VPN_PWVC_DESCR(pPwVcEntry,x)\
((pPwVcEntry)->au1PwVcDescr[x])

#define L2VPN_PWVC_CREATE_TIME(pPwVcEntry)\
((pPwVcEntry)->u4CreateTime)

#define L2VPN_PWVC_UP_TIME(pPwVcEntry)\
((pPwVcEntry)->u4UpTime)

#define L2VPN_PWVC_LAST_CHANGE(pPwVcEntry)\
((pPwVcEntry)->u4LastChange)

#define L2VPN_PWVC_ADMIN_STATUS(pPwVcEntry)\
((pPwVcEntry)->i1AdminStatus)

#define L2VPN_PWVC_OPER_STATUS(pPwVcEntry)\
((pPwVcEntry)->i1OperStatus)

#define L2VPN_PWVC_LOCAL_STATUS(pPwVcEntry)\
((pPwVcEntry)->u1LocalStatus)

#define L2VPN_PWVC_RMT_STAT_CAP(pPwVcEntry)\
((pPwVcEntry)->i1RemoteStatusCapable)

#define L2VPN_PWVC_REMOTE_STATUS(pPwVcEntry)\
((pPwVcEntry)->u1RemoteStatus)

#define L2VPN_PWVC_TIME_ELAPSED(pPwVcEntry)\
((pPwVcEntry)->i4TimeElapsed)

#define L2VPN_PWVC_VALID_INTERVALS(pPwVcEntry)\
((pPwVcEntry)->i1ValidIntervals)

#define L2VPN_PWVC_ROW_STATUS(pPwVcEntry)\
((pPwVcEntry)->i1RowStatus)

#define L2VPN_PWVC_STORAGE_TYPE(pPwVcEntry)\
((pPwVcEntry)->i1StorageType)

#define L2VPN_PWVC_HARDWARE_STATUS(pPwVcEntry)\
((pPwVcEntry)->u1HwStatus)

#define L2VPN_PWVC_LOCAL_VE_ID(pPwVcEntry)\
((pPwVcEntry)->u4VplsBgpPwBindLocalVEId)

#define L2VPN_PWVC_REMOTE_VE_ID(pPwVcEntry)\
((pPwVcEntry)->u4VplsBgpPwBindRemoteVEId)

#define L2VPN_PWVC_OUT_IF_INDEX(pPwVcEntry)\
((pPwVcEntry)->u4PwL3Intf)

#define L2VPN_PWVC_PW_TYPE(pPwVcEntry)\
((pPwVcEntry)->u4PwType)

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
#define L2VPN_PWVC_NPAPI(pPwVcEntry)\
((pPwVcEntry)->u1NpApiCall)
#endif

  /*Vc Interval Performance table*/
#define L2VPN_PWVC_PERF_INTVL2ARRAY_IDX(c,i)\
    i=((c>i)?c-i:L2VPN_PWVC_MAX_PERF_INTV+c-i)

#define L2VPN_PWVC_PERF_INT_VALID_DATA(x,y)\
   ((x)->aPwVcPerfIntervalInfo[y-1].i1ValidData)

#define L2VPN_PWVC_PERF_INT_TIME_ELAPSED(x,y)\
   ((x)->aPwVcPerfIntervalInfo[y-1].i4IntTimeElapsed)

#define L2VPN_PWVC_PERF_INT_IN_HC_PKTS(x,y)\
   &((x)->aPwVcPerfIntervalInfo[y-1].InHCPkts)

#define L2VPN_PWVC_PERF_INT_IN_HC_BYTES(x,y)\
   &((x)->aPwVcPerfIntervalInfo[y-1].InHCBytes)

#define L2VPN_PWVC_PERF_INT_OUT_HC_PKTS(x,y)\
   &((x)->aPwVcPerfIntervalInfo[y-1].OutHCPkts)

#define L2VPN_PWVC_PERF_INT_OUT_HC_BYTES(x,y)\
   &((x)->aPwVcPerfIntervalInfo[y-1].OutHCBytes)


   /*Vc Performance total table*/
#define L2VPN_PWVC_PERF_1DAY_INTVL2ARRAY_IDX(c,i)\
    i=((c>=i)?c-i:L2VPN_PWVC_MAX_PERF_DAYS+c-i)+1

#define L2VPN_PWVC_PERF_1DAY_VALID_DATA(x,y)\
   ((x)->aPwVcPerf1DayIntervalInfo[y-1].i1IsValidData)

#define L2VPN_PWVC_PERF_1DAY_MONI_TIME(x,y)\
   ((x)->aPwVcPerf1DayIntervalInfo[y-1].u4MoniTime)

#define L2VPN_PWVC_PERF_1DAY_IN_HC_PKTS(x,y)\
   &((x)->aPwVcPerf1DayIntervalInfo[y-1].InHCPkts)

#define L2VPN_PWVC_PERF_1DAY_IN_HC_BYTS(x,y)\
   &((x)->aPwVcPerf1DayIntervalInfo[y-1].InHCByts)

#define L2VPN_PWVC_PERF_1DAY_OUT_HC_PKTS(x,y)\
   &((x)->aPwVcPerf1DayIntervalInfo[y-1].OutHCPkts)

#define L2VPN_PWVC_PERF_1DAY_OUT_HC_BYTS(x,y)\
   &((x)->aPwVcPerf1DayIntervalInfo[y-1].OutHCByts)

   /* Pw-Mpls - MIB Related */

   /*PW MPLS VC Table */
#define L2VPN_PWVC_MPLS_OUT_ENTRY(pPwVcMplsEntry)\
&((pPwVcMplsEntry)->PwVcMplsOutTnl)

#define L2VPN_PWVC_MPLS_IN_ENTRY(pPwVcMplsEntry)\
&((pPwVcMplsEntry)->PwVcMplsInTnl)

#define L2VPN_PWVC_MPLS_MPLS_TYPE(pPwVcMplsEntry)\
((pPwVcMplsEntry)->u1MplsType)

#define L2VPN_PWVC_MPLS_EXP_BITS_MODE(pPwVcMplsEntry)\
((pPwVcMplsEntry)->i1ExpModeAndBits)

#define L2VPN_PWVC_MPLS_EXP_BITS(pPwVcMplsEntry)\
((pPwVcMplsEntry)->i1ExpModeAndBits)

#define L2VPN_PWVC_MPLS_TTL(pPwVcMplsEntry)\
((pPwVcMplsEntry)->u1Ttl)

#define L2VPN_PWVC_MPLS_LCL_LDPID(pPwVcMplsEntry)\
((pPwVcMplsEntry)->LocalLdpID)

#define L2VPN_PWVC_MPLS_LCL_LDP_ENTID(pPwVcMplsEntry)\
((pPwVcMplsEntry)->u4LocalLdpEntityID)

#define L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX(pPwVcMplsEntry)\
((pPwVcMplsEntry)->u4LocalLdpEntityIndex)

#define L2VPN_PWVC_MPLS_PEER_LDPID(pPwVcMplsEntry)\
((pPwVcMplsEntry)->PeerLdpID)

#define L2VPN_PWVC_MPLS_STORAGE_TYPE(pPwVcMplsEntry)\
((pPwVcMplsEntry)->i1StorageType)

   /*PW Vc MPLS Outbound Tunnel Table */

#define L2VPN_PWVC_MPLS_TNL_XC_INDEX(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->unTnlInfo.NonTeInfo.u4LsrXcIndex)

#define L2VPN_PWVC_MPLS_TNL_TNL_INDEX(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->unTnlInfo.TeInfo.u4TnlIndex)

#define L2VPN_PWVC_MPLS_TNL_TNL_INST(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->unTnlInfo.TeInfo.u2TnlInstance)

#define L2VPN_PWVC_MPLS_TNL_TNL_LCLLSR(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->unTnlInfo.TeInfo.TnlLclLSR)

#define L2VPN_PWVC_MPLS_TNL_TNL_PEERLSR(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->unTnlInfo.TeInfo.TnlPeerLSR)
 
#define L2VPN_PWVC_MPLS_TNL_IF_INDEX(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->unTnlInfo.VcInfo.u4IfIndex)

#define L2VPN_PWVC_MPLS_TNL_TYPE(pPwVcMplsTnlEntry)\
((pPwVcMplsTnlEntry)->u1TnlType)

/* PwVc Mpls Inbound Tunnel Table */

#define L2VPN_PWVC_MPLS_IN_TNL_XC_INDEX(pPwVcMplsInTnlEntry)\
((pPwVcMplsInTnlEntry)->u4LsrXcIndex)

#define L2VPN_PWVC_MPLS_IN_TNL_TNL_INDEX(pPwVcMplsInTnlEntry)\
((pPwVcMplsInTnlEntry)->unInTnlInfo.TeInfo.u4TnlIndex)

#define L2VPN_PWVC_MPLS_IN_TNL_TNL_INST(pPwVcMplsInTnlEntry)\
((pPwVcMplsInTnlEntry)->unInTnlInfo.TeInfo.u2TnlInstance)

#define L2VPN_PWVC_MPLS_IN_TNL_TNL_LCLLSR(pPwVcMplsInTnlEntry)\
((pPwVcMplsInTnlEntry)->unInTnlInfo.TeInfo.TnlLclLSR)

#define L2VPN_PWVC_MPLS_IN_TNL_TNL_PEERLSR(pPwVcMplsInTnlEntry)\
((pPwVcMplsInTnlEntry)->unInTnlInfo.TeInfo.TnlPeerLSR)

#define L2VPN_PWVC_MPLS_NONTE_MAP_LIST(x) \
((x)->pPwNonTeMapRBTreeList)
#define L2VPN_PWVC_MPLS_NONTE_IN_MAP_LIST(x) \
((x)->pPwNonTeInMapRBTreeList)
#define L2VPN_PWVC_MPLS_TE_MAP_LIST(x) \
((x)->pPwTeMapRBTreeList)
#define L2VPN_PWVC_MPLS_TE_IN_MAP_LIST(x) \
((x)->pPwTeInMapRBTreeList)
#define L2VPN_PWVC_MPLS_VCONLY_MAP_LIST(x) \
((x)->pPwVcOnlyMapRBTreeList)

#define L2VPN_PWVC_ENET_ENTRY_LIST(pPwVcEnetServSpecEntry)\
(&((pPwVcEnetServSpecEntry)->EnetEntryList))
 
#define L2VPN_PWVC_ENET_PW_INSTANCE(pPwVcEnetEntry)\
(pPwVcEnetEntry->u4Instance)

#define L2VPN_PWVC_ENET_PW_VLAN(pPwVcEnetEntry)\
(pPwVcEnetEntry->u2PwVlan)

#define L2VPN_PWVC_ENET_VLAN_MODE(pPwVcEnetEntry)\
(pPwVcEnetEntry->i1VlanMode)

#define L2VPN_PWVC_ENET_PORT_VLAN(pPwVcEnetEntry)\
(pPwVcEnetEntry->u2PortVlan)

#define L2VPN_PWVC_ENET_PORT_IF_INDEX(pPwVcEnetEntry)\
(pPwVcEnetEntry->i4PortIfIndex)

#define L2VPN_PWVC_ENET_VC_IF_INDEX(pPwVcEnetEntry)\
(pPwVcEnetEntry->i4VcIfIndex)

#define L2VPN_PWVC_ENET_ROWSTATUS(pPwVcEnetEntry)\
(pPwVcEnetEntry->i1RowStatus)

#define L2VPN_PWVC_ENET_STORAGE_TYPE(pPwVcEnetEntry)\
(pPwVcEnetEntry->i1StorageType)

   /*PW Vc EnetMpls PriMapping Table */ 

#define L2VPN_ENET_MPLS_PRI_MAPPING(pPwVcEnetServSpecEntry)\
((pPwVcEnetServSpecEntry)->pMplsPriMappingEntry->u2PriMapping)

#define L2VPN_ENET_MPLS_PRI_MAP_ROWSTATUS(pPwVcEnetServSpecEntry)\
((pPwVcEnetServSpecEntry)->pMplsPriMappingEntry->i1PriMappingRowStatus)

#define L2VPN_ENET_MPLS_PRI_MAP_STORAGE(pPwVcEnetServSpecEntry)\
((pPwVcEnetServSpecEntry)->pMplsPriMappingEntry->i1PriMappingStorageType)


 /* Interface message Macros */
/* Macros for accessing memebers of tLdpL2VpnPwVcEntry from
 * tLdpL2VpnPwVcEvtInfo
 */
#define LDP_L2VPN_EVT_INFO_PEER_ADDR(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->PwVcPeerAddr)

#define LDP_L2VPN_EVT_INFO_PWVC_INDEX(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->u4PwVcIndex)

#define LDP_L2VPN_EVT_INFO_PWVC_ID(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->u4PwVcID)

#define LDP_L2VPN_EVT_INFO_GROUP_ID(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->u4PwVcLocalGroupID)

#define LDP_L2VPN_EVT_INFO_IF_MTU(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->u2PwVcLocalIfMtu)

#define LDP_L2VPN_EVT_INFO_LDP_ID(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->PwVcMplsLocalLdpID)

#define LDP_L2VPN_EVT_INFO_PWVC_TYPE(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->i1PwVcType)

#define LDP_L2VPN_EVT_INFO_PEER_ADDR_TYPE(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->i1PwVcPeerAddrType)

#define LDP_L2VPN_EVT_INFO_CONTROL_WORD(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->i1PwVcControlWord)

#define LDP_L2VPN_EVT_INFO_IF_STRING(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->i1PwVcLocalIfString)

#define LDP_L2VPN_EVT_INFO_CW_REQ(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->i1PwVcCWRequirement)

#define LDP_L2VPN_EVT_INFO_CW_PREF(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->pLdpL2VpnPwVcEntry->i1PwVcCWPreference)


/* Macros for accessing memebers of tLdpL2VpnPwVcEntry */
#define LDP_L2VPN_EVT_INFO_EVENT(pLdpL2VpnPwVcEvtInfo)\
((pLdpL2VpnPwVcEvtInfo)->u4Event)


#define LDP_PEER_CEM_PAYLOAD_BYTES(pLdpPwVcPeerInfo)\
((pLdpPwVcPeerInfo)->u2CEMPayloadBytes)

#define LDP_PEER_MAX_CONCAT_ATM_CELLS(pLdpPwVcPeerInfo)\
((pLdpPwVcPeerInfo)->u2MaxNoOfConcatATMCells)


/* Macros for accessing members of tL2VpnLdpPwVcEvtInfo */
#define L2VPN_LDP_PWVC_INFO_VC_INDEX(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.u4PwVcIndex)

#define L2VPN_LDP_PWVC_INFO_REM_GRP_ID(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.u4PwVcRemoteGroupID)

#define L2VPN_LDP_PWVC_INFO_OUT_VC_LABEL(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.u4PwVcOutboundVcLabel)

#define L2VPN_LDP_PWVC_INFO_IN_VC_LABEL(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.u4PwVcInboundVcLabel)

#define L2VPN_LDP_PWVC_INFO_REM_IF_MTU(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.u2PwVcRemoteIfMtu)

#define L2VPN_LDP_PWVC_INFO_PEER_LDP_ID(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.PwVcMplsPeerLdpID)

#define L2VPN_LDP_PWVC_INFO_REM_IF_STR(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.au1PwVcRemoteIfString)

#define L2VPN_LDP_PWVC_INFO_REMOTE_CW(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPwVcEntry.i1PwVcRemoteControlWord)

#define L2VPN_LDP_PWVC_INFO_PEER_ID(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.L2VpnLdpPeerId)

#define L2VPN_LDP_PWVC_INFO_EVENT_TYPE(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->u4EvtType)

#define L2VPN_PWVC_LBL_MSG_LIST(pPwVcActivePeerSsnEntry)\
(&((pPwVcActivePeerSsnEntry)->LblMsgList))
#define L2VPN_PWVC_LIST(pPwVcActivePeerSsnEntry)\
(&((pPwVcActivePeerSsnEntry)->PwVcList))

#define L2VPN_SESSION_ICCP_NODE_LIST(pPwVcActivePeerSsnEntry)\
(&((pPwVcActivePeerSsnEntry)->IccpNodeList))


/* L2VPN -> LDP session registration / de-registration requests */
#define L2VPN_SESSION_PWVC_REG_REQ(pPwVcEntry)\
(\
    L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL ||\
    L2VpnSendLdpPeerSsnCreateReq (\
        &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),\
        (pPwVcEntry->i4PeerAddrType),\
        L2VPN_PWVC_MPLS_LCL_LDP_ENTID ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)))\
    == L2VPN_SUCCESS\
)

#define L2VPN_SESSION_PWVC_DEREG_REQ(pPwVcEntry)\
(\
    L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) == NULL ||\
    L2VpnSendLdpDeRegReq (\
        &L2VPN_PWVC_PEER_ADDR (pPwVcEntry),\
        (pPwVcEntry->i4PeerAddrType),\
        L2VPN_PWVC_MPLS_LCL_LDP_ENTID ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)),\
        L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX ((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry)))\
    == L2VPN_SUCCESS\
)

#define L2VPN_SESSION_ICCP_REG_REQ(pNodeEntry)\
(\
    (pNodeEntry)->pSession == NULL ||\
    L2VpnSendLdpPeerSsnCreateReq (\
        &(pNodeEntry)->Addr,\
        (INT4) (pNodeEntry)->u1AddrType,\
        (pNodeEntry)->pSession->u4LocalLdpEntityID)\
    == L2VPN_SUCCESS\
)

#define L2VPN_SESSION_ICCP_DEREG_REQ(pNodeEntry)\
(\
    (pNodeEntry)->pSession == NULL ||\
    L2VpnSendLdpDeRegReq (\
        &(pNodeEntry)->Addr,\
        (INT4) (pNodeEntry)->u1AddrType,\
        (pNodeEntry)->pSession->u4LocalLdpEntityID,\
        (pNodeEntry)->pSession->u4LdpEntityIndex)\
    == L2VPN_SUCCESS\
)

#ifndef MEMCPY
#define MEMCPY(pu1Dest, pu1Src, n)    memcpy(pu1Dest, pu1Src, n)
#endif


/* PW Redundancy */
#define L2VPN_PWRED_AC_STATUS(L2VpnQMsg) \
L2VpnQMsg.L2VpnEvtInfo.PwRedAcInfo.u1AcStatus
#define L2VPN_PWRED_AC_DLAG_PORT_CNT(L2VpnQMsg) \
L2VpnQMsg.L2VpnEvtInfo.PwRedAcInfo.u1DlagPortCnt
#define L2VPN_PWRED_AC_DLAG_PORTS(L2VpnQMsg) \
L2VpnQMsg.L2VpnEvtInfo.PwRedAcInfo.au2DlagPort


#define L2VPN_QMSG_TYPE L2VpnQMsg.u4MsgType

#define L2VPN_ADMIN_WAIT_FOR_RESP_FLAG(L2VpnQMsg) \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.u4WaitForRespFlag
#define L2VPN_ADMIN_SUBEVT_TYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.u4EvtType
#define L2VPN_ADMIN_SUBEVT_PARAM \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.u4Param
#define L2VPN_ADMIN_EVT_PWVC_EVTTYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4EvtType
#define L2VPN_ADMIN_EVT_PWVC_INDEX \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcAdminEvtInfo.u4PwVcIndex

/*MS-PW */
#define L2VPN_ADMIN_EVT_MSPW_EVTTYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo.u4EvtType
#define L2VPN_ADMIN_EVT_MSPW_INDEX \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.MsPwAdminEvtInfo
/*MS-PW */

#define L2VPN_ADMIN_EVT_PWVC_SSTYPE \
L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.u4ServSpecType
#define L2VPN_ADMIN_EVT_SSEVT_TYPE \
L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.EnetAdminEvtInfo.u4EvtType
#define L2VPN_ADMIN_EVT_SSENET_VCINDEX \
L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.EnetAdminEvtInfo.EnetEntryIndex.u4PwVcIndex
#define L2VPN_ADMIN_EVT_SSENET_VLANID \
L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.EnetAdminEvtInfo.EnetEntryIndex.i2PwVcEnetPwVlan

#define L2VPN_ADMIN_EVT_PSN_TYPE \
L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.u4PsnType
#define L2VPN_ADMIN_EVT_PSN_MPLS_EVTTYPE \
L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.u4EvtType
#define L2VPN_ADMIN_EVT_PSN_MPLS_VCINDEX \
L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.unEvtInfo.TnlEntryIndex.u4PwVcIndex
#define L2VPN_ADMIN_EVT_PSN_MPLS_TNLINDEX \
L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.unEvtInfo.TnlEntryIndex.u4TnlInOutIndex

/* PW Redundancy */
#define L2VPN_ADMIN_EVT_RED_GROUP_EVTTYPE(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedGroupAdminEvtInfo.u4EvtType)
#define L2VPN_ADMIN_EVT_RED_GROUP_INDEX(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedGroupAdminEvtInfo.u4GroupId)

#define L2VPN_ADMIN_EVT_RED_NODE_EVTTYPE(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedNodeAdminEvtInfo.u4EvtType)
#define L2VPN_ADMIN_EVT_RED_NODE_GROUP(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedNodeAdminEvtInfo.u4GroupId)
#define L2VPN_ADMIN_EVT_RED_NODE_ADDRTYPE(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedNodeAdminEvtInfo.u1AddrType)
#define L2VPN_ADMIN_EVT_RED_NODE_ADDR(pAdminEvt) \
    (&((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedNodeAdminEvtInfo.Addr)

#define L2VPN_ADMIN_EVT_RED_PW_EVTTYPE(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedPwAdminEvtInfo.u4EvtType)
#define L2VPN_ADMIN_EVT_RED_PW_GROUP(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedPwAdminEvtInfo.u4GroupId)
#define L2VPN_ADMIN_EVT_RED_PW_INDEX(pAdminEvt) \
    (((tL2VpnAdminEvtInfo *) (pAdminEvt))->unEvtInfo.RedPwAdminEvtInfo.u4PwIndex)

#ifdef VPLSADS_WANTED
#define L2VPN_ADMIN_EVT_VPLS_EVTTYPE \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.u4EvtType
#define L2VPN_ADMIN_EVT_VPLS_VPLSINDEX \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.u4VplsIndex
#define L2VPN_ADMIN_EVT_VPLS_MTU \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.u4Mtu
#define L2VPN_ADMIN_EVT_VPLS_VEID \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.u4VeId
#define L2VPN_ADMIN_EVT_VPLS_LB \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.pVplsLBInfoBase
#define L2VPN_ADMIN_EVT_VPLS_RD \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.au1RouteDistinguisher
#define L2VPN_ADMIN_EVT_VPLS_RT \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.au1RouteTarget
#define L2VPN_ADMIN_EVT_VPLS_CONTROLWORD \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.bControlWordFlag
#define L2VPN_ADMIN_EVT_VPLS_RTTYPE \
    L2VpnAdminEvtInfo.unEvtInfo.VplsAdminEvtInfo.u1RTType
#endif

#define L2VPN_PSN_EVT_PSN_TYPE \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.u4PsnType
#define L2VPN_PSN_EVT_PSN_MPLSTYPE \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.u4MplsType
#define L2VPN_PSN_EVT_PSN_MPLSTNL_EVT \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4EvtType
#define L2VPN_PSN_EVT_PSN_MPLSTNL_INDEX \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4PwVcMplsTnlIndex
#define L2VPN_PSN_EVT_PSN_MPLSTNL_INST \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u2PwVcMplsTnlInstance
#define L2VPN_PSN_EVT_PSN_MPLSTNL_BKP_INST \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u2PwVcMplsBkpTnlInstance
#define L2VPN_PSN_EVT_PSN_MPLSTNL_SWITCH_TYPE \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u1TnlSwitchingType
#define L2VPN_PSN_EVT_PSN_MPLSTNL_LCLID \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.PwVcMplsTnlLclLSR
#define L2VPN_PSN_EVT_PSN_MPLSTNL_PEERID \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.PwVcMplsTnlPeerLSR
#define L2VPN_PSN_EVT_PSN_MPLSTNL_XC_INDEX \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4TnlXcIndex
#define L2VPN_PSN_EVT_PSN_MPLSTNL_IFINDEX \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4TnlIfIndex
#define L2VPN_PSN_EVT_PSN_MPLSTNL_TNLROLE \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u1TnlRole
#define L2VPN_PSN_EVT_PSN_MPLSLSP_EVT \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.u4EvtType
#define L2VPN_PSN_EVT_PSN_MPLSTNL_DEL_TNL_INTF \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u1DeleteTnlAction
#define L2VPN_PSN_EVT_PSN_MPLSTNL_MP_LABEL1 \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4TnlBkpMPLabel1
#define L2VPN_PSN_EVT_PSN_MPLSTNL_MP_LABEL2 \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4TnlBkpMPLabel2
#define L2VPN_PSN_EVT_PSN_MPLSTNL_MP_LABEL3 \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.u4TnlBkpMPLabel3
#define L2VPN_PSN_EVT_PSN_MPLSTNL_NEXT_HOP_MAC \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsTeEvtInfo.au1NextHopMac
#define L2VPN_PSN_EVT_PSN_MPLSLSP_INDEX \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.u4PwVcMplsOutLsrXcIndex
#define L2VPN_PSN_EVT_PSN_MPLSLSP_ADDR \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.PeerOrSrcAddr
#define L2VPN_PSN_EVT_PSN_MPLSLSP_ADDR_TYPE \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.u1AddrType
#define L2VPN_PSN_EVT_PSN_MPLSLSP_MP_LABEL1 \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.u4TnlBkpMPLabel1
#define L2VPN_PSN_EVT_PSN_MPLSLSP_MP_LABEL2 \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.u4TnlBkpMPLabel2
#define L2VPN_PSN_EVT_PSN_MPLSLSP_MP_LABEL3 \
L2VpnQMsg.L2VpnEvtInfo.PwVcPSNEvtInfo.unEvtInfo.L2VpnMplsPwVcPSNEvtInfo.unEvtInfo.MplsNonTeEvtInfo.u4TnlBkpMPLabel3

#define L2VPN_QMSG_ADMIN_EVT_PWVC_SSTYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.u4ServSpecType
#define L2VPN_QMSG_ADMIN_EVT_SSEVT_TYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.EnetAdminEvtInfo.u4EvtType
#define L2VPN_QMSG_ADMIN_EVT_SSENET_VCINDEX \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.EnetAdminEvtInfo.EnetEntryIndex.u4PwVcIndex
#define L2VPN_QMSG_ADMIN_EVT_SSENET_VLANID \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcServAdminEvtInfo.EnetAdminEvtInfo.EnetEntryIndex.i2PwVcEnetPwVlan

#define L2VPN_QMSG_ADMIN_EVT_PSN_TYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.u4PsnType
#define L2VPN_QMSG_ADMIN_EVT_PSN_MPLS_EVTTYPE \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.u4EvtType
#define L2VPN_QMSG_ADMIN_EVT_PSN_MPLS_VCINDEX \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.unEvtInfo.TnlEntryIndex.u4PwVcIndex
#define L2VPN_QMSG_ADMIN_EVT_PSN_MPLS_TNLINDEX \
L2VpnQMsg.L2VpnEvtInfo.L2VpnAdminEvtInfo.unEvtInfo.PwVcPSNAdminEvtInfo.MplsAdminEvtInfo.unEvtInfo.TnlEntryIndex.u4TnlInOutIndex

/* VPLS Related Macros */

#define L2VPN_VPLS_INFO_PTR       ((tVPLSInfo *)&gPwVcGlobalInfo.ppVPLSTable[0])
#define L2VPN_VPLS_POOL_ID                L2VPNMemPoolIds[MAX_L2VPN_VPLS_ENTRIES_SIZING_ID]
#define L2VPN_VPLS_TAB_POOL_ID            L2VPNMemPoolIds[MAX_L2VPN_VPLS_INFO_SIZING_ID]
#ifdef VPLSADS_WANTED
#define L2VPN_VPLS_RD_POOL_ID             L2VPNMemPoolIds[MAX_L2VPN_VPLS_RD_ENTRIES_SIZING_ID]
#define L2VPN_VPLS_RT_POOL_ID             L2VPNMemPoolIds[MAX_L2VPN_VPLS_RT_ENTRIES_SIZING_ID]
#define L2VPN_VPLS_VE_POOL_ID             L2VPNMemPoolIds[MAX_L2VPN_VPLS_VE_ENTRIES_SIZING_ID]
#define L2VPN_VPLS_AC_POOL_ID             L2VPNMemPoolIds[MAX_L2VPN_VPLS_AC_ENTRIES_SIZING_ID]
#define L2VPN_VPLS_LB_POOL_ID             L2VPNMemPoolIds[MAX_L2VPN_VPLS_LB_ENTRIES_SIZING_ID]
#endif
#ifdef HVPLS_WANTED
#define L2VPN_VPLS_BIND_POOL_ID           L2VPNMemPoolIds[MAX_L2VPN_VPLS_BIND_ENTRIES_SIZING_ID]
#endif
#define L2VPN_DATA_BUFFER                 L2VPNMemPoolIds[MAX_L2VPN_DATA_BUFFER_SIZING_ID]
#define L2VPN_VPLS_INDEX(x)               (x)->u4VplsInstance
#define L2VPN_VPLS_VSI(x)                 (x)->i4VplsVSI
#define L2VPN_VPLS_VPN_ID(x)              (x)->au1VplsVpnID
#define L2VPN_VPLS_NAME(x)                (x)->au1VplsName
#define L2VPN_VPLS_DESCR(x)               (x)->au1VplsDescr
#define L2VPN_VPLS_FDB_HIGH_THRESHOLD(x)  (x)->u4VplsFDBHighThreshold
#define L2VPN_VPLS_FDB_LOW_THRESHOLD(x)   (x)->u4VplsFDBLowThreshold
#define L2VPN_VPLS_ROW_STATUS(x)          (x)->i1RowStatus
/* VPLS FDB */
#define L2VPN_VPLS_FDB_ID(x)              (x)->u2VplsFdbId

#define L2VPN_VPLS_STORAGE_TYPE(x)        (x)->u1StorageType
#define L2VPN_VPLS_OPER_STATUS(x)         (x)->u4VplsStatusOperStatus
#define L2VPN_VPLS_PEER_COUNT(x)          (x)->u4VplsStatusPeerCount
#define L2VPN_VPLS_VE_RANGE_SIZE(x)       (x)->u4VplsBgpConfigVERangeSize
#define L2VPN_VPLS_MTU(x)                 (x)->u4Mtu
#define L2VPN_VPLS_CONTROL_WORD(x)        (x)->bControlWordFlag
#define L2VPN_VPLS_SIG_TYPE(x)            (x)->u4SignallingType
#ifdef VPLSADS_WANTED
#define L2VPN_VPLS_LABEL_BLOCK(x)         (x)->pVPLSLBInfoBase
#define L2VPN_VPLS_DEFAULT_RD(x)          (x)->au1DefaultRD
#define L2VPN_VPLS_PREVIOUS_DEFAULT_RD(x) (x)->au1PreviousDefaultRD
#define L2VPN_VPLS_DEFAULT_RT(x)          (x)->au1DefaultRT
#define L2VPN_VPLS_PREVIOUS_DEFAULT_RT(x) (x)->au1PreviousDefaultRT

/* Vpls Rd Entry */
#define L2VPN_VPLSRD_VPLS_INSTANCE(x)          (x)->u4VplsInstance
#define L2VPN_VPLSRD_ROUTE_DISTINGUISHER(x)    (x)->au1VplsBgpADConfigRouteDistinguisher
#define L2VPN_VPLSRD_CONFIG_PREFIX(x)          (x)->u4VplsBgpADConfigPrefix
#define L2VPN_VPLSRD_VPLS_ID(x)                (x)->au1VplsBgpADConfigVplsId
#define L2VPN_VPLSRD_ROW_STATUS(x)             (x)->i1VplsBgpADConfigRowStatus
#define L2VPN_VPLSRD_STORAGE_TYPE(x)           (x)->u1VplsBgpADConfigStorageType

/* Vpls Rt Entry */
#define L2VPN_VPLSRT_VPLS_INSTANCE(x)          (x)->u4VplsInstance
#define L2VPN_VPLSRT_RT_INDEX(x)               (x)->u4VplsBgpRteTargetIndex
#define L2VPN_VPLSRT_RT_TYPE(x)                (x)->u1VplsBgpRteTargetRTType
#define L2VPN_VPLSRT_ROUTE_TARGET(x)           (x)->au1VplsBgpRteTargetRT
#define L2VPN_VPLSRT_ROW_STATUS(x)             (x)->i1VplsBgpRteTargetRowStatus
#define L2VPN_VPLSRT_STORAGE_TYPE(x)           (x)->u1VplsBgpRteTargetStorageType

/* Vpls Ve Entry */
#define L2VPN_VPLSVE_VPLS_INSTANCE(x)          (x)->u4VplsInstance
#define L2VPN_VPLSVE_VE_ID(x)                  (x)->u4VplsBgpVEId
#define L2VPN_VPLSVE_VE_NAME(x)                (x)->au1VplsBgpVEName
#define L2VPN_VPLSVE_VE_PREFRENCE(x)           (x)->u4VplsBgpVEPreference
#define L2VPN_VPLSVE_ROW_STATUS(x)             (x)->i1VplsBgpVERowStatus
#define L2VPN_VPLSVE_STORAGE_TYPE(x)           (x)->u1VplsBgpVEStorageType

/* Vpls AC Map Entry */
#define L2VPN_VPLSAC_VPLS_INSTANCE(x)          (x)->u4VplsInstance
#define L2VPN_VPLSAC_INDEX(x)                  (x)->u4AcIndex
#define L2VPN_VPLSAC_PORT_INDEX(x)             (x)->u4AcPortIndex
#define L2VPN_VPLSAC_VLAN_ID(x)                (x)->u4AcVlanId
#define L2VPN_VPLSAC_ROW_STATUS(x)             (x)->i1VplsAcMapRowStatus
#define L2VPN_VPLSAC_OPER_STATUS(x)            (x)->u4AcOperStatus
#endif
/* Vpls Bind Entry */
#ifdef HVPLS_WANTED
#define L2VPN_VPLS_PW_BIND_VPLS_INSTANCE(x)         (x)->u4VplsInstance
#define L2VPN_VPLS_PW_BIND_PWINDEX(x)               (x)->u4PwIndex
#define L2VPN_VPLS_PW_BIND_CONFIG_TYPE(x)       (x)->u4VplsPwBindConfigType
#define L2VPN_VPLS_PW_BIND_BIND_TYPE(x)              (x)->u4VplsPwBindType
#define L2VPN_VPLS_PW_BIND_ROW_STATUS(x)             (x)->i1VplsPwBindRowStatus
#endif
#define L2VPN_ARRAY_LEN(au1Array,i4Max,i4Len) {\
  for(i4Len=0;(au1Array[i4Len]!= 0 && i4Len< i4Max);i4Len++);}

#define L2VPN_VPLS_PW_SCAN(pVplsEntry,pPwVplsNode)\
  TMO_SLL_Scan (&pVplsEntry->PwList, pPwVplsNode, tTMO_SLL_NODE *)

/* macro to find base address of structure given the member address */
#define L2VPN_GET_BASE_PTR(type, memberName,pMember) \
    (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName) )

/*MS-PW */

/* Macro used to convert the input 
  to required datatype for FsMsPwMaxEntries*/

#define  L2VPN_FILL_FSMSPWMAXENTRIES(u4FsMsPwMaxEntries,\
   pau1FsMsPwMaxEntries)\
  {\
  if (pau1FsMsPwMaxEntries != NULL)\
  {\
   u4FsMsPwMaxEntries =CLI_PTR_TO_U4(pau1FsMsPwMaxEntries);\
  }\
  }

/* Macro used to fill the CLI structure for FsMsPwConfigTable 
 using the input given in def file */

#define  L2VPN_FILL_FSMSPWCONFIGTABLE_ARGS(pL2vpnFsMsPwConfigTable,\
   pL2vpnIsSetFsMsPwConfigTable,\
   pau1FsMsPwIndex1,\
   pau1FsMsPwIndex2,\
   pau1FsMsPwRowStatus)\
  {\
  if (pau1FsMsPwIndex1 !=L2VPN_ZERO )\
  {\
   pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1 =(pau1FsMsPwIndex1);\
   pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 = OSIX_TRUE;\
  }\
  else\
  {\
   pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex1 = OSIX_FALSE;\
  }\
  if (pau1FsMsPwIndex2 != L2VPN_ZERO)\
  {\
   pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2 =(pau1FsMsPwIndex2);\
   pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex2 = OSIX_TRUE;\
  }\
  else\
  {\
   pL2vpnIsSetFsMsPwConfigTable->bFsMsPwIndex2 = OSIX_FALSE;\
  }\
  if (pau1FsMsPwRowStatus !=L2VPN_ZERO )\
  {\
   pL2vpnFsMsPwConfigTable->i4FsMsPwRowStatus = (pau1FsMsPwRowStatus);\
   pL2vpnIsSetFsMsPwConfigTable->bFsMsPwRowStatus = OSIX_TRUE;\
  }\
  }

/* Macro used to fill the index values in  structure for FsMsPwConfigTable 
 using the input given in def file */

#define  L2VPN_FILL_FSMSPWCONFIGTABLE_INDEX(pL2vpnFsMsPwConfigTable,\
   pau1FsMsPwIndex1,\
   pau1FsMsPwIndex2)\
  {\
  if (pau1FsMsPwIndex1 !=NULL)\
  {\
   pL2vpnFsMsPwConfigTable->u4FsMsPwIndex1 = (pau1FsMsPwIndex1);\
  }\
  if (pau1FsMsPwIndex2 != NULL)\
  {\
   pL2vpnFsMsPwConfigTable->u4FsMsPwIndex2 =(pau1FsMsPwIndex2);\
  }\
  }

#define FSMSPWCONFIGTABLE_BLOCK_SIZE       10


#define L2VPN_MAX_MSPW_ENTRIES(x) ((x).L2vpnMsPwGlbMib.u4FsMsPwMaxEntries)

#define L2VPN_RED_STATUS_FORWARD_ENABLE(x)      ((x)->b1SpeRedStatusForwardEnable)

#define L2VPN_MAXIMUM_PWVC_ENTRIES        FsL2VPNSizingParams \
                                          [MAX_L2VPN_PW_VC_ENTRIES_SIZING_ID].u4PreAllocatedUnits
#define L2VPN_PWVC_INDEX_MAXVAL           FsL2VPNSizingParams \
                                          [MAX_L2VPN_PW_VC_ENTRIES_SIZING_ID].u4PreAllocatedUnits
#define L2VPN_MAX_VPLS_ENTRIES            FsL2VPNSizingParams \
                                          [MAX_L2VPN_VPLS_ENTRIES_SIZING_ID].u4PreAllocatedUnits

#define L2VPN_MIN_MSPW_ENTRIES 1

#define L2VPN_MSPW_UP              1
#define L2VPN_MSPW_DOWN            2
#define L2VPN_MSPW_DELETE          5
#define L2VPN_MPLS_PORT_ENTRY_INFO_TABLE  gPwVcEnetGlobalInfo.MplsPortEntryRBTree
#define MPLS_MULTIPLEX             1
#define MPLS_BUNDLE                2

/*MS-PW */

/* GR Related Macros */
enum {
    L2VPN_GR_FULLY_SYNCHRONIZED = 0, /* Non Stale Entries */
    L2VPN_GR_LOCAL_SYNCHRONIZED, /* Refers Stale Entries, but has sent Lbl Mapping
                                    Msg */
    L2VPN_GR_REMOTE_SYNCHRONIZED, /*Refers Stale Entries, but has received 
                                    Lbl Mapping Msg*/
    L2VPN_GR_NOT_SYNCHRONIZED /* Refers Stale Entries */
};
#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
#define L2VpnVplsHwListAdd                                    MplsL2vpnVplsHwListAdd
#define L2VpnVplsHwListUpdate                                 MplsL2vpnVplsHwListUpdate
#define L2VpnVplsHwListDelete                                 MplsL2vpnVplsHwListDelete
#define L2VpnVplsHwListGetFirst                       MplsL2vpnVplsHwListGetFirst
#define L2VpnVplsHwListGet                                    MplsL2vpnVplsHwListGet
#define L2VpnVplsHwListGetNext                                MplsL2vpnVplsHwListGetNext
#define L2VPN_VPLS_HW_LIST_VPLS_INDEX         MPLS_L2VPN_VPLS_HW_LIST_VPLS_INDEX
#define L2VPN_VPLS_HW_LIST_ENET_INDEX         MPLS_L2VPN_VPLS_HW_LIST_ENET_INDEX
#define L2VPN_VPLS_HW_LIST_NPAPI_STATUS               MPLS_L2VPN_VPLS_HW_LIST_NPAPI_STATUS
#define L2VPN_VPLS_HW_LIST_VPNID                      MPLS_L2VPN_VPLS_HW_LIST_VPNID
#define L2VPN_VPLS_HW_LIST_FDBID                      MPLS_L2VPN_VPLS_HW_LIST_FDBID

#define L2VpnHwListAdd      MplsL2vpnHwListAdd
#define L2VpnHwListUpdate     MplsL2vpnHwListUpdate
#define L2VpnHwListDelete     MplsL2vpnHwListDelete
#define L2VpnHwListGetFirst    MplsL2vpnHwListGetFirst
#define L2VpnHwListGet      MplsL2vpnHwListGet
#define L2VpnHwListGetNext     MplsL2vpnHwListGetNext
#define L2VpnAllocateMemoryForHwList  MplsAllocateMemoryForHwList
#define L2vpnHwListUpdateNpapiStatus MplsL2vpnHwListUpdateNpapiStatus
#define L2VPN_PW_HW_LIST_VPLS_INDEX  MPLS_PW_HW_LIST_VPLS_INDEX
#define L2VPN_PW_HW_LIST_PW_INDEX   MPLS_PW_HW_LIST_PW_INDEX
#define L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX MPLS_PW_HW_LIST_LCL_LDP_ENT_INDEX 
#define L2VPN_PW_HW_LIST_IN_LABEL   MPLS_PW_HW_LIST_IN_LABEL
#define L2VPN_PW_HW_LIST_OUT_LABEL   MPLS_PW_HW_LIST_OUT_LABEL        
#define L2VPN_PW_HW_LIST_PEER_ADDRESS  MPLS_PW_HW_LIST_PEER_ADDRESS
#define L2VPN_PW_HW_LIST_REMOTE_VE_ID  MPLS_PW_HW_LIST_REMOTE_VE_ID
#define L2VPN_PW_HW_LIST_OUT_IF_INDEX  MPLS_PW_HW_LIST_OUT_IF_INDEX 
#define L2VPN_PW_HW_LIST_OUT_PORT   MPLS_PW_HW_LIST_OUT_PORT 
#define L2VPN_PW_HW_LIST_LSP_IN_LABEL  MPLS_PW_HW_LIST_LSP_IN_LABEL   
#define L2VPN_PW_HW_LIST_LSP_OUT_LABEL  MPLS_PW_HW_LIST_LSP_OUT_LABEL   
#define L2VPN_PW_HW_LIST_IN_IF_INDEX  MPLS_PW_HW_LIST_IN_IF_INDEX     
#define L2VPN_PW_HW_LIST_AC_ID  MPLS_PW_HW_LIST_AC_ID     
#define L2VPN_PW_HW_LIST_NPAPI_STATUS  MPLS_PW_HW_LIST_NPAPI_STATUS
#define L2VPN_PW_HW_LIST_STALE_STATUS  MPLS_PW_HW_LIST_STALE_STATUS
#define L2VPN_PW_HW_LIST_PEER_ADDRESS_TYPE MPLS_PW_HW_LIST_PEER_ADDRESS_TYPE
#define L2VPN_PW_HW_LIST_PEER_IP_ADDRESS   MPLS_PW_HW_LIST_PEER_IP_ADDRESS 
#define L2VPN_PW_HW_LIST_PEER_IPV6_ADDRESS MPLS_PW_HW_LIST_PEER_IPV6_ADDRESS
#define L2VPN_PW_HW_LIST_IS_STATIC_PW MPLS_PW_HW_LIST_IS_STATIC_PW
#define L2VPN_PW_STATUS_NOT_STALE   MPLS_PW_STATUS_NOT_STALE
#define L2VPN_PW_STATUS_STALE    MPLS_PW_STATUS_STALE
#define L2VPN_HA_IS_SWITCHOVER      TRUE
#define L2VPN_PWVC_NPAPI_CALLED   MPLS_PWVC_NPAPI_CALLED  
#define L2VPN_PWVC_NPAPI_SUCCESS  MPLS_PWVC_NPAPI_SUCCESS 
#define L2VPN_PWILM_NPAPI_CALLED  MPLS_PWILM_NPAPI_CALLED 
#define L2VPN_PWILM_NPAPI_SUCCESS  MPLS_PWILM_NPAPI_SUCCESS
#define L2VPN_PWAC_NPAPI_CALLED   MPLS_PWAC_NPAPI_CALLED
#define L2VPN_PWAC_NPAPI_SUCCESS  MPLS_PWAC_NPAPI_SUCCESS
#define L2VPN_PWALL_NPAPI_SET   MPLS_PWALL_NPAPI_SET
#define L2VPN_VPLS_NPAPI_CALLED                       MPLS_L2VPN_VPLS_NPAPI_CALLED
#define L2VPN_VPLS_NPAPI_SUCCESS        MPLS_L2VPN_VPLS_NPAPI_SUCCESS
#define L2VPN_VPLS_NPAPI_ALL                  MPLS_L2VPN_VPLS_NPAPI_ALL

#endif
#endif /*_L2VP_MACS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file pwmacs.h                              */
/*---------------------------------------------------------------------------*/
