/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mspwlwg.h,v 1.2 2011/03/07 11:24:27 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _L2VPMSPWLWG_H
#define _L2VPMSPWLWG_H

/* Structure used by CLI to indicate which 
	all objects to be set in FsMsPwConfigTable */

typedef struct
{
	BOOL1  bFsMsPwIndex1;
	BOOL1  bFsMsPwIndex2;
	BOOL1  bFsMsPwRowStatus;
} tL2vpnIsSetFsMsPwConfigTable;

/* Structure used by L2vpn protocol for FsMsPwConfigTable */

typedef struct
{
	tRBNodeEmbd  FsMsPwConfigTableNode;
	UINT4 u4FsMsPwIndex1;
	UINT4 u4FsMsPwIndex2;
	INT4 i4FsMsPwOperStatus;
	INT4 i4FsMsPwRowStatus;
} tL2vpnFsMsPwConfigTable;


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsPwMaxEntries ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsPwMaxEntries ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsPwMaxEntries ARG_LIST((UINT4 * , UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsPwMaxEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMsPwConfigTable. */
INT1
nmhValidateIndexInstanceFsMsPwConfigTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMsPwConfigTable  */

INT1
nmhGetFirstIndexFsMsPwConfigTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMsPwConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMsPwOperStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMsPwRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMsPwRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMsPwRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMsPwConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif
