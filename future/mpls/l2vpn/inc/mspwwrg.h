/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mspwwrg.h,v 1.1 2010/11/17 14:03:17 siva Exp $
*
* Description: 
* *********************************************************************/

#ifndef _L2VPNWR_H
#define _L2VPNWR_H
VOID RegisterFSMSPW (VOID);
VOID UnRegisterFSMSPW (VOID);
INT4 FsMsPwMaxEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMsPwMaxEntriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsPwMaxEntriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMsPwMaxEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMsPwConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMsPwOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsPwRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMsPwRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMsPwRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMsPwConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _L2VPNWR_H */
