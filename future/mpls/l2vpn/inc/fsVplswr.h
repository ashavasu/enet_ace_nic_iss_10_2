#ifndef _FSVPLSWR_H
#define _FSVPLSWR_H
INT4 GetNextIndexVplsBgpConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSVPLS(VOID);

VOID UnRegisterFSVPLS(VOID);
INT4 VplsBgpConfigVERangeSizeGet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpConfigVERangeSizeSet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpConfigVERangeSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VplsBgpConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexVplsBgpVETable(tSnmpIndex *, tSnmpIndex *);
INT4 VplsBgpVENameGet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVEPreferenceGet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVERowStatusGet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVEStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVENameSet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVEPreferenceSet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVERowStatusSet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVEStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpVENameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VplsBgpVEPreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VplsBgpVERowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VplsBgpVEStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VplsBgpVETableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexVplsBgpPwBindTable(tSnmpIndex *, tSnmpIndex *);
INT4 VplsBgpPwBindLocalVEIdGet(tSnmpIndex *, tRetVal *);
INT4 VplsBgpPwBindRemoteVEIdGet(tSnmpIndex *, tRetVal *);
#endif /* _FSVPLSWR_H */
