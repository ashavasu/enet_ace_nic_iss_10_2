/*-----------------------------------------------------------------------------
 * $Id: l2vpclip.h,v 1.42 2015/11/17 10:36:16 siva Exp $
 * FILE  NAME             : l2vpclip.h
 * PRINCIPAL AUTHOR       : Aricent Inc.
 * SUBSYSTEM NAME         : MPLS
 * MODULE NAME            : L2VPN
 * LANGUAGE               : ANSI-C
 * TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)
 * DATE OF FIRST RELEASE  :
 * DESCRIPTION            : This file contains the prototypes of functions
 *                             present in l2vpcli.c
 *----------------------------------------------------------------------------*/
#ifndef __L2VPCLIP_H__
#define __L2VPCLIP_H__

typedef struct _L2vpCliArgs
{
    tSNMP_OCTET_STRING_TYPE       Saii;
    tSNMP_OCTET_STRING_TYPE       Taii;
    tSNMP_OCTET_STRING_TYPE       Agi;
    UINT4                         u4Flag;
    uGenU4Addr      PeerAddr;
    UINT4                         u4LocalLabel;
    UINT4                         u4RemoteLabel;
    UINT4                         u4OutTunnelId;
    UINT4                         u4OutDestTunnelId;
    UINT4                         u4OutSrcTunnelId;
    UINT4                         u4InTunnelId;
    UINT4                         u4InDestTunnelId;
    UINT4                         u4InSrcTunnelId;
    UINT4                         u4PwOwner;
    UINT4                         u4VlanId;
    UINT4                         u4PwVlanId;
    UINT4                         u4PwId;
    UINT4                         u4PwIndex;
    UINT4                         u4LocalGroupId;
    UINT4                         u4VplsIndex;
    UINT4                         u4EntityId;
    UINT4                         u4SrcGlobalId;
    UINT4                         u4SrcNodeId;
    UINT4                         u4SrcAcId;
    UINT4                         u4DestGlobalId;
    UINT4                         u4DestNodeId;
    UINT4                         u4DestAcId;
    UINT4                         u4LocalCapabAdvert;
    UINT4                         u4LocalCCAdvert;
    UINT4                         u4LocalCVAdvert;
    UINT4                         u4RemoteCCAdvert;
    UINT4                         u4RemoteCVAdvert;
    UINT4                         u4GenAgiType;
    UINT4                         u4GenLocalAiiType;
    UINT4                         u4GenRemoteAiiType;   
    UINT4                         u4InactiveFlag;
    UINT4                         u4PwRedGrpId;
    INT4                          i4PeerAddrType;
    INT4                          i4PortIfIndex;
    INT4                          i4PwType;
    INT4                          i4VlanMode;
    INT4                          i4PwMode;
    INT4                          i4OamStatus;
    INT4                          i4CtrlWordStatus;   
    INT4                          i4PwStatusNotif;
    INT4                          i4PwOamStatusNotif;
    INT4                          i4RedPwPriority;
    UINT1                         u1AiiFormat;
    UINT1                         u1RedPwPreference;
    UINT1                         u1IsBackupPw;
    UINT1                         u1PwStatus;
    UINT4                         u4Mtu;
    INT4                          i4HoldingPriority;
#ifdef HVPLS_WANTED
    UINT1                         u1SplitHorizonStatus;
    UINT1                         au1Reserved[3];
#endif
 
}
tL2vpnCliArgs;

/* structure used during CLI display */
typedef struct 
{
    tPwVcEntry *pPwVcEntry;
    UINT4 u4DispStatus;  /* To display detailed info OR summary */
    uGenU4Addr PeerAddr;
    UINT4 u4VcId;        /* Values given in CLI command */
    UINT4 u4PwIndex;     /* Values given in CLI command */
}tCliShowPw;

/* structure for storing the pseudowire redundancy related parameter */
typedef struct
{
    UINT4           u4PwRedGrpId;
    UINT4           u4PwRedIccpGrpId;
    UINT4           u4PwRedForceActPw;
    UINT1           u1PwRedGrpName[32];
    UINT2           u2PwRedNegTimeOut;
    UINT1           u1ReversionStatus;
    UINT1           u1ProtType;
    UINT1           u1IsPwRedEnabled;
    UINT1           u1PwRedSyncStatus;
    UINT1           u1PwRedNegTimeOut;
    UINT1           u1PwRedWtrVal;
    UINT1           u1PwRedContResType;
    UINT1           u1PwRedLockProtect;
    UINT1           u1PwRedStatus;
    UINT1           u1MultiHomingApps;
}tL2vpnCliPwRedArgs;

/* Function prototypes  of l2vpcli.c */
INT4
L2VpnPwVcCreate (tCliHandle CliHandle, tL2vpnCliArgs *pL2vpnCliArgs); 
INT4 L2VpnCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
INT4
L2VpnPwVcShowEnetInfo(tCliHandle CliHandle, tCliShowPw *pCliShowPw, BOOL1 *pb1TitleWanted);  
INT4
L2VpnCliVcOamConfigure (tCliHandle CliHandle, tL2vpnCliArgs *pL2vpnCliArgs); 
INT4
L2VpnCliConfigGlobalCcTypes(tCliHandle CliHandle, UINT4 u4GlobCcFlag);
INT4
L2VpnCliConfigGlobalCvTypes(tCliHandle CliHandle, UINT4 u4GlobCvFlag);
INT4
L2VpnCliDisableGlobalCcTypes(tCliHandle CliHandle);
INT4
L2VpnCliDisableGlobalCvTypes(tCliHandle CliHandle);
INT4
L2VpnCliShowVccvCapabs(tCliHandle CliHandle);
INT4
L2VpnCliShowHwCapabs(tCliHandle CliHandle);
PUBLIC INT4
L2VpnCliEnableDebug (tCliHandle CliHandle, INT4 i4Val);
PUBLIC INT4
L2VpnCliDisableDebug (tCliHandle CliHandle, INT4 i4Val);
INT4
L2VpnPwVcCreateVcOnly (tCliHandle CliHandle,UINT4 u4Flag, UINT4 u4PeerAddr, UINT4 u4InBoundLabel, UINT4 u4OutBoundLabel);
INT4
L2VpnPwVcDelete (tCliHandle CliHandle, tL2vpnCliArgs *pL2vpnCliArgs);
INT4
L2VpnPwVcShow (tCliHandle CliHandle, tL2vpnCliArgs *pL2vpnCliArgs);
INT4 PwMplsTypeSet(tCliHandle CliHandle,UINT4 u4PwIndex,UINT4 u4Flag);
INT4 PwDestroy(tCliHandle CliHandle,UINT4 u4PwIndex);
INT4 PwEnetTablePopulate(tCliHandle CliHandle,UINT4 u4PwIndex,
                         UINT4 u4PwEnetPwVlan, UINT4 u4PwEnetPortVlan,
                         INT4 i4PwEnetVlanMode,INT4 i4IfIndex);
INT4 PwOutboundTablePopulate(tCliHandle CliHandle,UINT4 u4PwIndex,
                              tL2vpnCliArgs *pL2vpnCliArgs);
INT4 PwTablePopulate(tCliHandle CliHandle,UINT4 u4PwIndex, tL2vpnCliArgs *pL2vpnCliArgs);
INT4 PwUpdtOamParams (tCliHandle CliHandle, tL2vpnCliArgs *pL2vpnCliArgs, tPwVcEntry *pPwVcEntry);

VOID CliShowPwVCStatistics(tCliHandle CliHandle,UINT4 u4PwIndex);
INT4 CliShowPwDetail(tCliHandle CliHandle,UINT4 u4PwIndex,UINT4 u4PwEnetInstance,tGenU4Addr GenU4Addr,UINT4 u4Flag);
VOID
CliShowPwSummary (UINT4 u4PwIndex, UINT4 *pu4OperUnknown,
                  UINT4 *pu4OperUp, UINT4 *pu4OperDown, UINT4 *pu4OperDormant,
                  UINT4 *pu4OperLldown,  UINT4 *pu4AdminDown);
INT4
SetL2VpnPwMode(tCliHandle CliHandle,UINT4 u4PwIndex,INT4 i4Mode);

INT4
L2VpnCliSetSigProtocol ARG_LIST ((tCliHandle CliHandle, UINT4 u4Flag));  

INT4 L2VpnCliVfiCreate ARG_LIST ((tCliHandle CliHandle, UINT1 *pu1VfiName, UINT4 *pu4VfiMode));
INT4 L2VpnCliVfiDelete ARG_LIST ((tCliHandle CliHandle, UINT1 *pu1VfiName, UINT4 *pu4VfiMode));
INT4 L2VpnCliVfiModeChange ARG_LIST ((tCliHandle CliHandle,UINT1 *pu1VfiName));
/* VPLS FDB */
INT4 L2VpnCliVpnCreate ARG_LIST ((tCliHandle CliHandle, UINT4 u4VpnId, 
                                  UINT4 u4VplsFdbId));
INT4 L2VpnCliVpnDelete ARG_LIST ((tCliHandle CliHandle, UINT4 u4VpnId));
INT4 L2VpnCliNeighborCreate ARG_LIST ((tCliHandle CliHandle, 
                                       tL2vpnCliArgs *pL2vpnCliArgs));
INT4 L2VpnCliNeighborDelete ARG_LIST ((tCliHandle CliHandle,
                                       tL2vpnCliArgs *pL2vpnCliArgs));
INT4 L2VpnCliXCCreate ARG_LIST ((tCliHandle CliHandle, UINT1 *pu1VfiName, 
                                 tL2vpnCliArgs *pL2vpnCliArgs));
INT4 L2VpnCliXCDelete ARG_LIST ((tCliHandle CliHandle, UINT1 *pu1VfiName,
                                 UINT4 u4Mode, UINT4 u4IfIndexOrVlan));
INT1 L2VpnCliShowVfiDetails ARG_LIST ((tCliHandle CliHandle,
                                       UINT1 *pu1VfiName));
INT1 L2VpnCliShowVfi ARG_LIST ((tCliHandle CliHandle));

INT4 MplsGetPwModeFromVplsIndex (UINT4, INT4 *);
INT4 MplsPwEnetGet (UINT4, INT4 *, INT4 *, UINT4 *, UINT4 *);
INT4 L2VpnCliSetPwMplsNonTeEntityId (UINT4 u4PwIndex, UINT4 u4EntityId);
INT4 L2VpnCheckForLastPw (tPwVcEntry *pPwVcEntry);

#ifdef VPLSADS_WANTED
INT4
L2VpnCliRdCreate (tCliHandle CliHandle, UINT1 * pu1VplsRd);

INT4
L2VpnCliRdDelete (tCliHandle CliHandle);

INT4
L2VpnCliRtCreate (tCliHandle CliHandle, UINT1 * pu1VplsRt, UINT4 u4VplsRtType);

INT4
L2VpnCliRtDelete (tCliHandle CliHandle, UINT1 * pu1VplsRt);

INT4
L2VpnCliSetControlWord (tCliHandle CliHandle, UINT4 u4VplsControlWord);

INT4
L2VpnCliSetMtu (tCliHandle CliHandle, UINT4 u4VplsMtu);

INT4
L2VpnCliVeCreate (tCliHandle CliHandle, UINT4 u4VplsVeId, UINT1 * pu4VplsVeName);

INT4
L2VpnCliVeDelete (tCliHandle CliHandle, UINT4 u4VplsVeId);

INT4
L2VpnShowVplsHwList (tCliHandle CliHandle);

INT4
L2VpnCreateVplsACMap(tVPLSEntry *pVplsEntry, tL2vpnCliArgs * pL2vpnCliArgs);

INT4
L2VpnDeleteVplsACMap(tVPLSEntry *pVplsEntry, INT4 i4IfIndex, UINT4 u4VlanId);
#endif
#ifdef HVPLS_WANTED
INT4
L2VpnCreateVplsPwBindEntry ( UINT4 u4PwIndex, UINT1 u1SplitHorizonStatus);

 
INT4 L2VpnDelVplsPwBindEntry (UINT4 u4PwIndex);
#endif


/*MS-PW */

#ifdef __L2VPNCLIG_C__
CONST CHR1  *L2vpnCliErrString [] = {
    "\n \n",
    "% PW VC entry not found  \r\n",
    "% Unable to configure PW crossconnection\r\n",
    "\n \n"
};
#endif
INT4 L2VpnMsPwShow (tCliHandle CliHandle, UINT4 u4Flag,tPwVcEntry*,
        tPwVcEntry *);
VOID CliShowMsPwDetail(tCliHandle CliHandle,tL2vpnFsMsPwConfigTable *,
 tPwVcEntry*, tPwVcEntry *);
INT4 L2vpnCliSetFsMsPwMaxEntries (tCliHandle CliHandle,UINT4 *pFsMsPwMaxEntries);
INT4 L2vpnCliSetFsMsPwConfigTable (tCliHandle CliHandle,tL2vpnFsMsPwConfigTable * pL2vpnSetFsMsPwConfigTable,tL2vpnIsSetFsMsPwConfigTable * pL2vpnIsSetFsMsPwConfigTable);

/*MS-PW */

/* PSW-REDUNDANCY prototypes*/
INT4 L2VpCliSetPwRedStatus ARG_LIST ((tCliHandle CliHandle,
                                      tL2vpnCliPwRedArgs *L2vpnCliPwRedArgs));

INT4
L2VpCliSetPwRedSyncNotification ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs));

INT4
L2VpCliSetPwRedPwStatusNotification ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs));

INT4
L2VpCliSetPwRedNegTimeOut ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs));

INT4
L2VpCliConfigPwRedGrp ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT1 u1PwRedGrpStatus));

INT4
L2VpCliConfigPwRedGrpActive ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs, UINT1 u1PwRedGrpStatus));

INT4
L2VpCliSetPwRedGrpRevertion ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliSetPwRedGrpProtType ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliSetPwRedGrpName ARG_LIST ((tCliHandle CliHandle,
                       UINT1* pu1PwRedGrpName,UINT4 u4PwRedGrpId));

INT4
L2VpCliSetPwRedGrpWtrValue ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliSetPwRedContentionMethod ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliSetPwRedLockoutProtect ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));


INT4
L2VpCliSetPwRedNodeStatus ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliForcePwRedActivePw ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliSetPwRedDualHomApps ARG_LIST ((tCliHandle CliHandle,
                       tL2vpnCliPwRedArgs* pL2vpnCliPwRedArgs,UINT4 u4PwRedGrpId));

INT4
L2VpCliConfigPwRedIccpGrp ARG_LIST ((tCliHandle CliHandle,
                        UINT4 u4PwRedIccpId));

INT4
L2VpCliConfigPwRedIccpNeighbor ARG_LIST ((tCliHandle CliHandle,
                        UINT4 u4PwRedIccpId,UINT4 u4IpAddress,UINT1 u1AddrType));


INT4
L2VpCliConfigPwRedIccpNeighborActivate ARG_LIST ((tCliHandle CliHandle,
                        UINT4 u4PwRedIccpId,UINT4 u4IpAddress,UINT1 u1AddrType));


INT4
L2VpCliDeletePwRedIccpNeighbor ARG_LIST ((tCliHandle CliHandle,
                        UINT4 u4PwRedIccpId,UINT4 u4IpAddress,UINT1 u1AddrType));

VOID
L2VpnPwRedShowRunningConfigTable(tCliHandle CliHandle);

VOID
L2VpnSRCPwRedNodeTable(tCliHandle CliHandle);

VOID
L2VpnSRCPwRedGrpTable (tCliHandle CliHandle);

VOID
L2VpnPwRedShowRunningConfigScalar (tCliHandle CliHandle);

INT4
L2VpnShowRunningConfig (tCliHandle CliHandle);

INT4
L2VpCliShowPwRedGlobalInfo (tCliHandle CliHandle);

INT4
L2VpCliShowPwRedGrpInfo(tCliHandle CliHandle, UINT4 u4RedGrpId);

VOID
L2VpnCliShowPwRedConfig(tCliHandle CliHandle, UINT4 u4RedGrpId,UINT4 *pu4PagingStatus);

INT4 
L2VpCliShowPwRedPwInfo (tCliHandle CliHandle, UINT4 *pu4GroupId);

INT4
L2VpCliSetHoldingPriority(UINT4 *pu4ErrorCode, UINT4 u4PwIndex,INT4 i4HoldingPriority);

VOID 
L2VpCliDisplayRedPwInfo (tCliHandle CliHandle, tL2vpnRedundancyPwEntry *pRgPwEntry);

VOID 
L2VpCliShowRedPwStatus (UINT4 u4Status, UINT1 *pu1Status);

INT4 
L2VpCliShowPwRedNbrInfo (tCliHandle CliHandle, UINT4 *pu4GroupId);

VOID 
L2VpCliDisplayPwRedNbrInfo (tCliHandle CliHandle, tL2vpnRedundancyNodeEntry *pNodeEntry);

INT4 
L2VpCliShowPwRedIccpPwInfo (tCliHandle CliHandle, UINT4 *pu4GroupId);

VOID 
L2VpCliDisplayIccpPwInfo (tCliHandle CliHandle,tL2vpnIccpPwEntry *pIccpPwEntry);

INT4
L2VpCliCreateRedPwEntry(tCliHandle CliHandle,UINT4 u4PwIndex,tL2vpnCliArgs *pL2vpnCliArgs);

INT4 L2VpCliDeleteRedPwEntry(tCliHandle CliHandle,UINT4 u4PwIndex,
                              tL2vpnCliArgs * pL2vpnCliArgs,
                              UINT1 u1RedPwRowStatus);
INT4
L2VpCliCheckIfPwIsBackup(UINT4 u4PwEnetPortVlan,INT4 i4PortIfIndex, UINT4 *pu4PwIndex);

INT4
L2VpnPwVcDelBasedOnOwner(tCliHandle CliHandle,tL2vpnCliArgs *pL2vpnCliArgs,tPwVcEntry *pPwVcEntry);

INT4 L2VpnConfigPwPref (tCliHandle CliHandle,tL2vpnCliArgs *pL2vpnCliArgs);
INT4 L2VpnCheckforAC( tL2vpnCliArgs * pL2vpnCliArgs );
#endif /*__L2VPNCLIP_H__*/
