/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwmlw.h,v 1.4 2008/08/20 15:14:26 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for PwMplsTable. */
INT1
nmhValidateIndexInstancePwMplsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwMplsTable  */

INT1
nmhGetFirstIndexPwMplsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwMplsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwMplsMplsType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsExpBitsMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwMplsExpBits ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwMplsTtl ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwMplsLocalLdpID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsLocalLdpEntityID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsPeerLdpID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPwMplsMplsType ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwMplsExpBitsMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwMplsExpBits ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwMplsTtl ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwMplsLocalLdpID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwMplsLocalLdpEntityID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwMplsStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PwMplsMplsType ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwMplsExpBitsMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwMplsExpBits ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwMplsTtl ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwMplsLocalLdpID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwMplsLocalLdpEntityID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwMplsStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PwMplsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PwMplsOutboundTable. */
INT1
nmhValidateIndexInstancePwMplsOutboundTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwMplsOutboundTable  */

INT1
nmhGetFirstIndexPwMplsOutboundTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwMplsOutboundTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwMplsOutboundLsrXcIndex ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsOutboundTunnelIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwMplsOutboundTunnelInstance ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwMplsOutboundTunnelLclLSR ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsOutboundTunnelPeerLSR ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwMplsOutboundIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwMplsOutboundTunnelTypeInUse ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPwMplsOutboundLsrXcIndex ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwMplsOutboundTunnelIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwMplsOutboundTunnelLclLSR ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwMplsOutboundTunnelPeerLSR ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwMplsOutboundIfIndex ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PwMplsOutboundLsrXcIndex ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwMplsOutboundTunnelIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwMplsOutboundTunnelLclLSR ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwMplsOutboundTunnelPeerLSR ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwMplsOutboundIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PwMplsOutboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PwMplsInboundTable. */
INT1
nmhValidateIndexInstancePwMplsInboundTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwMplsInboundTable  */

INT1
nmhGetFirstIndexPwMplsInboundTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwMplsInboundTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwMplsInboundXcIndex ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for PwMplsNonTeMappingTable. */
INT1
nmhValidateIndexInstancePwMplsNonTeMappingTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwMplsNonTeMappingTable  */

INT1
nmhGetFirstIndexPwMplsNonTeMappingTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwMplsNonTeMappingTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for PwMplsTeMappingTable. */
INT1
nmhValidateIndexInstancePwMplsTeMappingTable ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwMplsTeMappingTable  */

INT1
nmhGetFirstIndexPwMplsTeMappingTable ARG_LIST((UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwMplsTeMappingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
