/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: l2vpnsz.h,v 1.12 2016/01/29 13:14:45 siva Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
enum {
    MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY_SIZING_ID,
    MAX_L2VPN_DATA_BUFFER_SIZING_ID,
    MAX_L2VPN_ENET_ENTRIES_SIZING_ID,
    MAX_L2VPN_ENET_PRIMAPPING_ENTRIES_SIZING_ID,
    MAX_L2VPN_ENET_SERV_SPEC_ENTRY_SIZING_ID,
    MAX_L2VPN_IF_DESCRIPTOR_SIZING_ID,
    MAX_L2VPN_MPLS_API_IN_INFO_ENTRIES_SIZING_ID,
    MAX_L2VPN_MPLS_API_OUT_INFO_ENTRIES_SIZING_ID,
    MAX_L2VPN_MPLS_INMAPPING_ENTRIES_SIZING_ID,
    MAX_L2VPN_MPLS_PORT_ENTRY_INFO_SIZING_ID,
    MAX_L2VPN_MS_PW_ENTRIES_SIZING_ID,
    MAX_L2VPN_PW_VC_DORMANT_ENTRIES_SIZING_ID,
    MAX_L2VPN_PW_VC_ENTRIES_SIZING_ID,
    MAX_L2VPN_PW_VC_INFO_SIZING_ID,
    MAX_L2VPN_PW_VC_LBLMSG_ENTRIES_SIZING_ID,
    MAX_L2VPN_PW_VC_MPLS_ENTRY_SIZING_ID,
    MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES_SIZING_ID,
    MAX_L2VPN_REDUNDANCY_SIZING_ID,
    MAX_L2VPN_REDUNDANCY_NODE_SIZING_ID,
    MAX_L2VPN_REDUNDANCY_PW_SIZING_ID,
    MAX_L2VPN_ICCP_PW_SIZING_ID,
    MAX_L2VPN_ICCP_PWVC_REQUESTS_SIZING_ID,
    MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES_SIZING_ID,
    MAX_L2VPN_Q_MSG_SIZING_ID,
    MAX_L2VPN_VPLS_ENTRIES_SIZING_ID,
    MAX_L2VPN_VPLS_INFO_SIZING_ID,
    MAX_L2VPN_IFINDEX_ENTRIES_SIZING_ID,
#ifdef HVPLS_WANTED
    MAX_L2VPN_VPLS_BIND_ENTRIES_SIZING_ID,
#endif
#ifdef VPLSADS_WANTED
    MAX_L2VPN_VPLS_RD_ENTRIES_SIZING_ID,
    MAX_L2VPN_VPLS_RT_ENTRIES_SIZING_ID,
    MAX_L2VPN_VPLS_VE_ENTRIES_SIZING_ID,
    MAX_L2VPN_VPLS_AC_ENTRIES_SIZING_ID,
    MAX_L2VPN_VPLS_LB_ENTRIES_SIZING_ID,
#endif
 L2VPN_MAX_SIZING_ID
};


#ifdef  _L2VPNSZ_C
tMemPoolId L2VPNMemPoolIds[ L2VPN_MAX_SIZING_ID];
INT4  L2vpnSizingMemCreateMemPools(VOID);
VOID  L2vpnSizingMemDeleteMemPools(VOID);
INT4  L2vpnSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _L2VPNSZ_C  */
extern tMemPoolId L2VPNMemPoolIds[ ];
extern INT4  L2vpnSizingMemCreateMemPools(VOID);
extern VOID  L2vpnSizingMemDeleteMemPools(VOID);
extern INT4  L2vpnSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _L2VPNSZ_C  */


#ifdef  _L2VPNSZ_C
tFsModSizingParams FsL2VPNSizingParams [] = {
{ "tPwVcActivePeerSsnEntry", "MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY", sizeof(tPwVcActivePeerSsnEntry),MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY, MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY,0 },
{ "tL2vpnDefMTUSize", "MAX_L2VPN_DATA_BUFFER", sizeof(tL2vpnDefMTUSize),MAX_L2VPN_DATA_BUFFER, MAX_L2VPN_DATA_BUFFER,0 },
{ "tPwVcEnetEntry", "MAX_L2VPN_ENET_ENTRIES", sizeof(tPwVcEnetEntry),MAX_L2VPN_ENET_ENTRIES, MAX_L2VPN_ENET_ENTRIES,0 },
{ "tPwVcEnetMplsPriMappingEntry", "MAX_L2VPN_ENET_PRIMAPPING_ENTRIES", sizeof(tPwVcEnetMplsPriMappingEntry),MAX_L2VPN_ENET_PRIMAPPING_ENTRIES, MAX_L2VPN_ENET_PRIMAPPING_ENTRIES,0 },
{ "tPwVcEnetServSpecEntry", "MAX_L2VPN_ENET_SERV_SPEC_ENTRY", sizeof(tPwVcEnetServSpecEntry),MAX_L2VPN_ENET_SERV_SPEC_ENTRY, MAX_L2VPN_ENET_SERV_SPEC_ENTRY,0 },
{ "tPwVcMaxIfStrLenSize", "MAX_L2VPN_IF_DESCRIPTOR", sizeof(tPwVcMaxIfStrLenSize),MAX_L2VPN_IF_DESCRIPTOR, MAX_L2VPN_IF_DESCRIPTOR,0 },
{ "tMplsApiInInfo", "MAX_L2VPN_MPLS_API_IN_INFO_ENTRIES", sizeof(tMplsApiInInfo),MAX_L2VPN_MPLS_API_IN_INFO_ENTRIES, MAX_L2VPN_MPLS_API_IN_INFO_ENTRIES,0 },
{ "tMplsApiOutInfo", "MAX_L2VPN_MPLS_API_OUT_INFO_ENTRIES", sizeof(tMplsApiOutInfo),MAX_L2VPN_MPLS_API_OUT_INFO_ENTRIES, MAX_L2VPN_MPLS_API_OUT_INFO_ENTRIES,0 },
{ "tPwVcMplsInMappingEntry", "MAX_L2VPN_MPLS_INMAPPING_ENTRIES", sizeof(tPwVcMplsInMappingEntry),MAX_L2VPN_MPLS_INMAPPING_ENTRIES, MAX_L2VPN_MPLS_INMAPPING_ENTRIES,0 },
{ "tMplsPortEntryInfo", "MAX_L2VPN_MPLS_PORT_ENTRY_INFO", sizeof(tMplsPortEntryInfo),MAX_L2VPN_MPLS_PORT_ENTRY_INFO, MAX_L2VPN_MPLS_PORT_ENTRY_INFO,0 },
{ "tL2vpnFsMsPwConfigTable", "MAX_L2VPN_MS_PW_ENTRIES", sizeof(tL2vpnFsMsPwConfigTable),MAX_L2VPN_MS_PW_ENTRIES, MAX_L2VPN_MS_PW_ENTRIES,0 },
{ "tPwVcDormantVcEntry", "MAX_L2VPN_PW_VC_DORMANT_ENTRIES", sizeof(tPwVcDormantVcEntry),MAX_L2VPN_PW_VC_DORMANT_ENTRIES, MAX_L2VPN_PW_VC_DORMANT_ENTRIES,0 },
{ "tPwVcEntry", "MAX_L2VPN_PW_VC_ENTRIES", sizeof(tPwVcEntry),MAX_L2VPN_PW_VC_ENTRIES, MAX_L2VPN_PW_VC_ENTRIES,0 },
{ "tPwVcInfoSize", "MAX_L2VPN_PW_VC_INFO", sizeof(tPwVcInfoSize),MAX_L2VPN_PW_VC_INFO, MAX_L2VPN_PW_VC_INFO,0 },
{ "tPwVcLblMsgEntry", "MAX_L2VPN_PW_VC_LBLMSG_ENTRIES", sizeof(tPwVcLblMsgEntry),MAX_L2VPN_PW_VC_LBLMSG_ENTRIES, MAX_L2VPN_PW_VC_LBLMSG_ENTRIES,0 },
{ "tPwVcMplsEntry", "MAX_L2VPN_PW_VC_MPLS_ENTRY", sizeof(tPwVcMplsEntry),MAX_L2VPN_PW_VC_MPLS_ENTRY, MAX_L2VPN_PW_VC_MPLS_ENTRY,0 },
{ "tPwVcMplsMappingEntry", "MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES", sizeof(tPwVcMplsMappingEntry),MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES, MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES,0 },
{ "tL2vpnRedundancyEntry",      "MAX_L2VPN_REDUNDANCY_ENTRIES",         sizeof (tL2vpnRedundancyEntry),     MAX_L2VPN_REDUNDANCY_ENTRIES,       MAX_L2VPN_REDUNDANCY_ENTRIES, 0 },  /* PW-RED SIZING INFO. */
{ "tL2vpnRedundancyNodeEntry",  "MAX_L2VPN_REDUNDANCY_NODE_ENTRIES",    sizeof (tL2vpnRedundancyNodeEntry), MAX_L2VPN_REDUNDANCY_NODE_ENTRIES,  MAX_L2VPN_REDUNDANCY_NODE_ENTRIES, 0 },
{ "tL2vpnRedundancyPwEntry",    "MAX_L2VPN_REDUNDANCY_PW_ENTRIES",      sizeof (tL2vpnRedundancyPwEntry),   MAX_L2VPN_REDUNDANCY_PW_ENTRIES,    MAX_L2VPN_REDUNDANCY_PW_ENTRIES, 0 },
{ "tL2vpnIccpPwEntry",          "MAX_L2VPN_ICCP_PW_ENTRIES",            sizeof (tL2vpnIccpPwEntry),         MAX_L2VPN_ICCP_PW_ENTRIES,          MAX_L2VPN_ICCP_PW_ENTRIES, 0 },
{ "tPwRedDataEvtPwFec",         "MAX_L2VPN_ICCP_PWVC_REQUESTS",         sizeof (tPwRedDataEvtPwFec),        MAX_L2VPN_ICCP_PWVC_REQUESTS,       MAX_L2VPN_ICCP_PWVC_REQUESTS, 0 },
{ "tPwRedDataEvtPwData",        "MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES",     sizeof (tPwRedDataEvtPwData),       MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES,   MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES, 0 },
{ "tL2VpnQMsg", "MAX_L2VPN_Q_MSG", sizeof(tL2VpnQMsg),MAX_L2VPN_Q_MSG, MAX_L2VPN_Q_MSG,0 },
{ "tVPLSEntry", "MAX_L2VPN_VPLS_ENTRIES", sizeof(tVPLSEntry),MAX_L2VPN_VPLS_ENTRIES, MAX_L2VPN_VPLS_ENTRIES,0 },
{ "tVPLSInfoSize", "MAX_L2VPN_VPLS_INFO", sizeof(tVPLSInfoSize),MAX_L2VPN_VPLS_INFO, MAX_L2VPN_VPLS_INFO,0 },
{ "tPwIfIndexMap", "MAX_L2VPN_IFINDEX_ENTRIES", sizeof(tPwIfIndexMap),MAX_L2VPN_IFINDEX_ENTRIES, MAX_L2VPN_IFINDEX_ENTRIES,0 },
#ifdef HVPLS_WANTED
{ "tVplsPwBindEntry", "MAX_L2VPN_VPLS_BIND_ENTRIES", sizeof(tVplsPwBindEntry),MAX_L2VPN_VPLS_BIND_ENTRIES, MAX_L2VPN_VPLS_BIND_ENTRIES,0 },
#endif
#ifdef VPLSADS_WANTED
{ "tVPLSRdEntry", "MAX_L2VPN_VPLS_RD_ENTRIES", sizeof(tVPLSRdEntry),MAX_L2VPN_VPLS_RD_ENTRIES, MAX_L2VPN_VPLS_RD_ENTRIES,0 },
{ "tVPLSRtEntry", "MAX_L2VPN_VPLS_RT_ENTRIES", sizeof(tVPLSRtEntry),MAX_L2VPN_VPLS_RT_ENTRIES, MAX_L2VPN_VPLS_RT_ENTRIES,0 },
{ "tVPLSVeEntry", "MAX_L2VPN_VPLS_VE_ENTRIES", sizeof(tVPLSVeEntry),MAX_L2VPN_VPLS_VE_ENTRIES, MAX_L2VPN_VPLS_VE_ENTRIES,0 },
{ "tVplsAcMapEntry", "MAX_L2VPN_VPLS_AC_ENTRIES", sizeof(tVplsAcMapEntry),MAX_L2VPN_VPLS_AC_ENTRIES, MAX_L2VPN_VPLS_AC_ENTRIES,0 },
{ "tVPLSLBInfo", "MAX_L2VPN_VPLS_LB_ENTRIES", MAX_L2VPN_VPLS_LB_BLOCK_SIZE,MAX_L2VPN_VPLS_ENTRIES, MAX_L2VPN_VPLS_ENTRIES,0 },
#endif
{"\0","\0",0,0,0,0}
};
#else  /*  _L2VPNSZ_C  */
extern tFsModSizingParams FsL2VPNSizingParams [];
#endif /*  _L2VPNSZ_C  */


