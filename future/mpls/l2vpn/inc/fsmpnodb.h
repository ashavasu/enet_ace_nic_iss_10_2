/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpnodb.h,v 1.2 2010/10/21 09:24:57 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPNODB_H
#define _FSMPNODB_H


UINT4 fsmpno [] ={1,3,6,1,4,1,2076,13,10};
tSNMP_OID_TYPE fsmpnoOID = {9, fsmpno};


UINT4 FsMplsPwStatusNotifEnable [ ] ={1,3,6,1,4,1,2076,13,10,1,1};
UINT4 FsMplsPwOAMStatusNotifEnable [ ] ={1,3,6,1,4,1,2076,13,10,1,2};




tMbDbEntry fsmpnoMibEntry[]= {

{{11,FsMplsPwStatusNotifEnable}, NULL, FsMplsPwStatusNotifEnableGet, FsMplsPwStatusNotifEnableSet, FsMplsPwStatusNotifEnableTest, FsMplsPwStatusNotifEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsPwOAMStatusNotifEnable}, NULL, FsMplsPwOAMStatusNotifEnableGet, FsMplsPwOAMStatusNotifEnableSet, FsMplsPwOAMStatusNotifEnableTest, FsMplsPwOAMStatusNotifEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fsmpnoEntry = { 2, fsmpnoMibEntry };

#endif /* _FSMPNODB_H */

