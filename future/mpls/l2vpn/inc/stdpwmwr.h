#ifndef _STDPWMWR_H
#define _STDPWMWR_H
INT4 GetNextIndexPwMplsTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDPWM(VOID);
INT4 PwMplsMplsTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsExpBitsModeGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsExpBitsGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsTtlGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsLocalLdpIDGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsLocalLdpEntityIDGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsPeerLdpIDGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsMplsTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsExpBitsModeSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsExpBitsSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsTtlSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsLocalLdpIDSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsLocalLdpEntityIDSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsMplsTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsExpBitsModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsExpBitsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsTtlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsLocalLdpIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsLocalLdpEntityIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexPwMplsOutboundTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwMplsOutboundLsrXcIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelInstanceGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelLclLSRGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelPeerLSRGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelTypeInUseGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundLsrXcIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelLclLSRSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelPeerLSRSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundLsrXcIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelLclLSRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTunnelPeerLSRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwMplsOutboundTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexPwMplsInboundTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwMplsInboundXcIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwMplsNonTeMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwMplsNonTeMappingDirectionGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsNonTeMappingXcIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsNonTeMappingIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsNonTeMappingVcIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwMplsTeMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwMplsTeMappingTunnelIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsTeMappingTunnelInstanceGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsTeMappingTunnelPeerLsrIDGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsTeMappingTunnelLocalLsrIDGet(tSnmpIndex *, tRetVal *);
INT4 PwMplsTeMappingVcIndexGet(tSnmpIndex *, tRetVal *);
#endif /* _STDPWMWR_H */
