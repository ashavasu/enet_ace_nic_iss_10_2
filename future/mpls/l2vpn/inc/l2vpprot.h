/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpprot.h,v 1.83 2016/07/28 07:47:33 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpprot.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains function prototypes for L2VPN
 *                              Module.
 *---------------------------------------------------------------------------*/

#ifndef _L2VPPROT_H 
#define _L2VPPROT_H
INT4 StartL2VpnTask ARG_LIST ((VOID));

VOID L2vpnRegisterL2vpnMibs ( VOID );

INT4 L2VpnProcessL2VpnAdminEvent
ARG_LIST ((tL2VpnAdminEvtInfo * pL2VpnAdminEvtInfo));

VOID L2VpnProcessQMsgs ARG_LIST ((VOID));
VOID L2VpnDeleteHashTable
ARG_LIST ((tTMO_HASH_TABLE * pTable, tMemPoolId PoolId));

INT4 L2VpnDisableL2VpnService ARG_LIST ((UINT4 u4AdminStatus));

INT4 L2VpnEnableL2VpnService ARG_LIST ((VOID));

INT4 L2VpnProcessPwVcAdminEvent
ARG_LIST ((tPwVcAdminEvtInfo *pPwVcAdminEvtInfo));

INT4 L2VpnProcessEnetServAdminEvent
ARG_LIST ((tPwVcEnetAdminEvtInfo *pPwVcEnetAdminEvtInfo));

INT4 L2VpnProcessServAdminEvent
ARG_LIST ((tPwVcServSpecAdminEvtInfo *pPwVcServAdminEvtInfo));

INT4 L2VpnProcessLdpNotifEvent
ARG_LIST ((tPwVcNotifEvtInfo  *pNotifInfo));

INT4 L2VpnProcessLdpNotifMsg
ARG_LIST ((tPwVcNotifEvtInfo  *pNotifInfo));

INT4 L2VpnProcessLdpSsnUpEvent
ARG_LIST ((tPwVcSsnEvtInfo *pSsnInfo));

INT4 L2VpnProcessLdpSsnDownEvent ARG_LIST ((tPwVcSsnEvtInfo *pSsnInfo));

INT4 L2VpnProcessSigEvent
ARG_LIST ((tPwVcSigEvtInfo *pPwVcSigEvtInfo));

INT4 L2VpnProcessMplsTeLspEvent
ARG_LIST ((tL2VpnMplsPwVcMplsTeEvtInfo *pMplsTeEvtInfo,
            UINT4 u4Event));

INT4 L2VpnProcessMplsNonTeLspEvent
ARG_LIST ((tL2VpnMplsPwVcMplsNonTeEvtInfo *pLspEvtInfo,
            UINT4 u4Event));

INT4 L2VpnPrcsIngressLspEvt
ARG_LIST ((tL2VpnMplsPwVcMplsNonTeEvtInfo *pLspEvtInfo,
            UINT4 u4Event));

INT4 L2VpnPrcsEgressLspEvt
ARG_LIST ((tL2VpnMplsPwVcMplsNonTeEvtInfo *pLspEvtInfo,
            UINT4 u4Event));

INT4 L2VpnProcessMplsVcEvent
ARG_LIST ((tL2VpnMplsPwVcMplsVcEvtInfo *pVcEvtInfo,
            UINT4 u4Event));

INT4
L2VpnPrcsIngressTnlEvt ARG_LIST ((tL2VpnMplsPwVcMplsTeEvtInfo *pMplsTeEvtInfo, 
            UINT4 u4Event));

INT4 L2VpnHandlePsnTnlUpOrDown ARG_LIST ((tPwVcEntry *pPwVcEntry,
            UINT4 u4Event));

INT4
L2VpnPrcsEgressTnlEvt ARG_LIST ((tL2VpnMplsPwVcMplsTeEvtInfo *pMplsTeEvtInfo,
            UINT4 u4Event));

INT4 L2VpnProcessPsnEvent ARG_LIST ((tPwVcPSNEvtInfo *pPwVcPsnEvtInfo));

INT4 L2VpnProcessIfUpEvent ARG_LIST ((UINT4 u4IfIndex, UINT2 u2PortVlan));

INT4 L2VpnProcessIfDownEvent ARG_LIST ((UINT4 u4IfIndex, UINT2 u2PortVlan));

INT4 L2VpnProcessIfEvent ARG_LIST ((tL2VpnIfEvtInfo *pL2VpnIfEvtInfo));

INT4 L2VpnProcessFwderEvent
ARG_LIST ((tPwVcFwdPlaneEvtInfo *pL2VpnFwdEvtInfo));

INT4 L2VpnProcessRouteEvent ARG_LIST ((tL2VpnRtEvtInfo *pPwRtEvtInfo));

INT4 L2VpnGetMplsEntryStatus
ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1TnlDir));

INT4 L2VpnProcessLdpLblWcWdrawMsg ARG_LIST ((tPwVcLblMsgInfo *pLblInfo));

INT4 L2VpnSendLblMapOrNotifMsg ARG_LIST((tPwVcEntry * pPwVcEntry));

INT4 L2VpnSendLblWdrawOrNotifMsg ARG_LIST((tPwVcEntry * pPwVcEntry));

INT4 L2VpnProcessLdpLblRelMsg ARG_LIST ((tPwVcLblMsgInfo *pLblInfo));

INT4 L2VpnProcessLdpLblWdrawMsg ARG_LIST ((tPwVcLblMsgInfo *pLblInfo));

INT4 L2VpnSendLdpPwVcLblMapMsg ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4 L2VpnSendLdpPwVcLblRelMsg ARG_LIST ((tPwVcEntry *pPwVcEntry, 
            INT1 i1Status));
INT4 L2VpnSendLdpPwVcLblWdrawMsg
ARG_LIST ((tPwVcEntry *pPwVcEntry, INT1 i1Status));

INT4 L2VpnSendLdpPwVcDeleteReq ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4 L2VpnSendLdpPwVcCreateReq ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4 L2VpnSendLdpPeerSsnCreateReq ARG_LIST ((
    uGenAddr *pPeerAddr, INT4 i4PeerAddrType, UINT4 u4LocalLdpEntityID));

INT4 L2VpnSendLdpDeRegReq ARG_LIST ((
    uGenAddr *pPeerAddr, INT4 i4PeerAddrType, UINT4 u4LocalLdpEntityID, UINT4 u4LocalLdpEntityIndex));

INT4 L2VpnAddRemVcToPeerSsnList ARG_LIST ((tPwVcLblMsgEntry *pLblMsg));

INT4 L2VpnGetPeerSession (
        UINT1 u1AddrType, uGenAddr *pAddr, tPwVcActivePeerSsnEntry **ppSession);

INT4 L2VpnProcessLdpLblMapMsg ARG_LIST ((tPwVcLblMsgInfo *pLblInfo));

INT4 L2VpnProcessLdpLblMapReqMsg ARG_LIST ((tPwVcLblMsgInfo *pLblInfo));

tPwVcActivePeerSsnEntry * L2VpnUpdatePeerSsnList 
       ARG_LIST ((tGenU4Addr GenU4Addr, UINT1 u1Action, VOID * pL2vpInEntry));

INT4 L2VpnCheckSsnFromPeerSsnList ARG_LIST ((
            VOID * pL2vpnEntry, BOOL1 b1IsIccpSession));

VOID L2VpnDeletePwVcEntry ARG_LIST ((tPwVcEntry *pPwVcEntry));

VOID L2VpnDeletePwEnetSpecEntry (tPwVcEntry * pPwVcEntry);

INT4 L2VpnCheckIfStatus ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4 L2VpnCheckAcUpStatus ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1UpdateLocalStatus));

INT4 L2VpnEstablishPwVc ARG_LIST ((tPwVcEntry * pPwVcEntry));

VOID L2VpnAddPwVcToPwVcTable ARG_LIST ((tPwVcEntry * pPwVcEntry));

VOID L2VpnDeletePwVcFromPwVcTable ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnSetupPwVc ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnDeletePwVc ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnDownPwVc ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnGetPwVcInfo ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnUpdatePwVcOutboundList
ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1Event));

INT4 L2VpnUpdatePwVcInboundList
ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1Event));

INT4 L2VpnUpdatePwVcOperStatus
ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1Event));


INT4 L2VpnLdpPostEvent ARG_LIST ((tL2VpnLdpPwVcEvtInfo *pPwVcEvtInfo));

INT4 L2VpnProcessPwVcCleanupEvent ARG_LIST ((tTmrAppTimer *pAppTimer));

VOID L2VpnInitPwVcIndexTable ARG_LIST ((VOID));
VOID L2VpnInitPwVcInTnlIndexTable ARG_LIST ((VOID));
VOID L2VpnInitPwVcOutTnlIndexTable ARG_LIST ((VOID));


INT4 L2VpnRegisterWithMplsModule ARG_LIST ((tPwVcEntry *pPwVcEntry, 
            UINT1 u1TnlDir));

INT4 L2VpnDeRegisterWithMplsModule ARG_LIST ((tPwVcEntry *pPwVcEntry, 
            UINT1 u1TnlDir));

INT4 L2VpnUpdateTeTnlAssociation ARG_LIST ((tPwVcEntry *pPwVcEntry, 
            UINT1 u1TnlDir,
            UINT1 u1AssociateFlag));

INT4 L2VpnGetTeLspStatus ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1TnlDir));

INT4 L2VpnGetNonTeLspStatus
ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1TnlDir));

INT4 L2VpnLdpRegisterApplication ARG_LIST ((VOID));

INT4 L2VpnLdpDeRegisterApplication ARG_LIST ((VOID));

INT4 L2VpnFmPwVcSet ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4 L2VpnFmUpdatePwVcStatus
ARG_LIST ((tPwVcEntry *pPwVcEntry, UINT1 u1PwOperStatus, UINT1 u1OperApp));

INT4
L2VpnUpdatePwVcStatus ARG_LIST ((tPwVcEntry * pPwVcEntry, 
            UINT1 u1PwOperStatus, VOID *pSInfo,
            UINT1 u1OperApp));

INT4 L2VpnRegisterWithIfModule ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnDeRegisterWithIfModule ARG_LIST ((tPwVcEntry * pPwVcEntry));

UINT4 L2VpnInternalEventHandler ARG_LIST ((UINT1 *pi1TmpMsg));

UINT4 L2VpnAdminEventHandler ARG_LIST ((VOID *pAdminEvtInfo, UINT4 u4Event));

INT4 L2VpnLdpEventHandler ARG_LIST ((VOID *pMsg, UINT4 u4Event));

INT4 L2VpnLdpGetEventInfo (VOID **ppL2VpnQMsg);
INT4 L2VpnLdpCleanEventInfo (VOID *pL2VpnQMsg, BOOL1 b1CleanQMsg, BOOL1 b1CleanNodes);
INT4 L2VpnLdpSendEventInfo (VOID *pL2VpnQMsg, UINT4 u4Event);
tPwRedDataEvtPwFec * L2VpnLdpGetEventPwFec ARG_LIST ((VOID));
INT4 L2VpnLdpCleanEventPwFec (VOID *pEvtPwFec);
tPwRedDataEvtPwData  * L2VpnLdpGetEventPwData ARG_LIST ((VOID));
INT4 L2VpnLdpCleanEventPwData (VOID *pEvtPwData);


INT4 L2VpnSendLdpPwVcLblMapReqMsg ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnSendLdpPwVcNotifMsg ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnSendLdpPwVcNotifEvent ARG_LIST ((tPwVcEntry * pPwVcEntry));

INT4 L2VpnSendLdpPwVcLblWcWdrawMsg
ARG_LIST ((tPwVcEntry * pPwVcEntry, UINT1 i1Status));

INT4 L2VpnSendLdpPeerSsnRelReq ARG_LIST ((tPwVcEntry * pPwVcEntry));

UINT1 L2VpnEnqueueMsgToL2VpnQ
ARG_LIST ((UINT1 u1EvtType, tL2VpnQMsg * pL2VpnQMsg));

VOID L2vpnInitPwVcEntry
ARG_LIST ((UINT4 u4PwVcIndex, tPwVcEntry *pPwVcEntry));

VOID L2vpnInitPwVcMplsEntry
ARG_LIST ((tPwVcMplsEntry *pPwVcMplsEntry));

VOID L2vpnInitPwVcEnetEntry
ARG_LIST ((tPwVcEnetEntry *pPwVcEnetEntry));

INT4 L2VpnFmQueryPwVcStats ARG_LIST ((tPwVcEntry *pPwVcEntry));

VOID L2VpnIfEventHandler ARG_LIST ((UINT4 u4IfIndex, UINT2 u2PortVlan, UINT1 u1Status));

VOID L2VpnFmQueryAllPwVcStats ARG_LIST ((VOID));

INT4 L2VpnReleaseWaitingRemLblMsg ARG_LIST ((tPwVcLblMsgInfo *pLblMsgInfo));

INT4 L2VpnCheckPortOperStatus ARG_LIST ((INT4 i4PortIfIndex));

INT4 L2VpnPostTeEvent 
ARG_LIST ((UINT4 u4TnlIndex, UINT2 u2TnlInstance, UINT4 u4TnlIngressLSRId,
            UINT4 u4TnlEgressLSRId, UINT4 *pMplsLspId, UINT1 u1AssociateFlag, 
            UINT4 u4Event));

INT4 RegisterWithIfModule ARG_LIST ((INT4 i4PortIfIndex, INT4 (*pIfHandler)(VOID)));

INT4 DeRegisterWithIfModule ARG_LIST ((INT4 i4PortIfIndex));

INT4 L2VpnEnablePwVc ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4 L2VpnDisablePwVc ARG_LIST ((tPwVcEntry *pPwVcEntry));

VOID L2VpnUpdateGlobalStats
ARG_LIST ((tPwVcEntry * pPwVcEntry, UINT1 u1Event));

VOID L2VpnPrintPwVc ARG_LIST ((tPwVcEntry *pPwVcEntry));

VOID L2vpUpdateLdpUpStatus ARG_LIST((VOID));

INT4 L2VpnGetPerfCurrentInterval (UINT4 u4Period, INT1 *pi1CurInterval,
        UINT4 *pu4RemTime, UINT4 *pu4TimeElapsed);
tVPLSEntry * L2vpnCreateVplsEntry ARG_LIST ((UINT4 u4InstanceIndex));

INT4 L2vpnDeleteVplsEntry ARG_LIST ((UINT4 u4InstanceIndex));

INT4 L2vpnVplsVfiAdd ARG_LIST ((tVPLSEntry *pVplsEntry));

INT4 L2vpnVplsVfiDelete ARG_LIST ((tVPLSEntry *pVplsEntry));

tVPLSEntry * L2VpnGetVplsEntryFromVFI ARG_LIST ((UINT1 *pu1VplsName));

tVPLSEntry * L2VpnGetVplsEntryFromInstanceIndex 
ARG_LIST ((UINT4 u4InstanceIndex));
#ifdef HVPLS_WANTED
tVPLSEntry         *
L2VpnGetVplsEntryFromVpnId ARG_LIST  ((UINT4 u4VpnId));
INT1
L2VpnVplsFdbAlarm ARG_LIST ((tVPLSEntry *pVplsEntry, UINT1 u1State));
#endif

INT4
L2vpnAddPwVplsEntry ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4
L2vpnDeletePwVplsEntry ARG_LIST ((tPwVcEntry *pPwVcEntry));

INT4
L2VpnMplsL2PktVplsFwd ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Vsi, UINT4 u4IfIndex));

INT4
L2VpnEncapAndFwdPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf, tPwVcEntry *pPwVcEntry, UINT4 u4IfIndex));
INT4
L2VpnWrEncapAndFwdPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4PwVcIndex, UINT4 u4IfIndex));
tPwVcEntry         *
L2VpnGetPwVcEntryFromLabel ARG_LIST ((UINT4 u4Label, UINT4 u4IfIndex));
tPwVcEntry         *
L2VpnGetPwVcEntryFromOutLabel ARG_LIST ((UINT4 u4Label));

INT4
PwEntryACExistsCheck ARG_LIST ((INT4 i4ContextId, UINT4 u4PwVcIndex, UINT4 u4PwEnetPortVlan,
            INT4 i4PortIfIndex, BOOL1 bIsCheckOnSameVpls));
INT4 L2VpnDeletePsnTnlIf ARG_LIST ((UINT4 u4PwL3Intf, UINT1 *pu1NextHopMac,
            UINT4 u4PsnDelAct));
INT4 L2VpnRegWithNonTeLowerLayer ARG_LIST ((tPwVcEntry * pPwVcEntry));
INT4 L2VpnDeRegWithNonTeLowerLayer ARG_LIST((tPwVcEntry * pPwVcEntry));
INT4 L2VpnHandleNonTeLowerLayerDown ARG_LIST((tPwVcEntry * pPwVcEntry));
UINT1 L2VpnIsPwIdOrGenFecExist ARG_LIST((tPwVcEntry *pPwVcEntry));
INT4
L2VpnMplsFrrILMHwAddForMP (tPwVcEntry *pPwVcEntry, UINT4 u4BkpTnlXcIndex, VOID *pSlotInfo);
INT4
L2VpnMplsFrrILMHwDelForMP (tPwVcEntry *pPwVcEntry, UINT4 u4InOuterMPLabel1,
        UINT4 u4InOuterMPLabel2, UINT4 u4InOuterMPLabel3,
        BOOL1 bIsPwVcOperLlDown);
INT4 L2VpnMplsILMHwDelForPopSearch (UINT4 u4TunnelLabel,
        UINT4 u4LdpLabel,
        UINT4 u4IfIndex,
        BOOL1 bIsPwVcOperLlDown,
        VOID *pSlotInfo,INT4 *i4IsIlmDeleted);
INT4 L2VpnPwVcIlmHwDelForNonTePsn (tPwVcEntry * pPwVcEntry,
        tMplsHwIlmInfo * pMplsHwIlmInfo,
        BOOL1 bIsPwVcOperLlDown,
        VOID *pSlotInfo);
INT4 L2VpnPwVcIlmHwAddForNonTePsn (tPwVcEntry * pPwVcEntry,
        tMplsHwIlmInfo * pMplsHwIlmInfo,
        VOID *pSlotInfo);
INT4 L2VpnPwVcHwDelForNonTePsn (tPwVcEntry * pPwVcEntry,
        tMplsHwVcTnlInfo * pMplsHwVcInfo);
INT4 L2VpnPwVcHwAddForNonTePsn (tPwVcEntry * pPwVcEntry,
        tMplsHwVcTnlInfo * pMplsHwVcInfo);
INT4 L2VpnPwVcHwVpnAcDel (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);
INT4 L2VpnPwVcHwVpnAcAdd (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);
INT4 L2VpnPwVcHwAdd (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);

INT4 L2VpnPwVcHwAddP2MPDest (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);
INT4 L2VpnPwVcHwDelP2MPDest (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);

INT4
L2VpnPwVcHwAddWrp (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);
INT4
L2VpnPwVcHwDelWrp (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);
INT4
PwEntryHwACExistsCheck (tVPLSEntry *pVplsEntry,
                     UINT4 u4PwEnetPortVlan, INT4 i4PortIfIndex);

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)

INT4 L2VpnPwVcHwListCreate(tPwVcEntry * pPwVcEntry,
        tMplsHwVcTnlInfo * pMplsHwVcInfo,
        tL2VpnPwHwList *pL2VpnPwHwList);     
INT4
L2vpnGrDeleteLdpPwEntryOnMisMatch(tL2VpnPwHwList  *pL2VpnPwHwListEntry);

INT4 L2VpnPwIlmHwListUpdate(tPwVcEntry * pPwVcEntry,
        tMplsHwIlmInfo  *pMplsHwIlmInfo,
        tL2VpnPwHwList *pL2VpnPwHwListEntry);

INT4 L2VpnMarkLdpPwEntriesStale (VOID);

INT4
L2VpnFlushPwVcDynamicEntries(tPwVcActivePeerSsnEntry *pPeerSsn);

INT4
L2VpnPwHwListDelete(tL2VpnPwHwList *pL2VpnPwHwListUpdate,
        UINT1 u1HwStatus);
INT4
L2VpnVplsHwListRemove(tL2VpnVplsHwList *pL2VpnVplsHwListUpdate,
        UINT1 u1HwStatus);


INT4
L2VpnLdpSendLblRelEvent(tL2VpnPwHwList  *pL2VpnPwHwListEntry);

INT4
L2VpnPwHwListAddUpdate(tL2VpnPwHwList *pL2VpnPwHwListEntry,
        UINT1 u1HwStatus);
INT4
L2VpnVplsHwListAddUpdate(tL2VpnVplsHwList *pL2VpnVplsHwListEntry,
        UINT1 u1HwStatus);


INT4
L2VpnGetNextLabelFromHwList(tL2VpnPwHwList  *pL2VpnPwHwListEntry,
      tL2VpnPwHwList  *pL2VpnPwHwListNextEntry,
      UINT4 * pu4InLabel);

VOID
L2VpnSendReadyEventToLdp(VOID);

INT4
L2VpnDelStalePwIlm (tL2VpnPwHwList *pL2VpnPwHwListEntry, VOID *pSlotInfo);
INT4
L2VpnDelStalePwVc (tL2VpnPwHwList *pL2VpnPwHwListEntry, VOID *pSlotInfo);
VOID
L2VpnSetPwAdminStatusAtGoStandby(INT4 i4SetValPwAdminStatus);
#endif
INT4
L2VpnHwVplsFlushMac (tPwVcEntry * pPwVcEntry,
          tVPLSEntry *pVplsEntry);
#ifdef HVPLS_WANTED
INT4 L2VpnSendLdpMacWdrlMsg (tPwVcEntry * pPwVcEntry);
INT4
L2VpnProcessLdpAddrWdrEvent (tPwVcNotifEvtInfo * pNotifInfo);
INT4
L2VpnMplsPktVplsFwd (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4VplsInstance,
        UINT4 u4IfIndex,UINT4 u4PwVcIndex);

INT4
L2VpnIsPwVpws (UINT4 u4PwVcIndex);
#endif

INT4
L2VpnPwVcHwAddPwNonTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);
INT4
L2VpnPwVcHwAddPwTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tPwVcMplsEntry *pPwVcMplsEntry,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);
INT4
L2VpnPwVcHwAddVcOnly (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tPwVcMplsEntry *pPwVcMplsEntry,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);

#ifdef MPLS_IPV6_WANTED
INT4
L2VpnPwVcIpv6HwAddVcOnly (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
                          tPwVcMplsEntry * pPwVcMplsEntry,
                          tMplsHwVcTnlInfo * pMplsHwVcInfo,
                          tMplsHwVplsInfo * pMplsHwVplsInfo, UINT4 u4VpnId);
#endif

INT4
L2VpnPwVcHwDelPwNonTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);
INT4
L2VpnPwVcHwDelPwTe (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tPwVcMplsEntry *pPwVcMplsEntry,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);
INT4
L2VpnPwVcHwDelVcOnly (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo,
        tPwVcMplsEntry *pPwVcMplsEntry,
        tMplsHwVcTnlInfo *pMplsHwVcInfo,
        tMplsHwVplsInfo *pMplsHwVplsInfo, UINT4 u4VpnId);



INT4 L2VpnPwVcIlmHwAdd (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);
INT4 L2VpnPwVcHwDel (tPwVcEntry * pPwVcEntry, VOID *pSlotInfo);
INT4 L2VpnPwVcIlmHwDel (tPwVcEntry * pPwVcEntry, BOOL1 bIsPwVcOperLlDown,
        VOID *pSlotInfo);
INT4
L2VpnPwVcFetchAcInfo (tPwVcEntry * pPwVcEntry, UINT4 *pu4VpnAcVlan,
        UINT4 *pu4VpnAcPhyPort, UINT4 *pu4VpnPwVlan,
        INT1 *pi1VpnPwVlanMode);
INT4
L2VpnPwVcVplsHwVpnAcAdd (tPwVcEntry * pPwVcEntry, 
        tPwVcEnetEntry *pPwVcEnetEntry);
INT4
L2VpnPwVcVplsHwVpnAcDel (tPwVcEntry * pPwVcEntry, 
        tPwVcEnetEntry *pPwVcEnetEntry);
INT4
L2VpnDestryEnetServInstance (tPwVcEntry *pPwVcEntry,
        tPwVcEnetEntry *pPwVcEnetEntry);
INT4
L2VpnCheckForAllPwsDown (UINT4 u4VplsIndex, BOOL1 *pbIsAllPwsDown);
INT4
L2VpnCheckAndGetLastOperUpPw (UINT4 u4VplsIndex, UINT4 *pu4PwVcIndex);
INT4
L2VpnCheckValidTnlForPw (tPwVcEntry * pPwVcEntry);
UINT4
L2VpnGetVpnIdAndVsiFromPw (tPwVcEntry *pPwVcEntry, UINT4 *pu4VpnId,
        INT4 *pi4Vsi);
INT4
L2VpnUpdateOperStatus (tPwVcEntry * pPwVcEntry, UINT1 u1Event,
        UINT1 u1OperApp);

VOID
L2VpnExtChkAndUpdatePwOperStatus (tPwVcEntry * pPwVcEntry, UINT1 u1Event, 
        UINT1 u1OperApp, BOOL1 *pbIsOperChged);
INT4
L2vpnExtGetPwInfo (tMplsApiInInfo *pInMplsApiInfo,
        tMplsApiOutInfo *pOutMplsApiInfo);
INT4
L2vpnExtPwUpdateSessionParams (tMplsApiInInfo *pInMplsApiInfo);
INT4
L2vpnExtHandlePathStatusChgForPw (tMplsApiInInfo *pInMplsApiInfo);
INT4
L2vpnExtGetPwInfoFromInLblInfo (tMplsApiInInfo *pInMplsApiInfo,
        tMplsApiOutInfo *pOutMplsApiInfo);
INT4
L2vpnExtUpdatePwLpsStatus (tMplsApiInInfo *pInMplsApiInfo);

VOID
L2vpnPortSnmpSendTrap (tSNMP_VAR_BIND * pTrapMsg, CHR1 * pc1SysLogMsg);

tPwVcEntry * L2VpnGetPwVcEntryFromIndex (UINT4 pPwVcIndex);
tPwVcEntry * L2VpnGetWorkingPwEntryFromIndex (UINT4 pPwVcIndex);
tPwVcEntry * L2VpnGetPwVcEntryFromPwId (UINT4 u4PwVcID, INT1 i1PwVcType);
tPwVcEntry * L2VpnGetVplsPwVcEntryFromPwAgi (UINT1 *pu1Agi,tGenU4Addr GenU4Addr); 
tPwVcEntry * L2VpnGetPwVcEntryFromPwAgi (UINT1 *pu1Agi, UINT1 *pu1Saii,UINT1 *pu1Taii);
tPwVcEntry * L2VpnGetPwVcEntry (INT1 i1PwOwner, UINT4 u1PwId, UINT1 u1PwType,
                                UINT1 *pu1Agi, UINT1 *pu1Saii,UINT1 *pu1Taii,
                                tGenU4Addr GenU4Addr
                               );
INT1 L2VpnGetControlWordMandate (INT1 i1PwType);

INT4
L2vpnExtUpdateOamAppStatus (tMplsApiInInfo * pInMplsApiInfo);

/* l2vpvccv.c */
VOID L2VpnVccvSelectMatchingCCTypes (tPwVcEntry *pPwVcEntry);
VOID L2VpnVccvSelectMatchingCVTypes (tPwVcEntry *pPwVcEntry);
INT4 L2VpnVccvValidateCC (UINT1 u1CcType);
INT4 L2VpnVccvValidateCV (UINT1 u1CvType);
INT4 L2VpnVccvValidateCapabAdvert (UINT1 u1CapabAdvert);
UINT1 L2VpnVccvReverseValue (UINT1 u1Value);
/*l2vpdb.c*/
INT4
L2VpnPwSetInVcLabel (tPwVcEntry *pPwVcEntry, UINT4 u4InVcLabel);
INT4 L2VpnPwIdCreateTable (VOID);
VOID L2VpnPwIdDeleteTable (VOID);
INT4 L2VpnPwIdAddNode (tPwVcEntry * pPwVcEntry);
VOID L2VpnPwIdDelNode (tPwVcEntry * pPwVcEntry);
tPwVcEntry         *
L2VpnGetPwVcEntryFromVcId (UINT4 u4PwVcId);
tPwVcEntry         *
L2VpnGetPwVcEntryFromAgiAndTaii (UINT1 *pu1Agi, UINT1 *pu1Taii);
PUBLIC INT4
L2VpnPwIfIndexCreateTable (VOID);
PUBLIC VOID
L2VpnPwIfIndexDeleteTable (VOID);
PUBLIC INT4
L2VpnPwIfIndexAddNode(tPwVcEntry * pPwVcEntry);
PUBLIC VOID
L2VpnPwIfIndexDeleteNode (tPwVcEntry * pPwVcEntry);
tPwIfIndexMap  *
L2VpnPwIfIndexGetNode (UINT4 u4PwIfIndex);
INT4
L2VpnGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex);
INT4
L2vpnExtNotifyPwStatusToApp (tPwVcEntry *pPwVcEntry,
        UINT1 u1OperApp);
#ifdef HVPLS_WANTED
INT4
L2vpnExtNotifyPwIfStatusToApp (tPwVcEntry *pPwVcEntry,
 UINT1 u1OperStatus);
INT4
L2vpnHandleCfmPacket (tCRU_BUF_CHAIN_HEADER * pBuf, 
 UINT4 u4PwIfIndex, UINT4 u4PktSize);
#endif

INT4
L2VpnProcessOamMegEvent (tL2VpnOamInfo *pOamInfo);

INT4
L2VpnProcessAcStatusMsg(tL2VpnPwRedAcStatusInfo *pL2VpnPwRedAcStatusInfo);
INT4
L2vpnExtHdlMegAssocAndOamStatus (UINT4 u4MainReqType,
        tMplsApiInInfo * pInMplsApiInfo);
INT1
L2VpnValidatePsnEntry (UINT4 u4PwIndex);

UINT4
L2VpnValidateLenForAIIType (UINT4 u4PwAiiType, UINT1 u1PwAiiLen);

BOOL1
L2VpnUtilIsRemoteStatusCapable (tPwVcEntry * pPwVcEntry);

BOOL1
L2VpnUtilIsRemoteStatusNotCapable (tPwVcEntry * pPwVcEntry);

BOOL1
L2VpnUtilIsPwAcMatched (tPwVcEntry *pPwVcEntry, tPwVcEnetEntry *pPwVcEnetEntry,
        UINT4 u4IfIndex, UINT2 u2PortVlan);

VOID
L2VpnHandleAcUpEvent (tPwVcEntry *pPwVcEntry, UINT4 u4Event);

VOID
L2VpnHandleAcUp (tPwVcEntry *pPwVcEntry, UINT4 u4Event);

VOID
L2VpnHandleAcDownEvent (tPwVcEntry *pPwVcEntry, UINT4 u4Event);

VOID
L2VpnHandleAcDown (tPwVcEntry *pPwVcEntry, UINT4 u4Event);

/*l2vpport.c*/
PUBLIC INT4
L2VpnPortGetNodeId (tMplsGlobalNodeId * pMplsGlobalNodeId);
/* l2vptrap.c */
VOID L2vpnSnmpPwSendTrap (UINT4 u4MplsPwIndex, UINT4 u4TrapType,
        UINT1 u1TrapStatus);
/*MS-PW */

INT4 L2VpnProcessMsPwAdminEvent
ARG_LIST ((tMsPwAdminEvtInfo *pMsPwAdminEvtInfo));

INT4 L2VpnUpdateMsPwOperStatus
ARG_LIST ((tL2vpnFsMsPwConfigTable *pL2vpnFsMsPwConfigTable , UINT1 u1Event));

INT4 L2VpnFmUpdateMsPwStatus
ARG_LIST ((tPwVcEntry *pPriPwVcEntry,tPwVcEntry *pPwSecVcEntry,
            UINT1 u1PwOperStatus));

INT4 L2VpnUpdateMsPwStatus (tPwVcEntry * pPriPwVcEntry,
        tPwVcEntry * pSecPwVcEntry, UINT1 u1PwOperStatus, VOID *pSlotInfo);

tL2vpnFsMsPwConfigTable *L2VpnGetMsPwEntry(UINT4 u4PriPwVcIndex,
        UINT4 u4SecPwVcIndex);

INT4 L2VpnMsPwForwardIlmHwAdd (tPwVcEntry * pPriPwVcEntry,
        tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo);

INT4 L2VpnMsPwReverseIlmHwAdd(tPwVcEntry * pPriPwVcEntry,
        tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo);

INT4 L2VpnMsPwForwardIlmHwDel (tPwVcEntry * pPriPwVcEntry,
        tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo);

INT4 L2VpnMsPwReverseIlmHwDel(tPwVcEntry * pPriPwVcEntry,
        tPwVcEntry * pSecPwVcEntry, VOID *pSlotInfo);
INT4 MplsGetOutTunnelParams(tPwVcMplsTnlEntry *pMplsOutTnlEntry,
        UINT4 *pu4XcIndex,UINT1 *);

INT4 L2VpnGetDstMacForVcOnly(tPwVcEntry *pPwVcEntry,UINT1 *pu1PwDstMac);

PUBLIC INT4 L2vpnUtlCreatMemPool PROTO ((VOID));


PUBLIC INT4 L2vpnUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 L2vpnUtlDeleteRBTree PROTO ((VOID));

PUBLIC INT4 L2vpnFsMsPwConfigTableCreate PROTO ((VOID));

PUBLIC INT4 FsMsPwConfigTableRBCmp PROTO ((tRBElem *, tRBElem *));
INT4 L2vpnGetFsMsPwMaxEntries PROTO ((UINT4 *));
INT4 L2vpnTestFsMsPwMaxEntries PROTO ((UINT4 *,UINT4 ));
INT4 L2vpnSetFsMsPwMaxEntries PROTO ((UINT4 ));
tL2vpnFsMsPwConfigTable * L2vpnGetFirstFsMsPwConfigTable PROTO ((VOID));
tL2vpnFsMsPwConfigTable * L2vpnGetNextFsMsPwConfigTable PROTO
((tL2vpnFsMsPwConfigTable *));
INT4 L2vpnGetAllFsMsPwConfigTable PROTO ((tL2vpnFsMsPwConfigTable *));
INT4 L2vpnTestAllFsMsPwConfigTable PROTO ((UINT4 *, tL2vpnFsMsPwConfigTable *,
            tL2vpnIsSetFsMsPwConfigTable *, INT4 , INT4));
INT4 L2vpnSetAllFsMsPwConfigTable PROTO ((tL2vpnFsMsPwConfigTable *,
            tL2vpnIsSetFsMsPwConfigTable *, INT4 , INT4 ));
INT4 L2vpnSetAllFsMsPwConfigTableTrigger PROTO ((tL2vpnFsMsPwConfigTable *,
            tL2vpnIsSetFsMsPwConfigTable *, INT4));
INT4 FsMsPwConfigTableFilterInputs PROTO ((tL2vpnFsMsPwConfigTable *,
            tL2vpnFsMsPwConfigTable *, tL2vpnIsSetFsMsPwConfigTable *));
INT4 L2vpnUtilUpdateFsMsPwConfigTable PROTO ((tL2vpnFsMsPwConfigTable *,
            tL2vpnFsMsPwConfigTable *));
BOOL1
L2vpnUtilIsPsnTeTnlP2MP (tPwVcEntry * pPwVcEntry);

INT4
L2vpnExtGetPktCnt PROTO((tMplsApiInInfo * pInMplsApiInfo,
            tMplsApiOutInfo * pOutMplsApiInfo));

INT4
L2vpnExtIncrOutPktCnt PROTO((tMplsApiInInfo * pInMplsApiInfo,
            tMplsApiOutInfo * pOutMplsApiInfo));
INT4
L2vpnExtIncrInPktCnt PROTO((tMplsApiInInfo * pInMplsApiInfo,
            tMplsApiOutInfo * pOutMplsApiInfo));

INT4
L2vpnExtUpdatePwApp PROTO((tMplsApiInInfo * pInMplsApiInfo, 
            tMplsApiOutInfo * pOutMplsApiInfo));

#ifdef HVPLS_WANTED
INT4
L2vpnExtRegisterPwForNotif PROTO((tMplsApiInInfo * pInMplsApiInfo, 
     tMplsApiOutInfo * pOutMplsApiInfo));
#endif

INT4
L2VpnGetMultiplexBundleStatus(UINT4 u4PortIfIndex,UINT1 u1PortService);

tMplsPortEntryInfo *                                                                                                         L2VpnMplsPortEntryGetNode (UINT4 u4IfIndex);

INT4
L2VpnValidatePortStatus (INT4 i4PortIfIndex);

INT4
L2VpnPortServiceStatus (INT4 i4PortIfIndex);

/*MS-PW */
/* VPLS FDB */
INT4
L2VpnVlanValidateVlanId PROTO ((UINT4 u4ContextId, UINT2 u2VplsFdbId));

VOID
L2VpnDeleteL3FtnAndPsnTnlIf PROTO ((tL2VpnMplsPwVcMplsTeEvtInfo * pMplsTeEvtInfo,
            UINT4 u4Event));
VOID
L2VpnHandlePsnTnlDeletion PROTO ((UINT4 u4TnlIndex, UINT2 u2TnlInstance, 
            UINT4 u4LclLsr, UINT4 u4PeerLsr, 
            UINT4 u4PwL3Intf, UINT1 *pu1NextHopMac, UINT4 u4PsnDelAction));
UINT1 
L2VpPortGetPortOwnerFromIndex PROTO ((UINT4 u4IfIndex));

/* Out Label based RBTree */
INT4 L2VpnPwCreateOutLabelBasedRBTree (VOID);
VOID L2VpnPwDeleteOutLabelBasedRBTree (VOID);
INT4
L2VpnPwSetOutVcLabel (tPwVcEntry * pPwVcEntry, UINT4 u4OutVcLabel);

INT4
L2vpnCheckAndHandleAcDownEvent (tPwVcEntry *pPwVcEntry, 
        tPwVcEnetEntry  *pPwVcEnetEntry, 
        UINT4 u4Event, UINT1 *pu1Flag);
INT4
L2vpnCheckAndHandleAcUpEvent (tPwVcEntry *pPwVcEntry, 
        tPwVcEnetEntry  *pPwVcEnetEntry,
        UINT1 *pu1Flag);
UINT4
L2VpnUtilHashIfIndexCmp (tTMO_HASH_NODE * pCurNode, UINT1 *ptr);
UINT4
L2VpnUtilGetRedGrpIdFromRedGrpName (UINT1 *pu1RgName);

tL2vpnRedundancyEntry *
L2VpnUtilGetNextRGNode (tL2vpnRedundancyEntry *pRgGroup);

tL2vpnRedundancyPwEntry *
L2VpnUtilGetNextRedundancyPwEntryByIndex (UINT4 u4RgIndex, UINT4 u4PwIndex);

tL2vpnRedundancyNodeEntry *
L2VpnUtilGetNextRedundancyNodeEntryByIndex (
        UINT4 u4RgIndex, UINT1 u1AddrType, uGenAddr Addr);

UINT4
L2VpnUtilCheckPwExistence (UINT4 u4PwIndex, UINT4 u4PwRedGroupIndex);
UINT4
L2VpnUtilValidateHwStatus (tPwVcEntry * pPwVcEntry, UINT1 u1PwOperStatus);
VOID
L2VpnUtilSetHwStatus (tPwVcEntry * pPwVcEntry, UINT1 u1HwStatus, UINT1 u1Flag);
UINT1
L2VpnUtilFindHwStatus (tPwVcEntry * pPwVcEntry);

UINT4
L2VpUtilCheckAcGroupMatch (UINT4 u4FsL2VpnPwRedPwIndex, UINT4 u4FsL2VpnPwRedGroupIndex);

UINT4
L2VpnUtilCheckIfPwIsAssoWithGrp (UINT4 u4PwIndex, UINT4 *u4GrpIndex);

INT4
L2VpnPwRedSetVpnId (UINT4 u4PwRedGroupIndex, UINT4 u4PwRedPwIndex);


/* PW-RED internal DB routines */
INT4 L2VpnRedundancyInit (VOID);
INT4 L2VpnRedundancyEnable (VOID);
INT4 L2VpnRedundancyDisable (VOID);

tL2vpnRedundancyEntry *L2VpnGetRedundancyEntryByIndex (UINT4 u4RgIndex);
tL2vpnRedundancyEntry *L2vpnRedundancyGroupCreate (UINT4 u4RgIndex);
INT4 L2vpnRedundancyGroupActivate (UINT4 u4RgIndex);
INT4 L2vpnRedundancyGroupDeActivate (UINT4 u4RgIndex);
INT4 L2vpnRedundancyGroupDelete (tL2vpnRedundancyEntry *pRgGroup);

tL2vpnRedundancyNodeEntry *L2VpnGetRedundancyNodeEntryByIndex (UINT4 u4RgIndex, UINT1 u1AddrType, uGenAddr *pAddr);
tL2vpnRedundancyNodeEntry *L2vpnRedundancyNodeCreate (UINT4 u4RgIndex, UINT1 u1AddrType, uGenAddr *pAddr);
INT4 L2vpnRedundancyNodeActivate (UINT4 u4RgIndex, UINT1 u1AddrType, uGenAddr *pAddr);
INT4 L2vpnRedundancyNodeDeActivate (UINT4 u4RgIndex, UINT1 u1AddrType, uGenAddr *pAddr);
INT4 L2vpnRedundancyNodeDelete (tL2vpnRedundancyNodeEntry *pRgNode);

tL2vpnRedundancyPwEntry *L2VpnGetRedundancyPwEntryByIndex (UINT4 u4RgIndex, UINT4 u4PwIndex);
tL2vpnRedundancyPwEntry *L2VpnGetRedundancyPwEntryByPwIndex (UINT4 u4PwIndex);
tL2vpnRedundancyPwEntry *L2vpnRedundancyPwCreate (UINT4 u4RgIndex, tPwVcEntry *pPwVcEntry);
INT4 L2vpnRedundancyPwActivate (UINT4 u4RgIndex, tPwVcEntry *pPwVcEntry);
INT4 L2vpnRedundancyPwDeActivate (UINT4 u4RgIndex, tPwVcEntry *pPwVcEntry);
INT4 L2vpnRedundancyPwDelete (tL2vpnRedundancyPwEntry *pRgPw);

tL2vpnIccpPwEntry *L2VpnGetIccpPwEntryByIndex (UINT4 u4RgIndex, tMplsRouterId *pRouterId, tL2vpnPwFec *pFec);
tL2vpnIccpPwEntry *L2VpnGetIccpPwEntryByRgRoId (UINT4 u4RgIndex, tMplsRouterId *pRouterId, tL2vpnIccpRoId *pRoId);
tL2vpnIccpPwEntry *L2VpnGetIccpPwEntryByRgServiceName (UINT4 u4RgIndex, tMplsRouterId *pRouterId, tL2vpnIccpServiceName *pName);
tL2vpnIccpPwEntry *L2VpnRedundancyIccpPwCreate (
        tL2vpnRedundancyPwEntry *pRgPw,
        tL2vpnRedundancyNodeEntry *pRgNode, tL2vpnIccpPwEntry *pIccpPw);
INT4 L2VpnRedundancyIccpPwDelete (tL2vpnIccpPwEntry *pRgIccpPw);


/* PW-RED forwarding logic routines */
INT4 L2vpnRedundancyNodeStateUpdate (tL2vpnRedundancyNodeEntry *pRgNode, UINT1 u1Event);
INT4 L2vpnRedundancyPwStateUpdate (tL2vpnRedundancyPwEntry *pRgPw, UINT1 u1Event);

INT4 L2vpnFmRedundancyPwForwardingUpdate (tPwVcEntry *pPwVcEntry,tL2vpnRedundancyEntry *pRgGrp);

INT4 L2vpnRedundancyIccpPwDataRx (tL2VpnLdpIccpEvtInfo *pLdpIccpEvtInfo);

INT4 L2vpnRedundancyIccpPwDebug (tL2VpnLdpIccpEvtInfo *pLdpIccpEvtInfo);

INT4 L2VpnRedundancyTimerEvent (VOID);
INT4
L2VpnPwRedVplsHwModify (tPwVcEntry * pPwVcEntry,BOOL1 bIsBlocking,
        VOID *pSlotInfo);

INT4
L2VpnPwRedVcHwModify(tPwVcEntry *pPwVcEntry,tL2vpnRedundancyEntry *pRgGrp,
        VOID *pSlotInfo);
INT4
L2VpnPwRedVcHwModifyWrp (UINT4 u4VpnId,UINT4 u4OperActVpnId,tPwVcEntry *pPwVcEntry,
        tMplsHwVcTnlInfo *pMplsHwVcInfo, VOID* pSlotInfo);

/* l2vpn redundancy*/
INT4 L2vpnRedundancyEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4 L2vpnRedundancyNodeEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4 L2vpnRedundancyPwEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4 L2vpnIccpPwEntryRbCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);

INT4 L2vpnRedundancyPwCmp (tL2vpnIccpPwEntry *pIccpPw1, tL2vpnIccpPwEntry *pIccpPw2);


INT4 L2vpnRedundancyNegotiate (UINT4 u4RgIndex, BOOL1 b1AltPriority);
INT4 L2vpnRedundancySwitchover (tL2vpnRedundancyPwEntry *pRgPw);
INT4 L2vpnRedundancyStandby (tL2vpnRedundancyPwEntry *pRgPw);

INT4 L2vpnRedundancyCalcBestPwPriority (UINT2 *pu2BestPriority, UINT4 u4RgIndex, UINT2 u2PrevPriority);
INT4 L2vpnRedundancyCalcBestPw (UINT4 u4RgIndex, UINT2 u2Priority, tL2vpnIccpPwEntry **ppNegIccpPw);

INT4 L2vpnRedundancyIccpPwDataTx (tL2vpnRedundancyNodeEntry *pRgNode, tL2vpnIccpPwEntry *pRgIccpPw,
        UINT2 u2ResponseNum, UINT1 u1ResponseType, 
        tTMO_SLL *pResponsePwList, UINT1 u1RequestType);

INT4 L2vpnRedundancyIccpNodeNotify (
        UINT4 u4RgIndex, tL2vpnIccpPwEntry *pRgIccpPw,
        UINT1 u1ResponseType, UINT1 u1RequestType);
INT4 L2vpnRedundancyNodeIccpPurge (tL2vpnRedundancyNodeEntry *pRgNode);

INT4 L2vpnRedundancyPwRemoteNotify (UINT4 u4RgIndex);

UINT4
L2VpnUtilCheckRedPwPeerAdd(UINT4 u4PwRedPwIndex,
        UINT4 u4PwRedGroupIndex);
UINT4
L2VpnUtilGetMatchPwRGEntry(UINT4 u4PwRedPwIndex,
        UINT4 *u4PwRedGroupIndex);
INT4
L2VpPwRedProcessAcDownEvent(UINT4 u4IfIndex, UINT2 u2PortVlan);
INT4
L2VpPwRedProcessAcUpEvent(UINT4 u4IfIndex, UINT2 u2PortVlan);
INT4
L2VpnRedundancyResetOtherStandbyPw(UINT4 u4RgIndex,UINT4 u4PwIndex);
VOID L2VpPwRedActionforAwaitingPw(tL2vpnIccpPwEntry *pRgIccpPw,tL2vpnRedundancyPwEntry *pRgPw,
        tL2vpnRedundancyEntry *pRgGroup,BOOL1* b1DataUpdated);
VOID
L2VpPwRedHandleRemoteSWOver(tL2vpnIccpPwEntry *pRgIccpPw,tL2vpnRedundancyPwEntry *pRgPw,
        tL2vpnRedundancyEntry *pRgGroup,BOOL1* b1DataUpdated);
VOID
L2VpPwRedHandleChngInLocalStatus(tL2vpnIccpPwEntry *pRgIccpPw,tL2vpnRedundancyPwEntry *pRgPw);
VOID
L2VpPwRedHandleChngInRemoteStatus(tL2vpnIccpPwEntry *pRgIccpPw,tL2vpnRedundancyPwEntry *pRgPw,
        tL2vpnRedundancyEntry *pRgGroup,BOOL1 *b1ReNegotiate);
INT4
L2VpPwRedHandlePwUpEvent(tL2vpnIccpPwEntry *pRgIccpPw,tL2vpnRedundancyPwEntry *pRgPw,
        tL2vpnRedundancyEntry * pRgGroup,BOOL1 *b1ReNegotiate,
        BOOL1 *b1LocalStandby,BOOL1 *b1LocalSwitchover);
VOID
L2VpnPwRedSnmpNotifyRoleChng(UINT4 u4RgIndex,UINT4 u4PwIndex,UINT1 u1TrapType,
        UINT1 u1LocalStatus, UINT1 u1RemoteStatus);
tSNMP_OID_TYPE     *L2VpUtilMakeObjIdFromDotNew (INT1 *pi1TxtStr);
INT4 L2VpUtilParseSubIdNew (UINT1 **ppu1TempPtr);

VOID
L2VpnPwRedSnmpNotifyNegoFail(UINT4  u4RgIndex,tL2vpnIccpPwEntry *pOperActivePw,
        UINT4  u4AdminActivePw);


VOID
L2VpnPwRedSnmpNotifyForceActive (UINT4 u4RgIndex,
        tL2vpnIccpPwEntry * pOperActivePw,
        UINT4 u4AdminActivePw);
VOID
L2VpnUtilSetEnetHwStatus (INT4 i4PortIfIndex, UINT2 u2PortVlan,
        UINT1 u1HwStatus, UINT4 u4PwAcId);
/* GR Related Functions */
INT4
L2vpnGrProcessLdpGrShutEvent (tPwVcSsnEvtInfo    *pSsnInfo, UINT4 u4Event);
INT4
L2vpnGrProcessLdpDelStaleEntryEvent (tPwVcSsnEvtInfo *pSsnInfo);
INT4
L2vpnGrProcessLdpDelAllStaleEntries (VOID);
INT4
L2vpnGrSetGrRecoveryTime (tPwVcSsnEvtInfo *pSsnInfo);
INT4
L2VpnGrProcessRecoveryTmrExpiry (tTmrAppTimer *pAppTimer);
VOID
L2vpnProcessTimerEvents (VOID);
INT4 L2VpnProcessLdpGrSsnUpEvent
ARG_LIST ((tPwVcSsnEvtInfo *pSsnInfo));
INT4
L2VpnMatchPwVcEntryWithTai (UINT1 *pu1Agi,UINT1 *pu1Taii);
INT4
L2vpnGetIccpFrmSsnInfo (UINT4 u4RgIndex, tPwVcActivePeerSsnEntry *pSession,
        tPwVcEntry *pPwVcEntry);
VOID
L2VpnHandlePendingMsgsToLdp (VOID);

INT4
L2VpnHandleSendMsgToLdp (tPwVcEntry *pPwVcEntry, UINT4 u4SendToLdpMsgType);

VOID
L2VpnStartPendingMsgListTimer (VOID);
UINT4
L2VpnPassValidInLabelToHw (tPwVcEntry * pPwVcEntry);

UINT4
L2VpnGetPwIndexFromVplsIndexForVpws (UINT4 u4VplsInstance);

INT1
L2VpnSetPwAdminStatus (UINT4 u4PwIndex, INT4 i4SetValPwAdminStatus);

#ifdef HVPLS_WANTED
tVplsPwBindEntry *
L2vpnGetVplsPwBindEntry (UINT4 u4VplsConfigIndex, UINT4 u4PwIndex);
INT1
L2VpnVplsOperStatusTrap (tVPLSEntry * pVplsEntry);
INT4
L2VpnCheckNotifMaxRate(VOID);
VOID
L2VpnProcessNotifRateTmrExpiry (VOID);
#endif
VOID L2VpnIsAcPresent (UINT4 u4PwIndex, UINT2 u2VlanId, UINT1 *u1IsAcPresent);
INT1
L2VpnSetPwRowStatus (UINT4 u4PwIndex, INT4 i4SetValPwRowStatus);

INT1
L2VpnSetPwEnetRowStatus (UINT4 u4PwIndex, UINT4 u4PwEnetPwInstance,
        INT4 i4SetValPwEnetRowStatus);

VOID L2vpnDelEnetHashNode (tTMO_HASH_NODE  *pEnetHashNode);

INT4
MplsXCShowRunningConfigInterface (tCliHandle CliHandle);

#ifdef L2VPN_HA_WANTED
VOID L2VpnRmSendPwVcHwListEntry (tL2VpnPwHwList *pPwVcHwList, UINT1 u1OpType);
VOID L2VpnRmSendVplsHwListEntry (tL2VpnVplsHwList *pVplsHwList, UINT1 u1OpType);
#endif

#ifdef VPLSADS_WANTED
VOID
L2VpnProcessBgpEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo);

INT4
L2VpnVplsPwDestroy (UINT4 u4PwIndex);

INT4
L2VpnVplsPwTablePopulate (UINT4 u4VplsIndex,
                          UINT4 u4PwIndex,
                          tGenU4Addr GenU4Addr,
                          UINT4 u4LocalLabel,
                          UINT4 u4RemoteLabel,
                          UINT4 u4VplsMtu,
                          BOOL1 bControlWordFlag,
                          UINT4 u4VplsLocalVeId,
                          UINT4 u4VplsRemoteVeId);

INT4
L2VpnVplsNeighborCreate (UINT4 u4VplsIndex,
                         UINT4 u4LocalLabel,
                         UINT4 u4RemoteLabel,
                         tGenU4Addr GenU4Addr,
                         UINT4 u4VplsLocalVeId,
                         UINT4 u4VplsRemoteVeId,
                         UINT4 u4VplsMtu,
                         BOOL1 bControlWordFlag);

INT4
L2VpnProcessVplsAdminEvent (tL2vpnVplsAdminEvtInfo * pVplsAdminEvtInfo);

INT4
L2VpnProcessVplsUpEvent (UINT4 u4VplsIndex);

INT4
L2VpnProcessVplsDownEvent (UINT4 u4VplsIndex,
        UINT4 u4Mtu,
        UINT4 u4VeId,
        tVPLSLBInfo *pVPLSLBInfoBase,
        UINT1 *pu1RouteDistinguisher,
        UINT1 *pu1RouteTarget,
        BOOL1 bControlWordFlag);

INT4
L2VpnProcessVplsDeleteEvent (UINT4 u4VplsIndex);

INT4
L2VpnGetVplsAndRtIndexFromRtName (UINT1 * pu1RouteTarget,
        UINT4 * pu4VplsIndex,
        UINT4 * pu4RtIndex);

INT4
L2VpnGetPwIndexFromRemoteVPLSVe (UINT4 u4VplsIndex,
        UINT4 u4RemoteVeId, UINT4 * pu4PwIndex);

INT4
L2VpnGetLocalVeIdFromVPLSIndex (UINT4 u4VplsIndex, UINT4 * pu4VeId);

INT4
L2VpnAllocateLb (UINT4 * pu4LabelBase);

INT4
L2VpnReleaseLb (UINT4 u4LabelBase);

INT4
L2VpnGetExportRtValue (UINT4 u4VplsIndex, UINT1 u1IsDefaultRt,
        UINT1 * pu1RouteTarget);

INT4
L2VpnGetRdValue (UINT4 u4VplsIndex, UINT1 * pu1RouteDistinguisher);

INT4
L2VpnSendVplsDownAdminEvent (UINT4 u4VplsConfigIndex);

INT4
L2VpnSendVplsUpAdminEvent (UINT4 u4VplsConfigIndex);

INT4
L2VpnSendVplsRowDeleteAdminEvent (UINT4 u4VplsConfigIndex);

INT4
L2VpnSendVplsDeleteAdminEvent (UINT4 u4VplsConfigIndex);

INT4
PwCreateDynamicEnetEntry (UINT4 u4PwIndex, UINT4 u4EnetInstance,
        UINT4 u4PwEnetPwVlan, UINT4 u4PwEnetPortVlan,
        UINT4 u4IfIndex, UINT4 u4Mode);

INT4
L2VpnGetDefaultRdValue(UINT4 u4VplsIndex, UINT1 * pu1RouteDistinguisher);

INT4
L2VpnGetDefaultRtValue(UINT4 u4VplsIndex, UINT1 * pu1RouteTarget);

INT4
L2VpnGetVplsIndexFromRd (UINT1 * pu1RouteDistinguisher, UINT4 * pu4VplsIndex);

INT4
L2VpnGetVplsIndexFromRt (UINT1 * pu1RouteTarget, UINT4 * pu4VplsIndex);

INT4
L2VpnGetFreeRtIndexForVpls (UINT4 u4VplsIndex, UINT4 * pu4RtIndex);

VOID
L2VpnSetRtIndexForVpls (UINT4 u4VplsIndex, UINT4 u4RtIndex);

VOID
L2VpnReleaseRtIndexForVpls (UINT4 u4VplsIndex, UINT4 u4RtIndex);

INT4
L2VpnGetFreeAcIndexForVplsAc (UINT4 u4VplsIndex, UINT4 * pu4AcIndex);

VOID
L2VpnSetAcIndexForVplsAc (UINT4 u4VplsIndex, UINT4 u4AcIndex);

VOID
L2VpnReleaseAcIndexForVplsAc (UINT4 u4VplsIndex, UINT4 u4AcIndex);

INT4
L2VpnGetAcIndexFromIfandVlan (UINT4 u4IfIndex,
        UINT4 u4VlanId,
        UINT4 * pu4AcIndex);

VOID
L2VpnIsActiveAcPresent (UINT4 u4VplsInstance, UINT1 *pu1IsAcPresent);

VOID
L2VpnCliShowAutoDiscoveredVfiDetails(tCliHandle CliHandle, tVPLSEntry * pVplsEntry);
#if defined(LDP_GR_WANTED) || defined(VPLS_GR_WANTED)
INT4
L2VpnGrCliShowHwList(tCliHandle CliHandle);
#endif
VOID
L2VpnCliShowRtDetails(tCliHandle CliHandle, UINT4 u4VplsIndex);

VOID
L2VpnCliShowVplsAcDetails(tCliHandle CliHandle, UINT4 u4VplsIndex);

VOID
L2VpnCliShowRunningAutoDiscoveredVfi(tCliHandle CliHandle,
        tVPLSEntry * pVplsEntry);

VOID
L2VpnCliShowRunningRt(tCliHandle CliHandle, UINT4 u4VplsIndex);

VOID
L2VpnCliShowRunningVplsAc(tCliHandle CliHandle, UINT4 u4VplsIndex,
        INT4 i4TestEnetIfIndex, UINT4 u4TestPortVlanId);
INT4
L2VpnSendVplsAssociationDelEvent (UINT4 u4VplsConfigIndex);

INT4
L2VpnGetNonDefaultRdValue(UINT4 u4VplsIndex, UINT1 *pu1RouteDistinguisher);
INT4
L2VpnDeleteRTs(UINT4 u4VplsIndex);
INT4
L2VpnVplsAssocDelete (UINT4 u4InstanceIndex);

INT4
L2VpnUtilParseAndGenerateRdRt (UINT1* pu1RandomString, UINT1* pu1RdRt);

INT4 L2VpnSendRtAddAdminEvent(UINT4 u4VplsIndex,
        UINT1 *pu1RouteTarget,
        UINT1 u1RTType);

INT4 L2VpnSendRtDeleteAdminEvent(UINT4 u4VplsIndex,
        UINT1 *pu1RouteTarget,
        UINT1 u1RTType);

INT4 L2VpnProcessRtAddEvent(UINT4 u4VplsIndex,
        UINT1 *pu1RouteTarget,
        UINT1 u1RTType);

INT4 L2VpnProcessRtDeleteEvent(UINT4 u4VplsIndex,
        UINT1 *pu1RouteTarget,
        UINT1 u1RTType);

VOID L2VpnHandleIfDownForVplsAc(UINT4 u4IfIndex);

VOID L2VpnHandleIfUpForVplsAc(UINT4 u4IfIndex);

INT4 L2VpnACExistsCheck(UINT4 u4PwEnetPortVlan, INT4 i4PortIfIndex);
#endif

#endif /*_L2VPPROT_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file l2vpprot.h                             */
/*---------------------------------------------------------------------------*/
