
/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vpdbg.h,v 1.7 2013/12/20 03:25:56 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpdbg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros and definition for trace/dbg
 *                             messages
 *---------------------------------------------------------------------------*/
#ifndef _L2VP_DBG_H
#define _L2VP_DBG_H

/* Debug Related Macros */

#define L2VPN_DBG_FLAG gL2VpnGlobalInfo.u4TrcFlag
#define L2VPN_DBG_LVL gL2VpnGlobalInfo.u4TrcLvl

#define L2VPN_DBG(u4Value, pu1Format)     \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format)

#define L2VPN_DBG1(u4Value, pu1Format, Arg1)     \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1)

#define L2VPN_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2)

#define L2VPN_DBG3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3)

#define L2VPN_DBG4(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4)     \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4)

#define L2VPN_DBG5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)   \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)

#define L2VPN_DBG6(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define L2VPN_DBG7(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#define L2VPN_DBG8(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)

#define L2VPN_DBG9(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)

#define L2VPN_DBG10(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9, Arg10) \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
             UtlTrcLog (L2VPN_DBG_FLAG, u4Value, "L2VPN", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9, \
                        Arg10)

#define L2VPN_DUMP(u4Value,pc1Msg) \
        if((u4Value) == (u4Value & L2VPN_DBG_FLAG)) \
                   UtlTrcPrint(pc1Msg)
#endif
