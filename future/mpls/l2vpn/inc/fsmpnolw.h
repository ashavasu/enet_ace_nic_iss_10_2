/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpnolw.h,v 1.2 2010/10/21 09:25:01 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsPwStatusNotifEnable ARG_LIST((INT4 *));

INT1
nmhGetFsMplsPwOAMStatusNotifEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsPwStatusNotifEnable ARG_LIST((INT4 ));

INT1
nmhSetFsMplsPwOAMStatusNotifEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsPwStatusNotifEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsPwOAMStatusNotifEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsPwStatusNotifEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsPwOAMStatusNotifEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
