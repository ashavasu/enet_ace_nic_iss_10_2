/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwedb.h,v 1.5 2008/08/20 15:14:26 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDPWEDB_H
#define _STDPWEDB_H

UINT1 PwEnetTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwEnetMplsPriMappingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwEnetStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdpwe [] ={1,3,6,1,4,1,2076,13,4};
tSNMP_OID_TYPE stdpweOID = {9, stdpwe};


UINT4 PwEnetPwInstance [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,1};
UINT4 PwEnetPwVlan [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,2};
UINT4 PwEnetVlanMode [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,3};
UINT4 PwEnetPortVlan [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,4};
UINT4 PwEnetPortIfIndex [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,5};
UINT4 PwEnetVcIfIndex [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,6};
UINT4 PwEnetRowStatus [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,7};
UINT4 PwEnetStorageType [ ] ={1,3,6,1,4,1,2076,13,4,1,1,1,8};
UINT4 PwEnetMplsPriMapping [ ] ={1,3,6,1,4,1,2076,13,4,1,2,1,1};
UINT4 PwEnetMplsPriMappingRowStatus [ ] ={1,3,6,1,4,1,2076,13,4,1,2,1,2};
UINT4 PwEnetMplsPriMappingStorageType [ ] ={1,3,6,1,4,1,2076,13,4,1,2,1,3};
UINT4 PwEnetStatsIllegalVlan [ ] ={1,3,6,1,4,1,2076,13,4,1,3,1,1};
UINT4 PwEnetStatsIllegalLength [ ] ={1,3,6,1,4,1,2076,13,4,1,3,1,2};


tMbDbEntry stdpweMibEntry[]= {

{{13,PwEnetPwInstance}, GetNextIndexPwEnetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwEnetTableINDEX, 2, 0, 0, NULL},

{{13,PwEnetPwVlan}, GetNextIndexPwEnetTable, PwEnetPwVlanGet, PwEnetPwVlanSet, PwEnetPwVlanTest, PwEnetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 0, NULL},

{{13,PwEnetVlanMode}, GetNextIndexPwEnetTable, PwEnetVlanModeGet, PwEnetVlanModeSet, PwEnetVlanModeTest, PwEnetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 0, "2"},

{{13,PwEnetPortVlan}, GetNextIndexPwEnetTable, PwEnetPortVlanGet, PwEnetPortVlanSet, PwEnetPortVlanTest, PwEnetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 0, "4097"},

{{13,PwEnetPortIfIndex}, GetNextIndexPwEnetTable, PwEnetPortIfIndexGet, PwEnetPortIfIndexSet, PwEnetPortIfIndexTest, PwEnetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 0, NULL},

{{13,PwEnetVcIfIndex}, GetNextIndexPwEnetTable, PwEnetVcIfIndexGet, PwEnetVcIfIndexSet, PwEnetVcIfIndexTest, PwEnetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 0, "0"},

{{13,PwEnetRowStatus}, GetNextIndexPwEnetTable, PwEnetRowStatusGet, PwEnetRowStatusSet, PwEnetRowStatusTest, PwEnetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 1, NULL},

{{13,PwEnetStorageType}, GetNextIndexPwEnetTable, PwEnetStorageTypeGet, PwEnetStorageTypeSet, PwEnetStorageTypeTest, PwEnetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetTableINDEX, 2, 0, 0, NULL},

{{13,PwEnetMplsPriMapping}, GetNextIndexPwEnetMplsPriMappingTable, PwEnetMplsPriMappingGet, PwEnetMplsPriMappingSet, PwEnetMplsPriMappingTest, PwEnetMplsPriMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwEnetMplsPriMappingTableINDEX, 1, 0, 0, NULL},

{{13,PwEnetMplsPriMappingRowStatus}, GetNextIndexPwEnetMplsPriMappingTable, PwEnetMplsPriMappingRowStatusGet, PwEnetMplsPriMappingRowStatusSet, PwEnetMplsPriMappingRowStatusTest, PwEnetMplsPriMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetMplsPriMappingTableINDEX, 1, 0, 1, NULL},

{{13,PwEnetMplsPriMappingStorageType}, GetNextIndexPwEnetMplsPriMappingTable, PwEnetMplsPriMappingStorageTypeGet, PwEnetMplsPriMappingStorageTypeSet, PwEnetMplsPriMappingStorageTypeTest, PwEnetMplsPriMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwEnetMplsPriMappingTableINDEX, 1, 0, 0, NULL},

{{13,PwEnetStatsIllegalVlan}, GetNextIndexPwEnetStatsTable, PwEnetStatsIllegalVlanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwEnetStatsTableINDEX, 1, 0, 0, NULL},

{{13,PwEnetStatsIllegalLength}, GetNextIndexPwEnetStatsTable, PwEnetStatsIllegalLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwEnetStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData stdpweEntry = { 13, stdpweMibEntry };
#endif /* _STDPWEDB_H */

