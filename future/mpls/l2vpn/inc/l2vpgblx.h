/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpgblx.h,v 1.16 2014/11/08 11:41:09 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpgblx.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the global variable
 *                             extern definitions
 *---------------------------------------------------------------------------*/


#ifndef _L2VPGBEX_H 
#define _L2VPGBEX_H


extern tL2VpnGlobalInfo    gL2VpnGlobalInfo;
extern tPwVcGlobalInfo     gPwVcGlobalInfo;
extern tPwVcMplsGlobalInfo gPwVcMplsGlobalInfo; 
extern tPwVcEnetGlobalInfo gPwVcEnetGlobalInfo;
extern tL2vpnMsPwGlobals   gL2vpnMsPwGlobals;  /*MS-PW */
extern tL2vpnRedundancyGlobals     gL2vpnRgGlobals;    /* PW Redundancy */

extern tPwVcGlobalInfo     *gpPwVcGlobalInfo;
extern tPwVcMplsGlobalInfo *gpPwVcMplsGlobalInfo;
extern tPwVcEnetGlobalInfo *gpPwVcEnetGlobalInfo;
extern tL2VpnGlobalInfo    *gpL2VpnGlobalInfo;
extern UINT2               gu2BgpVplsLblGroupId;

extern INT4 LdpL2VpnEventHandler (VOID *pPwVcEvtInfo, UINT4 u4Event);

/* PW Redundancy */
extern INT4 LdpL2VpnGetEventInfo (VOID **ppEvtInfo);
extern INT4 LdpL2VpnCleanEventInfo (VOID *pvEvtInfo, BOOL1 b1CleanEvent, BOOL1 b1CleanNodes);
extern INT4 LdpL2VpnSendEventInfo (VOID *pEvtInfo, UINT4 u4Event);
extern INT4 LdpL2VpnGetEventPwFec (VOID **ppEvtPwFec);
extern INT4 LdpL2VpnCleanEventPwFec (VOID *pEvtPwFec);
extern INT4 LdpL2VpnGetEventPwData (VOID **ppEvtPwData);
extern INT4 LdpL2VpnCleanEventPwData (VOID *pEvtPwData);

extern UINT4 TeCheckTnlOperStatus (UINT4 u4TnlIndex, UINT2 u2TnlInstance,
                                   UINT4 u4TnlIngressLSRId,
                                   UINT4 u4TnlEgressLSRId);
extern UINT1 TeUpdateTnlAssociation (UINT1 u1AssociateFlag, UINT4 u4TnlIndex,
                                     UINT2 u2TnlInstance,
                                     UINT4 u4TnlIngressLSRId,
                                     UINT4 u4TnlEgressLSRId);

extern INT4 LdpRegisterApplication (UINT1 u1Id, UINT4 *pu4AppId);

extern INT4 LdpDeRegisterApplication (UINT4 u4Id);


extern INT1 MplsFmFtnConfigure (UINT4 u4DestAddr, UINT4 u4DestAddrPrfxLen,
                                UINT4 u4FtnTos, INT4 i4FtnFecType,
                                INT4 i4FtnTnlId, INT4 i4FtnTnlInstance,
                                UINT4 *pu4ErrorCode);
extern UINT4
LdpGetInLabelFrmPrefix (tL2vpnLabelArgs * pL2vpnLabelArgs);

extern INT1 LdpIsLdpInitialised PROTO ((VOID));
extern INT1 LdpGetFecInLabelForDestAddr(tL2vpnLabelArgs * pL2vpnLabelArgs, BOOL1 bIsManualPwVc);
extern INT1 LdpGetFecInLabelFromLoopbackIntf(BOOL1 bIsManualPwVc, tL2vpnLabelArgs * pL2vpnLabelArgs, UINT4 u4LdpIp);

extern UINT4
LdpGetSrcTransAddrFromDestAddr (tGenU4Addr *pDestTransAddr,tGenU4Addr *pSrcTransAddr,
                                BOOL1 bIsManualPwVc);

extern VOID LdpSendInvalidRgDataPkt (VOID);
#endif /*_L2VPGBEX_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file l2vpgbl.h                              */
/*---------------------------------------------------------------------------*/
