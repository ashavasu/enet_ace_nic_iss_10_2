/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwelw.h,v 1.4 2008/08/20 15:14:26 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for PwEnetTable. */
INT1
nmhValidateIndexInstancePwEnetTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwEnetTable  */

INT1
nmhGetFirstIndexPwEnetTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwEnetTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwEnetPwVlan ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetPwEnetVlanMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPwEnetPortVlan ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetPwEnetPortIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPwEnetVcIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPwEnetRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPwEnetStorageType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPwEnetPwVlan ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetPwEnetVlanMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetPwEnetPortVlan ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetPwEnetPortIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetPwEnetVcIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetPwEnetRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetPwEnetStorageType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PwEnetPwVlan ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2PwEnetVlanMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2PwEnetPortVlan ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2PwEnetPortIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2PwEnetVcIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2PwEnetRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2PwEnetStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PwEnetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PwEnetMplsPriMappingTable. */
INT1
nmhValidateIndexInstancePwEnetMplsPriMappingTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwEnetMplsPriMappingTable  */

INT1
nmhGetFirstIndexPwEnetMplsPriMappingTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwEnetMplsPriMappingTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwEnetMplsPriMapping ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwEnetMplsPriMappingRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwEnetMplsPriMappingStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPwEnetMplsPriMapping ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwEnetMplsPriMappingRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwEnetMplsPriMappingStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PwEnetMplsPriMapping ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwEnetMplsPriMappingRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwEnetMplsPriMappingStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PwEnetMplsPriMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PwEnetStatsTable. */
INT1
nmhValidateIndexInstancePwEnetStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwEnetStatsTable  */

INT1
nmhGetFirstIndexPwEnetStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwEnetStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwEnetStatsIllegalVlan ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwEnetStatsIllegalLength ARG_LIST((UINT4 ,UINT4 *));
