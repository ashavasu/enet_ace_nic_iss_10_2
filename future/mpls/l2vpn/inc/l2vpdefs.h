/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpdefs.h,v 1.67 2016/07/28 07:47:31 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vpdefs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants used across sub modules
 *---------------------------------------------------------------------------*/


#ifndef _L2VPN_DEFS_H
#define _L2VPN_DEFS_H

#define L2VPN_PWVC_FEC_TYPE        0x80
#define L2VPN_GEN_FEC_TYPE         0x81

#define L2VPN_VPWS                 1
#define L2VPN_VPLS                 2
#define L2VPN_IPLS                 3 /* Not supported */

#define L2VPN_INVALID            -1
#define L2VPN_VALID     0
/* Return Values */
#define L2VPN_OK                   1
#define L2VPN_NOT_OK               0
#define L2VPN_TRUE                 1
#define L2VPN_FALSE                0
#define L2VPN_EQUAL                0
#define L2VPN_NOT_EQUAL           (~0)
#define L2VPN_SMALLER              1
#define L2VPN_GREATER              2

#define L2VPN_RB_LESSER           -1 
#define L2VPN_RB_EQUAL             0
#define L2VPN_RB_GREATER           1

#define L2VPN_ADMIN_UP             1
#define L2VPN_ADMIN_DOWN           2
#define L2VPN_ADMIN_TESTING        3
#define L2VPN_ADMIN_NIS            4
#define LDP_UP_EVENT               5

#define L2VPN_GEN_FEC_GLOBAL_ID_OFFSET 0
#define L2VPN_GEN_FEC_NODE_ID_OFFSET   4
#define L2VPN_GEN_FEC_AC_ID_OFFSET     8

#define L2VPN_FEC_PWID_TYPE         0x80
#define L2VPN_FEC_GEN_TYPE          0x81

#define L2VPN_GEN_FEC_AGI_TYPE_1    1
#define L2VPN_GEN_FEC_AII_TYPE_1     1
#define L2VPN_GEN_FEC_AII_TYPE_2     2

#define L2VPN_ADMIN_UP_IN_PROGRESS    4
#define L2VPN_ADMIN_DOWN_IN_PROGRESS  5

#define L2VPN_DEF_TRACE               L2VPN_DBG_FNC_CRT_FLAG
#define L2VPN_INTERVAL_TIMER          1

/* Size of L2VPN Trap notification sting */
#define L2VPN_NOTIF_STR_SIZE  50

/* Pseudo Wire VC Oper status */
#define L2VPN_PWVC_OPER_UP             MPLS_OPER_UP
#define L2VPN_PWVC_OPER_DOWN           MPLS_OPER_DOWN
#define L2VPN_PWVC_OPER_TESTING        MPLS_OPER_TESTING
#define L2VPN_PWVC_OPER_UNKWN          MPLS_OPER_UNKNOWN
#define L2VPN_PWVC_OPER_DORMANT        MPLS_OPER_DORMANT
#define L2VPN_PWVC_OPER_NOTPRES        MPLS_OPER_NOT_PRESENT
#define L2VPN_PWVC_OPER_LLDOWN         MPLS_OPER_LOWER_LAYER_DOWN

/* OperStatus */
#define L2VPN_ON                   1
#define L2VPN_OFF                  0

#define L2VPN_ZERO                 0

#define L2VPN_PWVC_OPER_APP_CP_OR_MGMT 1
#define L2VPN_PWVC_OPER_APP_OAM        2

#define L2VPN_OAM_STATUS_TRAP           3
#define L2VPN_PW_STATUS_TRAP            4

#define L2VPN_MAP_RCVD  0x01
#define L2VPN_REL_WAIT  0x02
#define L2VPN_MAP_SENT  0x10

/* TruthValue as defined in RFC 1903 (Textual Convention - SNMPv2) */
#define L2VPN_SNMP_TRUE            1
#define L2VPN_SNMP_FALSE           2

#define L2VPN_SNMP_NO              0
#define L2VPN_SNMP_YES             1


/* Session State Values. */
#define L2VPN_SNMP_NON_EXISTENT    1
#define L2VPN_SNMP_INITIALIZED     2
#define L2VPN_SNMP_OPEN_RECV       3
#define L2VPN_SNMP_OPEN_SENT       4
#define L2VPN_SNMP_OPERATIONAL     5

#define L2VPN_ONE 1
#define L2VPN_TWO 2
#define L2VPN_THREE 3
#define L2VPN_UPNIB  0xf0
#define L2VPN_LOWNIB 0x0f

#define L2VPN_INVALID_LABEL        0xFFFFFFFF

/* Admin Status */
#define L2VPN_ENABLED              1
#define L2VPN_DISABLED             2

/* Storage Types */
#define L2VPN_STORAGE_OTHER        1
#define L2VPN_STORAGE_VOLATILE     2
#define L2VPN_STORAGE_NONVOLATILE  3
#define L2VPN_STORAGE_PERMANENT    4
#define L2VPN_STORAGE_READONLY     5


/* Memory Pool related */
#define L2VPN_DEF_MTU              1500
#define L2VPN_MSG_BLK_SIZE       L2VPN_DEF_MTU
#define L2VPN_MEM_TYPE           MEM_DEFAULT_MEMORY_TYPE

/* Task related */
#define L2VPN_TSK_NAME           "VPNT"

/* 
 * NOTE: This value L2VPN_TSK_PRIORITY indicates the priority at which L2VPN
 * Task is currently created with. This value needs to be suitably assigned
 * based on the final target requirements. 
 */
#define L2VPN_TSK_PRIORITY       45

/* Queue related */
#define L2VPN_QNAME              "VPNQ"    /* Name of L2VPN's Msg Queue */
#define L2VPN_RESP_QNAME         "VPNRQ" 

/* 
 * NOTE: This value L2VPN_QDEPTH indicates the maximum number of messages that
 * can be enqueued into the L2VPN's message queue. IF required this has to be
 * modified at the time of porting based on the target support.
 */
#define L2VPN_QDEPTH           256
#define L2VPN_RESP_QDEPTH      20

#define L2VPN_QMODE       OSIX_LOCAL
#define L2VPN_QFLAGS      OSIX_NO_WAIT
#define L2VPN_QTIMEOUT    0
#define L2VPN_SEM_NAME    (UINT1 *)"L2VP"
#define L2VPN_SEM_INITIAL_COUNT 10
/* BUFFER related */
#define L2VPN_DEF_BUF_SIZE       16
#define L2VPN_DEF_OFFSET         0


/* Flags */

/* Interface Type */
#define L2VPN_IFACE_ETHERNET_TYPE   0x01
#define L2VPN_IFACE_ATM_TYPE        0x02
#define L2VPN_IFACE_FR_TYPE         0x03


/* Status Types */

#define L2VPN_PWVC_LOCAL_ALLOC         1
#define L2VPN_PWVC_PEER_ALLOC          2

#define L2VPN_PWVC_MAX_IF_STR_LEN  MAX_LDP_L2VPN_IF_STR_LEN
#define L2VPN_PWVC_MAX_NAME_LEN    L2VPN_PWVC_MAX_LEN
#define L2VPN_PWVC_MAX_DSCR_LEN    L2VPN_PWVC_MAX_LEN
#define L2VPN_PWVC_MAX_AI_LEN      L2VPN_MAX_AI_LEN

#define L2VPN_PWVC_MAX_AGI_LEN           8
#define L2VPN_PWVC_MAX_AI_TYPE1_LEN      4
#define L2VPN_PWVC_MAX_AI_TYPE2_LEN      12

#define L2VPN_PWVC_AGI_TYPE0             0
#define L2VPN_PWVC_AGI_TYPE1             1
#define L2VPN_PWVC_AGI_TYPE2             2

#define L2VPN_PWVC_AGI_TYPE0_ASN_OFFSET  2
#define L2VPN_PWVC_AGI_TYPE1_ASN_OFFSET  2
#define L2VPN_PWVC_AGI_TYPE2_ASN_OFFSET  2

#define L2VPN_PWVC_AGI_TYPE0_ASN_LENGTH  2
#define L2VPN_PWVC_AGI_TYPE1_ASN_LENGTH  4
#define L2VPN_PWVC_AGI_TYPE2_ASN_LENGTH  4

#define L2VPN_PWVC_AGI_TYPE0_ID_OFFSET   4
#define L2VPN_PWVC_AGI_TYPE1_ID_OFFSET   6
#define L2VPN_PWVC_AGI_TYPE2_ID_OFFSET   6

#define L2VPN_PWVC_AGI_TYPE0_ID_LENGTH   4
#define L2VPN_PWVC_AGI_TYPE1_ID_LENGTH   2
#define L2VPN_PWVC_AGI_TYPE2_ID_LENGTH   2

#define L2VPN_PWVC_SAII_IP  0x80
#define L2VPN_PWVC_TAII_IP  0x40

#define L2VPN_PWVC_MAX_PEER_ADDR_LEN    256

/* Row Status */
#define L2VPN_PWVC_ACTIVE         1

#define L2VPN_PWVC_NOTINSERVICE   2
#define L2VPN_PWVC_NOTREADY       3
#define L2VPN_PWVC_CREATEANDGO    4
#define L2VPN_PWVC_CREATEANDWAIT  5
#define L2VPN_PWVC_DESTROY        6
#define L2VPN_PWVC_CREATE         7

/* PseudoWire Type */
#define L2VPN_PWVC_TYPE_ETH_VLAN  0x0004
#define L2VPN_PWVC_TYPE_ETH       0x0005

/* PSN Types */
#define L2VPN_PWVC_PSN_TYPE_MPLS        1
#define L2VPN_PWVC_PSN_TYPE_L2TP        2
#define L2VPN_PWVC_PSN_TYPE_IP          3
#define L2VPN_PWVC_PSN_TYPE_MPLSOVERIP  4
#define L2VPN_PWVC_PSN_TYPE_GRE         5
#define L2VPN_PWVC_PSN_TYPE_OTHER       6

#define L2VPN_PWVC_SETUP_PRIO_MINVAL   0
#define L2VPN_PWVC_SETUP_PRIO_MAXVAL   7

#define L2VPN_PWVC_HOLD_PRIO_MINVAL   0
#define L2VPN_PWVC_HOLD_PRIO_MAXVAL   7

#define L2VPN_PWVC_IPV4_ADDR_TYPE     1
#define L2VPN_PWVC_IPV6_ADDR_TYPE     2
#define L2VPN_PWVC_UNKNOWN_ADDR_TYPE  3

#define L2VPN_PWVC_MIN_PW_IF_IDX      0

#define L2VPN_PWVC_CTRLW_NOTSUPPORTED   0
#define L2VPN_PWVC_CTRLW_PREFERRED      1
#define L2VPN_PWVC_CTRLW_NOT_PREFERRED  2
#define L2VPN_PWVC_CTRLW_MANDATORY      3
#define L2VPN_PWVC_CTRLW_NOT_MANDATORY  4

/* Status of the Control Word negotiation */
#define L2VPN_PWVC_WAITINGFORNEXTMSG         1
#define L2VPN_PWVC_SENTWRONGBITERRORCODE     2
#define L2VPN_PWVC_RXWD_WWRONGBITERRORCODE   3
#define L2VPN_PWVC_ILLE_RECV_BIT             4
#define L2VPN_PWVC_CWPRESENT                 5
#define L2VPN_PWVC_CWNOTPRESENT              6
#define L2VPN_PWVC_NOTYETKNOWN               7

/* Status of Fragmentation */
#define L2VPN_PWVC_NO_FRAG                  0
#define L2VPN_PWVC_CFG_FRAG_GRTHAN_PSNMTU   1
#define L2VPN_PWVC_CFG_FRAG_BUT_RMT_INCAP   2
#define L2VPN_PWVC_RMT_FRAG_CAP             3
#define L2VPN_PWVC_FRAG_ENABLED             4

/* Local configuration of FCS Retention for the PW */
#define L2VPN_PWVC_FCSRETN_DISABLE   1
#define L2VPN_PWVC_FCSRETN_ENABLE    2

/* FCS Retention negotiation status */
#define L2VPN_PWVC_RMT_IND_UNKNOWN         0
#define L2VPN_PWVC_RMT_REQ_FCS_RETN        1
#define L2VPN_PWVC_FCS_RETN_ENABLED        2
#define L2VPN_PWVC_FCS_RETN_DISABLED       3
#define L2VPN_PWVC_LCL_FCS_RETN_CFG_ERR    4
#define L2VPN_PWVC_FCS_SIZE_MISMATCH       5

/* Pseudowire Status */
#define L2VPN_PWVC_STATUS_FORWARDING            0x00
#define L2VPN_PWVC_STATUS_NOT_FORWARDING        0x01
#define L2VPN_PWVC_STATUS_CUST_FACE_RX_FAULT    0x02
#define L2VPN_PWVC_STATUS_CUST_FACE_TX_FAULT    0x04
#define L2VPN_PWVC_STATUS_PSN_FACE_RX_FAULT     0x08
#define L2VPN_PWVC_STATUS_PSN_FACE_TX_FAULT     0x10
/* 1st (pwNotForwarding), 4th (PSN Rx Fault), 5th (PSN Tx Fault) Bit set */
#define L2VPN_PWVC_STATUS_PSN_BITMASK           0x19
/* 2nd (AC Rx Fault), 3rd (AC Tx Fault) bit set */
#define L2VPN_PWVC_STATUS_AC_BITMASK            0x06

#define L2VPN_PWVC_IS_ONLY_AC_FAULT(pPwVcEntry)  \
    ((((L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) == \
        L2VPN_PWVC_STATUS_AC_BITMASK) &&\
       (L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) == \
        L2VPN_PWVC_STATUS_FORWARDING))\
      ||\
      ((L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) == \
        L2VPN_PWVC_STATUS_AC_BITMASK) &&\
       (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) == \
        L2VPN_PWVC_STATUS_FORWARDING))\
      ||\
      ((L2VPN_PWVC_REMOTE_STATUS (pPwVcEntry) == \
        L2VPN_PWVC_STATUS_AC_BITMASK) &&\
       (L2VPN_PWVC_LOCAL_STATUS (pPwVcEntry) == \
        L2VPN_PWVC_STATUS_AC_BITMASK))) ? L2VPN_TRUE : L2VPN_FALSE)

/* Remote Node Capability */
#define L2VPN_PWVC_RMT_STAT_NOT_APPL       1
#define L2VPN_PWVC_RMT_STAT_NOT_YET_KNOWN  2
#define L2VPN_PWVC_RMT_STAT_CAPABLE        3
#define L2VPN_PWVC_RMT_STAT_NOT_CAPABLE    4

#define L2VPN_PWVC_MIN_MTU_SIZE   0
#define L2VPN_PWVC_MAX_MTU_SIZE   65535

#define L2VPN_PWVC_REMOTE_CTRLW_YES        1
#define L2VPN_PWVC_REMOTE_CTRLW_NO         2
#define L2VPN_PWVC_REMOTE_CTRLW_UNKNOWN    3

#define L2VPN_PWVC_IN_LBL_MIN_VAL    160001
#define L2VPN_PWVC_IN_LBL_MAX_VAL    200000

#define L2VPN_PWVC_OUT_LBL_MIN_VAL   160001
#define L2VPN_PWVC_OUT_LBL_MAX_VAL   200000

#define L2VPN_PWVC_NAME_LEN   256

#define L2VPN_PWVC_STORAGE_OTHER         1
#define L2VPN_PWVC_STORAGE_VOLATILE      2
#define L2VPN_PWVC_STORAGE_NONVOLATILE   3
#define L2VPN_PWVC_STORAGE_PERMANENT     4
#define L2VPN_PWVC_STORAGE_READONLY      5

/* Default Values */
#define L2VPN_PWVC_DEF_VC_TYPE          0x0005
#define L2VPN_PWVC_DEF_VC_OWNER         2
#define L2VPN_PWVC_DEF_PSN_TYPE         1
#define L2VPN_PWVC_DEF_SETUP_PRIO       0
#define L2VPN_PWVC_DEF_HOLD_PRIO        0
#define L2VPN_PWVC_DEF_PADDR_TYPE       1
#define L2VPN_PWVC_DEF_CW_PRESENT       5
#define L2VPN_PWVC_DEF_LOCAL_IFMTU      0
#define L2VPN_PWVC_DEF_ADMIN_STATUS     2
#define L2VPN_PWVC_DEF_OPER_STATUS      2
#define L2VPN_PWVC_CP_DEF_OPER_STATUS   L2VPN_PWVC_OPER_UNKWN  
#define L2VPN_PWVC_DEF_LOCAL_STATUS     1
#define L2VPN_PWVC_DEF_REMOTE_STATUS    1
#define L2VPN_PWVC_DEF_TIME_ELAPSED     0
#define L2VPN_PWVC_DEF_VALID_INTERVAL   0
#define L2VPN_PWVC_DEF_STORAGE_TYPE     2

#define L2VPN_PWVC_DEF_SNMP_FALSE       2
#define L2VPN_PWVC_DEF_ATTACHED_PW_IDX  0
#define L2VPN_PWVC_DEF_PW_IF_IDX       0
#define L2VPN_PWVC_DEF_LCL_CAP_ADV   0
#define L2VPN_PWVC_DEF_RMT_CAP_ADV   0
#define L2VPN_PWVC_DEF_REMOTE_IFMTU   0
#define L2VPN_PWVC_DEF_FRAG_CFG_SIZE   0
#define L2VPN_PWVC_DEF_RMT_FRAG_CAP   0
#define L2VPN_PWVC_DEF_FCS_RETN_CFG   1
#define L2VPN_PWVC_DEF_FCS_RETN_STAT   3
#define L2VPN_PWVC_DEF_RMT_STAT_CAP   2

#define L2VPN_MPLS_DEF_MPLS_TYPE_NONTE  0x02
#define L2VPN_MPLS_DEF_EBIT_MD_OUT_TNL  1
#define L2VPN_MPLS_DEF_EBITS          0
#define L2VPN_MPLS_DEF_TTL          2

#define L2VPN_PWVC_ENET_DEF_PW_VLAN    4097
#define L2VPN_PWVC_ENET_DEF_VLAN_MODE  2

#define L2VPN_HASH_SIZE         32
#define L2VPN_PWVC_MPLS_PSN_TNL_STAT_UNKNOWN 0xff

#define L2VPN_MANDATORY                                    1
#define L2VPN_OPTIONAL                                     2

#define L2VPN_PREFERRED                                    1
#define L2VPN_NOT_PREFERRED                                2

/*********** Events definition ******************/

/* L2VPN task level Events */
#define L2VPN_MSG_EVENT                           0x00000001
#define L2VPN_CLEANUP_TMR_EXP_EVENT               0x00000002
#define L2VPN_PWVC_PERF_TMR_EXP_EVENT             0x00000004
#define L2VPN_ADMIN_UP_EVENT                      0x00000008
#define L2VPN_ADMIN_DOWN_EVENT                    0x00000010
#define L2VPN_RECOVERY_TMR_EXP_EVENT              0x00000020
#define L2VPN_TMR_EVENT                           0x00000040
#define L2VPNRED_NEG_TMR_EVENT                    0x00000080
#define L2VPN_LDP_UP_EVT                          0x00000100
#define L2VPN_PEND_MSG_TO_LDP_TMR_EVENT           0x00000200
#ifdef VPLS_GR_WANTED
#define L2VPN_BGP_GR_TMR_EXP_EVENT      0x00000400
#endif
#ifdef HVPLS_WANTED
#define L2VPN_NOTIF_RATE_TMR_EXP_EVENT              0x00000800
#endif

/* L2VPN Q Message Types */
#define L2VPN_ADMIN_EVENT                         0x80000000
#define L2VPN_PWVC_ADMIN_EVENT                    0x40000000
#define L2VPN_PWVC_PSN_ADMIN_EVENT                0x20000000
#define L2VPN_PWVC_SERV_ADMIN_EVENT               0x10000000
#define L2VPN_RED_GROUP_ADMIN_EVENT               0x08000000
#define L2VPN_RED_NODE_ADMIN_EVENT                0x04000000
#define L2VPN_RED_PW_ADMIN_EVENT                  0x02000000
#ifdef VPLSADS_WANTED
#define L2VPN_VPLS_ADMIN_EVENT                    0x01000000
#endif

#define L2VPN_PWVC_SIG_EVENT                      0x08000000
#define L2VPN_PWVC_PSN_EVENT                      0x04000000
#define L2VPN_PWVC_FWD_EVENT                      0x02000000
#define L2VPN_PWVC_IF_EVENT                       0x01000000
#define L2VPN_PWVC_ROUTE_EVENT                    0x00800000
#define L2VPN_PWVC_OAM_MEG_EVENT                  0x00400000
#define L2VPN_MSPW_EVENT                          0x00400000  /*MS-PW */
#define L2VPN_DLAG_AC_STATUS_EVENT                  0x00200000  /* PW-RED */

#ifdef VPLSADS_WANTED
#define L2VPN_BGP_SIG_EVENT                       0x00100000  /* BGP */
#endif
#define L2VPN_RM_EVENT                            0x00080000 /* L2VPN-HA */
/* Admin Event Types */
#define L2VPN_PWVC_ADMIN_UP_EVENT                 0x40000001
#define L2VPN_PWVC_ADMIN_DOWN_EVENT               0x40000002
#define L2VPN_PWVC_NIS_EVENT                      0x40000004

#ifdef VPLSADS_WANTED
/* VPLS Admin Event Types */
#define L2VPN_VPLS_UP_EVENT                       0x80000000
#define L2VPN_VPLS_DOWN_EVENT                     0x40000000
#define L2VPN_VPLS_DELETE_EVENT                   0x20000000
#define L2VPN_VPLS_ROW_DELETE_EVENT               0x10000000
#define L2VPN_VPLS_ASSOC_DELETE_EVENT             0x08000000
#define L2VPN_VPLS_RT_ADD_EVENT                   0x04000000
#define L2VPN_VPLS_RT_DELETE_EVENT                0x02000000
#endif

#define L2VPN_PWVC_MPLS_IN_TNL_ACTIVE             0x20000001
#define L2VPN_PWVC_MPLS_IN_TNL_DESTROY            0x20000002
#define L2VPN_PWVC_MPLS_IN_TNL_NIS                0x20000004
#define L2VPN_PWVC_MPLS_IN_TNL_DOWN               0x20000008
#define L2VPN_PWVC_MPLS_OUT_TNL_ACTIVE            0x20000010
#define L2VPN_PWVC_MPLS_OUT_TNL_DESTROY           0x20000020
#define L2VPN_PWVC_MPLS_OUT_TNL_NIS               0x20000040
#define L2VPN_PWVC_MPLS_OUT_TNL_DOWN              0x20000080

#define L2VPN_PWVC_ENET_ACTIVE                    0x10000001
#define L2VPN_PWVC_ENET_DESTROY                   0x10000002
#define L2VPN_PWVC_ENET_NIS                       0x10000004
#define L2VPN_PWVC_ENET_DOWN                      0x10000008
#define L2VPN_PWVC_ENET_MPLS_PRI_MAP_ACTIVE       0x10000010
#define L2VPN_PWVC_ENET_MPLS_PRI_MAP_DESTROY      0x10000020
#define L2VPN_PWVC_ENET_MPLS_PRI_MAP_NIS          0x10000040
#define L2VPN_PWVC_ENET_MPLS_PRI_MAP_DOWN         0x10000080

#define L2VPN_MSPW_ACTIVE_EVENT                   0x00400001 /*MS-PW */
#define L2VPN_MSPW_DESTROY_EVENT                  0x00400002 /*MS-PW */

/* Interface Messages */
#define L2VPN_IF_UP                               0x01000001
#define L2VPN_IF_DOWN                             0x01000002

#define L2VPN_FM_PWVC_DOWN_EVENT                  0x00800001
#define L2VPN_FM_PWVC_UP_EVENT                    0x00800002
#define L2VPN_FM_PWVC_RESTART_EVENT               0x00800004

/* u4PsnType in tPwVcPSNAdminEvtInfo - all the other standard types
 * can be defined here */
#define L2VPN_PWVC_PSN_MPLS                       0x00100001

/* u4ServSpecType in tPwVcServSpecAdminEvtInfo - all the other standard
 * types can be defined here */
#define L2VPN_PWVC_SERV_ETH                       0x00100001
#define L2VPN_PWVC_SERV_ETH_VLAN                  0x00100002
#define L2VPN_PWVC_SERV_ETH_VPLS                  0x00100004

/* u4SigType in tPwVcSigEvtInfo - all the other standard
 * types can be defined here */
#define L2VPN_PWVC_SIG_LDP                        0x00010001
#define L2VPN_SIG_LDP                             0x00000001

/* u4MplsType in tL2VpnMplsPwVcPSNEvtInfo - all the other standard 
 * types can be defined here */
#define L2VPN_PWVC_MPLS_TNL                       0x00000101
#define L2VPN_PWVC_MPLS_LSP                       0x00000102
#define L2VPN_PWVC_MPLS_VC                        0x00000103

/* Configuration constants */

#define L2VPN_MAX_PSN_TYPES                   6
#define L2VPN_PSN_TYPE_MPLS                   1
#define L2VPN_MAX_VC_TYPES                   10

/* for accessing global stats */
/* add for additional types   */
#define L2VPN_PSN_STAT_MPLS                   0
#define L2VPN_SERV_STAT_ETH                   0

#define L2VPN_PWVC_MAX_STR_LEN                MAX_LDP_L2VPN_IF_STR_LEN

#define L2VPN_PWVC_PERF_INTV_IN_MTS         15
#define L2VPN_PWVC_MIN_PERF_INTV            1
#define L2VPN_PWVC_MAX_PERF_INTV            (24*60/L2VPN_PWVC_PERF_INTV_IN_MTS)
#define L2VPN_PWVC_MAX_PERF_DAYS            2
/* Performance counter interval in milliseconds */
#define L2VPN_PWVC_PERF_INTV_IN_MS          (L2VPN_PWVC_PERF_INTV_IN_MTS*60*1000)

/* Pending Lbl Map Message Sending timer is 2 seconds */
#define L2VPN_PEND_LBL_MAP_SEND_TIME          2

/* FS-L2VPN-MIB related min, max and
 *  * default values */
#define L2VPN_NO_TRACE_FLAG                   0x0000
#define L2VPN_CRT_ERR_FLAG                    0x0001
#define DBG_ERR_NCRT_FLAG                     0x0002
#define L2VPN_CRT_DBG_FLAG                    0x0003
#define L2VPN_NCRT_DBG_FLAG                   0x0004
#define L2VPN_IN_PKT_DBG_FLAG                 0x1000
#define L2VPN_OUT_PKT_DBG_FLAG                0x2000
#define L2VPN_DEF_PWVC_CLEANUP_INTV     (10*60*SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
#define L2VPN_PWVC_PERF_TIMER_INTV      (L2VPN_PWVC_PERF_INTV_IN_MTS*60*SYS_NUM_OF_TIME_UNITS_IN_A_SEC)

#define L2VPN_MIN_PWVC_ENTRIES            1

#define L2VPN_MIN_PWVC_MPLS_ENTRIES            1
#define L2VPN_MAXIMUM_PWVC_MPLS_ENTRIES        L2VPN_DEF_PWVC_MPLS_ENTRIES

#define L2VPN_MIN_PWVC_MPLS_IO_ENTRIES            1
#define L2VPN_MAX_PWVC_MPLS_IO_ENTRIES        65535
#define L2VPN_DEF_PWVC_MPLS_IO_ENTRIES           16

#define L2VPN_MIN_PWVC_ENET_SS_ENTRIES            1
#define L2VPN_MAX_PWVC_ENET_SS_ENTRIES        L2VPN_DEF_PWVC_ENET_SS_ENTRIES

#define L2VPN_MIN_PWVC_ENET_ENTRIES            1
#define L2VPN_MAX_PWVC_ENET_ENTRIES        L2VPN_DEF_PWVC_ENET_ENTRIES
#define L2VPN_MAX_PWVC_BIND_ENTRIES        MAX_L2VPN_PW_VC_ENTRIES

#define L2VPN_DEF_ENET_MPLS_PRIMAP_ENTRIES       16
/* PW-MIB related min, max and default
 *  * values */

/* PW-Enet-mib related min, max and default
 *  * values */
#define L2VPN_PWVC_INDEX_MINVAL                 1

#define L2VPN_PWVC_ENET_VLAN_MINVAL           0
#define L2VPN_PWVC_ENET_VLAN_MAXVAL           4097

#define L2VPN_MPLS_TYPE_TENONTE               0x03
#define L2VPN_MPLS_TYPE_TEVCONLY              0x05
#define L2VPN_MPLS_TYPE_NONTEVCONLY           0x06
#define L2VPN_MPLS_TYPE_TENONTEVCONLY         0x07

#define L2VPN_TUNNEL_TYPE_IN_USE_NOT_YET_KNOWN        1
#define L2VPN_TUNNEL_TYPE_IN_USE_MPLS_TE              2
#define L2VPN_TUNNEL_TYPE_IN_USE_MPLS_NONTE           3
#define L2VPN_TUNNEL_TYPE_IN_USE_VCONLY               4

#define L2VPN_OUTBOUND_DIR                    2
#define L2VPN_INBOUND_DIR                     1

#define L2VPN_MPLS_EBIT_MD_OUT_TNL            1
#define L2VPN_MPLS_EBIT_MD_SPEC_VAL           2
#define L2VPN_MPLS_EBIT_MD_SER_DEP            3
#define L2VPN_MAX_LDPID_LEN                   6
#define L2VPN_MPLS_EXP_BIT_MAX                7
#define L2VPN_UINT1_MAX                     255
#define TE_TNL_INDEX_MAXVAL                  65535
#define TE_TNL_INST_MAXVAL                   1024
#define L2VPN_MAX_LSR_XC_INDEX_LEN    24

#define L2VPN_MPLS_TE_TNL_STAT_UP                     1
#define L2VPN_MPLS_TE_TNL_STAT_DOWN                   2
#define L2VPN_MPLS_TE_TNL_STAT_UNKNOWN                0
#define L2VPN_MPLS_LSP_STAT_UP                    1
#define L2VPN_MPLS_LSP_STAT_DOWN                  2
#define L2VPN_MPLS_LSP_STAT_UNKNOWN               0
#define L2VPN_MPLS_PWVC_VC_STAT_UP                     1
#define L2VPN_MPLS_PWVC_VC_STAT_DOWN                   2
#define L2VPN_MPLS_PWVC_VC_STAT_UNKNOWN                0

#define L2VPN_VLANMODE_OTHER                 0
#define L2VPN_VLANMODE_PORTBASED             1
#define L2VPN_VLANMODE_NOCHANGE              2
#define L2VPN_VLANMODE_CHANGEVLAN            3
#define L2VPN_VLANMODE_ADDVLAN               4
#define L2VPN_VLANMODE_REMOVEVLAN            5

#define L2VPN_VLANCFG         4097       
#define NON_APPLICABLE_OBJECT 4097
/* MPLSPriMappingTable */
#define PRI_MAPPING_MASK 0xFF80
#define PRI0_MAPPING     0x0001
#define PRI1_MAPPING  0x0002
#define PRI2_MAPPING        0x0004
#define PRI3_MAPPING      0x0008
#define PRI4_MAPPING     0x0010
#define PRI5_MAPPING        0x0020
#define PRI6_MAPPING     0x0040
#define PRI7_MAPPING     0x0080
#define UNTAGGED_MAPPING    0x0100

#define REV_PRI0_MAPPING        0x8000
#define REV_PRI1_MAPPING        0x4000
#define REV_PRI2_MAPPING        0x2000
#define REV_PRI3_MAPPING        0x1000
#define REV_PRI4_MAPPING        0x0800
#define REV_PRI5_MAPPING        0x0400
#define REV_PRI6_MAPPING        0x0200
#define REV_PRI7_MAPPING        0x0100
#define REV_UNTAGGED_MAPPING    0x0080

#define  L2VPN_BCAST_ADDR                 0xffffffff
#define  L2VPN_MCAST_NET_ADDR             0xe0000000
#define  L2VPN_MCAST_NET_INV_ADDR         0xe00000ff
#define  L2VPN_NULL_ADDR                  0x00000000

/* debugs definition - dummy */
#define DBG_ERR_NCRT   0x0000ffff
#define DBG_TRC_INF    0x00ffff00

#define L2VPN_LDP_EVENT 0x00800001

#define L2VPN_SSN_CREATE              1
#define L2VPN_SSN_ADD                 2
#define L2VPN_SSN_DELETE              3
#define L2VPN_PWVC_ADD_IN_SESSION     4
#define L2VPN_PWVC_DELETE_IN_SESSION  5
#define L2VPN_ICCP_ADD_IN_SESSION     6
#define L2VPN_ICCP_DELETE_IN_SESSION  7

#define L2VPN_TNL_IN  1
#define L2VPN_TNL_OUT 2

#define L2VPN_PWVC_TNL_UP              1
#define L2VPN_PWVC_TNL_DOWN            2
#define L2VPN_PWVC_TNL_REESTB          8
#define L2VPN_PWVC_TE_FRR_MP_ADD       9
#define L2VPN_PWVC_TE_FRR_MP_DEL       10
#define L2VPN_PWVC_FRR_TNL_PLR_ADD     11
#define L2VPN_PWVC_FRR_TNL_PLR_DEL     12
#define L2VPN_PWVC_P2MP_LSP_BRANCH_ADD 13
#define L2VPN_PWVC_P2MP_LSP_BRANCH_DELETE 14

#define L2VPN_PWVC_PSN_TNL_UP        101
#define L2VPN_PWVC_PSN_TNL_DOWN      102
#define L2VPN_PWVC_PSN_TNL_DESTROY   106

#define L2VPN_IF_STAT_UP      1
#define L2VPN_IF_STAT_DOWN    2
#define L2VPN_PWVC_AC_IF_DOWN 3

#define L2VPN_APP_L2VPN       1
#define L2VPN_IPV4ADR_LEN     4

#define L2VPN_SESSION_UP   1
#define L2VPN_SESSION_DOWN 2

#define L2VPN_LDP_STAT_PWVC_C_BIT_NONE      0
#define L2VPN_LDP_STAT_PWVC_WRONG_C_BIT     1
#define L2VPN_LDP_STAT_PWVC_ILLEGAL_C_BIT   2
#define L2VPN_LDP_STAT_PWVC_LBL_WR_NOTSUPP  3
#define L2VPN_LDP_STAT_PWVC_GEN_MISCONF_ERR 4
#define L2VPN_LDP_STAT_PWVC_UNASSIGNED_TAI  5

#define L2VPN_TE_TNL_STAT_REQ        0x02000001
#define L2VPN_TE_TNL_ASSOCIATE_REQ   0x02000002

#define L2VPN_PW_INACTIVE_FLAG_SET  1

#define L2VPN_ZERO_IP           0x0 

#define L2VPN_VCCV_MAX_CC_CV_LEN 8

/* GenFec AGI Types */
#define L2VPN_GEN_PWVC_AGI_TYPE_1        1

/* VPLS related default values */
#define L2VPN_MAX_VPLS_DESCR_LEN                   32
#define L2VPN_MAX_VPLS_VPNID_LEN                    7
#define L2VPN_VPLS_DEF_HIGH_THRESHOLD              95
#define L2VPN_VPLS_DEF_LOW_THRESHOLD               90   

#ifdef VPLSADS_WANTED
#define L2VPN_VPLS_VBS_DEFAULT_VALUE              10
#define L2VPN_VPLS_DEFAULT_NUMBER_OF_SITES        50

#define L2VPN_MAX_RD_RT_STRING_LEN                32

#define L2VPN_VPLS_MAX_LB                         ( L2VPN_VPLS_DEFAULT_NUMBER_OF_SITES / L2VPN_VPLS_VBS_DEFAULT_VALUE )

#define L2VPN_VPLS_OPER_STAT_OTHER                 0
#define L2VPN_VPLS_OPER_STAT_UP                    1
#define L2VPN_VPLS_OPER_STAT_DOWN                  2

#define L2VPN_VPLS_DEFAULT_MTU                     1518

#define L2VPN_VPLS_RT_IMPORT                       1
#define L2VPN_VPLS_RT_EXPORT                       2
#define L2VPN_VPLS_RT_BOTH                         3

#define L2VPN_VPLS_RD_TYPE_MIN                     L2VPN_VPLS_RD_TYPE_0
#define L2VPN_VPLS_RD_TYPE_0                       0
#define L2VPN_VPLS_RD_TYPE_1                       1
#define L2VPN_VPLS_RD_TYPE_2                       2
#define L2VPN_VPLS_RD_TYPE_MAX                     L2VPN_VPLS_RD_TYPE_2

#define L2VPN_VPLS_RT_TYPE_MIN                     L2VPN_VPLS_RT_TYPE_0
#define L2VPN_VPLS_RT_TYPE_0                       0
#define L2VPN_VPLS_RT_TYPE_1                       1
#define L2VPN_VPLS_RT_TYPE_2                       2
#define L2VPN_VPLS_RT_TYPE_MAX                     L2VPN_VPLS_RT_TYPE_2

#define L2VPN_VPLS_RD_SUBTYPE                      0
#define L2VPN_VPLS_RT_SUBTYPE                      2

#define L2VPN_VPLSAC_INVALID_PORT_INDEX         0xFFFFFFFF
#define L2VPN_VPLSAC_INVALID_VLAN_ID         0xFFFFFFFF
#define L2VPN_VPLS_AC_OPER_STATUS_UP               1
#define L2VPN_VPLS_AC_OPER_STATUS_DOWN             2
#endif
#ifdef HVPLS_WANTED
#define L2VPN_VPLS_PW_BIND_INVALID_CONFIG_TYPE   0
#define L2VPN_VPLS_PW_BIND_INVALID_BIND_TYPE     0
#define VPLS_FWD_FULL_ALARM_RAISED           2
#define VPLS_FWD_FULL_ALARM_CLEARED          3
#define L2VPN_VPLS_PW_BIND_SIG_LDP           1
#define L2VPN_VPLS_PW_BIND_SIG_BGP           2
#endif


enum {
    L2VPN_VPLS_SIG_MIN = 0,
    L2VPN_VPLS_SIG_LDP,
    L2VPN_VPLS_SIG_BGP,
    L2VPN_VPLS_SIG_NONE,
    L2VPN_VPLS_SIG_MAX
};
enum {
    L2VPN_VPLS_TYPE_MIN = 0,
    L2VPN_VPLS_PW_MESH,
    L2VPN_VPLS_PW_SPOKE,
    L2VPN_VPLS_TYPE_MAX
};
/* SYS_DEF_MAX_NUM_CONTEXTS - 1*/
#define L2VPN_MAX_VSI_ENTRIES                     255 
#define L2VPN_VPLS_MAX_FDB_THRESHOLD              100

/* VPLS FDB : the following macros should be replaced with orig macro 
 * provided by VLAN */
#define L2VPN_VPLS_FDB_DEF_VAL    L2VPN_VPLS_FDB_MIN_VAL
#define L2VPN_VPLS_FDB_MIN_VAL    VLAN_VFI_MIN_ID
#define L2VPN_VPLS_FDB_MAX_VAL    VLAN_VFI_MAX_ID


#define L2VPN_COMPUTE_HASH_INDEX(u4IfIndex) (u4IfIndex%L2VPN_HASH_SIZE)

#define L2VPN_PSN_TNL_IN        0x01
#define L2VPN_PSN_TNL_OUT       0x02
#define L2VPN_PSN_TNL_BOTH      0x03

#define L2VPN_PW_WORKING_PATH      0
#define L2VPN_PW_PROTECTION_PATH   1

#define L2VPN_PW_PSN_TNL_TYPE_P2P  0
#define L2VPN_PW_PSN_TNL_TYPE_P2MP 1

enum
{
    L2VPN_PWVC_ROUTE_UNKNOWN   =  0,    /* Route for this PW is still not known. 
                                           This is only the Initial Value */
    L2VPN_PWVC_ROUTE_KNOWN     =  1,    /* Route for this PW is known. PW is moved
                                           to this state if during database updation 
                                           Route is learnt. */
    L2VPN_PWVC_ROUTE_FAILED    =  2     /* Route for this PW that was failed during  
                                           database updation for PW. */
};
/* GR Related Macros */
#define L2VPN_GR_NOT_STARTED           1
#define L2VPN_GR_SHUT_DOWN_IN_PROGRESS 2
#define L2VPN_GR_RECONNECT_IN_PROGRESS 3
#define L2VPN_GR_ABORTED               4
#define L2VPN_GR_RECOVERY_IN_PROGRESS  5
#define L2VPN_GR_COMPLETED             6

/* PW Redundancy */

/* CLI constants */
#define L2VPN_PWRED_ENABLED                 1
#define L2VPN_PWRED_DISABLED                2
#define L2VPN_PWRED_ENABLE_DEFAULT          L2VPN_PWRED_ENABLED

#define L2VPN_PWRED_DUAL_HOME_LEN           1
#define L2VPN_PWRED_MODE_REVERTIVE          1
#define L2VPN_PWRED_MODE_NON_REVERTIVE      2

#define L2VPNRED_LOCKOUT_DISABLE            0
#define L2VPNRED_LOCKOUT_ENABLE             1
#define L2VPNRED_LOCKOUT_DEFAULT            L2VPNRED_LOCKOUT_DISABLE

#define L2VPN_PWRED_GRP_MIN_WTR_VAL         1
#define L2VPN_PWRED_GRP_MAX_WTR_VAL         180

#define L2VPN_BCAST_ADDR             0xffffffff
#define L2VPN_LOOPBACK_ADDR          0x7f000001
#define L2VPN_MCAST_NET_ADDR         0xe0000000
#define L2VPN_MCAST_NET_INV_ADDR     0xe00000ff
#define L2VPN_NULL_IPADDR          0x00000000

#define L2VPN_PWRED_SYNC_NOTIF_DISABLE      0

#define L2VPN_PWRED_SYNC_NOTIF_ENABLE       1

#define L2VPN_PWRED_PWSTATUS_NOTIF_ENABLE   1
#define L2VPN_PWRED_PWSTATUS_NOTIF_DISABLE  0

#define L2VPN_PWRED_GROUP_CREATE            1
#define L2VPN_PWRED_GROUP_DELETE            2

#define L2VPN_PWRED_REVERT_ENABLE           L2VPN_PWRED_MODE_REVERTIVE
#define L2VPN_PWRED_REVERT_DISABLE          L2VPN_PWRED_MODE_NON_REVERTIVE


#define L2VPN_PWRED_INIT_DUAL_HOME          0x00000000

#define L2VPN_PWRED_MANUAL_SWITCHOVER_CMD   5

#define L2VPN_PWRED_CLASS_MODE              "pw-red-class"
#define L2VPN_PWRED_NODE_MODE               "iccp-grp"
#define L2VPN_PWRED_MODE                    "pw-redundancy"
#define L2VPN_PWRED_BACKUP_MODE             "backup-peer"

#define L2VPN_IS_PSW_REMOTE                 1

#define L2VPN_PWRED_INIT_DUAL_HOME         0x00000000

#define L2VPN_PWRED_GRP_NAME_LEN           33

#define L2VPN_PWRED_GRP_DEF_NEG_TIMEOUT     60

#define L2VPN_IS_PSW_REMOTE        1

#define L2VPN_PWRED_MAXPW_FOR_SWITCH_MODE  2
#define L2VPN_WAIT_FOR_RESP                       1
#define L2VPN_DONT_WAIT_FOR_RESP                  (2)


#define L2VPN_TRAP_DLAG_ACTIVE_TO_STANDBY       1
#define L2VPN_TRAP_DLAG_STANDBY_TO_ACTIVE       2
#define SNMP_V2_TRAP_OID_LEN                   15

enum
{
    L2VPN_PWRED_SHOW_PG_CONFIG
};

enum
{
    /* RG Group events */
    L2VPNRED_EVENT_RG_CREATE                = 201,
    L2VPNRED_EVENT_RG_ACTIVATE,
    L2VPNRED_EVENT_RG_DEACTIVATE,
    L2VPNRED_EVENT_RG_DESTROY,

    /* RG Sibling Node events */
    L2VPNRED_EVENT_NODE_CREATE,
    L2VPNRED_EVENT_NODE_ACTIVATE,
    L2VPNRED_EVENT_NODE_SESSION_UP,
    L2VPNRED_EVENT_NODE_SESSION_DOWN,
    L2VPNRED_EVENT_NODE_PW_DATA_CHANGE,
    L2VPNRED_EVENT_NODE_DEACTIVATE,
    L2VPNRED_EVENT_NODE_DESTROY,

    /* RG PW events */
    L2VPNRED_EVENT_PW_CREATE,
    L2VPNRED_EVENT_PW_ACTIVATE,
    L2VPNRED_EVENT_PW_STATE_CHANGE,
    L2VPNRED_EVENT_PW_SWITCHOVER_LOCAL,
    L2VPNRED_EVENT_PW_DEACTIVATE,
    L2VPNRED_EVENT_PW_DESTROY,

    /* RG PW AC events */
    L2VPNRED_EVENT_AC_STATE_CHANGE,


    /* PW-RED Forwarding logic constants */
    L2VPNRED_MODE_BRIDGE                    = 1,
    L2VPNRED_MODE_SWITCH                    = 2,
    L2VPNRED_MODE_ONEISTON                  = 3,
    L2VPNRED_MODE_DEFAULT                   = L2VPNRED_MODE_SWITCH,

    L2VPNRED_CONTENTION_INDEPENDENT         = 1,
    L2VPNRED_CONTENTION_MASTERSLAVE         = 2,
    L2VPNRED_CONTENTION_DEFAULT             = L2VPNRED_CONTENTION_INDEPENDENT,

    L2VPNRED_MASTERSLAVE_MASTER             = 1,
    L2VPNRED_MASTERSLAVE_SLAVE              = 2,
    L2VPNRED_MASTERSLAVE_DEFAULT            = L2VPNRED_MASTERSLAVE_SLAVE,

    L2VPNRED_MULTIHOMINGAPP_LAGG            = 0x80,
    L2VPNRED_MULTIHOMINGAPP_DEFAULT         = L2VPNRED_MULTIHOMINGAPP_LAGG,

    L2VPNRED_STATUS_FORWARDING_NEGOTIATON   = 0x01,
    L2VPNRED_STATUS_SWITCHOVER_NEGOTIATON   = 0x02,
    L2VPNRED_STATUS_NEGOTIATION_SUCCESS     = 0x04,
    L2VPNRED_STATUS_WAITING_TO_RESTORE      = 0x08,
    L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE  = 0x10,
    L2VPNRED_STATUS_DEFAULT                 = L2VPNRED_STATUS_REDUNDANCY_UNAVAILABLE,

    L2VPNRED_NODE_STATUS_DISCONNECTED       = 0x00,
    L2VPNRED_NODE_STATUS_CONNECTED          = 0x01,
    L2VPNRED_NODE_STATUS_LOCAL_SYNC         = 0x02,
    L2VPNRED_NODE_STATUS_CONFIG_AWAITED     = 0x04,
    L2VPNRED_NODE_STATUS_STATE_AWAITED      = 0x08,
    L2VPNRED_NODE_STATUS_REMOTE_SYNC        = 0x10,

    L2VPNRED_PW_STATUS_NONE                 = 0x0000,
    L2VPNRED_PW_STATUS_LOCAL_FORWARD        = 0x0001,
    L2VPNRED_PW_STATUS_LOCAL_STANDBY        = 0x0002,
    L2VPNRED_PW_STATUS_LOCAL_SWITCHOVER     = 0x0004,
    L2VPNRED_PW_STATUS_REMOTE_SWITCHOVER    = 0x0008,
    L2VPNRED_PW_STATUS_REMOTE_AWAITED       = 0x0010,
    L2VPNRED_PW_STATUS_REMOTE_CONFLICT      = 0x0020,
    L2VPNRED_PW_STATUS_NODE_SWITCHOVER      = 0x0040,
    L2VPNRED_PW_STATUS_LOCAL_UPDATED        = 0x0080,
    L2VPNRED_PW_STATUS_REMOTE_STANDBY       = 0x0100,
    L2VPNRED_PW_STATUS_REMOTE_FORWARD       = 0x0200,

    L2VPNRED_AC_STATUS_UP                   = 0x01,
    L2VPNRED_AC_STATUS_ACTIVE               = 0x02,
    L2VPNRED_AC_STATUS_STANDBY              = 0x04,
    L2VPNRED_AC_STATUS_DOWN                 = 0x08,

    L2VPNRED_PW_PREFERENCE_PRIMARY          = 1,
    L2VPNRED_PW_PREFERENCE_SECONDARY        = 2,
    L2VPNRED_PW_PREFERENCE_DEFAULT          = L2VPNRED_PW_PREFERENCE_SECONDARY,

    L2VPNRED_NEGOTIATION_RETRIES            = 4,

    L2VPN_PWVC_STATUS_AC_UP_MASK            = 0x06,
    L2VPN_PWVC_STATUS_PSN_UP_MASK           = 0x18,
    L2VPN_PWVC_STATUS_UP_MASK               = 0x1F,
    L2VPN_PWVC_STATUS_RED_MASK              = 0x60,
    L2VPN_PWVC_STATUS_STANDBY               = 0X20,
    L2VPN_PWVC_STATUS_SWITCHOVER_REQUEST    = 0X40,

    L2VPNRED_PW_PRIORITY_MIN                = 0xFFFF,
    L2VPNRED_PW_PRIORITY_SECONDARY_MASK     = 0xF800,
    L2VPNRED_PW_PRIORITY_MASK               = 0xF000,
    L2VPNRED_PW_PRIORITY_MAX                = 0x0000,
};


enum {

    L2VPN_RM_INIT = 1,           /* The L2VPN  instance is up */

    L2VPN_RM_ACTIVE_STANDBY_UP, /* The current L2VPN instance is up and
                                   functioning as active node and its peer
                                   is functioning as the standby */

    L2VPN_RM_ACTIVE_STANDBY_DOWN, /* The current L2VPN instance is up
                                     and functioning as active node but
                                     its peer is down */

    L2VPN_RM_STANDBY              /* The node is in standby state */
};

#define L2VPN_RM_BLKUPDT_NOT_STARTED    1 /* Bulk Update is not started*/
#define L2VPN_RM_BLKUPDT_INPROGRESS     2 /* Bulk Update is in progress*/
#define L2VPN_RM_BLKUPDT_COMPLETED      3 /* Bulk Update is not completed */
#define L2VPN_RM_BLKUPDT_ABORTED        4 /*Bulk Update is aborted */
#define L2VPN_ENABLED                   1
#define L2VPN_MAX_MSG_TO_LDP_COUNT       100

#endif /*_L2VPN_DEFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file L2vpdefs.h                             */
/*---------------------------------------------------------------------------*/
