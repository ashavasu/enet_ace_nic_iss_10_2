/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsVplsdb.h,v 1.1 2014/03/09 13:30:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVPLSDB_H
#define _FSVPLSDB_H

UINT1 VplsBgpConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 VplsBgpVETableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 VplsBgpPwBindTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsVpls [] ={1,3,6,1,4,1,29601,2,86};
tSNMP_OID_TYPE fsVplsOID = {9, fsVpls};


UINT4 VplsBgpConfigVERangeSize [ ] ={1,3,6,1,4,1,29601,2,86,1,1,1,1};
UINT4 VplsBgpVEId [ ] ={1,3,6,1,4,1,29601,2,86,1,2,1,1};
UINT4 VplsBgpVEName [ ] ={1,3,6,1,4,1,29601,2,86,1,2,1,2};
UINT4 VplsBgpVEPreference [ ] ={1,3,6,1,4,1,29601,2,86,1,2,1,3};
UINT4 VplsBgpVERowStatus [ ] ={1,3,6,1,4,1,29601,2,86,1,2,1,5};
UINT4 VplsBgpVEStorageType [ ] ={1,3,6,1,4,1,29601,2,86,1,2,1,6};
UINT4 VplsBgpPwBindLocalVEId [ ] ={1,3,6,1,4,1,29601,2,86,1,3,1,1};
UINT4 VplsBgpPwBindRemoteVEId [ ] ={1,3,6,1,4,1,29601,2,86,1,3,1,2};




tMbDbEntry fsVplsMibEntry[]= {

{{13,VplsBgpConfigVERangeSize}, GetNextIndexVplsBgpConfigTable, VplsBgpConfigVERangeSizeGet, VplsBgpConfigVERangeSizeSet, VplsBgpConfigVERangeSizeTest, VplsBgpConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, VplsBgpConfigTableINDEX, 1, 0, 0, "0"},

{{13,VplsBgpVEId}, GetNextIndexVplsBgpVETable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, VplsBgpVETableINDEX, 2, 0, 0, NULL},

{{13,VplsBgpVEName}, GetNextIndexVplsBgpVETable, VplsBgpVENameGet, VplsBgpVENameSet, VplsBgpVENameTest, VplsBgpVETableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VplsBgpVETableINDEX, 2, 0, 0, ""},

{{13,VplsBgpVEPreference}, GetNextIndexVplsBgpVETable, VplsBgpVEPreferenceGet, VplsBgpVEPreferenceSet, VplsBgpVEPreferenceTest, VplsBgpVETableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, VplsBgpVETableINDEX, 2, 0, 0, "0"},

{{13,VplsBgpVERowStatus}, GetNextIndexVplsBgpVETable, VplsBgpVERowStatusGet, VplsBgpVERowStatusSet, VplsBgpVERowStatusTest, VplsBgpVETableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpVETableINDEX, 2, 0, 1, NULL},

{{13,VplsBgpVEStorageType}, GetNextIndexVplsBgpVETable, VplsBgpVEStorageTypeGet, VplsBgpVEStorageTypeSet, VplsBgpVEStorageTypeTest, VplsBgpVETableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpVETableINDEX, 2, 0, 0, "2"},

{{13,VplsBgpPwBindLocalVEId}, GetNextIndexVplsBgpPwBindTable, VplsBgpPwBindLocalVEIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, VplsBgpPwBindTableINDEX, 2, 0, 0, NULL},

{{13,VplsBgpPwBindRemoteVEId}, GetNextIndexVplsBgpPwBindTable, VplsBgpPwBindRemoteVEIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, VplsBgpPwBindTableINDEX, 2, 0, 0, NULL},
};
tMibData fsVplsEntry = { 8, fsVplsMibEntry };

#endif /* _FSVPLSDB_H */

