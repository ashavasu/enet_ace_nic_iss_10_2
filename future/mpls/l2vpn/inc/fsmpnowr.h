/* $Id: fsmpnowr.h,v 1.2 2010/10/21 09:25:03 prabuc Exp $ */
#ifndef _FSMPNOWR_H
#define _FSMPNOWR_H

VOID RegisterFSMPNO(VOID);

VOID UnRegisterFSMPNO(VOID);
INT4 FsMplsPwStatusNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPwOAMStatusNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPwStatusNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPwOAMStatusNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPwStatusNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsPwOAMStatusNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsPwStatusNotifEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsPwOAMStatusNotifEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSMPNOWR_H */
