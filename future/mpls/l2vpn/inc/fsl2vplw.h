/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsl2vplw.h,v 1.5 2015/09/15 06:43:00 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsConfigIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for VplsConfigTable. */
INT1
nmhValidateIndexInstanceVplsConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsConfigTable  */

INT1
nmhGetFirstIndexVplsConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsConfigName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsConfigDescr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsConfigAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsConfigMacLearning ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsConfigDiscardUnknownDest ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsConfigMacAging ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsConfigFwdFullHighWatermark ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetVplsConfigFwdFullLowWatermark ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetVplsConfigRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsConfigMtu ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetVplsConfigVpnId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsConfigStorageType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsConfigSignalingType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsConfigName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVplsConfigDescr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVplsConfigAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsConfigMacLearning ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsConfigDiscardUnknownDest ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsConfigMacAging ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsConfigFwdFullHighWatermark ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetVplsConfigFwdFullLowWatermark ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetVplsConfigRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsConfigMtu ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetVplsConfigStorageType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsConfigSignalingType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsConfigName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VplsConfigDescr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VplsConfigAdminStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsConfigMacLearning ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsConfigDiscardUnknownDest ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsConfigMacAging ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsConfigFwdFullHighWatermark ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2VplsConfigFwdFullLowWatermark ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2VplsConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsConfigMtu ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2VplsConfigStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsConfigSignalingType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VplsStatusTable. */
INT1
nmhValidateIndexInstanceVplsStatusTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsStatusTable  */

INT1
nmhGetFirstIndexVplsStatusTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsStatusTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsStatusOperStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsStatusPeerCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for VplsPwBindTable. */
INT1
nmhValidateIndexInstanceVplsPwBindTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsPwBindTable  */

INT1
nmhGetFirstIndexVplsPwBindTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsPwBindTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsPwBindConfigType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetVplsPwBindType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetVplsPwBindRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetVplsPwBindStorageType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsPwBindConfigType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetVplsPwBindType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetVplsPwBindRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetVplsPwBindStorageType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsPwBindConfigType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2VplsPwBindType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2VplsPwBindRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2VplsPwBindStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsPwBindTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VplsBgpADConfigTable. */
INT1
nmhValidateIndexInstanceVplsBgpADConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsBgpADConfigTable  */

INT1
nmhGetFirstIndexVplsBgpADConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsBgpADConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsBgpADConfigRouteDistinguisher ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsBgpADConfigPrefix ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetVplsBgpADConfigVplsId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsBgpADConfigRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetVplsBgpADConfigStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsBgpADConfigRouteDistinguisher ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVplsBgpADConfigPrefix ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetVplsBgpADConfigVplsId ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVplsBgpADConfigRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetVplsBgpADConfigStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsBgpADConfigRouteDistinguisher ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VplsBgpADConfigPrefix ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2VplsBgpADConfigVplsId ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VplsBgpADConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2VplsBgpADConfigStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsBgpADConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VplsBgpRteTargetTable. */
INT1
nmhValidateIndexInstanceVplsBgpRteTargetTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VplsBgpRteTargetTable  */

INT1
nmhGetFirstIndexVplsBgpRteTargetTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVplsBgpRteTargetTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsBgpRteTargetRTType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetVplsBgpRteTargetRT ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVplsBgpRteTargetRTRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetVplsBgpRteTargetStorageType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsBgpRteTargetRTType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetVplsBgpRteTargetRT ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVplsBgpRteTargetRTRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetVplsBgpRteTargetStorageType ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsBgpRteTargetRTType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2VplsBgpRteTargetRT ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VplsBgpRteTargetRTRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2VplsBgpRteTargetStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsBgpRteTargetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVplsStatusNotifEnable ARG_LIST((INT4 *));

INT1
nmhGetVplsNotificationMaxRate ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVplsStatusNotifEnable ARG_LIST((INT4 ));

INT1
nmhSetVplsNotificationMaxRate ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VplsStatusNotifEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2VplsNotificationMaxRate ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VplsStatusNotifEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2VplsNotificationMaxRate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsL2VpnPwRedundancyStatus ARG_LIST((INT4 *));

INT1
nmhGetFsL2VpnPwRedNegotiationTimeOut ARG_LIST((UINT4 *));

INT1
nmhGetFsL2VpnPwRedundancySyncFailNotifyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsL2VpnPwRedundancyPwStatusNotifyEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsL2VpnPwRedundancyStatus ARG_LIST((INT4 ));

INT1
nmhSetFsL2VpnPwRedNegotiationTimeOut ARG_LIST((UINT4 ));

INT1
nmhSetFsL2VpnPwRedundancySyncFailNotifyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsL2VpnPwRedundancyPwStatusNotifyEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsL2VpnPwRedundancyStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedNegotiationTimeOut ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsL2VpnPwRedundancySyncFailNotifyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedundancyPwStatusNotifyEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsL2VpnPwRedundancyStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsL2VpnPwRedNegotiationTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsL2VpnPwRedundancySyncFailNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsL2VpnPwRedundancyPwStatusNotifyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsL2VpnPwRedGroupTable. */
INT1
nmhValidateIndexInstanceFsL2VpnPwRedGroupTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsL2VpnPwRedGroupTable  */

INT1
nmhGetFirstIndexFsL2VpnPwRedGroupTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsL2VpnPwRedGroupTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsL2VpnPwRedGroupProtType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupReversionType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupContentionResolutionMethod ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupLockoutProtection ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupMasterSlaveMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupDualHomeApps ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedGroupName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedGroupStatus ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedGroupOperActivePw ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsL2VpnPwRedGroupWtrTimer ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsL2VpnPwRedGroupAdminCmd ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupAdminActivePw ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsL2VpnPwRedGroupAdminCmdStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedGroupRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsL2VpnPwRedGroupProtType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedGroupReversionType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedGroupContentionResolutionMethod ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedGroupLockoutProtection ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedGroupMasterSlaveMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedGroupDualHomeApps ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsL2VpnPwRedGroupName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsL2VpnPwRedGroupWtrTimer ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsL2VpnPwRedGroupAdminCmd ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedGroupAdminActivePw ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsL2VpnPwRedGroupRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsL2VpnPwRedGroupProtType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupReversionType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupContentionResolutionMethod ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupLockoutProtection ARG_LIST((UINT4 *  ,UINT4  ,INT4     ));

INT1
nmhTestv2FsL2VpnPwRedGroupMasterSlaveMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupDualHomeApps ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsL2VpnPwRedGroupName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsL2VpnPwRedGroupWtrTimer ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupAdminCmd ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupAdminActivePw ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsL2VpnPwRedGroupRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsL2VpnPwRedGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsL2VpnPwRedNodeTable. */
INT1
nmhValidateIndexInstanceFsL2VpnPwRedNodeTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsL2VpnPwRedNodeTable  */

INT1
nmhGetFirstIndexFsL2VpnPwRedNodeTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsL2VpnPwRedNodeTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsL2VpnPwRedNodeLocalLdpID ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedNodeLocalLdpEntityIndex ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsL2VpnPwRedNodePeerLdpID ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedNodeStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedNodeRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsL2VpnPwRedNodeRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsL2VpnPwRedNodeRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsL2VpnPwRedNodeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsL2VpnPwRedPwTable. */
INT1
nmhValidateIndexInstanceFsL2VpnPwRedPwTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsL2VpnPwRedPwTable  */

INT1
nmhGetFirstIndexFsL2VpnPwRedPwTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsL2VpnPwRedPwTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsL2VpnPwRedPwPreferance ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedPwLocalStatus ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedPwRemoteStatus ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedPwOperStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsL2VpnPwRedPwRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsL2VpnPwRedPwPreferance ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsL2VpnPwRedPwRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsL2VpnPwRedPwPreferance ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedPwRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsL2VpnPwRedPwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsL2VpnPwRedIccpPwTable. */
INT1
nmhValidateIndexInstanceFsL2VpnPwRedIccpPwTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsL2VpnPwRedIccpPwTable  */

INT1
nmhGetFirstIndexFsL2VpnPwRedIccpPwTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsL2VpnPwRedIccpPwTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsL2VpnPwRedIccpPwRoId ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsL2VpnPwRedIccpPwPriority ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsL2VpnPwRedIccpPwStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsL2VpnPwRedSimulateFailure ARG_LIST((INT4 *));

INT1
nmhGetFsL2VpnPwRedSimulateFailureForNbr ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsL2VpnPwRedSimulateFailure ARG_LIST((INT4 ));

INT1
nmhSetFsL2VpnPwRedSimulateFailureForNbr ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsL2VpnPwRedSimulateFailure ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsL2VpnPwRedSimulateFailureForNbr ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsL2VpnPwRedSimulateFailure ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsL2VpnPwRedSimulateFailureForNbr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
