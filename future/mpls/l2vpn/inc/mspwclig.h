/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mspwclig.h,v 1.1 2010/11/17 14:03:10 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/
#ifndef _L2VPMSPW_MIB_CLIG_H
#define _L2VPMSPW_MIB_CLIG_H


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsMsPwMaxEntries[11];

extern UINT4 FsMsPwIndex1[13];

extern UINT4 FsMsPwIndex2[13];

extern UINT4 FsMsPwRowStatus[13];
#endif
