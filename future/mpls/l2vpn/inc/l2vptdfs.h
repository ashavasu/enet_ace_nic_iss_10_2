
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
 * $Id: l2vptdfs.h,v 1.70 2017/11/09 13:21:53 siva Exp $
*
*******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : l2vptdfs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used in the L2VPN
 *                             Module.
 *---------------------------------------------------------------------------*/


#ifndef _L2VPTDFS_H 
#define _L2VPTDFS_H

/*--------------- L2VPN Data structure defintions ---------------*/

/* Macros used for system sizing */

#define L2VPN_PW_VC_INFO \
        (L2VPN_DEF_PWVC_ENTRIES * sizeof(tPwVcInfo))

#define L2VPN_VPLS_INFO \
 (sizeof (tVPLSInfo) * MAX_L2VPN_VPLS_ENTRIES)

typedef long long       ULONG8;

typedef struct _L2VpnTimer
{
    tTmrAppTimer        AppTimer;
    UINT4               u4Event;
}
tL2vpnTimer;

typedef struct _l2vpnRMInfo
{
  UINT4 u4l2vpRmState;
  UINT4 u4PeerCount; 
  UINT4 u4DynBulkUpdatStatus;
   UINT4 u4L2vpnRmGoActiveReason;
  UINT1 u1BulkUpdModuleStatus;
  BOOL1 b1IsBulkReqRcvd;
  UINT1 u1AdminState;
  UINT1 u1Rsvd;
  
}tL2VpnRMInfo;

typedef struct _L2VpnGlobalInfo
{
   tOsixTaskId       TaskId;
   tOsixQId          QId;
   tOsixQId          ResQId;
   tOsixSemId        SemId;
   tTimerListId      TimerListId;
   tL2vpnTimer       PerfTimer;
   tL2vpnTimer       PendMsgToLdpTimer;
#ifdef VPLS_GR_WANTED
   tL2vpnTimer   BgpGrTimer;
#endif
   tL2VpnRMInfo      l2vpRmInfo;
   UINT4             u4TrcFlag;
   UINT4             u4TrcLvl;
   UINT4             u4MaxVplsEntries;
   INT4              i4AdminStatus;
   INT4              i4SysLogId;
#ifdef HVPLS_WANTED
   tL2vpnTimer       NotifRateTimer;
   INT4              i4VplsStatusNotifEnable;
   UINT4             u4VplsNotificationMaxRate;          
   UINT4             u4VplsNotifCounter; 
#endif
   
   UINT1             u1L2VpnInitialised;
   UINT1             u1LocalCcTypeCapabilities; /*CC Types configured in control plane*/
   UINT1             u1LocalCvTypeCapabilities; /*CV Types configured in control plane*/
   UINT1             u1LocalHwCcTypeCapabilities;/*CC Types supported by hardware*/
   
   BOOL1             b1PwStatusNotif;  /*Pw Status trap/notification*/
   BOOL1             b1PwOamStatusNotif; /*Pw Oam status trap/notification*/
   UINT1             u1PwRedStatus;
   BOOL1             b1IsSendToLdpTmrStarted;
#ifdef VPLS_GR_WANTED
   BOOL1    b1IsBgpGrTmrStarted;
#endif   
#ifdef HVPLS_WANTED
   BOOL1    b1IsNotifRateTmrStarted;
   UINT1    au1Padding[3];
#endif
   BOOL1             b1IsSendToLdpTmrExpHandled;
   UINT1             u1SendToLdpSpacingTime;
   UINT1             u1SendToLdpMsgSpacingCount;
   UINT1             u1AdminStatusProgress;
#ifdef VPLS_GR_WANTED
   UINT1    au1Pad[3];
#endif
}
tL2VpnGlobalInfo;

/*--------------- PW-VC Data structure defintions ---------------*/

typedef struct _PwVcPerfIntervalEntry
{
   FS_UINT8 InHCPkts;
   FS_UINT8 InHCBytes;
   FS_UINT8 OutHCPkts;
   FS_UINT8 OutHCBytes;

   INT4     i4IntTimeElapsed;
   INT1     i1ValidData;
   INT1     ai1Pad[3];
}
tPwVcPerfIntervalEntry;

typedef struct _PwVcPerfTotalEntry
{
   FS_UINT8 InHCPkts;
   FS_UINT8 InHCByts;
   FS_UINT8 OutHCPkts;
   FS_UINT8 OutHCByts;

   UINT4    u4MoniTime;    /*amt of time over which monitoring was done*/
   INT1     i1IsValidData;
   INT1     ai1Pad[3];
}
tPwVcPerfTotalEntry;


#ifdef HVPLS_WANTED
typedef struct _PwVcErpsMegInfo 
{
 UINT4   u4ErpsMegIndex;
 UINT4   u4ErpsMeIndex;
 UINT4   u4ErpsMepIndex;
 UINT2               u2LastPwStatusSend;
 INT1               ai1Pad[2];
}tPwVcErpsMegInfo;
#endif

typedef struct _PwVcEntry
{
    /* SsnNode should be fist element for
     * TMO_DLL access */ 
    tTMO_DLL_NODE    SsnNode;
    tTMO_DLL_NODE    PwInNode;
    tTMO_DLL_NODE    PwOutNode;
    tTMO_SLL_NODE    VplsNode;
    tTMO_DLL_NODE    PendMsgToLdpNode;
    tRBNodeEmbd      InVcLblRbNode;
    tRBNodeEmbd      PwIdRbNode; /* Embedded RB Tree node for PWID 
                                    based PW search */
    tRBNodeEmbd      OutVcLblRbNode; /* Embedded RB Tree node for OutLabel based
                                        PW Search */
#ifdef HVPLS_WANTED            
    /*VPLS - RES in ring specific information*/
    tPwVcErpsMegInfo     ErpsMegInfo;          
#endif           
    uGenAddr         PeerAddr;

    VOID             *pPSNEntry;
    VOID             *pServSpecEntry; /* Pointer to AC Entry.
                                         AC can be of type Enet, ATM,
                                         TDM etc.., */

    UINT4             u4PwVcIndex;

    UINT4             u4PwVcID; /* Working path PW when 
                                   the protection is desired */ 

    UINT4             u4BkpPwVcID; /* If this record is for working path PW, 
                                      this value indicates the VC ID of 
                                      protection path PW.

                                      If this record is for protection path PW,
                                      this value indicates the VC ID of working
                                      path PW. */

    UINT4             u4LocalGroupID;
    UINT4             u4RemoteGroupID;

    UINT4             u4AttachedPwIndex;
    UINT4             u4PwIfIndex;
    UINT4             u4FragmentCfgSize;
    UINT4             u4LastChange;

    UINT4             u4OutVcLabel;
    UINT4             u4InVcLabel;
    UINT4             u4PrevInVcLabel;
    UINT4             u4OutVcLabelReqId;
    UINT4             u4InVcLabelReqId;
    /* This label is meaningful when L2VPN uses FRR N:1 backup tunnel, 
     * the ILM programming with MP label prior to link/node failure helps fast 
     * switching the VPN traffic via backup */
    UINT4             u4InOuterMPLabel1; 
    /* This label is meaningful when L2VPN uses FRR N:1 backup tunnel, 
     * the ILM programming with MP label prior to link/node failure helps fast 
     * switching the VPN traffic via backup */
    UINT4             u4InOuterMPLabel2;
    /* This label is useful when we use FRR 1:1 or N:1
     * => 1:1 backup tnl in label to handle link/node failure
     * => N:1 protected tnl in label to handle link/node failure
     * */
    UINT4             u4InMPLabel1; 
    /* This label is useful when we use FRR 1:1 or N:1
     * => 1:1 backup tnl in label to handle link/node failure
     * => N:1 protected tnl in label to handle link/node failure
     * */
    UINT4             u4InMPLabel2; 

    UINT4             u4InOuterLabel; /* Incoming PSN(Outer) Label */
    UINT4             u4OutOuterLabel; /* Outgoing PSN(Outer) Label */
    /* LDP over RSVP case incoming label */ 
    UINT4             u4InOuterLdpLabel; 
    UINT4             u4MPInIfIndex;

    /* BFD session index */
    UINT4             u4ProactiveSessionIndex; /* Proactive Session index */
    /* MPLS-TP OAM specific information */
    UINT4             u4MegIndex; /* Maintenance Entity Group index */
    UINT4             u4MeIndex; /* Maintenance Entity index */
    UINT4             u4MpIndex; /* Maintenance Point index */

    UINT4             u4LspId;
    UINT4             u4InIfIndex;
    UINT4             u4PwL3Intf;

    UINT4             u4CreateTime;
    UINT4             u4UpTime;
    INT4              i4TimeElapsed;
    UINT4             u4VplsInstance;
    UINT4             u4PwType;         /*MPLS or MPLS-TP Pseudowire */
    uGenU4Addr        GenNonTeTnlSrcAddr;

    UINT4             u4GenAgiType;     /*Attachment Group Identifier Type*/
    UINT4             u4LocalAiiType;   /*Local Attachment Individual Identifier Type*/
    UINT4             u4RemoteAiiType;  /*Remote Attachment Individual Identifier Type*/

    UINT4             u4VplsBgpPwBindLocalVEId;
    UINT4             u4VplsBgpPwBindRemoteVEId;

    INT4              i4NPop; 

    INT4              i4PeerAddrType;  /*Type of Peer Address - ipv4(1) for MPLS 
                                         unknown(0) for MPLS-TP*/
    UINT1             u1LocalCcSelected; /*Locally Selected Control Channel type*/
    UINT1             u1LocalCvSelected; /*Locally selected Connectivity verification type*/
    UINT1             u1LocalCapabAdvert;/*Status Indication/pwVCCV*/
    UINT1             u1LocalCcAdvert;   /*Local CC Type configured for peer*/

    UINT1             u1LocalCvAdvert;   /*Local CV Type configured for peer*/
    UINT1             u1RemoteCcAdvert;  /*Remote CC Type Configured/Signalled */
    UINT1             u1RemoteCvAdvert;  /*Remote CV Type Configured/Signalled*/
    UINT1             u1ProtStatus; /* Protection available/
                                       Protection not avialable/
                                       Protection not in use 
                                       Protection Not Applicable*/ 


    UINT1             au1RemoteIfString[L2VPN_PWVC_MAX_IF_STR_LEN];
    UINT1             au1PwVcName[L2VPN_PWVC_MAX_NAME_LEN];
    UINT1             au1PwVcDescr[L2VPN_PWVC_MAX_DSCR_LEN];
    UINT1             au1Agi[L2VPN_PWVC_MAX_AI_LEN]; /* Group Attachment ID */
    UINT1             au1Saii[L2VPN_PWVC_MAX_AI_LEN]; /* Source Attachment Individual ID */
    UINT1             au1Taii[L2VPN_PWVC_MAX_AI_LEN]; /* Target Attachment Individual ID */

    UINT2             u2LocalIfMtu;
    UINT2             u2RemoteIfMtu;
    UINT2             u2NextFreeEnetInstance; /* Next Free Enet PW Instance */
    UINT2             u2SeqNumber;      /* For Pw ethernet control word */

    UINT1             u1MapStatus;
    UINT1             u1LocalStatus; /* PW Local Status */
    UINT1             u1RemoteStatus; /* PW Remote Status */
    UINT1             u1RemoteCapabilities;

    UINT1             u1RmtFragCapability;
    UINT1             u1FcsRetentionStatus;
    UINT1             u1AgiLen;
    UINT1             u1SaiiLen;

    UINT1             u1TaiiLen;
    UINT1             u1PwVcMode; /* VPWS (1)-default, VPLS (2), IPLS (3) */
    INT1              i1ValidIntervals;
    /* ControlWord is a Boolean value */
    INT1              i1ControlWord;
    /* LocalIfString is a Boolean value */

    INT1              i1LocalIfString;
    INT1              i1PwVcType;
    INT1              i1PwVcOwner;
    INT1              i1PwVcPsnType;

    INT1              i1SetUpPriority;
    INT1              i1HoldingPriority;
    INT1              i1AdminStatus;
    INT1              i1OperStatus; /* Overall operational status of the PW
                                       1. Control plane or management plane 
                                       operational status 
                                       2. OAM operational status if desired   
                                     */
    INT1              i1CwStatus;
    INT1              i1FcsRetentionCfg;
    INT1              i1RemoteStatusCapable;
    INT1              i1RowStatus;

    INT1              i1StorageType;
   
    /* Pseudowire Hardware Status.
     * For VPWS:
     * --------
     * First two bits of the first nibble indicates the current status of the
     * Pseudowire in the Hardware.
     * First two bits of the next nibble indicates the actual status passed to the
     * Hardware.
     * For VPLS:
     * --------
     * Holds TRUE or FALSE
     */
    /* VPWS:
     *           A   B           C   D
     *           |   |           |   |
     *           v   V           v   V
     * ---------------------------------
     * |   |   |   |   |   |   |   |   |   
     * ---------------------------------
     * If A is set, it means that Pseudowire is in H/W.
     * If B is set, it means that AC is in H/W.
     * If C is set, it means that PW programming (Add/Del) to be done in H/W.
     * If D is set, it means that AC programming (Add/Del) to be done in H/W.
     * 
     * By comparing AB and CD, i.e, current status in the H/W and the 
     * programming (Add/Del) requested to the H/w, programming should be done.
     * NPAPI should take care of this.
     */
    UINT1             u1HwStatus; 
    UINT1             u1ArpResolveStatus;
    UINT1             u1PwRouteStatus;


    UINT1             au1NextHopMac[MAC_ADDR_LEN]; /* Nexthop mac address 
                                                      applicable for pwOnly
                                                      case */
    UINT1             u1IsSigNonTeLsp; 
    UINT1             u1PsnTnlStatus;  /* Indicates the operational status of
                                          Ingress and Egress Tunnel cumulatively.
                                          If corresponding bit is set it indicates
                                          the associated tunnel is oper up.
                                          L2VPN_PSN_TNL_IN   - 0x01
                                          L2VPN_PSN_TNL_OUT  - 0x02
                                          L2VPN_PSN_TNL_BOTH - 0x03
                                        */

    BOOL1             bOamEnable;       /* External Oam status(Enable/Disable) 
                                           - OAM enabled/disabled upon proactive
                                           session index update by 
                                           external module */
    UINT1             u1OamOperStatus; /* OAM operational status */

    UINT1             u1CPOrMgmtOperStatus; /* Control plane or managment 
                                               operational status */

    BOOL1             bIsStaticPw; /* PW becomes static configurated if 
                                      the local label and remote labels are associated */


    BOOL1             bPwIntOamEnable;  /* Internal Oam status(Enable/Disable), 
                                           this variable determies whether to accept 
                                           the external OAM update or not */
    UINT1             u1PrevLocalStatus; /* PW Previous Local Status */
    UINT1             u1PwPathType;   /* This variable indicates whether the PW
                                         is working PW or protection PW. */
    UINT1             u1MplsOamAppType;  /* Specifies the type of OAM - Y.1731/BFD */ 
    UINT1             u1TnlType;         /* Underlying Tunnel type */
    UINT1             u1AiiFormat;

    UINT1             u1GrSyncStatus;  /* Maps to fsMplsL2VpnPwSynchronizationStatus
                                          of fsmpls MIB */

    UINT1             u1AppOwner;       /* Specifies the applications running
                                           over this PW.*/
    UINT1             u1ElpsMode;       /* 1+1 or 1:1 */
    BOOL1             b1IsStaticLabel; /*Static Pw label assignment */
    UINT1             u1SendToLdpMsgType;
     BOOL1             b1IsIlmEntryPresent;
    UINT2              u2ReqVlanId;
    UINT1              u1MplsPmAppType; /* Specifies the type of RFC6374 */
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    UINT1             u1NpApiCall; /* TRUE/FALSE which will decide for NPAPI call*/
#else  
    UINT1             au1Pad[1];  
#endif
}
tPwVcEntry;


/*------------- VPLS Data Structure Defintions -------------*/

#ifdef VPLSADS_WANTED
typedef struct _VPLSLBInfo
{
    UINT4       u4LabelBlock;
    UINT4       u4ReferenceCount;
}tVPLSLBInfo;
#endif

typedef struct _VPLSEntry
{
    tRBNodeEmbd NextEntry;                       /* Embedded RBTree next node*/ 
    tTMO_SLL    PwList;                  
    UINT4       u4VplsInstance;                     /* VPLS Instance */
    UINT4       u4VplsFDBHighThreshold;             /* High FDB Threshold */
    UINT4       u4VplsFDBLowThreshold;              /* Low FDB Threshold */
    UINT4       u4VplsStatusOperStatus;
    UINT4       u4VplsStatusPeerCount;
    UINT4       u4VplsBgpConfigVERangeSize;
    UINT4       u4Mtu;
    UINT4       u4SignallingType;
    UINT4       u4VplsFDBCounter;
    INT4        i4VplsVSI;                          /* Bridge Identifier */  
#ifdef VPLSADS_WANTED
    tVPLSLBInfo *pVPLSLBInfoBase;
#endif
    UINT2       u2VplsFdbId; /* For mapping VplsInstance to a L2 FDB Id */
    UINT1       au1VplsName[L2VPN_MAX_VPLS_NAME_LEN + 4];   /* VPLS Name  */
    UINT1       au1VplsDescr[L2VPN_MAX_VPLS_DESCR_LEN + 4]; /* VPLS Descriptor */
    UINT1       au1VplsVpnID[L2VPN_MAX_VPLS_VPNID_LEN + 4]; /* VPN ID */
    INT1        i1RowStatus;                        /* Row Status */
    /* au1VplsVpnID and i1RowStatus together satisfies the padding. Hence 
     * u2VplsFdbId comes after the INT1 variable i1RowStatus
     */
    /* VPLS FDB */
    UINT1       u1StorageType;
    BOOL1       bControlWordFlag;    
#ifdef VPLSADS_WANTED
    UINT1       au1DefaultRD[L2VPN_MAX_VPLS_RD_LEN];  
    UINT1       au1PreviousDefaultRD[L2VPN_MAX_VPLS_RD_LEN];  
    UINT1       au1DefaultRT[L2VPN_MAX_VPLS_RT_LEN];
    UINT1       au1PreviousDefaultRT[L2VPN_MAX_VPLS_RT_LEN];
#endif
} tVPLSEntry;

typedef struct _VPLSInfo
{
    tVPLSEntry       *pVplsEntry;
}
tVPLSInfo;

/* This structure is added for system sizing.
 * It should not be used for any other purpose.
 */

typedef struct _VPLSInfoSize
{
    UINT1       au1VPLSInfo[L2VPN_VPLS_INFO];
}
tVPLSInfoSize;

#ifdef VPLSADS_WANTED
typedef struct _VPLSRdEntry
{
    tRBNodeEmbd NextEntry;                       /* Embedded RBTree next node*/
    UINT4       u4VplsInstance;                                              
    UINT4       u4VplsBgpADConfigPrefix;                                      
    UINT1       au1VplsBgpADConfigRouteDistinguisher[L2VPN_MAX_VPLS_RD_LEN];  
    UINT1       au1VplsBgpADConfigVplsId[L2VPN_MAX_VPLS_RD_LEN]; 
    INT1        i1VplsBgpADConfigRowStatus;                                   
    UINT1       u1VplsBgpADConfigStorageType;                                
    UINT1       au1Reserved[2]; /* For Padding */ 
} tVPLSRdEntry;

typedef struct _VPLSRtEntry
{
    tRBNodeEmbd NextEntry;                       /* Embedded RBTree next node*/
    UINT4       u4VplsInstance;
    UINT4       u4VplsBgpRteTargetIndex;
    UINT1       au1VplsBgpRteTargetRT[L2VPN_MAX_VPLS_RT_LEN];
    UINT1       u1VplsBgpRteTargetRTType;
    INT1        i1VplsBgpRteTargetRowStatus;
    UINT1       u1VplsBgpRteTargetStorageType;
    UINT1       au1Reserved[1]; /* For Padding */ 
} tVPLSRtEntry;

typedef struct _VPLSVeEntry
{
    tRBNodeEmbd NextEntry;                       /* Embedded RBTree next node*/
    UINT4       u4VplsInstance;
    UINT4       u4VplsBgpVEId;
    UINT4       u4VplsBgpVEPreference;
    UINT1       au1VplsBgpVEName[L2VPN_MAX_VPLS_NAME_LEN + 4];
    INT1        i1VplsBgpVERowStatus;
    UINT1       u1VplsBgpVEStorageType;
    UINT1       au1Reserved[2]; /* For Padding */ 
} tVPLSVeEntry;

/*Structure to store AC-VPLS mapping in case of Autodiscovered VPLS*/
typedef struct _VplsAcMapEntry     
{
 tRBNodeEmbd NextEntry;      /* Embedded RBTree next node*/
 UINT4       u4VplsInstance;
 UINT4   u4AcIndex;
 UINT4  u4AcPortIndex;
 UINT4  u4AcVlanId;
 UINT4  u4AcOperStatus;
 INT1  i1VplsAcMapRowStatus; 
    UINT1       au1Reserved[3]; /* For Padding */ 
}tVplsAcMapEntry;
#endif
#ifdef HVPLS_WANTED
typedef struct _VplsPwBindEntry
{
        tRBNodeEmbd NextEntry;                    
        UINT4       u4VplsInstance;
        UINT4           u4PwIndex;
        UINT4           u4VplsPwBindConfigType;
        UINT4           u4VplsPwBindType;
        INT1            i1VplsPwBindRowStatus;
        UINT1       au1Reserved[3]; /* For Padding */
}tVplsPwBindEntry;
#endif

/* This structure is added for system sizing.
 * It should not be used for any other purpose.
 */

typedef struct _PwVcMaxIfStrLenSize
{
   UINT1        au1PwVcMaxIfStrLen[L2VPN_PWVC_MAX_IF_STR_LEN];
}
tPwVcMaxIfStrLenSize;

/* This structure is added for system sizing.
 * It should not be used for any other purpose.
 */

typedef struct _L2vpnDefMTUSize
{
   UINT1        au1L2vpnDefMTUSize[L2VPN_DEF_MTU];
}
tL2vpnDefMTUSize;

typedef struct _PwVcPerfStatsEntry
{
   tPwVcPerfIntervalEntry  aPwVcPerfIntervalInfo[L2VPN_PWVC_MAX_PERF_INTV];
   tPwVcPerfTotalEntry     aPwVcPerf1DayIntervalInfo[L2VPN_PWVC_MAX_PERF_DAYS];
   tPwVcEntry             *pPwVcEntry;
   UINT1                   u1PwVcPerfDayIndex;
   INT1                    ai1Pad[3];
}
tPwVcPerfStatsEntry;

typedef struct _PwVcCfgParams
{
   UINT4             u4MaxPwVcEntries;
   UINT4             u4MaxCleanupInterval;
   UINT4             u4MaxDormantPwVcEntries;
   UINT4             u4MaxWaitingPeerPwVcEntries;
   UINT4             u4MaxPeerSessionEntries;
}
tPwVcCfgParams;

typedef struct _PwVcGlobalStats
{
   UINT4             u4ActivePwVcEntries;
   UINT4             u4ActivePwVcEntriesPerPSN[L2VPN_MAX_PSN_TYPES];
   UINT4             u4ActivePwVcEntriesPerVCType[L2VPN_MAX_VC_TYPES];
   UINT4             u4NoOfPwVcEntriesCreated;
   UINT4             u4NoOfPwVcEntriesDestroyed;
#ifdef VPLS_GR_WANTED
   UINT4             u4NoOfBgpPwStaleEntries;
#endif
}
tPwVcGlobalStats;


typedef tPwVcPerfStatsEntry tPwVcInfo; 

/* This Structure is added for system sizing.
 * It should not be used for any other purpose. */

typedef struct _PwVcInfoSize
{
   UINT1      au1PwVcInfo[L2VPN_PW_VC_INFO];
}
tPwVcInfoSize;


typedef struct _PwVcActivePeerSsnEntry
{
   tTMO_DLL          LblMsgList;
   tTMO_DLL          PwVcList;
   tTMO_DLL          IccpNodeList;

   uGenAddr          PeerAddr;
   UINT4             u4LocalLdpEntityID;
   UINT4             u4LdpEntityIndex;
   UINT4             u4PeerLdpId;

   UINT4             u4Status;
   UINT4             u4NoOfStalePw;
   /* Variables Added for GR Purpose - Starts */
   /* Recovery Timer started upon Queue Full */
   tL2vpnTimer       RecoveryTimer;
   /* Recovery Time as conveyed by LDP */
   UINT2             u2RecoveryTime;
   /* Gr Progress Status */
   UINT1             u1RecoveryProgress;
   /* Ends */
   INT1              i1SsnCreateReq;
   UINT1             u1PeerSsnLdpRegStatus;
   BOOL1             b1IsActive;
   UINT1             u1AddrType;
   UINT1             u1Pad;
}
tPwVcActivePeerSsnEntry;

typedef struct _PwVcLblMsgEntry
{
   tTMO_DLL_NODE     NextNode;
   tL2vpnTimer       CleanupTimer; 
   tPwVcLblMsgInfo   LblInfo;
}
tPwVcLblMsgEntry;


/* maintained in the order of decreasing PW VC priority */
typedef struct _PwVcDormantVcEntry
{
   tTMO_SLL_NODE     NextNode;
   tPwVcEntry       *pPwVcEntry;
}
tPwVcDormantVcEntry;

typedef struct _PwVcGlobalInfo
{
   tTMO_SLL          DormantVcsList;
   tTMO_DLL          PendMsgToLdpList;
   tRBTree           PwIdTable;
   tRBTree           PwIfIndexTable;
   tRBTree           PwVcOutLabelRBTree;
   tPwVcCfgParams    PwVcCfgParams;
   tPwVcGlobalStats  PwVcGlobalStats;
   tTMO_HASH_TABLE   *pPwIfHashPtr;
   struct rbtree *   pPwVcFec128RBTree;
   struct rbtree *   pPwVcFec129RBTree;
   struct rbtree *   pPwVcFec129VplsRBTree;
   struct rbtree *   pPwVcPeerAddrRBTree;
   struct rbtree *   pVplsVFIRBTree;
#ifdef HVPLS_WANTED
   struct rbtree *   pVplsPwBindRBTree;
#endif
#ifdef VPLSADS_WANTED
   struct rbtree *   pVplsRDRBTree;
   struct rbtree *   pVplsRTRBTree;
   struct rbtree *   pVplsVERBTree;
   struct rbtree *   pVplsACRBTree;
#endif
   struct rbtree *   pPwVcInLabelRBTree;
   tPwVcInfo         **ppPwVcInfoTable;
   tVPLSInfo         **ppVPLSTable;
   UINT4             u4PwVcPerfTotalErrorPkts;    /* Counter */
   UINT4             u4PwVcNextFreeIndex;
   INT1              i1PwVcPerfCurInterval;
   UINT1             au1Pad[3];
}
tPwVcGlobalInfo;


/*------------- PW-VC-MPLS Data structure defintions -------------*/

typedef struct _PwVcMplsNonTeTnlIndex
{
   UINT4             u4LsrXcIndex;
   UINT4             u4EntityId;
} tPwVcMplsNonTeTnlIndex;


typedef struct _PwVcMplsVcIndex
{
   UINT4              u4IfIndex;
} tPwVcMplsVcIndex;


typedef struct _PwVcMplsTnlEntry
{
   tPwVcEntry       *pPwVcEntry;

   union {
      tPwVcMplsTeTnlIndex    TeInfo;
      tPwVcMplsNonTeTnlIndex NonTeInfo;
      tPwVcMplsVcIndex       VcInfo;
   }unTnlInfo;

   UINT1             u1Direction;
   UINT1             u1TnlType;
   UINT1             au1Pad[2];
}
tPwVcMplsTnlEntry;

typedef struct _PwVcMplsInTnlEntry
{
   UINT4             u4LsrXcIndex;

   union {
       tPwVcMplsTeTnlIndex    TeInfo; /* Contains union of Incoming Tunnel 
                                         Information. */
       /* In Future we may need to have Non-Te Information and 
        * VcOnly Information. */
   }unInTnlInfo;
}
tPwVcMplsInTnlEntry;

typedef struct _PwVcMplsEntry
{
/* The List of Outbound and Inbound entires are maintained
 * in sorted order
 */ 
   tPwVcMplsTnlEntry PwVcMplsOutTnl;
   tPwVcMplsInTnlEntry PwVcMplsInTnl;

   tLdpId            LocalLdpID;
   tLdpId            PeerLdpID;
   UINT4             u4LocalLdpEntityID;
   UINT4             u4LocalLdpEntityIndex;
   
   UINT1             u1MplsType;
   UINT1             u1Ttl;
   
   INT1              i1ExpModeAndBits;
   INT1              i1StorageType;
}
tPwVcMplsEntry;

typedef struct _L2VpnMplsPwVcNonTeEvtInfo
{
    uGenAddr        PeerOrSrcAddr; 
    UINT1           u1AddrType;
    UINT1           au1pad[3];
}
tL2VpnMplsPwVcNonTeEvtInfo; /* To be made visible to other modules */

typedef struct _PwVcMplsMappingEntry 
{
  INT4     i4TnlType;
  union {
    tPwVcMplsTeTnlIndex    TeInfo;
    tL2VpnMplsPwVcNonTeEvtInfo NonTeInfo;
    tPwVcMplsVcIndex       VcInfo;
  }unTnlInfo;
  tTMO_DLL PwOutList;
}tPwVcMplsMappingEntry;

typedef struct _PwVcMplsInTnlMappingEntry
{
    union {
        tPwVcMplsTeTnlIndex    TeInfo; /* Contains mapping information about
                                          Incoming Tunnel */
        tL2VpnMplsPwVcNonTeEvtInfo NonTeInfo; /* Contains mapping information
                                                 about Incoming LSP. */
     /* In Future we may need to have Non-Te Information and
      * VcOnly Information. */
    }unInTnlInfo;
    tTMO_DLL PwInList;
}tPwVcMplsInMappingEntry;

typedef struct _PwVcMplsCfgParams
{
   UINT4             u4MaxMplsEntries;
   UINT4             u4MaxMplsTnlEntries;
   UINT4             u4MaxMplsMapEntries;
   UINT4             u4MaxMplsInOutEntries;
}
tPwVcMplsCfgParams;

typedef struct _PwVcMplsGlobalInfo
{
   tPwVcMplsCfgParams    PwVcMplsCfgParams;

/* Both Non Te Mapping Table List and Te Mapping Table List are
 * maintained in sorted order as required by the respective tables
 * in PW-MPLS-MIB */
  struct rbtree *pPwTeMapRBTreeList;
  struct rbtree *pPwTeInMapRBTreeList;
  struct rbtree *pPwNonTeMapRBTreeList;
  struct rbtree *pPwNonTeInMapRBTreeList;
  struct rbtree *pPwVcOnlyMapRBTreeList;
   UINT4             u4OutNextFreeIndex;
   UINT4             u4InNextFreeIndex;
}
tPwVcMplsGlobalInfo;


/*------------- PW-VC-ENET Data structure defintions -------------*/

typedef struct _PwVcEnetEntry
{
/* tTMO_SLL_NODE is used to link PwVcEnet Entries belonging to the
 * same PW VC (i.e., with the same PwVcIndex */
   tTMO_SLL_NODE     NextEnetEntry;

   INT4              i4PortIfIndex;
   INT4              i4VcIfIndex;
   UINT4             u4Instance;
   UINT4             u4PwAcId; /*Unique identifier for Enet entry*/

   UINT2             u2PwVlan;
   UINT2             u2PortVlan;
   INT1              i1VlanMode;

   INT1              i1RowStatus;
   INT1              i1StorageType;
   UINT1             u1HwStatus;
   tTMO_HASH_NODE    EnetNode;
   tPwVcEntry        *pPwVcEntry;
   UINT1             u1EnetMode; /*Ethernet mode/Vlan Mode*/
   UINT1             au1Pad[3];
}
tPwVcEnetEntry;

typedef struct _PwVcEnetMplsPriMappingEntry
{
   UINT2             u2PriMapping;
   INT1              i1PriMappingRowStatus;
   INT1              i1PriMappingStorageType;
}
tPwVcEnetMplsPriMappingEntry;

typedef struct _PwVcEnetServSpecEntry
{
/* The List of Pw Vc Enet Entries are maintained in
 * sorted order */
   tTMO_SLL          EnetEntryList;
   tPwVcEnetMplsPriMappingEntry    *pMplsPriMappingEntry;
}
tPwVcEnetServSpecEntry;

typedef struct _PwVcEnetCfgParams
{
   UINT4             u4MaxServSpecEntries;
   UINT4             u4MaxEnetEntriesPerPw; 
   UINT4             u4MaxEnetMplsPriMapEntries; 
}
tPwVcEnetCfgParams;

typedef struct _PwVcEnetGlobalInfo
{
   tPwVcEnetCfgParams    PwVcEnetCfgParams;
   tRBTree               MplsPortEntryRBTree;
}
tPwVcEnetGlobalInfo;


typedef struct _PwVcSSGlobalInfo
{
 tPwVcEnetGlobalInfo  EnetInfo;
 /* Add other serv spec globals here */
}
tPwVcSSGlobalInfo;

/* MS-PW */

/*structure for all scalars in the mib*/
typedef struct
{
 UINT4     u4FsMsPwMaxEntries;
 tRBTree   FsMsPwConfigTable;
} tL2vpnMsPwGlbMib;

typedef struct L2VPN_GLOBALS {
 tL2vpnMsPwGlbMib    L2vpnMsPwGlbMib;
 tMemPoolId          L2vpnFsMsPwConfigTablePoolId;
 UINT4               u4NoOfMsPwEntries;/*Configured MSPW entries */
    BOOL1               b1SpeRedStatusForwardEnable;
    UINT1               au1Pad[3];
} tL2vpnMsPwGlobals;

/*MS-PW */

/*Structure has mapping for PSW interface index with MPLS PSW index */
typedef struct {
 tRBNodeEmbd      PwIfIndexRbNode; 
 tPwVcEntry       *pPwVcEntry; 
}tPwIfIndexMap;

typedef struct {
    tRBNodeEmbd    MplsPortRbNode;
    UINT4          u4IfIndex;
    UINT1          u1MultiplexStatus;
    UINT1          u1BundleStatus;
    UINT1          u1AllToOneBundleStatus;
    INT1           i1RowStatus;
} tMplsPortEntryInfo;

/**
 * PW Redundancy Group
 */
typedef struct _L2vpnRedundancyEntry
{
    tRBNodeEmbd         RbNode;
    tTmrAppTimer        NegTimer;
    tL2vpnIccpPwEntry  *pOperActivePw;
    tL2vpnIccpPwEntry  *pNegForwardingPw;
    tL2vpnIccpPwEntry  *pNegSwitchoverPw;
    UINT1               au1RgGrpName[L2VPN_PWRED_GRP_NAME_LEN];
    UINT1               u1Mode;
    UINT1               u1ContentionResolutionMethod;
    UINT1               u1MasterSlaveMode;
    UINT4               u4Index;
    UINT4               u4AdminActivePw;
    UINT4               u4MessageId;
    UINT4               u4PwGroupId;
    UINT2               u2NumNode;
    UINT2               u2NumPw;
    UINT2               u2NumIccpPw;
    UINT2               u2NumUpPw;
    UINT2               u2NumPwAwaiting;
    UINT2               u2NegPriority;
    UINT2               u2WaitToRestoreTime;
    UINT2               u2HoldOffTime;
    UINT1               u1MultiHomingApps;
    UINT1               u1Status;
    UINT1               u1RowStatus;
    UINT1               u1LockoutProtect;
    BOOL1               b1ReversionEnable;
    BOOL1               b1ReElectionNotReqd;
    UINT1               au1Pad[2];
}tL2vpnRedundancyEntry;

/**
 * PW Redundancy Node
 */
typedef struct _L2vpnRedundancyNodeEntry
{
    tRBNodeEmbd         RbNode;
    tTMO_DLL_NODE       SessionNode;
    tPwVcActivePeerSsnEntry    *pSession;
    uGenAddr            Addr;
    UINT4               u4RgIndex;
    UINT2               u2NumIccpPw;
    UINT1               u1AddrType;
    UINT1               u1RowStatus;
    UINT1               u1Status;
    UINT1               au1Pad [3];
}tL2vpnRedundancyNodeEntry;

/**
 * PW Redundancy Group PW
 */
typedef struct _L2vpnRedundancyPwEntry
{
    tRBNodeEmbd         RbNode;
    tPwVcEntry         *pPwVcEntry;
    tL2vpnIccpPwEntry  *pIccpPwEntry;
    UINT4               u4RgIndex;
    UINT4               u4PwIndex;
    UINT4               u4PwVcId;
    UINT1               u1Preference;
    UINT1               u1RowStatus;
    UINT1               u1AcStatus;
    UINT1               u1Pad;
}tL2vpnRedundancyPwEntry;


/**
 * PW Redundancy Globals
 */
typedef struct _L2vpnRedundancyGlobals
{
    tRBTree             RgList;
    tRBTree             RgNodeList;
    tRBTree             RgPwList;
    tRBTree             RgIccpPwList;
    tTimerListId        NegTimerList;
    UINT4               u4NegotiationTimeout;
    BOOL1               b1MultiHomingEnable;
    BOOL1               b1SyncFailNotifEnable;
    BOOL1               b1PwStatusNotifEnable;
    UINT1               u1Pad;
} tL2vpnRedundancyGlobals;

/*      Pw Redundancy       */


#endif /*_L2VPTDFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file l2vptdfs.h                             */
/*---------------------------------------------------------------------------*/
