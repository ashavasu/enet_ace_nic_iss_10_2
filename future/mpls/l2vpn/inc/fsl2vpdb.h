/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsl2vpdb.h,v 1.5 2015/09/15 06:43:00 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSL2VPDB_H
#define _FSL2VPDB_H

UINT1 VplsConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 VplsStatusTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 VplsPwBindTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 VplsBgpADConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 VplsBgpRteTargetTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsL2VpnPwRedGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsL2VpnPwRedNodeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsL2VpnPwRedPwTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsL2VpnPwRedIccpPwTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,8 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsl2vp [] ={1,3,6,1,4,1,29601,2,72};
tSNMP_OID_TYPE fsl2vpOID = {9, fsl2vp};


UINT4 VplsConfigIndexNext [ ] ={1,3,6,1,4,1,29601,2,72,1,1};
UINT4 VplsConfigIndex [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,1};
UINT4 VplsConfigName [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,2};
UINT4 VplsConfigDescr [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,3};
UINT4 VplsConfigAdminStatus [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,4};
UINT4 VplsConfigMacLearning [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,6};
UINT4 VplsConfigDiscardUnknownDest [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,7};
UINT4 VplsConfigMacAging [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,8};
UINT4 VplsConfigFwdFullHighWatermark [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,10};
UINT4 VplsConfigFwdFullLowWatermark [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,11};
UINT4 VplsConfigRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,12};
UINT4 VplsConfigMtu [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,13};
UINT4 VplsConfigVpnId [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,14};
UINT4 VplsConfigStorageType [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,15};
UINT4 VplsConfigSignalingType [ ] ={1,3,6,1,4,1,29601,2,72,1,2,1,16};
UINT4 VplsStatusOperStatus [ ] ={1,3,6,1,4,1,29601,2,72,1,3,1,1};
UINT4 VplsStatusPeerCount [ ] ={1,3,6,1,4,1,29601,2,72,1,3,1,2};
UINT4 VplsPwBindConfigType [ ] ={1,3,6,1,4,1,29601,2,72,1,4,1,1};
UINT4 VplsPwBindType [ ] ={1,3,6,1,4,1,29601,2,72,1,4,1,2};
UINT4 VplsPwBindRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,1,4,1,3};
UINT4 VplsPwBindStorageType [ ] ={1,3,6,1,4,1,29601,2,72,1,4,1,4};
UINT4 VplsBgpADConfigRouteDistinguisher [ ] ={1,3,6,1,4,1,29601,2,72,1,5,1,1};
UINT4 VplsBgpADConfigPrefix [ ] ={1,3,6,1,4,1,29601,2,72,1,5,1,2};
UINT4 VplsBgpADConfigVplsId [ ] ={1,3,6,1,4,1,29601,2,72,1,5,1,3};
UINT4 VplsBgpADConfigRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,1,5,1,4};
UINT4 VplsBgpADConfigStorageType [ ] ={1,3,6,1,4,1,29601,2,72,1,5,1,5};
UINT4 VplsBgpRteTargetIndex [ ] ={1,3,6,1,4,1,29601,2,72,1,6,1,1};
UINT4 VplsBgpRteTargetRTType [ ] ={1,3,6,1,4,1,29601,2,72,1,6,1,2};
UINT4 VplsBgpRteTargetRT [ ] ={1,3,6,1,4,1,29601,2,72,1,6,1,3};
UINT4 VplsBgpRteTargetRTRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,1,6,1,4};
UINT4 VplsBgpRteTargetStorageType [ ] ={1,3,6,1,4,1,29601,2,72,1,6,1,5};
UINT4 VplsStatusNotifEnable [ ] ={1,3,6,1,4,1,29601,2,72,1,7};
UINT4 VplsNotificationMaxRate [ ] ={1,3,6,1,4,1,29601,2,72,1,8};
UINT4 FsL2VpnPwRedundancyStatus [ ] ={1,3,6,1,4,1,29601,2,72,3,1};
UINT4 FsL2VpnPwRedNegotiationTimeOut [ ] ={1,3,6,1,4,1,29601,2,72,3,2};
UINT4 FsL2VpnPwRedundancySyncFailNotifyEnable [ ] ={1,3,6,1,4,1,29601,2,72,3,3};
UINT4 FsL2VpnPwRedundancyPwStatusNotifyEnable [ ] ={1,3,6,1,4,1,29601,2,72,3,4};
UINT4 FsL2VpnPwRedGroupIndex [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,1};
UINT4 FsL2VpnPwRedGroupProtType [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,2};
UINT4 FsL2VpnPwRedGroupReversionType [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,3};
UINT4 FsL2VpnPwRedGroupContentionResolutionMethod [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,4};
UINT4 FsL2VpnPwRedGroupLockoutProtection [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,5};
UINT4 FsL2VpnPwRedGroupMasterSlaveMode [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,6};
UINT4 FsL2VpnPwRedGroupDualHomeApps [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,7};
UINT4 FsL2VpnPwRedGroupName [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,8};
UINT4 FsL2VpnPwRedGroupStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,9};
UINT4 FsL2VpnPwRedGroupOperActivePw [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,10};
UINT4 FsL2VpnPwRedGroupWtrTimer [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,11};
UINT4 FsL2VpnPwRedGroupAdminCmd [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,12};
UINT4 FsL2VpnPwRedGroupAdminActivePw [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,13};
UINT4 FsL2VpnPwRedGroupAdminCmdStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,14};
UINT4 FsL2VpnPwRedGroupRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,1,1,15};
UINT4 FsL2VpnPwRedNodeAddrType [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,1};
UINT4 FsL2VpnPwRedNodeAddr [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,2};
UINT4 FsL2VpnPwRedNodeLocalLdpID [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,3};
UINT4 FsL2VpnPwRedNodeLocalLdpEntityIndex [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,4};
UINT4 FsL2VpnPwRedNodePeerLdpID [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,5};
UINT4 FsL2VpnPwRedNodeStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,6};
UINT4 FsL2VpnPwRedNodeRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,2,1,7};
UINT4 FsL2VpnPwRedPwIndex [ ] ={1,3,6,1,4,1,29601,2,72,4,3,1,1};
UINT4 FsL2VpnPwRedPwPreferance [ ] ={1,3,6,1,4,1,29601,2,72,4,3,1,2};
UINT4 FsL2VpnPwRedPwLocalStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,3,1,3};
UINT4 FsL2VpnPwRedPwRemoteStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,3,1,4};
UINT4 FsL2VpnPwRedPwOperStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,3,1,5};
UINT4 FsL2VpnPwRedPwRowStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,3,1,6};
UINT4 FsL2VpnPwRedIccpPwRgIndex [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,1};
UINT4 FsL2VpnPwRedIccpPwHeadLsr [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,2};
UINT4 FsL2VpnPwRedIccpPwFecType [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,3};
UINT4 FsL2VpnPwRedIccpPwTailLsr [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,4};
UINT4 FsL2VpnPwRedIccpPwGroup [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,5};
UINT4 FsL2VpnPwRedIccpPwId [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,6};
UINT4 FsL2VpnPwRedIccpPwAgiType [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,7};
UINT4 FsL2VpnPwRedIccpPwAgi [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,8};
UINT4 FsL2VpnPwRedIccpPwLocalAiiType [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,9};
UINT4 FsL2VpnPwRedIccpPwLocalAii [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,10};
UINT4 FsL2VpnPwRedIccpPwRemoteAiiType [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,11};
UINT4 FsL2VpnPwRedIccpPwRemoteAii [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,12};
UINT4 FsL2VpnPwRedIccpPwRoId [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,13};
UINT4 FsL2VpnPwRedIccpPwPriority [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,14};
UINT4 FsL2VpnPwRedIccpPwStatus [ ] ={1,3,6,1,4,1,29601,2,72,4,4,1,15};
UINT4 FsL2VpnPwRedSimulateFailure [ ] ={1,3,6,1,4,1,29601,2,72,4,5,1};
UINT4 FsL2VpnPwRedSimulateFailureForNbr [ ] ={1,3,6,1,4,1,29601,2,72,4,5,2};




tMbDbEntry fsl2vpMibEntry[]= {

{{11,VplsConfigIndexNext}, NULL, VplsConfigIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,VplsConfigIndex}, GetNextIndexVplsConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, VplsConfigTableINDEX, 1, 0, 0, NULL},

{{13,VplsConfigName}, GetNextIndexVplsConfigTable, VplsConfigNameGet, VplsConfigNameSet, VplsConfigNameTest, VplsConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, ""},

{{13,VplsConfigDescr}, GetNextIndexVplsConfigTable, VplsConfigDescrGet, VplsConfigDescrSet, VplsConfigDescrTest, VplsConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, ""},

{{13,VplsConfigAdminStatus}, GetNextIndexVplsConfigTable, VplsConfigAdminStatusGet, VplsConfigAdminStatusSet, VplsConfigAdminStatusTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "2"},

{{13,VplsConfigMacLearning}, GetNextIndexVplsConfigTable, VplsConfigMacLearningGet, VplsConfigMacLearningSet, VplsConfigMacLearningTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "1"},

{{13,VplsConfigDiscardUnknownDest}, GetNextIndexVplsConfigTable, VplsConfigDiscardUnknownDestGet, VplsConfigDiscardUnknownDestSet, VplsConfigDiscardUnknownDestTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "2"},

{{13,VplsConfigMacAging}, GetNextIndexVplsConfigTable, VplsConfigMacAgingGet, VplsConfigMacAgingSet, VplsConfigMacAgingTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "1"},

{{13,VplsConfigFwdFullHighWatermark}, GetNextIndexVplsConfigTable, VplsConfigFwdFullHighWatermarkGet, VplsConfigFwdFullHighWatermarkSet, VplsConfigFwdFullHighWatermarkTest, VplsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "95"},

{{13,VplsConfigFwdFullLowWatermark}, GetNextIndexVplsConfigTable, VplsConfigFwdFullLowWatermarkGet, VplsConfigFwdFullLowWatermarkSet, VplsConfigFwdFullLowWatermarkTest, VplsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "90"},

{{13,VplsConfigRowStatus}, GetNextIndexVplsConfigTable, VplsConfigRowStatusGet, VplsConfigRowStatusSet, VplsConfigRowStatusTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 1, NULL},

{{13,VplsConfigMtu}, GetNextIndexVplsConfigTable, VplsConfigMtuGet, VplsConfigMtuSet, VplsConfigMtuTest, VplsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "1518"},

{{13,VplsConfigVpnId}, GetNextIndexVplsConfigTable, VplsConfigVpnIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, VplsConfigTableINDEX, 1, 0, 0, NULL},

{{13,VplsConfigStorageType}, GetNextIndexVplsConfigTable, VplsConfigStorageTypeGet, VplsConfigStorageTypeSet, VplsConfigStorageTypeTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "2"},

{{13,VplsConfigSignalingType}, GetNextIndexVplsConfigTable, VplsConfigSignalingTypeGet, VplsConfigSignalingTypeSet, VplsConfigSignalingTypeTest, VplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsConfigTableINDEX, 1, 0, 0, "3"},

{{13,VplsStatusOperStatus}, GetNextIndexVplsStatusTable, VplsStatusOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, VplsStatusTableINDEX, 1, 0, 0, NULL},

{{13,VplsStatusPeerCount}, GetNextIndexVplsStatusTable, VplsStatusPeerCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VplsStatusTableINDEX, 1, 0, 0, NULL},

{{13,VplsPwBindConfigType}, GetNextIndexVplsPwBindTable, VplsPwBindConfigTypeGet, VplsPwBindConfigTypeSet, VplsPwBindConfigTypeTest, VplsPwBindTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsPwBindTableINDEX, 2, 0, 0, NULL},

{{13,VplsPwBindType}, GetNextIndexVplsPwBindTable, VplsPwBindTypeGet, VplsPwBindTypeSet, VplsPwBindTypeTest, VplsPwBindTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsPwBindTableINDEX, 2, 0, 0, NULL},

{{13,VplsPwBindRowStatus}, GetNextIndexVplsPwBindTable, VplsPwBindRowStatusGet, VplsPwBindRowStatusSet, VplsPwBindRowStatusTest, VplsPwBindTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsPwBindTableINDEX, 2, 0, 1, NULL},

{{13,VplsPwBindStorageType}, GetNextIndexVplsPwBindTable, VplsPwBindStorageTypeGet, VplsPwBindStorageTypeSet, VplsPwBindStorageTypeTest, VplsPwBindTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsPwBindTableINDEX, 2, 0, 0, "2"},

{{13,VplsBgpADConfigRouteDistinguisher}, GetNextIndexVplsBgpADConfigTable, VplsBgpADConfigRouteDistinguisherGet, VplsBgpADConfigRouteDistinguisherSet, VplsBgpADConfigRouteDistinguisherTest, VplsBgpADConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VplsBgpADConfigTableINDEX, 1, 0, 0, NULL},

{{13,VplsBgpADConfigPrefix}, GetNextIndexVplsBgpADConfigTable, VplsBgpADConfigPrefixGet, VplsBgpADConfigPrefixSet, VplsBgpADConfigPrefixTest, VplsBgpADConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, VplsBgpADConfigTableINDEX, 1, 0, 0, "0"},

{{13,VplsBgpADConfigVplsId}, GetNextIndexVplsBgpADConfigTable, VplsBgpADConfigVplsIdGet, VplsBgpADConfigVplsIdSet, VplsBgpADConfigVplsIdTest, VplsBgpADConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VplsBgpADConfigTableINDEX, 1, 0, 0, NULL},

{{13,VplsBgpADConfigRowStatus}, GetNextIndexVplsBgpADConfigTable, VplsBgpADConfigRowStatusGet, VplsBgpADConfigRowStatusSet, VplsBgpADConfigRowStatusTest, VplsBgpADConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpADConfigTableINDEX, 1, 0, 1, NULL},

{{13,VplsBgpADConfigStorageType}, GetNextIndexVplsBgpADConfigTable, VplsBgpADConfigStorageTypeGet, VplsBgpADConfigStorageTypeSet, VplsBgpADConfigStorageTypeTest, VplsBgpADConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpADConfigTableINDEX, 1, 0, 0, "3"},

{{13,VplsBgpRteTargetIndex}, GetNextIndexVplsBgpRteTargetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, VplsBgpRteTargetTableINDEX, 2, 0, 0, NULL},

{{13,VplsBgpRteTargetRTType}, GetNextIndexVplsBgpRteTargetTable, VplsBgpRteTargetRTTypeGet, VplsBgpRteTargetRTTypeSet, VplsBgpRteTargetRTTypeTest, VplsBgpRteTargetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpRteTargetTableINDEX, 2, 0, 0, NULL},

{{13,VplsBgpRteTargetRT}, GetNextIndexVplsBgpRteTargetTable, VplsBgpRteTargetRTGet, VplsBgpRteTargetRTSet, VplsBgpRteTargetRTTest, VplsBgpRteTargetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VplsBgpRteTargetTableINDEX, 2, 0, 0, NULL},

{{13,VplsBgpRteTargetRTRowStatus}, GetNextIndexVplsBgpRteTargetTable, VplsBgpRteTargetRTRowStatusGet, VplsBgpRteTargetRTRowStatusSet, VplsBgpRteTargetRTRowStatusTest, VplsBgpRteTargetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpRteTargetTableINDEX, 2, 0, 1, NULL},

{{13,VplsBgpRteTargetStorageType}, GetNextIndexVplsBgpRteTargetTable, VplsBgpRteTargetStorageTypeGet, VplsBgpRteTargetStorageTypeSet, VplsBgpRteTargetStorageTypeTest, VplsBgpRteTargetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VplsBgpRteTargetTableINDEX, 2, 0, 0, "2"},

{{11,VplsStatusNotifEnable}, NULL, VplsStatusNotifEnableGet, VplsStatusNotifEnableSet, VplsStatusNotifEnableTest, VplsStatusNotifEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,VplsNotificationMaxRate}, NULL, VplsNotificationMaxRateGet, VplsNotificationMaxRateSet, VplsNotificationMaxRateTest, VplsNotificationMaxRateDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsL2VpnPwRedundancyStatus}, NULL, FsL2VpnPwRedundancyStatusGet, FsL2VpnPwRedundancyStatusSet, FsL2VpnPwRedundancyStatusTest, FsL2VpnPwRedundancyStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsL2VpnPwRedNegotiationTimeOut}, NULL, FsL2VpnPwRedNegotiationTimeOutGet, FsL2VpnPwRedNegotiationTimeOutSet, FsL2VpnPwRedNegotiationTimeOutTest, FsL2VpnPwRedNegotiationTimeOutDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{11,FsL2VpnPwRedundancySyncFailNotifyEnable}, NULL, FsL2VpnPwRedundancySyncFailNotifyEnableGet, FsL2VpnPwRedundancySyncFailNotifyEnableSet, FsL2VpnPwRedundancySyncFailNotifyEnableTest, FsL2VpnPwRedundancySyncFailNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsL2VpnPwRedundancyPwStatusNotifyEnable}, NULL, FsL2VpnPwRedundancyPwStatusNotifyEnableGet, FsL2VpnPwRedundancyPwStatusNotifyEnableSet, FsL2VpnPwRedundancyPwStatusNotifyEnableTest, FsL2VpnPwRedundancyPwStatusNotifyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsL2VpnPwRedGroupIndex}, GetNextIndexFsL2VpnPwRedGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupProtType}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupProtTypeGet, FsL2VpnPwRedGroupProtTypeSet, FsL2VpnPwRedGroupProtTypeTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "2"},

{{13,FsL2VpnPwRedGroupReversionType}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupReversionTypeGet, FsL2VpnPwRedGroupReversionTypeSet, FsL2VpnPwRedGroupReversionTypeTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "2"},

{{13,FsL2VpnPwRedGroupContentionResolutionMethod}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupContentionResolutionMethodGet, FsL2VpnPwRedGroupContentionResolutionMethodSet, FsL2VpnPwRedGroupContentionResolutionMethodTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "1"},

{{13,FsL2VpnPwRedGroupLockoutProtection}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupLockoutProtectionGet, FsL2VpnPwRedGroupLockoutProtectionSet, FsL2VpnPwRedGroupLockoutProtectionTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "0"},

{{13,FsL2VpnPwRedGroupMasterSlaveMode}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupMasterSlaveModeGet, FsL2VpnPwRedGroupMasterSlaveModeSet, FsL2VpnPwRedGroupMasterSlaveModeTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "2"},

{{13,FsL2VpnPwRedGroupDualHomeApps}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupDualHomeAppsGet, FsL2VpnPwRedGroupDualHomeAppsSet, FsL2VpnPwRedGroupDualHomeAppsTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "0"},

{{13,FsL2VpnPwRedGroupName}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupNameGet, FsL2VpnPwRedGroupNameSet, FsL2VpnPwRedGroupNameTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupStatus}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupOperActivePw}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupOperActivePwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupWtrTimer}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupWtrTimerGet, FsL2VpnPwRedGroupWtrTimerSet, FsL2VpnPwRedGroupWtrTimerTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupAdminCmd}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupAdminCmdGet, FsL2VpnPwRedGroupAdminCmdSet, FsL2VpnPwRedGroupAdminCmdTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, "2"},

{{13,FsL2VpnPwRedGroupAdminActivePw}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupAdminActivePwGet, FsL2VpnPwRedGroupAdminActivePwSet, FsL2VpnPwRedGroupAdminActivePwTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupAdminCmdStatus}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupAdminCmdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsL2VpnPwRedGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsL2VpnPwRedGroupRowStatus}, GetNextIndexFsL2VpnPwRedGroupTable, FsL2VpnPwRedGroupRowStatusGet, FsL2VpnPwRedGroupRowStatusSet, FsL2VpnPwRedGroupRowStatusTest, FsL2VpnPwRedGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedGroupTableINDEX, 1, 0, 1, NULL},

{{13,FsL2VpnPwRedNodeAddrType}, GetNextIndexFsL2VpnPwRedNodeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsL2VpnPwRedNodeTableINDEX, 3, 0, 0, NULL},

{{13,FsL2VpnPwRedNodeAddr}, GetNextIndexFsL2VpnPwRedNodeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsL2VpnPwRedNodeTableINDEX, 3, 0, 0, NULL},

{{13,FsL2VpnPwRedNodeLocalLdpID}, GetNextIndexFsL2VpnPwRedNodeTable, FsL2VpnPwRedNodeLocalLdpIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedNodeTableINDEX, 3, 0, 0, NULL},

{{13,FsL2VpnPwRedNodeLocalLdpEntityIndex}, GetNextIndexFsL2VpnPwRedNodeTable, FsL2VpnPwRedNodeLocalLdpEntityIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsL2VpnPwRedNodeTableINDEX, 3, 0, 0, NULL},

{{13,FsL2VpnPwRedNodePeerLdpID}, GetNextIndexFsL2VpnPwRedNodeTable, FsL2VpnPwRedNodePeerLdpIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedNodeTableINDEX, 3, 0, 0, NULL},

{{13,FsL2VpnPwRedNodeStatus}, GetNextIndexFsL2VpnPwRedNodeTable, FsL2VpnPwRedNodeStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedNodeTableINDEX, 3, 0, 0, NULL},

{{13,FsL2VpnPwRedNodeRowStatus}, GetNextIndexFsL2VpnPwRedNodeTable, FsL2VpnPwRedNodeRowStatusGet, FsL2VpnPwRedNodeRowStatusSet, FsL2VpnPwRedNodeRowStatusTest, FsL2VpnPwRedNodeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedNodeTableINDEX, 3, 0, 1, NULL},

{{13,FsL2VpnPwRedPwIndex}, GetNextIndexFsL2VpnPwRedPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsL2VpnPwRedPwTableINDEX, 2, 0, 0, NULL},

{{13,FsL2VpnPwRedPwPreferance}, GetNextIndexFsL2VpnPwRedPwTable, FsL2VpnPwRedPwPreferanceGet, FsL2VpnPwRedPwPreferanceSet, FsL2VpnPwRedPwPreferanceTest, FsL2VpnPwRedPwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedPwTableINDEX, 2, 0, 0, "2"},

{{13,FsL2VpnPwRedPwLocalStatus}, GetNextIndexFsL2VpnPwRedPwTable, FsL2VpnPwRedPwLocalStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedPwTableINDEX, 2, 0, 0, NULL},

{{13,FsL2VpnPwRedPwRemoteStatus}, GetNextIndexFsL2VpnPwRedPwTable, FsL2VpnPwRedPwRemoteStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedPwTableINDEX, 2, 0, 0, NULL},

{{13,FsL2VpnPwRedPwOperStatus}, GetNextIndexFsL2VpnPwRedPwTable, FsL2VpnPwRedPwOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsL2VpnPwRedPwTableINDEX, 2, 0, 0, NULL},

{{13,FsL2VpnPwRedPwRowStatus}, GetNextIndexFsL2VpnPwRedPwTable, FsL2VpnPwRedPwRowStatusGet, FsL2VpnPwRedPwRowStatusSet, FsL2VpnPwRedPwRowStatusTest, FsL2VpnPwRedPwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsL2VpnPwRedPwTableINDEX, 2, 0, 1, NULL},

{{13,FsL2VpnPwRedIccpPwRgIndex}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwHeadLsr}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwFecType}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwTailLsr}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwGroup}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwId}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwAgiType}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwAgi}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwLocalAiiType}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwLocalAii}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwRemoteAiiType}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwRemoteAii}, GetNextIndexFsL2VpnPwRedIccpPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwRoId}, GetNextIndexFsL2VpnPwRedIccpPwTable, FsL2VpnPwRedIccpPwRoIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwPriority}, GetNextIndexFsL2VpnPwRedIccpPwTable, FsL2VpnPwRedIccpPwPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{13,FsL2VpnPwRedIccpPwStatus}, GetNextIndexFsL2VpnPwRedIccpPwTable, FsL2VpnPwRedIccpPwStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsL2VpnPwRedIccpPwTableINDEX, 12, 0, 0, NULL},

{{12,FsL2VpnPwRedSimulateFailure}, NULL, FsL2VpnPwRedSimulateFailureGet, FsL2VpnPwRedSimulateFailureSet, FsL2VpnPwRedSimulateFailureTest, FsL2VpnPwRedSimulateFailureDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsL2VpnPwRedSimulateFailureForNbr}, NULL, FsL2VpnPwRedSimulateFailureForNbrGet, FsL2VpnPwRedSimulateFailureForNbrSet, FsL2VpnPwRedSimulateFailureForNbrTest, FsL2VpnPwRedSimulateFailureForNbrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsl2vpEntry = { 82, fsl2vpMibEntry };

#endif /* _FSL2VPDB_H */

