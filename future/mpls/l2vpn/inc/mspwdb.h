/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mspwdb.h,v 1.1 2010/11/17 14:03:06 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSPWDB_H
#define _FSMSPWDB_H

UINT1 FsMsPwConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmspw [] ={1,3,6,1,4,1,29601,2,57};
tSNMP_OID_TYPE fsmspwOID = {9, fsmspw};


UINT4 FsMsPwMaxEntries [ ] ={1,3,6,1,4,1,29601,2,57,1,1};
UINT4 FsMsPwIndex1 [ ] ={1,3,6,1,4,1,29601,2,57,1,2,1,1};
UINT4 FsMsPwIndex2 [ ] ={1,3,6,1,4,1,29601,2,57,1,2,1,2};
UINT4 FsMsPwOperStatus [ ] ={1,3,6,1,4,1,29601,2,57,1,2,1,3};
UINT4 FsMsPwRowStatus [ ] ={1,3,6,1,4,1,29601,2,57,1,2,1,4};




tMbDbEntry fsmspwMibEntry[]= {

{{11,FsMsPwMaxEntries}, NULL, FsMsPwMaxEntriesGet, FsMsPwMaxEntriesSet, FsMsPwMaxEntriesTest, FsMsPwMaxEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "64"},

{{13,FsMsPwIndex1}, GetNextIndexFsMsPwConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMsPwConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsMsPwIndex2}, GetNextIndexFsMsPwConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMsPwConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsMsPwOperStatus}, GetNextIndexFsMsPwConfigTable, FsMsPwOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMsPwConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsMsPwRowStatus}, GetNextIndexFsMsPwConfigTable, FsMsPwRowStatusGet, FsMsPwRowStatusSet, FsMsPwRowStatusTest, FsMsPwConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMsPwConfigTableINDEX, 2, 0, 1, NULL},
};
tMibData fsmspwEntry = { 5, fsmspwMibEntry };

#endif /* _FSMSPWDB_H */

