/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwlw.h,v 1.5 2008/08/20 15:14:26 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for PwTable. */
INT1
nmhValidateIndexInstancePwTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwTable  */

INT1
nmhGetFirstIndexPwTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwOwner ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwPsnType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwSetUpPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwHoldingPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwPeerAddrType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwPeerAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwAttachedPwIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwLocalGroupID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwGroupAttachmentID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwLocalAttachmentID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwPeerAttachmentID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwCwPreference ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwLocalIfMtu ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwLocalIfString ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwLocalCapabAdvert ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwRemoteGroupID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwCwStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwRemoteIfMtu ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwRemoteIfString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwRemoteCapabilities ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwFragmentCfgSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwRmtFragCapability ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwFcsRetentioncfg ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwFcsRetentionStatus ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwOutboundLabel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwInboundLabel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwDescr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwCreateTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwUpTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwLastChange ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwOperStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwLocalStatus ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwRemoteStatusCapable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwRemoteStatus ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPwTimeElapsed ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwValidIntervals ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetPwStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPwType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwOwner ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwPsnType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwSetUpPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwHoldingPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwPeerAddrType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwPeerAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwAttachedPwIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwID ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwLocalGroupID ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwGroupAttachmentID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwLocalAttachmentID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwPeerAttachmentID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwCwPreference ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwLocalIfMtu ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwLocalIfString ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwLocalCapabAdvert ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwFragmentCfgSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwFcsRetentioncfg ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwOutboundLabel ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwInboundLabel ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetPwName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwDescr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPwAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetPwStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PwType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwOwner ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwPsnType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwSetUpPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwHoldingPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwPeerAddrType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwPeerAddr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwAttachedPwIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwID ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwLocalGroupID ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwGroupAttachmentID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwLocalAttachmentID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwPeerAttachmentID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwCwPreference ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwLocalIfMtu ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwLocalIfString ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwLocalCapabAdvert ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwFragmentCfgSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwFcsRetentioncfg ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwOutboundLabel ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwInboundLabel ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2PwName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwDescr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PwAdminStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2PwStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PwPerfCurrentTable. */
INT1
nmhValidateIndexInstancePwPerfCurrentTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwPerfCurrentTable  */

INT1
nmhGetFirstIndexPwPerfCurrentTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwPerfCurrentTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwPerfCurrentInHCPackets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfCurrentInHCBytes ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfCurrentOutHCPackets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfCurrentOutHCBytes ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfCurrentInPackets ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwPerfCurrentInBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwPerfCurrentOutPackets ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetPwPerfCurrentOutBytes ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for PwPerfIntervalTable. */
INT1
nmhValidateIndexInstancePwPerfIntervalTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PwPerfIntervalTable  */

INT1
nmhGetFirstIndexPwPerfIntervalTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwPerfIntervalTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwPerfIntervalValidData ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetPwPerfIntervalTimeElapsed ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetPwPerfIntervalInHCPackets ARG_LIST((UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfIntervalInHCBytes ARG_LIST((UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfIntervalOutHCPackets ARG_LIST((UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfIntervalOutHCBytes ARG_LIST((UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerfIntervalInPackets ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetPwPerfIntervalInBytes ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetPwPerfIntervalOutPackets ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetPwPerfIntervalOutBytes ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for PwPerf1DayIntervalTable. */
INT1
nmhValidateIndexInstancePwPerf1DayIntervalTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwPerf1DayIntervalTable  */

INT1
nmhGetFirstIndexPwPerf1DayIntervalTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwPerf1DayIntervalTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwPerf1DayIntervalValidData ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPwPerf1DayIntervalMoniSecs ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetPwPerf1DayIntervalInHCPackets ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerf1DayIntervalInHCBytes ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerf1DayIntervalOutHCPackets ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPwPerf1DayIntervalOutHCBytes ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwPerfTotalErrorPackets ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for PwIndexMappingTable. */
INT1
nmhValidateIndexInstancePwIndexMappingTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for PwIndexMappingTable  */

INT1
nmhGetFirstIndexPwIndexMappingTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwIndexMappingTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwIndexMappingPwIndex ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for PwPeerMappingTable. */
INT1
nmhValidateIndexInstancePwPeerMappingTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for PwPeerMappingTable  */

INT1
nmhGetFirstIndexPwPeerMappingTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPwPeerMappingTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwPeerMappingPwIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPwUpDownNotifEnable ARG_LIST((INT4 *));

INT1
nmhGetPwDeletedNotifEnable ARG_LIST((INT4 *));

INT1
nmhGetPwNotifRate ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPwUpDownNotifEnable ARG_LIST((INT4 ));

INT1
nmhSetPwDeletedNotifEnable ARG_LIST((INT4 ));

INT1
nmhSetPwNotifRate ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PwUpDownNotifEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PwDeletedNotifEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PwNotifRate ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PwUpDownNotifEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PwDeletedNotifEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PwNotifRate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
