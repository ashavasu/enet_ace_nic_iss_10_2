/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpbgp.h,v 1.2 2014/08/25 11:59:03 siva Exp $
 *****************************************************************************
 *    FILE  NAME             : l2vpbgp.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2-VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Prototypes of bgp interface submodule.
 *---------------------------------------------------------------------------*/


#ifndef _L2VPN_BGP_H
#define _L2VPN_BGP_H

VOID
L2VpnProcessBgpAdvEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo);

VOID
L2VpnProcessBgpWithdrawEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo);

INT4
L2VpnSendBgpAdvEvent (UINT4 u4VplsInstance,
                      UINT4 u4VBO);

INT4
L2VpnSendBgpWithdrawEvent (UINT4 u4VplsInstance,
                           UINT4 u4VplsLocalVEId,
                           UINT4 u4VplsMtu,
                           UINT4 u4VBO,
                           UINT4 u4LabelBase,
                           UINT1 *pu1RouteDistinguisher,
                           UINT1 *pu1RouteTarget,
                           BOOL1 bControlWordFlag);

VOID
L2VpnSendBgpRtAddEvent (UINT4 u4VplsIndex,
                        UINT1 *pu1RouteTarget,
                        UINT1 u1RTType );

VOID
L2VpnSendBgpRtDeleteEvent (UINT4 u4VplsIndex,
                           UINT1 *pu1RouteTarget,
                           UINT1 u1RTType );

VOID
L2VpnProcessBgpUpEvent (VOID);

VOID
L2VpnProcessBgpDownEvent (VOID);
#ifdef VPLS_GR_WANTED
VOID
L2VpnProcessBgpGREvent (VOID);
VOID
L2VpnProcessBgpGRInProgressEvent (tL2VpnBgpEvtInfo * pL2VpnBgpEvtInfo);
INT4
L2VpnMarkBgpPwEntriesStale (VOID);
INT4
L2VpnSendBgpRtsAddEvent (UINT4 u4VplsIndex);
INT4
L2VpnSendBgpGrAdvEvent (UINT4 u4VplsInstance, UINT4 u4VBO);
VOID
L2VpnSendBgpEoVplsEvent (VOID);
INT4
L2VpnReserveLabelFromHwList (tL2VpnPwHwList *pL2VpnPwHwListEntry);
VOID
L2VpnBgpDeleteStaleEntries (VOID);
INT4
L2VpnSetLb (UINT4 u4LabelBase);
VOID
L2VpnBgpDeletePwEntryOnMismatch (tL2VpnPwHwList  *pL2VpnPwHwListEntry);

#endif
VOID
L2VpnSendBgpVplsCreateEvent (UINT4 u4VplsIndex);

VOID
L2VpnSendBgpVplsDeleteEvent (UINT4 u4VplsIndex);

VOID
L2VpnSendBgpVplsUpEvent (UINT4 u4VplsIndex);

VOID
L2VpnSendBgpVplsDownEvent (UINT4 u4VplsIndex);

#endif /*_L2VPN_DEFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file L2vpdefs.h                             */
/*---------------------------------------------------------------------------*/
