/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpgbl.h,v 1.6 2015/11/12 12:25:26 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the global variable
 *                             definitions
 *---------------------------------------------------------------------------*/
#ifndef _L2VPGBL_H 
#define _L2VPGBL_H
/*
tL2VpnGlobalInfo gL2VpnInfo;
tPwVcGlobalInfo gPwVcGlobalInfo;
tPwVcMplsGlobalInfo gPwVcMplsGblInfo; 
tPwVcEnetGlobalInfo gPwVcEnetGblInfo;
*/
/* Global Variables */

tL2VpnGlobalInfo    gL2VpnGlobalInfo;
tPwVcGlobalInfo     gPwVcGlobalInfo;
tPwVcMplsGlobalInfo gPwVcMplsGlobalInfo;
tPwVcEnetGlobalInfo gPwVcEnetGlobalInfo;
tL2vpnMsPwGlobals   gL2vpnMsPwGlobals;  /*MS-PW */
tL2vpnRedundancyGlobals     gL2vpnRgGlobals;    /* PW Redundancy */
tL2vpnPwRedTest     gL2vpnPwRedTest;

tL2VpnGlobalInfo    *gpL2VpnGlobalInfo = &gL2VpnGlobalInfo;
tPwVcGlobalInfo     *gpPwVcGlobalInfo = &gPwVcGlobalInfo;
tPwVcMplsGlobalInfo *gpPwVcMplsGlobalInfo = &gPwVcMplsGlobalInfo;
tPwVcEnetGlobalInfo *gpPwVcEnetGlobalInfo = &gPwVcEnetGlobalInfo;
#endif /*_L2VPGBL_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file l2vpgbl.h                              */
/*---------------------------------------------------------------------------*/
