/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: l2vpincs.h,v 1.18 2014/11/08 11:41:09 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpincs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains all the associated include
 *                             files for the L2VPN support.
 *---------------------------------------------------------------------------*/

#ifndef _L2VPINCS_H
#define _L2VPINCS_H



/* Common Include Files */
#include "lr.h"
#include "cust.h"
#include "cfa.h"
#include "ip.h"
#include "ipvx.h"
#include "include.h"
#include "fm.h"
/* RBTree Include */
#include "redblack.h"

/* label manager  includes */
#include "lblmgrex.h"

/* Trie includes */
#include "trieapif.h"
#include "triecidr.h"
#include "elps.h"
/* mplsFM includes */
#include "mplssize.h"
#include "mplsdiff.h"
#include "mpls.h"
#include "mplsdefs.h"
#include "mplfmext.h"
#include "mplsutil.h"
#include "fsmplslw.h"
#include "mplcmndb.h"
#include "mplsdbsz.h"
#include "mplshwlist.h"

/* Te includes */
#include "teextrn.h"
/* Index manager include */
#include "inmgrex.h"
#include "indexmgr.h"
/* Mpls NP include */
#include "mplsnp.h"
#include "mplsnpex.h"

/*MS-PW */
#include "mspwlwg.h"
#include "mspwwrg.h"

/* L2VPN related files */
#include "l2vpdefs.h"
#include "l2vpextn.h"
#include "l2vpif.h"
#include "l2vptdfs.h"
#include "l2vpmacs.h"
#include "l2vpprot.h"
#include "l2vpgblx.h"
#include "l2vpnsz.h"
#include "l2vpdbg.h"
#include "l2vprm.h"
#include "../../../inc/rmgr.h"
#include "cli.h"
#include "l2vpclip.h"

/*PW related files */
#include "stdpwwr.h"
#include "stdpwlw.h"
#include "stdpwmwr.h"
#include "stdpwmlw.h"
#include "stdpwewr.h"
#include "stdpwelw.h"
/* PW Notification files */
#include "fsmpnolw.h"
#include "fsmpnowr.h"
#include "fsl2vplw.h"
#include "fsl2vpwr.h"
#ifdef VPLSADS_WANTED
#include "l2vpbgp.h"
#include "fsVplslw.h"
#include "fsVplswr.h"
#include "bgp.h"
#endif
#include "ip6util.h"
#endif /* _L2VPINCS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file l2vpincs.h                             */
/*---------------------------------------------------------------------------*/
