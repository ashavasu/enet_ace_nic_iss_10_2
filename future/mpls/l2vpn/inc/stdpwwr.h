#ifndef _STDPWWR_H
#define _STDPWWR_H

VOID RegisterSTDPW(VOID);
INT4 PwIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwOwnerGet(tSnmpIndex *, tRetVal *);
INT4 PwPsnTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwSetUpPriorityGet(tSnmpIndex *, tRetVal *);
INT4 PwHoldingPriorityGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerAddrGet(tSnmpIndex *, tRetVal *);
INT4 PwAttachedPwIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwIDGet(tSnmpIndex *, tRetVal *);
INT4 PwLocalGroupIDGet(tSnmpIndex *, tRetVal *);
INT4 PwGroupAttachmentIDGet(tSnmpIndex *, tRetVal *);
INT4 PwLocalAttachmentIDGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerAttachmentIDGet(tSnmpIndex *, tRetVal *);
INT4 PwCwPreferenceGet(tSnmpIndex *, tRetVal *);
INT4 PwLocalIfMtuGet(tSnmpIndex *, tRetVal *);
INT4 PwLocalIfStringGet(tSnmpIndex *, tRetVal *);
INT4 PwLocalCapabAdvertGet(tSnmpIndex *, tRetVal *);
INT4 PwRemoteGroupIDGet(tSnmpIndex *, tRetVal *);
INT4 PwCwStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwRemoteIfMtuGet(tSnmpIndex *, tRetVal *);
INT4 PwRemoteIfStringGet(tSnmpIndex *, tRetVal *);
INT4 PwRemoteCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 PwFragmentCfgSizeGet(tSnmpIndex *, tRetVal *);
INT4 PwRmtFragCapabilityGet(tSnmpIndex *, tRetVal *);
INT4 PwFcsRetentioncfgGet(tSnmpIndex *, tRetVal *);
INT4 PwFcsRetentionStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwOutboundLabelGet(tSnmpIndex *, tRetVal *);
INT4 PwInboundLabelGet(tSnmpIndex *, tRetVal *);
INT4 PwNameGet(tSnmpIndex *, tRetVal *);
INT4 PwDescrGet(tSnmpIndex *, tRetVal *);
INT4 PwCreateTimeGet(tSnmpIndex *, tRetVal *);
INT4 PwUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 PwLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 PwAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwLocalStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwRemoteStatusCapableGet(tSnmpIndex *, tRetVal *);
INT4 PwRemoteStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwTimeElapsedGet(tSnmpIndex *, tRetVal *);
INT4 PwValidIntervalsGet(tSnmpIndex *, tRetVal *);
INT4 PwRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwOwnerSet(tSnmpIndex *, tRetVal *);
INT4 PwPsnTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwSetUpPrioritySet(tSnmpIndex *, tRetVal *);
INT4 PwHoldingPrioritySet(tSnmpIndex *, tRetVal *);
INT4 PwPeerAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwPeerAddrSet(tSnmpIndex *, tRetVal *);
INT4 PwAttachedPwIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwIDSet(tSnmpIndex *, tRetVal *);
INT4 PwLocalGroupIDSet(tSnmpIndex *, tRetVal *);
INT4 PwGroupAttachmentIDSet(tSnmpIndex *, tRetVal *);
INT4 PwLocalAttachmentIDSet(tSnmpIndex *, tRetVal *);
INT4 PwPeerAttachmentIDSet(tSnmpIndex *, tRetVal *);
INT4 PwCwPreferenceSet(tSnmpIndex *, tRetVal *);
INT4 PwLocalIfMtuSet(tSnmpIndex *, tRetVal *);
INT4 PwLocalIfStringSet(tSnmpIndex *, tRetVal *);
INT4 PwLocalCapabAdvertSet(tSnmpIndex *, tRetVal *);
INT4 PwFragmentCfgSizeSet(tSnmpIndex *, tRetVal *);
INT4 PwFcsRetentioncfgSet(tSnmpIndex *, tRetVal *);
INT4 PwOutboundLabelSet(tSnmpIndex *, tRetVal *);
INT4 PwInboundLabelSet(tSnmpIndex *, tRetVal *);
INT4 PwNameSet(tSnmpIndex *, tRetVal *);
INT4 PwDescrSet(tSnmpIndex *, tRetVal *);
INT4 PwAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 PwRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 PwStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwOwnerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwPsnTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwSetUpPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwHoldingPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwPeerAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwPeerAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwAttachedPwIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwLocalGroupIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwGroupAttachmentIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwLocalAttachmentIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwPeerAttachmentIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwCwPreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwLocalIfMtuTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwLocalIfStringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwLocalCapabAdvertTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwFragmentCfgSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwFcsRetentioncfgTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwOutboundLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwInboundLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwDescrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



























INT4 GetNextIndexPwPerfCurrentTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwPerfCurrentInHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentInHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentOutHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentOutHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentInPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentInBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentOutPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfCurrentOutBytesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwPerfIntervalTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwPerfIntervalNumberGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalValidDataGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalTimeElapsedGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalInHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalInHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalOutHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalOutHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalInPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalInBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalOutPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfIntervalOutBytesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwPerf1DayIntervalTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwPerf1DayIntervalNumberGet(tSnmpIndex *, tRetVal *);
INT4 PwPerf1DayIntervalValidDataGet(tSnmpIndex *, tRetVal *);
INT4 PwPerf1DayIntervalMoniSecsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerf1DayIntervalInHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerf1DayIntervalInHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerf1DayIntervalOutHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 PwPerf1DayIntervalOutHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 PwPerfTotalErrorPacketsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwIndexMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwIndexMappingPwTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwIndexMappingPwIDGet(tSnmpIndex *, tRetVal *);
INT4 PwIndexMappingPeerAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwIndexMappingPeerAddrGet(tSnmpIndex *, tRetVal *);
INT4 PwIndexMappingPwIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPwPeerMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwPeerMappingPeerAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerMappingPeerAddrGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerMappingPwTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerMappingPwIDGet(tSnmpIndex *, tRetVal *);
INT4 PwPeerMappingPwIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwUpDownNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 PwDeletedNotifEnableGet(tSnmpIndex *, tRetVal *);
INT4 PwNotifRateGet(tSnmpIndex *, tRetVal *);
INT4 PwUpDownNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 PwDeletedNotifEnableSet(tSnmpIndex *, tRetVal *);
INT4 PwNotifRateSet(tSnmpIndex *, tRetVal *);
INT4 PwUpDownNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwDeletedNotifEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwNotifRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwUpDownNotifEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PwDeletedNotifEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PwNotifRateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);



#endif /* _STDPWWR_H */
