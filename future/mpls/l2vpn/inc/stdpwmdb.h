/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwmdb.h,v 1.5 2008/08/20 15:14:26 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDPWMDB_H
#define _STDPWMDB_H

UINT1 PwMplsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwMplsOutboundTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwMplsInboundTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwMplsNonTeMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwMplsTeMappingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdpwm [] ={1,3,6,1,4,1,2076,13,6};
tSNMP_OID_TYPE stdpwmOID = {9, stdpwm};


UINT4 PwMplsMplsType [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,1};
UINT4 PwMplsExpBitsMode [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,2};
UINT4 PwMplsExpBits [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,3};
UINT4 PwMplsTtl [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,4};
UINT4 PwMplsLocalLdpID [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,5};
UINT4 PwMplsLocalLdpEntityID [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,6};
UINT4 PwMplsPeerLdpID [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,7};
UINT4 PwMplsStorageType [ ] ={1,3,6,1,4,1,2076,13,6,1,1,1,8};
UINT4 PwMplsOutboundLsrXcIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,1};
UINT4 PwMplsOutboundTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,2};
UINT4 PwMplsOutboundTunnelInstance [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,3};
UINT4 PwMplsOutboundTunnelLclLSR [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,4};
UINT4 PwMplsOutboundTunnelPeerLSR [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,5};
UINT4 PwMplsOutboundIfIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,6};
UINT4 PwMplsOutboundTunnelTypeInUse [ ] ={1,3,6,1,4,1,2076,13,6,1,2,1,7};
UINT4 PwMplsInboundXcIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,3,1,1};
UINT4 PwMplsNonTeMappingDirection [ ] ={1,3,6,1,4,1,2076,13,6,1,4,1,1};
UINT4 PwMplsNonTeMappingXcIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,4,1,2};
UINT4 PwMplsNonTeMappingIfIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,4,1,3};
UINT4 PwMplsNonTeMappingVcIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,4,1,4};
UINT4 PwMplsTeMappingTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,5,1,1};
UINT4 PwMplsTeMappingTunnelInstance [ ] ={1,3,6,1,4,1,2076,13,6,1,5,1,2};
UINT4 PwMplsTeMappingTunnelPeerLsrID [ ] ={1,3,6,1,4,1,2076,13,6,1,5,1,3};
UINT4 PwMplsTeMappingTunnelLocalLsrID [ ] ={1,3,6,1,4,1,2076,13,6,1,5,1,4};
UINT4 PwMplsTeMappingVcIndex [ ] ={1,3,6,1,4,1,2076,13,6,1,5,1,5};


tMbDbEntry stdpwmMibEntry[]= {

{{13,PwMplsMplsType}, GetNextIndexPwMplsTable, PwMplsMplsTypeGet, PwMplsMplsTypeSet, PwMplsMplsTypeTest, PwMplsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, "1"},

{{13,PwMplsExpBitsMode}, GetNextIndexPwMplsTable, PwMplsExpBitsModeGet, PwMplsExpBitsModeSet, PwMplsExpBitsModeTest, PwMplsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, "1"},

{{13,PwMplsExpBits}, GetNextIndexPwMplsTable, PwMplsExpBitsGet, PwMplsExpBitsSet, PwMplsExpBitsTest, PwMplsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, "0"},

{{13,PwMplsTtl}, GetNextIndexPwMplsTable, PwMplsTtlGet, PwMplsTtlSet, PwMplsTtlTest, PwMplsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, "2"},

{{13,PwMplsLocalLdpID}, GetNextIndexPwMplsTable, PwMplsLocalLdpIDGet, PwMplsLocalLdpIDSet, PwMplsLocalLdpIDTest, PwMplsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsLocalLdpEntityID}, GetNextIndexPwMplsTable, PwMplsLocalLdpEntityIDGet, PwMplsLocalLdpEntityIDSet, PwMplsLocalLdpEntityIDTest, PwMplsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsPeerLdpID}, GetNextIndexPwMplsTable, PwMplsPeerLdpIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwMplsTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsStorageType}, GetNextIndexPwMplsTable, PwMplsStorageTypeGet, PwMplsStorageTypeSet, PwMplsStorageTypeTest, PwMplsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwMplsTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundLsrXcIndex}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundLsrXcIndexGet, PwMplsOutboundLsrXcIndexSet, PwMplsOutboundLsrXcIndexTest, PwMplsOutboundTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundTunnelIndex}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundTunnelIndexGet, PwMplsOutboundTunnelIndexSet, PwMplsOutboundTunnelIndexTest, PwMplsOutboundTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundTunnelInstance}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundTunnelInstanceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundTunnelLclLSR}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundTunnelLclLSRGet, PwMplsOutboundTunnelLclLSRSet, PwMplsOutboundTunnelLclLSRTest, PwMplsOutboundTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundTunnelPeerLSR}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundTunnelPeerLSRGet, PwMplsOutboundTunnelPeerLSRSet, PwMplsOutboundTunnelPeerLSRTest, PwMplsOutboundTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundIfIndex}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundIfIndexGet, PwMplsOutboundIfIndexSet, PwMplsOutboundIfIndexTest, PwMplsOutboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsOutboundTunnelTypeInUse}, GetNextIndexPwMplsOutboundTable, PwMplsOutboundTunnelTypeInUseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PwMplsOutboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsInboundXcIndex}, GetNextIndexPwMplsInboundTable, PwMplsInboundXcIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwMplsInboundTableINDEX, 1, 0, 0, NULL},

{{13,PwMplsNonTeMappingDirection}, GetNextIndexPwMplsNonTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PwMplsNonTeMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwMplsNonTeMappingXcIndex}, GetNextIndexPwMplsNonTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, PwMplsNonTeMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwMplsNonTeMappingIfIndex}, GetNextIndexPwMplsNonTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PwMplsNonTeMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwMplsNonTeMappingVcIndex}, GetNextIndexPwMplsNonTeMappingTable, PwMplsNonTeMappingVcIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwMplsNonTeMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwMplsTeMappingTunnelIndex}, GetNextIndexPwMplsTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwMplsTeMappingTableINDEX, 5, 0, 0, NULL},

{{13,PwMplsTeMappingTunnelInstance}, GetNextIndexPwMplsTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwMplsTeMappingTableINDEX, 5, 0, 0, NULL},

{{13,PwMplsTeMappingTunnelPeerLsrID}, GetNextIndexPwMplsTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, PwMplsTeMappingTableINDEX, 5, 0, 0, NULL},

{{13,PwMplsTeMappingTunnelLocalLsrID}, GetNextIndexPwMplsTeMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, PwMplsTeMappingTableINDEX, 5, 0, 0, NULL},

{{13,PwMplsTeMappingVcIndex}, GetNextIndexPwMplsTeMappingTable, PwMplsTeMappingVcIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwMplsTeMappingTableINDEX, 5, 0, 0, NULL},
};
tMibData stdpwmEntry = { 25, stdpwmMibEntry };
#endif /* _STDPWMDB_H */

