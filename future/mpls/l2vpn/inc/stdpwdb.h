/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpwdb.h,v 1.6 2008/08/20 15:14:26 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDPWDB_H
#define _STDPWDB_H

UINT1 PwTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwPerfCurrentTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwPerfIntervalTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 PwPerf1DayIntervalTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 PwIndexMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 PwPeerMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdpw [] ={1,3,6,1,4,1,2076,13,5};
tSNMP_OID_TYPE stdpwOID = {9, stdpw};


UINT4 PwIndexNext [ ] ={1,3,6,1,4,1,2076,13,5,1,1};
UINT4 PwIndex [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,1};
UINT4 PwType [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,2};
UINT4 PwOwner [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,3};
UINT4 PwPsnType [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,4};
UINT4 PwSetUpPriority [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,5};
UINT4 PwHoldingPriority [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,6};
UINT4 PwPeerAddrType [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,8};
UINT4 PwPeerAddr [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,9};
UINT4 PwAttachedPwIndex [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,10};
UINT4 PwIfIndex [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,11};
UINT4 PwID [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,12};
UINT4 PwLocalGroupID [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,13};
UINT4 PwGroupAttachmentID [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,14};
UINT4 PwLocalAttachmentID [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,15};
UINT4 PwPeerAttachmentID [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,16};
UINT4 PwCwPreference [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,17};
UINT4 PwLocalIfMtu [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,18};
UINT4 PwLocalIfString [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,19};
UINT4 PwLocalCapabAdvert [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,20};
UINT4 PwRemoteGroupID [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,21};
UINT4 PwCwStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,22};
UINT4 PwRemoteIfMtu [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,23};
UINT4 PwRemoteIfString [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,24};
UINT4 PwRemoteCapabilities [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,25};
UINT4 PwFragmentCfgSize [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,26};
UINT4 PwRmtFragCapability [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,27};
UINT4 PwFcsRetentioncfg [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,28};
UINT4 PwFcsRetentionStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,29};
UINT4 PwOutboundLabel [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,30};
UINT4 PwInboundLabel [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,31};
UINT4 PwName [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,32};
UINT4 PwDescr [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,33};
UINT4 PwCreateTime [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,34};
UINT4 PwUpTime [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,35};
UINT4 PwLastChange [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,36};
UINT4 PwAdminStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,37};
UINT4 PwOperStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,38};
UINT4 PwLocalStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,39};
UINT4 PwRemoteStatusCapable [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,40};
UINT4 PwRemoteStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,41};
UINT4 PwTimeElapsed [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,42};
UINT4 PwValidIntervals [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,43};
UINT4 PwRowStatus [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,44};
UINT4 PwStorageType [ ] ={1,3,6,1,4,1,2076,13,5,1,2,1,45};
UINT4 PwPerfCurrentInHCPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,1};
UINT4 PwPerfCurrentInHCBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,2};
UINT4 PwPerfCurrentOutHCPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,3};
UINT4 PwPerfCurrentOutHCBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,4};
UINT4 PwPerfCurrentInPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,5};
UINT4 PwPerfCurrentInBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,6};
UINT4 PwPerfCurrentOutPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,7};
UINT4 PwPerfCurrentOutBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,3,1,8};
UINT4 PwPerfIntervalNumber [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,1};
UINT4 PwPerfIntervalValidData [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,2};
UINT4 PwPerfIntervalTimeElapsed [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,3};
UINT4 PwPerfIntervalInHCPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,4};
UINT4 PwPerfIntervalInHCBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,5};
UINT4 PwPerfIntervalOutHCPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,6};
UINT4 PwPerfIntervalOutHCBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,7};
UINT4 PwPerfIntervalInPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,8};
UINT4 PwPerfIntervalInBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,9};
UINT4 PwPerfIntervalOutPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,10};
UINT4 PwPerfIntervalOutBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,4,1,11};
UINT4 PwPerf1DayIntervalNumber [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,1};
UINT4 PwPerf1DayIntervalValidData [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,2};
UINT4 PwPerf1DayIntervalMoniSecs [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,3};
UINT4 PwPerf1DayIntervalInHCPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,4};
UINT4 PwPerf1DayIntervalInHCBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,5};
UINT4 PwPerf1DayIntervalOutHCPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,6};
UINT4 PwPerf1DayIntervalOutHCBytes [ ] ={1,3,6,1,4,1,2076,13,5,1,5,1,7};
UINT4 PwPerfTotalErrorPackets [ ] ={1,3,6,1,4,1,2076,13,5,1,6};
UINT4 PwIndexMappingPwType [ ] ={1,3,6,1,4,1,2076,13,5,1,7,1,1};
UINT4 PwIndexMappingPwID [ ] ={1,3,6,1,4,1,2076,13,5,1,7,1,2};
UINT4 PwIndexMappingPeerAddrType [ ] ={1,3,6,1,4,1,2076,13,5,1,7,1,3};
UINT4 PwIndexMappingPeerAddr [ ] ={1,3,6,1,4,1,2076,13,5,1,7,1,4};
UINT4 PwIndexMappingPwIndex [ ] ={1,3,6,1,4,1,2076,13,5,1,7,1,5};
UINT4 PwPeerMappingPeerAddrType [ ] ={1,3,6,1,4,1,2076,13,5,1,8,1,1};
UINT4 PwPeerMappingPeerAddr [ ] ={1,3,6,1,4,1,2076,13,5,1,8,1,2};
UINT4 PwPeerMappingPwType [ ] ={1,3,6,1,4,1,2076,13,5,1,8,1,3};
UINT4 PwPeerMappingPwID [ ] ={1,3,6,1,4,1,2076,13,5,1,8,1,4};
UINT4 PwPeerMappingPwIndex [ ] ={1,3,6,1,4,1,2076,13,5,1,8,1,5};
UINT4 PwUpDownNotifEnable [ ] ={1,3,6,1,4,1,2076,13,5,1,9};
UINT4 PwDeletedNotifEnable [ ] ={1,3,6,1,4,1,2076,13,5,1,10};
UINT4 PwNotifRate [ ] ={1,3,6,1,4,1,2076,13,5,1,11};


tMbDbEntry stdpwMibEntry[]= {

{{11,PwIndexNext}, NULL, PwIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,PwIndex}, GetNextIndexPwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwType}, GetNextIndexPwTable, PwTypeGet, PwTypeSet, PwTypeTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwOwner}, GetNextIndexPwTable, PwOwnerGet, PwOwnerSet, PwOwnerTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwPsnType}, GetNextIndexPwTable, PwPsnTypeGet, PwPsnTypeSet, PwPsnTypeTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwSetUpPriority}, GetNextIndexPwTable, PwSetUpPriorityGet, PwSetUpPrioritySet, PwSetUpPriorityTest, PwTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "0"},

{{13,PwHoldingPriority}, GetNextIndexPwTable, PwHoldingPriorityGet, PwHoldingPrioritySet, PwHoldingPriorityTest, PwTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "0"},

{{13,PwPeerAddrType}, GetNextIndexPwTable, PwPeerAddrTypeGet, PwPeerAddrTypeSet, PwPeerAddrTypeTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwPeerAddr}, GetNextIndexPwTable, PwPeerAddrGet, PwPeerAddrSet, PwPeerAddrTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwAttachedPwIndex}, GetNextIndexPwTable, PwAttachedPwIndexGet, PwAttachedPwIndexSet, PwAttachedPwIndexTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "0"},

{{13,PwIfIndex}, GetNextIndexPwTable, PwIfIndexGet, PwIfIndexSet, PwIfIndexTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "0"},

{{13,PwID}, GetNextIndexPwTable, PwIDGet, PwIDSet, PwIDTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwLocalGroupID}, GetNextIndexPwTable, PwLocalGroupIDGet, PwLocalGroupIDSet, PwLocalGroupIDTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwGroupAttachmentID}, GetNextIndexPwTable, PwGroupAttachmentIDGet, PwGroupAttachmentIDSet, PwGroupAttachmentIDTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwLocalAttachmentID}, GetNextIndexPwTable, PwLocalAttachmentIDGet, PwLocalAttachmentIDSet, PwLocalAttachmentIDTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwPeerAttachmentID}, GetNextIndexPwTable, PwPeerAttachmentIDGet, PwPeerAttachmentIDSet, PwPeerAttachmentIDTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwCwPreference}, GetNextIndexPwTable, PwCwPreferenceGet, PwCwPreferenceSet, PwCwPreferenceTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "2"},

{{13,PwLocalIfMtu}, GetNextIndexPwTable, PwLocalIfMtuGet, PwLocalIfMtuSet, PwLocalIfMtuTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "0"},

{{13,PwLocalIfString}, GetNextIndexPwTable, PwLocalIfStringGet, PwLocalIfStringSet, PwLocalIfStringTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "2"},

{{13,PwLocalCapabAdvert}, GetNextIndexPwTable, PwLocalCapabAdvertGet, PwLocalCapabAdvertSet, PwLocalCapabAdvertTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRemoteGroupID}, GetNextIndexPwTable, PwRemoteGroupIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwCwStatus}, GetNextIndexPwTable, PwCwStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRemoteIfMtu}, GetNextIndexPwTable, PwRemoteIfMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRemoteIfString}, GetNextIndexPwTable, PwRemoteIfStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRemoteCapabilities}, GetNextIndexPwTable, PwRemoteCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwFragmentCfgSize}, GetNextIndexPwTable, PwFragmentCfgSizeGet, PwFragmentCfgSizeSet, PwFragmentCfgSizeTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "0"},

{{13,PwRmtFragCapability}, GetNextIndexPwTable, PwRmtFragCapabilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwFcsRetentioncfg}, GetNextIndexPwTable, PwFcsRetentioncfgGet, PwFcsRetentioncfgSet, PwFcsRetentioncfgTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, "1"},

{{13,PwFcsRetentionStatus}, GetNextIndexPwTable, PwFcsRetentionStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwOutboundLabel}, GetNextIndexPwTable, PwOutboundLabelGet, PwOutboundLabelSet, PwOutboundLabelTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwInboundLabel}, GetNextIndexPwTable, PwInboundLabelGet, PwInboundLabelSet, PwInboundLabelTest, PwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwName}, GetNextIndexPwTable, PwNameGet, PwNameSet, PwNameTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwDescr}, GetNextIndexPwTable, PwDescrGet, PwDescrSet, PwDescrTest, PwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwCreateTime}, GetNextIndexPwTable, PwCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwUpTime}, GetNextIndexPwTable, PwUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwLastChange}, GetNextIndexPwTable, PwLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwAdminStatus}, GetNextIndexPwTable, PwAdminStatusGet, PwAdminStatusSet, PwAdminStatusTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwOperStatus}, GetNextIndexPwTable, PwOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwLocalStatus}, GetNextIndexPwTable, PwLocalStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRemoteStatusCapable}, GetNextIndexPwTable, PwRemoteStatusCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRemoteStatus}, GetNextIndexPwTable, PwRemoteStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwTimeElapsed}, GetNextIndexPwTable, PwTimeElapsedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwValidIntervals}, GetNextIndexPwTable, PwValidIntervalsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwRowStatus}, GetNextIndexPwTable, PwRowStatusGet, PwRowStatusSet, PwRowStatusTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 1, NULL},

{{13,PwStorageType}, GetNextIndexPwTable, PwStorageTypeGet, PwStorageTypeSet, PwStorageTypeTest, PwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PwTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentInHCPackets}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentInHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentInHCBytes}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentInHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentOutHCPackets}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentOutHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentOutHCBytes}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentOutHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentInPackets}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentInPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentInBytes}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentInBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentOutPackets}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentOutPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfCurrentOutBytes}, GetNextIndexPwPerfCurrentTable, PwPerfCurrentOutBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfCurrentTableINDEX, 1, 0, 0, NULL},

{{13,PwPerfIntervalNumber}, GetNextIndexPwPerfIntervalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalValidData}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalValidDataGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalTimeElapsed}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalTimeElapsedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalInHCPackets}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalInHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalInHCBytes}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalInHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalOutHCPackets}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalOutHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalOutHCBytes}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalOutHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalInPackets}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalInPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalInBytes}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalInBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalOutPackets}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalOutPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerfIntervalOutBytes}, GetNextIndexPwPerfIntervalTable, PwPerfIntervalOutBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, PwPerfIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalNumber}, GetNextIndexPwPerf1DayIntervalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalValidData}, GetNextIndexPwPerf1DayIntervalTable, PwPerf1DayIntervalValidDataGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalMoniSecs}, GetNextIndexPwPerf1DayIntervalTable, PwPerf1DayIntervalMoniSecsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalInHCPackets}, GetNextIndexPwPerf1DayIntervalTable, PwPerf1DayIntervalInHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalInHCBytes}, GetNextIndexPwPerf1DayIntervalTable, PwPerf1DayIntervalInHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalOutHCPackets}, GetNextIndexPwPerf1DayIntervalTable, PwPerf1DayIntervalOutHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{13,PwPerf1DayIntervalOutHCBytes}, GetNextIndexPwPerf1DayIntervalTable, PwPerf1DayIntervalOutHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, PwPerf1DayIntervalTableINDEX, 2, 0, 0, NULL},

{{11,PwPerfTotalErrorPackets}, NULL, PwPerfTotalErrorPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,PwIndexMappingPwType}, GetNextIndexPwIndexMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PwIndexMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwIndexMappingPwID}, GetNextIndexPwIndexMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwIndexMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwIndexMappingPeerAddrType}, GetNextIndexPwIndexMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PwIndexMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwIndexMappingPeerAddr}, GetNextIndexPwIndexMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, PwIndexMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwIndexMappingPwIndex}, GetNextIndexPwIndexMappingTable, PwIndexMappingPwIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwIndexMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwPeerMappingPeerAddrType}, GetNextIndexPwPeerMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PwPeerMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwPeerMappingPeerAddr}, GetNextIndexPwPeerMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, PwPeerMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwPeerMappingPwType}, GetNextIndexPwPeerMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PwPeerMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwPeerMappingPwID}, GetNextIndexPwPeerMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, PwPeerMappingTableINDEX, 4, 0, 0, NULL},

{{13,PwPeerMappingPwIndex}, GetNextIndexPwPeerMappingTable, PwPeerMappingPwIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, PwPeerMappingTableINDEX, 4, 0, 0, NULL},

{{11,PwUpDownNotifEnable}, NULL, PwUpDownNotifEnableGet, PwUpDownNotifEnableSet, PwUpDownNotifEnableTest, PwUpDownNotifEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,PwDeletedNotifEnable}, NULL, PwDeletedNotifEnableGet, PwDeletedNotifEnableSet, PwDeletedNotifEnableTest, PwDeletedNotifEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,PwNotifRate}, NULL, PwNotifRateGet, PwNotifRateSet, PwNotifRateTest, PwNotifRateDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData stdpwEntry = { 85, stdpwMibEntry };
#endif /* _STDPWDB_H */

