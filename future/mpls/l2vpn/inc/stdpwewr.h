#ifndef _STDPWEWR_H
#define _STDPWEWR_H
INT4 GetNextIndexPwEnetTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDPWE(VOID);
INT4 PwEnetPwInstanceGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPwVlanGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetVlanModeGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPortVlanGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPortIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetVcIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPwVlanSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetVlanModeSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPortVlanSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPortIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetVcIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetPwVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetVlanModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetPortVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetPortIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetVcIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexPwEnetMplsPriMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwEnetMplsPriMappingGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PwEnetMplsPriMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexPwEnetStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 PwEnetStatsIllegalVlanGet(tSnmpIndex *, tRetVal *);
INT4 PwEnetStatsIllegalLengthGet(tSnmpIndex *, tRetVal *);
#endif /* _STDPWEWR_H */
