#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.8 2012/05/24 13:03:11 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 15/07/2003                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureRSVPTE       |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files

PROJECT_NAME        = FutureL2VPN
PROJECT_BASE_DIR    = ${BASE_DIR}/mpls/l2vpn
MPLS_INCL_DIR       = $(BASE_DIR)/mpls/mplsinc
PROJECT_SOURCE_DIR  = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR  = ${PROJECT_BASE_DIR}/obj
MPLS_FM_INCL_DIR    = $(BASE_DIR)/mpls/mplsrtr/inc
MPLS_CDM_DIR        = $(BASE_DIR)/mpls/mplsdb
MPLS_OAM_DIR        = $(BASE_DIR)/mpls/oam/inc
MPLS_NPAPI_INC_DIR  = $(BASE_DIR)/inc/npapi
MPLS_NPAPI_DIR      = $(BASE_DIR)/npapi
MPLS_INC_DIR        = $(BASE_DIR)/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/l2vpdefs.h \
                $(PROJECT_INCLUDE_DIR)/l2vpincs.h \
                $(PROJECT_INCLUDE_DIR)/l2vpif.h \
                $(PROJECT_INCLUDE_DIR)/l2vpmacs.h \
                $(PROJECT_INCLUDE_DIR)/l2vpdbg.h \
                $(PROJECT_INCLUDE_DIR)/l2vptdfs.h \
                $(PROJECT_INCLUDE_DIR)/mspw.h\
                $(PROJECT_INCLUDE_DIR)/fsl2vpdb.h\
                $(PROJECT_INCLUDE_DIR)/fsl2vpwr.h\
                $(PROJECT_INCLUDE_DIR)/fsl2vplw.h 

PROJECT_FINAL_INCLUDES_DIRS    = -I$(PROJECT_INCLUDE_DIR) \
                                 -I$(MPLS_INCL_DIR) \
                                 -I$(MPLS_FM_INCL_DIR) \
                                 -I$(MPLS_CDM_DIR) \
                                 -I$(MPLS_OAM_INC_DIR) \
                                 -I$(MPLS_NPAPI_INC_DIR) \
                                 $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES    = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES    = $(COMMON_DEPENDENCIES) \
                $(PROJECT_FINAL_INCLUDE_FILES) \
                $(MPLS_BASE_DIR)/make.h \
                $(MPLS_INCL_DIR)/mplssize.h \
                $(PROJECT_BASE_DIR)/Makefile \
                $(PROJECT_BASE_DIR)/make.h


