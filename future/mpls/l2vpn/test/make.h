# Copyright (C) 2010 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.2 2014/08/25 11:58:57 siva Exp $
include ../../../LR/make.h
include ../../../LR/make.rule
include ../make.h

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
MPLS_INCL_DIR           = $(BASE_DIR)/mpls/mplsinc
MPLS_DB_DIR             = $(BASE_DIR)/mpls/mplsdb
PROJECT_TEST_DIR        = ${PROJECT_BASE_DIR}/test
PROJECT_INCLUDE_DIR     = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR      = ${PROJECT_BASE_DIR}/obj
MPLS_L2VPN_DIR          = $(BASE_DIR)/mpls/l2vpn/inc
MPLS_RTR_DIR          = $(BASE_DIR)/mpls/mplsrtr/inc

L2VPN_TEST_BASE_DIR  = ${BASE_DIR}/mpls/l2vpn/test
L2VPN_TEST_SRC_DIR   = ${L2VPN_TEST_BASE_DIR}/src
L2VPN_TEST_INC_DIR   = ${L2VPN_TEST_BASE_DIR}/inc
L2VPN_TEST_OBJ_DIR   = ${L2VPN_TEST_BASE_DIR}/obj

L2VPN_TEST_INCLUDES  = -I$(L2VPN_TEST_INC_DIR) \
                     -I$(PROJECT_INCLUDE_DIR) \
                     -I$(MPLS_DB_DIR) \
                     -I$(MPLS_INCL_DIR) \
                     -I$(MPLS_L2VPN_DIR) \
                     -I$(MPLS_RTR_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h                  #
##########################################################################
