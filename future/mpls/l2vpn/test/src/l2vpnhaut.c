/********************************************************************
 *** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vpnhaut.c,v 1.1 2014/08/25 12:23:30 siva Exp $
 ***
 *** Description: MPLS API  UT Test cases.
 *** NOTE: This file should not be include in release packaging.
 **************************************************************************/

#include "l2vpincs.h"
#include "iss.h"
#include "../../../../rm/inc/rmtdfs.h"
#include "../../../../rm/inc/rmglob.h"
#include "../../../../rm/inc/rmmacs.h"

#define L2VPN_HA_UT_CASES 87

#define INVALID_MESSAGE      0 
#define L2VPN_MAX_BULK_SIZE  1600
#define ERROR_MESSAGE        0
extern UINT4  u4TotalPassCase;
extern UINT4  u4TotalFailCase;
extern tRmInfo gRmInfo;
extern tMemPoolCfg gtMemPoolCfg;

VOID CallL2VpnHaUTCases(tCliHandle CliHandle);
VOID L2VpnHaExecUnitTest(tCliHandle CliHandle, INT4 i4Count);
VOID L2VpnRmHandleRmMessageEvent (tL2VpnRmMsg *pRmL2VpnMsg);
VOID L2VpnRmHandleGoActiveEvent (VOID);
VOID L2VpnRmHandleGoStandbyEvent (VOID);
VOID L2VpnRmHandleStandbyUpEvent (tRmNodeInfo * pRmNode);
VOID L2VpnRmHandleStandbyDownEvent (tRmNodeInfo * pRmNode);
VOID L2VpnRmSendBulkReq (VOID);
VOID L2VpnRmHandleConfRestoreCompEvt(VOID);
VOID L2VpnRmInitBulkUpdateFlags (VOID);
VOID L2VpnRmProcessBulkReq (VOID);
VOID L2VpnRmProcessBulkTail (VOID);
VOID L2VpnRmSendBulkTail (VOID);
INT4 L2VpnProcessPeerEventType(UINT1 u1EventType);
INT4 L2VpnProcessRmEventType(UINT1 u1EventType);
INT4 L2VpnConstructRmMsg (UINT1 u1OpType);
INT4 L2VpnCallBackFun(UINT1 u1Event);
INT4 L2VpnBulkReq(UINT1 u1MsgType);
INT4 L2VpnPopulateHwEntry(VOID);
INT4 L2VpnSendBulkInfo(VOID);
INT4 L2VpnAddHwListToRB(VOID);
INT4 L2VpnTestNodeState(VOID);
INT4 L2VpnProcessOpType(VOID);

tL2VpnQMsg   *gpu1TmpMsg[MAX_L2VPN_Q_MSG];
tRmPktQMsg   *gpRmPktQMg[MAX_RM_PKTQ_MSG];

INT4 L2VpnPopulateHwEntry()
{
    tL2VpnPwHwList  L2VpnPwHwList;
    UINT1           u1OpType = ADD_PW;

    MEMSET(&L2VpnPwHwList, L2VPN_ZERO, sizeof(tL2VpnPwHwList));
    L2VpnPwHwList.u4VplsIndex   = 1;
    L2VpnPwHwList.u4PwIndex      = 1;
    L2VpnPwHwList.u4InLabel     = 100;
    L2VpnPwHwList.u4OutLabel    = 200;
    L2VpnPwHwList.PeerAddress.i4IpAddrType = 1000;
    L2VpnPwHwList.u4RemoteVEId =  500;
    L2VpnPwHwList.u4OutIfIndex =  10;
    L2VpnPwHwList.u4LspInLabel =  300;
    L2VpnPwHwList.u4InIfIndex =   20;
    L2VpnPwHwList.u1StaleStatus = 1;

    L2VpnRmSendPwVcHwListEntry (&L2VpnPwHwList, u1OpType);
    return L2VPN_SUCCESS;

}

INT4 L2VpnSendBulkInfo()
{
    tL2VpnPwHwList     HwList;
    INT4               i4Index = 0;
    INT4               i4VplsIndex = 1,i4PwIndex = 1;
    UINT1              u1HwStatus = 0;

    for(i4Index = 1 ;i4Index <= 30 ; i4Index++)
    {
        

        MEMSET (&HwList, 0, sizeof(tL2VpnPwHwList));


        HwList.u4VplsIndex = i4VplsIndex++;
        HwList.u4PwIndex = i4PwIndex++;
        HwList.u4InLabel = 100;
        HwList.u4OutLabel = 200;
        HwList.PeerAddress.i4IpAddrType= 100 ;
        HwList.u4RemoteVEId = 500;
        HwList.u4OutIfIndex = 10;
        HwList.u4LspInLabel = 300;
        HwList.u4InIfIndex = 20;
        HwList.u1NpapiStatus = 1;
        HwList.u1StaleStatus = 1;

        u1HwStatus = HwList.u1NpapiStatus;
        L2VpnPwHwListAddUpdate (&HwList, u1HwStatus);

    }

    L2VpnRmSendBulkPwVcInfo();
    return L2VPN_SUCCESS;

}

INT4 L2VpnMsgCountZero()
{
    UINT2 u2MaxPwVcEntries = L2VPN_ZERO ;
    INT4  i4Index = L2VPN_ZERO ;
    tL2VpnPwHwList HwList;
    INT4 i4VplsIndex = L2VPN_ZERO;
    INT4 i4PwIndex = L2VPN_ZERO;
    UINT1 u1HwStatus = L2VPN_ZERO;
 
    u2MaxPwVcEntries = (L2VPN_RM_SYN_MAX_SIZE - (L2VPN_RM_MSG_HDR_SIZE + sizeof(UINT2)))/sizeof(tL2VpnPwHwList); 
    for(i4Index = 0 ;i4Index <u2MaxPwVcEntries ; i4Index++)
    {
        MEMSET (&HwList, 0, sizeof(tL2VpnPwHwList));
        HwList.u4VplsIndex = i4VplsIndex++;
        HwList.u4PwIndex = i4PwIndex++;
        HwList.u4InLabel = 100;
        HwList.u4OutLabel = 200;
        HwList.PeerAddress.i4IpAddrType= 100 ;
        HwList.u4RemoteVEId = 500;
        HwList.u4OutIfIndex = 10;
        HwList.u4LspInLabel = 300;
        HwList.u4InIfIndex = 20;
        HwList.u1NpapiStatus = 1;
        HwList.u1StaleStatus = 1;
        u1HwStatus = HwList.u1NpapiStatus;
        L2VpnPwHwListDelete(&HwList, u1HwStatus);
       
    }
    for(i4Index = 0 ;i4Index <u2MaxPwVcEntries ; i4Index++)
    {
        MEMSET (&HwList, 0, sizeof(tL2VpnPwHwList));
        HwList.u4VplsIndex = i4VplsIndex++;
        HwList.u4PwIndex = i4PwIndex++;
        HwList.u4InLabel = 100;
        HwList.u4OutLabel = 200;
        HwList.PeerAddress.i4IpAddrType= 100 ;
        HwList.u4RemoteVEId = 500;
        HwList.u4OutIfIndex = 10;
        HwList.u4LspInLabel = 300;
        HwList.u4InIfIndex = 20;
        HwList.u1NpapiStatus = 1;
        HwList.u1StaleStatus = 1;
        u1HwStatus = HwList.u1NpapiStatus;
        L2VpnPwHwListDelete(&HwList, u1HwStatus);
       L2VpnHwListAdd(&HwList);

     }

    
    L2VpnRmSendBulkPwVcInfo();
    
    return L2VPN_SUCCESS;

}


INT4 L2VpnSendBulkInfoFailed()
{
    tL2VpnPwHwList     HwList;
    INT4               i4Index = 0;
    INT4               i4VplsIndex = 1,i4PwIndex = 1;
    UINT1              u1HwStatus = 0;

    for(i4Index = 1 ;i4Index <= 30 ; i4Index++)
    {


        MEMSET (&HwList, 0, sizeof(tL2VpnPwHwList));


        HwList.u4VplsIndex = i4VplsIndex++;
        HwList.u4PwIndex = i4PwIndex++;
        HwList.u4InLabel = 100;
        HwList.u4OutLabel = 200;
        HwList.PeerAddress.i4IpAddrType= 100 ;
        HwList.u4RemoteVEId = 500;
        HwList.u4OutIfIndex = 10;
        HwList.u4LspInLabel = 300;
        HwList.u4InIfIndex = 20;
        HwList.u1NpapiStatus = 1;
        HwList.u1StaleStatus = 1;

        u1HwStatus = HwList.u1NpapiStatus;
        L2VpnPwHwListAddUpdate (&HwList, u1HwStatus);

    }
    
    L2VpnMemAllocPool();
    L2VpnRmSendBulkPwVcInfo();
    L2VpnMemReleasePool();
    return L2VPN_SUCCESS;

}

INT4 L2VpnBulkCountFailed()
{
    tL2VpnPwHwList     HwList;
    INT4               i4Index = 0;
    INT4               i4VplsIndex = 1,i4PwIndex = 1;
    UINT1              u1HwStatus = 0;

    gRmInfo.u1NumPeers = 2;
     for(i4Index = 1 ;i4Index <= 30 ; i4Index++)
    {


        MEMSET (&HwList, 0, sizeof(tL2VpnPwHwList));


        HwList.u4VplsIndex = i4VplsIndex++;
        HwList.u4PwIndex = i4PwIndex++;
        HwList.u4InLabel = 100;
        HwList.u4OutLabel = 200;
        HwList.PeerAddress.i4IpAddrType= 100 ;
        HwList.u4RemoteVEId = 500;
        HwList.u4OutIfIndex = 10;
        HwList.u4LspInLabel = 300;
        HwList.u4InIfIndex = 20;
        HwList.u1NpapiStatus = 1;
        HwList.u1StaleStatus = 1;

         u1HwStatus = HwList.u1NpapiStatus;
         L2VpnPwHwListDelete(&HwList, u1HwStatus); 

    }

         MEMSET (&HwList, 0, sizeof(tL2VpnPwHwList));
       
        HwList.u4VplsIndex = 60;
        HwList.u4PwIndex = 60;
        HwList.u4InLabel = 100;
        HwList.u4OutLabel = 200;
        HwList.PeerAddress.i4IpAddrType= 100 ;
        HwList.u4RemoteVEId = 500;
        HwList.u4OutIfIndex = 10;
        HwList.u4LspInLabel = 300;
        HwList.u4InIfIndex = 20;
        HwList.u1NpapiStatus = 1;
        HwList.u1StaleStatus = 1;

        u1HwStatus = HwList.u1NpapiStatus;
        L2VpnHwListAdd(&HwList);
    L2VpnMemAllocPool();
    L2VpnRmSendBulkPwVcInfo();
    L2VpnMemReleasePool();
    return L2VPN_SUCCESS;


}

INT4 L2VpnReleaseMemPool()
{
    UINT4    u4Count = 0;
    for(u4Count = 1; u4Count <= MAX_L2VPN_Q_MSG; u4Count++)
    {
        if(MemReleaseMemBlock (L2VPN_Q_POOL_ID, gpu1TmpMsg[u4Count]) == OSIX_FAILURE)
        {
            break;
        }
    }
     return L2VPN_SUCCESS;
}

INT4 L2VpnMemAllocPool()
{

 UINT4    u4Count = 0;
 for(u4Count = 1; u4Count <= MAX_RM_PKTQ_MSG; u4Count++)
 {  
   gpRmPktQMg[u4Count] = (tRmPktQMsg *) MemAllocMemBlk (gRmInfo.RmPktQMsgPoolId);
   if(gpRmPktQMg[u4Count] == NULL)
   {
     break;
   }
 }
  return L2VPN_SUCCESS;
}

INT4 L2VpnMemReleasePool()
{
   UINT4    u4Count = 0; 
   for(u4Count = 1; u4Count <= MAX_RM_PKTQ_MSG; u4Count++)
   {
     if(MemReleaseMemBlock (gRmInfo.RmPktQMsgPoolId, gpRmPktQMg[u4Count]) == OSIX_FAILURE)
     {
            break;
     }
   }
   return L2VPN_SUCCESS;
}


INT4 L2VpnUtFreeMsgQueue()
{
    UINT4 u4Count  = 0;
    UINT1   *pu1TmpMsg = NULL;

    for(u4Count = 1; u4Count <= L2VPN_QDEPTH ; u4Count++)
    {
        
      OsixQueRecv (L2VPN_QID,  &pu1TmpMsg, OSIX_DEF_MSG_LEN,0);
    }
  
   return L2VPN_SUCCESS;
}

INT4 L2VpnConstructRmMsg (UINT1 u1OpType)
{
    tRmMsg        *pMsg = NULL;
    UINT4          u4Offset = L2VPN_ZERO;
    UINT2          u2MsgLength = L2VPN_ZERO;
    UINT2          u2RmMsgLength = L2VPN_ZERO;
    tL2VpnPwHwList L2VpnPwVcHwList;
    tL2VpnRmMsg   *pL2VpnRmMsg = NULL;
    tRmHdr         RmHdr;

    pL2VpnRmMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    MEMSET (pL2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));  
    MEMSET (&L2VpnPwVcHwList, L2VPN_ZERO, sizeof(tL2VpnPwHwList));
    MEMSET (&RmHdr, L2VPN_ZERO, sizeof(tRmHdr));
    RmHdr.u4SeqNo = 100;
    L2VpnPwVcHwList.u4VplsIndex = 10;
    L2VpnPwVcHwList.u4PwIndex = 20;
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;   
    u2MsgLength = sizeof (tL2VpnPwHwList);
    u2RmMsgLength = L2VPN_RM_MSG_HDR_SIZE + u2MsgLength + sizeof(UINT1);
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2RmMsgLength));
    pMsg = CRU_BUF_Allocate_MsgBufChain (u2RmMsgLength, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }

    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, L2VPN_RM_SYNC_HWLIST_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    /* Fill the Message  Length */
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);

    /* ADD or DELETE type */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1OpType);
    u4Offset = u4Offset + sizeof (UINT1);


    BUF_COPY_OVER_CHAIN(pMsg, &L2VpnPwVcHwList, u4Offset, sizeof(tL2VpnPwHwList));
    u4Offset = u4Offset + sizeof(tL2VpnPwHwList);
    CRU_BUF_Prepend_BufChain (pMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr));

    pL2VpnRmMsg->pBuff = pMsg;
    pL2VpnRmMsg->u2Length = RM_HDR_LENGTH + u2RmMsgLength;
    pL2VpnRmMsg->u1Event = RM_MESSAGE;
    /* L2VpnRmHandleRmMessageEvent(pL2VpnRmMsg); */
    L2VpnProcessRmEvent(pL2VpnRmMsg);
    MemReleaseMemBlock (L2VPN_Q_POOL_ID, pL2VpnRmMsg);

    return L2VPN_SUCCESS;     

}

INT4 L2VpnErrorRmLen()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    tL2VpnRmMsg   L2VpnRmMsg;

    MEMSET (&L2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1) L2VPN_RM_BULK_UPDT_REQ_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN ;
    L2VpnRmMsg.pBuff = pMsg;
    L2VpnRmMsg.u2Length = L2VPN_RM_MSG_HDR_SIZE ;
    L2VpnRmHandleRmMessageEvent(&L2VpnRmMsg);
    return L2VPN_SUCCESS;

}
INT4 L2VpnTestNodeState()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    
    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1) L2VPN_RM_BULK_UPDT_REQ_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
    L2VpnRmProcessSyncHwListEntry(pMsg, &u4Offset);
    return L2VPN_SUCCESS;
}

INT4 L2VpnAddUpdateType()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    UINT2    u2RmLen = L2VPN_ZERO;
    UINT1    u1HwStatus = L2VPN_ZERO; 
    tL2VpnPwHwList L2VpnPwVcHwList;
    u2RmLen = sizeof(UINT1) + sizeof(tL2VpnPwHwList);
    pMsg = CRU_BUF_Allocate_MsgBufChain (u2RmLen, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    MEMSET(&L2VpnPwVcHwList, 0, sizeof(tL2VpnPwHwList));
    L2VpnPwVcHwList.u4VplsIndex  = 50;
    L2VpnPwVcHwList.u4PwIndex    = 50;
    L2VpnPwVcHwList.u4InLabel = 100;
    L2VpnPwVcHwList.u4OutLabel = 200;
    L2VpnPwVcHwList.PeerAddress.i4IpAddrType= 100 ;
    L2VpnPwVcHwList.u4RemoteVEId = 500;
    L2VpnPwVcHwList.u4OutIfIndex = 10;
    L2VpnPwVcHwList.u4LspInLabel = 300;
    L2VpnPwVcHwList.u4InIfIndex = 20;
    L2VpnPwVcHwList.u1NpapiStatus = MPLS_PWVC_NPAPI_CALLED | MPLS_PWVC_NPAPI_SUCCESS | MPLS_PWAC_NPAPI_CALLED;
    L2VpnPwVcHwList.u1StaleStatus = 1;

    u1HwStatus = L2VpnPwVcHwList.u1NpapiStatus;
    L2VpnHwListAdd(&L2VpnPwVcHwList);
    L2VpnPwVcHwList.u1NpapiStatus=MPLS_PWILM_NPAPI_CALLED ;
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1) ADD_UPDATE_PW);
    u4Offset = u4Offset + sizeof (UINT1);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
    BUF_COPY_OVER_CHAIN(pMsg, &L2VpnPwVcHwList, u4Offset, sizeof(tL2VpnPwHwList));
    u4Offset = u4Offset + sizeof(tL2VpnPwHwList);
    u4Offset = L2VPN_ZERO;
    L2VpnRmProcessSyncHwListEntry(pMsg, &u4Offset);
    return L2VPN_SUCCESS;
}




INT4 L2VpnProcessOpType()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1)ADD_UPDATE_PW);
    u4Offset = u4Offset + sizeof (UINT1);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
    u4Offset = L2VPN_ZERO;
    L2VpnRmProcessSyncHwListEntry(pMsg, &u4Offset);
    return L2VPN_SUCCESS;
}



INT4 L2VpnCheckNodeState()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    tL2VpnRmMsg   L2VpnRmMsg ;

    
    MEMSET (&L2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1) L2VPN_RM_BULK_UPDT_REQ_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN ;
    L2VpnRmMsg.pBuff = pMsg;
    L2VpnRmMsg.u2Length = RM_HDR_LENGTH + L2VPN_RM_MSG_HDR_SIZE ;
    L2VpnRmHandleRmMessageEvent(&L2VpnRmMsg);
    return L2VPN_SUCCESS;

}

INT4 L2VpnCheckNodeStateDown()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    tL2VpnRmMsg   *pL2VpnRmMsg = NULL;

    pL2VpnRmMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    MEMSET (pL2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,100);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
    pL2VpnRmMsg->pBuff = pMsg;
    pL2VpnRmMsg->u2Length = RM_HDR_LENGTH + L2VPN_RM_MSG_HDR_SIZE ;
    L2VpnRmHandleRmMessageEvent(pL2VpnRmMsg);
    MemReleaseMemBlock (L2VPN_Q_POOL_ID, pL2VpnRmMsg);
    return L2VPN_SUCCESS;

}

INT4 L2VpnCheckNodeStateUp()
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    tL2VpnRmMsg   L2VpnRmMsg ;


    MEMSET (&L2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1) L2VPN_RM_BULK_UPDT_REQ_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP ;
    L2VpnRmMsg.pBuff = pMsg;
    L2VpnRmMsg.u2Length = RM_HDR_LENGTH + L2VPN_RM_MSG_HDR_SIZE ;
    L2VpnRmHandleRmMessageEvent(&L2VpnRmMsg);
    return L2VPN_SUCCESS;

}

INT4 L2VpnBulkReq(UINT1 u1MsgType)
{
    tRmMsg  *pMsg = NULL;
    UINT4    u4Offset = L2VPN_ZERO;
    tL2VpnRmMsg   *pL2VpnRmMsg = NULL;

    pL2VpnRmMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    MEMSET (pL2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);    
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1MsgType);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP ;   
    pL2VpnRmMsg->pBuff = pMsg;
    pL2VpnRmMsg->u2Length = RM_HDR_LENGTH + L2VPN_RM_MSG_HDR_SIZE ;
    L2VpnRmHandleRmMessageEvent(pL2VpnRmMsg);
    MemReleaseMemBlock (L2VPN_Q_POOL_ID, pL2VpnRmMsg);
    
    return L2VPN_SUCCESS;


}

INT4 L2VpnCallBackFun(UINT1 u1Event)
{

    tRmMsg      *pMsg = NULL;
    UINT4       u4Offset = L2VPN_ZERO;

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Event);
    u4Offset = u4Offset + sizeof (UINT1);
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    u4Offset = u4Offset + sizeof (UINT2);
    L2VpnRmEventHandler(u1Event, pMsg, (UINT2)L2VPN_RM_MSG_HDR_SIZE);
    return L2VPN_SUCCESS;  

}

INT4 L2VpnAddHwListToRB()
{

    tRmMsg         *pMsg = NULL;
    tL2VpnRmMsg    *pL2VpnRmMsg = NULL;
    tL2VpnPwHwList  L2VpnHwListEntry;
    tRmHdr          RmHdr;
    UINT2           u2Count = 20;
    UINT1           u1Range = L2VPN_ZERO ;
    UINT2           u2MsgLength = L2VPN_ZERO;
    UINT2           u2RmMsgLength = L2VPN_ZERO;
    INT4            i4VplsIndex = 1, i4PwIndex = 1;
    UINT4           u4Offset = L2VPN_ZERO ;

    pL2VpnRmMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    MEMSET (pL2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));
    MEMSET (&RmHdr, 0,sizeof(tRmHdr));
    u2MsgLength = sizeof (tL2VpnPwHwList)*u2Count;
    u2RmMsgLength = L2VPN_RM_MSG_HDR_SIZE + u2MsgLength + sizeof(UINT2);

    RmHdr.u4SeqNo = 100;
    RmHdr.u4TotLen = OSIX_HTONL ((RM_HDR_LENGTH + u2RmMsgLength));
    pMsg = CRU_BUF_Allocate_MsgBufChain (u2RmMsgLength, L2VPN_ZERO);
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }
    L2VPN_RM_PUT_1_BYTE(pMsg, u4Offset, L2VPN_RM_BULK_UPDATE_MSG);
    L2VPN_RM_PUT_2_BYTE(pMsg, u4Offset, u2RmMsgLength);
    L2VPN_RM_PUT_2_BYTE(pMsg, u4Offset, u2Count);
    for(u1Range = L2VPN_ZERO; u1Range < u2Count; u1Range++)
    {


        MEMSET (&L2VpnHwListEntry, 0, sizeof(tL2VpnPwHwList));

        L2VpnHwListEntry.u4VplsIndex = i4VplsIndex++;
        L2VpnHwListEntry.u4PwIndex = i4PwIndex++;
        L2VpnHwListEntry.u4InLabel = 100;
        L2VpnHwListEntry.u4OutLabel = 200;
        L2VpnHwListEntry.PeerAddress.i4IpAddrType= 100 ;
        L2VpnHwListEntry.u4RemoteVEId = 500;
        L2VpnHwListEntry.u4OutIfIndex = 10;
        L2VpnHwListEntry.u4LspInLabel = 300;
        L2VpnHwListEntry.u4InIfIndex = 20;
        L2VpnHwListEntry.u1NpapiStatus = 1;
        L2VpnHwListEntry.u1StaleStatus = 1;

        BUF_COPY_OVER_CHAIN(pMsg, &L2VpnHwListEntry, u4Offset, sizeof(tL2VpnPwHwList));
        u4Offset = u4Offset + sizeof(tL2VpnPwHwList);     
    }

    CRU_BUF_Prepend_BufChain (pMsg, (UINT1 *) &RmHdr, sizeof (tRmHdr));    

    pL2VpnRmMsg->pBuff = pMsg;
    pL2VpnRmMsg->u2Length = RM_HDR_LENGTH + u2RmMsgLength;
    pL2VpnRmMsg->u1Event = RM_MESSAGE;
    L2VpnRmHandleRmMessageEvent(pL2VpnRmMsg);
    MemReleaseMemBlock (L2VPN_Q_POOL_ID, pL2VpnRmMsg);
    return L2VPN_SUCCESS;

}

INT4 L2VpnProcessRmEventType(UINT1 u1EventType)
{
    tL2VpnRmMsg *pL2VpnRmMsg = NULL;

    pL2VpnRmMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    MEMSET (pL2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));
    gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = 1;

    pL2VpnRmMsg->u1Event = u1EventType;
    L2VpnProcessRmEvent(pL2VpnRmMsg);
    MemReleaseMemBlock (L2VPN_Q_POOL_ID, pL2VpnRmMsg);
    return  L2VPN_SUCCESS;
}

INT4 L2VpnProcessPeerEventType(UINT1 u1EventType)
{

    tL2VpnRmMsg      *pL2VpnRmMsg = NULL ;
    tRmMsg           *pMsg = NULL;

    pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_BULK_TAIL_MSG_LEN, L2VPN_ZERO);
    pL2VpnRmMsg  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
    MEMSET (pL2VpnRmMsg, 0, sizeof(tL2VpnRmMsg));
    if(pMsg == NULL)
    {
        L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
        return L2VPN_FAILURE;
    }

    pL2VpnRmMsg->u1Event = u1EventType;
    pL2VpnRmMsg->pBuff = pMsg;
    L2VpnProcessRmEvent(pL2VpnRmMsg);
    MemReleaseMemBlock (L2VPN_Q_POOL_ID, pL2VpnRmMsg);
    return L2VPN_SUCCESS;

}

VOID CallL2VpnHaUTCases(tCliHandle CliHandle)
{
    INT4                i4Count = 1;

    for (i4Count = 1; i4Count <= L2VPN_HA_UT_CASES; i4Count++)
    {
        L2VpnHaExecUnitTest(CliHandle, i4Count);
    }
}

VOID L2VpnHaExecUnitTest(tCliHandle CliHandle, INT4 i4UtId)
{
    INT4                i4RetVal = L2VPN_FAILURE;

    switch (i4UtId)
    {
        case 1:
            {
                /* To check dynamic sync to standby node and add Hw list 
                   entry to RBTree */
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
                if(L2VpnPopulateHwEntry()== L2VPN_SUCCESS)
                {
                    i4RetVal = L2VPN_SUCCESS;
                }

            }
            break;      

        case 2:
            {
                /* Rainy Day - check INIT node state */
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT;     
                if(L2VpnPopulateHwEntry() == L2VPN_SUCCESS)
                {
                    i4RetVal = L2VPN_SUCCESS;
                }
            }
            break;

        case 3:
            {
                /* Rainy Day - check standby node state */
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN;
                if(L2VpnPopulateHwEntry() == L2VPN_SUCCESS)
                {
                    i4RetVal = L2VPN_SUCCESS; 
                }
            }
            break;

        case 4:
            {
                /* Rainy Day - check Null condition*/
                tL2VpnPwHwList *pPwVcHwList = NULL;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
                L2VpnRmSendPwVcHwListEntry (pPwVcHwList, ADD_PW);                      
                i4RetVal = L2VPN_SUCCESS; 
            }
            break;
        case 5:
            {
                /* Rainy Day - send message to RM failed */   
                tRmMsg  *pMsg = NULL;  
                UINT4    u4Offset = L2VPN_ZERO;

                pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
                L2VPN_RM_PUT_1_BYTE (pMsg, u4Offset, L2VPN_RM_SYNC_HWLIST_MSG);
                L2VPN_RM_PUT_2_BYTE (pMsg, u4Offset, L2VPN_MAX_BULK_SIZE); 
                if(L2VpnRmSendMsgToRm (pMsg, L2VPN_MAX_BULK_SIZE) != L2VPN_SUCCESS)
                {
                    i4RetVal = L2VPN_SUCCESS;
                }
            }
            break;
        case 6:
            {
                /* Sunny Day - Add Hw list entry to RBTree */

                i4RetVal = L2VpnConstructRmMsg (ADD_PW);
                if(i4RetVal != L2VPN_FAILURE)
                {
                    i4RetVal = L2VPN_SUCCESS;
                }

            }


            break;                    

        case 7:
            { /* Sunny Day - Add or Update Hw list entry to RBTree */

                i4RetVal =  L2VpnConstructRmMsg (ADD_UPDATE_PW);
                if(i4RetVal != L2VPN_FAILURE)
                {
                    i4RetVal = L2VPN_SUCCESS;
                }
            }
            break;
        case 8:
            {
                /* Sunny Day - Delete Hw list entry to RBTree */ 
                i4RetVal = L2VpnConstructRmMsg (DEL_PW);
                if(i4RetVal != L2VPN_FAILURE)
                {
                    i4RetVal = L2VPN_SUCCESS ;
                }
            }
            break;
        case 9:
            {
                /* Sunny Day - Send Bulk sync message to RM*/
                i4RetVal = L2VpnSendBulkInfo();
                if( i4RetVal != L2VPN_FAILURE)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }
            }
            break;
        case 10:
            {   /* sunny day - Add HW entry to RBTree at standby side*/
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY ; 
                if (L2VpnAddHwListToRB() == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }            
            }
            break;
        case 11:
            {
                /* sunny day - To check GO_ACTIVE event */
                UINT1 u1EventType = GO_ACTIVE ;
                gL2VpnGlobalInfo.l2vpRmInfo.u4PeerCount = 1;

                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }          

            }
            break;
        case 12:
            {
                /* Rainy day - Check peer node state down*/
                UINT1 u1EventType = GO_ACTIVE;
                if(L2VpnProcessRmEventType (u1EventType)== L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 13:
            {
                /* Rainy Day - Check INIT state */
                UINT1 u1EventType = GO_ACTIVE;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT ;

                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 14:
            {
                /* Sunny Day - check standby state */
                UINT1 u1EventType = GO_ACTIVE;              
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 15:
            {
                /* Rainy Day - Received GO_STANDBY event when the node is INIT */
                UINT1 u1EventType = GO_STANDBY;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT;
                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 16:
            {
                /* Sunny Day - Received GO_STANDBY event when the node is standby */
                UINT1 u1EventType = GO_STANDBY;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 17:
            {
                /* Sunny Day - Received RM_CONFIG_RESTORE_COMPLETE event */

                UINT1 u1EventType = RM_CONFIG_RESTORE_COMPLETE;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT ;
                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 18:
            {
                /* Sunny Day - Received RM_PEER_UP event */
                UINT1 u1EventType = RM_PEER_UP;
                if(L2VpnProcessPeerEventType(u1EventType)== L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }
            }
            break;
        case 19:
            { 
                /* Sunny Day - Recieved RM_PEER_DOWN event */
                UINT1 u1EventType = RM_PEER_DOWN ;
                if(L2VpnProcessPeerEventType(u1EventType)== L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 20:
            {
                tRmNodeInfo  RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                RmNode.u1NumStandby = 1;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState  = L2VPN_RM_ACTIVE_STANDBY_DOWN;
                gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd = L2VPN_RM_TRUE ;
                L2VpnRmHandleStandbyUpEvent(&RmNode);
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 21:
            {
                tRmNodeInfo  RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
                L2VpnRmHandleStandbyUpEvent(&RmNode);
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 22:
            {
                tRmNodeInfo  RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
                L2VpnRmHandleStandbyDownEvent(&RmNode);
                i4RetVal =  L2VPN_SUCCESS;               

            }
            break;
        case 23:
            {
                tRmNodeInfo  RmNode ;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                RmNode.u1NumStandby = 1;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN;
                L2VpnRmHandleStandbyDownEvent(&RmNode);
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 24:
            {
                UINT1 u1EventType = L2_INITIATE_BULK_UPDATES;
                if(L2VpnProcessRmEventType (u1EventType) == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }
            }
            break;
        case 25:
            {
                UINT1 u1MsgType = L2VPN_RM_BULK_UPDT_REQ_MSG;
                if(L2VpnBulkReq(u1MsgType) == L2VPN_SUCCESS)
                {

                    i4RetVal =  L2VPN_SUCCESS; 
                }    

            }
            break;
        case 26:
            {
                i4RetVal = L2VpnBulkReq ((UINT1) L2VPN_RM_BULK_UPDT_TAIL_MSG) ;
                if(i4RetVal == L2VPN_SUCCESS)
                {

                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 27:
            {
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
                L2VpnRmProcessBulkReq ();
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 28:
            {
                L2VpnRmSendBulkTail();
                i4RetVal =  L2VPN_SUCCESS;    

            }
            break;

        case 29:
            {
                L2VpnRmProcessBulkTail();
                i4RetVal =  L2VPN_SUCCESS;  

            }
            break;
        case 30:
            {
                L2VpnRmSendBulkReq();
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 31:
            {
                L2VpnRmSendBulkAbort(RM_MEMALLOC_FAIL);
                i4RetVal =  L2VPN_SUCCESS; 

            }
            break;
        case 32:
            {
                L2VpnRmInit();
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 33:
            {
                tRmNodeInfo RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                RmNode.u1NumStandby = 1;
                gL2VpnGlobalInfo.l2vpRmInfo.b1IsBulkReqRcvd = L2VPN_RM_TRUE;
                L2VpnRmHandleStandbyUpEvent (&RmNode);
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 34:
            {
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
                tRmNodeInfo RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                L2VpnRmHandleStandbyUpEvent (&RmNode);
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 35:
            {
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
                tRmNodeInfo RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));

                L2VpnRmHandleStandbyDownEvent(&RmNode);
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 36:
            {
                tRmNodeInfo RmNode;
                MEMSET(&RmNode, 0, sizeof(tRmNodeInfo));
                RmNode.u1NumStandby = 1;
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
                L2VpnRmHandleStandbyDownEvent(&RmNode);
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;   
        case 37:
            {
                i4RetVal = L2VpnCallBackFun((UINT1) RM_MESSAGE);                  
                if(i4RetVal == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 38:
            {

                i4RetVal = L2VpnCallBackFun((UINT1)INVALID_MESSAGE);
                if(i4RetVal == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 39:
            {
                tRmMsg* pData = NULL;
                UINT1    u1Event = RM_MESSAGE;
                UINT2 u2DataLen = 1;
                L2VpnRmEventHandler(u1Event, pData, u2DataLen);
                i4RetVal =  L2VPN_SUCCESS;

            } 
            break;
        case 40:
            {
                tRmMsg  Rmdata;
                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                UINT1    u1Event = RM_MESSAGE;
                UINT2 u2DataLen = 1;
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 41:
            {
                tRmMsg  Rmdata;
                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                UINT1    u1Event = RM_PEER_UP;
                UINT2 u2DataLen = 4;
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 42:
            {
                i4RetVal = L2VpnRmRegisterWithRM();
                if(i4RetVal == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;
        case 43:
            {
                i4RetVal = L2VpnRmDeRegisterWithRM();
                if(i4RetVal == L2VPN_SUCCESS)
                {
                    i4RetVal =  L2VPN_SUCCESS;
                }

            }
            break;   
        case 44:
            {
                UINT4 u4SeqNo = 100;
                L2VpnRmApiSendProtoAckToRM(u4SeqNo);
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;    
        case 45:
            {
                L2VpnRmGetStandbyNodeCount();
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 46:
            {
                L2VpnRmGetRmNodeState();
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 47:
            {
                UINT2   u2Size = 10;
                L2VpnRmAllocForRmMsg (u2Size);
                i4RetVal =  L2VPN_SUCCESS;
            }
            break;
        case 48:
            {
                tRmMsg   Rmdata;
                UINT1    u1Event = RM_MESSAGE;
                UINT4    u4Index = 0;
                UINT2    u2DataLen = 4;

                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                for(u4Index = 1; u4Index <= MAX_L2VPN_Q_MSG ; u4Index++)
                {
                    gpu1TmpMsg[u4Index]  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
                    if(gpu1TmpMsg[u4Index] == NULL)
                    {
                        break;
                    }
                }

                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                L2VpnReleaseMemPool();
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 49:
            {
                tRmMsg   Rmdata;
                UINT1    u1Event = RM_PEER_UP;
                UINT4    u4Index = 0;
                UINT2    u2DataLen = 1;

                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                for(u4Index = 1; u4Index <= MAX_L2VPN_Q_MSG ; u4Index++)
                {
                    gpu1TmpMsg[u4Index]  = (tL2VpnQMsg *) MemAllocMemBlk (L2VPN_Q_POOL_ID);
                    if(gpu1TmpMsg[u4Index] == NULL)
                    {
                        break;
                    }
                }

                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                L2VpnReleaseMemPool();
                
                i4RetVal =  L2VPN_SUCCESS;

            }
            break;
        case 50:
            {
                tRmMsg   Rmdata;
                UINT1    u1Event = RM_MESSAGE;
                UINT4    u4Count = 0;
                UINT2    u2DataLen = 10;
                UINT1   *pu1TmpMsg = NULL;

                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L2VPN_Q_POOL_ID);
                for(u4Count = 1; u4Count <= L2VPN_QDEPTH; u4Count++)
                {
                    OsixQueSend (L2VPN_QID, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN);
                }
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                L2VpnUtFreeMsgQueue(); 
                MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);
                i4RetVal =  L2VPN_SUCCESS;
            }
            
            break;
       case 51:
             {
                gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT;
                L2VpnRmProcessBulkReq ();
                i4RetVal =  L2VPN_SUCCESS;
             }
             break;
      case 52:
            {
              gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN;
              L2VpnRmProcessBulkReq ();
              i4RetVal =  L2VPN_SUCCESS;

            }
            break;
      case 53:
            {
              L2VpnRmSendBulkAbort(ERROR_MESSAGE);
              i4RetVal =  L2VPN_SUCCESS;

            }
            break;
     case 54:
            {
              tL2VpnRmMsg  RmL2VpnMsg;
              MEMSET(&RmL2VpnMsg, 0, sizeof(tL2VpnRmMsg));
              RmL2VpnMsg.u1Event = 100;
              L2VpnProcessRmEvent(&RmL2VpnMsg); 
              i4RetVal =  L2VPN_SUCCESS; 
            }
            break;
     case 55:
           {
               gRmInfo.u4NodeState = 2;

               gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_INIT;

               L2VpnRmHandleConfRestoreCompEvt();
               i4RetVal =  L2VPN_SUCCESS;
           }
           break;
    case 56:
          {
              gRmInfo.u1NumPeers = 2;

              L2VpnRmHandleGoActiveEvent();
              i4RetVal =  L2VPN_SUCCESS;
          }
          break;
    case 57:
           {
             L2VpnErrorRmLen();
             i4RetVal =  L2VPN_SUCCESS;
           }
           break;
    case 58:
          {
            L2VpnCheckNodeState();    
            i4RetVal =  L2VPN_SUCCESS;        

          }
          break;
    case 59:
          {
             L2VpnCheckNodeStateDown();
             i4RetVal =  L2VPN_SUCCESS;
          }
          break;
    case 60:
         {
            L2VpnMemAllocPool();
            L2VpnRmSendBulkTail();
            L2VpnMemReleasePool();   
            i4RetVal = L2VPN_SUCCESS;         
         }
         break;
    case 61:
         {
          L2VpnCheckNodeStateUp();
          i4RetVal = L2VPN_SUCCESS;

         }
         break;
    case 62:
         { 
           gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;          
           L2VpnRmHandleGoStandbyEvent();
           i4RetVal = L2VPN_SUCCESS;
         }
         break;
    case 63:
         {
           gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_DOWN;
           L2VpnRmHandleGoStandbyEvent();
           i4RetVal = L2VPN_SUCCESS;

         }
         break;
     case 64:
          {
               L2VpnMemAllocPool();
               L2VpnRmSendBulkReq();
               L2VpnMemReleasePool();
               i4RetVal = L2VPN_SUCCESS;
          }
          break;
     case 65:
          {
              gRmInfo.bIsTxBuffFull = RM_TRUE;
              L2VpnRmSendBulkReq();
              i4RetVal = L2VPN_SUCCESS;
          } 
          break;
     case 66:
          {
                tRmMsg   Rmdata;
                UINT1    u1Event = RM_PEER_UP;
                UINT4    u4Count = 0;
                UINT2    u2DataLen = 1;
                UINT1   *pu1TmpMsg = NULL;

                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L2VPN_Q_POOL_ID);
                for(u4Count = 1; u4Count <= L2VPN_QDEPTH; u4Count++)
                {
                    OsixQueSend (L2VPN_QID, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN);
                }
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                L2VpnUtFreeMsgQueue();
                MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);
                i4RetVal =  L2VPN_SUCCESS;
          }
          break;
      case 67:
            {
                UINT4    u4Count = 0;
                
                for(u4Count = 1; u4Count <= MAX_RM_APPS; u4Count++)
                {
                  if(MemAllocateMemBlock(gRmInfo.RmRegMemPoolId, (UINT1 **) &(gRmInfo.RmAppInfo[u4Count])) != MEM_SUCCESS)
                  {
                     break;
                  }
                } 

                   L2VpnRmRegisterWithRM ();
                for(u4Count = 1; u4Count <= MAX_RM_APPS; u4Count++)
                {
                    if(MemReleaseMemBlock(gRmInfo.RmRegMemPoolId,(UINT1 *) gRmInfo.RmAppInfo[u4Count]) != MEM_SUCCESS)
                    {
                      break;
                    }
                }
               
                  i4RetVal =  L2VPN_SUCCESS;
           }
           break;
     case 68:
           {
              UINT1 *pu1Block = NULL;
              MemAllocateMemBlock(gRmInfo.RmMsgPoolId,&pu1Block);
              
              
              L2VpnRmRelRmMsgMem (pu1Block);
              i4RetVal =  L2VPN_SUCCESS;               
           }
           break;
    case 69:
           { 
                gRmInfo.RmAppInfo[RM_L2VPN_APP_ID] = NULL; 
                L2VpnRmDeRegisterWithRM();
                i4RetVal =  L2VPN_SUCCESS;
                L2VpnRmRegisterWithRM();
           }    
           break;
     case 70:
          {
                 gRmInfo.RmTaskId = RM_INIT;
                 tRmProtoEvt Evt;
                 MEMSET(&Evt, 0, sizeof(tRmProtoEvt));
                 Evt.u4Event = RM_PROTOCOL_SEND_EVENT;
                 L2VpnRmSendEventToRm(&Evt);
                 i4RetVal =  L2VPN_SUCCESS;
          }
          break;
    case 71:
         {
                tRmMsg   Rmdata;
                UINT1    u1Event = RM_PEER_DOWN;
                UINT4    u4Count = 0;
                UINT2    u2DataLen = 1;
                UINT1   *pu1TmpMsg = NULL;

                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L2VPN_Q_POOL_ID);
                for(u4Count = 1; u4Count <= L2VPN_QDEPTH; u4Count++)
                {
                    OsixQueSend (L2VPN_QID, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN);
                }
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                L2VpnUtFreeMsgQueue();
                MemReleaseMemBlock (L2VPN_Q_POOL_ID, pu1TmpMsg);
                i4RetVal =  L2VPN_SUCCESS;
         }
         break;
    case 72:
         {
 
                tRmMsg  Rmdata;
                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                UINT1    u1Event = L2_INITIATE_BULK_UPDATES;
                UINT2 u2DataLen = 1;
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                i4RetVal =  L2VPN_SUCCESS;
         }
         break;
     case 73:
         {
                tRmMsg  Rmdata;
                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                UINT1    u1Event = GO_STANDBY;
                UINT2 u2DataLen = 1;
                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                i4RetVal =  L2VPN_SUCCESS;
         }
         break;
    case 74:
        {
             /*   tRmMsg   Rmdata;
                UINT1    u1Event = RM_PEER_DOWN;
                UINT4    u4Index = 0;
                UINT2    u2DataLen = 1;

                MEMSET(&Rmdata, 0, sizeof(tRmMsg));
                
                for(u4Index = 1; u4Index <= MAX_L2VPN_Q_MSG ; u4Index++)
                {
                    gpu1TmpMsg[u4Index]  = (tL2VpnQMsg *) MemAllocMemBlk (L2VPN_Q_POOL_ID);
                    if(gpu1TmpMsg[u4Index] == NULL)
                    {
                        break;
                    }
                }

                L2VpnRmEventHandler(u1Event, &Rmdata, u2DataLen);
                L2VpnReleaseMemPool();

                i4RetVal =  L2VPN_SUCCESS;*/
        }
        break;
    case 75:
          {
              L2VpnTestNodeState();
              i4RetVal =  L2VPN_SUCCESS;
          }
          break;
    case 76:
          {
            gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
            L2VpnProcessOpType();
            i4RetVal =  L2VPN_SUCCESS;

          }
          break;        
    case 77:
          {
            gRmInfo.bIsTxBuffFull = RM_TRUE;
            L2VpnRmSendBulkTail();
            i4RetVal =  L2VPN_SUCCESS;
          }
          break;
    case 78:
          {
              tRmMsg  *pMsg = NULL;
              UINT4    u4Offset = L2VPN_ZERO;

              pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
              if(pMsg == NULL)
              {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
                i4RetVal = L2VPN_FAILURE;
              }
              RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1)DEL_PW);
              u4Offset = u4Offset + sizeof (UINT1);
              gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
              u4Offset = L2VPN_ZERO;
              L2VpnRmProcessSyncHwListEntry(pMsg, &u4Offset);
              i4RetVal =  L2VPN_SUCCESS;
              
          } 
          break;
     case 79:
            {
              tRmMsg  *pMsg = NULL;
              UINT4    u4Offset = L2VPN_ZERO;

              pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);
              if(pMsg == NULL)
              {
                L2VPN_DBG (L2VPN_DBG_LVL_DBG_FLAG, "Memory Allocation Failed\n");
                i4RetVal= L2VPN_FAILURE;
              }
              RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset,(UINT1)INVALID_MESSAGE);
              u4Offset = u4Offset + sizeof (UINT1);
              gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_STANDBY;
              u4Offset = L2VPN_ZERO;
              L2VpnRmProcessSyncHwListEntry(pMsg, &u4Offset);
              i4RetVal =  L2VPN_SUCCESS;

            }
            break;
      case 80:
            {
              L2VpnAddUpdateType();
              i4RetVal =  L2VPN_SUCCESS;
            }
           break;
      case 81:
             {
                
               tRmMsg  *pMsg = NULL;
               UINT4    u4Offset = L2VPN_ZERO;
               UINT2    u2Count = L2VPN_ZERO;
               gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
               u4Offset = L2VPN_ZERO;
               L2VpnRmProcessBulkUpdateMsg(pMsg, &u4Offset);
               i4RetVal =  L2VPN_SUCCESS;

             }
             break;
     case 82:
           {
                  tL2VpnPwHwList  L2VpnPwHwList;
                  UINT1           u1OpType = ADD_PW;
                  UINT4           u4Index = L2VPN_ZERO;
                  
                  gL2VpnGlobalInfo.l2vpRmInfo.u4l2vpRmState = L2VPN_RM_ACTIVE_STANDBY_UP;
                  MEMSET(&L2VpnPwHwList, L2VPN_ZERO, sizeof(tL2VpnPwHwList));
                  L2VpnPwHwList.u4VplsIndex   = 1;
                  L2VpnPwHwList.u4PwIndex      = 1;
                  L2VpnPwHwList.u4InLabel     = 100;
                  L2VpnPwHwList.u4OutLabel    = 200;
                  L2VpnPwHwList.PeerAddress.i4IpAddrType = 1000;
                  L2VpnPwHwList.u4RemoteVEId =  500;
                  L2VpnPwHwList.u4OutIfIndex =  10;
                  L2VpnPwHwList.u4LspInLabel =  300;
                  L2VpnPwHwList.u4InIfIndex =   20;
                  L2VpnPwHwList.u1StaleStatus = 1;
                  L2VpnMemAllocPool();
                  L2VpnRmSendPwVcHwListEntry(&L2VpnPwHwList, u1OpType);
                  L2VpnMemReleasePool();
                
                i4RetVal =  L2VPN_SUCCESS;         

           }
           break;
     case 83:
          {
            L2VpnRmSendBulkPwVcInfo();
            i4RetVal =  L2VPN_SUCCESS;               
          }
         break; 
     case 84:
           {
            L2VpnSendBulkInfoFailed();
            i4RetVal = L2VPN_SUCCESS;
           }  
           break;       
      case 85:
           {
              gRmInfo.u4NodeState = 0;
              L2VpnBulkCountFailed();
              i4RetVal = L2VPN_SUCCESS;
           } 
           break;
     case 86:
           { 
            L2VpnMsgCountZero();
            i4RetVal = L2VPN_SUCCESS;

           }
           break; 
    case 87:
          {
            tRmMsg *pMsg = NULL;
            UINT4   u4Index = L2VPN_ZERO;
            pMsg = CRU_BUF_Allocate_MsgBufChain (L2VPN_RM_MSG_HDR_SIZE, L2VPN_ZERO);            
            
           
            for(u4Index = 1; u4Index <= MAX_L2VPN_Q_MSG ; u4Index++)
            {
               gpu1TmpMsg[u4Index]  = MemAllocMemBlk (L2VPN_Q_POOL_ID);
               if(gpu1TmpMsg[u4Index] == NULL)
               {
                        break;
               }
           }
            L2VpnRmEventHandler((UINT1)RM_PEER_DOWN, pMsg, 1);
            L2VpnReleaseMemPool();
            i4RetVal = L2VPN_SUCCESS;
          }
          break;
    }    
   

    if (i4RetVal == L2VPN_FAILURE)
    {
        u4TotalFailCase++;
        CliPrintf(CliHandle,"!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!\r\n",
                i4UtId, "FAILED");
    }
    else
    {
        u4TotalPassCase++;
        CliPrintf(CliHandle,"$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$\r\n",
                i4UtId, "PASSED");

    }
}
