/********************************************************************
 *** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vpngrut.c,v 1.1 2014/08/25 12:23:30 siva Exp $
 ***
 *** Description: MPLS API  UT Test cases.
 *** NOTE: This file should not be include in release packaging.
 *** ***********************************************************************/


#ifndef __TLDP_PW_GR_TEST_C__
#define __TLDP_PW_GR_TEST_C__

#include "l2vpincs.h"
#include "iss.h"
#include "../../../mpls/mplsinc/lblmgrex.h"
#include "../../../mpls/ldp/inc/ldpincs.h"
#define TLDP_PW_GR_UT_CASES 58


extern UINT4  u4TotalPassedCase;
extern UINT4  u4TotalFailedCase;
VOID CallTLdpPwGrUTCases (tCliHandle CliHandle);
VOID TLdpPwGrExecUnitTest (tCliHandle CliHandle, INT4 i4Count);

VOID L2VpnUpdateHwList(tL2VpnPwHwList *pL2VpnPwHwListEntry,UINT4 u4VplsIndex, 
						UINT4 u4PwIndex);
VOID CreatePwCli(VOID);

extern INT4 CreateVpls (UINT4 u4VeId);
extern VOID
CreateBgpEventInfo (tL2VpnBgpEvtInfo *pL2VpnBgpEvtInfo, UINT4 u4Duration);
extern INT4 CreateBgpPw (UINT4 u4VeId);
VOID
CallTLdpPwGrUTCases (tCliHandle CliHandle)
{
    INT4                i4Count = 1;

    for (i4Count = 1; i4Count <= TLDP_PW_GR_UT_CASES; i4Count++)
    {
        TLdpPwGrExecUnitTest (CliHandle, i4Count);
    }
}

VOID TLdpPwGrExecUnitTest ( tCliHandle CliHandle, INT4 i4UtId)
{
	INT4                i4RetVal = L2VPN_FAILURE;

	switch (i4UtId)
	{

		case 1:
			{
				/************************************************************************/
				/* TEST CASE 1 	 : Check Memory is allocated to HW List is successful	*/
				/* FUNCTION NAME : MplsAllocateMemoryForHwList() 						*/
				/* SUNNY DAY 															*/	
				/************************************************************************/
				
				tMplsL2VpnPwHwList *pMplsL2VpnPwHwList = NULL;
				MplsAllocateMemoryForHwList(&pMplsL2VpnPwHwList);
				if(pMplsL2VpnPwHwList != NULL)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MemReleaseMemBlock (MPLS_PW_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnPwHwList);
			}
			break;
		case 2:
			{
				/************************************************************************/
				/* TEST CASE 2   : Check Memory is allocated to HW List is failed		*/
				/* FUNCTION NAME : MplsAllocateMemoryForHwList() 						*/
				/* RAINY DAY 															*/	
				/************************************************************************/
				INT4 i4Index = 0;
				tMplsL2VpnPwHwList *pMplsL2VpnPwHwList[MAX_MPLSDB_PW_VC_ENTRIES] = {0};
				tMplsL2VpnPwHwList *pMplsL2VpnPwHwListEntry = NULL;

				for(i4Index = 0 ; i4Index < MAX_MPLSDB_PW_VC_ENTRIES; i4Index++)
				{
					pMplsL2VpnPwHwList[i4Index] = (tMplsL2VpnPwHwList *) MemAllocMemBlk
                        			(MPLS_PW_HW_LIST_POOL_ID);	
				}
				
				MplsAllocateMemoryForHwList(&pMplsL2VpnPwHwListEntry);
				if(pMplsL2VpnPwHwListEntry == NULL)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				for(i4Index = 0 ; i4Index < MAX_MPLSDB_PW_VC_ENTRIES; i4Index++)
				{
					MemReleaseMemBlock (MPLS_PW_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnPwHwList[i4Index]);
				}	

			}
			break;
		case 3:
			{
				/************************************************************************/
				/* TEST CASE 3 	 : Creation of PW HW-List must be successful			*/
				/* FUNCTION NAME : MplsCreateRbTreeForPwHwList 							*/
				/* SUNNY DAY 															*/	
				/************************************************************************/
				if(MplsCreateRbTreeForPwHwList() == MPLS_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}

			}
			break;
		case 4:
			{
				/************************************************************************/
				/* TEST CASE 4 	 : Adding Pw Info in HW-List is failed, because of 		*/
 				/* 		    		Memory Aloocation failure							*/
				/* FUNCTION NAME : MplsL2vpnHwListAdd        							*/
				/* SUNNY DAY 															*/	
				/************************************************************************/
				INT4 i4Index = 0;
				tMplsL2VpnPwHwList *pMplsL2VpnPwHwList[MAX_MPLSDB_PW_VC_ENTRIES] = {0};
				tMplsL2VpnPwHwList *pMplsL2VpnPwHwListEntry = NULL;
				tL2VpnPwHwList L2VpnPwHwListEntry;
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);

				for(i4Index = 0 ; i4Index < MAX_MPLSDB_PW_VC_ENTRIES; i4Index++)
				{
					pMplsL2VpnPwHwList[i4Index] = (tMplsL2VpnPwHwList *) MemAllocMemBlk
						(MPLS_PW_HW_LIST_POOL_ID);
				}
				if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;

				}
				for(i4Index = 0 ; i4Index < MAX_MPLSDB_PW_VC_ENTRIES; i4Index++)
				{
					MemReleaseMemBlock (MPLS_PW_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnPwHwList[i4Index]);
				}
	
			}
			break;
		case 5:
			{
				/************************************************************************/
				/* TEST CASE 5 	 : Adding Pw Info in HW-Listt							*/
				/* FUNCTION NAME : MplsL2vpnHwListAdd 									*/
				/* SUNNY DAY 															*/	
				/************************************************************************/

					tL2VpnPwHwList L2VpnPwHwListEntry;
					MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
					L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				
					if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
					{
						i4RetVal = L2VPN_SUCCESS;

					}
					MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
			}
			break;
		case 6:
			{
				/************************************************************************/
				/* TEST CASE 6 	 : Adding Pw Info in HW-Listt should be failed			*/
				/* FUNCTION NAME : MplsL2vpnHwListAdd 									*/
				/* RAINY DAY 															*/	
				/************************************************************************/

				tL2VpnPwHwList L2VpnPwHwListEntry;
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				gMplsL2VpnPwHwListTree = NULL;
				if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;

				}
				MplsCreateRbTreeForPwHwList();
			}
			break;
		case 7:
			{
				/************************************************************************/
				/* TEST CASE 7 	 : Check show command print the hw-list or not			*/
				/* FUNCTION NAME : L2VpnGrCliShowHwList 								*/
				/* SUNNY DAY 															*/	
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;

				}
				if(L2VpnGrCliShowHwList(CliHandle) == CLI_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
			}
			break;
		case 8:
			{
				/************************************************************************/
				/* TEST CASE 8 	 : Check show command print nothing when there is no 	*/
 				/* 									entry in hw-list					*/
				/* FUNCTION NAME : L2VpnGrCliShowHwList 								*/
				/* SUNNY DAY 															*/	
				/************************************************************************/
				if(L2VpnGrCliShowHwList(CliHandle) == CLI_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;
				}

			}
			break;
		case 9:
			{
				/************************************************************************/
				/* TEST CASE 9 	 : Deletion from hardware must be successful			*/
				/* FUNCTION NAME : MplsL2vpnHwListDelete 								*/
				/* SUNNY DAY 															*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;	
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				if(MplsL2vpnHwListDelete(&L2VpnPwHwListEntry)==MPLS_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}

			break;
			}
		case 10:
			{
				/************************************************************************/
				/* TEST CASE 10  : Deletion from hardware must be failed				*/
				/* FUNCTION NAME : MplsL2vpnHwListDelete 								*/
				/* RAINY DAY 															*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				if(MplsL2vpnHwListDelete(&L2VpnPwHwListEntry)==MPLS_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			}
			break;
		case 11:
			{
				/************************************************************************/
				/* TEST CASE 11  : Get the first element from hw-list					*/
				/* FUNCTION NAME : MplsL2vpnHwListGetFirst 								*/
				/* SUNNY DAY 															*/
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;	
				tL2VpnPwHwList L2VpnPwHwListFirstEntry;	
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				if(MplsL2vpnHwListGetFirst(&L2VpnPwHwListFirstEntry)== 
						MPLS_SUCCESS)
				{	
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				
			break;
			}
		case 12:
			{
				/************************************************************************/
				/* TEST CASE 12  : Get the first element from hw-list must be failed 	*/
 				/*					When there is no entry in Hw-List                	*/
				/* FUNCTION NAME : MplsL2vpnHwListGetFirst 							 	*/
				/* RAINY DAY 														 	*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;	
				tL2VpnPwHwList L2VpnPwHwListFirstEntry;	
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				if(MplsL2vpnHwListGetFirst(&L2VpnPwHwListFirstEntry)== 
						MPLS_FAILURE)
				{	
					i4RetVal = L2VPN_SUCCESS;
				}
								
			break;
			}
		case 13:
			{
				/************************************************************************/
				/* TEST CASE 13  : Fecthed the PW info from Hw-List based on key 		*/
				/* FUNCTION NAME : MplsL2vpnHwListGet									*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				
				tL2VpnPwHwList  L2VpnPwHwListEntry;
				tL2VpnPwHwList  L2VpnPwHwEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				MEMSET(&L2VpnPwHwEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				L2VpnPwHwEntry.u4VplsIndex = 1;
				L2VpnPwHwEntry.u4PwIndex = 1;
				if(MplsL2vpnHwListGet(&L2VpnPwHwEntry,&L2VpnPwHwListEntry)
											== MPLS_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;	
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
	
			break;
			}
		case 14:
			{
				/************************************************************************/
				/* TEST CASE 14  : Fecthed the PW info from Hw-List based on key is 	*/
 				/*  				failed because entry is not present for that key 	*/
				/* FUNCTION NAME : MplsL2vpnHwListGet									*/
				/* RAINY DAY    														*/
				/************************************************************************/
	
				tL2VpnPwHwList  L2VpnPwHwListKey;
				tL2VpnPwHwList  L2VpnPwHwListEntry;
				tL2VpnPwHwList  L2VpnPwHwListGetEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
							MEMSET(&L2VpnPwHwListGetEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				L2VpnPwHwListKey.u4VplsIndex = 2;
				L2VpnPwHwListKey.u4PwIndex = 2;
				if(MplsL2vpnHwListGet(&L2VpnPwHwListKey,&L2VpnPwHwListGetEntry)
									== MPLS_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;	
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
	
			break;
			}
		case 15:
			{
				/************************************************************************/
				/* TEST CASE 15  : This API get the next entry from HW-List				*/
				/* FUNCTION NAME : MplsL2vpnHwListGetNext								*/
				/* SUNNY DAY    														*/
				/************************************************************************/

				tL2VpnPwHwList  L2VpnPwHwListEntry;
				tL2VpnPwHwList  pL2VpnPwHwListEntry;
				tL2VpnPwHwList  pL2VpnPwHwListEntry1;
				tL2VpnPwHwList  pL2VpnPwHwListGetEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				MEMSET(&pL2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				MEMSET(&pL2VpnPwHwListEntry1,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				
				L2VpnUpdateHwList(&pL2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&pL2VpnPwHwListEntry);
				
				L2VpnUpdateHwList(&pL2VpnPwHwListEntry1,2,2);
				MplsL2vpnHwListAdd(&pL2VpnPwHwListEntry1);

				L2VpnPwHwListEntry.u4VplsIndex = 1;
				L2VpnPwHwListEntry.u4PwIndex = 1;
				if(MplsL2vpnHwListGetNext(&L2VpnPwHwListEntry,&pL2VpnPwHwListGetEntry)
										== MPLS_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;	
				}
				MplsL2vpnHwListDelete(&pL2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&pL2VpnPwHwListEntry1);

			break;
			}

		case 16:
			{
				/************************************************************************/
				/* TEST CASE 16  : The API should return failure if we give the maximum */
 				/* 					key present in RBTree								*/
				/* FUNCTION NAME : MplsL2vpnHwListGetNext								*/
				/* RAINY DAY    														*/
				/************************************************************************/

				tL2VpnPwHwList  L2VpnPwHwListEntry;
				tL2VpnPwHwList  pL2VpnPwHwListEntry;
				tL2VpnPwHwList  pL2VpnPwHwListEntry1;
				tL2VpnPwHwList  pL2VpnPwHwListGetEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				MEMSET(&pL2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				MEMSET(&pL2VpnPwHwListEntry1,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				
				L2VpnUpdateHwList(&pL2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&pL2VpnPwHwListEntry);
				
				L2VpnUpdateHwList(&pL2VpnPwHwListEntry1,2,2);
				MplsL2vpnHwListAdd(&pL2VpnPwHwListEntry1);

				L2VpnPwHwListEntry.u4VplsIndex = 2;
				L2VpnPwHwListEntry.u4PwIndex = 2;
				if(MplsL2vpnHwListGetNext(&L2VpnPwHwListEntry,&pL2VpnPwHwListGetEntry)
										== MPLS_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;	
				}
				MplsL2vpnHwListDelete(&pL2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&pL2VpnPwHwListEntry1);


	
			break;
			}

		case 17:
			{
				/************************************************************************/
				/* TEST CASE 17  : Hw-List will destroy successfully					*/
				/* FUNCTION NAME : MplsL2vpnHwListDestroy								*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				
				MplsL2vpnHwListDestroy();
				if(gMplsL2VpnPwHwListTree == NULL)
				{
					 i4RetVal = L2VPN_SUCCESS;
				}	
				MplsCreateRbTreeForPwHwList();
			break;
			}

		case 18:
			{
				/************************************************************************/
				/* TEST CASE 18  :                                                      */
				/* FUNCTION NAME : LdpSendGrStartEventToL2Vpn                           */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				MgmtUnLock ();
				CliExecuteAppCmd("configure terminal");
				CliExecuteAppCmd("mpls ldp");
				CliExecuteAppCmd("no mpls ldp graceful-restart full");
				CliExecuteAppCmd("end");
				LdpSendGrStartEventToL2Vpn();
				i4RetVal = L2VPN_SUCCESS;

	
			break;
			}

		case 19:
			{
				/************************************************************************/
				/* TEST CASE 19  : This API should return success when Hw-List is 		*/
 				/* 					created successfully for static PW					*/
				/* FUNCTION NAME : L2VpnPwVcHwListCreate								*/
				/* SUNNY DAY    														*/
				/************************************************************************/

				tPwVcEntry 			* pPwVcEntry = NULL;
				tMplsHwVcTnlInfo 	* pMplsHwVcInfo = NULL;
				tL2VpnPwHwList 		  L2VpnPwHwListEntry;
				tPwVcMplsEntry 		* pPSNEntry = NULL;
	
				pPwVcEntry = (tPwVcEntry *)malloc(sizeof(tPwVcEntry));
				pMplsHwVcInfo = (tMplsHwVcTnlInfo *)malloc(sizeof(tMplsHwVcTnlInfo));
				pPSNEntry	=	(tPwVcMplsEntry *)malloc(sizeof(tPwVcMplsEntry));
				L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) = pPSNEntry;
				
				L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = 1;
				L2VPN_PWVC_INDEX (pPwVcEntry) = 1;			
				pPwVcEntry->u4VplsBgpPwBindRemoteVEId = 0;
				L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))	=1;
				pMplsHwVcInfo->OutLspLabel.u.MplsShimLabel = 100001;
				pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel = 100002;
				pMplsHwVcInfo->u4PwL3Intf = 1069;
				pMplsHwVcInfo->u4PwOutPort = 1;
				pPwVcEntry->bIsStaticPw = TRUE;
				L2VPN_PWVC_OWNER (pPwVcEntry) = L2VPN_PWVC_OWNER_GEN_FEC_SIG;

				if((L2VpnPwVcHwListCreate(pPwVcEntry,pMplsHwVcInfo,&L2VpnPwHwListEntry) == L2VPN_SUCCESS)) 
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				free(pPwVcEntry);
				free(pMplsHwVcInfo);
				free(pPSNEntry);

			break;
			}

		case 20:
			{
				/************************************************************************/
				/* TEST CASE 20  : This API should not mark BGP PW enties  				*/
				/*  				as stale                                            */
				/* FUNCTION NAME : L2VpnMarkLdpPwEntriesStale							*/
				/* RAINY DAY    														*/
				/************************************************************************/
				tL2VpnPwHwList  L2VpnPwHwListEntry;
				tL2VpnPwHwList  L2VpnPwHwListKey;
				tL2VpnPwHwList  L2VpnPwHwListEntry1;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				MEMSET(&L2VpnPwHwListEntry1,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry1,2,2);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1);

				MgmtUnLock ();
				CreatePwCli();
				MgmtLock ();
				L2VPN_PW_HW_LIST_REMOTE_VE_ID(&L2VpnPwHwListEntry1)= 1;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry1);
				if(L2VpnMarkLdpPwEntriesStale() == L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);
				break;
			}

		case 21:
			{
				/************************************************************************/
				/* TEST CASE 21  : This API should return success when Hw-List is       */
 				/*  				created successfully for dynamic PW                 */
				/* FUNCTION NAME : L2VpnPwVcHwListCreate								*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				tPwVcEntry 			* pPwVcEntry = NULL;
				tMplsHwVcTnlInfo 	* pMplsHwVcInfo = NULL;
				tL2VpnPwHwList 	 	  L2VpnPwHwListEntry;
				tL2VpnPwHwList 		  L2VpnPwHwListEntry1;
				tPwVcMplsEntry 		* pPSNEntry = NULL;
	
				pPwVcEntry = (tPwVcEntry *)malloc(sizeof(tPwVcEntry));
				pMplsHwVcInfo = (tMplsHwVcTnlInfo *)malloc(sizeof(tMplsHwVcTnlInfo));
				pPSNEntry	=	(tPwVcMplsEntry *)malloc(sizeof(tPwVcMplsEntry));
				L2VPN_PWVC_PSN_ENTRY (pPwVcEntry) = pPSNEntry;
				
				L2VPN_PWVC_VPLS_INDEX (pPwVcEntry) = 1;
				L2VPN_PWVC_INDEX (pPwVcEntry) = 1;			
				pPwVcEntry->u4VplsBgpPwBindRemoteVEId = 0;
				L2VPN_PWVC_MPLS_LCL_LDP_ENT_INDEX((tPwVcMplsEntry *) L2VPN_PWVC_PSN_ENTRY (pPwVcEntry))	=1;
				pMplsHwVcInfo->OutLspLabel.u.MplsShimLabel = 100001;
				pMplsHwVcInfo->PwOutVcLabel.u.MplsShimLabel = 100002;
				pMplsHwVcInfo->u4PwL3Intf = 1069;
				pMplsHwVcInfo->u4PwOutPort = 1;
				pPwVcEntry->bIsStaticPw = TRUE;
				L2VPN_PWVC_OWNER (pPwVcEntry) = L2VPN_PWVC_OWNER_OTHER;

				if((L2VpnPwVcHwListCreate(pPwVcEntry,pMplsHwVcInfo,&L2VpnPwHwListEntry) == L2VPN_SUCCESS))		
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				i4RetVal = L2VPN_FAILURE;
				pPwVcEntry->bIsStaticPw = FALSE;
				if((L2VpnPwVcHwListCreate(pPwVcEntry,pMplsHwVcInfo,&L2VpnPwHwListEntry1) == L2VPN_SUCCESS))
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				free(pPwVcEntry);
				free(pMplsHwVcInfo);
				free(pPSNEntry);
	
			break;
			}

		case 22:
			{
				/************************************************************************/
				/* TEST CASE 22  : This API should successfully update the ILM Entry in */
 				/* 				   in HW-List											*/
				/* FUNCTION NAME : L2VpnPwIlmHwListUpdate								*/
				/* SUNNY DAY     :													*/
				/************************************************************************/
				
				tPwVcEntry          PwVcEntry;
				tMplsHwIlmInfo  	MplsHwIlmInfo;
				tL2VpnPwHwList      L2VpnPwHwListEntry;
				tPwVcMplsEntry      PSNEntry;
		
				MEMSET(&PwVcEntry,0,sizeof(tPwVcEntry));
				MEMSET(&MplsHwIlmInfo,0,sizeof(tL2VpnPwHwList));	
				MEMSET(&PSNEntry,0,sizeof(tPwVcMplsEntry));	
				
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,2,2);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);				

				L2VPN_PWVC_VPLS_INDEX (&PwVcEntry)=2;
				L2VPN_PWVC_INDEX(&PwVcEntry)=2;
				L2VPN_PWVC_PSN_ENTRY (&PwVcEntry) = &PSNEntry;
				MplsHwIlmInfo.u4InL3Intf = 4;
				MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = 100001;
				MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = 100002;
				L2VPN_PWVC_MPLS_MPLS_TYPE (&PSNEntry) = L2VPN_MPLS_TYPE_NONTE;
				if(L2VpnPwIlmHwListUpdate(&PwVcEntry,&MplsHwIlmInfo,&L2VpnPwHwListEntry)
												== L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				i4RetVal = L2VPN_FAILURE;
				L2VPN_PWVC_MPLS_MPLS_TYPE (&PSNEntry) = L2VPN_MPLS_TYPE_VCONLY;
				if(L2VpnPwIlmHwListUpdate(&PwVcEntry,&MplsHwIlmInfo,&L2VpnPwHwListEntry)
												== L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
								
			break;
			}

		case 23:
			{
				/************************************************************************/
				/* TEST CASE 23  : This API should successfully mark all the LDPPW enties*/
 				/* 				  	as stale											*/
				/* FUNCTION NAME : L2VpnMarkLdpPwEntriesStale							*/
				/* SUNNY DAY     :														*/
				/************************************************************************/
				
				tL2VpnPwHwList  L2VpnPwHwListEntry;
				tL2VpnPwHwList  L2VpnPwHwListKey;
				tL2VpnPwHwList	L2VpnPwHwListEntry1;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				MEMSET(&L2VpnPwHwListEntry1,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry1,2,2);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1);	
				
				MgmtUnLock ();
				CreatePwCli();
				MgmtLock ();	
				if(L2VpnMarkLdpPwEntriesStale() == L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);
				L2VpnMarkLdpPwEntriesStale();		
	
			break;
			}

		case 24:
			{
				/************************************************************************/
				/* TEST CASE 24  : This API should return failure if there is no        */
 				/* 				   entry in HW-List 									*/
				/* FUNCTION NAME : L2VpnGetNextLabelFromHwList							*/
				/* SUNNY DAY     :														*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry1;
				tL2VpnPwHwList L2VpnPwHwListEntry;
				tL2VpnPwHwList  L2VpnPwHwListKey;
				UINT4 			u4InLabel = 0;

				if(L2VpnGetNextLabelFromHwList(&L2VpnPwHwListEntry,&L2VpnPwHwListEntry,
										&u4InLabel)== L2VPN_INVALID)
				{
					i4RetVal = L2VPN_SUCCESS;
				}

				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				L2VpnUpdateHwList(&L2VpnPwHwListEntry1,2,2);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1);
			 	i4RetVal = L2VPN_FAILURE;	
				if(L2VpnGetNextLabelFromHwList(&L2VpnPwHwListEntry,&L2VpnPwHwListEntry,
										&u4InLabel)== L2VPN_VALID)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				
			 	i4RetVal = L2VPN_FAILURE;	
				L2VpnPwHwListKey.u4VplsIndex = 1;
				L2VpnPwHwListKey.u4PwIndex = 1;
				MplsL2vpnHwListGet(&L2VpnPwHwListKey,&L2VpnPwHwListEntry);	
				L2VPN_PW_HW_LIST_IS_STATIC_PW(&L2VpnPwHwListEntry1)= 1;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry1);		
				if(L2VpnGetNextLabelFromHwList(&L2VpnPwHwListEntry,&L2VpnPwHwListEntry,
										&u4InLabel)== L2VPN_INVALID)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			 	i4RetVal = L2VPN_FAILURE;	
				L2VpnPwHwListKey.u4VplsIndex = 1;
				L2VpnPwHwListKey.u4PwIndex = 1;
				MplsL2vpnHwListGet(&L2VpnPwHwListKey,&L2VpnPwHwListEntry);	
				L2VPN_PW_HW_LIST_REMOTE_VE_ID(&L2VpnPwHwListEntry1)= 1;
				L2VPN_PW_HW_LIST_IS_STATIC_PW(&L2VpnPwHwListEntry1)= 0;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry1);
				if(L2VpnGetNextLabelFromHwList(&L2VpnPwHwListEntry,&L2VpnPwHwListEntry,
										&u4InLabel)== L2VPN_INVALID)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			 	i4RetVal = L2VPN_FAILURE;	
				L2VpnPwHwListKey.u4VplsIndex = 1;
				L2VpnPwHwListKey.u4PwIndex = 1;
				MplsL2vpnHwListGet(&L2VpnPwHwListKey,&L2VpnPwHwListEntry);	
				L2VPN_PW_HW_LIST_REMOTE_VE_ID(&L2VpnPwHwListEntry1)= 1;
				L2VPN_PW_HW_LIST_IS_STATIC_PW(&L2VpnPwHwListEntry1)= 1;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry1);
				if(L2VpnGetNextLabelFromHwList(&L2VpnPwHwListEntry,&L2VpnPwHwListEntry,
										&u4InLabel)== L2VPN_INVALID)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				L2VpnPwHwListKey.u4VplsIndex = 1;
				L2VpnPwHwListKey.u4PwIndex = 1;
				MplsL2vpnHwListGet(&L2VpnPwHwListKey,&L2VpnPwHwListEntry);	
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);

			break;
			}

		case 25:
			{
				/************************************************************************/
				/* TEST CASE 25   : Adding Pw Info in HW-Listt should be failed          */
				/* FUNCTION NAME  : MplsL2vpnHwListAdd                                   */
				/* RAINY DAY                                                            */
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				gMplsL2VpnPwHwListTree = NULL;
				if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;

				}
				MplsCreateRbTreeForPwHwList();
				break;
			}

		case 26:
			{
				/************************************************************************/
				/* TEST CASE 26  :            TODO: Discuss why two keys are requied	*/
				/* FUNCTION NAME :														*/
				/* 			DAY    														*/
				/************************************************************************/
				
				tL2VpnPwHwList  L2VpnPwHwListKey;
				tL2VpnPwHwList L2VpnPwHwListEntry;
				tL2VpnPwHwList L2VpnPwHwListEntry1;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));

				L2VpnUpdateHwList(&L2VpnPwHwListEntry,0,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				
				L2VpnUpdateHwList(&L2VpnPwHwListEntry1,0,2);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1);

				L2VpnPwHwListKey.u4VplsIndex = 0;
				L2VpnPwHwListKey.u4PwIndex = 1;
				MplsL2vpnHwListGet(&L2VpnPwHwListKey,&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);


			break;
			}

		case 27:
			{
				/************************************************************************/
				/* TEST CASE 27  : 														*/
				/* FUNCTION NAME :L2VpnPwHwListAddUpdate								*/
				/* RAINY DAY    														*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);

				gMplsL2VpnPwHwListTree = NULL;
				if(L2VpnPwHwListAddUpdate(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_CALLED)
						== L2VPN_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsCreateRbTreeForPwHwList();	
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
			break;
			}

		case 28:
			{
				/************************************************************************/
				/* TEST CASE 28  : 														*/
				/* FUNCTION NAME :L2VpnPwHwListAddUpdate								*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);

				if(L2VpnPwHwListAddUpdate(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_SUCCESS)
						== L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}	
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
			break;
			}
		case 29:
			{
				/************************************************************************/
				/* TEST CASE 29  : 														*/
				/* FUNCTION NAME :L2VpnPwHwListAddUpdate								*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				L2VPN_PW_HW_LIST_STALE_STATUS(&L2VpnPwHwListEntry)= 0;
				
				if(L2VpnPwHwListAddUpdate(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_SUCCESS)
						== L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}	
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);

			break;
			}


		case 30:
			{
#if 0
				/************************************************************************/
				/* TEST CASE 23  : 														*/
				/* FUNCTION NAME :	L2VpnPwHwListGet									*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				tPwVcEntry 		PwVcEntry;
				tL2VpnPwHwList *pL2VpnPwHwListEntry = NULL;
				
				L2VPN_PWVC_VPLS_INDEX (&PwVcEntry) = 1;
 				L2VPN_PWVC_INDEX(&PwVcEntry) = 1;		

				L2VpnAllocateMemoryForHwList(&pL2VpnPwHwListEntry);
				MEMSET(pL2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(pL2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(pL2VpnPwHwListEntry);
				if(L2VpnPwHwListGet(&PwVcEntry,&pL2VpnPwHwListEntry)
							== L2VPN_SUCCESS)
				{
				}
				MplsL2vpnHwListDelete(pL2VpnPwHwListEntry);
#endif
					i4RetVal = L2VPN_SUCCESS;
			break;
			}

		case 31:
			{
#if 0
				/************************************************************************/
				/* TEST CASE 23  : 														*/
				/* FUNCTION NAME : L2VpnPwHwListGet										*/
				/* RAINY DAY    														*/
				/************************************************************************/
				tPwVcEntry 		PwVcEntry;
				tL2VpnPwHwList *pL2VpnPwHwListEntry = NULL;
				
				L2VPN_PWVC_VPLS_INDEX (&PwVcEntry) = 1;
 				L2VPN_PWVC_INDEX(&PwVcEntry) = 1;		
				
				if(L2VpnPwHwListGet(&PwVcEntry,&pL2VpnPwHwListEntry)
						== L2VPN_FAILURE)
				{	
				}
#endif
					i4RetVal = L2VPN_SUCCESS;
			break;
			}

		case 32:
			{
				/************************************************************************/
				/* TEST CASE 23  : 														*/
				/* FUNCTION NAME :	L2VpnPwHwListDelete									*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				L2VPN_PW_HW_LIST_NPAPI_STATUS(&L2VpnPwHwListEntry) |=
							MPLS_PWVC_NPAPI_CALLED;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
				if(L2VpnPwHwListDelete(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_CALLED)
						== L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
	
			break;
			}

		case 33:
			{
				/************************************************************************/
				/* TEST CASE 23  : 														*/
				/* FUNCTION NAME :L2VpnPwHwListDelete									*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				L2VPN_PW_HW_LIST_NPAPI_STATUS(&L2VpnPwHwListEntry) |=
							MPLS_PWVC_NPAPI_CALLED;
				L2VPN_PW_HW_LIST_NPAPI_STATUS(&L2VpnPwHwListEntry) |=
							MPLS_PWVC_NPAPI_SUCCESS;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
				if(L2VpnPwHwListDelete(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_CALLED)
						== L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);

	
			break;
			}

		case 34:
			{
				/************************************************************************/
				/* TEST CASE 23  : 														*/
				/* FUNCTION NAME :	L2VpnPwHwListDelete									*/
				/* RAINY	DAY    														*/
				/************************************************************************/

				tL2VpnPwHwList L2VpnPwHwListEntry;
				if(L2VpnPwHwListDelete(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_CALLED)
						== L2VPN_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
	
			break;
			}

		case 35:
			{
				/************************************************************************/
				/* TEST CASE 23  : 														*/
				/* FUNCTION NAME : L2VpnPwHwListDelete									*/
				/* SUNNY DAY    														*/
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				L2VPN_PW_HW_LIST_NPAPI_STATUS(&L2VpnPwHwListEntry) |=
							MPLS_PWVC_NPAPI_CALLED;
				MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
				if(L2VpnPwHwListDelete(&L2VpnPwHwListEntry,MPLS_PWVC_NPAPI_CALLED)
						== L2VPN_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			
			break;
			}
		case 36:
			{
				/************************************************************************/
				/* TEST CASE 23  :                                                      */
				/* FUNCTION NAME : L2VpnLdpSendLblRelEvent                              */
				/* SUNNY DAY                                                            */
				/************************************************************************/
			
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
		
				MgmtUnLock ();	
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("shutdown ldp");
				MgmtLock ();
				if(L2VpnLdpSendLblRelEvent(&L2VpnPwHwListEntry)== L2VPN_FAILURE)
				{
					 i4RetVal = L2VPN_SUCCESS;
				}
				MgmtUnLock ();	
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("start ldp");
				MgmtLock ();
				break;
			}
		case 37:
			{
				/************************************************************************/
				/* TEST CASE 23  :                                                      */
				/* FUNCTION NAME : L2VpnLdpSendLblRelEvent                              */
				/* SUNNY DAY                                                            */
				/************************************************************************/

				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				MgmtUnLock ();
				CreatePwCli();
				MgmtLock ();	
				if(L2VpnLdpSendLblRelEvent(&L2VpnPwHwListEntry)==L2VPN_SUCCESS)
				{	
					 i4RetVal = L2VPN_SUCCESS;
				}	
				break;
			}

		case 38:
			{
				/************************************************************************/
				/* TEST CASE 38  :                                                      */
				/* FUNCTION NAME : MplsL2vpnHwListUpdate                                */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				
				tL2VpnPwHwList L2VpnPwHwListEntry;
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));

				if(MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry)==MPLS_FAILURE)	
				{
					i4RetVal = L2VPN_SUCCESS;
				}
				break;
			}
	
		case 39:
			{
				/************************************************************************/
				/* TEST CASE 39  :                                                      */
				/* FUNCTION NAME : L2VpnFlushPwVcDynamicEntries                         */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				tPwVcActivePeerSsnEntry *pPeerSsn=NULL;

				MgmtUnLock ();
                CreatePwCli();
                MgmtLock ();	
				pPeerSsn = RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
				if (pPeerSsn == NULL)
				{
					L2VPN_DBG (L2VPN_DBG_GRACEFUL_RESTART,
							"L2vpnGrProcessLdpGrShutEvent: No corresponding PeerSsn Found\r\n");
					return L2VPN_FAILURE;
				}
				if(L2VpnFlushPwVcDynamicEntries(pPeerSsn) == L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			}
			break;
		case 40:
			{
				/************************************************************************/
				/* TEST CASE 40  :                                                      */
				/* FUNCTION NAME : L2VpnFlushPwVcDynamicEntries                         */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				
				tPwVcActivePeerSsnEntry *pPeerSsn=NULL;
				if(L2VpnFlushPwVcDynamicEntries(pPeerSsn) == L2VPN_FAILURE)
				{
					i4RetVal = L2VPN_SUCCESS;
				}

			}
			break;
		case 41:
			{
				/************************************************************************/
				/* TEST CASE 41  :                                                      */
				/* FUNCTION NAME :L2VpnFlushPwVcDynamicEntries                          */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				tPwVcActivePeerSsnEntry *pPeerSsn=NULL;

				MgmtUnLock ();
				CreatePwCli();
				MgmtLock ();
				pPeerSsn = RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
				MgmtUnLock ();
				CliExecuteAppCmd("switch default");
				CliExecuteAppCmd("vlan 100");
				CliExecuteAppCmd("mpls l2transport pwidfec 33.33.33.33 pwid 2 groupid 1 mplstype non-te");
				CliExecuteAppCmd("end");
				MgmtLock ();
				if(L2VpnFlushPwVcDynamicEntries(pPeerSsn) == L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			}
			break;
		case 42:
			{
				/************************************************************************/
				/* TEST CASE 42  :                                                      */
				/* FUNCTION NAME : LdpGrInitProcess                                     */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				
				LdpGrInitProcess();
				i4RetVal = L2VPN_SUCCESS;	
			}
			break;
		case 43:
			{
				/************************************************************************/
				/* TEST CASE 43  : Destroy of Hw-List Will fail							*/
				/* FUNCTION NAME : MplsL2vpnHwListDestroy								*/
				/* RAINY DAY    														*/
				/************************************************************************/
				gMplsL2VpnPwHwListTree = NULL;	
				MplsL2vpnHwListDestroy();
				i4RetVal = L2VPN_SUCCESS;
				MplsCreateRbTreeForPwHwList();	

			}
			break;
		case 44:
			{
				/************************************************************************/
				/* TEST CASE 44  :                                                      */
				/* FUNCTION NAME : LdpSendGrStartEventToL2Vpn                           */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				MgmtUnLock ();
				CliExecuteAppCmd("configure terminal");
				CliExecuteAppCmd("mpls ldp");
				CliExecuteAppCmd("mpls ldp graceful-restart full");
				CliExecuteAppCmd("mpls ldp graceful-restart timers neighbor-liveness 150");
				CliExecuteAppCmd("mpls ldp graceful-restart timers forwarding-holding 300");
				CliExecuteAppCmd("mpls ldp graceful-restart timers max-recovery 300");
				CliExecuteAppCmd("end");
				MgmtLock ();
				LdpSendGrStartEventToL2Vpn();
				i4RetVal = L2VPN_SUCCESS;
			}
			break;
		case 45:
			{
				/************************************************************************/
				/* TEST CASE 45  :                                                      */
				/* FUNCTION NAME : LdpSendGrStartEventToL2Vpn                           */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				MgmtUnLock ();
				CliExecuteAppCmd("configure terminal");
				CliExecuteAppCmd("mpls ldp");
				CliExecuteAppCmd("mpls ldp graceful-restart full");
				CliExecuteAppCmd("mpls ldp graceful-restart timers neighbor-liveness 150");
				CliExecuteAppCmd("mpls ldp graceful-restart timers forwarding-holding 300");
				CliExecuteAppCmd("mpls ldp graceful-restart timers max-recovery 300");
				CliExecuteAppCmd("end");
				MgmtLock ();
				gL2VpnGlobalInfo.i4AdminStatus =L2VPN_ADMIN_DOWN;
				LdpSendGrStartEventToL2Vpn();
				gL2VpnGlobalInfo.i4AdminStatus =L2VPN_ADMIN_UP;
				i4RetVal = L2VPN_SUCCESS;

			}
			break;

		case 46:
			{
				/************************************************************************/
				/* TEST CASE 46  :                                                      */
				/* FUNCTION NAME : LdpReserveLabelToLabelManager                        */
				/* SUNNY DAY                                                            */
				/************************************************************************/
		
				tL2VpnPwHwList L2VpnPwHwListEntry1;
				tL2VpnPwHwList L2VpnPwHwListEntry;
				tL2VpnPwHwList  L2VpnPwHwListKey;
				UINT4           u4InLabel = 0;

				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,1);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				L2VpnUpdateHwList(&L2VpnPwHwListEntry1,2,2);
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1);	
			
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);		
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);		
				LdpReserveLabelToLabelManager();
				i4RetVal = L2VPN_SUCCESS;
			}
			break;

		case 47:
			{
				/************************************************************************/
				/* TEST CASE 47  : When entity type is not targeted                     */
				/* FUNCTION NAME : LdpReserveLabelToLabelManager                        */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				MgmtUnLock ();

				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("mpls ldp");
				CliExecuteAppCmd("entity 2");
				CliExecuteAppCmd("ldp label range min 160200 max 160399 interface vlan 2");
				CliExecuteAppCmd("no shutdown");
				CliExecuteAppCmd("end");
				MgmtLock ();

				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =2;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
	
				LdpReserveLabelToLabelManager();			
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);		
				i4RetVal = L2VPN_SUCCESS;
			}
			break;

		case 48:
			{
				/************************************************************************/
				/* TEST CASE 48  :                                                      */
				/* FUNCTION NAME :LdpReserveLabelToLabelManager                         */
				/* SUNNY DAY                                                            */
				/************************************************************************/

				MgmtUnLock ();
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("interface vlan 2");
				CliExecuteAppCmd("shutdown");
				CliExecuteAppCmd("ip address 20.0.0.1 255.0.0.0");
				CliExecuteAppCmd("mpls ip");
				CliExecuteAppCmd("no shutdown");
				CliExecuteAppCmd("end");
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("mpls ldp");
				CliExecuteAppCmd("entity 3");
				CliExecuteAppCmd("neighbor 33.33.33.33 targeted");
				CliExecuteAppCmd("ldp label range min 160200 max 160399 interface vlan 2");
				CliExecuteAppCmd("transport-address tlv loopback 0");
				CliExecuteAppCmd("label distribution unsolicited");
				CliExecuteAppCmd("label retention liberal");
				CliExecuteAppCmd("no shutdown");
				CliExecuteAppCmd("end");
				MgmtLock ();

				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =3;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				LdpReserveLabelToLabelManager();

				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;	


			}
			break;

		case 49:
			{
				/************************************************************************/
				/* TEST CASE 49  :                                                      */
				/* FUNCTION NAME : LdpReserveLabelToLabelManager                        */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =3;
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=164000;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				LdpReserveLabelToLabelManager();

				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;	

				break;

			}
		case 50:
			{
				/************************************************************************/
				/* TEST CASE 50  :                                                      */
				/* FUNCTION NAME : LdpReserveLabelToLabelManager                        */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =3;
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=160220;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);

				LdpReserveLabelToLabelManager();

				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;	

				break;

			}
		case 51:
			{
				/************************************************************************/
				/* TEST CASE 51  :                                                      */
				/* FUNCTION NAME : LdpReserveLabelToLabelManager                        */
				/* SUNNY DAY                                                            */
				/************************************************************************/

				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =3;
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=160220;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				LblMgrAssignLblToLblGroup(3,0,160220);
				LdpReserveLabelToLabelManager();

				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =10;
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=160220;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				LdpReserveLabelToLabelManager();
				i4RetVal = L2VPN_SUCCESS;
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				break;

			}
		case 52:
			{
				/************************************************************************/
				/* TEST CASE 52  :                                                      */
				/* FUNCTION NAME : L2VpnSendReadyEventToLdp		                        */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				
				gLdpInfo.LdpIncarn[(0)].u1RowStatus=NOT_IN_SERVICE;
				L2VpnSendReadyEventToLdp();
				gLdpInfo.LdpIncarn[(0)].u1RowStatus=ACTIVE;
				i4RetVal = L2VPN_SUCCESS;
				break;

			}

		case 53:
			{
				/************************************************************************/
				/* TEST CASE 53  :                                                      */
				/* FUNCTION NAME : L2vpnGrDeleteLdpPwEntryOnMisMatch*                   */
				/* SUNNY DAY                                                            */
				/************************************************************************/

				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =3;
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=160220;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				L2vpnGrDeleteLdpPwEntryOnMisMatch(&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;
				break;

			}
		case 54:
			{
				/************************************************************************/
				/* TEST CASE 54  :                                                      */
				/* FUNCTION NAME : L2vpnGrDeleteLdpPwEntryOnMisMatch                    */
				/* SUNNY DAY                                                            */
				/************************************************************************/

	
				tL2VpnPwHwList L2VpnPwHwListEntry;

				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,5,5);
				L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(&L2VpnPwHwListEntry) =3;
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=160220;
				L2VPN_PW_HW_LIST_REMOTE_VE_ID(&L2VpnPwHwListEntry)= 0;
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				gLdpInfo.LdpIncarn[(0)].u1RowStatus=NOT_IN_SERVICE;
				L2vpnGrDeleteLdpPwEntryOnMisMatch(&L2VpnPwHwListEntry);
				gLdpInfo.LdpIncarn[(0)].u1RowStatus=ACTIVE;
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;
			

				break;

			}
		case 55:
			{
				/************************************************************************/
				/* TEST CASE 55  :                                                      */
				/* FUNCTION NAME : L2vpnGrDeleteLdpPwEntryOnMisMatch                    */
				/* SUNNY DAY                                                            */
				/************************************************************************/


				tL2VpnPwHwList L2VpnPwHwListEntry;
				UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
				MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
				L2VpnUpdateHwList(&L2VpnPwHwListEntry,1,11);
				L2VPN_PW_HW_LIST_IN_LABEL(&L2VpnPwHwListEntry)=160220;
				L2VPN_PW_HW_LIST_REMOTE_VE_ID(&L2VpnPwHwListEntry)= 0;
				
				MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
				CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &i4RetVal);
				L2vpnGrDeleteLdpPwEntryOnMisMatch (&L2VpnPwHwListEntry);
				L2VPN_PW_HW_LIST_VPLS_INDEX(&L2VpnPwHwListEntry)=2;
				L2vpnGrDeleteLdpPwEntryOnMisMatch (&L2VpnPwHwListEntry);
				MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);
				i4RetVal = L2VPN_SUCCESS;
				break;
			}
	case 56:
			{
				/************************************************************************/
				/* TEST CASE 41  :                                                      */
				/* FUNCTION NAME :L2VpnFlushPwVcDynamicEntries                          */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				tPwVcActivePeerSsnEntry *pPeerSsn=NULL;

				MgmtUnLock ();
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("switch default");
				CliExecuteAppCmd("vlan 100");
				CliExecuteAppCmd("mpls l2transport pwidfec 33.33.33.33 pwid 1 groupid 1 mplstype non-te");
				CliExecuteAppCmd("end");
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("switch default");
				CliExecuteAppCmd("vlan 200");
				CliExecuteAppCmd("mpls l2transport pwidfec 33.33.33.33 pwid 2 groupid 1 mplstype non-te");
				CliExecuteAppCmd("end");
				MgmtLock ();
				pPeerSsn = RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
				if(L2VpnFlushPwVcDynamicEntries(pPeerSsn) == L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}
			break;
			}
	case 57:
			{
				/************************************************************************/
				/* TEST CASE 57  :                                                      */
				/* FUNCTION NAME :L2VpnFlushPwVcDynamicEntries                          */
				/* SUNNY DAY                                                            */
				/************************************************************************/
				tPwVcActivePeerSsnEntry *pPeerSsn=NULL;
				tPwVcLblMsgEntry   *pLblEntry = NULL;
				MgmtUnLock ();
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("switch default");
				CliExecuteAppCmd("vlan 100");
				CliExecuteAppCmd("mpls l2transport pwidfec 33.33.33.33 pwid 1 groupid 1 mplstype non-te");
				CliExecuteAppCmd("end");
				CliExecuteAppCmd("c t");
				CliExecuteAppCmd("switch default");
				CliExecuteAppCmd("vlan 200");
				CliExecuteAppCmd("mpls l2transport pwidfec 33.33.33.33 pwid 2 groupid 1 mplstype non-te");
				CliExecuteAppCmd("end");
				MgmtLock ();
				pPeerSsn = RBTreeGetFirst (L2VPN_PWVC_PEERADDR_RB_PTR (gpPwVcGlobalInfo));
							if(L2VpnFlushPwVcDynamicEntries(pPeerSsn) == L2VPN_SUCCESS)
				{
					i4RetVal = L2VPN_SUCCESS;
				}

			break;
			}

	case 58:
			{
				/************************************************************************/
				/* TEST CASE   :   This Test Case is written, so that HW-List creation  */
 				/* 									failed								*/
				/* FUNCTION NAME :														*/
				/* RAINY	DAY    														*/
				/************************************************************************/
				tPwVcEntry          * pPwVcEntry[10000] = {0};
				UINT4				u4Index = 0;
				for(u4Index = 0; u4Index < 10000; u4Index++)
				{
					pPwVcEntry[u4Index] = (tPwVcEntry *)malloc(sizeof(tPwVcEntry));
					MplsHwListInit();	
					if(MplsCreateRbTreeForPwHwList() == MPLS_FAILURE)
					{
						i4RetVal = L2VPN_SUCCESS;
					}
				}
				for(u4Index = 0; u4Index < 10000; u4Index++)
				{
					free(pPwVcEntry[u4Index]);
				}
				gMplsL2VpnPwHwListTree = NULL;	
	
			break;
			}



		default:
    			CliPrintf(CliHandle,"%% No test case is defined for ID: %d\r\n",
              	i4UtId);
    		break;

	}
	if (i4RetVal == L2VPN_FAILURE)
	{
		CliPrintf(CliHandle,"!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!\r\n",
				i4UtId, "FAILED");
		u4TotalFailedCase++;
	}
	else
	{
		CliPrintf(CliHandle,"$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$\r\n",
				i4UtId, "PASSED");
		u4TotalPassedCase++;
	}

}
VOID L2VpnUpdateHwList(tL2VpnPwHwList *pL2VpnPwHwListEntry,UINT4 u4VplsIndex, UINT4 u4PwIndex)
{
	L2VPN_PW_HW_LIST_VPLS_INDEX(pL2VpnPwHwListEntry)=u4VplsIndex;
	L2VPN_PW_HW_LIST_PW_INDEX(pL2VpnPwHwListEntry)  =u4PwIndex;
	L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(pL2VpnPwHwListEntry) =1;
	L2VPN_PW_HW_LIST_IN_LABEL(pL2VpnPwHwListEntry)  = 1;
	L2VPN_PW_HW_LIST_LSP_OUT_LABEL(pL2VpnPwHwListEntry)= 1;
	L2VPN_PW_HW_LIST_OUT_LABEL(pL2VpnPwHwListEntry) =1;
	L2VPN_PW_HW_LIST_REMOTE_VE_ID(pL2VpnPwHwListEntry)= 0;
	L2VPN_PW_HW_LIST_OUT_IF_INDEX(pL2VpnPwHwListEntry)= 1;
	L2VPN_PW_HW_LIST_LSP_IN_LABEL(pL2VpnPwHwListEntry)= 1;
	L2VPN_PW_HW_LIST_IN_IF_INDEX(pL2VpnPwHwListEntry) = 1;
	L2VPN_PW_HW_LIST_NPAPI_STATUS(pL2VpnPwHwListEntry)= 0;
	L2VPN_PW_HW_LIST_STALE_STATUS(pL2VpnPwHwListEntry)= 2;
	L2VPN_PW_HW_LIST_OUT_PORT(pL2VpnPwHwListEntry)= 1;
	L2VPN_PW_HW_LIST_IS_STATIC_PW(pL2VpnPwHwListEntry) = 0;
}

VOID CreatePwCli()
{

/*	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("switch default");
	CliExecuteAppCmd("vlan 2");
	CliExecuteAppCmd("ports gigabitethernet 0/1");
	CliExecuteAppCmd("exit");
	CliExecuteAppCmd("exit");
	CliExecuteAppCmd("interface vlan 2");
	CliExecuteAppCmd("shutdown");
	CliExecuteAppCmd("ip address 20.0.0.1 255.0.0.0");
	CliExecuteAppCmd("mpls ip");
	CliExecuteAppCmd("no shutdown");
	CliExecuteAppCmd("end");
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("mpls ldp");
	CliExecuteAppCmd("router-id loopback 0 force");
	CliExecuteAppCmd("entity 1");
	CliExecuteAppCmd("holdtime 120");
	CliExecuteAppCmd("discovery hello holdtime 60");
	CliExecuteAppCmd("ldp label range min 100 max 199 interface vlan 2");
	CliExecuteAppCmd("label distribution unsolicited");
	CliExecuteAppCmd("label retention liberal");
	CliExecuteAppCmd("transport-address tlv loopback 0");
	CliExecuteAppCmd("no shutdown");
	CliExecuteAppCmd("end");*/
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("mpls ldp");
	CliExecuteAppCmd("entity 3");
	CliExecuteAppCmd("neighbor 33.33.33.33 targeted");
	CliExecuteAppCmd("ldp label range min 160200 max 160399 interface vlan 2");
	CliExecuteAppCmd("transport-address tlv loopback 0");
	CliExecuteAppCmd("label distribution unsolicited");
	CliExecuteAppCmd("label retention liberal");
	CliExecuteAppCmd("no shutdown");
	CliExecuteAppCmd("end");
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("switch default");
	CliExecuteAppCmd("vlan 100");
	CliExecuteAppCmd("mpls l2transport pwidfec 33.33.33.33 pwid 1 groupid 1 mplstype non-te");
	CliExecuteAppCmd("end");
	
}
#endif
