/********************************************************************
  *** Copyright (C) 2010 Aricent Inc . All Rights Reserved
  ***
  *** $Id: l2vpnhautcli.c,v 1.1 2014/08/25 12:23:30 siva Exp $
  ***
  *** Description: TLDP-GR HA UT Test cases cli file.
  *** NOTE: This file should not be include in release packaging.
***************************************************************************/


#include "l2vpincs.h"
#include "mplscli.h"

#define MAX_ARGS 5

extern VOID  L2VpnHaExecUnitTest (tCliHandle CliHandle, INT4 i4TestId);
extern VOID  CallL2VpnHaUTCases(tCliHandle CliHandle);


UINT4  u4TotalPassCase = 0;
UINT4  u4TotalFailCase = 0;


INT4
cli_process_l2vpn_ha_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...);

INT4 L2VpnHaUt(tCliHandle CliHandle, INT4 i4TestType);

INT4
cli_process_l2vpn_ha_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...)
{
        va_list            ap;
        UINT4             *args[MAX_ARGS];
        INT1               argno = 0;
        INT4               i4TestcaseNo ;
        va_start (ap, u4Command);
        /* Walk through the arguements and store in args array.
 *          *  *      * */
        while (1)
        {
                args[argno++] = va_arg (ap, UINT4 *);
                if (argno == MAX_ARGS)
                        break;
        }
        va_end (ap);

        u4TotalPassCase = 0;
        u4TotalFailCase = 0;

        switch (u4Command)
        {
                case CLI_L2VPN_HA_UT:

                        if (args[1] == NULL)
                        {
                                i4TestcaseNo = 0;
                        }
                          else
                        {
                                i4TestcaseNo = *args[1];
                        }
                        L2VpnHaUt(CliHandle, i4TestcaseNo);
                        CliPrintf(CliHandle,"\n Total passed: %d \n", u4TotalPassCase);
                        CliPrintf(CliHandle,"\n Total failed: %d \n", u4TotalFailCase);
                        break;

        }
        return CLI_SUCCESS;
}


INT4
L2VpnHaUt(tCliHandle CliHandle, INT4 i4TestType)
{
    if(i4TestType == 0)
    {
        /* Calls all the test cases */
        CallL2VpnHaUTCases(CliHandle);
        return CLI_SUCCESS;
    }
    else
    {   /* Calls the particular test case only */
        L2VpnHaExecUnitTest(CliHandle, i4TestType);
        return CLI_SUCCESS;
    }
}

