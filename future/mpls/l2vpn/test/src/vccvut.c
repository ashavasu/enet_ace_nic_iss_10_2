/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: vccvut.c,v 1.5 2013/12/18 12:50:11 siva Exp $
 **
 ** Description: MPLS API  UT Test cases.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/
#ifndef __VCCV_UNIT_TEST_C__
#define __VCCV_UNIT_TEST_C__

#include "l2vpincs.h"
#include "iss.h"

#define VCCV_MAX_UT_CASES 60

/* As per L2VPN code, values 1 to 5 are assigned to other modes of PW. Hence used the value 6 */
/* L2VPN_PWVC_OWNER_MANUAL - 1
L2VPN_PWVC_OWNER_PWID_FEC_SIG - 2
L2VPN_PWVC_OWNER_GEN_FEC_SIG - 3
*/
#define PWVC_GEN_FEC_TYPE2 6
#define PWVC_MANUAL_WITHOUT_PWID 7
#define PWVC_GEN_FEC_TYPE1_WITHOUT_PWID 8
#define PWVC_GEN_FEC_TYPE2_PWID 9
#define PWVC_GEN_FEC_TYPE1_WITH_GLOB_NODE_ID 10
#define PWVC_GEN_FEC_TYPE2_WITH_PEER_ADDR 11
#define PWVC_GEN_FEC_TYPE2_WITH_VCCV_DIS 12
#define PWVC_GEN_FEC_TYPE2_WITH_CW_DIS 13
#define PWVC_GEN_FEC_TYPE2_WITH_CW_ENA 14

#define VCCV_ALL_CC_TYPES  (L2VPN_VCCV_CC_ACH | L2VPN_VCCV_CC_RAL | L2VPN_VCCV_CC_TTL_EXP)
#define VCCV_ALL_CV_TYPES (L2VPN_VCCV_CV_LSPP | L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY | L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS | L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY | L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS)
#define VCCV_CV_TYPE_BFD_LSPP  (L2VPN_VCCV_CV_LSPP | L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS)

#define BIT2_SET 0x20            /* BIT 2: 0x20 - bit 0 1 2 3 4 5 6 7 8 - 0 starts from MSB */
#define BIT3_SET 0x10            /* BIT 3: 0x10 */
#define BIT6_SET 0x02            /* BIT 7: 0x02 */

/* MIB objects - macros used for verification */
enum
{
    VCCV_SCALAR_CC = 1,
    VCCV_SCALAR_CV,
    VCCV_SCALAR_HW_CC,
    VCCV_SCALAR_PW_STATUS_NOTIFICATION,
    VCCV_SCALAR_OAM_STATUS_NOTIFICATION,
    VCCV_SCALAR_TRC,
    L2VPN_ADMIN_STS,
    L2VPN_PW_ROW_STATUS,
    VCCV_PW_LCL_CAPAB_ADVERT,
    VCCV_PW_LOCAL_CC_ADVERT,
    VCCV_PW_LOCAL_CV_ADVERT,
    VCCV_PW_REMOTE_CC_ADVERT,
    VCCV_PW_REMOTE_CV_ADVERT,
    VCCV_PW_LCL_CC_SELECTED,
    VCCV_PW_LCL_CV_SELECTED,
    VCCV_PW_OAM_STS,
    VCCV_PW_GEN_AGI_TYPE,
    VCCV_PW_GEN_LCL_AII_TYPE,
    VCCV_PW_GEN_REMOTE_AII_TYPE,
    VCCV_PW_SSN_IDX
};

VOID                CallVccvUTCases (VOID);
VOID                VccvExecUnitTest (INT4 i4TestId);
VOID                CreateConfPw (UINT4 u4PwMode);
VOID                VerifyPwOperStsCli (VOID);
UINT4               VerifyPwOperStsNmh (VOID);
UINT4               VerifyObject (UINT4 u4Object, UINT4 u4ValToVerify);
UINT4               CallTestRoutine (UINT4 u4Object, INT4 u4TestVal,
                                     tSNMP_OCTET_STRING_TYPE * pOctetStr);
UINT4               CallSetRoutine (UINT4 u4Object, UINT4 u4SetVal);
UINT4               CallGetRoutine (UINT4 u4Object, UINT4 u4ValObtained,
                                    tSNMP_OCTET_STRING_TYPE * OctetStr);
VOID                ConfPreReq (VOID);
VOID                ConfManualPw (VOID);
VOID                ConfPwIdPw (VOID);
VOID                ConfGenType1Pw (VOID);
VOID                ConfGenType2Pw (VOID);
VOID                ConfManualPwWithoutPwId (VOID);
VOID                ConfGenType1PwWithoutPwId (VOID);
VOID                ConfGenType1PwWithGlobNodeId (VOID);
VOID                ConfGenType2PwWithoutPwId (VOID);
VOID                ConfGenType2PwWithPeerAddr (VOID);
VOID                ConfGenType2PwWithVccvDisabled (VOID);
VOID                ConfGenType2PwWithCwEnabled (VOID);
VOID                ConfGenType2PwWithCwDisabled (VOID);
VOID                ConfPwOamParms (VOID);
VOID                ConfPwOamStatus (VOID);
VOID                DeletePreReq (VOID);
VOID                DeletePw (UINT4 u4PwMode);

VOID
CallVccvUTCases (VOID)
{
    INT4                i4Count = 1;

    for (i4Count = 1; i4Count <= VCCV_MAX_UT_CASES; i4Count++)
    {
        VccvExecUnitTest (i4Count);
        sleep (2);
    }
}

VOID
VccvExecUnitTest (INT4 i4UtId)
{

    UINT4               u4RetVal = OSIX_FAILURE;
    UINT4               u4RetVal1 = OSIX_FAILURE;
    UINT4               u4RetVal2 = OSIX_FAILURE;
    UINT4               u4ValToVerify = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    static UINT1        u1Val = 0;
    UINT4               u4TestVal = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    OctetStr.pu1_OctetList = &u1Val;

    switch (i4UtId)
    {
        case 1:
            PRINTF ("TestCaseId %d: To verify for successful creation of manual"
                    " mode of PW without OAM parameters configured - to verify"
                    " backward compatibility\r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 2:
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "129 FEC Type 2 without OAM parameters configured\r\n",
                    i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 3:
            PRINTF ("TestCaseId %d: To verify for successful creation of manual"
                    " mode of PW with OAM parameters configured\r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            ConfPwOamParms ();
            ConfPwOamStatus ();
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            if (u4RetVal == OSIX_SUCCESS)
            {
                u4RetVal1 = VerifyObject (VCCV_PW_LOCAL_CC_ADVERT,
                                          VCCV_ALL_CC_TYPES);
                u4RetVal2 = VerifyObject (VCCV_PW_LOCAL_CV_ADVERT,
                                          VCCV_ALL_CV_TYPES);
                if ((u4RetVal1 == OSIX_FAILURE) || (u4RetVal2 == OSIX_FAILURE))
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (L2VPN_PWVC_OWNER_MANUAL);
                    break;
                }
                u4RetVal1 = VerifyObject (VCCV_PW_REMOTE_CC_ADVERT,
                                          VCCV_ALL_CC_TYPES);
                u4RetVal2 = VerifyObject (VCCV_PW_REMOTE_CV_ADVERT,
                                          VCCV_ALL_CV_TYPES);
                if ((u4RetVal1 == OSIX_FAILURE) || (u4RetVal2 == OSIX_FAILURE))
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (L2VPN_PWVC_OWNER_MANUAL);
                    break;
                }
                if (VerifyObject (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE) ==
                    OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (L2VPN_PWVC_OWNER_MANUAL);
                    break;
                }
            }
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 4:
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "129 FEC Type 2 with OAM parameters configured\r\n",
                    i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            ConfPwOamParms ();
            ConfPwOamStatus ();
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            if (u4RetVal == OSIX_SUCCESS)
            {
                u4RetVal1 = VerifyObject (VCCV_PW_LOCAL_CC_ADVERT,
                                          VCCV_ALL_CC_TYPES);
                u4RetVal2 = VerifyObject (VCCV_PW_LOCAL_CV_ADVERT,
                                          VCCV_ALL_CV_TYPES);
                if ((u4RetVal1 == OSIX_FAILURE) || (u4RetVal2 == OSIX_FAILURE))
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (PWVC_GEN_FEC_TYPE2);
                    break;
                }
                u4RetVal1 = VerifyObject (VCCV_PW_REMOTE_CC_ADVERT,
                                          VCCV_ALL_CC_TYPES);
                u4RetVal2 = VerifyObject (VCCV_PW_REMOTE_CV_ADVERT,
                                          VCCV_ALL_CV_TYPES);
                if ((u4RetVal1 == OSIX_FAILURE) || (u4RetVal2 == OSIX_FAILURE))
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (PWVC_GEN_FEC_TYPE2);
                    break;
                }
                if (VerifyObject (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE) ==
                    OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (PWVC_GEN_FEC_TYPE2);
                    break;
                }
                if (VerifyObject (VCCV_PW_GEN_AGI_TYPE, 1) == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (PWVC_GEN_FEC_TYPE2);
                    break;
                }
                if (VerifyObject (VCCV_PW_GEN_LCL_AII_TYPE, 2) == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (PWVC_GEN_FEC_TYPE2);
                    break;
                }
                if (VerifyObject (VCCV_PW_GEN_REMOTE_AII_TYPE, 2) ==
                    OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                    DeletePw (PWVC_GEN_FEC_TYPE2);
                    break;
                }
            }
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 5:
            PRINTF ("TestCaseId %d: To verify configuration of scalar CC type "
                    "configuration\r\n", i4UtId);
            CliTakeAppContext ();
            MGMT_UNLOCK ();
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd
                ("pw-cc-capability pw-ach router-alert-label ttl-expiry");
            CliGiveAppContext ();
            MGMT_LOCK ();
            u4ValToVerify = VCCV_ALL_CC_TYPES;
            u4RetVal = VerifyObject (VCCV_SCALAR_CC, u4ValToVerify);
            break;

        case 6:
            PRINTF ("TestCaseId %d: To verify configuration of scalar CC type "
                    "configuration\r\n", i4UtId);
            CliTakeAppContext ();
            MGMT_UNLOCK ();
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd
                ("pw-cv-capability lsp-ping bfd-ip-encap-fault bfd-ip-encap-fault-status-notify bfd-ach-encap-fault bfd-ach-encap-fault-status-notify");
            CliGiveAppContext ();
            MGMT_LOCK ();
            u4ValToVerify = VCCV_ALL_CV_TYPES;
            u4RetVal = VerifyObject (VCCV_SCALAR_CV, u4ValToVerify);
            break;

        case 7:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnTrcFlag when an invalid value"
                    " of less then min is given when invalid value of "
                    "< min val is given\r\n", i4UtId);
            u4TestVal = -1;
            u4RetVal = CallTestRoutine (VCCV_SCALAR_TRC, u4TestVal, NULL);
            break;

        case 8:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnTrcFlag when an invalid value of"
                    " > max val is given\r\n", i4UtId);
            u4TestVal = L2VPN_DBG_ALL_FLAG + 1;
            u4RetVal = CallTestRoutine (VCCV_SCALAR_TRC, u4TestVal, NULL);
            break;

        case 9:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert when global "
                    "PW admin status is disabled\r\n", i4UtId);
            u4RetVal = CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_DOWN);
            if (u4RetVal == OSIX_FAILURE)
            {
                PRINTF ("CallSetRoutine for L2VPN admin status failed!!!\r\n");
            }

            u4RetVal =
                CallTestRoutine (VCCV_PW_LCL_CAPAB_ADVERT, -1, &OctetStr);

            /* clean-up */
            if (CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_UP) ==
                OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status UP failed!!!\r\n");
            }
            break;

        case 10:
            PRINTF ("TestCaseId %d: To verify the functionality of"
                    " nmhTestv2FsMplsL2VpnPwLocalCapabAdvert when invalid "
                    "length is given in octet string\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.i4_Length = 0;
            u4RetVal =
                CallTestRoutine (VCCV_PW_LCL_CAPAB_ADVERT, -1, &OctetStr);
            break;

        case 11:
            PRINTF ("TestCaseId %d: To verify the functionality of"
                    " nmhTestv2FsMplsL2VpnPwLocalCapabAdvert when PW entry "
                    "does not exist\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (L2VPN_PW_STATUS_INDICATION |
                                         L2VPN_PW_VCCV_CAPABLE);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_LCL_CAPAB_ADVERT, -1, &OctetStr);
            break;

        case 12:
            PRINTF ("TestCaseId %d: To verify the functionality of"
                    " nmhTestv2FsMplsL2VpnPwLocalCapabAdvert when PW row status "
                    "is active \r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] =
                (UINT1) (L2VPN_PW_STATUS_INDICATION | L2VPN_PW_VCCV_CAPABLE);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_LCL_CAPAB_ADVERT, -1, &OctetStr);
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 13:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnPwLocalCapabAdvert when invalid value "
                    "is set \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            CallSetRoutine (L2VPN_PW_ROW_STATUS, L2VPN_PWVC_NOTINSERVICE);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = L2VPN_PW_STATUS_INDICATION |
                L2VPN_PW_VCCV_CAPABLE | BIT2_SET;
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_LCL_CAPAB_ADVERT, -1, &OctetStr);
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 14:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert when global PW admin "
                    "status is disabled\r\n", i4UtId);
            u4RetVal = CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_DOWN);
            if (u4RetVal == OSIX_FAILURE)
            {
                PRINTF ("CallSetRoutine for L2VPN admin status failed!!!\r\n");
            }

            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CC_ADVERT, -1, &OctetStr);

            /* clean-up */
            if (CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_UP) ==
                OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status UP failed!!!\r\n");
            }
            break;

        case 15:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    " nmhTestv2FsMplsL2VpnPwLocalCCAdvert when invalid length "
                    "is given in octet string\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.i4_Length = 0;
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CC_ADVERT, -1, &OctetStr);
            break;

        case 16:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert when PW entry does "
                    "not exist\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CC_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CC_ADVERT, -1, &OctetStr);
            break;

        case 17:
            PRINTF ("TestCaseId %d: To verify the functionality of "
                    "nmhTestv2FsMplsL2VpnPwLocalCCAdvert when PW row status "
                    "is active \r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CC_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CC_ADVERT, -1, &OctetStr);
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 18:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwLocalCCAdvert"
                 " when invalid value is set \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            CallSetRoutine (L2VPN_PW_ROW_STATUS, L2VPN_PWVC_NOTINSERVICE);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CC_TYPES | BIT3_SET);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CC_ADVERT, -1, &OctetStr);
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 19:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwLocalCVAdvert"
                 " when global PW admin status is disabled\r\n", i4UtId);
            u4RetVal = CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_DOWN);
            if (u4RetVal == OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status DOWN failed!!!\r\n");
            }
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CV_ADVERT, -1, &OctetStr);

            /* clean-up */
            if (CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_UP) ==
                OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status UP failed!!!\r\n");
            }
            break;

        case 20:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwLocalCVAdvert"
                 " when invalid length is given in octet string\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.i4_Length = 0;
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CV_ADVERT, -1, &OctetStr);
            break;

        case 21:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwLocalCVAdvert"
                 " when PW entry does not exist\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CV_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CV_ADVERT, -1, &OctetStr);
            break;

        case 22:
            PRINTF ("TestCaseId %d: To verify the functionality of"
                    " nmhTestv2FsMplsL2VpnPwLocalCVAdvert when PW row status "
                    "is active \r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CV_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CV_ADVERT, -1, &OctetStr);
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 23:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwLocalCVAdvert"
                 " when invalid value is set \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            CallSetRoutine (L2VPN_PW_ROW_STATUS, L2VPN_PWVC_NOTINSERVICE);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CV_TYPES | BIT6_SET);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal = CallTestRoutine (VCCV_PW_LOCAL_CV_ADVERT, -1, &OctetStr);
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 24:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCCAdvert"
                 " when global PW admin status is disabled\r\n", i4UtId);
            u4RetVal = CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_DOWN);
            if (u4RetVal == OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status DOWN failed!!!\r\n");
            }
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CC_ADVERT, -1, &OctetStr);

            /* clean-up */
            if (CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_UP) ==
                OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status UP failed!!!\r\n");
            }
            break;

        case 25:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCCAdvert"
                 " when invalid length is given in octet string\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.i4_Length = 0;
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CC_ADVERT, -1, &OctetStr);
            break;

        case 26:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCCAdvert"
                 " when PW entry does not exist\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CC_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CC_ADVERT, -1, &OctetStr);
            break;

        case 27:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCCAdvert"
                 " when PW row status is active \r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CC_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CC_ADVERT, -1, &OctetStr);
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 28:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCCAdvert"
                 " when invalid value is set \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            CallSetRoutine (L2VPN_PW_ROW_STATUS, L2VPN_PWVC_NOTINSERVICE);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CC_TYPES | BIT3_SET);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CC_ADVERT, -1, &OctetStr);
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 29:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCVAdvert"
                 " when global PW admin status is disabled\r\n", i4UtId);
            u4RetVal = CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_DOWN);
            if (u4RetVal == OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status UP failed!!!\r\n");
            }
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CV_ADVERT, -1, &OctetStr);

            /* clean-up */
            if (CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_UP) ==
                OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status DOWN failed!!!\r\n");
            }
            break;

        case 30:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCVAdvert"
                 " when invalid length is given in octet string\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.i4_Length = 0;
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CV_ADVERT, -1, &OctetStr);
            break;

        case 31:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCVAdvert"
                 " when PW entry does not exist\r\n", i4UtId);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CV_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CV_ADVERT, -1, &OctetStr);
            break;

        case 32:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCVAdvert"
                 " when PW row status is active \r\n", i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_MANUAL);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CV_TYPES);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CV_ADVERT, -1, &OctetStr);
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 33:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwRemoteCVAdvert"
                 " when invalid value is set \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            CallSetRoutine (L2VPN_PW_ROW_STATUS, L2VPN_PWVC_NOTINSERVICE);
            OctetStr.pu1_OctetList = &u1Val;
            OctetStr.pu1_OctetList[0] = (UINT1) (VCCV_ALL_CV_TYPES | BIT6_SET);
            OctetStr.i4_Length = sizeof (UINT1);
            u4RetVal =
                CallTestRoutine (VCCV_PW_REMOTE_CV_ADVERT, -1, &OctetStr);
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 34:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwOamEnable"
                 " when global PW admin status is disabled\r\n", i4UtId);
            u4RetVal = CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_DOWN);
            if (u4RetVal == OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status UP failed!!!\r\n");
            }
            u4RetVal = CallTestRoutine (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE, NULL);
            /* clean-up */
            if (CallSetRoutine (L2VPN_ADMIN_STS, L2VPN_ADMIN_UP) ==
                OSIX_FAILURE)
            {
                PRINTF
                    ("CallSetRoutine for L2VPN admin status DOWN failed!!!\r\n");
            }
            break;

        case 35:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwOamEnable"
                 " when invalid value other than Truth val is given (0) \r\n",
                 i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            /* valid truth val: 2-True, 1-false */
            u4RetVal = CallTestRoutine (VCCV_PW_OAM_STS, 0, NULL);
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 36:
            PRINTF
                ("TestCaseId %d: To verify the functionality of nmhTestv2FsMplsL2VpnPwOamEnable"
                 " when PW entry is not present \r\n", i4UtId);
            /* valid truth val: 2-True, 1-false */
            u4RetVal = CallTestRoutine (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE, NULL);
            break;

        case 37:
            PRINTF
                ("TestCaseId %d: To verify the functionality of trap notification enable & disable\r\n",
                 i4UtId);
            CliTakeAppContext ();
            MGMT_UNLOCK ();
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("pseudowire-notification pw-status");
            CliExecuteAppCmd ("pseudowire-notification pw-oam-status");
            CliGiveAppContext ();
            MGMT_LOCK ();

            u4RetVal1 =
                VerifyObject (VCCV_SCALAR_PW_STATUS_NOTIFICATION,
                              MPLS_SNMP_TRUE);
            u4RetVal2 =
                VerifyObject (VCCV_SCALAR_OAM_STATUS_NOTIFICATION,
                              MPLS_SNMP_TRUE);
            if ((u4RetVal1 == OSIX_SUCCESS) && (u4RetVal2 == OSIX_SUCCESS))
            {
                u4RetVal = OSIX_SUCCESS;
            }
            else
            {
                PRINTF
                    ("ERROR: PW status notification or OAM status notification enable FAILED !!!\r\n");
            }

            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            /* Verify trap generation manually when PW oper sts is UP/DOWN */
            VerifyPwOperStsCli ();
            DeletePw (PWVC_GEN_FEC_TYPE2);

            CliTakeAppContext ();
            MGMT_UNLOCK ();
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd
                ("no pseudowire-notification pw-status pw-oam-status");
            CliGiveAppContext ();
            MGMT_LOCK ();

            u4RetVal1 =
                VerifyObject (VCCV_SCALAR_PW_STATUS_NOTIFICATION,
                              MPLS_SNMP_FALSE);
            u4RetVal2 =
                VerifyObject (VCCV_SCALAR_OAM_STATUS_NOTIFICATION,
                              MPLS_SNMP_FALSE);
            if ((u4RetVal1 == OSIX_SUCCESS) && (u4RetVal2 == OSIX_SUCCESS))
            {
                u4RetVal = OSIX_SUCCESS;
            }
            else
            {
                PRINTF
                    ("ERROR: PW status notification or OAM status notification disable FAILED !!!\r\n");
            }
            break;

        case 38:
            /* Need a remote peer for this test case to verify the PW oper status */
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "128 FEC PW creation without OAM parameters configured\r\n",
                    i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_PWID_FEC_SIG);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (L2VPN_PWVC_OWNER_PWID_FEC_SIG);
            break;

        case 39:
            /* Need a remote peer for this test case to verify the PW oper status */
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "129 FEC - Type 1 PW creation without OAM parameters configured\r\n",
                    i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_GEN_FEC_SIG);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (L2VPN_PWVC_OWNER_GEN_FEC_SIG);
            break;

        case 40:
            /* Need a remote peer for this test case to verify the PW oper status */
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "128 FEC PW creation with OAM parameters configured\r\n",
                    i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_PWID_FEC_SIG);
            ConfPwOamParms ();
            ConfPwOamStatus ();
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (L2VPN_PWVC_OWNER_PWID_FEC_SIG);
            break;

        case 41:
            /* Need a remote peer for this test case to verify the PW oper status */
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "129 FEC Type 1 with OAM parameters configured\r\n",
                    i4UtId);
            CreateConfPw (L2VPN_PWVC_OWNER_GEN_FEC_SIG);
            ConfPwOamParms ();
            ConfPwOamStatus ();
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (L2VPN_PWVC_OWNER_GEN_FEC_SIG);
            break;

        case 42:
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "manual PW with pw-id not configured \r\n", i4UtId);
            CreateConfPw (PWVC_MANUAL_WITHOUT_PWID);
            VerifyPwOperStsCli ();
            /* To verify the pw-id auto-generation manually */
            u4RetVal = VerifyPwOperStsNmh ();
            /* Delete the PW manually. pw-id is auto-generated in this case, use show cmd to get the pwid auto-generated value and delete the PW manually */
            DeletePw (L2VPN_PWVC_OWNER_MANUAL);
            break;

        case 43:
            /* Need a remote peer for this test case to berify the oper status */
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "GEN FEC - Type 1 with pw-id not configured \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE1_WITHOUT_PWID);
            VerifyPwOperStsCli ();
            /* To verify the pw-id auto-generation manually */
            u4RetVal = VerifyPwOperStsNmh ();
            /* Delete the PW manually. pw-id is auto-generated in this case, use show cmd to get the pwid auto-generated value and delete the PW manually */
            DeletePw (L2VPN_PWVC_OWNER_GEN_FEC_SIG);
            break;

        case 44:
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "GEN FEC - Type 2 with pw-id not configured \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2_PWID);
            VerifyPwOperStsCli ();
            /* To verify the pw-id auto-geneation manually */
            u4RetVal = VerifyPwOperStsNmh ();
            /* Delete the PW manually. As the pw-id is auto-generated in this case, verify it through CLI command and delete the PW */
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;

        case 45:
            PRINTF
                ("TestCaseId %d: To verify for wrong configuration sequence of "
                 "GEN FEC - Type 1 PW with global id, node id configured \n",
                 i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE1_WITH_GLOB_NODE_ID);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            if (u4RetVal == OSIX_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            DeletePw (PWVC_GEN_FEC_TYPE1_WITH_GLOB_NODE_ID);
            break;

        case 46:
            PRINTF
                ("TestCaseId %d: To verify for wrong configuration sequence of "
                 "GEN FEC - Type 2 PW with PeerAddr configured \n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2_WITH_PEER_ADDR);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            if (u4RetVal == OSIX_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            DeletePw (PWVC_GEN_FEC_TYPE2_WITH_PEER_ADDR);
            break;

        case 47:
            PRINTF ("TestCaseId %d: To verify for successful creation of "
                    "GEN FEC - Type 2 with VCCV disabled\r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2_WITH_VCCV_DIS);
            VerifyPwOperStsCli ();
            u4RetVal = VerifyPwOperStsNmh ();
            DeletePw (PWVC_GEN_FEC_TYPE2);
            break;
        case 48:
            PRINTF ("TestCaseId %d: To verify for successful enable/disable of "
                    "various trace options \r\n", i4UtId);
            CallSetRoutine (VCCV_SCALAR_TRC, L2VPN_DBG_VCCV_CR_TRC);
            u4RetVal1 = VerifyObject (VCCV_SCALAR_TRC, L2VPN_DBG_VCCV_CR_TRC);
            CallSetRoutine (VCCV_SCALAR_TRC, L2VPN_DBG_VCCV_MGMT_TRC);
            u4RetVal2 = VerifyObject (VCCV_SCALAR_TRC, L2VPN_DBG_VCCV_MGMT_TRC);
            CallSetRoutine (VCCV_SCALAR_TRC, L2VPN_DBG_VCCV_CAPAB_EXG_TRC);
            u4RetVal =
                VerifyObject (VCCV_SCALAR_TRC, L2VPN_DBG_VCCV_CAPAB_EXG_TRC);
            if (((u4RetVal1 == OSIX_SUCCESS) && (u4RetVal2 == OSIX_SUCCESS))
                || (u4RetVal == OSIX_SUCCESS))
            {
                u4RetVal = OSIX_SUCCESS;
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            CallSetRoutine (VCCV_SCALAR_TRC, L2VPN_DBG_NONE_FLAG);
            break;
        case 49:
            PRINTF ("TestCaseId %d: To verify for successful enable/disable of "
                    "control word on GEN FEC - Type 2 PW \r\n", i4UtId);
            CreateConfPw (PWVC_GEN_FEC_TYPE2_WITH_CW_DIS);
            VerifyPwOperStsCli ();
            u4RetVal1 = VerifyPwOperStsNmh ();
            DeletePw (PWVC_GEN_FEC_TYPE2);
            CreateConfPw (PWVC_GEN_FEC_TYPE2_WITH_CW_ENA);
            VerifyPwOperStsCli ();
            u4RetVal2 = VerifyPwOperStsNmh ();
            DeletePw (PWVC_GEN_FEC_TYPE2);
            if ((u4RetVal1 == OSIX_SUCCESS) && (u4RetVal2 == OSIX_SUCCESS))
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case 50:
            PRINTF ("TestCaseId %d: To verify for successful operation of"
                    "nmhGet routines for Gen Fec - Type 2 PW \r\n", i4UtId);
            /* GET of all scalar objects */
            u4ValToVerify = VCCV_ALL_CC_TYPES;
            u4RetVal = VerifyObject (VCCV_SCALAR_CC, u4ValToVerify);
            u4ValToVerify = L2VPN_VCCV_CV_LSPP;
            u4RetVal = VerifyObject (VCCV_SCALAR_CV, u4ValToVerify);

            /* HW CC type should be modified based on the HW CC capability. */
            u4RetVal = VerifyObject (VCCV_SCALAR_HW_CC, 0);
            u4RetVal =
                VerifyObject (VCCV_SCALAR_PW_STATUS_NOTIFICATION,
                              MPLS_SNMP_FALSE);
            u4RetVal =
                VerifyObject (VCCV_SCALAR_OAM_STATUS_NOTIFICATION,
                              MPLS_SNMP_FALSE);

            CreateConfPw (PWVC_GEN_FEC_TYPE2);
            ConfPwOamParms ();

            /* GET of all tabular objects when PW VC entry is present */
            /* u4RetVal of all objects should be OSIX_SUCCESS. To be verified
             * manually */
            u4RetVal = VerifyObject (VCCV_PW_LCL_CAPAB_ADVERT,
                                     (L2VPN_PW_STATUS_INDICATION
                                      | L2VPN_PW_VCCV_CAPABLE));
            u4RetVal = VerifyObject (VCCV_PW_LCL_CC_SELECTED,
                                     L2VPN_VCCV_CC_ACH);
            u4RetVal = VerifyObject (VCCV_PW_LCL_CV_SELECTED,
                                     VCCV_CV_TYPE_BFD_LSPP);
            u4RetVal = VerifyObject (VCCV_PW_LOCAL_CC_ADVERT,
                                     VCCV_ALL_CC_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_LOCAL_CV_ADVERT,
                                     VCCV_ALL_CV_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_REMOTE_CC_ADVERT,
                                     VCCV_ALL_CC_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_REMOTE_CV_ADVERT,
                                     VCCV_ALL_CV_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE);
            u4RetVal = VerifyObject (VCCV_PW_GEN_AGI_TYPE, 1);
            u4RetVal = VerifyObject (VCCV_PW_GEN_LCL_AII_TYPE, 2);
            u4RetVal = VerifyObject (VCCV_PW_GEN_REMOTE_AII_TYPE, 2);

            /* Proactive session index should be verified against 0 
             * as this testing is done without a peer */
            u4RetVal = VerifyObject (VCCV_PW_SSN_IDX, 0);

            DeletePw (PWVC_GEN_FEC_TYPE2);

            /* GET of all tabular objects when PW VC entry is not available */
            /* u4RetVal of all objects should be OSIX_FAILURE. To be verified 
             * manually  */
            u4RetVal = VerifyObject (VCCV_PW_LCL_CAPAB_ADVERT,
                                     (L2VPN_PW_STATUS_INDICATION
                                      | L2VPN_PW_VCCV_CAPABLE));
            u4RetVal = VerifyObject (VCCV_PW_LCL_CC_SELECTED,
                                     L2VPN_VCCV_CC_ACH);
            u4RetVal = VerifyObject (VCCV_PW_LCL_CV_SELECTED,
                                     VCCV_CV_TYPE_BFD_LSPP);
            u4RetVal = VerifyObject (VCCV_PW_LOCAL_CC_ADVERT,
                                     VCCV_ALL_CC_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_LOCAL_CV_ADVERT,
                                     VCCV_ALL_CV_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_REMOTE_CC_ADVERT,
                                     VCCV_ALL_CC_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_REMOTE_CV_ADVERT,
                                     VCCV_ALL_CV_TYPES);
            u4RetVal = VerifyObject (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE);
            u4RetVal = VerifyObject (VCCV_PW_GEN_AGI_TYPE, 1);
            u4RetVal = VerifyObject (VCCV_PW_GEN_LCL_AII_TYPE, 2);
            u4RetVal = VerifyObject (VCCV_PW_GEN_REMOTE_AII_TYPE, 2);
            u4RetVal = VerifyObject (VCCV_PW_SSN_IDX, 1);
            u4RetVal = OSIX_SUCCESS;
            break;

        case 51:
            PRINTF ("TestCaseId %d: To verify for successful operation of "
                    "nmhSet routines when PW entry does not exist \r\n",
                    i4UtId);
            /* To verify anually: u4RetVal of all function calls should 
             * be OSIX_FAILURE */
            u4RetVal = CallSetRoutine (VCCV_PW_LCL_CAPAB_ADVERT,
                                       L2VPN_PW_STATUS_INDICATION |
                                       L2VPN_PW_VCCV_CAPABLE);
            u4RetVal =
                CallSetRoutine (VCCV_PW_LOCAL_CC_ADVERT, VCCV_ALL_CC_TYPES);
            u4RetVal =
                CallSetRoutine (VCCV_PW_LOCAL_CV_ADVERT, VCCV_ALL_CV_TYPES);
            u4RetVal =
                CallSetRoutine (VCCV_PW_REMOTE_CC_ADVERT, VCCV_ALL_CC_TYPES);
            u4RetVal =
                CallSetRoutine (VCCV_PW_REMOTE_CV_ADVERT, VCCV_ALL_CV_TYPES);
            u4RetVal = CallSetRoutine (VCCV_PW_OAM_STS, MPLS_SNMP_TRUE);
            u4RetVal = CallSetRoutine (VCCV_PW_GEN_AGI_TYPE, 1);
            u4RetVal = CallSetRoutine (VCCV_PW_GEN_LCL_AII_TYPE, 2);
            u4RetVal = CallSetRoutine (VCCV_PW_GEN_REMOTE_AII_TYPE, 2);
            u4RetVal = OSIX_SUCCESS;
            break;

        default:
            PRINTF ("%% No test case is defined for ID: %d\r\n", i4UtId);
            break;
    }

    /* Display the result */
    if (u4RetVal == OSIX_FAILURE)
    {
        printf ("!!!!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!!!!\r\n", i4UtId,
                "FAILED");
    }
    else
    {
        printf ("$$$$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$$$$\r\n", i4UtId,
                "PASSED");
    }
}

VOID
CreateConfPw (UINT4 u4PwMode)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    ConfPreReq ();

    switch (u4PwMode)
    {
        case L2VPN_PWVC_OWNER_MANUAL:
            ConfManualPw ();
            break;
        case L2VPN_PWVC_OWNER_PWID_FEC_SIG:
            ConfPwIdPw ();
            /* sleep (2); */
            break;
        case L2VPN_PWVC_OWNER_GEN_FEC_SIG:
            ConfGenType1Pw ();
            /* sleep (2); */
            break;
        case PWVC_GEN_FEC_TYPE2:
            ConfGenType2Pw ();
            break;
        case PWVC_MANUAL_WITHOUT_PWID:
            ConfManualPwWithoutPwId ();
            break;
        case PWVC_GEN_FEC_TYPE1_WITHOUT_PWID:
            ConfGenType1PwWithoutPwId ();
            /* sleep (2); */
            break;
        case PWVC_GEN_FEC_TYPE2_PWID:
            ConfGenType2PwWithoutPwId ();
            break;
        case PWVC_GEN_FEC_TYPE1_WITH_GLOB_NODE_ID:
            ConfGenType1PwWithGlobNodeId ();
            break;
        case PWVC_GEN_FEC_TYPE2_WITH_PEER_ADDR:
            ConfGenType2PwWithPeerAddr ();
            break;
        case PWVC_GEN_FEC_TYPE2_WITH_VCCV_DIS:
            ConfGenType2PwWithVccvDisabled ();
            break;
        case PWVC_GEN_FEC_TYPE2_WITH_CW_DIS:
            ConfGenType2PwWithCwDisabled ();
            break;
        case PWVC_GEN_FEC_TYPE2_WITH_CW_ENA:
            ConfGenType2PwWithCwEnabled ();
            break;
        default:
            PRINTF ("CreateConfPw: Invalid Pw Mode !!!\r\n");
            break;
    }

    CliGiveAppContext ();
    MGMT_LOCK ();
}

UINT4
CallSetRoutine (UINT4 u4Object, UINT4 u4SetVal)
{
    UINT4               u4RetVal = OSIX_FAILURE;
    UINT4               u4PwId = 1;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               u1Var = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    OctetStr.pu1_OctetList = &u1Var;

    MPLS_L2VPN_LOCK ();

    switch (u4Object)
    {
        case L2VPN_ADMIN_STS:
            if (nmhSetFsMplsL2VpnAdminStatus (u4SetVal) == SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case L2VPN_PW_ROW_STATUS:
            if (nmhSetPwRowStatus (u4PwId, u4SetVal) == SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_SCALAR_TRC:
            if (nmhSetFsMplsL2VpnTrcFlag (u4SetVal) == SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_LCL_CAPAB_ADVERT:
            OctetStr.pu1_OctetList[0] = u4SetVal;
            if (nmhSetFsMplsL2VpnPwLocalCapabAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_GEN_AGI_TYPE:
            if (nmhSetFsMplsL2VpnPwGenAGIType (u4PwId, u4SetVal) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_GEN_LCL_AII_TYPE:
            if (nmhSetFsMplsL2VpnPwGenLocalAIIType (u4PwId, u4SetVal) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_GEN_REMOTE_AII_TYPE:
            if (nmhSetFsMplsL2VpnPwGenRemoteAIIType (u4PwId, u4SetVal) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_LOCAL_CC_ADVERT:
            OctetStr.pu1_OctetList[0] = u4SetVal;
            if (nmhSetFsMplsL2VpnPwLocalCCAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_LOCAL_CV_ADVERT:
            OctetStr.pu1_OctetList[0] = u4SetVal;
            if (nmhSetFsMplsL2VpnPwLocalCVAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_REMOTE_CC_ADVERT:
            OctetStr.pu1_OctetList[0] = u4SetVal;
            if (nmhSetFsMplsL2VpnPwRemoteCCAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_REMOTE_CV_ADVERT:
            OctetStr.pu1_OctetList[0] = u4SetVal;
            if (nmhSetFsMplsL2VpnPwRemoteCVAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_OAM_STS:
            if (nmhSetFsMplsL2VpnPwOamEnable (u4PwId, u4SetVal) == SNMP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        default:
            PRINTF ("CallSetRoutine: Invalid SetVal!!\r\n");
            break;
    }

    MPLS_L2VPN_UNLOCK ();

    sleep (1);

    return (u4RetVal);
}

UINT4
CallTestRoutine (UINT4 u4Object, INT4 u4TestVal,
                 tSNMP_OCTET_STRING_TYPE * pOctetStr)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4RetVal = OSIX_FAILURE;

    MPLS_L2VPN_LOCK ();

    switch (u4Object)
    {
        case VCCV_SCALAR_TRC:
            if (nmhTestv2FsMplsL2VpnTrcFlag (&u4ErrCode, u4TestVal) ==
                SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_LCL_CAPAB_ADVERT:
            if (nmhTestv2FsMplsL2VpnPwLocalCapabAdvert
                (&u4ErrCode, 1, pOctetStr) == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_LOCAL_CC_ADVERT:
            if (nmhTestv2FsMplsL2VpnPwLocalCCAdvert (&u4ErrCode, 1, pOctetStr)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_LOCAL_CV_ADVERT:
            if (nmhTestv2FsMplsL2VpnPwLocalCVAdvert (&u4ErrCode, 1, pOctetStr)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_REMOTE_CC_ADVERT:
            if (nmhTestv2FsMplsL2VpnPwRemoteCCAdvert (&u4ErrCode, 1, pOctetStr)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_REMOTE_CV_ADVERT:
            if (nmhTestv2FsMplsL2VpnPwRemoteCVAdvert (&u4ErrCode, 1, pOctetStr)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_OAM_STS:
            if (nmhTestv2FsMplsL2VpnPwOamEnable (&u4ErrCode, 1, u4TestVal) ==
                SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_PW_GEN_AGI_TYPE:
            if (nmhTestv2FsMplsL2VpnPwGenAGIType (&u4ErrCode, 1, u4TestVal) ==
                SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_GEN_LCL_AII_TYPE:
            if (nmhTestv2FsMplsL2VpnPwGenLocalAIIType (&u4ErrCode, 1, u4TestVal)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;

        case VCCV_PW_GEN_REMOTE_AII_TYPE:
            if (nmhTestv2FsMplsL2VpnPwGenRemoteAIIType
                (&u4ErrCode, 1, u4TestVal) == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_SCALAR_CC:
            if (nmhTestv2FsMplsLocalCCTypesCapabilities (&u4ErrCode, pOctetStr)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        case VCCV_SCALAR_CV:
            if (nmhTestv2FsMplsLocalCVTypesCapabilities (&u4ErrCode, pOctetStr)
                == SNMP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        default:
            PRINTF ("CallTestRoutine: Invalid TestVal!!\r\n");
            break;
    }

    MPLS_L2VPN_UNLOCK ();

    return (u4RetVal);
}

UINT4
VerifyObject (UINT4 u4Object, UINT4 u4ValToVerify)
{
    UINT4               u4RetVal = OSIX_FAILURE;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4Status = 0;
    INT4                i4TrcVal = 0;
    UINT1               u1Var = 0;
    UINT4               u4PwId = 1;
    UINT4               u4Val = 0;
    INT4                i4Val = 0;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    OctetStr.pu1_OctetList = &u1Var;

    MPLS_L2VPN_LOCK ();
    switch (u4Object)
    {
        case VCCV_SCALAR_CC:
            if (nmhGetFsMplsLocalCCTypesCapabilities (&OctetStr) ==
                SNMP_SUCCESS)
            {
                if (OctetStr.pu1_OctetList[0] & (UINT1) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
                else
                {
                    PRINTF ("VerifyObject: CC type verification failed!!!\r\n");
                }
            }

            break;

        case VCCV_SCALAR_CV:
            if (nmhGetFsMplsLocalCVTypesCapabilities (&OctetStr) ==
                SNMP_SUCCESS)
            {
                if (OctetStr.pu1_OctetList[0] & (UINT1) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
                else
                {
                    PRINTF ("VerifyObject: CV type verification failed!!!\r\n");
                }
            }
            break;

        case VCCV_SCALAR_HW_CC:    /* Expectation: code to be compiled with NPAPI_WANTED */
            if (nmhGetFsMplsHwCCTypeCapabilities (&OctetStr) == SNMP_SUCCESS)
            {
                if (OctetStr.pu1_OctetList[0] & (UINT1) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
                else
                {
                    PRINTF
                        ("VerifyObject: HW CC type verification failed!!!\r\n");
                }
            }
            break;

        case VCCV_SCALAR_PW_STATUS_NOTIFICATION:
            if (nmhGetFsMplsPwStatusNotifEnable (&i4Status) == SNMP_SUCCESS)
            {
                if (i4Status == (INT4) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
                else
                {
                    PRINTF
                        ("VerifyObject: PW TRAP status notification verification failed!!!\r\n");
                }
            }
            break;

        case VCCV_SCALAR_OAM_STATUS_NOTIFICATION:
            if (nmhGetFsMplsPwOAMStatusNotifEnable (&i4Status) == SNMP_SUCCESS)
            {
                if (i4Status == (INT4) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
                else
                {
                    PRINTF
                        ("VerifyObject: OAM TRAP status notification verification failed!!!\r\n");
                }
            }
            break;

        case VCCV_PW_LOCAL_CC_ADVERT:
            if (nmhGetFsMplsL2VpnPwLocalCCAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    VCCV_ALL_CC_TYPES)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_LOCAL_CV_ADVERT:
            if (nmhGetFsMplsL2VpnPwLocalCVAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    VCCV_ALL_CV_TYPES)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_REMOTE_CC_ADVERT:
            if (nmhGetFsMplsL2VpnPwRemoteCCAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    VCCV_ALL_CC_TYPES)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_REMOTE_CV_ADVERT:
            if (nmhGetFsMplsL2VpnPwRemoteCVAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    VCCV_ALL_CV_TYPES)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_OAM_STS:
            if (nmhGetFsMplsL2VpnPwOamEnable (u4PwId, &i4Val) == SNMP_SUCCESS)
            {
                if (i4Val == (INT4) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;

        case VCCV_PW_GEN_AGI_TYPE:
            if (nmhGetFsMplsL2VpnPwGenAGIType (u4PwId, &u4Val) == SNMP_SUCCESS)
            {
                if (u4Val == u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_GEN_LCL_AII_TYPE:
            if (nmhGetFsMplsL2VpnPwGenLocalAIIType (u4PwId, &u4Val) ==
                SNMP_SUCCESS)
            {
                if (u4Val == u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_GEN_REMOTE_AII_TYPE:
            if (nmhGetFsMplsL2VpnPwGenRemoteAIIType (u4PwId, &u4Val) ==
                SNMP_SUCCESS)
            {
                if (u4Val == u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_LCL_CC_SELECTED:
            if (nmhGetFsMplsL2VpnPwLocalCCSelected (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    L2VPN_VCCV_CC_ACH)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;

        case VCCV_PW_LCL_CV_SELECTED:
            if (nmhGetFsMplsL2VpnPwLocalCVSelected (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    VCCV_CV_TYPE_BFD_LSPP)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_LCL_CAPAB_ADVERT:
            if (nmhGetFsMplsL2VpnPwLocalCapabAdvert (u4PwId, &OctetStr) ==
                SNMP_SUCCESS)
            {
                if ((OctetStr.pu1_OctetList[0] & u4ValToVerify) ==
                    (L2VPN_PW_STATUS_INDICATION | L2VPN_PW_VCCV_CAPABLE))
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_SCALAR_TRC:
            if (nmhGetFsMplsL2VpnTrcFlag (&i4TrcVal) == SNMP_SUCCESS)
            {
                if (i4TrcVal == (INT4) u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case VCCV_PW_SSN_IDX:
            u4Val = 0;
            if (nmhGetFsMplsL2VpnProactiveOamSsnIndex (u4PwId, &u4Val) ==
                SNMP_SUCCESS)
            {
                if (u4Val == u4ValToVerify)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        default:
            PRINTF ("VerifyObject: Invalid scalar object\r\n");
            break;
    }

    MPLS_L2VPN_UNLOCK ();

    return (u4RetVal);
}

VOID
VerifyPwOperStsCli (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("show ip interface");
    CliExecuteAppCmd ("show in description");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliExecuteAppCmd ("show mpls l2transport summary");
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliExecuteAppCmd ("show mpls l2transport vc-id 1 detail");
    CliExecuteAppCmd ("show mpls l2transport binding");
    CliExecuteAppCmd ("show mpls l2transport hw-capability");
    CliExecuteAppCmd ("show mpls l2transport global information");

    CliGiveAppContext ();
    MGMT_LOCK ();
}

UINT4
VerifyPwOperStsNmh ()
{
    INT4                i4OperStatus = L2VPN_PWVC_OPER_DOWN;
    UINT4               u4PwId = 1;
    UINT4               u4RetVal = OSIX_FAILURE;

    MPLS_L2VPN_LOCK ();

    if (nmhGetPwOperStatus (u4PwId, &i4OperStatus) == SNMP_SUCCESS)
    {
        if (i4OperStatus == L2VPN_PWVC_OPER_UP)
        {
            u4RetVal = OSIX_SUCCESS;
        }
        else
        {
            PRINTF ("ERROR: PW operational status is DOWN!!!\r\n");
        }
    }

    MPLS_L2VPN_UNLOCK ();

    return (u4RetVal);
}

VOID
DeletePw (UINT4 u4PwMode)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    switch (u4PwMode)
    {
        case L2VPN_PWVC_OWNER_MANUAL:
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("switch default;vlan 2");
            CliExecuteAppCmd
                ("no mpls l2transport manual 33.33.33.33 pwid 1 locallabel 260001 remotelabel 270001");
            CliExecuteAppCmd ("end");
            break;
        case L2VPN_PWVC_OWNER_PWID_FEC_SIG:
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("switch default;vlan 2");
            CliExecuteAppCmd
                ("no mpls l2transport pwidfec 33.33.33.33 pwid 1 groupid 1");
            CliExecuteAppCmd ("end");
            break;
        case L2VPN_PWVC_OWNER_GEN_FEC_SIG:
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("switch default;vlan 2");
            CliExecuteAppCmd
                ("no mpls l2transport genfec 33.33.33.33 agi ARI saii SRC taii DST pwid 1");
            CliExecuteAppCmd ("end");
            break;
        case PWVC_GEN_FEC_TYPE2:
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("switch default;vlan 2");
            CliExecuteAppCmd
                ("no mpls l2transport genfec global-id 100 node-id 50 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010");
            CliExecuteAppCmd ("end");
            break;
        case PWVC_GEN_FEC_TYPE1_WITH_GLOB_NODE_ID:
            /* Invalid configuration. Global Id, Node Id configuration will not be allowed for GEN FEC Type 1. Allowed only for Type 2 */
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("switch default;vlan 2");
            CliExecuteAppCmd
                ("no mpls l2transport genfec global-id 100 node-id 50 agi ARI saii SRC taii DST pwid 1");
            CliExecuteAppCmd ("end");
            break;
        case PWVC_GEN_FEC_TYPE2_WITH_PEER_ADDR:
            /* Invalid configuration. PeerAddr configuration will not be allowed for GEN FEC Type 2. Allowed only for Type 1 */
            CliExecuteAppCmd ("c t");
            CliExecuteAppCmd ("switch default;vlan 2");
            CliExecuteAppCmd
                ("no mpls l2transport genfec 33.33.33.33 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010");
            CliExecuteAppCmd ("end");
            break;
        default:
            printf ("DeletePw: Invalid PW mode !!!\r\n");
            break;
    }

    DeletePreReq ();

    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
DeletePreReq (VOID)
{
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("no interface mplstunnel 2");
    CliExecuteAppCmd ("no interface loopback 0");

    CliExecuteAppCmd ("interface vlan 2;no mpls ip;exit");

    CliExecuteAppCmd ("no interface vlan 2");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2;no ports;exit");
    CliExecuteAppCmd ("no vlan 2");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("int gi 0/1;shut;no map switch default;exit");
    CliExecuteAppCmd ("no interface gi 0/1");
    CliExecuteAppCmd ("end");
}

VOID
ConfPreReq (VOID)
{
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("interface loopback 0");
    CliExecuteAppCmd ("ip address 11.11.11.11 255.255.255.255");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 33.33.33.33 source 11.11.11.11");
    CliExecuteAppCmd ("tunnel mode mpls traffic-eng");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 50");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 2");
    CliExecuteAppCmd ("tunnel mpls destination 11.11.11.11 source 33.33.33.33");
    CliExecuteAppCmd ("tunnel mode mpls traffic-eng");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 50");
    CliExecuteAppCmd ("tunnel mpls static in-label 200040 vlan 2");
    CliExecuteAppCmd ("no shutdown");

    CliExecuteAppCmd ("end");

    sleep (2);
}

VOID
ConfPwOamParms (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("pw-cc-capability pw-ach router-alert-label ttl-expiry");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("pw-cv-capability lsp-ping bfd-ip-encap-fault bfd-ip-encap-fault-status-notify bfd-ach-encap-fault bfd-ach-encap-fault-status-notify");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("pseudowire-oam pwid 1 oam disable local-cc-type pw-ach router-alert-label ttl-expiry local-cv-type bfd-ip-encap-fault bfd-ip-encap-fault-status-notify bfd-ach-encap-fault bfd-ach-encap-fault-status-notify lsp-ping");
    CliExecuteAppCmd
        ("pseudowire-oam pwid 1 oam disable remote-cc-type pw-ach router-alert-label ttl-expiry remote-cv-type bfd-ip-encap-fault bfd-ip-encap-fault-status-notify bfd-ach-encap-fault bfd-ach-encap-fault-status-notify lsp-ping");
    CliExecuteAppCmd ("end");

    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
ConfPwOamStatus (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd ("pseudowire-oam pwid 1 oam enable");
    CliExecuteAppCmd ("end");

    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
ConfManualPw (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("mpls l2transport manual 33.33.33.33 pwid 1 locallabel 260001 remotelabel 270001 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfPwIdPw (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("mpls l2transport pwidfec 33.33.33.33 pwid 1 groupid 1 control-word enable mplstype non-te entity 3");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType1Pw (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("mpls l2transport genfec 33.33.33.33 agi ARI saii SRC taii DST pwid 1 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType1PwWithGlobNodeId (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("mpls l2transport genfec global-id 100 node-id 50 agi ARI saii SRC taii DST pwid 1 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType2Pw (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd
        (" mpls l2transport genfec global-id 100 node-id 50 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType2PwWithPeerAddr (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd
        (" mpls l2transport genfec 33.33.33.33 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType2PwWithVccvDisabled (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd
        (" mpls l2transport genfec global-id 100 node-id 50 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010 control-word enable vccv disable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType2PwWithCwDisabled (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd
        (" mpls l2transport genfec global-id 100 node-id 50 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010 control-word disable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType2PwWithCwEnabled (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd
        (" mpls l2transport genfec global-id 100 node-id 50 agi ARI src-ac-id 10 dst-ac-id 20 pwid 1 locallabel 260010 remotelabel 270010 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfManualPwWithoutPwId (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("mpls l2transport manual 33.33.33.33 locallabel 260001 remotelabel 270001 control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType1PwWithoutPwId (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd (" vlan 2");
    CliExecuteAppCmd
        ("mpls l2transport genfec 33.33.33.33 agi ARI saii SRC taii DST control-word enable mplstype te 1 2");
    CliExecuteAppCmd ("end");
}

VOID
ConfGenType2PwWithoutPwId (VOID)
{
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd
        (" mpls l2transport genfec global-id 100 node-id 50 agi ARI src-ac-id 10 dst-ac-id 20 locallabel 260010 remotelabel 270010 mplstype te 1 2");
    CliExecuteAppCmd ("end");
}
#endif /* __VCCV_UNIT_TEST_C__ */
