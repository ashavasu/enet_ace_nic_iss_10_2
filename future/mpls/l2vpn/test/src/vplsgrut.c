/********************************************************************
 ** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 **
 ** $Id: vplsgrut.c,v 1.1 2014/08/25 12:23:30 siva Exp $
 **
 ** Description: MPLS API  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/
#ifndef __VPLS_GR_UNIT_TEST_C__
#define __VPLS_GR_UNIT_TEST_C__

#include "l2vpincs.h"
#include "iss.h"
#include "l2vpextn.h"
#include "mplsred.h"
#define VPLS_MAX_UT_CASES 200

extern UINT4  u4TotalPassedCases;
extern UINT4  u4TotalFailedCases;

VOID
CallVplsGRUTCases (tCliHandle CliHandle);
INT4
CreateVpls (UINT4 u4VeId);
VOID
VplsGRExecUnitTest (tCliHandle CliHandle, INT4 i4UtId);
INT4
CreateBgpPw (UINT4 u4VeId);
VOID L2VpnUpdateBgpPwHwList(tL2VpnPwHwList *pL2VpnPwHwListEntry,UINT4 u4VplsIndex,
                                               UINT4 u4PwIndex,UINT4 u2VeId, UINT4 u4InLabel);
VOID
CreateBgpEventInfo (tL2VpnBgpEvtInfo *pL2VpnBgpEvtInfo, UINT4 u4Duration);
INT4
DeleteBgpPw (UINT4 u4VeId);
INT4
CreateSecondAC ();
INT4
CreateThirdAC (UINT4 u4VplsIndex);
INT4
DeleteSecondAC (UINT4 u4VplsIndex);

VOID
CallVplsGRUTCases (tCliHandle CliHandle)
{
    INT4                i4Count = 100;

    for (i4Count = 100; i4Count <= VPLS_MAX_UT_CASES; i4Count++)
    {
        VplsGRExecUnitTest (CliHandle, i4Count);
    }
}
VOID
VplsGRExecUnitTest (tCliHandle CliHandle, INT4 i4UtId)
{
	UINT4	u4RetVal = OSIX_SUCCESS;
	INT4	i4RetVal = L2VPN_SUCCESS;
	
	switch (i4UtId)
	{
		case 100:
		{	MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			break;
		}
		case 101:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,0,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
		
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 102:
		{	
			tL2VpnPwHwList L2VpnPwHwListEntry;

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}

		case 103:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_FAILURE)
			{
    			i4RetVal = L2VPN_FAILURE;
			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();

			DeleteVplsEntry (1);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 104:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			
			if(OSIX_FAILURE == CreateVpls (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			CreateAc(1, 1, 3, 200, &u4RetVal);
			if (OSIX_FAILURE == u4RetVal)
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}	
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	

			break;
		}
		case 105:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();

			L2VpnProcessBgpGREvent ();
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 106:
		{
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnMarkBgpPwEntriesStale ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			break;
		}
		case 107:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,0,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnMarkBgpPwEntriesStale ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;

		}
		case 108:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,1,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnMarkBgpPwEntriesStale ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;

		}
		case 109:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,1,400001);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnReserveLabelFromHwList (&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 110:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnReserveLabelFromHwList (&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			DeleteVplsEntry (1);
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 111:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			sleep (1);
			CreateVe (1, 1, &u4RetVal);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnReserveLabelFromHwList (&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			sleep (1);	
			DeleteVe (1, 1, &u4RetVal);
			DeleteVplsEntry (1);
				
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 112:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnReserveLabelFromHwList (&L2VpnPwHwListEntry);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 113:
		{
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSetLb (1);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			break;
		}
		case 114:
		{
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSetLb (450000);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			break;
		}
		case 115:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,90);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 116:
		{
			sleep (1);
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,90);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			break;
		}
		case 117:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,90);
			gpL2VpnGlobalInfo->b1IsBgpGrTmrStarted = L2VPN_TRUE;
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 118:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 119:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			UINT1 au1VplsName1[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS2";
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			sleep (1);	
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateSecondAC ())
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			if (OSIX_FAILURE == DeleteSecondAC (2))
			{
				u4RetVal = OSIX_FAILURE;
			}
			DeleteVplsEntry (2);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 120:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
				i4RetVal = L2VPN_SUCCESS;
			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 121:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
				i4RetVal = L2VPN_SUCCESS;
			}
			CreateVplsEntry(1, L2VPN_VPLS_SIG_NONE, au1VplsName, 1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 122:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnPwHwList L2VpnPwHwListEntry1;
			 
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET(&L2VpnPwHwListEntry1,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,0,400001);
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry1,11,1,0,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnMarkBgpPwEntriesStale ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			sleep (1);	
			DeleteVe (1, 10, &u4RetVal);
			DeleteVplsEntry (1);
				
			break;

		}
		case 124:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			UINT1 au1VplsName1[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS2";
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
				i4RetVal = L2VPN_SUCCESS;
			}

			CreateVplsEntry(1, L2VPN_VPLS_SIG_NONE, au1VplsName, 1, &u4RetVal);
			sleep (1);
			CreateVplsEntry(2, L2VPN_VPLS_SIG_NONE, au1VplsName1, 2, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);
			DeleteVplsEntry (2);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 125:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			UINT1 au1VplsName1[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS2";
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			sleep (1);	
			CreateVplsEntry(2, L2VPN_VPLS_SIG_NONE, au1VplsName1, 2, &u4RetVal);
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateSecondAC ())
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			if (OSIX_FAILURE == DeleteSecondAC (2))
			{
				u4RetVal = OSIX_FAILURE;
			}
			DeleteVplsEntry (2);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 126:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			UINT1 au1VplsName1[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS2";
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			sleep (1);	
			CreateVplsEntry(2, L2VPN_VPLS_SIG_NONE, au1VplsName1, 2, &u4RetVal);
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateThirdAC (2))
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			if (OSIX_FAILURE == DeleteSecondAC (2))
			{
				u4RetVal = OSIX_FAILURE;
			}
			DeleteVplsEntry (2);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 127:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			UINT1 au1VplsName1[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS2";
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			sleep (1);	
			CreateVplsEntry(2, L2VPN_VPLS_SIG_BGP, au1VplsName1, 2, &u4RetVal);
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			DeleteVplsEntry (2);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 128:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			tVPLSEntry         *pVplsEntry = NULL;
			UINT1 au1VplsName1[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS2";
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			sleep (1);	
			CreateVplsEntry(2, L2VPN_VPLS_SIG_BGP, au1VplsName1, 2, &u4RetVal);
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (2);
			L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_UP;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateThirdAC (2))
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			if (OSIX_FAILURE == DeleteSecondAC (2))
			{
				u4RetVal = OSIX_FAILURE;
			}
			DeleteVplsEntry (2);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 130:
		{
			UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
					{0x02,0x02,0x00,0x00,0x00,0x02,0x00,0x64};
			tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
			VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
			VplsRouteTarget1.pu1_OctetList = au1Rt1;

			CreateRt(1, 1, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			CreateRt(2, 2, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSendBgpRtsAddEvent (1);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteRt(1,1,&u4RetVal);
			DeleteRt(2,2,&u4RetVal);

			break;
		}
		case 131:
		{
			UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
					{0x02,0x02,0x00,0x00,0x00,0x02,0x00,0x64};
			tVPLSRtEntry    *pVplsRtEntry = NULL;
			tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
			VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
			VplsRouteTarget1.pu1_OctetList = au1Rt1;

			CreateRt(1, 1, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			CreateRt(2, 2, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			tVPLSRtEntry    VplsRtEntry;
			MEMSET (&VplsRtEntry, L2VPN_ZERO, sizeof (tVPLSRtEntry));
			L2VPN_VPLSRT_VPLS_INSTANCE (&VplsRtEntry) = 1;
			L2VPN_VPLSRT_RT_INDEX (&VplsRtEntry) = 1;
			pVplsRtEntry = (tVPLSRtEntry *)RBTreeGet (
							L2VPN_VPLS_RT_RB_PTR (gpPwVcGlobalInfo),
							&VplsRtEntry);
			L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry) = NOT_READY;
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSendBgpRtsAddEvent (1);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			L2VPN_VPLSRT_ROW_STATUS (pVplsRtEntry) = ACTIVE;
			DeleteRt(1,1,&u4RetVal);
			DeleteRt(2,2,&u4RetVal);

			break;
		}
		case 132:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnProcessBgpGREvent ();
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 133:
		{
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSendBgpGrAdvEvent (1,0);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();

			break;
		}
		case 134:
		{
				
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSendBgpGrAdvEvent (1,0);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);
			break;
		}
		case 135:
		{
				
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
					{0x02,0x02,0x00,0x00,0x00,0x02,0x00,0x64};
	
			tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
			VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
			VplsRouteTarget1.pu1_OctetList = au1Rt1;

			CreateRt(1, 1, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			sleep (2);
			L2VpnSendBgpGrAdvEvent (1,0);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteRt (1,1,&u4RetVal);
			DeleteVplsEntry (1);
			break;
		}
		case 136:
		{
				
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
					{0x02,0x02,0x00,0x00,0x00,0x02,0x00,0x64};
			
			tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
			UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] =
					{0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
			VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
			VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;
	
			tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
			VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
			VplsRouteTarget1.pu1_OctetList = au1Rt1;

			CreateRt(1, 1, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			
			CreateRd(1, &VplsRouteDistinguisher1, &u4RetVal);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSendBgpGrAdvEvent (1,0);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteRt (1,1,&u4RetVal);
			DeleteRd (1,1,&u4RetVal);
			DeleteVplsEntry (1);
			break;
		}
		case 137:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,0,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 138:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,1,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
			L2VPN_PW_STATUS_NOT_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 139:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,1,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
			L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 140:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
			L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 141:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnPwHwList L2VpnPwHwListEntry1;
			sleep (1);	
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry1,1,2,1,400001);
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}

			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
			L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);	
	
			break;
		}
		case 142:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 143:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 144:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4LabelBlock = L2VPN_INVALID_LABEL;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 145:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4LabelBlock = 450000;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400050);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeleteStaleEntries ();
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 146:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,11,11,0,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 147:
		{	tL2VpnPwHwList L2VpnPwHwListEntry;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,11,0,400001);

			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;

			}
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
	
			break;
		}
		case 148:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4LabelBlock = 450000;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400050);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 149:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 150:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 151:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (10))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 152:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4ReferenceCount = 0;		
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4LabelBlock = L2VPN_INVALID_LABEL;		
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,10,400011);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			L2VPN_PW_HW_LIST_STALE_STATUS (&L2VpnPwHwListEntry) =
				L2VPN_PW_STATUS_STALE;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 153:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 	
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,10,400011);
			MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwIlm (&L2VpnPwHwListEntry, NULL);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;	
		}
		case 154:
		{
			tL2VpnPwHwList L2VpnPwHwListEntry;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,10,400011);
			MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListEntry) = 0;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwIlm (&L2VpnPwHwListEntry, NULL);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 155:
		{
			sleep(1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,10,400011);
			MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			L2VPN_PW_HW_LIST_LSP_IN_LABEL (&L2VpnPwHwListEntry) = 0;
			MplsL2vpnHwListUpdate(&L2VpnPwHwListEntry);
			gMplsNodeStatus = MPLS_NODE_STANDBY;
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwIlm (&L2VpnPwHwListEntry, NULL);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 156:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			 	
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,10,400011);
			MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;	
		}
		case 157:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			 	
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400011);
			MplsL2vpnHwListAdd(&L2VpnPwHwListEntry);
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);

			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			DeleteVplsEntry (1);
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;	
		}
		case 158:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,0,400011);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 159:
		{
			break;
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			tPwVcEntry          *pPwVcEntry = NULL;
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			MplsL2VpnRelPwVcIndex (1);	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,2,0,400011);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			sleep (1);
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			if (NULL != pPwVcEntry)
				L2VPN_PWVC_MODE (pPwVcEntry) = L2VPN_VPWS;
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			L2VPN_PWVC_MODE (pPwVcEntry)=L2VPN_VPLS;	
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 160:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			tPwVcEntry          *pPwVcEntry = NULL;	
			tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
			MplsL2VpnRelPwVcIndex (1);	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400011);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			L2VPN_PWVC_MODE (pPwVcEntry) = L2VPN_VPWS;
			pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
					(L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
					L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
			L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) =0;
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			L2VPN_PWVC_MODE (pPwVcEntry)=L2VPN_VPLS;	
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 161:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			tPwVcEntry          *pPwVcEntry = NULL;	
			tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			if (pPwVcEntry != NULL)
			{
				L2VpnDeletePwVc (pPwVcEntry);
			}
			sleep (1);	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400011);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			L2VPN_PWVC_MODE (pPwVcEntry) = L2VPN_VPWS;
			pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
					(L2VPN_PWVC_ENET_ENTRY_LIST ((tPwVcEnetServSpecEntry *)
					L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)));
			L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) =0;
			L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry)=0;
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			L2VPN_PWVC_MODE (pPwVcEntry)=L2VPN_VPLS;	
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 162:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			tPwVcEntry          *pPwVcEntry = NULL;	
			tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
			sleep (1);	
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400011);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			L2VPN_PWVC_MODE (pPwVcEntry) = L2VPN_VPWS;
			gMplsNodeStatus = MPLS_NODE_ACTIVE;	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			L2VPN_PWVC_MODE (pPwVcEntry) = L2VPN_VPLS;
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 163:
		{
			break;
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnPwHwList L2VpnPwHwListEntry1;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry1,1,2,21,400021);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			
			UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] =
					{0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};

			MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
			MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd1,L2VPN_MAX_VPLS_RD_LEN);
			
			CreateBgpEventInfo2 (&L2VpnBgpEvtInfo, 60);
			L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);	
			gMplsNodeStatus = MPLS_NODE_ACTIVE;	
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);	
			break;
		}
		case 164:
		{
			break;
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
			tPwVcEntry          *pPwVcEntry = NULL;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
						((tPwVcEnetServSpecEntry *)
						L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
						pPwVcEnetEntry, tPwVcEnetEntry *)
			{
				pPwVcEnetEntry->u1HwStatus = MPLS_TRUE;
			}	
				
			gMplsNodeStatus = MPLS_NODE_ACTIVE;	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
			
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 165:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;	
			tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
			tPwVcEntry          *pPwVcEntry = NULL;
			UINT4 u4Port;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			pPwVcEntry = L2VpnGetPwVcEntryFromIndex (1);
			TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
						((tPwVcEnetServSpecEntry *)
						L2VPN_PWVC_ENET_ENTRY (pPwVcEntry)),
						pPwVcEnetEntry, tPwVcEnetEntry *)
			{
				pPwVcEnetEntry->u1HwStatus = MPLS_TRUE;
				u4Port = pPwVcEnetEntry->i4PortIfIndex;
				pPwVcEnetEntry->i4PortIfIndex = 4294967295; 
			}	
				
			gMplsNodeStatus = MPLS_NODE_STANDBY;	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnDelStalePwVc (&L2VpnPwHwListEntry, NULL);
						
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;
		}
		case 166:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVplsAcMapEntry     *pVplsAcMapNextEntry = NULL;
			tVPLSEntry         *pVplsEntry = NULL;
			tVplsAcMapEntry     VplsAcMapEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET (&VplsAcMapEntry, L2VPN_ZERO, sizeof (tVplsAcMapEntry));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			sleep (1);
			CreateVe (1, 10, &u4RetVal);
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_UP;

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateThirdAC (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcMapEntry) = 1;
			pVplsAcMapNextEntry = (tVplsAcMapEntry *)
								RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo), &VplsAcMapEntry,
								NULL);
			L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapNextEntry) = 4294967292;	
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			sleep (1);	
			DeleteVe (1, 10, &u4RetVal);
			DeleteVplsEntry (1);
			if (OSIX_FAILURE == DeleteSecondAC (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 167:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVplsAcMapEntry     *pVplsAcMapNextEntry = NULL;
			tVPLSEntry         *pVplsEntry = NULL;
			tVplsAcMapEntry     VplsAcMapEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET (&VplsAcMapEntry, L2VPN_ZERO, sizeof (tVplsAcMapEntry));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			sleep (1);
			CreateVe (1, 10, &u4RetVal);
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_UP;

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateThirdAC (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcMapEntry) = 1;
			pVplsAcMapNextEntry = (tVplsAcMapEntry *)
								RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo), &VplsAcMapEntry,
								NULL);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("c t");
        	CliExecuteAppCmd("i g 0/6");
        	CliExecuteAppCmd("map switch default");
        	CliExecuteAppCmd("en");
			MGMT_LOCK ();
			L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapNextEntry) = 6;	
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			sleep (1);	
			DeleteVe (1, 10, &u4RetVal);
			DeleteVplsEntry (1);
			if (OSIX_FAILURE == DeleteSecondAC (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 168:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVplsAcMapEntry     *pVplsAcMapNextEntry = NULL;
			tVPLSEntry         *pVplsEntry = NULL;
			tVplsAcMapEntry     VplsAcMapEntry;
			tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET (&VplsAcMapEntry, L2VPN_ZERO, sizeof (tVplsAcMapEntry));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,1,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}

			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			sleep (1);
			CreateVe (1, 10, &u4RetVal);
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_OPER_STATUS (pVplsEntry) = L2VPN_VPLS_OPER_STAT_UP;

			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			CreateBgpEventInfo (&L2VpnBgpEvtInfo,8640001*7);
			if (OSIX_FAILURE ==CreateThirdAC (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
			L2VPN_VPLSAC_VPLS_INSTANCE (&VplsAcMapEntry) = 1;
			pVplsAcMapNextEntry = (tVplsAcMapEntry *)
								RBTreeGetNext (L2VPN_VPLS_AC_RB_PTR (gpPwVcGlobalInfo), &VplsAcMapEntry,
								NULL);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("c t");
        	CliExecuteAppCmd("i g 0/6");
        	CliExecuteAppCmd("map switch default");
        	CliExecuteAppCmd("en");
			MGMT_LOCK ();
			L2VPN_VPLSAC_PORT_INDEX (pVplsAcMapNextEntry) = 6;	
			L2VPN_VPLSAC_OPER_STATUS (pVplsAcMapNextEntry) = L2VPN_VPLS_AC_OPER_STATUS_UP;	
			L2VpnProcessBgpGRInProgressEvent (&L2VpnBgpEvtInfo);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			
			sleep (1);	
			DeleteVe (1, 10, &u4RetVal);
			DeleteVplsEntry (1);
			if (OSIX_FAILURE == DeleteSecondAC (1))
			{
				u4RetVal = OSIX_FAILURE;
			}
	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 169:
		{
			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("c t");
			CliExecuteAppCmd("no ip bgp four-byte-asn");
			CliExecuteAppCmd("router bgp 100");
			CliExecuteAppCmd("end");
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			L2VpnSendBgpRtsAddEvent (1);
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
			break;
		}
		case 170:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tVPLSEntry         *pVplsEntry = NULL;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
				
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();
			pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
			L2VPN_VPLS_LABEL_BLOCK (pVplsEntry)[0].u4LabelBlock = L2VPN_INVALID_LABEL;	
			L2VpnReserveLabelFromHwList (&L2VpnPwHwListEntry);	
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			break;

		}
		case 171:
		{
			sleep (1);
			tL2VpnPwHwList L2VpnPwHwListEntry;
			tL2VpnPwHwList L2VpnPwHwListEntry1;
			tVPLSEntry         *pVplsEntry = NULL;
			if (OSIX_FAILURE == CreateBgpPw (1))
			{
				u4RetVal = OSIX_FAILURE;
				break;
			}

			 
			 
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			MEMSET(&L2VpnPwHwListEntry,L2VPN_ZERO,sizeof(tL2VpnPwHwList));
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry,1,1,0,400001);
			L2VpnUpdateBgpPwHwList(&L2VpnPwHwListEntry1,2,2,0,400001);
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			if(MplsL2vpnHwListAdd(&L2VpnPwHwListEntry1) == MPLS_SUCCESS)
			{
    			i4RetVal = L2VPN_SUCCESS;
			}
			MGMT_UNLOCK ();
			CliExecuteAppCmd("debug mpls l2 all");
			MGMT_LOCK ();	
			L2VpnProcessBgpGREvent ();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("no debug mpls l2 all");
			MGMT_LOCK ();
				
			if (OSIX_FAILURE == DeleteBgpPw (1))
			{
				/*u4RetVal = OSIX_FAILURE;*/
			}
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry);	
			MplsL2vpnHwListDelete(&L2VpnPwHwListEntry1);	
			break;

		}
	
		default:
			return;

	}	
	/* Display the result */
	if (u4RetVal == OSIX_FAILURE)
	{
		CliPrintf(CliHandle,"!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!\r\n",
						i4UtId, "FAILED");
		u4TotalFailedCases++;
	}
	else
	{
		CliPrintf(CliHandle,"$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$\r\n",
						i4UtId, "PASSED");
		u4TotalPassedCases++;
	}
}

INT4 
CreateVpls (UINT4 u4VeId)
{
			UINT4	u4RetVal;
			tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
			tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
			
			MGMT_UNLOCK ();
            CliExecuteAppCmd("c t");
            CliExecuteAppCmd("i g 0/3");
            CliExecuteAppCmd("map switch default");
            CliExecuteAppCmd("no sh");
            CliExecuteAppCmd("en");
            CliExecuteAppCmd("c t");
            CliExecuteAppCmd("vlan 200");
            CliExecuteAppCmd("ports gig 0/3 untagged gig 0/3");
            CliExecuteAppCmd("en");
			MGMT_LOCK ();
			
			UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] =
					{0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
			VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
			VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;
			CreateRd(1, &VplsRouteDistinguisher1, &u4RetVal);	
			if (OSIX_FAILURE == u4RetVal)
			{
				return OSIX_FAILURE;
			}
			UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
					{0x02,0x02,0x00,0x00,0x00,0x02,0x00,0x64};
			VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
			VplsRouteTarget1.pu1_OctetList = au1Rt1;
			CreateRt(1, 1, L2VPN_VPLS_RT_BOTH, &VplsRouteTarget1, &u4RetVal);
			if (OSIX_FAILURE == u4RetVal)
			{
				return OSIX_FAILURE;
			}

			UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";
			CreateVe (1, u4VeId, &u4RetVal);
			if (OSIX_FAILURE == u4RetVal)
			{
				return OSIX_FAILURE;
			}

			CreateVplsEntry (1, L2VPN_VPLS_SIG_BGP, au1VplsName, 1, &u4RetVal);
			if (OSIX_FAILURE == u4RetVal)
			{
				return OSIX_FAILURE;
			}
	return OSIX_SUCCESS;
}
INT4
CreateBgpPw (UINT4 u4VeId)
{
		UINT4 u4RetVal;
		tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
		UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] =
				{0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
			/*if (OSIX_FAILURE == CreateVpls (u4VeId))
			{
				return OSIX_FAILURE;
			}*/
			CreateVpls (u4VeId);
            sleep (1);
			CreateAc(1, 1, 3, 200, &u4RetVal);
			/*if (OSIX_FAILURE == u4RetVal)
			{
				return OSIX_FAILURE;
			}*/

			sleep (2);
			MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
			MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd1,L2VPN_MAX_VPLS_RD_LEN);
			CreateBgpEventInfo (&L2VpnBgpEvtInfo, 60);
			L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);			


}
VOID 
CreateBgpEventInfo (tL2VpnBgpEvtInfo *pL2VpnBgpEvtInfo, UINT4 u4Duration)
{
	UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] =
					{0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
	tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
	VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
	VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;

	MEMCPY(L2VPN_BGP_VPLS_RD(pL2VpnBgpEvtInfo), au1Rd1,L2VPN_MAX_VPLS_RD_LEN);
	L2VPN_BGP_VPLS_VPLSINDEX (pL2VpnBgpEvtInfo) = 1;
	L2VPN_BGP_VPLS_MTU(pL2VpnBgpEvtInfo) = 1518;
	L2VPN_BGP_VPLS_CONTROL_FLAG(pL2VpnBgpEvtInfo) = L2VPN_DISABLED;
	L2VPN_BGP_VPLS_VBO(pL2VpnBgpEvtInfo) = 1;
	L2VPN_BGP_VPLS_VBS(pL2VpnBgpEvtInfo) = 10;
	L2VPN_BGP_VPLS_VE_ID(pL2VpnBgpEvtInfo) = 10;
	L2VPN_BGP_VPLS_LB(pL2VpnBgpEvtInfo) = 400011;
	L2VPN_BGP_VPLS_IP_ADDR(pL2VpnBgpEvtInfo) = 370546198;	
	L2VPN_BGP_GR_TMR_INTERVAL(pL2VpnBgpEvtInfo) = u4Duration;	
}
VOID 
CreateBgpEventInfo2 (tL2VpnBgpEvtInfo *pL2VpnBgpEvtInfo, UINT4 u4Duration)
{
	UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] =
					{0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
	tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
	VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
	VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;

	MEMCPY(L2VPN_BGP_VPLS_RD(pL2VpnBgpEvtInfo), au1Rd1,L2VPN_MAX_VPLS_RD_LEN);
	L2VPN_BGP_VPLS_VPLSINDEX (pL2VpnBgpEvtInfo) = 1;
	L2VPN_BGP_VPLS_MTU(pL2VpnBgpEvtInfo) = 1518;
	L2VPN_BGP_VPLS_CONTROL_FLAG(pL2VpnBgpEvtInfo) = L2VPN_DISABLED;
	L2VPN_BGP_VPLS_VBO(pL2VpnBgpEvtInfo) = 2;
	L2VPN_BGP_VPLS_VBS(pL2VpnBgpEvtInfo) = 10;
	L2VPN_BGP_VPLS_VE_ID(pL2VpnBgpEvtInfo) = 20;
	L2VPN_BGP_VPLS_LB(pL2VpnBgpEvtInfo) = 400021;
	L2VPN_BGP_VPLS_IP_ADDR(pL2VpnBgpEvtInfo) = 387389207;	
	L2VPN_BGP_GR_TMR_INTERVAL(pL2VpnBgpEvtInfo) = u4Duration;	
}

INT4
DeleteBgpPw (UINT4 u4VeId)
{
	UINT4 u4RetVal;	
	DeleteVplsEntry (1);
	sleep (1);
	DeleteAc (1, 1, &u4RetVal);
	if (OSIX_FAILURE == u4RetVal)
	{
		return OSIX_FAILURE;
	}
	DeleteVe (1,u4VeId,&u4RetVal);
	if (OSIX_FAILURE == u4RetVal)
	{
		return OSIX_FAILURE;
	}

	L2VpnSendVplsAssociationDelEvent (1);	
		
			MGMT_UNLOCK ();
            CliExecuteAppCmd("c t");
            CliExecuteAppCmd("i g 0/3");
            CliExecuteAppCmd("no map switch default");
            CliExecuteAppCmd("ex");
            CliExecuteAppCmd("no i g 0/3");
            CliExecuteAppCmd("en");
            CliExecuteAppCmd("c t");
            CliExecuteAppCmd("no vlan 200");
            CliExecuteAppCmd("en");
			MGMT_LOCK ();
	return OSIX_SUCCESS;
}
		INT4
CreateSecondAC ()
{
	UINT4 u4RetVal;	
	MGMT_UNLOCK ();
		CliExecuteAppCmd("c t");
		CliExecuteAppCmd("i g 0/4");
		CliExecuteAppCmd("map switch default");
		CliExecuteAppCmd("no sh");
		CliExecuteAppCmd("en");
		CliExecuteAppCmd("c t");
		CliExecuteAppCmd("vlan 400");
		CliExecuteAppCmd("ports gig 0/4 untagged gig 0/4");
		CliExecuteAppCmd("en");
		MGMT_LOCK ();
        CreateAc(2, 2, 0, 400, &u4RetVal);
		if (u4RetVal == OSIX_FAILURE)
		{
			return OSIX_FAILURE;
		}
		return OSIX_SUCCESS;
}
INT4
CreateThirdAC (UINT4 u4VplsIndex)
{
	UINT4 u4RetVal;	
	MGMT_UNLOCK ();
		CliExecuteAppCmd("c t");
		CliExecuteAppCmd("i g 0/4");
		CliExecuteAppCmd("map switch default");
		CliExecuteAppCmd("no sh");
		CliExecuteAppCmd("en");
		CliExecuteAppCmd("c t");
		CliExecuteAppCmd("vlan 400");
		CliExecuteAppCmd("ports gig 0/4 untagged gig 0/4");
		CliExecuteAppCmd("en");
		MGMT_LOCK ();
        CreateAc(u4VplsIndex, 2, 4294967295, 400, &u4RetVal);
		if (u4RetVal == OSIX_FAILURE)
		{
			return OSIX_FAILURE;
		}
		return OSIX_SUCCESS;
}

INT4
DeleteSecondAC (UINT4 u4VplsIndex)
{
	UINT4 u4RetVal;
	DeleteAc (u4VplsIndex, 2, &u4RetVal);
	if (OSIX_FAILURE == u4RetVal)
	{
		return OSIX_FAILURE;
	}
	MGMT_UNLOCK ();
            CliExecuteAppCmd("c t");
            CliExecuteAppCmd("i g 0/4");
            CliExecuteAppCmd("no map switch default");
            CliExecuteAppCmd("ex");
            CliExecuteAppCmd("no i g 0/4");
            CliExecuteAppCmd("en");
            CliExecuteAppCmd("c t");
            CliExecuteAppCmd("no vlan 400");
            CliExecuteAppCmd("en");
			MGMT_LOCK ();
	return OSIX_SUCCESS;
}

VOID L2VpnUpdateBgpPwHwList(tL2VpnPwHwList *pL2VpnPwHwListEntry,UINT4 u4VplsIndex, UINT4 u4PwIndex,UINT4 u2VeId, UINT4 u4InLabel)
{
    L2VPN_PW_HW_LIST_VPLS_INDEX(pL2VpnPwHwListEntry)=u4VplsIndex;
    L2VPN_PW_HW_LIST_PW_INDEX(pL2VpnPwHwListEntry)  =u4PwIndex;
    L2VPN_PW_HW_LIST_LCL_LDP_ENT_INDEX(pL2VpnPwHwListEntry) =1;
    L2VPN_PW_HW_LIST_IN_LABEL(pL2VpnPwHwListEntry)  = u4InLabel;
    L2VPN_PW_HW_LIST_LSP_OUT_LABEL(pL2VpnPwHwListEntry)= 1;
    L2VPN_PW_HW_LIST_OUT_LABEL(pL2VpnPwHwListEntry) =1;
    L2VPN_PW_HW_LIST_REMOTE_VE_ID(pL2VpnPwHwListEntry)= u2VeId;
    L2VPN_PW_HW_LIST_OUT_IF_INDEX(pL2VpnPwHwListEntry)= 1;
    L2VPN_PW_HW_LIST_LSP_IN_LABEL(pL2VpnPwHwListEntry)= 1;
    L2VPN_PW_HW_LIST_IN_IF_INDEX(pL2VpnPwHwListEntry) = 1;
    L2VPN_PW_HW_LIST_NPAPI_STATUS(pL2VpnPwHwListEntry)= 0;
    L2VPN_PW_HW_LIST_STALE_STATUS(pL2VpnPwHwListEntry)= 2;
    L2VPN_PW_HW_LIST_OUT_PORT(pL2VpnPwHwListEntry)= 1;
    L2VPN_PW_HW_LIST_IS_STATIC_PW(pL2VpnPwHwListEntry) = 0;
}

#endif
