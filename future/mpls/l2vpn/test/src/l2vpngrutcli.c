/********************************************************************
 *** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 ***
 *** $Id: l2vpngrutcli.c,v 1.1 2014/08/25 12:23:30 siva Exp $
 ***
 *** Description: TLDP-GR UT Test cases cli file.
 *** NOTE: This file should not be include in release packaging.
 **************************************************************************/


#include "l2vpincs.h"
#include "mplscli.h"

#define MAX_ARGS 5

extern VOID  TLdpPwGrExecUnitTest (tCliHandle CliHandle, INT4 i4TestId);
extern VOID  CallTLdpPwGrUTCases(tCliHandle CliHandle);


UINT4  u4TotalPassedCase = 0;
UINT4  u4TotalFailedCase = 0;


INT4
cli_process_tldp_pw_gr_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...);

INT4 TLdpPwGrUt(tCliHandle CliHandle, INT4 i4TestType);

/*  Function is called from vcutcmd.def file */

	INT4
cli_process_tldp_pw_gr_test_cmd(tCliHandle CliHandle, UINT4 u4Command,...)
{
	va_list            ap;
	UINT4             *args[MAX_ARGS];
	INT1               argno = 0;
	INT4               i4TestcaseNo ;
	va_start (ap, u4Command);
	/* Walk through the arguements and store in args array.
	 *  *      * */
	while (1)
	{
		args[argno++] = va_arg (ap, UINT4 *);
		if (argno == MAX_ARGS)
			break;
	}
	va_end (ap);
	
	u4TotalPassedCase = 0;
	u4TotalFailedCase = 0;
	
	switch (u4Command)
	{
		case CLI_TLDP_PW_GR_UT: 
			
			if (args[1] == NULL)
			{
				i4TestcaseNo = 0;
			}
			else
			{
				i4TestcaseNo = *args[1];
			}
			TLdpPwGrUt (CliHandle, i4TestcaseNo);
			CliPrintf(CliHandle,"\n Total passed: %d \n", u4TotalPassedCase);
			CliPrintf(CliHandle,"\n Total failed: %d \n", u4TotalFailedCase);
			break;

	}
	return CLI_SUCCESS;
}


INT4
TLdpPwGrUt(tCliHandle CliHandle, INT4 i4TestType)
{
    if(i4TestType == 0)
    {
        /* Calls all the test cases */
        CallTLdpPwGrUTCases(CliHandle);
        return CLI_SUCCESS;
    }
    else
    {   /* Calls the particular test case only */
        TLdpPwGrExecUnitTest (CliHandle, i4TestType);
        return CLI_SUCCESS;
    }
}

