/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: vplsutcli.c,v 1.3 2014/11/08 11:41:06 siva Exp $
 **
 ** Description: VPLS-AD UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "l2vpincs.h"
#include "mplscli.h"

#define MAX_ARGS 5

extern VOID  VplsExecUnitTest (tCliHandle CliHandle, INT4 i4TestId);
extern VOID  CallVplsUTCases(tCliHandle CliHandle);

UINT4  u4TotalPassedCases = 0;
UINT4  u4TotalFailedCases = 0;

INT4
cli_process_vplsad_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);

INT4 VplsUt(tCliHandle CliHandle, INT4 i4TestType);

/*  Function is called from vcutcmd.def file */
INT4
cli_process_vplsads_gr_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...)
{
	va_list            ap;
	UINT4             *args[MAX_ARGS];
	INT1               argno = 0;
	INT4               i4TestcaseNo ;
	va_start (ap, u4Command);

	/* Walk through the arguements and store in args array.
 	*  *      * */
	while (1)
	{
    	args[argno++] = va_arg (ap, UINT4 *);
    	if (argno == MAX_ARGS)
        	break;
	}
	u4TotalPassedCases = 0;
	u4TotalFailedCases = 0;
	
	switch (u4Command)
	{
		case CLI_VPLSADS_GR_UT:
			if (args[1] == NULL)
			{
				i4TestcaseNo = 0;
			}
			else
			{
				i4TestcaseNo = *args[1];
			}
			VplsGRUt (CliHandle, i4TestcaseNo);
			CliPrintf(CliHandle,"\n Total passed: %d \n", u4TotalPassedCases);
			CliPrintf(CliHandle,"\n Total failed: %d \n", u4TotalFailedCases);
			break;
	}
	return CLI_SUCCESS;
}

INT4
cli_process_vplsads_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...)
{
    va_list            ap;
    UINT4             *args[MAX_ARGS];                                                               
    INT1               argno = 0;                                                                    
    INT4               i4TestcaseNo ;                                                                
    va_start (ap, u4Command);                                                                    
    /* Walk through the arguements and store in args array.
     *      * */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    u4TotalPassedCases = 0;
    u4TotalFailedCases = 0;

    switch (u4Command)

    {
        case CLI_VPLS_ADS_UT:

            MplsSetBgpAdminState(L2VPN_BGP_ADMIN_UP);
            if (args[1] == NULL)
            {
                i4TestcaseNo = 0;
            }
            else
            {
                i4TestcaseNo = *args[1];
            }
            VplsUt (CliHandle, i4TestcaseNo);
            CliPrintf(CliHandle,"\n Total passed: %d \n", u4TotalPassedCases);
            CliPrintf(CliHandle,"\n Total failed: %d \n", u4TotalFailedCases);
            break;
    }
    return CLI_SUCCESS;
}
    
/* Function to call the test case functions in mplstest.c */
INT4
VplsGRUt(tCliHandle CliHandle, INT4 i4TestType)
{
    if(i4TestType == 0)
    {
        /* Calls all the test cases */
        CallVplsGRUTCases(CliHandle);
        return CLI_SUCCESS;
    }
    else                                                                                             
    {   /* Calls the particular test case only */
        VplsGRExecUnitTest (CliHandle, i4TestType);
        return CLI_SUCCESS;                                                                      
    }                                                                                            
}

INT4
VplsUt(tCliHandle CliHandle, INT4 i4TestType)
{
    if(i4TestType == 0)
    {
        /* Calls all the test cases */
        CallVplsUTCases(CliHandle);
        return CLI_SUCCESS;
    }
    else                                                                                             
    {   /* Calls the particular test case only */
        VplsExecUnitTest (CliHandle, i4TestType);
        return CLI_SUCCESS;                                                                      
    }                                                                                            
}

