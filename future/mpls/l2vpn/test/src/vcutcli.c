/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: vcutcli.c,v 1.2 2014/02/27 13:50:39 siva Exp $
 **
 ** Description: VCCV UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "l2vpincs.h"
#include "mplscli.h"

#define MAX_ARGS 5

extern VOID         VccvExecUnitTest (INT4 i4TestId);
extern VOID         CallVccvUTCases (VOID);

INT4                VccvUt (INT4 i4TestType);

/*  Function is called from vcutcmd.def file */

INT4
cli_process_vccv_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT1                argno = 0;
    INT4                i4TestcaseNo;
    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    /* Walk through the arguements and store in args array.
     *      * */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case CLI_VCCV_UT:

            if (args[1] == NULL)
            {
                i4TestcaseNo = 0;
            }
            else
            {
                i4TestcaseNo = *args[1];
            }
            VccvUt (i4TestcaseNo);
            break;
    }
    return CLI_SUCCESS;
}

/* Function to call the test case functions in mplstest.c */

INT4
VccvUt (INT4 i4TestType)
{
    if (i4TestType == 0)
    {
        /* Calls all the test cases */
        CallVccvUTCases ();
        return CLI_SUCCESS;
    }
    else
    {                            /* Calls the particular test case only */
        VccvExecUnitTest (i4TestType);
        return CLI_SUCCESS;
    }
}
