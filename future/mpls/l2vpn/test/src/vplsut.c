/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: vplsut.c,v 1.3 2014/11/08 11:41:06 siva Exp $
 **
 ** Description: MPLS API  UT Test cases.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/
#ifndef __VPLS_UNIT_TEST_C__
#define __VPLS_UNIT_TEST_C__

#include "l2vpincs.h"
#include "iss.h"

#define VPLS_MAX_UT_CASES 411
#define SET 1
#define NOT_SET 0

extern UINT4  u4TotalPassedCases;
extern UINT4  u4TotalFailedCases;

extern INT4
L2VpnCliParseAndGenerateRdRt ARG_LIST ((UINT1* pu1RandomString,
                                        UINT1* pu1RdRt));

VOID                CallVplsUTCases (tCliHandle CliHandle);
VOID                VplsExecUnitTest (tCliHandle CliHandle, INT4 i4TestId);
VOID                
CallVplsUTCases (tCliHandle CliHandle);
VOID                
VplsExecUnitTest (tCliHandle CliHandle, INT4 i4TestId);
INT1
nmhGetFirstIndexVplsBgpConfigTableUT (VOID);
INT1
nmhGetVplsBgpPwBindLocalVEIdUT (VOID);
INT1
nmhGetFirstIndexVplsBgpPwBindTableUT (VOID);
INT1                    
nmhSetVplsBgpVENameUT (VOID);
INT1
nmhGetNextIndexVplsBgpVETableUT (VOID);
INT1
nmhGetVplsBgpPwBindRemoteVEIdUT (VOID);
INT1
nmhSetVplsBgpVENameUT (VOID);
INT1
nmhSetVplsBgpVEPreferenceUT (VOID);
INT1
nmhTestv2VplsBgpVERowStatusUT (INT4 i4SetValVplsBgpVERowStatus);
INT1
nmhTestv2VplsBgpVEStorageTypeUT(VOID);
INT1
nmhSetVplsBgpVERowStatusUT(INT4 i4SetValVplsBgpVERowStatus);
INT1
nmhTestv2VplsBgpVENameUT (VOID);
INT1
nmhTestv2VplsBgpVEPreferenceUT (VOID);
INT1
nmhSetVplsBgpVEStorageTypeUT (VOID);
INT1
nmhGetVplsBgpVEPreferenceUT (INT4 iset);
INT1
nmhGetVplsBgpVENameUT (VOID);
INT1
nmhGetVplsBgpVEStorageTypeUT (INT4 iset);
INT1
nmhGetFirstIndexVplsBgpVETableUT(INT4 iset);

VOID
CreateRd (UINT4 u4VplsIndex, tSNMP_OCTET_STRING_TYPE *pVplsRouteDistinguisher,
          UINT4 * pu4RetVal);

VOID
CreateRt (UINT4 u4VplsIndex, UINT4 u4RtIndex, INT4 i4RtType,
          tSNMP_OCTET_STRING_TYPE * pVplsRouteTarget, UINT4 * pu4RetVal);

VOID
DeleteRd (UINT4 u4VplsIndex, UINT4 * pu4RetVal);

VOID
DeleteRt (UINT4 u4VplsIndex, UINT4 u4RtIndex, UINT4 * pu4RetVal);

VOID
CreateVe (UINT4 u4VplsIndex, UINT4 u4VeId, UINT4 * pu4RetVal);

VOID
DeleteVe (UINT4 u4VplsIndex, UINT4 u4VeId, UINT4 * pu4RetVal);

VOID
CreateAc (UINT4 u4VplsIndex, UINT4 u4AcIndex, UINT4 u4IfIndex,
          UINT4 u4Vlan, UINT4 * pu4RetVal);

VOID
DeleteAc (UINT4 u4VplsIndex, UINT4 u4AcIndex, UINT4 * pu4RetVal);

VOID
CreateVplsEntry(UINT4 u4VplsIndex, INT4 i4SigType, UINT1 *pu1VplsName, 
                UINT4 u4VpnId, UINT4 * pu4RetVal);
INT4
DeleteVplsEntry(UINT4 u4VplsIndex);
INT4
DeleteVplsEntry (UINT4 u4VplsIndex)
{
	if (nmhSetFsMplsVplsRowStatus (u4VplsIndex, DESTROY) == SNMP_FAILURE)
    {
        return L2VPN_FAILURE;
    }
	return L2VPN_SUCCESS;
}
VOID
CallVplsUTCases (tCliHandle CliHandle)
{
    INT4                i4Count = 100;

    for (i4Count = 100; i4Count <= VPLS_MAX_UT_CASES; i4Count++)
    {
        VplsExecUnitTest (CliHandle, i4Count);
    }
}

VOID
CreateRd (UINT4 u4VplsIndex, tSNMP_OCTET_STRING_TYPE *pVplsRouteDistinguisher,
          UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetVplsBgpADConfigRowStatus(u4VplsIndex, CREATE_AND_WAIT);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetVplsBgpADConfigRouteDistinguisher(u4VplsIndex,
                                   pVplsRouteDistinguisher);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetVplsBgpADConfigRowStatus(u4VplsIndex, ACTIVE);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    return;
}

VOID
CreateRt (UINT4 u4VplsIndex, UINT4 u4RtIndex, INT4 i4RtType,
          tSNMP_OCTET_STRING_TYPE * pVplsRouteTarget, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(u4VplsIndex, u4RtIndex, 
                                                 CREATE_AND_WAIT);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetVplsBgpRteTargetRT(u4VplsIndex, u4RtIndex,
                                   pVplsRouteTarget);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetVplsBgpRteTargetRTType(u4VplsIndex, u4RtIndex, i4RtType);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(u4VplsIndex, u4RtIndex, 
                                                 ACTIVE);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    return;
}

VOID
DeleteRd (UINT4 u4VplsIndex, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetVplsBgpADConfigRowStatus(u4VplsIndex, DESTROY);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
    }
}

VOID
DeleteRt (UINT4 u4VplsIndex, UINT4 u4RtIndex, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(u4VplsIndex, u4RtIndex, 
                                                 DESTROY);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
    }
}

VOID
CreateVe (UINT4 u4VplsIndex, UINT4 u4VeId, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetVplsBgpVERowStatus(u4VplsIndex, u4VeId, CREATE_AND_WAIT);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetVplsBgpVERowStatus(u4VplsIndex, u4VeId, ACTIVE);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    return;
}

VOID
DeleteVe (UINT4 u4VplsIndex, UINT4 u4VeId, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetVplsBgpVERowStatus(u4VplsIndex, u4VeId, DESTROY);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }
    return;
}

VOID
CreateAc (UINT4 u4VplsIndex, UINT4 u4AcIndex, UINT4 u4IfIndex,
          UINT4 u4Vlan, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(u4VplsIndex, u4AcIndex,
                                              CREATE_AND_WAIT);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    /*if ( 0 != u4IfIndex )*/
    {
        i4RetVal = nmhSetFsMplsVplsAcMapPortIfIndex(u4VplsIndex, u4AcIndex,
                                                    u4IfIndex);
        if ( SNMP_FAILURE == i4RetVal )
        {
            *pu4RetVal = OSIX_FAILURE;
            return;
        }
    }

    if ( 0 != u4Vlan )
    {
        i4RetVal = nmhSetFsMplsVplsAcMapPortVlan(u4VplsIndex, u4AcIndex,
                                                 u4Vlan);
        if ( SNMP_FAILURE == i4RetVal )
        {
            *pu4RetVal = OSIX_FAILURE;
            return;
        }
    }

    i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(u4VplsIndex, u4AcIndex,
                                              ACTIVE);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    return;
}

VOID
DeleteAc (UINT4 u4VplsIndex, UINT4 u4AcIndex, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;

    i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(u4VplsIndex, u4AcIndex,
                                              DESTROY);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }
	*pu4RetVal = OSIX_SUCCESS;
    return;
}

VOID
CreateVplsEntry(UINT4 u4VplsIndex, INT4 i4SigType, UINT1 *pu1VplsName, 
                UINT4 u4VpnId, UINT4 * pu4RetVal)
{
    INT4 i4RetVal;
    tSNMP_OCTET_STRING_TYPE VplsName;
    tSNMP_OCTET_STRING_TYPE VpnId;
    UINT1 au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN] =
                 {'A', 'r', 'i', 0x00, 0x00, 0x00, 0x02};

    VpnId.i4_Length = L2VPN_MAX_VPLS_VPNID_LEN;
    VpnId.pu1_OctetList = au1VpnId;

    VplsName.i4_Length = STRLEN(pu1VplsName);
    VplsName.pu1_OctetList = pu1VplsName;

    u4VpnId = OSIX_HTONL(u4VpnId);
    MEMCPY(au1VpnId + 3, &u4VpnId, 4);

    i4RetVal = nmhSetFsMplsVplsRowStatus(u4VplsIndex, CREATE_AND_WAIT);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetFsMplsVplsVpnId(u4VplsIndex, &VpnId);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    if ( SNMP_FAILURE == nmhSetFsMplsVplsName(u4VplsIndex, &VplsName) )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetFsmplsVplsSignalingType(u4VplsIndex, i4SigType);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    i4RetVal = nmhSetFsMplsVplsRowStatus(u4VplsIndex, ACTIVE);
    if ( SNMP_FAILURE == i4RetVal )
    {
        *pu4RetVal = OSIX_FAILURE;
        return;
    }

    return;
}

VOID
VplsExecUnitTest (tCliHandle CliHandle, INT4 i4UtId)
{

    UINT4               u4RetVal = OSIX_SUCCESS;
    INT4                i4RetVal = L2VPN_FAILURE;

    switch (i4UtId)
    {
        case 100:
            {
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("set cli pagination off");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpVETable");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpADConfigEntry");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpRteTargetEntry");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpADConfigEntry");
                CliExecuteAppCmd("snmpwalk mib name fsMplsVplsConfigEntry");
                CliExecuteAppCmd("snmpwalk mib name fsMplsVplsAcMapTable");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpPwBindTable");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
            }
            break;

        case 101:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher;
                i4RetVal = nmhGetVplsBgpADConfigRouteDistinguisher(1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 102:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher;
                UINT4 u4ErrorCode;
                i4RetVal = nmhTestv2VplsBgpADConfigRouteDistinguisher(
                                               &u4ErrorCode,1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpADConfigRouteDistinguisher(1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 103:
            {
                UINT4 u4VplsConfigPrefix = L2VPN_ZERO;
                i4RetVal = nmhGetVplsBgpADConfigPrefix(1, &u4VplsConfigPrefix);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 104:
            {
                UINT4 u4VplsConfigPrefix = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigPrefix(&u4ErrorCode, 1, 
                                                          u4VplsConfigPrefix);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigPrefix(1, u4VplsConfigPrefix);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 105:
            {
                tSNMP_OCTET_STRING_TYPE VplsBgpVplsId;
                i4RetVal = nmhGetVplsBgpADConfigVplsId(1, 
                                               &VplsBgpVplsId);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 106:
            {
                tSNMP_OCTET_STRING_TYPE VplsBgpVplsId;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigVplsId(&u4ErrorCode, 1,
                                                          &VplsBgpVplsId);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigVplsId(1, 
                                               &VplsBgpVplsId);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 107:
            {
                INT4 i4StorageType = L2VPN_ZERO;
                i4RetVal = nmhGetVplsBgpADConfigStorageType(1, &i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 108:
            {
                INT4 i4StorageType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigStorageType(1, i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 109:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RetVal = nmhGetVplsBgpADConfigRowStatus(1, &i4RowStatus);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 110:
        case 120:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = ACTIVE;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( ( 110 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 120 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( ( 110 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 120 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 111:
        case 121:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = NOT_IN_SERVICE;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( ( 111 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 121 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( ( 111 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 121 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 112:
        case 122:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = DESTROY;
                UINT4 u4ErrorCode;

                i4RowStatus = NOT_READY;
                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( 112 == i4UtId && SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( 112 == i4UtId && SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RowStatus = CREATE_AND_GO;
                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( 112 == i4UtId && SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( 112 == i4UtId && SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RowStatus = 0;
                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( 112 == i4UtId && SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( 112 == i4UtId && SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RowStatus = DESTROY;
                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( ( 112 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 122 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( ( 112 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 122 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 113:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = CREATE_AND_WAIT;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 114:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher;
                UINT4 u4ErrorCode;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                                 {0x03,0x03,0x00,0x20,0x00,0x00,0x00,0x64};
                
                VplsRouteDistinguisher.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher.pu1_OctetList = au1Rd;
                
                i4RetVal = nmhTestv2VplsBgpADConfigRouteDistinguisher(
                                               &u4ErrorCode, 1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                VplsRouteDistinguisher.i4_Length = L2VPN_MAX_VPLS_RD_LEN + 1;
                VplsRouteDistinguisher.pu1_OctetList = au1Rd;
                
                i4RetVal = nmhTestv2VplsBgpADConfigRouteDistinguisher(
                                               &u4ErrorCode, 1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 115:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher;
                UINT4 u4ErrorCode;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                                 {0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x64};
                
                VplsRouteDistinguisher.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher.pu1_OctetList = au1Rd;
                
                MplsUpdateASN(0, 2);

                i4RetVal = nmhTestv2VplsBgpADConfigRouteDistinguisher(
                                               &u4ErrorCode, 1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                
                i4RetVal = nmhSetVplsBgpADConfigRouteDistinguisher(1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 116:
            {
                UINT4 u4ConfigPrefix = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigPrefix(&u4ErrorCode, 1,
                                                          u4ConfigPrefix);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigPrefix(1, u4ConfigPrefix);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 117:
            {
                tSNMP_OCTET_STRING_TYPE VplsId;
                UINT4 u4ErrorCode;
                UINT1 au1VplsId[L2VPN_MAX_VPLS_RD_LEN] = 
                                 {0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
                
                VplsId.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsId.pu1_OctetList = au1VplsId;
                
                i4RetVal = nmhTestv2VplsBgpADConfigVplsId(&u4ErrorCode, 1,
                                               &VplsId);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                
                i4RetVal = nmhSetVplsBgpADConfigVplsId(1, &VplsId);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 118:
            {
                UINT4 u4StorageType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               u4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 119:
            {
                UINT4 u4StorageType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                u4StorageType = L2VPN_STORAGE_VOLATILE;
                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               u4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4StorageType = L2VPN_STORAGE_OTHER;
                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               u4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4StorageType = L2VPN_STORAGE_PERMANENT;
                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               u4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4StorageType = L2VPN_STORAGE_READONLY;
                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               u4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4StorageType = L2VPN_STORAGE_NONVOLATILE;
                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(&u4ErrorCode, 1,
                                                               u4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpADConfigStorageType(1, u4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 123:
            {
                INT4 i4RtType = L2VPN_ZERO;
                i4RetVal = nmhGetVplsBgpRteTargetRTType(1, 1, &i4RtType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 124:
            {
                INT4 i4RtType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTType(&u4ErrorCode, 1, 1,
                                                           i4RtType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetRTType(1, 1, i4RtType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 125:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
                i4RetVal = nmhGetVplsBgpRteTargetRT(1, 1, &VplsRouteTarget);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 126:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRT(&u4ErrorCode,1, 1,
                                                       &VplsRouteTarget);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
        
                i4RetVal = nmhSetVplsBgpRteTargetRT(1, 1, &VplsRouteTarget);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 127:
            {
                INT4 i4StorageType = L2VPN_ZERO;
                i4RetVal = nmhGetVplsBgpRteTargetStorageType(1, 1, 
                                                             &i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 128:
            {
                INT4 i4StorageType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                             1, i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetStorageType(1, 1, 
                                                             i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 129:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RetVal = nmhGetVplsBgpRteTargetRTRowStatus(1, 1,&i4RowStatus);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 130:
        case 140:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = ACTIVE;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, i4RowStatus);
                if ( ( 130 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 140 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, i4RowStatus);
                if ( ( 130 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 140 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 131:
        case 141:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = NOT_IN_SERVICE;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, i4RowStatus);
                if ( ( 131 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 141 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, i4RowStatus);
                if ( ( 131 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 141 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 132:
        case 142:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = DESTROY;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, NOT_READY);
                if ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) 
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, NOT_READY);
                if ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) 
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, CREATE_AND_GO);
                if ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) 
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1,1,CREATE_AND_GO);
                if ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) 
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, 0);
                if ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) 
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, 0);
                if ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) 
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, i4RowStatus);
                if ( ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 142 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, i4RowStatus);
                if ( ( 132 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 142 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 133:
            {
                INT4 i4RowStatus = L2VPN_ZERO;
                i4RowStatus = CREATE_AND_WAIT;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode, 
                                                           1, 1, i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 134:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
                UINT4 u4ErrorCode;
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN + 1] = 
                                 {0x03,0x03,0x00,0x20,0x00,0x00,0x00,0x64};
                
                VplsRouteTarget.i4_Length = L2VPN_MAX_VPLS_RT_LEN + 1;
                VplsRouteTarget.pu1_OctetList = au1Rt;
                
                i4RetVal = nmhTestv2VplsBgpRteTargetRT(
                                               &u4ErrorCode, 1, 1, 
                                               &VplsRouteTarget);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                VplsRouteTarget.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget.pu1_OctetList = au1Rt;
                
                i4RetVal = nmhTestv2VplsBgpRteTargetRT(
                                               &u4ErrorCode, 1, 1, 
                                               &VplsRouteTarget);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 135:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
                UINT4 u4ErrorCode;
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                                 {0x00,0x02,0x00,0x02,0x00,0x00,0x00,0x64};
                
                VplsRouteTarget.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget.pu1_OctetList = au1Rt;
                
                i4RetVal = nmhTestv2VplsBgpRteTargetRT(
                                               &u4ErrorCode, 1, 1, 
                                               &VplsRouteTarget);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                
                i4RetVal = nmhSetVplsBgpRteTargetRT(1, 1, 
                                                    &VplsRouteTarget);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 136:
            {
                INT4 i4RtType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTType(&u4ErrorCode, 1,
                                                           1, i4RtType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 137:
            {
                INT4 i4RtType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RtType = L2VPN_VPLS_RT_BOTH;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTType(&u4ErrorCode, 1,
                                                           1, i4RtType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetRTType(1, 1, i4RtType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 138:
            {
                INT4 i4StorageType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 139:
            {
                INT4 i4StorageType = L2VPN_ZERO;
                UINT4 u4ErrorCode;

                i4StorageType = L2VPN_STORAGE_NONVOLATILE;
                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_OTHER;
                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_PERMANENT;
                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_READONLY;
                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_VOLATILE;
                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_VOLATILE;
                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(&u4ErrorCode, 1,
                                                              1, i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpRteTargetStorageType(1, 1, 
                                                             i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 143:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher2;
                UINT4 u4VplsIndex = L2VPN_ZERO;
                UINT4 u4NextVplsIndex = L2VPN_ZERO;
                UINT4 u4VplsConfigPrefix;
                INT4  i4StorageType;
                INT4  i4RowStatus;
                UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
                UINT1 au1Rd2[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x65};
                
                VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;

                VplsRouteDistinguisher2.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher2.pu1_OctetList = au1Rd2;

                CreateRd(1, &VplsRouteDistinguisher1, &u4RetVal);
                CreateRd(2, &VplsRouteDistinguisher2, &u4RetVal);

                i4RetVal = nmhGetFirstIndexVplsBgpADConfigTable(&u4VplsIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigRouteDistinguisher(1, 
                                               &VplsRouteDistinguisher1);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigPrefix(1, &u4VplsConfigPrefix);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigVplsId(1, 
                                               &VplsRouteDistinguisher1);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigStorageType(1, &i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigRowStatus(1, &i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetNextIndexVplsBgpADConfigTable(u4VplsIndex, 
                                                             &u4NextVplsIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigRouteDistinguisher(2, 
                                               &VplsRouteDistinguisher2);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigPrefix(2, &u4VplsConfigPrefix);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigVplsId(2, 
                                               &VplsRouteDistinguisher2);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigStorageType(2, &i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpADConfigRowStatus(2, &i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4VplsIndex = u4NextVplsIndex;

                i4RetVal = nmhGetNextIndexVplsBgpADConfigTable(u4VplsIndex, 
                                                             &u4NextVplsIndex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 144:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget2;
                UINT4 u4VplsIndex = L2VPN_ZERO;
                UINT4 u4RtIndex = L2VPN_ZERO;
                UINT4 u4NextVplsIndex = L2VPN_ZERO;
                UINT4 u4NextRtIndex = L2VPN_ZERO;
                INT4  i4StorageType;
                INT4  i4RowStatus;
                INT4  i4RtType;
                UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
                          {0x02,0x02,0x00,0x00,0x00,0x02,0x00,0x64};
                UINT1 au1Rt2[L2VPN_MAX_VPLS_RT_LEN] = 
                          {0x01,0x02,0x00,0x00,0x00,0x02,0x00,0x65};
                
                VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget1.pu1_OctetList = au1Rt1;

                VplsRouteTarget2.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget2.pu1_OctetList = au1Rt2;

                MplsUpdateASN(1, 2);
                CreateRt(1, 1, L2VPN_VPLS_RT_IMPORT, 
                         &VplsRouteTarget1, &u4RetVal);
                CreateRt(1, 2, L2VPN_VPLS_RT_BOTH, 
                         &VplsRouteTarget2, &u4RetVal);
                MplsUpdateASN(0, 2);

                i4RetVal = nmhGetFirstIndexVplsBgpRteTargetTable(&u4VplsIndex,
                                                                 &u4RtIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetRTType(1, 1, &i4RtType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetRT(1, 1, &VplsRouteTarget1);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetStorageType(1, 1, 
                                                             &i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetRTRowStatus(1, 1,&i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetNextIndexVplsBgpRteTargetTable(u4VplsIndex, 
                                                             &u4NextVplsIndex,
                                                             u4RtIndex,
                                                             &u4NextRtIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetRTType(1, 2, &i4RtType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetRT(1, 2, &VplsRouteTarget2);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetStorageType(1, 2, 
                                                             &i4StorageType);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetVplsBgpRteTargetRTRowStatus(1, 2,&i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4VplsIndex = u4NextVplsIndex;
                u4RtIndex = u4NextRtIndex;

                i4RetVal = nmhGetNextIndexVplsBgpRteTargetTable(u4VplsIndex, 
                                                             &u4NextVplsIndex,
                                                             u4RtIndex,
                                                             &u4NextRtIndex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 145:
            {
                UINT4 u4Mtu;
                i4RetVal = nmhGetFsmplsVplsMtu(1, &u4Mtu);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 146:
            {
                INT4 i4StorageType;
                i4RetVal = nmhGetFsmplsVplsStorageType(1, &i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 147:
            {
                INT4 i4SigType;
                i4RetVal = nmhGetFsmplsVplsSignalingType(1, &i4SigType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 148:
            {
                INT4 i4ControlWord;
                i4RetVal = nmhGetFsmplsVplsControlWord(1, &i4ControlWord);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 149:
        case 158:
            {
                UINT4 u4Mtu;
                UINT4 u4ErrorCode;

                u4Mtu = 1000;
                i4RetVal = nmhTestv2FsmplsVplsMtu(&u4ErrorCode, 1, u4Mtu);
                if ( ( 149 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 158 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsmplsVplsMtu(1, u4Mtu);
                if ( ( 149 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 158 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 150:
        case 159:
            {
                INT4 i4StorageType;
                UINT4 u4ErrorCode;

                i4StorageType = L2VPN_STORAGE_OTHER;
                i4RetVal = nmhTestv2FsmplsVplsStorageType(&u4ErrorCode, 1, 
                                                          i4StorageType);
                if ( 159 == i4UtId && SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_VOLATILE;
                i4RetVal = nmhTestv2FsmplsVplsStorageType(&u4ErrorCode, 1, 
                                                          i4StorageType);
                if ( 159 == i4UtId && SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_PERMANENT;
                i4RetVal = nmhTestv2FsmplsVplsStorageType(&u4ErrorCode, 1, 
                                                          i4StorageType);
                if ( 159 == i4UtId && SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_READONLY;
                i4RetVal = nmhTestv2FsmplsVplsStorageType(&u4ErrorCode, 1, 
                                                          i4StorageType);
                if ( 159 == i4UtId && SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4StorageType = L2VPN_STORAGE_NONVOLATILE;
                i4RetVal = nmhTestv2FsmplsVplsStorageType(&u4ErrorCode, 1, 
                                                          i4StorageType);
                if ( ( 150 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 159 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsmplsVplsStorageType(1, i4StorageType);
                if ( ( 150 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 159 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 151:
        case 160:
            {
                INT4 i4SigType;
                UINT4 u4ErrorCode;

                i4SigType = L2VPN_VPLS_SIG_BGP;
                i4RetVal = nmhTestv2FsmplsVplsSignalingType(&u4ErrorCode, 1, 
                                                            i4SigType);
                if ( ( 151 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 160 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsmplsVplsSignalingType(1, i4SigType);
                if ( ( 151 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 160 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 152:
        case 161:
            {
                INT4 i4ControlWord;
                UINT4 u4ErrorCode;

                i4ControlWord = L2VPN_ENABLED;
                i4RetVal = nmhTestv2FsmplsVplsControlWord(&u4ErrorCode, 1, 
                                                          i4ControlWord);
                if ( ( 152 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 161 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsmplsVplsControlWord(1, i4ControlWord);
                if ( ( 152 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 161 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 153:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsMplsVplsRowStatus(&u4ErrorCode, 1, 
                                                        CREATE_AND_WAIT);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsRowStatus(1, CREATE_AND_WAIT);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 154:
            {
                UINT4 u4Mtu;
                UINT4 u4ErrorCode;

                u4Mtu = L2VPN_PWVC_MAX_MTU_SIZE + 1;
                i4RetVal = nmhTestv2FsmplsVplsMtu(&u4ErrorCode, 1, u4Mtu);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 155:
            {
                INT4 i4StorageType;
                UINT4 u4ErrorCode;

                i4StorageType = 0;
                i4RetVal = nmhTestv2FsmplsVplsStorageType(&u4ErrorCode, 1, 
                                                          i4StorageType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 156:
            {
                INT4 i4SigType;
                UINT4 u4ErrorCode;

                i4SigType = L2VPN_VPLS_SIG_MAX;
                i4RetVal = nmhTestv2FsmplsVplsSignalingType(&u4ErrorCode, 1, 
                                                            i4SigType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4SigType = L2VPN_VPLS_SIG_MIN;
                i4RetVal = nmhTestv2FsmplsVplsSignalingType(&u4ErrorCode, 1, 
                                                            i4SigType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 157:
            {
                INT4 i4ControlWord;
                UINT4 u4ErrorCode;

                i4ControlWord = 0;
                i4RetVal = nmhTestv2FsmplsVplsControlWord(&u4ErrorCode, 1, 
                                                          i4ControlWord);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 162:
            {
                UINT1 au1GetRd[L2VPN_MAX_VPLS_RD_LEN];
                if ( L2VPN_FAILURE != L2VpnGetDefaultRdValue(3, au1GetRd) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 163:
            {
                tSNMP_OCTET_STRING_TYPE VpnId;
                tVPLSEntry  *pVplsEntry = NULL;
                UINT1 au1VpnId[L2VPN_MAX_VPLS_VPNID_LEN] = 
                             {'A', 'r', 'i', 0x00, 0x00, 0x00, 0x02};
                UINT1 au1GetRd[L2VPN_MAX_VPLS_RD_LEN];
                UINT1 au1TestRdType0[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x0d, 0x56, 0x00, 0x00, 0x00, 0x02};
                UINT1 au1TestRdType2[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x02, 0x00, 0x00, 0x00, 0x0d, 0x56, 0x00, 0x02};

                VpnId.i4_Length = L2VPN_MAX_VPLS_VPNID_LEN;
                VpnId.pu1_OctetList = au1VpnId;

                i4RetVal = nmhSetFsMplsVplsVpnId(1, &VpnId);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
                if ( NULL == pVplsEntry )
                {
                    u4RetVal = OSIX_FAILURE;
                    break;
                }
              
                MplsUpdateASN(0, 3414); 
                if ( L2VPN_FAILURE == L2VpnGetDefaultRdValue(1, au1GetRd) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1GetRd, au1TestRdType0, L2VPN_MAX_VPLS_RD_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                MEMSET(L2VPN_VPLS_DEFAULT_RD(pVplsEntry), 0, 
                       L2VPN_MAX_VPLS_RD_LEN);
               
                MplsUpdateASN(1, 3414); 
                if ( L2VPN_FAILURE == L2VpnGetDefaultRdValue(1, au1GetRd) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if ( L2VPN_FAILURE == L2VpnGetDefaultRdValue(1, au1GetRd) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1GetRd, au1TestRdType2, L2VPN_MAX_VPLS_RD_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
               
                MEMSET(L2VPN_VPLS_DEFAULT_RD(pVplsEntry), 0, 
                       L2VPN_MAX_VPLS_RD_LEN);
            }
            break;

        case 164:
            {
                UINT1 au1GetRt[L2VPN_MAX_VPLS_RT_LEN];
                if ( L2VPN_FAILURE != L2VpnGetDefaultRtValue(3, au1GetRt) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 165:
            {
                tVPLSEntry  *pVplsEntry = NULL;
                UINT1 au1GetRt[L2VPN_MAX_VPLS_RT_LEN];
                UINT1 au1TestRtType0[L2VPN_MAX_VPLS_RT_LEN] = 
                            {0x00, 0x02, 0x0d, 0x56, 0x00, 0x00, 0x00, 0x02};
                UINT1 au1TestRtType2[L2VPN_MAX_VPLS_RT_LEN] = 
                            {0x02, 0x02, 0x00, 0x00, 0x0d, 0x56, 0x00, 0x02};

                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
                if ( NULL == pVplsEntry )
                {
                    u4RetVal = OSIX_FAILURE;
                    break;
                }
               
                MplsUpdateASN(0, 3414); 
                if ( L2VPN_FAILURE == L2VpnGetDefaultRtValue(1, au1GetRt) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1GetRt, au1TestRtType0, L2VPN_MAX_VPLS_RT_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                MEMSET(L2VPN_VPLS_DEFAULT_RT(pVplsEntry), 0, 
                       L2VPN_MAX_VPLS_RT_LEN);
               
                MplsUpdateASN(1, 3414); 
                if ( L2VPN_FAILURE == L2VpnGetDefaultRtValue(1, au1GetRt) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if ( L2VPN_FAILURE == L2VpnGetDefaultRtValue(1, au1GetRt) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1GetRt, au1TestRtType2, L2VPN_MAX_VPLS_RT_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
               
                MEMSET(L2VPN_VPLS_DEFAULT_RT(pVplsEntry), 0, 
                       L2VPN_MAX_VPLS_RT_LEN);
            }
            break;

        case 166:
            {
                UINT4 u4VplsIndex;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x65};

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, NOT_IN_SERVICE);

                if ( L2VPN_FAILURE == L2VpnGetVplsIndexFromRd(au1Rd, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 2 != u4VplsIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 167:
            {
                UINT4 u4VplsIndex;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(1, ACTIVE);

                if ( L2VPN_FAILURE == L2VpnGetVplsIndexFromRd(au1Rd, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4VplsIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 168:
            {
                UINT4 u4VplsIndex;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x02};

                if ( L2VPN_FAILURE != L2VpnGetVplsIndexFromRd(au1Rd, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 169:
            {
                tSNMP_OCTET_STRING_TYPE VplsName;
                UINT4 u4VplsIndex;
                UINT1 au1RdType0[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x02};
                UINT1 au1RdType1[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x01,0x00,0x00,0x40,0x00,0x00,0x00,0x02};
                UINT1 au1RdType2[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x02,0x00,0x00,0x40,0x00,0x00,0x00,0x02};
                UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS1";

                VplsName.i4_Length = STRLEN(au1VplsName);
                VplsName.pu1_OctetList = au1VplsName;

                CreateVe(1,1,&u4RetVal);

                if ( SNMP_FAILURE == nmhSetFsMplsVplsName(1, &VplsName) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( SNMP_FAILURE == nmhSetFsMplsVplsRowStatus(1, ACTIVE) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( L2VPN_FAILURE == L2VpnGetVplsIndexFromRd(au1RdType0, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4VplsIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( L2VPN_FAILURE == L2VpnGetVplsIndexFromRd(au1RdType1, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4VplsIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( L2VPN_FAILURE == L2VpnGetVplsIndexFromRd(au1RdType2, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4VplsIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 170:
            {
                UINT4 u4VeId;

                if ( L2VPN_FAILURE != L2VpnGetLocalVeIdFromVPLSIndex(3, 
                                                                     &u4VeId) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 171:
            {
                UINT4 u4VeId;

                if ( L2VPN_FAILURE == L2VpnGetLocalVeIdFromVPLSIndex(1, 
                                                                     &u4VeId) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4VeId )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 172:
            {
                UINT4 u4LabelBase;
                UINT4 u4TestLabelBase = 400001;
                UINT4 u4Index;

                for ( u4Index = 1 ; u4Index <= 5000 ; u4Index++ )
                {
                    if ( L2VPN_FAILURE == L2VpnAllocateLb(&u4LabelBase) )
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    if ( u4LabelBase != u4TestLabelBase )
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    u4TestLabelBase += 10;
                }
                if ( L2VPN_FAILURE != L2VpnAllocateLb(&u4LabelBase) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 173:
            {
                UINT4 u4LabelBase = 400001;
                UINT4 u4Index;

                for ( u4Index = 1 ; u4Index <= 5000 ; u4Index++ )
                {
                    if ( L2VPN_FAILURE == L2VpnReleaseLb(u4LabelBase) )
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    u4LabelBase += 10;
                }
            }
            break;

        case 174:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN];
                if ( L2VPN_FAILURE != L2VpnGetExportRtValue(2, L2VPN_TRUE, 
                                                            au1Rt) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 175:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN];
                if ( L2VPN_FAILURE == L2VpnGetExportRtValue(1, L2VPN_TRUE,
                                                            au1Rt) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 176:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN];
                if ( L2VPN_FAILURE != L2VpnGetRdValue(4, au1Rd) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 177:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN];
                if ( L2VPN_FAILURE == L2VpnGetRdValue(1, au1Rd) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 178:
            {
                UINT4 u4VplsIndex; 
                UINT4 u4RtIndex;
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] =
                          {0x00,0x02,0x00,0x60,0x00,0x00,0x00,0x64};

                if ( L2VPN_FAILURE != L2VpnGetVplsAndRtIndexFromRtName(au1Rt,
                                                                  &u4VplsIndex,
                                                                  &u4RtIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 179:
            {
                UINT4 u4VplsIndex; 
                UINT4 u4RtIndex;
                UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] =
                          {0x01,0x02,0x00,0x00,0x00,0x02,0x00,0x65};

                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, 
                                                             NOT_IN_SERVICE);

                if ( L2VPN_FAILURE == L2VpnGetVplsAndRtIndexFromRtName(au1Rt1,
                                                                  &u4VplsIndex,
                                                                  &u4RtIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4VplsIndex || 2 != u4RtIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                MplsUpdateASN(1, 2);
                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(1, 1, ACTIVE);
                MplsUpdateASN(1, 3414); 
            }
            break;

        case 180:
            {
                UINT4 u4Index;
                UINT4 u4RtIndex;
                INT4  i4RetStatus;

                for ( u4Index = 1; u4Index <= MAX_L2VPN_VPLS_ENTRIES; 
                      u4Index ++ )
                {
                    i4RetStatus = L2VpnGetFreeRtIndexForVpls(6, &u4RtIndex);
                    if ( L2VPN_FAILURE == i4RetStatus )
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    L2VpnSetRtIndexForVpls(6, u4RtIndex);
                }

                i4RetStatus = L2VpnGetFreeRtIndexForVpls(6, &u4RtIndex);
                if ( L2VPN_FAILURE != i4RetStatus )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                L2VpnReleaseRtIndexForVpls(6, u4RtIndex);

                i4RetStatus = L2VpnGetFreeRtIndexForVpls(6, &u4RtIndex);
                if ( L2VPN_FAILURE == i4RetStatus )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 181:
            {
                UINT4 u4Index;
                UINT4 u4AcIndex;
                INT4  i4RetStatus;

                for ( u4Index = 1; u4Index <= MAX_L2VPN_VPLS_ENTRIES; 
                      u4Index ++ )
                {
                    i4RetStatus = L2VpnGetFreeAcIndexForVplsAc(1, &u4AcIndex);
                    if ( L2VPN_FAILURE == i4RetStatus )
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    L2VpnSetAcIndexForVplsAc(1, u4AcIndex);
                }

                i4RetStatus = L2VpnGetFreeAcIndexForVplsAc(1, &u4AcIndex);
                if ( L2VPN_FAILURE != i4RetStatus )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                L2VpnReleaseAcIndexForVplsAc(1, u4AcIndex);

                i4RetStatus = L2VpnGetFreeAcIndexForVplsAc(1, &u4AcIndex);
                if ( L2VPN_FAILURE == i4RetStatus )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 182:
            {
                UINT4 u4IfIndex;
                i4RetVal = nmhGetFsMplsVplsAcMapPortIfIndex(1, 1, &u4IfIndex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 183:
            {
                UINT4 u4PortVlan;
                i4RetVal = nmhGetFsMplsVplsAcMapPortVlan(1, 1, &u4PortVlan);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 184:
            {
                INT4 i4RowStatus;
                i4RetVal = nmhGetFsMplsVplsAcMapRowStatus(1, 1, &i4RowStatus);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 185:
        case 193:
        case 196:
            {
                UINT4 u4IfIndex = 0;
                UINT4 u4ErrorCode;

                u4IfIndex = 2;
                if ( 193 == i4UtId )
                {
                    MGMT_UNLOCK ();
                    CliExecuteAppCmd("c t");
                    CliExecuteAppCmd("i g 0/2");
                    CliExecuteAppCmd("map switch default");
                    CliExecuteAppCmd("no sh");
                    CliExecuteAppCmd("en");
                    MGMT_LOCK ();
                }
                i4RetVal = nmhTestv2FsMplsVplsAcMapPortIfIndex(&u4ErrorCode, 
                                                               1, 1, u4IfIndex);
                if ( ( 185 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 193 == i4UtId && SNMP_FAILURE == i4RetVal ) ||
                     ( 196 == i4UtId && SNMP_FAILURE != i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsAcMapPortIfIndex(1, 1, u4IfIndex);
                if ( ( 185 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 193 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 186:
        case 194:
        case 197:
            {
                UINT4 u4PortVlan = 0;
                UINT4 u4ErrorCode;

                u4PortVlan = 100;
                if ( 194 == i4UtId )
                {
                    MGMT_UNLOCK ();
                    CliExecuteAppCmd("c t");
                    CliExecuteAppCmd("vlan 100");
                    CliExecuteAppCmd("ports gig 0/2 untagged gig 0/2");
                    CliExecuteAppCmd("en");
                    MGMT_LOCK ();
                }
                i4RetVal = nmhTestv2FsMplsVplsAcMapPortVlan(&u4ErrorCode, 1, 1,
                                                         u4PortVlan);
                if ( ( 186 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 194 == i4UtId && SNMP_FAILURE == i4RetVal ) ||
                     ( 197 == i4UtId && SNMP_FAILURE != i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsAcMapPortVlan(1, 1, u4PortVlan);
                if ( ( 186 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 194 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 187:
        case 195:
            {
                INT4 i4RowStatus = 0;
                UINT4 u4ErrorCode;

                i4RowStatus = ACTIVE;
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 1, 
                                                             1, i4RowStatus);
                if ( ( 187 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 195 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(1, 1, i4RowStatus);
                if ( ( 187 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 195 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 188:
        case 198:
            {
                INT4 i4RowStatus = 0;
                UINT4 u4ErrorCode;

                i4RowStatus = NOT_IN_SERVICE;
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 1, 
                                                             1, i4RowStatus);
                if ( ( 188 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 198 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(1, 1, i4RowStatus);
                if ( ( 188 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 198 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;


        case 189:
        case 199:
            {
                INT4 i4RowStatus = 0;
                UINT4 u4ErrorCode;

                i4RowStatus = DESTROY;
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 1, 
                                                             1, i4RowStatus);
                if ( ( 189 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 199 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(1, 1, i4RowStatus);
                if ( ( 189 == i4UtId && SNMP_FAILURE != i4RetVal ) ||
                     ( 199 == i4UtId && SNMP_FAILURE == i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;


        case 190:
            {
                INT4 i4RowStatus = 0;
                UINT4 u4ErrorCode;

                i4RowStatus = CREATE_AND_WAIT;
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 1, 
                                                             1, i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(1, 1, i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 191:
            {
                UINT4 u4IfIndex = 0;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsMplsVplsAcMapPortIfIndex(&u4ErrorCode, 
                                                               1, 1, u4IfIndex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4IfIndex = 2;
                i4RetVal = nmhTestv2FsMplsVplsAcMapPortIfIndex(&u4ErrorCode, 
                                                               1, 1, u4IfIndex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 192:
            {
                UINT4 u4PortVlan = L2VPN_VLANCFG + 1;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsMplsVplsAcMapPortVlan(&u4ErrorCode, 1, 1,
                                                         u4PortVlan);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4PortVlan = 100;
                i4RetVal = nmhTestv2FsMplsVplsAcMapPortVlan(&u4ErrorCode, 1, 1,
                                                         u4PortVlan);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 200:
            {
                INT4 i4RowStatus = 0;
                UINT4 u4ErrorCode;

                i4RowStatus = CREATE_AND_WAIT;
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 0, 
                                                             1, i4RowStatus);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 201:
            {
                UINT4 u4VplsIndex;
                UINT4 u4AcIndex;
                UINT4 u4NextVplsIndex;
                UINT4 u4NextAcIndex;
                UINT4 u4IfIndex;
                UINT4 u4PortVlan;
                INT4  i4RowStatus;

                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/3");
                CliExecuteAppCmd("map switch default");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("vlan 200");
                CliExecuteAppCmd("ports gig 0/3 untagged gig 0/3");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/4");
                CliExecuteAppCmd("map switch default");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("vlan 300");
                CliExecuteAppCmd("ports gig 0/4 untagged gig 0/4");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();
                CreateAc(4, 1, 3, 200, &u4RetVal);
                CreateAc(4, 2, 4, 300, &u4RetVal);

                i4RetVal = nmhGetFirstIndexFsMplsVplsAcMapTable(&u4VplsIndex,
                                                                &u4AcIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 4 != u4VplsIndex && 1 != u4AcIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetFsMplsVplsAcMapPortIfIndex(u4VplsIndex, 
                                                       u4AcIndex, &u4IfIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetFsMplsVplsAcMapPortVlan(u4VplsIndex,
                                                       u4AcIndex, &u4PortVlan);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetFsMplsVplsAcMapRowStatus(u4VplsIndex, 
                                                      u4AcIndex, &i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetNextIndexFsMplsVplsAcMapTable(u4VplsIndex,
                                                               &u4NextVplsIndex,
                                                               u4AcIndex,
                                                               &u4NextAcIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                u4AcIndex = u4NextAcIndex;
                u4VplsIndex = u4NextVplsIndex;
                if ( 4 != u4VplsIndex && 2 != u4AcIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetFsMplsVplsAcMapPortIfIndex(u4VplsIndex, 
                                                       u4AcIndex, &u4IfIndex);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetFsMplsVplsAcMapPortVlan(u4VplsIndex,
                                                       u4AcIndex, &u4PortVlan);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetFsMplsVplsAcMapRowStatus(u4VplsIndex, 
                                                      u4AcIndex, &i4RowStatus);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhGetNextIndexFsMplsVplsAcMapTable(u4VplsIndex,
                                                               &u4NextVplsIndex,
                                                               u4AcIndex,
                                                               &u4NextAcIndex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 202:
            {
                UINT1 u1IsAcPresent;

                L2VpnIsActiveAcPresent(3, &u1IsAcPresent);
                if ( L2VPN_TRUE == u1IsAcPresent )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 203:
            {
                UINT1 u1IsAcPresent;

                L2VpnIsActiveAcPresent(4, &u1IsAcPresent);
                if ( L2VPN_TRUE != u1IsAcPresent )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 204:
            {
                UINT1 u1IsAcPresent;

                nmhSetFsMplsVplsAcMapRowStatus(4,1,NOT_IN_SERVICE);

                L2VpnIsActiveAcPresent(4, &u1IsAcPresent);
                if ( L2VPN_TRUE != u1IsAcPresent )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                nmhSetFsMplsVplsAcMapRowStatus(4,1,ACTIVE);
            }
            break;

        case 205:
            {
                UINT4 u4AcIndex;

                i4RetVal = L2VpnGetAcIndexFromIfandVlan(4, 200, &u4AcIndex);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 206:
            {
                UINT4 u4AcIndex;

                i4RetVal = L2VpnGetAcIndexFromIfandVlan(3, 200, &u4AcIndex);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 1 != u4AcIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 207:
            {
                UINT4 u4AcIndex;

                nmhSetFsMplsVplsAcMapRowStatus(4,1,NOT_IN_SERVICE);

                i4RetVal = L2VpnGetAcIndexFromIfandVlan(4, 300, &u4AcIndex);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 2 != u4AcIndex )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                nmhSetFsMplsVplsAcMapRowStatus(4,1,ACTIVE);
            }
            break;

        case 208:
            {
                i4RetVal = L2VpnSendVplsUpAdminEvent(10);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 209:
            {
                UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS10";

                CreateVplsEntry(10, L2VPN_VPLS_SIG_NONE, au1VplsName, 10, 
                                &u4RetVal);

                i4RetVal = L2VpnSendVplsUpAdminEvent(10);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 210:
            {
                UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS11";
                UINT1 au1VplsBgpVEName[L2VPN_MAX_VPLS_NAME_LEN] = "VE11";
                tSNMP_OCTET_STRING_TYPE VplsBgpVEName;
                
                VplsBgpVEName.i4_Length = 4;
                VplsBgpVEName.pu1_OctetList = au1VplsBgpVEName;
                CreateVe(11, 1, &u4RetVal);
                nmhSetVplsBgpVEName(11,1, &VplsBgpVEName);
                CreateVplsEntry(11, L2VPN_VPLS_SIG_BGP, au1VplsName, 11, 
                                &u4RetVal);

                i4RetVal = L2VpnSendVplsUpAdminEvent(11);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 211:
            {
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/5");
                CliExecuteAppCmd("map switch default");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("vlan 400");
                CliExecuteAppCmd("ports gig 0/5 untagged gig 0/5");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();
                CreateAc(11, 1, 5, 400, &u4RetVal);

                i4RetVal = L2VpnSendVplsUpAdminEvent(11);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 212:
            {
                i4RetVal = L2VpnSendVplsDownAdminEvent(9);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 213:
            {
                i4RetVal = L2VpnSendVplsDownAdminEvent(10);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 214:
            {
                i4RetVal = nmhSetFsMplsVplsRowStatus(11, NOT_IN_SERVICE);
                i4RetVal = L2VpnSendVplsDownAdminEvent(11);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 215:
            {
                i4RetVal = nmhSetFsMplsVplsRowStatus(11, ACTIVE);
                i4RetVal = L2VpnSendVplsDownAdminEvent(11);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 216:
            {
                i4RetVal = L2VpnVplsPwTablePopulate(1, 0, 250, 400001, 400002,
                                                    100, L2VPN_DISABLED, 1, 1);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 217:
            {
                MplsL2VpnSetPwVcIndex (1);
                i4RetVal = L2VpnVplsPwTablePopulate(1, 1, 250, 400001, 400002,
                                                    100, L2VPN_DISABLED, 1, 1);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 218:
            {
                MplsL2VpnRelPwVcIndex (1);
                i4RetVal = L2VpnVplsPwTablePopulate(1, 1, 250, 400001, 400002,
                                                    100, L2VPN_DISABLED, 1, 1);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 219:
            {
                MplsL2VpnRelPwVcIndex (1);
                i4RetVal = L2VpnVplsPwTablePopulate(1, 1, 250, 400001, 400002,
                                                    100, L2VPN_DISABLED, 1, 1);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 220:
            {
                i4RetVal = L2VpnVplsPwDestroy(2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 221:
            {
                i4RetVal = L2VpnVplsPwDestroy(1);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 222:
            {
                i4RetVal = L2VpnVplsNeighborCreate(8, 400001, 400002, 250,
                                                   1, 1, 100, L2VPN_DISABLED);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 223:
            {
                DeleteAc(4, 1, &u4RetVal);
                DeleteAc(4, 2, &u4RetVal);
                DeleteAc(11, 1, &u4RetVal);
                CreateAc(13, 1, 3, 200, &u4RetVal);
                i4RetVal = L2VpnVplsNeighborCreate(11, 400001, 400002, 250,
                                                   1, 1, 100, L2VPN_DISABLED);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                DeleteAc(13, 1, &u4RetVal);
            }
            break;

        case 224:
            {
                CreateAc(11, 1, 3, 200, &u4RetVal);
                CreateAc(11, 2, 4, 300, &u4RetVal);
                i4RetVal = L2VpnVplsNeighborCreate(11, 400001, 400002, 250,
                                                   1, 1, 100, L2VPN_DISABLED);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 225:
            {
                i4RetVal = L2VpnVplsNeighborCreate(11, 400004, 400005, 250,
                                                   4, 4, 100, L2VPN_DISABLED);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 226:
            {
                i4RetVal = L2VpnProcessVplsUpEvent(8);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 227:
            {
                i4RetVal = L2VpnProcessVplsUpEvent(11);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 228:
            {
                tL2vpnVplsAdminEvtInfo VplsAdminEvtInfo;
                
                MEMSET(&VplsAdminEvtInfo, 0, sizeof(VplsAdminEvtInfo));

                VplsAdminEvtInfo.u4EvtType = L2VPN_VPLS_UP_EVENT;
                i4RetVal = L2VpnProcessVplsAdminEvent(&VplsAdminEvtInfo);
            }
            break;

        case 229:
            {
                tL2vpnVplsAdminEvtInfo VplsAdminEvtInfo;
                
                MEMSET(&VplsAdminEvtInfo, 0, sizeof(VplsAdminEvtInfo));

                VplsAdminEvtInfo.u4EvtType = L2VPN_VPLS_DOWN_EVENT;
                i4RetVal = L2VpnProcessVplsAdminEvent(&VplsAdminEvtInfo);
            }
            break;

        case 230:
            {
                tL2vpnVplsAdminEvtInfo VplsAdminEvtInfo;
                
                MEMSET(&VplsAdminEvtInfo, 0, sizeof(VplsAdminEvtInfo));

                i4RetVal = L2VpnProcessVplsAdminEvent(&VplsAdminEvtInfo);
            }
            break;

        case 231:
            {
                i4RetVal = PwCreateDynamicEnetEntry(1,1,100,200,3,2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 232:
            {
                i4RetVal = PwCreateDynamicEnetEntry(1,3,100,200,3,2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 233:
            {
                i4RetVal = PwCreateDynamicEnetEntry(1,4,100,100,3,2);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 234:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(L2VpnBgpEvtInfo));
                i4RetVal = L2VpnBgpEventHandler(&L2VpnBgpEvtInfo);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 235:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00, 0x04, 0x03, 0x06, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(21);

                i4RetVal = L2VpnCliRdCreate(CliHandle, au1Rd);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 236:
        case 237:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00, 0x00, 0x00, 0x02, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(21);
                MplsUpdateASN(0, 2);

                i4RetVal = L2VpnCliRdCreate(CliHandle, au1Rd);
                if ( ( 236 == i4UtId && CLI_FAILURE == i4RetVal ) ||
                     ( 237 == i4UtId && CLI_FAILURE != i4RetVal ) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 238:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00, 0x00, 0x00, 0x02, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRdCreate(CliHandle, au1Rd);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 239:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00, 0x00, 0x00, 0x02, 0x03, 0x0a, 0x45, 0x54};
                UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS22";

                CreateVe(22,1,&u4RetVal);
                CreateVplsEntry(22, L2VPN_VPLS_SIG_BGP, au1VplsName, 22, 
                                &u4RetVal);

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRdCreate(CliHandle, au1Rd);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 240:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00, 0x00, 0x00, 0x02, 0x09, 0x0a, 0x45, 0x54};
                UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS23";

                CreateVe(23,1,&u4RetVal);
                CreateVplsEntry(23, L2VPN_VPLS_SIG_BGP, au1VplsName, 23, 
                                &u4RetVal);

                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliRdCreate(CliHandle, au1Rd);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 241:
            {
                CLI_SET_MPLS_VFI_ID(25);

                i4RetVal = L2VpnCliRdDelete(CliHandle);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 242:
            {
                CLI_SET_MPLS_VFI_ID(21);

                i4RetVal = L2VpnCliRdDelete(CliHandle);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 243:
            {
                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRdDelete(CliHandle);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 244:
            {
                CLI_SET_MPLS_VFI_ID(23);

                nmhSetFsMplsVplsRowStatus(23, NOT_IN_SERVICE);

                i4RetVal = L2VpnCliSetControlWord(CliHandle, 0);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 245:
            {
                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliSetControlWord(CliHandle, L2VPN_ENABLED);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 246:
            {
                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliSetControlWord(CliHandle, 0);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 247:
            {
                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliSetControlWord(CliHandle, L2VPN_DISABLED);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 248:
            {
                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliSetMtu(CliHandle, L2VPN_PWVC_MAX_MTU_SIZE+1);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 249:
            {
                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliSetMtu(CliHandle, 1000);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 250:
            {
                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliSetMtu(CliHandle, L2VPN_PWVC_MAX_MTU_SIZE+1);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 251:
            {
                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliSetMtu(CliHandle, L2VPN_PWVC_MAX_MTU_SIZE);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 252:
            {
                UINT1 au1VeName[L2VPN_MAX_VPLS_NAME_LEN + 1] = "ABC";

                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliVeCreate(CliHandle, 55, au1VeName);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 253:
            {
                UINT1 au1VeName[L2VPN_MAX_VPLS_NAME_LEN + 6];

                MEMSET(au1VeName, 50, L2VPN_MAX_VPLS_NAME_LEN + 6);

                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliVeCreate(CliHandle, 5, au1VeName);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 254:
            {
                UINT1 au1VeName[L2VPN_MAX_VPLS_NAME_LEN + 1] = "VE5";

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliVeCreate(CliHandle, 5, au1VeName);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 255:
            {
                UINT1 au1VeName[L2VPN_MAX_VPLS_NAME_LEN + 1] = "VE5";

                CLI_SET_MPLS_VFI_ID(23);

                DeleteVe(23,1,&u4RetVal);

                i4RetVal = L2VpnCliVeCreate(CliHandle, 5, au1VeName);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 256:
            {
                CLI_SET_MPLS_VFI_ID(27);

                i4RetVal = L2VpnCliVeDelete(CliHandle, 5);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 257:
            {
                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliVeDelete(CliHandle, 9);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 258:
            {
                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliVeDelete(CliHandle, 5);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 259:
            {
                CLI_SET_MPLS_VFI_ID(23);

                CreateVe(23,5,&u4RetVal);
                i4RetVal = L2VpnCliVeDelete(CliHandle, 5);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 260:
            {
                tVPLSEntry VplsEntry; 
                tL2vpnCliArgs L2vpnCliArgs;

                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 51;
                i4RetVal = L2VpnCreateVplsACMap(&VplsEntry, &L2vpnCliArgs);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 261:
            {
                tVPLSEntry VplsEntry; 
                tL2vpnCliArgs L2vpnCliArgs;

                MEMSET(&L2vpnCliArgs, 0, sizeof(L2vpnCliArgs));
                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 22;
                L2vpnCliArgs.i4PortIfIndex = 9;
                i4RetVal = L2VpnCreateVplsACMap(&VplsEntry, &L2vpnCliArgs);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 262:
            {
                tVPLSEntry VplsEntry; 
                tL2vpnCliArgs L2vpnCliArgs;

                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/9");
                CliExecuteAppCmd("map switch default");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(&L2vpnCliArgs, 0, sizeof(L2vpnCliArgs));
                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 22;
                L2vpnCliArgs.i4PortIfIndex = 9;
                L2vpnCliArgs.u4VlanId = 800;
                i4RetVal = L2VpnCreateVplsACMap(&VplsEntry, &L2vpnCliArgs);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 263:
            {
                tVPLSEntry VplsEntry; 
                tL2vpnCliArgs L2vpnCliArgs;

                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("vlan 800");
                CliExecuteAppCmd("ports gig 0/9 untagged gig 0/9");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(&L2vpnCliArgs, 0, sizeof(L2vpnCliArgs));
                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 22;
                L2vpnCliArgs.i4PortIfIndex = 9;
                L2vpnCliArgs.u4VlanId = 800;
                i4RetVal = L2VpnCreateVplsACMap(&VplsEntry, &L2vpnCliArgs);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 264:
            {
                tVPLSEntry VplsEntry; 
                tL2vpnCliArgs L2vpnCliArgs;

                MEMSET(&L2vpnCliArgs, 0, sizeof(L2vpnCliArgs));
                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 22;
                L2vpnCliArgs.i4PortIfIndex = 9;
                L2vpnCliArgs.u4VlanId = 800;
                i4RetVal = L2VpnCreateVplsACMap(&VplsEntry, &L2vpnCliArgs);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 265:
            {
                tVPLSEntry VplsEntry; 

                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 22;
                i4RetVal = L2VpnDeleteVplsACMap(&VplsEntry, 32, 45);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 266:
            {
                tVPLSEntry VplsEntry; 

                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 29;
                i4RetVal = L2VpnDeleteVplsACMap(&VplsEntry, 9, 800);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 267:
            {
                tVPLSEntry VplsEntry; 

                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 22;
                i4RetVal = L2VpnDeleteVplsACMap(&VplsEntry, 9, 800);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 268:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x04, 0x03, 0x06, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 
                                            L2VPN_VPLS_RT_EXPORT);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 269:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x03, 0x06, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 0); 
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 270:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0x0c, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 
                                            L2VPN_VPLS_RT_EXPORT);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 271:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0x0c, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 
                                            L2VPN_VPLS_RT_EXPORT);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 272:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0xc9, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 
                                            L2VPN_VPLS_RT_EXPORT);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 273:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0xb9, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 
                                            L2VPN_VPLS_RT_IMPORT);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 274:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtDelete(CliHandle, au1Rt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 275:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0x09, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(28);

                i4RetVal = L2VpnCliRtDelete(CliHandle, au1Rt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 276:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0xb9, 0x0a, 0x45, 0x54};

                CLI_SET_MPLS_VFI_ID(22);

                i4RetVal = L2VpnCliRtDelete(CliHandle, au1Rt);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 277:
            {
                UINT1 au1RandomString[50] = "500:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];
                UINT1 au1ValidateRdRt[L2VPN_MAX_VPLS_RD_LEN] = 
                       {0x00, 0x00, 0x01, 0xf4, 0x00, 0x00, 0x00, 0x64};

                MEMSET(au1RdRt, 0, L2VPN_MAX_VPLS_RD_LEN);

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1RdRt, au1ValidateRdRt, 
                                 L2VPN_MAX_VPLS_RD_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 278:
            {
                UINT1 au1RandomString[50] = "30.40.50.60:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];
                UINT1 au1ValidateRdRt[L2VPN_MAX_VPLS_RD_LEN] = 
                       {0x01, 0x00, 0x1e, 0x28, 0x32, 0x3c, 0x00, 0x64};

                MEMSET(au1RdRt, 0, L2VPN_MAX_VPLS_RD_LEN);
                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1RdRt, au1ValidateRdRt, 
                                 L2VPN_MAX_VPLS_RD_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 279:
            {
                UINT1 au1RandomString[50] = "30.60:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];
                UINT1 au1ValidateRdRt[L2VPN_MAX_VPLS_RD_LEN] = 
                       {0x02, 0x00, 0x00, 0x1e, 0x00, 0x3c, 0x00, 0x64};

                MEMSET(au1RdRt, 0, L2VPN_MAX_VPLS_RD_LEN);
                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1RdRt, au1ValidateRdRt, 
                                 L2VPN_MAX_VPLS_RD_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 280:
            {
                UINT1 au1RandomString[50] = "196607:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];
                UINT1 au1ValidateRdRt[L2VPN_MAX_VPLS_RD_LEN] = 
                       {0x02, 0x00, 0x00, 0x02, 0xff, 0xff, 0x00, 0x64};

                MEMSET(au1RdRt, 0, L2VPN_MAX_VPLS_RD_LEN);
                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if ( 0 != MEMCMP(au1RdRt, au1ValidateRdRt, 
                                 L2VPN_MAX_VPLS_RD_LEN) )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 281:
            {
                UINT1 au1RandomString[50] = "196607100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 282:
            {
                UINT1 au1RandomString[50] = "19660:71.00"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 283:
            {
                UINT1 au1RandomString[50] = "19.6.6:7100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 284:
            {
                UINT1 au1RandomString[50] = "1.9.6.6.7:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 285:
            {
                UINT1 au1RandomString[50] = "19:660:7100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 286:
            {
                UINT1 au1RandomString[50] = "19.66:87100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 287:
            {
                UINT1 au1RandomString1[50] = "500.55.60.80:100"; 
                UINT1 au1RandomString2[50] = "50.550.60.80:100"; 
                UINT1 au1RandomString3[50] = "50.50.600.80:100"; 
                UINT1 au1RandomString4[50] = "50.50.60.800:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString1, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString2, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString3, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString4, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 288:
            {
                UINT1 au1RandomString[50] = "96607.6456:100"; 
                UINT1 au1RandomString1[50] = "9607.96456:100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString1, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 289:
            {
                nmhSetFsMplsVplsAcMapRowStatus(11,2,NOT_IN_SERVICE);
                CreateAc(11, 3, 3, 0, &u4RetVal);
                CreateAc(11, 4, 0, 300, &u4RetVal);
                CreateAc(17, 4, 5, 300, &u4RetVal);
                MGMT_UNLOCK ();
                CliExecuteAppCmd("show vfi");
                MGMT_LOCK ();
            }
            break;

        case 290:
            {
                MGMT_UNLOCK ();
                CliExecuteAppCmd("show run mpls");
                MGMT_LOCK ();
            }
            break;

        case 291:
            u4RetVal = nmhSetVplsBgpVENameUT();

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;           
 
            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
             break;
 
        case 292:
            u4RetVal = nmhSetVplsBgpVEPreferenceUT();

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;
            
            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
            break;

        case 293:
            u4RetVal = nmhSetVplsBgpVEStorageTypeUT();

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;
            
            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
            break;

        case 294:
            u4RetVal = nmhTestv2VplsBgpVERowStatusUT (CREATE_AND_WAIT);
            u4RetVal = nmhSetVplsBgpVERowStatusUT (CREATE_AND_WAIT);

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 295:
                u4RetVal = nmhTestv2VplsBgpVENameUT();
                    u4RetVal = nmhSetVplsBgpVENameUT();
            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                    break;

        case 296:
                u4RetVal = nmhTestv2VplsBgpVEPreferenceUT();
                u4RetVal = nmhSetVplsBgpVEPreferenceUT();
            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 297:
            u4RetVal = nmhTestv2VplsBgpVEStorageTypeUT();
            u4RetVal = nmhSetVplsBgpVEStorageTypeUT();

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
            break;

        case 298:
            u4RetVal = nmhTestv2VplsBgpVERowStatusUT (ACTIVE);
            u4RetVal = nmhSetVplsBgpVERowStatusUT (ACTIVE);

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 299:
            u4RetVal = nmhGetVplsBgpVEPreferenceUT(SET);

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 300:
            u4RetVal = nmhGetVplsBgpVENameUT();

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 301:
            u4RetVal = nmhGetVplsBgpVEStorageTypeUT(SET);

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 302:
            u4RetVal = nmhGetNextIndexVplsBgpVETableUT();
            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 303:
            u4RetVal = nmhGetVplsBgpPwBindRemoteVEIdUT();
            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 304:
            u4RetVal = nmhGetFirstIndexVplsBgpPwBindTableUT();
            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 305:
            u4RetVal = nmhGetVplsBgpPwBindLocalVEIdUT();
            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 306:
            u4RetVal = nmhGetFirstIndexVplsBgpConfigTableUT();
            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 307:
            u4RetVal = nmhGetFirstIndexVplsBgpVETableUT(SET);

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 308:
            u4RetVal = nmhTestv2VplsBgpVERowStatusUT (DESTROY);
            u4RetVal = nmhSetVplsBgpVERowStatusUT (DESTROY);

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;
        case 309:
            u4RetVal = nmhGetVplsBgpVEPreferenceUT(NOT_SET);

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 310:
            u4RetVal = nmhGetVplsBgpVENameUT();

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;

            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;
                break;

        case 311:
            u4RetVal = nmhGetVplsBgpVEStorageTypeUT(NOT_SET);

            if (u4RetVal == SNMP_FAILURE)
                u4RetVal = SNMP_SUCCESS;
            if ( SNMP_SUCCESS == u4RetVal )
                u4RetVal = OSIX_SUCCESS;
            else
                u4RetVal = OSIX_FAILURE;

                break;

        case 312:
            {
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(NULL, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 313:
            {
                UINT1 au1RandomString[50] = "19:"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 314:
            {
                UINT1 au1RandomString[50] = "1ab9.6:7100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 315:
            {
                UINT1 au1RandomString[50] = "19.6.6.8:97100"; 
                UINT1 au1RdRt[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = L2VpnCliParseAndGenerateRdRt(au1RandomString, 
                                                        au1RdRt);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 316:
            {
                tVPLSEntry VplsEntry; 
                tL2vpnCliArgs L2vpnCliArgs;
                UINT4 u4Index;

                for ( u4Index = 1; u4Index <= MAX_L2VPN_VPLS_ENTRIES; 
                      u4Index ++ )
                {
                    L2VpnSetAcIndexForVplsAc(19, u4Index);
                }

                L2VPN_PWVC_VPLS_INDEX(&VplsEntry) = 19;
                i4RetVal = L2VpnCreateVplsACMap(&VplsEntry, &L2vpnCliArgs);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                for ( u4Index = 1; u4Index <= MAX_L2VPN_VPLS_ENTRIES; 
                      u4Index ++ )
                {
                    L2VpnReleaseAcIndexForVplsAc(19, u4Index);
                }
            }
            break;

        case 317:
            {
                UINT1 au1VeName[L2VPN_MAX_VPLS_NAME_LEN + 20] = 
                                        "123456789123456789123456789123456789";

                CLI_SET_MPLS_VFI_ID(30);

                i4RetVal = L2VpnCliVeCreate(CliHandle, 5, au1VeName);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 318:
            {
                UINT4 u4Index;
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = "asas";

                CLI_SET_MPLS_VFI_ID(19);
                for ( u4Index = 1; u4Index <= MAX_L2VPN_VPLS_ENTRIES; 
                      u4Index ++ )
                {
                    L2VpnSetRtIndexForVpls(19, u4Index);
                }

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 2);
                if ( CLI_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                for ( u4Index = 1; u4Index <= MAX_L2VPN_VPLS_ENTRIES; 
                      u4Index ++ )
                {
                    L2VpnReleaseRtIndexForVpls(19, u4Index);
                }
            }
            break;

        case 319:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0x09, 0x0a, 0xd5, 0x54};

                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliRtCreate(CliHandle, au1Rt, 
                                            L2VPN_VPLS_RT_IMPORT);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 320:
            {
                UINT1 au1Rt[L2VPN_MAX_VPLS_RT_LEN] = 
                             {0x00, 0x02, 0x00, 0x02, 0x09, 0x0a, 0xd5, 0x54};

                CLI_SET_MPLS_VFI_ID(23);

                i4RetVal = L2VpnCliRtDelete(CliHandle, au1Rt);
                if ( CLI_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 321:
            {
                L2VpnCliShowRunningVplsAc(CliHandle, 19,0,0);
            }
            break;

        case 322:
            {
                L2VpnCliShowRunningVplsAc(CliHandle, 11, 0xFFFFFFFF, 0);
                L2VpnCliShowRunningVplsAc(CliHandle, 11, 0, 0);
            }
            break;

        case 323:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher2;
                tVPLSEntry VplsEntry;
                UINT1 au1VplsName[L2VPN_MAX_VPLS_NAME_LEN] = "VPLS19";
                UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x01,0x00,0x01,0x20,0x01,0x03,0x00,0x64};
                UINT1 au1Rd2[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x02,0x00,0x01,0x20,0x01,0x03,0x00,0x64};

                VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;
                VplsRouteDistinguisher2.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher2.pu1_OctetList = au1Rd2;
 
                L2VPN_VPLS_INDEX(&VplsEntry) = 19;
                L2VpnCliShowAutoDiscoveredVfiDetails(CliHandle, &VplsEntry);

                CreateVe(19, 1, &u4RetVal);
                CreateVplsEntry(19, L2VPN_VPLS_SIG_BGP, au1VplsName, 19, 
                                &u4RetVal);
                nmhSetFsMplsVplsRowStatus(19, NOT_IN_SERVICE);
                CreateRd(19, &VplsRouteDistinguisher1, &u4RetVal);
                nmhSetFsMplsVplsRowStatus(19, ACTIVE);
                L2VPN_VPLS_INDEX(&VplsEntry) = 19;
                L2VpnCliShowAutoDiscoveredVfiDetails(CliHandle, &VplsEntry);
                nmhSetFsMplsVplsRowStatus(19, NOT_IN_SERVICE);
                DeleteRd(19, &u4RetVal);
                nmhSetFsMplsVplsRowStatus(19, ACTIVE);

                nmhSetFsMplsVplsRowStatus(19, NOT_IN_SERVICE);
                CreateRd(19, &VplsRouteDistinguisher2, &u4RetVal);
                nmhSetFsMplsVplsRowStatus(19, ACTIVE);
                L2VPN_VPLS_INDEX(&VplsEntry) = 19;
                L2VpnCliShowAutoDiscoveredVfiDetails(CliHandle, &VplsEntry);
                nmhSetFsMplsVplsRowStatus(19, NOT_IN_SERVICE);
                DeleteRd(19, &u4RetVal);
                nmhSetFsMplsVplsRowStatus(19, ACTIVE);

                nmhSetFsMplsVplsRowStatus(19, NOT_IN_SERVICE);
                DeleteVe(19, 1, &u4RetVal);
                L2VPN_VPLS_INDEX(&VplsEntry) = 19;
                L2VpnCliShowAutoDiscoveredVfiDetails(CliHandle, &VplsEntry);
                CreateVe(19,1,&u4RetVal);
                nmhSetFsMplsVplsRowStatus(19, ACTIVE);
            }
            break;

        case 324:
            {
                i4RetVal = L2VpnVplsPwTablePopulate(1, 5000, 250, 400001, 400002,
                                                    100, L2VPN_DISABLED, 1, 1);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = L2VpnVplsPwTablePopulate(1, 6, 250, 0, 0,
                                                    500, L2VPN_DISABLED, 5, 5);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 325:
            {
                INT4 i4SigType;
                UINT4 u4ErrorCode;

                i4SigType = L2VPN_VPLS_SIG_BGP;
                i4RetVal = nmhTestv2FsmplsVplsSignalingType(&u4ErrorCode, 19, 
                                                            i4SigType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 326:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsmplsVplsControlWord(&u4ErrorCode, 19, 
                                                          L2VPN_ENABLED);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 327:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsmplsVplsMtu(&u4ErrorCode, 19, 
                                                  1000);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 328:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(19, 1,
                                              CREATE_AND_WAIT);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 19,
                                                             1, ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetFsMplsVplsAcMapPortVlan(19, 1, 400);
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 19,
                                                             1, ACTIVE);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 329:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 19,
                                                             1, CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 330:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 19,
                                                             1, 0);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(19, 1,
                                              0);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 19,
                                                             1, CREATE_AND_GO);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(&u4ErrorCode, 19,
                                                             1, NOT_READY);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 331:
            {
                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(19, 1,
                                                          ACTIVE);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                i4RetVal = nmhSetFsMplsVplsAcMapRowStatus(19, 1,
                                                          ACTIVE);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 332:
            {
                tSNMP_OCTET_STRING_TYPE RouteDistinguisher;
                UINT4 u4ConfigPrefix = 0;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigRouteDistinguisher(
                                         &u4ErrorCode, 1, &RouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigPrefix(
                                         &u4ErrorCode, 1, u4ConfigPrefix);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigVplsId(
                                         &u4ErrorCode, 1, &RouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 333:
            {
                tSNMP_OCTET_STRING_TYPE RouteTarget;
                INT4 i4RtType = 0;
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTType(
                                         &u4ErrorCode, 1, 1, i4RtType);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRT(
                                         &u4ErrorCode, 1, 1, &RouteTarget);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 334:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 0,
                                                             CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 100,
                                                             CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 335:
            {
                UINT4 u4ErrorCode;
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
                UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x64};
                VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;
                i4RetVal = nmhSetVplsBgpADConfigRowStatus(3, CREATE_AND_WAIT);
                i4RetVal = nmhSetVplsBgpADConfigRouteDistinguisher(3,
                                                    &VplsRouteDistinguisher1);

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 3,
                                                             ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 336:
            {
                UINT4 u4ErrorCode;
                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(&u4ErrorCode, 1,
                                                             DESTROY);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 337:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode,
                                                        0, 1, CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode,
                                                      100, 1, CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 338:
            {
                UINT4 u4ErrorCode;
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
                UINT1 au1Rt[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00,0x02,0x00,0x20,0x00,0x00,0x00,0x69};
                VplsRouteTarget.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget.pu1_OctetList = au1Rt;
                i4RetVal = nmhSetVplsBgpRteTargetRTRowStatus(19, 1, 
                                                            CREATE_AND_WAIT);
                i4RetVal = nmhSetVplsBgpRteTargetRT(19, 1,
                                                    &VplsRouteTarget);

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode,
                                                      19, 1, CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode,
                                                          19, 1, ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 339:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode,
                                                          19, 1, DESTROY);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 340:
            {
                UINT4 u4ErrorCode;
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget;
                UINT1 au1Rt[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x00,0x02,0x00,0x02,0x00,0x0e,0x00,0x69};
                VplsRouteTarget.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget.pu1_OctetList = au1Rt;
                i4RetVal = nmhSetVplsBgpRteTargetRT(19, 1,
                                                    &VplsRouteTarget);
                i4RetVal = nmhSetVplsBgpRteTargetRTType(19, 1,
                                                    L2VPN_VPLS_RT_IMPORT);

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(&u4ErrorCode,
                                                          19, 1, ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 341:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
                L2VPN_BGP_VPLS_MSG_TYPE(&L2VpnBgpEvtInfo) = 
                                              L2VPN_BGP_ADVERTISE_MSG;
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 342:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
                L2VPN_BGP_VPLS_MSG_TYPE(&L2VpnBgpEvtInfo) = 
                                              L2VPN_BGP_WITHDRAW_MSG;
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 343:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
                L2VPN_BGP_VPLS_MSG_TYPE(&L2VpnBgpEvtInfo) = 
                                              L2VPN_BGP_ADMIN_UP;
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 344:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
                L2VPN_BGP_VPLS_MSG_TYPE(&L2VpnBgpEvtInfo) = 
                                              L2VPN_BGP_ADMIN_DOWN;
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
                L2VPN_BGP_VPLS_MSG_TYPE(&L2VpnBgpEvtInfo) = 
                                              L2VPN_BGP_ADMIN_UP;
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 345:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
                L2VPN_BGP_VPLS_MSG_TYPE(&L2VpnBgpEvtInfo) = 
                                              L2VPN_BGP_MODULE_DOWN;
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 346:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));
                L2VpnProcessBgpEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 347:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 348:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x65};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 349:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 350:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                CreateAc(1, 1, 3, 600, &u4RetVal);
                sleep(5);
                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 370546198;
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 351:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 11;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 370546198;
                
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 352:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 0;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 0;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 370546198;
                
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 353:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 1;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_LB(&L2VpnBgpEvtInfo) = 300011;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 370546198;
                
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 354:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 1;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_LB(&L2VpnBgpEvtInfo) = 300011;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 370546198;
                
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 355:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 1;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 5;
                L2VPN_BGP_VPLS_LB(&L2VpnBgpEvtInfo) = 300051;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 370546199;
                
                L2VpnProcessBgpAdvEvent(&L2VpnBgpEvtInfo);
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpPwBindTable");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
            }
            break;

        case 356:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 357:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x65};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 358:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 359:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 11;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 360:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 0;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 0;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 361:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 1;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_LB(&L2VpnBgpEvtInfo) = 300011;
                
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 362:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 1;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_LB(&L2VpnBgpEvtInfo) = 300011;
                
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 363:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                L2VPN_BGP_VPLS_MTU(&L2VpnBgpEvtInfo) = 1518;
                L2VPN_BGP_VPLS_CONTROL_FLAG(&L2VpnBgpEvtInfo) = L2VPN_ENABLED;
                L2VPN_BGP_VPLS_VBO(&L2VpnBgpEvtInfo) = 1;
                L2VPN_BGP_VPLS_VBS(&L2VpnBgpEvtInfo) = 10;
                L2VPN_BGP_VPLS_VE_ID(&L2VpnBgpEvtInfo) = 5;
                L2VPN_BGP_VPLS_LB(&L2VpnBgpEvtInfo) = 300051;
                L2VPN_BGP_VPLS_IP_ADDR(&L2VpnBgpEvtInfo) = 346;
                
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 364:
            {
                tL2VpnBgpEvtInfo L2VpnBgpEvtInfo;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x64};

                MEMSET(&L2VpnBgpEvtInfo, 0, sizeof(tL2VpnBgpEvtInfo));

                MEMCPY(L2VPN_BGP_VPLS_RD(&L2VpnBgpEvtInfo), au1Rd,
                                         L2VPN_MAX_VPLS_RD_LEN);
                DeleteAc(1, 1, &u4RetVal);
                sleep(5);
                L2VpnProcessBgpWithdrawEvent(&L2VpnBgpEvtInfo);
            }
            break;

        case 365:
            {
                i4RetVal = L2VpnSendBgpAdvEvent(6,2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 366:
            {
                i4RetVal = L2VpnSendBgpWithdrawEvent(6,2,2,2,2,NULL,NULL,1);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 367:
            {
                MplsSetBgpAdminState(L2VPN_BGP_ADMIN_DOWN);
                i4RetVal = L2VpnSendBgpAdvEvent(11,2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 368:
            {
                MplsSetBgpAdminState(L2VPN_BGP_ADMIN_UP);
                i4RetVal = L2VpnSendBgpAdvEvent(11,2);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 369:
            {
                UINT1 au1RD[L2VPN_MAX_VPLS_RD_LEN] = "Saurabh";
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "Gupta";

                i4RetVal = L2VpnSendBgpWithdrawEvent(11,2,2,2,2,au1RD,au1RT,1);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 370:
            {
                tVPLSLBInfo VPLSLBInfoBase;
                UINT1 au1RD[L2VPN_MAX_VPLS_RD_LEN] = "Saurabh";
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "Gupta";

                i4RetVal = L2VpnProcessVplsDownEvent(6,100,1,
                                 &VPLSLBInfoBase,au1RD,au1RT,1);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 371:
            {
                tVPLSEntry    *pVplsEntry = NULL;
                UINT1 au1RD[L2VPN_MAX_VPLS_RD_LEN] = "Saurabh";
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "Gupta";

                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (11);
                if ( NULL == pVplsEntry )
                {
                    u4RetVal = OSIX_FAILURE;
                    break;
                }
                i4RetVal = L2VpnProcessVplsDownEvent(11,100,1,
                           L2VPN_VPLS_LABEL_BLOCK(pVplsEntry),au1RD,au1RT,1);
                if ( L2VPN_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 372:
            {
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN];

                i4RetVal = nmhSetVplsBgpADConfigRowStatus(8, CREATE_AND_WAIT);
                i4RetVal = L2VpnGetRdValue(8, au1Rd);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 373:
            {
                UINT4 u4VplsIndex;
                UINT1 au1Rd[L2VPN_MAX_VPLS_RD_LEN] = 
                            {0x05,0x00,0x00,0x20,0x00,0x00,0x00,0x65};

                if ( L2VPN_FAILURE != L2VpnGetVplsIndexFromRd(au1Rd, 
                                                              &u4VplsIndex) )
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;

        case 374:
            {
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "saurabh";
                L2VpnSendBgpRtAddEvent(1, au1RT, L2VPN_VPLS_RT_IMPORT);
            }
            break;

        case 375:
            {
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "saurabh";
                L2VpnSendBgpRtAddEvent(1, au1RT, 0);
            }
            break;

        case 376:
            {
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "saurabh";
                L2VpnSendBgpRtDeleteEvent(1, au1RT, L2VPN_VPLS_RT_IMPORT);
            }
            break;

        case 377:
            {
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "saurabh";
                L2VpnSendBgpRtDeleteEvent(1, au1RT, L2VPN_VPLS_RT_EXPORT);
            }
            break;

        case 378:
            {
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "saurabh";
                L2VpnSendBgpRtDeleteEvent(1, au1RT, L2VPN_VPLS_RT_BOTH);
            }
            break;

        case 379:
            {
                UINT1 au1RT[L2VPN_MAX_VPLS_RT_LEN] = "saurabh";
                L2VpnSendBgpRtDeleteEvent(1, au1RT, 0);
            }
            break;

        case 380:
            {
                L2VpnProcessBgpDownEvent();
            }
            break;

        case 381:
            {
                L2VpnProcessBgpUpEvent();
            }
            break;

        case 382:
            {
                L2VpnSendBgpVplsCreateEvent(8);
            }
            break;

        case 383:
            {
                L2VpnSendBgpVplsCreateEvent(23);
            }
            break;

        case 384:
            {
                L2VpnSendBgpVplsCreateEvent(11);
            }
            break;

        case 385:
            {
                L2VpnSendBgpVplsDeleteEvent(11);
            }
            break;

        case 386:
            {
                MplsSetBgpAdminState(L2VPN_BGP_ADMIN_DOWN);
                L2VpnSendBgpVplsUpEvent(8);
            }
            break;

        case 387:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteTarget1;
                UINT1 au1Rt1[L2VPN_MAX_VPLS_RT_LEN] = 
                          {0x00,0x02,0x00,0x20,0x00,0x00,0x45,0x64};
                MplsSetBgpAdminState(L2VPN_BGP_ADMIN_UP);
                VplsRouteTarget1.i4_Length = L2VPN_MAX_VPLS_RT_LEN;
                VplsRouteTarget1.pu1_OctetList = au1Rt1;
                CreateRt(8, 1, L2VPN_VPLS_RT_EXPORT, 
                         &VplsRouteTarget1, &u4RetVal);
                nmhSetVplsBgpADConfigRowStatus(8, ACTIVE);
                L2VpnSendBgpVplsUpEvent(8);
            }
            break;

        case 388:
            {
                L2VpnSendBgpVplsUpEvent(23);
            }
            break;

        case 389:
            {
                L2VpnSendBgpVplsUpEvent(11);
            }
            break;

        case 390:
            {
                L2VpnSendBgpVplsDownEvent(11);
            }
            break;

        case 391:
            {
                UINT4 u4LabelBase;
                UINT4 u4TestLabelBase = 400001;
                UINT4 u4Index;

                for ( u4Index = 1 ; u4Index <= 5000 ; u4Index++ )
                {
                    if ( L2VPN_FAILURE == L2VpnAllocateLb(&u4LabelBase) )
                    {
                        break;
                    }
                    u4TestLabelBase += 10;
                }
                i4RetVal = L2VpnSendBgpAdvEvent(11,2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
                for ( u4Index = 1 ; u4Index <= 5000 ; u4Index++ )
                {
                    if ( L2VPN_FAILURE == L2VpnReleaseLb(u4LabelBase) )
                    {
                        break;
                    }
                    u4TestLabelBase += 10;
                }
            }
            break;

        case 392:
            {
                i4RetVal = L2VpnSendBgpAdvEvent(23,2);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 393:
            {
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpVETable");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpADConfigEntry");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpRteTargetEntry");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpADConfigEntry");
                CliExecuteAppCmd("snmpwalk mib name fsMplsVplsConfigEntry");
                CliExecuteAppCmd("snmpwalk mib name fsMplsVplsAcMapTable");
                CliExecuteAppCmd("snmpwalk mib name vplsBgpPwBindTable");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
            }
            break;

        case 394:
            {
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher;
                UINT4 u4ErrorCode;

                L2VPN_ADMIN_STATUS = L2VPN_ADMIN_DOWN;
                i4RetVal = nmhTestv2VplsBgpADConfigRouteDistinguisher(
                                               &u4ErrorCode,1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigPrefix(
                                               &u4ErrorCode,1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigVplsId(
                                               &u4ErrorCode,1, 
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigRowStatus(
                                               &u4ErrorCode,1, 
                                               ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpADConfigStorageType(
                                               &u4ErrorCode,1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTType(
                                               &u4ErrorCode,1, 1,
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRT(
                                               &u4ErrorCode,1, 1,
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetRTRowStatus(
                                               &u4ErrorCode,1, 1,
                                               ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpRteTargetStorageType(
                                               &u4ErrorCode,1, 1,
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpVEName(
                                               &u4ErrorCode,1, 1,
                                               &VplsRouteDistinguisher);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpVEPreference(
                                               &u4ErrorCode,1, 1,
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpVERowStatus(
                                               &u4ErrorCode,1, 1,
                                               ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpVEStorageType(
                                               &u4ErrorCode,1, 1,
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsMplsVplsAcMapPortIfIndex(
                                               &u4ErrorCode,1, 1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsMplsVplsAcMapPortVlan(
                                               &u4ErrorCode,1, 1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(
                                               &u4ErrorCode,1, 1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsmplsVplsMtu(
                                               &u4ErrorCode,1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsmplsVplsStorageType(
                                               &u4ErrorCode,1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsmplsVplsSignalingType(
                                               &u4ErrorCode,1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2FsmplsVplsControlWord(
                                               &u4ErrorCode,1, 
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                L2VPN_ADMIN_STATUS = L2VPN_ADMIN_UP;
            }
            break;

        case 395:
            {
                UINT4 u4vplsindex;
                UINT4 u4pwindex;

                nmhGetNextIndexVplsBgpPwBindTable(0, &u4vplsindex, 
                                                  1, &u4pwindex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                nmhGetNextIndexVplsBgpPwBindTable(900, &u4vplsindex, 
                                                  1, &u4pwindex);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 396:
            {
                UINT4 u4ErrorCode;
                i4RetVal = nmhTestv2VplsBgpVEStorageType(
                                               &u4ErrorCode,1, 11,
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 397:
            {
                UINT4 u4ErrorCode;
                i4RetVal = nmhTestv2VplsBgpVEStorageType(
                                               &u4ErrorCode,1, 1,
                                               0);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 398:
            {
                UINT4 u4ErrorCode;
                tSNMP_OCTET_STRING_TYPE VplsBgpVEName;
                i4RetVal = nmhTestv2VplsBgpVEName(
                                               &u4ErrorCode,1, 11,
                                               &VplsBgpVEName);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 399:
            {
                UINT4 u4ErrorCode;
                i4RetVal = nmhTestv2VplsBgpVEPreference(
                                               &u4ErrorCode,1, 11,
                                               2);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 400:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpVERowStatus(
                                               &u4ErrorCode,1, 1,
                                               CREATE_AND_WAIT);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;


        case 401:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpVERowStatus(
                                               &u4ErrorCode,1, 1,
                                               NOT_IN_SERVICE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 402:
            {
                UINT4 u4ErrorCode;

                i4RetVal = nmhTestv2VplsBgpVERowStatus(
                                               &u4ErrorCode,1, 1,
                                               CREATE_AND_GO);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpVERowStatus(
                                               &u4ErrorCode,1, 1,
                                               NOT_READY);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhTestv2VplsBgpVERowStatus(
                                               &u4ErrorCode,1, 1,
                                               0);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 403:
            {
                i4RetVal = nmhSetVplsBgpVERowStatus(
                                               1, 11,
                                               ACTIVE);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 404:
            {
                i4RetVal = nmhSetVplsBgpVERowStatus(
                                               1, 1,
                                               NOT_IN_SERVICE);
                if ( SNMP_FAILURE == i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 405:
            {
                i4RetVal = nmhSetVplsBgpVERowStatus(
                                               1, 1,
                                               CREATE_AND_GO);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpVERowStatus(
                                               1, 1,
                                               NOT_READY);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }

                i4RetVal = nmhSetVplsBgpVERowStatus(
                                               1, 1,
                                               0);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 406:
            {
                INT4 i4RowStatus;

                i4RetVal = nmhGetVplsBgpVERowStatus(
                                               1, 11,
                                               &i4RowStatus);
                if ( SNMP_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 407:
            {
                L2VpnCliShowRunningRt(CliHandle, 19);
            }
            break;

        case 408:
            {
                L2VpnCliShowRunningRt(CliHandle, 1);
            }
            break;

        case 409:
            {
                tVPLSEntry *pVplsEntry;
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher1;
                tSNMP_OCTET_STRING_TYPE VplsRouteDistinguisher2;
                UINT1 au1Rd1[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x02,0x00,0x00,0x00,0x00,0x02,0x0c,0x64};
                UINT1 au1Rd2[L2VPN_MAX_VPLS_RD_LEN] = 
                             {0x01,0x00,0x00,0x20,0x03,0x00,0x0d,0x65};
                
                VplsRouteDistinguisher1.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher1.pu1_OctetList = au1Rd1;

                VplsRouteDistinguisher2.i4_Length = L2VPN_MAX_VPLS_RD_LEN;
                VplsRouteDistinguisher2.pu1_OctetList = au1Rd2;

                DeleteRd(1, &u4RetVal);
                DeleteRd(2, &u4RetVal);
                DeleteRd(3, &u4RetVal);
                DeleteRd(8, &u4RetVal);

                MplsUpdateASN(1, 2);
                CreateRd(1, &VplsRouteDistinguisher1, &u4RetVal);
                CreateRd(10, &VplsRouteDistinguisher2, &u4RetVal);
                MplsUpdateASN(0, 2);

                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (1);
                L2VpnCliShowRunningAutoDiscoveredVfi(CliHandle, pVplsEntry);
                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (10);
                L2VpnCliShowRunningAutoDiscoveredVfi(CliHandle, pVplsEntry);
            }
            break;

        case 410:
            {
                tVPLSEntry *pVplsEntry;

                nmhSetVplsBgpADConfigRowStatus(10, NOT_IN_SERVICE);
                pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (10);
                L2VpnCliShowRunningAutoDiscoveredVfi(CliHandle, pVplsEntry);
                DeleteRd(1, &u4RetVal);
                DeleteRd(10, &u4RetVal);
            }
            break;

        case 411:
            {
                i4RetVal = L2VpnVplsNeighborCreate(23, 400001, 400002, 250,
                                                   1, 1, 100, L2VPN_DISABLED);
                if ( L2VPN_FAILURE != i4RetVal )
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        default:
            CliPrintf(CliHandle,"%% No test case is defined for ID: %d\r\n",
                      i4UtId);
            break;
    }

    /* Display the result */
    if (u4RetVal == OSIX_FAILURE)
    {
        CliPrintf(CliHandle,"!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!\r\n",
                  i4UtId, "FAILED");
        u4TotalFailedCases++;
    }
    else
    {
        CliPrintf(CliHandle,"$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$\r\n",
                  i4UtId, "PASSED");
        u4TotalPassedCases++;
    }
}

INT1
nmhGetFirstIndexVplsBgpConfigTableUT (VOID)
{
        UINT4 *pu4VplsConfigIndex = (UINT4 *)malloc(sizeof(UINT4));
        UINT1  u4RetVal = SNMP_FAILURE;

        u4RetVal = nmhGetFirstIndexVplsBgpConfigTable(pu4VplsConfigIndex);
        
        return u4RetVal;
}

INT1
nmhGetVplsBgpPwBindLocalVEIdUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT4 *pu4RetValVplsBgpPwBindLocalVEId = (UINT4 *)malloc(sizeof(UINT4));
        UINT1  u4RetVal = SNMP_FAILURE;

        u4RetVal = nmhGetVplsBgpPwBindLocalVEId(u4VplsConfigIndex, u4VplsBgpVEId, pu4RetValVplsBgpPwBindLocalVEId);

        free(pu4RetValVplsBgpPwBindLocalVEId);

        return u4RetVal;
}

INT1
nmhGetVplsBgpPwBindRemoteVEIdUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT4 *pu4RetValVplsBgpPwBindRemoteVEId = (UINT4 *)malloc(sizeof(UINT4));
        UINT1  u4RetVal = SNMP_FAILURE;
        
        u4RetVal = nmhGetVplsBgpPwBindRemoteVEId(u4VplsConfigIndex, u4VplsBgpVEId, pu4RetValVplsBgpPwBindRemoteVEId);

        free(pu4RetValVplsBgpPwBindRemoteVEId);        

        return u4RetVal;
}

INT1
nmhGetFirstIndexVplsBgpPwBindTableUT (VOID)
{
        UINT4 *pu4VplsConfigIndex = (UINT4 *)malloc(sizeof(UINT4));
        UINT4 *pu4NextVplsBgpVEId = (UINT4 *)malloc(sizeof(UINT4));
        UINT1  u4RetVal = SNMP_FAILURE;

        u4RetVal = nmhGetFirstIndexVplsBgpPwBindTable(pu4VplsConfigIndex, pu4NextVplsBgpVEId);
        nmhGetNextIndexVplsBgpPwBindTable(*pu4VplsConfigIndex, 
                                          pu4VplsConfigIndex, 
                                          *pu4NextVplsBgpVEId,
                                          pu4NextVplsBgpVEId);

        free(pu4VplsConfigIndex);
        free(pu4NextVplsBgpVEId);

        return u4RetVal;
}        

INT1
nmhGetNextIndexVplsBgpVETableUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT4 *pu4NextVplsConfigIndex = (UINT4 *)malloc(sizeof(UINT4));
        UINT4 *pu4NextVplsBgpVEId = (UINT4 *)malloc(sizeof(UINT4));
        UINT1  u4RetVal = SNMP_FAILURE;
        
        u4RetVal = nmhGetNextIndexVplsBgpVETable(u4VplsConfigIndex, pu4NextVplsConfigIndex, u4VplsBgpVEId, pu4NextVplsBgpVEId);

        free(pu4NextVplsConfigIndex);
        free(pu4NextVplsBgpVEId);

        return u4RetVal;
}

INT1
nmhGetFirstIndexVplsBgpVETableUT(INT4 iset)
{
        UINT4 *pu4VplsConfigIndex = (UINT4 *)malloc(sizeof(UINT4));
        UINT4 *pu4VplsBgpVEId = (UINT4 *)malloc(sizeof(UINT4));
        UINT1  u4RetVal = SNMP_FAILURE;

        u4RetVal = nmhGetFirstIndexVplsBgpVETable (pu4VplsConfigIndex, pu4VplsBgpVEId);

        if (1 == iset)
        {
                if (1 == *pu4VplsConfigIndex && 1 == *pu4VplsBgpVEId)
                        u4RetVal = SNMP_SUCCESS;
                else
                        u4RetVal = SNMP_FAILURE;
        }

        
        free(pu4VplsConfigIndex);
        free(pu4VplsBgpVEId);

        return u4RetVal;
}


INT1
nmhGetVplsBgpVEStorageTypeUT (INT4 iset)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        INT4 *pi4RetValVplsBgpVEStorageType =(INT4 *)malloc(sizeof(INT4));

        u4RetVal = nmhGetVplsBgpVEStorageType(u4VplsConfigIndex, u4VplsBgpVEId, pi4RetValVplsBgpVEStorageType);

        if (1 == iset)
        {
                if (4 == *pi4RetValVplsBgpVEStorageType)
                        u4RetVal = SNMP_SUCCESS;
                else
                        u4RetVal = SNMP_FAILURE;
        }                


        free(pi4RetValVplsBgpVEStorageType);

        return u4RetVal;
}

INT1
nmhGetVplsBgpVENameUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        tSNMP_OCTET_STRING_TYPE OctetStr;
        static UINT1        u1Val = 0;

        MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        OctetStr.pu1_OctetList = &u1Val;
        OctetStr.i4_Length = 0;

        u4RetVal = nmhGetVplsBgpVEName(u4VplsConfigIndex, u4VplsBgpVEId, &OctetStr);

        return u4RetVal;
}

INT1
nmhGetVplsBgpVEPreferenceUT (INT4 iset)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        UINT4 *pu4RetValVplsBgpVEPreference = (UINT4 *)malloc(sizeof(UINT4)) ;

        u4RetVal = nmhGetVplsBgpVEPreference(u4VplsConfigIndex, u4VplsBgpVEId, pu4RetValVplsBgpVEPreference);

        if (1 == iset)
        {
                if (3 == *pu4RetValVplsBgpVEPreference)
                        u4RetVal = SNMP_SUCCESS;
                else
                        u4RetVal = SNMP_FAILURE;
        }


        free(pu4RetValVplsBgpVEPreference);

        return u4RetVal;
}

INT1 
nmhTestv2VplsBgpVEPreferenceUT (VOID)
{
        UINT4 ErrorCode = 2;
        UINT4 *pu4ErrorCode = &ErrorCode;
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        UINT4 u4TestValVplsBgpVEPreference = 4;

        u4RetVal = nmhTestv2VplsBgpVEPreference(pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, u4TestValVplsBgpVEPreference);

        return u4RetVal;
}

INT1
nmhTestv2VplsBgpVEStorageTypeUT(VOID)
{
        UINT4 ErrorCode = 2;
        UINT4 *pu4ErrorCode = &ErrorCode;
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        INT4 i4TestValVplsBgpVEStorageType = 4;

        i4TestValVplsBgpVEStorageType = 1;
        u4RetVal = nmhTestv2VplsBgpVEStorageType (pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, i4TestValVplsBgpVEStorageType);
        i4TestValVplsBgpVEStorageType = 2;
        u4RetVal = nmhTestv2VplsBgpVEStorageType (pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, i4TestValVplsBgpVEStorageType);
        i4TestValVplsBgpVEStorageType = 3;
        u4RetVal = nmhTestv2VplsBgpVEStorageType (pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, i4TestValVplsBgpVEStorageType);
        i4TestValVplsBgpVEStorageType = 4;
        u4RetVal = nmhTestv2VplsBgpVEStorageType (pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, i4TestValVplsBgpVEStorageType);
        i4TestValVplsBgpVEStorageType = 5;
        u4RetVal = nmhTestv2VplsBgpVEStorageType (pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, i4TestValVplsBgpVEStorageType);

        return u4RetVal;
}

INT1
nmhTestv2VplsBgpVERowStatusUT (INT4 i4SetValVplsBgpVERowStatus)
{
        UINT4 ErrorCode = 2;
        UINT4 *pu4ErrorCode = &ErrorCode;
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        INT4 i4TestValVplsBgpVERowStatus = i4SetValVplsBgpVERowStatus;

        u4RetVal = nmhTestv2VplsBgpVERowStatus (pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, i4TestValVplsBgpVERowStatus);

        return u4RetVal;
}

INT1
nmhTestv2VplsBgpVENameUT (VOID)
{
        UINT4 ErrorCode = 2;
        UINT4 *pu4ErrorCode = &ErrorCode;
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        
        tSNMP_OCTET_STRING_TYPE OctetStr;
        static UINT1        u1Val = 0;

        MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        OctetStr.pu1_OctetList = &u1Val;
        OctetStr.i4_Length = 0;

        u4RetVal = nmhTestv2VplsBgpVEName(pu4ErrorCode, u4VplsConfigIndex, u4VplsBgpVEId, &OctetStr );

        return u4RetVal;
}

INT1
nmhSetVplsBgpVEStorageTypeUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        INT4 i4SetValVplsBgpVEStorageType = 4;

        u4RetVal = nmhSetVplsBgpVEStorageType(u4VplsConfigIndex, u4VplsBgpVEId, i4SetValVplsBgpVEStorageType);

        return u4RetVal;
}


INT1
nmhSetVplsBgpVEPreferenceUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;
        UINT4 u4SetValVplsBgpVEPreference = 3;

        u4RetVal = nmhSetVplsBgpVEPreference(u4VplsConfigIndex, u4VplsBgpVEId, u4SetValVplsBgpVEPreference);

        return u4RetVal;
}

INT1
nmhSetVplsBgpVENameUT (VOID)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;

        tSNMP_OCTET_STRING_TYPE OctetStr;
        static UINT1        u1Val = 0;

        MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        OctetStr.pu1_OctetList = &u1Val;
        OctetStr.i4_Length = 0;
        
        u4RetVal = nmhSetVplsBgpVEName(u4VplsConfigIndex, u4VplsBgpVEId, &OctetStr);

        return u4RetVal;
}

INT1
nmhSetVplsBgpVERowStatusUT(INT4 i4SetValVplsBgpVERowStatus)
{
        UINT4 u4VplsConfigIndex = 31;
        UINT4 u4VplsBgpVEId = 2;
        UINT1  u4RetVal = SNMP_FAILURE;

        u4RetVal = nmhSetVplsBgpVERowStatus(u4VplsConfigIndex, u4VplsBgpVEId, i4SetValVplsBgpVERowStatus);

        return u4RetVal;
}        

#endif
