/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs6374db.h,v 1.2 2017/07/25 12:00:36 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FS6374DB_H
#define _FS6374DB_H

UINT1 Fs6374ServiceConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fs6374ParamsConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fs6374LMTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs6374DMTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs6374LMStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs6374DMStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs6374StatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fs6374SystemConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs6374NotificationsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fs6374 [] ={1,3,6,1,4,1,29601,2,107};
tSNMP_OID_TYPE fs6374OID = {9, fs6374};


UINT4 Fs6374ContextId [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,1};
UINT4 Fs6374ServiceName [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,2};
UINT4 Fs6374MplsPathType [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,3};
UINT4 Fs6374FwdTnlIdOrPwId [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,4};
UINT4 Fs6374RevTnlId [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,5};
UINT4 Fs6374SrcIpAddr [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,6};
UINT4 Fs6374DestIpAddr [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,7};
UINT4 Fs6374EncapType [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,8};
UINT4 Fs6374TrafficClass [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,9};
UINT4 Fs6374RowStatus [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,10};
UINT4 Fs6374DyadicMeasurement [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,11};
UINT4 Fs6374TSFNegotiation [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,12};
UINT4 Fs6374TSNegotiatedFormat [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,13};
UINT4 Fs6374DyadicProactiveRole [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,14};
UINT4 Fs6374QueryTransmitRetryCount [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,15};
UINT4 Fs6374SessionIntervalQueryStatus [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,16};
UINT4 Fs6374MinReceptionIntervalInMilliseconds [ ] ={1,3,6,1,4,1,29601,2,107,1,1,1,17};
UINT4 Fs6374LMType [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,1};
UINT4 Fs6374LMMethod [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,2};
UINT4 Fs6374LMMode [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,3};
UINT4 Fs6374LMNoOfMessages [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,4};
UINT4 Fs6374LMTimeIntervalInMilliseconds [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,5};
UINT4 Fs6374LMTimeStampFormat [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,6};
UINT4 Fs6374LMTransmitStatus [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,7};
UINT4 Fs6374DMType [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,8};
UINT4 Fs6374DMMode [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,9};
UINT4 Fs6374DMTimeIntervalInMilliseconds [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,10};
UINT4 Fs6374DMNoOfMessages [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,11};
UINT4 Fs6374DMTimeStampFormat [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,12};
UINT4 Fs6374DMTransmitStatus [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,13};
UINT4 Fs6374CmbLMDMType [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,14};
UINT4 Fs6374CmbLMDMMethod [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,15};
UINT4 Fs6374CmbLMDMMode [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,16};
UINT4 Fs6374CmbLMDMNoOfMessages [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,17};
UINT4 Fs6374CmbLMDMTimeIntervalInMilliseconds [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,18};
UINT4 Fs6374CmbLMDMTimeStampFormat [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,19};
UINT4 Fs6374CmbLMDMTransmitStatus [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,20};
UINT4 Fs6374DMPaddingSize [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,21};
UINT4 Fs6374CmbLMDMPaddingSize [ ] ={1,3,6,1,4,1,29601,2,107,2,1,1,22};
UINT4 Fs6374LMSessionId [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,1};
UINT4 Fs6374LMSeqNum [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,2};
UINT4 Fs6374LMTxLossAtSenderEnd [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,3};
UINT4 Fs6374LMRxLossAtSenderEnd [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,4};
UINT4 Fs6374LMRxLossAtReceiverEnd [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,5};
UINT4 Fs6374LMMeasurementTimeTakenInMilliseconds [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,6};
UINT4 Fs6374LMTransmitResultOK [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,7};
UINT4 Fs6374LMTestPktsCount [ ] ={1,3,6,1,4,1,29601,2,107,3,1,1,8};
UINT4 Fs6374DMSessionId [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,1};
UINT4 Fs6374DMSeqNum [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,2};
UINT4 Fs6374DMDelayValue [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,3};
UINT4 Fs6374DMRTDelayValue [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,4};
UINT4 Fs6374DMIPDV [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,5};
UINT4 Fs6374DMPDV [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,6};
UINT4 Fs6374DMTransmitResultOK [ ] ={1,3,6,1,4,1,29601,2,107,4,1,1,7};
UINT4 Fs6374LMStatsMplsType [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,1};
UINT4 Fs6374LMStatsFwdTnlIdOrPwId [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,2};
UINT4 Fs6374LMStatsRevTnlId [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,3};
UINT4 Fs6374LMStatsSrcIpAddr [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,4};
UINT4 Fs6374LMStatsDestIpAddr [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,5};
UINT4 Fs6374LMStatsTrafficClass [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,6};
UINT4 Fs6374LMStatsMode [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,7};
UINT4 Fs6374LMStatsMethod [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,8};
UINT4 Fs6374LMStatsType [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,9};
UINT4 Fs6374LMStatsTimeStampFormat [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,10};
UINT4 Fs6374LMStatsStartTime [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,11};
UINT4 Fs6374LMStatsFarEndLossMin [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,12};
UINT4 Fs6374LMStatsFarEndLossMax [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,13};
UINT4 Fs6374LMStatsFarEndLossAvg [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,14};
UINT4 Fs6374LMStatsNearEndLossMin [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,15};
UINT4 Fs6374LMStatsNearEndLossMax [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,16};
UINT4 Fs6374LMStatsNearEndLossAvg [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,17};
UINT4 Fs6374LMStatsTxPktCount [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,18};
UINT4 Fs6374LMStatsRxPktCount [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,19};
UINT4 Fs6374LMStatsThroughputpercent [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,20};
UINT4 Fs6374LMStatsResponseErrors [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,21};
UINT4 Fs6374LMStatsResponseTimeout [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,22};
UINT4 Fs6374LMStatsMeasurementOnGoing [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,23};
UINT4 Fs6374LMDyadicMeasurement [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,24};
UINT4 Fs6374LMRemoteSessionId [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,25};
UINT4 Fs6374LMDyadicProactiveRole [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,26};
UINT4 Fs6374IsLMBufferSQIEnabled [ ] ={1,3,6,1,4,1,29601,2,107,5,1,1,27};
UINT4 Fs6374DMStatsMplsType [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,1};
UINT4 Fs6374DMStatsFwdTnlIdOrPwId [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,2};
UINT4 Fs6374DMStatsRevTnlId [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,3};
UINT4 Fs6374DMStatsSrcIpAddr [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,4};
UINT4 Fs6374DMStatsDestIpAddr [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,5};
UINT4 Fs6374DMStatsTrafficClass [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,6};
UINT4 Fs6374DMStatsMode [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,7};
UINT4 Fs6374DMStatsType [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,8};
UINT4 Fs6374DMStatsTimeStampFormat [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,9};
UINT4 Fs6374DMStatsStartTime [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,10};
UINT4 Fs6374DMStatsMinDelay [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,11};
UINT4 Fs6374DMStatsMaxDelay [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,12};
UINT4 Fs6374DMStatsAvgDelay [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,13};
UINT4 Fs6374DMStatsMinRTDelay [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,14};
UINT4 Fs6374DMStatsMaxRTDelay [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,15};
UINT4 Fs6374DMStatsAvgRTDelay [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,16};
UINT4 Fs6374DMStatsMinIPDV [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,17};
UINT4 Fs6374DMStatsMaxIPDV [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,18};
UINT4 Fs6374DMStatsAvgIPDV [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,19};
UINT4 Fs6374DMStatsMinPDV [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,20};
UINT4 Fs6374DMStatsMaxPDV [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,21};
UINT4 Fs6374DMStatsAvgPDV [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,22};
UINT4 Fs6374DMStatsTxPktCount [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,23};
UINT4 Fs6374DMStatsRxPktCount [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,24};
UINT4 Fs6374DMStatsResponseErrors [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,25};
UINT4 Fs6374DMStatsResponseTimeout [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,26};
UINT4 Fs6374DMStatsMeasurementOnGoing [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,27};
UINT4 Fs6374DMDyadicMeasurement [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,28};
UINT4 Fs6374DMRemoteSessionId [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,29};
UINT4 Fs6374DMDyadicProactiveRole [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,30};
UINT4 Fs6374DMStatsPaddingSize [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,31};
UINT4 Fs6374IsDMBufferSQIEnabled [ ] ={1,3,6,1,4,1,29601,2,107,6,1,1,32};
UINT4 Fs6374StatsLmmOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,1};
UINT4 Fs6374StatsLmmIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,2};
UINT4 Fs6374StatsLmrOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,3};
UINT4 Fs6374StatsLmrIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,4};
UINT4 Fs6374Stats1LmOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,5};
UINT4 Fs6374Stats1LmIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,6};
UINT4 Fs6374Stats1DmOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,7};
UINT4 Fs6374Stats1DmIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,8};
UINT4 Fs6374StatsDmmOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,9};
UINT4 Fs6374StatsDmmIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,10};
UINT4 Fs6374StatsDmrOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,11};
UINT4 Fs6374StatsDmrIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,12};
UINT4 Fs6374StatsCmb1LmDmOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,13};
UINT4 Fs6374StatsCmb1LmDmIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,14};
UINT4 Fs6374StatsCmbLmmDmmOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,15};
UINT4 Fs6374StatsCmbLmmDmmIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,16};
UINT4 Fs6374StatsCmbLmrDmrOut [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,17};
UINT4 Fs6374StatsCmbLmrDmrIn [ ] ={1,3,6,1,4,1,29601,2,107,7,1,1,18};
UINT4 Fs6374SystemControl [ ] ={1,3,6,1,4,1,29601,2,107,8,1,1,1};
UINT4 Fs6374ModuleStatus [ ] ={1,3,6,1,4,1,29601,2,107,8,1,1,2};
UINT4 Fs6374DMBufferClear [ ] ={1,3,6,1,4,1,29601,2,107,8,1,1,3};
UINT4 Fs6374LMBufferClear [ ] ={1,3,6,1,4,1,29601,2,107,8,1,1,4};
UINT4 Fs6374DebugLevel [ ] ={1,3,6,1,4,1,29601,2,107,8,1,1,5};
UINT4 Fs6374TSFormatSupported [ ] ={1,3,6,1,4,1,29601,2,107,8,1,1,6};
UINT4 Fs6374LMDMTrapControl [ ] ={1,3,6,1,4,1,29601,2,107,9,1,1,1};




tMbDbEntry fs6374MibEntry[]= {

{{13,Fs6374ContextId}, GetNextIndexFs6374ServiceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374ServiceName}, GetNextIndexFs6374ServiceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374MplsPathType}, GetNextIndexFs6374ServiceConfigTable, Fs6374MplsPathTypeGet, Fs6374MplsPathTypeSet, Fs6374MplsPathTypeTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374FwdTnlIdOrPwId}, GetNextIndexFs6374ServiceConfigTable, Fs6374FwdTnlIdOrPwIdGet, Fs6374FwdTnlIdOrPwIdSet, Fs6374FwdTnlIdOrPwIdTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374RevTnlId}, GetNextIndexFs6374ServiceConfigTable, Fs6374RevTnlIdGet, Fs6374RevTnlIdSet, Fs6374RevTnlIdTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374SrcIpAddr}, GetNextIndexFs6374ServiceConfigTable, Fs6374SrcIpAddrGet, Fs6374SrcIpAddrSet, Fs6374SrcIpAddrTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374DestIpAddr}, GetNextIndexFs6374ServiceConfigTable, Fs6374DestIpAddrGet, Fs6374DestIpAddrSet, Fs6374DestIpAddrTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374EncapType}, GetNextIndexFs6374ServiceConfigTable, Fs6374EncapTypeGet, Fs6374EncapTypeSet, Fs6374EncapTypeTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "1"},

{{13,Fs6374TrafficClass}, GetNextIndexFs6374ServiceConfigTable, Fs6374TrafficClassGet, Fs6374TrafficClassSet, Fs6374TrafficClassTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "0"},

{{13,Fs6374RowStatus}, GetNextIndexFs6374ServiceConfigTable, Fs6374RowStatusGet, Fs6374RowStatusSet, Fs6374RowStatusTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 1, NULL},

{{13,Fs6374DyadicMeasurement}, GetNextIndexFs6374ServiceConfigTable, Fs6374DyadicMeasurementGet, Fs6374DyadicMeasurementSet, Fs6374DyadicMeasurementTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374TSFNegotiation}, GetNextIndexFs6374ServiceConfigTable, Fs6374TSFNegotiationGet, Fs6374TSFNegotiationSet, Fs6374TSFNegotiationTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374TSNegotiatedFormat}, GetNextIndexFs6374ServiceConfigTable, Fs6374TSNegotiatedFormatGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374ServiceConfigTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374DyadicProactiveRole}, GetNextIndexFs6374ServiceConfigTable, Fs6374DyadicProactiveRoleGet, Fs6374DyadicProactiveRoleSet, Fs6374DyadicProactiveRoleTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374QueryTransmitRetryCount}, GetNextIndexFs6374ServiceConfigTable, Fs6374QueryTransmitRetryCountGet, Fs6374QueryTransmitRetryCountSet, Fs6374QueryTransmitRetryCountTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "25"},

{{13,Fs6374SessionIntervalQueryStatus}, GetNextIndexFs6374ServiceConfigTable, Fs6374SessionIntervalQueryStatusGet, Fs6374SessionIntervalQueryStatusSet, Fs6374SessionIntervalQueryStatusTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "1"},

{{13,Fs6374MinReceptionIntervalInMilliseconds}, GetNextIndexFs6374ServiceConfigTable, Fs6374MinReceptionIntervalInMillisecondsGet, Fs6374MinReceptionIntervalInMillisecondsSet, Fs6374MinReceptionIntervalInMillisecondsTest, Fs6374ServiceConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ServiceConfigTableINDEX, 2, 0, 0, "100"},

{{13,Fs6374LMType}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMTypeGet, Fs6374LMTypeSet, Fs6374LMTypeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374LMMethod}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMMethodGet, Fs6374LMMethodSet, Fs6374LMMethodTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374LMMode}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMModeGet, Fs6374LMModeSet, Fs6374LMModeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "1"},

{{13,Fs6374LMNoOfMessages}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMNoOfMessagesGet, Fs6374LMNoOfMessagesSet, Fs6374LMNoOfMessagesTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "10"},

{{13,Fs6374LMTimeIntervalInMilliseconds}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMTimeIntervalInMillisecondsGet, Fs6374LMTimeIntervalInMillisecondsSet, Fs6374LMTimeIntervalInMillisecondsTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "1000"},

{{13,Fs6374LMTimeStampFormat}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMTimeStampFormatGet, Fs6374LMTimeStampFormatSet, Fs6374LMTimeStampFormatTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "3"},

{{13,Fs6374LMTransmitStatus}, GetNextIndexFs6374ParamsConfigTable, Fs6374LMTransmitStatusGet, Fs6374LMTransmitStatusSet, Fs6374LMTransmitStatusTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "3"},

{{13,Fs6374DMType}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMTypeGet, Fs6374DMTypeSet, Fs6374DMTypeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374DMMode}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMModeGet, Fs6374DMModeSet, Fs6374DMModeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "1"},

{{13,Fs6374DMTimeIntervalInMilliseconds}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMTimeIntervalInMillisecondsGet, Fs6374DMTimeIntervalInMillisecondsSet, Fs6374DMTimeIntervalInMillisecondsTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "1000"},

{{13,Fs6374DMNoOfMessages}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMNoOfMessagesGet, Fs6374DMNoOfMessagesSet, Fs6374DMNoOfMessagesTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "10"},

{{13,Fs6374DMTimeStampFormat}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMTimeStampFormatGet, Fs6374DMTimeStampFormatSet, Fs6374DMTimeStampFormatTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "3"},

{{13,Fs6374DMTransmitStatus}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMTransmitStatusGet, Fs6374DMTransmitStatusSet, Fs6374DMTransmitStatusTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "3"},

{{13,Fs6374CmbLMDMType}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMTypeGet, Fs6374CmbLMDMTypeSet, Fs6374CmbLMDMTypeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374CmbLMDMMethod}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMMethodGet, Fs6374CmbLMDMMethodSet, Fs6374CmbLMDMMethodTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "2"},

{{13,Fs6374CmbLMDMMode}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMModeGet, Fs6374CmbLMDMModeSet, Fs6374CmbLMDMModeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "1"},

{{13,Fs6374CmbLMDMNoOfMessages}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMNoOfMessagesGet, Fs6374CmbLMDMNoOfMessagesSet, Fs6374CmbLMDMNoOfMessagesTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "10"},

{{13,Fs6374CmbLMDMTimeIntervalInMilliseconds}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMTimeIntervalInMillisecondsGet, Fs6374CmbLMDMTimeIntervalInMillisecondsSet, Fs6374CmbLMDMTimeIntervalInMillisecondsTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "1000"},

{{13,Fs6374CmbLMDMTimeStampFormat}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMTimeStampFormatGet, Fs6374CmbLMDMTimeStampFormatSet, Fs6374CmbLMDMTimeStampFormatTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "3"},

{{13,Fs6374CmbLMDMTransmitStatus}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMTransmitStatusGet, Fs6374CmbLMDMTransmitStatusSet, Fs6374CmbLMDMTransmitStatusTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "3"},

{{13,Fs6374DMPaddingSize}, GetNextIndexFs6374ParamsConfigTable, Fs6374DMPaddingSizeGet, Fs6374DMPaddingSizeSet, Fs6374DMPaddingSizeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "0"},

{{13,Fs6374CmbLMDMPaddingSize}, GetNextIndexFs6374ParamsConfigTable, Fs6374CmbLMDMPaddingSizeGet, Fs6374CmbLMDMPaddingSizeSet, Fs6374CmbLMDMPaddingSizeTest, Fs6374ParamsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374ParamsConfigTableINDEX, 2, 0, 0, "0"},

{{13,Fs6374LMSessionId}, GetNextIndexFs6374LMTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374LMSeqNum}, GetNextIndexFs6374LMTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374LMTxLossAtSenderEnd}, GetNextIndexFs6374LMTable, Fs6374LMTxLossAtSenderEndGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374LMRxLossAtSenderEnd}, GetNextIndexFs6374LMTable, Fs6374LMRxLossAtSenderEndGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374LMRxLossAtReceiverEnd}, GetNextIndexFs6374LMTable, Fs6374LMRxLossAtReceiverEndGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374LMMeasurementTimeTakenInMilliseconds}, GetNextIndexFs6374LMTable, Fs6374LMMeasurementTimeTakenInMillisecondsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374LMTransmitResultOK}, GetNextIndexFs6374LMTable, Fs6374LMTransmitResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374LMTableINDEX, 4, 0, 0, "1"},

{{13,Fs6374LMTestPktsCount}, GetNextIndexFs6374LMTable, Fs6374LMTestPktsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMSessionId}, GetNextIndexFs6374DMTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs6374DMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMSeqNum}, GetNextIndexFs6374DMTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs6374DMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMDelayValue}, GetNextIndexFs6374DMTable, Fs6374DMDelayValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMRTDelayValue}, GetNextIndexFs6374DMTable, Fs6374DMRTDelayValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMIPDV}, GetNextIndexFs6374DMTable, Fs6374DMIPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMPDV}, GetNextIndexFs6374DMTable, Fs6374DMPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMTableINDEX, 4, 0, 0, NULL},

{{13,Fs6374DMTransmitResultOK}, GetNextIndexFs6374DMTable, Fs6374DMTransmitResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374DMTableINDEX, 4, 0, 0, "1"},

{{13,Fs6374LMStatsMplsType}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsMplsTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsFwdTnlIdOrPwId}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsFwdTnlIdOrPwIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsRevTnlId}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsRevTnlIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsSrcIpAddr}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsSrcIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsDestIpAddr}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsDestIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsTrafficClass}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsTrafficClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsMode}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsMethod}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsType}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsTimeStampFormat}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsTimeStampFormatGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsStartTime}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsFarEndLossMin}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsFarEndLossMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsFarEndLossMax}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsFarEndLossMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsFarEndLossAvg}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsFarEndLossAvgGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsNearEndLossMin}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsNearEndLossMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsNearEndLossMax}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsNearEndLossMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsNearEndLossAvg}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsNearEndLossAvgGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsTxPktCount}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsTxPktCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsRxPktCount}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsRxPktCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsThroughputpercent}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsThroughputpercentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMStatsResponseErrors}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsResponseErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, "0"},

{{13,Fs6374LMStatsResponseTimeout}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsResponseTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, "0"},

{{13,Fs6374LMStatsMeasurementOnGoing}, GetNextIndexFs6374LMStatsTable, Fs6374LMStatsMeasurementOnGoingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, "0"},

{{13,Fs6374LMDyadicMeasurement}, GetNextIndexFs6374LMStatsTable, Fs6374LMDyadicMeasurementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, "2"},

{{13,Fs6374LMRemoteSessionId}, GetNextIndexFs6374LMStatsTable, Fs6374LMRemoteSessionIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374LMDyadicProactiveRole}, GetNextIndexFs6374LMStatsTable, Fs6374LMDyadicProactiveRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374IsLMBufferSQIEnabled}, GetNextIndexFs6374LMStatsTable, Fs6374IsLMBufferSQIEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374LMStatsTableINDEX, 3, 0, 0, "2"},

{{13,Fs6374DMStatsMplsType}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMplsTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsFwdTnlIdOrPwId}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsFwdTnlIdOrPwIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsRevTnlId}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsRevTnlIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsSrcIpAddr}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsSrcIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsDestIpAddr}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsDestIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsTrafficClass}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsTrafficClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMode}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsType}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsTimeStampFormat}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsTimeStampFormatGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsStartTime}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMinDelay}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMinDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMaxDelay}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMaxDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsAvgDelay}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsAvgDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMinRTDelay}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMinRTDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMaxRTDelay}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMaxRTDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsAvgRTDelay}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsAvgRTDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMinIPDV}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMinIPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMaxIPDV}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMaxIPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsAvgIPDV}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsAvgIPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMinPDV}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMinPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsMaxPDV}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMaxPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsAvgPDV}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsAvgPDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsTxPktCount}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsTxPktCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsRxPktCount}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsRxPktCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsResponseErrors}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsResponseErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, "0"},

{{13,Fs6374DMStatsResponseTimeout}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsResponseTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, "0"},

{{13,Fs6374DMStatsMeasurementOnGoing}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsMeasurementOnGoingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, "0"},

{{13,Fs6374DMDyadicMeasurement}, GetNextIndexFs6374DMStatsTable, Fs6374DMDyadicMeasurementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, "2"},

{{13,Fs6374DMRemoteSessionId}, GetNextIndexFs6374DMStatsTable, Fs6374DMRemoteSessionIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMDyadicProactiveRole}, GetNextIndexFs6374DMStatsTable, Fs6374DMDyadicProactiveRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374DMStatsPaddingSize}, GetNextIndexFs6374DMStatsTable, Fs6374DMStatsPaddingSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs6374IsDMBufferSQIEnabled}, GetNextIndexFs6374DMStatsTable, Fs6374IsDMBufferSQIEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374DMStatsTableINDEX, 3, 0, 0, "2"},

{{13,Fs6374StatsLmmOut}, GetNextIndexFs6374StatsTable, Fs6374StatsLmmOutGet, Fs6374StatsLmmOutSet, Fs6374StatsLmmOutTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsLmmIn}, GetNextIndexFs6374StatsTable, Fs6374StatsLmmInGet, Fs6374StatsLmmInSet, Fs6374StatsLmmInTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsLmrOut}, GetNextIndexFs6374StatsTable, Fs6374StatsLmrOutGet, Fs6374StatsLmrOutSet, Fs6374StatsLmrOutTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsLmrIn}, GetNextIndexFs6374StatsTable, Fs6374StatsLmrInGet, Fs6374StatsLmrInSet, Fs6374StatsLmrInTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374Stats1LmOut}, GetNextIndexFs6374StatsTable, Fs6374Stats1LmOutGet, Fs6374Stats1LmOutSet, Fs6374Stats1LmOutTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374Stats1LmIn}, GetNextIndexFs6374StatsTable, Fs6374Stats1LmInGet, Fs6374Stats1LmInSet, Fs6374Stats1LmInTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374Stats1DmOut}, GetNextIndexFs6374StatsTable, Fs6374Stats1DmOutGet, Fs6374Stats1DmOutSet, Fs6374Stats1DmOutTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374Stats1DmIn}, GetNextIndexFs6374StatsTable, Fs6374Stats1DmInGet, Fs6374Stats1DmInSet, Fs6374Stats1DmInTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsDmmOut}, GetNextIndexFs6374StatsTable, Fs6374StatsDmmOutGet, Fs6374StatsDmmOutSet, Fs6374StatsDmmOutTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsDmmIn}, GetNextIndexFs6374StatsTable, Fs6374StatsDmmInGet, Fs6374StatsDmmInSet, Fs6374StatsDmmInTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsDmrOut}, GetNextIndexFs6374StatsTable, Fs6374StatsDmrOutGet, Fs6374StatsDmrOutSet, Fs6374StatsDmrOutTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsDmrIn}, GetNextIndexFs6374StatsTable, Fs6374StatsDmrInGet, Fs6374StatsDmrInSet, Fs6374StatsDmrInTest, Fs6374StatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsCmb1LmDmOut}, GetNextIndexFs6374StatsTable, Fs6374StatsCmb1LmDmOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsCmb1LmDmIn}, GetNextIndexFs6374StatsTable, Fs6374StatsCmb1LmDmInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsCmbLmmDmmOut}, GetNextIndexFs6374StatsTable, Fs6374StatsCmbLmmDmmOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsCmbLmmDmmIn}, GetNextIndexFs6374StatsTable, Fs6374StatsCmbLmmDmmInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsCmbLmrDmrOut}, GetNextIndexFs6374StatsTable, Fs6374StatsCmbLmrDmrOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374StatsCmbLmrDmrIn}, GetNextIndexFs6374StatsTable, Fs6374StatsCmbLmrDmrInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs6374StatsTableINDEX, 2, 0, 0, NULL},

{{13,Fs6374SystemControl}, GetNextIndexFs6374SystemConfigTable, Fs6374SystemControlGet, Fs6374SystemControlSet, Fs6374SystemControlTest, Fs6374SystemConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374SystemConfigTableINDEX, 1, 0, 0, "1"},

{{13,Fs6374ModuleStatus}, GetNextIndexFs6374SystemConfigTable, Fs6374ModuleStatusGet, Fs6374ModuleStatusSet, Fs6374ModuleStatusTest, Fs6374SystemConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374SystemConfigTableINDEX, 1, 0, 0, "1"},

{{13,Fs6374DMBufferClear}, GetNextIndexFs6374SystemConfigTable, Fs6374DMBufferClearGet, Fs6374DMBufferClearSet, Fs6374DMBufferClearTest, Fs6374SystemConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374SystemConfigTableINDEX, 1, 0, 0, NULL},

{{13,Fs6374LMBufferClear}, GetNextIndexFs6374SystemConfigTable, Fs6374LMBufferClearGet, Fs6374LMBufferClearSet, Fs6374LMBufferClearTest, Fs6374SystemConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs6374SystemConfigTableINDEX, 1, 0, 0, NULL},

{{13,Fs6374DebugLevel}, GetNextIndexFs6374SystemConfigTable, Fs6374DebugLevelGet, Fs6374DebugLevelSet, Fs6374DebugLevelTest, Fs6374SystemConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs6374SystemConfigTableINDEX, 1, 0, 0, "3"},

{{13,Fs6374TSFormatSupported}, GetNextIndexFs6374SystemConfigTable, Fs6374TSFormatSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs6374SystemConfigTableINDEX, 1, 0, 0, NULL},

{{13,Fs6374LMDMTrapControl}, GetNextIndexFs6374NotificationsTable, Fs6374LMDMTrapControlGet, Fs6374LMDMTrapControlSet, Fs6374LMDMTrapControlTest, Fs6374NotificationsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fs6374NotificationsTableINDEX, 1, 0, 0, NULL},
};
tMibData fs6374Entry = { 138, fs6374MibEntry };

#endif /* _FS6374DB_H */

