/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: r6374tmr.h,v 1.1 2016/07/06 10:51:55 siva Exp $
*
* Description: This file contains definitions for r6374 Timer
 *******************************************************************/

#ifndef __R6374TMR_H__
#define __R6374TMR_H__

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC
typedef struct _RFC6374_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tR6374TmrDesc;


#endif  /* __R6374TMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  r6374tmr.h                      */
/*-----------------------------------------------------------------------*/
