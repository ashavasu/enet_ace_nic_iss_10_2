/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * 
 * $Id: r6374prot.h,v 1.2 2017/07/25 12:00:36 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of r6374 module.
 *******************************************************************/

#ifndef __R6374PROT_H__
#define __R6374PROT_H__ 

/* Add Prototypes here */

/**************************r6374main.c*******************************/
PUBLIC VOID R6374TaskMain           PROTO ((VOID));

PUBLIC VOID R6374TaskDeInit         PROTO ((VOID));
PUBLIC UINT4 R6374TaskInit          PROTO ((VOID));

PUBLIC INT4 R6374TaskLock           PROTO ((VOID));
PUBLIC INT4 R6374TaskUnLock         PROTO ((VOID));

PUBLIC INT4 R6374VcmRegister        PROTO ((VOID));
PUBLIC VOID R6374VcmDeRegister      PROTO ((VOID));

PUBLIC INT4 R6374CreateAllTables    PROTO ((VOID));
PUBLIC VOID R6374DestroyAllTables   PROTO ((UINT4 u4ContextId));

PUBLIC UINT4 R6374MemInit           PROTO ((VOID));
PUBLIC VOID R6374MemClear           PROTO ((VOID));
/**************************r6374que.c********************************/
PUBLIC VOID R6374QueProcessMsgs        PROTO ((VOID));
PUBLIC INT4 R6374QueEnqMsg             PROTO ((tR6374QMsg *pQMsg));
/**************************r6374tmr.c********************************/
PUBLIC VOID  R6374TmrInitTmrDesc      PROTO(( VOID));

PUBLIC INT4  R6374TmrInit             PROTO(( VOID));
PUBLIC VOID  R6374TmrDeInit           PROTO(( VOID));

PUBLIC INT4  R6374TmrStartTmr         PROTO ((UINT1 , tR6374ServiceConfigTableEntry *, UINT4));
PUBLIC INT4  R6374TmrStopTmr          PROTO ((UINT1 , tR6374ServiceConfigTableEntry *));

PUBLIC VOID  R6374TmrHandleExpiry     PROTO((VOID));

PUBLIC VOID  R6374TmrLMQueryTmrExp    PROTO ((VOID *pArg));

PUBLIC VOID  R6374TmrDMQueryTmrExp    PROTO ((VOID *pArg));

PUBLIC VOID  R6374TmrLMDMQueryTmrExp    PROTO ((VOID *pArg));
/**************************r6374trc.c*******************************/
PUBLIC CHR1  *R6374Trc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
PUBLIC VOID   R6374TrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
PUBLIC VOID   R6374TrcWrite      PROTO(( CHR1 *s));
PUBLIC VOID   R6374TrcDump       PROTO ((UINT1 *pu1Buf, UINT4 i4Len));
/************************* r6374port.c *************/
PUBLIC INT4 R6374PortHandleExtInteraction PROTO ((tR6374ExtInParams *, tR6374ExtOutParams *));
PUBLIC INT4 R6374MplsHandleExtInteraction PROTO ((UINT4 , tMplsApiInInfo * , tMplsApiOutInfo * ));
PUBLIC VOID R6374CfaGetSysMacAddress      PROTO ((tR6374MacAddr SwitchMac));
PUBLIC VOID R6374GetSysSuppTimeFormat PROTO ((UINT1  * u1SysSuppTimeFormat));
/************************* r6374bfd.c *************/
PUBLIC INT4 R6374GetBfdPktCount     PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo, UINT4 *pu4Tx, UINT4 *pu4Rx));
PUBLIC INT4 R6374GetBfdTrafficClass PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo, UINT4 *pu4TraffciClass));
PUBLIC INT4 R6374GetBfdSessIndex    PROTO ((tR6374ServiceConfigTableEntry *, tR6374ExtInParams *, tR6374ExtOutParams *, UINT4 , UINT4 *));
#ifdef RFC6374STUB_WANTED
PUBLIC INT4 R6374StubGetBfdCounter PROTO ((UINT4 *, UINT4 *));
#endif

/************************* r6374mpls.c *************/
PUBLIC INT4 R6374MplsRegCallBack            PROTO ((VOID));
PUBLIC INT4 R6374RegMplsPath                PROTO ((BOOL1 bRFC6374RegMplsPath, tR6374ServiceConfigTableEntry * pServiceConfig));

/*PUBLIC INT4 R6374MplsGetBaseServiceOID      PROTO ((UINT4 u4ContextId, UINT1 u1PathType, tServicePtr * pServicePtr));
PUBLIC INT4 R6374MplsValidateServiceOID     PROTO ((UINT4 u4ContextId, UINT1 u1PathType, tServicePtr * pServicePtr));
PUBLIC INT4 R6374MplsGetOidFromPathId       PROTO ((UINT4 u4ContextId, tR6374MplsParams * pMplsPathId, tServicePtr * pServicePtr));
PUBLIC INT4 R6374MplsGetPwIndex             PROTO ((UINT4 u4PswId, UINT4 *pu4PwIndex));*/

PUBLIC INT4 R6374MplsTxPkt                  PROTO ((tR6374BufChainHeader * pBuf, tR6374PktSmInfo *pPduSmInfo, UINT1 ));
PUBLIC INT4 R6374GetMplsPathInfo            PROTO ((UINT4 u4ContextId, tR6374MplsParams * pR6374MplsPathParams, UINT1 u1Mode, 
                                                    tR6374MplsPathInfo * pR6374MplsPathInfo));
PUBLIC INT4 R6374MplsConstructPacket        PROTO ((tR6374ProcInfo * pMplsProcInfo, tR6374MplsPathInfo * pMplsPathInfo, UINT4 *pu4IfIndex, UINT1 u1ServiceType, UINT1 u1Method, UINT1 *pu1DestMac, tCRU_BUF_CHAIN_HEADER * pBuf));
PUBLIC INT4 R6374SendPktToMpls              PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DestMac, tCRU_BUF_CHAIN_HEADER * pBuf));
PUBLIC INT4 R6374MplsProcessPdu             PROTO ((tR6374BufChainHeader * pBuf, tR6374MplsParams * pMplsParams, UINT2 u2ChnType));
PUBLIC INT4 R6374RxProcessMplsHeaders       PROTO ((tR6374BufChainHeader * pBuf, UINT4 u4IfIndex, UINT4 u4PktLen, tR6374MplsParams * pMplsPathId));
PUBLIC VOID R6374RxGetMplsHdr               PROTO ((tR6374BufChainHeader * pBuf, tMplsHdr * pHdr));
PUBLIC UINT1 R6374MplsGetPduType            PROTO ((tR6374BufChainHeader * pBuf, UINT2 u2ChnType));
PUBLIC UINT4 R6374RxDeMux                   PROTO ((tR6374PktSmInfo PduSmInfo, UINT1 *));
PUBLIC INT4 R6374ProcessMplsPdu             PROTO ((tR6374BufChainHeader * pBuf, UINT4 u4IfIndex));
PUBLIC INT4 R6374GetMplsPktCnt              PROTO ((tR6374ServiceConfigTableEntry  *, UINT4 *, UINT4 *, UINT4 *, UINT4 *));

/************************* r6374utl.c *************/
PUBLIC INT4 R6374CreateContext              PROTO ((UINT4 u4ContextId));
PUBLIC INT4 R6374DeleteContext              PROTO ((UINT4 u4ContextId));

PUBLIC INT4 R6374ServiceConfigTableCreate   PROTO ((VOID));
PUBLIC INT4 R6374LmBufferTableCreate        PROTO ((VOID));
PUBLIC INT4 R6374LmStatsTableCreate         PROTO ((VOID));
PUBLIC INT4 R6374DmBufferTableCreate        PROTO ((VOID));
PUBLIC INT4 R6374DmStatsTableCreate         PROTO ((VOID));

PUBLIC INT4 R6374LmBufferTableRBCmp         PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC INT4 R6374LmStatsTableRBCmp          PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC INT4 R6374DmStatsTableRBCmp          PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC INT4 R6374DmBufferTableRBCmp         PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC INT4 R6374ServiceConfigTableRBCmp    PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

PUBLIC VOID R6374SnmpLwGetDmTransactionStats PROTO ((tR6374DmBufferTableEntry *, 
           tR6374DmStatsTableEntry *));

PUBLIC VOID R6374UtilDeleteSerConfigTableInContext PROTO ((UINT4 u4ContextId));
PUBLIC VOID R6374UtilDeleteDmBufTableInContext PROTO ((UINT4 u4ContextId));
PUBLIC VOID R6374UtilDeleteLmBufTableInContext PROTO ((UINT4 u4ContextId));
PUBLIC VOID R6374UtilDeleteLmStatsTableInContext PROTO ((UINT4 u4ContextId));
PUBLIC VOID R6374UtilDeleteDmStatsTableInContext PROTO ((UINT4 u4ContextId));

PUBLIC VOID R6374UtilGetCurrentTime         PROTO ((tR6374TSRepresentation *pTimeStamp, UINT1 u1TSFormat));
PUBLIC INT4 R6374UtilCalTimeDiff            PROTO ((tR6374TSRepresentation * pTimeOp1, tR6374TSRepresentation * pTimeOp2, tR6374TSRepresentation * pTimeResult));

PUBLIC tR6374LmStatsTableEntry * R6374UtilGetLmStatsTableInfo               PROTO ((tR6374ServiceConfigTableEntry *, UINT4 ));
PUBLIC tR6374DmStatsTableEntry * R6374UtilGetDmStatsTableInfo               PROTO ((tR6374ServiceConfigTableEntry *, UINT4 ));

PUBLIC tR6374ServiceConfigTableEntry * R6374UtlCreateService                PROTO ((UINT4 , UINT1 *));
PUBLIC tR6374ServiceConfigTableEntry * R6374UtilGetServiceFromName          PROTO ((UINT4 u4ContextId, UINT1 * pu1ServiceName));

PUBLIC INT4 R6374UtilGetIpEntriesForService     PROTO((tR6374ServiceConfigTableEntry *pServiceInfo, UINT4 pTestValFs6374IpAddr, UINT4 u4PwId, UINT4 *));
PUBLIC INT4 R6374UtilGetTunnelEntriesForService  PROTO((tR6374ServiceConfigTableEntry *, UINT4, UINT4, UINT4, UINT4 *));
PUBLIC tR6374ServiceConfigTableEntry * R6374UtilGetServiceFromMplsParams    PROTO ((tR6374MplsParams *));

PUBLIC INT4 R6374UtilPostTransaction                                        PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo, UINT1 u1TransType));
PUBLIC UINT4 R6374UtilGetTimeFromEpoch      PROTO ((VOID));
PUBLIC UINT4 R6374UtlStartSystemControl     PROTO ((VOID));
PUBLIC VOID R6374UtlShutSystemControl       PROTO ((UINT4 u4ContextId));
PUBLIC UINT4 R6374UtlEnableModuleStatus     PROTO ((VOID));    
PUBLIC VOID R6374UtlDisableModuleStatus     PROTO ((VOID));

PUBLIC INT4 R6374UtlFreeEntryFn             PROTO ((tRBElem * pElem, UINT4 u4Arg));
PUBLIC VOID R6374UtlSecondsToDate           PROTO ((UINT4 u4sec, UINT1 *pu1DateFormat));

PUBLIC INT4 R6374UtilRmEntriesFromDmStatsForService     PROTO((UINT4 u4ContextId, UINT1 * pu1ServiceName));
PUBLIC INT4 R6374UtilRmEntriesFromLmStatsForService     PROTO((UINT4 u4ContextId, UINT1 * pu1ServiceName));
PUBLIC INT4 R6374UtilRmEntriesFromDmBufferForService    PROTO((UINT4 u4ContextId, UINT1 * pu1ServiceName));
PUBLIC INT4 R6374UtilRmEntriesFromLmBufferForService    PROTO((UINT4 u4ContextId, UINT1 * pu1ServiceName));
PUBLIC INT4 R6374UtlIsDMMeasOngoing                     PROTO((UINT4 u4ContextId));
PUBLIC INT4 R6374UtlIsLMMeasOngoing                     PROTO((UINT4 u4ContextId));
PUBLIC INT4 R6374UtilProcessPathStatus                  PROTO ((tR6374QMsg *));
PUBLIC VOID R6374UtilGetSessId                          PROTO((tR6374LmDmInfoTableEntry  *pLmDmInfo, 
                                                                tR6374LmInfoTableEntry  *pLmInfo,
                                                                tR6374DmInfoTableEntry  *pDmInfo,
                                                                UINT4 u4SessionId, UINT4 u4SessType));

PUBLIC UINT4 R6374UtilValidateTestStatus     PROTO ((tR6374ServiceConfigTableEntry *pServiceInfoEntry,
                                                    INT4 i4TestStatus, UINT1 u1TestType));
PUBLIC UINT4 R6374UtilValidateMplsPathParams PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo, BOOL1 *b1IfAlreadyRegister));

PUBLIC UINT4 R6374UtilTestStatus PROTO ((tR6374ServiceConfigTableEntry *pServiceInfoEntry, 
            INT4 i4TestStatus, UINT1 u1TestType, 
     UINT1 u1TestMode));
PUBLIC UINT4 R6374UtilGetSessionStats PROTO ((tR6374ReqParams * pR6374ReqParams,
                                  tR6374RespParams * pR6374RespParams));

PUBLIC UINT1
R6374UtilAdjustSessionQueryInterval PROTO ((tR6374ServiceConfigTableEntry*,
            UINT4 u4SessQueryInt, UINT1 u1TestMode));

PUBLIC UINT1
R6374UtilCheckInterQueryInterval PROTO ((tR6374ServiceConfigTableEntry*,
       UINT4 u4LmOriginTimeStampSeconds, UINT4 u4LmOriginTimeStampNanoSeconds, UINT1 u1TestMode));

PUBLIC UINT1
R6374UtilIncrementSessionId PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo,
            UINT4 u4SessType));

VOID R6374UtilCalculatePower PROTO ((UINT4 u4Base , UINT4 u4Expo , UINT4 *pu4Result));
PUBLIC UINT1
R6374UtilCleanUpStatsBuffer PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo,
            UINT4 u4SessType));

/************************* r6374lmini.c *************/
PUBLIC UINT4 R6374LmInitiator                   PROTO ((tR6374PktSmInfo * pPduSmInfo, UINT1 u1EventID));
PUBLIC INT4 R6374LmInitXmitLmmPdu               PROTO ((tR6374PktSmInfo * pPduSmInfo));
PUBLIC VOID R6374LmInitFormatLmmPduHdr          PROTO ((tR6374ServiceConfigTableEntry  * pServiceInfo, UINT1 **ppu1LmmPdu));
PUBLIC VOID R6374LmInitStopLmTransaction        PROTO ((tR6374ServiceConfigTableEntry *));
PUBLIC INT4 R6374LmInitXmit1LmPdu               PROTO ((tR6374PktSmInfo * pPduSmInfo));
PUBLIC VOID R6374LmInitFormat1LmPduHdr          PROTO ((tR6374LmInfoTableEntry *pLmInfo, UINT1 **ppu1Lm1Pdu));
PUBLIC INT4 R6374LmResetVar PROTO ((tR6374LmInfoTableEntry *pLmInfo));
/************************* r6374lminr.c *************/
PUBLIC INT4 R6374ProcessLmr                            PROTO((tR6374PktSmInfo * pPktSmInfo, UINT1 * pLmrpdu));

PUBLIC UINT4 R6374CalcLossMeas                         PROTO ((tR6374PktSmInfo * pPduSmInfo, tR6374LmBufferTableEntry * pLossLmBuffNode, UINT1 u1LossMeasurementType));

PUBLIC tR6374LmBufferTableEntry *R6374LmInitAddLmEntry PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo, UINT4, UINT1));
PUBLIC UINT4 R6374LmAddStats                           PROTO ((tR6374PktSmInfo *, tR6374LmBufferTableEntry *, UINT1));
PUBLIC UINT4 R6374DmAddStats                           PROTO ((tR6374PktSmInfo *, tR6374DmBufferTableEntry *, UINT1));

PUBLIC UINT4 R6374CalcFrameLoss PROTO ((tR6374PktSmInfo * pPktSmInfo, tR6374TSRepresentation   TotalDurationTxRx,
                              UINT1 u1MeasurementType, UINT4 u4SessionId, UINT4 u4SeqCount));

/************************* r6374lmresp.c *************/
PUBLIC INT4 R6374ProcessLmm                            PROTO ((tR6374PktSmInfo * pPduSmInfo, UINT1 *pu1Pdu));
PUBLIC UINT4 R6374LmResXmitLmrPdu                      PROTO ((tR6374PktSmInfo * pPduSmInfo, UINT1   *pu1RevPdu));
PUBLIC INT4 R6374DmResXmitDmrPdu                      PROTO ((tR6374PktSmInfo *pPduSmInfo, UINT1 *pu1RevPdu));
PUBLIC VOID R6374LmInitFormatLmrPduHdr                 PROTO ((tR6374LmInfoTableEntry *pLmInfo, UINT1 *pu1RevPdu, UINT1 **ppu1LmrPdu));
PUBLIC VOID R6374DmInitFormatDmrPduHdr                 PROTO ((tR6374DmInfoTableEntry *pDmInfo, UINT1 *pu1RevPdu, UINT1 **ppu1DmrPdu));
PUBLIC INT4 R6374LmDyadicMeasurement                   PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));
/************************* r6374dmini.c *************/
PUBLIC UINT4 R6374DmInitiator                          PROTO ((tR6374PktSmInfo * pPduSmInfo, UINT1 u1EventID));
PUBLIC tR6374DmBufferTableEntry *R6374DmInitAddDmEntry PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo, UINT4, UINT1));
PUBLIC VOID R6374DmInitStopDmTransaction               PROTO ((tR6374ServiceConfigTableEntry *));
PUBLIC VOID R6374DmTxPutPadTlv PROTO ((UINT1 **ppu1PduEnd, UINT4 u4PadSize));

/************************* r6374dminr.c *************/
PUBLIC INT4 R6374ProcessDmr                            PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));
PUBLIC INT4 R6374Process1Dm                         PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));
PUBLIC INT4 R6374Process1LmDm                         PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));
PUBLIC UINT4
R6374CalcFrameDelay     PROTO ((tR6374PktSmInfo * pPktSmInfo, tR6374RxDmrPktInfo *pDmrPktInfo,
                               UINT1 u1MeasurementType));
PUBLIC INT4 R63741DmCalcDiffInTimeRep PROTO ((tR6374TSRepresentation *,
                                                 tR6374TSRepresentation *,
                                                 tR6374TSRepresentation *));
PUBLIC VOID R6374DmCalcFrameDelayVariation PROTO ((tR6374ServiceConfigTableEntry *,
                                                   tR6374DmBufferTableEntry *));


PUBLIC UINT4
R6374Calc1FrameDelay PROTO ((tR6374PktSmInfo * pPktSmInfo, tR6374Rx1DmPktInfo
                             *p1DmPktInfo, UINT1 u1MeasurementType, UINT4 u4SessionId));

PUBLIC VOID
R6374DmRxPutPadTlv PROTO ((UINT1 **ppu1PduEnd, UINT1 *pu1RevPdu, UINT2 u2RxPduLen));

/*PUBLIC INT4 R6374ParseDmr                              PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));*/
/************************* r6374dmres.c *************/
/*PUBLIC INT4 R6374ParseDmm                              PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pkt));*/
PUBLIC INT4 R6374ProcessDmm                            PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pkt));
PUBLIC INT4 R6374DmDyadicMeasurement                   PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));
/************************* r6374api.c *************/
PUBLIC VOID R6374ApiVcmCallback     PROTO ((UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1Event));
PUBLIC INT4 R6374ApiMplsCallBack    PROTO ((UINT4 u4ModId, VOID *pMplsEventNotif));
/************************* r6374trap.c *************/
tSNMP_OID_TYPE     *
R6374MakeObjIdFrmString PROTO ((INT1 *pi1TextStr, UINT1 *pu1TableName));
PUBLIC VOID R6374SendNotification PROTO ((tR6374NotifyInfo * pNotifyInfo));
PUBLIC INT4 R6374IsTrapEnabled PROTO ((UINT1 u1Trap));

/************************** r6374cmbini.c **********/
PUBLIC UINT4
R6374LmDmInitiator PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 u1EventID));
PUBLIC INT4
R6374LmDmInitXmitLmmDmmPdu PROTO ((tR6374PktSmInfo * pPktSmInfo));
PUBLIC VOID
R6374LmDmInitFormatLmmDmmPduHdr PROTO ((tR6374ServiceConfigTableEntry *pServiceInfo,
                              UINT1 **ppu1Pdu));
PUBLIC VOID
R6374LmDmInitStopLmDmTransaction PROTO ((tR6374ServiceConfigTableEntry * pServiceInfo));

PUBLIC INT4
R6374LmDmInitXmit1LmDmPdu PROTO ((tR6374PktSmInfo * pPktSmInfo));

PUBLIC VOID
R6374LmDmInitFormat1LmDmPduHdr PROTO ((tR6374LmInfoTableEntry *pLmInfo, UINT1 **ppu1Pdu));

PUBLIC VOID
R6374UpdateLmDmInfo PROTO ((tR6374ServiceConfigTableEntry * pServiceInfo));

/************************** r6374cmbres.c **********/
PUBLIC INT4
R6374ProcessLmmDmm PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));

PUBLIC UINT4
R6374LmDmResXmitLmrDmrPdu PROTO ((tR6374PktSmInfo * pPktSmInfo,
                             UINT1  *pu1RevPdu));

PUBLIC VOID
R6374LmDmInitFormatLmrDmrPduHdr PROTO ((tR6374LmDmInfoTableEntry *pLmDmInfo,
                                  UINT1 *pu1RevPdu, UINT1 **ppu1Pdu));

PUBLIC VOID
R6374LmDmInitPutLmrDmrPduInfo PROTO ((tR6374PktSmInfo * pPktSmInfo, 
          UINT1 **ppu1Pdu, UINT4 u4Txb));

PUBLIC INT4
R6374CombLmDmDyadicMeasurement PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu));

PUBLIC INT4 
R6374CombLmDmResetVar PROTO ((tR6374LmDmInfoTableEntry *pLmDmInfo));

/************************** r6374cmbinr.c **********/
PUBLIC INT4
R6374ProcessLmrDmr PROTO ((tR6374PktSmInfo * pPktSmInfo, UINT1 *pdu));


#endif   /* __R6374PROT_H__ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  r6374prot.h                     */
/*-----------------------------------------------------------------------*/
