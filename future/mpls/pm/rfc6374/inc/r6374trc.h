/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374trc.h,v 1.3 2017/07/25 12:00:36 siva Exp $
 *
 * Description:   This file contains procedures and definitions
 *                used for debugging in rfc6374 module. 
 ********************************************************************/

#ifndef __R6374TRC_H__
#define __R6374TRC_H__


#define  RFC6374_MODULE_TRACE     \
                  gR6374GlobalInfo.u4TraceLevel

#ifdef TRACE_WANTED

#define RFC6374_MODULE_NAME        ((const char *)"[RFC6374]")

#define   RFC6374_TRC(Flag, CxtId, Fmt ) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt)

#define   RFC6374_TRC1(Flag, CxtId, Fmt, Arg) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt, Arg)

#define   RFC6374_TRC2(Flag, CxtId, Fmt, Arg1, Arg2) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt, Arg1, Arg2)

#define   RFC6374_TRC3(Flag, CxtId, Fmt, Arg1, Arg2, Arg3) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt, Arg1, Arg2, Arg3)

#define   RFC6374_TRC4(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4)

#define   RFC6374_TRC5(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)

#define   RFC6374_TRC6(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
          UtlTrcLog(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define   RFC6374_PKT_DUMP(Flag, CxtId, pBuf, Length, Fmt)\
          MOD_PKT_DUMP(RFC6374_MODULE_TRACE, Flag, RFC6374_MODULE_NAME, pBuf, Length, Fmt)

#define   RFC6374_GLOBAL_TRC(Fmt) \
         UtlTrcLog (RFC6374_CRITICAL_TRC, RFC6374_CRITICAL_TRC, RFC6374_MODULE_NAME, Fmt)

#else

#define   RFC6374_TRC(Flag, CxtId, Fmt) 
#define   RFC6374_TRC1(Flag, CxtId, Fmt, Arg)
#define   RFC6374_TRC2(Flag, CxtId, Fmt, Arg1, Arg2)
#define   RFC6374_TRC3(Flag, CxtId, Fmt, Arg1, Arg2, Arg3)
#define   RFC6374_TRC4(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)
#define   RFC6374_TRC5(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define   RFC6374_TRC6(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define   RFC6374_PKT_DUMP(Flag, CxtId, pBuf, Length, Fmt)\
{\
    UNUSED_PARAM(Length);\
}

#define  RFC6374_GLOBAL_TRC(Fmt) do{ }while(0);
#endif /*TRACE_WANTED*/

#endif /*__R6374TRC_H__*/
