/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374inc.h,v 1.1 2016/07/06 10:51:54 siva Exp $
 *
 * Description: This file contains the super set  of all header files
 *              rfc6374  module.
 *******************************************************************/
#ifndef __R6374INC_H__
#define __R6374INC_H__

/* Common ISS files */
#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"
#include "iss.h"
#include "cli.h"
#include "fm.h"
#include "fsvlan.h"
#include "vcm.h"
#include "npapi.h"
#include "params.h"

#include "dbutil.h"
#include "fssyslog.h"


/* BFD & MPLS files */
#include "bfd.h"
#include "mplsutl.h"

/* RFC6374 common files */
#include "r6374.h"
#include "r6374cli.h"

/* RFC6374 files */
#include "r6374defn.h"
#include "r6374macs.h"
#include "r6374tdfs.h"
#include "r6374tmr.h"
#include "r6374trc.h"
#include "r6374sz.h"
#include "r6374prot.h"
#include "fs6374lw.h"
#include "fs6374wr.h"

#ifdef __R6374MAIN_C__
#include "r6374glob.h"
#else
#include "r6374extn.h"
#endif

#endif /*__R6374INC_H__*/


