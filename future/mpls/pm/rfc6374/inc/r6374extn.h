
/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374extn.h,v 1.2 2017/07/25 12:00:36 siva Exp $
 *
 * Description: This file contains global variables and global
 *              structures used in  rfc6374  module.
 *******************************************************************/

#ifndef __R6374EXTN_H__
#define __R6374EXTN_H__
extern tR6374GlobalInfo    gR6374GlobalInfo;

extern UINT1 gau1R6374SystemStatus[SYS_DEF_MAX_NUM_CONTEXTS];
extern UINT1 gau1RFC6374Pdu [RFC6374_PDU_SIZE];

#endif /* end of __R6374EXTN__ */

