/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374macs.h,v 1.3 2017/07/25 12:00:36 siva Exp $
 *
 * Description: This file contains the macro definition for the RFC6374
 *              Module.
 *********************************************************************/
#ifndef __R6374MACS_H__
#define __R6374MACS_H__
/****************************************************************************/
/* Macro to get context information */
#define RFC6374_GET_CONTEXT_INFO(u4ContextId) gR6374GlobalInfo.apContextInfo[u4ContextId]
#define RFC6374_SELECT_CONTEXT(u4ContextId) RFC6374SelectContext (u4ContextId)
#define RFC6374_RELEASE_CONTEXT() RFC6374ReleaseContext()
#define RFC6374_CURR_CONTEXT_ID() (RFC6374_CURR_CONTEXT_INFO())->u4ContextId
/****************************************************************************/
/* Macro for storing Syslog ID */
#define RFC6374_SYSLOG_ID                gR6374GlobalInfo.i4SysLogId

/****************************************************************************/
/* Macro for PDU  */
#define RFC6374_PDU      gR6374GlobalInfo.pu1Pdu

/****************************************************************************/
/* Macro for storing task related */

#define RFC6374_TASK_ID()                gR6374GlobalInfo.TaskId
#define RFC6374_SEM_ID()                 gR6374GlobalInfo.SemId
#define RFC6374_QUEUE_ID()               gR6374GlobalInfo.MsgQId

#define RFC6374_QMSG_POOL()              gR6374GlobalInfo.MsgQPoolId

#define RFC6374_TIMER_LIST_ID()           gR6374GlobalInfo.TimerListId

#define RFC6374_CONTEXT_NAME(u4ContextId) \
                       gR6374GlobalInfo.aContextInfo[u4ContextId].au1ContextName
/****************************************************************************/
/* Macros for RBtree*/
#define RFC6374_SERVICECONFIG_TABLE         gR6374GlobalInfo.ServiceConfigTable
#define RFC6374_DMBUFFER_TABLE              gR6374GlobalInfo.DmBufferTable
#define RFC6374_DMSTATS_TABLE               gR6374GlobalInfo.DmStatsTable
#define RFC6374_LMBUFFER_TABLE              gR6374GlobalInfo.LmBufferTable
#define RFC6374_LMSTATS_TABLE               gR6374GlobalInfo.LmStatsTable
#define RFC6374_NOTIFICATIONS_TABLE         gR6374GlobalInfo.NotificationsTable
/****************************************************************************/
/* Definitions of Library functions used*/
#define RFC6374_MEMCPY              MEMCPY
#define RFC6374_MEMCMP              MEMCMP
#define RFC6374_MEMSET              MEMSET
#define RFC6374_STRCMP              STRCMP
#define RFC6374_STRCPY              STRCPY
#define RFC6374_STRNCPY             STRNCPY
#define RFC6374_STRTOK              STRTOK
#define RFC6374_STRLEN              STRLEN
#define RFC6374_MALLOC              MEM_MALLOC
#define RFC6374_MEM_FREE            MEM_FREE
#define RFC6374_HTONS               OSIX_HTONS
#define RFC6374_NTOHS               OSIX_NTOHS
#define RFC6374_HTONL               OSIX_HTONL
#define RFC6374_NTOHL               OSIX_NTOHL
/****************************************************************************/
/*Macros used to assign values to buffer */
#define RFC6374_PUT_1BYTE(pu1PktBuf,u1Val)\
    do{\
        *pu1PktBuf = u1Val;\
        pu1PktBuf += 1;\
    }while(0)

#define RFC6374_PUT_2BYTE(pu1PktBuf, u2Val)\
    do{\
        UINT2 u2Value = RFC6374_HTONS(u2Val);\
        MEMCPY (pu1PktBuf, &u2Value, sizeof (UINT2));\
        pu1PktBuf += sizeof (UINT2);\
    }while(0)

#define RFC6374_PUT_4BYTE(pu1PktBuf, u4Val)\
    do{\
        UINT4 u4Value = RFC6374_HTONL(u4Val);\
        MEMCPY (pu1PktBuf, &u4Value, sizeof (UINT4));\
        pu1PktBuf += sizeof (UINT4);\
    }while(0)

#define RFC6374_PUT_NBYTE(pu1PktBuf, pu1Src, u4Len)\
    do{\
        MEMCPY ((UINT1 *) pu1PktBuf, (UINT1 *) pu1Src, u4Len);\
        pu1PktBuf += u4Len;\
    }while(0)

/* Macros used to extract values from buffer */
#define RFC6374_GET_1BYTE(u1Val, pu1Buf)\
    do{\
        u1Val = *pu1Buf;\
        pu1Buf += 1;\
    }while(0)

#define RFC6374_GET_2BYTE(u2Val, pu1Buf)\
    do{\
        MEMCPY (&u2Val, pu1Buf, 2);\
        pu1Buf += 2;\
        u2Val = (UINT2) (RFC6374_NTOHS(u2Val));\
    }while(0)

#define RFC6374_GET_4BYTE(u4Val, pu1Buf)\
    do{\
        MEMCPY (&u4Val, pu1Buf, 4);\
        pu1Buf += 4;\
        u4Val = (UINT4) (RFC6374_NTOHL(u4Val));\
    }while(0)

#define RFC6374_GET_NBYTE(pu1Dest, pu1Buf, u4Len)\
    do{\
        MEMCPY ((UINT1 *) pu1Dest, (UINT1 *) pu1Buf, u4Len);\
        pu1Buf += u4Len;\
    }while(0)


#define RFC6374_DUMP_PKT(u4ContextId, pu1Buf, u4Len)\
{\
    if (gR6374GlobalInfo.u4TraceLevel & RFC6374_PKT_DUMP_TRC)\
    {\
            R6374TrcDump(pu1Buf, u4Len);\
    }\
}

#define RFC6374_OCTETSTRING_TO_INTEGER(OctetString,u4Index) { \
    u4Index = 0;\
    if(OctetString.i4_Length > 0 && OctetString.i4_Length <= \
            RFC6374_IPV4_ADDR_LEN)\
    {\
        MEMCPY((UINT1 *)&u4Index,OctetString.pu1_OctetList,\
                OctetString.i4_Length);\
        u4Index = OSIX_NTOHL(u4Index);\
    }\
}
#define RFC6374_IPADDR_TO_STR(pString, u4Value)\
{\
    tUtlInAddr          IpAddr;\
\
         IpAddr.u4Addr = (u4Value);\
\
         pString = (CHR1 *)CLI_INET_NTOA (IpAddr);\
\
}

/* Octect String Conversion Macors */
#define RFC6374_OCTETSTRING_TO_FLOAT(pOctetString, f4Val) \
        SSCANF( (CHR1 *)pOctetString->pu1_OctetList, "%f", (DBL8)&f4Val)

#define RFC6374_FLOAT_TO_OCTETSTRING(f4Value,pOctetString) \
        SPRINTF( (CHR1 *)pOctetString->pu1_OctetList, "%f", (DBL8)f4Value)

/****************************************************************************/
/* CRU related Macros */
#define RFC6374_ALLOC_CRU_BUF                    CRU_BUF_Allocate_MsgBufChain
#define RFC6374_RELEASE_CRU_BUF                  CRU_BUF_Release_MsgBufChain
#define RFC6374_COPY_OVER_CRU_BUF                CRU_BUF_Copy_OverBufChain
#define RFC6374_COPY_FROM_CRU_BUF                CRU_BUF_Copy_FromBufChain
#define RFC6374_GET_CRU_VALID_BYTE_COUNT         CRU_BUF_Get_ChainValidByteCount
#define RFC6374_CRU_BUF_MOVE_VALID_OFFSET        CRU_BUF_Move_ValidOffset
#define RFC6374_GET_DATA_PTR_IF_LINEAR           CRU_BUF_Get_DataPtr_IfLinear
#define RFC6374_DUPLICATE_CRU_BUF                CRU_BUF_Duplicate_BufChain
#define RFC6374_PREPEND_CRU_BUF                  CRU_BUF_Prepend_BufChain
#define RFC6374_DEL_CRU_BUF_AT_END               CRU_BUF_Delete_BufChainAtEnd
#define RFC6374_FRAGMENT_CRU_BUF                 CRU_BUF_Fragment_BufChain
#define RFC6374_CONCAT_CRU_BUF                   CRU_BUF_Concat_MsgBufChains
#define RFC6374_CRU_SUCCESS                      CRU_SUCCESS   
#define RFC6374_CRU_FAILURE                      CRU_FAILURE

#define RFC6374_CRU_GET_1_BYTE(pMsg, u4Offset, u1Value)\
       CRU_BUF_Copy_FromBufChain(pMsg,((UINT1 *) &u1Value), u4Offset, 1)

#define RFC6374_CRU_GET_2_BYTE(pMsg, u4Offset, u2Value) \
    do{\
       CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
       u2Value = RFC6374_NTOHS (u2Value);\
    }while(0)
#define RFC6374_CRU_GET_4_BYTE(pMsg, u4Offset, u4Value) \
    do{\
       CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
       u4Value = RFC6374_NTOHL (u4Value);\
    }while(0)

#define RFC6374_CRU_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,\
                                    u4_StringLength) \
do{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1_StringMemArea, u4Offset,\
                                       u4_StringLength);\
}while(0)

#define RFC6374_BUF_SET_PRIORITY(pBuf,u1Priority)\
do \
   {\
    UINT4 u4Byte=0; \
    u4Byte = pBuf->ModuleData.u4Reserved3; \
    u4Byte = u4Byte&0xffffff00; \
    u4Byte = u4Byte|u1Priority; \
    pBuf->ModuleData.u4Reserved2 = u4Byte;\
    }\
while(0)
/* Macro to Set the OpCode in the Reserved byte of Buffer Module data
* Last 1 byte of the Reserved field is utilized for that*/
#define RFC6374_BUF_SET_OPCODE(pBuf,u1OpCode)\
do \
   {\
    UINT4 u4Byte=0; \
    u4Byte = pBuf->ModuleData.u4Reserved3; \
    u4Byte = u4Byte&0xffffff00; \
    u4Byte = u4Byte|u1OpCode; \
    pBuf->ModuleData.u4Reserved3 = u4Byte;\
    }\
while(0)
/****************************************************************************/
/* Macro for freeing memory block to the Memory Pools */
#define RFC6374_FREE_MEM_BLOCK(RFC6374PoolId, pu1Msg)\
    MemReleaseMemBlock (RFC6374PoolId, pu1Msg)

/* Macro for getting the number of free Units in the MemPool */
#define RFC6374_GET_FREE_MEM_UNITS(RFC6374PoolId) \
        MemGetFreeUnits(RFC6374PoolId)
/****************************************************************************/
/* Macros For Back Pointer */
#define RFC6374_GET_SERVICE_FROM_PDUSM(pPktSmInfo)\
        (pPktSmInfo->pServiceConfigInfo)

#define RFC6374_GET_DM_INFO_FROM_PDUSM(pPktSmInfo)\
        (&pPktSmInfo->pServiceConfigInfo->DmInfo)

#define RFC6374_GET_LM_INFO_FROM_SERVICE(pServiceInfo)\
        (&pServiceInfo->LmInfo)

#define RFC6374_GET_LM_INFO_FROM_PDUSM(pPktSmInfo)\
        (&pPktSmInfo->pServiceConfigInfo->LmInfo)

#define RFC6374_GET_DM_INFO_FROM_SERVICE(pServiceInfo)\
        (&pServiceInfo->DmInfo)

#define RFC6374_GET_LMDM_INFO_FROM_SERVICE(pServiceInfo)\
        (&pServiceInfo->LmDmInfo)

#define RFC6374_GET_LMDM_INFO_FROM_PDUSM(pPktSmInfo)\
        (&pPktSmInfo->pServiceConfigInfo->LmDmInfo)

#define RFC6374_GET_SYS_TIME            OsixGetSysTime
/****************************************************************************/
/* Loss Measurement Related Macros */
#define RFC6374_LM_INCR_SESSION_ID(pLmInfo)\
        (pLmInfo->u4LmSessionId++)

#define RFC6374_LM_INCR_SEQ_COUNT(pLmInfo)\
        (pLmInfo->u4LmSeqCount++)

#define RFC6374_LM_INCR_RX_SEQ_COUNT(pLmInfo)\
        (pLmInfo->u4RxLmSeqCount++)

#define RFC6374_LM_INCR_BUFFER_ADD_COUNT(pLmInfo)\
        (pLmInfo->u1LmBufferAddCount++)

#define RFC6374_LM_INCR_ERR_PKT(pLmInfo)\
        (pLmInfo->u4RxLmPacketLoss++)

#define RFC6374_LM_RESET_SEQ_COUNT(pLmInfo)\
        (pLmInfo->u4LmSeqCount = RFC6374_RESET)

#define RFC6374_LM_RESET_ROSEQ_COUNT(pLmInfo)\
        (pLmInfo->u4LmResetSeqCount = RFC6374_RESET)

#define RFC6374_LM_RESET_RX_ROSEQ_COUNT(pLmInfo)\
        (pLmInfo->u4RxLmResetSeqCount = RFC6374_RESET)

#define RFC6374_LM_RESET_BUFFER_ADD_COUNT(pLmInfo)\
        (pLmInfo->u1LmBufferAddCount = RFC6374_RESET)

#define RFC6374_LM_DECR_RUNNING_COUNT(pLmInfo)\
        (pLmInfo->u2LmRunningCount--)

#define RFC6374_LM_RESET_PROACTIVE_TX_COUNT(pLmInfo)\
        (pLmInfo->u4LmNoOfProTxCount = RFC6374_RESET)

#define RFC6374_LM_INCR_PROACTIVE_TX_COUNT(pLmInfo)\
        (pLmInfo->u4LmNoOfProTxCount++)

#define RFC6374_LM_RESET_ORIGIN_TIME_STAMP(pLmInfo) \
        (RFC6374_MEMSET (&(pLmInfo->LmOriginTimeStamp), \
            RFC6374_RESET, sizeof(tR6374TSRepresentation)))

#define RFC6374_LM_SET_RUNNING_COUNT(pLmInfo)\
        (pLmInfo->u2LmRunningCount = (UINT2)(pLmInfo->u2LmNoOfMessages + 1))

#define RFC6374_LM_INCR_LM_NO_RESP_TX_COUNT(pLmInfo)\
        (pLmInfo->u4LmNoRespTxCount++)

#define RFC6374_LM_RESET_LM_NO_RESP_TX_COUNT(pLmInfo)\
        (pLmInfo->u4LmNoRespTxCount = RFC6374_RESET)

#define RFC6374_LM_RESET_RX_PKT_LOSS(pLmInfo)\
        (pLmInfo->u4RxLmPacketLoss = RFC6374_RESET)

#define RFC6374_LM_INCR_RESP_TIMEOUT_RETRIES_COUNT(pLmInfo)\
        (pLmInfo->u1LmRespTimeOutReTries++)

#define RFC6374_LM_RESET_RESP_TIMEOUT_RETRIES_COUNT(pLmInfo)\
        (pLmInfo->u1LmRespTimeOutReTries = RFC6374_RESET)

#define RFC6374_LM_RESET_PACKETLOSS(pLmInfo)\
        (pLmInfo->u4RxLmPacketLoss = RFC6374_RESET)

#define RFC6374_LM_RESET_RESP_TX_COUNT(pLmInfo)\
        (pLmInfo->u4LmNoRespTxCount = RFC6374_RESET)
/* Total Running LM */
#define RFC6374_LM_INCR_SESSION_COUNT()\
        (gR6374GlobalInfo.u1LmTotalSessionCount++)

#define RFC6374_LM_DECR_SESSION_COUNT() \
        (gR6374GlobalInfo.u1LmTotalSessionCount--)

#define RFC6374_LM_GET_CURRENT_SESSION_COUNT()\
        (gR6374GlobalInfo.u1LmTotalSessionCount)

/* Combined LMDM */
#define RFC6374_LMDM_INCR_SESSION_COUNT()\
        (gR6374GlobalInfo.u1LmDmTotalSessionCount++)

#define RFC6374_LMDM_GET_CURRENT_SESSION_COUNT()\
        (gR6374GlobalInfo.u1LmDmTotalSessionCount)

#define RFC6374_LMDM_INCR_SESSION_ID(pLmDmInfo)\
        (pLmDmInfo->u4LmDmSessionId++)

#define RFC6374_LMDM_RESET_SEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmDmSeqCount = RFC6374_RESET)

#define RFC6374_LMDM_RESET_BUFFER_ADD_COUNT(pLmDmInfo)\
        (pLmDmInfo->u1LmDmBufferAddCount = RFC6374_RESET)

#define RFC6374_LMDM_RESET_NO_RESP_TX_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmDmNoRespTxCount = RFC6374_RESET)

#define RFC6374_LMDM_SET_RUNNING_COUNT(pLmDmInfo)\
        (pLmDmInfo->u2LmDmRunningCount = (UINT2)(pLmDmInfo->u2LmDmNoOfMessages + 1))

#define RFC6374_LMDM_RESET_RX_PKT_LOSS(pLmDmInfo)\
        (pLmDmInfo->u4RxLmDmPacketLoss = RFC6374_RESET)

#define RFC6374_LMDM_DECR_RUNNING_COUNT(pLmDmInfo)\
        (pLmDmInfo->u2LmDmRunningCount--)

#define RFC6374_LMDM_RESET_PROACTIVE_TX_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmDmNoOfProTxCount = RFC6374_RESET)

#define RFC6374_LMDM_INCR_PROACTIVE_TX_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmDmNoOfProTxCount++)

#define RFC6374_LMDM_INCR_NO_RESP_TX_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmDmNoRespTxCount++)

#define RFC6374_LMDM_DECR_SESSION_COUNT() \
        (gR6374GlobalInfo.u1LmDmTotalSessionCount--)

#define RFC6374_LMDM_INCR_ERR_PKT(pLmDmInfo)\
        (pLmDmInfo->u4RxLmDmPacketLoss++)

#define RFC6374_LMDM_INCR_SEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmDmSeqCount++)

#define RFC6374_LMDM_INCR_RX_SEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4RxLmDmSeqCount++)

#define RFC6374_LMDM_RESET_ONEWAY_LM_ROSEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4RxLmResetSeqCount = RFC6374_RESET)

#define RFC6374_LMDM_RESET_ONEWAY_DM_ROSEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4RxDmResetSeqCount = RFC6374_RESET)

#define RFC6374_LMDM_RESET_TWOWAY_LM_ROSEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4LmResetSeqCount = RFC6374_RESET)

#define RFC6374_LMDM_RESET_TWOWAY_DM_ROSEQ_COUNT(pLmDmInfo)\
        (pLmDmInfo->u4DmResetSeqCount = RFC6374_RESET)

#define RFC6374_LMDM_INCR_RESP_TIMEOUT_RETRIES_COUNT(pLmDmInfo)\
        (pLmDmInfo->u1LmDmRespTimeOutReTries++)

#define RFC6374_LMDM_RESET_RESP_TIMEOUT_RETRIES_COUNT(pLmDmInfo)\
            (pLmDmInfo->u1LmDmRespTimeOutReTries = RFC6374_RESET)

#define RFC6374_LMDM_RESET_PACKETLOSS(pLmDmInfo)\
            (pLmDmInfo->u4RxLmDmPacketLoss = RFC6374_RESET)

#define RFC6374_LMDM_RESET_RESP_TX_COUNT(pLmDmInfo)\
            (pLmDmInfo->u4LmDmNoRespTxCount = RFC6374_RESET)

/* Macros to Loss Calculations */
#define RFC6374_CALCULATE_LM(u4FirstVal,u4SecVal,u4ThirdVal,u4FourthVal,u4Res) \
do \
{ \
      UINT4 u4Temp1 = 0; \
      UINT4 u4Temp2 = 0; \
      if ( u4FirstVal > u4SecVal) \
      { \
               u4Temp1 = u4FirstVal - u4SecVal;\
            }\
      else\
      {\
               u4Temp1 = u4SecVal - u4FirstVal;\
            }\
      if (u4ThirdVal > u4FourthVal)\
      {\
               u4Temp2 = u4ThirdVal - u4FourthVal;\
            }\
      else\
      {\
               u4Temp2 = u4FourthVal - u4ThirdVal;\
            }\
      if( u4Temp1 > u4Temp2)\
      {\
               u4Res = u4Temp1 - u4Temp2;\
            }\
      else\
      {\
               u4Res = RFC6374_INIT_VAL;\
            }\
}while(0)

#define RFC6374_CALCULATE_MIN_LM(u4FirstVal, u4SecVal) \
do \
{ \
    if (u4FirstVal > u4SecVal) \
    { \
        u4FirstVal = u4SecVal; \
    } \
}while(0)

#define RFC6374_CALCULATE_MAX_LM(u4FirstVal, u4SecVal) \
do \
{ \
    if (u4FirstVal < u4SecVal) \
    { \
        u4FirstVal = u4SecVal; \
    } \
}while(0)

#define RFC6374_CALCULATE_AVG_LM(u4FirstVal, u4SecVal) \
do \
{ \
    u4FirstVal = ((u4FirstVal + u4SecVal)/2); \
}while(0)

#define RFC6374_CALCULATE_THROUGH_PUT_LM(f4FirstVal, A_Tx, A_Tx_Pre, B_Rx, B_Rx_Pre, B_Tx, B_Tx_Pre, A_Rx, A_Rx_Pre) \
do \
{ \
    FLT4    f4ThroughPut = 0.0; \
    FLT4    f4Temp = 0.0; \
    UINT4   u4TotalTxCount = RFC6374_INIT_VAL; \
    UINT4   u4TotalRxCount = RFC6374_INIT_VAL; \
    u4TotalTxCount = (A_Tx - A_Tx_Pre) + (B_Tx - B_Tx_Pre);\
    u4TotalRxCount = (A_Rx - A_Rx_Pre) + (B_Rx - B_Rx_Pre);\
    if(u4TotalRxCount < u4TotalTxCount) \
    {\
        f4Temp = (FLT4)((FLT4) u4TotalRxCount / (FLT4) u4TotalTxCount);\
    }\
    else\
    {\
        /*If Rx > Tx, assume 100% throughput*/\
        f4Temp = 1;\
    }\
    f4ThroughPut = (f4Temp * 100); \
    if (f4FirstVal == RFC6374_INIT_VAL) \
    { \
        f4FirstVal = f4ThroughPut; \
    } \
    else \
    { \
        f4FirstVal = ((f4FirstVal + f4ThroughPut)/2); \
    } \
}while(0)

/****************************************************************************/
/* Delay Mesurement related macros*/
#define RFC6374_INCR_DM_TRANS_ID(pServiceNameInfo)pServiceNameInfo->DmInfo.u4DmSessionId ++
#define RFC6374_INCR_DM_SEQ_NUM(pServiceNameInfo)pServiceNameInfo->DmInfo.u4DmSeqCount++
#define RFC6374_INCR_1DM_RCVD_COUNT(pServiceNameInfo)pServiceNameInfo->DmInfo.u4DmNoOf1DmIn++
#define RFC6374_INCR_DMR_RCVD_COUNT(pServiceNameInfo)pServiceNameInfo->DmInfo.u4DmNoOfDmrIn++

#define RFC6374_DM_DECR_SESSION_COUNT() \
        (gR6374GlobalInfo.u1DmTotalSessionCount--)

#define RFC6374_DM_INCR_SESSION_COUNT()\
        (gR6374GlobalInfo.u1DmTotalSessionCount++)

#define RFC6374_DM_GET_CURRENT_SESSION_COUNT()\
        (gR6374GlobalInfo.u1DmTotalSessionCount)

#define RFC6374_DM_RESET_SEQ_COUNT(pDmInfo)\
        (pDmInfo->u4DmSeqCount = RFC6374_RESET)

#define RFC6374_DM_RESET_ROSEQ_COUNT(pDmInfo)\
        (pDmInfo->u4DmResetSeqCount = RFC6374_RESET)

#define RFC6374_DM_RESET_RX_ROSEQ_COUNT(pDmInfo)\
        (pDmInfo->u4RxDmResetSeqCount = RFC6374_RESET)

#define RFC6374_DM_INCR_SESSION_ID(pDmInfo)\
        (pDmInfo->u4DmSessionId++)

#define RFC6374_DM_RESET_BUFFER_ADD_COUNT(pDmInfo)\
        (pDmInfo->u1DmBufferAddCount = RFC6374_RESET)

#define RFC6374_DM_INCR_BUFFER_ADD_COUNT(pDmInfo)\
        (pDmInfo->u1DmBufferAddCount++)

#define RFC6374_DM_DECR_RUNNING_COUNT(pDmInfo)\
        (pDmInfo->u2DmRunningCount--)

#define RFC6374_DM_RESET_PROACTIVE_TX_COUNT(pDmInfo)\
        (pDmInfo->u4DmNoOfProTxCount = RFC6374_RESET)

#define RFC6374_DM_INCR_PROACTIVE_TX_COUNT(pDmInfo)\
        (pDmInfo->u4DmNoOfProTxCount++)

#define RFC6374_DM_SET_RUNNING_COUNT(pDmInfo)\
    (pDmInfo->u2DmRunningCount = pDmInfo->u2DmNoOfMessages)

#define RFC6374_DM_INCR_RX_SEQ_COUNT(pDmInfo)\
        (pDmInfo->u4RxDmSeqCount++)

#define RFC6374_DM_INCR_SEQ_COUNT(pDmInfo)\
        (pDmInfo->u4DmSeqCount++)

#define RFC6374_DM_INCR_ERR_PKT(pDmInfo)\
            (pDmInfo->u4RxDmPacketLoss++)

#define RFC6374_DM_INCR_DM_NO_RESP_TX_COUNT(pDmInfo)\
        (pDmInfo->u4DmNoRespTxCount++)

#define RFC6374_DM_RESET_DM_NO_RESP_TX_COUNT(pDmInfo)\
        (pDmInfo->u4DmNoRespTxCount = RFC6374_RESET)

#define RFC6374_DM_RESET_RX_PKT_LOSS(pDmInfo)\
        (pDmInfo->u4RxDmPacketLoss = RFC6374_RESET)
       
#define RFC6374_DM_INCR_RESP_TIMEOUT_RETRIES_COUNT(pDmInfo)\
        (pDmInfo->u1DmRespTimeOutReTries++)

#define RFC6374_DM_RESET_RESP_TIMEOUT_RETRIES_COUNT(pDmInfo)\
        (pDmInfo->u1DmRespTimeOutReTries = RFC6374_RESET)
       
#define RFC6374_DM_RESET_PACKETLOSS(pDmInfo)\
        (pDmInfo->u4RxDmPacketLoss = RFC6374_RESET)

#define RFC6374_DM_RESET_RESP_TX_COUNT(pDmInfo)\
        (pDmInfo->u4DmNoRespTxCount = RFC6374_RESET)
/****************************************************************************/
#define RFC6374_CONVERT_SNMP_TIME_TICKS_TO_MSEC(u4SnmpTimeTicks)\
            (u4SnmpTimeTicks*10)

/****************************************************************************/
/* Global Statistics */
#define RFC6374_INCR_MEMORY_FAILURE_COUNT()gR6374GlobalInfo.u4MemoryFailureCount++
#define RFC6374_INCR_BUFFER_FAILURE_COUNT() gR6374GlobalInfo.u4BufferFailureCount++

/****************************************************************************/
/* Macro for allocating Memory Blocks from the Memory Pools */
#define RFC6374_ALLOC_MEM_BLOCK_PD_BUFF_TABLE(pPktDelayBuffNode) \
    (pPktDelayBuffNode = (tR6374DmBufferTableEntry *) MemAllocMemBlk(RFC6374_DMBUFFERTABLE_POOLID))

#define RFC6374_PD_BUFFER_POOL\
        (RFC6374__CURR_CONTEXT_INFO())->PktDelayBuffMemPool

#define RFC6374_ALLOC_MEM_BLOCK_MSGQ(pMsg)\
        (pMsg = (tR6374QMsg *) MemAllocMemBlk(RFC6374_QMSG_POOL()))

#define RFC6374_PD_BUFFER_TABLE\
        (RFC6374_CURR_CONTEXT_INFO())->PktDelayBuffer

#define RFC6374_COMPARE_SERVICE_NAME(SerName1,SerName2)\
        ((!RFC6374_MEMCMP(SerName1,SerName2,(RFC6374_SER_NAME_LENGTH)))?(RFC6374_SUCCESS):(RFC6374_FAILURE))

#define RFC6374_IS_SYSTEM_STARTED(u4ContextId)\
        (RFC6374_SYSTEM_STATUS(u4ContextId)==RFC6374_START)
#define RFC6374_IS_SYSTEM_SHUTDOWN(u4ContextId)\
        (!RFC6374_IS_SYSTEM_STARTED(u4ContextId))

#define RFC6374_SYSTEM_STATUS(u4ContextId) \
    ((u4ContextId >= \
    R6374_MAX_CONTEXTS)?(R6374_SHUTDOWN):(gau1R6374SystemStatus[u4ContextId]))

#define RFC6374_COPY_TIME_REPRESENTATION(pDstTimRep,pSrcTimeRep)\
    RFC6374_MEMCPY(pDstTimRep,pSrcTimeRep,sizeof(tR6374TSRepresentation))

#define RFC6374_IS_SYSTEM_INITIALISED == RFC6374_TRUE

/****************************************************************************/
/* Service Stats counter */
#define RFC6374_INCR_STATS_LMM_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmmOut++

#define RFC6374_INCR_STATS_LMM_IN(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmmIn++

#define RFC6374_INCR_STATS_LMR_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmrOut++

#define RFC6374_INCR_STATS_LMR_IN(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmrIn++

#define RFC6374_INCR_STATS_1LM_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4Stats1LmOut++

#define RFC6374_INCR_STATS_1LM_IN(pServiceInfo)\
    pServiceInfo->Stats.u4Stats1LmIn++

#define RFC6374_INCR_STATS_DMM_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4StatsDmmOut++

#define RFC6374_INCR_STATS_DMM_IN(pServiceInfo)\
    pServiceInfo->Stats.u4StatsDmmIn++

#define RFC6374_INCR_STATS_DMR_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4StatsDmrOut++

#define RFC6374_INCR_STATS_DMR_IN(pServiceInfo)\
    pServiceInfo->Stats.u4StatsDmrIn++

#define RFC6374_INCR_STATS_1DM_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4Stats1DmOut++

#define RFC6374_INCR_STATS_1DM_IN(pServiceInfo)\
    pServiceInfo->Stats.u4Stats1DmIn++

#define RFC6374_INCR_STATS_LMMDMM_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmmDmmOut++

#define RFC6374_INCR_STATS_LMMDMM_IN(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmmDmmIn++

#define RFC6374_INCR_STATS_LMRDMR_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmrDmrOut++

#define RFC6374_INCR_STATS_LMRDMR_IN(pServiceInfo)\
    pServiceInfo->Stats.u4StatsLmrDmrIn++

#define RFC6374_INCR_STATS_1LMDM_OUT(pServiceInfo)\
    pServiceInfo->Stats.u4Stats1LmDmOut++

#define RFC6374_INCR_STATS_1LMDM_IN(pServiceInfo)\
    pServiceInfo->Stats.u4Stats1LmDmIn++

/****************************************************************************/

#define RFC6374_GET_SESSION_ID_FROM_MAC(au1MacAddr, u4SessionId)\
{\
    u4SessionId = (UINT4) (au1MacAddr[3] * (1) + au1MacAddr[4] * (16) + au1MacAddr[5] * (16 * 16));\
}

/* Session Rollong-back related macros */
#define RFC6374_SESS_MAX_ROLLING_LIMIT 10000000 

/* Deadline to wait for response, after sending last query and
 * before closing a session*/
#define R6374_MAX_DEADLINE_IN_MS 1000

#endif /* end of __R6374MACS_H__ */
