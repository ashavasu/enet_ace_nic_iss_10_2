/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374defn.h,v 1.2 2017/07/25 12:00:36 siva Exp $
 *
 * Description: This file contains definitions for r6374 module.
 *******************************************************************/

#ifndef __RFC6374DEFN_H__
#define __RFC6374DEFN_H__

/* PDU Types */
enum
{
RFC6374_PDU_TYPE_DIRECT_LMM=1,
RFC6374_PDU_TYPE_DIRECT_LMR,
RFC6374_PDU_TYPE_INFERED_LMM,
RFC6374_PDU_TYPE_INFERED_LMR,
RFC6374_PDU_TYPE_DMM,
RFC6374_PDU_TYPE_DMR,
RFC6374_PDU_TYPE_INFERED_LMMDMM,
RFC6374_PDU_TYPE_INFERED_LMRDMR,
RFC6374_PDU_TYPE_DIRECT_LMMDMM,
RFC6374_PDU_TYPE_DIRECT_LMRDMR
};

/* Timer types */
enum{
    RFC6374_LM_QUERY_TMR = 0,    /* LM Interval type for timer between
                                    two LM Packets */
    RFC6374_DM_QUERY_TMR,        /* DM Interval type for timer between
                                    two DM Packets */
    RFC6374_LMDM_QUERY_TMR,      /* LMDM Interval type for timer between
                                    two LMDM Packets */
    RFC6374_MAX_TMRS           /* Max Timer for RFC6374 */
};

/* Validation Control Code */
enum{
    RFC6374_VALIDATION_SUCCESS = 0,    /* When PDU Fields are correct */ 
    RFC6374_INVALID_VERSION,           /* Recevied Version is invalid */
    RFC6374_INVALID_DATA_FORMAT,       /* Recevied Data Flag is invalid */
    RFC6374_INVALID_CTRL_CODE,         /* Recevied Packet is having invalid 
                                          Control code */ 
    RFC6374_INVALID_TS_FORMAT,          /* Invalid TimeStamp Format in
                                          Query/Responder */
    RFC6374_INVALID_QUERY_INTERVAL       /* Query sent from Querier at 
                                          unsupported interval */

};

/* Test Mode */
enum
{
RFC6374_TEST_MODE_LM = 1, /* Independent LM */
RFC6374_TEST_MODE_DM,    /* Independent DM */
RFC6374_TEST_MODE_LMDM,  /* Combined LMDM */
RFC6374_TEST_MODE_LM_DM_ALL /*Either independent LM & DM or Combined LMDM */
};

/******************************** Mempools*************************************/
#define RFC6374_INPUTPARAMS_POOLID \
        R6374MemPoolIds[MAX_RFC6374_INPUT_PARAMS_SIZING_ID]

#define RFC6374_OUTPUTPARAMS_POOLID \
        R6374MemPoolIds[MAX_RFC6374_OUTPUT_PARAMS_SIZING_ID]

#define RFC6374_MPLSINPUTPARAMS_POOLID \
        R6374MemPoolIds[MAX_RFC6374_MPLS_API_INPUT_BUF_SIZING_ID]

#define RFC6374_MPLSOUTPUTPARAMS_POOLID \
        R6374MemPoolIds[MAX_RFC6374_MPLS_API_OUTPUT_BUF_SIZING_ID]

#define RFC6374_PDU_BUFFER_POOLID \
        R6374MemPoolIds[MAX_RFC6374_PDU_BUF_SIZING_ID]

#define RFC6374_SERVICECONFIGTABLE_POOLID \
        R6374MemPoolIds[MAX_RFC6374_SERVICECONFIGTABLE_SIZING_ID]

#define RFC6374_LMBUFFERTABLE_POOLID \
        R6374MemPoolIds[MAX_RFC6374_LMBUFFERTABLE_SIZING_ID]

#define RFC6374_LMSTATSTABLE_POOLID \
        R6374MemPoolIds[MAX_RFC6374_LMSTATSTABLE_SIZING_ID]

#define RFC6374_DMBUFFERTABLE_POOLID \
        R6374MemPoolIds[MAX_RFC6374_DMBUFFERTABLE_SIZING_ID]

#define RFC6374_DMSTATSTABLE_POOLID \
        R6374MemPoolIds[MAX_RFC6374_DMSTATSTABLE_SIZING_ID]

#define RFC6374_MPLS_PATH_INFO_POOLID \
        R6374MemPoolIds[MAX_RFC6374_MPLS_PATH_INFO_SIZING_ID]

/******************************************************************************/
/***************************Generic Task related*******************************/
#define  RFC6374_TIMER_EVENT                    0x00000001 
#define  RFC6374_QMSG_EVENT                     0x00000002 
#define  RFC6374_PKT_RX_EVENT                   0x00000004

#define  RFC6374_MAX_CONTEXT_NAME_LEN            VCM_ALIAS_MAX_LEN

#define  RFC6374_IPV4_ADDR_LEN                   4

#define  RFC6374_TASK_PRIORITY                  40
#define  RFC6374_QUEUE_NAME                     (const UINT1 *) "RFC6374Q"
#define  RFC6374_MUT_EXCL_SEM_NAME              (const UINT1 *) "RFC6374M"
#define  RFC6374_TASK_NAME                      (const UINT1 *) "RFC6374"
#define  RFC6374_QUEUE_DEPTH                    100
#define  RFC6374_SEM_CREATE_INIT_CNT            1
/******************************************************************************/
/****************************RFC6374 Default values****************************/
/* Service Related Default Values */
#define RFC6374_DEFAULT_TTL_VAL                         255
#define RFC6374_DEFAULT_TRAFFIC_CLASS                   0
#define RFC6374_DEFAULT_ENCAP_TYPE                      RFC6374_ENCAP_GAL_GACH
#define RFC6374_DEFAULT_DYADIC_MEASUREMENT              RFC6374_DYADIC_MEAS_DISABLE
#define RFC6374_DEFAULT_NEGOTIATION_STATUS              RFC6374_NEGOTIATION_DISABLE
#define RFC6374_DEFAULT_DYADIC_PROACTIVE_ROLE           RFC6374_DYADIC_PRO_ROLE_PASSIVE

/* Loss Related Default Values */
#define RFC6374_LM_DEFAULT_TYPE                         RFC6374_LM_TYPE_TWO_WAY 
#define RFC6374_LM_DEFAULT_METHOD                       RFC6374_LM_METHOD_INFERED 
#define RFC6374_LM_DEFAULT_MODE                         RFC6374_LM_MODE_ONDEMAND
#define RFC6374_LM_DEFAULT_ONDEMAND_COUNT               10 
#define RFC6374_LM_DEFAULT_INTERVAL                     1000
#define RFC6374_LM_DEFAULT_TS_FORMAT                    RFC6374_TS_FORMAT_IEEE1588

/* Delay Related Default Values */
#define RFC6374_DM_DEFAULT_TYPE                         RFC6374_DM_TYPE_TWO_WAY 
#define RFC6374_DM_DEFAULT_MODE                         RFC6374_DM_MODE_ONDEMAND
#define RFC6374_DM_DEFAULT_ONDEMAND_COUNT               10
#define RFC6374_DM_DEFAULT_INTERVAL                     1000
#define RFC6374_DM_DEFAULT_TS_FORMAT                    RFC6374_TS_FORMAT_IEEE1588

/* Loss and delay Related Default Values */
#define RFC6374_LMDM_DEFAULT_TYPE                   RFC6374_LMDM_TYPE_TWO_WAY 
#define RFC6374_LMDM_DEFAULT_METHOD                 RFC6374_LMDM_METHOD_INFERED 
#define RFC6374_LMDM_DEFAULT_MODE                   RFC6374_LMDM_MODE_ONDEMAND
#define RFC6374_LMDM_DEFAULT_ONDEMAND_COUNT         10 
#define RFC6374_LMDM_DEFAULT_INTERVAL               1000
#define RFC6374_LMDM_DEFAULT_TS_FORMAT              RFC6374_TS_FORMAT_IEEE1588

/******************************************************************************/
/****************************Generic Constants*********************************/
#define RFC6374_RB_BUFFER_MAX_COUNT                     RFC6374_MAX_SEQUENCE_COUNT_PER_SESSION 
#define RFC6374_MAX_ONGOING_SESSION                     RFC6374_MAX_SESSION_COUNT_PER_SERVICE

#define RFC6374_RESP_TIMEOUT_RETRIES_MAX_COUNT          4

#define RFC6374_TX_STATUS_START                         1
#define RFC6374_TX_STATUS_STOP                          2
#define RFC6374_TX_STATUS_READY                         3
#define RFC6374_TX_STATUS_NOT_READY                     4

#define RFC6374_MEASUREMENT_TYPE_LOSS                   1
#define RFC6374_MEASUREMENT_TYPE_DELAY                  2
#define RFC6374_MEASUREMENT_TYPE_LOSSDELAY              3

#define RFC6374_MEASUREMENT_COMPLETE                    0
#define RFC6374_MEASUREMENT_ONGOING                     1
#define RFC6374_MEASUREMENT_ABORTED                     2

#define RFC6374_MIN_TIME_INTERVAL_IN_MS                 100
#define RFC6374_MAX_TIME_INTERVAL_IN_MS                 10000

#define RFC6374_MIN_RECEPTION_INTERVAL                  CLI_RFC6374_DEFAULT_RECEPTION_INTERVAL
#define RFC6374_MAX_RECEPTION_INTERVAL                  10000
#define RFC6374_DEFAULT_RECEPTION_INTERVAL              RFC6374_MIN_RECEPTION_INTERVAL

#define RFC6374_MIN_QUERY_TRANSMIT_RETRYCNT             3
#define RFC6374_MAX_QUERY_TRANSMIT_RETRYCNT             100
#define RFC6374_DEFAULT_QUERY_TRANSMIT_RETRYCNT         25

#define RFC6374_DYADIC_PRO_ROLE_ACTIVE                  1
#define RFC6374_DYADIC_PRO_ROLE_PASSIVE                 2

#define RFC6374_MIN_TRAFFIC_CLASS                       0
#define RFC6374_MAX_TRAFFIC_CLASS                       7

#define RFC6374_DYADIC_MEAS_ENABLE                      1
#define RFC6374_DYADIC_MEAS_DISABLE                     2

#define RFC6374_SESS_INT_QUERY_ENABLE                   1
#define RFC6374_SESS_INT_QUERY_DISABLE                  2

#define RFC6374_MIN_ONDEMAND_MSG_COUNT                  1
#define RFC6374_MAX_ONDEMAND_MSG_COUNT                  1000
#define RFC6374_MAX_LMDM_ONDEMAND_MSG_COUNT             1000

#define RFC6374_MIN_PACKET_SIZE                         100
#define RFC6374_MAX_PACKET_SIZE                         1450
#define RFC6374_DEFAULT_PACKET_SIZE                     200

#define RFC6374_NO_COMP_DST    4094

#define RFC6374_SESS_TYPE_INDEPENDENT                   1
#define RFC6374_SESS_TYPE_COMBINED                      2

#define RFC6374_SUPP_TS_FORMAT_NTP                 0x01
#define RFC6374_SUPP_TS_FORMAT_IEEE1588            0x02

/* Whenever proactive or on-demand test is aborted
 * by user*/
#define RFC6374_TEST_ABORTED                            1
/* whenever proactive test is getting restarted */
#define RFC6374_PROACTIVE_TEST_RESTARTED                2
/* Proactive test can be started in a two way
 * 1. while configuring mode as proactive
 * with availability of all the mandatory parameters or
 * 2. mode is already configured as proactive but test didnt
 * get started due to unavailability of mandatory parameters.
 * now configuring those parameters to a service */
#define RFC6374_PROACTIVE_TEST_START                    3 
/*****************************************************************************/
/******************************** LM related values***************************/
#define RFC6374_LM_TYPE_ONE_WAY             CLI_RFC6374_TYPE_ONE_WAY  /* One way */
#define RFC6374_LM_TYPE_TWO_WAY             CLI_RFC6374_TYPE_TWO_WAY  /* Two way */

#define RFC6374_LM_MODE_ONDEMAND            CLI_RFC6374_MODE_ONDEMAND /* Ondemand */
#define RFC6374_LM_MODE_PROACTIVE           CLI_RFC6374_MODE_PROACTIVE /* ProActive*/

#define RFC6374_LM_METHOD_DIRECT            CLI_RFC6374_METHOD_DIRECT /* Direct */
#define RFC6374_LM_METHOD_INFERED           CLI_RFC6374_METHOD_INFERED /* Inferred */

/******************************************************************************/
/******************************** DM related values****************************/
#define RFC6374_DM_TYPE_ONE_WAY             CLI_RFC6374_TYPE_ONE_WAY /* One way */
#define RFC6374_DM_TYPE_TWO_WAY             CLI_RFC6374_TYPE_TWO_WAY /* Two way */

#define RFC6374_DM_MODE_ONDEMAND            CLI_RFC6374_MODE_ONDEMAND /* OnDemand */
#define RFC6374_DM_MODE_PROACTIVE           CLI_RFC6374_MODE_PROACTIVE /* ProActive */  

#define RFC6374_NEGOTIATION_ENABLE      CLI_RFC6374_NEGOTIATION_ENABLE  /* Negotiation Enable */
#define RFC6374_NEGOTIATION_DISABLE     CLI_RFC6374_NEGOTIATION_DISABLE /* Negotiation Disable */

/******************************************************************************/
/************************Combined LMDM related values ************************/
#define RFC6374_LMDM_TYPE_ONE_WAY    CLI_RFC6374_TYPE_LMDM_ONE_WAY  /* One way */
#define RFC6374_LMDM_TYPE_TWO_WAY    CLI_RFC6374_TYPE_LMDM_TWO_WAY  /* Two way */

#define RFC6374_LMDM_MODE_ONDEMAND   CLI_RFC6374_MODE_LMDM_ONDEMAND /* Ondemand */
#define RFC6374_LMDM_MODE_PROACTIVE  CLI_RFC6374_MODE_LMDM_PROACTIVE /* ProActive*/

#define RFC6374_LMDM_METHOD_DIRECT   CLI_RFC6374_METHOD_LMDM_DIRECT /* Direct */
#define RFC6374_LMDM_METHOD_INFERED  CLI_RFC6374_METHOD_LMDM_INFERED /* Inferred */

#define RFC6374_LMDM_CAL_ONE_WAY     3  /* One way */
/******************************************************************************/
     /* Padding TLV */
#define RFC6374_MIN_PADDING_SIZE           CLI_RFC6374_MIN_PADDING_SIZE 
#define RFC6374_MAX_PADDING_SIZE           CLI_RFC6374_MAX_PADDING_SIZE 
#define RFC6374_PADDING_TLV_COPY_IN_RESP   0x0 /* Padding - copy in response. 
          Section 3.5 */
#define RFC6374_DEFAULT_PAD_SIZE           RFC6374_MIN_PADDING_SIZE
#define RFC6374_MAX_PAD_SIZE_SINGLE_TLV       255
/*********************************RFC6374 PDU related**************************/
#define RFC6374_PDU_VERSION                 0x0 /* 4bit PDU Version */
#define RFC6374_PDU_QUERY_FLAG              0x0 /* 4bit Flag Set 
                                                 * |R|T|0|0| R Bit 
                                                 * set to Query value 0 */
#define RFC6374_PDU_RESP_FLAG               1<<3 /* 4bit Flag Set
                                                   |R|T|0|0| R Bit 
                                                   Set to Response value 1*/
#define RFC6374_PDU_RESERVED_FIELD          0x0
#define RFC6374_PDU_RESERVED_FIELD_LEN      3    /* 3 bytes */

#define RFC6374_LMDM_PDU_RESERVED_LEN       2    /* 2 bytes */

/* Control Code */
#define RFC6374_CTRL_CODE_LENGTH            1   /* Field length */
#define RFC6374_CTRL_CODE_RESP_INBAND_REQ   0x0 /* 8bit Control Code 
                                                 * Set for In Band Response */ 
#define RFC6374_CTRL_CODE_RESP_OUTBAND_REQ  0x1 /* 8bit Control Code
                                                 * Set for Out Band Response */
#define RFC6374_CTRL_CODE_RESP_NOT_REQ      0x2 /* 8bit Control Code
                                                 * Set for Out Band Response */
#define RFC6374_RCVD_CTRL_CODE_SUCCESS      0x1 /* 8bit Control Code
                                                   Set for Successful Response*/
#define RFC6374_CTRL_CODE_UNSUP_VERSION   0x11/* 8bit Control Code
                                                   Set for UnSupported Version 
                                                   Recevied from Querier */
#define RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT 0x13 /* 8bit Control Code 
                                                      Set for UnSupported Data 
                                                      Format from Querier */
#define RFC6374_CTRL_CODE_UNSUP_CTRL_CODE 0x12 /* 8bit Control code
                                                   set for Unspported control
                                                   is recevied at Responder */
#define RFC6374_CTRL_CODE_DATA_FORMAT_INVALID 0x2 /* 8bit control code
                                                   Set when Timestamp format is
                                                   invalid from expected */

#define RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL 0x18   /* 8bit control code
                                                        Set when queries are sent at interval 
                                                        lesser than responder's receiving interval*/

/* Channel Type */
#define RFC6374_CHANNEL_TYPE_DIRECT_LM      0x000A
#define RFC6374_CHANNEL_TYPE_INFERED_LM     0x000B
#define RFC6374_CHANNEL_TYPE_DM             0x000C
#define RFC6374_CHANNEL_TYPE_DIRECT_LMDM    0x000D
#define RFC6374_CHANNEL_TYPE_INFERED_LMDM   0x000E

/* TS format */
#define RFC6374_TS_FORMAT_NULL              0
#define RFC6374_TS_FORMAT_SEQ_NUM           1
#define RFC6374_TS_FORMAT_NTP               2   
#define RFC6374_TS_FORMAT_IEEE1588          3
/******************************************************************************/
/************************************LM PDU Related****************************/
#define RFC6374_LM_PDU_MSG_LEN              0x34 /* Total PDU length=52 
                                                    including Verision, Flag 
                                                    and Control Code Feild */
#define RFC6374_LM_32BIT_COUNTERS           0x0  /* 4bit DFLAG set 
                                                   |X|B|0|0| X bit
                                                   Set to 0 for 32bit counter
                                                   and B set to 0 for Packet 
                                                   Count */
#define RFC6374_LM_64BIT_COUNTERS           1<<3 /* 4bit DFLAG set
                                                   |X|B|0|0| X bit
                                                   Set to 1 for 64bit counter
                                                   and B set to 0 for Packet 
                                                   Count */

#define RFC6374_LM_PKT_COUNT                0x0
#define RFC6374_LM_OCTET_COUNT              1<<2
/******************************************************************************/
/************************************DM PDU Related****************************/
#define RFC6374_DM_PDU_MSG_LEN              0x2C /* Total PDU length=44
                                                    including Verision, Flag 
                                                    and Control Code Feild */
#define RFC6374_DMM_RESERVED_SIZE           24   /* Size of reserved bytes to
                                                    be filled in DMM frame */
#define RFC6374_TIMESTAMP_FIELD_SIZE        8    /* Size of the TimeStamp
                                                    field used in 1DM and DMM*/
#define RFC6374_1DM_RESERVED_SIZE           24   /* Size of reserved bytes 
                                                    to be filled in 1DM frame */
#define RFC6374_1DM_DEF_TRANS_ID            1
/******************************************************************************/

/************************************Combined LMDM PDU Related****************/
#define RFC6374_LMDM_PDU_MSG_LEN            0x4C /* Total PDU length=76 
                                                    including Verision, Flag 
                                                    and Control Code Feild */
#define RFC6374_LMDM_32BIT_COUNTERS         0x0  /* 4bit DFLAG set 
                                                    |X|B|0|0| X bit
                                                    Set to 0 for 32bit counter
                                                    and B set to 0 for Packet 
                                                    Count */
#define RFC6374_LM_64BIT_COUNTERS           1<<3 /* 4bit DFLAG set
                                                   |X|B|0|0| X bit
                                                   Set to 1 for 64bit counter
                                                   and B set to 0 for Packet 
                                                   Count */
#define RFC6374_LMDM_RESERVED_SIZE          24   /* Size of reserved bytes to
                                                    be filled in a frame */
#define RFC6374_LMDM_PKT_COUNT              0x0
#define RFC6374_LM_OCTET_COUNT              1<<2
/******************************************************************************/

/************************TLV Length*********************************************/
#define RFC6374_SESS_QUERY_INT_TLV_LEN      0x06 /* Session Query Interval TLV 
                                                  length*/
/******************************************************************************/

/************************************Timer Related****************************/
#define RFC6374_NUM_OF_TIME_UNITS_IN_A_SEC SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define RFC6374_NUM_OF_MSEC_IN_A_TIME_UNIT (1000/RFC6374_NUM_OF_TIME_UNITS_IN_A_SEC)
#define RFC6374_NUM_OF_NSEC_IN_A_USEC       1000
#define RFC6374_NUM_OF_USEC_IN_A_MSEC       1000
#define RFC6374_NUM_OF_MSEC_IN_A_SEC        1000
#define RFC6374_NUM_OF_USEC_IN_A_SEC        1000000
#define RFC6374_NUM_OF_NSEC_IN_A_SEC        1000000000
#define RFC6374_NUM_OF_SEC_IN_A_MIN         60
#define RFC6374_NUM_OF_MIN_IN_A_HOUR        60
#define RFC6374_NUM_OF_TICKS_IN_A_MSEC      10
#define RFC6374_RESPONSE_TIMER              3
/*****************************************************************************/
/************************************MPLS Related****************************/
#define RFC6374_MAX_LABEL_STACK                MPLS_MAX_LABEL_STACK
#define RFC6374_INVALID_LABEL                  0xFFFFFFFF

/* The maximun number of labels should be less than 0xff */
#define RFC6374_INVALID_INDEX                  0xff

/* Path Id related constants */
#define RFC6374_MPLS_PATH_TYPE_TNL             MPLS_PATH_TYPE_TUNNEL
#define RFC6374_MPLS_PATH_TYPE_LSP             MPLS_PATH_TYPE_NONTE_LSP
#define RFC6374_MPLS_PATH_TYPE_PW              MPLS_PATH_TYPE_PW

/*PW specific*/
#define RFC6374_L2VPN_VCCV_CC_RAL              0x40 /* Bit 1: Type 2: MPLS Router Alert Label */
#define RFC6374_L2VPN_VCCV_CC_TTL_EXP          0x20 /* Bit 2: Type 3: MPLS PW Label with TTL ==1 */

/* ACH TYPE */
#define RFC6374_ENCAP_NO_GAL_GACH          CLI_RFC6374_ACH_HEADER
#define RFC6374_ENCAP_GAL_GACH          CLI_RFC6374_GAL_GACH_HEADER

#define RFC6374_GAL_LABEL                      13
#define RFC6374_RAL_LABEL                      1
#define RFC6374_CHANNEL_TYPE                   0x8902

#define RFC6374_MPLS_LABEL_LEN                 4
#define RFC6374_ACH_HEADER_LEN                 4
#define RFC6374_ACH_HEADER_VER_RSVD_LEN        2

#define RFC6374_PROTOCOL_HEADER_LEN            32
#define RFC6374_MAX_PDU_LEN                    1500 

#define RFC6374_MIN_TTL                        1
#define RFC6374_MAX_TTL                        255

/* Queue */
#define  RFC6374_MPLS_PDU_IN_QUEUE             1

/* MPLS Port status*/
#define RFC6374_MPLS_PATH_UP                   1
#define RFC6374_MPLS_PATH_DOWN                 2
/*****************************************************************************/
/************************************Traps Related****************************/
#define RFC6374_OBJECT_NAME_LEN                    256
#define RFC6374_TRAP_OID                           "1.3.6.1.4.1.29601.2.107.9"

/*****************************************************************************/
/************************************SNMP Related****************************/
#define RFC6374_ROW_STATUS_ACTIVE                      ACTIVE
#define RFC6374_ROW_STATUS_NOT_IN_SERVICE              NOT_IN_SERVICE
#define RFC6374_ROW_STATUS_NOT_READY                   NOT_READY
#define RFC6374_ROW_STATUS_CREATE_AND_GO               CREATE_AND_GO
#define RFC6374_ROW_STATUS_CREATE_AND_WAIT             CREATE_AND_WAIT
#define RFC6374_ROW_STATUS_DESTROY                     DESTROY
/*****************************************************************************/
/************************************General**********************************/
#define RFC6374_DEFAULT_CONTEXT_ID                  0
#define RFC6374_MAX_CONTEXTS                        SYS_DEF_MAX_NUM_CONTEXTS
#define RFC6374_INVALID_CONTEXT                    (RFC6374_MAX_CONTEXTS + 1)

#define RFC6374_BITS_PER_BYTE          8  

/* General Table manipulation macros */
#define RFC6374_MAX_NAME_LEN                       32
/*****************************************************************************/
/************************************SYSLOG**********************************/
#define RFC6374_MAX_SYSLOG_MSG_LEN                 256
#define RFC6374_MAX_BYTES_IN_A_LINE                24
#define RFC6374_MAX_BYTES_IN_A_TOKEN               8
#define RFC6374_OBJECT_VALUE_LEN                   128
/*****************************************************************************/
/***************************Generic Constants**********************************/
#define RFC6374_SUCCESS                        (OSIX_SUCCESS)
#define RFC6374_FAILURE                        (OSIX_FAILURE)

#define RFC6374_RB_SUCCESS                       RB_SUCCESS
#define RFC6374_RB_FAILURE                       RB_FAILURE

#define RFC6374_SNMP_TRUE                      1
#define RFC6374_SNMP_FALSE                     2

#define RFC6374_ENABLED                        1
#define RFC6374_DISABLED                       2

#define RFC6374_START                           1
#define RFC6374_SHUTDOWN                        2

#define RFC6374_SET                             1
#define RFC6374_RESET                           0  
#define RFC6374_INIT_VAL                        0  

#define RFC6374_TRUE                            TRUE
#define RFC6374_FALSE                           FALSE

#define RFC6374_ADD                             1
#define RFC6374_DEL                             2

#define RFC6374_INCR_VAL                        1   /* Increment value */
#define RFC6374_DECR_VAL                        1   /* Decrement value */

#define RFC6374_REQ_OWNER_MGMT_CLI              1
#define RFC6374_REQ_OWNER_MGMT_SNMP             2

/* Array indexes */
#define RFC6374_INDEX_ZERO                      0
#define RFC6374_INDEX_ONE                       1
#define RFC6374_INDEX_TWO                       2
#define RFC6374_INDEX_THREE                     3
#define RFC6374_INDEX_FOUR                      4
#define RFC6374_INDEX_FIVE                      5
#define RFC6374_INDEX_SIX                       6
#define RFC6374_INDEX_SEVEN                     7
#define RFC6374_INDEX_EIGHT                     8
#define RFC6374_INDEX_NINE                      9
#define RFC6374_INDEX_TEN                       10
#define RFC6374_INDEX_ELEVEN                    11
#define RFC6374_INDEX_TWELVE                    12
#define RFC6374_INDEX_THIRTEEN                  13
#define RFC6374_INDEX_FOURTEEN                  14
#define RFC6374_INDEX_FIFTEEN                   15
#define RFC6374_INDEX_SIXTEEN                   16
#define RFC6374_INDEX_SEVENTEEN                 17
#define RFC6374_INDEX_EIGHTEEN                  18
#define RFC6374_INDEX_NINETEEN                  19
#define RFC6374_INDEX_TWINTY                    20

#define RFC6374_ARRAY_SIZE_64                   64

#define RFC6374_VAL_0                            0
#define RFC6374_VAL_1                            1
#define RFC6374_VAL_2                            2
#define RFC6374_VAL_3                            3
#define RFC6374_VAL_4                            4
#define RFC6374_VAL_5                            5
#define RFC6374_VAL_6                            6
#define RFC6374_VAL_7                            7
#define RFC6374_VAL_8                            8
#define RFC6374_VAL_9                            9
#define RFC6374_VAL_10                           10
#define RFC6374_VAL_40                           40
#define RFC6374_VAL_72                           72

/* Through Put Lenght */
#define RFC6374_THROUGH_PUT_LENGTH               6

/*****************************************************************************/
/*For clear buffer*/
#define RFC6374_LMBUFFER_ENTRY                  1
#define RFC6374_DMBUFFER_ENTRY                  2
#define RFC6374_LMSTATS_ENTRY                   3
#define RFC6374_DMSTATS_ENTRY                   4


/*****************************************************************************/
/* SQI TLV related macros */
#define RFC6374_SQI_TLV_TYPE                    2
#define RFC6374_SQI_TLV_LENGTH                  4

#define RFC6374_ALLOWABLE_RECEP_INT_VARIATION   (10)
#define RFC6374_TOTAL_PERCENT                    100

#endif  /* __RFC6374DEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file r6374defn.h                      */
/*-----------------------------------------------------------------------*/
