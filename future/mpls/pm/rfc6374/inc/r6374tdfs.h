/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374tdfs.h,v 1.4 2017/07/25 12:00:36 siva Exp $
 * 
 * Description: This file contains the data structure for LBLT Task. 
 *********************************************************************/

#ifndef __R6374TDFS_H
#define __R6374TDFS_H

typedef tCRU_INTERFACE            tR6374Interface;
typedef tCfaIfInfo                tR6374CfaIfInfo;
typedef tTMO_SLL                  tR6374Sll;
typedef tTMO_SLL_NODE             tR6374SllNode;
typedef tTMO_DLL                  tR6374Dll;
typedef tTMO_DLL_NODE             tR6374DllNode;
typedef tRBTree                   tR6374RBTree;
typedef tRBNodeEmbd               tR6374RBNodeEmbd;
typedef tMemPoolId                tR6374MemPoolId;
typedef tOsixSysTime              tR6374SysTime;
typedef UINT1                     tR6374MsgType;
typedef tMacAddr                  tR6374MacAddr;

/****************************************************************************/
/* Structure containing service pointer */
typedef struct
{
    UINT4 au4ServicePtr[256];
    UINT4 u4OidLength;
}tServicePtr;
/****************************************************************************/
/* request types for MPLS exit function */
typedef enum
{
    RFC6374_VCM_REGISTER_WITH_VCM,
    RFC6374_VCM_DEREGISTER_WITH_VCM,
    RFC6374_REQ_BFD_GET_SESS_INDEX_FROM_PW_INFO,
    RFC6374_REQ_BFD_GET_SESS_INDEX_FROM_LSP_INFO,
    RFC6374_REQ_BFD_GET_TXRX_FROM_SESS_INDEX,
    RFC6374_REQ_BFD_GET_EXP_FROM_SESS_INDEX,
    RFC6374_FM_SEND_TRAP,
    RFC6374_VCM_GET_CXT_ID_FRM_NAME
}tR6374ExtReqType;
/****************************************************************************/
typedef struct
{
    CHR1    ac1SyslogMsg [RFC6374_MAX_SYSLOG_MSG_LEN];
    UINT4   u4SyslogLevel;
}tR6374SyslogMsg;
/****************************************************************************/
/* This structure is the output for the Exit Funtion of RFC6374 */
typedef struct R6374ExtOutParams
{
    tBfdRespParams     BfdRespParams;   
                            /*Response from BFD Module*/
    UINT4              u4ContextId;
}tR6374ExtOutParams;
/****************************************************************************/
/* This Structure is the input for the Exit Function
 * (Function to interact with the external modules) */
typedef struct R6374ExtInParams
{
    tR6374ExtReqType  eExtReqType;  
       /* This variable denotes the 
                             * type of the external request */
    union
    {
        tBfdReqParams     BfdReqParams;  
       /* To get info from BFD Module */
        tR6374SyslogMsg   SyslogMsg;
        tFmFaultMsg       FmFaultMsg;
        UINT1             au1ContextName[RFC6374_MAX_CONTEXT_NAME_LEN];
    }uInParams;
    UINT4             u4ContextId;
#define InBfdReqParams    uInParams.BfdReqParams
#define InSyslogMsg       uInParams.SyslogMsg
#define InFmFaultMsg      uInParams.FmFaultMsg
}tR6374ExtInParams;
/****************************************************************************/
/* Structure containing complete MPLS path information fetched from MPLS 
 * module*/
typedef struct R6374MplsPathInfo
{
    tTeTnlApiInfo MplsTeTnlApiInfo;
    tPwApiInfo    MplsPwApiInfo;
    UINT2         u2PathFilled;
    UINT1         u1PathStatus;
    UINT1         au1Pad[1];
#define RFC6374_CHECK_PATH_TE_AVAIL    0x1
#define RFC6374_CHECK_PATH_PW_AVAIL    0x2    
}tR6374MplsPathInfo;
/****************************************************************************/
/* PW related info */
typedef struct R6374MplsPwParams
{
   UINT4     u4IpAddr; /* Peer Address */
   UINT4     u4PwId;   /* Virtual Circuit Identifier */
}tR6374MplsPwParams;
/****************************************************************************/
/* LSP related info */
typedef struct R6374MplsLspParams
{
   UINT4     u4SrcIpAddr; /* Src Address */
   UINT4     u4DestIpAddr; /* Peer Address */
   UINT4     u4FrwdTunnelId;    /*Tunnel ID*/
   UINT4     u4RevTunnelId;    /* Reverse Tunnel ID*/
}tR6374MplsLspParams;
/****************************************************************************/
/* MPLS Path Identification parameters */
typedef struct R6374MplsParams {
    union{
        tR6374MplsPwParams   MplsPwParams; 
        tR6374MplsLspParams  MplsLspParams;
    }uMplsPathParams;
     UINT1                 u1MplsPathType; 
       /* Determines the type of the path
                                            * either LSP or PW */
     UINT1                 u1MeasurementType; 
       /*OneWay or TwoWay */
     UINT1                 u1EncapType;
                            /*Mpls with or without GAL header */
     UINT1                 au1Pad[1];      
       /* Array used for padding */
#define PwMplsPathParams uMplsPathParams.MplsPwParams
#define LspMplsPathParams uMplsPathParams.MplsLspParams
}tR6374MplsParams;
/****************************************************************************/
/* MPLS path Process information parameters */
typedef struct R6374ProcInfo
{
    tR6374MplsParams    *pMplsParms; 
       /* MPLS-TP Path Identification variable */
    UINT1               u1TrafficClass; 
       /* Traffi Class to be filled in the EXP 
                                           field of the MPLS header.*/
    UINT1               u1Ttl;      
       /* TTL to be filled in MPLS header */
    UINT1               au1Pad[2];
} tR6374ProcInfo;
/****************************************************************************/
/* Loss Measurement Info */
typedef struct R6374LmInfoTableEntry
{ 
    tR6374TSRepresentation   LmOriginTimeStamp;
    tR6374TSRepresentation   previousRxLMOriginTimeStamp;
    tTmrBlk        LMQueryTimer;
    UINT4          u4PreTxCounterf;
       /* Value of TxFCf received in the previous
         * LM frame */
    UINT4          u4PreRxCounterf;
       /* Value of RxFCf received in the previous
         *  LM frame */
    UINT4          u4PreTxCounterb;
       /* Value of TxFCb received in the previous
         *  LM frame */
    UINT4          u4PreRxCounterb;
       /* Value of RxFCb at the time of reception
                                         * of last LMR frame*/
    UINT4          u4TxCounterf;        
                            /* A_Tx For Dyadic */
    UINT4          u4RxCounterb;        
                            /* B_Rx For Dyadic */
    UINT4          u4LmSessionId; 
                            /* Local Session ID */
    UINT4          u4RxLmSessionId; 
                            /* Remote Session ID */
    UINT4          u4LmSeqCount;       
                            /* Variable used to store the received LMR seq 
         * count in two-way */
    UINT4          u4RxLmSeqCount;
                            /* Variable used to store the received LMM seq 
         * count in one-way*/
    UINT4          u4LmResetSeqCount;       
                            /* Variable used for two way rolling buffer */
    UINT4          u4RxLmResetSeqCount; 
                            /* Variable used for one way rolling buffer */
    UINT4          u4RxLmPacketLoss;
    UINT4          u4LmNoRespTxCount; 
                            /* Variable used to store the no of LMM packet that 
                             * haven't received response from peer node  as LMR */
    UINT4          u4LmNoOfProTxCount; 
       /* Variable used to update the total transmitted
                             * count for proactive test*/
    UINT2          u2LmConfiguredTimeInterval; 
    UINT2          u2LmTimeInterval; 
    UINT2          u2LmRespTimeInterval; 
    UINT2          u2LmNoOfMessages;
    UINT2          u2LmRunningCount;
    UINT1          u1LmStatus; 
    UINT1          u1LmType;
    UINT1          u1LmMethod;
    UINT1          u1LmMode;
    UINT1          u1PrevLmMode;
       /* Variable used to update the previous Lm mode.
                             * Based on this value, test abort trap will be 
                             * send out if proactive test is aborted.*/
    UINT1          u1LmTSFormat;
                            /* Variable used to hold the preferred
                             * time stamp format of the node */
    UINT1          u1LmQTSFormat;
                            /* Variable used for loss measumrent
                             * procedure, to take the timestamp values */
    UINT1          u1LmBufferAddCount;
    BOOL1          b1LmResultOk;
    BOOL1          b1LmTestStatus; 
                           /* Variable used to send a trap when on-demand or
                            * pro-active test is aborted by adminstrator or 
                            * by LSP/PW down and send a pro-active test 
                            * restart trap when LSP/PW comes up*/
    UINT1          u1LmRespTimeOutReTries; /* used to proactive response time out
                                              retries be done */
    UINT1          u1SessQueryIntervalState; /* Indicates the Session Interval
                                                Query state in Querier side*/
    BOOL1          b1IsSessQueryTlvRequired; /* Indicates whether to send SQI TLV
                                                in Responder side */
    UINT1         u1LmInvalidField;
    BOOL1         bIsLmStartSessPosted;
    BOOL1         bDeadlineTimeCrossed;
    UINT1         au1Pad[2];
}tR6374LmInfoTableEntry;
/****************************************************************************/
/*2Dm Pdu related required info*/
typedef struct R6374DmInfoTableEntry
{ 
   tR6374TSRepresentation MinPKtDelayValue;
    tR6374TSRepresentation   previousRxDMOriginTimeStamp;
   tR6374TSRepresentation   DmOriginTimeStamp;
   tTmrBlk           DMQueryTimer;
    tR6374TSRepresentation   TxTimeStampf;  
                                   /* A_Tx For Dyadic */
    tR6374TSRepresentation   RxTimeStampf;   
                                   /* B_Rx For Dyadic */
   UINT4             u4DmSessionId; 
                                   /* Local Session ID */
    UINT4                    u4RxDmSessionId; 
                                   /* Remote Session ID */
   UINT4             u4DmSeqCount;       
                                   /* Variable used to store the received DMR 
                             * seq count in two-way*/
   UINT4                 u4RxDmSeqCount;
                                   /* Variable used to store the received 
                                    * DMM Seq count in one-way*/
    UINT4                    u4DmResetSeqCount; 
                                   /* Variable used for 
                                    * two way rolling buffer */
    UINT4                    u4RxDmResetSeqCount; 
                                   /* Variable used for 
                                    * one way rolling buffer */
   UINT4                 u4RxDmPacketLoss;
   UINT4             u4DmNoOf1DmIn;
   UINT4             u4PktDelayThreshold;
   UINT4             u4DmNoRespTxCount; 
                                   /* Variable used to store the no of DMM packet 
               * that haven't received response from peer node 
               * as DMR */
    UINT4                    u4DmNoOfProTxCount;
                                   /* Variable used to update the total transmitted
                                    * count for proactive test */
    UINT4                    u4DmPadSize;
         /* Variable used to hold the padding tlv 
          * pattern size */
    UINT2                u2DmConfiguredTimeInterval; 
   UINT2             u2DmTimeInterval; 
   UINT2             u2DmRespTimeInterval; 
   UINT2             u2DmNoOfMessages;
   UINT2                 u2DmRunningCount;
   UINT1             u1DmStatus; 
   UINT1             u1DmType;
   UINT1             u1DmMode;
    UINT1                    u1PrevDmMode;
              /* Variable used to update the previous Dm mode.
                                    * Based on this value, test abort trap will be 
                                    * send out if proactive test is aborted.*/
   UINT1                 u1DmTSFormat;
                                   /* Variable used to hold the preferred
        * time stamp format of the node */
    UINT1                    u1DmQTSFormat;
                                   /* Variable used for delay measumrent
               * procedure, to take the timestamp values */
   UINT1                 u1DmBufferAddCount;
   BOOL1             b1DmResultOk;
    BOOL1                    b1DmTestStatus; 
                                   /* Variable used to send a trap when 
                                    * on-demand or pro-active test is 
                                    * aborted by adminstrator or 
                                    * by LSP/PW down and send a pro-active test 
                                    * restart trap when LSP/PW comes up*/
    BOOL1               b1DmInitResponder;
                                /* This Variable is set when Test is Initiated by
                                 * other node while receving the Initial packet
                                 * from query node */
    UINT1               u1DmRespTimeOutReTries; /* used to proactive response time out
                                                   retries be done */

    UINT1               u1SessQueryIntervalState; /* Indicates the Session Interval
                                                Query state in Querier side*/
    BOOL1               b1IsSessQueryTlvRequired; /* Indicates whether to send SQI TLV
                                                in Responder side */
    UINT1               u1DmInvalidField;

    UINT1  u1DmPadTlvCount ;
                                   /* This variable is used to count the
                      * number of padding TLV has to be included 
                      * in a DM pdu based on the padsize */
    BOOL1               bIsDmStartSessPosted;
    BOOL1               bDeadlineTimeCrossed;
    UINT1               u1Pad;

}tR6374DmInfoTableEntry;

/*************************************************************************/
/* Combined Loss and Delay Measurement Info */
typedef struct R6374LmDmInfoTableEntry
{
    tR6374TSRepresentation   LmDmOriginTimeStamp; 
    tR6374TSRepresentation   previousRxLMDMOriginTimeStamp;
    tTmrBlk              LMDMQueryTimer;
    tR6374TSRepresentation   TxTimeStampf;  
         /* A_Tx For Dyadic  */
    tR6374TSRepresentation   RxTimeStampf;  
                              /* B_Rx For Dyadic */
    UINT4                    u4PreCLmTxCounterf;
                              /* Value of TxFCf received in the previous
                               * LM frame */
    UINT4                    u4PreCLmRxCounterf;
                              /* Value of RxFCf received in the previous
                               *  LM frame */
    UINT4                    u4PreCLmTxCounterb;
                              /* Value of TxFCb received in the previous
                               *  LM frame */
    UINT4                    u4PreCLmRxCounterb;
                              /* Value of RxFCb at the time of reception
                               * of last LMR frame*/
    UINT4                    u4TxCounterf;   
                               /* A_Tx For Dyadic */
    UINT4                    u4RxCounterb;   
                               /* B_Rx For Dyadic */
    UINT4                u4LmDmSessionId;   
                               /* Local Session ID */
    UINT4                    u4RxLmDmSessionId; 
                               /* Remote Session ID */
    UINT4                    u4LmDmSeqCount;      
           /* Variable used to store the received  
                         * Combined LMRDMR seq count in two way*/
    UINT4                    u4RxLmDmSeqCount;
                               /* Variable used to store the received  
                         * Combined LMMDMM seq count in one-way */
    UINT4                    u4LmResetSeqCount;       
                               /* Variable used for combined two way loss 
           * rolling buffer */
    UINT4                    u4DmResetSeqCount;       
                               /* Variable used for combined two way dealy 
           * rolling buffer */
    UINT4                    u4RxLmResetSeqCount; 
                               /* Variable used for combined one way loss 
    * rolling buffer */
    UINT4                    u4RxDmResetSeqCount; 
                               /* Variable used for combined one way delay 
     * rolling buffer */
    UINT4                    u4RxLmDmPacketLoss;
    UINT4                    u4LmDmNoRespTxCount;
                               /* Variable used to store the no of CLMDM packet 
           * that haven't received response from peer node 
           * as CLMRDMR */
    UINT4                    u4LmDmNoOfProTxCount; 
          /* Variable used to update the total transmitted
                                * count for proactive test*/
    UINT4                    u4LmDmPadSize;
         /* Variable used to hold the padding tlv 
          * pattern size */
    UINT2                    u2LmDmConfiguredTimeInterval; 
    UINT2                    u2LmDmTimeInterval; 
    UINT2                    u2LmDmNoOfMessages;
    UINT2                    u2LmDmRunningCount;
    UINT1                    u1LmDmStatus; 
    UINT1                    u1LmDmType;
    UINT1                    u1LmDmMethod;
    UINT1                    u1LmDmMode;
    UINT1                    u1PrevLmDmMode;
                               /* Variable used to update the previous LmDm mode.
                                * Based on this value, test abort trap will be
                                * send out if proactive test is aborted.*/
    UINT1                    u1LmDmTSFormat;
                                /* Variable used to hold the preferred
                                 * time stamp format of the node */
    UINT1                    u1LmDmQTSFormat;
                                /* Variable used for delay measumrent
                      * procedure, to take the timestamp values */
    UINT1                    u1LmDmBufferAddCount;
    BOOL1                    b1LmDmResultOk;
    BOOL1                    b1LmDmTestStatus; 
                                /* Variable used to send a trap when on-demand or
                                 * pro-active test is aborted by adminstrator or 
                                 * by LSP/PW down and send a pro-active test 
                                 * restart trap when LSP/PW comes up*/
    BOOL1                    b1LmDmInitResponder;
                                /* This Variable is set when Test is Initiated by
                                 * other node while receving the Initial packet 
                                 * from query node */
    UINT1                    u1LmDmRespTimeOutReTries; 
                         /* used to proactive response time out
                                 * retries be done */
    UINT1                    u1SessQueryIntervalState; 
                  /* Indicates the Session Interval
                                 * Query state in Querier side*/
    BOOL1                    b1IsSessQueryTlvRequired; 
    /* Indicates whether to send SQI TLV
                                 * in Responder side */
    UINT1                    u1LmDmInvalidField;
    UINT1       u1LmDmPadTlvCount ;
                                /* This variable is used to count the
                   * number of padding TLV has to be included 
                   * in a Combined LMDM pdu based on the padsize */
    BOOL1                    bIsLmDmStartSessPosted;
    BOOL1                    bDeadlineTimeCrossed;
    UINT1                    au1Pad[2];
}tR6374LmDmInfoTableEntry;

/****************************************************************************/
/* Service Stats Entry */
typedef struct R6374StatsTableEntry
{
    UINT4           u4StatsLmmOut;
    UINT4           u4StatsLmmIn;
    UINT4           u4StatsLmrOut;
    UINT4           u4StatsLmrIn;
    UINT4           u4Stats1LmOut;
    UINT4           u4Stats1LmIn;
    UINT4           u4Stats1DmOut;
    UINT4           u4Stats1DmIn;
    UINT4           u4StatsDmmOut;
    UINT4           u4StatsDmmIn;
    UINT4           u4StatsDmrOut;
    UINT4           u4StatsDmrIn;
    UINT4           u4StatsLmmDmmOut;
    UINT4           u4StatsLmmDmmIn;
    UINT4           u4StatsLmrDmrOut;
    UINT4           u4StatsLmrDmrIn;
    UINT4           u4Stats1LmDmOut;
    UINT4           u4Stats1LmDmIn;

}tR6374StatsTableEntry;
/****************************************************************************/
/* Service related info */
typedef struct R6374ServiceConfigTableEntry
{
    tR6374RBNodeEmbd           ServiceConfigTableNode;
    tR6374MplsParams           R6374MplsParams;  
    tR6374LmInfoTableEntry     LmInfo;
    tR6374DmInfoTableEntry     DmInfo;
    tR6374LmDmInfoTableEntry   LmDmInfo;
    tR6374StatsTableEntry      Stats; 
    UINT4                      u4ContextId;   
    UINT4                      u4LastLmDmSessId; /* To store the last Session 
                                                    ID only from Combined 
                                                    LMDM Session */
    UINT4                      u4LastLmSessId;  /* To Store Last Loss Measurement
                                                   Session ID from either Indep LM
                                                   or combLMDM */
    UINT4                      u4LastDmSessId;  /* To Store Last Delay Measurement
                                                   Session ID from either Indep DM
                                                   or combLMDM */
    INT4                       i4Negostatus;
                                  /*Timestamp negotiation status*/
    UINT1                      au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1                      u1TimeToLive;
    UINT2                      u2MinReceptionInterval;
    UINT1                      u1TrafficClass;
    UINT1                      u1EncapType;
    UINT1                      u1PathStatus;
   UINT1               u1RowStatus;
                                   /* Row Status for the service */
    UINT1                      u1DyadicMeasurement;
    UINT1                      u1NegoSuppTsFormat;
                                   /* Negotiated TS format*/
    UINT1                      u1DyadicProactiveRole;
    UINT1                      u1SessIntQueryStatus;
    UINT1                      u1QueryTransmitRetryCount;
    UINT1                      u1Pad[3];
}tR6374ServiceConfigTableEntry;
/****************************************************************************/
/*Trap Info */
typedef struct R6374NotifyInfo
{
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               u1TrapType;
    UINT1               au1Pad[2]; 
}tR6374NotifyInfo;

/****************************************************************************/
/* Delay Measurement Info */
/*1Dm Pdu related required info*/
typedef struct R6374Rx1DmPktInfo
{
   tR6374TSRepresentation   TxTimeStampf; 
           /* Carries the value of the
                                            * TimeStamp inserted in the
                                            * 1DM frame */
   tR6374TSRepresentation   RxTimeStampf; 
                                /* This field is reserved for
                                            * storing the RxTimeStampf for
                                            * 1DM receiving equipment.*/
}tR6374Rx1DmPktInfo;

/****************************************************************************/
/* Dmm Pkt related required info */
typedef struct R6374RxDmmPktInfo
{
   tR6374TSRepresentation   TxTimeStampf; 
   tR6374TSRepresentation   RxTimeStampf; 

}tR6374RxDmmPktInfo;
/****************************************************************************/
/* Dmr Pkt related required info */
typedef struct R6374RxDmrPktInfo
{
   tR6374TSRepresentation   TxTimeStampf;                            
   tR6374TSRepresentation   RxTimeStampf; 
   tR6374TSRepresentation   TxTimeStampb;
   tR6374TSRepresentation   RxTimeStampb;
}tR6374RxDmrPktInfo;
/****************************************************************************/
/* Lmm Pkt related required info */
typedef struct R6374Rx1LmPktInfo
{
   UINT4                      u4TxCounterf;
   UINT4                      u4RxCounterf;
}tR6374Rx1LmPktInfo;
/****************************************************************************/
/* Lmm Pkt related required info */
typedef struct R6374RxLmmPktInfo
{
   UINT4                      u4TxCounterf; 
   UINT4                      u4RxCounterf;
}tR6374RxLmmPktInfo;
/****************************************************************************/
/* Lmr Pkt related required info */
typedef struct R6374RxLmrPktInfo
{
   UINT4                      u4TxCounterf; /* A_Tx */
   UINT4                      u4RxCounterf; /* A_Rx */
   UINT4                      u4TxCounterb; /* B_Tx */
   UINT4                      u4RxCounterb; /* B_Rx */
}tR6374RxLmrPktInfo;

/****************************************************************************/
/* Combined LmDm Pkt related required info */
typedef struct R6374RxLmrDmrPktInfo
{
   tR6374RxLmrPktInfo LmrPktInfo;
   tR6374RxDmrPktInfo DmrPktInfo;
}tR6374RxLmrDmrPktInfo;

typedef struct R6374RxLmmDmmPktInfo
{
   tR6374RxLmmPktInfo LmmPktInfo;
   tR6374RxDmmPktInfo DmmPktInfo;
}tR6374RxLmmDmmPktInfo;

typedef struct R6374Rx1LmDmPktInfo
{
   tR6374Rx1LmPktInfo OneLmPktInfo;
   tR6374Rx1DmPktInfo OneDmPktInfo;
}tR6374Rx1LmDmPktInfo;

/****************************************************************************/
/* Delay Buffer Information */
typedef struct R6374DmBufferTableEntry
{
   tR6374TSRepresentation  MeasurementTimeStamp;                           
   tR6374TSRepresentation  PktDelayValue;
   tR6374TSRepresentation  PktRTDelayValue;
   tR6374TSRepresentation  InterPktDelayVariation;
   tR6374TSRepresentation  PktDelayVariation;
   tR6374SysTime           MeasurementTimeTaken;
   tR6374RBNodeEmbd        DmBufferNode;                     
   tR6374RBTree            PktDelayBuffer;    
                               /* RBTree for packet Delay rolling buffer 
            * indexed by TimeStamp */
   UINT4                   u4ContextId;   
   UINT4                   u4SessionId;
   UINT4                   u4SeqCount;    
   UINT4                   u4ChannelId1;
   UINT4                   u4ChannelId2;
   UINT4                   u4ChannelIpAddr1;
   UINT4                   u4ChannelIpAddr2;
   UINT4                   u4BufSeqId;
   UINT1                   au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
   UINT1                   u1Resultok;
   UINT1                   au1Pad[2];
}tR6374DmBufferTableEntry;
/****************************************************************************/
/* Delay Stats Information */
typedef struct R6374DmStatsTableEntry 
{
   tR6374TSRepresentation  MinPktDelayValue;
   tR6374TSRepresentation  MaxPktDelayValue;
   tR6374TSRepresentation  AvgPktDelayValue;
   tR6374TSRepresentation  MinPktRTDelayValue;
   tR6374TSRepresentation  MaxPktRTDelayValue;
   tR6374TSRepresentation  AvgPktRTDelayValue;
   tR6374TSRepresentation  MinInterPktDelayVariation;
   tR6374TSRepresentation  MaxInterPktDelayVariation;
   tR6374TSRepresentation  AvgInterPktDelayVariation;
   tR6374TSRepresentation  MinPktDelayVariation;
   tR6374TSRepresentation  MaxPktDelayVariation;
   tR6374TSRepresentation  AvgPktDelayVariation;
   tR6374RBNodeEmbd         DmStatsNode;
   tR6374SysTime            RxMeasurementStartTime;
   UINT4                    u4SessionId;
   UINT4                    u4RxSessionId;
   UINT4                    u4ContextId;   
   UINT4                    u4ChannelId1;
   UINT4                    u4ChannelId2;
   UINT4                    u4ChannelIpAddr1;
   UINT4                    u4ChannelIpAddr2;
   UINT4                    u4DMMSent;
   UINT4                    u4DMMRcvd;
   UINT4                    u4DMRRcvd;
   UINT4                    u41DMRcvd;
   UINT4                    u4NoOfErroredRespRcvd;
   UINT4                    u4PadSize;
                          /* To display in DM stats on which
                    * padding size the calculation was 
                    * happened */
   UINT1                    au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
   UINT1                    u1MplsPathType;
   UINT2                    u2DmAdjustedInterval;
   UINT1                    u1TrafficClass;
   UINT1                    u1DelayType;
   UINT1                    u1DelayMode;
   UINT1                    u1DelayTSFormat;
   UINT1                    u1MeasurementOngoing;
   UINT1                    u1SessType;
                                /* To identify whether session type is for 
      * independent DM or combined LmDm */
   UINT1                    u1DyadicMeasurement;
   UINT1                    u1DyadicProactiveRole;
   BOOL1                    b1IsDMBufferSQIEnabled;
   UINT1                    au1Pad[3];
}tR6374DmStatsTableEntry;
/****************************************************************************/
/* Loss Buffer Information */
typedef struct R6374LmBufferTableEntry 
{
   tR6374TSRepresentation   MeasurementTimeTaken;
   tR6374RBNodeEmbd         LmBufferNode;
   UINT4                    u4ContextId;   
   UINT4                    u4SessionId;
   UINT4                    u4SeqCount;    
   UINT4                    u4ChannelId1;
   UINT4                    u4ChannelId2;
   UINT4                    u4ChannelIpAddr1;
   UINT4                    u4ChannelIpAddr2;
   UINT4                    u4CalcTxLossSender;     
     /* Far End loss at Querier */
   UINT4                    u4CalcRxLossSender;     
      /* Near End loss at Querier */
   UINT4                    u4CalcRxLossReceiver;   
      /* Near End loss at Responder */
   UINT4                    u4BufSeqId;
   UINT4                    u4TxTestPktsCount;      /* No of packets transmitted
             by the test generator */
   UINT1                    au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
   UINT1                    u1Resultok;
   UINT1                    u1MplsPathType;
   UINT1                    au1Pad[1]; 
}tR6374LmBufferTableEntry;
/****************************************************************************/
/* Loss Stats Information */
typedef struct R6374LmStatsTableEntry 
{
   tR6374SysTime            RxMeasurementStartTime;
   tR6374RBNodeEmbd         LmStatsNode;
   FLT4                     f4ThroughPut;
   UINT4                    u4ContextId;   
   UINT4                    u4SessionId;
   UINT4                    u4RxSessionId;
   UINT4                    u4ChannelId1;
   UINT4                    u4ChannelId2;
   UINT4                    u4ChannelIpAddr1;
   UINT4                    u4ChannelIpAddr2;
   UINT4                    u4MinCalcTxLossSender;
   UINT4                    u4MaxCalcTxLossSender;
   UINT4                    u4AvgCalcTxLossSender;
   UINT4                    u4MinCalcRxLossSender;
   UINT4                    u4MaxCalcRxLossSender;
   UINT4                    u4AvgCalcRxLossSender;
   UINT4                    u4MinCalcRxLossReceiver;
   UINT4                    u4MaxCalcRxLossReceiver;
   UINT4                    u4AvgCalcRxLossReceiver;
   UINT4                    u4LMMSent;
   UINT4                    u4LMMRcvd;
   UINT4                    u4LMRRcvd;
   UINT4                    u41LMRcvd;
   UINT4                    u4NoOfErroredRespRcvd;
   UINT1                    au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
   UINT1                    u1MplsPathType;
   UINT2                    u2LmAdjustedInterval;
   UINT1                    u1TrafficClass;
   UINT1                    u1LossType;
   UINT1                    u1LossMethod;
   UINT1                    u1LossMode;
   UINT1                    u1LossTSFormat;
   UINT1                    u1MeasurementOngoing;
   UINT1                    u1SessType;
                                /* To identify whether session type is for 
      * independent LM or combined LmDm */
   UINT1                    u1DyadicMeasurement;
   UINT1                    u1DyadicProactiveRole;
   BOOL1                    b1IsLMBufferSQIEnabled;
   UINT1                    au1Pad[2];
}tR6374LmStatsTableEntry;
/****************************************************************************/
/*Used for storing information from PDU parsed by Channel Type DeMux */
typedef struct R6374PktSmInfo
{
   union
   { 
       tR6374RxDmmPktInfo    Dmm;
       tR6374RxDmrPktInfo    Dmr;
       tR6374Rx1DmPktInfo      OneDm; 
    /* Contains 1DM PDU (part of union
                                     * uPduInfo) info */
       tR6374RxLmmPktInfo    Lmm;
       tR6374RxLmrPktInfo    Lmr;
       tR6374Rx1LmPktInfo      OneLm; 
    /* Contain 1LM PDU (part of union
                                 * uPduInfo) info */
       tR6374RxLmmDmmPktInfo   LmmDmm;
       tR6374RxLmrDmrPktInfo   LmrDmr;
       tR6374Rx1LmDmPktInfo    OneLmDm; 
    /* Contain combined 1LMDM PDU (part of union
                                 * uPduInfo) info */
   } uPktInfo;
   tR6374ServiceConfigTableEntry *pServiceConfigInfo;
   tR6374MplsParams              *pMplsParams;
   tR6374BufChainHeader          *pBuf;
   UINT1                         u1PduType;
   UINT1                         au1Pad[3];    
} tR6374PktSmInfo;
/****************************************************************************/
/* Message to be queued in the message queue*/
typedef struct R6374QMsg
{
    tR6374BufChainHeader      * pR6374Buf; /*from MPLS-RTR*/
    tR6374MplsParams            R6374MplsParams;
    UINT4                       u4ContextId;
    UINT4                       u4IfIndex;    
    tR6374MsgType               u1MsgType;     
    UINT1                       au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1                       u1MplsTnlPwOperState;
    UINT1                       au1Pad[1];    
} tR6374QMsg;
/****************************************************************************/
typedef struct R6374ContextInfo{
    UINT4                  u4ContextId;
    UINT4                  u4TraceOption;
                              /* This variable will maintain a bitmap for trace
                               * options. If bit is set, this will represent
                               * option is enabled and if bit is not set then
                               * that particular trace option is disabled. */
    UINT1                  au1ContextName[VCM_ALIAS_MAX_LEN];
                              /* Context alias name */
    UINT1                  u1ModuleStatus;
                              /* This variable maintains RFC6374 module status of
                               * the context. */
    UINT1                  auPad[3];
}tR6374ContextInfo;
/****************************************************************************/
typedef struct R6374GlobalInfo {
    tTimerListId          TimerListId;            
                /* Timer List Identifier for 
                                                     RFC6374 Task */
    tOsixTaskId            TaskId;
    tOsixQId               MsgQId;
    tOsixSemId             SemId;
    tMemPoolId             MsgQPoolId;
    tR6374ContextInfo      aContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];
    tR6374RBTree           ServiceConfigTable;
    tR6374RBTree           ParamsConfigTable;
    tR6374RBTree           LmBufferTable;
    tR6374RBTree           LmStatsTable;
    tR6374RBTree           DmBufferTable;
    tR6374RBTree           DmStatsTable;
    tR6374RBTree           NotificationsTable;
    tTmrDesc               aTmrDesc[RFC6374_MAX_TMRS];
    UINT1                  au1TaskSemName[8];
    INT4                   i4SysLogId;
    UINT4                  u4Trc;
    UINT4                  u4TraceLevel;
    UINT4                  u4MemoryFailureCount;    
         /* Message Queue Memory 
                               * allocation failure count */
    UINT4                  u4BufferFailureCount;   
         /* Buffer allocation failure
                                                    * counter */
    UINT1                 *pu1Pdu;       
         /* Global pointer */
    UINT2                  u2TrapControl; 
    UINT1                  u1LmTotalSessionCount;  
         /* Loss Buffer can hold only
                                                    * 10 total Session Info */
    UINT1                  u1DmTotalSessionCount;  
         /* Delay Buffer can hold only
                                                    * 10 total Session Info */
    UINT1                  u1LmDmTotalSessionCount;  
         /* Combined Loss and Delay Buffer can hold only
                               * 10 total Session Info */
    UINT1                  u1TotalServices;
                /* Max services are 150*/
    UINT1                  u1ModuleStatus;
    UINT1                  u1ClearLM;
    UINT1                  u1ClearDM;
    UINT1                  u1SysSuppTimeFormat;
                              /* System supported time stamp format */
    UINT1                  au1Res[2];
    UINT1                  u1SystemControl[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT1                  au1Pad [(((SYS_DEF_MAX_NUM_CONTEXTS + 1)*3)+1)% 4];
}tR6374GlobalInfo;

typedef enum 
{
 RFC6374_SQI_RESET_STATE,
 RFC6374_SQI_INIT_STATE,
 RFC6374_SQI_ADJUSTED_STATE
}tR6374SessQueryIntervalState;
#endif /* __R6374TDFS_H */
/****************************************************************************
  End of File r6374tdfs.h
 ****************************************************************************/
