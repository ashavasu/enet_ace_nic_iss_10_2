/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374dmini.c,v 1.7 2017/07/31 13:40:18 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way
 *              and 2 way Delay Mesaurement of Control Sub Module.
 *******************************************************************/
#define __R6374DMINI_C__
#include "r6374inc.h"
PRIVATE INT4 R6374DmInitXmitDmmPdu PROTO ((tR6374PktSmInfo *));
PRIVATE VOID R6374DmInitFormatDmmPduHdr PROTO ((tR6374ServiceConfigTableEntry *,
                                                UINT1 **));
PRIVATE VOID R6374DmInitPutDmmInfo PROTO ((tR6374ServiceConfigTableEntry *,
                                           UINT1 **, tR6374TSRepresentation *));
PRIVATE INT4 R6374DmInitXmit1DmPdu PROTO ((tR6374PktSmInfo *));
PRIVATE VOID R6374DmInitFormat1DmPduHdr PROTO ((tR6374DmInfoTableEntry *,
                                                UINT1 **));
PRIVATE VOID R6374DmInitPut1DmInfo PROTO ((tR6374DmInfoTableEntry *, UINT1 **));
PRIVATE VOID R6374DmResetVar PROTO ((tR6374DmInfoTableEntry *));

/*******************************************************************************
 * Function           : R6374DmInitiator
 *
 * Description        : The routine implements the DM Initiator, it calls up
 *                      routine to format and transmit 1DM/DMM frame.
 *                      the various events.
 *
 * Input(s)           : tR6374PktSmInfo - Service Related Info.
 *                      u1EventID - Process Event 
 *
 * Output(s)          : None
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374DmInitiator (tR6374PktSmInfo * pPktSmInfo, UINT1 u1EventID)
{
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374NotifyInfo    R6374DmNotify;
    tR6374MacAddr       SwitchMacAddr;
    UINT4               u4TempSessionId = RFC6374_INIT_VAL;
    UINT4               u4RetVal = RFC6374_SUCCESS;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitiator() \r\n");

    RFC6374_MEMSET (&R6374DmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));
    RFC6374_MEMSET (&SwitchMacAddr, RFC6374_INIT_VAL, sizeof (tR6374MacAddr));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    /* Check for the event received */
    switch (u1EventID)
    {
        case RFC6374_DM_START_SESSION:

            /* Check if connection is ready before starting of DM */
            if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_READY)
            {
                pDmInfo->u1DmStatus = RFC6374_TX_STATUS_NOT_READY;
                /* Reset start session posted flag */
                pDmInfo->bIsDmStartSessPosted = RFC6374_FALSE;
                /* Send proactive restart trap incase of pro-active test 
                 * restarted by receiving LSP/PW up event */
                if (((pDmInfo->
                      b1DmTestStatus & RFC6374_PROACTIVE_TEST_RESTARTED) ==
                     RFC6374_PROACTIVE_TEST_RESTARTED)
                    && (pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE))
                {
                    /* Proactive Test re-start Trap */
                    R6374DmNotify.u1TrapType =
                        RFC6374_PROACTIVE_TEST_RESTART_TRAP;
                    RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                                     pServiceInfo->au1ServiceName,
                                     RFC6374_STRLEN (pServiceInfo->
                                                     au1ServiceName));
                    R6374SendNotification (&R6374DmNotify);

                    RFC6374_TRC1 (RFC6374_CRITICAL_TRC,
                                  RFC6374_DEFAULT_CONTEXT_ID,
                                  "Proactive Delay Measurement test "
                                  "restarted for Service name : %s \r\n",
                                  pServiceInfo->au1ServiceName);
#ifdef SYSLOG_WANTED
                    SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL,
                                  (UINT4) RFC6374_SYSLOG_ID,
                                  "Proactive Delay Measurement test restarted for Service name : %s"
                                  "\r\n", pServiceInfo->au1ServiceName));
#endif
                }
                /* Reset test restart status as false */
                pDmInfo->b1DmTestStatus &= ~RFC6374_PROACTIVE_TEST_RESTARTED;
            }
            else
            {
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID, " R6374DmInitiator:"
                             "Unable to Start DM Session, As Aleady one "
                             "Running.\r\n");
                u4RetVal = RFC6374_FAILURE;
                break;
            }

            /* Get Initial Session ID */
            if (pDmInfo->u4DmSessionId == RFC6374_INIT_VAL)
            {
                R6374CfaGetSysMacAddress (SwitchMacAddr);
                RFC6374_GET_SESSION_ID_FROM_MAC (SwitchMacAddr,
                                                 u4TempSessionId);
                R6374UtilGetSessId (NULL, NULL, pDmInfo, u4TempSessionId,
                                    RFC6374_VAL_2);
                RFC6374_DM_INCR_SESSION_ID (pDmInfo);
            }
            else
            {
                /* Increase Session ID/ transaction ID */
                R6374UtilIncrementSessionId (pServiceInfo, RFC6374_VAL_2);
            }

            /* As there can be only 10 Sesssion In Buffer,
             * Increase gobal Session Count for DM */
            RFC6374_DM_INCR_SESSION_COUNT ();

            /* Initiate Seq Counter */
            RFC6374_DM_RESET_SEQ_COUNT (pDmInfo);

            /* Initiate rolling buffer Seq Counter */
            RFC6374_DM_RESET_ROSEQ_COUNT (pDmInfo);

            /* Initiate Buffer Counter that adds in RB TRee */
            RFC6374_DM_RESET_BUFFER_ADD_COUNT (pDmInfo);

            /* Update the no of message to be send only for
             * on-demand transmission */
            if (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND)
            {
                /* Initialize Running Count */
                RFC6374_DM_SET_RUNNING_COUNT (pDmInfo);
            }

            /* Reset Proactive Transmission Count */
            RFC6374_DM_RESET_PROACTIVE_TX_COUNT (pDmInfo);

            /* Reset No Response Tx Count */
            RFC6374_DM_RESET_DM_NO_RESP_TX_COUNT (pDmInfo);

            /* Reset Packet Loss Value */
            RFC6374_DM_RESET_RX_PKT_LOSS (pDmInfo);

            /* Initiate SQI TLV . Ensure dyadicis disabled */
            if ((pServiceInfo->u1SessIntQueryStatus ==
                 CLI_RFC6374_SESS_INT_QUERY_ENABLE) &&
                (pServiceInfo->u1DyadicMeasurement !=
                 RFC6374_DYADIC_MEAS_ENABLE) &&
                (pDmInfo->u1DmType == RFC6374_DM_TYPE_TWO_WAY))
            {
                pServiceInfo->DmInfo.u1SessQueryIntervalState =
                    RFC6374_SQI_INIT_STATE;
            }

            /* On a Query Timer Expiry */
        case RFC6374_DM_INTERVAL_EXPIRY:

            /* Send 1DM/DMM */
            if (pDmInfo->u1DmType == RFC6374_DM_TYPE_TWO_WAY)
            {
                /* counter wil be reset to zero in DMR processing place
                 * whenever response PDU is received. If DMR is
                 * not received for more than QueryTransmitRetryCount
                 * number of DMM PDUs
                 * then transmission will be stopped */

                if (pDmInfo->u4DmNoRespTxCount ==
                    (UINT4) pServiceInfo->u1QueryTransmitRetryCount)
                {
                    /* Response Timeout Trap */
                    R6374DmNotify.u1TrapType = RFC6374_RESP_TIMEOUT_TRAP;
                    RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                                     pServiceInfo->au1ServiceName,
                                     RFC6374_STRLEN (pServiceInfo->
                                                     au1ServiceName));
                    R6374SendNotification (&R6374DmNotify);

                    RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC |
                                  RFC6374_ALL_FAILURE_TRC,
                                  RFC6374_DEFAULT_CONTEXT_ID,
                                  "R6374DmInitiator: "
                                  "u4DmNoRespTxCount :%d "
                                  "Stopping dm transaction "
                                  "as no of retries exceed\r\n",
                                  pDmInfo->u4DmNoRespTxCount);

                    /* Test is aborting because of no response */
                    pDmInfo->b1DmTestStatus |= RFC6374_TEST_ABORTED;

                    /* Stop the DM Transaction */
                    R6374DmInitStopDmTransaction (pServiceInfo);

                    /* Restart Test if mode is Proactive */
                    if ((pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
                        (pDmInfo->u1DmRespTimeOutReTries !=
                         RFC6374_RESP_TIMEOUT_RETRIES_MAX_COUNT))
                    {
                        /* Increment the Timeout Retires Counter */
                        RFC6374_DM_INCR_RESP_TIMEOUT_RETRIES_COUNT (pDmInfo);

                        /* Check if the Query interval is less 
                         * then 1Sec i.e default value */
                        if (pDmInfo->u2DmTimeInterval <
                            RFC6374_DM_DEFAULT_INTERVAL)
                        {
                            /* Start the Timer of DM Restart */
                            if (R6374TmrStartTmr
                                (RFC6374_DM_QUERY_TMR, pServiceInfo,
                                 ((UINT4)
                                  (RFC6374_VAL_1 *
                                   pDmInfo->u1DmRespTimeOutReTries))) !=
                                RFC6374_SUCCESS)
                            {
                                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                             RFC6374_ALL_FAILURE_TRC,
                                             RFC6374_DEFAULT_CONTEXT_ID,
                                             "R6374DmInitiator: "
                                             "Failure starting DM INTERVAL timer\r\n");
                                return RFC6374_FAILURE;
                            }
                        }
                        else    /* When Query interval timmer is more than a Sec */
                        {
                            /* Start the Timer of DM Restart */
                            if (R6374TmrStartTmr
                                (RFC6374_DM_QUERY_TMR, pServiceInfo,
                                 ((UINT4)
                                  (pDmInfo->u2DmTimeInterval *
                                   pDmInfo->u1DmRespTimeOutReTries))) !=
                                RFC6374_SUCCESS)
                            {
                                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                             RFC6374_ALL_FAILURE_TRC,
                                             RFC6374_DEFAULT_CONTEXT_ID,
                                             "R6374DmInitiator: "
                                             "Failure starting DM INTERVAL timer\r\n");
                                return RFC6374_FAILURE;
                            }
                        }
                    }
                    else        /* Reset the Max Retries Counter */
                    {
                        RFC6374_DM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pDmInfo);
                    }
                    break;
                }

                /* Call the routine to transmit the DMM Packet */
                if (R6374DmInitXmitDmmPdu (pPktSmInfo) != RFC6374_SUCCESS)

                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374DmInitiator: "
                                 "Unable to transmit DMM Pdu\r\n");
                    u4RetVal = RFC6374_FAILURE;
                    break;
                }
            }
            else
            {
                /* Call the routine to transmit the 1DM Packet */
                if (R6374DmInitXmit1DmPdu (pPktSmInfo) != RFC6374_SUCCESS)

                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374DmInitiator: "
                                 "Unable to transmit 1DM Pdu\r\n");
                    u4RetVal = RFC6374_FAILURE;
                    break;
                }
            }
            break;

        case RFC6374_DM_STOP_SESSION:

            /* Stop the DM Transaction */
            R6374DmInitStopDmTransaction (pServiceInfo);

            RFC6374_DM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pDmInfo);
            break;
        default:

            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374DmInitiator: "
                         "Event Not Supported\r\n");
            u4RetVal = RFC6374_FAILURE;

            break;
    }

    if (u4RetVal == RFC6374_FAILURE)
    {
        /* Update Result OK to False */
        pDmInfo->b1DmResultOk = RFC6374_FALSE;

        /* Test is aborting because of no response */
        pDmInfo->b1DmTestStatus |= RFC6374_TEST_ABORTED;

        /* Stop the DM Transaction */
        R6374DmInitStopDmTransaction (pServiceInfo);

        RFC6374_DM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pDmInfo);
    }
    else
    {
        /* Update Result OK to True */
        pDmInfo->b1DmResultOk = RFC6374_TRUE;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitiator() \r\n");
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : R6374DmInitXmitDmmPdu
 *
 * Description        : This routine formats and transmits the DMM PDU - Two way
 *
 * Input(s)           : tR6374PktSmInfo - Service Related Info 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PRIVATE INT4
R6374DmInitXmitDmmPdu (tR6374PktSmInfo * pPktSmInfo)
{
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374TSRepresentation TxTimeStampf;
    INT4                i4RetVal = RFC6374_INIT_VAL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT4               u4DmInterval = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1              *pu1DmmPduStart = NULL;
    UINT1              *pu1DmmPduEnd = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitXmitDmmPdu() \r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_PDUSM (pPktSmInfo);

    /*Check if Number Of Message to be transmitted have been reached */
    if ((pDmInfo->u2DmRunningCount == RFC6374_INIT_VAL) &&
        (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND))

    {

        if (pDmInfo->bDeadlineTimeCrossed == RFC6374_FALSE)
        {
            /* Wait for some extra time (constant for all services) in order
             * to get responses for all transmitted queries */
            pDmInfo->bDeadlineTimeCrossed = RFC6374_TRUE;

            if (R6374TmrStartTmr
                (RFC6374_DM_QUERY_TMR, pServiceInfo, R6374_MAX_DEADLINE_IN_MS)
                != RFC6374_SUCCESS)

            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374DmInitXmitDmmPdu: "
                             "Failure starting DM query timer\r\n");
                return RFC6374_FAILURE;
            }

            return RFC6374_SUCCESS;
        }
        /* Reset deadline flag */
        pDmInfo->bDeadlineTimeCrossed = RFC6374_FALSE;

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmitDmmPdu: "
                     "Stopping DM Transaction as total number of 1DM/DMM"
                     "have been transmitted\r\n");

        /* Reset test abort status as false */
        pDmInfo->b1DmTestStatus &= ~RFC6374_TEST_ABORTED;

        /*Stop DM Transaction */
        R6374DmInitStopDmTransaction (pServiceInfo);

        RFC6374_DM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pDmInfo);
        return RFC6374_SUCCESS;
    }

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmitDmmPdu: " "Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1DmmPduStart = RFC6374_PDU;
    pu1DmmPduEnd = RFC6374_PDU;

    /* Format the Dmm PDU header */
    R6374DmInitFormatDmmPduHdr (pServiceInfo, &pu1DmmPduEnd);

    /* Fill out the Information like TimeStamps into the DMM PDU */
    R6374DmInitPutDmmInfo (pServiceInfo, &pu1DmmPduEnd, &TxTimeStampf);

    u2PduLength = (UINT2) (pu1DmmPduEnd - pu1DmmPduStart);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1DmmPduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmitDmmPdu: "
                     "Buffer copy operation failed\r\n");

        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    u4DmInterval = pDmInfo->u2DmTimeInterval;

    /* Start DM Query Timer */
    if (R6374TmrStartTmr
        (RFC6374_DM_QUERY_TMR, pServiceInfo, u4DmInterval) != RFC6374_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmitDmmPdu: "
                     "Failure starting DM query timer\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Dump the packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374DmInitXmitDmmPdu: [Sending] Dumping two-way "
                      "DMM PDU\r\n");

    /* Transmit the DMM over MPLS Path */
    i4RetVal =
        R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_DELAY);
    if (i4RetVal != RFC6374_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmitDmmPdu: "
                     "DMM transmission to the lower layer failed\r\n");

        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Decrement the no of DMMs to be transmitted if
     * infinite transmission is not desired */
    if (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND)

    {
        /* Decrement the number of DM Message to be transmitted */
        RFC6374_DM_DECR_RUNNING_COUNT (pDmInfo);
    }

    /* Increment the no of DMM transmitted count if
     * Pro-Active transmission is desired */
    if (pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE)
    {
        RFC6374_DM_INCR_PROACTIVE_TX_COUNT (pDmInfo);
    }

    /* Increment DMM Out Count */
    RFC6374_INCR_STATS_DMM_OUT (pServiceInfo);

    /* Increment DM Tx count to stop the transmission 
     * when response is not received for more than 4 Pdu */

    if ((pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) ||
        (pDmInfo->u1DmType == RFC6374_DM_TYPE_TWO_WAY))
    {
        RFC6374_DM_INCR_DM_NO_RESP_TX_COUNT (pDmInfo);
    }

    RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374DmInitXmitDmmPdu: DMM transmission to the lower "
                  "layer success for Sess Id : %d Dmm Tx %d Running Count %d "
                  "No Response Tx Count %d \r\n", pDmInfo->u4DmSessionId,
                  pServiceInfo->Stats.u4StatsDmmOut, pDmInfo->u2DmRunningCount,
                  pDmInfo->u4DmNoRespTxCount);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitXmitDmmPdu() \r\n");

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374DmInitFormatDmmPduHdr
 *
 * Description        : This rotuine is used to fill the DMM PDU Header.
 *
 * Input              : pServiceInfo - Service Related Config Info
 *
 * Output(s)          : ppu1DmmPdu - Pointer to Pointer to the DMM PDU.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
R6374DmInitFormatDmmPduHdr (tR6374ServiceConfigTableEntry * pServiceInfo,
                            UINT1 **ppu1DmmPdu)
{
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    UINT1              *pu1DmmPdu = NULL;
    UINT2               u2PduLen = RFC6374_INIT_VAL;
    UINT1               u1QueryTF = RFC6374_INIT_VAL;
    UINT1               u1VerFlag = RFC6374_INIT_VAL;

    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitFormatDmmPduHdr() \r\n");

    pu1DmmPdu = *ppu1DmmPdu;

    /* Version and flag(R|T|0|0) */
    u1VerFlag = RFC6374_PDU_VERSION;
    RFC6374_PUT_1BYTE (pu1DmmPdu, u1VerFlag);

    /* Delay Measurement PDU Length (44) */
    u2PduLen = RFC6374_DM_PDU_MSG_LEN;
    /* Control Code */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* As for Dyadic Response is not sent so
         * Response not required */
        RFC6374_PUT_1BYTE (pu1DmmPdu, RFC6374_CTRL_CODE_RESP_NOT_REQ);
    }
    else
    {
        /* Dyadic is not enabled and measurement is start for two way */
        RFC6374_PUT_1BYTE (pu1DmmPdu, RFC6374_CTRL_CODE_RESP_INBAND_REQ);

        /* Delay Measurement PDU Length */
        if (pDmInfo->u4DmPadSize != RFC6374_INIT_VAL)
        {
            /* Delay Measurement PDU Length (44) +
             * Number of PadTlvCount * (padding tlv type (1) + tlv length (1)) +
             * padsize (variable bytes)  */
            u2PduLen = (UINT2) (u2PduLen +
                                (pDmInfo->u1DmPadTlvCount * RFC6374_VAL_2) +
                                (UINT2) (pDmInfo->u4DmPadSize));
        }
    }

    if (pDmInfo->u1SessQueryIntervalState > RFC6374_SQI_RESET_STATE)
    {
        u2PduLen = (UINT2) (u2PduLen + RFC6374_SESS_QUERY_INT_TLV_LEN);
    }

    RFC6374_PUT_2BYTE (pu1DmmPdu, u2PduLen);

    /* Querier and responder timestamp  */
    u1QueryTF = pDmInfo->u1DmQTSFormat;
    u1QueryTF = (UINT1) (u1QueryTF << RFC6374_VAL_4);

    RFC6374_PUT_1BYTE (pu1DmmPdu, u1QueryTF);

    RFC6374_PUT_1BYTE (pu1DmmPdu, 0);

    /* Reserved Bits */
    RFC6374_PUT_2BYTE (pu1DmmPdu, 0);

    /* Session Idntifier and DS Traffic Class */
    RFC6374_PUT_4BYTE (pu1DmmPdu, pDmInfo->u4DmSessionId);

    *ppu1DmmPdu = pu1DmmPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitFormatDmmPduHdr() \r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374DmInitPutDmmInfo
 *
 * Description        : This routine is used to fill the timestamp in the DMM 
 *                      PDU.
 *
 * Input(s)           : pServiceInfo - Service Related Config Info
 *
 * Output(s)          : pTxTimeStampf - Pointer to the TxTimeStampf filled in
 *                      the DMM pdu.
 *                      ppu1DmmPdu - Pointer to pointer to the DMM Pdu.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
R6374DmInitPutDmmInfo (tR6374ServiceConfigTableEntry * pServiceInfo,
                       UINT1 **ppu1DmmPdu,
                       tR6374TSRepresentation * pTxTimeStampf)
{
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    UINT1              *pu1DmmPdu = NULL;
    UINT4               u4TimeInterval = RFC6374_INIT_VAL;

    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitPutDmmInfo() \r\n");

    pu1DmmPdu = *ppu1DmmPdu;

    /* Call the API to get the Current TimeStamp */
    R6374UtilGetCurrentTime (pTxTimeStampf, pDmInfo->u1DmQTSFormat);

    /* If Dyadic is enabled then TimeStamp 1, TimeStamp 3 and
     * TimeStamp 4 has to be sent for other node calculation */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* A_Tx Time Stamp */
        RFC6374_PUT_4BYTE (pu1DmmPdu, pTxTimeStampf->u4Seconds);
        RFC6374_PUT_4BYTE (pu1DmmPdu, pTxTimeStampf->u4NanoSeconds);

        /* B_Rx Time Stamp */
        RFC6374_PUT_4BYTE (pu1DmmPdu, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1DmmPdu, RFC6374_INIT_VAL);

        /* A_TxP Time Stamp of previous Query */
        RFC6374_PUT_4BYTE (pu1DmmPdu, pDmInfo->TxTimeStampf.u4Seconds);
        RFC6374_PUT_4BYTE (pu1DmmPdu, pDmInfo->TxTimeStampf.u4NanoSeconds);

        /* B_RxP Time Stamp of previous Query */
        RFC6374_PUT_4BYTE (pu1DmmPdu, pDmInfo->RxTimeStampf.u4Seconds);
        RFC6374_PUT_4BYTE (pu1DmmPdu, pDmInfo->RxTimeStampf.u4NanoSeconds);
    }
    else
    {
        /* Fill the 4 bytes with Time Units in Seconds */
        RFC6374_PUT_4BYTE (pu1DmmPdu, pTxTimeStampf->u4Seconds);

        /* Fill the next 4 bytes with Time Units in nano Seconds */
        RFC6374_PUT_4BYTE (pu1DmmPdu, pTxTimeStampf->u4NanoSeconds);

        /* Fill the next 24  bytes with ZERO as it will be used by
         * the end receving DMM/DMR frame
         */
        RFC6374_MEMSET (pu1DmmPdu, RFC6374_PDU_RESERVED_FIELD,
                        RFC6374_DMM_RESERVED_SIZE);
        pu1DmmPdu = pu1DmmPdu + RFC6374_DMM_RESERVED_SIZE;

        /* Check and Add SQI TLV */

        if (pServiceInfo->DmInfo.u1SessQueryIntervalState ==
            RFC6374_SQI_INIT_STATE)
        {
            /* SQI TLV with zero value need to be appended to the buffer */
            RFC6374_PUT_1BYTE (pu1DmmPdu, RFC6374_SQI_TLV_TYPE);
            RFC6374_PUT_1BYTE (pu1DmmPdu, RFC6374_SQI_TLV_LENGTH);
            RFC6374_PUT_4BYTE (pu1DmmPdu, RFC6374_INIT_VAL);

        }
        else if (pServiceInfo->DmInfo.u1SessQueryIntervalState ==
                 RFC6374_SQI_ADJUSTED_STATE)
        {
            /* SQI TLV with adjusted value need to be appended to the buffer */
            RFC6374_PUT_1BYTE (pu1DmmPdu, RFC6374_SQI_TLV_TYPE);
            RFC6374_PUT_1BYTE (pu1DmmPdu, RFC6374_SQI_TLV_LENGTH);
            /* Get and fill the adjusted value from database */
            u4TimeInterval = (UINT4) pServiceInfo->DmInfo.u2DmTimeInterval;
            RFC6374_PUT_4BYTE (pu1DmmPdu, u4TimeInterval);
        }
        if (pDmInfo->u4DmPadSize != RFC6374_INIT_VAL)
        {
            R6374DmTxPutPadTlv (&pu1DmmPdu, pDmInfo->u4DmPadSize);
        }
    }

    *ppu1DmmPdu = pu1DmmPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitPutDmmInfo() \r\n");
    return;

}

/*****************************************************************************
 * Function           : R6374DmInitAddDmEntry
 *
 * Description        : This routine is used to add the entry of the transmitted
 *                      DMM frame and of received DMR frame into the frame delay
 *                      buffer.
 *
 * Input(s)           : pServiceInfo - Pointer to the structure that stores the
 *                      information regarding Service
 *
 * Output(s)          : None
 *
 * Returns            : tR6374DmBufferTableEntry * - Pointer to the node added
 ******************************************************************************/
PUBLIC tR6374DmBufferTableEntry *
R6374DmInitAddDmEntry (tR6374ServiceConfigTableEntry * pServiceInfo,
                       UINT4 u4DmSessionId, UINT1 u1MeasurementType)
{
    tR6374DmBufferTableEntry *pPktDelyBuffNode = NULL;
    tR6374DmBufferTableEntry *pFirstPktDelyBuffEntry = NULL;
    tR6374DmBufferTableEntry TempDelayBuffEntry;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    UINT4               u4SeqCount = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitAddDmEntry() \r\n");

    RFC6374_MEMSET (&TempDelayBuffEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /* Sequence counter to be incremented in Dm Structure only 
     * if DM is initiated */
    /* Increase Seq Count */
    if (u1MeasurementType == RFC6374_DM_TYPE_ONE_WAY)
    {
        RFC6374_DM_INCR_RX_SEQ_COUNT (pDmInfo);
    }
    else if (u1MeasurementType == RFC6374_DM_TYPE_TWO_WAY)
    {
        RFC6374_DM_INCR_SEQ_COUNT (pDmInfo);
    }
    /* Allocate a memory block for packet delay buffer node */
    RFC6374_ALLOC_MEM_BLOCK_PD_BUFF_TABLE (pPktDelyBuffNode);

    while (pPktDelyBuffNode == NULL)
    {
        /* Memory allocation failed, so now check if the memory pool is
         * exhausted */

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitAddDmEntry: "
                     "failure allocating memory for frame delay buffer node\r\n");

        if (RFC6374_GET_FREE_MEM_UNITS (RFC6374_DMBUFFERTABLE_POOLID) !=
            RFC6374_INIT_VAL)
        {
            /* memory failure has occured even when we have free
             * memory in the pool */

            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmInitAddDmEntry: "
                         "Packet delay memory pool corruption\r\n");

            RFC6374_INCR_MEMORY_FAILURE_COUNT ();
            return NULL;
        }

        /* Memory pool is exhausted for sure, now we can delete the first entry
         * in the pool to have some free memory */

        pFirstPktDelyBuffEntry = (tR6374DmBufferTableEntry *)
            RBTreeGetFirst (RFC6374_DMBUFFER_TABLE);

        if (pFirstPktDelyBuffEntry == NULL)
        {
            /* Memory pool is exhausted and we dont have anyting in the 
             * packet delay table to free.*/

            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmInitAddDmEntry: "
                         "Frame delay buffer table corruption \r\n");
            return NULL;
        }

        /* Remove the first node from the table */
        RBTreeRem (RFC6374_DMBUFFER_TABLE, pFirstPktDelyBuffEntry);

        /* Free the memory for the node */
        RFC6374_FREE_MEM_BLOCK (RFC6374_DMBUFFERTABLE_POOLID,
                                (UINT1 *) pFirstPktDelyBuffEntry);

        pFirstPktDelyBuffEntry = NULL;

        /* Now we can again try for allocating memory */
        RFC6374_ALLOC_MEM_BLOCK_PD_BUFF_TABLE (pPktDelyBuffNode);
    }

    RFC6374_MEMSET (pPktDelyBuffNode, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    /* Config Params */
    pPktDelyBuffNode->u4ContextId = pServiceInfo->u4ContextId;

    RFC6374_MEMCPY (pPktDelyBuffNode->au1ServiceName,
                    pServiceInfo->au1ServiceName,
                    RFC6374_STRLEN (pServiceInfo->au1ServiceName));

    if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        /* PW ID (VcId) */
        pPktDelyBuffNode->u4ChannelId1 =
            pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId;
        /* PW Peer IP Address */
        pPktDelyBuffNode->u4ChannelIpAddr1 =
            pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr;
    }
    else
    {
        /* LSP Forwards ID */
        pPktDelyBuffNode->u4ChannelId1 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4FrwdTunnelId;
        /* LSP Reverse ID */
        pPktDelyBuffNode->u4ChannelId2 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4RevTunnelId;
        /* LSP Source Address */
        pPktDelyBuffNode->u4ChannelIpAddr1 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;
        /* LSP Destination Address */
        pPktDelyBuffNode->u4ChannelIpAddr2 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
    }

    /* Session Params */
    pPktDelyBuffNode->u4SessionId = u4DmSessionId;

    if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
    {
        pPktDelyBuffNode->u4SeqCount = pLmDmInfo->u4RxLmDmSeqCount;
        pPktDelyBuffNode->u4BufSeqId = pLmDmInfo->u4RxLmDmSeqCount;
        u4SeqCount = pLmDmInfo->u4RxDmResetSeqCount;
    }
    else if (u1MeasurementType == RFC6374_DM_TYPE_ONE_WAY)
    {
        pPktDelyBuffNode->u4SeqCount = pDmInfo->u4RxDmSeqCount;
        pPktDelyBuffNode->u4BufSeqId = pDmInfo->u4RxDmSeqCount;
        u4SeqCount = pDmInfo->u4RxDmResetSeqCount;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            pPktDelyBuffNode->u4SeqCount = pLmDmInfo->u4LmDmSeqCount;
            pPktDelyBuffNode->u4BufSeqId = pLmDmInfo->u4LmDmSeqCount;
            u4SeqCount = pLmDmInfo->u4DmResetSeqCount;
        }
        else
        {
            pPktDelyBuffNode->u4SeqCount = pDmInfo->u4DmSeqCount;
            pPktDelyBuffNode->u4BufSeqId = pDmInfo->u4DmSeqCount;
            u4SeqCount = pDmInfo->u4DmResetSeqCount;
        }
    }

    RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374DmInitAddDmEntry: Service name : %s Sess Id : %d "
                  "Seq Count : %d Rolling Buffer Reset Seq Count : %d \r\n",
                  pServiceInfo->au1ServiceName, u4DmSessionId,
                  pPktDelyBuffNode->u4SeqCount, u4SeqCount);

    /* R6374_RB_BUFFER_MAX_COUNT to 5 */
    if (pPktDelyBuffNode->u4SeqCount <= RFC6374_RB_BUFFER_MAX_COUNT)
    {
        /* Add the node into the table */
        if (RBTreeAdd (RFC6374_DMBUFFER_TABLE, pPktDelyBuffNode) !=
            RFC6374_RB_SUCCESS)

        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmInitAddDmEntry: "
                         "failure adding node into the frame delay table \r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_DMBUFFERTABLE_POOLID,
                                    (UINT1 *) pPktDelyBuffNode);
            return NULL;
        }
        /* Increase Buffer Add Count in RB Tree */
        RFC6374_DM_INCR_BUFFER_ADD_COUNT (pDmInfo);

        if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            RFC6374_LMDM_RESET_ONEWAY_DM_ROSEQ_COUNT (pLmDmInfo);
        }
        else if (u1MeasurementType == RFC6374_LM_TYPE_ONE_WAY)
        {
            RFC6374_DM_RESET_RX_ROSEQ_COUNT (pDmInfo);
        }
    }
    else
    {
        /* As Count Has reached 5 per session entry,Now remove the 1st Node
         *  from Buffer Particular Service Name Current Session ID */

        TempDelayBuffEntry.u4SessionId = u4DmSessionId;

        /* Default Context ID */
        TempDelayBuffEntry.u4ContextId = pServiceInfo->u4ContextId;

        /* As we have to remove first node, So get 1st entry always,
         * after Buffer add count is 5 */
        if (u4SeqCount == RFC6374_RB_BUFFER_MAX_COUNT)
        {
            /* If count is reached 5 reset to 0 */
            u4SeqCount = RFC6374_RESET;
        }

        TempDelayBuffEntry.u4SeqCount = ++u4SeqCount;

        /* Service Name on which Session is Running */
        RFC6374_MEMCPY (TempDelayBuffEntry.au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        RFC6374_STRLEN (pServiceInfo->au1ServiceName));

        pFirstPktDelyBuffEntry = (tR6374DmBufferTableEntry *)
            RBTreeGet (RFC6374_DMBUFFER_TABLE,
                       (tRBElem *) & TempDelayBuffEntry);

        if (pFirstPktDelyBuffEntry == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374DmInitAddDmEntry: "
                         " Node for Seq Count 1 Not found for particular service"
                         ", session ID\r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_DMBUFFERTABLE_POOLID,
                                    (UINT1 *) pPktDelyBuffNode);
            return NULL;
        }
        /* Changes Seq Count of next Buffer node to removed seq Count */
        pPktDelyBuffNode->u4SeqCount = u4SeqCount;

        if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            pLmDmInfo->u4RxDmResetSeqCount = u4SeqCount;
        }
        else if (u1MeasurementType == RFC6374_LM_TYPE_ONE_WAY)
        {
            pDmInfo->u4RxDmResetSeqCount = u4SeqCount;
        }
        else
        {
            if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
            {
                pLmDmInfo->u4DmResetSeqCount = u4SeqCount;
            }
            else
            {
                pDmInfo->u4DmResetSeqCount = u4SeqCount;
            }
        }

        /* Copy the RB Node */
        RFC6374_MEMCPY (&pPktDelyBuffNode->DmBufferNode,
                        &pFirstPktDelyBuffEntry->DmBufferNode,
                        sizeof (tR6374RBNodeEmbd));

        RFC6374_MEMSET (pFirstPktDelyBuffEntry, RFC6374_INIT_VAL,
                        sizeof (tR6374DmBufferTableEntry));

        RFC6374_MEMCPY (pFirstPktDelyBuffEntry, pPktDelyBuffNode,
                        sizeof (tR6374DmBufferTableEntry));

        RFC6374_FREE_MEM_BLOCK (RFC6374_DMBUFFERTABLE_POOLID,
                                (UINT1 *) pPktDelyBuffNode);

        return pFirstPktDelyBuffEntry;
    }

    /* Store the current DM type (1DM/DMM) into the frame delay buffer */
    /*pPktDelyBuffNode->u1DelayType = pServiceInfo->DmInfo.u1DmType; */

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitAddDmEntry \r\n");

    return pPktDelyBuffNode;
}

/*******************************************************************************
 * Function Name      : R6374DmInitStopDmTransaction
 *
 * Description        : This routine is used to stop the on going DM transaction
 *
 * Input(s)           : pPktSmInfo - pointer to the Service Related Config 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
R6374DmInitStopDmTransaction (tR6374ServiceConfigTableEntry * pServiceInfo)
{
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374DmStatsTableEntry *pDmStatsTableInfo = NULL;
    tR6374NotifyInfo    R6374DmNotify;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitStopDmTransaction \r\n");

    RFC6374_MEMSET (&R6374DmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    /* Stop DM Query Timer */
    R6374TmrStopTmr (RFC6374_DM_QUERY_TMR, pServiceInfo);

    if (pDmInfo->u1DmType == RFC6374_DM_TYPE_TWO_WAY)
    {
        /* Set Measurement Status as Competed
         * Calculate if any delay due to timeoue */
        pDmStatsTableInfo = R6374UtilGetDmStatsTableInfo (pServiceInfo,
                                                          pDmInfo->
                                                          u4DmSessionId);
        if (pDmStatsTableInfo == NULL)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374DmInitStopDmTransaction: Get DM Stats "
                          "failed for session Id %d \r\n",
                          pDmInfo->u4DmSessionId);
        }
        else
        {
            if ((pDmInfo->b1DmTestStatus & RFC6374_TEST_ABORTED) ==
                RFC6374_TEST_ABORTED)
            {
                /* Measurement is Aborted by user or due to Path down */
                pDmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_ABORTED;
            }
            else
            {
                /* Measurement Completed */
                pDmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_COMPLETE;
            }

            if (pDmStatsTableInfo->u1DelayMode == RFC6374_DM_MODE_ONDEMAND)
            {
                /* Actual DMM Sent */
                pDmStatsTableInfo->u4DMMSent =
                    (UINT4) (pDmInfo->u2DmNoOfMessages -
                             pDmInfo->u2DmRunningCount);
            }

            /* Actual DM recevied */
            if ((pDmStatsTableInfo->u1DyadicMeasurement ==
                 RFC6374_DYADIC_MEAS_ENABLE)
                && pDmInfo->b1DmInitResponder == RFC6374_TRUE)
            {
                /* Add one is done if the responder has initiated the DM */
                pDmStatsTableInfo->u4DMMRcvd = pDmStatsTableInfo->u4DMMRcvd + 1;
            }

            /* No. of Error Packet Recevied */
            pDmStatsTableInfo->u4NoOfErroredRespRcvd =
                pDmInfo->u4RxDmPacketLoss;
        }
    }

    /* Decrement Global DM Running Count */
    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        RFC6374_DM_DECR_SESSION_COUNT ();

        /* Send Test Abort trap for the below cases
         * 1. Test aborted by Administrator
         * 2. LSP/PW down : On-Demand - Based on Running count  
         *                  proactive - Based on Previous mode when
         mode is changed from on-demand
         to proactive to stop the test
         - Based on DM mode when test is
         aborted due to LSP/PW down*/

        if (((pDmInfo->b1DmTestStatus & RFC6374_TEST_ABORTED) ==
             RFC6374_TEST_ABORTED) ||
            (pDmInfo->u2DmRunningCount != RFC6374_INIT_VAL) ||
            (pDmInfo->u1PrevDmMode == RFC6374_DM_MODE_PROACTIVE) ||
            (pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE))
        {
            /* Test Abort Trap */
            R6374DmNotify.u1TrapType = RFC6374_TEST_ABORT_TRAP;
            RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                             pServiceInfo->au1ServiceName,
                             RFC6374_STRLEN (pServiceInfo->au1ServiceName));
            R6374SendNotification (&R6374DmNotify);

            RFC6374_TRC2 (RFC6374_CRITICAL_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                          "Delay Measurement test aborted for Service name : %s "
                          "Session Id : %d \r\n", pServiceInfo->au1ServiceName,
                          pDmInfo->u4DmSessionId);
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) RFC6374_SYSLOG_ID,
                          "Delay Measurement test aborted for Service name : %s Session Id : %d"
                          "\r\n", pServiceInfo->au1ServiceName,
                          pDmInfo->u4DmSessionId));
#endif

            if (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND)
            {
                pDmInfo->u1PrevDmMode = RFC6374_DM_MODE_ONDEMAND;
            }
        }

        /* Reset test abort status as false */
        pDmInfo->b1DmTestStatus &= ~RFC6374_TEST_ABORTED;
    }

    /* Revert Back the Tx Status */
    pDmInfo->u1DmStatus = RFC6374_TX_STATUS_READY;

    /* Reset Responder Initiated to False */
    pDmInfo->b1DmInitResponder = RFC6374_FALSE;

    /* Initiate Seq Counter */
    RFC6374_DM_RESET_SEQ_COUNT (pDmInfo);

    /* Reset Values */
    R6374DmResetVar (pDmInfo);

    /* Reset loss amd tx count */
    RFC6374_DM_RESET_PACKETLOSS (pDmInfo);
    RFC6374_DM_RESET_RESP_TX_COUNT (pDmInfo);

    /* Reset SQI TLV */
    if (pServiceInfo->u1SessIntQueryStatus == CLI_RFC6374_SESS_INT_QUERY_ENABLE)
    {
        pServiceInfo->DmInfo.u1SessQueryIntervalState = RFC6374_SQI_RESET_STATE;
        /* Restore the adjusted interval with Configured interval */
        pServiceInfo->DmInfo.u2DmTimeInterval =
            pServiceInfo->DmInfo.u2DmConfiguredTimeInterval;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitStopDmTransaction \r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374DmInitXmit1DmPdu
 *
 * Description        : This routine formats and transmits the 1DM PDU.
 *
 * Input(s)           : pPktSmInfo - Pointer the Service Related Config 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PRIVATE INT4
R6374DmInitXmit1DmPdu (tR6374PktSmInfo * pPktSmInfo)
{
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1              *pu1Dm1wPduStart = NULL;
    UINT1              *pu1Dm1wPduEnd = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitXmit1DmPdu\r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    /*Check if Number Of Message to be transmitted have been reached */
    if ((pDmInfo->u2DmRunningCount == RFC6374_INIT_VAL) &&
        (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND))

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     " R6374DmInitXmit1DmPdu: "
                     " Stopping DM Transaction as total number of 1DM"
                     " have been transmitted\r\n");

        /*Stop 1DM Transaction */
        R6374DmInitStopDmTransaction (pServiceInfo);

        return RFC6374_SUCCESS;
    }

    /* Allocate CRU Buffer */

    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmit1DmPdu: " "Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1Dm1wPduStart = RFC6374_PDU;
    pu1Dm1wPduEnd = RFC6374_PDU;

    /* Format the  1Dm PDU header */
    R6374DmInitFormat1DmPduHdr (pDmInfo, &pu1Dm1wPduEnd);

    /* Fill out the Information like TimeStamps into the 1DM PDU */
    R6374DmInitPut1DmInfo (pDmInfo, &pu1Dm1wPduEnd);
    u2PduLength = (UINT2) (pu1Dm1wPduEnd - pu1Dm1wPduStart);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1Dm1wPduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmit1DmPdu: "
                     "Buffer copy operation failed\r\n");

        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Start DM query Timer */
    if (R6374TmrStartTmr (RFC6374_DM_QUERY_TMR, pServiceInfo,
                          (pDmInfo->u2DmTimeInterval)) != RFC6374_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmit1DmPdu: "
                     "Failure starting DM While timer\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Dump the Packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374DmInitXmit1DmPdu: [Sending] Dumping one-way"
                      " DMM PDU\r\n");

    /* Transmit the DMM over MPLS Path */
    i4RetVal =
        R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_DELAY);
    if (i4RetVal != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmInitXmit1DmPdu: "
                     "1DM transmission to the lower layer failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Decrement the no of DMMs to be transmitted if
     * infinite transmission is not desired */
    if (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND)

    {
        /* Decrement the number of DM Message to be transmitted */
        RFC6374_DM_DECR_RUNNING_COUNT (pDmInfo);
    }

    /* Increment 1DM OUT */
    RFC6374_INCR_STATS_1DM_OUT (pServiceInfo);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitXmit1DmPdu\r\n");

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374DmInitFormat1DmPduHdr
 *
 * Description        : This rotuine is used to fill the 1DM CFM PDU Header.
 *
 * Input              : pDmInfo - Pointer to the DM structure
 *
 * Output(s)          : ppu1Dm1wPdu - Pointer to Pointer to the 1DM PDU.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
R6374DmInitFormat1DmPduHdr (tR6374DmInfoTableEntry * pDmInfo,
                            UINT1 **ppu1Dm1wPdu)
{
    UINT1              *pu1Dm1wPdu = NULL;
    UINT1               u1QueryTF = RFC6374_INIT_VAL;
    UINT1               u1VerFlag = RFC6374_INIT_VAL;
    UINT2               u2PduLen = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitFormat1DmPduHdr\r\n");

    pu1Dm1wPdu = *ppu1Dm1wPdu;

    /* Version and flag(R|T|0|0) */
    u1VerFlag = RFC6374_PDU_VERSION;    /* Need to set as 0 */
    RFC6374_PUT_1BYTE (pu1Dm1wPdu, u1VerFlag);

    /* Control Code */
    RFC6374_PUT_1BYTE (pu1Dm1wPdu, RFC6374_CTRL_CODE_RESP_NOT_REQ);

    /* Delay Measurement PDU Length */
    if (pDmInfo->u4DmPadSize != RFC6374_INIT_VAL)
    {
        /* Delay Measurement PDU Length (44) +
         * Number of PadTlvCount * (padding tlv type (1) + tlv length (1)) +
         * padsize (variable bytes)  */
        u2PduLen = (UINT2) (RFC6374_DM_PDU_MSG_LEN +
                            (pDmInfo->u1DmPadTlvCount * RFC6374_VAL_2) +
                            (UINT2) (pDmInfo->u4DmPadSize));
    }
    else
    {
        u2PduLen = RFC6374_DM_PDU_MSG_LEN;
    }

    RFC6374_PUT_2BYTE (pu1Dm1wPdu, u2PduLen);

    /* Querier and responder timestamp  */
    u1QueryTF = pDmInfo->u1DmQTSFormat;
    u1QueryTF = (UINT1) (u1QueryTF << RFC6374_VAL_4);

    RFC6374_PUT_1BYTE (pu1Dm1wPdu, u1QueryTF);
    RFC6374_PUT_1BYTE (pu1Dm1wPdu, 0);

    /* Reserved Bits  */
    RFC6374_PUT_2BYTE (pu1Dm1wPdu, 0);

    /* Session Identifier */
    RFC6374_PUT_4BYTE (pu1Dm1wPdu, pDmInfo->u4DmSessionId);
    *ppu1Dm1wPdu = pu1Dm1wPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitFormat1DmPduHdr\r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374DmInitPut1DmInfo
 *
 * Description        : This routine is used to fill the timestamp in the 1DM PDU.
 *
 * Input              : pDmInfo - Pointer to the DM structure
 *
 * Output(s)          : ppu1Dm1wPdu - Pointer to pointer to the 1DM Pdu..
 *
 * Return Value(s)    : None
 ******************************************************************************/
PRIVATE VOID
R6374DmInitPut1DmInfo (tR6374DmInfoTableEntry * pDmInfo, UINT1 **ppu1Dm1wPdu)
{
    tR6374TSRepresentation TxTimeStampf;
    UINT1              *pu1Dm1wPdu = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitPut1DmInfo\r\n");

    pu1Dm1wPdu = *ppu1Dm1wPdu;

    /* Call the API to get the Current TimeStamp */
    R6374UtilGetCurrentTime (&TxTimeStampf, pDmInfo->u1DmQTSFormat);

    /* Fill the 4 bytes with Time Units in Seconds */
    RFC6374_PUT_4BYTE (pu1Dm1wPdu, TxTimeStampf.u4Seconds);

    /* Fill the next 4 bytes with Time Units in nano Seconds */
    RFC6374_PUT_4BYTE (pu1Dm1wPdu, TxTimeStampf.u4NanoSeconds);

    /* Fill the next 8 bytes with ZERO */
    RFC6374_MEMSET (pu1Dm1wPdu, RFC6374_PDU_RESERVED_FIELD,
                    RFC6374_1DM_RESERVED_SIZE);
    pu1Dm1wPdu = pu1Dm1wPdu + RFC6374_1DM_RESERVED_SIZE;

    if (pDmInfo->u4DmPadSize != RFC6374_INIT_VAL)
    {
        R6374DmTxPutPadTlv (&pu1Dm1wPdu, pDmInfo->u4DmPadSize);
    }

    *ppu1Dm1wPdu = pu1Dm1wPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitPut1DmInfo\r\n");
    return;
}

/****************************************************************************
 * Function Name      : R6374DmResetVar                                     *
 * Description        : This rotuine is used to reset the Tx and Rx         *
 *             Timestamp values                                    *
 * Input(s)           : pDmInfo - Pointer to Dm Structure                   *
 * Output(s)          : None                                                *
 * Return Value(s)    : None                                                *
 ****************************************************************************/
PRIVATE VOID
R6374DmResetVar (tR6374DmInfoTableEntry * pDmInfo)
{
    RFC6374_MEMSET (&(pDmInfo->TxTimeStampf), RFC6374_RESET,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&(pDmInfo->RxTimeStampf), RFC6374_RESET,
                    sizeof (tR6374TSRepresentation));
}

/*****************************************************************************
 * Function Name      : R6374DmTxPutPadTlv                                     *
 * Description        : This rotuine is used to fill the PAD TLVs in the PDU *
 * Input(s)           : ppu1PduEnd - Pointer to the Pdu                      *
 *                      u4PadSize  - Pad Size                                *
 * Output(s)          : None                                                 *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R6374DmTxPutPadTlv (UINT1 **ppu1PduEnd, UINT4 u4PadSize)
{

    UINT1              *pu1Pdu = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmTxPutPadTlv\r\n");

    pu1Pdu = *ppu1PduEnd;

    /* Fill the Padding TLV based on the Pad Size */
    do
    {
        /* Padding TLV Type */
        RFC6374_PUT_1BYTE (pu1Pdu, RFC6374_PADDING_TLV_COPY_IN_RESP);

        if (u4PadSize > RFC6374_MAX_PAD_SIZE_SINGLE_TLV)
        {
            /* Padding TLV length 
             * The Length field indicates the size in bytes of the 
             * Value field*/
            RFC6374_PUT_1BYTE (pu1Pdu, RFC6374_MAX_PAD_SIZE_SINGLE_TLV);

            /* Value field. Currently value zero is updated
             * as padding pattern for the specified padding size*/
            MEMSET (pu1Pdu, 0, RFC6374_MAX_PAD_SIZE_SINGLE_TLV);
            pu1Pdu += RFC6374_MAX_PAD_SIZE_SINGLE_TLV;

            u4PadSize = u4PadSize - RFC6374_MAX_PAD_SIZE_SINGLE_TLV;
        }
        else
        {
            /* Padding TLV length 
             * The Length field indicates the size in bytes of the 
             * Value field*/
            RFC6374_PUT_1BYTE (pu1Pdu, (UINT1) u4PadSize);
            /* Value field. Currently value zero is updated
             * as padding pattern for the specified padding size*/
            MEMSET (pu1Pdu, 0, (UINT1) u4PadSize);
            pu1Pdu += (UINT1) u4PadSize;
            break;
        }
    }
    while (u4PadSize > RFC6374_INIT_VAL);

    *ppu1PduEnd = pu1Pdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmTTxxPutPadTlv\r\n");

}
