/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs6374lw.c,v 1.4 2017/07/25 12:00:37 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "r6374inc.h"

PRIVATE INT4
 
 
 
 R6374LwUtilValidateServParams (tR6374ServiceConfigTableEntry * pServiceInfo,
                                UINT1 u1TestType, UINT4 *pu4ErrorCode);

/* LOW LEVEL Routines for Table : Fs6374ServiceConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374ServiceConfigTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374ServiceConfigTable (UINT4 u4Fs6374ContextId,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374ServiceConfigTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374ServiceConfigTable (UINT4 *pu4Fs6374ContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Assign the index */
    *pu4Fs6374ContextId = pR6374ServiceConfigTableEntry->u4ContextId;

    RFC6374_MEMSET (pFs6374ServiceName->pu1_OctetList, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374ServiceConfigTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374ServiceConfigTableEntry->
                                    au1ServiceName));

    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374ServiceConfigTableEntry->au1ServiceName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374ServiceConfigTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374ServiceConfigTable (UINT4 u4Fs6374ContextId,
                                         UINT4 *pu4NextFs6374ContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFs6374ServiceName,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pNextR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pNextR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                       (tRBElem *) & R6374ServiceConfigTableEntry, NULL);

    if (pNextR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs6374ContextId = pNextR6374ServiceConfigTableEntry->u4ContextId;

    RFC6374_MEMSET (pNextFs6374ServiceName->pu1_OctetList, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374ServiceConfigTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374ServiceConfigTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN
                (pNextR6374ServiceConfigTableEntry->au1ServiceName));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374MplsPathType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374MplsPathType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374MplsPathType (UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          INT4 *pi4RetValFs6374MplsPathType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *) RBTreeGet
        (RFC6374_SERVICECONFIG_TABLE,
         (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374MplsPathType =
        (INT4) pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374FwdTnlIdOrPwId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374FwdTnlIdOrPwId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374FwdTnlIdOrPwId (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 *pu4RetValFs6374FwdTnlIdOrPwId)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        *pu4RetValFs6374FwdTnlIdOrPwId =
            pR6374ServiceConfigTableEntry->R6374MplsParams.
            LspMplsPathParams.u4FrwdTunnelId;
    }
    else
    {
        *pu4RetValFs6374FwdTnlIdOrPwId =
            pR6374ServiceConfigTableEntry->R6374MplsParams.
            PwMplsPathParams.u4PwId;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374RevTnlId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374RevTnlId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374RevTnlId (UINT4 u4Fs6374ContextId,
                      tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                      UINT4 *pu4RetValFs6374RevTnlId)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374RevTnlId = pR6374ServiceConfigTableEntry->
        R6374MplsParams.LspMplsPathParams.u4RevTunnelId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374SrcIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374SrcIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374SrcIpAddr (UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       tSNMP_OCTET_STRING_TYPE * pRetValFs6374SrcIpAddr)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        RFC6374_MEMCPY (pRetValFs6374SrcIpAddr->pu1_OctetList,
                        &pR6374ServiceConfigTableEntry->R6374MplsParams.
                        LspMplsPathParams.u4SrcIpAddr, RFC6374_IPV4_ADDR_LEN);
        pRetValFs6374SrcIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DestIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DestIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DestIpAddr (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        tSNMP_OCTET_STRING_TYPE * pRetValFs6374DestIpAddr)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        RFC6374_MEMCPY (pRetValFs6374DestIpAddr->pu1_OctetList,
                        &pR6374ServiceConfigTableEntry->
                        R6374MplsParams.LspMplsPathParams.u4DestIpAddr,
                        RFC6374_IPV4_ADDR_LEN);
        pRetValFs6374DestIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    }
    else
    {
        RFC6374_MEMCPY (pRetValFs6374DestIpAddr->pu1_OctetList,
                        &pR6374ServiceConfigTableEntry->R6374MplsParams.
                        PwMplsPathParams.u4IpAddr, RFC6374_IPV4_ADDR_LEN);
        pRetValFs6374DestIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374EncapType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374EncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374EncapType (UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 *pi4RetValFs6374EncapType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374EncapType =
        (INT4) pR6374ServiceConfigTableEntry->u1EncapType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374TrafficClass
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374TrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374TrafficClass (UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName, INT4
                          *pi4RetValFs6374TrafficClass)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374TrafficClass = (INT4) pR6374ServiceConfigTableEntry->
        u1TrafficClass;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374RowStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374RowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374RowStatus (UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 *pi4RetValFs6374RowStatus)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374RowStatus =
        (INT4) pR6374ServiceConfigTableEntry->u1RowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DyadicMeasurement
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DyadicMeasurement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DyadicMeasurement (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 *pi4RetValFs6374DyadicMeasurement)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DyadicMeasurement = (INT4) pR6374ServiceConfigTableEntry->
        u1DyadicMeasurement;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374TSFNegotiation
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374TSFNegotiation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374TSFNegotiation (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            INT4 *pi4RetValFs6374TSFNegotiation)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374TSFNegotiation = pR6374ServiceConfigTableEntry->
        i4Negostatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374TSNegotiatedFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374TSNegotiatedFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374TSNegotiatedFormat (UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                UINT4 *pu4RetValFs6374TSNegotiatedFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374TSNegotiatedFormat = (UINT4) pR6374ServiceConfigTableEntry->
        u1NegoSuppTsFormat;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DyadicProactiveRole
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DyadicProactiveRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DyadicProactiveRole (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 INT4 *pi4RetValFs6374DyadicProactiveRole)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DyadicProactiveRole = (INT4) pR6374ServiceConfigTableEntry->
        u1DyadicProactiveRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374SessionIntervalQueryStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374SessionIntervalQueryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374SessionIntervalQueryStatus (UINT4 u4Fs6374ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFs6374ServiceName,
                                        INT4
                                        *pi4RetValFs6374SessionIntervalQueryStatus)
{

    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374SessionIntervalQueryStatus =
        (INT4) (pR6374ServiceConfigTableEntry->u1SessIntQueryStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374MinReceptionIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374MinReceptionIntervalInMilliseconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374MinReceptionIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFs6374ServiceName,
                                                UINT4
                                                *pu4RetValFs6374MinReceptionIntervalInMilliseconds)
{

    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374MinReceptionIntervalInMilliseconds =
        (INT4) (pR6374ServiceConfigTableEntry->u2MinReceptionInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374QueryTransmitRetryCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374QueryTransmitRetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374QueryTransmitRetryCount (UINT4 u4Fs6374ContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFs6374ServiceName,
                                     UINT4
                                     *pu4RetValFs6374QueryTransmitRetryCount)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFs6374QueryTransmitRetryCount =
        (UINT4) pR6374ServiceConfigTableEntry->u1QueryTransmitRetryCount;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs6374MplsPathType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374MplsPathType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374MplsPathType (UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          INT4 i4SetValFs6374MplsPathType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *) RBTreeGet
        (RFC6374_SERVICECONFIG_TABLE,
         (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType =
        (UINT1) i4SetValFs6374MplsPathType;

    if (R6374LwUtilValidateServParams (pR6374ServiceConfigTableEntry,
                                       RFC6374_PROACTIVE_TEST_START, NULL)
        == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "nmhSetFs6374MplsPathType : unable to start "
                     "Proactive test\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374FwdTnlIdOrPwId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374FwdTnlIdOrPwId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374FwdTnlIdOrPwId (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4SetValFs6374FwdTnlIdOrPwId)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    BOOL1               b1IfAlreadyRegister = RFC6374_FALSE;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Check if the path is already registered for Status */
    if (R6374UtilValidateMplsPathParams (pR6374ServiceConfigTableEntry,
                                         &b1IfAlreadyRegister) !=
        RFC6374_FAILURE)
    {
        /* De-Register as service is already registered and Tunnel/PW
         * params is changed */
        if (b1IfAlreadyRegister == RFC6374_TRUE)
        {
            if (R6374RegMplsPath (RFC6374_FALSE, pR6374ServiceConfigTableEntry)
                != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                             "nmhSetFs6374FwdTnlIdOrPwId: "
                             "\tSNMP:De-REG Event with MPLS Failed\r\n");
                return SNMP_FAILURE;
            }
        }
        b1IfAlreadyRegister = RFC6374_FALSE;
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
            u4FrwdTunnelId = u4SetValFs6374FwdTnlIdOrPwId;
    }
    else
    {
        pR6374ServiceConfigTableEntry->R6374MplsParams.PwMplsPathParams.u4PwId =
            u4SetValFs6374FwdTnlIdOrPwId;
    }

    /* Test Shouldn't be initiated and regsiter for path status when reset of 
     * MPLS path params are done */
    if ((pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
         RFC6374_MPLS_PATH_TYPE_TNL &&
         pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
         u4FrwdTunnelId != RFC6374_INIT_VAL) ||
        (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
         RFC6374_MPLS_PATH_TYPE_PW &&
         pR6374ServiceConfigTableEntry->R6374MplsParams.PwMplsPathParams.
         u4PwId != RFC6374_INIT_VAL))
    {
        if (R6374LwUtilValidateServParams (pR6374ServiceConfigTableEntry,
                                           RFC6374_PROACTIVE_TEST_START, NULL)
            == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374FwdTnlIdOrPwId : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }

        /* Validate the MPLS PATH configured 
         * and the REG MPLS for Path UP/DOWN Event */
        if (R6374UtilValidateMplsPathParams (pR6374ServiceConfigTableEntry,
                                             &b1IfAlreadyRegister) !=
            RFC6374_FAILURE)
        {
            /* If Validation is success now REG for EVENT */
            if (R6374RegMplsPath (RFC6374_TRUE, pR6374ServiceConfigTableEntry)
                != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                             "nmhSetFs6374FwdTnlIdOrPwId: "
                             "\tSNMP:REG Event with MPLS Failed\r\n");
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374RevTnlId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374RevTnlId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374RevTnlId (UINT4 u4Fs6374ContextId,
                      tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                      UINT4 u4SetValFs6374RevTnlId)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
        u4RevTunnelId = u4SetValFs6374RevTnlId;

    /* Test Shouldn't be initiated and regsiter for path status when reset of
     * MPLS path params are done */
    if (pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
        u4RevTunnelId != RFC6374_INIT_VAL)
    {
        if (R6374LwUtilValidateServParams (pR6374ServiceConfigTableEntry,
                                           RFC6374_PROACTIVE_TEST_START, NULL)
            == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374RevTnlId : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374SrcIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374SrcIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374SrcIpAddr (UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       tSNMP_OCTET_STRING_TYPE * pSetValFs6374SrcIpAddr)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    BOOL1               b1IfAlreadyRegister = RFC6374_FALSE;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry =
        (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        /* Check if the path is already registered for Status */
        if (R6374UtilValidateMplsPathParams (pR6374ServiceConfigTableEntry,
                                             &b1IfAlreadyRegister) !=
            RFC6374_FAILURE)
        {
            /* De-Register as service is already registered and Tunnel/PW
             * params is changed */
            if (b1IfAlreadyRegister == RFC6374_TRUE)
            {
                if (R6374RegMplsPath
                    (RFC6374_FALSE,
                     pR6374ServiceConfigTableEntry) != RFC6374_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                                 "nmhSetFs6374SrcIpAddr: "
                                 "\tSNMP:De-REG Event with MPLS Failed\r\n");
                    return SNMP_FAILURE;
                }
            }
            b1IfAlreadyRegister = RFC6374_FALSE;
        }
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        RFC6374_MEMCPY ((UINT1 *) &pR6374ServiceConfigTableEntry->
                        R6374MplsParams.LspMplsPathParams.u4SrcIpAddr,
                        (pSetValFs6374SrcIpAddr->pu1_OctetList),
                        (pSetValFs6374SrcIpAddr->i4_Length));
    }

    /* Test Shouldn't be initiated and regsiter for path status when reset of
     * MPLS path params are done */
    if (pR6374ServiceConfigTableEntry->R6374MplsParams.
        LspMplsPathParams.u4SrcIpAddr != RFC6374_INIT_VAL)
    {
        if (R6374LwUtilValidateServParams (pR6374ServiceConfigTableEntry,
                                           RFC6374_PROACTIVE_TEST_START, NULL)
            == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374SrcIpAddr : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }

        if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
            RFC6374_MPLS_PATH_TYPE_TNL)
        {
            /* Validate the MPLS PATH configured
             * and the REG MPLS for Path UP/DOWN Event */
            if (R6374UtilValidateMplsPathParams (pR6374ServiceConfigTableEntry,
                                                 &b1IfAlreadyRegister) !=
                RFC6374_FAILURE)
            {
                /* If Validation is success now REG for EVENT */
                if (R6374RegMplsPath
                    (RFC6374_TRUE,
                     pR6374ServiceConfigTableEntry) != RFC6374_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                                 "nmhSetFs6374SrcIpAddr: "
                                 "\tSNMP:REG Event with MPLS Failed\r\n");
                    return SNMP_FAILURE;
                }
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DestIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DestIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DestIpAddr (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        tSNMP_OCTET_STRING_TYPE * pSetValFs6374DestIpAddr)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    BOOL1               b1IfAlreadyRegister = RFC6374_FALSE;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry =
        (tR6374ServiceConfigTableEntry *) RBTreeGet
        (RFC6374_SERVICECONFIG_TABLE,
         (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        /* Check if the path is already registered for Status */
        if (R6374UtilValidateMplsPathParams (pR6374ServiceConfigTableEntry,
                                             &b1IfAlreadyRegister) !=
            RFC6374_FAILURE)
        {
            /* De-Register as service is already registered and Tunnel/PW
             * params is changed */
            if (b1IfAlreadyRegister == RFC6374_TRUE)
            {
                if (R6374RegMplsPath
                    (RFC6374_FALSE,
                     pR6374ServiceConfigTableEntry) != RFC6374_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                                 "nmhSetFs6374DestIpAddr: "
                                 "\tSNMP:De-REG Event with MPLS Failed\r\n");
                    return SNMP_FAILURE;
                }
            }
            b1IfAlreadyRegister = RFC6374_FALSE;
        }
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        RFC6374_MEMCPY ((UINT1 *) &pR6374ServiceConfigTableEntry->
                        R6374MplsParams.LspMplsPathParams.u4DestIpAddr,
                        (pSetValFs6374DestIpAddr->pu1_OctetList),
                        (pSetValFs6374DestIpAddr->i4_Length));
    }
    else
    {
        RFC6374_MEMCPY ((UINT1 *) &pR6374ServiceConfigTableEntry->
                        R6374MplsParams.PwMplsPathParams.u4IpAddr,
                        (pSetValFs6374DestIpAddr->pu1_OctetList),
                        (pSetValFs6374DestIpAddr->i4_Length));
    }

    /* Test Shouldn't be initiated and regsiter for path status when reset of
     * MPLS path params are done */
    if (((pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
          RFC6374_MPLS_PATH_TYPE_TNL) &&
         (pR6374ServiceConfigTableEntry->R6374MplsParams.
          LspMplsPathParams.u4DestIpAddr != RFC6374_INIT_VAL)) ||
        ((pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
          RFC6374_MPLS_PATH_TYPE_PW) &&
         (pR6374ServiceConfigTableEntry->R6374MplsParams.PwMplsPathParams.
          u4IpAddr != RFC6374_INIT_VAL)))
    {
        if (R6374LwUtilValidateServParams (pR6374ServiceConfigTableEntry,
                                           RFC6374_PROACTIVE_TEST_START, NULL)
            == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374DestIpAddr : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }

        if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
            RFC6374_MPLS_PATH_TYPE_TNL)
        {
            /* Validate the MPLS PATH configured
             * and the REG MPLS for Path UP/DOWN Event */
            if (R6374UtilValidateMplsPathParams (pR6374ServiceConfigTableEntry,
                                                 &b1IfAlreadyRegister) !=
                RFC6374_FAILURE)
            {
                /* If Validation is success now REG for EVENT */
                if (R6374RegMplsPath
                    (RFC6374_TRUE,
                     pR6374ServiceConfigTableEntry) != RFC6374_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                                 "nmhSetFs6374DestIpAddr: "
                                 "\tSNMP:REG Event with MPLS Failed\r\n");
                    return SNMP_FAILURE;
                }
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374EncapType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374EncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374EncapType (UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 i4SetValFs6374EncapType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *) RBTreeGet
        (RFC6374_SERVICECONFIG_TABLE,
         (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u1EncapType =
        (UINT1) i4SetValFs6374EncapType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374TrafficClass
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374TrafficClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374TrafficClass (UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          INT4 i4SetValFs6374TrafficClass)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u1TrafficClass =
        (UINT1) i4SetValFs6374TrafficClass;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374RowStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374RowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374RowStatus (UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 i4SetValFs6374RowStatus)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN] = { 0 };
    BOOL1               b1IfAlreadyRegister = RFC6374_FALSE;

    RFC6374_MEMCPY (au1ServiceName, pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    /* Check if Entry is already present */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                au1ServiceName);

    if (pServiceInfo != NULL)
    {
        if (pServiceInfo->u1RowStatus == i4SetValFs6374RowStatus)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValFs6374RowStatus == RFC6374_ROW_STATUS_DESTROY)
    {
        return SNMP_SUCCESS;
    }
    else if ((i4SetValFs6374RowStatus != RFC6374_ROW_STATUS_CREATE_AND_GO) &&
             (i4SetValFs6374RowStatus != RFC6374_ROW_STATUS_CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFs6374RowStatus)
    {
        case RFC6374_ROW_STATUS_CREATE_AND_WAIT:
        case RFC6374_ROW_STATUS_CREATE_AND_GO:
            /* Case Entry is not present 
             * Create a new Entry */
            pServiceInfo = R6374UtlCreateService (u4Fs6374ContextId,
                                                  au1ServiceName);
            if (pServiceInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            gR6374GlobalInfo.u1TotalServices++;
            break;
        case RFC6374_ROW_STATUS_DESTROY:
            /* As Serivce is Present 
             * Delete the Service */
            pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                        pFs6374ServiceName->
                                                        pu1_OctetList);
            if (pServiceInfo != NULL)
            {
                /* Remove the Entries form the corresp LM and DM buffer / stats
                 * Table*/
                R6374UtilRmEntriesFromLmBufferForService (u4Fs6374ContextId,
                                                          pFs6374ServiceName->
                                                          pu1_OctetList);
                R6374UtilRmEntriesFromLmStatsForService (u4Fs6374ContextId,
                                                         pFs6374ServiceName->
                                                         pu1_OctetList);
                R6374UtilRmEntriesFromDmBufferForService (u4Fs6374ContextId,
                                                          pFs6374ServiceName->
                                                          pu1_OctetList);
                R6374UtilRmEntriesFromDmStatsForService (u4Fs6374ContextId,
                                                         pFs6374ServiceName->
                                                         pu1_OctetList);
                /* Validate the MPLS PATH configured
                 * and the DE - REG MPLS for Path UP/DOWN Event */
                if (R6374UtilValidateMplsPathParams (pServiceInfo,
                                                     &b1IfAlreadyRegister) !=
                    RFC6374_FAILURE)
                {
                    /* Now De-Reg For the Event */
                    if (R6374RegMplsPath (RFC6374_FALSE, pServiceInfo) !=
                        RFC6374_SUCCESS)
                    {
                        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                     RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                                     "nmhSetFs6374RowStatus: "
                                     "\tSNMP:De-REG Event with MPLS Failed\r\n");
                        return SNMP_FAILURE;
                    }
                }

                /* Now Free the memory */
                RBTreeRem (RFC6374_SERVICECONFIG_TABLE, pServiceInfo);
                RFC6374_FREE_MEM_BLOCK (RFC6374_SERVICECONFIGTABLE_POOLID,
                                        (UINT1 *) pServiceInfo);
                pServiceInfo = NULL;

                gR6374GlobalInfo.u1TotalServices--;
            }
            break;
        case RFC6374_ROW_STATUS_ACTIVE:

            pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                        pFs6374ServiceName->
                                                        pu1_OctetList);

            if (pServiceInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pServiceInfo->u1RowStatus == i4SetValFs6374RowStatus)
            {
                return SNMP_SUCCESS;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    if (pServiceInfo != NULL)
    {
        pServiceInfo->u1RowStatus = (UINT1) i4SetValFs6374RowStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DyadicMeasurement
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DyadicMeasurement
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DyadicMeasurement (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 i4SetValFs6374DyadicMeasurement)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u1DyadicMeasurement =
        (UINT1) i4SetValFs6374DyadicMeasurement;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374TSFNegotiation
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374TSFNegotiation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374TSFNegotiation (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            INT4 i4SetValFs6374TSFNegotiation)
{
    tR6374ServiceConfigTableEntry R6374ServiceInfo;
    tR6374ServiceConfigTableEntry *pR6374ServiceInfoEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceInfo.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceInfo.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceInfoEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE, (tRBElem *) & R6374ServiceInfo);

    if (pR6374ServiceInfoEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceInfoEntry->i4Negostatus = i4SetValFs6374TSFNegotiation;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DyadicProactiveRole
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DyadicProactiveRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DyadicProactiveRole (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 INT4 i4SetValFs6374DyadicProactiveRole)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u1DyadicProactiveRole =
        (UINT1) i4SetValFs6374DyadicProactiveRole;

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    if (i4SetValFs6374DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_ACTIVE)
    {
        if (pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE)
        {
            if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                     RFC6374_TX_STATUS_START, RFC6374_INIT_VAL,
                                     RFC6374_TEST_MODE_LM) == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhSetFs6374DyadicProactiveRole: unable to start "
                             "Proactive test\r\n");
                return SNMP_FAILURE;
            }
        }
        if (pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE)
        {
            if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                     RFC6374_TX_STATUS_START, RFC6374_INIT_VAL,
                                     RFC6374_TEST_MODE_DM) == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhSetFs6374DyadicProactiveRole: unable to start "
                             "Proactive test\r\n");
                return SNMP_FAILURE;
            }
        }
        if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
        {
            if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                     RFC6374_TX_STATUS_START, RFC6374_INIT_VAL,
                                     RFC6374_TEST_MODE_LMDM) == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhSetFs6374DyadicProactiveRole: unable to start "
                             "Proactive test\r\n");
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374SessionIntervalQueryStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374SessionIntervalQueryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374SessionIntervalQueryStatus (UINT4 u4Fs6374ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFs6374ServiceName,
                                        INT4
                                        i4SetValFs6374SessionIntervalQueryStatus)
{

    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u1SessIntQueryStatus =
        (UINT1) i4SetValFs6374SessionIntervalQueryStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374MinReceptionIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374MinReceptionIntervalInMilliseconds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374MinReceptionIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFs6374ServiceName,
                                                UINT4
                                                u4SetValFs6374MinReceptionIntervalInMilliseconds)
{

    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u2MinReceptionInterval =
        (UINT2) u4SetValFs6374MinReceptionIntervalInMilliseconds;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374QueryTransmitRetryCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374QueryTransmitRetryCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374QueryTransmitRetryCount (UINT4 u4Fs6374ContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFs6374ServiceName,
                                     UINT4
                                     u4SetValFs6374QueryTransmitRetryCount)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->u1QueryTransmitRetryCount =
        (UINT1) u4SetValFs6374QueryTransmitRetryCount;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs6374MplsPathType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374MplsPathType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374MplsPathType (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             INT4 i4TestValFs6374MplsPathType)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    /* Get the Service present and if not throw error */
    if ((i4TestValFs6374MplsPathType != RFC6374_MPLS_PATH_TYPE_TNL) &&
        (i4TestValFs6374MplsPathType != RFC6374_MPLS_PATH_TYPE_PW))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }
    /* Check the Service Path configured initial time */
    if ((pServiceInfo->R6374MplsParams.u1MplsPathType ==
         RFC6374_MPLS_PATH_TYPE_TNL)
        || (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            RFC6374_MPLS_PATH_TYPE_PW))
    {
        if (pServiceInfo->R6374MplsParams.u1MplsPathType !=
            i4TestValFs6374MplsPathType)
        {
            if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                RFC6374_MPLS_PATH_TYPE_TNL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SERVICE_CONFIG_PATH_TUNNEL);
                return SNMP_FAILURE;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SERVICE_CONFIG_PATH_PW);
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374FwdTnlIdOrPwId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374FwdTnlIdOrPwId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374FwdTnlIdOrPwId (UINT4 *pu4ErrorCode,
                               UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4TestValFs6374FwdTnlIdOrPwId)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    UINT4               u4IpAddr = RFC6374_INIT_VAL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        if ((u4TestValFs6374FwdTnlIdOrPwId != RFC6374_INIT_VAL) &&
            (pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
             u4RevTunnelId == u4TestValFs6374FwdTnlIdOrPwId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_FWD_REV_TNL_SAME);
            return SNMP_FAILURE;
        }

        if (R6374UtilGetTunnelEntriesForService (pR6374ServiceConfigTableEntry,
                                                 u4TestValFs6374FwdTnlIdOrPwId,
                                                 RFC6374_INIT_VAL,
                                                 RFC6374_INIT_VAL,
                                                 pu4ErrorCode) ==
            RFC6374_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4IpAddr =
            pR6374ServiceConfigTableEntry->R6374MplsParams.PwMplsPathParams.
            u4IpAddr;
        if (R6374UtilGetTunnelEntriesForService
            (pR6374ServiceConfigTableEntry, u4TestValFs6374FwdTnlIdOrPwId,
             RFC6374_INIT_VAL, u4IpAddr, pu4ErrorCode) == RFC6374_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374RevTnlId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374RevTnlId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374RevTnlId (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4TestValFs6374RevTnlId)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType !=
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValFs6374RevTnlId != RFC6374_INIT_VAL) &&
        (pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
         u4FrwdTunnelId == u4TestValFs6374RevTnlId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_FWD_REV_TNL_SAME);
        return SNMP_FAILURE;
    }

    if (R6374UtilGetTunnelEntriesForService (pR6374ServiceConfigTableEntry,
                                             RFC6374_INIT_VAL,
                                             u4TestValFs6374RevTnlId,
                                             RFC6374_INIT_VAL, pu4ErrorCode)
        == RFC6374_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374SrcIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374SrcIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374SrcIpAddr (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          tSNMP_OCTET_STRING_TYPE * pTestValFs6374SrcIpAddr)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tSNMP_OCTET_STRING_TYPE TestValFs6374SrcIpAddr;
    UINT4               u4TstSrcIpAddr = 0;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, 0,
                    sizeof (tR6374ServiceConfigTableEntry));
    RFC6374_MEMSET (&TestValFs6374SrcIpAddr, 0,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType !=
        RFC6374_MPLS_PATH_TYPE_TNL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    TestValFs6374SrcIpAddr.i4_Length = pTestValFs6374SrcIpAddr->i4_Length;
    TestValFs6374SrcIpAddr.pu1_OctetList =
        pTestValFs6374SrcIpAddr->pu1_OctetList;

    RFC6374_OCTETSTRING_TO_INTEGER (TestValFs6374SrcIpAddr, u4TstSrcIpAddr);
    u4TstSrcIpAddr = OSIX_HTONL (u4TstSrcIpAddr);

    if (u4TstSrcIpAddr == RFC6374_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_IP_ADDR_ZERO);
        return SNMP_FAILURE;
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
        u4DestIpAddr == u4TstSrcIpAddr)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_SRC_DST_IP_ADDR_SAME);
        return SNMP_FAILURE;
    }

    if (R6374UtilGetIpEntriesForService (pR6374ServiceConfigTableEntry,
                                         u4TstSrcIpAddr, RFC6374_INIT_VAL,
                                         pu4ErrorCode) == RFC6374_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DestIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DestIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DestIpAddr (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           tSNMP_OCTET_STRING_TYPE * pTestValFs6374DestIpAddr)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tSNMP_OCTET_STRING_TYPE TestValFs6374DestIpAddr;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    UINT4               u4TstDestIpAddr = RFC6374_INIT_VAL;
    UINT4               u4PwId = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, 0,
                    sizeof (tR6374ServiceConfigTableEntry));
    RFC6374_MEMSET (&TestValFs6374DestIpAddr, 0,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pTestValFs6374DestIpAddr->pu1_OctetList == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);
    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }

    TestValFs6374DestIpAddr.i4_Length = pTestValFs6374DestIpAddr->i4_Length;
    TestValFs6374DestIpAddr.pu1_OctetList =
        pTestValFs6374DestIpAddr->pu1_OctetList;

    RFC6374_OCTETSTRING_TO_INTEGER (TestValFs6374DestIpAddr, u4TstDestIpAddr);
    u4TstDestIpAddr = OSIX_HTONL (u4TstDestIpAddr);

    if (u4TstDestIpAddr == RFC6374_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_IP_ADDR_ZERO);
        return SNMP_FAILURE;
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType
        == RFC6374_MPLS_PATH_TYPE_TNL)
    {
        if (pR6374ServiceConfigTableEntry->R6374MplsParams.LspMplsPathParams.
            u4SrcIpAddr == u4TstDestIpAddr)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_SRC_DST_IP_ADDR_SAME);
            return SNMP_FAILURE;
        }
    }

    if (pR6374ServiceConfigTableEntry->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_PW)
    {
        u4PwId = pR6374ServiceConfigTableEntry->R6374MplsParams.
            uMplsPathParams.MplsPwParams.u4PwId;
    }
    else
    {
        /* Comparison between Destination IP of Tunnel and PW should not happen 
         * when PW is configured first and then Tunnel */
        u4PwId = RFC6374_NO_COMP_DST;
    }
    if (R6374UtilGetIpEntriesForService (pR6374ServiceConfigTableEntry,
                                         u4TstDestIpAddr, u4PwId,
                                         pu4ErrorCode) == RFC6374_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374EncapType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374EncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374EncapType (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          INT4 i4TestValFs6374EncapType)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);

    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374EncapType != RFC6374_ENCAP_NO_GAL_GACH &&
        i4TestValFs6374EncapType != RFC6374_ENCAP_GAL_GACH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374TrafficClass
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374TrafficClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374TrafficClass (UINT4 *pu4ErrorCode,
                             UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             INT4 i4TestValFs6374TrafficClass)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if (i4TestValFs6374TrafficClass < RFC6374_MIN_TRAFFIC_CLASS ||
        i4TestValFs6374TrafficClass > RFC6374_MAX_TRAFFIC_CLASS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374RowStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374RowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374RowStatus (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          INT4 i4TestValFs6374RowStatus)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    /* Check row status possible values */
    if ((i4TestValFs6374RowStatus < RFC6374_ROW_STATUS_ACTIVE) ||
        (i4TestValFs6374RowStatus > RFC6374_ROW_STATUS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* As services reached MAX count of 150, no further creation */
    if ((i4TestValFs6374RowStatus != RFC6374_ROW_STATUS_DESTROY) &&
        (gR6374GlobalInfo.u1TotalServices == RFC6374_MAX_SERVICE_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_MAX_SERVICES_CREATED);
        return SNMP_FAILURE;
    }
    /* Check if Entry is already present */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);

    switch (i4TestValFs6374RowStatus)
    {
        case RFC6374_ROW_STATUS_CREATE_AND_WAIT:
        case RFC6374_ROW_STATUS_CREATE_AND_GO:
            /* If Service is already Present then throw error */
            if (pServiceInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SERVICE_ALREADY_CREATED);
                return SNMP_FAILURE;
            }
            /* validate service name length */
            if ((pFs6374ServiceName->i4_Length <= RFC6374_INIT_VAL) ||
                (pFs6374ServiceName->i4_Length >= RFC6374_SERVICE_NAME_MAX_LEN))
            {
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "Invalid length service name.\r\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }
            break;
        case RFC6374_ROW_STATUS_DESTROY:
            if (pServiceInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
                return SNMP_FAILURE;
            }

            /* Check if Service is already Running for LM or DM
             * before deleting the service */
            pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);

            if ((pLmInfo != NULL) &&
                (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SESSION_ONGOING);
                return SNMP_FAILURE;
            }

            pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

            if ((pDmInfo != NULL) &&
                (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SESSION_ONGOING);
                return SNMP_FAILURE;
            }

            /* Check if Service is already Running for LM or DM
             * before deleting the service */
            pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

            if ((pLmDmInfo != NULL) &&
                (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_SESSION_ONGOING);
                return SNMP_FAILURE;
            }

            break;
        case RFC6374_ROW_STATUS_ACTIVE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DyadicMeasurement
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DyadicMeasurement
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DyadicMeasurement (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  INT4 i4TestValFs6374DyadicMeasurement)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if (i4TestValFs6374DyadicMeasurement != RFC6374_DYADIC_MEAS_ENABLE &&
        i4TestValFs6374DyadicMeasurement != RFC6374_DYADIC_MEAS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFs6374DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) &&
        (pServiceInfo->i4Negostatus == RFC6374_NEGOTIATION_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_TSF_NEGO_ENABLED);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before changing Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    /* Check if any of the Measurement type is set to one-way */
    if (pLmInfo->u1LmType == RFC6374_LM_TYPE_ONE_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLE_LOSS_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmType == RFC6374_DM_TYPE_ONE_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLE_DELAY_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_ONE_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLE_COMB_LMDM_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374TSFNegotiation
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374TSFNegotiation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374TSFNegotiation (UINT4 *pu4ErrorCode,
                               UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 i4TestValFs6374TSFNegotiation)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((i4TestValFs6374TSFNegotiation != RFC6374_NEGOTIATION_ENABLE) &&
        (i4TestValFs6374TSFNegotiation != RFC6374_NEGOTIATION_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    if ((i4TestValFs6374TSFNegotiation == RFC6374_NEGOTIATION_ENABLE) &&
        (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLED);
        return SNMP_FAILURE;
    }

    /* Check if any test is running before changing Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DyadicProactiveRole
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DyadicProactiveRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DyadicProactiveRole (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    INT4 i4TestValFs6374DyadicProactiveRole)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if (i4TestValFs6374DyadicProactiveRole != RFC6374_DYADIC_PRO_ROLE_ACTIVE &&
        i4TestValFs6374DyadicProactiveRole != RFC6374_DYADIC_PRO_ROLE_PASSIVE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before changing Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    /* Check if any of the Measurement type is set to one-way */
    if (pLmInfo->u1LmType == RFC6374_LM_TYPE_ONE_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLE_LOSS_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmType == RFC6374_DM_TYPE_ONE_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLE_DELAY_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_ONE_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_ENABLE_COMB_LMDM_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374SessionIntervalQueryStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374SessionIntervalQueryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374SessionIntervalQueryStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fs6374ContextId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFs6374ServiceName,
                                           INT4
                                           i4TestValFs6374SessionIntervalQueryStatus)
{

    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((i4TestValFs6374SessionIntervalQueryStatus !=
         RFC6374_SESS_INT_QUERY_ENABLE)
        && (i4TestValFs6374SessionIntervalQueryStatus !=
            RFC6374_SESS_INT_QUERY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "Invalid value for input.\r\n");
        return SNMP_FAILURE;
    }

    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if any test is running before changing Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374MinReceptionIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374MinReceptionIntervalInMilliseconds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374MinReceptionIntervalInMilliseconds (UINT4 *pu4ErrorCode,
                                                   UINT4 u4Fs6374ContextId,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFs6374ServiceName,
                                                   UINT4
                                                   u4TestValFs6374MinReceptionIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    /* Check for range of values */
    if ((u4TestValFs6374MinReceptionIntervalInMilliseconds <
         RFC6374_MIN_RECEPTION_INTERVAL) ||
        (u4TestValFs6374MinReceptionIntervalInMilliseconds >
         RFC6374_MAX_RECEPTION_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "Input value out of valid range.\r\n");
        return SNMP_FAILURE;
    }

    /* Check if service is valid or not */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pLmInfo);
    UNUSED_PARAM (pDmInfo);
    UNUSED_PARAM (pLmDmInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374QueryTransmitRetryCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374QueryTransmitRetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374QueryTransmitRetryCount (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs6374ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFs6374ServiceName,
                                        UINT4
                                        u4TestValFs6374QueryTransmitRetryCount)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    /*Check value range */
    if ((u4TestValFs6374QueryTransmitRetryCount <
         RFC6374_MIN_QUERY_TRANSMIT_RETRYCNT) ||
        (u4TestValFs6374QueryTransmitRetryCount >
         RFC6374_MAX_QUERY_TRANSMIT_RETRYCNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "Input value out of valid range.\r\n");
        return SNMP_FAILURE;
    }

    /* Check if service is valid or not */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    /* Check if any test is running before changing Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    else if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs6374ServiceConfigTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs6374ServiceConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374ParamsConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374ParamsConfigTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374ParamsConfigTable (UINT4 u4Fs6374ContextId,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374ParamsConfigTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374ParamsConfigTable (UINT4 *pu4Fs6374ContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4Fs6374ContextId = pR6374ServiceConfigTableEntry->u4ContextId;

    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374ServiceConfigTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374ServiceConfigTableEntry->
                                    au1ServiceName));

    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374ServiceConfigTableEntry->au1ServiceName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374ParamsConfigTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374ParamsConfigTable (UINT4 u4Fs6374ContextId,
                                        UINT4 *pu4NextFs6374ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFs6374ServiceName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pNextR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pNextR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                       (tRBElem *) & R6374ServiceConfigTableEntry, NULL);

    if (pNextR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFs6374ContextId = pNextR6374ServiceConfigTableEntry->u4ContextId;

    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374ServiceConfigTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374ServiceConfigTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN
                (pNextR6374ServiceConfigTableEntry->au1ServiceName));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374LMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMType (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 *pi4RetValFs6374LMType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFs6374LMType =
        (UINT1) pR6374ServiceConfigTableEntry->LmInfo.u1LmType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMMethod (UINT4 u4Fs6374ContextId,
                      tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                      INT4 *pi4RetValFs6374LMMethod)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMMethod =
        (INT4) pR6374ServiceConfigTableEntry->LmInfo.u1LmMethod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMMode (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 *pi4RetValFs6374LMMode)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFs6374LMMode =
        (INT4) pR6374ServiceConfigTableEntry->LmInfo.u1LmMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMNoOfMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMNoOfMessages (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 *pu4RetValFs6374LMNoOfMessages)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMNoOfMessages = (UINT4)
        pR6374ServiceConfigTableEntry->LmInfo.u2LmNoOfMessages;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMTimeIntervalInMilliseconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMTimeIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFs6374ServiceName,
                                          UINT4
                                          *pu4RetValFs6374LMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMTimeIntervalInMilliseconds =
        (INT4) pR6374ServiceConfigTableEntry->LmInfo.u2LmConfiguredTimeInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMTimeStampFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMTimeStampFormat (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 *pi4RetValFs6374LMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMTimeStampFormat =
        (INT4) pR6374ServiceConfigTableEntry->LmInfo.u1LmTSFormat;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374LMTransmitStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMTransmitStatus (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              INT4 *pi4RetValFs6374LMTransmitStatus)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMTransmitStatus =
        (INT4) pR6374ServiceConfigTableEntry->LmInfo.u1LmStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMType (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 *pi4RetValFs6374DMType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMType =
        (INT4) pR6374ServiceConfigTableEntry->DmInfo.u1DmType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMMode (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 *pi4RetValFs6374DMMode)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFs6374DMMode =
        (INT4) pR6374ServiceConfigTableEntry->DmInfo.u1DmMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMTimeIntervalInMilliseconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMTimeIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFs6374ServiceName,
                                          UINT4
                                          *pu4RetValFs6374DMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFs6374DMTimeIntervalInMilliseconds =
        (UINT4) pR6374ServiceConfigTableEntry->DmInfo.
        u2DmConfiguredTimeInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMNoOfMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMNoOfMessages (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 *pu4RetValFs6374DMNoOfMessages)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFs6374DMNoOfMessages =
        (UINT4) pR6374ServiceConfigTableEntry->DmInfo.u2DmNoOfMessages;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMTimeStampFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMTimeStampFormat (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 *pi4RetValFs6374DMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFs6374DMTimeStampFormat =
        (INT4) pR6374ServiceConfigTableEntry->DmInfo.u1DmTSFormat;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMTransmitStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMTransmitStatus (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              INT4 *pi4RetValFs6374DMTransmitStatus)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMTransmitStatus =
        (INT4) pR6374ServiceConfigTableEntry->DmInfo.u1DmStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMType (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         INT4 *pi4RetValFs6374CmbLMDMType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374CmbLMDMType =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMMethod (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           INT4 *pi4RetValFs6374CmbLMDMMethod)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374CmbLMDMMethod =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmMethod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMMode (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         INT4 *pi4RetValFs6374CmbLMDMMode)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374CmbLMDMMode =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMNoOfMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMNoOfMessages (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 *pu4RetValFs6374CmbLMDMNoOfMessages)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374CmbLMDMNoOfMessages =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.u2LmDmNoOfMessages;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMTimeIntervalInMilliseconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMTimeIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFs6374ServiceName,
                                               UINT4
                                               *pu4RetValFs6374CmbLMDMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374CmbLMDMTimeIntervalInMilliseconds =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.
        u2LmDmConfiguredTimeInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMTimeStampFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMTimeStampFormat (UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    INT4 *pi4RetValFs6374CmbLMDMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374CmbLMDMTimeStampFormat =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmTSFormat;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMTransmitStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMTransmitStatus (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   INT4 *pi4RetValFs6374CmbLMDMTransmitStatus)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374CmbLMDMTransmitStatus =
        (INT4) pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374DMPaddingSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMPaddingSize (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 *pu4RetValFs6374DMPaddingSize)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMPaddingSize =
        pR6374ServiceConfigTableEntry->DmInfo.u4DmPadSize;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374CmbLMDMPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374CmbLMDMPaddingSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374CmbLMDMPaddingSize (UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                UINT4 *pu4RetValFs6374CmbLMDMPaddingSize)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374CmbLMDMPaddingSize =
        (UINT4) pR6374ServiceConfigTableEntry->LmDmInfo.u4LmDmPadSize;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs6374LMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMType (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 i4SetValFs6374LMType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmInfo.u1LmType =
        (UINT1) i4SetValFs6374LMType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMMethod (UINT4 u4Fs6374ContextId,
                      tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                      INT4 i4SetValFs6374LMMethod)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmInfo.u1LmMethod =
        (UINT1) i4SetValFs6374LMMethod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMMode (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 i4SetValFs6374LMMode)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    UINT1               u1LmMode = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1LmMode = pR6374ServiceConfigTableEntry->LmInfo.u1LmMode;

    if ((u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
        (i4SetValFs6374LMMode == RFC6374_LM_MODE_ONDEMAND) &&
        (pR6374ServiceConfigTableEntry->LmInfo.u1LmStatus
         == RFC6374_TX_STATUS_NOT_READY))
    {
        if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                 RFC6374_TX_STATUS_STOP, RFC6374_INIT_VAL,
                                 RFC6374_TEST_MODE_LM) == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374LMMode : unable to stop "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }
    pR6374ServiceConfigTableEntry->LmInfo.u1LmMode =
        (UINT1) i4SetValFs6374LMMode;

    /* when the mode as set as Proactive then test has to be started 
     * immediatley */
    if (i4SetValFs6374LMMode == RFC6374_LM_MODE_PROACTIVE)
    {
        pR6374ServiceConfigTableEntry->LmInfo.u1PrevLmMode =
            RFC6374_LM_MODE_PROACTIVE;

        if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                 RFC6374_TX_STATUS_START, RFC6374_INIT_VAL,
                                 RFC6374_TEST_MODE_LM) == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374LMMode : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }

    /* Changes of the previous mode is setmode is for ondemand */
    if (pR6374ServiceConfigTableEntry->LmInfo.u1LmMode ==
        RFC6374_LM_MODE_ONDEMAND)
    {
        pR6374ServiceConfigTableEntry->LmInfo.u1PrevLmMode =
            RFC6374_LM_MODE_ONDEMAND;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMNoOfMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMNoOfMessages (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4SetValFs6374LMNoOfMessages)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmInfo.u2LmNoOfMessages =
        (UINT2) u4SetValFs6374LMNoOfMessages;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMTimeIntervalInMilliseconds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMTimeIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFs6374ServiceName,
                                          UINT4
                                          u4SetValFs6374LMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmInfo.u2LmTimeInterval =
        (UINT2) u4SetValFs6374LMTimeIntervalInMilliseconds;
    pR6374ServiceConfigTableEntry->LmInfo.u2LmConfiguredTimeInterval =
        (UINT2) u4SetValFs6374LMTimeIntervalInMilliseconds;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMTimeStampFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMTimeStampFormat (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 i4SetValFs6374LMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pR6374ServiceConfigTableEntry->LmInfo.u1LmTSFormat =
        (UINT1) i4SetValFs6374LMTimeStampFormat;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374LMTransmitStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMTransmitStatus (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              INT4 i4SetValFs6374LMTransmitStatus)
{
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceTableInfo = NULL;

    if (gR6374GlobalInfo.u1ModuleStatus != RFC6374_ENABLED)
    {
        return SNMP_FAILURE;
    }

    if (pFs6374ServiceName == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4Fs6374ContextId,
                     "nmhSetFsR6374TransmitLmStatus: "
                     "\tSNMP: Service Name NULL\n");
        return SNMP_FAILURE;
    }

    pServiceTableInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                     pFs6374ServiceName->
                                                     pu1_OctetList);
    if (pServiceTableInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4Fs6374ContextId,
                     "nmhSetFsR6374TransmitLmStatus: "
                     "\tSNMP:NO service Entry\n");
        return SNMP_FAILURE;
    }

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceTableInfo);

    /* Service entry corresponding to exists */
    /* Set the tranmsit LM status to corresponding service entry */
    if (i4SetValFs6374LMTransmitStatus == RFC6374_TX_STATUS_STOP)
    {
        /* Variable used to update test abort status as true */
        pLmInfo->b1LmTestStatus |= RFC6374_TEST_ABORTED;

        if (R6374UtilPostTransaction (pServiceTableInfo,
                                      RFC6374_LM_STOP_SESSION) !=
            RFC6374_SUCCESS)

        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         u4Fs6374ContextId,
                         "nmhSetFsR6374TransmitLmStatus: "
                         "\tSNMP:Post Event for Transaction Initiation Failed\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    pLmInfo->u1LmQTSFormat = pLmInfo->u1LmTSFormat;

    /* Initiate LM transaction and send event to LM Initiator for the
     * same */
    if (R6374UtilPostTransaction (pServiceTableInfo,
                                  RFC6374_LM_START_SESSION) != RFC6374_SUCCESS)
    {
        pLmInfo->b1LmResultOk = RFC6374_FALSE;
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4Fs6374ContextId,
                     "nmhSetFsR6374TransmitLmStatus: "
                     "\tSNMP: Post Event for Transaction Initiation Failed\n");
        return SNMP_FAILURE;
    }

    /* Set bIsLmStartSessPosted flag to indicate that
     * the LM status is soon going to be NOT_READY and
     * no other LM session can be started*/
    pLmInfo->bIsLmStartSessPosted = RFC6374_TRUE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMType (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 i4SetValFs6374DMType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->DmInfo.u1DmType =
        (UINT1) i4SetValFs6374DMType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMMode (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    INT4 i4SetValFs6374DMMode)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    UINT1               u1DmMode = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1DmMode = pR6374ServiceConfigTableEntry->DmInfo.u1DmMode;

    if ((u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
        (i4SetValFs6374DMMode == RFC6374_DM_MODE_ONDEMAND) &&
        (pR6374ServiceConfigTableEntry->DmInfo.u1DmStatus
         == RFC6374_TX_STATUS_NOT_READY))
    {
        if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                 RFC6374_TX_STATUS_STOP, RFC6374_INIT_VAL,
                                 RFC6374_TEST_MODE_DM) == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374DMMode : unable to stop "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }

    pR6374ServiceConfigTableEntry->DmInfo.u1DmMode =
        (UINT1) i4SetValFs6374DMMode;

    /* when the mode as set as Proactive then test has to be started 
     * immediatley */
    if (i4SetValFs6374DMMode == RFC6374_DM_MODE_PROACTIVE)
    {
        pR6374ServiceConfigTableEntry->DmInfo.u1PrevDmMode =
            RFC6374_DM_MODE_PROACTIVE;

        if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                 RFC6374_TX_STATUS_START, RFC6374_INIT_VAL,
                                 RFC6374_TEST_MODE_DM) == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374DMMode : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }

    /* Changes of the previous mode is setmode is for ondemand */
    if (pR6374ServiceConfigTableEntry->DmInfo.u1DmMode ==
        RFC6374_DM_MODE_ONDEMAND)
    {
        pR6374ServiceConfigTableEntry->DmInfo.u1PrevDmMode =
            RFC6374_DM_MODE_ONDEMAND;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMTimeIntervalInMilliseconds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMTimeIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFs6374ServiceName,
                                          UINT4
                                          u4SetValFs6374DMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pR6374ServiceConfigTableEntry->DmInfo.u2DmTimeInterval =
        (UINT2) u4SetValFs6374DMTimeIntervalInMilliseconds;
    pR6374ServiceConfigTableEntry->DmInfo.u2DmConfiguredTimeInterval =
        (UINT2) u4SetValFs6374DMTimeIntervalInMilliseconds;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMNoOfMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMNoOfMessages (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4SetValFs6374DMNoOfMessages)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pR6374ServiceConfigTableEntry->DmInfo.u2DmNoOfMessages =
        (UINT2) u4SetValFs6374DMNoOfMessages;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMTimeStampFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMTimeStampFormat (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               INT4 i4SetValFs6374DMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);
    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pR6374ServiceConfigTableEntry->DmInfo.u1DmTSFormat =
        (UINT1) i4SetValFs6374DMTimeStampFormat;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMTransmitStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMTransmitStatus (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              INT4 i4SetValFs6374DMTransmitStatus)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    if (gR6374GlobalInfo.u1ModuleStatus != RFC6374_ENABLED)
    {
        return SNMP_FAILURE;
    }
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    /* Service entry corresponding to  exists */
    /* Set the tranmsit DM status to corresponding service entry */
    if (i4SetValFs6374DMTransmitStatus == RFC6374_TX_STATUS_STOP)
    {
        /* Variable used to update test abort status as true */
        pDmInfo->b1DmTestStatus |= RFC6374_TEST_ABORTED;

        if (R6374UtilPostTransaction (pR6374ServiceConfigTableEntry,
                                      RFC6374_DM_STOP_SESSION) !=
            RFC6374_SUCCESS)

        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                         RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFsR6374TransmitDmStatus: "
                         "\tSNMP:Post Event for Transaction Initiation Failed\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    pDmInfo->u1DmQTSFormat = pDmInfo->u1DmTSFormat;

    /* Initiate DM transaction and send event to DM Initiator for the
     * same */
    if (R6374UtilPostTransaction
        (pR6374ServiceConfigTableEntry,
         RFC6374_DM_START_SESSION) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                     RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "nmhSetFsR6374TransmitDmStatus: "
                     "\tSNMP:Post Event for Transaction Initiation Failed\n");
        return SNMP_FAILURE;
    }

    /* Set bIsDmStartSessPosted flag to indicate that
     * the DM status is soon going to be NOT_READY and
     * no other DM session can be started*/
    pDmInfo->bIsDmStartSessPosted = RFC6374_TRUE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMType (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         INT4 i4SetValFs6374CmbLMDMType)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmType =
        (UINT1) i4SetValFs6374CmbLMDMType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMMethod (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           INT4 i4SetValFs6374CmbLMDMMethod)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmMethod =
        (UINT1) i4SetValFs6374CmbLMDMMethod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMMode (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         INT4 i4SetValFs6374CmbLMDMMode)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    UINT1               u1LmDmMode = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1LmDmMode = pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmMode;

    if ((u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
        (i4SetValFs6374CmbLMDMMode == RFC6374_LMDM_MODE_ONDEMAND) &&
        (pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmStatus
         == RFC6374_TX_STATUS_NOT_READY))
    {
        if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                 RFC6374_TX_STATUS_STOP, RFC6374_INIT_VAL,
                                 RFC6374_TEST_MODE_LMDM) == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374CmbLMDMMode : unable to stop "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmMode =
        (UINT1) i4SetValFs6374CmbLMDMMode;
    /* when the mode as set as Proactive then test has to be started 
     * immediatley */
    if (i4SetValFs6374CmbLMDMMode == RFC6374_LMDM_MODE_PROACTIVE)
    {
        pR6374ServiceConfigTableEntry->LmDmInfo.u1PrevLmDmMode =
            RFC6374_LMDM_MODE_PROACTIVE;

        if (R6374UtilTestStatus (pR6374ServiceConfigTableEntry,
                                 RFC6374_TX_STATUS_START, RFC6374_INIT_VAL,
                                 RFC6374_TEST_MODE_LMDM) == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374CmbLMDMMode : unable to start "
                         "Proactive test\r\n");
            return SNMP_FAILURE;
        }
    }

    /* Changes of the previous mode is setmode is for ondemand */
    if (pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmMode ==
        RFC6374_LMDM_MODE_ONDEMAND)
    {
        pR6374ServiceConfigTableEntry->LmDmInfo.u1PrevLmDmMode =
            RFC6374_LMDM_MODE_ONDEMAND;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMNoOfMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMNoOfMessages (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4SetValFs6374CmbLMDMNoOfMessages)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u2LmDmNoOfMessages =
        (UINT2) u4SetValFs6374CmbLMDMNoOfMessages;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMTimeIntervalInMilliseconds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMTimeIntervalInMilliseconds (UINT4 u4Fs6374ContextId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFs6374ServiceName,
                                               UINT4
                                               u4SetValFs6374CmbLMDMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u2LmDmTimeInterval =
        (UINT2) u4SetValFs6374CmbLMDMTimeIntervalInMilliseconds;
    pR6374ServiceConfigTableEntry->LmDmInfo.u2LmDmConfiguredTimeInterval =
        (UINT2) u4SetValFs6374CmbLMDMTimeIntervalInMilliseconds;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMTimeStampFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMTimeStampFormat (UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    INT4 i4SetValFs6374CmbLMDMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmTSFormat =
        (UINT1) i4SetValFs6374CmbLMDMTimeStampFormat;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMTransmitStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMTransmitStatus (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   INT4 i4SetValFs6374CmbLMDMTransmitStatus)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceConfigTableEntry);

    /* Set the tranmsit combined LMDM status to corresponding service 
     * entry */
    if (i4SetValFs6374CmbLMDMTransmitStatus == RFC6374_TX_STATUS_STOP)
    {
        /* Variable used to update test abort status as true */
        pLmDmInfo->b1LmDmTestStatus |= RFC6374_TEST_ABORTED;

        if (R6374UtilPostTransaction (pR6374ServiceConfigTableEntry,
                                      RFC6374_LMDM_STOP_SESSION) !=
            RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                         RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhSetFs6374CmbLMDMTransmitStatus: "
                         "\tSNMP:Post Event for Transaction stop Failed\n");
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }

    pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;

    /* Initiate Combined LMDM transaction and send event to Combined LMDM 
     * Initiator for the same */
    if (R6374UtilPostTransaction (pR6374ServiceConfigTableEntry,
                                  RFC6374_LMDM_START_SESSION) !=
        RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                     RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "nmhSetFs6374CmbLMDMTransmitStatus: "
                     "\tSNMP:Post Event for Transaction Initiation Failed\n");
        return SNMP_FAILURE;
    }

    /* Set bIsLmDmStartSessPosted flag to indicate that
     * the Com LM/DM status is soon going to be NOT_READY and
     * no other LM/DM session can be started*/
    pLmDmInfo->bIsLmDmStartSessPosted = RFC6374_TRUE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374DMPaddingSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMPaddingSize (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4SetValFs6374DMPaddingSize)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    UINT4               u4PadSize = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->DmInfo.u4DmPadSize =
        u4SetValFs6374DMPaddingSize;

    /* Reset PadTlvCount because whenever the padding tlv
     * size gets changed, PadTlvCount has to be calculated based
     * on the new size */
    pR6374ServiceConfigTableEntry->DmInfo.u1DmPadTlvCount = RFC6374_INIT_VAL;

    u4PadSize = u4SetValFs6374DMPaddingSize;

    if (u4PadSize != RFC6374_INIT_VAL)
    {
        do
        {
            if (u4PadSize > RFC6374_MAX_PAD_SIZE_SINGLE_TLV)
            {
                pR6374ServiceConfigTableEntry->DmInfo.u1DmPadTlvCount++;
                u4PadSize = u4PadSize - RFC6374_MAX_PAD_SIZE_SINGLE_TLV;
            }
            else
            {
                pR6374ServiceConfigTableEntry->DmInfo.u1DmPadTlvCount++;
                break;
            }
        }
        while (u4PadSize != RFC6374_INIT_VAL);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374CmbLMDMPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374CmbLMDMPaddingSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374CmbLMDMPaddingSize (UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                UINT4 u4SetValFs6374CmbLMDMPaddingSize)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    UINT4               u4PadSize = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));
    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->LmDmInfo.u4LmDmPadSize =
        u4SetValFs6374CmbLMDMPaddingSize;

    /* Reset PadTlvCount because whenever the padding tlv
     * size gets changed, PadTlvCount has to be calculated based
     * on the new size */
    pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmPadTlvCount =
        RFC6374_INIT_VAL;

    u4PadSize = u4SetValFs6374CmbLMDMPaddingSize;

    if (u4PadSize != RFC6374_INIT_VAL)
    {
        do
        {
            if (u4PadSize > RFC6374_MAX_PAD_SIZE_SINGLE_TLV)
            {
                pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmPadTlvCount++;
                u4PadSize = u4PadSize - RFC6374_MAX_PAD_SIZE_SINGLE_TLV;
            }
            else
            {
                pR6374ServiceConfigTableEntry->LmDmInfo.u1LmDmPadTlvCount++;
                break;
            }
        }
        while (u4PadSize != RFC6374_INIT_VAL);
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMType (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 i4TestValFs6374LMType)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    if (i4TestValFs6374LMType != RFC6374_LM_TYPE_ONE_WAY &&
        i4TestValFs6374LMType != RFC6374_LM_TYPE_TWO_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    /* Error check for Dyadic Measurement when type is one-way */
    if ((pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) &&
        (i4TestValFs6374LMType == RFC6374_LM_TYPE_ONE_WAY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_LOSS_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMMethod (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         INT4 i4TestValFs6374LMMethod)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    if (i4TestValFs6374LMMethod != RFC6374_LM_METHOD_INFERED &&
        i4TestValFs6374LMMethod != RFC6374_LM_METHOD_DIRECT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMMode (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 i4TestValFs6374LMMode)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((i4TestValFs6374LMMode != RFC6374_LM_MODE_ONDEMAND) &&
        (i4TestValFs6374LMMode != RFC6374_LM_MODE_PROACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        /* Configuring Lm mode as on-demand is allowed 
         * to stop the proactive test.But
         * Configuring Lm mode as on-demand/proactive is not allowed 
         * for the below scenarios.
         * 1. Proactive test is selected and trying to configure the 
         * mode as proactive
         * 2. On-Demand test is selected and trying to configure the 
         * mode as proactive
         * 3. On-Demand test is selcted and trying to configure the mode 
         * as on-demand */
        if (!((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
              (i4TestValFs6374LMMode == RFC6374_LM_MODE_ONDEMAND)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }
    /* It is possible to set the mode as proactive for Loss/Delay/Combined
     * Loss-Delay before associating mandatory parameters
     * and whenever the mandatory parameters are getting configured
     * test will be initiated based on the mode. So, it is possible to start 
     * the Loss,Delay and Combined Loss/Delay test and which will in-turn
     * leads to incorrect calculation. To avoid that if combined LMDM mode 
     * as aleady set as proactve then LM mode cannot be modified 
     * as Proactive and vice versa. */
    if ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
        (i4TestValFs6374LMMode == RFC6374_LM_MODE_PROACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LM_DM_MODE_PROACTIVE_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMNoOfMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMNoOfMessages (UINT4 *pu4ErrorCode,
                               UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4TestValFs6374LMNoOfMessages)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    if (u4TestValFs6374LMNoOfMessages < RFC6374_MIN_ONDEMAND_MSG_COUNT ||
        u4TestValFs6374LMNoOfMessages > RFC6374_MAX_ONDEMAND_MSG_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMTimeIntervalInMilliseconds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMTimeIntervalInMilliseconds (UINT4 *pu4ErrorCode,
                                             UINT4 u4Fs6374ContextId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFs6374ServiceName,
                                             UINT4
                                             u4TestValFs6374LMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    if (u4TestValFs6374LMTimeIntervalInMilliseconds <
        RFC6374_MIN_TIME_INTERVAL_IN_MS
        || u4TestValFs6374LMTimeIntervalInMilliseconds >
        RFC6374_MAX_TIME_INTERVAL_IN_MS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMTimeStampFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMTimeStampFormat (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  INT4 i4TestValFs6374LMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    if ((i4TestValFs6374LMTimeStampFormat != RFC6374_TS_FORMAT_NULL) &&
        (i4TestValFs6374LMTimeStampFormat != RFC6374_TS_FORMAT_SEQ_NUM) &&
        (i4TestValFs6374LMTimeStampFormat != RFC6374_TS_FORMAT_NTP) &&
        (i4TestValFs6374LMTimeStampFormat != RFC6374_TS_FORMAT_IEEE1588))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFs6374LMTimeStampFormat == RFC6374_TS_FORMAT_NTP) &&
        ((gR6374GlobalInfo.u1SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_NTP)
         != RFC6374_SUPP_TS_FORMAT_NTP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RFC6374_CLI_TSF_NTP_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test is already before changing Params */
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LOSS_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374LMTransmitStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMTransmitStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 INT4 i4TestValFs6374LMTransmitStatus)
{
    tR6374MplsPathInfo *pMplsPathInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceTableInfo = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4TrafficClass = RFC6374_INIT_VAL;

    if (gR6374GlobalInfo.u1ModuleStatus != RFC6374_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_MODULE_DISABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFs6374LMTransmitStatus != RFC6374_TX_STATUS_START) &&
        (i4TestValFs6374LMTransmitStatus != RFC6374_TX_STATUS_STOP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374LMTransmitStatus == RFC6374_TX_STATUS_START)
    {
        if (((RFC6374_DM_GET_CURRENT_SESSION_COUNT ()) +
             (RFC6374_LM_GET_CURRENT_SESSION_COUNT ()) +
             (RFC6374_LMDM_GET_CURRENT_SESSION_COUNT ())) >=
            RFC6374_MAX_SESSION_COUNT_PER_SERVICE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (RFC6374_CLI_MAX_SESSIONS_RUNNING);
            return SNMP_FAILURE;
        }
    }
    /* Get Service Info For particular Service Name */
    pServiceTableInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                     pFs6374ServiceName->
                                                     pu1_OctetList);
    if (pServiceTableInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceTableInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceTableInfo);

    /* For Dyadic Proactive measurememt if role is set as passive then dont initiate 
     * the test */
    if ((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
        (pServiceTableInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
        && (pServiceTableInfo->u1DyadicProactiveRole ==
            RFC6374_DYADIC_PRO_ROLE_PASSIVE)
        && (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_READY)
        && (i4TestValFs6374LMTransmitStatus != RFC6374_TX_STATUS_STOP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374LMTransmitStatus == RFC6374_TX_STATUS_STOP)
    {
        if (pLmInfo->u1LmStatus != RFC6374_TX_STATUS_NOT_READY)
        {
            /* when test is not running and user try to stop the
             * test then error will be thrown */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_TEST_NOT_IN_PROGRESS);
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhTestv2Fs6374LMTransmitStatus: "
                         "\tSNMP: Test is not in-progress for LM \r\n");
            return SNMP_FAILURE;
        }
    }
    /* This routine throws an error if the mandatory parameters
     * are not configured before starting the test */
    if (R6374LwUtilValidateServParams (pServiceTableInfo, RFC6374_INIT_VAL,
                                       pu4ErrorCode) == RFC6374_FAILURE)
    {
        /* As the function R6374LwUtilValidateServParams itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    /* Allocate Memory for MPLS Path Info Structure */
    pMplsPathInfo = (tR6374MplsPathInfo *) MemAllocMemBlk
        (RFC6374_MPLS_PATH_INFO_POOLID);
    if (pMplsPathInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "nmhTestv2Fs6374LMTransmitStatus: "
                     "\tSNMP Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return SNMP_FAILURE;
    }

    RFC6374_MEMSET (pMplsPathInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374MplsPathInfo));

    /* Check Status is Not Ready only if Transmit status comes for START LM */
    if (i4TestValFs6374LMTransmitStatus == RFC6374_TX_STATUS_START)
    {
        if ((pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY) ||
            (pLmInfo->bIsLmStartSessPosted == RFC6374_TRUE) ||
            (pLmInfo->u1LmRespTimeOutReTries != RFC6374_INIT_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_LOSS_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        if ((pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY) ||
            (pLmDmInfo->bIsLmDmStartSessPosted == RFC6374_TRUE) ||
            (pLmDmInfo->u1LmDmRespTimeOutReTries != RFC6374_INIT_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        else                    /* If TX status is READY */
        {
            pServiceTableInfo->R6374MplsParams.u1MeasurementType =
                pLmInfo->u1LmType;

            /* Check MPLS Path Status and BFD Session/Traffic */
            i4RetVal = R6374GetMplsPathInfo (pServiceTableInfo->u4ContextId,
                                             &pServiceTableInfo->
                                             R6374MplsParams, pLmInfo->u1LmMode,
                                             pMplsPathInfo);
            if (i4RetVal == RFC6374_FAILURE)
            {
                /* MPLS Path Status down */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_DOWN);
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhTestv2Fs6374LMTransmitStatus: "
                             "\tSNMP:Unable to Fetch MPLS Path Info \r\n");
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                return SNMP_FAILURE;
            }

            /* Check Status */
            if ((pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN) ||
                (pServiceTableInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN))
            {
                /* MPLS Path Status down */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_DOWN);
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhTestv2Fs6374LMTransmitStatus: "
                             "\tSNMP:MPLS Path is Down \r\n");
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                return SNMP_FAILURE;
            }

            /* If MPLS Path is up validate BFD is Active on Particular
             * Traffic Class/Exp As Configured TrafficClass in
             * RFC6374 */
            /* Get TrafficClass/EXP for Particular LSP/PW */
#ifdef RFC6374STUB_WANTED
            if (pServiceTableInfo->R6374MplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_PW)
#endif
            {
                i4RetVal =
                    R6374GetBfdTrafficClass (pServiceTableInfo,
                                             &u4TrafficClass);
                if (i4RetVal == RFC6374_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (RFC6374_CLI_BFD_DOWN);
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "nmhTestv2Fs6374LMTransmitStatus: "
                                 "\tSNMP : Unable to Fetch BFD Traffic Class \r\n");
                    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                            (UINT1 *) pMplsPathInfo);
                    return SNMP_FAILURE;
                }

                /* Validate Traffic Recieved From BFD */
                if (pServiceTableInfo->u1TrafficClass != (UINT1) u4TrafficClass)
                {
                    /* BFD Session Traffic Class Not Supported */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (RFC6374_CLI_BFD_TRAFFIC_CLASS_MISMATCH);
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "nmhTestv2Fs6374LMTransmitStatus: "
                                 "\tSNMP :Traffic Miss Match for Particular LSP/PW \r\n");
                    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                            (UINT1 *) pMplsPathInfo);
                    return SNMP_FAILURE;
                }
            }
        }
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMType (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 i4TestValFs6374DMType)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    if (i4TestValFs6374DMType != RFC6374_DM_TYPE_ONE_WAY &&
        i4TestValFs6374DMType != RFC6374_DM_TYPE_TWO_WAY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    /* Error check for Dyadic Measurement when type is one-way */
    if ((pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) &&
        (i4TestValFs6374DMType == RFC6374_DM_TYPE_ONE_WAY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_DELAY_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMMode (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                       tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                       INT4 i4TestValFs6374DMMode)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((i4TestValFs6374DMMode != RFC6374_DM_MODE_ONDEMAND) &&
        (i4TestValFs6374DMMode != RFC6374_DM_MODE_PROACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        /* Configuring Dm mode as on-demand is allowed 
         * to stop the proactive test.But
         * Configuring Dm mode as on-demand/proactive is not allowed 
         * for the below scenarios.
         * 1. Proactive test is selected and trying to configure the 
         * mode as proactive
         * 2. On-Demand test is selected and trying to configure the 
         * mode as proactive
         * 3. On-Demand test is selcted and trying to configure the mode 
         * as on-demand */
        if (!((pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
              (i4TestValFs6374DMMode == RFC6374_DM_MODE_ONDEMAND)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }
    /* It is possible to set the mode as proactive for Loss/Delay/Combined
     * Loss-Delay before associating mandatory parameters
     * and whenever the mandatory parameters are getting configured
     * test will be initiated based on the mode. So, it is possible to start 
     * the Loss,Delay and Combined Loss/Delay test and which will in-turn
     * leads to incorrect calculation. To avoid that if combined LMDM mode 
     * as aleady set as proactve then DM mode cannot be modified 
     * as Proactive and vice versa. */
    if ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
        (i4TestValFs6374DMMode == RFC6374_DM_MODE_PROACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_LM_DM_MODE_PROACTIVE_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMTimeIntervalInMilliseconds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMTimeIntervalInMilliseconds (UINT4 *pu4ErrorCode,
                                             UINT4 u4Fs6374ContextId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFs6374ServiceName,
                                             UINT4
                                             u4TestValFs6374DMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    if (u4TestValFs6374DMTimeIntervalInMilliseconds <
        RFC6374_MIN_TIME_INTERVAL_IN_MS ||
        u4TestValFs6374DMTimeIntervalInMilliseconds >
        RFC6374_MAX_TIME_INTERVAL_IN_MS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMNoOfMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMNoOfMessages (UINT4 *pu4ErrorCode,
                               UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4TestValFs6374DMNoOfMessages)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    if (u4TestValFs6374DMNoOfMessages < RFC6374_MIN_ONDEMAND_MSG_COUNT ||
        u4TestValFs6374DMNoOfMessages > RFC6374_MAX_ONDEMAND_MSG_COUNT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if Service is already Running before chaning Params */
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMTimeStampFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMTimeStampFormat (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  INT4 i4TestValFs6374DMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    if ((i4TestValFs6374DMTimeStampFormat != RFC6374_TS_FORMAT_NTP) &&
        (i4TestValFs6374DMTimeStampFormat != RFC6374_TS_FORMAT_IEEE1588))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFs6374DMTimeStampFormat == RFC6374_TS_FORMAT_NTP) &&
        ((gR6374GlobalInfo.u1SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_NTP)
         != RFC6374_SUPP_TS_FORMAT_NTP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RFC6374_CLI_TSF_NTP_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test is already before changing Params */
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMTransmitStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMTransmitStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 INT4 i4TestValFs6374DMTransmitStatus)
{
    tR6374MplsPathInfo *pMplsPathInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;

    if (gR6374GlobalInfo.u1ModuleStatus != RFC6374_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_MODULE_DISABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFs6374DMTransmitStatus != RFC6374_TX_STATUS_START) &&
        (i4TestValFs6374DMTransmitStatus != RFC6374_TX_STATUS_STOP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374DMTransmitStatus == RFC6374_TX_STATUS_START)
    {
        if (((RFC6374_DM_GET_CURRENT_SESSION_COUNT ()) +
             (RFC6374_LM_GET_CURRENT_SESSION_COUNT ()) +
             (RFC6374_LMDM_GET_CURRENT_SESSION_COUNT ())) >=
            RFC6374_MAX_SESSION_COUNT_PER_SERVICE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (RFC6374_CLI_MAX_SESSIONS_RUNNING);
            return SNMP_FAILURE;
        }
    }

    /* Check if Entry is already present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /* For Dyadic Proactive measurememt if role is set as passive then dont initiate
     * the test */
    if ((pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
        (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) &&
        (pServiceInfo->u1DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_PASSIVE)
        && (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_READY)
        && (i4TestValFs6374DMTransmitStatus != RFC6374_TX_STATUS_STOP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374DMTransmitStatus == RFC6374_TX_STATUS_STOP)
    {
        if (pDmInfo->u1DmStatus != RFC6374_TX_STATUS_NOT_READY)
        {
            /* when test is not running and user try to stop the
             * test then error will be thrown */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_TEST_NOT_IN_PROGRESS);
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhTestv2Fs6374DMTransmitStatus: "
                         "\tSNMP: Test is not in-progress for DM \r\n");
            return SNMP_FAILURE;
        }
    }

    /* This routine throws an error if the mandatory parameters
     * are not configured before starting the test */
    if (R6374LwUtilValidateServParams (pServiceInfo, RFC6374_INIT_VAL,
                                       pu4ErrorCode) == RFC6374_FAILURE)
    {
        /* As the function R6374LwUtilValidateServParams itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Allocate Memory for MPLS Path Info Structure */
    pMplsPathInfo = (tR6374MplsPathInfo *) MemAllocMemBlk
        (RFC6374_MPLS_PATH_INFO_POOLID);
    if (pMplsPathInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "nmhTestv2Fs6374DMTransmitStatus: "
                     "\tSNMP Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return SNMP_FAILURE;
    }

    RFC6374_MEMSET (pMplsPathInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374MplsPathInfo));

    /* Check Status is Not Ready only if Transmit status comes for START DM */
    if (i4TestValFs6374DMTransmitStatus == RFC6374_TX_STATUS_START)
    {
        if ((pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY) ||
            (pDmInfo->bIsDmStartSessPosted == RFC6374_TRUE) ||
            (pDmInfo->u1DmRespTimeOutReTries != RFC6374_INIT_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_DELAY_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        if ((pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY) ||
            (pLmDmInfo->bIsLmDmStartSessPosted == RFC6374_TRUE) ||
            (pLmDmInfo->u1LmDmRespTimeOutReTries != RFC6374_INIT_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        else                    /* If TX Status in READY */
        {
            pServiceInfo->R6374MplsParams.u1MeasurementType = pDmInfo->u1DmType;
            /* Check MPLS Path Status */
            i4RetVal = R6374GetMplsPathInfo (pServiceInfo->u4ContextId,
                                             &pServiceInfo->R6374MplsParams,
                                             pDmInfo->u1DmMode, pMplsPathInfo);
            if (i4RetVal == RFC6374_FAILURE)
            {
                /* MPLS Path Status down */
                CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_DOWN);
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhTestv2Fs6374DMTransmitStatus: "
                             "\tSNMP : Unable to Fetch MPLS Path Info \r\n");
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                return SNMP_FAILURE;
            }
            if ((pServiceInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN) ||
                (pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN))
            {
                /* MPLS Path Status down */
                CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_DOWN);
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhTestv2Fs6374DMTransmitStatus: "
                             "\tSNMP :MPLS Path is down\r\n");
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                return SNMP_FAILURE;
            }
        }
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMType (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            INT4 i4TestValFs6374CmbLMDMType)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((i4TestValFs6374CmbLMDMType != RFC6374_LMDM_TYPE_ONE_WAY) &&
        (i4TestValFs6374CmbLMDMType != RFC6374_LMDM_TYPE_TWO_WAY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    /* Error check for Dyadic Measurement when type is one-way */
    if ((pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) &&
        (i4TestValFs6374CmbLMDMType == RFC6374_LMDM_TYPE_ONE_WAY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DYADIC_MEAS_COMB_LMDM_TYPE_ONE_WAY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMMethod (UINT4 *pu4ErrorCode,
                              UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              INT4 i4TestValFs6374CmbLMDMMethod)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if (i4TestValFs6374CmbLMDMMethod != RFC6374_LMDM_METHOD_INFERED &&
        i4TestValFs6374CmbLMDMMethod != RFC6374_LMDM_METHOD_DIRECT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMMode (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            INT4 i4TestValFs6374CmbLMDMMode)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    if ((i4TestValFs6374CmbLMDMMode != RFC6374_LMDM_MODE_ONDEMAND) &&
        (i4TestValFs6374CmbLMDMMode != RFC6374_LMDM_MODE_PROACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        /* Configuring Combined LMDM mode as on-demand is allowed 
         * to stop the proactive test.But
         * Configuring LmDm mode as on-demand/proactive is not allowed 
         * for the below scenarios.
         * 1. Proactive test is selected and trying to configure the 
         * mode as proactive
         * 2. On-Demand test is selected and trying to configure the 
         * mode as proactive
         * 3. On-Demand test is selcted and trying to configure the mode 
         * as on-demand */
        if (!((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
              (i4TestValFs6374CmbLMDMMode == RFC6374_LMDM_MODE_ONDEMAND)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
            return SNMP_FAILURE;
        }
    }
    /* It is possible to set the mode as proactive for Loss/Delay/Combined
     * Loss-Delay before associating mandatory parameters
     * and whenever the mandatory parameters are getting configured
     * test will be initiated based on the mode. So, it is possible to start 
     * the Loss,Delay and Combined Loss/Delay test and which will in-turn
     * leads to incorrect calculation. To avoid that if LM/DM mode 
     * as aleady set as proactve then combined LMDM mode cannot be modified 
     * as Proactive and vice versa. */
    if (((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) ||
         (pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE)) &&
        (i4TestValFs6374CmbLMDMMode == RFC6374_LMDM_MODE_PROACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_MODE_PROACTIVE_NOT_ALLOWED);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMNoOfMessages
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMNoOfMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMNoOfMessages (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 u4TestValFs6374CmbLMDMNoOfMessages)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((u4TestValFs6374CmbLMDMNoOfMessages <
         RFC6374_MIN_ONDEMAND_MSG_COUNT) ||
        (u4TestValFs6374CmbLMDMNoOfMessages >
         RFC6374_MAX_LMDM_ONDEMAND_MSG_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMTimeIntervalInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMTimeIntervalInMilliseconds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMTimeIntervalInMilliseconds (UINT4 *pu4ErrorCode,
                                                  UINT4 u4Fs6374ContextId,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFs6374ServiceName,
                                                  UINT4
                                                  u4TestValFs6374CmbLMDMTimeIntervalInMilliseconds)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((u4TestValFs6374CmbLMDMTimeIntervalInMilliseconds <
         RFC6374_MIN_TIME_INTERVAL_IN_MS) ||
        (u4TestValFs6374CmbLMDMTimeIntervalInMilliseconds >
         RFC6374_MAX_TIME_INTERVAL_IN_MS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMTimeStampFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMTimeStampFormat (UINT4 *pu4ErrorCode,
                                       UINT4 u4Fs6374ContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFs6374ServiceName,
                                       INT4
                                       i4TestValFs6374CmbLMDMTimeStampFormat)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if ((i4TestValFs6374CmbLMDMTimeStampFormat != RFC6374_TS_FORMAT_NTP) &&
        (i4TestValFs6374CmbLMDMTimeStampFormat != RFC6374_TS_FORMAT_IEEE1588))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFs6374CmbLMDMTimeStampFormat == RFC6374_TS_FORMAT_NTP) &&
        ((gR6374GlobalInfo.u1SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_NTP)
         != RFC6374_SUPP_TS_FORMAT_NTP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RFC6374_CLI_TSF_NTP_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMTransmitStatus
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMTransmitStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMTransmitStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fs6374ContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFs6374ServiceName,
                                      INT4 i4TestValFs6374CmbLMDMTransmitStatus)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374MplsPathInfo *pMplsPathInfo = NULL;
    UINT4               u4TrafficClass = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_SUCCESS;

    if (gR6374GlobalInfo.u1ModuleStatus != RFC6374_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_MODULE_DISABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFs6374CmbLMDMTransmitStatus != RFC6374_TX_STATUS_START) &&
        (i4TestValFs6374CmbLMDMTransmitStatus != RFC6374_TX_STATUS_STOP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374CmbLMDMTransmitStatus == RFC6374_TX_STATUS_START)
    {
        /* Maximum session allowed for a service is 10
         * so, before starting the test, please validate the
         * session count maintained globally for Loss, delay and
         * combined loss & delay measurement test */
        if (((RFC6374_DM_GET_CURRENT_SESSION_COUNT ()) +
             (RFC6374_LM_GET_CURRENT_SESSION_COUNT ()) +
             (RFC6374_LMDM_GET_CURRENT_SESSION_COUNT ())) >=
            RFC6374_MAX_SESSION_COUNT_PER_SERVICE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (RFC6374_CLI_MAX_SESSIONS_RUNNING);
            return SNMP_FAILURE;
        }
    }

    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    /* For Dyadic Proactive measurememt if role is set as passive then dont initiate
     * the test */
    if ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
        (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) &&
        (pServiceInfo->u1DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_PASSIVE)
        && (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_READY)
        && (i4TestValFs6374CmbLMDMTransmitStatus != RFC6374_TX_STATUS_STOP))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs6374CmbLMDMTransmitStatus == RFC6374_TX_STATUS_STOP)
    {
        if (pLmDmInfo->u1LmDmStatus != RFC6374_TX_STATUS_NOT_READY)
        {
            /* when test is not running and user try to stop the
             * test then error will be thrown */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_TEST_NOT_IN_PROGRESS);
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "nmhTestv2Fs6374CmbLMDMTransmitStatus: "
                         "\tSNMP: Test is not in-progress for Combined LMDM \r\n");
            return SNMP_FAILURE;
        }
    }

    /* This routine throws an error if the mandatory parameters
     * are not configured before starting the test */
    if (R6374LwUtilValidateServParams (pServiceInfo, RFC6374_INIT_VAL,
                                       pu4ErrorCode) == RFC6374_FAILURE)
    {
        /* As the function R6374LwUtilValidateServParams itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Allocate Memory for MPLS Path Info Structure */
    pMplsPathInfo = (tR6374MplsPathInfo *) MemAllocMemBlk
        (RFC6374_MPLS_PATH_INFO_POOLID);
    if (pMplsPathInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "nmhTestv2Fs6374CmbLMDMTransmitStatus: "
                     "\tSNMP Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return SNMP_FAILURE;
    }

    RFC6374_MEMSET (pMplsPathInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374MplsPathInfo));

    /* Start the test only if Transmit status is Ready */
    if (i4TestValFs6374CmbLMDMTransmitStatus == RFC6374_TX_STATUS_START)
    {
        if ((pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY) ||
            (pLmDmInfo->bIsLmDmStartSessPosted == RFC6374_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        else if ((pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY) ||
                 (pLmInfo->bIsLmStartSessPosted == RFC6374_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_LOSS_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        else if ((pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY) ||
                 (pDmInfo->bIsDmStartSessPosted == RFC6374_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RFC6374_CLI_DELAY_MEASUREMENT_ONGOING);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                    (UINT1 *) pMplsPathInfo);
            return SNMP_FAILURE;
        }
        else                    /* check the Tunnel status or BFD status if transmit status 
                                 * in READY */
        {
            pServiceInfo->R6374MplsParams.u1MeasurementType =
                pLmDmInfo->u1LmDmType;
            /* Check MPLS Path Status */
            i4RetVal = R6374GetMplsPathInfo (pServiceInfo->u4ContextId,
                                             &pServiceInfo->R6374MplsParams,
                                             pLmDmInfo->u1LmDmMode,
                                             pMplsPathInfo);
            if (i4RetVal == RFC6374_FAILURE)
            {
                /* MPLS Path Status down */
                CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_DOWN);
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhTestv2Fs6374CmbLMDMTransmitStatus: "
                             "\tSNMP : Unable to fetch MPLS path info \r\n");
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                return SNMP_FAILURE;
            }
            if ((pServiceInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN) ||
                (pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN))
            {
                /* MPLS Path Status down */
                CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_DOWN);
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "nmhTestv2Fs6374CmbLMDMTransmitStatus: "
                             "\tSNMP :MPLS Path is down\r\n");
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                return SNMP_FAILURE;
            }
            /* If MPLS Path is up, validate BFD is Active on Particular
             * Traffic Class/Exp As Configured TrafficClass in
             * RFC6374 */
            /* Get TrafficClass/EXP for Particular LSP/PW */
#ifdef RFC6374STUB_WANTED
            if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_PW)
#endif
            {
                i4RetVal = R6374GetBfdTrafficClass
                    (pServiceInfo, &u4TrafficClass);
                if (i4RetVal == RFC6374_FAILURE)
                {
                    CLI_SET_ERR (RFC6374_CLI_BFD_DOWN);
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "nmhTestv2Fs6374CmbLMDMTransmitStatus: "
                                 "\tSNMP : Unable to Fetch BFD Traffic "
                                 "Class \r\n");
                    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                            (UINT1 *) pMplsPathInfo);
                    return SNMP_FAILURE;
                }

                /* Validate Traffic Recieved From BFD */
                if (pServiceInfo->u1TrafficClass != (UINT1) u4TrafficClass)
                {
                    /* BFD Session Traffic Class Not Supported */
                    CLI_SET_ERR (RFC6374_CLI_BFD_TRAFFIC_CLASS_MISMATCH);
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "nmhTestv2Fs6374CmbLMDMTransmitStatus: "
                                 "\tSNMP :Traffic Miss Match for Particular "
                                 "LSP/PW \r\n");
                    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                            (UINT1 *) pMplsPathInfo);
                    return SNMP_FAILURE;
                }
            }
        }
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                            (UINT1 *) pMplsPathInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374DMPaddingSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMPaddingSize (UINT4 *pu4ErrorCode,
                              UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 u4TestValFs6374DMPaddingSize)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    if (u4TestValFs6374DMPaddingSize > RFC6374_MAX_PADDING_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RFC6374_TRC1 (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "nmhTestv2Fs6374DMPaddingSize : "
                      "\tSNMP : Invalid padding size value %d \r\n",
                      u4TestValFs6374DMPaddingSize);
        return SNMP_FAILURE;
    }
    /* Get the Service present and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }
    /* Check if test is already before changing Params */
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374CmbLMDMPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374CmbLMDMPaddingSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374CmbLMDMPaddingSize (UINT4 *pu4ErrorCode,
                                   UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4TestValFs6374CmbLMDMPaddingSize)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    if (u4TestValFs6374CmbLMDMPaddingSize > RFC6374_MAX_PADDING_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RFC6374_TRC1 (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "nmhTestv2Fs6374CmbLMDMPaddingSize : "
                      "\tSNMP : Invalid padding size value %d \r\n",
                      u4TestValFs6374CmbLMDMPaddingSize);
        return SNMP_FAILURE;
    }
    /* Get the Service info and if not throw error */
    pServiceInfo = R6374UtilGetServiceFromName (u4Fs6374ContextId,
                                                pFs6374ServiceName->
                                                pu1_OctetList);
    if (pServiceInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_SERVICE_NOT_PRESENT);
        return SNMP_FAILURE;
    }

    /* Check if test (combined loss and delay measurement) is already 
     * running for this service before chaning Params */
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RFC6374_CLI_CMB_LOSS_DELAY_SESSION_ONGOING);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs6374ParamsConfigTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs6374ParamsConfigTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374LMTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374LMTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374LMTable (UINT4 u4Fs6374ContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFs6374ServiceName,
                                       UINT4 u4Fs6374LMSessionId,
                                       UINT4 u4Fs6374LMSeqNum)
{
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGet (RFC6374_LMBUFFER_TABLE,
                   (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374LMTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374LMTable (UINT4 *pu4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 *pu4Fs6374LMSessionId,
                               UINT4 *pu4Fs6374LMSeqNum)
{
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGetFirst (RFC6374_LMBUFFER_TABLE);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4Fs6374ContextId = pR6374LMBufferTableEntry->u4ContextId;

    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374LMBufferTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374LMBufferTableEntry->au1ServiceName));

    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374LMBufferTableEntry->au1ServiceName));

    *pu4Fs6374LMSessionId = pR6374LMBufferTableEntry->u4SessionId;
    *pu4Fs6374LMSeqNum = pR6374LMBufferTableEntry->u4SeqCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374LMTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
                Fs6374LMSessionId
                nextFs6374LMSessionId
                Fs6374LMSeqNum
                nextFs6374LMSeqNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374LMTable (UINT4 u4Fs6374ContextId,
                              UINT4 *pu4NextFs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              tSNMP_OCTET_STRING_TYPE * pNextFs6374ServiceName,
                              UINT4 u4Fs6374LMSessionId,
                              UINT4 *pu4NextFs6374LMSessionId,
                              UINT4 u4Fs6374LMSeqNum,
                              UINT4 *pu4NextFs6374LMSeqNum)
{
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pNextR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pNextR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGetNext (RFC6374_LMBUFFER_TABLE,
                       (tRBElem *) & R6374LMBufferTableEntry, NULL);

    if (pNextR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs6374ContextId = pNextR6374LMBufferTableEntry->u4ContextId;
    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374LMBufferTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374LMBufferTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pNextR6374LMBufferTableEntry->au1ServiceName));
    *pu4NextFs6374LMSessionId = pNextR6374LMBufferTableEntry->u4SessionId;
    *pu4NextFs6374LMSeqNum = pNextR6374LMBufferTableEntry->u4SeqCount;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374LMTxLossAtSenderEnd
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum

                The Object 
                retValFs6374LMTxLossAtSenderEnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMTxLossAtSenderEnd (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4Fs6374LMSessionId,
                                 UINT4 u4Fs6374LMSeqNum,
                                 UINT4 *pu4RetValFs6374LMTxLossAtSenderEnd)
{
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGet (RFC6374_LMBUFFER_TABLE,
                   (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMTxLossAtSenderEnd =
        pR6374LMBufferTableEntry->u4CalcTxLossSender;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMRxLossAtSenderEnd
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum

                The Object 
                retValFs6374LMRxLossAtSenderEnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMRxLossAtSenderEnd (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4Fs6374LMSessionId,
                                 UINT4 u4Fs6374LMSeqNum,
                                 UINT4 *pu4RetValFs6374LMRxLossAtSenderEnd)
{
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGet (RFC6374_LMBUFFER_TABLE,
                   (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMRxLossAtSenderEnd =
        pR6374LMBufferTableEntry->u4CalcRxLossSender;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMRxLossAtReceiverEnd
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum

                The Object 
                retValFs6374LMRxLossAtReceiverEnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMRxLossAtReceiverEnd (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 u4Fs6374LMSeqNum,
                                   UINT4 *pu4RetValFs6374LMRxLossAtReceiverEnd)
{
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGet (RFC6374_LMBUFFER_TABLE,
                   (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMRxLossAtReceiverEnd =
        pR6374LMBufferTableEntry->u4CalcRxLossReceiver;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMMeasurementTimeTakenInMilliseconds
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum

                The Object 
                retValFs6374LMMeasurementTimeTakenInMilliseconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMMeasurementTimeTakenInMilliseconds (UINT4 u4Fs6374ContextId,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFs6374ServiceName,
                                                  UINT4 u4Fs6374LMSessionId,
                                                  UINT4 u4Fs6374LMSeqNum,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pRetValFs6374LMMeasurementTimeTakenInMilliseconds)
{
    UINT1              *pu1ResultTimeStampf = NULL;
    UINT4               u4AvgSec = RFC6374_INIT_VAL;
    UINT4               u4AvgNanoSec = RFC6374_INIT_VAL;
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *) RBTreeGet
        (RFC6374_LMBUFFER_TABLE, (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1ResultTimeStampf =
        pRetValFs6374LMMeasurementTimeTakenInMilliseconds->pu1_OctetList;

    u4AvgSec = pR6374LMBufferTableEntry->MeasurementTimeTaken.u4Seconds;
    u4AvgNanoSec = pR6374LMBufferTableEntry->MeasurementTimeTaken.u4NanoSeconds;

    RFC6374_PUT_4BYTE (pu1ResultTimeStampf, u4AvgSec);
    RFC6374_PUT_4BYTE (pu1ResultTimeStampf, u4AvgNanoSec);

    pRetValFs6374LMMeasurementTimeTakenInMilliseconds->i4_Length =
        RFC6374_TIMESTAMP_FIELD_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMTransmitResultOK
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum

                The Object 
                retValFs6374LMTransmitResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMTransmitResultOK (UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                UINT4 u4Fs6374LMSessionId,
                                UINT4 u4Fs6374LMSeqNum,
                                INT4 *pi4RetValFs6374LMTransmitResultOK)
{
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *) RBTreeGet
        (RFC6374_LMBUFFER_TABLE, (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMTransmitResultOK =
        (INT4) pR6374LMBufferTableEntry->u1Resultok;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMTestPktsCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
                Fs6374LMSeqNum

                The Object
                retValFs6374LMTestPktsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMTestPktsCount (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374LMSessionId,
                             UINT4 u4Fs6374LMSeqNum,
                             UINT4 *pu4RetValFs6374LMTestPktsCount)
{

    tR6374LmBufferTableEntry R6374LMBufferTableEntry;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    R6374LMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMBufferTableEntry.u4SessionId = u4Fs6374LMSessionId;
    R6374LMBufferTableEntry.u4SeqCount = u4Fs6374LMSeqNum;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGet (RFC6374_LMBUFFER_TABLE,
                   (tRBElem *) & R6374LMBufferTableEntry);

    if (pR6374LMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMTestPktsCount =
        pR6374LMBufferTableEntry->u4TxTestPktsCount;

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Fs6374DMTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374DMTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374DMTable (UINT4 u4Fs6374ContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFs6374ServiceName,
                                       UINT4 u4Fs6374DMSessionId,
                                       UINT4 u4Fs6374DMSeqNum)
{
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;
    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGet (RFC6374_DMBUFFER_TABLE,
                   (tRBElem *) & R6374DMBufferTableEntry);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374DMTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374DMTable (UINT4 *pu4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 *pu4Fs6374DMSessionId,
                               UINT4 *pu4Fs6374DMSeqNum)
{
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGetFirst (RFC6374_DMBUFFER_TABLE);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Assign the index */
    *pu4Fs6374ContextId = pR6374DMBufferTableEntry->u4ContextId;

    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374DMBufferTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374DMBufferTableEntry->au1ServiceName));
    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374DMBufferTableEntry->au1ServiceName));

    *pu4Fs6374DMSessionId = pR6374DMBufferTableEntry->u4SessionId;

    *pu4Fs6374DMSeqNum = pR6374DMBufferTableEntry->u4SeqCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374DMTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
                Fs6374DMSessionId
                nextFs6374DMSessionId
                Fs6374DMSeqNum
                nextFs6374DMSeqNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374DMTable (UINT4 u4Fs6374ContextId,
                              UINT4 *pu4NextFs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              tSNMP_OCTET_STRING_TYPE * pNextFs6374ServiceName,
                              UINT4 u4Fs6374DMSessionId,
                              UINT4 *pu4NextFs6374DMSessionId,
                              UINT4 u4Fs6374DMSeqNum,
                              UINT4 *pu4NextFs6374DMSeqNum)
{
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pNextR6374DmBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pNextR6374DmBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGetNext (RFC6374_DMBUFFER_TABLE,
                       (tRBElem *) & R6374DMBufferTableEntry, NULL);

    if (pNextR6374DmBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs6374ContextId = pNextR6374DmBufferTableEntry->u4ContextId;

    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374DmBufferTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374DmBufferTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pNextR6374DmBufferTableEntry->au1ServiceName));

    *pu4NextFs6374DMSessionId = pNextR6374DmBufferTableEntry->u4SessionId;

    *pu4NextFs6374DMSeqNum = pNextR6374DmBufferTableEntry->u4SeqCount;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374DMDelayValue
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum

                The Object 
                retValFs6374DMDelayValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMDelayValue (UINT4 u4Fs6374ContextId,
                          tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                          UINT4 u4Fs6374DMSessionId, UINT4 u4Fs6374DMSeqNum,
                          tSNMP_OCTET_STRING_TYPE * pRetValFs6374DMDelayValue)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGet (RFC6374_DMBUFFER_TABLE,
                   (tRBElem *) & R6374DMBufferTableEntry);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pu1TxTimeStampf = pRetValFs6374DMDelayValue->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->PktDelayValue.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->PktDelayValue.u4NanoSeconds);
    pRetValFs6374DMDelayValue->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMRTDelayValue
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum

                The Object 
                retValFs6374DMRTDelayValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMRTDelayValue (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4Fs6374DMSessionId, UINT4 u4Fs6374DMSeqNum,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFs6374DMRTDelayValue)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGet (RFC6374_DMBUFFER_TABLE,
                   (tRBElem *) & R6374DMBufferTableEntry);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMRTDelayValue->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->PktRTDelayValue.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->PktRTDelayValue.u4NanoSeconds);
    pRetValFs6374DMRTDelayValue->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMIPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum

                The Object 
                retValFs6374DMIPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMIPDV (UINT4 u4Fs6374ContextId,
                    tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                    UINT4 u4Fs6374DMSessionId, UINT4 u4Fs6374DMSeqNum,
                    tSNMP_OCTET_STRING_TYPE * pRetValFs6374DMIPDV)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGet (RFC6374_DMBUFFER_TABLE,
                   (tRBElem *) & R6374DMBufferTableEntry);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMIPDV->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->InterPktDelayVariation.
                       u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->InterPktDelayVariation.
                       u4NanoSeconds);

    pRetValFs6374DMIPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum

                The Object 
                retValFs6374DMPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMPDV (UINT4 u4Fs6374ContextId,
                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                   UINT4 u4Fs6374DMSessionId, UINT4 u4Fs6374DMSeqNum,
                   tSNMP_OCTET_STRING_TYPE * pRetValFs6374DMPDV)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGet (RFC6374_DMBUFFER_TABLE,
                   (tRBElem *) & R6374DMBufferTableEntry);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMPDV->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->PktDelayVariation.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMBufferTableEntry->PktDelayVariation.
                       u4NanoSeconds);

    pRetValFs6374DMPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMTransmitResultOK
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
                Fs6374DMSeqNum

                The Object 
                retValFs6374DMTransmitResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMTransmitResultOK (UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                UINT4 u4Fs6374DMSessionId,
                                UINT4 u4Fs6374DMSeqNum,
                                INT4 *pi4RetValFs6374DMTransmitResultOK)
{
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    R6374DMBufferTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMBufferTableEntry.u4SessionId = u4Fs6374DMSessionId;
    R6374DMBufferTableEntry.u4SeqCount = u4Fs6374DMSeqNum;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGet (RFC6374_DMBUFFER_TABLE,
                   (tRBElem *) & R6374DMBufferTableEntry);

    if (pR6374DMBufferTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMTransmitResultOK =
        (INT4) pR6374DMBufferTableEntry->u1Resultok;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374LMStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374LMStatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374LMStatsTable (UINT4 u4Fs6374ContextId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFs6374ServiceName,
                                            UINT4 u4Fs6374LMSessionId)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374LMStatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374LMStatsTable (UINT4 *pu4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 *pu4Fs6374LMSessionId)
{
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGetFirst (RFC6374_LMSTATS_TABLE);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Assign the index */
    *pu4Fs6374ContextId = pR6374LMStatsTableEntry->u4ContextId;
    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374LMStatsTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374LMStatsTableEntry->au1ServiceName));

    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374LMStatsTableEntry->au1ServiceName));

    *pu4Fs6374LMSessionId = pR6374LMStatsTableEntry->u4SessionId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374LMStatsTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
                Fs6374LMSessionId
                nextFs6374LMSessionId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374LMStatsTable (UINT4 u4Fs6374ContextId,
                                   UINT4 *pu4NextFs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 *pu4NextFs6374LMSessionId)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pNextR6374LmStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pNextR6374LmStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGetNext (RFC6374_LMSTATS_TABLE,
                       (tRBElem *) & R6374LMStatsTableEntry, NULL);

    if (pNextR6374LmStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs6374ContextId = pNextR6374LmStatsTableEntry->u4ContextId;

    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374LmStatsTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374LmStatsTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pNextR6374LmStatsTableEntry->au1ServiceName));

    *pu4NextFs6374LMSessionId = pNextR6374LmStatsTableEntry->u4SessionId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsMplsType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsMplsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsMplsType (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374LMSessionId,
                             INT4 *pi4RetValFs6374LMStatsMplsType)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsMplsType =
        (INT4) pR6374LMStatsTableEntry->u1MplsPathType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsFwdTnlIdOrPwId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsFwdTnlIdOrPwId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsFwdTnlIdOrPwId (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 *pu4RetValFs6374LMStatsFwdTnlIdOrPwId)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsFwdTnlIdOrPwId =
        pR6374LMStatsTableEntry->u4ChannelId1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsRevTnlId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsRevTnlId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsRevTnlId (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374LMSessionId,
                             UINT4 *pu4RetValFs6374LMStatsRevTnlId)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsRevTnlId = pR6374LMStatsTableEntry->u4ChannelId2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsSrcIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsSrcIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsSrcIpAddr (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 u4Fs6374LMSessionId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFs6374LMStatsSrcIpAddr)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RFC6374_MEMCPY (pRetValFs6374LMStatsSrcIpAddr->pu1_OctetList,
                    &pR6374LMStatsTableEntry->
                    u4ChannelIpAddr1, RFC6374_IPV4_ADDR_LEN);
    pRetValFs6374LMStatsSrcIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsDestIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsDestIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsDestIpAddr (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374LMSessionId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFs6374LMStatsDestIpAddr)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    RFC6374_MEMCPY (pRetValFs6374LMStatsDestIpAddr->pu1_OctetList,
                    &pR6374LMStatsTableEntry->
                    u4ChannelIpAddr2, RFC6374_IPV4_ADDR_LEN);
    pRetValFs6374LMStatsDestIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsTrafficClass
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId
    
                The Object 
                retValFs6374LMStatsTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsTrafficClass (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4Fs6374LMSessionId,
                                 INT4 *pi4RetValFs6374LMStatsTrafficClass)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsTrafficClass =
        (INT4) pR6374LMStatsTableEntry->u1TrafficClass;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsMode (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4Fs6374LMSessionId,
                         INT4 *pi4RetValFs6374LMStatsMode)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsMode = pR6374LMStatsTableEntry->u1LossMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsMethod
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsMethod (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4Fs6374LMSessionId,
                           INT4 *pi4RetValFs6374LMStatsMethod)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsMethod = pR6374LMStatsTableEntry->u1LossMethod;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsType (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4Fs6374LMSessionId,
                         INT4 *pi4RetValFs6374LMStatsType)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;
    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));
    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);
    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsType = pR6374LMStatsTableEntry->u1LossType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsTimeStampFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsTimeStampFormat (UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 u4Fs6374LMSessionId,
                                    INT4 *pi4RetValFs6374LMStatsTimeStampFormat)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;
    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsTimeStampFormat =
        (INT4) pR6374LMStatsTableEntry->u1LossTSFormat;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsStartTime
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsStartTime (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 u4Fs6374LMSessionId,
                              UINT4 *pu4RetValFs6374LMStatsStartTime)
{
    tR6374LmStatsTableEntry R6374LmStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LmStatsTableInfo = NULL;

    RFC6374_MEMSET (&R6374LmStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LmStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LmStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LmStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LmStatsTableInfo = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LmStatsTableEntry);
    if (pR6374LmStatsTableInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsStartTime =
        pR6374LmStatsTableInfo->RxMeasurementStartTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsFarEndLossMin
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsFarEndLossMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsFarEndLossMin (UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  UINT4 u4Fs6374LMSessionId,
                                  UINT4 *pu4RetValFs6374LMStatsFarEndLossMin)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsFarEndLossMin =
        pR6374LMStatsTableEntry->u4MinCalcTxLossSender;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsFarEndLossMax
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsFarEndLossMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsFarEndLossMax (UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  UINT4 u4Fs6374LMSessionId,
                                  UINT4 *pu4RetValFs6374LMStatsFarEndLossMax)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsFarEndLossMax =
        pR6374LMStatsTableEntry->u4MaxCalcTxLossSender;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsFarEndLossAvg
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsFarEndLossAvg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsFarEndLossAvg (UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  UINT4 u4Fs6374LMSessionId,
                                  UINT4 *pu4RetValFs6374LMStatsFarEndLossAvg)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsFarEndLossAvg = RFC6374_INIT_VAL;

    if ((pR6374LMStatsTableEntry->u1DyadicMeasurement ==
         RFC6374_DYADIC_MEAS_ENABLE) &&
        (pR6374LMStatsTableEntry->u4LMMRcvd != RFC6374_INIT_VAL))
    {
        *pu4RetValFs6374LMStatsFarEndLossAvg =
            pR6374LMStatsTableEntry->u4AvgCalcTxLossSender /
            pR6374LMStatsTableEntry->u4LMMRcvd;
    }
    else
    {
        if (pR6374LMStatsTableEntry->u4LMRRcvd != RFC6374_INIT_VAL)
        {
            *pu4RetValFs6374LMStatsFarEndLossAvg =
                pR6374LMStatsTableEntry->u4AvgCalcTxLossSender /
                pR6374LMStatsTableEntry->u4LMRRcvd;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsNearEndLossMin
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsNearEndLossMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsNearEndLossMin (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 *pu4RetValFs6374LMStatsNearEndLossMin)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pR6374LMStatsTableEntry->u1LossType == RFC6374_LM_TYPE_ONE_WAY)
    {
        *pu4RetValFs6374LMStatsNearEndLossMin =
            pR6374LMStatsTableEntry->u4MinCalcRxLossReceiver;
    }
    else
    {
        *pu4RetValFs6374LMStatsNearEndLossMin =
            pR6374LMStatsTableEntry->u4MinCalcRxLossSender;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsNearEndLossMax
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsNearEndLossMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsNearEndLossMax (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 *pu4RetValFs6374LMStatsNearEndLossMax)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pR6374LMStatsTableEntry->u1LossType == RFC6374_LM_TYPE_ONE_WAY)
    {
        *pu4RetValFs6374LMStatsNearEndLossMax =
            pR6374LMStatsTableEntry->u4MaxCalcRxLossReceiver;
    }
    else
    {
        *pu4RetValFs6374LMStatsNearEndLossMax =
            pR6374LMStatsTableEntry->u4MaxCalcRxLossSender;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsNearEndLossAvg
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsNearEndLossAvg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsNearEndLossAvg (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 *pu4RetValFs6374LMStatsNearEndLossAvg)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsNearEndLossAvg = RFC6374_INIT_VAL;

    if (pR6374LMStatsTableEntry->u1LossType == RFC6374_LM_TYPE_ONE_WAY)
    {
        if (pR6374LMStatsTableEntry->u41LMRcvd != RFC6374_INIT_VAL)
        {
            *pu4RetValFs6374LMStatsNearEndLossAvg =
                pR6374LMStatsTableEntry->u4AvgCalcRxLossReceiver /
                pR6374LMStatsTableEntry->u41LMRcvd;
        }
    }
    else
    {
        if ((pR6374LMStatsTableEntry->u1DyadicMeasurement ==
             RFC6374_DYADIC_MEAS_ENABLE) &&
            (pR6374LMStatsTableEntry->u4LMMRcvd != RFC6374_INIT_VAL))
        {
            *pu4RetValFs6374LMStatsNearEndLossAvg =
                pR6374LMStatsTableEntry->u4AvgCalcRxLossSender /
                pR6374LMStatsTableEntry->u4LMMRcvd;
        }
        else
        {
            if (pR6374LMStatsTableEntry->u4LMRRcvd != RFC6374_INIT_VAL)
            {
                *pu4RetValFs6374LMStatsNearEndLossAvg =
                    pR6374LMStatsTableEntry->u4AvgCalcRxLossSender /
                    pR6374LMStatsTableEntry->u4LMRRcvd;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsTxPktCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsTxPktCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsTxPktCount (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374LMSessionId,
                               UINT4 *pu4RetValFs6374LMStatsTxPktCount)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsTxPktCount = pR6374LMStatsTableEntry->u4LMMSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsRxPktCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsRxPktCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsRxPktCount (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374LMSessionId,
                               UINT4 *pu4RetValFs6374LMStatsRxPktCount)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pR6374LMStatsTableEntry->u1LossType == RFC6374_LM_TYPE_TWO_WAY)
    {
        if (pR6374LMStatsTableEntry->u1DyadicMeasurement ==
            RFC6374_DYADIC_MEAS_ENABLE)
        {
            *pu4RetValFs6374LMStatsRxPktCount =
                pR6374LMStatsTableEntry->u4LMMRcvd;
        }
        else
        {
            *pu4RetValFs6374LMStatsRxPktCount =
                pR6374LMStatsTableEntry->u4LMRRcvd;
        }
    }
    else
    {
        *pu4RetValFs6374LMStatsRxPktCount = pR6374LMStatsTableEntry->u41LMRcvd;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsThroughputpercent
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsThroughputpercent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsThroughputpercent (UINT4 u4Fs6374ContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFs6374ServiceName,
                                      UINT4 u4Fs6374LMSessionId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFs6374LMStatsThroughputpercent)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RFC6374_FLOAT_TO_OCTETSTRING (pR6374LMStatsTableEntry->f4ThroughPut,
                                      pRetValFs6374LMStatsThroughputpercent) ==
        EOF)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4Fs6374ContextId,
                     "nmhGetFs6374LMStatsThroughputpercent: "
                     "\tSNMP: ThroughPut Conversion Error\n");
        return SNMP_FAILURE;
    }
    pRetValFs6374LMStatsThroughputpercent->i4_Length =
        RFC6374_THROUGH_PUT_LENGTH;

    pRetValFs6374LMStatsThroughputpercent->
        pu1_OctetList[RFC6374_THROUGH_PUT_LENGTH - 1] = RFC6374_INIT_VAL;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsResponseErrors
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsResponseErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsResponseErrors (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   UINT4 *pu4RetValFs6374LMStatsResponseErrors)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMStatsResponseErrors =
        pR6374LMStatsTableEntry->u4NoOfErroredRespRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsResponseTimeout
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsResponseTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsResponseTimeout (UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 u4Fs6374LMSessionId,
                                    UINT4
                                    *pu4RetValFs6374LMStatsResponseTimeout)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pR6374LMStatsTableEntry->u1LossType == RFC6374_LM_TYPE_TWO_WAY)
    {
        if (pR6374LMStatsTableEntry->u1DyadicMeasurement ==
            RFC6374_DYADIC_MEAS_ENABLE)
        {
            *pu4RetValFs6374LMStatsResponseTimeout =
                (pR6374LMStatsTableEntry->u4LMMSent -
                 (pR6374LMStatsTableEntry->u4LMMRcvd +
                  pR6374LMStatsTableEntry->u4NoOfErroredRespRcvd));
        }
        else
        {
            *pu4RetValFs6374LMStatsResponseTimeout =
                (pR6374LMStatsTableEntry->u4LMMSent -
                 (pR6374LMStatsTableEntry->u4LMRRcvd +
                  pR6374LMStatsTableEntry->u4NoOfErroredRespRcvd));
        }
    }
    else
    {
        *pu4RetValFs6374LMStatsResponseTimeout = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMStatsMeasurementOnGoing
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMStatsMeasurementOnGoing
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMStatsMeasurementOnGoing (UINT4 u4Fs6374ContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFs6374ServiceName,
                                       UINT4 u4Fs6374LMSessionId,
                                       INT4
                                       *pi4RetValFs6374LMStatsMeasurementOnGoing)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;
    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));
    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;
    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMStatsMeasurementOnGoing =
        (INT4) pR6374LMStatsTableEntry->u1MeasurementOngoing;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMDyadicMeasurement
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMDyadicMeasurement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMDyadicMeasurement (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4Fs6374LMSessionId,
                                 INT4 *pi4RetValFs6374LMDyadicMeasurement)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;
    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMDyadicMeasurement =
        (INT4) pR6374LMStatsTableEntry->u1DyadicMeasurement;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMRemoteSessionId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMRemoteSessionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMRemoteSessionId (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374LMSessionId,
                               UINT4 *pu4RetValFs6374LMRemoteSessionId)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374LMRemoteSessionId = pR6374LMStatsTableEntry->u4RxSessionId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMDyadicProactiveRole
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374LMDyadicProactiveRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMDyadicProactiveRole (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374LMSessionId,
                                   INT4 *pi4RetValFs6374LMDyadicProactiveRole)
{
    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374LMDyadicProactiveRole =
        pR6374LMStatsTableEntry->u1DyadicProactiveRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374IsLMBufferSQIEnabled
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374LMSessionId

                The Object 
                retValFs6374IsLMBufferSQIEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374IsLMBufferSQIEnabled (UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  UINT4 u4Fs6374LMSessionId,
                                  INT4 *pi4RetValFs6374IsLMBufferSQIEnabled)
{

    tR6374LmStatsTableEntry R6374LMStatsTableEntry;
    tR6374LmStatsTableEntry *pR6374LMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374LMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    R6374LMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374LMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374LMStatsTableEntry.u4SessionId = u4Fs6374LMSessionId;

    pR6374LMStatsTableEntry = (tR6374LmStatsTableEntry *)
        RBTreeGet (RFC6374_LMSTATS_TABLE, (tRBElem *) & R6374LMStatsTableEntry);

    if (pR6374LMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374IsLMBufferSQIEnabled =
        (INT4) pR6374LMStatsTableEntry->b1IsLMBufferSQIEnabled;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374DMStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374DMStatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374DMStatsTable (UINT4 u4Fs6374ContextId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFs6374ServiceName,
                                            UINT4 u4Fs6374DMSessionId)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374DMStatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374DMStatsTable (UINT4 *pu4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 *pu4Fs6374DMSessionId)
{
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGetFirst (RFC6374_DMSTATS_TABLE);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Assign the index */
    *pu4Fs6374ContextId = pR6374DMStatsTableEntry->u4ContextId;

    RFC6374_MEMSET (pFs6374ServiceName->pu1_OctetList, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374DMStatsTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374DMStatsTableEntry->au1ServiceName));
    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374DMStatsTableEntry->au1ServiceName));

    *pu4Fs6374DMSessionId = pR6374DMStatsTableEntry->u4SessionId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374DMStatsTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
                Fs6374DMSessionId
                nextFs6374DMSessionId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374DMStatsTable (UINT4 u4Fs6374ContextId,
                                   UINT4 *pu4NextFs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFs6374ServiceName,
                                   UINT4 u4Fs6374DMSessionId,
                                   UINT4 *pu4NextFs6374DMSessionId)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pNextR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pNextR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGetNext (RFC6374_DMSTATS_TABLE,
                       (tRBElem *) & R6374DMStatsTableEntry, NULL);

    if (pNextR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs6374ContextId = pNextR6374DMStatsTableEntry->u4ContextId;

    RFC6374_MEMSET (pNextFs6374ServiceName->pu1_OctetList, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374DMStatsTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374DMStatsTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pNextR6374DMStatsTableEntry->au1ServiceName));

    *pu4NextFs6374DMSessionId = pNextR6374DMStatsTableEntry->u4SessionId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMplsType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMplsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMplsType (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374DMSessionId,
                             INT4 *pi4RetValFs6374DMStatsMplsType)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMStatsMplsType =
        (INT4) pR6374DMStatsTableEntry->u1MplsPathType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsFwdTnlIdOrPwId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsFwdTnlIdOrPwId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsFwdTnlIdOrPwId (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374DMSessionId,
                                   UINT4 *pu4RetValFs6374DMStatsFwdTnlIdOrPwId)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMStatsFwdTnlIdOrPwId =
        pR6374DMStatsTableEntry->u4ChannelId1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsRevTnlId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsRevTnlId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsRevTnlId (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374DMSessionId,
                             UINT4 *pu4RetValFs6374DMStatsRevTnlId)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMStatsRevTnlId = pR6374DMStatsTableEntry->u4ChannelId2;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsSrcIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsSrcIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsSrcIpAddr (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 u4Fs6374DMSessionId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFs6374DMStatsSrcIpAddr)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RFC6374_MEMCPY (pRetValFs6374DMStatsSrcIpAddr->pu1_OctetList,
                    &pR6374DMStatsTableEntry->u4ChannelIpAddr1,
                    RFC6374_IPV4_ADDR_LEN);
    pRetValFs6374DMStatsSrcIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsDestIpAddr
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsDestIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsDestIpAddr (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFs6374DMStatsDestIpAddr)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RFC6374_MEMCPY (pRetValFs6374DMStatsDestIpAddr->pu1_OctetList,
                    &pR6374DMStatsTableEntry->
                    u4ChannelIpAddr2, RFC6374_IPV4_ADDR_LEN);
    pRetValFs6374DMStatsDestIpAddr->i4_Length = RFC6374_IPV4_ADDR_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsTrafficClass
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsTrafficClass (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4Fs6374DMSessionId,
                                 INT4 *pi4RetValFs6374DMStatsTrafficClass)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMStatsTrafficClass =
        (INT4) pR6374DMStatsTableEntry->u1TrafficClass;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMode
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMode (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4Fs6374DMSessionId,
                         INT4 *pi4RetValFs6374DMStatsMode)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMStatsMode = (INT4) pR6374DMStatsTableEntry->u1DelayMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsType
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsType (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4Fs6374DMSessionId,
                         INT4 *pi4RetValFs6374DMStatsType)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMStatsType = (INT4) pR6374DMStatsTableEntry->u1DelayType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsTimeStampFormat
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsTimeStampFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsTimeStampFormat (UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 u4Fs6374DMSessionId,
                                    INT4 *pi4RetValFs6374DMStatsTimeStampFormat)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMStatsTimeStampFormat =
        (INT4) pR6374DMStatsTableEntry->u1DelayTSFormat;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsStartTime
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsStartTime (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 u4Fs6374DMSessionId,
                              UINT4 *pu4RetValFs6374DMStatsStartTime)
{
    tR6374DmStatsTableEntry R6374DmStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DmStatsTableInfo = NULL;

    RFC6374_MEMSET (&R6374DmStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DmStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DmStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DmStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DmStatsTableInfo = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DmStatsTableEntry);
    if (pR6374DmStatsTableInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMStatsStartTime =
        pR6374DmStatsTableInfo->RxMeasurementStartTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMinDelay
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMinDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMinDelay (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374DMSessionId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFs6374DMStatsMinDelay)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMinDelay->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinPktDelayValue.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinPktDelayValue.u4NanoSeconds);

    pRetValFs6374DMStatsMinDelay->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMaxDelay
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMaxDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMaxDelay (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374DMSessionId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFs6374DMStatsMaxDelay)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMaxDelay->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxPktDelayValue.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxPktDelayValue.u4NanoSeconds);

    pRetValFs6374DMStatsMaxDelay->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsAvgDelay
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsAvgDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsAvgDelay (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 u4Fs6374DMSessionId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFs6374DMStatsAvgDelay)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;
    UINT4               u4AvgSec = RFC6374_INIT_VAL;
    UINT4               u4AvgSecRem = RFC6374_INIT_VAL;
    UINT4               u4AvgNanoSec = RFC6374_INIT_VAL;
    UINT4               u4DmRcvCount = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsAvgDelay->pu1_OctetList;

    if (pR6374DMStatsTableEntry->u1DelayType == RFC6374_DM_TYPE_ONE_WAY)
    {
        u4DmRcvCount = pR6374DMStatsTableEntry->u41DMRcvd;
    }
    else
    {
        /* Check if Delay was through Dyadic */
        if (pR6374DMStatsTableEntry->u1DyadicMeasurement ==
            RFC6374_DYADIC_MEAS_ENABLE)
        {
            u4DmRcvCount = pR6374DMStatsTableEntry->u4DMMRcvd;
        }
        else
        {
            u4DmRcvCount = pR6374DMStatsTableEntry->u4DMRRcvd;
        }
    }
    if (u4DmRcvCount != RFC6374_INIT_VAL)
    {
        u4AvgSec =
            pR6374DMStatsTableEntry->AvgPktDelayValue.u4Seconds / u4DmRcvCount;
        u4AvgSecRem =
            pR6374DMStatsTableEntry->AvgPktDelayValue.u4Seconds % u4DmRcvCount;

        /* Convert the Remainder seconds to Nanoseconds */
        if (u4AvgSecRem)
        {
            u4AvgNanoSec = (RFC6374_NUM_OF_NSEC_IN_A_SEC / u4DmRcvCount) *
                u4AvgSecRem;
        }
        u4AvgNanoSec +=
            pR6374DMStatsTableEntry->AvgPktDelayValue.u4NanoSeconds /
            u4DmRcvCount;
    }
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgSec);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgNanoSec);

    pRetValFs6374DMStatsAvgDelay->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMinRTDelay
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMinRTDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMinRTDelay (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFs6374DMStatsMinRTDelay)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMinRTDelay->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinPktRTDelayValue.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinPktRTDelayValue.
                       u4NanoSeconds);

    pRetValFs6374DMStatsMinRTDelay->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMaxRTDelay
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMaxRTDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMaxRTDelay (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFs6374DMStatsMaxRTDelay)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMaxRTDelay->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxPktRTDelayValue.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxPktRTDelayValue.
                       u4NanoSeconds);

    pRetValFs6374DMStatsMaxRTDelay->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsAvgRTDelay
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsAvgRTDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsAvgRTDelay (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFs6374DMStatsAvgRTDelay)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;
    UINT4               u4AvgSec = RFC6374_INIT_VAL;
    UINT4               u4AvgNanoSec = RFC6374_INIT_VAL;
    UINT4               u4DmRcvCount = RFC6374_INIT_VAL;
    UINT4               u4AvgSecRem = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsAvgRTDelay->pu1_OctetList;

    /* Check if Delay was done through Dyadic */
    if (pR6374DMStatsTableEntry->u1DyadicMeasurement ==
        RFC6374_DYADIC_MEAS_ENABLE)
    {
        u4DmRcvCount = pR6374DMStatsTableEntry->u4DMMRcvd;
    }
    else
    {
        u4DmRcvCount = pR6374DMStatsTableEntry->u4DMRRcvd;
    }

    if (u4DmRcvCount != RFC6374_INIT_VAL)
    {
        u4AvgSec =
            pR6374DMStatsTableEntry->AvgPktRTDelayValue.u4Seconds /
            u4DmRcvCount;

        u4AvgSecRem =
            pR6374DMStatsTableEntry->AvgPktRTDelayValue.u4Seconds %
            u4DmRcvCount;

        /* Convert the Remainder seconds to Nanoseconds */
        if (u4AvgSecRem)
        {
            u4AvgNanoSec = (RFC6374_NUM_OF_NSEC_IN_A_SEC / u4DmRcvCount) *
                u4AvgSecRem;
        }
        u4AvgNanoSec +=
            pR6374DMStatsTableEntry->AvgPktRTDelayValue.u4NanoSeconds /
            u4DmRcvCount;

    }
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgSec);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgNanoSec);

    pRetValFs6374DMStatsAvgRTDelay->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMinIPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMinIPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMinIPDV (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4Fs6374DMSessionId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFs6374DMStatsMinIPDV)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMinIPDV->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinInterPktDelayVariation.
                       u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinInterPktDelayVariation.
                       u4NanoSeconds);

    pRetValFs6374DMStatsMinIPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMaxIPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMaxIPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMaxIPDV (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4Fs6374DMSessionId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFs6374DMStatsMaxIPDV)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMaxIPDV->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxInterPktDelayVariation.
                       u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxInterPktDelayVariation.
                       u4NanoSeconds);

    pRetValFs6374DMStatsMaxIPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsAvgIPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsAvgIPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsAvgIPDV (UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4Fs6374DMSessionId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFs6374DMStatsAvgIPDV)
{

    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;
    UINT4               u4AvgSec = RFC6374_INIT_VAL;
    UINT4               u4AvgNanoSec = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsAvgIPDV->pu1_OctetList;

    if (pR6374DMStatsTableEntry->u41DMRcvd != RFC6374_INIT_VAL)
    {
        u4AvgSec =
            pR6374DMStatsTableEntry->AvgInterPktDelayVariation.u4Seconds /
            pR6374DMStatsTableEntry->u41DMRcvd;
        u4AvgNanoSec =
            pR6374DMStatsTableEntry->AvgInterPktDelayVariation.u4NanoSeconds /
            pR6374DMStatsTableEntry->u41DMRcvd;
    }
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgSec);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgNanoSec);

    pRetValFs6374DMStatsAvgIPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMinPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMinPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMinPDV (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4Fs6374DMSessionId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFs6374DMStatsMinPDV)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMinPDV->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinPktDelayVariation.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MinPktDelayVariation.
                       u4NanoSeconds);

    pRetValFs6374DMStatsMinPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMaxPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMaxPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMaxPDV (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4Fs6374DMSessionId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFs6374DMStatsMaxPDV)
{
    UINT1              *pu1TxTimeStampf = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsMaxPDV->pu1_OctetList;
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxPktDelayVariation.u4Seconds);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf,
                       pR6374DMStatsTableEntry->MaxPktDelayVariation.
                       u4NanoSeconds);

    pRetValFs6374DMStatsMaxPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsAvgPDV
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsAvgPDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsAvgPDV (UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4Fs6374DMSessionId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFs6374DMStatsAvgPDV)
{

    UINT1              *pu1TxTimeStampf = NULL;
    UINT4               u4AvgSec = RFC6374_INIT_VAL;
    UINT4               u4AvgNanoSec = RFC6374_INIT_VAL;

    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu1TxTimeStampf = pRetValFs6374DMStatsAvgPDV->pu1_OctetList;

    if (pR6374DMStatsTableEntry->u41DMRcvd != RFC6374_INIT_VAL)
    {
        u4AvgSec =
            pR6374DMStatsTableEntry->AvgPktDelayVariation.u4Seconds /
            pR6374DMStatsTableEntry->u41DMRcvd;
        u4AvgNanoSec =
            pR6374DMStatsTableEntry->AvgPktDelayVariation.u4NanoSeconds /
            pR6374DMStatsTableEntry->u41DMRcvd;
    }
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgSec);
    RFC6374_PUT_4BYTE (pu1TxTimeStampf, u4AvgNanoSec);

    pRetValFs6374DMStatsAvgPDV->i4_Length = RFC6374_TIMESTAMP_FIELD_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsTxPktCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsTxPktCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsTxPktCount (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               UINT4 *pu4RetValFs6374DMStatsTxPktCount)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMStatsTxPktCount = pR6374DMStatsTableEntry->u4DMMSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsRxPktCount
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsRxPktCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsRxPktCount (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               UINT4 *pu4RetValFs6374DMStatsRxPktCount)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pR6374DMStatsTableEntry->u1DelayType == RFC6374_DM_TYPE_TWO_WAY)
    {
        if (pR6374DMStatsTableEntry->u1DyadicMeasurement ==
            RFC6374_DYADIC_MEAS_ENABLE)
        {
            *pu4RetValFs6374DMStatsRxPktCount =
                pR6374DMStatsTableEntry->u4DMMRcvd;
        }
        else
        {
            *pu4RetValFs6374DMStatsRxPktCount =
                pR6374DMStatsTableEntry->u4DMRRcvd;
        }
    }
    else
    {
        *pu4RetValFs6374DMStatsRxPktCount = pR6374DMStatsTableEntry->u41DMRcvd;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsResponseErrors
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsResponseErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsResponseErrors (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374DMSessionId,
                                   UINT4 *pu4RetValFs6374DMStatsResponseErrors)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMStatsResponseErrors =
        pR6374DMStatsTableEntry->u4NoOfErroredRespRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsResponseTimeout
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsResponseTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsResponseTimeout (UINT4 u4Fs6374ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFs6374ServiceName,
                                    UINT4 u4Fs6374DMSessionId,
                                    UINT4
                                    *pu4RetValFs6374DMStatsResponseTimeout)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pR6374DMStatsTableEntry->u1DelayType == RFC6374_DM_TYPE_TWO_WAY)
    {
        if (pR6374DMStatsTableEntry->u1DyadicMeasurement ==
            RFC6374_DYADIC_MEAS_ENABLE)
        {
            *pu4RetValFs6374DMStatsResponseTimeout =
                (pR6374DMStatsTableEntry->u4DMMSent -
                 (pR6374DMStatsTableEntry->u4DMMRcvd +
                  pR6374DMStatsTableEntry->u4NoOfErroredRespRcvd));
        }
        else
        {
            *pu4RetValFs6374DMStatsResponseTimeout =
                (pR6374DMStatsTableEntry->u4DMMSent -
                 (pR6374DMStatsTableEntry->u4DMRRcvd +
                  pR6374DMStatsTableEntry->u4NoOfErroredRespRcvd));
        }
    }
    else
    {
        *pu4RetValFs6374DMStatsResponseTimeout = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsMeasurementOnGoing
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsMeasurementOnGoing
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsMeasurementOnGoing (UINT4 u4Fs6374ContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFs6374ServiceName,
                                       UINT4 u4Fs6374DMSessionId,
                                       INT4
                                       *pi4RetValFs6374DMStatsMeasurementOnGoing)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMStatsMeasurementOnGoing =
        (INT4) pR6374DMStatsTableEntry->u1MeasurementOngoing;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMDyadicMeasurement
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMDyadicMeasurement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMDyadicMeasurement (UINT4 u4Fs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 UINT4 u4Fs6374DMSessionId,
                                 INT4 *pi4RetValFs6374DMDyadicMeasurement)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMDyadicMeasurement =
        (INT4) pR6374DMStatsTableEntry->u1DyadicMeasurement;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMRemoteSessionId
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMRemoteSessionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMRemoteSessionId (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 u4Fs6374DMSessionId,
                               UINT4 *pu4RetValFs6374DMRemoteSessionId)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMRemoteSessionId = pR6374DMStatsTableEntry->u4RxSessionId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMDyadicProactiveRole
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMDyadicProactiveRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMDyadicProactiveRole (UINT4 u4Fs6374ContextId,
                                   tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                   UINT4 u4Fs6374DMSessionId,
                                   INT4 *pi4RetValFs6374DMDyadicProactiveRole)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374DMDyadicProactiveRole =
        pR6374DMStatsTableEntry->u1DyadicProactiveRole;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMStatsPaddingSize
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374DMStatsPaddingSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMStatsPaddingSize (UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                UINT4 u4Fs6374DMSessionId,
                                UINT4 *pu4RetValFs6374DMStatsPaddingSize)
{
    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374DMStatsPaddingSize = pR6374DMStatsTableEntry->u4PadSize;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs6374IsDMBufferSQIEnabled
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
                Fs6374DMSessionId

                The Object 
                retValFs6374IsDMBufferSQIEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374IsDMBufferSQIEnabled (UINT4 u4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                  UINT4 u4Fs6374DMSessionId,
                                  INT4 *pi4RetValFs6374IsDMBufferSQIEnabled)
{

    tR6374DmStatsTableEntry R6374DMStatsTableEntry;
    tR6374DmStatsTableEntry *pR6374DMStatsTableEntry = NULL;

    RFC6374_MEMSET (&R6374DMStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    R6374DMStatsTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374DMStatsTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    R6374DMStatsTableEntry.u4SessionId = u4Fs6374DMSessionId;

    pR6374DMStatsTableEntry = (tR6374DmStatsTableEntry *)
        RBTreeGet (RFC6374_DMSTATS_TABLE, (tRBElem *) & R6374DMStatsTableEntry);

    if (pR6374DMStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFs6374IsDMBufferSQIEnabled =
        (INT4) pR6374DMStatsTableEntry->b1IsDMBufferSQIEnabled;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374StatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374StatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374StatsTable (UINT4 u4Fs6374ContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374StatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374StatsTable (UINT4 *pu4Fs6374ContextId,
                                  tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4Fs6374ContextId = pR6374ServiceConfigTableEntry->u4ContextId;

    RFC6374_MEMCPY (pFs6374ServiceName->pu1_OctetList,
                    pR6374ServiceConfigTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pR6374ServiceConfigTableEntry->
                                    au1ServiceName));

    pFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN (pR6374ServiceConfigTableEntry->au1ServiceName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374StatsTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
                Fs6374ServiceName
                nextFs6374ServiceName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374StatsTable (UINT4 u4Fs6374ContextId,
                                 UINT4 *pu4NextFs6374ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFs6374ServiceName)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pNextR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pNextR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                       (tRBElem *) & R6374ServiceConfigTableEntry, NULL);

    if (pNextR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFs6374ContextId = pNextR6374ServiceConfigTableEntry->u4ContextId;

    RFC6374_MEMCPY (pNextFs6374ServiceName->pu1_OctetList,
                    pNextR6374ServiceConfigTableEntry->au1ServiceName,
                    RFC6374_STRLEN (pNextR6374ServiceConfigTableEntry->
                                    au1ServiceName));
    pNextFs6374ServiceName->i4_Length =
        (INT4) (RFC6374_STRLEN
                (pNextR6374ServiceConfigTableEntry->au1ServiceName));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374StatsLmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsLmmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsLmmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 *pu4RetValFs6374StatsLmmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsLmmOut =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsLmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsLmmIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsLmmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 *pu4RetValFs6374StatsLmmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsLmmIn =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsLmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsLmrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsLmrOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 *pu4RetValFs6374StatsLmrOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsLmrOut =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmrOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsLmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsLmrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsLmrIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 *pu4RetValFs6374StatsLmrIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsLmrIn =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374Stats1LmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374Stats1LmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374Stats1LmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 *pu4RetValFs6374Stats1LmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374Stats1LmOut =
        pR6374ServiceConfigTableEntry->Stats.u4Stats1LmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374Stats1LmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374Stats1LmIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374Stats1LmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 *pu4RetValFs6374Stats1LmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374Stats1LmIn =
        pR6374ServiceConfigTableEntry->Stats.u4Stats1LmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374Stats1DmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374Stats1DmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374Stats1DmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 *pu4RetValFs6374Stats1DmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374Stats1DmOut =
        pR6374ServiceConfigTableEntry->Stats.u4Stats1DmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374Stats1DmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374Stats1DmIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374Stats1DmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 *pu4RetValFs6374Stats1DmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374Stats1DmIn =
        pR6374ServiceConfigTableEntry->Stats.u4Stats1DmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsDmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsDmmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsDmmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 *pu4RetValFs6374StatsDmmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFs6374StatsDmmOut =
        pR6374ServiceConfigTableEntry->Stats.u4StatsDmmOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsDmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsDmmIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsDmmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 *pu4RetValFs6374StatsDmmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFs6374StatsDmmIn =
        pR6374ServiceConfigTableEntry->Stats.u4StatsDmmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsDmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsDmrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsDmrOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 *pu4RetValFs6374StatsDmrOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsDmrOut =
        pR6374ServiceConfigTableEntry->Stats.u4StatsDmrOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsDmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsDmrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsDmrIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 *pu4RetValFs6374StatsDmrIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsDmrIn =
        pR6374ServiceConfigTableEntry->Stats.u4StatsDmrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsCmb1LmDmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsCmb1LmDmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsCmb1LmDmOut (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 *pu4RetValFs6374StatsCmb1LmDmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsCmb1LmDmOut =
        pR6374ServiceConfigTableEntry->Stats.u4Stats1LmDmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsCmb1LmDmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsCmb1LmDmIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsCmb1LmDmIn (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                             UINT4 *pu4RetValFs6374StatsCmb1LmDmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsCmb1LmDmIn =
        pR6374ServiceConfigTableEntry->Stats.u4Stats1LmDmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsCmbLmmDmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsCmbLmmDmmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsCmbLmmDmmOut (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 *pu4RetValFs6374StatsCmbLmmDmmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsCmbLmmDmmOut =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmmDmmOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsCmbLmmDmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsCmbLmmDmmIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsCmbLmmDmmIn (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 *pu4RetValFs6374StatsCmbLmmDmmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsCmbLmmDmmIn =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmmDmmIn;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsCmbLmrDmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsCmbLmrDmrOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsCmbLmrDmrOut (UINT4 u4Fs6374ContextId,
                               tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                               UINT4 *pu4RetValFs6374StatsCmbLmrDmrOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsCmbLmrDmrOut =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmrDmrOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374StatsCmbLmrDmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                retValFs6374StatsCmbLmrDmrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374StatsCmbLmrDmrIn (UINT4 u4Fs6374ContextId,
                              tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                              UINT4 *pu4RetValFs6374StatsCmbLmrDmrIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFs6374StatsCmbLmrDmrIn =
        pR6374ServiceConfigTableEntry->Stats.u4StatsLmrDmrIn;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs6374StatsLmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsLmmOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsLmmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4SetValFs6374StatsLmmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsLmmOut =
        u4SetValFs6374StatsLmmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsLmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsLmmIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsLmmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 u4SetValFs6374StatsLmmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsLmmIn =
        u4SetValFs6374StatsLmmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsLmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsLmrOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsLmrOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4SetValFs6374StatsLmrOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsLmrOut =
        u4SetValFs6374StatsLmrOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsLmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsLmrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsLmrIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 u4SetValFs6374StatsLmrIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsLmrIn =
        u4SetValFs6374StatsLmrIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374Stats1LmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374Stats1LmOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374Stats1LmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4SetValFs6374Stats1LmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4Stats1LmOut =
        u4SetValFs6374Stats1LmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374Stats1LmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374Stats1LmIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374Stats1LmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 u4SetValFs6374Stats1LmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4Stats1LmIn =
        u4SetValFs6374Stats1LmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374Stats1DmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374Stats1DmOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374Stats1DmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4SetValFs6374Stats1DmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4Stats1DmOut =
        u4SetValFs6374Stats1DmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374Stats1DmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374Stats1DmIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374Stats1DmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 u4SetValFs6374Stats1DmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4Stats1DmIn =
        u4SetValFs6374Stats1DmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsDmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsDmmOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsDmmOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4SetValFs6374StatsDmmOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsDmmOut =
        u4SetValFs6374StatsDmmOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsDmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsDmmIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsDmmIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 u4SetValFs6374StatsDmmIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsDmmIn =
        u4SetValFs6374StatsDmmIn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsDmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsDmrOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsDmrOut (UINT4 u4Fs6374ContextId,
                         tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                         UINT4 u4SetValFs6374StatsDmrOut)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsDmrOut =
        u4SetValFs6374StatsDmrOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374StatsDmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                setValFs6374StatsDmrIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374StatsDmrIn (UINT4 u4Fs6374ContextId,
                        tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                        UINT4 u4SetValFs6374StatsDmrIn)
{
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4Fs6374ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pFs6374ServiceName->pu1_OctetList,
                    pFs6374ServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pR6374ServiceConfigTableEntry->Stats.u4StatsDmrIn =
        u4SetValFs6374StatsDmrIn;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsLmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsLmmOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsLmmOut (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4TestValFs6374StatsLmmOut)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsLmmOut != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsLmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsLmmIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsLmmIn (UINT4 *pu4ErrorCode,
                           UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4TestValFs6374StatsLmmIn)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsLmmIn != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsLmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsLmrOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsLmrOut (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4TestValFs6374StatsLmrOut)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsLmrOut != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsLmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsLmrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsLmrIn (UINT4 *pu4ErrorCode,
                           UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4TestValFs6374StatsLmrIn)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsLmrIn != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374Stats1LmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374Stats1LmOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374Stats1LmOut (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4TestValFs6374Stats1LmOut)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374Stats1LmOut != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374Stats1LmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374Stats1LmIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374Stats1LmIn (UINT4 *pu4ErrorCode,
                           UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4TestValFs6374Stats1LmIn)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374Stats1LmIn != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374Stats1DmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374Stats1DmOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374Stats1DmOut (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4TestValFs6374Stats1DmOut)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374Stats1DmOut != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374Stats1DmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374Stats1DmIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374Stats1DmIn (UINT4 *pu4ErrorCode,
                           UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4TestValFs6374Stats1DmIn)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374Stats1DmIn != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsDmmOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsDmmOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsDmmOut (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4TestValFs6374StatsDmmOut)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsDmmOut != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsDmmIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsDmmIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsDmmIn (UINT4 *pu4ErrorCode,
                           UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4TestValFs6374StatsDmmIn)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsDmmIn != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsDmrOut
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsDmrOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsDmrOut (UINT4 *pu4ErrorCode,
                            UINT4 u4Fs6374ContextId,
                            tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                            UINT4 u4TestValFs6374StatsDmrOut)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsDmrOut != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374StatsDmrIn
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName

                The Object 
                testValFs6374StatsDmrIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374StatsDmrIn (UINT4 *pu4ErrorCode,
                           UINT4 u4Fs6374ContextId,
                           tSNMP_OCTET_STRING_TYPE * pFs6374ServiceName,
                           UINT4 u4TestValFs6374StatsDmrIn)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pFs6374ServiceName);
    if (u4TestValFs6374StatsDmrIn != RFC6374_INIT_VAL)    /*ZERO allowed as 
                                                           this param is only 
                                                           for clearing the 
                                                           counters */
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs6374StatsTable
 Input       :  The Indices
                Fs6374ContextId
                Fs6374ServiceName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs6374StatsTable (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374SystemConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374SystemConfigTable
 Input       :  The Indices
                Fs6374ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374SystemConfigTable (UINT4 u4Fs6374ContextId)
{
    if (u4Fs6374ContextId != RFC6374_DEFAULT_CONTEXT_ID)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374SystemConfigTable
 Input       :  The Indices
                Fs6374ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374SystemConfigTable (UINT4 *pu4Fs6374ContextId)
{
    *pu4Fs6374ContextId = RFC6374_DEFAULT_CONTEXT_ID;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374SystemConfigTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374SystemConfigTable (UINT4 u4Fs6374ContextId,
                                        UINT4 *pu4NextFs6374ContextId)
{
    if (gR6374GlobalInfo.u1SystemControl[u4Fs6374ContextId + 1]
        != RFC6374_START)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFs6374ContextId = u4Fs6374ContextId + 1;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374SystemControl
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374SystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374SystemControl (UINT4 u4Fs6374ContextId,
                           INT4 *pi4RetValFs6374SystemControl)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    *pi4RetValFs6374SystemControl =
        (INT4) gR6374GlobalInfo.u1SystemControl[RFC6374_DEFAULT_CONTEXT_ID];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374ModuleStatus
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374ModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374ModuleStatus (UINT4 u4Fs6374ContextId,
                          INT4 *pi4RetValFs6374ModuleStatus)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    *pi4RetValFs6374ModuleStatus = (INT4) gR6374GlobalInfo.u1ModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DMBufferClear
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374DMBufferClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DMBufferClear (UINT4 u4Fs6374ContextId,
                           INT4 *pi4RetValFs6374DMBufferClear)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    *pi4RetValFs6374DMBufferClear = (INT4) gR6374GlobalInfo.u1ClearDM;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374LMBufferClear
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374LMBufferClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMBufferClear (UINT4 u4Fs6374ContextId,
                           INT4 *pi4RetValFs6374LMBufferClear)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    *pi4RetValFs6374LMBufferClear = (INT4) gR6374GlobalInfo.u1ClearLM;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374DebugLevel
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374DebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374DebugLevel (UINT4 u4Fs6374ContextId,
                        UINT4 *pu4RetValFs6374DebugLevel)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    *pu4RetValFs6374DebugLevel = gR6374GlobalInfo.u4TraceLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs6374TSFormatSupported
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374TSFormatSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374TSFormatSupported (UINT4 u4Fs6374ContextId,
                               INT4 *pi4RetValFs6374TSFormatSupported)
{
    UNUSED_PARAM (u4Fs6374ContextId);

    *pi4RetValFs6374TSFormatSupported =
        (INT4) gR6374GlobalInfo.u1SysSuppTimeFormat;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs6374SystemControl
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                setValFs6374SystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374SystemControl (UINT4 u4Fs6374ContextId,
                           INT4 i4SetValFs6374SystemControl)
{
    tR6374ServiceConfigTableEntry *pServiceConfigEntry = NULL;
    tR6374ServiceConfigTableEntry *pNextServiceConfigEntry = NULL;
    UINT4               u4RetVal = RFC6374_SUCCESS;
    BOOL1               b1IfAlreadyRegister = RFC6374_FALSE;

    /* De-Regester with MPLS module Path Status event if PM is shuting Down */
    if (i4SetValFs6374SystemControl == RFC6374_SHUTDOWN)
    {
        /* De-Reg for all the service's */
        pServiceConfigEntry = RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

        while (pServiceConfigEntry != NULL)
        {
            if (pServiceConfigEntry->u4ContextId == u4Fs6374ContextId)
            {
                /* Validate the MPLS PATH configured
                 * and the DE-REG MPLS for Path UP/DOWN Event */
                if (R6374UtilValidateMplsPathParams (pServiceConfigEntry,
                                                     &b1IfAlreadyRegister) !=
                    RFC6374_FAILURE)
                {
                    /* If Validation is success now DE-REG for EVENT */
                    if (R6374RegMplsPath (RFC6374_FALSE, pServiceConfigEntry) !=
                        RFC6374_SUCCESS)
                    {
                        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                     RFC6374_ALL_FAILURE_TRC, u4Fs6374ContextId,
                                     "nmhSetFs6374SystemControl: "
                                     "\tSNMP:DE-REG Event with MPLS Failed\r\n");
                        return SNMP_FAILURE;
                    }
                }
            }
            pNextServiceConfigEntry =
                RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                               (tRBElem *) pServiceConfigEntry, NULL);
            pServiceConfigEntry = pNextServiceConfigEntry;
        }
    }

    if (gR6374GlobalInfo.u1SystemControl[u4Fs6374ContextId]
        != (UINT1) i4SetValFs6374SystemControl)
    {
        if (i4SetValFs6374SystemControl == RFC6374_START)
        {
            u4RetVal = R6374UtlStartSystemControl ();
        }
        else
        {
            R6374UtlShutSystemControl (u4Fs6374ContextId);
        }
        if (u4RetVal == RFC6374_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    gR6374GlobalInfo.u1SystemControl[u4Fs6374ContextId] =
        (UINT1) i4SetValFs6374SystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374ModuleStatus
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                setValFs6374ModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374ModuleStatus (UINT4 u4Fs6374ContextId,
                          INT4 i4SetValFs6374ModuleStatus)
{
    tR6374ServiceConfigTableEntry *pServiceConfigEntry = NULL;
    tR6374ServiceConfigTableEntry *pNextServiceConfigEntry = NULL;
    BOOL1               b1IfAlreadyRegister = RFC6374_FALSE;

    if (gR6374GlobalInfo.u1ModuleStatus != (UINT1) i4SetValFs6374ModuleStatus)
    {
        /* De-Regester with MPLS module Path Status event if PM is Disabled */
        /* De-Reg for all the service's */
        pServiceConfigEntry = RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

        while (pServiceConfigEntry != NULL)
        {
            if (pServiceConfigEntry->u4ContextId == u4Fs6374ContextId)
            {
                /* Validate the MPLS PATH configured
                 * and the DE - REG MPLS for Path UP/DOWN Event */
                if (R6374UtilValidateMplsPathParams (pServiceConfigEntry,
                                                     &b1IfAlreadyRegister) !=
                    RFC6374_FAILURE)
                {
                    /* If Validation is success now DE-REG for EVENT */
                    if (i4SetValFs6374ModuleStatus == RFC6374_ENABLED)
                    {
                        if (R6374RegMplsPath (RFC6374_TRUE, pServiceConfigEntry)
                            != RFC6374_SUCCESS)
                        {
                            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                         RFC6374_ALL_FAILURE_TRC,
                                         u4Fs6374ContextId,
                                         "nmhSetFs6374ModuleStatus: "
                                         "\tSNMP:REG Event with MPLS Failed\r\n");
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        if (R6374RegMplsPath
                            (RFC6374_FALSE,
                             pServiceConfigEntry) != RFC6374_SUCCESS)
                        {
                            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                         RFC6374_ALL_FAILURE_TRC,
                                         u4Fs6374ContextId,
                                         "nmhSetFs6374ModuleStatus: "
                                         "\tSNMP:DE-REG Event with MPLS Failed\r\n");
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
            pNextServiceConfigEntry =
                RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                               (tRBElem *) pServiceConfigEntry, NULL);
            pServiceConfigEntry = pNextServiceConfigEntry;
        }

        if (i4SetValFs6374ModuleStatus == RFC6374_ENABLED)
        {
            R6374UtlEnableModuleStatus ();
        }
        else
        {
            R6374UtlDisableModuleStatus ();
        }
    }
    gR6374GlobalInfo.u1ModuleStatus = (UINT1) i4SetValFs6374ModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DMBufferClear
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                setValFs6374DMBufferClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DMBufferClear (UINT4 u4Fs6374ContextId,
                           INT4 i4SetValFs6374DMBufferClear)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    gR6374GlobalInfo.u1ClearDM = (UINT1) i4SetValFs6374DMBufferClear;
    /* Clear the Buffer */
    RBTreeDrain (RFC6374_DMBUFFER_TABLE,
                 (tRBKeyFreeFn) R6374UtlFreeEntryFn, RFC6374_DMBUFFER_ENTRY);
    /* Clear the Stats */
    RBTreeDrain (RFC6374_DMSTATS_TABLE,
                 (tRBKeyFreeFn) R6374UtlFreeEntryFn, RFC6374_DMSTATS_ENTRY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374LMBufferClear
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                setValFs6374LMBufferClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMBufferClear (UINT4 u4Fs6374ContextId,
                           INT4 i4SetValFs6374LMBufferClear)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    gR6374GlobalInfo.u1ClearLM = (UINT1) i4SetValFs6374LMBufferClear;
    /* Clear the Buffer */
    RBTreeDrain (RFC6374_LMBUFFER_TABLE,
                 (tRBKeyFreeFn) R6374UtlFreeEntryFn, RFC6374_LMBUFFER_ENTRY);
    /* Clear the Stats */
    RBTreeDrain (RFC6374_LMSTATS_TABLE,
                 (tRBKeyFreeFn) R6374UtlFreeEntryFn, RFC6374_LMSTATS_ENTRY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs6374DebugLevel
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                setValFs6374DebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374DebugLevel (UINT4 u4Fs6374ContextId, UINT4 u4SetValFs6374DebugLevel)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    /* Set the trace option,
       BIT 0 - Initialisation and Shutdown Trace.
       BIT 1 - Management trace.
       BIT 2 - Critical trace.
       BIT 3 - Control plane.
       BIT 4 - Packet Dump.
       BIT 5 - OS Resource trace.
       BIT 6 - All Failure trace (All failures including Packet Validation).
       BIT 7 - Function Entry.
       BIT 8 - Function Exit.
       Multiple options can be selected by setting multiple bits */
    gR6374GlobalInfo.u4TraceLevel = u4SetValFs6374DebugLevel;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs6374SystemControl
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                testValFs6374SystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374SystemControl (UINT4 *pu4ErrorCode,
                              UINT4 u4Fs6374ContextId,
                              INT4 i4TestValFs6374SystemControl)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    if (i4TestValFs6374SystemControl != RFC6374_START &&
        i4TestValFs6374SystemControl != RFC6374_SHUTDOWN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374ModuleStatus
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                testValFs6374ModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374ModuleStatus (UINT4 *pu4ErrorCode,
                             UINT4 u4Fs6374ContextId,
                             INT4 i4TestValFs6374ModuleStatus)
{
    UNUSED_PARAM (u4Fs6374ContextId);
    if (i4TestValFs6374ModuleStatus != RFC6374_ENABLED &&
        i4TestValFs6374ModuleStatus != RFC6374_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DMBufferClear
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                testValFs6374DMBufferClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DMBufferClear (UINT4 *pu4ErrorCode,
                              UINT4 u4Fs6374ContextId,
                              INT4 i4TestValFs6374DMBufferClear)
{
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;

    pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
        RBTreeGetFirst (RFC6374_DMBUFFER_TABLE);

    if (pR6374DMBufferTableEntry == NULL)
    {
        CLI_SET_ERR (RFC6374_CLI_DM_TABLE_EMPTY);
        return SNMP_FAILURE;
    }
    if (i4TestValFs6374DMBufferClear != RFC6374_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (R6374UtlIsDMMeasOngoing (u4Fs6374ContextId) != RFC6374_FALSE)
    {
        CLI_SET_ERR (RFC6374_CLI_CLEAR_BUFFER_NOT_POSSIBLE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMBufferClear
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                testValFs6374LMBufferClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMBufferClear (UINT4 *pu4ErrorCode,
                              UINT4 u4Fs6374ContextId,
                              INT4 i4TestValFs6374LMBufferClear)
{
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;

    pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
        RBTreeGetFirst (RFC6374_LMBUFFER_TABLE);

    if (pR6374LMBufferTableEntry == NULL)
    {
        CLI_SET_ERR (RFC6374_CLI_LM_TABLE_EMPTY);
        return SNMP_FAILURE;
    }
    if (i4TestValFs6374LMBufferClear != RFC6374_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (R6374UtlIsLMMeasOngoing (u4Fs6374ContextId) != RFC6374_FALSE)
    {
        CLI_SET_ERR (RFC6374_CLI_CLEAR_BUFFER_NOT_POSSIBLE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs6374DebugLevel
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                testValFs6374DebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374DebugLevel (UINT4 *pu4ErrorCode, UINT4 u4Fs6374ContextId,
                           UINT4 u4TestValFs6374DebugLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (u4TestValFs6374DebugLevel);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs6374SystemConfigTable
 Input       :  The Indices
                Fs6374ContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs6374SystemConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList
                                 * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs6374NotificationsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs6374NotificationsTable
 Input       :  The Indices
                Fs6374ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs6374NotificationsTable (UINT4 u4Fs6374ContextId)
{
    if (u4Fs6374ContextId != RFC6374_DEFAULT_CONTEXT_ID)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs6374NotificationsTable
 Input       :  The Indices
                Fs6374ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs6374NotificationsTable (UINT4 *pu4Fs6374ContextId)
{
    *pu4Fs6374ContextId = RFC6374_DEFAULT_CONTEXT_ID;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs6374NotificationsTable
 Input       :  The Indices
                Fs6374ContextId
                nextFs6374ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs6374NotificationsTable (UINT4 u4Fs6374ContextId,
                                         UINT4 *pu4NextFs6374ContextId)
{
    if (gR6374GlobalInfo.u1SystemControl[u4Fs6374ContextId + 1]
        != RFC6374_START)
    {
        return SNMP_FAILURE;
    }
    *pu4NextFs6374ContextId = u4Fs6374ContextId + 1;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs6374LMDMTrapControl
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                retValFs6374LMDMTrapControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs6374LMDMTrapControl (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFs6374LMDMTrapControl)
{
    UINT1              *pu1TrapControl = NULL;

    pu1TrapControl = pRetValFs6374LMDMTrapControl->pu1_OctetList;

    RFC6374_PUT_2BYTE (pu1TrapControl, gR6374GlobalInfo.u2TrapControl);

    pRetValFs6374LMDMTrapControl->i4_Length = sizeof (UINT2);

    UNUSED_PARAM (u4Fs6374ContextId);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs6374LMDMTrapControl
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                setValFs6374LMDMTrapControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs6374LMDMTrapControl (UINT4 u4Fs6374ContextId,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFs6374LMDMTrapControl)
{
    UINT1              *pu1TrapControl = NULL;

    pu1TrapControl = pSetValFs6374LMDMTrapControl->pu1_OctetList;

    RFC6374_GET_2BYTE (gR6374GlobalInfo.u2TrapControl, pu1TrapControl);

    UNUSED_PARAM (u4Fs6374ContextId);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs6374LMDMTrapControl
 Input       :  The Indices
                Fs6374ContextId

                The Object 
                testValFs6374LMDMTrapControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs6374LMDMTrapControl (UINT4 *pu4ErrorCode,
                                UINT4 u4Fs6374ContextId,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFs6374LMDMTrapControl)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Fs6374ContextId);
    UNUSED_PARAM (pTestValFs6374LMDMTrapControl);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs6374NotificationsTable
 Input       :  The Indices
                Fs6374ContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs6374NotificationsTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/********************************************************************************
 * FUNCTION NAME    : R6374LwUtilValidateServParams                             *
 *                                                                              *
 * DESCRIPTION      : This function will check whether all the mandatory        *
 *                    parameters before changing the mode as proactive          *
 *                    or before starting the test                               *
 *                                                                              *
 * INPUT            : pServiceInfo - Pointer to ServiceInfo Entry               *
 *                    u1TestType - Test Type Lm/Dm/Combined LmDm                *
 *                                                                              *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code                  *
 *                                                                              *
 * RETURNS          : RFC6374_SUCCESS/RFC6374_FAILURE                           *
 *                                                                              *
 *******************************************************************************/
PRIVATE INT4
R6374LwUtilValidateServParams (tR6374ServiceConfigTableEntry * pServiceInfo,
                               UINT1 u1TestType, UINT4 *pu4ErrorCode)
{
    UINT1               u1MplsPathType = RFC6374_INIT_VAL;
    UINT4               u4SrcIpAddr = RFC6374_INIT_VAL;
    UINT4               u4DestIpAddr = RFC6374_INIT_VAL;
    UINT4               u4PwId = RFC6374_INIT_VAL;
    UINT4               u4PwIpAddr = RFC6374_INIT_VAL;

    if (pServiceInfo == NULL)
    {
        return RFC6374_FAILURE;
    }

    u1MplsPathType = pServiceInfo->R6374MplsParams.u1MplsPathType;
    u4SrcIpAddr = pServiceInfo->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;
    u4DestIpAddr = pServiceInfo->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
    u4PwIpAddr = pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr;
    u4PwId = pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId;

    if (u1TestType == RFC6374_PROACTIVE_TEST_START)
    {
        if (((u1MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL) &&
             (u4SrcIpAddr != RFC6374_INIT_VAL) &&
             (u4DestIpAddr != RFC6374_INIT_VAL)) ||
            ((u1MplsPathType == RFC6374_MPLS_PATH_TYPE_PW) &&
             (u4PwIpAddr != RFC6374_INIT_VAL) && (u4PwId != RFC6374_INIT_VAL)))
        {
            if (R6374UtilTestStatus (pServiceInfo, RFC6374_TX_STATUS_START,
                                     RFC6374_PROACTIVE_TEST_START,
                                     RFC6374_TEST_MODE_LM_DM_ALL)
                == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LwUtilValidateServParams : unable to start "
                             "Proactive test\r\n");
                return RFC6374_FAILURE;
            }
        }
        return RFC6374_SUCCESS;
    }
    /* Check if Service has PW/Tunnel configured */
    if (u1MplsPathType == RFC6374_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_NOT_CONFIG);
        RFC6374_TRC1 (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374LwUtilValidateServParams : Mandatory parameter"
                      " Path type is not configured to start the test\r\n",
                      pServiceInfo->au1ServiceName);
        return RFC6374_FAILURE;
    }
    else if (u1MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
    {
        if ((u4SrcIpAddr == RFC6374_INIT_VAL)
            || (u4DestIpAddr == RFC6374_INIT_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_NOT_CONFIG);
            RFC6374_TRC1 (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374LwUtilValidateServParams : Mandatory parameter "
                          "Soruce IP/Destination IP/Tunnel Id is not configured "
                          "to start the test\r\n",
                          pServiceInfo->au1ServiceName);
            return RFC6374_FAILURE;
        }
    }
    else if (u1MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if ((u4PwIpAddr == RFC6374_INIT_VAL) || (u4PwId == RFC6374_INIT_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (RFC6374_CLI_MPLS_PATH_NOT_CONFIG);
            RFC6374_TRC1 (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374LwUtilValidateServParams : Mandatory parameter "
                          "Destination IP/PW Id is not configured "
                          "to start the test\r\n",
                          pServiceInfo->au1ServiceName);
            return RFC6374_FAILURE;
        }
    }
    return RFC6374_SUCCESS;
}
