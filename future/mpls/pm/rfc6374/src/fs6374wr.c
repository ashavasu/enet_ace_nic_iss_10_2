/* $Id: fs6374wr.c,v 1.2 2017/07/25 12:00:37 siva Exp $ */

# include  "lr.h"
# include  "fssnmp.h"
# include  "fs6374lw.h"
# include  "fs6374wr.h"
# include  "fs6374db.h"

INT4
GetNextIndexFs6374ServiceConfigTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374ServiceConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374ServiceConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFS6374 ()
{
    SNMPRegisterMib (&fs6374OID, &fs6374Entry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fs6374OID, (const UINT1 *) "fs6374");
}

VOID
UnRegisterFS6374 ()
{
    SNMPUnRegisterMib (&fs6374OID, &fs6374Entry);
    SNMPDelSysorEntry (&fs6374OID, (const UINT1 *) "fs6374");
}

INT4
Fs6374MplsPathTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374MplsPathType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374FwdTnlIdOrPwIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374FwdTnlIdOrPwId (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374RevTnlIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374RevTnlId (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374SrcIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374SrcIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->pOctetStrValue));

}

INT4
Fs6374DestIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DestIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
Fs6374EncapTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374EncapType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374TrafficClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374TrafficClass (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374RowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374RowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DyadicMeasurementGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DyadicMeasurement (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374TSFNegotiationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374TSFNegotiation (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374TSNegotiatedFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374TSNegotiatedFormat
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DyadicProactiveRoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DyadicProactiveRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374SessionIntervalQueryStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374SessionIntervalQueryStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374MinReceptionIntervalInMillisecondsGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374MinReceptionIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374QueryTransmitRetryCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ServiceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374QueryTransmitRetryCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374MplsPathTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374MplsPathType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fs6374FwdTnlIdOrPwIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374FwdTnlIdOrPwId (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374RevTnlIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374RevTnlId (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->u4_ULongValue));

}

INT4
Fs6374SrcIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374SrcIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->pOctetStrValue));

}

INT4
Fs6374DestIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DestIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
Fs6374EncapTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374EncapType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374TrafficClassSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374TrafficClass (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fs6374RowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374RowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374DyadicMeasurementSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DyadicMeasurement (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fs6374TSFNegotiationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374TSFNegotiation (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fs6374DyadicProactiveRoleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DyadicProactiveRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fs6374SessionIntervalQueryStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFs6374SessionIntervalQueryStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fs6374MinReceptionIntervalInMillisecondsSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFs6374MinReceptionIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374QueryTransmitRetryCountSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFs6374QueryTransmitRetryCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374MplsPathTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374MplsPathType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fs6374FwdTnlIdOrPwIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374FwdTnlIdOrPwId (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fs6374RevTnlIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374RevTnlId (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374SrcIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374SrcIpAddr (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->pOctetStrValue));

}

INT4
Fs6374DestIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DestIpAddr (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fs6374EncapTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374EncapType (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fs6374TrafficClassTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374TrafficClass (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fs6374RowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374RowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fs6374DyadicMeasurementTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DyadicMeasurement (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fs6374TSFNegotiationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374TSFNegotiation (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fs6374DyadicProactiveRoleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DyadicProactiveRole (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fs6374SessionIntervalQueryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374SessionIntervalQueryStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fs6374MinReceptionIntervalInMillisecondsTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374MinReceptionIntervalInMilliseconds (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               pOctetStrValue,
                                                               pMultiData->
                                                               u4_ULongValue));

}

INT4
Fs6374QueryTransmitRetryCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374QueryTransmitRetryCount (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374ServiceConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fs6374ServiceConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFs6374ParamsConfigTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374ParamsConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374ParamsConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374LMTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMType (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMMethod (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMNoOfMessagesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMNoOfMessages (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMTimeIntervalInMillisecondsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMTimeIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMTimeStampFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMTimeStampFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMTransmitStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMTransmitStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMType (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMTimeIntervalInMillisecondsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMTimeIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMNoOfMessagesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMNoOfMessages (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMTimeStampFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMTimeStampFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMTransmitStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMTransmitStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374CmbLMDMTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374CmbLMDMMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMMethod (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374CmbLMDMModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374CmbLMDMNoOfMessagesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMNoOfMessages
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374CmbLMDMTimeIntervalInMillisecondsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMTimeIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374CmbLMDMTimeStampFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMTimeStampFormat
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374CmbLMDMTransmitStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMTransmitStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMPaddingSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMPaddingSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374CmbLMDMPaddingSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374ParamsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374CmbLMDMPaddingSize
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMType (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiData->i4_SLongValue));

}

INT4
Fs6374LMMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMMethod (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiData->i4_SLongValue));

}

INT4
Fs6374LMModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiData->i4_SLongValue));

}

INT4
Fs6374LMNoOfMessagesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMNoOfMessages (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374LMTimeIntervalInMillisecondsSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFs6374LMTimeIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374LMTimeStampFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMTimeStampFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fs6374LMTransmitStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMTransmitStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fs6374DMTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMType (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiData->i4_SLongValue));

}

INT4
Fs6374DMModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiData->i4_SLongValue));

}

INT4
Fs6374DMTimeIntervalInMillisecondsSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFs6374DMTimeIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374DMNoOfMessagesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMNoOfMessages (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374DMTimeStampFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMTimeStampFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fs6374DMTransmitStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMTransmitStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMMethod (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMNoOfMessagesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMNoOfMessages
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374CmbLMDMTimeIntervalInMillisecondsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMTimeIntervalInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374CmbLMDMTimeStampFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMTimeStampFormat
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMTransmitStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMTransmitStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fs6374DMPaddingSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMPaddingSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374CmbLMDMPaddingSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374CmbLMDMPaddingSize
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fs6374LMTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMType (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374LMMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMMethod (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Fs6374LMModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMMode (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374LMNoOfMessagesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMNoOfMessages (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fs6374LMTimeIntervalInMillisecondsTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMTimeIntervalInMilliseconds (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         pOctetStrValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
Fs6374LMTimeStampFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMTimeStampFormat (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fs6374LMTransmitStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMTransmitStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fs6374DMTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMType (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374DMModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMMode (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374DMTimeIntervalInMillisecondsTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMTimeIntervalInMilliseconds (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         pOctetStrValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
Fs6374DMNoOfMessagesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMNoOfMessages (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fs6374DMTimeStampFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMTimeStampFormat (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fs6374DMTransmitStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMTransmitStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMType (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMMethod (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMMode (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMNoOfMessagesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMNoOfMessages (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->u4_ULongValue));

}

INT4
Fs6374CmbLMDMTimeIntervalInMillisecondsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMTimeIntervalInMilliseconds (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              pOctetStrValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
Fs6374CmbLMDMTimeStampFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMTimeStampFormat (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fs6374CmbLMDMTransmitStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMTransmitStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fs6374DMPaddingSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMPaddingSize (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Fs6374CmbLMDMPaddingSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374CmbLMDMPaddingSize (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->u4_ULongValue));

}

INT4
Fs6374ParamsConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fs6374ParamsConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFs6374LMTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374LMTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374LMTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374LMTxLossAtSenderEndGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMTxLossAtSenderEnd
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMRxLossAtSenderEndGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMRxLossAtSenderEnd
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMRxLossAtReceiverEndGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMRxLossAtReceiverEnd
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMMeasurementTimeTakenInMillisecondsGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMMeasurementTimeTakenInMilliseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Fs6374LMTransmitResultOKGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMTransmitResultOK
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMTestPktsCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMTestPktsCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFs6374DMTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374DMTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374DMTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374DMDelayValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMDelayValue (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
Fs6374DMRTDelayValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMRTDelayValue (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
Fs6374DMIPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMIPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].pOctetStrValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                pMultiData->pOctetStrValue));

}

INT4
Fs6374DMPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].u4_ULongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               pMultiData->pOctetStrValue));

}

INT4
Fs6374DMTransmitResultOKGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMTransmitResultOK
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFs6374LMStatsTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374LMStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374LMStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374LMStatsMplsTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsMplsType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMStatsFwdTnlIdOrPwIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsFwdTnlIdOrPwId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsRevTnlIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsRevTnlId (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsSrcIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsSrcIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fs6374LMStatsDestIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsDestIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fs6374LMStatsTrafficClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsTrafficClass
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMStatsModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMStatsMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsMethod (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMStatsTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMStatsTimeStampFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsTimeStampFormat
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMStatsStartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsStartTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsFarEndLossMinGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsFarEndLossMin
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsFarEndLossMaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsFarEndLossMax
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsFarEndLossAvgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsFarEndLossAvg
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsNearEndLossMinGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsNearEndLossMin
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsNearEndLossMaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsNearEndLossMax
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsNearEndLossAvgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsNearEndLossAvg
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsTxPktCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsTxPktCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsRxPktCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsRxPktCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsThroughputpercentGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsThroughputpercent
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Fs6374LMStatsResponseErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsResponseErrors
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsResponseTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsResponseTimeout
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMStatsMeasurementOnGoingGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMStatsMeasurementOnGoing
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMDyadicMeasurementGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMDyadicMeasurement
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMRemoteSessionIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMRemoteSessionId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374LMDyadicProactiveRoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMDyadicProactiveRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374IsLMBufferSQIEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374LMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374IsLMBufferSQIEnabled
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFs6374DMStatsTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374DMStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374DMStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374DMStatsMplsTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMplsType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMStatsFwdTnlIdOrPwIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsFwdTnlIdOrPwId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsRevTnlIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsRevTnlId (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsSrcIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsSrcIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsDestIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsDestIpAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsTrafficClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsTrafficClass
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMStatsModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMStatsTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsType (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMStatsTimeStampFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsTimeStampFormat
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMStatsStartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsStartTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsMinDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMinDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMaxDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMaxDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsAvgDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsAvgDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMinRTDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMinRTDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMaxRTDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMaxRTDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsAvgRTDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsAvgRTDelay (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMinIPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMinIPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMaxIPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMaxIPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsAvgIPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsAvgIPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMinPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMinPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsMaxPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMaxPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsAvgPDVGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsAvgPDV (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fs6374DMStatsTxPktCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsTxPktCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsRxPktCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsRxPktCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsResponseErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsResponseErrors
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsResponseTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsResponseTimeout
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMStatsMeasurementOnGoingGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsMeasurementOnGoing
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMDyadicMeasurementGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMDyadicMeasurement
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMRemoteSessionIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMRemoteSessionId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374DMDyadicProactiveRoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMDyadicProactiveRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMStatsPaddingSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMStatsPaddingSize
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374IsDMBufferSQIEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374DMStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374IsDMBufferSQIEnabled
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFs6374StatsTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374StatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374StatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374StatsLmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsLmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsLmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsLmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsLmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsLmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsLmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsLmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374Stats1LmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374Stats1LmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374Stats1LmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374Stats1LmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374Stats1DmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374Stats1DmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374Stats1DmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374Stats1DmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsDmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsDmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsDmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsDmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsDmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsDmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsDmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsDmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsCmb1LmDmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsCmb1LmDmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsCmb1LmDmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsCmb1LmDmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsCmbLmmDmmOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsCmbLmmDmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsCmbLmmDmmInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsCmbLmmDmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsCmbLmrDmrOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsCmbLmrDmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsCmbLmrDmrInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374StatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374StatsCmbLmrDmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374StatsLmmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsLmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsLmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsLmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsLmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1LmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374Stats1LmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1LmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374Stats1LmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1DmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374Stats1DmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1DmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374Stats1DmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmmOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsDmmOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmmInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsDmmIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmrOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsDmrOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmrInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374StatsDmrIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsLmmOut (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsLmmIn (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsLmrOut (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsLmrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsLmrIn (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1LmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374Stats1LmOut (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1LmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374Stats1LmIn (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1DmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374Stats1DmOut (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374Stats1DmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374Stats1DmIn (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmmOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsDmmOut (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmmInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsDmmIn (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmrOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsDmrOut (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsDmrInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374StatsDmrIn (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374StatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fs6374StatsTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFs6374SystemConfigTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374SystemConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374SystemConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374SystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374SystemConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374SystemControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374ModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374SystemConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374ModuleStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DMBufferClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374SystemConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DMBufferClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374LMBufferClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374SystemConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMBufferClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374DebugLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374SystemConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374DebugLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
Fs6374TSFormatSupportedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374SystemConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374TSFormatSupported (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fs6374SystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374SystemControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fs6374ModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374ModuleStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fs6374DMBufferClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DMBufferClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fs6374LMBufferClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMBufferClear (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fs6374DebugLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374DebugLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
Fs6374SystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374SystemControl (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fs6374ModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374ModuleStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fs6374DMBufferClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DMBufferClear (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fs6374LMBufferClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMBufferClear (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fs6374DebugLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374DebugLevel (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fs6374SystemConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fs6374SystemConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFs6374NotificationsTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFs6374NotificationsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFs6374NotificationsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fs6374LMDMTrapControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFs6374NotificationsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFs6374LMDMTrapControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fs6374LMDMTrapControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFs6374LMDMTrapControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Fs6374LMDMTrapControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fs6374LMDMTrapControl (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Fs6374NotificationsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fs6374NotificationsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
