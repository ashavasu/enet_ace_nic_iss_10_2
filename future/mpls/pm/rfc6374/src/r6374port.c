/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * $Id: r6374port.c,v 1.3 2017/07/25 12:00:37 siva Exp $
 *
 * Description : This file contains the utility
 *               functions of the RFC6374 module
 *****************************************************************************/
#ifndef _R6374PORT_C_
#define _R6374PORT_C_

#include "r6374inc.h"
#include "snmputil.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : R6374PortHandleExtInteraction                              */
/*                                                                           */
/* Description  : This function is the common exit  point for interacting    */
/*                with all external module.                                  */
/*                                                                           */
/* Input        :                                                            */
/*                pR6374ExtReqInParams - Pointer to the Request params.      */
/*                                                                           */
/* Output       : pR6374ExtReqOutParams - Pointer to the response params.    */
/*                                                                           */
/* Returns      : SUCCESS/FAILURE                                            */
/*                                                                           */
/*****************************************************************************/
INT4
R6374PortHandleExtInteraction (tR6374ExtInParams * pR6374ExtReqInParams,
                               tR6374ExtOutParams * pR6374ExtReqOutParams)
{
    tFmFaultMsg         FmFaultMsg;
    tVcmRegInfo         VcmRegInfo;
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_SUCCESS;

    RFC6374_MEMSET ((&FmFaultMsg), RFC6374_INIT_VAL, sizeof (tFmFaultMsg));
    RFC6374_MEMSET (&VcmRegInfo, RFC6374_INIT_VAL, sizeof (tVcmRegInfo));

    /* External requests handling */
    switch (pR6374ExtReqInParams->eExtReqType)
    {
        case RFC6374_REQ_BFD_GET_SESS_INDEX_FROM_PW_INFO:
            /* This Case is used to Retrive the BFD Session Index from
             * PW Info
             * Input : 
             *     u4ContextId, 
             *     InBfdReqParams - PW Info
             * Output :
             *     BfdRespParams - BFD Session Index
             * Return :
             *     RFC6374_SUCCESS or RFC6374_FAILURE*/
#ifdef BFD_WANTED
            if ((BfdApiHandleExtRequest (pR6374ExtReqInParams->u4ContextId,
                                         &pR6374ExtReqInParams->InBfdReqParams,
                                         &pR6374ExtReqOutParams->
                                         BfdRespParams)) == RFC6374_FAILURE)
            {
                i4RetVal = RFC6374_FAILURE;
            }
#endif
            break;
        case RFC6374_REQ_BFD_GET_SESS_INDEX_FROM_LSP_INFO:
            /* This Case is used to Retrive the BFD Session Index from
             * LSP Info
             * Input :
             *     u4ContextId,
             *     InBfdReqParams - LSP Info
             * Output :
             *     BfdRespParams - BFD Session Index
             * Return :
             * RFC6374_SUCCESS or RFC6374_FAILURE*/
#ifdef BFD_WANTED
            if ((BfdApiHandleExtRequest (pR6374ExtReqInParams->u4ContextId,
                                         &pR6374ExtReqInParams->InBfdReqParams,
                                         &pR6374ExtReqOutParams->
                                         BfdRespParams)) == RFC6374_FAILURE)
            {
                i4RetVal = RFC6374_FAILURE;
            }
#endif
            break;
        case RFC6374_REQ_BFD_GET_TXRX_FROM_SESS_INDEX:
            /* This Case is used to Retrive the BFD Session Info(TX and RX)
             * from BFD Session Index
             * Input :
             *     u4ContextId,
             *     InBfdReqParams - BFD Session Index
             * Output :
             *     BfdRespParams - BFD Tx and Rx Counter
             * Return :
             * RFC6374_SUCCESS or RFC6374_FAILURE*/
#ifdef BFD_WANTED
            if ((BfdApiHandleExtRequest (pR6374ExtReqInParams->u4ContextId,
                                         &pR6374ExtReqInParams->InBfdReqParams,
                                         &pR6374ExtReqOutParams->
                                         BfdRespParams)) == RFC6374_FAILURE)
            {
                i4RetVal = RFC6374_FAILURE;
            }
#endif
            break;
        case RFC6374_REQ_BFD_GET_EXP_FROM_SESS_INDEX:
            /* This case is used to retive the bfd traffic/exp value
             * from BFD Session Index for a particular PW/LSP Tunnel
             * Input :
             *      u4ContextId,
             *      BfdReqParams - BFD Session Index
             * Output :
             *      BfdRespParams - BFD Exp/TrafficClass
             * Return :
             * RFC6374_SUCCESS or RFC6374_FAILURE*/
#ifdef BFD_WANTED
            if ((BfdApiHandleExtRequest (pR6374ExtReqInParams->u4ContextId,
                                         &pR6374ExtReqInParams->InBfdReqParams,
                                         &pR6374ExtReqOutParams->
                                         BfdRespParams)) == RFC6374_FAILURE)
            {
                i4RetVal = RFC6374_FAILURE;
            }
#endif
            break;
        case RFC6374_VCM_REGISTER_WITH_VCM:

            VcmRegInfo.pIfMapChngAndCxtChng = R6374ApiVcmCallback;
            VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
            VcmRegInfo.u1ProtoId = RFC6374_PROTOCOL_ID;

            if (VcmRegisterHLProtocol (&VcmRegInfo) != VCM_SUCCESS)
            {
                return RFC6374_FAILURE;
            }
            break;

            /* This request type is to get the context name from the given
             * context ID by calling the VCM module API
             * */
        case RFC6374_VCM_DEREGISTER_WITH_VCM:

            if (VcmDeRegisterHLProtocol (RFC6374_PROTOCOL_ID) != VCM_SUCCESS)
            {
                return RFC6374_FAILURE;
            }
            break;
        case RFC6374_FM_SEND_TRAP:
            MEMCPY (&FmFaultMsg, &(pR6374ExtReqInParams->InFmFaultMsg),
                    sizeof (tFmFaultMsg));

            if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
            {
                return RFC6374_FAILURE;
            }
            break;
        case RFC6374_VCM_GET_CXT_ID_FRM_NAME:
            if (VcmIsSwitchExist
                (pR6374ExtReqInParams->uInParams.au1ContextName,
                 &u4ContextId) != VCM_TRUE)
            {
                return RFC6374_FAILURE;
            }
            pR6374ExtReqOutParams->u4ContextId = u4ContextId;

            break;
        default:
            i4RetVal = RFC6374_FAILURE;

    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : R6374MplsHandleExtInteraction                    */
/*                                                                           */
/*    Description         : This function is the exit point for interacting  */
/*                          with MPLS module.                                */
/*                                                                           */
/*    Input(s)            : u4ReqType - Type of interaction                  */
/*                          pInMplsApiInfo - Pointer to the Request params   */
/*                                                                           */
/*    Output(s)           : pOutMplsApiInfo - Pointer to the response params */
/*                                                                           */
/*    Returns             : RFC6374_SUCCESS or RFC6374_FAILURE               */
/*****************************************************************************/
INT4
R6374MplsHandleExtInteraction (UINT4 u4ReqType,
                               tMplsApiInInfo * pInMplsApiInfo,
                               tMplsApiOutInfo * pOutMplsApiInfo)
{
#ifdef MPLS_WANTED
    return (MplsApiHandleExternalRequest (u4ReqType,
                                          pInMplsApiInfo, pOutMplsApiInfo));
#else
    UNUSED_PARAM (u4ReqType);
    UNUSED_PARAM (pInMplsApiInfo);
    UNUSED_PARAM (pOutMplsApiInfo);
    return RFC6374_SUCCESS;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : R6374CfaGetSysMacAddress                        */
/*                                                                          */
/*    Description         : This function returns the Switch's Base Mac     */
/*                          address read from NVRAM                         */
/*    Input(s)            : None.                                           */
/*                                                                          */
/*    Output(s)           : pSwitchMac - the Switch Mac address.            */
/*                                                                          */
/*    Returns            : None                                             */
/* **************************************************************************/
PUBLIC VOID
R6374CfaGetSysMacAddress (tR6374MacAddr SwitchMac)
{
    CfaGetSysMacAddress (SwitchMac);
    return;
}

/****************************************************************************
 *    Function Name       : R6374GetSysSuppTimeFormat                       *
 *    Description         : This function returns the System supported      *
 *                          timestamp format                                *
 *    Input(s)            : None.                                           *
 *    Output(s)           : u1SysSuppTimeFormat - Timestamp format          *
 *    Returns             : None                                            *
 ***************************************************************************/
PUBLIC VOID
R6374GetSysSuppTimeFormat (UINT1 *u1SysSuppTimeFormat)
{
    UINT1               u1SuppTimeFormat = RFC6374_INIT_VAL;
    CfaGetSysSuppTimeFormat (&u1SuppTimeFormat);
    *u1SysSuppTimeFormat = u1SuppTimeFormat;
    return;
}
#endif /*_R6374PORT_C_*/
