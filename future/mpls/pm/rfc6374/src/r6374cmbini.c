/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374cmbini.c,v 1.2 2017/07/31 13:40:18 siva Exp $
 *
 * Description: This file contains the R6374 task combined loss 
 *              and delay  measurement initiator.
 *******************************************************************/
#ifndef __R6374CMBINI_C__
#define __R6374CMBINI_C__
#include "r6374inc.h"

PRIVATE VOID
 
 
 
 R6374LmDmInitPutLmmDmmInfo (tR6374ServiceConfigTableEntry * pServiceInfo,
                             UINT1 **ppu1Pdu,
                             tR6374TSRepresentation * pTxTimeStampf);
/****************************************************************************
 * Function           : R6374LmDmInitiator                    *
 *                                         *
 * Description        : The routine implements the combined LMDM Initiator, *
 *                      it calls up routine to format and transmit combined *
 *                      one way LMDM or two way LMDM frame and also handles *
 *                      other events.                                       *
 *                                          *
 * Input(s)           : tR6374PktSmInfo - Service Related Info.            *
 *                      u1EventID - Process Event                *
 *                                        *
 * Output(s)          : None                            *
 *                                        *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE            *
 ***************************************************************************/
PUBLIC UINT4
R6374LmDmInitiator (tR6374PktSmInfo * pPktSmInfo, UINT1 u1EventID)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374NotifyInfo    R6374LmDmNotify;
    tR6374MacAddr       SwitchMacAddr;
    UINT4               u4TempSessionId = RFC6374_INIT_VAL;
    UINT4               u4RetVal = RFC6374_SUCCESS;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmDmInitiator() \r\n");

    RFC6374_MEMSET (&R6374LmDmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));
    RFC6374_MEMSET (&SwitchMacAddr, RFC6374_INIT_VAL, sizeof (tR6374MacAddr));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /* Check for the event received */
    switch (u1EventID)
    {
        case RFC6374_LMDM_START_SESSION:

            /*Done start the test if the test is already running */
            if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_READY)
            {
                pLmDmInfo->u1LmDmStatus = RFC6374_TX_STATUS_NOT_READY;
                /* Reset start session posted flag */
                pLmDmInfo->bIsLmDmStartSessPosted = RFC6374_FALSE;

                /* Send proactive restart trap incase of pro-active test 
                 * restarted by receiving LSP/PW up event */
                if (((pLmDmInfo->
                      b1LmDmTestStatus & RFC6374_PROACTIVE_TEST_RESTARTED) ==
                     RFC6374_PROACTIVE_TEST_RESTARTED)
                    && (pLmDmInfo->u1LmDmMode == RFC6374_LM_MODE_PROACTIVE))
                {
                    /* Proactive Test re-start Trap */
                    R6374LmDmNotify.u1TrapType =
                        RFC6374_PROACTIVE_TEST_RESTART_TRAP;
                    RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                                     pServiceInfo->au1ServiceName,
                                     RFC6374_STRLEN (pServiceInfo->
                                                     au1ServiceName));
                    R6374SendNotification (&R6374LmDmNotify);

                    RFC6374_TRC1 (RFC6374_CRITICAL_TRC,
                                  RFC6374_DEFAULT_CONTEXT_ID,
                                  " Proactive Combined Loss-Delay Measurement test "
                                  "restarted for Service name : %s \r\n",
                                  pServiceInfo->au1ServiceName);

#ifdef SYSLOG_WANTED
                    SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL,
                                  (UINT4) RFC6374_SYSLOG_ID,
                                  "Proactive Combined Loss-Delay Measurement test restarted for Service name : %s"
                                  "\r\n", pServiceInfo->au1ServiceName));
#endif
                }
                /* Reset test restart status as false */
                pLmDmInfo->b1LmDmTestStatus &=
                    ~RFC6374_PROACTIVE_TEST_RESTARTED;
            }
            else
            {
                u4RetVal = RFC6374_FAILURE;
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             " R6374LmDmInitiator:"
                             "Unable to Start Combined LMDM Session, "
                             "As Aleady one Running.\r\n");
                break;
            }

            if (pLmDmInfo->u4LmDmSessionId == RFC6374_INIT_VAL)
            {
                R6374CfaGetSysMacAddress (SwitchMacAddr);
                RFC6374_GET_SESSION_ID_FROM_MAC (SwitchMacAddr,
                                                 u4TempSessionId);
                R6374UtilGetSessId (pLmDmInfo, NULL, NULL, u4TempSessionId,
                                    RFC6374_VAL_3);
                RFC6374_LMDM_INCR_SESSION_ID (pLmDmInfo);
            }
            else
            {
                /* Increase Session ID/ transaction ID */
                R6374UtilIncrementSessionId (pServiceInfo, RFC6374_VAL_3);
            }

            /* As there can be only 10 Sesssion for a service,
             * Increase gobal Session Count */
            RFC6374_LMDM_INCR_SESSION_COUNT ();

            /* Initiate Seq Counter */
            RFC6374_LMDM_RESET_SEQ_COUNT (pLmDmInfo);

            /* Initiate Rolling buffer for one-way Lm Seq Counter */
            RFC6374_LMDM_RESET_ONEWAY_LM_ROSEQ_COUNT (pLmDmInfo);

            /* Initiate Rolling buffer for one-way Dm Seq Counter */
            RFC6374_LMDM_RESET_ONEWAY_DM_ROSEQ_COUNT (pLmDmInfo);

            /* Initiate Rolling buffer for two-way Lm Seq Counter */
            RFC6374_LMDM_RESET_TWOWAY_LM_ROSEQ_COUNT (pLmDmInfo);

            /* Initiate Rolling buffer for two-way Dm Seq Counter */
            RFC6374_LMDM_RESET_TWOWAY_DM_ROSEQ_COUNT (pLmDmInfo);

            /* Initiate Buffer Counter that adds in RB TRee */
            RFC6374_LMDM_RESET_BUFFER_ADD_COUNT (pLmDmInfo);

            /* Update the no of message to be send only for
             * on-demand transmission */
            if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
            {
                /* Initialize Running Count */
                RFC6374_LMDM_SET_RUNNING_COUNT (pLmDmInfo);
            }

            /* Reset Proactive Transmission Count */
            RFC6374_LMDM_RESET_PROACTIVE_TX_COUNT (pLmDmInfo);

            /* Reset No Response Tx Count */
            RFC6374_LMDM_RESET_NO_RESP_TX_COUNT (pLmDmInfo);

            /* Reset Packet Loss Value */
            RFC6374_LMDM_RESET_RX_PKT_LOSS (pLmDmInfo);

            /* Initiate SQI TLV. Ensure Dyadic is disabled */
            if ((pServiceInfo->u1SessIntQueryStatus ==
                 CLI_RFC6374_SESS_INT_QUERY_ENABLE) &&
                (pServiceInfo->u1DyadicMeasurement !=
                 RFC6374_DYADIC_MEAS_ENABLE) &&
                (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_TWO_WAY))
            {
                pServiceInfo->LmDmInfo.u1SessQueryIntervalState =
                    RFC6374_SQI_INIT_STATE;
            }

            /*called to update independent loss and
             * delay structure with combined loss and delay information */
            R6374UpdateLmDmInfo (pServiceInfo);

        case RFC6374_LMDM_INTERVAL_EXPIRY:    /* On a Query Timer Expiry */

            if (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_TWO_WAY)
            {
                /* counter wil be decremented in combined LMRDMR processing 
                 * place whenever PDU is received but if reply is not
                 * received for QueryTransmitRetryCount number of PDU's 
                 * then transmission will be stopped */

                if (pLmDmInfo->u4LmDmNoRespTxCount ==
                    (UINT4) pServiceInfo->u1QueryTransmitRetryCount)
                {
                    /* Response Timeout Trap */
                    R6374LmDmNotify.u1TrapType = RFC6374_RESP_TIMEOUT_TRAP;
                    RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                                     pServiceInfo->au1ServiceName,
                                     RFC6374_STRLEN (pServiceInfo->
                                                     au1ServiceName));
                    R6374SendNotification (&R6374LmDmNotify);

                    RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC |
                                  RFC6374_ALL_FAILURE_TRC,
                                  RFC6374_DEFAULT_CONTEXT_ID,
                                  "R6374LmDmInitiator: "
                                  "u4LmDmNoRespTxCount :%d "
                                  "Stopping lm transaction "
                                  "as no of retries exceed\r\n",
                                  pLmDmInfo->u4LmDmNoRespTxCount);

                    /* Test is aborting because of no response */
                    pLmDmInfo->b1LmDmTestStatus |= RFC6374_TEST_ABORTED;

                    /* Stop the LM Transaction */
                    R6374LmDmInitStopLmDmTransaction (pServiceInfo);

                    /* Restart Test if mode is Proactive */
                    if ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
                        && (pLmDmInfo->u1LmDmRespTimeOutReTries !=
                            RFC6374_RESP_TIMEOUT_RETRIES_MAX_COUNT))
                    {
                        /* Increment the Timeout Retires Counter */
                        RFC6374_LMDM_INCR_RESP_TIMEOUT_RETRIES_COUNT
                            (pLmDmInfo);

                        /* Check if the Query interval is less then 
                         * 1Sec i.e default value */
                        if (pLmDmInfo->u2LmDmTimeInterval <
                            RFC6374_LMDM_DEFAULT_INTERVAL)
                        {
                            /* Start the Timer of Comb LMDM Restart */
                            if (R6374TmrStartTmr
                                (RFC6374_LMDM_QUERY_TMR, pServiceInfo,
                                 ((UINT4)
                                  (RFC6374_VAL_1 *
                                   pLmDmInfo->u1LmDmRespTimeOutReTries))) !=
                                RFC6374_SUCCESS)
                            {
                                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                             RFC6374_ALL_FAILURE_TRC,
                                             RFC6374_DEFAULT_CONTEXT_ID,
                                             "R6374LmDmInitiator: "
                                             "Failure starting DM INTERVAL timer\r\n");
                                return RFC6374_FAILURE;
                            }
                        }
                        else    /* When Query interval timmer is more than a Sec */
                        {
                            /* Start the Timer of Comb LMDM Restart */
                            if (R6374TmrStartTmr
                                (RFC6374_LMDM_QUERY_TMR, pServiceInfo,
                                 ((UINT4)
                                  (pLmDmInfo->u2LmDmTimeInterval *
                                   pLmDmInfo->u1LmDmRespTimeOutReTries))) !=
                                RFC6374_SUCCESS)
                            {
                                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                             RFC6374_ALL_FAILURE_TRC,
                                             RFC6374_DEFAULT_CONTEXT_ID,
                                             "R6374LmDmInitiator: "
                                             "Failure starting DM INTERVAL timer\r\n");
                                return RFC6374_FAILURE;
                            }
                        }
                    }
                    else        /* Reset the Max Retries Counter */
                    {
                        RFC6374_LMDM_RESET_RESP_TIMEOUT_RETRIES_COUNT
                            (pLmDmInfo);
                    }
                    break;
                }

                /* Call the routine to transmit the combined LMDM Packet */
                if (R6374LmDmInitXmitLmmDmmPdu (pPktSmInfo) != RFC6374_SUCCESS)

                {
                    u4RetVal = RFC6374_FAILURE;
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374LmDmInitiator: Unable to transmit"
                                 " Two way combined LmDm test\r\n");
                    break;
                }
            }
            else
            {
                /* Call the routine to transmit the combined 1LMDM Packet */
                if (R6374LmDmInitXmit1LmDmPdu (pPktSmInfo) != RFC6374_SUCCESS)
                {
                    u4RetVal = RFC6374_FAILURE;
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374LmDmInitiator: Unable to transmit"
                                 " One way 1LMDM Pdu\r\n");
                    break;
                }
            }
            break;

        case RFC6374_LMDM_STOP_SESSION:

            /* Stop the combined LmDm Transaction */
            R6374LmDmInitStopLmDmTransaction (pServiceInfo);

            RFC6374_LMDM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmDmInfo);
            break;

        default:
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374LmDmInitiator: Event Not Supported\r\n");
            u4RetVal = RFC6374_FAILURE;
            break;
    }

    if (u4RetVal == RFC6374_FAILURE)
    {
        /* Update Result OK to False */
        pLmDmInfo->b1LmDmResultOk = RFC6374_FALSE;

        /* Test is aborting because of no response */
        pLmDmInfo->b1LmDmTestStatus |= RFC6374_TEST_ABORTED;

        /* Stop the combined LmDm Transaction */
        R6374LmDmInitStopLmDmTransaction (pServiceInfo);

        RFC6374_LMDM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmDmInfo);
    }
    else
    {
        /* Update Result OK to True */
        pLmDmInfo->b1LmDmResultOk = RFC6374_TRUE;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmDmInitiator\r\n");
    return u4RetVal;
}

/****************************************************************************
 * Function Name      : R6374LmDmInitXmitLmmDmmPdu                          *
 * Description        : This routine formats and transmits combined         *
 *             LMDM - Two way                                      *
 * Input(s)           : tR6374PktSmInfo - Service Related Info              *
 * Output(s)          : None                                                *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE            *
 ****************************************************************************/
PUBLIC INT4
R6374LmDmInitXmitLmmDmmPdu (tR6374PktSmInfo * pPktSmInfo)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374TSRepresentation TxTimeStampf;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4TimeInterval = RFC6374_INIT_VAL;
    UINT4               u4Txf = RFC6374_INIT_VAL;
    UINT4               u4Rxf = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4               u4Duration = RFC6374_INIT_VAL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmDmInitXmitLmmDmmPdu\r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /*Check if Number Of Message to be transmitted have been reached */
    if ((pLmDmInfo->u2LmDmRunningCount == RFC6374_INIT_VAL) &&
        (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND))
    {

        if (pLmDmInfo->bDeadlineTimeCrossed == RFC6374_FALSE)
        {
            /* Wait for some extra time (constant for all services) in order
             * to get responses for all transmitted queries */
            pLmDmInfo->bDeadlineTimeCrossed = RFC6374_TRUE;

            if (R6374TmrStartTmr
                (RFC6374_LMDM_QUERY_TMR, pServiceInfo, R6374_MAX_DEADLINE_IN_MS)
                != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmDmInitXmitLmmDmmPdu: "
                             "Failure starting LM/DM query timer\r\n");
                return RFC6374_FAILURE;
            }
            return RFC6374_SUCCESS;
        }
        /* Reset deadline flag */
        pLmDmInfo->bDeadlineTimeCrossed = RFC6374_FALSE;

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmDmInitXmitLmmDmmPdu: "
                     "Stopping combined LmDm Transaction as total number of "
                     "Pdu have been transmitted\r\n");

        /* Reset test abort status as false */
        pLmDmInfo->b1LmDmTestStatus &= ~RFC6374_TEST_ABORTED;

        /*Stop combined LmDm Transaction */
        R6374LmDmInitStopLmDmTransaction (pServiceInfo);

        RFC6374_LMDM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmDmInfo);
        return RFC6374_SUCCESS;
    }

    /* Get Current Time */
    R6374UtilGetCurrentTime (&pLmDmInfo->LmDmOriginTimeStamp,
                             pLmDmInfo->u1LmDmQTSFormat);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmDmInitXmitLmmDmmPdu: Buffer allocation failed"
                     "\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1PduStart = RFC6374_PDU;
    pu1PduEnd = RFC6374_PDU;

    /* Format the combined LMMDMM PDU header */
    R6374LmDmInitFormatLmmDmmPduHdr (pServiceInfo, &pu1PduEnd);

    /* Call BFD module to get the counter */
    if (pLmDmInfo->u1LmDmMethod == RFC6374_LMDM_METHOD_INFERED)
    {
        pPktSmInfo->u1PduType = RFC6374_LMDM_METHOD_INFERED;
#ifdef RFC6374STUB_WANTED
        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
        }
        else
#endif
        {
            i4RetVal = R6374GetBfdPktCount (pServiceInfo, &u4Txf, &u4Rxf);
            if (i4RetVal == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmDmInitXmitLmmDmmPdu: Get BFD counter "
                             "failed\r\n");
                RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
                return i4RetVal;
            }
        }
    }
    else if (pLmDmInfo->u1LmDmMethod == RFC6374_LMDM_METHOD_DIRECT)
    {
        pPktSmInfo->u1PduType = RFC6374_LMDM_METHOD_DIRECT;
        R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                            &u4RcvTnlTx, &u4RcvTnlRx);

        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            u4Txf = u4PwOrFwrdTnlTx;
            u4Rxf = u4PwOrFwrdTnlRx;
        }
        else
        {
            u4Txf = u4PwOrFwrdTnlTx;
        }
    }
    /* Update TimeStamp information in combined LMMDMM Pdu */
    R6374LmDmInitPutLmmDmmInfo (pServiceInfo, &pu1PduEnd, &TxTimeStampf);

    /* If Dyadic is enabled then Counter 1, Counter 3 and
     * Counter 4 has to be sent for other node calculation */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* A_Tx */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, u4Txf);

        /* B_Rx */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);

        /* A_TxP Counter of previous Query */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, pLmDmInfo->u4TxCounterf);

        /* B_RxP Counter of previous Query */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, pLmDmInfo->u4RxCounterb);
    }
    else
    {
        /* This is case when 64bit counter bit is not set */
        /* Counter1 A_TxP */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, u4Txf);
        /* Counter 2 */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        /* Counter 3 */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        /* Counter 4 */
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);

        if (pServiceInfo->LmDmInfo.u1SessQueryIntervalState ==
            RFC6374_SQI_INIT_STATE)
        {
            /* SQI TLV with zero value need to be appended to the buffer */
            RFC6374_PUT_1BYTE (pu1PduEnd, RFC6374_SQI_TLV_TYPE);
            RFC6374_PUT_1BYTE (pu1PduEnd, RFC6374_SQI_TLV_LENGTH);
            RFC6374_PUT_4BYTE (pu1PduEnd, RFC6374_INIT_VAL);

        }
        else if (pServiceInfo->LmDmInfo.u1SessQueryIntervalState ==
                 RFC6374_SQI_ADJUSTED_STATE)
        {
            /* SQI TLV with adjusted value need to be appended to the buffer */
            RFC6374_PUT_1BYTE (pu1PduEnd, RFC6374_SQI_TLV_TYPE);
            RFC6374_PUT_1BYTE (pu1PduEnd, RFC6374_SQI_TLV_LENGTH);
            /* Get and fill the adjusted value from database */
            u4TimeInterval =
                (UINT4) (pServiceInfo->LmDmInfo.u2LmDmTimeInterval);
            RFC6374_PUT_4BYTE (pu1PduEnd, u4TimeInterval);
        }
        if (pLmDmInfo->u4LmDmPadSize != RFC6374_INIT_VAL)
        {
            R6374DmTxPutPadTlv (&pu1PduEnd, pLmDmInfo->u4LmDmPadSize);
        }
    }

    u2PduLength = (UINT2) (pu1PduEnd - pu1PduStart);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmitLmmDmmPdu:"
                     " Buffer copy operation failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    u4Duration = pLmDmInfo->u2LmDmTimeInterval;

    /* Timer of Combined LMDM Query */
    if (R6374TmrStartTmr (RFC6374_LMDM_QUERY_TMR, pServiceInfo, u4Duration)
        != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmitLmmDmmPdu:"
                     "Failure starting LmDm interval timer\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Dump the packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374LmDmInitXmitLmDmPdu: [Sending] Dumping two-way"
                      " LMMDMM PDU\r\n");

    /* Transmit the combined LmmDmm over MPLS Path */
    i4RetVal =
        R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_LOSSDELAY);
    if (i4RetVal != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmitLmmDmmPdu:"
                     " Transmit two way combined LmDm Pdu failed\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Decrement the no of combined LmmDmm to be transmitted if
     * Pro-Active transmission is not desired */
    if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
    {
        /* Decrement the number of Combined LmDm Message/Running Count 
         * to be transmitted */
        RFC6374_LMDM_DECR_RUNNING_COUNT (pLmDmInfo);
    }

    /* Increment the no of LMM transmitted count if
     * Pro-Active transmission is desired */
    if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
    {
        RFC6374_LMDM_INCR_PROACTIVE_TX_COUNT (pLmDmInfo);
    }

    /* Increment Combined LmDm Out Count */
    RFC6374_INCR_STATS_LMMDMM_OUT (pServiceInfo);

    /* Increment Combined LmDm Tx count to stop the transmission 
     * when response is not received for more than 25 Pdu */
    if ((pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) ||
        (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_TWO_WAY))
    {
        RFC6374_LMDM_INCR_NO_RESP_TX_COUNT (pLmDmInfo);
    }

    RFC6374_TRC5 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374LmDmInitXmitLmmDmmPdu: Transmission to the lower "
                  "layer success for Sess Id : %d Tx Count %d Running Count %d"
                  " No Response Tx Count %d Proactive Tx Count %d \r\n",
                  pLmDmInfo->u4LmDmSessionId, pServiceInfo->Stats.u4StatsLmmOut,
                  pLmDmInfo->u2LmDmRunningCount, pLmDmInfo->u4LmDmNoRespTxCount,
                  pLmDmInfo->u4LmDmNoOfProTxCount);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmDmInitXmitLmmDmmPdu\r\n");

    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374LmDmInitFormatLmmDmmPduHdr                  *
 *                                          *
 * Description        : This rotuine is used to construct the combined Loss   *
 *              and delay measurement PDU                      *
 * Input              : pServiceInfo  - Service Related Config Info           *
 *                                          *
 * Output(s)          : ppu1Pdu - Pointer to combined LMMDMM PDU              *
 *                                                  *
 * Return Value(s)    : None                                                  *
 ******************************************************************************/
PUBLIC VOID
R6374LmDmInitFormatLmmDmmPduHdr (tR6374ServiceConfigTableEntry * pServiceInfo,
                                 UINT1 **ppu1Pdu)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo;
    UINT4               u4SessId = RFC6374_INIT_VAL;
    UINT1              *pu1Pdu = NULL;
    UINT1               au1ResVal[RFC6374_LMDM_PDU_RESERVED_LEN] =
        { RFC6374_INIT_VAL };
    UINT2               u2PduLen = RFC6374_INIT_VAL;
    UINT1               u1VerFlag = RFC6374_INIT_VAL;
    UINT1               u1DflagOTF = RFC6374_INIT_VAL;
    UINT1               u1QueryTF = RFC6374_INIT_VAL;
    UINT1               u1DiffServId = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmDmInitFormatLmmDmmPduHdr\r\n");

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);
    pu1Pdu = *ppu1Pdu;

    /* Protocol Version and flag(R|T|0|0) */
    /* Currently T flag set as Zero even traffic class is 
     * configured with non-default value*/
    u1VerFlag = RFC6374_PDU_VERSION << RFC6374_VAL_4;

    u1VerFlag = u1VerFlag | RFC6374_PDU_QUERY_FLAG;
    RFC6374_PUT_1BYTE (pu1Pdu, u1VerFlag);

    /* Control Code */
    if (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_TWO_WAY)
    {
        if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
        {
            /* As for Dyadic Response is not sent so
             * Response not required */
            RFC6374_PUT_1BYTE (pu1Pdu, RFC6374_CTRL_CODE_RESP_NOT_REQ);
        }
        else
        {
            /* Dyadic is not enabled and measurement is start for two way */
            RFC6374_PUT_1BYTE (pu1Pdu, RFC6374_CTRL_CODE_RESP_INBAND_REQ);
        }
    }
    else
    {
        /* Comb LMDM One-way Control code */
        RFC6374_PUT_1BYTE (pu1Pdu, RFC6374_CTRL_CODE_RESP_NOT_REQ);

    }

    if ((pServiceInfo->u1DyadicMeasurement != RFC6374_DYADIC_MEAS_ENABLE) &&
        (pLmDmInfo->u4LmDmPadSize != RFC6374_INIT_VAL))
    {
        /* Combined Loss and Delay Measurement PDU Length (76) +
         * No of PadTlvCount * (padding tlv type (1) + tlv length (1)) +
         * padsize (variable bytes)  */
        u2PduLen = (UINT2) (RFC6374_LMDM_PDU_MSG_LEN +
                            (pLmDmInfo->u1LmDmPadTlvCount * RFC6374_VAL_2) +
                            (UINT2) (pLmDmInfo->u4LmDmPadSize));
    }
    else
    {                            /* Combined Loss and Delay Measurement PDU Length (76) */
        u2PduLen = RFC6374_LMDM_PDU_MSG_LEN;
    }

    if (pServiceInfo->LmDmInfo.u1SessQueryIntervalState >
        RFC6374_SQI_RESET_STATE)
    {
        u2PduLen = (UINT2) (u2PduLen + RFC6374_SESS_QUERY_INT_TLV_LEN);
    }
    RFC6374_PUT_2BYTE (pu1Pdu, u2PduLen);

    /* DFlag OTF (X|B|0|0) */
    /* Currently 32bit and packet count are supported */
    u1DflagOTF = RFC6374_LMDM_32BIT_COUNTERS | RFC6374_LMDM_PKT_COUNT;

    u1DflagOTF = (UINT1) (u1DflagOTF << RFC6374_VAL_6);
    u1QueryTF = pLmDmInfo->u1LmDmQTSFormat;
    u1DflagOTF = u1DflagOTF | u1QueryTF;

    RFC6374_PUT_1BYTE (pu1Pdu, u1DflagOTF);
    /* In sender side no need to fill Responder TimeStamp Format
     * and Responder's Prefered TimeStamp Format */
    RFC6374_PUT_1BYTE (pu1Pdu, RFC6374_INIT_VAL);

    /* Reserved Bits 16 bits */
    RFC6374_PUT_NBYTE (pu1Pdu, au1ResVal, RFC6374_LMDM_PDU_RESERVED_LEN);

    /* Session Identifier and DS Traffic Class */

    /* To identify the session detail , unique number is 
     * concatenated with Mac Address.
     * For LM, value will be 1 and DM,
     * value will be 2 and combined LMDM, value will be 3 */
    u4SessId = pLmDmInfo->u4LmDmSessionId;

    u4SessId = u4SessId | u1DiffServId;
    RFC6374_PUT_4BYTE (pu1Pdu, u4SessId);
    *ppu1Pdu = pu1Pdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmDmInitFormatLmmPduHdr\r\n");
    return;
}

/******************************************************************************
 * Function Name      : R6374LmDmInitStopLmDmTransaction                      *
 * Description        : This routine is used to stop the on going             *
 *                     Combined LMDM transaction                             *
 * Input(s)           : pPktSmInfo - pointer to the Service Related           *
 *                      Config                                   *
 * Output(s)          : None                                                  *
 * Return Value(s)    : None                              *
 ******************************************************************************/
PUBLIC VOID
R6374LmDmInitStopLmDmTransaction (tR6374ServiceConfigTableEntry * pServiceInfo)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmStatsTableEntry *pLmStatsTableInfo = NULL;
    tR6374DmStatsTableEntry *pDmStatsTableInfo = NULL;
    tR6374NotifyInfo    R6374LmDmNotify;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmDmInitStopLmDmTransaction\r\n");

    RFC6374_MEMSET (&R6374LmDmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    /* Stop Combined LMDM Query Timer */
    R6374TmrStopTmr (RFC6374_LMDM_QUERY_TMR, pServiceInfo);

    if (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_TWO_WAY)
    {
        /* Set Measurement Status as Competed 
         * Calculate if any packet loss due to timeoue */
        pLmStatsTableInfo = R6374UtilGetLmStatsTableInfo (pServiceInfo,
                                                          pLmDmInfo->
                                                          u4LmDmSessionId);

        if (pLmStatsTableInfo == NULL)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374LmDmInitStopLmDmTransaction: Get LM Stats "
                          "failed for Session Id : %d \r\n",
                          pLmDmInfo->u4LmDmSessionId);
        }
        else
        {
            if ((pLmDmInfo->b1LmDmTestStatus & RFC6374_TEST_ABORTED) ==
                RFC6374_TEST_ABORTED)
            {
                /* Measurement is Aborted by user or due to Path down */
                pLmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_ABORTED;
            }
            else
            {
                /* Measurement Completed */
                pLmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_COMPLETE;
            }

            if (pLmStatsTableInfo->u1LossMode == RFC6374_LMDM_MODE_ONDEMAND)
            {
                /* Actual combined LmDm Sent */
                pLmStatsTableInfo->u4LMMSent = (UINT4)
                    (pLmDmInfo->u2LmDmNoOfMessages + RFC6374_INDEX_ONE -
                     pLmDmInfo->u2LmDmRunningCount);
            }

            /* Actual Comb LmDm recevied */
            if ((pLmStatsTableInfo->u1DyadicMeasurement ==
                 RFC6374_DYADIC_MEAS_ENABLE)
                && pLmDmInfo->b1LmDmInitResponder == RFC6374_TRUE)
            {
                /* Add one is done if the responder has initiated the Comb LMDM */
                pLmStatsTableInfo->u4LMMRcvd = pLmStatsTableInfo->u4LMMRcvd + 1;
            }

            /* No Of Error Packet Recevied */
            pLmStatsTableInfo->u4NoOfErroredRespRcvd =
                pLmDmInfo->u4RxLmDmPacketLoss;
        }

        /* Set Measurement Status as Competed
         * Calculate if any delay due to timeoue */
        pDmStatsTableInfo = R6374UtilGetDmStatsTableInfo (pServiceInfo,
                                                          pLmDmInfo->
                                                          u4LmDmSessionId);
        if (pDmStatsTableInfo == NULL)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374LmDmInitStopLmDmTransaction: Get DM Stats "
                          "failed for session Id : %d\r\n",
                          pLmDmInfo->u4LmDmSessionId);
        }
        else
        {
            if ((pLmDmInfo->b1LmDmTestStatus & RFC6374_TEST_ABORTED) ==
                RFC6374_TEST_ABORTED)
            {
                /* Measurement is Aborted by user or due to Path down */
                pDmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_ABORTED;
            }
            else
            {
                /* Measurement Completed */
                pDmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_COMPLETE;
            }

            if (pDmStatsTableInfo->u1DelayMode == RFC6374_LMDM_MODE_ONDEMAND)
            {
                /* Actual combined LmDm Sent */
                pDmStatsTableInfo->u4DMMSent = (UINT4)
                    (pLmDmInfo->u2LmDmNoOfMessages + RFC6374_INDEX_ONE -
                     pLmDmInfo->u2LmDmRunningCount);
            }

            /* Actual Comb LmDm recevied */
            if ((pDmStatsTableInfo->u1DyadicMeasurement ==
                 RFC6374_DYADIC_MEAS_ENABLE)
                && pLmDmInfo->b1LmDmInitResponder == RFC6374_TRUE)
            {
                /* Add one is done if the responder has initiated the Comb LMDM */
                pDmStatsTableInfo->u4DMMRcvd = pDmStatsTableInfo->u4DMMRcvd + 1;
            }

            /* No. of Error Packet Recevied */
            pDmStatsTableInfo->u4NoOfErroredRespRcvd =
                pLmDmInfo->u4RxLmDmPacketLoss;
        }
    }

    /* Decrement Global LM Running Count */
    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        RFC6374_LMDM_DECR_SESSION_COUNT ();

        /* Send Test Abort trap for the below cases
         * 1. Test aborted by Administrator
         * 2. LSP/PW down : On-Demand - Based on Running count
         *                   proactive - Based on Previous mode when
         mode is changed from on-demand
         to proactive to stop the test
         - Based on LMDM mode when test is
         aborted due to LSP/PW down*/

        if (((pLmDmInfo->b1LmDmTestStatus & RFC6374_TEST_ABORTED) ==
             RFC6374_TEST_ABORTED) ||
            (pLmDmInfo->u2LmDmRunningCount != RFC6374_INIT_VAL) ||
            (pLmDmInfo->u1PrevLmDmMode == RFC6374_LMDM_MODE_PROACTIVE) ||
            (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE))
        {
            /* Test Abort Trap */
            R6374LmDmNotify.u1TrapType = RFC6374_TEST_ABORT_TRAP;
            RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                             pServiceInfo->au1ServiceName,
                             RFC6374_STRLEN (pServiceInfo->au1ServiceName));
            R6374SendNotification (&R6374LmDmNotify);

            RFC6374_TRC2 (RFC6374_CRITICAL_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                          "Combined Loss and Delay Measurement test aborted "
                          "for Service name : %s Session Id : %d"
                          " \r\n", pServiceInfo->au1ServiceName,
                          pLmDmInfo->u4LmDmSessionId);

#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) RFC6374_SYSLOG_ID,
                          "Combined Loss and Delay Measurement test aborted for Service name : %s Session Id : %d "
                          "\r\n", pServiceInfo->au1ServiceName,
                          pLmDmInfo->u4LmDmSessionId));
#endif

            if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
            {
                pLmDmInfo->u1PrevLmDmMode = RFC6374_LMDM_MODE_ONDEMAND;
            }
        }

        /* Reset test abort status as false */
        pLmDmInfo->b1LmDmTestStatus &= ~RFC6374_TEST_ABORTED;
    }

    /* Revert Back the Tx Status */
    pLmDmInfo->u1LmDmStatus = RFC6374_TX_STATUS_READY;

    /* Reset Responder Initiated to False */
    pLmDmInfo->b1LmDmInitResponder = RFC6374_FALSE;

    /* Initiate Seq Counter For the Next Trasmission for Dyadic 
     * and Non-Dyadic Measurement */
    RFC6374_LMDM_RESET_SEQ_COUNT (pLmDmInfo);

    /* As using LM and DM Seq Count for buffer Add
     * Need to reset the same */
    RFC6374_LM_RESET_SEQ_COUNT (pLmInfo);
    RFC6374_DM_RESET_SEQ_COUNT (pDmInfo);

    /* Reset LM previous counter Values */
    R6374LmResetVar (pLmInfo);

    /* Reset 3 and 4, Counter and TimeStamp values of CombLMDM 
     * used PDU */
    R6374CombLmDmResetVar (pLmDmInfo);

    /* Reset loss amd tx count */
    RFC6374_LMDM_RESET_PACKETLOSS (pLmDmInfo);
    RFC6374_LMDM_RESET_RESP_TX_COUNT (pLmDmInfo);

    /* Reset SQI TLV */
    if (pServiceInfo->u1SessIntQueryStatus == CLI_RFC6374_SESS_INT_QUERY_ENABLE)
    {
        pServiceInfo->LmDmInfo.u1SessQueryIntervalState =
            RFC6374_SQI_RESET_STATE;
        /* Restore the adjusted interval with Configured interval */
        pServiceInfo->LmDmInfo.u2LmDmTimeInterval =
            pServiceInfo->LmDmInfo.u2LmDmConfiguredTimeInterval;
        pServiceInfo->LmInfo.u2LmTimeInterval =
            pServiceInfo->LmInfo.u2LmConfiguredTimeInterval;
        pServiceInfo->DmInfo.u2DmTimeInterval =
            pServiceInfo->DmInfo.u2DmConfiguredTimeInterval;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmDmInitStopLmDmTransaction\r\n");
    return;
}

/********************************************************************************
 * Function Name      : R6374LmDmInitXmit1LmDmPdu                               *
 *                                                                              *
 * Description        : This routine formats and transmits the 1DMLM PDU for    *
 *             One way                                                 *
 *                                                                              *
 * Input(s)           : pPktSmInfo - Pointer the Service Related Config         *
 *                                                                              *
 * Output(s)          : None                                                    *
 *                                                                              *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE                       *
 *******************************************************************************/
PUBLIC INT4
R6374LmDmInitXmit1LmDmPdu (tR6374PktSmInfo * pPktSmInfo)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374TSRepresentation TxTimeStampf;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4Duration = RFC6374_INIT_VAL;
    UINT4               u4Txf = RFC6374_INIT_VAL;
    UINT4               u4Rxf = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT1              *pu1LmDm1PduStart = NULL;
    UINT1              *pu1LmDm1PduEnd = NULL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmDmInitXmit1LmDmPdu \r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /*Check if Number Of Message to be transmitted have been reached */
    if ((pLmDmInfo->u2LmDmRunningCount == RFC6374_INIT_VAL) &&
        (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_DEFAULT_MODE))
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmDmInitXmit1LmDmPdu: Stopping 1way combined LmDm "
                     "Transaction as total number of 1LMDM"
                     "have been transmitted\r\n");
        /*Stop Lm Transaction */
        R6374LmDmInitStopLmDmTransaction (pServiceInfo);
        return RFC6374_SUCCESS;
    }

    /* Update Origin Timestamp info */
    /* Get Current Time */
    R6374UtilGetCurrentTime (&pLmDmInfo->LmDmOriginTimeStamp,
                             pLmDmInfo->u1LmDmQTSFormat);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmit1LmDmPdu: "
                     "Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1LmDm1PduStart = RFC6374_PDU;
    pu1LmDm1PduEnd = RFC6374_PDU;

    /* Format the 1LmDm PDU header */
    R6374LmDmInitFormatLmmDmmPduHdr (pServiceInfo, &pu1LmDm1PduEnd);

    /* Call BFD module to get the counter */
    if (pLmDmInfo->u1LmDmMethod == RFC6374_LMDM_METHOD_INFERED)
    {
        pPktSmInfo->u1PduType = RFC6374_LMDM_METHOD_INFERED;
#ifdef RFC6374STUB_WANTED
        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
        }
        else
#endif
        {
            i4RetVal = R6374GetBfdPktCount (pServiceInfo, &u4Txf, &u4Rxf);
            if (i4RetVal == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmDmInitXmit1LmDmPdu: "
                             "Get BFD Counter Failed\r\n");
                RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
                return i4RetVal;
            }
        }
    }
    else if (pLmDmInfo->u1LmDmMethod == RFC6374_LMDM_METHOD_DIRECT)
    {
        pPktSmInfo->u1PduType = RFC6374_LMDM_METHOD_DIRECT;
        R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                            &u4RcvTnlTx, &u4RcvTnlRx);

        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            u4Txf = u4PwOrFwrdTnlTx;
            u4Rxf = u4PwOrFwrdTnlRx;
        }
        else
        {
            u4Txf = u4PwOrFwrdTnlTx;
        }
    }

    /* Update TimeStamp information in combined LMMDMM Pdu 
     * for 1 way*/
    R6374LmDmInitPutLmmDmmInfo (pServiceInfo, &pu1LmDm1PduEnd, &TxTimeStampf);

    /* this is case when 64bit counter bit is not set */
    /* Counter 1 */
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, u4Txf);
    /* COunter 2 */
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);
    /* Counter 3 */
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);
    /* Counter 4 */
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmDm1PduEnd, RFC6374_INIT_VAL);

    if (pLmDmInfo->u4LmDmPadSize != RFC6374_INIT_VAL)
    {
        R6374DmTxPutPadTlv (&pu1LmDm1PduEnd, pLmDmInfo->u4LmDmPadSize);
    }

    u2PduLength = (UINT2) (pu1LmDm1PduEnd - pu1LmDm1PduStart);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1LmDm1PduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmit1LmDmPdu: "
                     "Buffer copy operation failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    u4Duration = pLmDmInfo->u2LmDmTimeInterval;

    /* Start LMDM Query Timer */
    if (R6374TmrStartTmr (RFC6374_LMDM_QUERY_TMR, pServiceInfo, u4Duration)
        != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmit1LmDmPdu: "
                     "Failure starting combined LmDm periodic timer\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Dump the packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374LmDmInitXmit1LmDmPdu: [Sending] Dumping one-way"
                      " LMMDMM PDU\r\n");

    /* Transmit the 1LMDM over MPLS Path */
    i4RetVal =
        R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_LOSSDELAY);
    if (i4RetVal != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmInitXmit1LmDmPdu: "
                     "Combined 1LmDm transmission to the lower layer failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Decrement the no of 1LMDMs to be transmitted if
     * infinite transmission is not desired */
    if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
    {
        /* Decrement the number of combined 1 way LmDm Message to be 
         * transmitted */
        RFC6374_LMDM_DECR_RUNNING_COUNT (pLmDmInfo);
    }

    /* Increment 1LMDM OUT */
    RFC6374_INCR_STATS_1LMDM_OUT (pServiceInfo);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmDmInitXmit1LmDmPdu\r\n");

    return RFC6374_SUCCESS;
}

/****************************************************************************
 * Function           : R6374UpdateLmDmInfo                    *
 *                                         *
 * Description        : The routine updates the loss and delay structure    *
 *                      with combined loss-daly information.                *
 *                                          *
 * Input(s)           : tR6374ServiceConfigTableEntry - Service Related     *
 *                 Info.                                *
 *                                        *
 * Output(s)          : None                            *
 *                                        *
 * Returns            : None                            *
 ***************************************************************************/
PUBLIC VOID
R6374UpdateLmDmInfo (tR6374ServiceConfigTableEntry * pServiceInfo)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    pLmInfo->u4RxLmPacketLoss = pLmDmInfo->u4RxLmDmPacketLoss;
    pDmInfo->u4RxDmPacketLoss = pLmDmInfo->u4RxLmDmPacketLoss;
}

/***********************************************************************
 * Function Name      : R6374LmDmInitPutLmmDmmInfo                     *
 * Description        : This routine is used to fill the timestamp     *
 *                      in the combined LMMDMM Pdu                       *
 * Input(s)           : pServiceInfo - Service Related Config Info     *
 * Output(s)          : ppu1Pdu - Pointer to pointer to the DMM Pdu.   *
 * Return Value(s)    : None                                           *
 ***********************************************************************/
PRIVATE VOID
R6374LmDmInitPutLmmDmmInfo (tR6374ServiceConfigTableEntry * pServiceInfo,
                            UINT1 **ppu1Pdu,
                            tR6374TSRepresentation * pTxTimeStampf)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    UINT1              *pu1Pdu = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitPutDmmInfo() \r\n");

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);
    pu1Pdu = *ppu1Pdu;

    /* Call the API to get the Current TimeStamp */
    R6374UtilGetCurrentTime (pTxTimeStampf, pLmDmInfo->u1LmDmQTSFormat);

    /* If Dyadic is enabled then TimeStamp 1, TimeStamp 3 and 
     * TimeStamp 4 has to be sent for other node calculation */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* A_Tx Time Stamp */
        RFC6374_PUT_4BYTE (pu1Pdu, pTxTimeStampf->u4Seconds);
        RFC6374_PUT_4BYTE (pu1Pdu, pTxTimeStampf->u4NanoSeconds);

        /* B_Rx Time Stamp */
        RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL);

        /* A_TxP Time Stamp of previous Query */
        RFC6374_PUT_4BYTE (pu1Pdu, pLmDmInfo->TxTimeStampf.u4Seconds);
        RFC6374_PUT_4BYTE (pu1Pdu, pLmDmInfo->TxTimeStampf.u4NanoSeconds);

        /* B_RxP Time Stamp of previous Query */
        RFC6374_PUT_4BYTE (pu1Pdu, pLmDmInfo->RxTimeStampf.u4Seconds);
        RFC6374_PUT_4BYTE (pu1Pdu, pLmDmInfo->RxTimeStampf.u4NanoSeconds);

    }
    else
    {
        /* Fill the 4 bytes with Time Units in Seconds */
        RFC6374_PUT_4BYTE (pu1Pdu, pTxTimeStampf->u4Seconds);

        /* Fill the next 4 bytes with Time Units in nano Seconds */
        RFC6374_PUT_4BYTE (pu1Pdu, pTxTimeStampf->u4NanoSeconds);

        /* Fill the next 24  bytes with ZERO as it will be used by
         * the end processing Combined LMMDMM/LMRDMR frame
         */
        RFC6374_MEMSET (pu1Pdu, RFC6374_PDU_RESERVED_FIELD,
                        RFC6374_LMDM_RESERVED_SIZE);

        pu1Pdu = pu1Pdu + RFC6374_LMDM_RESERVED_SIZE;
    }

    *ppu1Pdu = pu1Pdu;

    RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374LmDmInitPutLmmDmmInfo: TimeStamp of querier in "
                  "seconds: %x NanoSeconds : %x \r\n",
                  pTxTimeStampf->u4Seconds, pTxTimeStampf->u4NanoSeconds);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmDmInitPutLmmDmmInfo() \r\n");
    return;
}

PUBLIC INT4
R6374CombLmDmResetVar (tR6374LmDmInfoTableEntry * pLmDmInfo)
{
    /* Reset LM values */
    pLmDmInfo->u4TxCounterf = RFC6374_RESET;
    pLmDmInfo->u4RxCounterb = RFC6374_RESET;

    /* Reset DM Values */
    RFC6374_MEMSET (&pLmDmInfo->TxTimeStampf, RFC6374_RESET,
                    sizeof (pLmDmInfo->TxTimeStampf));
    RFC6374_MEMSET (&pLmDmInfo->RxTimeStampf, RFC6374_RESET,
                    sizeof (pLmDmInfo->RxTimeStampf));
    return RFC6374_SUCCESS;
}

#endif
