
 /******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 *  $Id: r6374trap.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description : This file contains the functions to send trap to the manager
 *                
 * ****************************************************************************/
#ifndef __R6374TRAP_C__
#define __R6374TRAP_C__

#include "r6374inc.h"
#include "fs6374.h"
CHR1               *gac1R6374TrapMsg[] = {
    NULL,
    "Unsupported Version.",
    "Unsupported control code.",
    "Connection mismatch.",
    "Data format invalid.",
    "Unsupported mandatory TLV.",
    "Unsupported timestamp.",
    "Response timeout.",
    "Resource unavailable.",
    "Test is aborted.",
    "Proactive test restarted.",
    "Unsupported query interval."
};

#ifdef SNMP_3_WANTED
/******************************************************************************
 * Function :   R6374MakeObjIdFrmString
 *
 * Description: This Function retuns the OID  of the given string for the 
 *              given MIB.
 *
 * Input    :   pi1TextStr - pointer to the string.
 *              pTableName - TableName has to be fetched.
 *
 * Output   :   None.
 *
 * Returns  :   pOidPtr or NULL
 *******************************************************************************/
tSNMP_OID_TYPE     *
R6374MakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = RFC6374_INIT_VAL;
    INT1                ai1TempBuffer[RFC6374_OBJECT_NAME_LEN + RFC6374_VAL_1]
        = { RFC6374_INIT_VAL };
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
    pu1TempPtr = (UINT1 *) pi1TextStr;
    UINT2               u2Len = 0;

    for (u2Index = RFC6374_INIT_VAL;
         ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
          (u2Index < RFC6374_OBJECT_NAME_LEN)); u2Index++)
    {
        ai1TempBuffer[u2Index] = (INT1) *pu1TempPtr++;
    }
    ai1TempBuffer[u2Index] = '\0';
    for (u2Index = RFC6374_INIT_VAL; pTableName[u2Index].pName != NULL;
         u2Index++)
    {
        if ((STRCMP
             (pTableName[u2Index].pName,
              (INT1 *) ai1TempBuffer) == RFC6374_INIT_VAL)
            && (STRLEN ((INT1 *) ai1TempBuffer) ==
                STRLEN (pTableName[u2Index].pName)))
        {
            u2Len =
                (UINT2) (STRLEN (pTableName[u2Index].pNumber) <
                         (sizeof (ai1TempBuffer) - 1) ?
                         (STRLEN (pTableName[u2Index].pNumber))
                         : (sizeof (ai1TempBuffer) - 1));

            STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                     u2Len);
            ai1TempBuffer[u2Len] = '\0';
            break;
        }
    }
    if (pTableName[u2Index].pName == NULL)
    {
        return (NULL);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    pOidPtr = SNMP_AGT_GetOidFromString ((INT1 *) ai1TempBuffer);

    return (pOidPtr);
}
#endif

/******************************************************************************
 * Function Name      : R6374SendNotification 
 *
 * Description        : This routine will send the snmp traps 
 *
 * Input(s)           : u1TrapId - Trap ID
 *                      pNotifyInfo - Pointer to tR6374TrapMsg
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
R6374SendNotification (tR6374NotifyInfo * pNotifyInfo)
{

#ifdef SNMP_3_WANTED
    tR6374ExtInParams  *pR6374ExtInParams;
    tR6374ExtOutParams *pR6374ExtOutParams;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pSnmpOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    INT4                i4OidLen = 0;
    UINT2               u2Len = RFC6374_INIT_VAL;
    UINT1               au1Buf[RFC6374_MAX_NAME_LEN];
    UINT1               au1Val[RFC6374_OBJECT_VALUE_LEN];
    UINT1               au1SyslogStr[RFC6374_MAX_SYSLOG_MSG_LEN];

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Allocate Memory for R6374 IN Structure */
    pR6374ExtInParams = (tR6374ExtInParams *) MemAllocMemBlk
        (RFC6374_INPUTPARAMS_POOLID);
    if (pR6374ExtInParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374SendNotification: " "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return;
    }

    /* Allocate Memory for R6374 OUT Structure */
    pR6374ExtOutParams = (tR6374ExtOutParams *) MemAllocMemBlk
        (RFC6374_OUTPUTPARAMS_POOLID);

    if (pR6374ExtOutParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374SendNotification: " "Mempool allocation failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return;
    }

    RFC6374_MEMSET (pR6374ExtInParams, RFC6374_INIT_VAL,
                    sizeof (tR6374ExtInParams));
    RFC6374_MEMSET (pR6374ExtOutParams, RFC6374_INIT_VAL,
                    sizeof (tR6374ExtOutParams));

    if (R6374IsTrapEnabled (pNotifyInfo->u1TrapType) == RFC6374_FALSE)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "\r\nTrap is disabled");
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    i4OidLen = (INT4) (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    pSnmpOid = alloc_oid (i4OidLen);

    if (pSnmpOid == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    MEMCPY (pSnmpOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpOid->u4_Length = (UINT4) i4OidLen;

    MEMSET (au1SyslogStr, 0, sizeof (au1SyslogStr));
    SPRINTF ((char *) au1SyslogStr, "[Ctx : %s][service : %s]-",
             "default", pNotifyInfo->au1ServiceName);

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((CHR1 *) au1Buf, (RFC6374_MAX_NAME_LEN), "fs6374Notifications");

    pEnterpriseOid = R6374MakeObjIdFrmString ((INT1 *) au1Buf,
                                              (UINT1 *)
                                              fs6374_orig_mib_oid_table);

    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
        pNotifyInfo->u1TrapType;

    pVbList = SNMP_AGT_FormVarBind (pSnmpOid, SNMP_DATA_TYPE_OBJECT_ID,
                                    0L, 0, NULL, pEnterpriseOid, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        SNMP_FreeOid (pEnterpriseOid);
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    pStartVb = pVbList;

    /* Filling the service-Name Now */
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((CHR1 *) au1Buf, (RFC6374_MAX_NAME_LEN), "fs6374ServiceName");

    pOid = R6374MakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs6374_orig_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    pOstring = SNMP_AGT_FormOctetString (pNotifyInfo->au1ServiceName,
                                         (INT4) ((RFC6374_STRLEN
                                                  (pNotifyInfo->
                                                   au1ServiceName))));

    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pSnmpOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring, NULL,
                                                  SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pSnmpOid);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* SysLog */
    RFC6374_MEMSET (au1Val, 0, sizeof (au1Val));
    u2Len =
        (UINT2) (STRLEN (gac1R6374TrapMsg[pNotifyInfo->u1TrapType]) <
                 (sizeof (au1Val) - 1) ?
                 STRLEN (gac1R6374TrapMsg[pNotifyInfo->u1TrapType])
                 : (sizeof (au1Val) - 1));

    STRNCPY (au1Val, gac1R6374TrapMsg[pNotifyInfo->u1TrapType], u2Len);
    au1Val[u2Len] = '\0';
    STRCAT (au1SyslogStr, au1Val);

    pR6374ExtInParams->u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;
    pR6374ExtInParams->eExtReqType = RFC6374_FM_SEND_TRAP;

    pR6374ExtInParams->InFmFaultMsg.pTrapMsg = pStartVb;
    pR6374ExtInParams->InFmFaultMsg.pSyslogMsg = (UINT1 *) au1SyslogStr;
    pR6374ExtInParams->InFmFaultMsg.u4GenTrapType = ENTERPRISE_SPECIFIC;
    pR6374ExtInParams->InFmFaultMsg.u4SpecTrapType =
        (UINT4) pNotifyInfo->u1TrapType;
    pR6374ExtInParams->InFmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_RFC6374;

    if (R6374PortHandleExtInteraction (pR6374ExtInParams, pR6374ExtOutParams) !=
        RFC6374_SUCCESS)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return;
    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
    RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtOutParams);

#else
    UNUSED_PARAM (pNotifyInfo);
#endif
}

/******************************************************************************
 * Function Name      : R6374IsTrapEnabled
 *
 * Description        : This routine will check whther trap is enabled or not 
 *
 * Input(s)           : u1Trap - Trap
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_TRUE/RFC6374_FALSE
 *****************************************************************************/
PUBLIC INT4
R6374IsTrapEnabled (UINT1 u1Trap)
{
    if (gR6374GlobalInfo.u2TrapControl & (1 << u1Trap))
    {
        return RFC6374_TRUE;
    }

    return RFC6374_FALSE;
}
#endif
