/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374mpls.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the R6374 task interaction
 * with the MPLS module.
 *
 *******************************************************************/

#ifndef _R6374MPTP_C_
#define _R6374MPTP_C_

#include "r6374inc.h"

/******************************************************************************
 * Function Name      : R6374MplsRegCallBack
 *
 * Description        : This routine is used to register a callback function
 *                      with MPLS module, to receive LM,DM PDUs and MPLS 
 *                      transport path status change notifications.
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
PUBLIC INT4
R6374MplsRegCallBack (VOID)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (RFC6374_MPLSINPUTPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsRegCallBack: " "Mempool allocation failed\r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (pMplsApiInInfo, RFC6374_INIT_VAL, sizeof (tMplsApiInInfo));

    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
    pMplsApiInInfo->u4SubReqType = MPLS_APP_REG_CALL_BACK;
    pMplsApiInInfo->InRegParams.u4ModId = MPLS_RFC6374_APP_ID;
    pMplsApiInInfo->InRegParams.u4Events =
        (MPLS_TNL_UP_EVENT | MPLS_TNL_DOWN_EVENT | MPLS_PW_UP_EVENT |
         MPLS_PW_DOWN_EVENT | MPLS_RFC6374_PACKET);
    pMplsApiInInfo->InRegParams.pFnRcvPkt = R6374ApiMplsCallBack;

    /* Register Call back function with MPLS module */
    if (R6374MplsHandleExtInteraction (MPLS_OAM_REG_APP_FOR_NOTIF,
                                       pMplsApiInInfo, NULL) == RFC6374_FAILURE)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        return RFC6374_FAILURE;
    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374RegMplsPath
 *
 * Description        : This routine is used to register MPLS-TP transport
 *                      path for receiving status change notification.
 *
 * Input(s)           : bRFC6374RegMplsPath - TRUE/FALSE to register/deregister
 *                      pServiceConfig - Service Related Info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
PUBLIC INT4
R6374RegMplsPath (BOOL1 bRFC6374RegMplsPath,
                  tR6374ServiceConfigTableEntry * pServiceConfig)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;

    if (pServiceConfig == NULL)
    {
        return RFC6374_FAILURE;
    }

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (RFC6374_MPLSINPUTPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374RegMplsPath: " "Mempool allocation failed\r\n");
        return RFC6374_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));

    pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
    pMplsApiInInfo->u4ContextId = pServiceConfig->u4ContextId;
    pMplsApiInInfo->u4SubReqType = MPLS_APP_REG_PATH_FOR_NOTIF;

    if (pServiceConfig->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;

        /* Fill the path id with PW VC ID */
        pMplsApiInInfo->InPathId.PwId.u4VcId =
            pServiceConfig->R6374MplsParams.PwMplsPathParams.u4PwId;
    }
    else
    {
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

        /* Fill the MPLS-TP tunnel identifier information */
        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pServiceConfig->R6374MplsParams.LspMplsPathParams.u4FrwdTunnelId;

        pMplsApiInInfo->InPathId.TnlId.
            SrcNodeId.MplsRouterId.u4_addr[0] =
            pServiceConfig->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;

        pMplsApiInInfo->InPathId.TnlId.
            DstNodeId.MplsRouterId.u4_addr[0] =
            pServiceConfig->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
    }

    pMplsApiInInfo->InRFC6374Status = bRFC6374RegMplsPath;

    /* Register MPLS path to receive status change notification */
    if (R6374MplsHandleExtInteraction (MPLS_OAM_REG_APP_FOR_NOTIF,
                                       pMplsApiInInfo, NULL) == OSIX_FAILURE)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        return RFC6374_SUCCESS;
    }

    MemReleaseMemBlock (RFC6374_MPLSINPUTPARAMS_POOLID,
                        (UINT1 *) pMplsApiInInfo);
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374MplsTxPkt
 *
 * Description        : This routine is used to transmit the R6374 Loss-PDU 
 *                      over MPLS transport path.
 *
 * Input(s)           : pBuf - Pointer the packet buffer
 *                      pPduSmInfo - Service Related Info
 *                      u1ServiceType - LOSS/DELAY
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
PUBLIC INT4
R6374MplsTxPkt (tR6374BufChainHeader * pBuf, tR6374PktSmInfo * pPduSmInfo,
                UINT1 u1ServiceType)
{
    tR6374ProcInfo      MplsPrcInfo;
    tR6374NotifyInfo    R6374LmNotify;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374MplsPathInfo *pMplsPathInfo = NULL;
    UINT1               au1DestMac[MAC_ADDR_LEN];
    UINT1               u1Method = RFC6374_INIT_VAL;
    UINT1               u1MeasurementType = RFC6374_INIT_VAL;
    UINT1               u1Mode = RFC6374_INIT_VAL;
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT4               u4IfIndex = RFC6374_INIT_VAL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering the R6374MplsTxPkt\r\n");

    if (pBuf == NULL)
    {
        /* Packet buffer or MPLS path information is NULL */
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsTxPkt: "
                     "Packet buffer or MPLS " "path information is NULL \r\n");
        return RFC6374_FAILURE;
    }

    /* Allocate Memory for MPLS Path Info Structure */
    pMplsPathInfo = (tR6374MplsPathInfo *) MemAllocMemBlk
        (RFC6374_MPLS_PATH_INFO_POOLID);
    if (pMplsPathInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsTxPkt: " "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (au1DestMac, RFC6374_INIT_VAL, MAC_ADDR_LEN);
    RFC6374_MEMSET (&MplsPrcInfo, RFC6374_INIT_VAL, sizeof (tR6374ProcInfo));
    RFC6374_MEMSET (pMplsPathInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374MplsPathInfo));
    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPduSmInfo);
    pPduSmInfo->pMplsParams = &(pServiceInfo->R6374MplsParams);

    u4ContextId = pServiceInfo->u4ContextId;

    if (u1ServiceType == RFC6374_MEASUREMENT_TYPE_LOSS)
    {
        /*  u1Method = pServiceInfo->LmInfo.u1LmMethod; */
        u1Method = pPduSmInfo->u1PduType;
        u1Mode = pServiceInfo->LmInfo.u1LmMode;
        u1MeasurementType = pServiceInfo->LmInfo.u1LmType;
    }
    else if (u1ServiceType == RFC6374_MEASUREMENT_TYPE_DELAY)
    {
        u1Mode = pServiceInfo->DmInfo.u1DmMode;
        u1MeasurementType = pServiceInfo->DmInfo.u1DmType;
    }
    else if (u1ServiceType == RFC6374_MEASUREMENT_TYPE_LOSSDELAY)
    {
        /*  u1Method = pServiceInfo->LmDmInfo.u1LmDmMethod; */
        u1Method = pPduSmInfo->u1PduType;
        u1Mode = pServiceInfo->LmDmInfo.u1LmDmMode;
        u1MeasurementType = pServiceInfo->LmDmInfo.u1LmDmType;
    }

    pPduSmInfo->pMplsParams->u1MeasurementType = u1MeasurementType;
    pPduSmInfo->pMplsParams->u1EncapType = pServiceInfo->u1EncapType;

    /* Get the MPLS path information from MPLS module */
    if (R6374GetMplsPathInfo (u4ContextId,
                              pPduSmInfo->pMplsParams,
                              u1Mode, pMplsPathInfo) == RFC6374_FAILURE)
    {
        /* Path Status down */
        R6374LmNotify.u1TrapType = RFC6374_RES_UNAVAIL_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsTxPkt: " "MPLS Path Information get failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                (UINT1 *) pMplsPathInfo);
        return RFC6374_FAILURE;
    }

    if (pMplsPathInfo->u1PathStatus == MPLS_PATH_STATUS_DOWN)
    {
        /* Path Status down */
        R6374LmNotify.u1TrapType = RFC6374_RES_UNAVAIL_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsTxPkt: " " MPLS Path status is down\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                (UINT1 *) pMplsPathInfo);
        return RFC6374_FAILURE;
    }

    MplsPrcInfo.pMplsParms = pPduSmInfo->pMplsParams;
    MplsPrcInfo.u1TrafficClass = pServiceInfo->u1TrafficClass;
    MplsPrcInfo.u1Ttl = pServiceInfo->u1TimeToLive;

    /* Construct MPLS headers */
    if (R6374MplsConstructPacket (&MplsPrcInfo, pMplsPathInfo,
                                  &u4IfIndex, u1ServiceType, u1Method,
                                  au1DestMac, pBuf) == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsTxPkt: " "MPLS Packet Construction failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                (UINT1 *) pMplsPathInfo);
        return RFC6374_FAILURE;
    }

    /* Dump the packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374MplsTxPkt: [Sending] Dumping RFC6374 PDU along with "
                      "MPLS Header\r\n");

    /* Send the packet to MPLS-RTR module for MPLS forwarding */
    if (R6374SendPktToMpls (u4ContextId, u4IfIndex, au1DestMac, pBuf)
        == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374MplsTxPkt: "
                     "sending packet to MPLS-RTR module failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                                (UINT1 *) pMplsPathInfo);
        return RFC6374_FAILURE;
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLS_PATH_INFO_POOLID,
                            (UINT1 *) pMplsPathInfo);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting from R6374MplsTxPkt\r\n");
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374GetMplsPathInfo
 *
 * Description        : This routine is used to fetch the MPLS path
 *                      information from MPLS module.
 *
 * Input(s)           : u4ContextId - Context Id
 *                      pMplsParams - pointer to the path identifier
 *                      structure
 *                      u1Mode - LM/DM/CombLMDM mode Pro-active/On-Demand
 *
 * Output(s)          : tR6374MplsPathInfo - pointer to the structure
 *                      containing the path information
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
PUBLIC INT4
R6374GetMplsPathInfo (UINT4 u4ContextId,
                      tR6374MplsParams * pR6374MplsPathParams,
                      UINT1 u1Mode, tR6374MplsPathInfo * pR6374MplsPathInfo)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    BOOL1               bGetMplsTnlInfoFromPw = OSIX_FALSE;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering the R6374GetMplsPathInfo\r\n");

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (RFC6374_MPLSINPUTPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetMplsPathInfo: " "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    /* Allocate memory for the Output structure from
     * MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (RFC6374_MPLSOUTPUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetMplsPathInfo: " "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (pMplsApiInInfo, RFC6374_INIT_VAL, sizeof (tMplsApiInInfo));
    RFC6374_MEMSET (pMplsApiOutInfo, RFC6374_INIT_VAL,
                    sizeof (tMplsApiOutInfo));

    /*Check The Path Type */
    if (pR6374MplsPathParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        /* Fill the MPLS Input information to fetch the
         * complete PW path information */
        pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;

        /* Fill the path id with PW VC ID */
        pMplsApiInInfo->InPathId.PwId.u4VcId =
            pR6374MplsPathParams->PwMplsPathParams.u4PwId;

        /* Fetch PW path information from MPLS module */
        if (R6374MplsHandleExtInteraction (MPLS_GET_PW_INFO,
                                           pMplsApiInInfo,
                                           pMplsApiOutInfo) == RFC6374_FAILURE)
        {
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
            return RFC6374_FAILURE;
        }
        /* Check the PW status */
        if (pMplsApiOutInfo->OutPwInfo.i1OperStatus != MPLS_OPER_UP)
        {
            pR6374MplsPathInfo->u1PathStatus = MPLS_PATH_STATUS_DOWN;
        }
        /* Copy the PW information, to Path info structure, from
         * MPLS output structure */
        RFC6374_MEMCPY (&(pR6374MplsPathInfo->MplsPwApiInfo),
                        &(pMplsApiOutInfo->OutPwInfo), sizeof (tPwApiInfo));

        pR6374MplsPathInfo->u2PathFilled |= RFC6374_CHECK_PATH_PW_AVAIL;

        if ((pR6374MplsPathInfo->MplsPwApiInfo.u1MplsType == L2VPN_MPLS_TYPE_TE)
            || (pR6374MplsPathInfo->MplsPwApiInfo.u1MplsType ==
                L2VPN_MPLS_TYPE_NONTE))
        {
            /* Underlying path for PW is TE tunnel */
            bGetMplsTnlInfoFromPw = OSIX_TRUE;
        }
    }

    /* Check if Path type is tunnel */
    if ((pR6374MplsPathParams->u1MplsPathType == MPLS_PATH_TYPE_TUNNEL) ||
        (bGetMplsTnlInfoFromPw == OSIX_TRUE))
    {
        /* Fill the MPLS Input information */
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

        if (bGetMplsTnlInfoFromPw == OSIX_TRUE)
        {
            /* Fill the underlying tunnel id, with info from PW */
            RFC6374_MEMCPY (&(pMplsApiInInfo->InPathId.TnlId),
                            &(pR6374MplsPathInfo->MplsPwApiInfo.TeTnlId),
                            sizeof (tMplsTnlLspId));
            pMplsApiInInfo->InPathId.TnlId.
                DstNodeId.MplsRouterId.u4_addr[0] =
                pR6374MplsPathParams->PwMplsPathParams.u4IpAddr;
        }
        else
        {
            /* Fill the MPLS tunnel identifier information */
            pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                pR6374MplsPathParams->LspMplsPathParams.u4FrwdTunnelId;

            pMplsApiInInfo->InPathId.TnlId.
                SrcNodeId.MplsRouterId.u4_addr[0] =
                pR6374MplsPathParams->LspMplsPathParams.u4SrcIpAddr;

            pMplsApiInInfo->InPathId.TnlId.
                DstNodeId.MplsRouterId.u4_addr[0] =
                pR6374MplsPathParams->LspMplsPathParams.u4DestIpAddr;
        }

        /* Fetch MPLS Tunnel path information from MPLS module */
        if (R6374MplsHandleExtInteraction (MPLS_GET_TUNNEL_INFO,
                                           pMplsApiInInfo,
                                           pMplsApiOutInfo) == RFC6374_FAILURE)
        {
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, " R6374GetMplsPathInfo: "
                         "Unable to fetch Fwd Tunnel/PW info from MPLS\r\n");
            return RFC6374_FAILURE;
        }
        /* No need to check the tunnel state,
         * if the tunnel is an underlying path for PW. In that case,
         * PW state is sufficient. If the MPLS path is Tunnel
         * then check for the tunnel role. For Forward tunnel, the role
         * will be Egress and Reverse tunnel, the role will be Ingress.*/
        if (bGetMplsTnlInfoFromPw != OSIX_TRUE)
        {
            if ((pMplsApiOutInfo->OutTeTnlInfo.u1OperStatus == MPLS_OPER_DOWN)
                || (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS))
            {
                pR6374MplsPathInfo->u1PathStatus = MPLS_PATH_STATUS_DOWN;
            }
        }

        /* Copy MPLS tunnel path information */
        MEMCPY (&(pR6374MplsPathInfo->MplsTeTnlApiInfo),
                &(pMplsApiOutInfo->OutTeTnlInfo), sizeof (tTeTnlApiInfo));

        pR6374MplsPathInfo->u2PathFilled |= RFC6374_CHECK_PATH_TE_AVAIL;

        /*Check the oper status of the Reverse Tunnel Information */

        if ((bGetMplsTnlInfoFromPw != OSIX_TRUE) &&
            (pR6374MplsPathParams->u1MeasurementType ==
             CLI_RFC6374_TYPE_TWO_WAY) && (u1Mode != CLI_RFC6374_MODE_PROACTIVE)
            && (pR6374MplsPathParams->LspMplsPathParams.u4RevTunnelId !=
                RFC6374_INIT_VAL))
        {
            /* u4SrcTnlNum ---> u4RevTunnelId;
             * SrcNodeId.MplsRouterId.u4_addr[0] ---> u4DestIpAddr;
             * DstNodeId.MplsRouterId.u4_addr[0] ---> u4SrcIpAddr;
             */

            /* Fill the MPLS Input information */
            pMplsApiInInfo->u4ContextId = u4ContextId;
            pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
            pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

            /* Fill the MPLS tunnel identifier information */
            pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                pR6374MplsPathParams->LspMplsPathParams.u4RevTunnelId;

            pMplsApiInInfo->InPathId.TnlId.
                SrcNodeId.MplsRouterId.u4_addr[0] =
                pR6374MplsPathParams->LspMplsPathParams.u4DestIpAddr;

            pMplsApiInInfo->InPathId.TnlId.
                DstNodeId.MplsRouterId.u4_addr[0] =
                pR6374MplsPathParams->LspMplsPathParams.u4SrcIpAddr;

            /* Fetch MPLS Tunnel path information from MPLS module */
            if (R6374MplsHandleExtInteraction (MPLS_GET_TUNNEL_INFO,
                                               pMplsApiInInfo,
                                               pMplsApiOutInfo) ==
                RFC6374_FAILURE)
            {
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsApiInInfo);
                RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsApiOutInfo);

                /*Reset the u2PathFilled */
                pR6374MplsPathInfo->u2PathFilled &=
                    (UINT2) (~RFC6374_CHECK_PATH_TE_AVAIL);
                /* Unable to Fetch Rev Tunnel Info from MPLS indicates
                 * Reverse Tunnel is not yet created in this Node*/
                pR6374MplsPathInfo->u1PathStatus = MPLS_PATH_STATUS_DOWN;

                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             " R6374GetMplsPathInfo: "
                             "Unable to fetch Rev Tunnel info from MPLS\r\n");
                return RFC6374_FAILURE;
            }

            if (pMplsApiOutInfo->OutTeTnlInfo.u1OperStatus == MPLS_OPER_DOWN)
            {
                pR6374MplsPathInfo->u1PathStatus = MPLS_PATH_STATUS_DOWN;
            }
        }

    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting from R6374GetMplsPathInfo\r\n");
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function     : R6374MplsConstructPacket
 *
 * Description  : Constructs the MPLS headers
 *
 * Input        : pMplsProcInfo  - Pointer to Mpls ProcInfo structure
 *                pMplsPathInfo - Pointer to the entire MPLS path info
 *
 * Output       : pu4IfIndex  - Pointer to the interface index
 *                pu1DestMac  - Contains the next hop MAC address
 *                pBuf        - Pointer to CRU Buf which contains constructed
 *                              packet
 *
 * Returns      : RFC6374_SUCCESS/RFC6374_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
R6374MplsConstructPacket (tR6374ProcInfo * pMplsProcInfo,
                          tR6374MplsPathInfo * pMplsPathInfo,
                          UINT4 *pu4IfIndex, UINT1 u1ServiceType,
                          UINT1 u1Method, UINT1 *pu1DestMac,
                          tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tMplsHdr            MplsHdr;
    tMplsAchHdr         MplsAchHdr;
    tMplsAchHdr         MplsGAchHdr;
    tMplsLblInfo        MplsLblInfo[MPLS_MAX_LABEL_STACK];
    UINT1               u1LblCnt = RFC6374_INIT_VAL;
    UINT1               u1Count = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering the R6374MplsConstructPacket\r\n");

    RFC6374_MEMSET (&MplsHdr, RFC6374_INIT_VAL, sizeof (tMplsHdr));
    RFC6374_MEMSET (&MplsAchHdr, RFC6374_INIT_VAL, sizeof (tMplsAchHdr));
    RFC6374_MEMSET (&MplsGAchHdr, RFC6374_INIT_VAL, sizeof (tMplsAchHdr));

    /* It is mandatory to have the G-ACH and Ach header
     * Because from this header only R6374 will be extracting the Protocol Type
     * before forwarding*/
    /*Fill G-ACH Header */

    MplsGAchHdr.u4AchType = 1;
    MplsGAchHdr.u1Version = 0;
    MplsGAchHdr.u1Rsvd = 0;

    if (u1ServiceType == RFC6374_MEASUREMENT_TYPE_LOSS)
    {
        if (u1Method == RFC6374_LM_METHOD_DIRECT)
        {
            MplsGAchHdr.u2ChannelType = RFC6374_CHANNEL_TYPE_DIRECT_LM;
        }
        if (u1Method == RFC6374_LM_METHOD_INFERED)
        {
            MplsGAchHdr.u2ChannelType = RFC6374_CHANNEL_TYPE_INFERED_LM;
        }

        /* Call MPLS utility function to fill G-ACH
         * header in the buffer */
        MplsUtlFillMplsAchHdr (&MplsGAchHdr, pBuf);
    }
    if (u1ServiceType == RFC6374_MEASUREMENT_TYPE_LOSSDELAY)
    {
        if (u1Method == RFC6374_LMDM_METHOD_DIRECT)
        {
            MplsGAchHdr.u2ChannelType = RFC6374_CHANNEL_TYPE_DIRECT_LMDM;
        }
        if (u1Method == RFC6374_LMDM_METHOD_INFERED)
        {
            MplsGAchHdr.u2ChannelType = RFC6374_CHANNEL_TYPE_INFERED_LMDM;
        }
        /* Call MPLS utility function to fill G-ACH
         * header in the buffer */
        MplsUtlFillMplsAchHdr (&MplsGAchHdr, pBuf);
    }

    else if (u1ServiceType == RFC6374_MEASUREMENT_TYPE_DELAY)
    {
        MplsGAchHdr.u2ChannelType = RFC6374_CHANNEL_TYPE_DM;
        MplsUtlFillMplsAchHdr (&MplsGAchHdr, pBuf);
    }
    /* Fill the LM/DM or Combined LMDM opcode and Priority in CRU Buffer */
    RFC6374_BUF_SET_OPCODE (pBuf, MplsGAchHdr.u2ChannelType);
    RFC6374_BUF_SET_PRIORITY (pBuf, pMplsProcInfo->u1TrafficClass);

    if (pMplsProcInfo->pMplsParms->u1EncapType == RFC6374_ENCAP_GAL_GACH)
    {
        /* Fill GAL Label */
        MplsHdr.u4Lbl = RFC6374_GAL_LABEL;
        MplsHdr.Ttl = 1;
        MplsHdr.Exp = pMplsProcInfo->u1TrafficClass;
        MplsHdr.SI = 1;            /*Bottom of Label Stack */
        /* Call MPLS utility function to fill GAL Label */
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
    }

    /* Check if path is PW */
    if ((pMplsPathInfo->u2PathFilled & RFC6374_CHECK_PATH_PW_AVAIL) ==
        RFC6374_CHECK_PATH_PW_AVAIL)
    {
        /* Fill PW Label */
        MplsHdr.u4Lbl = pMplsPathInfo->MplsPwApiInfo.u4OutVcLabel;

        if (pMplsPathInfo->MplsPwApiInfo.
            u1CcSelected & RFC6374_L2VPN_VCCV_CC_TTL_EXP)
        {
            MplsHdr.Ttl = 1;
        }
        else
        {
            MplsHdr.Ttl = pMplsPathInfo->MplsPwApiInfo.u1Ttl;
        }
        MplsHdr.Exp = pMplsProcInfo->u1TrafficClass;
        /* if GAL label exists. 'S' bit should */
        /* be set to 0 */
        if (pMplsProcInfo->pMplsParms->u1EncapType == RFC6374_ENCAP_GAL_GACH)
        {
            MplsHdr.SI = 0;
        }
        else
        {
            MplsHdr.SI = 1;        /*Bottom of Label Stack */
        }
        /* Call MPLS utility function to fill PW Label */
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);

        if (pMplsPathInfo->MplsPwApiInfo.
            u1CcSelected & RFC6374_L2VPN_VCCV_CC_RAL)
        {
            /*Set the Router Alert Label */
            MplsHdr.u4Lbl = 1;
            MplsHdr.Ttl = 1;
            MplsHdr.Exp = pMplsProcInfo->u1TrafficClass;
            MplsHdr.SI = 0;

            /* Call MPLS utility function to fill RAL Label */
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        }
    }

    /* Fill MPLS TE tunnel Label */
    if ((pMplsPathInfo->u2PathFilled & RFC6374_CHECK_PATH_TE_AVAIL) ==
        RFC6374_CHECK_PATH_TE_AVAIL)
    {
        /* Fill the out interface index */
        *pu4IfIndex = pMplsPathInfo->MplsTeTnlApiInfo.u4TnlIfIndex;

        /* Fill the Next hop MAC address */
        MEMCPY (pu1DestMac, pMplsPathInfo->MplsTeTnlApiInfo.au1NextHopMac,
                MAC_ADDR_LEN);

        /* Copy Label stack information */
        u1LblCnt = pMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
            FwdOutSegInfo.u1LblStkCnt;

        if (u1LblCnt <= MPLS_MAX_LABEL_STACK)
        {
            MEMCPY (MplsLblInfo,
                    pMplsPathInfo->
                    MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo.LblInfo,
                    (sizeof (tMplsLblInfo) * u1LblCnt));
        }
        else
        {
            /* Exceeding max label stack depth.
             * stack depth : MPLS_MAX_LABEL_STACK */
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374MplsConstructPacket: "
                         "Max No. of Label stack exceeded\r\n");
            return RFC6374_FAILURE;
        }
        /* Fill Tunnel Label */
        /* while (u1Count < u1LblCnt) */
        u1Count = u1LblCnt;
        while (u1Count > 0)
        {
            MplsHdr.u4Lbl = MplsLblInfo[u1Count - 1].u4Label;
            /* For the service created over LDP  (Non-Te)without static
             * tunnel creation, label information wont be available in 
             * MplsLblInfo strcuture . so it will be taken from
             * OutTeTnlInfo.TnlLspId.u4SrcTnlNum and this value is updated
             * via MPLS API*/
            if (MplsHdr.u4Lbl == RFC6374_INIT_VAL)
            {
                MplsHdr.u4Lbl =
                    pMplsPathInfo->MplsTeTnlApiInfo.TnlLspId.u4SrcTnlNum;
            }
            MplsHdr.Ttl = pMplsProcInfo->u1Ttl;
            MplsHdr.Exp = pMplsProcInfo->u1TrafficClass;

            /* GAL label exists. 'S' bit should */
            /* be set to 0 */
            if ((pMplsPathInfo->u2PathFilled & RFC6374_CHECK_PATH_PW_AVAIL) ==
                RFC6374_CHECK_PATH_PW_AVAIL)
            {
                MplsHdr.SI = 0;
            }
            else
            {
                /* for service created on a tunnel with encap type as GAL, 
                 * two labels will be applicable. Tunnel label and GAL 
                 * label. if encap type is selected as without gal 
                 * then only one label will be available and 
                 * that is tunnel label. So it has to be updated as
                 * bottol of stack. in other cases it will be updated as
                 * zero */
                if (pMplsProcInfo->pMplsParms->u1EncapType ==
                    RFC6374_ENCAP_GAL_GACH)
                {
                    MplsHdr.SI = 0;
                }
                else
                {
                    MplsHdr.SI = 1;
                    MplsHdr.Ttl = 2;
                }
            }
            /* Call MPLS utility function to fill MPLS Label */
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
            u1Count--;
        };
    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting from R6374MplsConstructPacket\r\n");
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function     : R6374SendPktToMpls
 *
 * Description  : This function is used to call external OAM function to
 *                give the packet to MPLS-RTR to forward
 * 
 * Input        : u4ContextId - Context Id
 *                u4IfIndex   - Interface Index
 *                pu1DestMac  - Contains Next hop MAC address
 *                pBuf        - Pointer to the packet CRU Buffer
 *
 * Output       : None
 *
 * Returns      : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
PUBLIC INT4
R6374SendPktToMpls (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DestMac,
                    tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering the R6374SendPktToMpls\r\n");

    /* Allocate memory for the Input structure to
     * MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (RFC6374_MPLSINPUTPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4ContextId, "R6374SendPktToMpls: "
                     "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    /* Allocate memory for the Output structure from
     * MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (RFC6374_MPLSOUTPUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4ContextId, "R6374SendPktToMpls: "
                     "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (pMplsApiInInfo, RFC6374_INIT_VAL, sizeof (tMplsApiInInfo));
    RFC6374_MEMSET (pMplsApiOutInfo, RFC6374_INIT_VAL,
                    sizeof (tMplsApiOutInfo));

    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
    pMplsApiInInfo->u4ContextId = u4ContextId;
    pMplsApiInInfo->InPktInfo.u4OutIfIndex = u4IfIndex;
    pMplsApiInInfo->InPktInfo.bIsUnNumberedIf = FALSE;
    pMplsApiInInfo->InPktInfo.bIsMplsLabelledPacket = TRUE;

    MEMCPY (pMplsApiInInfo->InPktInfo.au1DstMac, pu1DestMac, MAC_ADDR_LEN);

    /* Point the buffer to input structure */
    pMplsApiInInfo->InPktInfo.pBuf = pBuf;
    u4PduSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    UNUSED_PARAM (u4PduSize);
    /* Send the packet to MPLS-RTR module for MPLS forwarding */
    i4RetVal = R6374MplsHandleExtInteraction (MPLS_PACKET_HANDLE_FROM_APP,
                                              pMplsApiInInfo, pMplsApiOutInfo);

    if (i4RetVal == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4ContextId, "R6374SendPktToMpls: "
                     "MPLS Handle return Failure\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
        return RFC6374_FAILURE;
    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting from R6374SendPktToMpls\r\n");
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 *    FUNCTION NAME    : R6374ProcessMplsPdu     
 *
 *    DESCRIPTION      : This function parse the MPLS Labels/Headers and
 *                       processes the RFC6374 PDU (LM/DM) received over
 *                       the MPLS-TP networks. This routine invokes the
 *                       sub-routines to achieve the PDU reception.
 *                       
 *    INPUT            : pBuf - Pointer to the Buffer containing the PDU.     
 *                       u4IfIndex - Interface index over which the PDU has
 *                                   been received.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : RFC6374_SUCCESS/RFC6374_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
R6374ProcessMplsPdu (tR6374BufChainHeader * pBuf, UINT4 u4IfIndex)
{
    tR6374MplsParams    R6374MplsParams;
    UINT4               u4PktLen = RFC6374_INIT_VAL;
    UINT4               u4Offset = RFC6374_INIT_VAL;
    UINT2               u2RxChnType = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 "R6374ProcessMplsPdu: "
                 "RFC6374 PDU received over MPLS-TP path \r\n");

    RFC6374_MEMSET (&R6374MplsParams, 0, sizeof (tR6374MplsParams));

    u4PktLen = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /* Dump the packet */
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PktLen,
                      "R6374ProcessMplsPdu: [Receving] Dumping RFC6374 PDU with "
                      "MPLS headers\r\n");

    if (R6374RxProcessMplsHeaders (pBuf, u4IfIndex, u4PktLen,
                                   &R6374MplsParams) == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374ProcessMplsPdu: Unable to parse and process "
                     "the MPLS Labels/headers...\r\n");
        return RFC6374_FAILURE;
    }

    /*    0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       GACH  |0 0 0 1|Version|   Reserved    |         Channel Type          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       ACH   |0 0 0 1|Version|   Reserved    |         Channel Type          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    /* Pointer is now Pointing to ACH Header */
    /* Fetch the Channel Type  */
    /* Move the offset to point to the channel type field 2 bytes */
    u4Offset = u4Offset + RFC6374_ACH_HEADER_VER_RSVD_LEN;

    /* Get RxChnType from Received PDU */
    RFC6374_CRU_GET_2_BYTE (pBuf, u4Offset, u2RxChnType);

    /* Move Pointer to PDU Header */
    if (RFC6374_CRU_BUF_MOVE_VALID_OFFSET (pBuf, RFC6374_ACH_HEADER_LEN) ==
        CRU_FAILURE)
    {
        /* Unable to remove Mpls Header */
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374ProcessMplsPdu: " "Unable to move the offset \r\n");
        return RFC6374_FAILURE;
    }

    switch (u2RxChnType)
    {
        case RFC6374_CHANNEL_TYPE_DIRECT_LM:
        case RFC6374_CHANNEL_TYPE_INFERED_LM:
        case RFC6374_CHANNEL_TYPE_DIRECT_LMDM:
        case RFC6374_CHANNEL_TYPE_INFERED_LMDM:
        case RFC6374_CHANNEL_TYPE_DM:
            if (R6374MplsProcessPdu (pBuf, &R6374MplsParams, u2RxChnType) ==
                RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ProcessMplsPdu: "
                             "Unable to process the RFC6374 PDU...\r\n");
                return RFC6374_FAILURE;
            }
            break;
        default:
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374ProcessMplsPdu: "
                         "Unable to process the RFC6374 PDU...\r\n");
            return RFC6374_FAILURE;
    }

    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : R6374MplsProcessPdu
 *
 * Description        : This routine is used to process the RFC6374 PDU
 *                      received over the MPLS networks. This routine
 *                      identifies the service information based on the LSP/PW
 *                      identifier.
 *
 * Input(s)           : pBuf        - Pointer to the CRU Buffer of the received PDU.
 *                      pMplsParams - Pointer to the LSP/PW ID.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
R6374MplsProcessPdu (tR6374BufChainHeader * pBuf,
                     tR6374MplsParams * pMplsParams, UINT2 u2ChnType)
{
    PRIVATE tR6374PktSmInfo PduSmInfo;
    tR6374MplsParams    MplsParams;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    UINT1              *pu1R6374Pdu = NULL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&MplsParams, 0, sizeof (tR6374MplsParams));
    RFC6374_MEMSET (&PduSmInfo, 0, sizeof (tR6374PktSmInfo));

    RFC6374_MEMCPY (&MplsParams, pMplsParams, sizeof (tR6374MplsParams));

    pServiceInfo = R6374UtilGetServiceFromMplsParams (pMplsParams);
    if (pServiceInfo == NULL)
    {
        return RFC6374_FAILURE;
    }
    PduSmInfo.pBuf = pBuf;
    PduSmInfo.pServiceConfigInfo = pServiceInfo;
    PduSmInfo.u1PduType = R6374MplsGetPduType (pBuf, u2ChnType);

    /* Get the PDU from CUR Buffer */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /* Dump the packet */
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374MplsProcessPdu: [Receving] Dumping RFC6374 PDU\r\n");

    pu1R6374Pdu = RFC6374_GET_DATA_PTR_IF_LINEAR (pBuf, 0, u4PduSize);

    if (pu1R6374Pdu != NULL)
    {
        if (R6374RxDeMux (PduSmInfo, pu1R6374Pdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374MplsProcessPdu: "
                         "R6374RxMux returned Failure\r\n");
            return RFC6374_FAILURE;
        }
    }

    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374RxProcessMplsHeaders
 *
 * Description        : This routine is used to process the MPLS headers.
 *                      Strips the MPLS headers and fills the Path Id
 *                      information by querying MPLS module.
 *                      and LBLT tasks
 *
 * Input(s)           : pBuf - CRU Buffer Pointer to the packet with 
 *                      MPLS headers
 *                      u4IfIndex - Incoming Interface Index
 *                      u4PktLen  - Length of the packet
 *
 * Output(s)          : pBuf - CRU Buffer Pointer to the packet without
 *                      MPLS headers
 *                      pMplsPathId - pointer to the MPLS path identifier
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
PUBLIC INT4
R6374RxProcessMplsHeaders (tR6374BufChainHeader * pBuf, UINT4 u4IfIndex,
                           UINT4 u4PktLen, tR6374MplsParams * pMplsPathId)
{
    tMplsHdr            MplsHdr;
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    UINT4               u4OffSet = RFC6374_INIT_VAL;
    UINT4               u4InLabel = RFC6374_INIT_VAL;
    BOOL1               b1RalFlag = RFC6374_FALSE;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 "R6374RxProcessMplsHeaders: " "Process MPLS Headers\r\n");

    MEMSET (&MplsHdr, RFC6374_INIT_VAL, sizeof (tMplsHdr));

    /* Set Path as Tunnel */
    /*u4PathType = MPLS_PATH_TYPE_TUNNEL; */
    R6374RxGetMplsHdr (pBuf, &MplsHdr);
    /* Given Tunnel Lable */
    u4InLabel = MplsHdr.u4Lbl;

    do
    {
        R6374RxGetMplsHdr (pBuf, &MplsHdr);

        if (b1RalFlag)
        {
            /* Given PW Lable */
            u4InLabel = MplsHdr.u4Lbl;
            b1RalFlag = RFC6374_FALSE;
        }

        /* Check for RAL Lable */
        if (MplsHdr.u4Lbl == RFC6374_RAL_LABEL)
        {
            /*u4PathType = MPLS_PATH_TYPE_PW; */
            b1RalFlag = RFC6374_TRUE;
        }

        if (RFC6374_CRU_BUF_MOVE_VALID_OFFSET (pBuf, MPLS_HDR_LEN) ==
            CRU_FAILURE)
        {
            /* Unable to remove Mpls Header */
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374RxProcessMplsHeaders: "
                         "Unable to move the offset \r\n");
            return RFC6374_FAILURE;
        }
    }
    while ((MplsHdr.SI != 1) && (u4OffSet < u4PktLen));

    if (u4OffSet >= u4PktLen)
    {
        /* Invalid Packet encapsulation. No SI bit set in MPLS Hdr */
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374RxProcessMplsHeaders: "
                     "Invalid packet encapsulation \r\n");
        return RFC6374_FAILURE;
    }

    /* Allocate memory for the Input structure to MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (RFC6374_MPLSINPUTPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374RxProcessMplsHeaders: "
                     "Mempool allocation failed for pMplsApiInInfo \r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    /* Allocate memory for the Output structure from MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (RFC6374_MPLSOUTPUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374RxProcessMplsHeaders: "
                     "Mempool allocation failed for pMplsApiOutInfo \r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));
    /* Fill the MPLS Input structure */
    pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;
    pMplsApiInInfo->InInLblInfo.u4InIf = u4IfIndex;
    pMplsApiInInfo->InInLblInfo.u4Inlabel = u4InLabel;

    R6374MplsHandleExtInteraction (MPLS_INCR_IN_PACKET_COUNT,
                                   pMplsApiInInfo, pMplsApiOutInfo);

    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Get path information from the In label & interface */
    if (R6374MplsHandleExtInteraction (MPLS_GET_PATH_FROM_INLBL_INFO,
                                       pMplsApiInInfo,
                                       pMplsApiOutInfo) == OSIX_FAILURE)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiOutInfo);
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374RxProcessMplsHeaders: "
                     "Getting the path information failed \r\n");
        return RFC6374_FAILURE;
    }

    pMplsPathId->u1MplsPathType = (UINT1) pMplsApiOutInfo->u4PathType;

    if (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_PW)
    {
        pMplsPathId->PwMplsPathParams.u4PwId =
            pMplsApiOutInfo->OutPwInfo.MplsPwPathId.u4VcId;
    }
    else
    {
        pMplsPathId->LspMplsPathParams.u4RevTunnelId =
            pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4SrcTnlNum;
        /*        pMplsPathId->LspMplsPathParams.u4LspId =
           pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4LspNum;
         */ pMplsPathId->LspMplsPathParams.u4SrcIpAddr =
            pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum;
        pMplsPathId->LspMplsPathParams.u4DestIpAddr =
            pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4DstLocalMapNum;
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);

    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function     : R6374RxGetMplsHdr                                            
 *                                                                           
 * Description  : Parse the MPLS Header from the given buffer                
 *                                                                           
 * Input        : pBuf   - Pointer to the buffer                             
 *                                                                           
 * Output       : pHdr   - pointer to tMplsHdr to parse the MPLS header      
 *                                                                           
 * Returns      : None                                                       
 *****************************************************************************/
PUBLIC VOID
R6374RxGetMplsHdr (tR6374BufChainHeader * pBuf, tMplsHdr * pHdr)
{
    UINT4               u4Hdr = 0;

    RFC6374_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u4Hdr, 0, MPLS_HDR_LEN);
    u4Hdr = OSIX_NTOHL (u4Hdr);
    pHdr->u4Lbl = (u4Hdr & 0xfffff000) >> 12;
    pHdr->Exp = (u4Hdr & 0x00000e00) >> 9;
    pHdr->SI = (u4Hdr & 0x00000100) >> 8;
    pHdr->Ttl = u4Hdr & 0x000000ff;
}

/*****************************************************************************
 * Name               : R6374MplsGetPduType
 *
 * Description        : This routine is called to get the PDU type so that 
 *                      accordingly responder will be called
 *
 * Input(s)           : pBuf - Pointer to the received buffer
 *                      u2ChnType - Channel Type
 *
 * Output(s)          : None
 *
 * Return Value(s)    : u1PduType - LMM,LMR,DMM,DMR
 *****************************************************************************/
PUBLIC UINT1
R6374MplsGetPduType (tR6374BufChainHeader * pBuf, UINT2 u2ChnType)
{
    UINT1               u1PduType = 0;
    UINT1               u1VerFlag = 0;

    RFC6374_CRU_GET_1_BYTE (pBuf, 0, u1VerFlag);

    /* Check if it Querier or Responder */
    u1VerFlag = u1VerFlag & RFC6374_PDU_RESP_FLAG;

    if (u1VerFlag >> 3)            /* Response Flag Set */
    {
        if (RFC6374_CHANNEL_TYPE_DIRECT_LM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_DIRECT_LMR;
        }
        else if (RFC6374_CHANNEL_TYPE_INFERED_LM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_INFERED_LMR;
        }
        else if (RFC6374_CHANNEL_TYPE_DM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_DMR;
        }
        else if (RFC6374_CHANNEL_TYPE_DIRECT_LMDM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_DIRECT_LMRDMR;
        }
        else if (RFC6374_CHANNEL_TYPE_INFERED_LMDM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_INFERED_LMRDMR;
        }
    }
    else                        /* Query Flag Set */
    {
        if (RFC6374_CHANNEL_TYPE_DIRECT_LM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_DIRECT_LMM;
        }
        else if (RFC6374_CHANNEL_TYPE_INFERED_LM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_INFERED_LMM;
        }
        else if (RFC6374_CHANNEL_TYPE_DM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_DMM;
        }
        else if (RFC6374_CHANNEL_TYPE_DIRECT_LMDM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_DIRECT_LMMDMM;
        }
        else if (RFC6374_CHANNEL_TYPE_INFERED_LMDM == u2ChnType)
        {
            u1PduType = RFC6374_PDU_TYPE_INFERED_LMMDMM;
        }
    }
    return u1PduType;
}

/*****************************************************************************
 * Name               : R6374RxDeMux
 *
 * Description        : This routine is the demux routine
 *
 * Input(s)           : PduSmInfo - Pointer to the PduSmInfo
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS
 *****************************************************************************/
PUBLIC UINT4
R6374RxDeMux (tR6374PktSmInfo PduSmInfo, UINT1 *pu1R6374Pdu)
{
    switch (PduSmInfo.u1PduType)
    {
        case RFC6374_PDU_TYPE_DMM:
            R6374ProcessDmm (&PduSmInfo, pu1R6374Pdu);
            break;
        case RFC6374_PDU_TYPE_DMR:
            R6374ProcessDmr (&PduSmInfo, pu1R6374Pdu);
            break;
        case RFC6374_PDU_TYPE_INFERED_LMM:
        case RFC6374_PDU_TYPE_DIRECT_LMM:
            R6374ProcessLmm (&PduSmInfo, pu1R6374Pdu);
            break;
        case RFC6374_PDU_TYPE_INFERED_LMMDMM:
        case RFC6374_PDU_TYPE_DIRECT_LMMDMM:
            R6374ProcessLmmDmm (&PduSmInfo, pu1R6374Pdu);
            break;
        case RFC6374_PDU_TYPE_INFERED_LMR:
        case RFC6374_PDU_TYPE_DIRECT_LMR:
            R6374ProcessLmr (&PduSmInfo, pu1R6374Pdu);
            break;
        case RFC6374_PDU_TYPE_INFERED_LMRDMR:
        case RFC6374_PDU_TYPE_DIRECT_LMRDMR:
            R6374ProcessLmrDmr (&PduSmInfo, pu1R6374Pdu);
            break;
        default:
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374RxDeMux: " "Default Switch Case\r\n");
            return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : R6374GetMplsPktCnt 
 *
 * Description        : This function is used to get the Tx and Rx MPLS
                        Packet count from MPLS module. 
 *
 * Input(s)           : pServiceInfo - service related info
 *
 * Output(s)          : u4FwrdTnlTx - Forward Tunl Transmit count
 *                      u4FwrdTnlRx - Forward Tunl Recevice count
 *                      u4RcvTnlTx - Reverse Tunl Transmit count
 *                      u4RcvTnlRx - Reverse Tunl Recevice count
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
R6374GetMplsPktCnt (tR6374ServiceConfigTableEntry * pServiceInfo,
                    UINT4 *u4PwOrFwrdTnlTx, UINT4 *u4PwOrFwrdTnlRx,
                    UINT4 *u4RcvTnlTx, UINT4 *u4RcvTnlRx)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tR6374MplsParams   *pR6374MplsPathParams = NULL;

    pR6374MplsPathParams = &(pServiceInfo->R6374MplsParams);

    /* Allocate memory for the Input structure to MPLS module */
    pMplsApiInInfo = (tMplsApiInInfo *) MemAllocMemBlk
        (RFC6374_MPLSINPUTPARAMS_POOLID);
    if (pMplsApiInInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetMplsPktCnt: "
                     "Mempool allocation failed for pMplsApiInInfo \r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }
    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));

    /* Allocate memory for the Output structure from MPLS module */
    pMplsApiOutInfo = (tMplsApiOutInfo *) MemAllocMemBlk
        (RFC6374_MPLSOUTPUTPARAMS_POOLID);
    if (pMplsApiOutInfo == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsApiInInfo);
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetMplsPktCnt: "
                     "Mempool allocation failed for pMplsApiOutInfo \r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Fill the MPLS Input information to fetch the
     * Counter from MPLS */
    pMplsApiInInfo->u4ContextId = pServiceInfo->u4ContextId;
    pMplsApiInInfo->u4SrcModId = RFC6374_MODULE;

    pMplsApiInInfo->InPathId.u4PathType =
        pServiceInfo->R6374MplsParams.u1MplsPathType;

    pMplsApiInInfo->unIntInfo.MplsPktCntInfo.MplsPrevCount.
        u1StatsType = GET_MPLS_BOTH_STATS;

    if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
    {
        /* As for RFC6374 we need to fetch the counter for both forward
         * and reverse tunnel */

        /* Fetch for Forward Tunnel */
        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pR6374MplsPathParams->LspMplsPathParams.u4FrwdTunnelId;

        pMplsApiInInfo->InPathId.TnlId.
            SrcNodeId.MplsRouterId.u4_addr[0] =
            pR6374MplsPathParams->LspMplsPathParams.u4SrcIpAddr;

        pMplsApiInInfo->InPathId.TnlId.
            DstNodeId.MplsRouterId.u4_addr[0] =
            pR6374MplsPathParams->LspMplsPathParams.u4DestIpAddr;

        if (R6374MplsHandleExtInteraction
            (MPLS_GET_PACKET_COUNT, pMplsApiInInfo,
             pMplsApiOutInfo) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         u4ContextId, "R6374GetMplsPktCnt: "
                         "MPLS Handle return Failure\r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
            return RFC6374_FAILURE;
        }

        *u4PwOrFwrdTnlTx =
            pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4TxCnt;
        *u4PwOrFwrdTnlRx =
            pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4RxCnt;

        /* Fetch for Reverse Tunnel */
        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pR6374MplsPathParams->LspMplsPathParams.u4RevTunnelId;

        pMplsApiInInfo->InPathId.TnlId.
            SrcNodeId.MplsRouterId.u4_addr[0] =
            pR6374MplsPathParams->LspMplsPathParams.u4DestIpAddr;

        pMplsApiInInfo->InPathId.TnlId.
            DstNodeId.MplsRouterId.u4_addr[0] =
            pR6374MplsPathParams->LspMplsPathParams.u4SrcIpAddr;

        if (R6374MplsHandleExtInteraction
            (MPLS_GET_PACKET_COUNT, pMplsApiInInfo,
             pMplsApiOutInfo) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         u4ContextId, "R6374GetMplsPktCnt: "
                         "MPLS Handle return Failure\r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
            return RFC6374_FAILURE;
        }

        *u4RcvTnlTx =
            pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4TxCnt;
        *u4RcvTnlRx =
            pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4RxCnt;
    }
    else
    {
        pMplsApiInInfo->InPathId.PwId.u4VcId =
            pR6374MplsPathParams->PwMplsPathParams.u4PwId;

        if (R6374MplsHandleExtInteraction
            (MPLS_GET_PACKET_COUNT, pMplsApiInInfo,
             pMplsApiOutInfo) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         u4ContextId, "R6374GetMplsPktCnt: "
                         "MPLS Handle return Failure\r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiInInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsApiOutInfo);
            return RFC6374_FAILURE;
        }

        *u4PwOrFwrdTnlTx =
            pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4TxCnt;
        *u4PwOrFwrdTnlRx =
            pMplsApiOutInfo->unOutInfo.MplsPktCntRes.MplsPktCnt.u4RxCnt;
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiInInfo);
    RFC6374_FREE_MEM_BLOCK (RFC6374_MPLSOUTPUTPARAMS_POOLID,
                            (UINT1 *) pMplsApiOutInfo);
    return RFC6374_SUCCESS;
}
#endif
