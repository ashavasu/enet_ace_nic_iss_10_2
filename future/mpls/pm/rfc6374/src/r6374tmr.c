
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374tmr.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description : This file contains procedures containing R6374 Timer
 *               related operations
 *****************************************************************************/
#ifndef __R6374TMR_C__
#define __R6374TMR_C__
#include "r6374inc.h"

PRIVATE tR6374TmrDesc gaR6374TmrDesc[RFC6374_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : R6374TmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize R6374 Timer Descriptors                          *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
R6374TmrInitTmrDesc ()
{
    INT2                i2LmInfoOffset = RFC6374_INIT_VAL;
    INT2                i2DmInfoOffset = RFC6374_INIT_VAL;
    INT2                i2LmDmInfoOffset = RFC6374_INIT_VAL;

    i2LmInfoOffset =
        (INT2) (FSAP_OFFSETOF (tR6374ServiceConfigTableEntry, LmInfo));
    i2DmInfoOffset =
        (INT2) (FSAP_OFFSETOF (tR6374ServiceConfigTableEntry, DmInfo));
    i2LmDmInfoOffset =
        (INT2) (FSAP_OFFSETOF (tR6374ServiceConfigTableEntry, LmDmInfo));

    /* LM query Timer */
    gaR6374TmrDesc[RFC6374_LM_QUERY_TMR].pTmrExpFunc = R6374TmrLMQueryTmrExp;
    gaR6374TmrDesc[RFC6374_LM_QUERY_TMR].i2Offset = (INT2)
        (i2LmInfoOffset +
         (INT2) (FSAP_OFFSETOF (tR6374LmInfoTableEntry, LMQueryTimer)));

    /* DM query Timer */
    gaR6374TmrDesc[RFC6374_DM_QUERY_TMR].pTmrExpFunc = R6374TmrDMQueryTmrExp;
    gaR6374TmrDesc[RFC6374_DM_QUERY_TMR].i2Offset = (INT2)
        (i2DmInfoOffset +
         (INT2) (FSAP_OFFSETOF (tR6374DmInfoTableEntry, DMQueryTimer)));

    /* Combined LMDM query Timer */
    gaR6374TmrDesc[RFC6374_LMDM_QUERY_TMR].pTmrExpFunc =
        R6374TmrLMDMQueryTmrExp;
    gaR6374TmrDesc[RFC6374_LMDM_QUERY_TMR].i2Offset =
        (INT2) (i2LmDmInfoOffset +
                (INT2) (FSAP_OFFSETOF
                        (tR6374LmDmInfoTableEntry, LMDMQueryTimer)));

}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrInit                                               *
*                                                                           *
* Description  : Creates and initializes the R6374 Timer List               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RFC6374_SUCCESS/RFC6374_FAILURE                            *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
R6374TmrInit (VOID)
{
    if (TmrCreateTimerList ((CONST UINT1 *) RFC6374_TASK_NAME,
                            RFC6374_TIMER_EVENT, NULL,
                            (tTimerListId *) & (RFC6374_TIMER_LIST_ID ()))
        == TMR_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    R6374TmrInitTmrDesc ();

    return RFC6374_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrDeInit                                              *
*                                                                           *
* Description  : Deletes the R6374 Timer List                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
R6374TmrDeInit (VOID)
{
    if (RFC6374_TIMER_LIST_ID () != NULL)
    {
        TmrDeleteTimerList (RFC6374_TIMER_LIST_ID ());
    }

    MEMSET (gaR6374TmrDesc, 0, sizeof (gaR6374TmrDesc));

    return;
}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrStartTmr                                           *
*                                                                           *
* Description  : Starts R6374 Timer                                         *
*                                                                           *
* Input        : pR6374Tmr - pointer to tTmrBlk structure                   *
*                u1TmrId  - R6374 timer ID                                  *
*                u4Secs   - seconds                                         *
*                u4MilliSecs - MilliSeconds                                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RFC6374_SUCCESS/RFC6374_FAILURE                            *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R6374TmrStartTmr (UINT1 u1TimerType,
                  tR6374ServiceConfigTableEntry * pServiceInfo,
                  UINT4 u4Duration)
{
    tTmrBlk            *pR6374TmrBlk = NULL;
    UINT4               u4IntervalSec = RFC6374_INIT_VAL;
    UINT4               u4IntervalMSec = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_SUCCESS;

    if (u4Duration == 0)
    {
        return RFC6374_FAILURE;
    }

    switch (u1TimerType)
    {
        case RFC6374_LM_QUERY_TMR:
            pR6374TmrBlk = &(pServiceInfo->LmInfo.LMQueryTimer);
            break;
        case RFC6374_DM_QUERY_TMR:
            pR6374TmrBlk = &(pServiceInfo->DmInfo.DMQueryTimer);
            break;
        case RFC6374_LMDM_QUERY_TMR:
            pR6374TmrBlk = &(pServiceInfo->LmDmInfo.LMDMQueryTimer);
            break;
        default:
            i4RetVal = RFC6374_FAILURE;
            break;
    }
    if (i4RetVal == RFC6374_FAILURE)
    {
        return i4RetVal;
    }

    /* Converting Time Interval(in MSec) to Sec and MSec */
    u4IntervalSec = (u4Duration / 1000);
    u4IntervalMSec = (u4Duration % 1000);

    if (TmrStart (RFC6374_TIMER_LIST_ID (), pR6374TmrBlk, u1TimerType,
                  u4IntervalSec, u4IntervalMSec) == TMR_FAILURE)
    {
        return RFC6374_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrStopTmr                                            *
*                                                                           *
* Description  : Stop the R6374 Timer                                       *
*                                                                           *
* Input        : pR6374Tmr - pointer to tTmrBlk structure                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R6374TmrStopTmr (UINT1 u1TimerType,
                 tR6374ServiceConfigTableEntry * pServiceInfo)
{
    tTmrBlk            *pR6374TmrBlk = NULL;

    if (pServiceInfo == NULL)
    {
        return RFC6374_FAILURE;
    }

    switch (u1TimerType)
    {
        case RFC6374_LM_QUERY_TMR:
            pR6374TmrBlk = &(pServiceInfo->LmInfo.LMQueryTimer);
            break;
        case RFC6374_DM_QUERY_TMR:
            pR6374TmrBlk = &(pServiceInfo->DmInfo.DMQueryTimer);
            break;
        case RFC6374_LMDM_QUERY_TMR:
            pR6374TmrBlk = &(pServiceInfo->LmDmInfo.LMDMQueryTimer);
            break;
        default:
            return RFC6374_FAILURE;
    }

    if (TmrStop (RFC6374_TIMER_LIST_ID (), pR6374TmrBlk) == TMR_FAILURE)
    {
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
R6374TmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TmrId = 0;
    INT2                i2Offset = 0;

    while ((pExpiredTimers = TmrGetNextExpiredTimer (RFC6374_TIMER_LIST_ID ()))
           != NULL)
    {
        u1TmrId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        if (u1TmrId < RFC6374_MAX_TMRS)
        {
            i2Offset = gaR6374TmrDesc[u1TmrId].i2Offset;

            if (i2Offset < 0)
            {
                /* The timer function does not take any parameter */
                (*(gaR6374TmrDesc[u1TmrId].pTmrExpFunc)) (NULL);
            }
            else
            {
                /* The timer function requires a parameter */
                (*(gaR6374TmrDesc[u1TmrId].pTmrExpFunc))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrLMQueryTmrExp                                      *
*                                                                           *
* Description  : This function will be invoked in the case of LM query      *
*                timer expiry. It will performs the operations needed on the*
*                expiry of the timer                                        *
*                                                                           *
* Input        : pointer to the LM info table                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
R6374TmrLMQueryTmrExp (VOID *pArg)
{
    tR6374PktSmInfo     R6374PktSmInfo;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    RFC6374_MEMSET (&R6374PktSmInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374PktSmInfo));
    R6374PktSmInfo.pServiceConfigInfo = (tR6374ServiceConfigTableEntry *) pArg;

    if (R6374PktSmInfo.pServiceConfigInfo == NULL)
    {
        return;
    }

    pLmInfo =
        RFC6374_GET_LM_INFO_FROM_SERVICE (R6374PktSmInfo.pServiceConfigInfo);

    /* Check is Response Timeout is trying to 
     * Restart the session and mode is proactive */
    if ((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
        (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_READY) &&
        (pLmInfo->u4LmNoRespTxCount ==
         (UINT4) (R6374PktSmInfo.pServiceConfigInfo->
                  u1QueryTransmitRetryCount)))
    {
        /* Variable used to update proactive test status as restarted */
        pLmInfo->b1LmTestStatus |= RFC6374_PROACTIVE_TEST_RESTARTED;
        pLmInfo->u1PrevLmMode = pLmInfo->u1LmMode;

        R6374LmInitiator (&R6374PktSmInfo, RFC6374_LM_START_SESSION);
    }
    else
    {
        R6374LmInitiator (&R6374PktSmInfo, RFC6374_LM_INTERVAL_EXPIRY);
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : R6374TmrDMQueryTmrExp                                      *
*                                                                           *
* Description  : This function will be invoked in the case of DM query      *
*                timer expiry. It will performs the operations needed on the*
*                expiry of the timer                                        *
*                                                                           *
* Input        : pointer to the DM info table                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
R6374TmrDMQueryTmrExp (VOID *pArg)
{
    tR6374PktSmInfo     R6374PktSmInfo;
    tR6374DmInfoTableEntry *pDmInfo = NULL;

    RFC6374_MEMSET (&R6374PktSmInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374PktSmInfo));
    R6374PktSmInfo.pServiceConfigInfo = (tR6374ServiceConfigTableEntry *) pArg;

    if (R6374PktSmInfo.pServiceConfigInfo == NULL)
    {
        return;
    }

    pDmInfo =
        RFC6374_GET_DM_INFO_FROM_SERVICE (R6374PktSmInfo.pServiceConfigInfo);

    /* Check is Response Timeout is trying to
     * Restart the session and mode is proactive */
    if ((pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
        (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_READY) &&
        (pDmInfo->u4DmNoRespTxCount ==
         (UINT4) (R6374PktSmInfo.pServiceConfigInfo->
                  u1QueryTransmitRetryCount)))
    {
        /* Variable used to update proactive test status as restarted */
        pDmInfo->b1DmTestStatus |= RFC6374_PROACTIVE_TEST_RESTARTED;
        pDmInfo->u1PrevDmMode = pDmInfo->u1DmMode;

        R6374DmInitiator (&R6374PktSmInfo, RFC6374_DM_START_SESSION);
    }
    else
    {
        R6374DmInitiator (&R6374PktSmInfo, RFC6374_DM_INTERVAL_EXPIRY);
    }
    return;
}

/*****************************************************************************
 * Function     : R6374TmrLMDMQueryTmrExp                                    *
 *                                                                           *
 * Description  : This function will be invoked in the case of combined LMDM *
 *                timer expiry. It will performs the operations needed on the*
 *                expiry of the timer                                        *
 *                                                                           *
 * Input        : pointer to the Combined LMDM info table                    *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
R6374TmrLMDMQueryTmrExp (VOID *pArg)
{
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374PktSmInfo     R6374PktSmInfo;
    RFC6374_MEMSET (&R6374PktSmInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374PktSmInfo));
    R6374PktSmInfo.pServiceConfigInfo = (tR6374ServiceConfigTableEntry *) pArg;

    if (R6374PktSmInfo.pServiceConfigInfo == NULL)
    {
        return;
    }

    pLmDmInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (R6374PktSmInfo.pServiceConfigInfo);

    /* Check is Response Timeout is trying to
     * Restart the session and mode is proactive */
    if ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
        (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_READY) &&
        (pLmDmInfo->u4LmDmNoRespTxCount ==
         (UINT4) (R6374PktSmInfo.pServiceConfigInfo->
                  u1QueryTransmitRetryCount)))
    {
        /* Variable used to update proactive test status as restarted */
        pLmDmInfo->b1LmDmTestStatus |= RFC6374_PROACTIVE_TEST_RESTARTED;
        pLmDmInfo->u1PrevLmDmMode = pLmDmInfo->u1LmDmMode;

        R6374LmDmInitiator (&R6374PktSmInfo, RFC6374_LMDM_START_SESSION);
    }
    else
    {
        R6374LmDmInitiator (&R6374PktSmInfo, RFC6374_LMDM_INTERVAL_EXPIRY);
    }
    return;
}

#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  r6374tmr.c                      */
/*-----------------------------------------------------------------------*/
