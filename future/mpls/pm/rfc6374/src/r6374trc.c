 /******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374trc.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description : This file contains the functions to print the trace messages,
 *               send trap to the manager and syslog related functionalities
 *                
 * ****************************************************************************/

#ifndef __R6374TRC_C__
#define __R6374TRC_C__
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "r6374inc.h"

/****************************************************************************
*                                                                           *
* Function     : R6374TrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
R6374TrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("R6374: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : R6374TrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
R6374TrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : R6374Trc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

PUBLIC CHR1        *
R6374Trc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define RFC6374_TRC_BUF_SIZE    2000
    static CHR1         buf[RFC6374_TRC_BUF_SIZE];
    MEMSET (&ap, 0, sizeof (va_list));
    if ((u4Flags & (gR6374GlobalInfo.u4Trc)) > 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/******************************************************************************
 * Function   : R6374TrcDump 
 * Description: This function prints the given buffer in bytes
 * Input      : pu1Buf - Buffer to print
 *              i4Len  - Length of the buffer to print
 * Output     : None
 * Returns    : None 
 *****************************************************************************/
PUBLIC VOID
R6374TrcDump (UINT1 *pu1Buf, UINT4 i4Len)
{
    UINT4               u4Tmp;
    if (!pu1Buf)
        return;
    for (u4Tmp = 0; u4Tmp < i4Len; u4Tmp++)
    {
        if (u4Tmp)
        {
            if (!(u4Tmp % RFC6374_MAX_BYTES_IN_A_LINE))
            {
                printf ("\n");
            }
            else
            {
                if (!(u4Tmp % RFC6374_MAX_BYTES_IN_A_TOKEN))
                {
                    printf ("  ");
                }
                else
                {
                    if (u4Tmp)
                    {
                        printf (" ");
                    }
                }
            }
        }
        printf ("%02X", pu1Buf[u4Tmp]);
    }
    printf ("\n");
}

#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  r6374trc.c                      */
/*-----------------------------------------------------------------------*/
