/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374lminr.c,v 1.3 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the R6374 task loss measurement 
 * reply handler
 *******************************************************************/
#ifndef __R6374LMINR_C__
#define __R6374LMINR_C__
#include "r6374inc.h"
/*******************************************************************************
 * Function Name      : R6374ProcessLmr
 *
 * Description        : This is called to process the received LMR PDU
 *
 *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the
 *                      information regarding Service info, MPLS path info
 *                      and  other information related to the state machine.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374ProcessLmr (tR6374PktSmInfo * pPktSmInfo, UINT1 *pLmrpdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374RxLmrPktInfo *pLmrPktInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374TSRepresentation LossLmrRxTimeStamp;
    tR6374TSRepresentation TotalDurationTxRx;
    tR6374TSRepresentation LossLmmTxTimeStamp;
    tR6374NotifyInfo    R6374LmNotify;
    UINT4               u4RxLmrSessionId = RFC6374_INIT_VAL;
    UINT4               u4RxLmrVersion = RFC6374_INIT_VAL;
    UINT4               u4Txf = RFC6374_INIT_VAL;
    UINT4               u4Rxf = RFC6374_INIT_VAL;
    UINT4               u4Txb = RFC6374_INIT_VAL;
    UINT4               u4Rxb = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4               u4Temp = RFC6374_INIT_VAL;
    UINT4               u4SeqNo = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4SessQueryInt = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1               u1RxDflag = RFC6374_INIT_VAL;
    UINT1               u1RxControlCode = RFC6374_INIT_VAL;
    UINT1               u1TlvType = RFC6374_INIT_VAL;
    UINT1               u1TlvLength = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374ProcessLmr()\r\n");

    RFC6374_MEMSET (&LossLmrRxTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));
    RFC6374_MEMSET (&LossLmmTxTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pLmrPktInfo = &(pPktSmInfo->uPktInfo.Lmr);

    if (pLmInfo->u1LmStatus != RFC6374_TX_STATUS_NOT_READY)
    {
        i4RetVal = RFC6374_FAILURE;
        return i4RetVal;
    }
    /* Increment LMR In Count */
    RFC6374_INCR_STATS_LMR_IN (pServiceInfo);

    /* Get Current Time */
    R6374UtilGetCurrentTime (&LossLmrRxTimeStamp, pLmInfo->u1LmQTSFormat);

    /* Decrement LM Tx count when response is received for
     * Transmitted PDU */

    if (pLmInfo->u1LmType == RFC6374_LM_TYPE_TWO_WAY)
    {
        RFC6374_LM_RESET_LM_NO_RESP_TX_COUNT (pLmInfo);
    }

    /* Validate the Recived Control Code */
    /* Move the pointer of Lmr to Control Code */
    RFC6374_GET_1BYTE (u4RxLmrVersion, pLmrpdu);
    u4RxLmrVersion = u4RxLmrVersion >> 4;
    if (u4RxLmrVersion != RFC6374_PDU_VERSION)
    {
        /* Unsupported Version in LMR */
        R6374LmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        RFC6374_LM_INCR_ERR_PKT (pLmInfo);
        return RFC6374_FAILURE;
    }

    /* Get Control Code */
    RFC6374_GET_1BYTE (u1RxControlCode, pLmrpdu);
    if ((u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_VERSION) ||
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT) ||
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_CTRL_CODE) ||
        ((u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL) &&
         (pServiceInfo->u1SessIntQueryStatus !=
          CLI_RFC6374_SESS_INT_QUERY_ENABLE)))
    {
        /* Control Code from Responder */
        if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_VERSION)
        {
            /* Invalid Version Control code */
            R6374LmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT)
        {
            /* Invalid Data Format */
            R6374LmNotify.u1TrapType = RFC6374_DATA_FORMAT_INVALID_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_CTRL_CODE)
        {
            /* Invalid Control Code */
            R6374LmNotify.u1TrapType = RFC6374_UNSUP_CC_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL)
        {
            /* Unsupported Query Interval */
            /* In case Session nterval Query is disabled in Querier side, and
             * Unsupported Query Interval is got from Responder, abort the session*/
            R6374LmNotify.u1TrapType = RFC6374_UNSUPP_QUERY_INTERVAL;
        }
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        RFC6374_LM_INCR_ERR_PKT (pLmInfo);
        return RFC6374_FAILURE;
    }

    RFC6374_GET_2BYTE (u2PduLength, pLmrpdu);
    /* DFlag Validation */
    /* + Message (2Byte) */
    RFC6374_GET_1BYTE (u1RxDflag, pLmrpdu);
    u1RxDflag = u1RxDflag >> 4;
    if (u1RxDflag != (RFC6374_LM_32BIT_COUNTERS | RFC6374_LM_PKT_COUNT))
    {
        /* Unsupported Data Format */
        R6374LmNotify.u1TrapType = RFC6374_DATA_FORMAT_INVALID_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        RFC6374_LM_INCR_ERR_PKT (pLmInfo);
        return RFC6374_FAILURE;
    }

    /* Session ID Validation */
    /* + Resvered(3 byte) */
    pLmrpdu = pLmrpdu + 3;
    RFC6374_GET_4BYTE (u4RxLmrSessionId, pLmrpdu);
    if (u4RxLmrSessionId != pLmInfo->u4LmSessionId)
    {
        /* Wrong Session ID in LMR */
        R6374LmNotify.u1TrapType = RFC6374_CONN_MISMATCH_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        RFC6374_LM_INCR_ERR_PKT (pLmInfo);
        return RFC6374_FAILURE;
    }

    if (pLmInfo->u1LmQTSFormat == RFC6374_TS_FORMAT_SEQ_NUM)
    {
        /* Sequence no validation */
        RFC6374_GET_4BYTE (u4Temp, pLmrpdu);
        RFC6374_GET_4BYTE (u4SeqNo, pLmrpdu);
        if (u4SeqNo != (pLmInfo->u4LmSeqCount + RFC6374_INDEX_ONE))
        {
            RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID, "R6374ProcessLmr: "
                          " Seqno mismatch between received and sent count "
                          " Rcvd : %d sent: %d \r\n", u4SeqNo,
                          (pLmInfo->u4LmSeqCount + RFC6374_INDEX_ONE));
            return RFC6374_FAILURE;
        }
    }
    else
    {
        RFC6374_GET_4BYTE (LossLmmTxTimeStamp.u4Seconds, pLmrpdu);
        RFC6374_GET_4BYTE (LossLmmTxTimeStamp.u4NanoSeconds, pLmrpdu);

        /* For timestamp format NULL, no validation is required */
        if (pLmInfo->u1LmQTSFormat != RFC6374_TS_FORMAT_NULL)
        {
            /* Get the measurement time between two packets */
            if ((R6374UtilCalTimeDiff (&LossLmmTxTimeStamp,
                                       &LossLmrRxTimeStamp,
                                       &TotalDurationTxRx)) != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ProcessLmr: "
                             " calculation for time diff return failure\r\n");
                return RFC6374_FAILURE;
            }
        }
    }
    /* Get the Counter from LMR PDU
     * Txb,
     * Rxf,
     * Txf,
     * Rxb */
    /* Only when 64bit Counter is not Set */
    /* Counter 1 */
    RFC6374_GET_4BYTE (u4Temp, pLmrpdu);
    RFC6374_GET_4BYTE (u4Txb, pLmrpdu);
    /* Counter 2 */
    RFC6374_GET_4BYTE (u4Temp, pLmrpdu);
    RFC6374_GET_4BYTE (u4Rxf, pLmrpdu);
    /* Counter 3 */
    RFC6374_GET_4BYTE (u4Temp, pLmrpdu);
    RFC6374_GET_4BYTE (u4Txf, pLmrpdu);
    /* Counter 4 */
    RFC6374_GET_4BYTE (u4Temp, pLmrpdu);
    RFC6374_GET_4BYTE (u4Rxb, pLmrpdu);
    /* Get TLV . Check for Reception interval and adjust accordingly */
    if (((pLmInfo->u1SessQueryIntervalState == RFC6374_SQI_INIT_STATE) &&
         (u2PduLength > RFC6374_LM_PDU_MSG_LEN)) ||
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL))
    {
        RFC6374_GET_1BYTE (u1TlvType, pLmrpdu);
        RFC6374_GET_1BYTE (u1TlvLength, pLmrpdu);
        RFC6374_GET_4BYTE (u4SessQueryInt, pLmrpdu);
        /* Call function to adjust the session query interval */
        R6374UtilAdjustSessionQueryInterval (pServiceInfo, u4SessQueryInt,
                                             RFC6374_TEST_MODE_LM);
    }
    else if (pLmInfo->u1SessQueryIntervalState == RFC6374_SQI_ADJUSTED_STATE)
    {
        /* If reply without TLV, when Querier is in Adjusted state, it means that 
         * the responder has accepted the adjustment */
        pLmInfo->u1SessQueryIntervalState = RFC6374_SQI_RESET_STATE;
    }
    if (u4Rxf == RFC6374_INIT_VAL)
    {
        /* Get Rxf from BFD */
        if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMR)
        {
#ifdef RFC6374STUB_WANTED
            if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_TUNNEL)
            {
                R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
            }
            else
#endif
            {
                i4RetVal = R6374GetBfdPktCount (pServiceInfo, &u4Txf, &u4Rxf);
                if (i4RetVal == RFC6374_FAILURE)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374ProcessLmr: "
                                 "BFD Get Counter Retunred failure\r\n");
                    return RFC6374_FAILURE;
                }
            }
        }
        else if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMR)
        {
            R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx,
                                &u4PwOrFwrdTnlRx, &u4RcvTnlTx, &u4RcvTnlRx);

            if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_TUNNEL)
            {
                u4Rxf = u4RcvTnlRx;
            }
            else
            {
                u4Rxf = u4PwOrFwrdTnlRx;
            }
        }
    }

    /* Assign the Packet values */
    pLmrPktInfo->u4TxCounterf = u4Txf;    /*A_TxP */
    pLmrPktInfo->u4RxCounterf = u4Rxf;    /*A_RxP */
    pLmrPktInfo->u4TxCounterb = u4Txb;    /*B_TxP */
    pLmrPktInfo->u4RxCounterb = u4Rxb;    /*B_RxP */

    if (R6374CalcFrameLoss (pPktSmInfo, TotalDurationTxRx,
                            RFC6374_LM_TYPE_TWO_WAY, u4RxLmrSessionId,
                            pLmInfo->u4LmSeqCount) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374ProcessLmr: "
                     " calculation for frame loss return failure\r\n");
        return RFC6374_FAILURE;
    }

    /* Update previous counter */
    pLmInfo->u4PreTxCounterf = pLmrPktInfo->u4TxCounterf;    /*A_TxP */
    pLmInfo->u4PreRxCounterf = pLmrPktInfo->u4RxCounterf;    /*A_RxP */
    pLmInfo->u4PreTxCounterb = pLmrPktInfo->u4TxCounterb;    /*B_TxP */
    pLmInfo->u4PreRxCounterb = pLmrPktInfo->u4RxCounterb;    /*B_RxP */

    UNUSED_PARAM (u1TlvType);
    UNUSED_PARAM (u1TlvLength);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374ProcessLmr()\r\n");
    return i4RetVal;
}

/*****************************************************************************
 * Function           : R6374CalcLossMeas
 *
 * Description        : This Routine is used to calculate Tx&Rx/Far&Near 
 *                      at Sender loss and Rx at Recevier.
 *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the
 *                                   Service Info.
 *                      u1LossMeasurementType - One/Two Way
 *
 * Output(s)          : pLossLmBuffNode - Tx&Rx/Far&Near at Sender loss 
 *                                        and Rx at Recevier.
 *
 * Returns            : RFC6374_SUCCESS/RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374CalcLossMeas (tR6374PktSmInfo * pPktSmInfo,
                   tR6374LmBufferTableEntry * pLossLmBuffNode,
                   UINT1 u1LossMeasurementType)
{
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374RxLmrPktInfo *pLmrPktInfo = NULL;
    tR6374Rx1LmPktInfo *p1LmPktInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374CalcLossMeas\r\n");

    pLmInfo = RFC6374_GET_LM_INFO_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM (pPktSmInfo);
    if ((u1LossMeasurementType == RFC6374_LM_TYPE_ONE_WAY) ||
        (u1LossMeasurementType == RFC6374_LMDM_CAL_ONE_WAY))
    {
        p1LmPktInfo = &pPktSmInfo->uPktInfo.OneLm;

        pLossLmBuffNode->u4CalcRxLossReceiver = RFC6374_INIT_VAL;

        if (u1LossMeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            pLmInfo->u4PreTxCounterf = pLmDmInfo->u4PreCLmTxCounterf;
            pLmInfo->u4PreRxCounterb = pLmDmInfo->u4PreCLmRxCounterb;
            p1LmPktInfo = &pPktSmInfo->uPktInfo.OneLmDm.OneLmPktInfo;
        }

        RFC6374_CALCULATE_LM (p1LmPktInfo->u4TxCounterf,
                              pLmInfo->u4PreTxCounterf,
                              p1LmPktInfo->u4RxCounterf,
                              pLmInfo->u4PreRxCounterb,
                              pLossLmBuffNode->u4CalcRxLossReceiver);

        RFC6374_TRC5 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      " R6374CalcLossMeas : BFD Counters : Prev Tx : %d "
                      "Current Tx : %d Prev Rx : %d Current Rx : %d "
                      "RxLoss : %d \r\n", p1LmPktInfo->u4TxCounterf,
                      pLmInfo->u4PreTxCounterf, p1LmPktInfo->u4RxCounterf,
                      pLmInfo->u4PreRxCounterb,
                      pLossLmBuffNode->u4CalcRxLossReceiver);

        pLossLmBuffNode->u4TxTestPktsCount =
            p1LmPktInfo->u4TxCounterf - pLmInfo->u4PreTxCounterf;

    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            /* Comb LMDM Measurement */
            pLmrPktInfo = &pPktSmInfo->uPktInfo.LmrDmr.LmrPktInfo;
        }
        else
        {
            /* Loss Measurement */
            pLmrPktInfo = &pPktSmInfo->uPktInfo.Lmr;
        }
        /* Calulation of Far End and Near End */
        /* Near end loss calculation :
         * A_TxLoss[n-1,n] = (A_TxP[n] - A_TxP[n-1]) - (B_RxP[n] - B_RxP[n-1])
         */
        RFC6374_CALCULATE_LM (pLmrPktInfo->u4TxCounterf,
                              pLmInfo->u4PreTxCounterf,
                              pLmrPktInfo->u4RxCounterb,
                              pLmInfo->u4PreRxCounterb,
                              pLossLmBuffNode->u4CalcTxLossSender);

        RFC6374_TRC5 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374CalcLossMeas : Tx Loss: Current A_TxP[n]: %d "
                      "Prev A_TxP[n-1] : %d Current B_RxP[n] : %d "
                      "Prev B_RxP[n-1] : %d TxLoss : %d \r\n",
                      pLmrPktInfo->u4TxCounterf,
                      pLmInfo->u4PreTxCounterf, pLmrPktInfo->u4RxCounterb,
                      pLmInfo->u4PreRxCounterb,
                      pLossLmBuffNode->u4CalcTxLossSender);

        /* Far end loss calculation:
         * A_RxLoss[n-1,n] = (B_TxP[n] - B_TxP[n-1]) - (A_RxP[n] - A_RxP[n-1])
         */
        RFC6374_CALCULATE_LM (pLmrPktInfo->u4TxCounterb,
                              pLmInfo->u4PreTxCounterb,
                              pLmrPktInfo->u4RxCounterf,
                              pLmInfo->u4PreRxCounterf,
                              pLossLmBuffNode->u4CalcRxLossSender);

        RFC6374_TRC5 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374CalcLossMeas : Rx Loss: Current B_TxP[n] : %d "
                      "Prev B_TxP[n-1] : %d Current A_RxP[n] : %d "
                      "Prev A_RxP[n-1] : %d RxLoss : %d \r\n",
                      pLmrPktInfo->u4TxCounterb,
                      pLmInfo->u4PreTxCounterb, pLmrPktInfo->u4RxCounterf,
                      pLmInfo->u4PreRxCounterf,
                      pLossLmBuffNode->u4CalcRxLossSender);

        pLossLmBuffNode->u4TxTestPktsCount =
            pLmrPktInfo->u4TxCounterf - pLmInfo->u4PreTxCounterf;

    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374CalcLossMeas()\r\n");
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function           : R6374LmInitAddLmEntry
 *
 * Description        : This routine is used to add the entry of the transmitted
 *                      LMM frame in case of 100% frame loss and of received
 *                      LMR frame otherwise.
 *
 * Input(s)           : pServiceInfo - Pointer to the structure that stores the
 *                      information regarding Service
 *
 * Output(s)          : None
 *
 * Returns            : tR6374LmBufferTableEntry * - Pointer to the node added
 ******************************************************************************/
PUBLIC tR6374LmBufferTableEntry *
R6374LmInitAddLmEntry (tR6374ServiceConfigTableEntry * pServiceInfo,
                       UINT4 u4LmSessionId, UINT1 u1MeasurementType)
{
    tR6374LmBufferTableEntry *pLossBuffNode = NULL;
    tR6374LmBufferTableEntry *pFirstLossBuffEntry = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374LmBufferTableEntry TempLossBuffEntry;
    UINT4               u4SeqCount = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitAddLmEntry()\r\n");

    RFC6374_MEMSET (&TempLossBuffEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /* Increase Seq Count */
    if (u1MeasurementType == RFC6374_LM_TYPE_ONE_WAY)
    {
        RFC6374_LM_INCR_RX_SEQ_COUNT (pLmInfo);
    }
    else if (u1MeasurementType == RFC6374_LM_TYPE_TWO_WAY)
    {
        RFC6374_LM_INCR_SEQ_COUNT (pLmInfo);
    }
    else if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
    {
        RFC6374_LMDM_INCR_RX_SEQ_COUNT (pLmDmInfo);
    }

    /* Allocate a memory block for frame loss buffer node */
    pLossBuffNode = (tR6374LmBufferTableEntry *) MemAllocMemBlk
        (RFC6374_LMBUFFERTABLE_POOLID);
    if (pLossBuffNode == NULL)
    {
        /* Memory allocation failed, so now check if the memory pool is
         * exhausted */
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitAddLmEntry: "
                     "failure allocating memory for frame loss buffer node\r\n");
        if (RFC6374_GET_FREE_MEM_UNITS (RFC6374_LMBUFFERTABLE_POOLID)
            != RFC6374_INIT_VAL)
        {
            /* memory failure failure has occured even when we have free
             * memory in the pool */
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitAddLmEntry: "
                         "Loss memory pool corruption\r\n");
            RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        }
        return NULL;
    }

    /* Memset the Newly added Buffer */
    RFC6374_MEMSET (pLossBuffNode, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    /* Config Params */
    pLossBuffNode->u4ContextId = pServiceInfo->u4ContextId;
    RFC6374_MEMCPY (pLossBuffNode->au1ServiceName, pServiceInfo->au1ServiceName,
                    RFC6374_STRLEN (pServiceInfo->au1ServiceName));

    if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_PW)
    {
        /* PW ID (VcId) */
        pLossBuffNode->u4ChannelId1 =
            pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId;
        /* PW Peer IP Address */
        pLossBuffNode->u4ChannelIpAddr1 =
            pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr;
    }
    else
    {
        /* LSP Forwards ID */
        pLossBuffNode->u4ChannelId1 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4FrwdTunnelId;
        /* LSP Reverse ID */
        pLossBuffNode->u4ChannelId2 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4RevTunnelId;
        /* LSP Source Address */
        pLossBuffNode->u4ChannelIpAddr1 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;
        /* LSP Destination Address */
        pLossBuffNode->u4ChannelIpAddr2 =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
    }

    /* Session Params */
    pLossBuffNode->u4SessionId = u4LmSessionId;

    if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
    {
        pLossBuffNode->u4SeqCount = pLmDmInfo->u4RxLmDmSeqCount;
        pLossBuffNode->u4BufSeqId = pLmDmInfo->u4RxLmDmSeqCount;
        u4SeqCount = pLmDmInfo->u4RxLmResetSeqCount;
    }
    else if (u1MeasurementType == RFC6374_LM_TYPE_ONE_WAY)
    {
        pLossBuffNode->u4SeqCount = pLmInfo->u4RxLmSeqCount;
        pLossBuffNode->u4BufSeqId = pLmInfo->u4RxLmSeqCount;
        u4SeqCount = pLmInfo->u4RxLmResetSeqCount;
    }
    else
    {
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            pLossBuffNode->u4SeqCount = pLmDmInfo->u4LmDmSeqCount;
            pLossBuffNode->u4BufSeqId = pLmDmInfo->u4LmDmSeqCount;
            u4SeqCount = pLmDmInfo->u4LmResetSeqCount;
        }
        else
        {
            pLossBuffNode->u4SeqCount = pLmInfo->u4LmSeqCount;
            pLossBuffNode->u4BufSeqId = pLmInfo->u4LmSeqCount;
            u4SeqCount = pLmInfo->u4LmResetSeqCount;
        }
    }

    RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374LmInitAddLmEntry: Service name : %s Sess Id : %d "
                  "Seq Count : %d Rolling Buffer Reset Seq Count : %d \r\n",
                  pServiceInfo->au1ServiceName, u4LmSessionId,
                  pLossBuffNode->u4SeqCount, u4SeqCount);

    /* R6374_RB_BUFFER_MAX_COUNT to 5 */
    if (pLossBuffNode->u4SeqCount <= RFC6374_RB_BUFFER_MAX_COUNT)
    {
        /* Add the node into the table */
        if (RBTreeAdd (RFC6374_LMBUFFER_TABLE, pLossBuffNode) !=
            RFC6374_RB_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374LmInitAddLmEntry: "
                         "failure adding node into the frame loss table \r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_LMBUFFERTABLE_POOLID,
                                    (UINT1 *) pLossBuffNode);
            return NULL;
        }

        /* Increase Buffer Add Count in RB Tree */
        RFC6374_LM_INCR_BUFFER_ADD_COUNT (pLmInfo);

        if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            RFC6374_LMDM_RESET_ONEWAY_LM_ROSEQ_COUNT (pLmDmInfo);
        }
        else if (u1MeasurementType == RFC6374_LM_TYPE_ONE_WAY)
        {
            RFC6374_LM_RESET_RX_ROSEQ_COUNT (pLmInfo);
        }
    }
    else
    {
        /* As Count Has reached 5 per session entry
         * Now remove the 1st Node from Buffer
         * Particular Service Name */
        /* Current Session ID */
        TempLossBuffEntry.u4SessionId = u4LmSessionId;
        /* Default Context ID */
        TempLossBuffEntry.u4ContextId = pServiceInfo->u4ContextId;
        /* As we have to remove first node
         * So get 1st entry always, 
         * after Buffer add count is 5 */
        if (u4SeqCount == RFC6374_RB_BUFFER_MAX_COUNT)
        {
            /* If count is reached 5 reset to 0 */
            u4SeqCount = RFC6374_RESET;
        }
        TempLossBuffEntry.u4SeqCount = ++u4SeqCount;
        /* Service Name on which Session is Running */
        RFC6374_STRCPY (TempLossBuffEntry.au1ServiceName,
                        pServiceInfo->au1ServiceName);

        pFirstLossBuffEntry = RBTreeGet (RFC6374_LMBUFFER_TABLE,
                                         &TempLossBuffEntry);
        if (pFirstLossBuffEntry == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitAddLmEntry: "
                         " Node for Seq Count Not found for particular service"
                         ", session ID\r\n");
            RFC6374_FREE_MEM_BLOCK (RFC6374_LMBUFFERTABLE_POOLID,
                                    (UINT1 *) pLossBuffNode);
            return NULL;
        }

        /* Changes Seq Count of next Buffer node to removed seq Count */
        pLossBuffNode->u4SeqCount = u4SeqCount;

        if (u1MeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            pLmDmInfo->u4RxLmResetSeqCount = u4SeqCount;
        }
        else if (u1MeasurementType == RFC6374_LM_TYPE_ONE_WAY)
        {
            pLmInfo->u4RxLmResetSeqCount = u4SeqCount;
        }
        else
        {
            if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
            {
                pLmDmInfo->u4LmResetSeqCount = u4SeqCount;
            }
            else
            {
                pLmInfo->u4LmResetSeqCount = u4SeqCount;
            }
        }
        /* Copy the RB Node */
        RFC6374_MEMCPY (&pLossBuffNode->LmBufferNode,
                        &pFirstLossBuffEntry->LmBufferNode,
                        sizeof (tR6374RBNodeEmbd));

        RFC6374_MEMSET (pFirstLossBuffEntry, RFC6374_INIT_VAL,
                        sizeof (tR6374LmBufferTableEntry));

        RFC6374_MEMCPY (pFirstLossBuffEntry, pLossBuffNode,
                        sizeof (tR6374LmBufferTableEntry));
        RFC6374_FREE_MEM_BLOCK (RFC6374_LMBUFFERTABLE_POOLID,
                                (UINT1 *) pLossBuffNode);
        return pFirstLossBuffEntry;
    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmInitAddLmEntry()\r\n");
    return pLossBuffNode;
}

/*****************************************************************************
 * Function           : R6374LmAddStats
 *
 * Description        : This Routine is used to Add Statics to Loss Measurement
 *                      stats table.
 *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the
 *                                   Service Info.
 *                      u1LossMeasurementType - One/Two Way
 *                      pLossLmBuffNode - Tx&Rx/Far&Near at Sender loss
 *                                        and Rx at Recevier.
 *
 * Output(s)          : None
 *
 * Returns            : RFC6374_SUCCESS/RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374LmAddStats (tR6374PktSmInfo * pPktSmInfo,
                 tR6374LmBufferTableEntry * pLossLmBuffNode,
                 UINT1 u1LossMeasurementType)
{
    tR6374LmStatsTableEntry *pLmStatsNodeInfo = NULL;
    tR6374LmStatsTableEntry *pLmStatsNodeTableEntry = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374RxLmrPktInfo *pLmLmrInfo = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmAddStats()\r\n");

    pLmInfo = RFC6374_GET_LM_INFO_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);

    /* Check if Node is already present for Service Name, Current Session ID */
    pLmStatsNodeInfo = R6374UtilGetLmStatsTableInfo (pServiceInfo,
                                                     pLossLmBuffNode->
                                                     u4SessionId);
    if (pLmStatsNodeInfo == NULL)
    {
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                      RFC6374_DEFAULT_CONTEXT_ID, " R6374LmAddStats: Getting "
                      "stats failed, node might be present for session Id : "
                      "%d Allocate memory to add new entry \r\n",
                      pLossLmBuffNode->u4SessionId);

        /* Add a new Stats Node to table */
        /* Allocate Memory to New Node */
        pLmStatsNodeTableEntry = (tR6374LmStatsTableEntry *)
            MemAllocMemBlk (RFC6374_LMSTATSTABLE_POOLID);
        if (pLmStatsNodeTableEntry == NULL)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID, "R6374LmAddStats: "
                          "Allocating Memory to stats table failed for"
                          "session Id :  %d \r\n",
                          pLossLmBuffNode->u4SessionId);
            RFC6374_INCR_MEMORY_FAILURE_COUNT ();
            return RFC6374_FAILURE;
        }

        RFC6374_MEMSET (pLmStatsNodeTableEntry, RFC6374_INIT_VAL,
                        sizeof (tR6374LmStatsTableEntry));

        /* Add It Into RB Tree Table */
        pLmStatsNodeTableEntry->u4ContextId = pServiceInfo->u4ContextId;
        pLmStatsNodeTableEntry->u4SessionId = pLossLmBuffNode->u4SessionId;

        /* Every new LM/DM/CombLMDM Session initiated would be the latest Session ID */
        pServiceInfo->u4LastLmSessId = pLmStatsNodeTableEntry->u4SessionId;

        /* For Every Combined LMDM Session update its Latest Session ID */
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            pServiceInfo->u4LastLmDmSessId =
                pLmStatsNodeTableEntry->u4SessionId;
        }

        RFC6374_MEMCPY (pLmStatsNodeTableEntry->au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        STRLEN (pServiceInfo->au1ServiceName));

        /* Start Time */
        pLmStatsNodeTableEntry->RxMeasurementStartTime =
            R6374UtilGetTimeFromEpoch ();

        /* Traffic Class */
        pLmStatsNodeTableEntry->u1TrafficClass = pServiceInfo->u1TrafficClass;

        if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_PW)
        {
            /* PW ID (VcId) */
            pLmStatsNodeTableEntry->u4ChannelId1 =
                pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId;
            /* PW Peer IP Address */
            pLmStatsNodeTableEntry->u4ChannelIpAddr1 =
                pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr;
        }
        else
        {
            /* LSP Forwards ID */
            pLmStatsNodeTableEntry->u4ChannelId1 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4FrwdTunnelId;
            /* LSP Reverse ID */
            pLmStatsNodeTableEntry->u4ChannelId2 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4RevTunnelId;
            /* LSP Source Address */
            pLmStatsNodeTableEntry->u4ChannelIpAddr1 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;
            /* LSP Destination Address */
            pLmStatsNodeTableEntry->u4ChannelIpAddr2 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
        }
        /* Just Assign the values as it is first node */
        if ((u1LossMeasurementType == RFC6374_LM_TYPE_ONE_WAY) ||
            (u1LossMeasurementType == RFC6374_LMDM_CAL_ONE_WAY))
        {
            /* Loss Info */
            pLmStatsNodeTableEntry->u1LossType = RFC6374_LM_TYPE_ONE_WAY;

            if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMM)
            {
                pLmStatsNodeTableEntry->u1LossMethod =
                    RFC6374_LM_METHOD_INFERED;
            }
            if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMM)
            {
                pLmStatsNodeTableEntry->u1LossMethod = RFC6374_LM_METHOD_DIRECT;
            }
            if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM)
            {
                pLmStatsNodeTableEntry->u1LossMethod =
                    RFC6374_LM_METHOD_INFERED;
            }
            if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM)
            {
                pLmStatsNodeTableEntry->u1LossMethod = RFC6374_LM_METHOD_DIRECT;
            }
            if (u1LossMeasurementType == RFC6374_LM_TYPE_ONE_WAY)
            {
                /* Number of LMM Recevied */
                pLmStatsNodeTableEntry->u41LMRcvd = pLmInfo->u4RxLmSeqCount;
                /* Loss session Type - Independent DM or Combined Lm-Dm */
                pLmStatsNodeTableEntry->u1SessType =
                    RFC6374_SESS_TYPE_INDEPENDENT;
                /* Updating the TSformat used for the calculation */
                pLmStatsNodeTableEntry->u1LossTSFormat = pLmInfo->u1LmQTSFormat;
            }
            else if (u1LossMeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
            {
                /* Number of LMM Recevied */
                pLmStatsNodeTableEntry->u41LMRcvd = pLmDmInfo->u4RxLmDmSeqCount;
                /* Loss session Type - Independent DM or Combined Lm-Dm */
                pLmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_COMBINED;
                /* Updating the TSformat used for the calculation */
                pLmStatsNodeTableEntry->u1LossTSFormat =
                    pLmDmInfo->u1LmDmQTSFormat;
            }
        }
        else
        {
            /* Set Measuerment Status As On-going */
            pLmStatsNodeTableEntry->u1MeasurementOngoing =
                RFC6374_MEASUREMENT_ONGOING;

            /* Loss Info */
            pLmStatsNodeTableEntry->u1LossType = RFC6374_LM_TYPE_TWO_WAY;

            if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
            {
                /* Set Dyadic Enable in Stats */
                pLmStatsNodeTableEntry->u1DyadicMeasurement =
                    RFC6374_DYADIC_MEAS_ENABLE;

                /* Set is role was active/passive */
                pLmStatsNodeTableEntry->u1DyadicProactiveRole =
                    pServiceInfo->u1DyadicProactiveRole;

                /* Set the Remote Session ID */
                if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
                {
                    /* Comb LMDM Remote Session ID */
                    pLmStatsNodeTableEntry->u4RxSessionId =
                        pLmDmInfo->u4RxLmDmSessionId;
                }
                else
                {
                    /* Independent LM Remote Session ID */
                    pLmStatsNodeTableEntry->u4RxSessionId =
                        pLmInfo->u4RxLmSessionId;
                }

                if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMM)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_INFERED;
                }
                else if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMM)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_DIRECT;
                }
                else if (pPktSmInfo->u1PduType ==
                         RFC6374_PDU_TYPE_INFERED_LMMDMM)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_INFERED;
                }
                else if (pPktSmInfo->u1PduType ==
                         RFC6374_PDU_TYPE_DIRECT_LMMDMM)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_DIRECT;
                }
            }
            else
            {
                if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMR)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_INFERED;
                }
                if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMR)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_DIRECT;
                }
                if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMRDMR)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_INFERED;
                }
                if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMRDMR)
                {
                    pLmStatsNodeTableEntry->u1LossMethod =
                        RFC6374_LM_METHOD_DIRECT;
                }
                /* Update if LM buffer type is SQI enabled */
                if (pServiceInfo->u1SessIntQueryStatus ==
                    RFC6374_SESS_INT_QUERY_ENABLE)
                {
                    pLmStatsNodeTableEntry->b1IsLMBufferSQIEnabled = OSIX_TRUE;
                }
                else
                {
                    pLmStatsNodeTableEntry->b1IsLMBufferSQIEnabled = OSIX_FALSE;
                }
            }

            /* Update Loss mode as ondemand when Loss measurement is initiated.
             * It can be either in Independent mode or Combined 
             * LmDm mode */
            if (((pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND) &&
                 (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)) ||
                ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND) &&
                 (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)))
            {
                pLmStatsNodeTableEntry->u1LossMode = RFC6374_LM_MODE_ONDEMAND;
            }
            else
            {
                /* Update Loss mode as proactive when Loss measurement is initiated.
                 * It can be either in Independent mode or Combined 
                 * LmDm mode */
                if (((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
                     (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)) ||
                    ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
                     (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)))
                {
                    pLmStatsNodeTableEntry->u1LossMode =
                        RFC6374_LM_MODE_PROACTIVE;
                }
            }
        }

        pLmStatsNodeTableEntry->u1MplsPathType =
            pServiceInfo->R6374MplsParams.u1MplsPathType;

        /* Add Node */
        if (RBTreeAdd (RFC6374_LMSTATS_TABLE, pLmStatsNodeTableEntry) !=
            RFC6374_SUCCESS)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID, " R6374LmAddStats: Add "
                          "Stats to RB Tree Failed for session Id : %d \r\n",
                          pLossLmBuffNode->u4SessionId);
            return RFC6374_FAILURE;
        }
    }
    else
    {
        pLmStatsNodeTableEntry = pLmStatsNodeInfo;

        /* Adding Measurement With Previous value */
        if ((u1LossMeasurementType == RFC6374_LM_TYPE_ONE_WAY) ||
            (u1LossMeasurementType == RFC6374_LMDM_CAL_ONE_WAY))
        {
            /* First Calculation is done on 2nd packet recevied */
            if ((pLmInfo->u4RxLmSeqCount == RFC6374_VAL_2) ||
                (pLmDmInfo->u4RxLmDmSeqCount == RFC6374_VAL_2))
            {
                /* Min */
                pLmStatsNodeTableEntry->u4MinCalcRxLossReceiver =
                    pLossLmBuffNode->u4CalcRxLossReceiver;
                /* Avg */
                pLmStatsNodeTableEntry->u4AvgCalcRxLossReceiver =
                    pLossLmBuffNode->u4CalcRxLossReceiver;
                /* Max */
                pLmStatsNodeTableEntry->u4MaxCalcRxLossReceiver =
                    pLossLmBuffNode->u4CalcRxLossReceiver;
            }
            else
            {
                /* Min VAL */
                RFC6374_CALCULATE_MIN_LM (pLmStatsNodeInfo->
                                          u4MinCalcRxLossReceiver,
                                          pLossLmBuffNode->
                                          u4CalcRxLossReceiver);

                /* Avg VAL */
                pLmStatsNodeInfo->u4AvgCalcRxLossReceiver +=
                    pLossLmBuffNode->u4CalcRxLossReceiver;

                /* Max VAL */
                RFC6374_CALCULATE_MAX_LM (pLmStatsNodeInfo->
                                          u4MaxCalcRxLossReceiver,
                                          pLossLmBuffNode->
                                          u4CalcRxLossReceiver);
            }

            if (u1LossMeasurementType == RFC6374_LM_TYPE_ONE_WAY)
            {
                /* Number of LMM Recevied */
                pLmStatsNodeTableEntry->u41LMRcvd = pLmInfo->u4RxLmSeqCount;
            }
            else if (u1LossMeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
            {
                /* Number of Combined LmDm Recevied */
                pLmStatsNodeTableEntry->u41LMRcvd = pLmDmInfo->u4RxLmDmSeqCount;
            }
        }
        else
        {
            if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
            {
                /* Comb LMDM Measurement */
                pLmLmrInfo = &(pPktSmInfo->uPktInfo.LmrDmr.LmrPktInfo);
            }
            else
            {
                /* Loss Measurement */
                pLmLmrInfo = &(pPktSmInfo->uPktInfo.Lmr);
            }

            /* First Calculation is done on 2nd packet recevied */
            if ((pLmInfo->u4LmSeqCount == RFC6374_VAL_2) ||
                (pLmDmInfo->u4LmDmSeqCount == RFC6374_VAL_2))
            {
                /* Far end/Tx Loss Sender */
                /* Min */
                pLmStatsNodeInfo->u4MinCalcTxLossSender =
                    pLossLmBuffNode->u4CalcTxLossSender;
                /* Avg */
                pLmStatsNodeInfo->u4AvgCalcTxLossSender =
                    pLossLmBuffNode->u4CalcTxLossSender;
                /* Max */
                pLmStatsNodeInfo->u4MaxCalcTxLossSender =
                    pLossLmBuffNode->u4CalcTxLossSender;

                /* Near End/Rx Loss Sender  */
                /* Min */
                pLmStatsNodeInfo->u4MinCalcRxLossSender =
                    pLossLmBuffNode->u4CalcRxLossSender;
                /* Avg */
                pLmStatsNodeInfo->u4AvgCalcRxLossSender =
                    pLossLmBuffNode->u4CalcRxLossSender;
                /* Max */
                pLmStatsNodeInfo->u4MaxCalcRxLossSender =
                    pLossLmBuffNode->u4CalcRxLossSender;
            }
            else
            {
                /* Far End/Tx Loss Sender */
                /* Min VAL */
                RFC6374_CALCULATE_MIN_LM (pLmStatsNodeInfo->
                                          u4MinCalcTxLossSender,
                                          pLossLmBuffNode->u4CalcTxLossSender);

                /* Avg VAL */
                pLmStatsNodeInfo->u4AvgCalcTxLossSender +=
                    pLossLmBuffNode->u4CalcTxLossSender;

                /* Max VAL */
                RFC6374_CALCULATE_MAX_LM (pLmStatsNodeInfo->
                                          u4MaxCalcTxLossSender,
                                          pLossLmBuffNode->u4CalcTxLossSender);

                /* Near End/Rx Loss Sender */
                /* Min VAL */
                RFC6374_CALCULATE_MIN_LM (pLmStatsNodeInfo->
                                          u4MinCalcRxLossSender,
                                          pLossLmBuffNode->u4CalcRxLossSender);

                /* Avg VAL */
                pLmStatsNodeInfo->u4AvgCalcRxLossSender +=
                    pLossLmBuffNode->u4CalcRxLossSender;

                /* Max VAL */
                RFC6374_CALCULATE_MAX_LM (pLmStatsNodeInfo->
                                          u4MaxCalcRxLossSender,
                                          pLossLmBuffNode->u4CalcRxLossSender);

                /* Through Put */
                RFC6374_CALCULATE_THROUGH_PUT_LM (pLmStatsNodeTableEntry->
                                                  f4ThroughPut,
                                                  pLmLmrInfo->u4TxCounterf,
                                                  pLmInfo->u4PreTxCounterf,
                                                  pLmLmrInfo->u4RxCounterb,
                                                  pLmInfo->u4PreRxCounterb,
                                                  pLmLmrInfo->u4TxCounterb,
                                                  pLmInfo->u4PreTxCounterb,
                                                  pLmLmrInfo->u4RxCounterf,
                                                  pLmInfo->u4PreRxCounterf);
            }
        }
    }

    if (u1LossMeasurementType == RFC6374_LM_TYPE_TWO_WAY)
    {
        /* For Non-Dyadic Comb LMDM Run */
        if ((pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMRDMR) ||
            (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMRDMR) ||
            /* For Dyadic Comb LMDM Run */
            (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM) ||
            (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM))
        {
            if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
            {
                pLmStatsNodeTableEntry->u4LMMRcvd = pLmDmInfo->u4LmDmSeqCount;
            }
            else
            {
                pLmStatsNodeTableEntry->u4LMRRcvd = pLmDmInfo->u4LmDmSeqCount;
            }
            /* Loss session Type - Independent DM or Combined Lm-Dm */
            pLmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_COMBINED;

            /* Updating the TSformat used for the calculation */
            pLmStatsNodeTableEntry->u1LossTSFormat = pLmDmInfo->u1LmDmQTSFormat;

            /* Number of LMM Sent and LMR Recevied */
            if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
            {
                pLmStatsNodeTableEntry->u4LMMSent = (UINT4)
                    ((pLmDmInfo->u2LmDmNoOfMessages + RFC6374_INDEX_ONE)
                     - pLmDmInfo->u2LmDmRunningCount);
            }
            /* Number of Proactive LMM Sent updated for CombinedLMDM */
            else if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
            {
                pLmStatsNodeTableEntry->u4LMMSent =
                    pLmDmInfo->u4LmDmNoOfProTxCount;
            }
        }
        else
        {
            if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
            {
                pLmStatsNodeTableEntry->u4LMMRcvd = pLmInfo->u4LmSeqCount;
            }
            else
            {
                pLmStatsNodeTableEntry->u4LMRRcvd = pLmInfo->u4LmSeqCount;
            }
            /* Loss session Type - Independent DM or Combined Lm-Dm */
            pLmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_INDEPENDENT;

            /* Updating the TSformat used for the calculation */
            pLmStatsNodeTableEntry->u1LossTSFormat = pLmInfo->u1LmQTSFormat;

            /* NUmber of LMM Sent and LMR Recevied */
            if (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND)
            {
                pLmStatsNodeTableEntry->u4LMMSent = (UINT4)
                    ((pLmInfo->u2LmNoOfMessages + RFC6374_INDEX_ONE)
                     - pLmInfo->u2LmRunningCount);
            }
            /* Number of LMM Sent */
            else if (pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE)
            {
                pLmStatsNodeTableEntry->u4LMMSent = pLmInfo->u4LmNoOfProTxCount;
            }
        }
    }
    /* Store the adjusted query interval for this session */
    pLmStatsNodeTableEntry->u2LmAdjustedInterval =
        pServiceInfo->LmInfo.u2LmTimeInterval;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmAddStats()\r\n");
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374CalcFrameLoss                                     *
 *                                                                             *
 * Description        : This routine is called to calculate frame loss         *
 *                and store the result in Frame Loss Buffer.             *
 *                                                                             *
 * Input(s)           : pPktInfo - Pointer to the structure that stores the    *
 *                      information regarding Service info, MPLS path info     *
 *                      and  other information related to the state machine.   *
 *                      TotalDurationTxRx - Time caluculated between two       *
 *                      packets                                                *
 *                      u1MeasurementType - One way or two way                 *
 *                      u4SessionId - Session Identifier                       *
 *                      u4SeqCount  - Sequence Count                           *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374M_FAILURE               *
 *******************************************************************************/
PUBLIC UINT4
R6374CalcFrameLoss (tR6374PktSmInfo * pPktSmInfo,
                    tR6374TSRepresentation TotalDurationTxRx,
                    UINT1 u1MeasurementType, UINT4 u4SessionId,
                    UINT4 u4SeqCount)
{

    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmBufferTableEntry *pLossLmrBuffNode = NULL;

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);

    RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374CalcFrameLoss : Adding entry in LM buffer "
                  "for Service : %s SessId: %d \r\n",
                  pServiceInfo->au1ServiceName, u4SessionId);

    if (u4SeqCount == RFC6374_INIT_VAL)
    {
        /* This is the first Lmr/Combined LmrDmr received for this 
         * transaction.
         * Save the information in the Frame Loss Buffer
         */

        /*Add Entry in the LM Buffer */
        pLossLmrBuffNode = R6374LmInitAddLmEntry (pServiceInfo, u4SessionId,
                                                  u1MeasurementType);
        if (pLossLmrBuffNode == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         " R6374CalcFrameLoss: "
                         "Failure adding node in the Loss buffer\r\n");
            return RFC6374_FAILURE;
        }

        if ((TotalDurationTxRx.u4Seconds != RFC6374_INIT_VAL) &&
            (TotalDurationTxRx.u4NanoSeconds != RFC6374_INIT_VAL))
        {
            /* Copy Duration in to buffer */
            RFC6374_MEMCPY (&(pLossLmrBuffNode->MeasurementTimeTaken),
                            &TotalDurationTxRx,
                            sizeof (tR6374TSRepresentation));
        }

        pLossLmrBuffNode->u4CalcTxLossSender = RFC6374_INIT_VAL;
        pLossLmrBuffNode->u4CalcRxLossSender = RFC6374_INIT_VAL;

    }
    else
    {
        /*Add Entry in the LM Buffer */
        pLossLmrBuffNode = R6374LmInitAddLmEntry (pServiceInfo, u4SessionId,
                                                  u1MeasurementType);
        if (pLossLmrBuffNode == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         " R6374CalcFrameLoss: "
                         "Failure adding node in the Loss buffer\r\n");
            return RFC6374_FAILURE;
        }

        /* Copy Duration in to buffer */
        RFC6374_MEMCPY (&(pLossLmrBuffNode->MeasurementTimeTaken),
                        &TotalDurationTxRx, sizeof (tR6374TSRepresentation));

        if ((R6374CalcLossMeas (pPktSmInfo, pLossLmrBuffNode,
                                u1MeasurementType) != RFC6374_SUCCESS))
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, " R6374CalcFrameLoss: "
                         " Loss calculation failed\r\n");
            return RFC6374_FAILURE;
        }
    }
    /* Add Stats for LM Stats Table */
    if ((R6374LmAddStats (pPktSmInfo, pLossLmrBuffNode,
                          u1MeasurementType) != RFC6374_SUCCESS))
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     " R6374CalcFrameLoss: " "Adding Stats Failed\r\n");
        return RFC6374_FAILURE;
    }

    return RFC6374_SUCCESS;
}
#endif
