/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374lmres.c,v 1.4 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the R6374 task loss measurement 
 * responder
 *******************************************************************/
#ifndef __R6374LMRES_C__
#define __R6374LMRES_C__
#include "r6374inc.h"

/*******************************************************************************
 * Function Name      : R6374ProcessLmm
 *
 * Description        : This is called to parse the received LMM PDU and fill
 *                      required data structures
 *
 * Input(s)           : pPktSmInfo - Pointer to the Service Related Config
 *                      pu1Pdu  - Pointer to the recevied PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 *******************************************************************************/
PUBLIC INT4
R6374ProcessLmm (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374RxLmmPktInfo *pLmmPduInfo = NULL;
    tR6374Rx1LmPktInfo *p1LmPduInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374LmBufferTableEntry *pLossLmmBufferNode = NULL;
    tR6374LmBufferTableEntry LossBufferEntryInfo;
    tR6374TSRepresentation LossLmRxTimeStamp;
    tR6374TSRepresentation TotalDurationTxRx;
    UINT4               u4RxLmmVersion = RFC6374_INIT_VAL;
    UINT4               u4Txf = RFC6374_INIT_VAL;
    UINT4               u4Rxf = RFC6374_INIT_VAL;
    UINT4               u4TxTemp = RFC6374_INIT_VAL;
    UINT4               u4RxLmSessionId = RFC6374_INIT_VAL;
    UINT4               u4SessQueryInterval = RFC6374_INIT_VAL;
    UINT4               u4QueryOriginSeconds = RFC6374_INIT_VAL;
    UINT4               u4QueryOriginNanoSeconds = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1               u1TlvType = RFC6374_INIT_VAL;
    UINT1               u1TlvLength = RFC6374_INIT_VAL;
    UINT1              *pu1RevPdu = NULL;
    UINT1               u1RxLmCtrlCode = RFC6374_INIT_VAL;
    UINT1               u1RxDFlag = RFC6374_INIT_VAL;
    UINT1               u1RxLmmDFlagOTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374ProcessLmm()\r\n");

    RFC6374_MEMSET (&LossLmRxTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&LossBufferEntryInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    if (pPktSmInfo->pServiceConfigInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374ProcessLmm: " "Recevied Service Info is NULL\r\n");
        i4RetVal = RFC6374_FAILURE;
        return i4RetVal;
    }

    /* Keep the Received PDU Pointer */
    pu1RevPdu = pu1Pdu;

    pLmInfo = RFC6374_GET_LM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);

    /* Get Version Field from PDU */
    RFC6374_GET_1BYTE (u4RxLmmVersion, pu1Pdu);

    /* Get Control Code from PDU */
    RFC6374_GET_1BYTE (u1RxLmCtrlCode, pu1Pdu);

    /*Check if Response is requested */
    if (u1RxLmCtrlCode == RFC6374_CTRL_CODE_RESP_NOT_REQ)
    {
        if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
        {
            /* For Dyadic Measurement TimeStamp Negotiation is not applicable */
            pLmInfo->u1LmQTSFormat = pLmInfo->u1LmTSFormat;
            /* For Dyadic LMM Packet is sent with control code 0x2 */
            /*Increment LMM In */
            RFC6374_INCR_STATS_LMM_IN (pServiceInfo);

            if (R6374LmDyadicMeasurement (pPktSmInfo, pu1RevPdu) !=
                RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ProcessLmm: "
                             "Dyadic Measurement Failed\r\n");
                i4RetVal = RFC6374_FAILURE;
                return i4RetVal;
            }
        }
        else
        {
            /* Increment 1LM IN */
            RFC6374_INCR_STATS_1LM_IN (pServiceInfo);

            p1LmPduInfo = &(pPktSmInfo->uPktInfo.OneLm);

            /* Get DFALG + QTF format */
            pu1Pdu = pu1Pdu + RFC6374_VAL_2;

            RFC6374_GET_1BYTE (u1RxLmmDFlagOTF, pu1Pdu);

            /* Get the packet Recived time */
            R6374UtilGetCurrentTime (&LossLmRxTimeStamp, u1RxLmmDFlagOTF);

            /* Move to Session Identifier Field 
             * Reserved */
            pu1Pdu = pu1Pdu + RFC6374_VAL_3;

            /* Get Session Identifier */
            RFC6374_GET_4BYTE (u4RxLmSessionId, pu1Pdu);

            /* Check If the Buffer is Initial node not Persent 
             * to Reset the Rx Seq Count, Prev Counters */
            LossBufferEntryInfo.u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;
            LossBufferEntryInfo.u4SessionId = u4RxLmSessionId;
            LossBufferEntryInfo.u4SeqCount = RFC6374_VAL_1;
            RFC6374_STRCPY (LossBufferEntryInfo.au1ServiceName,
                            pServiceInfo->au1ServiceName);

            pLossLmmBufferNode = (tR6374LmBufferTableEntry *) RBTreeGet
                (RFC6374_LMBUFFER_TABLE, (tRBElem *) & LossBufferEntryInfo);
            if (pLossLmmBufferNode == NULL)
            {
                pLmInfo->u4RxLmSeqCount = RFC6374_RESET;
                pLmInfo->u4PreTxCounterf = RFC6374_RESET;
                pLmInfo->u4PreRxCounterb = RFC6374_RESET;
            }
            else
            {
                pLossLmmBufferNode = NULL;
                UNUSED_PARAM (pLossLmmBufferNode);
            }
            /* Get TIme of start */
            RFC6374_GET_4BYTE (pLmInfo->LmOriginTimeStamp.u4Seconds, pu1Pdu);
            RFC6374_GET_4BYTE (pLmInfo->LmOriginTimeStamp.u4NanoSeconds,
                               pu1Pdu);

            /* Get Counters As this is One way get */
            /* Only when 64bit Counter is not Set */
            /* Counter 1 */
            RFC6374_GET_4BYTE (u4TxTemp, pu1Pdu);
            RFC6374_GET_4BYTE (u4Txf, pu1Pdu);
            /* Counter 2 */
            RFC6374_GET_4BYTE (u4TxTemp, pu1Pdu);
            RFC6374_GET_4BYTE (u4Rxf, pu1Pdu);

            if (u4Rxf == RFC6374_INIT_VAL)
            {
                if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMM)
                {
#ifdef RFC6374STUB_WANTED
                    if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                        MPLS_PATH_TYPE_TUNNEL)
                    {
                        R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
                    }
                    else
#endif
                    {
                        i4RetVal = R6374GetBfdPktCount (pServiceInfo,
                                                        &u4Txf, &u4Rxf);
                        if (i4RetVal == RFC6374_FAILURE)
                        {
                            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                         RFC6374_ALL_FAILURE_TRC,
                                         RFC6374_DEFAULT_CONTEXT_ID,
                                         "R6374ProcessLmm: "
                                         "Get BFD Packet Counter failure\r\n");
                            return RFC6374_FAILURE;
                        }
                    }
                }
                else if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMM)
                {
                    R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx,
                                        &u4PwOrFwrdTnlRx, &u4RcvTnlTx,
                                        &u4RcvTnlRx);

                    if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                        MPLS_PATH_TYPE_TUNNEL)
                    {
                        u4Rxf = u4RcvTnlRx;
                    }
                    else
                    {
                        u4Rxf = u4PwOrFwrdTnlRx;
                    }
                }
            }
            if ((u1RxLmmDFlagOTF == RFC6374_TS_FORMAT_IEEE1588) ||
                (u1RxLmmDFlagOTF == RFC6374_TS_FORMAT_NTP))
            {
                /* Get the measurement time between two packets */
                if ((R6374UtilCalTimeDiff (&pLmInfo->LmOriginTimeStamp,
                                           &LossLmRxTimeStamp,
                                           &TotalDurationTxRx)) !=
                    RFC6374_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374ProcessLmm: "
                                 "Calculate the Time Difference failure\r\n");
                    return RFC6374_FAILURE;
                }
            }
            /* Timestamp received from querier updated
             * to QTS format which will be used for timestamp 
             * format calculation */
            pLmInfo->u1LmQTSFormat = u1RxLmmDFlagOTF;

            /*Copy Tx fetched from PDU and Rx fetched from BFD to pPktSmInfo */
            p1LmPduInfo->u4TxCounterf = u4Txf;
            p1LmPduInfo->u4RxCounterf = u4Rxf;

            if (R6374CalcFrameLoss (pPktSmInfo, TotalDurationTxRx,
                                    RFC6374_LM_TYPE_ONE_WAY, u4RxLmSessionId,
                                    pLmInfo->u4RxLmSeqCount) != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ProcessLmm: "
                             " calculation for frame loss return failure\r\n");
                return RFC6374_FAILURE;
            }

            pLmInfo->u4PreTxCounterf = p1LmPduInfo->u4TxCounterf;
            pLmInfo->u4PreRxCounterb = p1LmPduInfo->u4RxCounterf;
        }
    }
    else if (u1RxLmCtrlCode == RFC6374_CTRL_CODE_RESP_INBAND_REQ)
    {
        /*Increment LMM In */
        RFC6374_INCR_STATS_LMM_IN (pServiceInfo);

        /*Two Way Loss Measurement */
        pLmmPduInfo = &(pPktSmInfo->uPktInfo.Lmm);

        /* Version Validation */
        u4RxLmmVersion = u4RxLmmVersion >> 4;
        if (u4RxLmmVersion != RFC6374_PDU_VERSION)
        {
            /* Unsupported Version in LMM */
            pLmInfo->u1LmInvalidField = RFC6374_INVALID_VERSION;
        }
        /* Get PDU length */
        /* + Message Length (2 Byte) */
        RFC6374_GET_2BYTE (u2PduLength, pu1Pdu);

        /* Data Format Validation */
        RFC6374_GET_1BYTE (u1RxDFlag, pu1Pdu);
        /* For OTF Validation is not required for LM */

        u1RxDFlag = u1RxDFlag >> 4;
        if (u1RxDFlag != (RFC6374_LM_32BIT_COUNTERS | RFC6374_LM_PKT_COUNT))
        {
            /* Unsupported Data Flag */
            pLmInfo->u1LmInvalidField = RFC6374_INVALID_DATA_FORMAT;
        }

        /* Move the Pointer to Origin timestamp 
         * Reserved(3 Byte) + Session ID(4 Byte) */
        pu1Pdu = pu1Pdu + 7;
        /* Get TIme of start */
        RFC6374_GET_4BYTE (u4QueryOriginSeconds, pu1Pdu);
        RFC6374_GET_4BYTE (u4QueryOriginNanoSeconds, pu1Pdu);

        /* Calculate inter query interval and set if Unsupported Query Interval
         *  error message has to be initiated */
        R6374UtilCheckInterQueryInterval (pServiceInfo,
                                          u4QueryOriginSeconds,
                                          u4QueryOriginNanoSeconds,
                                          RFC6374_TEST_MODE_LM);

        /* Get Counters TX of the Forward */
        /* This is when 64bit Counter is not set */
        /* Counter 1 */
        RFC6374_GET_4BYTE (u4TxTemp, pu1Pdu);
        RFC6374_GET_4BYTE (u4Txf, pu1Pdu);
        /* Counter 2 */
        RFC6374_GET_4BYTE (u4TxTemp, pu1Pdu);
        RFC6374_GET_4BYTE (u4Rxf, pu1Pdu);

        if (u4Rxf == RFC6374_INIT_VAL)
        {
            if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMM)
            {
#ifdef RFC6374STUB_WANTED
                if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                    MPLS_PATH_TYPE_TUNNEL)
                {
                    R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
                }
                else
#endif
                {
                    i4RetVal = R6374GetBfdPktCount (pServiceInfo,
                                                    &u4Txf, &u4Rxf);
                    if (i4RetVal == RFC6374_FAILURE)
                    {
                        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                     RFC6374_ALL_FAILURE_TRC,
                                     RFC6374_DEFAULT_CONTEXT_ID,
                                     "R6374ProcessLmm: "
                                     "Get BFD Packet Counter failure\r\n");
                        return RFC6374_FAILURE;
                    }
                }
            }
            else if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMM)
            {
                R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx,
                                    &u4PwOrFwrdTnlRx, &u4RcvTnlTx, &u4RcvTnlRx);

                if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                    MPLS_PATH_TYPE_TUNNEL)
                {
                    u4Rxf = u4RcvTnlRx;
                }
                else
                {
                    u4Rxf = u4PwOrFwrdTnlRx;
                }
            }
        }

        /*Copy Tx fetched from PDU and Rx fetched from BFD to pPktSmInfo */
        pLmmPduInfo->u4TxCounterf = u4Txf;    /*A_TxP */
        pLmmPduInfo->u4RxCounterf = u4Rxf;    /*B_RxP */

        if (u2PduLength > RFC6374_LM_PDU_MSG_LEN)
        {
            /* Move pointer to TLV block */
            pu1Pdu = pu1Pdu + 16;
            /* Check if SQI TLV is present
             * 1. If SQI is there with interval zero, Response has to be sent with 
             *     Reception interval
             * 2. If SQI is there with non-zero interval, it means the Query interval
             *    has been adjusted in the other end.
             *    So reset the sending flag */
            RFC6374_GET_1BYTE (u1TlvType, pu1Pdu);
            if (u1TlvType == RFC6374_SQI_TLV_TYPE)
            {
                RFC6374_GET_1BYTE (u1TlvLength, pu1Pdu);
                RFC6374_GET_4BYTE (u4SessQueryInterval, pu1Pdu);

                if (u4SessQueryInterval == 0)
                {
                    /* Indicates to start initial adjustment */
                    pServiceInfo->LmInfo.b1IsSessQueryTlvRequired = OSIX_TRUE;
                }
                else
                {
                    /* Indicates initial or intermediate adjustment is done */
                    pServiceInfo->LmInfo.b1IsSessQueryTlvRequired = OSIX_FALSE;
                    pServiceInfo->LmInfo.u1LmInvalidField =
                        RFC6374_VALIDATION_SUCCESS;
                }
            }
        }
        /*Call Init process to send LMR */
        if (R6374LmResXmitLmrPdu (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374ProcessLmm: "
                         "failure occurred while responding a LMR for LMM\r\n");
            return RFC6374_FAILURE;
        }
    }
    else
    {
        /*Increment LMM In */
        RFC6374_INCR_STATS_LMM_IN (pServiceInfo);

        /* Invalid Control Code */
        pLmInfo->u1LmInvalidField = RFC6374_INVALID_CTRL_CODE;

        /*Call Init process to send LMR */
        if (R6374LmResXmitLmrPdu (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374ProcessLmm: "
                         "failure occurred while responding a LMR for LMM\r\n");
            return RFC6374_FAILURE;
        }
    }

    UNUSED_PARAM (u1TlvLength);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374ProcessLmm()\r\n");
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374LmResXmitLmrPdu
 *
 * Description        : This routine formats and transmits the LMR PDUs.
 *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the
 *                      information regarding Service info and MPLS pat Info
 *                      and other information related to the functionality.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374LmResXmitLmrPdu (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1RevPdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    UINT4               u4Txb = 0;
    UINT4               u4Rxb = 0;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT4               u4TimeInterval = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT2               u2PduLength = 0;
    UINT1              *pu1LmrPduStart = NULL;
    UINT1              *pu1LmrPduEnd = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmResXmitLmrPdu()\r\n");

    pLmInfo = RFC6374_GET_LM_INFO_FROM_PDUSM (pPktSmInfo);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmResXmitLmrPdu: Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1LmrPduStart = RFC6374_PDU;
    pu1LmrPduEnd = RFC6374_PDU;

    /* Format the Lmr PDU header */
    R6374LmInitFormatLmrPduHdr (pLmInfo, pu1RevPdu, &pu1LmrPduEnd);

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    /*Put Counter in LMR PDU */
    if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMM)
    {
        pPktSmInfo->u1PduType = RFC6374_LM_METHOD_INFERED;
#ifdef RFC6374STUB_WANTED
        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            R6374StubGetBfdCounter (&u4Txb, &u4Rxb);
        }
        else
#endif
        {
            i4RetVal = R6374GetBfdPktCount (pPktSmInfo->pServiceConfigInfo,
                                            &u4Txb, &u4Rxb);
            if (i4RetVal == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmResXmitLmrPdu: "
                             "Get BFD Packet Counter failure\r\n");
                RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
                return RFC6374_FAILURE;
            }
        }
    }
    else if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMM)
    {
        pPktSmInfo->u1PduType = RFC6374_LM_METHOD_DIRECT;
        R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                            &u4RcvTnlTx, &u4RcvTnlRx);

        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            u4Txb = u4PwOrFwrdTnlTx;
            u4Rxb = u4PwOrFwrdTnlRx;
        }
        else
        {
            u4Txb = u4PwOrFwrdTnlTx;
        }
    }

    /* Put Counter of Responder */
    /* case if 64bit counter bit is not set */
    /* counter 1 B_TxP */
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, u4Txb);    /*Tx of Responder */
    /* counter 2 A_RxP */
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, RFC6374_INIT_VAL);    /*Rx of Initiator Zero */
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, RFC6374_INIT_VAL);    /*Rx of Initiator Zero */
    /* Put Counter of Tx of Queriers 
     * Rx of Responder already in Lmm */
    /* Counter 3 A_TxP */
    /* Tx of initiator */
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, pPktSmInfo->uPktInfo.Lmm.u4TxCounterf);
    /* Counter 4 B_RxP */
    /* Rx of Responder */
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1LmrPduEnd, pPktSmInfo->uPktInfo.Lmm.u4RxCounterf);

    /* If SQI TLV is required, append it to the Response */
    if (pServiceInfo->LmInfo.b1IsSessQueryTlvRequired == OSIX_TRUE)
    {
        RFC6374_PUT_1BYTE (pu1LmrPduEnd, RFC6374_SQI_TLV_TYPE);
        RFC6374_PUT_1BYTE (pu1LmrPduEnd, RFC6374_SQI_TLV_LENGTH);
        /* Get and fill the adjusted value from database */
        u4TimeInterval = (UINT4) (pServiceInfo->u2MinReceptionInterval);
        RFC6374_PUT_4BYTE (pu1LmrPduEnd, u4TimeInterval);
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374LmResXmitLmrPdu: Response added with "
                      "SQI TLV with value %d\r\n",
                      pServiceInfo->u2MinReceptionInterval);
    }
    u2PduLength = (UINT2) (pu1LmrPduEnd - pu1LmrPduStart);

    pu1LmrPduEnd = pu1LmrPduStart + RFC6374_INDEX_TWO;
    RFC6374_PUT_2BYTE (pu1LmrPduEnd, u2PduLength);
    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1LmrPduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmitLmrPdu: Buffer copy operation failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Send the pBuf to MPLS */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);

    if (R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_LOSS)
        != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmitLmrPdu: " "Transmit LMR PDU failed\r\n");
        i4RetVal = RFC6374_FAILURE;
    }
    else
    {
        u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
        i4RetVal = RFC6374_SUCCESS;
    }

    UNUSED_PARAM (u4PduSize);
    if (i4RetVal != RFC6374_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmitLmrPdu: "
                     "LMR transmission to the lower layer failed\r\n");

        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /*Increment LMR OUT count */
    RFC6374_INCR_STATS_LMR_OUT (pPktSmInfo->pServiceConfigInfo);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmResXmitLmrPdu()\r\n");
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374LmInitFormatLmrPduHdr
 *
 * Description        : This routine formats LMR PDU
 *
 * Input(s)           : pLmInfo - Pointer to the structure that stores the
 *                      information regarding Loss Measurement.
 *                      pu1RevPdu -  Pointer to Recevied LMM PDU 
 *
 * Output(s)          : ppu1LmrPdu -  POinter to Pointer for LMR PDU
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC VOID
R6374LmInitFormatLmrPduHdr (tR6374LmInfoTableEntry * pLmInfo,
                            UINT1 *pu1RevPdu, UINT1 **ppu1LmrPdu)
{
    tR6374TSRepresentation OriginTimeStamp;
    UINT4               u4SessionId = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1               au1ReservedVal[RFC6374_PDU_RESERVED_FIELD_LEN]
        = { RFC6374_INIT_VAL };
    UINT1              *pu1LmrPdu = NULL;
    UINT1               u1VerFlag = RFC6374_INIT_VAL;
    UINT1               u1DFlagOTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitFormatLmrPduHdr()\r\n");

    RFC6374_MEMSET (&OriginTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    pu1LmrPdu = *ppu1LmrPdu;

    /* Get Version ang Flag
     * change R value to 1 and 
     * Copy T value from Rev PDU*/
    /*RFC6374_GET_1BYTE(u1VerFlag,pu1RevPdu); */
    u1VerFlag = RFC6374_PDU_VERSION | RFC6374_PDU_RESP_FLAG;

    /* Put Version and Flag */
    RFC6374_PUT_1BYTE (pu1LmrPdu, u1VerFlag);

    /* Check if Unsupported Query Interval has to be set */
    if (pLmInfo->u1LmInvalidField == RFC6374_INVALID_QUERY_INTERVAL)
    {
        RFC6374_PUT_1BYTE (pu1LmrPdu, RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL);
        /* Set back to Default for next Sequence */
        pLmInfo->u1LmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }
    else if (pLmInfo->u1LmInvalidField == RFC6374_VALIDATION_SUCCESS)
    {
        /* Put Control code as Success */
        RFC6374_PUT_1BYTE (pu1LmrPdu, RFC6374_RCVD_CTRL_CODE_SUCCESS);
    }
    else if (pLmInfo->u1LmInvalidField == RFC6374_INVALID_VERSION)
    {
        /* Put Control code as Invalid Version */
        RFC6374_PUT_1BYTE (pu1LmrPdu, RFC6374_CTRL_CODE_UNSUP_VERSION);

        /* Set back to Default for next Sequence */
        pLmInfo->u1LmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }
    else if (pLmInfo->u1LmInvalidField == RFC6374_INVALID_DATA_FORMAT)
    {
        /* Put Control code as Invalid Data Format */
        RFC6374_PUT_1BYTE (pu1LmrPdu, RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT);

        /* Set back to Default for next Sequence */
        pLmInfo->u1LmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }
    else if (pLmInfo->u1LmInvalidField == RFC6374_INVALID_CTRL_CODE)
    {
        /* Put Control code as Invalid control code */
        RFC6374_PUT_1BYTE (pu1LmrPdu, RFC6374_CTRL_CODE_UNSUP_CTRL_CODE);

        /* Set back to Default for next Sequence */
        pLmInfo->u1LmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }

    /*Put LMR PDU Length including verison, flag, message length field */
    u2PduLength = RFC6374_LM_PDU_MSG_LEN;
    RFC6374_PUT_2BYTE (pu1LmrPdu, u2PduLength);

    /* For now copy DFlag and OTF field */
    /* But when supporting for Counter 32Bit
     * Change Dflag X bit to 0 for 32bit counter
     * and just copy OTF in either case */
    /* move pu1RevPdu pointer to Dflag Field */
    pu1RevPdu = pu1RevPdu + 4;    /*size of version + control code + message field */
    RFC6374_GET_1BYTE (u1DFlagOTF, pu1RevPdu);
    /* Now Put in LMR PDU */
    RFC6374_PUT_1BYTE (pu1LmrPdu, u1DFlagOTF);

    /* Reserved Feild */
    RFC6374_PUT_NBYTE (pu1LmrPdu, au1ReservedVal,
                       RFC6374_PDU_RESERVED_FIELD_LEN);

    /* Copy Session Identifier, Origin Timestamp, 
     * and Origin Timestamp Format */
    /* Move the pu1RevPdu pointer to Session Identifier */
    pu1RevPdu = pu1RevPdu + 3;    /* REVERED field length */
    /* Get Session Identifer and DS field */
    RFC6374_GET_4BYTE (u4SessionId, pu1RevPdu);
    /* Put u4SesIdenDS in LMR PDU */
    RFC6374_PUT_4BYTE (pu1LmrPdu, u4SessionId);

    /* Get Origin Timestamp from pu1RevPdu */
    RFC6374_GET_4BYTE (OriginTimeStamp.u4Seconds, pu1RevPdu);
    RFC6374_GET_4BYTE (OriginTimeStamp.u4NanoSeconds, pu1RevPdu);

    /* Now Put in LMR PDU */
    RFC6374_PUT_4BYTE (pu1LmrPdu, OriginTimeStamp.u4Seconds);
    RFC6374_PUT_4BYTE (pu1LmrPdu, OriginTimeStamp.u4NanoSeconds);

    *ppu1LmrPdu = pu1LmrPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmInitFormatLmrPduHdr()\r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374LmDyadicMeasurement
 *
 * Description        : This routine initiate Dyadic measurment and does
 *                      calculation
 *
 * Input(s)           : pPktSmInfo - Pointer to the Service Related Config
 *                      pu1Pdu -  Pointer to Recevied LMM PDU
 *
 * Output(s)          : NULL
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374LmDyadicMeasurement (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374RxLmrPktInfo *pLossLmrPktInfo = NULL;
    tR6374TSRepresentation LossLmPduTimeStamp;
    tR6374TSRepresentation LossLmRxTimeStamp;
    tR6374TSRepresentation TotalDurationTxRx;
    tR6374NotifyInfo    R6374LmNotify;
    UINT4               u4Counter1 = RFC6374_INIT_VAL;
    UINT4               u4Counter2 = RFC6374_INIT_VAL;
    UINT4               u4Counter3 = RFC6374_INIT_VAL;
    UINT4               u4Counter4 = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4               u4TxTemp = RFC6374_INIT_VAL;
    UINT4               u4RxLmSessionId = RFC6374_INIT_VAL;
    UINT1               u1RxLmmVersion = RFC6374_INIT_VAL;
    UINT1               u1RxLmmDFlagOTF = RFC6374_INIT_VAL;
    UINT1              *pu1RevPdu = NULL;
    UINT1              *pu1CurPdu = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    INT4                i4SysSuppTimeFormat = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&LossLmPduTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&LossLmRxTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    pLmInfo = RFC6374_GET_LM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLossLmrPktInfo = &(pPktSmInfo->uPktInfo.Lmr);

    /* Retain a Copy of recevied LMM PDU Pointer */
    pu1CurPdu = pu1Pdu;
    pu1RevPdu = pu1Pdu;

    /* Move the Pointer to Counters */
    RFC6374_GET_1BYTE (u1RxLmmVersion, pu1CurPdu);    /* Get Version */
    u1RxLmmVersion = u1RxLmmVersion >> 4;
    if (u1RxLmmVersion != RFC6374_PDU_VERSION)
    {
        /* Unsupported Version in LMM */
        R6374LmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        return RFC6374_FAILURE;
    }

    /* ControlCode (1Byte) + Message (2Byte) */
    pu1CurPdu = pu1CurPdu + 3;

    RFC6374_GET_1BYTE (u1RxLmmDFlagOTF, pu1CurPdu);    /* Get DFlag and OTF */
    if ((u1RxLmmDFlagOTF >> 4) !=
        (RFC6374_LM_32BIT_COUNTERS | RFC6374_LM_PKT_COUNT))
    {
        /* Unsupported Data Flag */
        R6374LmNotify.u1TrapType = RFC6374_DATA_FORMAT_INVALID_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        return RFC6374_FAILURE;
    }
    i4SysSuppTimeFormat = (INT4) gR6374GlobalInfo.u1SysSuppTimeFormat;

    /* Unsupported timestamp format trap will be raised
     * when system does not support the timestamp format present in
     *  received QTF field */
    if (((u1RxLmmDFlagOTF == RFC6374_TS_FORMAT_IEEE1588) &&
         (!((i4SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_IEEE1588)
            == RFC6374_SUPP_TS_FORMAT_IEEE1588))) ||
        ((u1RxLmmDFlagOTF == RFC6374_TS_FORMAT_NTP) &&
         (!((i4SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_NTP)
            == RFC6374_SUPP_TS_FORMAT_NTP))))
    {
        /* Unsupported Time Stamp Format */
        R6374LmNotify.u1TrapType = RFC6374_UNSUP_TIMESTAMP_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);
        return RFC6374_FAILURE;
    }

    /* Reserved (3Byte) */
    pu1CurPdu = pu1CurPdu + 3;

    /* Session ID (4Byte) */
    RFC6374_GET_4BYTE (u4RxLmSessionId, pu1CurPdu);

    /* Dyadic Stop is exeicuted */
    if ((pLmInfo->u1LmStatus == RFC6374_TX_STATUS_READY) &&
        (pLmInfo->u4RxLmSessionId == u4RxLmSessionId))
    {
        /* This Node has exeicuted STOP measurement */
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmDyadicMeasurement: "
                     "Dyadic Measurement for LOSS is Stopped\r\n");
        return RFC6374_FAILURE;
    }

    if (pLmInfo->u4RxLmSessionId != u4RxLmSessionId)
    {
        pLmInfo->u4RxLmSessionId = u4RxLmSessionId;
    }

    /* Get the packet Recived time */
    R6374UtilGetCurrentTime (&LossLmRxTimeStamp, u1RxLmmDFlagOTF);

    /* Decrement Response Count */
    RFC6374_LM_RESET_LM_NO_RESP_TX_COUNT (pLmInfo);

    /* Get PDU Time Stamp */
    RFC6374_GET_4BYTE (LossLmPduTimeStamp.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (LossLmPduTimeStamp.u4NanoSeconds, pu1CurPdu);

    /* Get Counters As this is One way get */
    /* Counter 1 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter1, pu1CurPdu);

    /* Counter 2 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter2, pu1CurPdu);

    /* Counter 3 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter3, pu1CurPdu);

    /* Counter 4 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter4, pu1CurPdu);

    if (u4Counter2 == RFC6374_INIT_VAL)
    {
        if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMM)
        {
#ifdef RFC6374STUB_WANTED
            if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_TUNNEL)
            {
                R6374StubGetBfdCounter (&u4Counter1, &u4Counter2);
            }
            else
#endif
            {
                i4RetVal = R6374GetBfdPktCount (pServiceInfo,
                                                &u4Counter1, &u4Counter2);
                if (i4RetVal == RFC6374_FAILURE)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                 RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374LmDyadicMeasurement: "
                                 "Get BFD Packet Counter failure\r\n");
                    return RFC6374_FAILURE;
                }
            }
        }
        else if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMM)
        {
            R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx,
                                &u4PwOrFwrdTnlRx, &u4RcvTnlTx, &u4RcvTnlRx);

            if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
                MPLS_PATH_TYPE_TUNNEL)
            {
                u4Counter2 = u4RcvTnlRx;
            }
            else
            {
                u4Counter2 = u4PwOrFwrdTnlRx;
            }
        }

    }

    /* Packet Counter Assignement */
    pLossLmrPktInfo->u4TxCounterf = u4Counter1;    /* A_Tx */
    pLossLmrPktInfo->u4RxCounterb = u4Counter2;    /* B_Rx */
    pLossLmrPktInfo->u4TxCounterb = u4Counter3;    /* B_Tx */
    pLossLmrPktInfo->u4RxCounterf = u4Counter4;    /* A_Rx */

    /* Keep the Value to send in next query */
    pLmInfo->u4TxCounterf = u4Counter1;    /* A_Tx */
    pLmInfo->u4RxCounterb = u4Counter2;    /* B_Rx */

    if ((pLmInfo->u4LmSeqCount == RFC6374_INIT_VAL) &&
        (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_READY))
    {
        if ((R6374LmInitiator (pPktSmInfo, RFC6374_LM_START_SESSION))
            != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374LmDyadicMeasurement: "
                         "Inititate Dyadic Measurement failed for LMInitiator\r\n");
            return RFC6374_FAILURE;
        }
    }
    if ((u1RxLmmDFlagOTF == RFC6374_TS_FORMAT_IEEE1588) ||
        (u1RxLmmDFlagOTF == RFC6374_TS_FORMAT_NTP))
    {
        /* Dyadic Calculation for recevier node */
        /* Get the measurement time between two packets */
        if ((R6374UtilCalTimeDiff (&LossLmPduTimeStamp,
                                   &LossLmRxTimeStamp,
                                   &TotalDurationTxRx)) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374LmDyadicMeasurement: "
                         "Dyadic Calculate the Time Difference failure\r\n");
            return RFC6374_FAILURE;
        }
    }
    /* Timestamp received from querier updated
     * to QTS format which will be used for timestamp
     * format calculation */
    pLmInfo->u1LmQTSFormat = u1RxLmmDFlagOTF;

    if (R6374CalcFrameLoss (pPktSmInfo, TotalDurationTxRx,
                            RFC6374_LM_TYPE_TWO_WAY, pLmInfo->u4LmSessionId,
                            pLmInfo->u4LmSeqCount) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmDyadicMeasurement: "
                     "Dyadic calculation for frame loss return failure\r\n");
        /* update QTSFormat value as configured one after processing the
         * received Dyadic PDU because this format will be used for 
         * own dyadic lm pdu transmission */
        pLmInfo->u1LmQTSFormat = pLmInfo->u1LmTSFormat;
        return RFC6374_FAILURE;
    }

    /* update QTSFormat value as configured one after processing the
     * received Dyadic PDU. Since QTSFormat is updated as received QTS format 
     * for calculation purpose. */
    pLmInfo->u1LmQTSFormat = pLmInfo->u1LmTSFormat;

    /* Keep the counter */
    pLmInfo->u4PreTxCounterf = pLossLmrPktInfo->u4TxCounterf;    /* A_Tx */
    pLmInfo->u4PreRxCounterb = pLossLmrPktInfo->u4RxCounterb;    /* B_Rx */
    pLmInfo->u4PreTxCounterb = pLossLmrPktInfo->u4TxCounterb;    /* B_Tx */
    pLmInfo->u4PreRxCounterf = pLossLmrPktInfo->u4RxCounterf;    /* A_Rx */

    UNUSED_PARAM (pu1RevPdu);
    return RFC6374_SUCCESS;
}
#endif
