/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374main.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the R6374 task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#define __R6374MAIN_C__
#include "r6374inc.h"

/****************************************************************************
*                                                                           *
* Function     : R6374Task                                               *
*                                                                           *
* Description  : Main function of R6374.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
R6374TaskMain ()
{
    UINT4               u4Event = 0;

    while (OSIX_TRUE)
    {

        if (OsixReceiveEvent ((RFC6374_QMSG_EVENT | RFC6374_TIMER_EVENT),
                              OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) != RFC6374_SUCCESS)
        {
            continue;
        }

        R6374TaskLock ();
        if ((u4Event & RFC6374_TIMER_EVENT) == RFC6374_TIMER_EVENT)
        {
            R6374TmrHandleExpiry ();
        }

        if ((u4Event & RFC6374_QMSG_EVENT) == RFC6374_QMSG_EVENT)
        {
            R6374QueProcessMsgs ();
        }
        /* Mutual exclusion flag OFF */
        R6374TaskUnLock ();
    }
}

/****************************************************************************
*                                                                           *
* Function     : R6374TaskInit                                              *
*                                                                           *
* Description  : R6374 initialization routine.                              *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RFC6374_SUCCESS, if initialization succeeds                *
*                RFC6374_FAILURE, otherwise                                 *
*                                                                           *
*****************************************************************************/
PUBLIC UINT4
R6374TaskInit (VOID)
{
    UINT1               u1SuppTimeFormat = RFC6374_INIT_VAL;
    MEMSET (&gR6374GlobalInfo, 0, sizeof (gR6374GlobalInfo));
    MEMCPY (gR6374GlobalInfo.au1TaskSemName, RFC6374_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);

    /* Create the R6374 task semaphore. */
    if (OsixCreateSem (RFC6374_MUT_EXCL_SEM_NAME, RFC6374_SEM_CREATE_INIT_CNT,
                       RFC6374_INIT_VAL, &RFC6374_SEM_ID ()) == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Create the R6374 task Q. */
    if (OsixCreateQ (RFC6374_QUEUE_NAME, RFC6374_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (RFC6374_QUEUE_ID ())) == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Create RBTree for all the tables in R6374 module. */
    if (R6374CreateAllTables () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Create buffer pools for data structures */
    if (R6374MemInit () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Initialize the Timer list for all the timers. */
    if (R6374TmrInit () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Register with VCM module. */
    if (R6374VcmRegister () != RFC6374_SUCCESS)
    {
        return RFC6374_FAILURE;
    }

    if (OsixTskIdSelf (&RFC6374_TASK_ID ()) != RFC6374_SUCCESS)
    {
        return RFC6374_FAILURE;
    }
    gR6374GlobalInfo.u1SystemControl[RFC6374_DEFAULT_CONTEXT_ID] =
        RFC6374_START;
    gR6374GlobalInfo.u1ModuleStatus = RFC6374_ENABLED;
    /* Default trace option */
    gR6374GlobalInfo.u4TraceLevel = RFC6374_CRITICAL_TRC;

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to log messages. */
    RFC6374_SYSLOG_ID =
        SYS_LOG_REGISTER ((CONST UINT1 *) RFC6374_TASK_NAME,
                          SYSLOG_CRITICAL_LEVEL);
#endif

    R6374GetSysSuppTimeFormat (&u1SuppTimeFormat);

    gR6374GlobalInfo.u1SysSuppTimeFormat = u1SuppTimeFormat;

    return RFC6374_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : R6374DeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
R6374TaskDeInit (VOID)
{
    if (RFC6374_SEM_ID ())
    {
        OsixSemDel (RFC6374_SEM_ID ());
    }
    if (RFC6374_QUEUE_ID ())
    {
        OsixQueDel (RFC6374_QUEUE_ID ());
    }
    R6374TmrDeInit ();
    R6374VcmDeRegister ();
    R6374DestroyAllTables (RFC6374_DEFAULT_CONTEXT_ID);
    R6374MemClear ();

#ifdef SYSLOG_WANTED
    if (gR6374GlobalInfo.i4SysLogId != 0)
    {
        SYS_LOG_DEREGISTER ((UINT4) (gR6374GlobalInfo.i4SysLogId));
    }
#endif

    gR6374GlobalInfo.u1SystemControl[RFC6374_DEFAULT_CONTEXT_ID] =
        RFC6374_SHUTDOWN;
    gR6374GlobalInfo.u1ModuleStatus = RFC6374_DISABLED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : R6374TaskLock                                           */
/*                                                                           */
/* Description  : Lock the R6374 Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
R6374TaskLock (VOID)
{
    if (OsixSemTake (RFC6374_SEM_ID ()) == RFC6374_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : R6374TaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the R6374 Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
R6374TaskUnLock (VOID)
{
    OsixSemGive (RFC6374_SEM_ID ());

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : R6374MemInit                                               */
/*                                                                           */
/* Description  : Allocates all the memory that is required for R6374        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFC6374_SUCCESS or RFC6374_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
R6374MemInit (VOID)
{
    if (R6374SizingMemCreateMemPools () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }
    RFC6374_QMSG_POOL () = R6374MemPoolIds[MAX_RFC6374_QUEUE_DEPTH_SIZING_ID];

    return RFC6374_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : R6374MemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
R6374MemClear (VOID)
{
    R6374SizingMemDeleteMemPools ();

    RFC6374_QMSG_POOL () = RFC6374_INIT_VAL;
}

/*****************************************************************************/
/* Function     : R6374VcmRegister                                           */
/*                                                                           */
/* Description  : To register with VCM module, so that VCM will intimate     */
/*                about the context creation or deletion to RFC674   module  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFC6374_SUCCESS/RFC6374_FAILURE                            */
/*****************************************************************************/
INT4
R6374VcmRegister (VOID)
{
    tR6374ExtInParams  *pR6374ExtInParams = NULL;
    tR6374ExtOutParams *pR6374ExtOutParams = NULL;

    if ((pR6374ExtInParams = (tR6374ExtInParams *)
         MemAllocMemBlk (RFC6374_INPUTPARAMS_POOLID)) == NULL)
    {
        return RFC6374_FAILURE;
    }
    if ((pR6374ExtOutParams = (tR6374ExtOutParams *)
         MemAllocMemBlk (RFC6374_OUTPUTPARAMS_POOLID)) == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        return RFC6374_FAILURE;
    }

    MEMSET (pR6374ExtInParams, 0, sizeof (tR6374ExtInParams));
    MEMSET (pR6374ExtOutParams, 0, sizeof (tR6374ExtOutParams));

    pR6374ExtInParams->eExtReqType = RFC6374_VCM_REGISTER_WITH_VCM;
    pR6374ExtInParams->u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;

    if (R6374PortHandleExtInteraction (pR6374ExtInParams, pR6374ExtOutParams) !=
        RFC6374_SUCCESS)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return RFC6374_FAILURE;
    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
    RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtOutParams);
    return RFC6374_SUCCESS;
}

/*****************************************************************************/
/* Function     : R6374VcmDeRegister                                      */
/*                                                                           */
/* Description  : To de-register with the VCM module                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
R6374VcmDeRegister (VOID)
{
    tR6374ExtInParams  *pR6374ExtInParams = NULL;
    tR6374ExtOutParams *pR6374ExtOutParams = NULL;

    if ((pR6374ExtInParams = (tR6374ExtInParams *)
         MemAllocMemBlk (RFC6374_INPUTPARAMS_POOLID)) == NULL)
    {
        return;
    }
    if ((pR6374ExtOutParams = (tR6374ExtOutParams *)
         MemAllocMemBlk (RFC6374_OUTPUTPARAMS_POOLID)) == NULL)
    {
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        return;
    }

    MEMSET (pR6374ExtInParams, 0, sizeof (tR6374ExtInParams));
    MEMSET (pR6374ExtOutParams, 0, sizeof (tR6374ExtOutParams));

    pR6374ExtInParams->eExtReqType = RFC6374_VCM_DEREGISTER_WITH_VCM;
    pR6374ExtInParams->u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;

    if (R6374PortHandleExtInteraction (pR6374ExtInParams, pR6374ExtOutParams) !=
        RFC6374_SUCCESS)
    {
    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
    RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtOutParams);
}

/*****************************************************************************/
/* Function     : R6374DestroyAllTables                                      */
/*                                                                           */
/* Description  : To destroy all the tables created                          */
/*                                                                           */
/* Input        : u4ContextId                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
R6374DestroyAllTables (UINT4 u4ContextId)
{
    /* Delete Service Config RB Tree */
    R6374UtilDeleteSerConfigTableInContext (u4ContextId);

    /* Delete Delay Measurement RB Tree */
    R6374UtilDeleteDmBufTableInContext (u4ContextId);

    /* Delete Loss Measurement RB Tree */
    R6374UtilDeleteLmBufTableInContext (u4ContextId);

    /* Delete Delay Measurement RB Tree */
    R6374UtilDeleteLmStatsTableInContext (u4ContextId);

    /* Delete Loss Measurement RB Tree */
    R6374UtilDeleteDmStatsTableInContext (u4ContextId);

}

/****************************************************************************
 Function    :  R6374CreateAllTables
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/
PUBLIC INT4
R6374CreateAllTables ()
{
    if (R6374ServiceConfigTableCreate () == RFC6374_FAILURE)
    {
        return (RFC6374_FAILURE);
    }

    if (R6374LmBufferTableCreate () == RFC6374_FAILURE)
    {
        return (RFC6374_FAILURE);
    }

    if (R6374LmStatsTableCreate () == RFC6374_FAILURE)
    {
        return (RFC6374_FAILURE);
    }

    if (R6374DmBufferTableCreate () == RFC6374_FAILURE)
    {
        return (RFC6374_FAILURE);
    }

    if (R6374DmStatsTableCreate () == RFC6374_FAILURE)
    {
        return (RFC6374_FAILURE);
    }

    return RFC6374_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  R6374main.c                     */
/*-----------------------------------------------------------------------*/
