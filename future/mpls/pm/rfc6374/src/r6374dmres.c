/********************************************************************
 * Copyright (C) 2016  Aricent Inc . All Rights Reserved
 *
 * $Id: r6374dmres.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way and 2
 *              way Delay Mesaurement of Control Sub Module.
 *******************************************************************/
#ifndef __R6374DMRES_C__
#define __R6374DMRES_C__
#include "r6374inc.h"

/*******************************************************************************
 * Function           : R6374ProcessDmm
 *
 * Description        : This routine processes the received DMM PDU, and
 *                      trasnmits the DMR in response.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the
 *                      information regarding mp info, the PDU if received and
 *                      other information related to the functionality.
 *                      pu1Pdu  - Pointer to the recevied PDU
 *
 * Output(s)          : None.
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374ProcessDmm (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pkt)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374RxDmmPktInfo *pDmmPktInfo = NULL;
    tR6374NotifyInfo    R6374DmNotify;
    UINT1              *pu1RevPdu = NULL;
    UINT4               u4RxDmmVersion = RFC6374_INIT_VAL;
    UINT4               u4SessQueryInterval = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1               u1TlvType = RFC6374_INIT_VAL;
    UINT1               u1TlvLength = RFC6374_INIT_VAL;
    UINT1               u1DmCtrlCode = RFC6374_INIT_VAL;
    UINT1               u1QueryTF = RFC6374_INIT_VAL;
    UINT1               u1ResPrefTF = RFC6374_INIT_VAL;
    UINT1               u1ResTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374ProcessDmm()\r\n");

    RFC6374_MEMSET (&R6374DmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    if (pPktSmInfo->pServiceConfigInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374ProcessDmm: " "Recevied Service Info is NULL\r\n");
        return RFC6374_FAILURE;
    }

    pDmInfo = RFC6374_GET_DM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);

    /* Keep the Received PDU Pointer */
    pu1RevPdu = pu1Pkt;

    /* Get version from pdu */
    RFC6374_GET_1BYTE (u4RxDmmVersion, pu1Pkt);

    /* Get control code from pdu */
    RFC6374_GET_1BYTE (u1DmCtrlCode, pu1Pkt);

    /*Check if Response is requested */
    if (u1DmCtrlCode == RFC6374_CTRL_CODE_RESP_NOT_REQ)
    {
        if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
        {
            /* For Dyadic DMM Packet is sent with control code 0x2 */
            /* Increment DMM In */
            RFC6374_INCR_STATS_DMM_IN (pServiceInfo);

            if (R6374DmDyadicMeasurement (pPktSmInfo, pu1RevPdu) !=
                RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ProcessDmm: "
                             "Dyadic Measurement Failed\r\n");
                return RFC6374_FAILURE;
            }
        }
        else
        {
            pDmInfo->u1DmQTSFormat = pDmInfo->u1DmTSFormat;
            /*Call R6374Process1Dm to process 1DM */
            if (R6374Process1Dm (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ProcessDmm: "
                             "failure occurred while responding a DMR for DMM\r\n");
                return RFC6374_FAILURE;
            }
        }
    }
    else if (u1DmCtrlCode == RFC6374_CTRL_CODE_RESP_INBAND_REQ)
    {
        RFC6374_INCR_STATS_DMM_IN (pPktSmInfo->pServiceConfigInfo);

        pDmmPktInfo = &(pPktSmInfo->uPktInfo.Dmm);

        u4RxDmmVersion = u4RxDmmVersion >> 4;
        if (u4RxDmmVersion != RFC6374_PDU_VERSION)
        {
            /* Unsupported Version in DMR */
            pDmInfo->u1DmInvalidField = RFC6374_INVALID_VERSION;
        }

        u1ResTF = pDmInfo->u1DmTSFormat;
        u1ResPrefTF = pDmInfo->u1DmTSFormat;

        /* Get PDU length */
        /* + Message Length (2 Byte) */
        RFC6374_GET_2BYTE (u2PduLength, pu1Pkt);

        /* QTF Validation */
        /* + Message (2Byte) */
        RFC6374_GET_1BYTE (u1QueryTF, pu1Pkt);
        u1QueryTF = u1QueryTF >> 4;
        /* By receiving DM Query message, the responder determines whether 
         * it is capable of writing timestamps in the format specified by 
         * the QTF field. If so, the Responder Timestamp Format (RTF) field 
         * is set equal to the QTF field. If not, the RTF field is set equal 
         * to the Responder’s Preferred Timestamp Format (RPTF) field */
        if (u1QueryTF != u1ResTF)
        {
            /* Unsupported TimeStamp Format */
            pDmInfo->u1DmInvalidField = RFC6374_INVALID_TS_FORMAT;
            u1ResTF = u1ResPrefTF;
        }
        else
        {
            u1ResTF = u1QueryTF;
        }
        /* Get Timestamp */
        /* Reserved (3 Byte) + SessionID (4 Byte) */
        pu1Pkt = pu1Pkt + RFC6374_VAL_7;

        /* Copy the TxTimeStampf */
        RFC6374_GET_4BYTE (pDmmPktInfo->TxTimeStampf.u4Seconds, pu1Pkt);
        RFC6374_GET_4BYTE (pDmmPktInfo->TxTimeStampf.u4NanoSeconds, pu1Pkt);

        /* Calculate inter query interval and set if Unsupported Query Interval
         *  error message has to be initiated */
        R6374UtilCheckInterQueryInterval (pServiceInfo,
                                          pDmmPktInfo->TxTimeStampf.u4Seconds,
                                          pDmmPktInfo->TxTimeStampf.
                                          u4NanoSeconds, RFC6374_TEST_MODE_DM);

        /* Copy the Locally filled RxTimeStampf */
        RFC6374_GET_4BYTE (pDmmPktInfo->RxTimeStampf.u4Seconds, pu1Pkt);
        RFC6374_GET_4BYTE (pDmmPktInfo->RxTimeStampf.u4NanoSeconds, pu1Pkt);

        /* Check if RxTimeStampf was filled by the HW on reception */
        if ((pDmmPktInfo->RxTimeStampf.u4Seconds == RFC6374_INIT_VAL) &&
            (pDmmPktInfo->RxTimeStampf.u4NanoSeconds == RFC6374_INIT_VAL))
        {
            R6374UtilGetCurrentTime (&(pDmmPktInfo->RxTimeStampf), u1ResTF);
        }
        if (u2PduLength > RFC6374_DM_PDU_MSG_LEN)
        {
            /* Move pointer to TLV block */
            pu1Pkt = pu1Pkt + RFC6374_INDEX_SIXTEEN;
            /* Check if SQI TLV is present
             * 1. If SQI is there with interval zero, Response has to be sent with
             *     Reception interval
             * 2. If SQI is there with non-zero interval, it means the Query interval
             *    has been adjusted in the other end.
             *    So reset the sending flag */
            RFC6374_GET_1BYTE (u1TlvType, pu1Pkt);

            if (u1TlvType == RFC6374_SQI_TLV_TYPE)
            {
                RFC6374_GET_1BYTE (u1TlvLength, pu1Pkt);
                RFC6374_GET_4BYTE (u4SessQueryInterval, pu1Pkt);

                if (u4SessQueryInterval == RFC6374_INIT_VAL)
                {
                    /* Indicates to start initial adjustment */
                    pServiceInfo->DmInfo.b1IsSessQueryTlvRequired = OSIX_TRUE;
                }
                else
                {
                    /* Indicates initial or intermediate adjustment is done */
                    pServiceInfo->DmInfo.b1IsSessQueryTlvRequired = OSIX_FALSE;
                    pServiceInfo->DmInfo.u1DmInvalidField =
                        RFC6374_VALIDATION_SUCCESS;
                }
            }
        }

        /* Transmit the DMR */
        if (R6374DmResXmitDmrPdu (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374ProcessDmm:\r\n"
                         "failure occurred while responding a DMR for DMM\r\n");
            return RFC6374_FAILURE;
        }
    }
    else
    {
        RFC6374_INCR_STATS_DMM_IN (pPktSmInfo->pServiceConfigInfo);

        /* Invalid Control Code */
        pDmInfo->u1DmInvalidField = RFC6374_INVALID_CTRL_CODE;

        /* Transmit the DMR */
        if (R6374DmResXmitDmrPdu (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374ProcessDmm:\r\n"
                         "failure occurred while responding a DMR for DMM\r\n");
            return RFC6374_FAILURE;
        }
    }

    UNUSED_PARAM (u1TlvLength);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374ProcessDmm()\r\n");

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374DmResXmitDmrPdu
 *
 * Description        : This routine formats and transmits the DMR PDUs.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the
 *                      information regarding mp info, the PDU if received and
 *                      other information related to the functionality.
 *                      pu1RevPdu  - Pointer to the recevied PDU
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374DmResXmitDmrPdu (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1RevPdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374TSRepresentation TxTimeStampb;
    UINT4               u4TimeInterval = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT1              *pu1DmrPduStart = NULL;
    UINT1              *pu1DmrPduEnd = NULL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT2               u2RxPduLen = RFC6374_INIT_VAL;
    BOOL1               b1MemFlag = RFC6374_FAILURE;
    UINT1               u1ResTF = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&TxTimeStampb, RFC6374_INIT_VAL, sizeof (TxTimeStampb));

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmResXmitDmrPdu()\r\n");

    pDmInfo = RFC6374_GET_DM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmResXmitDmrPdu: "
                     "failure occurred while allocating the cru buffer\r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1DmrPduStart = RFC6374_PDU;
    pu1DmrPduEnd = RFC6374_PDU;

    /* Format the DMR PDU header */
    R6374DmInitFormatDmrPduHdr (pDmInfo, pu1RevPdu, &pu1DmrPduEnd);

    u1ResTF = pDmInfo->u1DmTSFormat;

    /* Generate the current timestamp as TxTimeStampb based on 
     * responder timestamp format and fill it in DMR frame */
    R6374UtilGetCurrentTime (&TxTimeStampb, u1ResTF);

    /*Move the Offset to TxTimeStampb Field */
    /* Counter 1 - Fill Tx of responder */
    RFC6374_PUT_4BYTE (pu1DmrPduEnd, TxTimeStampb.u4Seconds);
    RFC6374_PUT_4BYTE (pu1DmrPduEnd, TxTimeStampb.u4NanoSeconds);

    /* counter 2  - RX counter - To be filled by the receiver */
    RFC6374_PUT_4BYTE (pu1DmrPduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1DmrPduEnd, RFC6374_INIT_VAL);

    /* Counter 3 - copy Tx of Querier from pkt to counter 3 */
    RFC6374_PUT_4BYTE (pu1DmrPduEnd,
                       pPktSmInfo->uPktInfo.Dmm.TxTimeStampf.u4Seconds);
    RFC6374_PUT_4BYTE (pu1DmrPduEnd,
                       pPktSmInfo->uPktInfo.Dmm.TxTimeStampf.u4NanoSeconds);

    /* Counter 4 - copy Rx of Responder from pkt to counter 4 */
    RFC6374_PUT_4BYTE (pu1DmrPduEnd,
                       pPktSmInfo->uPktInfo.Dmm.RxTimeStampf.u4Seconds);
    RFC6374_PUT_4BYTE (pu1DmrPduEnd,
                       pPktSmInfo->uPktInfo.Dmm.RxTimeStampf.u4NanoSeconds);

    /* Get Pdu length */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_2;
    RFC6374_GET_2BYTE (u2RxPduLen, pu1RevPdu);

    /* Get Padding length */
    /* Currently pu1RevPdu is pointing the Pdu length field
     * Move 40 bytes from msg length to get Padding TLV Type field */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_40;

    /* If SQI TLV is required, append it to the Response */
    if (pServiceInfo->DmInfo.b1IsSessQueryTlvRequired == OSIX_TRUE)
    {
        RFC6374_PUT_1BYTE (pu1DmrPduEnd, RFC6374_SQI_TLV_TYPE);
        RFC6374_PUT_1BYTE (pu1DmrPduEnd, RFC6374_SQI_TLV_LENGTH);
        /* Get and fill the adjusted value from database */
        u4TimeInterval = (UINT4) (pServiceInfo->u2MinReceptionInterval);
        RFC6374_PUT_4BYTE (pu1DmrPduEnd, u4TimeInterval);
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374DmResXmitLmrPdu: Response added with "
                      "SQI TLV with value %d\r\n",
                      pServiceInfo->u2MinReceptionInterval);
    }
    if (u2RxPduLen > RFC6374_DM_PDU_MSG_LEN)
    {
        /*Consider only Padding length */
        u2RxPduLen = (UINT2) (u2RxPduLen - RFC6374_DM_PDU_MSG_LEN);
        R6374DmRxPutPadTlv (&pu1DmrPduEnd, pu1RevPdu, u2RxPduLen);
    }
    u2PduLength = (UINT2) (pu1DmrPduEnd - pu1DmrPduStart);

    pu1DmrPduEnd = pu1DmrPduStart + RFC6374_INDEX_TWO;
    RFC6374_PUT_2BYTE (pu1DmrPduEnd, u2PduLength);

    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1DmrPduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        b1MemFlag = RFC6374_SUCCESS;
    }

    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 "R6374DmResXmitDmrPdu: "
                 "Sending out DMR-PDU to lower layer...\r\n");

    /* Transmit the DMR over MPLS Path */
    i4RetVal = R6374MplsTxPkt (pBuf, pPktSmInfo,
                               RFC6374_MEASUREMENT_TYPE_DELAY);
    if (i4RetVal != RFC6374_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmResXmitDmrPdu: "
                     "failure occurred while formating and transmitting DMR frame\r\n");

        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
    }
    else if (b1MemFlag == RFC6374_SUCCESS)
    {
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
    }

    /*Increment DMR OUT count */
    RFC6374_INCR_STATS_DMR_OUT (pPktSmInfo->pServiceConfigInfo);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmResXmitDmrPdu()\r\n");

    return i4RetVal;
}

/*******************************************************************************
 * Function Name      : R6374DmInitFormatDmrPduHdr
 *
 * Description        : This routine formats DMR PDU
 *
 * Input(s)           : pDmInfo - Pointer to the structure that stores the
 *                      information regarding Loss Measurement.
 *                      pu1RevPdu -  Pointer to Recevied DMM PDU
 *
 * Output(s)          : ppu1DmrPdu -  POinter to Pointer for DMR PDU
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC VOID
R6374DmInitFormatDmrPduHdr (tR6374DmInfoTableEntry * pDmInfo,
                            UINT1 *pu1RevPdu, UINT1 **ppu1DmrPdu)
{
    UINT4               u4SessionId = RFC6374_INIT_VAL;
    UINT2               u2MsgLen = RFC6374_INIT_VAL;
    UINT1               u1VerFlag = RFC6374_INIT_VAL;
    UINT1               u1ResPrefTF = RFC6374_INIT_VAL;
    UINT1               u1QueryTF = RFC6374_INIT_VAL;
    UINT1               u1ResTF = RFC6374_INIT_VAL;
    UINT1              *pu1DmrPdu = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmInitFormatDmrPduHdr()\r\n");

    UNUSED_PARAM (pDmInfo);
    pu1DmrPdu = *ppu1DmrPdu;

    /* Get Version ang Flag change R value to 1 and
     * Copy T value from Rev PDU*/
    u1VerFlag = RFC6374_PDU_VERSION | RFC6374_PDU_RESP_FLAG;

    /* Put Version and Flag */
    RFC6374_PUT_1BYTE (pu1DmrPdu, u1VerFlag);

    /* Check if Unsupported Query Interval has to be set */
    if (pDmInfo->u1DmInvalidField == RFC6374_INVALID_QUERY_INTERVAL)
    {
        RFC6374_PUT_1BYTE (pu1DmrPdu, RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL);
        /* Set back to Default for next Sequence */
        pDmInfo->u1DmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }
    else if (pDmInfo->u1DmInvalidField == RFC6374_VALIDATION_SUCCESS)
    {
        /* Put Control code as Success */
        RFC6374_PUT_1BYTE (pu1DmrPdu, RFC6374_RCVD_CTRL_CODE_SUCCESS);
    }
    else if (pDmInfo->u1DmInvalidField == RFC6374_INVALID_VERSION)
    {
        RFC6374_PUT_1BYTE (pu1DmrPdu, RFC6374_CTRL_CODE_UNSUP_VERSION);

        /* Set back to Default for next Sequence */
        pDmInfo->u1DmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }
    else if (pDmInfo->u1DmInvalidField == RFC6374_INVALID_CTRL_CODE)
    {
        RFC6374_PUT_1BYTE (pu1DmrPdu, RFC6374_CTRL_CODE_UNSUP_CTRL_CODE);

        /* Set back to Default for next Sequence */
        pDmInfo->u1DmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }
    else if (pDmInfo->u1DmInvalidField == RFC6374_INVALID_TS_FORMAT)
    {
        RFC6374_PUT_1BYTE (pu1DmrPdu, RFC6374_CTRL_CODE_DATA_FORMAT_INVALID);

        /* Set back to Default for next Sequence */
        pDmInfo->u1DmInvalidField = RFC6374_VALIDATION_SUCCESS;
    }

    /* move pointer to get message length field */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_2;

    RFC6374_GET_2BYTE (u2MsgLen, pu1RevPdu);

    /*Put DMR PDU Length including verison, flag, message length field */

    RFC6374_PUT_2BYTE (pu1DmrPdu, u2MsgLen);

    RFC6374_GET_1BYTE (u1QueryTF, pu1RevPdu);

    u1ResTF = pDmInfo->u1DmTSFormat;

    u1ResTF = u1QueryTF | u1ResTF;
    RFC6374_PUT_1BYTE (pu1DmrPdu, u1ResTF);

    /* Responder Prefered Time Stamp Format for Negotiation
     * if QTF Dosen't match with RTF */
    u1ResPrefTF = pDmInfo->u1DmTSFormat;
    u1ResPrefTF = (UINT1) (u1ResPrefTF << RFC6374_VAL_4);
    u1ResPrefTF = u1ResPrefTF | 0x0000;
    RFC6374_PUT_1BYTE (pu1DmrPdu, u1ResPrefTF);

    /* Reserved Feild */
    RFC6374_PUT_2BYTE (pu1DmrPdu, RFC6374_INIT_VAL);

    /* Move the pu1RevPdu pointer to Session Identifier */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_3;

    /* Get Session Identifer and DS field */
    RFC6374_GET_4BYTE (u4SessionId, pu1RevPdu);
    /* Put u4SesIdenDS in DMR PDU */
    RFC6374_PUT_4BYTE (pu1DmrPdu, u4SessionId);

    *ppu1DmrPdu = pu1DmrPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmInitFormatDmrPduHdr()\r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374DmDyadicMeasurement
 *
 * Description        : This routine initiate Dyadic measurment and does
 *                      calculation
 *
 * Input(s)           : pPktSmInfo - Pointer to the Service Related Config
 *                      pu1Pdu -  Pointer to Recevied LMM PDU
 *
 * Output(s)          : NULL
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374DmDyadicMeasurement (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374RxDmrPktInfo *pDmrPktInfo = NULL;
    tR6374TSRepresentation TimeStamp1;
    tR6374TSRepresentation TimeStamp2;
    tR6374TSRepresentation TimeStamp3;
    tR6374TSRepresentation TimeStamp4;
    tR6374NotifyInfo    R6374DmNotify;
    UINT4               u4RxDmSessionId = RFC6374_INIT_VAL;
    UINT1               u1RxDmmVersion = RFC6374_INIT_VAL;
    UINT1               u1RxDmmQTFRTF = RFC6374_INIT_VAL;
    UINT1              *pu1RevPdu = NULL;
    UINT1              *pu1CurPdu = NULL;

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pDmrPktInfo = &(pPktSmInfo->uPktInfo.Dmr);

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmDyadicMeasurement()\r\n");

    RFC6374_MEMSET (&TimeStamp1, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&TimeStamp2, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&TimeStamp3, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&TimeStamp4, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&R6374DmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    /* Retain a Copy of recevied LMM PDU Pointer */
    pu1CurPdu = pu1Pdu;
    pu1RevPdu = pu1Pdu;

    /* Move the Pointer to Counters */
    RFC6374_GET_1BYTE (u1RxDmmVersion, pu1CurPdu);    /* Get Version */
    u1RxDmmVersion = u1RxDmmVersion >> 4;
    if (u1RxDmmVersion != RFC6374_PDU_VERSION)
    {
        /* Unsupported Version in DMM */
        R6374DmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374DmNotify);
        return RFC6374_FAILURE;
    }

    /* ControlCode (1Byte) + Message (2Byte) */
    pu1CurPdu = pu1CurPdu + 3;

    RFC6374_GET_1BYTE (u1RxDmmQTFRTF, pu1CurPdu);    /* QTF+RTF (1Byte) */
    u1RxDmmQTFRTF = u1RxDmmQTFRTF >> 4;
    if (u1RxDmmQTFRTF != pDmInfo->u1DmTSFormat)
    {
        /* Unsupported Time Stamp Format */
        R6374DmNotify.u1TrapType = RFC6374_UNSUP_TIMESTAMP_TRAP;
        RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374DmNotify);
        return RFC6374_FAILURE;
    }

    /* RPTF + Reserved (3Byte) */
    pu1CurPdu = pu1CurPdu + 3;

    /* Session ID (4Byte) */
    RFC6374_GET_4BYTE (u4RxDmSessionId, pu1CurPdu);

    /* Decrement Response Count */
    RFC6374_DM_RESET_DM_NO_RESP_TX_COUNT (pDmInfo);

    /* Dyadic Stop is exeicuted */
    if ((pDmInfo->u1DmStatus == RFC6374_TX_STATUS_READY) &&
        (pDmInfo->u4RxDmSessionId == u4RxDmSessionId))
    {
        /* This Node has exeicuted STOP measurement */
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmDyadicMeasurement: "
                     "Dyadic Measurement for DELAY is Stopped\r\n");
        return RFC6374_FAILURE;
    }

    if (pDmInfo->u4RxDmSessionId != u4RxDmSessionId)
    {
        pDmInfo->u4RxDmSessionId = u4RxDmSessionId;
    }

    /* Get the Time Stamp from PDU's */
    /* TimeStamp 1 */
    RFC6374_GET_4BYTE (TimeStamp1.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp1.u4NanoSeconds, pu1CurPdu);

    /* TimeStamp 2 */
    RFC6374_GET_4BYTE (TimeStamp2.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp2.u4NanoSeconds, pu1CurPdu);

    /* TimeStamp 3 */
    RFC6374_GET_4BYTE (TimeStamp3.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp3.u4NanoSeconds, pu1CurPdu);

    /* TimeStamp 4 */
    RFC6374_GET_4BYTE (TimeStamp4.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp4.u4NanoSeconds, pu1CurPdu);

    /* Get the B_Rx Time Stamp */
    if ((TimeStamp2.u4Seconds == RFC6374_INIT_VAL) ||
        (TimeStamp2.u4NanoSeconds == RFC6374_INIT_VAL))
    {
        R6374UtilGetCurrentTime (&TimeStamp2, u1RxDmmQTFRTF);
    }
    /* Keep the Value to send in next query */
    pDmInfo->TxTimeStampf.u4Seconds = TimeStamp1.u4Seconds;    /* A_Tx */
    pDmInfo->TxTimeStampf.u4NanoSeconds = TimeStamp1.u4NanoSeconds;

    pDmInfo->RxTimeStampf.u4Seconds = TimeStamp2.u4Seconds;    /* B_Rx */
    pDmInfo->RxTimeStampf.u4NanoSeconds = TimeStamp2.u4NanoSeconds;

    if ((pDmInfo->u4DmSeqCount == RFC6374_INIT_VAL) &&
        (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_READY))
    {
        /* For Dyadic Measurement TimeStamp Negotiation is not applicable */
        pDmInfo->u1DmQTSFormat = pDmInfo->u1DmTSFormat;

        if ((R6374DmInitiator (pPktSmInfo, RFC6374_DM_START_SESSION))
            != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmDyadicMeasurement: "
                         "Inititate Dyadic Measurement failed for DMInitiator\r\n");
            return RFC6374_FAILURE;
        }

        /* Set to True As responder has initiated a Measurement */
        pDmInfo->b1DmInitResponder = RFC6374_TRUE;
    }
    else
    {
        /* Time Stamp Initilization for Calculation for Two-way */
        /* A_Tx */
        pDmrPktInfo->TxTimeStampf.u4Seconds = TimeStamp3.u4Seconds;
        pDmrPktInfo->TxTimeStampf.u4NanoSeconds = TimeStamp3.u4NanoSeconds;

        /* B_Rx */
        pDmrPktInfo->RxTimeStampf.u4Seconds = TimeStamp4.u4Seconds;
        pDmrPktInfo->RxTimeStampf.u4NanoSeconds = TimeStamp4.u4NanoSeconds;

        /* B_Tx */
        pDmrPktInfo->TxTimeStampb.u4Seconds = TimeStamp1.u4Seconds;
        pDmrPktInfo->TxTimeStampb.u4NanoSeconds = TimeStamp1.u4NanoSeconds;

        /* A_Rx */
        pDmrPktInfo->RxTimeStampb.u4Seconds = TimeStamp2.u4Seconds;
        pDmrPktInfo->RxTimeStampb.u4NanoSeconds = TimeStamp2.u4NanoSeconds;

        /* Timestamp received from querier updated
         * to QTS format which will be used for timestamp
         * format calculation */
        pDmInfo->u1DmQTSFormat = u1RxDmmQTFRTF;

        if (R6374CalcFrameDelay (pPktSmInfo, pDmrPktInfo,
                                 RFC6374_DM_TYPE_TWO_WAY) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmDyadicMeasurement: "
                         "calculation for frame delay return failure\r\n");
            /* update QTSFormat value as configured one after processing the
             * received Dyadic PDU because this format will be used for 
             * own dyadic dm pdu transmission */
            pDmInfo->u1DmQTSFormat = pDmInfo->u1DmTSFormat;
            return RFC6374_FAILURE;
        }
        /* update QTSFormat value as configured one after processing the
         * received Dyadic PDU. Since QTSFormat is updated as received QTS format
         * for calculation purpose. */
        pDmInfo->u1DmQTSFormat = pDmInfo->u1DmTSFormat;
    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from  R6374DmDyadicMeasurement()\r\n");

    UNUSED_PARAM (pu1RevPdu);
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function Name      : R6374DmRxPutPadTlv                                   *
 * Description        : This rotuine is used to fill the PAD TLVs in the     *
 *                      DMR PDU                                              *
 * Input(s)           : ppu1PduEnd - Pointer to the Pdu                      *
 *                      pu1RevPdu - pointer to the received pdu              *
 *                      u4PadSize  - Pad Size                                *
 * Output(s)          : None                                                 *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
R6374DmRxPutPadTlv (UINT1 **ppu1PduEnd, UINT1 *pu1RevPdu, UINT2 u2RxPduLen)
{
    UINT1              *pu1Pdu = NULL;
    UINT1               u1PadType = RFC6374_INIT_VAL;
    UINT1               u1PadLen = RFC6374_INIT_VAL;
    INT2                i2PduLen = RFC6374_INIT_VAL;

    i2PduLen = (INT2) u2RxPduLen;

    pu1Pdu = *ppu1PduEnd;

    do
    {
        RFC6374_GET_1BYTE (u1PadType, pu1RevPdu);
        RFC6374_GET_1BYTE (u1PadLen, pu1RevPdu);

        if ((u1PadType == RFC6374_PADDING_TLV_COPY_IN_RESP) &&
            (u1PadLen != RFC6374_INIT_VAL))
        {
            RFC6374_PUT_1BYTE (pu1Pdu, u1PadType);
            RFC6374_PUT_1BYTE (pu1Pdu, u1PadLen);
            RFC6374_PUT_NBYTE (pu1Pdu, pu1RevPdu, u1PadLen);
        }
        pu1RevPdu = pu1RevPdu + u1PadLen;

        /* Decrement TLV whole size from i2PduLen
         * i.e, Type, Len, and Value corresponding
         * to Len */
        i2PduLen = (INT2) (i2PduLen - (INT2) (u1PadLen + RFC6374_VAL_2));

        if (i2PduLen == RFC6374_INIT_VAL)
        {
            /* No more TLV is exist */
            break;
        }
    }
    while (i2PduLen > RFC6374_INIT_VAL);

    *ppu1PduEnd = pu1Pdu;
}
#endif
