/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374bfd.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the R6374 task interaction 
 * with the BFD module.
 *
 *******************************************************************/
#ifndef __R6374BFD_C__
#define __R6374BFD_C__
#include "r6374inc.h"

/******************************************************************************
 * Function Name      : R6374GetBfdPktCount
 *
 * Description        : This routine is used to get call bfd Tx and Rx Counters
 *                      based on BFD Session Index.
 *
 * Input(s)           : pServiceInfo -  Service Related Config, BFD MPLS path
 *                                      params
 *
 * Output(s)          : pu4Tx - TxCounter
 *                      pu4Rx - RxCounter
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
INT4
R6374GetBfdPktCount (tR6374ServiceConfigTableEntry * pServiceInfo,
                     UINT4 *pu4Tx, UINT4 *pu4Rx)
{
    tR6374ExtInParams  *pR6374ExtInParams;
    tR6374ExtOutParams *pR6374ExtOutParams;
    tR6374NotifyInfo    R6374LmNotify;
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT4               u4SessionIndex = RFC6374_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;

    /* Allocate Memory for R6374 IN Structure */
    pR6374ExtInParams = (tR6374ExtInParams *) MemAllocMemBlk
        (RFC6374_INPUTPARAMS_POOLID);
    if (pR6374ExtInParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdPktCount: Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    /* Allocate Memory for R6374 OUT Structure */
    pR6374ExtOutParams = (tR6374ExtOutParams *) MemAllocMemBlk
        (RFC6374_OUTPUTPARAMS_POOLID);
    if (pR6374ExtOutParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdPktCount: " "Mempool allocation failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (pR6374ExtInParams, RFC6374_INIT_VAL,
                    sizeof (tR6374ExtInParams));
    RFC6374_MEMSET (pR6374ExtOutParams, RFC6374_INIT_VAL,
                    sizeof (tR6374ExtOutParams));
    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    u4ContextId = BFD_DEFAULT_CONTEXT_ID;

    if (R6374GetBfdSessIndex (pServiceInfo, pR6374ExtInParams,
                              pR6374ExtOutParams, u4ContextId,
                              &u4SessionIndex) != RFC6374_SUCCESS)
    {
        /* Raise Trap As BFD is Down */
        R6374LmNotify.u1TrapType = RFC6374_RES_UNAVAIL_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdPktCount: " "Get Session Index\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return RFC6374_FAILURE;
    }

    /*Get Count from Session Index */
    pR6374ExtInParams->eExtReqType = RFC6374_REQ_BFD_GET_TXRX_FROM_SESS_INDEX;
    pR6374ExtInParams->u4ContextId = u4ContextId;

    pR6374ExtInParams->InBfdReqParams.u4ReqType = BFD_GET_TXRX_FROM_SESS_INDEX;
    pR6374ExtInParams->InBfdReqParams.unReqInfo.
        BfdR6374Info.u4BfdSessionIndex = u4SessionIndex;

    i4RetVal = R6374PortHandleExtInteraction (pR6374ExtInParams,
                                              pR6374ExtOutParams);

    if (i4RetVal == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_MGMT_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdPktCount: R6374PortHandleExtInteraction failed\r\n");
    }

    /*At a time only one value is needed */
    /*Intial LMM and LMR NEED tx So Get TX */
    if (*pu4Tx == RFC6374_INIT_VAL)
    {
        *pu4Tx = pR6374ExtOutParams->BfdRespParams.
            BfdR6374Info.u4BfdSessPerfCtrlPktOut;
    }
    /*LMM reply need only Rx */
    else
    {
        *pu4Rx = pR6374ExtOutParams->BfdRespParams.
            BfdR6374Info.u4BfdSessPerfCtrlPktIn;

    }

    RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
    RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtOutParams);
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374GetBfdTrafficClass
 *
 * Description        : This routine is used to get the BFD TrafficClass/EXP
 *                      value based on BFD Seesion Index
 *
 * Input(s)           : pServiceInfo - Service Related Config
 *
 * Output(s)          : pu4TraffciClass - BFD TrafficClass/EXP Value
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
INT4
R6374GetBfdTrafficClass (tR6374ServiceConfigTableEntry * pServiceInfo,
                         UINT4 *pu4TraffciClass)
{
    tR6374ExtInParams  *pR6374ExtInParams;
    tR6374ExtOutParams *pR6374ExtOutParams;
    tR6374NotifyInfo    R6374LmNotify;
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT4               u4SessionIndex = RFC6374_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;

    /* Allocate Memory for R6374 IN Structure */
    pR6374ExtInParams = (tR6374ExtInParams *) MemAllocMemBlk
        (RFC6374_INPUTPARAMS_POOLID);
    if (pR6374ExtInParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdTrafficClass: "
                     "Mempool allocation failed\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    /* Allocate Memory for R6374 OUT Structure */
    pR6374ExtOutParams = (tR6374ExtOutParams *) MemAllocMemBlk
        (RFC6374_OUTPUTPARAMS_POOLID);

    if (pR6374ExtOutParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdTrafficClass: "
                     "Mempool allocation failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (pR6374ExtInParams, RFC6374_INIT_VAL,
                    sizeof (tR6374ExtInParams));
    RFC6374_MEMSET (pR6374ExtOutParams, RFC6374_INIT_VAL,
                    sizeof (tR6374ExtOutParams));
    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    u4ContextId = BFD_DEFAULT_CONTEXT_ID;

    if (R6374GetBfdSessIndex (pServiceInfo, pR6374ExtInParams,
                              pR6374ExtOutParams, u4ContextId,
                              &u4SessionIndex) != RFC6374_SUCCESS)
    {
        /* Raise Trap As BFD is Down */
        R6374LmNotify.u1TrapType = RFC6374_RES_UNAVAIL_TRAP;
        RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmNotify);

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdTrafficClass: Get Session Index Failed\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtInParams);
        RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                                (UINT1 *) pR6374ExtOutParams);
        return RFC6374_FAILURE;
    }

    /*Get Traffic from Session Index */
    pR6374ExtInParams->eExtReqType = RFC6374_REQ_BFD_GET_EXP_FROM_SESS_INDEX;
    pR6374ExtInParams->u4ContextId = u4ContextId;

    pR6374ExtInParams->InBfdReqParams.u4ReqType = BFD_GET_EXP_FROM_SESS_INDEX;
    pR6374ExtInParams->InBfdReqParams.unReqInfo.
        BfdR6374Info.u4BfdSessionIndex = u4SessionIndex;

    i4RetVal = R6374PortHandleExtInteraction (pR6374ExtInParams,
                                              pR6374ExtOutParams);

    if (i4RetVal == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_MGMT_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374GetBfdTrafficClass: R6374PortHandleExtInteraction failed\r\n");
    }

    *pu4TraffciClass = pR6374ExtOutParams->BfdRespParams.
        BfdR6374Info.u4BfdEXPValue;

    RFC6374_FREE_MEM_BLOCK (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
    RFC6374_FREE_MEM_BLOCK (RFC6374_OUTPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtOutParams);
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name      : R6374GetBfdSessIndex
 *
 * Description        : This routine is used to Get BFD Session Index based
 *                      MPLS Path  Params configured at BFD.
 *
 * Input(s)           : pServiceInfo - Serivce Related Config,
 *                      pR6374ExtInParams - R6374 IN Params
 *                      pR6374ExtOutParams - R6374 Out Params
 *                      u4ContextId - Context ID
 *
 * Output(s)          : pu4SessionIndex - BFD Session Index:
 *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE
 *****************************************************************************/
INT4
R6374GetBfdSessIndex (tR6374ServiceConfigTableEntry * pServiceInfo,
                      tR6374ExtInParams * pR6374ExtInParams,
                      tR6374ExtOutParams * pR6374ExtOutParams,
                      UINT4 u4ContextId, UINT4 *pu4SessionIndex)
{
    INT4                i4RetVal = RFC6374_SUCCESS;

    if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
        RFC6374_MPLS_PATH_TYPE_PW)
    {
        pR6374ExtInParams->eExtReqType =
            RFC6374_REQ_BFD_GET_SESS_INDEX_FROM_PW_INFO;
        pR6374ExtInParams->u4ContextId = u4ContextId;

        pR6374ExtInParams->InBfdReqParams.u4ReqType =
            BFD_GET_SESS_INDEX_FROM_PW_INFO;
        pR6374ExtInParams->InBfdReqParams.unReqInfo.BfdR6374Info.PathId.
            SessPwParams.PeerAddr.u4_addr[0] =
            pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr;
        pR6374ExtInParams->InBfdReqParams.unReqInfo.BfdR6374Info.PathId.
            SessPwParams.u4VcId =
            pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId;

        RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374GetBfdSessIndex: Fetch Session "
                      "Index for Path Type PW, IpAddr : %x Id: %d \r\n",
                      pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr,
                      pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId);

        i4RetVal = R6374PortHandleExtInteraction (pR6374ExtInParams,
                                                  pR6374ExtOutParams);
        if (i4RetVal == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374GetBfdSessIndex: Fetch Session "
                         "Index failed from PW\r\n");
            return i4RetVal;
        }
    }
    else
    {
        pR6374ExtInParams->eExtReqType =
            RFC6374_REQ_BFD_GET_SESS_INDEX_FROM_LSP_INFO;
        pR6374ExtInParams->u4ContextId = u4ContextId;

        pR6374ExtInParams->InBfdReqParams.u4ReqType =
            BFD_GET_SESS_INDEX_FROM_LSP_INFO;
        pR6374ExtInParams->InBfdReqParams.unReqInfo.BfdR6374Info.PathId.
            SessTeParams.SrcIpAddr.u4_addr[0] =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;
        pR6374ExtInParams->InBfdReqParams.unReqInfo.BfdR6374Info.PathId.
            SessTeParams.DstIpAddr.u4_addr[0] =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
        pR6374ExtInParams->InBfdReqParams.unReqInfo.BfdR6374Info.PathId.
            SessTeParams.u4TunnelId =
            pServiceInfo->R6374MplsParams.LspMplsPathParams.u4FrwdTunnelId;

        RFC6374_TRC3 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374GetBfdSessIndex: Fetch Session "
                      "Index for Path Type Tunnel, Src IpAddr : %x "
                      "Fwd Id: %d Dest Ip : %x \r\n",
                      pServiceInfo->R6374MplsParams.LspMplsPathParams.
                      u4SrcIpAddr,
                      pServiceInfo->R6374MplsParams.LspMplsPathParams.
                      u4FrwdTunnelId,
                      pServiceInfo->R6374MplsParams.LspMplsPathParams.
                      u4DestIpAddr);

        i4RetVal = R6374PortHandleExtInteraction (pR6374ExtInParams,
                                                  pR6374ExtOutParams);
        if (i4RetVal == RFC6374_FAILURE)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374GetBfdSessIndex: Fetch Session "
                         "Index failed for LSP\r\n");
            return i4RetVal;
        }
    }

    *pu4SessionIndex = pR6374ExtOutParams->BfdRespParams.
        BfdR6374Info.u4BfdSessionIndex;
    return i4RetVal;

}

/* STUBS */
#ifdef RFC6374STUB_WANTED
PUBLIC INT4
R6374StubGetBfdCounter (UINT4 *pTxCounter, UINT4 *pRxCounter)
{
    static UINT4        su4TxCounterAddition = 16;
    static UINT4        su4RxCounterAddition = 12;
    static UINT4        su4CounterMultipilier = 1;

    if (*pTxCounter == RFC6374_INIT_VAL)
    {
        *pTxCounter = su4TxCounterAddition * su4CounterMultipilier;
        su4TxCounterAddition++;
        su4CounterMultipilier++;
    }
    else
    {
        *pRxCounter = su4RxCounterAddition * su4CounterMultipilier;
        su4RxCounterAddition++;
        su4CounterMultipilier++;
    }
    return RFC6374_SUCCESS;
}
#endif
#endif /*__R6374BFD_C__*/
