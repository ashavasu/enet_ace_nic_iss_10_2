/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374task.c,v 1.1 2016/07/06 10:51:57 siva Exp $
 *
 * Description:This file contains procedures related to
 *             R6374 - Task Initialization
 *******************************************************************/
#ifndef __R6374TASK_C__
#define __R6374TASK_C__
#include "r6374inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : R6374TaskSpawn()                                           */
/*                                                                           */
/* Description  : This procedure is provided to Spawn R6374 Task             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
R6374TaskSpawn (INT1 *pu1Dummy)
{
    UNUSED_PARAM (pu1Dummy);

    /* task initializations */
    if (R6374TaskInit () == RFC6374_FAILURE)
    {
        R6374TaskDeInit ();
        lrInitComplete (RFC6374_FAILURE);
        return;
    }

    /* Register the protocol MIB with SNMP */
    RegisterFS6374 ();
    
    if (R6374MplsRegCallBack () != RFC6374_SUCCESS)
    {
        R6374TaskDeInit ();
        lrInitComplete (RFC6374_FAILURE);
        return;
    }

    lrInitComplete (RFC6374_SUCCESS);

    R6374TaskMain ();

    return;
}
#endif
/*------------------------------------------------------------------------*/
/*                        End of the file  r6374task.c                     */
/*------------------------------------------------------------------------*/
