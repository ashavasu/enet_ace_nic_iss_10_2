
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374api.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description : This file contains the entry function of the RFC6374 module
 *               which will be called by the extrenal modules
 *               messages queued
 *****************************************************************************/
#ifndef __R6374API_C__
#define __R6374API_C__
#include "r6374inc.h"

/****************************************************************************
*                                                                           *
* Function     : R6374ApiHandleExtRequest                                    *
*                                                                           *
* Description  : This function is to intimate the LSP Ping module to        *
*                perform the necesary action needed by the external modules.*
*                                                                           *
* Input        : pR6374ReqParams - Pointer to the input structure with the   *
*                necessary input parameters filled.                         *
*                                                                           *
* Output       : pR6374RespParams - It contain the error code to inform the  *
*                external module about the failures if any.                 *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
INT4
R6374ApiHandleExtRequest (tR6374ReqParams * pR6374ReqParams,
                          tR6374RespParams * pR6374RespParams)
{
    tR6374QMsg         *pR6374QMsg = NULL;

    /* Allocate memory for queue message. */
    if ((pR6374QMsg = (tR6374QMsg *)
         MemAllocMemBlk (RFC6374_QMSG_POOL ())) == NULL)
    {
        RFC6374_TRC (RFC6374_OS_RESOURCE_TRC,
                     pR6374ReqParams->u4ContextId,
                     "\r\nR6374ApiHandleExtRequest\r\n");

        pR6374RespParams->u1ErrorCode = RFC6374_QMSG_MEM_ALLOC_FAILED;
        return OSIX_FAILURE;
    }

    MEMSET (pR6374QMsg, 0, sizeof (tR6374QMsg));

    pR6374QMsg->u1MsgType = pR6374ReqParams->u1MsgType;
    pR6374QMsg->u4ContextId = pR6374ReqParams->u4ContextId;

    switch (pR6374QMsg->u1MsgType)
    {
        case RFC6374_CREATE_CONTEXT_MSG:
        case RFC6374_DELETE_CONTEXT_MSG:
            break;

        case RFC6374_RX_PDU_MSG:

            pR6374QMsg->pR6374Buf =
                pR6374ReqParams->unMsgParam.R6374RxPduInfo.pBuf;

            pR6374QMsg->u4IfIndex =
                pR6374ReqParams->unMsgParam.R6374RxPduInfo.u4IfIndex;

            break;
        case RFC6374_GET_SESSION_STATS:
            /* Input : 
             *       u1MsgType : RFC6374_GET_SESSION_STATS
             *       u4ContextId : 0
             *          
             *       R6374ServiceStats   
             *       au1ServiceName : Service Name
             *       u1SessionStatsType : RFC6374_SESSION_TYPE_LOSS/
             *                            RFC6374_SESSION_TYPE_DELAY
             *
             * Output : 
             *       Last Session Stats for LM/DM
             *       R6374DmStatsTableEntry : Last Delay Session Stats
             *       R6374LmStatsTableEntry : last Loss Session Stats
             * */

            if ((R6374UtilGetSessionStats (pR6374ReqParams, pR6374RespParams)
                 != RFC6374_SUCCESS))
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374ApiHandleExtRequest: "
                             "Get Stats failed\r\n");
                /* Release the Queue Message allocated 
                 * */
                if (RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (),
                                            (UINT1 *) pR6374QMsg) ==
                    MEM_FAILURE)
                {
                    RFC6374_TRC1 (RFC6374_OS_RESOURCE_TRC,
                                  pR6374ReqParams->u4ContextId,
                                  "R6374ApiHandleExtRequest",
                                  RFC6374_QUEUE_NAME);
                }
                return RFC6374_FAILURE;
            }
            break;

        default:
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                         pR6374ReqParams->u4ContextId,
                         "R6374ApiHandleExtRequest");
            pR6374RespParams->u1ErrorCode = RFC6374_MSG_TYPE_INVALID;

            /* Release the Queue Message allocated since the message type 
             * is invalid.
             * */
            if (RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (),
                                        (UINT1 *) pR6374QMsg) == MEM_FAILURE)
            {
                RFC6374_TRC1 (RFC6374_OS_RESOURCE_TRC,
                              pR6374ReqParams->u4ContextId,
                              "R6374ApiHandleExtRequest", RFC6374_QUEUE_NAME);
            }
            return OSIX_FAILURE;
    }

    if (R6374QueEnqMsg (pR6374QMsg) != OSIX_SUCCESS)
    {
        RFC6374_TRC (RFC6374_OS_RESOURCE_TRC,
                     pR6374ReqParams->u4ContextId, "R6374ApiHandleExtRequest");

        /* Release the received packet buffer when the enqueue is failed. */
        if (pR6374QMsg->u1MsgType == RFC6374_RX_PDU_MSG)
        {
            RFC6374_RELEASE_CRU_BUF (pR6374QMsg->pR6374Buf, FALSE);
        }

        if (RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (),
                                    (UINT1 *) pR6374QMsg) == MEM_FAILURE)
        {
            RFC6374_TRC1 (RFC6374_OS_RESOURCE_TRC,
                          pR6374ReqParams->u4ContextId,
                          "R6374ApiHandleExtRequest", RFC6374_QUEUE_NAME);
        }
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : R6374ApiVcmCallback                                        *
*                                                                           *
* Description  : This is the call back function registered with Module.     *
*                This call back will get called when a context is created   *
*                or deleted.                                                *
*                This is the notification to the RFC6374 module             *
*                to delete or create the context.                           *
*                                                                           *
* Input        : IfIndex- Interface index                                   *
*                u4VcmCxtId - Context Id which context has to be created or *
*                deleted.                                                   *
*                u1Event    - To specify Whether to create or delete a      *
*                             context                                       *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
R6374ApiVcmCallback (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1Event)
{
    tR6374ReqParams     R6374ReqParams;
    tR6374RespParams    R6374RespParams;
    UNUSED_PARAM (u4IpIfIndex);

    MEMSET (&R6374ReqParams, 0, sizeof (tR6374ReqParams));
    MEMSET (&R6374RespParams, 0, sizeof (tR6374RespParams));

    if (u1Event & VCM_CONTEXT_CREATE)
    {
        R6374ReqParams.u1MsgType = RFC6374_CREATE_CONTEXT_MSG;
        R6374ReqParams.u4ContextId = u4VcmCxtId;
    }
    else if (u1Event & VCM_CONTEXT_DELETE)
    {
        R6374ReqParams.u1MsgType = RFC6374_DELETE_CONTEXT_MSG;
        R6374ReqParams.u4ContextId = u4VcmCxtId;
    }
    else
    {
        return;
    }

    if (R6374ApiHandleExtRequest (&R6374ReqParams, &R6374RespParams) !=
        OSIX_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_INVALID_CONTEXT,
                     "R6374ApiVcmCallback");
    }
    return;
}

/*****************************************************************************
 * Function Name      : R6374ApiMplsCallBack                                 *
 *                                                                           *
 * Description        : This is the call back function provided by the       *
 *                      RFC6374. This API needs to be invoked for all the    *
 *                      notifications regarding the MPLS TP network. This    *
 *                      needs to be invoked during the following conditions. *
 *                      (1) RFC6374 PDU received over MPLS TP networks. The  *
 *                          pBuf, Opcode and u2Event will be the fields of   *
 *                          interest.                                        *
 *                          RFC6374_OPCODE_CCM      1  - OpCode for CCM PDUs *
 *                          RFC6374_OPCODE_LBR      2  - OpCode for LBR PDUs *
 *                          RFC6374_OPCODE_LBM      3  - OpCode for LBM PDUs *
 *                      (2) LSP/PW Path status changes. Only the PathId and  *
 *                          u2Event will be the fields of interest.          *
 *                                                                           *
 * Input(s)           : pMplsEventNotif - Pointer to the structure consisting*
 *                                        of information that needs to be    *
 *                                        notified to the RFC6374 module.    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 ****************************************************************************/
PUBLIC INT4
R6374ApiMplsCallBack (UINT4 u4ModId, VOID *pMplsEventNotif)
{
    tR6374QMsg         *pMsg = NULL;
    tMplsEventNotif    *pMplsEvent = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2RxChnType = 0;

    pMplsEvent = (tMplsEventNotif *) pMplsEventNotif;
    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_FN_ENTRY_TRC,
                 pMplsEvent->u4ContextId,
                 "R6374ApiMplsCallBack: RFC6374 PDU or LSP/PW Path status change"
                 " message received from MPLS TP networks \r\n");

    if (u4ModId != MPLS_RFC6374_APP_ID)
    {
        RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_CONTROL_PLANE_TRC |
                     RFC6374_ALL_FAILURE_TRC,
                     pMplsEvent->u4ContextId,
                     "R6374ApiMplsCallBack: RFC6374 MODULE - "
                     "Invalid Module Id \r\n");
        if (pMplsEvent->pBuf != NULL)
        {
            RFC6374_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
        }
        return RFC6374_FAILURE;
    }

    /* RFC6374 module expects only two events from MPLS currently (For more
     * information, please refer function description). Decode the message
     * type and post the event/message appropriately
     */
    switch (pMplsEvent->u2Event)
    {
            /* Based on the Opcode, packet is queued to the CC (for CCM) or
             * LBLT Task(for LBM/LBR) task. 
             */
        case MPLS_RFC6374_PACKET:
            /* Determine the RFC6374 header offset by calculating the MPLS 
             * header size 
             */
            /* Move the offset to point to the channel type in ACH header field */
            u4Offset = u4Offset + RFC6374_MPLS_LABEL_LEN +
                RFC6374_ACH_HEADER_LEN + RFC6374_ACH_HEADER_VER_RSVD_LEN;
            /* Get Opcode from Received PDU */
            RFC6374_CRU_GET_2_BYTE (pMplsEvent->pBuf, u4Offset, u2RxChnType);

            switch (u2RxChnType)
            {
                case RFC6374_CHANNEL_TYPE_DIRECT_LM:
                case RFC6374_CHANNEL_TYPE_INFERED_LM:
                case RFC6374_CHANNEL_TYPE_DM:
                    pMsg = (tR6374QMsg *) MemAllocMemBlk (RFC6374_QMSG_POOL ());
                    if (pMsg == NULL)
                    {
                        RFC6374_TRC (RFC6374_INIT_SHUT_TRC |
                                     RFC6374_ALL_FAILURE_TRC |
                                     RFC6374_OS_RESOURCE_TRC |
                                     RFC6374_CONTROL_PLANE_TRC,
                                     pMplsEvent->u4ContextId,
                                     "R6374ApiMplsCallBack: Unable to allocate "
                                     "Memory for RFC6374 PDU Received "
                                     "Q Message!!!!!!!!!!!!\r\n");
                        RFC6374_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
                        return RFC6374_FAILURE;
                    }

                    RFC6374_MEMSET (pMsg, RFC6374_INIT_VAL,
                                    sizeof (tR6374QMsg));
                    pMsg->u1MsgType = (tR6374MsgType) RFC6374_MPLS_PDU_IN_QUEUE;
                    pMsg->u4IfIndex = pMplsEvent->u4InIfIndex;
                    pMsg->u4ContextId = pMplsEvent->u4ContextId;
                    pMsg->pR6374Buf = pMplsEvent->pBuf;

                    /* Sending message and Event to own task */
                    if (R6374QueEnqMsg (pMsg) != RFC6374_SUCCESS)
                    {
                        RFC6374_TRC (RFC6374_INIT_SHUT_TRC |
                                     RFC6374_ALL_FAILURE_TRC,
                                     pMplsEvent->u4ContextId,
                                     "R6374ApiMplsCallBack: Posting to Message"
                                     "Queue & sending Event to task  FAILED"
                                     "!!! \r\n");
                        RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (),
                                                (UINT1 *) pMsg);
                        return RFC6374_FAILURE;
                    }

                    break;
                default:
                    RFC6374_TRC (RFC6374_INIT_SHUT_TRC |
                                 RFC6374_ALL_FAILURE_TRC,
                                 pMplsEvent->u4ContextId,
                                 "R6374ApiMplsCallBack: Invalid Opcode!!! \r\n");
                    return RFC6374_FAILURE;
            }                    /* end of switch - Opcode */

            break;
        case MPLS_TNL_UP_EVENT:
        case MPLS_TNL_DOWN_EVENT:
        case MPLS_PW_UP_EVENT:
        case MPLS_PW_DOWN_EVENT:
            /* Allocating MEM Block for the Message */
            pMsg = (tR6374QMsg *) MemAllocMemBlk (RFC6374_QMSG_POOL ());
            if (pMsg == NULL)
            {
                RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_ALL_FAILURE_TRC |
                             RFC6374_OS_RESOURCE_TRC |
                             RFC6374_CONTROL_PLANE_TRC, pMplsEvent->u4ContextId,
                             "R6374ApiMplsCallBack: Unable to allocate "
                             "Memory for Q Message!!!!!!!!!!!!\r\n");
                RFC6374_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                RFC6374_INCR_MEMORY_FAILURE_COUNT ();
                return RFC6374_FAILURE;
            }

            RFC6374_MEMSET (pMsg, RFC6374_INIT_VAL, sizeof (tR6374QMsg));
            pMsg->u1MsgType = (tR6374MsgType) RFC6374_MPLS_PATH_STATUS_CHG;
            pMsg->u4IfIndex = pMplsEvent->u4InIfIndex;
            pMsg->u4ContextId = pMplsEvent->u4ContextId;

            if ((pMplsEvent->u2Event == MPLS_TNL_UP_EVENT) ||
                (pMplsEvent->u2Event == MPLS_PW_UP_EVENT))
            {
                pMsg->u1MplsTnlPwOperState = RFC6374_MPLS_PATH_UP;
            }
            else if ((pMplsEvent->u2Event == MPLS_TNL_DOWN_EVENT) ||
                     (pMplsEvent->u2Event == MPLS_PW_DOWN_EVENT))
            {
                pMsg->u1MplsTnlPwOperState = RFC6374_MPLS_PATH_DOWN;
            }
            if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_TUNNEL)
            {
                pMsg->R6374MplsParams.u1MplsPathType = MPLS_PATH_TYPE_TUNNEL;
                pMsg->R6374MplsParams.LspMplsPathParams.
                    u4FrwdTunnelId = pMplsEvent->PathId.TnlId.u4SrcTnlNum;
                /* pMsg->R6374MplsParams.LspMplsPathParams.
                   u4LspId = pMplsEvent->PathId.TnlId.u4LspNum; */
                pMsg->R6374MplsParams.LspMplsPathParams.
                    u4SrcIpAddr =
                    pMplsEvent->PathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0];
                pMsg->R6374MplsParams.LspMplsPathParams.
                    u4DestIpAddr =
                    pMplsEvent->PathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0];
            }
            else if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_PW)
            {
                pMsg->R6374MplsParams.u1MplsPathType = MPLS_PATH_TYPE_PW;
                pMsg->R6374MplsParams.PwMplsPathParams.u4PwId =
                    pMplsEvent->PathId.unPathId.MplsPwId.u4VcId;
            }
            /* Sending Message and Event to own tasks */
            if (R6374QueEnqMsg (pMsg) != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             pMplsEvent->u4ContextId,
                             "R6374ApiMplsCallBack: Unable to allocate "
                             "Memory for Q Message!!!!!!!!!!!!\r\n");
                RFC6374_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
                RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (), (UINT1 *) pMsg);
                return RFC6374_FAILURE;
            }
            break;
        default:
            /* Unexpected event occured */
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC,
                         pMplsEvent->u4ContextId,
                         "R6374ApiMplsCallBack: Invalid event !!! \r\n");
            RFC6374_RELEASE_CRU_BUF (pMplsEvent->pBuf, FALSE);
            return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file r6374que.c                      */
/*-----------------------------------------------------------------------*/
