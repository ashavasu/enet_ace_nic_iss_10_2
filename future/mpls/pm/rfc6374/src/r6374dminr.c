/********************************************************************
 * Copyright (C) 2016 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374dminr.c,v 1.3 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the Functionality of the 1 way and 2
 *              way Delay Mesaurement of Control Sub Module.
 *******************************************************************/
#ifndef __R6374DMINR_C__
#define __R6374DMINR_C__

#include "r6374inc.h"
PRIVATE INT4 R6374DmCalcDiffInTimeRep PROTO ((tR6374TSRepresentation *,
                                              tR6374TSRepresentation *,
                                              tR6374TSRepresentation *));
/*******************************************************************************
 * Function           : R6374ProcessDmr
 *
 * Description        : This routine processes the received DMR PDU.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the
 *                      information regarding mp info, the PDU if received and
 *                      other information related to the functionality.
 *                      pu1Pdu - Pointer to pointer to the pu1Pdu.
 *
 * Output(s)          : None.
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374ProcessDmr (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374RxDmrPktInfo *pDmrPktInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374NotifyInfo    R6374DmNotify;
    UINT4               u4RxDmmVersion = RFC6374_INIT_VAL;
    UINT4               u4RxDmrSessionId = RFC6374_INIT_VAL;
    UINT4               u4SessQueryInt = RFC6374_INIT_VAL;
    UINT1               u1RxControlCode = RFC6374_INIT_VAL;
    UINT1               u1QTF = RFC6374_INIT_VAL;
    UINT1               u1ResTF = RFC6374_INIT_VAL;
    UINT1               u1ResPrefTF = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1               u1TlvType = RFC6374_INIT_VAL;
    UINT1               u1TlvLength = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&R6374DmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374ProcessDmr()\r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    if (pDmInfo->u1DmStatus != RFC6374_TX_STATUS_NOT_READY)
    {
        return RFC6374_FAILURE;
    }
    /* Increment DMR In Count */
    RFC6374_INCR_STATS_DMR_IN (pServiceInfo);
    /* Decrement DM Tx count when response is received for
     * Transmitted PDU */

    if (pDmInfo->u1DmType == RFC6374_DM_TYPE_TWO_WAY)
    {
        RFC6374_DM_RESET_DM_NO_RESP_TX_COUNT (pDmInfo);
    }

    pDmrPktInfo = &(pPktSmInfo->uPktInfo.Dmr);
    /*pu1Pdu = pu1Pdu + RFC6374_PDU_VERSION; */

    /* Get version from the pdu */
    RFC6374_GET_1BYTE (u4RxDmmVersion, pu1Pdu);
    u4RxDmmVersion = u4RxDmmVersion >> 4;
    if (u4RxDmmVersion != RFC6374_PDU_VERSION)
    {
        /* Unsupported Version in DMR */
        R6374DmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374DmNotify);
        RFC6374_DM_INCR_ERR_PKT (pDmInfo);
        return RFC6374_FAILURE;
    }

    /* Get control code */
    RFC6374_GET_1BYTE (u1RxControlCode, pu1Pdu);

    /* Move the pointer to get RTF and RPTF field */
    RFC6374_GET_2BYTE (u2PduLength, pu1Pdu);
    /* Get Responder Timestamp Format */
    RFC6374_GET_1BYTE (u1QTF, pu1Pdu);
    u1QTF = (UINT1) (u1QTF << RFC6374_VAL_4);
    u1ResTF = (UINT1) (u1QTF >> RFC6374_VAL_4);

    /* Get Responder Preferred Timestamp Format */
    RFC6374_GET_1BYTE (u1ResPrefTF, pu1Pdu);
    u1ResPrefTF = u1ResPrefTF >> RFC6374_VAL_4;

    if ((u1ResTF != pDmInfo->u1DmQTSFormat) &&
        (u1RxControlCode == RFC6374_CTRL_CODE_DATA_FORMAT_INVALID))
    {
        RFC6374_DM_INCR_ERR_PKT (pDmInfo);

        if ((pServiceInfo->i4Negostatus == RFC6374_NEGOTIATION_ENABLE) &&
            ((u1ResTF & gR6374GlobalInfo.u1SysSuppTimeFormat) ||
             (u1ResPrefTF & gR6374GlobalInfo.u1SysSuppTimeFormat)))
        {
            pDmInfo->u1DmQTSFormat = u1ResTF;
            pServiceInfo->u1NegoSuppTsFormat = u1ResTF;
            /* Timestamp negotiation is enabled and also
             * Responder preferred timestamp or responder timestamp 
             * is supported by sender node 
             * So change the QTF filed and re-initiate the negotiation
             * procedure for the current session and don't do
             * any processing. */
            return RFC6374_SUCCESS;
        }
        else
        {
            /* Invalid TF */
            R6374DmNotify.u1TrapType = RFC6374_UNSUP_TIMESTAMP_TRAP;
            RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                             pServiceInfo->au1ServiceName,
                             RFC6374_STRLEN (pServiceInfo->au1ServiceName));
            R6374SendNotification (&R6374DmNotify);
            return RFC6374_FAILURE;
        }
    }
    if ((u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_VERSION) ||
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_CTRL_CODE) ||
        ((u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL) &&
         (pServiceInfo->u1SessIntQueryStatus !=
          CLI_RFC6374_SESS_INT_QUERY_ENABLE)))
    {
        /* Control Code from Responder */
        if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_VERSION)
        {
            /* Invalid Version Control code */
            R6374DmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_CTRL_CODE)
        {
            /* Invalid Control Code */
            R6374DmNotify.u1TrapType = RFC6374_UNSUP_CC_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL)
        {
            /* Unsupported Query Interval */
            /* In case Session nterval Query is disabled in Querier side, and
             * Unsupported Query Interval is got from Responder, abort the session*/
            R6374DmNotify.u1TrapType = RFC6374_UNSUPP_QUERY_INTERVAL;
        }

        RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374DmNotify);
        RFC6374_DM_INCR_ERR_PKT (pDmInfo);
        return RFC6374_FAILURE;
    }

    /* Move the pointer to get session id */
    pu1Pdu = pu1Pdu + 2;
    RFC6374_GET_4BYTE (u4RxDmrSessionId, pu1Pdu);

    if (u4RxDmrSessionId != pDmInfo->u4DmSessionId)
    {
        /* Wrong Session ID in DMR */
        R6374DmNotify.u1TrapType = RFC6374_CONN_MISMATCH_TRAP;
        RFC6374_STRNCPY (&R6374DmNotify.au1ServiceName,
                         pServiceInfo->au1ServiceName,
                         RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374DmNotify);
        RFC6374_DM_INCR_ERR_PKT (pDmInfo);
        return RFC6374_FAILURE;
    }

    /* Copy the TxTimeStampf */
    RFC6374_GET_4BYTE (pDmrPktInfo->TxTimeStampb.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pDmrPktInfo->TxTimeStampb.u4NanoSeconds, pu1Pdu);

    /* Copy the RxTimeStampf */
    RFC6374_GET_4BYTE (pDmrPktInfo->RxTimeStampb.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pDmrPktInfo->RxTimeStampb.u4NanoSeconds, pu1Pdu);

    /* Copy the TxTimeStampb */
    RFC6374_GET_4BYTE (pDmrPktInfo->TxTimeStampf.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pDmrPktInfo->TxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Copy the Locally filled RxTimeStampb */
    RFC6374_GET_4BYTE (pDmrPktInfo->RxTimeStampf.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pDmrPktInfo->RxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* Get TLV . Check for Reception interval and adjust accordingly */
    if (((pDmInfo->u1SessQueryIntervalState == RFC6374_SQI_INIT_STATE) &&
         (u2PduLength > RFC6374_DM_PDU_MSG_LEN)) ||
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL))
    {
        RFC6374_GET_1BYTE (u1TlvType, pu1Pdu);
        RFC6374_GET_1BYTE (u1TlvLength, pu1Pdu);
        RFC6374_GET_4BYTE (u4SessQueryInt, pu1Pdu);
        /* Call function to adjust the session query interval */
        R6374UtilAdjustSessionQueryInterval (pServiceInfo, u4SessQueryInt,
                                             RFC6374_TEST_MODE_DM);
    }
    else if (pDmInfo->u1SessQueryIntervalState == RFC6374_SQI_ADJUSTED_STATE)
    {
        /* If reply without TLV, when Querier is in Adjusted state, it means that
         * the responder has accepted the adjustment */
        pDmInfo->u1SessQueryIntervalState = RFC6374_SQI_RESET_STATE;
    }

    if (R6374CalcFrameDelay (pPktSmInfo, pDmrPktInfo,
                             RFC6374_DM_TYPE_TWO_WAY) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, " R6374ProcessDmr: "
                     "calculation for frame delay return failure\r\n");
        return RFC6374_FAILURE;
    }

    UNUSED_PARAM (u1TlvLength);
    UNUSED_PARAM (u1TlvType);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from  R6374ProcessDmr()\r\n");

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function           : R6374Process1Dm
 *
 * Description        : This routine processes the received 1DM PDU.
 *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the
 *                      information regarding mp info, the PDU if received and
 *                      other information related to the functionality.
 *                      pu1Pkt - Pointer to pointer to the pu1Pkt.
 *
 * Output(s)          : None.
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374Process1Dm (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pkt)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374Rx1DmPktInfo *p1DmPktInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    UINT4               u4RxDmSessionId = RFC6374_INIT_VAL;
    UINT1               u1RxQueryTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering R6374Process1Dm()\r\n");

    pServiceInfo = pPktSmInfo->pServiceConfigInfo;
    p1DmPktInfo = &(pPktSmInfo->uPktInfo.OneDm);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    RFC6374_MEMSET (p1DmPktInfo, RFC6374_INIT_VAL, sizeof (tR6374Rx1DmPktInfo));

    if (pServiceInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374Process1Dm: "
                     "ServiceInfo is not found, discarding the received"
                     " 1DM frame\r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_INCR_STATS_1DM_IN (pPktSmInfo->pServiceConfigInfo);

    /* QTF Validation */
    /* version + control code + Message (2Byte) */
    pu1Pkt = pu1Pkt + RFC6374_VAL_4;
    RFC6374_GET_1BYTE (u1RxQueryTF, pu1Pkt);

    /* Timestamp received from querier updated
     * to QTS format which will be used for timestamp
     * format calculation */
    pDmInfo->u1DmQTSFormat = u1RxQueryTF >> RFC6374_VAL_4;

    /* Move to Session Identifier Field */
    pu1Pkt = pu1Pkt + RFC6374_VAL_3;

    /* Get Session Identifier */
    RFC6374_GET_4BYTE (u4RxDmSessionId, pu1Pkt);

    /* Copy the TxTimeStampf */
    RFC6374_GET_4BYTE (p1DmPktInfo->TxTimeStampf.u4Seconds, pu1Pkt);
    RFC6374_GET_4BYTE (p1DmPktInfo->TxTimeStampf.u4NanoSeconds, pu1Pkt);

    /* Copy the RxTimeStampf */
    RFC6374_GET_4BYTE (p1DmPktInfo->RxTimeStampf.u4Seconds, pu1Pkt);
    RFC6374_GET_4BYTE (p1DmPktInfo->RxTimeStampf.u4NanoSeconds, pu1Pkt);

    RFC6374_TRC3 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374Process1Dm: Received session Id : %d Received Tx "
                  "Seconds: %d NanoSeconds: %d \r\n", u4RxDmSessionId,
                  p1DmPktInfo->TxTimeStampf.u4Seconds,
                  p1DmPktInfo->TxTimeStampf.u4NanoSeconds);

    /* Check if RxTimeStampb was filled by the HW on reception */
    if ((p1DmPktInfo->RxTimeStampf.u4Seconds == RFC6374_INIT_VAL) &&
        (p1DmPktInfo->RxTimeStampf.u4NanoSeconds == RFC6374_INIT_VAL))
    {
        R6374UtilGetCurrentTime (&(p1DmPktInfo->RxTimeStampf),
                                 pDmInfo->u1DmQTSFormat);
    }
    if (R6374Calc1FrameDelay (pPktSmInfo, p1DmPktInfo,
                              RFC6374_DM_TYPE_ONE_WAY,
                              u4RxDmSessionId) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374Process1Dm: "
                     "Failure occured in 1 way frame delay calculation \r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374Process1Dm()\r\n");

    return RFC6374_SUCCESS;

}

/*******************************************************************************
 * Function           : R6374DmCalcDiffInTimeRep
 *
 * Description        : This routine is used to get the time difference between
 *                      two timestams.
 *
 * Input(s)           : pTimeOp1 - Pointer to TimeStamp operand one
 *                      pTimeOp2 -Pointer to  TimeStamp operand two
 *
 * Output(s)          : pTimeResult - Pointer to the resultant timestamp
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PRIVATE INT4
R6374DmCalcDiffInTimeRep (tR6374TSRepresentation * pTimeOp1,
                          tR6374TSRepresentation * pTimeOp2,
                          tR6374TSRepresentation * pTimeResult)
{
    INT4                i4NanoSeconds = RFC6374_INIT_VAL;
    UINT4               u4StoredTimeResult = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmCalcDiffInTimeRep()\r\n");

    /* Check for invalid timestamp values */
    if (pTimeOp1->u4Seconds < pTimeOp2->u4Seconds)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374DmCalcDiffInTimeRep: "
                     " invalid seconds value in operands\r\n");
        return RFC6374_FAILURE;
    }

    /* Store the Seconds value in the output timestamp to that it can be 
     * reverted back in case of failure*/
    u4StoredTimeResult = pTimeResult->u4Seconds;
    pTimeResult->u4Seconds = pTimeOp1->u4Seconds - pTimeOp2->u4Seconds;
    i4NanoSeconds = (INT4) (pTimeOp1->u4NanoSeconds - pTimeOp2->u4NanoSeconds);

    /* Check if wrap wround has happened in the time stamps */
    if (i4NanoSeconds < RFC6374_INIT_VAL)
    {
        /* Check if we have enough seconds to take the offset */
        if (pTimeResult->u4Seconds == RFC6374_INIT_VAL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmCalcDiffInTimeRep: "
                         " invalid seconds value in operands\r\n");
            /* Revert back the no of seconds the the output timestamp */
            pTimeResult->u4Seconds = u4StoredTimeResult;
            return RFC6374_FAILURE;
        }
        /* Decrement One Second from the Seconds field */
        pTimeResult->u4Seconds--;
        pTimeResult->u4NanoSeconds = (UINT4)
            (RFC6374_NUM_OF_NSEC_IN_A_SEC + i4NanoSeconds);
    }
    else
    {
        pTimeResult->u4NanoSeconds = (UINT4) i4NanoSeconds;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmCalcDiffInTimeRep()\r\n");
    return RFC6374_SUCCESS;

}

/*******************************************************************************
 * Function           : R63741DmCalcDiffInTimeRep
 *
 * Description        : This routine is used to get the time difference between
 *                      two timestamps.This routine is used to carry out the
 *                      calculations when the clocks at the Tx and Rx end are
 *                      not in synch.
 *
 * Input(s)           : pTimeOp1 - Pointer to TimeStamp operand one
 *                      pTimeOp2 -Pointer to  TimeStamp operand two
 *
 * Output(s)          : pTimeResult - Pointer to the resultant timestamp
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R63741DmCalcDiffInTimeRep (tR6374TSRepresentation * pTimeOp1,
                           tR6374TSRepresentation * pTimeOp2,
                           tR6374TSRepresentation * pTimeResult)
{
    INT4                i4NanoSeconds = RFC6374_INIT_VAL;
    UINT4               u4StoredTimeResult = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R63741DmCalcDiffInTimeRep()\r\n");

    /* Store the Seconds value in the output timestamp to that it can be
     * reverted back in case of failure*/
    u4StoredTimeResult = pTimeResult->u4Seconds;
    if (pTimeOp1->u4Seconds >= pTimeOp2->u4Seconds)
    {
        pTimeResult->u4Seconds = pTimeOp1->u4Seconds - pTimeOp2->u4Seconds;
        i4NanoSeconds =
            (INT4) (pTimeOp1->u4NanoSeconds - pTimeOp2->u4NanoSeconds);
    }
    else
    {
        pTimeResult->u4Seconds = pTimeOp2->u4Seconds - pTimeOp1->u4Seconds;
        i4NanoSeconds =
            (INT4) (pTimeOp2->u4NanoSeconds - pTimeOp1->u4NanoSeconds);
    }
    /* Check if wrap wround has happened in the time stamps */
    if (i4NanoSeconds < RFC6374_INIT_VAL)
    {
        /* Check if we have enough seconds to take the offset */
        if (pTimeResult->u4Seconds == RFC6374_INIT_VAL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R63741DmCalcDiffInTimeRep: "
                         " invalid seconds value in operands\r\n");
            /* Revert back the no of seconds the the output timestamp */
            pTimeResult->u4Seconds = u4StoredTimeResult;
            return RFC6374_FAILURE;
        }
        /* Decrement One Second from the Seconds field */
        pTimeResult->u4Seconds--;
        pTimeResult->u4NanoSeconds = (UINT4)
            (RFC6374_NUM_OF_NSEC_IN_A_SEC + i4NanoSeconds);
    }
    else
    {
        pTimeResult->u4NanoSeconds = (UINT4) i4NanoSeconds;
    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R63741DmCalcDiffInTimeRep()\r\n");
    return RFC6374_SUCCESS;

}

/*****************************************************************************
 * Function           : R6374DmCalcFrameDelayVariation
 *
 * Description        : This routine calculates the following types of Frame
 *                      Delay Variations:
 *                      1. Frame Delay Variation
 *                      2. Inter Frame Delay Variation
 *
 * Input(s)           : pServiceInfo - Pointer to service info
 *                      pPktDelayBuffNode - Pointer to packet Delay Buffer Node
 *
 * Output(s)          : None
 *
 * Returns            : None
 ******************************************************************************/
PUBLIC VOID
R6374DmCalcFrameDelayVariation (tR6374ServiceConfigTableEntry * pServiceInfo,
                                tR6374DmBufferTableEntry * pCurPktDelayBuffNode)
{
    tR6374DmBufferTableEntry *pPrvPktDelayBuffEntry = NULL;
    tR6374TSRepresentation PacketDelayVariation;
    tR6374DmBufferTableEntry PrevPktDelayBuffNode;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmCalcFrameDelayVariation()\r\n");

    RFC6374_MEMSET (&PacketDelayVariation, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&PrevPktDelayBuffNode, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    PrevPktDelayBuffNode.u4ContextId = pServiceInfo->u4ContextId;

    RFC6374_MEMCPY (PrevPktDelayBuffNode.au1ServiceName,
                    pServiceInfo->au1ServiceName,
                    RFC6374_STRLEN (pServiceInfo->au1ServiceName));

    PrevPktDelayBuffNode.u4SessionId = pCurPktDelayBuffNode->u4SessionId;
    PrevPktDelayBuffNode.u4SeqCount = pCurPktDelayBuffNode->u4SeqCount;
    /* We need to search for an entry which was stored just prior to this node
     * for the same transaction */
    PrevPktDelayBuffNode.u4SeqCount = PrevPktDelayBuffNode.u4SeqCount -
        RFC6374_DECR_VAL;
    pPrvPktDelayBuffEntry =
        (tR6374DmBufferTableEntry *) RBTreeGet (RFC6374_DMBUFFER_TABLE,
                                                (tRBElem *) &
                                                PrevPktDelayBuffNode);
    if (pPrvPktDelayBuffEntry != NULL)
    {
        /* Entry exists for the same transaction ID.
         * Calculate Frame Delay
         */
        if (pPrvPktDelayBuffEntry->PktDelayValue.u4Seconds >
            pCurPktDelayBuffNode->PktDelayValue.u4Seconds)
        {
            R6374DmCalcDiffInTimeRep (&(pPrvPktDelayBuffEntry->PktDelayValue),
                                      &(pCurPktDelayBuffNode->PktDelayValue),
                                      &PacketDelayVariation);
        }
        else if (pPrvPktDelayBuffEntry->PktDelayValue.u4Seconds ==
                 pCurPktDelayBuffNode->PktDelayValue.u4Seconds)
        {
            if (pPrvPktDelayBuffEntry->PktDelayValue.u4NanoSeconds >
                pCurPktDelayBuffNode->PktDelayValue.u4NanoSeconds)
            {

                R6374DmCalcDiffInTimeRep (&(pPrvPktDelayBuffEntry->
                                            PktDelayValue),
                                          &(pCurPktDelayBuffNode->
                                            PktDelayValue),
                                          &PacketDelayVariation);
            }
            else
            {

                R6374DmCalcDiffInTimeRep (&(pCurPktDelayBuffNode->
                                            PktDelayValue),
                                          &(pPrvPktDelayBuffEntry->
                                            PktDelayValue),
                                          &PacketDelayVariation);
            }
        }
        else
        {
            R6374DmCalcDiffInTimeRep (&(pCurPktDelayBuffNode->PktDelayValue),
                                      &(pPrvPktDelayBuffEntry->PktDelayValue),
                                      &PacketDelayVariation);
        }

        RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      " R6374DmCalcFrameDelayVariation: MinPktDelay : "
                      "Seconds : %x NanoSeconds : %x\r\n",
                      PacketDelayVariation.u4Seconds,
                      PacketDelayVariation.u4NanoSeconds);
    }
    else
    {
        /* This is the first node that is getting added in the buffer
         * packet Delay Variation cannot be calculated for this case.
         * Store the delay calculated as the Minimum Delay.
         */
        pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds =
            pCurPktDelayBuffNode->PktDelayValue.u4Seconds;
        pServiceInfo->DmInfo.MinPKtDelayValue.u4NanoSeconds =
            pCurPktDelayBuffNode->PktDelayValue.u4NanoSeconds;

        RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      " R6374DmCalcFrameDelayVariation: First node:"
                      "MinPktDelay : Seconds : %x NanoSeconds : %x\r\n",
                      pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds,
                      pServiceInfo->DmInfo.MinPKtDelayValue.u4NanoSeconds);
        return;
    }

    /* Store the value of Frame Delay Variation */
    RFC6374_COPY_TIME_REPRESENTATION (&(pCurPktDelayBuffNode->
                                        PktDelayVariation),
                                      &PacketDelayVariation);

    /* Calculate  Inter Frame Delay Variation */
    if (pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds >
        pCurPktDelayBuffNode->PktDelayValue.u4Seconds)
    {
        pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds =
            pCurPktDelayBuffNode->PktDelayValue.u4Seconds;
        pServiceInfo->DmInfo.MinPKtDelayValue.u4NanoSeconds =
            pCurPktDelayBuffNode->PktDelayValue.u4NanoSeconds;
    }

    if (pCurPktDelayBuffNode->PktDelayValue.u4Seconds ==
        pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds)
    {
        /* If its Nanoseconds value is also less or equal to the stored minimum
         * then this is the new minimum value. Otherwise the minimum remains the same
         */
        if (pCurPktDelayBuffNode->PktDelayValue.u4NanoSeconds <=
            pServiceInfo->DmInfo.MinPKtDelayValue.u4NanoSeconds)
        {

            pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds =
                pCurPktDelayBuffNode->PktDelayValue.u4Seconds;
            pServiceInfo->DmInfo.MinPKtDelayValue.u4NanoSeconds =
                pCurPktDelayBuffNode->PktDelayValue.u4NanoSeconds;
        }
    }
    R6374DmCalcDiffInTimeRep (&(pCurPktDelayBuffNode->PktDelayValue),
                              &(pServiceInfo->DmInfo.MinPKtDelayValue),
                              &(pCurPktDelayBuffNode->InterPktDelayVariation));

    RFC6374_TRC6 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374DmCalcFrameDelayVariation: Curent Packet : "
                  "Seconds : %x NanoSeconds : %x MinPktDelay Seconds: %x "
                  "Nanoseconds :%x InterPktDelay : Seconds : %x "
                  "NanoSeconds : %x\r\n",
                  pCurPktDelayBuffNode->PktDelayValue.u4Seconds,
                  pCurPktDelayBuffNode->PktDelayValue.u4NanoSeconds,
                  pServiceInfo->DmInfo.MinPKtDelayValue.u4Seconds,
                  pServiceInfo->DmInfo.MinPKtDelayValue.u4NanoSeconds,
                  pCurPktDelayBuffNode->InterPktDelayVariation.u4Seconds,
                  pCurPktDelayBuffNode->InterPktDelayVariation.u4NanoSeconds);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmCalcFrameDelayVariation()\r\n");
    return;
}

/*****************************************************************************
 * Function           : R6374DmAddStats
 *
 * Description        : This Routine is used to Add Statics to Loss Measurement
 *                      stats table.
 *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the
 *                                   Service Info.
 *                      u1DelayMeasurementType - One/Two Way
 *                      pDelayDmBuffNode - Tx&Rx/Far&Near at Sender loss
 *                                        and Rx at Recevier.
 *
 * Output(s)          : None
 *
 * Returns            : RFC6374_SUCCESS/RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374DmAddStats (tR6374PktSmInfo * pPktSmInfo,
                 tR6374DmBufferTableEntry * pDelayDmBuffNode,
                 UINT1 u1DelayMeasurementType)
{

    tR6374DmStatsTableEntry *pDmStatsNodeInfo = NULL;
    tR6374DmStatsTableEntry *pDmStatsNodeTableEntry = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374DmAddStats()\r\n");

    pDmInfo = RFC6374_GET_DM_INFO_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);

    /* Check if Node is already present for Service Name, Session ID */
    pDmStatsNodeInfo = R6374UtilGetDmStatsTableInfo (pServiceInfo,
                                                     pDelayDmBuffNode->
                                                     u4SessionId);

    if (pDmStatsNodeInfo == NULL)
    {
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                      RFC6374_DEFAULT_CONTEXT_ID, " R6374DmAddStats: Getting "
                      "stats failed for session Id : %d Allocate memory to add"
                      " new entry\r\n", pDelayDmBuffNode->u4SessionId);

        /* Add a new Stats Node to table Allocate Memory to New Node */
        pDmStatsNodeTableEntry = (tR6374DmStatsTableEntry *)
            MemAllocMemBlk (RFC6374_DMSTATSTABLE_POOLID);
        if (pDmStatsNodeTableEntry == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374DmAddStats: Allocating Memory"
                         " to Stats table failed\r\n");
            RFC6374_INCR_MEMORY_FAILURE_COUNT ();
            return RFC6374_FAILURE;
        }

        RFC6374_MEMSET (pDmStatsNodeTableEntry, RFC6374_INIT_VAL,
                        sizeof (tR6374DmStatsTableEntry));

        /* Add It Into RB Tree Table */
        pDmStatsNodeTableEntry->u4ContextId = pServiceInfo->u4ContextId;
        pDmStatsNodeTableEntry->u4SessionId = pDelayDmBuffNode->u4SessionId;
        pDmStatsNodeTableEntry->u1TrafficClass = pServiceInfo->u1TrafficClass;

        /* Every new LM/DM/CombLMDM Session initiated would be the latest Session ID */
        pServiceInfo->u4LastDmSessId = pDmStatsNodeTableEntry->u4SessionId;

        /* For Every Combined LMDM Session update its Latest Session ID */
        if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
        {
            pServiceInfo->u4LastLmDmSessId =
                pDmStatsNodeTableEntry->u4SessionId;
        }

        RFC6374_MEMCPY (pDmStatsNodeTableEntry->au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        RFC6374_STRLEN (pServiceInfo->au1ServiceName));

        /* Start Time */
        pDmStatsNodeTableEntry->RxMeasurementStartTime =
            R6374UtilGetTimeFromEpoch ();

        if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_PW)
        {
            /* PW ID (VcId) */
            pDmStatsNodeTableEntry->u4ChannelId1 =
                pServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId;
            /* PW Peer IP Address */
            pDmStatsNodeTableEntry->u4ChannelIpAddr1 =
                pServiceInfo->R6374MplsParams.PwMplsPathParams.u4IpAddr;
        }
        else
        {
            /* LSP Forwards ID */
            pDmStatsNodeTableEntry->u4ChannelId1 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4FrwdTunnelId;
            /* LSP Reverse ID */
            pDmStatsNodeTableEntry->u4ChannelId2 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4RevTunnelId;
            /* LSP Source Address */
            pDmStatsNodeTableEntry->u4ChannelIpAddr1 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4SrcIpAddr;
            /* LSP Destination Address */
            pDmStatsNodeTableEntry->u4ChannelIpAddr2 =
                pServiceInfo->R6374MplsParams.LspMplsPathParams.u4DestIpAddr;
        }
        /* Just Assign the values as it is first node */
        if (u1DelayMeasurementType == RFC6374_DM_TYPE_ONE_WAY)
        {
            /* Delay Type */
            pDmStatsNodeTableEntry->u1DelayType = RFC6374_DM_TYPE_ONE_WAY;
            /* Delay session Type - Independent DM or Combined Lm-Dm */
            pDmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_INDEPENDENT;
            /* Number of DMR Recevied */
            pDmStatsNodeTableEntry->u41DMRcvd = pDmInfo->u4RxDmSeqCount;
            /* updating the timestamp format used for the calculation */
            pDmStatsNodeTableEntry->u1DelayTSFormat = pDmInfo->u1DmQTSFormat;

        }
        /* Just Assign the values as it is first node */
        else if (u1DelayMeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            /* Delay Type */
            pDmStatsNodeTableEntry->u1DelayType = RFC6374_DM_TYPE_ONE_WAY;
            /* Delay session Type - Independent DM or Combined Lm-Dm */
            pDmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_COMBINED;
            /* Number of DMR Recevied */
            pDmStatsNodeTableEntry->u41DMRcvd = pLmDmInfo->u4RxLmDmSeqCount;
            /* updating the timestamp format used for the calculation */
            pDmStatsNodeTableEntry->u1DelayTSFormat =
                pLmDmInfo->u1LmDmQTSFormat;
        }
        else
        {
            /* Delay Type */
            pDmStatsNodeTableEntry->u1DelayType = RFC6374_DM_TYPE_TWO_WAY;
            pDmStatsNodeTableEntry->u1MeasurementOngoing =
                RFC6374_MEASUREMENT_ONGOING;

            /* If Delay was performed through Dyadic */
            if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
            {
                /* Set Dyadic to Enable */
                pDmStatsNodeTableEntry->u1DyadicMeasurement =
                    RFC6374_DYADIC_MEAS_ENABLE;

                /* Set the dyadic Proactive role active/passive */
                pDmStatsNodeTableEntry->u1DyadicProactiveRole =
                    pServiceInfo->u1DyadicProactiveRole;

                /* Set the Remote Session ID */
                if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
                {
                    /* Comb LMDM Remote Session ID */
                    pDmStatsNodeTableEntry->u4RxSessionId =
                        pLmDmInfo->u4RxLmDmSessionId;
                    /* updating the timestamp format used for the calculation */
                    pDmStatsNodeTableEntry->u1DelayTSFormat =
                        pLmDmInfo->u1LmDmQTSFormat;
                }
                else
                {
                    /* Independent DM Remote Session ID */
                    pDmStatsNodeTableEntry->u4RxSessionId =
                        pDmInfo->u4RxDmSessionId;
                    /* updating the timestamp format used for the calculation */
                    pDmStatsNodeTableEntry->u1DelayTSFormat =
                        pDmInfo->u1DmQTSFormat;
                }
            }
            else
            {
                /* Update if LM buffer type is SQI enabled */
                if (pServiceInfo->u1SessIntQueryStatus ==
                    RFC6374_SESS_INT_QUERY_ENABLE)
                {
                    pDmStatsNodeTableEntry->b1IsDMBufferSQIEnabled = OSIX_TRUE;
                }
                else
                {
                    pDmStatsNodeTableEntry->b1IsDMBufferSQIEnabled = OSIX_FALSE;
                }

            }

            /* Number of DMM Sent and DMR Recevied */
            if (((pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND) &&
                 (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)) ||
                ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND) &&
                 (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)))
            {
                pDmStatsNodeTableEntry->u1DelayMode = RFC6374_DM_MODE_ONDEMAND;
            }
            else
            {
                /* Update delay mode as proactive when delay measurement is initiated.
                 * It can be either in Independent mode or Combined 
                 * LmDm mode */
                if (((pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
                     (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)) ||
                    ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
                     (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)))
                {
                    pDmStatsNodeTableEntry->u1DelayMode =
                        RFC6374_DM_MODE_PROACTIVE;
                }
            }
        }

        pDmStatsNodeTableEntry->u1MplsPathType =
            pServiceInfo->R6374MplsParams.u1MplsPathType;

        /* Add Node */
        if (RBTreeAdd (RFC6374_DMSTATS_TABLE, pDmStatsNodeTableEntry) !=
            RFC6374_SUCCESS)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374DmAddStats: Add Stats to RB Tree failed "
                          "for session Id : %d\r\n",
                          pDelayDmBuffNode->u4SessionId);
            return RFC6374_FAILURE;
        }
        R6374SnmpLwGetDmTransactionStats (pDelayDmBuffNode,
                                          pDmStatsNodeTableEntry);
    }
    else
    {
        pDmStatsNodeTableEntry = pDmStatsNodeInfo;

        if (u1DelayMeasurementType == RFC6374_DM_TYPE_ONE_WAY)
        {
            pDmStatsNodeInfo->u41DMRcvd = pDmInfo->u4RxDmSeqCount;
        }
        if (u1DelayMeasurementType == RFC6374_LMDM_CAL_ONE_WAY)
        {
            pDmStatsNodeInfo->u41DMRcvd = pLmDmInfo->u4RxLmDmSeqCount;
        }

        R6374SnmpLwGetDmTransactionStats (pDelayDmBuffNode, pDmStatsNodeInfo);
    }
    /* Number of DMM Sent and DMR Recevied if Pdu Type is DMR
     * otherwise combined LmrDmr count*/

    if (u1DelayMeasurementType == RFC6374_DM_TYPE_TWO_WAY)
    {
        if ((pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMRDMR) ||
            (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMRDMR) ||
            /* For Dyadic Comb LMDM Run */
            (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM) ||
            (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM))
        {
            if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
            {
                pDmStatsNodeTableEntry->u4DMMSent = (UINT4)
                    ((pLmDmInfo->u2LmDmNoOfMessages + RFC6374_INDEX_ONE)
                     - pLmDmInfo->u2LmDmRunningCount);
            }
            /* Number of Proactive DMM Sent updated for CombinedLMDM */
            else if (pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
            {
                pDmStatsNodeTableEntry->u4DMMSent =
                    pLmDmInfo->u4LmDmNoOfProTxCount;
            }
            /* If Delay was performed through Dyadic
             * Then only DMM packets are recevied for calculation */
            if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
            {
                pDmStatsNodeTableEntry->u4DMMRcvd = pLmDmInfo->u4LmDmSeqCount;
            }
            else
            {
                pDmStatsNodeTableEntry->u4DMRRcvd = pLmDmInfo->u4LmDmSeqCount;
                /* Update Padding TLV size */
                pDmStatsNodeTableEntry->u4PadSize = pLmDmInfo->u4LmDmPadSize;
            }

            /* Delay session Type - Combined Lm-Dm */
            pDmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_COMBINED;
        }
        else
        {
            if (pDmInfo->u1DmMode == RFC6374_DM_MODE_ONDEMAND)
            {
                pDmStatsNodeTableEntry->u4DMMSent = (UINT4)
                    (pDmInfo->u2DmNoOfMessages - pDmInfo->u2DmRunningCount);
            }
            /* Number of DMM Sent updated for CombinedLMDM */
            else if (pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE)
            {
                pDmStatsNodeTableEntry->u4DMMSent = pDmInfo->u4DmNoOfProTxCount;
            }

            /* If Delay was performed through Dyadic 
             * Then only DMM packets are recevied for calculation */
            if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
            {
                pDmStatsNodeTableEntry->u4DMMRcvd = pDmInfo->u4DmSeqCount;
            }
            else
            {
                pDmStatsNodeTableEntry->u4DMRRcvd = pDmInfo->u4DmSeqCount;
                /* Update Padding TLV size */
                pDmStatsNodeTableEntry->u4PadSize = pDmInfo->u4DmPadSize;
            }
            /* Delay session Type - Independent DM */
            pDmStatsNodeTableEntry->u1SessType = RFC6374_SESS_TYPE_INDEPENDENT;
        }

    }
    /* Store the adjusted query interval for this session */
    pDmStatsNodeTableEntry->u2DmAdjustedInterval =
        pServiceInfo->DmInfo.u2DmTimeInterval;

    RFC6374_TRC5 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374DmAddStats: Service name : %s Sess Id : %d "
                  "Dmm Sent : %d Dmr Rcvd : %d Delay Mode : %d \r\n",
                  pServiceInfo->au1ServiceName, pDelayDmBuffNode->u4SessionId,
                  pDmStatsNodeTableEntry->u4DMMSent,
                  pDmStatsNodeTableEntry->u4DMRRcvd,
                  pDmStatsNodeTableEntry->u1DelayMode);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374DmAddStats()\r\n");
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374CalcFrameDelay                                    *
 *                                                                             *
 * Description        : This routine is called to calculate frame delay        *
 *                      and store the result in Frame delay Buffer.            *
 *                                                                             *
 * Input(s)           : pPktInfo - Pointer to the structure that stores the    *
 *                      information regarding Service info, MPLS path info     *
 *                      and  other information related to the state machine.   *
 *                      pDmrPktInfo - Pointer to DMR pkt structure             *
 *                      u1MeasurementType - One way or two way                 *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374M_FAILURE                     *
 *******************************************************************************/
PUBLIC UINT4
R6374CalcFrameDelay (tR6374PktSmInfo * pPktSmInfo,
                     tR6374RxDmrPktInfo * pDmrPktInfo, UINT1 u1MeasurementType)
{
    tR6374DmBufferTableEntry *pPktDelayBuffNode = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374DmBufferTableEntry *pPktDelayBuffNodeTmp = NULL;
    tR6374TSRepresentation ProcessingTime;
    tR6374TSRepresentation TotalDurationTxRx;
    UINT1               au1DelayValue[32] = { 0 };
    UINT4               u4SessId = RFC6374_INIT_VAL;
    UINT1               u1TSFormat = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&ProcessingTime, RFC6374_INIT_VAL, sizeof (ProcessingTime));
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    /* Update session Id detail */
    if (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        u4SessId = pLmDmInfo->u4LmDmSessionId;
        u1TSFormat = pLmDmInfo->u1LmDmQTSFormat;
    }
    /* Update session Id detail */
    if (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        u4SessId = pDmInfo->u4DmSessionId;
        u1TSFormat = pDmInfo->u1DmQTSFormat;
    }

    /* Check if RxTimeStampb was filled by the HW on reception */
    if ((pDmrPktInfo->RxTimeStampb.u4Seconds == RFC6374_INIT_VAL) ||
        (pDmrPktInfo->RxTimeStampb.u4NanoSeconds == RFC6374_INIT_VAL))
    {
        R6374UtilGetCurrentTime (&(pDmrPktInfo->RxTimeStampb), u1TSFormat);
    }

    RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374CalcFrameDelay : Tx TimeStamp of querier in "
                  "sec : %x NanoSec : %x Rx TimeStamp of querier in "
                  "Sec: %x NanoSec : %x \r\n",
                  pDmrPktInfo->TxTimeStampf.u4Seconds,
                  pDmrPktInfo->TxTimeStampf.u4NanoSeconds,
                  pDmrPktInfo->RxTimeStampb.u4Seconds,
                  pDmrPktInfo->RxTimeStampb.u4NanoSeconds);

    /* Calculate the Frame Delay Value, first we will calculate the frame delay
     * value plus processing time*/
    /* Frame Delay = Querier Rx - Querier Tx */
    /* Frame Delay = (RxTimeStampb - TxTimeStampf) */
    if (R6374DmCalcDiffInTimeRep (&(pDmrPktInfo->RxTimeStampb),
                                  &(pDmrPktInfo->TxTimeStampf),
                                  &(TotalDurationTxRx)) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     " R6374CalcFrameDelay: "
                     "discarding the received DMR frame, "
                     "invalid timestamps received\r\n");

        return RFC6374_FAILURE;
    }

    RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374CalcFrameDelay: Adding entry in DM buffer "
                  "for Service : %s SessId: %d \r\n",
                  pServiceInfo->au1ServiceName, u4SessId);

    /* Add Buffer Entry to Node */
    pPktDelayBuffNode = R6374DmInitAddDmEntry (pServiceInfo,
                                               u4SessId, u1MeasurementType);
    if (pPktDelayBuffNode == NULL)
    {
        /* Out of sequence DMR received, DMR frame will be discarded */
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, " R6374CalcFrameDelay: "
                     "discarding the received DMR frame as the corresponding"
                     " DMM entry is not found\r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_MEMCPY (&pPktDelayBuffNode->PktDelayValue,
                    &TotalDurationTxRx, sizeof (tR6374TSRepresentation));

    pPktDelayBuffNode->MeasurementTimeTaken =
        pDmrPktInfo->RxTimeStampb.u4Seconds;

    if (pDmInfo->u1DmType == RFC6374_DM_TYPE_TWO_WAY)
    {
        /* Update the Number of DMR received counter */
        /*RFC6374_INCR_DMR_RCVD_COUNT (pServiceInfo); */
        pPktDelayBuffNodeTmp = pPktDelayBuffNode;
    }

    /* Check if processing time is included in DMR, ie TxTimeStamb and
     * RxTimeStampf are non zero*/
    if (((pDmrPktInfo->TxTimeStampb.u4Seconds != RFC6374_INIT_VAL) ||
         (pDmrPktInfo->TxTimeStampb.u4NanoSeconds != RFC6374_INIT_VAL)) &&
        ((pDmrPktInfo->RxTimeStampf.u4Seconds != RFC6374_INIT_VAL) ||
         (pDmrPktInfo->RxTimeStampf.u4NanoSeconds != RFC6374_INIT_VAL)))
    {
        /* Calculate the Processing Time */
        /* Processing Time  = Responder Tx - Responder Rx */
        /* Processing Time = (TxTimeStampb - RxTimeStampf) */
        if (R6374DmCalcDiffInTimeRep (&(pDmrPktInfo->TxTimeStampb),
                                      &(pDmrPktInfo->RxTimeStampf),
                                      &(ProcessingTime)) == RFC6374_SUCCESS)
        {
            RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          " R6374CalcFrameDelay: Tx Timestamp of Responder:"
                          "Sec:%x NanoSec:%x RxTimeStamp of Responder "
                          "Sec:%x RxTimeStampf NanoSec:%x\r\n",
                          pDmrPktInfo->TxTimeStampb.u4Seconds,
                          pDmrPktInfo->TxTimeStampb.u4NanoSeconds,
                          pDmrPktInfo->RxTimeStampf.u4Seconds,
                          pDmrPktInfo->RxTimeStampf.u4NanoSeconds);

            /* substract the processing time from the calculate frame delay
             * ie Actual Frame Delay Value = Calculate Frame Delay - Processing
             * Time*/
            if (R6374DmCalcDiffInTimeRep (&(pPktDelayBuffNode->PktDelayValue),
                                          &(ProcessingTime),
                                          &(pPktDelayBuffNode->PktDelayValue))
                != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             " R6374CalcFrameDelay: "
                             "processing time cannot be substracted from"
                             "calculated frame delay, invalid timestamps"
                             "received\r\n");
            }
        }
        else
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         " R6374CalcFrameDelay: "
                         "processing time not calculated, "
                         "invalid timestamps received\r\n");
        }

    }

    RFC6374_MEMSET (au1DelayValue, 0, sizeof (au1DelayValue));

    RFC6374_TRC3 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  " R6374CalcFrameDelay: values in Delay buffer timestamp=%x, "
                  "Pkt-delay-seconds=%x, Pkt-delay-nanseconds=%x \r\n",
                  pPktDelayBuffNode->MeasurementTimeTaken,
                  pPktDelayBuffNode->PktDelayValue.u4Seconds,
                  pPktDelayBuffNode->PktDelayValue.u4NanoSeconds);

    /* Calculate the Frame Delay Variation */
    R6374DmCalcFrameDelayVariation (pServiceInfo, pPktDelayBuffNode);

    /*Calculate RT only for Non-Dyadic Delay */
    if (pServiceInfo->u1DyadicMeasurement != RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* Calculate the Round Trip  Delay Value */
        /* Frame Delay = (RxTimeStampb - TxTimeStampf) */
        if (R6374DmCalcDiffInTimeRep (&(pDmrPktInfo->RxTimeStampb),
                                      &(pDmrPktInfo->TxTimeStampf),
                                      &(pPktDelayBuffNode->PktRTDelayValue))
            != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         " R6374CalcFrameDelay: "
                         "discarding the received DMR frame, "
                         "invalid timestamps received\r\n");
            return RFC6374_FAILURE;
        }
    }

    /* Add Stats for DM Stats Table */
    if ((R6374DmAddStats (pPktSmInfo, pPktDelayBuffNode,
                          u1MeasurementType) != RFC6374_SUCCESS))
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, " R6374CalcFrameDelay: "
                     "Adding Stats Failed\r\n");
        return RFC6374_FAILURE;
    }

    UNUSED_PARAM (pPktDelayBuffNodeTmp);
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374Calc1FrameDelay                                   *
 *                                                                             *
 * Description        : This routine is called to calculate frame delay        *
 *                      and store the result in Frame delay Buffer.            *
 *                                                                             *
 * Input(s)           : pPktInfo - Pointer to the structure that stores the    *
 *                      information regarding Service info, MPLS path info     *
 *                      and  other information related to the state machine.   *
 *                      p1DmPktInfo - Pointer to 1DM pkt structure             *
 *                      u1MeasurementType - One way or two way                 *
 *                      u4SessionId - Session Identifier                   *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374M_FAILURE                     *
 *******************************************************************************/
PUBLIC UINT4
R6374Calc1FrameDelay (tR6374PktSmInfo * pPktSmInfo, tR6374Rx1DmPktInfo
                      * p1DmPktInfo, UINT1 u1MeasurementType, UINT4 u4SessionId)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmBufferTableEntry *pPktDelayBuffNode = NULL;
    tR6374DmBufferTableEntry *pDelay1DMBufferNode = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374DmBufferTableEntry DelayBufferEntryInfo;
    tR6374TSRepresentation PacketDelayValue;

    RFC6374_MEMSET (&PacketDelayValue, RFC6374_INIT_VAL,
                    sizeof (PacketDelayValue));
    RFC6374_MEMSET (&DelayBufferEntryInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfo);

    /* Check If the Buffer is Initial node not Persent
     * to Reset the Rx Seq Count, Prev Counters */

    DelayBufferEntryInfo.u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;
    DelayBufferEntryInfo.u4SessionId = u4SessionId;
    DelayBufferEntryInfo.u4SeqCount = RFC6374_VAL_1;

    RFC6374_MEMCPY (DelayBufferEntryInfo.au1ServiceName,
                    pPktSmInfo->pServiceConfigInfo->au1ServiceName,
                    RFC6374_STRLEN (pPktSmInfo->pServiceConfigInfo->
                                    au1ServiceName));

    pDelay1DMBufferNode = (tR6374DmBufferTableEntry *) RBTreeGet
        (RFC6374_DMBUFFER_TABLE, (tRBElem *) & DelayBufferEntryInfo);

    if (pDelay1DMBufferNode == NULL)
    {
        pDmInfo->u4RxDmSeqCount = RFC6374_RESET;
    }
    /* Calculate the packet Delay Value */
    /* packet Delay = (RxTimeStampb - TxTimeStampf) */
    if (R63741DmCalcDiffInTimeRep (&(p1DmPktInfo->RxTimeStampf),
                                   &(p1DmPktInfo->TxTimeStampf),
                                   &PacketDelayValue) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, " R6374Calc1FrameDelay: "
                     "discarding the received 1DM frame,"
                     "invalid timestamps received\r\n");
        return RFC6374_FAILURE;
    }

    /*Add Entry in the DM Buffer */
    pPktDelayBuffNode = R6374DmInitAddDmEntry (pServiceInfo,
                                               u4SessionId, u1MeasurementType);

    if (pPktDelayBuffNode == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, " R6374Calc1FrameDelay :"
                     " Failure adding node in the frame delay buffer\r\n");
        return RFC6374_FAILURE;
    }

    /* Store the Transmit time. Packet Delay and the Measurement Time
     * of the 1DM into the Packet delay buffer */

    RFC6374_COPY_TIME_REPRESENTATION (&
                                      (pPktDelayBuffNode->MeasurementTimeStamp),
                                      &(p1DmPktInfo->TxTimeStampf));
    RFC6374_COPY_TIME_REPRESENTATION (&(pPktDelayBuffNode->PktDelayValue),
                                      &(PacketDelayValue));
    pPktDelayBuffNode->MeasurementTimeTaken = R6374UtilGetTimeFromEpoch ();

    RFC6374_TRC3 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                  RFC6374_DEFAULT_CONTEXT_ID, " R6374Calc1FrameDelay : "
                  "values in delay buffer timestamp=%x, "
                  "pkt-delay-seconds=%x, pkt-delay-nanseconds=%x \r\n",
                  pPktDelayBuffNode->MeasurementTimeTaken,
                  pPktDelayBuffNode->PktDelayValue.u4Seconds,
                  pPktDelayBuffNode->PktDelayValue.u4NanoSeconds);

    /* Calculate the packet Delay Variation */
    R6374DmCalcFrameDelayVariation (pServiceInfo, pPktDelayBuffNode);

    /* Add Stats for DM Stats Table */
    if ((R6374DmAddStats (pPktSmInfo, pPktDelayBuffNode,
                          u1MeasurementType) != RFC6374_SUCCESS))
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     " R6374Process1Dm: Adding Stats Failed\r\n");
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

#endif
