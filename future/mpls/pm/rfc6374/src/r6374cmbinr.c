/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374cmbinr.c,v 1.1 2017/07/25 12:05:36 siva Exp $
 *
 * Description: This file contains the R6374 task loss and delay 
 *              measurement reply handler
 *******************************************************************/
#ifndef __R6374CMBINR_C__
#define __R6374CMBINR_C__
#include "r6374inc.h"
/******************************************************************************
 * Function Name      : R6374ProcessLmrDmr                                    *
 *                                                                            *
 * Description        : This function is called to process the received       *
 * 			Combined LmrDmr PDU                                   *
 *                                                                            *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the *
 *                      information regarding Service info, MPLS path info    *
 *                      and  other information related to the state machine.  *
 *		        ppdu - Received Pdu	                              *
 *	                                                                      *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE                     *
 ******************************************************************************/
PUBLIC INT4
R6374ProcessLmrDmr (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry  * pServiceInfo = NULL;
    tR6374RxLmrDmrPktInfo          * pLmrDmrPktInfo = NULL;
    tR6374LmInfoTableEntry         * pLmInfo = NULL;
    tR6374LmDmInfoTableEntry       * pLmDmInfo = NULL;
    tR6374TSRepresentation           LmrDmrRxTimeStamp;
    tR6374TSRepresentation           TotalDurationTxRx;
    tR6374NotifyInfo                 R6374LmDmNotify;
    UINT4                            u4RxSessionId = RFC6374_INIT_VAL;
    UINT4                            u4RxVersion = RFC6374_INIT_VAL;
    UINT4                            u4Txf = RFC6374_INIT_VAL;
    UINT4                            u4Rxf = RFC6374_INIT_VAL;
    UINT4                            u4Txb = RFC6374_INIT_VAL;
    UINT4                            u4Rxb = RFC6374_INIT_VAL;
    UINT4                            u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4                            u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4                            u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4                            u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4                            u4Temp = RFC6374_INIT_VAL;
    INT4                             i4RetVal = RFC6374_SUCCESS;
    UINT4                            u4SessQueryInt = RFC6374_INIT_VAL;
    UINT2                            u2PduLength = RFC6374_INIT_VAL;
    UINT1                            u1RxDflag = RFC6374_INIT_VAL;
    UINT1                            u1RxControlCode = RFC6374_INIT_VAL;
    UINT1                            u1TSFormat = RFC6374_INIT_VAL;
    UINT1                            u1ResTF = RFC6374_INIT_VAL;
    UINT1                            u1ResPrefTF = RFC6374_INIT_VAL;
    UINT1                            u1TlvType = RFC6374_INIT_VAL;
    UINT1                            u1TlvLength = RFC6374_INIT_VAL;



    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
		" Entering the R6374ProcessLmrDmr()\r\n");

    RFC6374_MEMSET (&LmrDmrRxTimeStamp, RFC6374_INIT_VAL , 
                             sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL , 
                             sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET (&R6374LmDmNotify, RFC6374_INIT_VAL,
                             sizeof(tR6374NotifyInfo));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);

    pLmrDmrPktInfo = &(pPktSmInfo->uPktInfo.LmrDmr);


    /* Increase Seq Count */
     RFC6374_LMDM_INCR_SEQ_COUNT (pLmDmInfo);

    if (pLmDmInfo->u1LmDmStatus != RFC6374_TX_STATUS_NOT_READY)
    {  
        i4RetVal = RFC6374_FAILURE;
        return i4RetVal;
    }
    
    /* Increment combined LmrDmr in Count */
    RFC6374_INCR_STATS_LMRDMR_IN (pServiceInfo);

    /* Reset LM Tx count when response is received for
     * Transmitted PDU */

    if (pLmDmInfo->u1LmDmType == RFC6374_LMDM_TYPE_TWO_WAY)
    {
        RFC6374_LMDM_RESET_NO_RESP_TX_COUNT (pLmDmInfo);
    }

    /* Get Current Time */
    R6374UtilGetCurrentTime (&LmrDmrRxTimeStamp, 
			     pLmDmInfo->u1LmDmQTSFormat);
 
    /* Validate the Received Control Code */
    /* Move the pointer of combined LmrDmr to Control Code */
    RFC6374_GET_1BYTE (u4RxVersion, pu1Pdu);

    u4RxVersion = u4RxVersion >> RFC6374_VAL_4;

    if (u4RxVersion != RFC6374_PDU_VERSION)
    {
        /* Unsupported Version in combined LmrDmr */
        R6374LmDmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP; 
        RFC6374_MEMCPY (&R6374LmDmNotify.au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        RFC6374_LMDM_INCR_ERR_PKT (pLmDmInfo);
        return RFC6374_FAILURE;
    }

    /* Get Control Code */
    RFC6374_GET_1BYTE (u1RxControlCode, pu1Pdu);

    if ((u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_VERSION) || 
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT) ||
        (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_CTRL_CODE) ||
        ((u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL) &&
         (pServiceInfo->u1SessIntQueryStatus != CLI_RFC6374_SESS_INT_QUERY_ENABLE)))
    {
        /* Control Code from Responder */
        if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_VERSION)
        {
            /* Invalid Version Control code */
            R6374LmDmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT)
        {
            /* Invalid Data Format */
            R6374LmDmNotify.u1TrapType = RFC6374_DATA_FORMAT_INVALID_TRAP; 
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUP_CTRL_CODE)
        {
            /* Invalid Control Code */
            R6374LmDmNotify.u1TrapType = RFC6374_UNSUP_CC_TRAP;
        }
        else if (u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL)
        {
            /* Unsupported Query Interval */
            /* In case Session nterval Query is disabled in Querier side, and
             * Unsupported Query Interval is got from Responder, abort the session*/
            R6374LmDmNotify.u1TrapType = RFC6374_UNSUPP_QUERY_INTERVAL;
        }

        RFC6374_MEMCPY (&R6374LmDmNotify.au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        RFC6374_LMDM_INCR_ERR_PKT (pLmDmInfo);
        return RFC6374_FAILURE;
    }

    RFC6374_GET_2BYTE(u2PduLength, pu1Pdu);
    /* DFlag Validation */
    /* + Message (2Byte) */

    RFC6374_GET_1BYTE(u1RxDflag, pu1Pdu);
    u1RxDflag = u1RxDflag >> RFC6374_VAL_4;

    if (u1RxDflag != (RFC6374_LMDM_32BIT_COUNTERS | RFC6374_LMDM_PKT_COUNT))
    {
        /* Unsupported Data Format */
        R6374LmDmNotify.u1TrapType = RFC6374_DATA_FORMAT_INVALID_TRAP;
        RFC6374_MEMCPY (&R6374LmDmNotify.au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        RFC6374_LMDM_INCR_ERR_PKT (pLmDmInfo);
        return RFC6374_FAILURE;
    }

   /* Get RTF and RPTF */
    RFC6374_GET_1BYTE(u1TSFormat, pu1Pdu);
    u1ResTF =  u1TSFormat >> RFC6374_VAL_4;

    u1ResPrefTF =  (UINT1)(u1TSFormat << RFC6374_VAL_4);
    u1ResPrefTF =  u1ResPrefTF >> RFC6374_VAL_4;

    if ((u1ResTF != pLmDmInfo->u1LmDmQTSFormat) &&
        (u1RxControlCode == RFC6374_CTRL_CODE_DATA_FORMAT_INVALID))
    {
        RFC6374_LMDM_INCR_ERR_PKT (pLmDmInfo);

        if ((pServiceInfo->i4Negostatus == RFC6374_NEGOTIATION_ENABLE) &&
            ((u1ResTF & gR6374GlobalInfo.u1SysSuppTimeFormat) ||
             (u1ResPrefTF & gR6374GlobalInfo.u1SysSuppTimeFormat)))
        {
              pLmDmInfo->u1LmDmQTSFormat = u1ResTF;
              pServiceInfo->u1NegoSuppTsFormat  = u1ResTF;
             /* Timestamp negotiation is enabled and also
              * Responder preferred timestamp is supported by sender node
              * So change the QTF filed and re-initiate the negotiation
              * procedure for the current session and don't do
              * any processing. */
              return RFC6374_SUCCESS;
        }
        else
        {
          /* Invalid TF */
          R6374LmDmNotify.u1TrapType = RFC6374_UNSUP_TIMESTAMP_TRAP;
          RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                           pServiceInfo->au1ServiceName,
                           RFC6374_STRLEN(pServiceInfo->au1ServiceName));
          R6374SendNotification (&R6374LmDmNotify);
          return RFC6374_FAILURE;
        }
    }
    /* Session ID Validation */
    /* + Resvered(3 byte) */
    pu1Pdu = pu1Pdu + RFC6374_VAL_2;      

    RFC6374_GET_4BYTE (u4RxSessionId, pu1Pdu);

    if (u4RxSessionId != pLmDmInfo->u4LmDmSessionId)
    {
        /* Wrong Session ID in combined LmrDmr Pdu */
        R6374LmDmNotify.u1TrapType = RFC6374_CONN_MISMATCH_TRAP;
        RFC6374_MEMCPY (&R6374LmDmNotify.au1ServiceName,
                        pServiceInfo->au1ServiceName,
                        RFC6374_STRLEN (pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        RFC6374_LMDM_INCR_ERR_PKT (pLmDmInfo);
        return RFC6374_FAILURE;
    } 
   /* TxTimeStamp  of responder*/
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.TxTimeStampb.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.TxTimeStampb.u4NanoSeconds, pu1Pdu);

    /* RxTimeStamp of querier */
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.RxTimeStampb.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.RxTimeStampb.u4NanoSeconds, pu1Pdu);

    /* TxTimeStamp of querier*/
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.TxTimeStampf.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.TxTimeStampf.u4NanoSeconds, pu1Pdu);

    /* RxTimeStamp of responder*/
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.RxTimeStampf.u4Seconds, pu1Pdu);
    RFC6374_GET_4BYTE (pLmrDmrPktInfo->DmrPktInfo.RxTimeStampf.u4NanoSeconds, pu1Pdu);
                  
    /* Get the Counter from LMR PDU
     * Txb,
     * Rxf,
     * Txf,
     * Rxb */

    /* Only when 64bit Counter is not Set */
    /* Counter 1 */
    RFC6374_GET_4BYTE (u4Temp, pu1Pdu );
    RFC6374_GET_4BYTE (u4Txb, pu1Pdu );
    /* Counter 2 */
    RFC6374_GET_4BYTE (u4Temp, pu1Pdu );
    RFC6374_GET_4BYTE (u4Rxf, pu1Pdu );
    /* Counter 3 */
    RFC6374_GET_4BYTE (u4Temp, pu1Pdu );
    RFC6374_GET_4BYTE (u4Txf, pu1Pdu );
    /* Counter 4 */
    RFC6374_GET_4BYTE (u4Temp, pu1Pdu );
    RFC6374_GET_4BYTE (u4Rxb, pu1Pdu );


    if(((pLmDmInfo->u1SessQueryIntervalState == RFC6374_SQI_INIT_STATE) &&
                 (u2PduLength > RFC6374_LMDM_PDU_MSG_LEN))||
            (u1RxControlCode == RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL))
        {
            RFC6374_GET_1BYTE (u1TlvType, pu1Pdu);
            RFC6374_GET_1BYTE (u1TlvLength, pu1Pdu);
            RFC6374_GET_4BYTE (u4SessQueryInt, pu1Pdu);
            /* Call function to adjust the session query interval */
            R6374UtilAdjustSessionQueryInterval (pServiceInfo,u4SessQueryInt,
                          RFC6374_TEST_MODE_LMDM);
        }
        else if (pLmDmInfo->u1SessQueryIntervalState == RFC6374_SQI_ADJUSTED_STATE)
        {
            /* If reply without TLV, when Querier is in Adjusted state, it means that
             * the responder has accepted the adjustment */
            pLmDmInfo->u1SessQueryIntervalState = RFC6374_SQI_RESET_STATE;
        }


    if (u4Rxf == RFC6374_INIT_VAL)
    {
        /* Get Rxf from BFD */
        if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMRDMR)
        {
#ifdef RFC6374STUB_WANTED
            if(pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
            }
            else
#endif
            {
                i4RetVal = R6374GetBfdPktCount (pServiceInfo, &u4Txf, &u4Rxf);
                if (i4RetVal == RFC6374_FAILURE)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374ProcessLmrDmr: "
                                 "BFD Get counter retunred failure\r\n");
                    return RFC6374_FAILURE;
                }
            }
        }
        else if(pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMRDMR)
        {
            R6374GetMplsPktCnt(pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                    &u4RcvTnlTx, &u4RcvTnlRx);

            if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                u4Rxf = u4RcvTnlRx;
            }
            else
            {
                u4Rxf = u4PwOrFwrdTnlRx;
            }
        }
    }
                                
   /* Get the measurement time between two packets */
    if ((R6374UtilCalTimeDiff (&(pLmDmInfo->LmDmOriginTimeStamp),
                    &LmrDmrRxTimeStamp, &TotalDurationTxRx)) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374ProcessLmr: "
                     " calculation for time diff return failure\r\n");
        return RFC6374_FAILURE;
    }
 
    /* Assign the Packet values */
    pLmrDmrPktInfo->LmrPktInfo.u4TxCounterf = u4Txf; /*A_TxP */ 
    pLmrDmrPktInfo->LmrPktInfo.u4RxCounterf = u4Rxf; /*A_RxP */
    pLmrDmrPktInfo->LmrPktInfo.u4TxCounterb = u4Txb; /*B_TxP */
    pLmrDmrPktInfo->LmrPktInfo.u4RxCounterb = u4Rxb; /*B_RxP */

    RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
 	          "R6374ProcessLmr: A_TxP %d A_RxP %d B_TxP %d B_RxP %d \r\n",
        	  u4Txf, u4Rxf, u4Txb, u4Rxb);

    if (R6374CalcFrameLoss (pPktSmInfo, TotalDurationTxRx,
			    RFC6374_LM_TYPE_TWO_WAY, u4RxSessionId,
			    pLmDmInfo->u4LmDmSeqCount) !=
                            RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, " R6374ProcessLmrDmr: "
                     " calculation for frame loss return failure\r\n");
        return RFC6374_FAILURE;
    }

    /* Update previous counter */
    pLmInfo->u4PreTxCounterf = pLmrDmrPktInfo->LmrPktInfo.u4TxCounterf; /* A_TxP */
    pLmInfo->u4PreRxCounterf = pLmrDmrPktInfo->LmrPktInfo.u4RxCounterf; /* A_RxP */
    pLmInfo->u4PreTxCounterb = pLmrDmrPktInfo->LmrPktInfo.u4TxCounterb; /* B_TxP */
    pLmInfo->u4PreRxCounterb = pLmrDmrPktInfo->LmrPktInfo.u4RxCounterb; /* B_RxP */

    if (R6374CalcFrameDelay (pPktSmInfo, &(pLmrDmrPktInfo->DmrPktInfo), 
			     RFC6374_DM_TYPE_TWO_WAY) != RFC6374_SUCCESS)
    {
       RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
	            RFC6374_DEFAULT_CONTEXT_ID, " R6374ProcessLmrDmr: "
                   " calculation for frame delay return failure\r\n");
       return RFC6374_FAILURE;
    }

    RFC6374_TRC4 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
		 "R6374ProcessLmr: Previous values: "
		 "A_TxP %d A_RxP %d B_TxP %d B_RxP %d \r\n",
		 pLmInfo->u4PreTxCounterf, pLmInfo->u4PreRxCounterf,
  		 pLmInfo->u4PreTxCounterb, pLmInfo->u4PreRxCounterb);

    UNUSED_PARAM(u1TlvLength);
    UNUSED_PARAM(u1TlvType);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
		 " Exiting from R6374ProcessLmrDmr()\r\n");
    return i4RetVal;
}
/*******************************************************************************
 * Function           : R6374Process1LmDm                                      *
 *                                                                             *
 * Description        : This routine processes the received 1LMDM PDU.         *
 *                                                                             *
 * Input(s)           : pPduSmInfo - Pointer to the structure that stores the  *
 *                      information regarding mp info, the PDU if received and *
 *                      other information related to the functionality.        *
 *                      pu1Pkt - Pointer to pointer to the pu1Pkt.             *
 *                                                                             *
 * Output(s)          : None.                                                  *
 *                                                                             *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE		       *
 ******************************************************************************/
PUBLIC INT4
R6374Process1LmDm (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pkt)
{
    tR6374ServiceConfigTableEntry      * pServiceInfo = NULL;
    tR6374LmBufferTableEntry           LossBufferEntryInfo;
    tR6374LmBufferTableEntry           * pLossLmmBufferNode = NULL;
    tR6374Rx1LmDmPktInfo               * p1LmDmPktInfo = NULL;
    tR6374LmDmInfoTableEntry           * pLmDmInfo = NULL;
    tR6374TSRepresentation             LossLmDmRxTimeStamp;
    tR6374TSRepresentation             TotalDurationTxRx;
    UINT4                              u4RxLmDmSessionId = RFC6374_INIT_VAL;
    UINT4                              u4TxTemp = RFC6374_INIT_VAL;
    UINT4                              u4Txf =  RFC6374_INIT_VAL;
    UINT4                              u4Rxf = RFC6374_INIT_VAL;
    UINT4                              u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4                              u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4                              u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4                              u4RcvTnlRx = RFC6374_INIT_VAL;
    INT4                               i4RetVal = RFC6374_SUCCESS;
    UINT1                              u1RxQueryTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                "Entering R6374Process1LmDm()\r\n");

    RFC6374_MEMSET (&LossLmDmRxTimeStamp, RFC6374_INIT_VAL,
		    sizeof(tR6374TSRepresentation));

    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL , 
                             sizeof(tR6374TSRepresentation));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM(pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM(pPktSmInfo);
    p1LmDmPktInfo = &(pPktSmInfo->uPktInfo.OneLmDm);

    RFC6374_MEMSET (p1LmDmPktInfo, RFC6374_INIT_VAL, 
		    sizeof (tR6374Rx1LmDmPktInfo));

    if (pServiceInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374Process1LmDm: "
                     "ServiceInfo is not found, discarding the received"
                     " Comined 1LmDm frame\r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_INCR_STATS_1LMDM_IN (pPktSmInfo->pServiceConfigInfo);

    /* Get the packet Recived time */
    R6374UtilGetCurrentTime (&LossLmDmRxTimeStamp, pLmDmInfo->u1LmDmQTSFormat);

    /* QTF Validation */
    /* version + control code + Message (2Byte) */
     pu1Pkt = pu1Pkt + RFC6374_VAL_4;
     RFC6374_GET_1BYTE (u1RxQueryTF, pu1Pkt);

    /* Timestamp received from querier updated
     * to QTS format which will be used for timestamp
     * format calculation */

    pLmDmInfo->u1LmDmQTSFormat =  (UINT1)(u1RxQueryTF << RFC6374_VAL_4);
    pLmDmInfo->u1LmDmQTSFormat =  pLmDmInfo->u1LmDmQTSFormat >> RFC6374_VAL_4;

    /* Move Pointer to Session Identifier Field */
    pu1Pkt = pu1Pkt + RFC6374_VAL_3;

    /* Get Session Identifier */
    RFC6374_GET_4BYTE (u4RxLmDmSessionId, pu1Pkt);

    /* Copy the TxTimeStampf */
    RFC6374_GET_4BYTE (p1LmDmPktInfo->OneDmPktInfo.TxTimeStampf.u4Seconds, pu1Pkt);
    RFC6374_GET_4BYTE (p1LmDmPktInfo->OneDmPktInfo.TxTimeStampf.u4NanoSeconds, pu1Pkt);

    /* Copy the RxTimeStampf */
    RFC6374_GET_4BYTE (p1LmDmPktInfo->OneDmPktInfo.RxTimeStampf.u4Seconds, pu1Pkt);
    RFC6374_GET_4BYTE (p1LmDmPktInfo->OneDmPktInfo.RxTimeStampf.u4NanoSeconds, pu1Pkt);

    /* Check if RxTimeStampf was filled by the HW on reception */
    if ((p1LmDmPktInfo->OneDmPktInfo.RxTimeStampf.u4Seconds == RFC6374_INIT_VAL) &&
        (p1LmDmPktInfo->OneDmPktInfo.RxTimeStampf.u4NanoSeconds == RFC6374_INIT_VAL))
    {
        R6374UtilGetCurrentTime (&(p1LmDmPktInfo->OneDmPktInfo.RxTimeStampf),
				  pLmDmInfo->u1LmDmQTSFormat);
    }

    RFC6374_TRC3 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
 		  "R6374Process1LmDm: Received session Id : %d Received "
 		  "Seconds: %x NanoSeconds: %x \r\n",u4RxLmDmSessionId,
	          p1LmDmPktInfo->OneDmPktInfo.TxTimeStampf.u4Seconds,
	          p1LmDmPktInfo->OneDmPktInfo.TxTimeStampf.u4NanoSeconds);

   /* Move pointer to get counter 1 */
    pu1Pkt = pu1Pkt + 16;
    
    /* Get Counters As this is One way get*/
    /* Only when 64bit Counter is not Set */
    /* Counter 1 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1Pkt);
    RFC6374_GET_4BYTE (u4Txf, pu1Pkt);
    /* Counter 2 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1Pkt);
    RFC6374_GET_4BYTE (u4Rxf, pu1Pkt);

    if(u4Rxf == RFC6374_INIT_VAL)
    {
        if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM)
        {
#ifdef RFC6374STUB_WANTED
            if(pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
            }
            else
#endif
           {
              i4RetVal =  R6374GetBfdPktCount (pServiceInfo,
                                               &u4Txf, &u4Rxf);
              if (i4RetVal == RFC6374_FAILURE)
              {
                  RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                               RFC6374_DEFAULT_CONTEXT_ID, "R6374Process1LmDm: "
                               "Get BFD Packet counter failure\r\n");
                   return RFC6374_FAILURE;
               }
           }
        }
        else if(pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM)
        {
            R6374GetMplsPktCnt(pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                    &u4RcvTnlTx, &u4RcvTnlRx);

            if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                u4Rxf = u4RcvTnlRx;
            }
            else
            {
                u4Rxf = u4PwOrFwrdTnlRx;
            }
        }
    }

     RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
		  "R6374Process1LmDm: Received Bfd counters in Pdu : "
	          "Tx : %d Rx : %d \r\n",u4Txf, u4Rxf);

    /* Check if the Buffer is initial node. If node is not present
     * Reset the Rx Seq Count, Prev Counters */
    /* Loss */
    LossBufferEntryInfo.u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;
    LossBufferEntryInfo.u4SessionId = u4RxLmDmSessionId;
    LossBufferEntryInfo.u4SeqCount = RFC6374_VAL_1;

    RFC6374_MEMCPY (LossBufferEntryInfo.au1ServiceName,
                    pServiceInfo->au1ServiceName,
                    RFC6374_STRLEN (pServiceInfo->au1ServiceName));

    pLossLmmBufferNode = (tR6374LmBufferTableEntry *) RBTreeGet
                    (RFC6374_LMBUFFER_TABLE, (tRBElem *)&LossBufferEntryInfo);

    if (pLossLmmBufferNode == NULL)
    {
        pLmDmInfo->u4RxLmDmSeqCount = RFC6374_RESET;
        pLmDmInfo->u4PreCLmTxCounterf = RFC6374_RESET;
        pLmDmInfo->u4PreCLmTxCounterb = RFC6374_RESET;
    }
    else
    {
      pLossLmmBufferNode = NULL;
      UNUSED_PARAM (pLossLmmBufferNode);
    }

     /* Get the measurement time between two packets */
    if ((R6374UtilCalTimeDiff (&p1LmDmPktInfo->OneDmPktInfo.TxTimeStampf,
                     &LossLmDmRxTimeStamp, &TotalDurationTxRx)) != RFC6374_SUCCESS)
    {
         RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                      RFC6374_DEFAULT_CONTEXT_ID, " R6374Process1LmDm: "
                     "Calculate the Time Difference failure\r\n");
         return RFC6374_FAILURE;
    }

    /*Copy Tx fetched from PDU and Rx fetched from BFD to pPktSmInfo*/
    p1LmDmPktInfo->OneLmPktInfo.u4TxCounterf = u4Txf;
    p1LmDmPktInfo->OneLmPktInfo.u4RxCounterf = u4Rxf;

    if (R6374CalcFrameLoss (pPktSmInfo, TotalDurationTxRx,
			    RFC6374_LMDM_CAL_ONE_WAY, u4RxLmDmSessionId,
			    pLmDmInfo->u4RxLmDmSeqCount) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
			    RFC6374_DEFAULT_CONTEXT_ID, " R6374Process1LmDm: "
			    " calculation for combined LmDm oneway frame "
			    "loss return failure\r\n");
       return RFC6374_FAILURE;
    }

    pLmDmInfo->u4PreCLmTxCounterf = p1LmDmPktInfo->OneLmPktInfo.u4TxCounterf;
    pLmDmInfo->u4PreCLmRxCounterb = p1LmDmPktInfo->OneLmPktInfo.u4RxCounterf;

    if (R6374Calc1FrameDelay (pPktSmInfo, &p1LmDmPktInfo->OneDmPktInfo,
                              RFC6374_LMDM_CAL_ONE_WAY, u4RxLmDmSessionId) 
			      != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374Process1DmDm: "
                     "Failure occured in 1 way frame delay calculation \r\n");
       return RFC6374_FAILURE;
   }

   RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                "Exiting from R6374Process1LmDm()\r\n");

    return RFC6374_SUCCESS;

}

#endif
