/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374cmbres.c,v 1.1 2017/07/25 12:05:36 siva Exp $
 *
 * Description: This file contains the R6374 task for combined loss 
 *              and measurement responder
 *******************************************************************/
#ifndef __R6374CMBLMRES_C__
#define __R6374CMBLMRES_C__
#include "r6374inc.h"


/*******************************************************************************
 * Function Name      : R6374ProcessLmmDmm                                     *
 * Description        : This is called to parse the received combined LMMDMM   *
 * 			PDU and fill required data structures                  *
 * Input(s)           : pPktSmInfo - Pointer to the Service Related Config     *
 *                      pu1Pdu  - Pointer to the recevied PDU                  *
 * Output(s)          : None                                                   *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE		       *
 *******************************************************************************/
PUBLIC INT4
R6374ProcessLmmDmm (tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry  * pServiceInfo = NULL;
    tR6374RxLmmDmmPktInfo          * pLmDmPduInfo = NULL;
    tR6374LmDmInfoTableEntry       * pLmDmInfo = NULL;
    tR6374LmBufferTableEntry         LossBufferEntryInfo;
    tR6374TSRepresentation           TotalDurationTxRx;
    tR6374NotifyInfo                 R6374LmNotify;
    UINT1                          * pu1RevPdu = NULL;
    UINT4                            u4Txf =  RFC6374_INIT_VAL;
    UINT4                            u4Rxf = RFC6374_INIT_VAL;
    UINT4                            u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4                            u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4                            u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4                            u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4                            u4TxTemp = RFC6374_INIT_VAL;
    UINT4                            u4SessQueryInterval = RFC6374_INIT_VAL;
    INT4                             i4RetVal = RFC6374_SUCCESS;
    UINT2                            u2PduLength = RFC6374_INIT_VAL;
    UINT1                            u1TlvType = RFC6374_INIT_VAL;
    UINT1                            u1TlvLength = RFC6374_INIT_VAL;
    UINT1                            u1RxLmDmCtrlCode = RFC6374_INIT_VAL;
    UINT1                            u1RxDFlag = RFC6374_INIT_VAL;
    UINT1                            u1RxLmDmVersion = RFC6374_INIT_VAL;
    UINT1                            u1RxOriginTF = RFC6374_INIT_VAL;
    UINT1                            u1QueryTF = RFC6374_INIT_VAL;
    UINT1                            u1ResPrefTF = RFC6374_INIT_VAL;
    UINT1                            u1ResTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
   	         " Entering the R6374ProcessLmmDmm()\r\n");

    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL , 
		    sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET (&LossBufferEntryInfo, RFC6374_INIT_VAL,
		    sizeof(tR6374LmBufferTableEntry));
    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
		    sizeof(tR6374NotifyInfo));

    if (pPktSmInfo->pServiceConfigInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID," R6374ProcessLmmDmm: "
                    "Recevied Service Info is NULL\r\n");
        i4RetVal = RFC6374_FAILURE;
        return i4RetVal;
    }

    /* Keep the Received PDU Pointer */
    pu1RevPdu = pu1Pdu;
    
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM (pPktSmInfo);
 
    /* Get Version Field from PDU */
    RFC6374_GET_1BYTE (u1RxLmDmVersion, pu1Pdu);

    /* Get Control Code from PDU */
    RFC6374_GET_1BYTE (u1RxLmDmCtrlCode, pu1Pdu);
    /*Check if Response is requested*/
    if (u1RxLmDmCtrlCode  == RFC6374_CTRL_CODE_RESP_NOT_REQ)
    {
        if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
        {
            /* For Dyadic LMM/DMM Packet is sent with control code 0x2 */
            /* Increment LMM/DMM In */
            RFC6374_INCR_STATS_LMMDMM_IN (pServiceInfo);

            if(R6374CombLmDmDyadicMeasurement(pPktSmInfo, pu1RevPdu)!= RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                        "R6374ProcessLmmDmm: "
                        "Dyadic Measurement Failed\r\n");
                i4RetVal = RFC6374_FAILURE;
                return i4RetVal;
            }
        }
        else
        {
            pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;
            /*Call R6374Process1LmDm to process 1LMDM*/
            if (R6374Process1LmDm (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                        RFC6374_DEFAULT_CONTEXT_ID, "R6374ProcessLmmDmm: "
                        "failure occurred in processing 1LmDm\r\n");
                return RFC6374_FAILURE;
            }
        }
    }
    else if (u1RxLmDmCtrlCode == RFC6374_CTRL_CODE_RESP_INBAND_REQ)
    {
        /*Increment combined LMDM receive count */
        RFC6374_INCR_STATS_LMMDMM_IN (pServiceInfo);

        /*Two Way Loss Measurement*/
        pLmDmPduInfo = &(pPktSmInfo->uPktInfo.LmmDmm);

        /* Version Validation */
        u1RxLmDmVersion = u1RxLmDmVersion >> RFC6374_VAL_4;

        if (u1RxLmDmVersion != RFC6374_PDU_VERSION)
        {
            /* Unsupported Version in LMM */
            pLmDmInfo->u1LmDmInvalidField = RFC6374_CTRL_CODE_UNSUP_VERSION;            
        }
        /* Get PDU length */
        /* + Message Length (2 Byte) */
        RFC6374_GET_2BYTE(u2PduLength, pu1Pdu);

        /* Data Format Validation */
        RFC6374_GET_1BYTE(u1RxDFlag, pu1Pdu);
        u1RxOriginTF = u1RxDFlag;       /* For OTF Validation */

        u1RxDFlag = u1RxDFlag >> RFC6374_VAL_6;
        if (u1RxDFlag != (RFC6374_LMDM_32BIT_COUNTERS | RFC6374_LMDM_PKT_COUNT))
        {
            /* Unsupported Data Flag */
            pLmDmInfo->u1LmDmInvalidField = RFC6374_CTRL_CODE_UNSUP_DATA_FORMAT; 
        }
 
        u1ResTF = pLmDmInfo->u1LmDmTSFormat;
        u1ResPrefTF = pLmDmInfo->u1LmDmTSFormat;
        /* By receiving DM Query message, the responder determines whether
         * it is capable of writing timestamps in the format specified by
         * the QTF field. If so, the Responder Timestamp Format (RTF) field
         * is set equal to the QTF field. If not, the RTF field is set equal
         * to the Responder's Preferred Timestamp Format (RPTF) field */

        u1QueryTF = (UINT1)(u1RxOriginTF << RFC6374_VAL_4);
        u1RxOriginTF = (UINT1)(u1QueryTF >> RFC6374_VAL_4);

        if (u1RxOriginTF != u1ResTF)
        {
            /* UnSupported TF */
            pLmDmInfo->u1LmDmInvalidField = RFC6374_CTRL_CODE_DATA_FORMAT_INVALID;
            u1ResTF = u1ResPrefTF;
        }
	else
    {
        u1ResTF = u1RxOriginTF; /*QTF of querier */
    }

        /* Move the Pointer to Timer fields from Reserved
         * Reserved(2 Byte) + RTF-RPTF (1 byte ) + Session Id + DS (4 byte)*/
        pu1Pdu = pu1Pdu + RFC6374_VAL_7;

       /* Copy the TxTimeStampf received from sender*/
        RFC6374_GET_4BYTE (pLmDmPduInfo->DmmPktInfo.TxTimeStampf.u4Seconds, 
			   pu1Pdu);
        RFC6374_GET_4BYTE (pLmDmPduInfo->DmmPktInfo.TxTimeStampf.u4NanoSeconds, 
			   pu1Pdu);
        /* Calculate inter query interval and set if Unsupported Query Interval
         *  error message has to be initiated */
        R6374UtilCheckInterQueryInterval(pServiceInfo,
               pLmDmPduInfo->DmmPktInfo.TxTimeStampf.u4Seconds,
               pLmDmPduInfo->DmmPktInfo.TxTimeStampf.u4NanoSeconds,
               RFC6374_TEST_MODE_LMDM);


        /* Copy the Locally filled RxTimeStampf */
        RFC6374_GET_4BYTE (pLmDmPduInfo->DmmPktInfo.RxTimeStampf.u4Seconds, 
			   pu1Pdu);
        RFC6374_GET_4BYTE (pLmDmPduInfo->DmmPktInfo.RxTimeStampf.u4NanoSeconds, 
			   pu1Pdu);

        /* Check if RxTimeStampf was filled by the HW on reception*/
        if ((pLmDmPduInfo->DmmPktInfo.RxTimeStampf.u4Seconds == 
						RFC6374_INIT_VAL) &&
            (pLmDmPduInfo->DmmPktInfo.RxTimeStampf.u4NanoSeconds 
						== RFC6374_INIT_VAL))
        {
           R6374UtilGetCurrentTime (&(pLmDmPduInfo->DmmPktInfo.RxTimeStampf),
				     u1ResTF);
        }

        /* Move the Pointer to counter fields from TimeStamp 2
         * TimeStamp 3(8 Byte) + Time Stamp 4 (8 byte)*/
        pu1Pdu = pu1Pdu + 16;

        /* Get Counters TX of the Forward */
        /* This is when 64bit Counter is not set */
        /* Counter 1 */
        RFC6374_GET_4BYTE (u4TxTemp, pu1Pdu);
        RFC6374_GET_4BYTE (u4Txf, pu1Pdu);
        /* Counter 2 */
        RFC6374_GET_4BYTE (u4TxTemp, pu1Pdu);
        RFC6374_GET_4BYTE (u4Rxf, pu1Pdu);

        if(u4Rxf == RFC6374_INIT_VAL)
        {
            if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM)
            {
#ifdef RFC6374STUB_WANTED
                if(pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
                {
                    R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
                }
                else
#endif
                {
                    i4RetVal =  R6374GetBfdPktCount(pPktSmInfo->pServiceConfigInfo,
                            &u4Txf, &u4Rxf);
                    if (i4RetVal == RFC6374_FAILURE)
                    {
                        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                                     RFC6374_DEFAULT_CONTEXT_ID,
	    	                     " R6374ProcessLmmDmm: "
                                     "Get BFD Packet Counter failure\r\n");
                        return RFC6374_FAILURE;
                    }
                }
            }
            else if(pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM)
            {
                R6374GetMplsPktCnt(pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                        &u4RcvTnlTx, &u4RcvTnlRx);

                if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
                {
                    u4Rxf = u4RcvTnlRx;
                }
                else
                {
                    u4Rxf = u4PwOrFwrdTnlRx;
                }
            }
        }
        
        /*Copy Tx fetched from PDU and Rx fetched from BFD to pPktSmInfo*/
        pLmDmPduInfo->LmmPktInfo.u4TxCounterf = u4Txf; /*A_TxP*/
        pLmDmPduInfo->LmmPktInfo.u4RxCounterf = u4Rxf; /*B_RxP*/

        if(u2PduLength > RFC6374_LMDM_PDU_MSG_LEN)
        {
            /* Move pointer to TLV block*/
            pu1Pdu = pu1Pdu + 16;
            /* Check if SQI TLV is present
             * 1. If SQI is there with interval zero, Response has to be sent with
             *     Reception interval
             * 2. If SQI is there with non-zero interval, it means the Query interval
             *    has been adjusted in the other end.
             *    So reset the sending flag */
            RFC6374_GET_1BYTE(u1TlvType,pu1Pdu);
            if(u1TlvType == RFC6374_SQI_TLV_TYPE)
            {
                RFC6374_GET_1BYTE(u1TlvLength,pu1Pdu);
                RFC6374_GET_4BYTE(u4SessQueryInterval,pu1Pdu);

                if(u4SessQueryInterval == 0)
                {
                    /* Indicates to start initial adjustment */
                    pServiceInfo->LmDmInfo.b1IsSessQueryTlvRequired = OSIX_TRUE;
                }
                else
                {
                    /* Indicates initial or intermediate adjustment is done*/
                    pServiceInfo->LmDmInfo.b1IsSessQueryTlvRequired = OSIX_FALSE;
                    pServiceInfo->LmDmInfo.u1LmDmInvalidField = RFC6374_VALIDATION_SUCCESS;
                }
            }
        }

        /*Call Init process to send LMR*/
        if (R6374LmDmResXmitLmrDmrPdu (pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                    RFC6374_DEFAULT_CONTEXT_ID, "R6374ProcessLmmDmm: "
                    "Failure occurred while responding a Combined LmrDmr for "
		    "Combined LmmDmm\r\n");
            return RFC6374_FAILURE;
        }
    }
    else
    {
        /*Increment LMM In */
        RFC6374_INCR_STATS_LMMDMM_IN (pServiceInfo);

        /* Invalid Control Code */
        pLmDmInfo->u1LmDmInvalidField = RFC6374_CTRL_CODE_UNSUP_CTRL_CODE;

        /*Call Init process to send LMR*/
        if (R6374LmDmResXmitLmrDmrPdu(pPktSmInfo, pu1RevPdu) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374ProcessLmmDmm: "
                         "Failure occurred while responding a Combined LmrDmr for "
		         "Combined LmmDmm\r\n");
            return RFC6374_FAILURE;
        }
    }

    UNUSED_PARAM(u1TlvLength);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
            " Exiting from R6374ProcessLmmDmm()\r\n");
    return RFC6374_SUCCESS;
}
/*******************************************************************************
 * Function Name      : R6374LmDmResXmitLmrDmrPdu                              *
 *                                                                             *
 * Description        : This routine formats and transmits the LMR PDUs.       *
 *                                                                             *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the  *
 *                      information regarding Service info and MPLS pat Info   *
 *                      and other information related to the functionality.    *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE		       *
 *******************************************************************************/
PUBLIC UINT4
R6374LmDmResXmitLmrDmrPdu (tR6374PktSmInfo * pPktSmInfo,
                           UINT1   *pu1RevPdu)
{
    tR6374ServiceConfigTableEntry  * pServiceInfo = NULL;
    tR6374BufChainHeader           * pBuf = NULL;
    tR6374LmDmInfoTableEntry       * pLmDmInfo = NULL;
    UINT4                     	   u4Txb = RFC6374_INIT_VAL;  
    UINT4                          u4Rxb = RFC6374_INIT_VAL;  
    UINT4                          u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4                          u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4                          u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4                          u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4                          u4PduSize = RFC6374_INIT_VAL;
    UINT1                          * pu1PduStart = NULL;
    UINT1                          * pu1PduEnd = NULL;
    INT4                           i4RetVal = RFC6374_SUCCESS;
    UINT2                          u2PduLength = RFC6374_INIT_VAL;  
    UINT2                          u2RxPduLen = RFC6374_INIT_VAL;
    UINT1                          u1PadType = RFC6374_INIT_VAL;
    UINT1                          u1PadLen = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmDmResXmitLmrDmrPdu()\r\n");

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM (pPktSmInfo);
    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM(pPktSmInfo);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmDmResXmitLmrDmrPdu: "
		     "Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1PduStart = RFC6374_PDU;
    pu1PduEnd = RFC6374_PDU;

    /* Format combined LmrDmr PDU header */
    R6374LmDmInitFormatLmrDmrPduHdr (pLmDmInfo, pu1RevPdu ,&pu1PduEnd);

    /* Put Counter in combined LmrDmr PDU */
    if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM)
    {
        pPktSmInfo->u1PduType = RFC6374_LMDM_METHOD_INFERED;
#ifdef RFC6374STUB_WANTED
        if(pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
        {
            R6374StubGetBfdCounter (&u4Txb, &u4Rxb);
        }
        else
#endif
        {
            i4RetVal =  R6374GetBfdPktCount(pPktSmInfo->pServiceConfigInfo,
                    &u4Txb, &u4Rxb);
            if (i4RetVal == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     	     RFC6374_DEFAULT_CONTEXT_ID,
	                     "R6374LmDmResXmitLmrDmrPdu: "
                             "Get BFD Packet counter failure\r\n");
                RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
                return RFC6374_FAILURE;
            }
        }
    }
    else if( pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM)
    {
        pPktSmInfo->u1PduType = RFC6374_LMDM_METHOD_DIRECT;
        R6374GetMplsPktCnt(pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                &u4RcvTnlTx, &u4RcvTnlRx);

        if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
        {
            u4Txb = u4PwOrFwrdTnlTx;
            u4Rxb = u4PwOrFwrdTnlRx;
        }
        else
        {
            u4Txb = u4PwOrFwrdTnlTx;
        }
    }

    /* Format the LmrDmr PDU */
    R6374LmDmInitPutLmrDmrPduInfo (pPktSmInfo, &pu1PduEnd, u4Txb);

    /* Get Pdu length */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_2;
    RFC6374_GET_2BYTE (u2RxPduLen, pu1RevPdu);

    /* Get Padding length */
    /* Currently pu1RevPdu is pointing the pdu length field
     * Move 72 bytes from msg length to get Padding TLV Type field */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_72;

    if (u2RxPduLen > RFC6374_LMDM_PDU_MSG_LEN)
    {
       /*Consider only Padding length */
       u2RxPduLen = (UINT2)(u2RxPduLen - RFC6374_LMDM_PDU_MSG_LEN);
       R6374DmRxPutPadTlv (&pu1PduEnd, pu1RevPdu, u2RxPduLen);
    }

    u2PduLength = (UINT2)(pu1PduEnd - pu1PduStart);
    pu1PduEnd = pu1PduStart + RFC6374_INDEX_TWO;
    RFC6374_PUT_2BYTE(pu1PduEnd, u2PduLength);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1PduStart, RFC6374_INIT_VAL,
                (UINT4) (u2PduLength)) != RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
	             RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmitLmrPdu: "
		    "Buffer copy operation failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Send the pBuf to MPLS */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);

    if(R6374MplsTxPkt(pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_LOSSDELAY)
                    != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmitLmrPdu: "
                    "Transmit Combined LmrDmr Pdu failed\r\n");
        i4RetVal = RFC6374_FAILURE;
    }
    else
    {
        u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
        i4RetVal = RFC6374_SUCCESS;
    }

    UNUSED_PARAM(u4PduSize);
    if (i4RetVal != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmitLmrPdu: "
                    "Combined LmrDmr transmission to the lower layer "
	            "failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /*Increment Combined LmrDmr Out count */
    RFC6374_INCR_STATS_LMRDMR_OUT(pPktSmInfo->pServiceConfigInfo);

    UNUSED_PARAM(u1PadLen);
    UNUSED_PARAM(u1PadType);
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
            " Exiting from R6374LmDmResXmitLmrDmrPdu()\r\n");
    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function Name      : R6374LmDmInitFormatLmrDmrPduHdr                      *
 *                                                                           *
 * Description        : This routine formats combined LMR and DMR PDU        *
 *                                                                           *
 * Input(s)           : pLmDmInfo - Pointer to the LmDm structure            *
 *                      pu1RevPdu -  Pointer to Recevied LMM PDU             *
 *                                                                           *
 * Output(s)          : ppu1LmrPdu -  POinter to Pointer for LMR PDU         *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE		     *
 *****************************************************************************/
PUBLIC VOID
R6374LmDmInitFormatLmrDmrPduHdr (tR6374LmDmInfoTableEntry *pLmDmInfo,
                                 UINT1 *pu1RevPdu, UINT1 **ppu1Pdu)
{
    UINT1     au1ResVal[RFC6374_LMDM_PDU_RESERVED_LEN] = {RFC6374_INIT_VAL};
    UINT1     *pu1Pdu = NULL;
    UINT4     u4SessionId = RFC6374_INIT_VAL;
    UINT2     u2MsgLen = RFC6374_INIT_VAL;
    UINT1     u1VerFlag = RFC6374_INIT_VAL;
    UINT1     u1DFlagOTF = RFC6374_INIT_VAL;
    UINT1     u1ResTF = RFC6374_INIT_VAL; 
    UINT1     u1ResPrefTF = RFC6374_INIT_VAL; 

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
                 " Entering the R6374LmInitFormatLmrPduHdr()\r\n");

    pu1Pdu = *ppu1Pdu;

    /* Protocol Version and flag(R|T|0|0) 
     * change R value to 1 and 
     * Copy T value from Rev PDU */

    RFC6374_GET_1BYTE (u1VerFlag, pu1RevPdu);

    u1VerFlag = u1VerFlag >> RFC6374_VAL_4;
    u1VerFlag = u1VerFlag | RFC6374_PDU_RESP_FLAG;

    RFC6374_PUT_1BYTE (pu1Pdu, u1VerFlag);

    /* Put Control code */
    RFC6374_PUT_1BYTE (pu1Pdu, pLmDmInfo->u1LmDmInvalidField);

    /* Set back to Default for next Sequence */
    pLmDmInfo->u1LmDmInvalidField = RFC6374_VALIDATION_SUCCESS;

    /* move pointer to get message length field */
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_1;

    RFC6374_GET_2BYTE (u2MsgLen, pu1RevPdu);

    RFC6374_PUT_2BYTE (pu1Pdu, u2MsgLen);

    /* For now copy DFlag and OTF field */
    RFC6374_GET_1BYTE (u1DFlagOTF, pu1RevPdu);

    /* Now Put in combined LmrDmr PDU*/
    RFC6374_PUT_1BYTE (pu1Pdu,u1DFlagOTF);

    /* Responder timestamp and Responder Prefered Time Stamp Format */
    u1ResTF = pLmDmInfo->u1LmDmTSFormat; 
    u1ResTF = (UINT1) (u1ResTF << RFC6374_VAL_4);
    u1ResPrefTF =  pLmDmInfo->u1LmDmTSFormat;
    u1ResPrefTF = u1ResTF | u1ResPrefTF ;

    RFC6374_PUT_1BYTE (pu1Pdu, u1ResPrefTF); 

    /* Reserved Field - 2 byte */
    RFC6374_PUT_NBYTE(pu1Pdu, au1ResVal, RFC6374_LMDM_PDU_RESERVED_LEN);    

    /* Copy Session Identifier and DS field */
    /* Move the pu1RevPdu pointer to Session Identifier 
     * from QTF field*/
    pu1RevPdu = pu1RevPdu + RFC6374_VAL_3; 

    /* Get Session Identifer and DS field */
    RFC6374_GET_4BYTE (u4SessionId, pu1RevPdu);

    /* Put u4SessionId and DS in LmrDmr PDU */
    RFC6374_PUT_4BYTE (pu1Pdu, u4SessionId);

    *ppu1Pdu = pu1Pdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID, 
                 " Exiting from R6374LmDmInitFormatLmrDmrPduHdr()\r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374LmDmInitPutLmrDmrPduInfo                          *
 *                                                                             *
 * Description        : This routine is used to fille the timestamp            *
 *			and counter vlaues in the combined LmrDmr Pdu          *
 *                                                                             *
 * Input(s)           : pPktSmInfo - Pointer to the structure that stores the  *
 *		        information regarding Service info and MPLS path Info  *
 *                      and other information related to the functionality.    *
 *                      u4Txb -  Counter of responder                          *
 *                                                                             *
 * Output(s)          : ppu1DmrPdu -  POinter to Pointer for DMR PDU           *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE                      *
 ******************************************************************************/
PUBLIC VOID
R6374LmDmInitPutLmrDmrPduInfo (tR6374PktSmInfo * pPktSmInfo, UINT1 **ppu1Pdu, 
			       UINT4 u4Txb)
{
    tR6374ServiceConfigTableEntry  * pServiceInfo = NULL;
    tR6374LmDmInfoTableEntry  * pLmDmInfo = NULL;
    tR6374TSRepresentation    TxTimeStampb;
    UINT4                     u4TimeInterval = RFC6374_INIT_VAL;
    UINT1                     * pu1Pdu = NULL;
    UINT1                     u1ResTF = RFC6374_INIT_VAL;

    RFC6374_MEMSET(&TxTimeStampb, RFC6374_INIT_VAL, sizeof(TxTimeStampb));

    pu1Pdu = *ppu1Pdu;

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_PDUSM (pPktSmInfo);
   pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM(pPktSmInfo);
 
    u1ResTF = pLmDmInfo->u1LmDmTSFormat;

    /* Generate the current timestamp as TxTimeStampb based on
     * responder timestamp format and fill it in DMR frame */

    /* Update TimeStamp fields */
    /* Generate the current timestamp as TxTimeStampb and fill it in 
     * LmrDmr frame */
    R6374UtilGetCurrentTime (&TxTimeStampb, u1ResTF);

    /*Move the Offset to TxTimeStampb Field */
    /* Counter 1 - Fill Tx of responder */
    RFC6374_PUT_4BYTE (pu1Pdu, TxTimeStampb.u4Seconds);
    RFC6374_PUT_4BYTE (pu1Pdu, TxTimeStampb.u4NanoSeconds);

    /* counter 2  - RX counter - To be filled by the receiver */
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL);

    /* Counter 3 - copy Tx of Querier from pkt to counter 3 */
    RFC6374_PUT_4BYTE (pu1Pdu,
         pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.TxTimeStampf.u4Seconds);
    RFC6374_PUT_4BYTE (pu1Pdu,
         pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.TxTimeStampf.u4NanoSeconds);

    /* Counter 4 - copy Rx of Responder from pkt to counter 4 */
    RFC6374_PUT_4BYTE (pu1Pdu,
           pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.RxTimeStampf.u4Seconds);
    RFC6374_PUT_4BYTE (pu1Pdu,
          pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.RxTimeStampf.u4NanoSeconds);

    /* Put Counter of Responder */
    /* case if 64bit counter bit is not set */
    /* counter 1 B_TxP*/
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL); 
    RFC6374_PUT_4BYTE (pu1Pdu, u4Txb);   /*Tx of Responder*/

    /* counter 2 A_RxP*/
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL);   /*Rx of Initiator Zero*/
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL);   /*Rx of Initiator Zero*/

    /* Put Counter of Tx of Queriers 
     * Rx of Responder already in Lmm */
    /* Counter 3 A_TxP*/
    /* Tx of initiator */
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL); 
    RFC6374_PUT_4BYTE (pu1Pdu, pPktSmInfo->uPktInfo.LmmDmm.LmmPktInfo.u4TxCounterf);
    /* Counter 4 B_RxP*/
    /* Rx of Responder */
    RFC6374_PUT_4BYTE (pu1Pdu, RFC6374_INIT_VAL); 
    RFC6374_PUT_4BYTE (pu1Pdu, pPktSmInfo->uPktInfo.LmmDmm.LmmPktInfo.u4RxCounterf);

    /* If SQI TLV is required, append it to the Response */
    if(pLmDmInfo->b1IsSessQueryTlvRequired == OSIX_TRUE)
    {
        RFC6374_PUT_1BYTE(pu1Pdu,RFC6374_SQI_TLV_TYPE);
        RFC6374_PUT_1BYTE(pu1Pdu,RFC6374_SQI_TLV_LENGTH);
        /* Get and fill the adjusted value from database*/
        u4TimeInterval = (UINT4)(pServiceInfo->u2MinReceptionInterval);
        RFC6374_PUT_4BYTE(pu1Pdu, u4TimeInterval);
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374LmDmInitPutLmrDmrPduInfo: Response added with "
                      "SQI TLV with value %d\r\n",
                       pServiceInfo->u2MinReceptionInterval);
    }



    *ppu1Pdu = pu1Pdu;

    RFC6374_TRC6 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " R6374LmDmInitPutLmrDmrPduInfo: TimeStamp of querier in "
                 "sec : %x NanoSec : %x Responder Rx TimeStamp in "
                 "Sec: %x NanoSec : %x Responder of Tx TimerStamp "
                 "in Sec : %x NanoSec : %x \r\n",
                 pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.TxTimeStampf.u4Seconds,
                 pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.TxTimeStampf.u4NanoSeconds,
                 pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.RxTimeStampf.u4Seconds,
                 pPktSmInfo->uPktInfo.LmmDmm.DmmPktInfo.RxTimeStampf.u4NanoSeconds,
                 TxTimeStampb.u4Seconds, TxTimeStampb.u4NanoSeconds);
}

/*******************************************************************************
 * Function Name      : R6374CombLmDmDyadicMeasurement
 *
 * Description        : This routine initiate Dyadic measurment and does
 *                      calculation
 *
 * Input(s)           : pPktSmInfo - Pointer to the Service Related Config
 *                      pu1Pdu -  Pointer to Recevied LMM PDU
 *
 * Output(s)          : NULL
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374CombLmDmDyadicMeasurement(tR6374PktSmInfo * pPktSmInfo, UINT1 *pu1Pdu)
{
    tR6374ServiceConfigTableEntry  * pServiceInfo = NULL;
    tR6374LmInfoTableEntry         * pLmInfo = NULL;
    tR6374LmDmInfoTableEntry       * pLmDmInfo = NULL;
    tR6374RxLmrDmrPktInfo          * pLmrDmrPktInfo = NULL;
    tR6374TSRepresentation           LossLmDmRxTimeStamp;
    tR6374TSRepresentation           TotalDurationTxRx;
    tR6374TSRepresentation           TimeStamp1;
    tR6374TSRepresentation           TimeStamp2;
    tR6374TSRepresentation           TimeStamp3;
    tR6374TSRepresentation           TimeStamp4;
    tR6374NotifyInfo                 R6374LmDmNotify;
    UINT4                            u4Counter1 = RFC6374_INIT_VAL;
    UINT4                            u4Counter2 = RFC6374_INIT_VAL;
    UINT4                            u4Counter3 = RFC6374_INIT_VAL;
    UINT4                            u4Counter4 = RFC6374_INIT_VAL;
    UINT4                            u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4                            u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4                            u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4                            u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT4                            u4TxTemp = RFC6374_INIT_VAL;
    UINT4                            u4RxLmDmSessionId = RFC6374_INIT_VAL;
    UINT1                            u1RxLmmDmmVersion = RFC6374_INIT_VAL;
    UINT1                            u1RxLmmDmmDFlagOTF = RFC6374_INIT_VAL;
    UINT1                          * pu1RevPdu = NULL;
    UINT1                          * pu1CurPdu = NULL;
    INT4                             i4RetVal = RFC6374_SUCCESS;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
            " Entering the R6374CombLmDmDyadicMeasurement()\r\n");

    RFC6374_MEMSET (&LossLmDmRxTimeStamp, RFC6374_INIT_VAL,
            sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET (&TotalDurationTxRx, RFC6374_INIT_VAL ,
            sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET(&R6374LmDmNotify, RFC6374_INIT_VAL,
            sizeof(tR6374NotifyInfo));
    RFC6374_MEMSET(&TimeStamp1, RFC6374_INIT_VAL, sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET(&TimeStamp2, RFC6374_INIT_VAL, sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET(&TimeStamp3, RFC6374_INIT_VAL, sizeof(tR6374TSRepresentation));
    RFC6374_MEMSET(&TimeStamp4, RFC6374_INIT_VAL, sizeof(tR6374TSRepresentation));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfo);
    pLmrDmrPktInfo = &(pPktSmInfo->uPktInfo.LmrDmr);

    /* Retain a Copy of recevied LMM PDU Pointer */
    pu1CurPdu = pu1Pdu;
    pu1RevPdu = pu1Pdu;

    /* Move the Pointer to Counters */
    RFC6374_GET_1BYTE(u1RxLmmDmmVersion, pu1CurPdu);   /* Get Version */
    u1RxLmmDmmVersion = u1RxLmmDmmVersion >> 4;
    if (u1RxLmmDmmVersion != RFC6374_PDU_VERSION)
    {
        /* Unsupported Version in LMM */
        R6374LmDmNotify.u1TrapType = RFC6374_UNSUP_VERSION_TRAP;
        RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                pServiceInfo->au1ServiceName,
                RFC6374_STRLEN(pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        return RFC6374_FAILURE;
    }

    /* ControlCode (1Byte) + Message (2Byte) */
    pu1CurPdu = pu1CurPdu + 3;

    RFC6374_GET_1BYTE(u1RxLmmDmmDFlagOTF, pu1CurPdu);     /* Get DFlag and OTF */
    if ((u1RxLmmDmmDFlagOTF >> 4) != (RFC6374_LM_32BIT_COUNTERS | RFC6374_LM_PKT_COUNT))
    {
        /* Unsupported Data Flag */
        R6374LmDmNotify.u1TrapType = RFC6374_DATA_FORMAT_INVALID_TRAP;
        RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                pServiceInfo->au1ServiceName,
                RFC6374_STRLEN(pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        return RFC6374_FAILURE;
    }

    if(u1RxLmmDmmDFlagOTF != pLmDmInfo->u1LmDmTSFormat)
    {
        /* Unsupported Time Stamp Format */
        R6374LmDmNotify.u1TrapType = RFC6374_UNSUP_TIMESTAMP_TRAP;
        RFC6374_STRNCPY (&R6374LmDmNotify.au1ServiceName,
                pServiceInfo->au1ServiceName,
                RFC6374_STRLEN(pServiceInfo->au1ServiceName));
        R6374SendNotification (&R6374LmDmNotify);
        return RFC6374_FAILURE;
    }

    /* RTF+RPTF (1Byte) + Reserved (2Byte) */
    pu1CurPdu = pu1CurPdu + 3;

    /* Session ID (4Byte) */
    RFC6374_GET_4BYTE (u4RxLmDmSessionId, pu1CurPdu);

    /* Dyadic Stop is exeicuted */
    if ((pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_READY) &&
            (pLmDmInfo->u4RxLmDmSessionId == u4RxLmDmSessionId))
    {
        /* This Node has exeicuted STOP measurement */
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                RFC6374_DEFAULT_CONTEXT_ID,
                "R6374CombLmDmDyadicMeasurement: "
                "Dyadic Measurement for LOSS is Stopped\r\n");
        return RFC6374_FAILURE;
    }

    if(pLmDmInfo->u4RxLmDmSessionId != u4RxLmDmSessionId)
    {
        pLmDmInfo->u4RxLmDmSessionId = u4RxLmDmSessionId;
    }

    /* Get the packet Recived time */
    R6374UtilGetCurrentTime (&LossLmDmRxTimeStamp, u1RxLmmDmmDFlagOTF);

    /* Reset LM Tx count when response is received for
     * Transmitted PDU */
    RFC6374_LMDM_RESET_NO_RESP_TX_COUNT (pLmDmInfo);

    /* Get Delay Time Stamps */
    /* Get the Time Stamp from PDU's */
    /* TimeStamp 1 */
    RFC6374_GET_4BYTE (TimeStamp1.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp1.u4NanoSeconds, pu1CurPdu);

    /* TimeStamp 2 */
    RFC6374_GET_4BYTE (TimeStamp2.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp2.u4NanoSeconds, pu1CurPdu);

    /* TimeStamp 3 */
    RFC6374_GET_4BYTE (TimeStamp3.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp3.u4NanoSeconds, pu1CurPdu);

    /* TimeStamp 4 */
    RFC6374_GET_4BYTE (TimeStamp4.u4Seconds, pu1CurPdu);
    RFC6374_GET_4BYTE (TimeStamp4.u4NanoSeconds, pu1CurPdu);

    /* Get the B_Rx Time Stamp */
    if ((TimeStamp2.u4Seconds == RFC6374_INIT_VAL) ||
            (TimeStamp2.u4NanoSeconds == RFC6374_INIT_VAL))
    {
        R6374UtilGetCurrentTime (&TimeStamp2, pLmDmInfo->u1LmDmQTSFormat);
    }

    /* Keep the Value to send in next query */
    pLmDmInfo->TxTimeStampf.u4Seconds = TimeStamp1.u4Seconds;     /* A_Tx */
    pLmDmInfo->TxTimeStampf.u4NanoSeconds = TimeStamp1.u4NanoSeconds;

    pLmDmInfo->RxTimeStampf.u4Seconds = TimeStamp2.u4Seconds;     /* B_Rx */
    pLmDmInfo->RxTimeStampf.u4NanoSeconds = TimeStamp2.u4NanoSeconds;

    /* Get Counters As this is One way get*/
    /* Counter 1 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter1, pu1CurPdu);

    /* Counter 2 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter2, pu1CurPdu);

    /* Counter 3 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter3, pu1CurPdu);

    /* Counter 4 */
    RFC6374_GET_4BYTE (u4TxTemp, pu1CurPdu);
    RFC6374_GET_4BYTE (u4Counter4, pu1CurPdu);

    if(u4Counter2 == RFC6374_INIT_VAL)
    {
        if (pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_INFERED_LMMDMM)
        {
#ifdef RFC6374STUB_WANTED
            if(pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                R6374StubGetBfdCounter (&u4Counter1, &u4Counter2);
            }
            else
#endif
            {
                i4RetVal =  R6374GetBfdPktCount(pServiceInfo,
                        &u4Counter1, &u4Counter2);
                if (i4RetVal == RFC6374_FAILURE)
                {
                    RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                            RFC6374_DEFAULT_CONTEXT_ID,
                            "R6374CombLmDmDyadicMeasurement: "
                            "Get BFD Packet Counter failure\r\n");
                    return RFC6374_FAILURE;
                }
            }
        }
        else if(pPktSmInfo->u1PduType == RFC6374_PDU_TYPE_DIRECT_LMMDMM)
        {
            R6374GetMplsPktCnt(pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                    &u4RcvTnlTx, &u4RcvTnlRx);

            if (pServiceInfo->R6374MplsParams.u1MplsPathType == MPLS_PATH_TYPE_TUNNEL)
            {
                u4Counter2 = u4RcvTnlRx;
            }
            else
            {
                u4Counter2 = u4PwOrFwrdTnlRx;
            }
        }
    }

    /* Keep the Value to send in next query */
    pLmDmInfo->u4TxCounterf = u4Counter1;     /* A_Tx */
    pLmDmInfo->u4RxCounterb = u4Counter2;     /* B_Rx */

    /* Initiate Comb LMDM */
    if ((pLmDmInfo->u4LmDmSeqCount == RFC6374_INIT_VAL) &&
            (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_READY))
    {
        /* For Dyadic Measurement TimeStamp Negotiation is not applicable */
        pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;
 
        if ((R6374LmDmInitiator(pPktSmInfo, RFC6374_LMDM_START_SESSION))
                != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                    RFC6374_DEFAULT_CONTEXT_ID,
                    "R6374CombLmDmDyadicMeasurement: "
                    "Inititate Dyadic Measurement failed for LMDM Initiator\r\n");
            return RFC6374_FAILURE;
        }
        /* Set to True As responder has initiated a Measurement */
        pLmDmInfo->b1LmDmInitResponder = RFC6374_TRUE;
    }
    else
    {
        /* Increase Seq Count for Comb LMDM */
        RFC6374_LMDM_INCR_SEQ_COUNT (pLmDmInfo);

        /* Time Stamp Initilization for Calculation for Two-way */
        /* A_Tx */
        pLmrDmrPktInfo->DmrPktInfo.TxTimeStampf.u4Seconds = TimeStamp3.u4Seconds;
        pLmrDmrPktInfo->DmrPktInfo.TxTimeStampf.u4NanoSeconds = TimeStamp3.u4NanoSeconds;

        /* B_Rx */
        pLmrDmrPktInfo->DmrPktInfo.RxTimeStampf.u4Seconds = TimeStamp4.u4Seconds;
        pLmrDmrPktInfo->DmrPktInfo.RxTimeStampf.u4NanoSeconds = TimeStamp4.u4NanoSeconds;

        /* B_Tx */
        pLmrDmrPktInfo->DmrPktInfo.TxTimeStampb.u4Seconds = TimeStamp1.u4Seconds;
        pLmrDmrPktInfo->DmrPktInfo.TxTimeStampb.u4NanoSeconds = TimeStamp1.u4NanoSeconds;

        /* A_Rx */
        pLmrDmrPktInfo->DmrPktInfo.RxTimeStampb.u4Seconds = TimeStamp2.u4Seconds;
        pLmrDmrPktInfo->DmrPktInfo.RxTimeStampb.u4NanoSeconds = TimeStamp2.u4NanoSeconds;

       /* Timestamp received from querier updated
        * to QTS format which will be used for timestamp
        * format calculation */
        pLmDmInfo->u1LmDmQTSFormat = u1RxLmmDmmDFlagOTF;

        if (R6374CalcFrameDelay (pPktSmInfo, &(pLmrDmrPktInfo->DmrPktInfo),
                    RFC6374_DM_TYPE_TWO_WAY) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                    RFC6374_DEFAULT_CONTEXT_ID, 
                    "R6374CombLmDmDyadicMeasurement: "
                    " calculation for frame delay return failure\r\n");
           /* update QTSFormat value as configured one after processing the
            * received Dyadic PDU because this format will be used for 
            * own dyadic combined lmdm pdu transmission */
            pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;
            return RFC6374_FAILURE; 
        }

        /* Dyadic Calculation for recevier node */
        /* Packet Counter Assignement */
        pLmrDmrPktInfo->LmrPktInfo.u4TxCounterf = u4Counter1;     /* A_Tx */
        pLmrDmrPktInfo->LmrPktInfo.u4RxCounterb = u4Counter2;     /* B_Rx */
        pLmrDmrPktInfo->LmrPktInfo.u4TxCounterb = u4Counter3;     /* B_Tx */
        pLmrDmrPktInfo->LmrPktInfo.u4RxCounterf = u4Counter4;     /* A_Rx */

        /* Get the measurement time between two packets */
        if ((R6374UtilCalTimeDiff (&TimeStamp1,
                        &LossLmDmRxTimeStamp, &TotalDurationTxRx)) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                    RFC6374_DEFAULT_CONTEXT_ID,
                    "R6374CombLmDmDyadicMeasurement: "
                    "Dyadic Calculate the Time Difference failure\r\n");
            pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;
            return RFC6374_FAILURE;
        }

        if (R6374CalcFrameLoss (pPktSmInfo, TotalDurationTxRx,
                    RFC6374_LM_TYPE_TWO_WAY, pLmDmInfo->u4LmDmSessionId,
                    pLmDmInfo->u4LmDmSeqCount) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                    RFC6374_DEFAULT_CONTEXT_ID,
                    "R6374CombLmDmDyadicMeasurement: "
                    "Dyadic calculation for frame loss return failure\r\n");
            pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;
            return RFC6374_FAILURE;
        }

       /* update QTSFormat value as configured one after processing the
        * received Dyadic PDU. Since QTSFormat is updated as received QTS format
        * for calculation purpose. */
        pLmDmInfo->u1LmDmQTSFormat = pLmDmInfo->u1LmDmTSFormat;

        /* Keep the counter */
        pLmInfo->u4PreTxCounterf = pLmrDmrPktInfo->LmrPktInfo.u4TxCounterf; /* A_Tx */
        pLmInfo->u4PreRxCounterb = pLmrDmrPktInfo->LmrPktInfo.u4RxCounterb; /* B_Rx */
        pLmInfo->u4PreTxCounterb = pLmrDmrPktInfo->LmrPktInfo.u4TxCounterb; /* B_Tx */
        pLmInfo->u4PreRxCounterf = pLmrDmrPktInfo->LmrPktInfo.u4RxCounterf; /* A_Rx */

        RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                " Exiting from  R6374CombLmDmDyadicMeasurement()\r\n");

    }
    UNUSED_PARAM(pu1RevPdu);
    return RFC6374_SUCCESS;
}
#endif
