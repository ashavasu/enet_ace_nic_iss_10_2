/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: r6374utl.c,v 1.3 2017/07/25 12:00:37 siva Exp $
*
* Description: This file contains utility functions used by protocol R6374
*********************************************************************/

#ifndef __R6374UTL_C__
#define __R6374UTL_C__
#include "r6374inc.h"

VOID                getstats (UINT1 a, UINT1 b);

/****************************************************************************
 Function    :  R6374ServiceConfigTableCreate
 Input       :  None
 Description :  This function creates the FsR6374ServiceConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/

PUBLIC INT4
R6374ServiceConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tR6374ServiceConfigTableEntry, ServiceConfigTableNode);

    if ((RFC6374_SERVICECONFIG_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               R6374ServiceConfigTableRBCmp)) == NULL)
    {
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
 Function    :  R6374LmBufferTableCreate
 Input       :  None
 Description :  This function creates the FsR6374LmBufferTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/

PUBLIC INT4
R6374LmBufferTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tR6374LmBufferTableEntry, LmBufferNode);

    if ((RFC6374_LMBUFFER_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               R6374LmBufferTableRBCmp)) == NULL)
    {
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
 Function    :  R6374LmStatsTableCreate
 Input       :  None
 Description :  This function creates the FsR6374LmStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/

PUBLIC INT4
R6374LmStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tR6374LmStatsTableEntry, LmStatsNode);

    if ((RFC6374_LMSTATS_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset, R6374LmStatsTableRBCmp)) == NULL)
    {
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
 Function    :  R6374DmBufferTableCreate
 Input       :  None
 Description :  This function creates the FsR6374DmBufferTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/

PUBLIC INT4
R6374DmBufferTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tR6374DmBufferTableEntry, DmBufferNode);

    if ((RFC6374_DMBUFFER_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               R6374DmBufferTableRBCmp)) == NULL)
    {
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
 Function    :  R6374DmStatsTableCreate
 Input       :  None
 Description :  This function creates the FsR6374DmStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/

PUBLIC INT4
R6374DmStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tR6374DmStatsTableEntry, DmStatsNode);

    if ((RFC6374_DMSTATS_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset, R6374DmStatsTableRBCmp)) == NULL)
    {
        return RFC6374_FAILURE;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
 Function    :  R6374ServiceConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsR6374ServiceConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
R6374ServiceConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry1 =
        (tR6374ServiceConfigTableEntry *) pRBElem1;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry2 =
        (tR6374ServiceConfigTableEntry *) pRBElem2;

    if (STRCMP (pR6374ServiceConfigTableEntry1->au1ServiceName,
                pR6374ServiceConfigTableEntry2->au1ServiceName) > 0)
    {
        return 1;
    }
    else if (STRCMP (pR6374ServiceConfigTableEntry1->au1ServiceName,
                     pR6374ServiceConfigTableEntry2->au1ServiceName) < 0)
    {
        return -1;
    }

    if (pR6374ServiceConfigTableEntry1->u4ContextId >
        pR6374ServiceConfigTableEntry2->u4ContextId)
    {
        return 1;
    }
    else if (pR6374ServiceConfigTableEntry1->u4ContextId <
             pR6374ServiceConfigTableEntry2->u4ContextId)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  R6374LmBufferTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsR6374LmBufferTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
R6374LmBufferTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tR6374LmBufferTableEntry *pR6374LmBufferTableEntry1 =
        (tR6374LmBufferTableEntry *) pRBElem1;
    tR6374LmBufferTableEntry *pR6374LmBufferTableEntry2 =
        (tR6374LmBufferTableEntry *) pRBElem2;

    if (STRCMP (pR6374LmBufferTableEntry1->au1ServiceName,
                pR6374LmBufferTableEntry2->au1ServiceName) > 0)
    {
        return 1;
    }
    else if (STRCMP (pR6374LmBufferTableEntry1->au1ServiceName,
                     pR6374LmBufferTableEntry2->au1ServiceName) < 0)
    {
        return -1;
    }

    if (pR6374LmBufferTableEntry1->u4ContextId >
        pR6374LmBufferTableEntry2->u4ContextId)
    {
        return 1;
    }
    else if (pR6374LmBufferTableEntry1->u4ContextId <
             pR6374LmBufferTableEntry2->u4ContextId)
    {
        return -1;
    }

    if (pR6374LmBufferTableEntry1->u4SessionId >
        pR6374LmBufferTableEntry2->u4SessionId)
    {
        return 1;
    }
    else if (pR6374LmBufferTableEntry1->u4SessionId <
             pR6374LmBufferTableEntry2->u4SessionId)
    {
        return -1;
    }

    if (pR6374LmBufferTableEntry1->u4SeqCount >
        pR6374LmBufferTableEntry2->u4SeqCount)
    {
        return 1;
    }
    else if (pR6374LmBufferTableEntry1->u4SeqCount <
             pR6374LmBufferTableEntry2->u4SeqCount)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  R6374LmStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsR6374LmStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
R6374LmStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tR6374LmStatsTableEntry *pR6374LmStatsTableEntry1 =
        (tR6374LmStatsTableEntry *) pRBElem1;
    tR6374LmStatsTableEntry *pR6374LmStatsTableEntry2 =
        (tR6374LmStatsTableEntry *) pRBElem2;

    if (STRCMP (pR6374LmStatsTableEntry1->au1ServiceName,
                pR6374LmStatsTableEntry2->au1ServiceName) > 0)
    {
        return 1;
    }
    else if (STRCMP (pR6374LmStatsTableEntry1->au1ServiceName,
                     pR6374LmStatsTableEntry2->au1ServiceName) < 0)
    {
        return -1;
    }

    if (pR6374LmStatsTableEntry1->u4ContextId >
        pR6374LmStatsTableEntry2->u4ContextId)
    {
        return 1;
    }
    else if (pR6374LmStatsTableEntry1->u4ContextId <
             pR6374LmStatsTableEntry2->u4ContextId)
    {
        return -1;
    }

    if (pR6374LmStatsTableEntry1->u4SessionId >
        pR6374LmStatsTableEntry2->u4SessionId)
    {
        return 1;
    }
    else if (pR6374LmStatsTableEntry1->u4SessionId <
             pR6374LmStatsTableEntry2->u4SessionId)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  R6374DmBufferTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsR6374DmBufferTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
R6374DmBufferTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tR6374DmBufferTableEntry *pR6374DmBufferTableEntry1 =
        (tR6374DmBufferTableEntry *) pRBElem1;
    tR6374DmBufferTableEntry *pR6374DmBufferTableEntry2 =
        (tR6374DmBufferTableEntry *) pRBElem2;

    if (STRCMP (pR6374DmBufferTableEntry1->au1ServiceName,
                pR6374DmBufferTableEntry2->au1ServiceName) > 0)
    {
        return 1;
    }
    else if (STRCMP (pR6374DmBufferTableEntry1->au1ServiceName,
                     pR6374DmBufferTableEntry2->au1ServiceName) < 0)
    {
        return -1;
    }

    if (pR6374DmBufferTableEntry1->u4ContextId >
        pR6374DmBufferTableEntry2->u4ContextId)
    {
        return 1;
    }
    else if (pR6374DmBufferTableEntry1->u4ContextId <
             pR6374DmBufferTableEntry2->u4ContextId)
    {
        return -1;
    }

    if (pR6374DmBufferTableEntry1->u4SessionId >
        pR6374DmBufferTableEntry2->u4SessionId)
    {
        return 1;
    }
    else if (pR6374DmBufferTableEntry1->u4SessionId <
             pR6374DmBufferTableEntry2->u4SessionId)
    {
        return -1;
    }

    if (pR6374DmBufferTableEntry1->u4SeqCount >
        pR6374DmBufferTableEntry2->u4SeqCount)
    {
        return 1;
    }
    else if (pR6374DmBufferTableEntry1->u4SeqCount <
             pR6374DmBufferTableEntry2->u4SeqCount)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  R6374DmStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsR6374DmStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
R6374DmStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tR6374DmStatsTableEntry *pR6374DmStatsTableEntry1 =
        (tR6374DmStatsTableEntry *) pRBElem1;
    tR6374DmStatsTableEntry *pR6374DmStatsTableEntry2 =
        (tR6374DmStatsTableEntry *) pRBElem2;

    if (STRCMP (pR6374DmStatsTableEntry1->au1ServiceName,
                pR6374DmStatsTableEntry2->au1ServiceName) > 0)
    {
        return 1;
    }
    else if (STRCMP (pR6374DmStatsTableEntry1->au1ServiceName,
                     pR6374DmStatsTableEntry2->au1ServiceName) < 0)
    {
        return -1;
    }

    if (pR6374DmStatsTableEntry1->u4ContextId >
        pR6374DmStatsTableEntry2->u4ContextId)
    {
        return 1;
    }
    else if (pR6374DmStatsTableEntry1->u4ContextId <
             pR6374DmStatsTableEntry2->u4ContextId)
    {
        return -1;
    }

    if (pR6374DmStatsTableEntry1->u4SessionId >
        pR6374DmStatsTableEntry2->u4SessionId)
    {
        return 1;
    }
    else if (pR6374DmStatsTableEntry1->u4SessionId <
             pR6374DmStatsTableEntry2->u4SessionId)
    {
        return -1;
    }

    return 0;
}

/*******************************************************************************
 * Function Name      : R6374UtilGetCurrentTime
 * 
 * Description        : This routine is used to get the current time stamp
 * 
 * Input(s)           : None
 *
 * Output(s)          : pTimeStamp - Pointer to the time tamp value.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
R6374UtilGetCurrentTime (tR6374TSRepresentation * pTimeStamp, UINT1 u1TSFormat)
{
    tUtlSysPreciseTime  SysPreciseTime;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering the R6374UtilGetCurrentTime\r\n");

    RFC6374_MEMSET (&SysPreciseTime, RFC6374_INIT_VAL,
                    sizeof (tUtlSysPreciseTime));
    if (u1TSFormat == RFC6374_TS_FORMAT_IEEE1588)
    {
        /* Function used to return the system time in seconds and 
         * nanoseconds */
        UtlGetPreciseSysTime (&SysPreciseTime);
    }
    if (u1TSFormat == RFC6374_TS_FORMAT_NTP)
    {
        /* Function used to return the system time in seconds and 
         * nanoseconds */
        UtlGetNTPSysTime (&SysPreciseTime);
    }
    /* Time returned in milliseconds */
    pTimeStamp->u4Seconds = SysPreciseTime.u4Sec;
    pTimeStamp->u4NanoSeconds = SysPreciseTime.u4NanoSec;

    RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374UtilGetCurrentTime: seconds=%x, nanoseconds=%x\r\n ",
                  pTimeStamp->u4Seconds, pTimeStamp->u4NanoSeconds);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374UtilGetCurrentTime\r\n");
    return;
}

/*******************************************************************************
 * Function           : R6374UtilCalTimeDiff
 *
 * Description        : This routine is used to get the time difference between
 *                      two timestams.
 *
 * Input(s)           : pTimeOp1 - Pointer to TimeStamp operand one
 *                      pTimeOp2 -Pointer to  TimeStamp operand two
 * 
 * Output(s)          : pTimeResult - Pointer to the resultant timestamp
 * 
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374UtilCalTimeDiff (tR6374TSRepresentation * pTimeOp1,
                      tR6374TSRepresentation * pTimeOp2,
                      tR6374TSRepresentation * pTimeResult)
{
    INT4                i4NanoSeconds = RFC6374_INIT_VAL;
    UINT4               u4StoredTimeResult = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374UtilCalTimeDiff\r\n");
    /* Check for invalid timestamp values */
    if (pTimeOp2->u4Seconds < pTimeOp1->u4Seconds)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilCalTimeDiff: "
                     " invalid seconds value in operands\r\n");
        return RFC6374_FAILURE;
    }
    /* Store the Seconds value in the output timestamp to that it can be reverted
     * back in case of failure*/
    u4StoredTimeResult = pTimeResult->u4Seconds;
    pTimeResult->u4Seconds = pTimeOp2->u4Seconds - pTimeOp1->u4Seconds;
    i4NanoSeconds = (INT4) (pTimeOp2->u4NanoSeconds - pTimeOp1->u4NanoSeconds);
    /* Check if wrap wround has happened in the time stamps */
    if (i4NanoSeconds < RFC6374_INIT_VAL)
    {
        /* Check if we have enough seconds to take the offset */
        if (pTimeResult->u4Seconds == RFC6374_INIT_VAL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilCalTimeDiff: "
                         "Warp around not possible as second is zero\r\n");
            /* Revert back the no of seconds the the output timestamp */
            pTimeResult->u4Seconds = u4StoredTimeResult;
            return RFC6374_FAILURE;
        }
        /* Decrement One Second from the Seconds field */
        pTimeResult->u4Seconds--;
        pTimeResult->u4NanoSeconds = (UINT4)
            (RFC6374_NUM_OF_NSEC_IN_A_SEC + i4NanoSeconds);
    }
    else
    {
        pTimeResult->u4NanoSeconds = (UINT4) i4NanoSeconds;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC,
                 RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374UtilCalTimeDiff\r\n");
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilGetServiceFromName
 *
 * Description        : This routine is used to get Service table Info
 *
 * Input(s)           : Context ID
 *                      Service Name
 *
 * Output(s)          : none
 *
 * Return Value(s)    : ServicetableInfo - tR6374ServiceConfigTableEntry
 ******************************************************************************/
PUBLIC tR6374ServiceConfigTableEntry *
R6374UtilGetServiceFromName (UINT4 u4ContextId, UINT1 *pu1ServiceName)
{
    tR6374ServiceConfigTableEntry ServiceTableEntry;
    tR6374ServiceConfigTableEntry *pServiceTableInfo = NULL;

    MEMSET (&ServiceTableEntry, RFC6374_INIT_VAL,
            sizeof (tR6374ServiceConfigTableEntry));

    ServiceTableEntry.u4ContextId = u4ContextId;

    RFC6374_MEMCPY (ServiceTableEntry.au1ServiceName, pu1ServiceName,
                    MEM_MAX_BYTES (RFC6374_STRLEN (pu1ServiceName),
                                   RFC6374_SERVICE_NAME_MAX_LEN));

    pServiceTableInfo =
        (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & ServiceTableEntry);
    if (pServiceTableInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     u4ContextId,
                     "R6374UtilGetSessTableInfo: "
                     "Get for Session Info From RB Tree Failed\r\n");
        return NULL;
    }
    return pServiceTableInfo;
}

/******************************************************************************
 *    Function Name       : R6374CreateContext                                *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context created, this *
 *                          function post a message to RFC6374 for creating a *
 *                          context                                           *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : RFC6374_SUCCESS                                    *
 *                         RFC6374_FAILURE                                    *
 *****************************************************************************/
PUBLIC INT4
R6374CreateContext (UINT4 u4ContextId)
{
    tR6374QMsg         *pMsg = NULL;

    /* Allocating MEM Block for the Message */
    pMsg = (tR6374QMsg *) MemAllocMemBlk (RFC6374_QMSG_POOL ());
    if (pMsg == NULL)
    {
        RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_ALL_FAILURE_TRC |
                     RFC6374_OS_RESOURCE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     u4ContextId,
                     "R6374CreateContext : MEM Block Allocation Failed \r\n");
        return RFC6374_FAILURE;
    }
    RFC6374_MEMSET (pMsg, RFC6374_INIT_VAL, sizeof (tR6374QMsg));
    pMsg->u1MsgType = (tR6374MsgType) RFC6374_CREATE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    /* Sending Message and Event to own tasks */
    if (R6374QueEnqMsg (pMsg) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_CONTROL_PLANE_TRC |
                     RFC6374_ALL_FAILURE_TRC,
                     u4ContextId,
                     "R6374CreateContext : Message Queuing Failed \n");

        RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (), (UINT1 *) pMsg);
        return RFC6374_FAILURE;
    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (), (UINT1 *) pMsg);
    return RFC6374_SUCCESS;
}

/******************************************************************************
 *    Function Name       : R6374DeleteContext                                 *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context deleted, this *
 *                          function ports a Delete context Message to RFC6374   *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : RFC6374_SUCCESS                                       *
 *                         RFC6374_FAILURE                                       *
 *****************************************************************************/

PUBLIC INT4
R6374DeleteContext (UINT4 u4ContextId)
{
    tR6374QMsg         *pMsg = NULL;

    /* Allocating MEM Block for the Message */
    pMsg = (tR6374QMsg *) MemAllocMemBlk (RFC6374_QMSG_POOL ());

    if (pMsg == NULL)
    {
        RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_ALL_FAILURE_TRC |
                     RFC6374_OS_RESOURCE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     u4ContextId,
                     "R6374DeleteContext : MEM Block Allocation Failed \r\n");
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (pMsg, RFC6374_INIT_VAL, sizeof (tR6374QMsg));

    pMsg->u1MsgType = (tR6374MsgType) RFC6374_DELETE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    /* Sending Message and Event to own tasks */
    if (R6374QueEnqMsg (pMsg) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_INIT_SHUT_TRC | RFC6374_CONTROL_PLANE_TRC |
                     RFC6374_ALL_FAILURE_TRC,
                     u4ContextId,
                     "R6374DeleteContext : Message Queuing Failed \n");

        RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (), (UINT1 *) pMsg);
        return RFC6374_FAILURE;
    }
    RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (), (UINT1 *) pMsg);
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name       : R6374UtilPostTransaction                           
 *                                                                           
 * Description         : This routine is used to send the transaction start/stop
 *                       event to the R6374 Task 
 *                                                                          
 * Input(s)            : tR6374ServiceConfigTableEntry - Service Related Info
 *                       UINT1 - u1TrasType 
 *                                                                          
 * Output(s)           : None                                               
 *                                                                          
 * Returns             : RFC6374_SUCCESS                                    
 *                       RFC6374_FAILURE                                    
 ******************************************************************************/
PUBLIC INT4
R6374UtilPostTransaction (tR6374ServiceConfigTableEntry * pServiceInfo,
                          UINT1 u1TransType)
{
    tR6374QMsg         *pMsg = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering R6374UtilPostTransaction\r\n");

    if (RFC6374_ALLOC_MEM_BLOCK_MSGQ (pMsg) == NULL)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilPostTransaction:"
                     "memory allocation failure\r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }
    RFC6374_MEMSET (pMsg, RFC6374_INIT_VAL, sizeof (tR6374QMsg));

    pMsg->u1MsgType = (tR6374MsgType) u1TransType;
    pMsg->u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;
    RFC6374_STRNCPY (pMsg->au1ServiceName, pServiceInfo->au1ServiceName,
                     RFC6374_STRLEN (pServiceInfo->au1ServiceName));

    if (R6374QueEnqMsg (pMsg) == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilPostTransaction:"
                     "memory allocation failure\r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (), (UINT1 *) pMsg);
        return RFC6374_FAILURE;
    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting R6374UtilPostTransaction\r\n");

    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name       : R6374UtilGetStatsTableInfo
 *
 * Description         : This routine is used to get the Stats Table entry
 *
 * Input(s)            : tR6374PktSmInfo - Service Info and Lm Info
 *
 * Output(s)           : None
 *
 * Returns             : tR6374LmStatsTableEntry - pLmStatsTableInfo
 ******************************************************************************/
PUBLIC tR6374LmStatsTableEntry *
R6374UtilGetLmStatsTableInfo (tR6374ServiceConfigTableEntry * pServiceInfo,
                              UINT4 u4CurrentLmSeesionId)
{
    tR6374LmStatsTableEntry *pLmStatsTableInfo = NULL;
    tR6374LmStatsTableEntry TempLmStatsTableEntry;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering R6374UtilGetLmStatsTableInfo\r\n");

    RFC6374_MEMSET (&TempLmStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));

    TempLmStatsTableEntry.u4ContextId = pServiceInfo->u4ContextId;

    TempLmStatsTableEntry.u4SessionId = u4CurrentLmSeesionId;

    RFC6374_STRCPY (TempLmStatsTableEntry.au1ServiceName,
                    pServiceInfo->au1ServiceName);

    pLmStatsTableInfo =
        (tR6374LmStatsTableEntry *) RBTreeGet (RFC6374_LMSTATS_TABLE,
                                               (tRBElem *) &
                                               TempLmStatsTableEntry);
    if (pLmStatsTableInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilGetLmStatsTableInfo: "
                     " Loss Measurement Stats Node Not Present\r\n");
        return NULL;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting R6374UtilGetLmStatsTableInfo\r\n");
    return pLmStatsTableInfo;
}

/******************************************************************************
 * Function Name       : R6374UtilGetStatsTableInfo
 *
 * Description         : This routine is used to get the Stats Table entry
 *
 * Input(s)            : pServiceInfo - Service Info 
 *
 * Output(s)           : None
 *
 * Returns             : tR6374LmStatsTableEntry - pLmStatsTableInfo
 ******************************************************************************/
PUBLIC tR6374DmStatsTableEntry *
R6374UtilGetDmStatsTableInfo (tR6374ServiceConfigTableEntry * pServiceInfo,
                              UINT4 u4CurrentDmSessionId)
{
    tR6374DmStatsTableEntry *pDmStatsTableInfo = NULL;
    tR6374DmStatsTableEntry TempDmStatsTableEntry;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering R6374UtilGetDmStatsTableInfo\r\n");

    RFC6374_MEMSET (&TempDmStatsTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));

    TempDmStatsTableEntry.u4ContextId = pServiceInfo->u4ContextId;

    TempDmStatsTableEntry.u4SessionId = u4CurrentDmSessionId;

    RFC6374_STRCPY (TempDmStatsTableEntry.au1ServiceName,
                    pServiceInfo->au1ServiceName);

    pDmStatsTableInfo = RBTreeGet (RFC6374_DMSTATS_TABLE,
                                   (tRBElem *) & TempDmStatsTableEntry);
    if (pDmStatsTableInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilGetDmStatsTableInfo: "
                     " Delay Measurement Stats Node Not Present\r\n");
        return NULL;
    }
    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting R6374UtilGetDmStatsTableInfo\r\n");
    return pDmStatsTableInfo;
}

/******************************************************************************
 * Function Name       : R6374UtilGetServiceFromMplsParams
 *
 * Description         : This routine is used to get the Stats Table entry
 *                       Info based on MPLS Path Info
 *
 * Input(s)            : tR6374MplsParams - MPLS path Info 
 *
 * Output(s)           : None
 *
 * Returns             : tR6374ServiceConfigTableEntry - pR6374ServiceInfo
 ******************************************************************************/
PUBLIC tR6374ServiceConfigTableEntry *
R6374UtilGetServiceFromMplsParams (tR6374MplsParams * pMplsPathParams)
{
    tR6374ServiceConfigTableEntry *pR6374ServiceInfo = NULL;
    tR6374ServiceConfigTableEntry R6374CurrentServiceInfo;
    BOOL1               bEntryFound = OSIX_FALSE;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering R6374UtilGetServiceTable\r\n");

    RFC6374_MEMSET (&R6374CurrentServiceInfo, RFC6374_INIT_VAL,
                    sizeof (R6374CurrentServiceInfo));
    pR6374ServiceInfo = RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);
    if (pR6374ServiceInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374UtilGetServiceTable: "
                     " Get First Node for Serivce Table Failed\r\n");
        return NULL;
    }
    do
    {
        R6374CurrentServiceInfo.u4ContextId = pR6374ServiceInfo->u4ContextId;

        RFC6374_STRCPY (R6374CurrentServiceInfo.au1ServiceName,
                        pR6374ServiceInfo->au1ServiceName);

        if (pMplsPathParams->u1MplsPathType == MPLS_PATH_TYPE_PW)
        {
            if (pR6374ServiceInfo->R6374MplsParams.PwMplsPathParams.u4PwId ==
                pMplsPathParams->PwMplsPathParams.u4PwId)
            {
                bEntryFound = OSIX_TRUE;
                break;
            }
        }
        else
        {
            if (pR6374ServiceInfo->R6374MplsParams.LspMplsPathParams.
                u4RevTunnelId ==
                pMplsPathParams->LspMplsPathParams.u4RevTunnelId)
            {
                bEntryFound = OSIX_TRUE;
                break;
            }
        }

        pR6374ServiceInfo = RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                                           (tRBElem *) &
                                           R6374CurrentServiceInfo, NULL);
        if (pR6374ServiceInfo == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilGetServiceTable: "
                         " Get Next Node for Service Table Failed\r\n");
            bEntryFound = OSIX_FALSE;
            break;
        }
    }
    while (1);

    if (bEntryFound == OSIX_TRUE)
    {
        RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "Exiting R6374UtilGetServiceTable\r\n");
        return pR6374ServiceInfo;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting R6374UtilGetServiceTable\r\n");
    return NULL;
}

/******************************************************************************
 * Function Name       : R6374UtlCreateService
 *
 * Description         : This routine is used to get return the Service Info 
 *                       Table pointer
 *
 * Input(s)            : u4ContextId - Default Context ID
 *                       pu1ServiceName - Service Name 
 *
 * Output(s)           : None
 *
 * Returns             : tR6374ServiceConfigTableEntry - pR6374ServiceInfo
 ******************************************************************************/
PUBLIC tR6374ServiceConfigTableEntry *
R6374UtlCreateService (UINT4 u4ContextId, UINT1 *pu1ServiceName)
{
    tR6374ServiceConfigTableEntry *pR6374ServiceEntryInfo = NULL;
    tR6374LmInfoTableEntry *pLmEntryInfo = NULL;
    tR6374DmInfoTableEntry *pDmEntryInfo = NULL;
    tR6374LmDmInfoTableEntry *pLmDmEntryInfo = NULL;

    UNUSED_PARAM (u4ContextId);
    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Entering R6374UtlCreateService\r\n");

    pR6374ServiceEntryInfo = (tR6374ServiceConfigTableEntry *) MemAllocMemBlk
        (RFC6374_SERVICECONFIGTABLE_POOLID);
    if (pR6374ServiceEntryInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtlCreateService: "
                     "Mempool allocation failed for pMplsApiInInfo \r\n");
        RFC6374_INCR_MEMORY_FAILURE_COUNT ();
        return NULL;
    }
    RFC6374_MEMSET (pR6374ServiceEntryInfo, 0x0,
                    sizeof (tR6374ServiceConfigTableEntry));
    pLmEntryInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pR6374ServiceEntryInfo);
    pDmEntryInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pR6374ServiceEntryInfo);
    pLmDmEntryInfo =
        RFC6374_GET_LMDM_INFO_FROM_SERVICE (pR6374ServiceEntryInfo);

    /* Default Service Values */
    /* Context ID */
    pR6374ServiceEntryInfo->u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;
    /* Service Name */
    RFC6374_MEMCPY (pR6374ServiceEntryInfo->au1ServiceName,
                    pu1ServiceName,
                    MEM_MAX_BYTES (RFC6374_STRLEN (pu1ServiceName),
                                   RFC6374_SERVICE_NAME_MAX_LEN));
    /* Time to Live */
    pR6374ServiceEntryInfo->u1TimeToLive = RFC6374_DEFAULT_TTL_VAL;
    /* Traffic Class */
    pR6374ServiceEntryInfo->u1TrafficClass = RFC6374_DEFAULT_TRAFFIC_CLASS;
    /* Dyadic Measurement */
    pR6374ServiceEntryInfo->u1DyadicMeasurement =
        RFC6374_DEFAULT_DYADIC_MEASUREMENT;
    /* Dyadic Proactive Role */
    pR6374ServiceEntryInfo->u1DyadicProactiveRole =
        RFC6374_DEFAULT_DYADIC_PROACTIVE_ROLE;
    pR6374ServiceEntryInfo->u2MinReceptionInterval =
        RFC6374_DEFAULT_RECEPTION_INTERVAL;
    pR6374ServiceEntryInfo->u1SessIntQueryStatus =
        RFC6374_SESS_INT_QUERY_ENABLE;
    pR6374ServiceEntryInfo->u1QueryTransmitRetryCount =
        RFC6374_DEFAULT_QUERY_TRANSMIT_RETRYCNT;
    /* Encapsulation */
    pR6374ServiceEntryInfo->u1EncapType = RFC6374_DEFAULT_ENCAP_TYPE;

    /* Default Loss Measurement Values */
    /* Way of Measurement one/two way */
    pLmEntryInfo->u1LmType = RFC6374_LM_DEFAULT_TYPE;
    /* Method Direct/Inferred */
    pLmEntryInfo->u1LmMethod = RFC6374_LM_DEFAULT_METHOD;
    /* Mode On-Demand/Proactive */
    pLmEntryInfo->u1LmMode = RFC6374_LM_DEFAULT_MODE;
    /* On-demand Count */
    pLmEntryInfo->u2LmNoOfMessages = RFC6374_LM_DEFAULT_ONDEMAND_COUNT;
    /* Interval between two Packets */
    pLmEntryInfo->u2LmTimeInterval = RFC6374_LM_DEFAULT_INTERVAL;

    /* To hold the default/Configured query interval */
    pLmEntryInfo->u2LmConfiguredTimeInterval = RFC6374_LM_DEFAULT_INTERVAL;

    pLmEntryInfo->u1LmStatus = RFC6374_TX_STATUS_READY;

    pLmEntryInfo->u1LmTSFormat = RFC6374_LM_DEFAULT_TS_FORMAT;

    /* Default Delay Measurement Values */
    /* Way of Measurement one/two way */
    pDmEntryInfo->u1DmType = RFC6374_DM_DEFAULT_TYPE;
    /* Mode On-Demand/Proactive */
    pDmEntryInfo->u1DmMode = RFC6374_DM_DEFAULT_MODE;
    /* On-demand Count */
    pDmEntryInfo->u2DmNoOfMessages = RFC6374_DM_DEFAULT_ONDEMAND_COUNT;
    /* Interval between two Packets */
    pDmEntryInfo->u2DmTimeInterval = RFC6374_DM_DEFAULT_INTERVAL;

    /* To hold the default/Configured query interval */
    pDmEntryInfo->u2DmConfiguredTimeInterval = RFC6374_DM_DEFAULT_INTERVAL;

    pDmEntryInfo->u1DmStatus = RFC6374_TX_STATUS_READY;

    pDmEntryInfo->u1DmTSFormat = RFC6374_DM_DEFAULT_TS_FORMAT;
    /* Default Pad size zero */
    pDmEntryInfo->u4DmPadSize = RFC6374_INIT_VAL;

    /* Default Combined Loss and Delay Measurement Values */
    /* Way of Measurement one/two way */
    pLmDmEntryInfo->u1LmDmType = RFC6374_LMDM_DEFAULT_TYPE;
    /* Method Direct/Inferred */
    pLmDmEntryInfo->u1LmDmMethod = RFC6374_LMDM_DEFAULT_METHOD;
    /* Mode On-Demand/Proactive */
    pLmDmEntryInfo->u1LmDmMode = RFC6374_LMDM_DEFAULT_MODE;
    /* On-demand Count */
    pLmDmEntryInfo->u2LmDmNoOfMessages = RFC6374_LMDM_DEFAULT_ONDEMAND_COUNT;
    /* Interval between two Packets */
    pLmDmEntryInfo->u2LmDmTimeInterval = RFC6374_LMDM_DEFAULT_INTERVAL;

    /* To hold the default/Configured query interval */
    pLmDmEntryInfo->u2LmDmConfiguredTimeInterval =
        RFC6374_LMDM_DEFAULT_INTERVAL;

    pLmDmEntryInfo->u1LmDmStatus = RFC6374_TX_STATUS_READY;

    pLmDmEntryInfo->u1LmDmTSFormat = RFC6374_LMDM_DEFAULT_TS_FORMAT;
    /* Default Pad size zero */
    pLmDmEntryInfo->u4LmDmPadSize = RFC6374_INIT_VAL;

    pR6374ServiceEntryInfo->u1NegoSuppTsFormat =
        RFC6374_SUPP_TS_FORMAT_IEEE1588;

    pR6374ServiceEntryInfo->i4Negostatus = RFC6374_DEFAULT_NEGOTIATION_STATUS;

    /* Base pointer assigned to construct DM pdu */
    RFC6374_PDU = gau1RFC6374Pdu;

    if (RBTreeAdd (RFC6374_SERVICECONFIG_TABLE, pR6374ServiceEntryInfo)
        != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtlCreateService: " "Add RB Tree Failed \r\n");
        RFC6374_FREE_MEM_BLOCK (RFC6374_SERVICECONFIGTABLE_POOLID,
                                (UINT1 *) pR6374ServiceEntryInfo);

        RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "Exiting R6374UtlCreateService\r\n");
        return NULL;
    }
    /* Set Row Status */
    pR6374ServiceEntryInfo->u1RowStatus = RFC6374_ROW_STATUS_ACTIVE;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 "Exiting R6374UtlCreateService\r\n");
    return pR6374ServiceEntryInfo;
}

/******************************************************************************
 * Function Name       : R6374UtlStartSystemControl
 * 
 * Description         : This routine is used to create mem pools and RB tree
 *                       when sys control is start
 *
 * Input(s)            : None 
 *
 * Output(s)           : None
 *
 * Returns             : RFC6374_SUCCESS/RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374UtlStartSystemControl (VOID)
{
    /* Create RBTree for all the tables in R6374 module. */
    if (R6374CreateAllTables () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Create buffer pools for data structures */
    if (R6374MemInit () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    /* Initialize the Timer list for all the timers. */
    if (R6374TmrInit () == RFC6374_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    gR6374GlobalInfo.u1SystemControl[RFC6374_DEFAULT_CONTEXT_ID] =
        RFC6374_START;
    gR6374GlobalInfo.u1ModuleStatus = RFC6374_ENABLED;

    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name       : R6374UtlShutSystemControl
 * 
 * Description         : This routine is used to delete mem pools and RB tree
 *                       when sys control is shutdown
 *
 * Input(s)            : u4ContextId - Context Identifier
 *
 * Output(s)           : None
 *
 * Returns             : RFC6374_SUCCESS/RFC6374_FAILURE
 ******************************************************************************/
PUBLIC VOID
R6374UtlShutSystemControl (UINT4 u4ContextId)
{
    R6374UtlDisableModuleStatus ();
    R6374DestroyAllTables (u4ContextId);
    gR6374GlobalInfo.u1SystemControl[RFC6374_DEFAULT_CONTEXT_ID] =
        RFC6374_SHUTDOWN;
    gR6374GlobalInfo.u1ModuleStatus = RFC6374_DISABLED;
    /*Reset all the global params */
    gR6374GlobalInfo.u4TraceLevel = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u4MemoryFailureCount = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u4BufferFailureCount = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u2TrapControl = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u1LmTotalSessionCount = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u1DmTotalSessionCount = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u1LmDmTotalSessionCount = RFC6374_INIT_VAL;
    gR6374GlobalInfo.u1TotalServices = RFC6374_INIT_VAL;
    RFC6374_PDU = NULL;
}

/******************************************************************************
 * Function Name       : R6374UtlEnableModuleStatus
 * 
 * Description         : This routine is used to enable LM/DM transmission
 *                       when module status is enabled
 *
 * Input(s)            : None 
 *
 * Output(s)           : None
 *
 * Returns             : RFC6374_SUCCESS/RFC6374_FAILURE
 ******************************************************************************/
PUBLIC UINT4
R6374UtlEnableModuleStatus (VOID)
{
    /*Phase 2 : Start the Proactive LM/DM measurement */
    gR6374GlobalInfo.u1ModuleStatus = RFC6374_ENABLED;
    return RFC6374_SUCCESS;
}

/******************************************************************************
 * Function Name       : R6374UtlDisableModuleStatus
 * 
 * Description         : This routine is used to disable LM/DM transmission
 *                       when  module status is disabled
 *
 * Input(s)            : None 
 *
 * Output(s)           : None
 *
 * Returns             : None
 ******************************************************************************/
PUBLIC VOID
R6374UtlDisableModuleStatus (VOID)
{
    /* Stop the ongoing transmission of all the services */
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4TransmitStatus = RFC6374_TX_STATUS_READY;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NextServiceName;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NextServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NextServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NextServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NextServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NextServiceName.pu1_OctetList = au1NextServiceName;
    NextServiceName.i4_Length = RFC6374_INIT_VAL;

    gR6374GlobalInfo.u1ModuleStatus = RFC6374_DISABLED;

    if (nmhGetFirstIndexFs6374ParamsConfigTable (&u4ContextId,
                                                 &ServiceName) != SNMP_FAILURE)
    {
        while (i4RetVal == SNMP_SUCCESS)
        {
            pServiceInfo = R6374UtilGetServiceFromName (u4ContextId,
                                                        ServiceName.
                                                        pu1_OctetList);
            if (pServiceInfo == NULL)
            {
                return;
            }
            nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                          &i4TransmitStatus);
            if (i4TransmitStatus == RFC6374_TX_STATUS_NOT_READY)
            {
                /* Variable used to update test abort status as true */
                pServiceInfo->LmInfo.b1LmTestStatus |= RFC6374_TEST_ABORTED;
                R6374LmInitStopLmTransaction (pServiceInfo);
            }
            nmhGetFs6374DMTransmitStatus (u4ContextId, &ServiceName,
                                          &i4TransmitStatus);
            if (i4TransmitStatus == RFC6374_TX_STATUS_NOT_READY)
            {
                /* Variable used to update test abort status as true */
                pServiceInfo->DmInfo.b1DmTestStatus |= RFC6374_TEST_ABORTED;
                R6374DmInitStopDmTransaction (pServiceInfo);
            }
            nmhGetFs6374CmbLMDMTransmitStatus (u4ContextId, &ServiceName,
                                               &i4TransmitStatus);
            if (i4TransmitStatus == RFC6374_TX_STATUS_NOT_READY)
            {
                /* Variable used to update test abort status as true */
                pServiceInfo->LmDmInfo.b1LmDmTestStatus |= RFC6374_TEST_ABORTED;
                R6374LmDmInitStopLmDmTransaction (pServiceInfo);
            }

            /* Get next index */
            i4RetVal = nmhGetNextIndexFs6374ParamsConfigTable (u4ContextId,
                                                               &u4NextContextId,
                                                               &ServiceName,
                                                               &NextServiceName);

            /* If valid index found */
            if (i4RetVal == SNMP_SUCCESS)
            {
                RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                                NextServiceName.pu1_OctetList,
                                NextServiceName.i4_Length);
                ServiceName.i4_Length = NextServiceName.i4_Length;
            }
        }
    }
}

/******************************************************************************
 * Function Name       : R6374UtlSecondsToDate
 * 
 * Description         : This routine is used to convert seconds to date
 *
 * Input(s)            : None 
 *
 * Output(s)           : None
 *
 * Returns             : None
 ******************************************************************************/
PUBLIC VOID
R6374UtlSecondsToDate (UINT4 u4sec, UINT1 *pu1DateFormat)
{
    tUtlTm              tm;
    UINT1              *au1Months[] = {
        (UINT1 *) "Jan", (UINT1 *) "Feb", (UINT1 *) "Mar",
        (UINT1 *) "Apr", (UINT1 *) "May", (UINT1 *) "Jun",
        (UINT1 *) "Jul", (UINT1 *) "Aug", (UINT1 *) "Sep",
        (UINT1 *) "Oct", (UINT1 *) "Nov", (UINT1 *) "Dec"
    };
    UtlGetTimeForTicks ((u4sec * RFC6374_NUM_OF_TIME_UNITS_IN_A_SEC), &tm);
    SPRINTF ((CHR1 *) pu1DateFormat, "%u %s %u %02u:%02u:%02u",
             tm.tm_mday,
             au1Months[tm.tm_mon],
             tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

/****************************************************************************
 *  
 *  FUNCTION NAME    : R6374UtlFreeEntryFn
 * 
 *  DESCRIPTION      : This function releases the memory allocated to
 *                     each node of the specified Table.
 *  
 *  INPUT            : pElem - pointer to the node to be freed,
 *                     u4Arg - From which table node needs to be freed.     
 *  
 *  OUTPUT           : None.
 *
 *  RETURNS          : RFC6374_SUCCESS / RFC6374_FAILURE
 * **************************************************************************/
PUBLIC INT4
R6374UtlFreeEntryFn (tRBElem * pElem, UINT4 u4Arg)
{
    tR6374LmBufferTableEntry *pR6374LmBufferNode = NULL;
    tR6374LmStatsTableEntry *pR6374LmStatsNode = NULL;
    tR6374DmBufferTableEntry *pR6374DmBufferNode = NULL;
    tR6374DmStatsTableEntry *pR6374DmStatsNode = NULL;

    switch (u4Arg)
    {
        case RFC6374_LMBUFFER_ENTRY:
            pR6374LmBufferNode = (tR6374LmBufferTableEntry *) pElem;

            /* Release memory allocated to Loss Buffer & Stats Table */
            RFC6374_FREE_MEM_BLOCK (RFC6374_LMBUFFERTABLE_POOLID,
                                    (UINT1 *) (pR6374LmBufferNode));
            break;
        case RFC6374_LMSTATS_ENTRY:
            pR6374LmStatsNode = (tR6374LmStatsTableEntry *) pElem;

            /* Release memory allocated to Loss Buffer & Stats Table */
            RFC6374_FREE_MEM_BLOCK (RFC6374_LMSTATSTABLE_POOLID,
                                    (UINT1 *) (pR6374LmStatsNode));
            break;
        case RFC6374_DMBUFFER_ENTRY:
            pR6374DmBufferNode = (tR6374DmBufferTableEntry *) pElem;

            /* Release memory allocated to Loss Buffer & Stats Table */
            RFC6374_FREE_MEM_BLOCK (RFC6374_DMBUFFERTABLE_POOLID,
                                    (UINT1 *) (pR6374DmBufferNode));
            break;
        case RFC6374_DMSTATS_ENTRY:
            pR6374DmStatsNode = (tR6374DmStatsTableEntry *) pElem;

            /* Release memory allocated to Loss Buffer & Stats Table */
            RFC6374_FREE_MEM_BLOCK (RFC6374_DMSTATSTABLE_POOLID,
                                    (UINT1 *) (pR6374DmStatsNode));
            break;
        default:
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtlFreeEntryFn: " "Switch Default case\r\n");
            return RFC6374_SUCCESS;
    }
    return RFC6374_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : R6374SnmpLwGetDmTransactionStats
 *
 *    DESCRIPTION      : This function updates the Min/Max/Avg values.
 *
 *    INPUT            : pFdBufferFirstNode - Frame Delay buffer node for the
 *                       same Transaction.
 *                       pDmStatsNodeInfo -> Stats info structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
R6374SnmpLwGetDmTransactionStats (tR6374DmBufferTableEntry * pFdBufferFirstNode,
                                  tR6374DmStatsTableEntry * pDmStatsNodeInfo)
{
    UINT4               u4TempNanoSeconds = RFC6374_INIT_VAL;
    UINT4               u4TempDelayMax = RFC6374_INIT_VAL;
    UINT4               u4TempDelayMin = RFC6374_INIT_VAL;
    UINT4               u4PreviousValue = RFC6374_INIT_VAL;

    /* Frame Delay */
    if ((pDmStatsNodeInfo->MinPktDelayValue.u4Seconds == RFC6374_INIT_VAL) &&
        (pDmStatsNodeInfo->MaxPktDelayValue.u4Seconds == RFC6374_INIT_VAL) &&
        (pDmStatsNodeInfo->AvgPktDelayValue.u4Seconds == RFC6374_INIT_VAL) &&
        (pDmStatsNodeInfo->MinPktDelayValue.u4NanoSeconds == RFC6374_INIT_VAL)
        && (pDmStatsNodeInfo->MaxPktDelayValue.u4NanoSeconds ==
            RFC6374_INIT_VAL)
        && (pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds ==
            RFC6374_INIT_VAL))
    {
        pDmStatsNodeInfo->MinPktDelayValue.u4Seconds =
            pFdBufferFirstNode->PktDelayValue.u4Seconds;
        pDmStatsNodeInfo->MinPktDelayValue.u4NanoSeconds =
            pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;
        pDmStatsNodeInfo->MaxPktDelayValue.u4Seconds =
            pFdBufferFirstNode->PktDelayValue.u4Seconds;
        pDmStatsNodeInfo->MaxPktDelayValue.u4NanoSeconds =
            pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;
        pDmStatsNodeInfo->AvgPktDelayValue.u4Seconds =
            pFdBufferFirstNode->PktDelayValue.u4Seconds;
        pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds =
            pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;
    }
    else
    {
        u4TempNanoSeconds = (pFdBufferFirstNode->PktDelayValue.u4Seconds *
                             RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;

        u4TempDelayMax = (pDmStatsNodeInfo->MaxPktDelayValue.u4Seconds *
                          RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pDmStatsNodeInfo->MaxPktDelayValue.u4NanoSeconds;

        u4TempDelayMin = (pDmStatsNodeInfo->MinPktDelayValue.u4Seconds *
                          RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pDmStatsNodeInfo->MinPktDelayValue.u4NanoSeconds;

        if (u4TempNanoSeconds > u4TempDelayMax)
        {
            pDmStatsNodeInfo->MaxPktDelayValue.u4Seconds =
                pFdBufferFirstNode->PktDelayValue.u4Seconds;
            pDmStatsNodeInfo->MaxPktDelayValue.u4NanoSeconds =
                pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;
        }
        else
        {
            if (u4TempNanoSeconds < u4TempDelayMin)
            {
                pDmStatsNodeInfo->MinPktDelayValue.u4Seconds =
                    pFdBufferFirstNode->PktDelayValue.u4Seconds;
                pDmStatsNodeInfo->MinPktDelayValue.u4NanoSeconds =
                    pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;
            }
        }
        /* Add seconds to the Node Info seconds */
        pDmStatsNodeInfo->AvgPktDelayValue.u4Seconds +=
            pFdBufferFirstNode->PktDelayValue.u4Seconds;

        /* Store previously stored nano seconds value */
        u4PreviousValue = pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds;

        /* Add nano seconds value to Node Info nano seconds */
        pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds +=
            pFdBufferFirstNode->PktDelayValue.u4NanoSeconds;

        /* Rollover case (this is just a safety check), we generally avoid
         * hitting the if case by adding a seconds if u4NanoSeconds is 
         * greater than RFC6374_NUM_OF_NSEC_IN_A_SEC
         */
        if (u4PreviousValue > pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds)
        {
            pDmStatsNodeInfo->AvgPktDelayValue.u4Seconds += 1;
        }
        /* Nano seconds is greater than RFC6374_NUM_OF_NSEC_IN_A_SEC , add '1' second */
        if (pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds >=
            RFC6374_NUM_OF_NSEC_IN_A_SEC)
        {
            pDmStatsNodeInfo->AvgPktDelayValue.u4Seconds += 1;
            pDmStatsNodeInfo->AvgPktDelayValue.u4NanoSeconds -=
                RFC6374_NUM_OF_NSEC_IN_A_SEC;
        }

    }
    u4TempNanoSeconds = RFC6374_INIT_VAL;
    u4TempDelayMax = RFC6374_INIT_VAL;
    u4TempDelayMin = RFC6374_INIT_VAL;

    if (pDmStatsNodeInfo->u1DelayType == RFC6374_DM_TYPE_TWO_WAY)
    {

        if ((pDmStatsNodeInfo->MinPktRTDelayValue.u4Seconds == RFC6374_INIT_VAL)
            && (pDmStatsNodeInfo->MaxPktRTDelayValue.u4Seconds ==
                RFC6374_INIT_VAL)
            && (pDmStatsNodeInfo->AvgPktRTDelayValue.u4Seconds ==
                RFC6374_INIT_VAL)
            && (pDmStatsNodeInfo->MinPktRTDelayValue.u4NanoSeconds ==
                RFC6374_INIT_VAL)
            && (pDmStatsNodeInfo->MaxPktRTDelayValue.u4NanoSeconds ==
                RFC6374_INIT_VAL)
            && (pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds ==
                RFC6374_INIT_VAL))
        {
            pDmStatsNodeInfo->MinPktRTDelayValue.u4Seconds =
                pFdBufferFirstNode->PktRTDelayValue.u4Seconds;
            pDmStatsNodeInfo->MinPktRTDelayValue.u4NanoSeconds =
                pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;
            pDmStatsNodeInfo->MaxPktRTDelayValue.u4Seconds =
                pFdBufferFirstNode->PktRTDelayValue.u4Seconds;
            pDmStatsNodeInfo->MaxPktRTDelayValue.u4NanoSeconds =
                pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;
            pDmStatsNodeInfo->AvgPktRTDelayValue.u4Seconds =
                pFdBufferFirstNode->PktRTDelayValue.u4Seconds;
            pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds =
                pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;
        }
        else
        {

            /* Round Trip Delay */
            u4TempNanoSeconds = (pFdBufferFirstNode->PktRTDelayValue.u4Seconds *
                                 RFC6374_NUM_OF_NSEC_IN_A_SEC) +
                pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;

            u4TempDelayMax = (pDmStatsNodeInfo->MaxPktRTDelayValue.u4Seconds *
                              RFC6374_NUM_OF_NSEC_IN_A_SEC) +
                pDmStatsNodeInfo->MaxPktRTDelayValue.u4NanoSeconds;

            u4TempDelayMin = (pDmStatsNodeInfo->MinPktRTDelayValue.u4Seconds *
                              RFC6374_NUM_OF_NSEC_IN_A_SEC) +
                pDmStatsNodeInfo->MinPktRTDelayValue.u4NanoSeconds;

            if (u4TempNanoSeconds > u4TempDelayMax)
            {
                pDmStatsNodeInfo->MaxPktRTDelayValue.u4Seconds =
                    pFdBufferFirstNode->PktRTDelayValue.u4Seconds;
                pDmStatsNodeInfo->MaxPktRTDelayValue.u4NanoSeconds =
                    pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;
            }
            else
            {
                if (u4TempNanoSeconds < u4TempDelayMin)
                {
                    pDmStatsNodeInfo->MinPktRTDelayValue.u4Seconds =
                        pFdBufferFirstNode->PktRTDelayValue.u4Seconds;
                    pDmStatsNodeInfo->MinPktRTDelayValue.u4NanoSeconds =
                        pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;
                }
            }

            /* Add seconds to the Node Info seconds */
            pDmStatsNodeInfo->AvgPktRTDelayValue.u4Seconds +=
                pFdBufferFirstNode->PktRTDelayValue.u4Seconds;

            /* Store previously stored nano seconds value */
            u4PreviousValue =
                pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds;

            /* Add th nano seconds value to Node Info nano seconds */
            pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds +=
                pFdBufferFirstNode->PktRTDelayValue.u4NanoSeconds;

            /* Rollover case (this is just a safety check), we generally avoid
             * hitting the if case by adding a seconds if u4NanoSeconds is 
             * greater than RFC6374_NUM_OF_NSEC_IN_A_SEC
             */
            if (u4PreviousValue >
                pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds)
            {
                pDmStatsNodeInfo->AvgPktRTDelayValue.u4Seconds += 1;
            }
            /* Nano seconds is greater than RFC6374_NUM_OF_NSEC_IN_A_SEC , add '1' second */
            if (pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds >=
                RFC6374_NUM_OF_NSEC_IN_A_SEC)
            {
                pDmStatsNodeInfo->AvgPktRTDelayValue.u4Seconds += 1;
                pDmStatsNodeInfo->AvgPktRTDelayValue.u4NanoSeconds -=
                    RFC6374_NUM_OF_NSEC_IN_A_SEC;
            }

        }
    }

    if (pDmStatsNodeInfo->u1DelayType == RFC6374_DM_TYPE_ONE_WAY)
    {
        /* Inter Packt Delay */
        u4TempNanoSeconds = RFC6374_INIT_VAL;
        u4TempDelayMax = RFC6374_INIT_VAL;
        u4TempDelayMin = RFC6374_INIT_VAL;

        u4TempNanoSeconds =
            (pFdBufferFirstNode->InterPktDelayVariation.u4Seconds *
             RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pFdBufferFirstNode->InterPktDelayVariation.u4NanoSeconds;

        u4TempDelayMax =
            (pDmStatsNodeInfo->MaxInterPktDelayVariation.u4Seconds *
             RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pDmStatsNodeInfo->MaxInterPktDelayVariation.u4NanoSeconds;

        u4TempDelayMin =
            (pDmStatsNodeInfo->MinInterPktDelayVariation.u4Seconds *
             RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pDmStatsNodeInfo->MinInterPktDelayVariation.u4NanoSeconds;

        if (u4TempNanoSeconds > u4TempDelayMax)
        {
            pDmStatsNodeInfo->MaxInterPktDelayVariation.u4Seconds =
                pFdBufferFirstNode->InterPktDelayVariation.u4Seconds;
            pDmStatsNodeInfo->MaxInterPktDelayVariation.u4NanoSeconds =
                pFdBufferFirstNode->InterPktDelayVariation.u4NanoSeconds;
        }
        else
        {
            if (u4TempNanoSeconds < u4TempDelayMin)
            {
                pDmStatsNodeInfo->MinInterPktDelayVariation.u4Seconds =
                    pFdBufferFirstNode->InterPktDelayVariation.u4Seconds;
                pDmStatsNodeInfo->MinInterPktDelayVariation.u4NanoSeconds =
                    pFdBufferFirstNode->InterPktDelayVariation.u4NanoSeconds;
            }
        }

        pDmStatsNodeInfo->AvgInterPktDelayVariation.u4Seconds +=
            pFdBufferFirstNode->InterPktDelayVariation.u4Seconds;
        pDmStatsNodeInfo->AvgInterPktDelayVariation.u4NanoSeconds +=
            pFdBufferFirstNode->InterPktDelayVariation.u4NanoSeconds;

        u4TempNanoSeconds = RFC6374_INIT_VAL;
        u4TempDelayMax = RFC6374_INIT_VAL;
        u4TempDelayMin = RFC6374_INIT_VAL;

        u4TempNanoSeconds = (pFdBufferFirstNode->PktDelayVariation.u4Seconds *
                             RFC6374_NUM_OF_NSEC_IN_A_SEC)
            + pFdBufferFirstNode->PktDelayVariation.u4NanoSeconds;

        u4TempDelayMax = (pDmStatsNodeInfo->MaxPktDelayVariation.u4Seconds *
                          RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pDmStatsNodeInfo->MaxPktDelayVariation.u4NanoSeconds;

        u4TempDelayMin = (pDmStatsNodeInfo->MinPktDelayVariation.u4Seconds *
                          RFC6374_NUM_OF_NSEC_IN_A_SEC) +
            pDmStatsNodeInfo->MinPktDelayVariation.u4NanoSeconds;

        if (u4TempNanoSeconds > u4TempDelayMax)
        {
            pDmStatsNodeInfo->MaxPktDelayVariation.u4Seconds =
                pFdBufferFirstNode->PktDelayVariation.u4Seconds;
            pDmStatsNodeInfo->MaxPktDelayVariation.u4NanoSeconds =
                pFdBufferFirstNode->PktDelayVariation.u4NanoSeconds;
        }
        else
        {
            if (u4TempNanoSeconds < u4TempDelayMin)
            {
                pDmStatsNodeInfo->MinPktDelayVariation.u4Seconds =
                    pFdBufferFirstNode->PktDelayVariation.u4Seconds;
                pDmStatsNodeInfo->MinPktDelayVariation.u4NanoSeconds =
                    pFdBufferFirstNode->PktDelayVariation.u4NanoSeconds;

            }
        }

        pDmStatsNodeInfo->AvgPktDelayVariation.u4Seconds +=
            pFdBufferFirstNode->PktDelayVariation.u4Seconds;
        pDmStatsNodeInfo->AvgPktDelayVariation.u4NanoSeconds +=
            pFdBufferFirstNode->PktDelayVariation.u4NanoSeconds;
    }
}

/*****************************************************************************
 * Function Name      : R6374UtilDeleteSerConfigTableInContext               *
 *                                                                           *
 * Description        : This routine deletes Service Config Table            *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R6374UtilDeleteSerConfigTableInContext (UINT4 u4ContextId)
{
    tR6374ServiceConfigTableEntry *pServiceConfigEntry = NULL;
    tR6374ServiceConfigTableEntry *pNextServiceConfigEntry = NULL;

    pServiceConfigEntry = RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

    while (pServiceConfigEntry != NULL)
    {
        if (pServiceConfigEntry->u4ContextId == u4ContextId)
        {
            pNextServiceConfigEntry =
                RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                               (tRBElem *) pServiceConfigEntry, NULL);

            if (RBTreeRemove (gR6374GlobalInfo.ServiceConfigTable,
                              (tRBElem *) pServiceConfigEntry) == RB_FAILURE)
            {
                return;
            }
            MemReleaseMemBlock (RFC6374_SERVICECONFIGTABLE_POOLID,
                                (UINT1 *) pServiceConfigEntry);
            pServiceConfigEntry = NULL;
        }
        pServiceConfigEntry = pNextServiceConfigEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : R6374UtilDeleteDmBufTableInContext                   *
 *                                                                           *
 * Description        : This routine deletes DM Buffer Table                 *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R6374UtilDeleteDmBufTableInContext (UINT4 u4ContextId)
{
    tR6374DmBufferTableEntry *pFdBuffEntry = NULL;
    tR6374DmBufferTableEntry *pNextFdBuffEntry = NULL;

    pFdBuffEntry = RBTreeGetFirst (RFC6374_DMBUFFER_TABLE);

    while (pFdBuffEntry != NULL)
    {
        if (pFdBuffEntry->u4ContextId == u4ContextId)
        {
            pNextFdBuffEntry = RBTreeGetNext (RFC6374_DMBUFFER_TABLE,
                                              (tRBElem *) pFdBuffEntry, NULL);

            if (RBTreeRemove (gR6374GlobalInfo.DmBufferTable,
                              (tRBElem *) pFdBuffEntry) == RB_FAILURE)
            {
                return;
            }
            MemReleaseMemBlock (RFC6374_DMBUFFERTABLE_POOLID,
                                (UINT1 *) pFdBuffEntry);
            pFdBuffEntry = NULL;
        }
        pFdBuffEntry = pNextFdBuffEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : R6374UtilDeleteLmBufTableInContext                   *
 *                                                                           *
 * Description        : This routine deletes LM Buffer Table                 *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R6374UtilDeleteLmBufTableInContext (UINT4 u4ContextId)
{
    tR6374LmBufferTableEntry *pLmBuffEntry = NULL;
    tR6374LmBufferTableEntry *pNextLmBuffEntry = NULL;

    pLmBuffEntry = RBTreeGetFirst (RFC6374_LMBUFFER_TABLE);

    while (pLmBuffEntry != NULL)
    {
        if (pLmBuffEntry->u4ContextId == u4ContextId)
        {
            pNextLmBuffEntry = RBTreeGetNext (RFC6374_LMBUFFER_TABLE,
                                              (tRBElem *) pLmBuffEntry, NULL);

            if (RBTreeRemove (gR6374GlobalInfo.LmBufferTable,
                              (tRBElem *) pLmBuffEntry) == RB_FAILURE)
            {
                return;
            }
            MemReleaseMemBlock (RFC6374_LMBUFFERTABLE_POOLID,
                                (UINT1 *) pLmBuffEntry);
            pLmBuffEntry = NULL;
        }
        pLmBuffEntry = pNextLmBuffEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : R6374UtilDeleteDmStatsTableInContext                 *
 *                                                                           *
 * Description        : This routine deletes DM Stats Table                  *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R6374UtilDeleteDmStatsTableInContext (UINT4 u4ContextId)
{
    tR6374DmStatsTableEntry *pFdStatsEntry = NULL;
    tR6374DmStatsTableEntry *pNextFdStatsEntry = NULL;

    pFdStatsEntry = RBTreeGetFirst (RFC6374_DMSTATS_TABLE);

    while (pFdStatsEntry != NULL)
    {
        if (pFdStatsEntry->u4ContextId == u4ContextId)
        {
            pNextFdStatsEntry = RBTreeGetNext (RFC6374_DMSTATS_TABLE,
                                               (tRBElem *) pFdStatsEntry, NULL);

            if (RBTreeRemove (gR6374GlobalInfo.DmStatsTable,
                              (tRBElem *) pFdStatsEntry) == RB_FAILURE)
            {
                return;
            }
            MemReleaseMemBlock (RFC6374_DMSTATSTABLE_POOLID,
                                (UINT1 *) pFdStatsEntry);
            pFdStatsEntry = NULL;
        }
        pFdStatsEntry = pNextFdStatsEntry;
    }
    return;
}

/*****************************************************************************
 * Function Name      : R6374UtilDeleteLmStatsTableInContext                 *
 *                                                                           *
 * Description        : This routine deletes DM Stats Table                  *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R6374UtilDeleteLmStatsTableInContext (UINT4 u4ContextId)
{
    tR6374LmStatsTableEntry *pLmStatsEntry = NULL;
    tR6374LmStatsTableEntry *pNextLmStatsEntry = NULL;

    pLmStatsEntry = RBTreeGetFirst (RFC6374_LMSTATS_TABLE);

    while (pLmStatsEntry != NULL)
    {
        if (pLmStatsEntry->u4ContextId == u4ContextId)
        {
            pNextLmStatsEntry = RBTreeGetNext (RFC6374_LMSTATS_TABLE,
                                               (tRBElem *) pLmStatsEntry, NULL);

            if (RBTreeRemove (gR6374GlobalInfo.LmStatsTable,
                              (tRBElem *) pLmStatsEntry) == RB_FAILURE)
            {
                return;
            }
            MemReleaseMemBlock (RFC6374_LMSTATSTABLE_POOLID,
                                (UINT1 *) pLmStatsEntry);
            pLmStatsEntry = NULL;
        }
        pLmStatsEntry = pNextLmStatsEntry;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : R6374UtilGetTimeFromEpoch                       */
/*                                                                          */
/*    Description         : This function returns the number of seconds     */
/*                          passed from epoch.                              */
/*    Input(s)            : None.                                           */
/*                                                                          */
/*    Output(s)           : None                                            */
/*                                                                          */
/*    Returns             : Number of seconds passed from epoch             */
/*                                                                          */
/* **************************************************************************/
PUBLIC UINT4
R6374UtilGetTimeFromEpoch (VOID)
{
    tOsixSysTime        SysTime;
    RFC6374_GET_SYS_TIME (&SysTime);
    return (SysTime / RFC6374_NUM_OF_TIME_UNITS_IN_A_SEC);
}

/*****************************************************************************
 * Function Name      : R6374UtilRmEntriesFromLmBufferForService             *
 *                                                                           *
 * Description        : This routine deletes LM Buffer Table entries when    *
 *                      service is deleted                                   *
 *                                                                           *
 * Input(s)           : ServiceName                                          *
 *                      u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 *****************************************************************************/
PUBLIC INT4
R6374UtilRmEntriesFromLmBufferForService (UINT4 u4ContextId,
                                          UINT1 *pu1ServiceName)
{
    tR6374LmBufferTableEntry *pLmBuffEntry = NULL;
    tR6374LmBufferTableEntry *pNextLmBuffEntry = NULL;
    UINT1               u1EntryFound = RFC6374_FALSE;

    pLmBuffEntry = RBTreeGetFirst (RFC6374_LMBUFFER_TABLE);

    while (pLmBuffEntry != NULL)
    {
        pNextLmBuffEntry = RBTreeGetNext (RFC6374_LMBUFFER_TABLE,
                                          (tRBElem *) pLmBuffEntry, NULL);

        if ((pLmBuffEntry->u4ContextId == u4ContextId) &&
            (RFC6374_STRCMP (pLmBuffEntry->au1ServiceName, pu1ServiceName)
             == RFC6374_INIT_VAL))
        {
            u1EntryFound = RFC6374_TRUE;
            if (RBTreeRemove (RFC6374_LMBUFFER_TABLE,
                              (tRBElem *) pLmBuffEntry) == RB_FAILURE)
            {
                return RFC6374_FAILURE;
            }

            MemReleaseMemBlock (RFC6374_LMBUFFERTABLE_POOLID,
                                (UINT1 *) pLmBuffEntry);
            pLmBuffEntry = NULL;
        }
        pLmBuffEntry = pNextLmBuffEntry;
    }

    if (u1EntryFound != RFC6374_TRUE)
    {
        return RFC6374_FAILURE;    /* No matching entry in Table */
    }

    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function Name      : R6374UtilRmEntriesFromLmStatsForService              *
 *                                                                           *
 * Description        : This routine deletes LM Stats Table entries when     *
 *                      service is deleted                                   *
 *                                                                           *
 * Input(s)           : ServiceName                                          *
 *                      u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 *****************************************************************************/
PUBLIC INT4
R6374UtilRmEntriesFromLmStatsForService (UINT4 u4ContextId,
                                         UINT1 *pu1ServiceName)
{
    tR6374LmStatsTableEntry *pLmStatsEntry = NULL;
    tR6374LmStatsTableEntry *pNextLmStatsEntry = NULL;
    UINT1               u1EntryFound = RFC6374_FALSE;

    pLmStatsEntry = RBTreeGetFirst (RFC6374_LMSTATS_TABLE);

    while (pLmStatsEntry != NULL)
    {
        pNextLmStatsEntry = RBTreeGetNext (RFC6374_LMSTATS_TABLE,
                                           (tRBElem *) pLmStatsEntry, NULL);

        if ((pLmStatsEntry->u4ContextId == u4ContextId) &&
            (RFC6374_STRCMP (pLmStatsEntry->au1ServiceName, pu1ServiceName)
             == RFC6374_INIT_VAL) &&
            (pLmStatsEntry->u1MeasurementOngoing !=
             RFC6374_MEASUREMENT_ONGOING))
        {
            u1EntryFound = RFC6374_TRUE;

            if (RBTreeRemove (RFC6374_LMSTATS_TABLE,
                              (tRBElem *) pLmStatsEntry) == RB_FAILURE)
            {
                return RFC6374_FAILURE;
            }
            MemReleaseMemBlock (RFC6374_LMSTATSTABLE_POOLID,
                                (UINT1 *) pLmStatsEntry);
            pLmStatsEntry = NULL;
        }
        pLmStatsEntry = pNextLmStatsEntry;
    }
    if (u1EntryFound != RFC6374_TRUE)
    {
        return RFC6374_FAILURE;    /* No matching entry in Table */
    }

    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function Name      : R6374UtilRmEntriesFromDmBufferForService             *
 *                                                                           *
 * Description        : This routine deletes DM Buffer Table entries when    *
 *                      service is deleted                                   *
 *                                                                           *
 * Input(s)           : ServiceName                                          *
 *                      u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 *****************************************************************************/
PUBLIC INT4
R6374UtilRmEntriesFromDmBufferForService (UINT4 u4ContextId,
                                          UINT1 *pu1ServiceName)
{
    tR6374DmBufferTableEntry *pDmBuffEntry = NULL;
    tR6374DmBufferTableEntry *pNextDmBuffEntry = NULL;
    UINT1               u1EntryFound = RFC6374_FALSE;

    pDmBuffEntry = RBTreeGetFirst (RFC6374_DMBUFFER_TABLE);

    while (pDmBuffEntry != NULL)
    {
        pNextDmBuffEntry = RBTreeGetNext (RFC6374_DMBUFFER_TABLE,
                                          (tRBElem *) pDmBuffEntry, NULL);

        if ((pDmBuffEntry->u4ContextId == u4ContextId) &&
            (RFC6374_STRCMP (pDmBuffEntry->au1ServiceName, pu1ServiceName)
             == RFC6374_INIT_VAL))
        {
            u1EntryFound = RFC6374_TRUE;
            if (RBTreeRemove (RFC6374_DMBUFFER_TABLE,
                              (tRBElem *) pDmBuffEntry) == RB_FAILURE)
            {
                return RFC6374_FAILURE;
            }
            MemReleaseMemBlock (RFC6374_DMBUFFERTABLE_POOLID,
                                (UINT1 *) pDmBuffEntry);
            pDmBuffEntry = NULL;
        }
        pDmBuffEntry = pNextDmBuffEntry;
    }
    if (u1EntryFound != RFC6374_TRUE)
    {
        return RFC6374_FAILURE;    /* No matching entry in Table */
    }

    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function Name      : R6374UtilRmEntriesFromDmStatsForService              *
 *                                                                           *
 * Description        : This routine deletes DM Stats Table entries when     *
 *                      service is deleted                                   *
 *                                                                           *
 * Input(s)           : ServiceName                                          *
 *                      u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 *****************************************************************************/
PUBLIC INT4
R6374UtilRmEntriesFromDmStatsForService (UINT4 u4ContextId,
                                         UINT1 *pu1ServiceName)
{
    tR6374DmStatsTableEntry *pDmStatsEntry = NULL;
    tR6374DmStatsTableEntry *pNextDmStatsEntry = NULL;
    UINT1               u1EntryFound = RFC6374_FALSE;

    pDmStatsEntry = RBTreeGetFirst (RFC6374_DMSTATS_TABLE);

    while (pDmStatsEntry != NULL)
    {
        pNextDmStatsEntry = RBTreeGetNext (RFC6374_DMSTATS_TABLE,
                                           (tRBElem *) pDmStatsEntry, NULL);

        if ((pDmStatsEntry->u4ContextId == u4ContextId) &&
            (RFC6374_STRCMP (pDmStatsEntry->au1ServiceName, pu1ServiceName)
             == RFC6374_INIT_VAL) &&
            (pDmStatsEntry->u1MeasurementOngoing !=
             RFC6374_MEASUREMENT_ONGOING))
        {
            u1EntryFound = RFC6374_TRUE;
            if (RBTreeRemove (RFC6374_DMSTATS_TABLE,
                              (tRBElem *) pDmStatsEntry) == RB_FAILURE)
            {
                return RFC6374_FAILURE;
            }
            MemReleaseMemBlock (RFC6374_DMSTATSTABLE_POOLID,
                                (UINT1 *) pDmStatsEntry);
            pDmStatsEntry = NULL;
        }
        pDmStatsEntry = pNextDmStatsEntry;
    }
    if (u1EntryFound != RFC6374_TRUE)
    {
        return RFC6374_FAILURE;    /* No matching entry in Table */
    }

    return RFC6374_SUCCESS;
}

/*****************************************************************************
 * Function Name      : R6374UtilGetIpEntriesForService                      *
 *                                                                           *
 * Description        : This routine validates any other service existing    *
 *                      service is configured with same source/destination   *
 *                      IP                                                   *
 *                                                                           *
 * Input(s)           : pCurrServiceInfo -> Service Strcuture                *
 *                      u4TestValFs6374IpAddr -> Ip address                  *
 *                      u4PwId  -> Pseudowire Id                             *
 *                      pu4ErrorCode -> Error code                           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 *****************************************************************************/

PUBLIC INT4
R6374UtilGetIpEntriesForService (tR6374ServiceConfigTableEntry *
                                 pCurrServiceInfo, UINT4 u4TestValFs6374IpAddr,
                                 UINT4 u4PwId, UINT4 *pu4ErrorCode)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;

    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;
    INT4                i4MplsPathTypeCurr = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT4               u4ServiceLen = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    i4MplsPathTypeCurr = pCurrServiceInfo->R6374MplsParams.u1MplsPathType;

    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4ContextId,
                                                  &ServiceName) == SNMP_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    u4NxtContextId = u4ContextId;

    RFC6374_MEMCPY (NxtServiceName.pu1_OctetList,
                    ServiceName.pu1_OctetList, ServiceName.i4_Length);

    NxtServiceName.i4_Length = ServiceName.i4_Length;

    do
    {
        u4ContextId = u4NxtContextId;

        RFC6374_MEMSET (ServiceName.pu1_OctetList, RFC6374_INIT_VAL,
                        sizeof (tSNMP_OCTET_STRING_TYPE));

        RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                        NxtServiceName.pu1_OctetList, NxtServiceName.i4_Length);

        ServiceName.i4_Length = NxtServiceName.i4_Length;

        pServiceInfo = R6374UtilGetServiceFromName (u4ContextId,
                                                    ServiceName.pu1_OctetList);

        if ((UINT4) ServiceName.i4_Length >
            (UINT4) (RFC6374_STRLEN (pCurrServiceInfo->au1ServiceName)))
        {
            u4ServiceLen = (UINT4) ServiceName.i4_Length;
        }
        else
        {
            u4ServiceLen =
                (UINT4) (RFC6374_STRLEN (pCurrServiceInfo->au1ServiceName));
        }

        if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                            pCurrServiceInfo->au1ServiceName,
                            u4ServiceLen) == RFC6374_INIT_VAL)
        {
            continue;
        }

        if (pServiceInfo != NULL)
        {
            i4MplsPathType = pServiceInfo->R6374MplsParams.u1MplsPathType;

            if ((i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW) &&
                (i4MplsPathTypeCurr == RFC6374_MPLS_PATH_TYPE_PW))
            {
                /* Pw id of a service cannot be configured as
                 * Pw Id of other service but can be configured with
                 * same destinaion IP.So throw error if it matches*/

                if ((u4TestValFs6374IpAddr) &&
                    (pServiceInfo->R6374MplsParams.uMplsPathParams.
                     MplsPwParams.u4IpAddr != RFC6374_INIT_VAL))
                {
                    if ((u4TestValFs6374IpAddr == pServiceInfo->R6374MplsParams.
                         uMplsPathParams.MplsPwParams.u4IpAddr))
                    {
                        if (u4PwId != RFC6374_INIT_VAL)
                        {
                            if (u4PwId ==
                                pServiceInfo->R6374MplsParams.uMplsPathParams.
                                MplsPwParams.u4PwId)
                            {
                                RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC,
                                              RFC6374_DEFAULT_CONTEXT_ID,
                                              "R6374UtilGetIpEntriesForService: "
                                              "service=%s with same Pw Id =%d "
                                              "and IP address \r\n",
                                              pCurrServiceInfo->au1ServiceName,
                                              pServiceInfo->R6374MplsParams.
                                              uMplsPathParams.MplsPwParams.
                                              u4PwId);
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                CLI_SET_ERR
                                    (RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_PW_IP_ID);
                                return RFC6374_FAILURE;
                            }
                        }
                    }
                }
            }
        }
    }
    while (nmhGetNextIndexFs6374ServiceConfigTable (u4ContextId,
                                                    &u4NxtContextId,
                                                    &ServiceName,
                                                    &NxtServiceName) ==
           SNMP_SUCCESS);

    return RFC6374_SUCCESS;

}

/*****************************************************************************
 * Function Name      : R6374UtilGetTunnelEntriesForService                  *
 *                                                                           *
 * Description        : This routine validates any other service existing    *
 *                      service is configured with same Fwd/Rev tunnel       *
 *                      Id                                                   *
 *                                                                           *
 * Input(s)           : pCurrServiceInfo -> Service Strcuture                *
 *                      u4TestTunnelId   -> Tunnel Id                        *
 *                      u4IpAddr  -> Ip Address                                 *
 *                      pu4ErrorCode -> Error code                           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                      *
 *****************************************************************************/
PUBLIC INT4
R6374UtilGetTunnelEntriesForService (tR6374ServiceConfigTableEntry *
                                     pCurrServiceInfo, UINT4 u4TestTunnelId,
                                     UINT4 u4RevTunnelId, UINT4 u4IpAddr,
                                     UINT4 *pu4ErrorCode)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;
    INT4                i4MplsPathTypeCurr = RFC6374_INIT_VAL;
    UINT4               u4ServiceLen = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    i4MplsPathTypeCurr = pCurrServiceInfo->R6374MplsParams.u1MplsPathType;

    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4ContextId,
                                                  &ServiceName) == SNMP_FAILURE)
    {
        return RFC6374_FAILURE;
    }

    u4NxtContextId = u4ContextId;
    RFC6374_MEMCPY (NxtServiceName.pu1_OctetList, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);
    NxtServiceName.i4_Length = ServiceName.i4_Length;

    do
    {
        u4ContextId = u4NxtContextId;

        RFC6374_MEMSET (ServiceName.pu1_OctetList, RFC6374_INIT_VAL,
                        sizeof (tSNMP_OCTET_STRING_TYPE));

        RFC6374_MEMCPY (ServiceName.pu1_OctetList, NxtServiceName.pu1_OctetList,
                        NxtServiceName.i4_Length);
        ServiceName.i4_Length = NxtServiceName.i4_Length;

        pServiceInfo = R6374UtilGetServiceFromName (u4ContextId,
                                                    ServiceName.pu1_OctetList);

        if (ServiceName.i4_Length > (INT4)
            (RFC6374_STRLEN (pCurrServiceInfo->au1ServiceName)))
        {
            u4ServiceLen = (UINT4) ServiceName.i4_Length;
        }
        else
        {
            u4ServiceLen =
                (UINT4) (RFC6374_STRLEN (pCurrServiceInfo->au1ServiceName));
        }

        /* Comparison should not be done for the same service */
        if (RFC6374_MEMCMP
            (ServiceName.pu1_OctetList, pCurrServiceInfo->au1ServiceName,
             u4ServiceLen) == RFC6374_INIT_VAL)
        {
            continue;
        }

        if (pServiceInfo != NULL)
        {
            i4MplsPathType = pServiceInfo->R6374MplsParams.u1MplsPathType;

            if ((i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL) &&
                (i4MplsPathTypeCurr == RFC6374_MPLS_PATH_TYPE_TNL))
            {
                if ((u4TestTunnelId || u4RevTunnelId) &&
                    ((pServiceInfo->R6374MplsParams.uMplsPathParams.
                      MplsLspParams.u4FrwdTunnelId != RFC6374_INIT_VAL) ||
                     (pServiceInfo->R6374MplsParams.uMplsPathParams.
                      MplsLspParams.u4RevTunnelId != RFC6374_INIT_VAL)))
                {
                    /* Two services cannot have same pair of tunnel IDs. */
                    if (u4TestTunnelId == pServiceInfo->R6374MplsParams.
                        uMplsPathParams.MplsLspParams.u4FrwdTunnelId)

                    {
                        if ((pCurrServiceInfo->R6374MplsParams.
                             LspMplsPathParams.u4RevTunnelId !=
                             RFC6374_INIT_VAL) &&
                            (pCurrServiceInfo->R6374MplsParams.
                             LspMplsPathParams.u4RevTunnelId ==
                             pServiceInfo->R6374MplsParams.
                             LspMplsPathParams.u4RevTunnelId))
                        {
                            RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC,
                                          RFC6374_DEFAULT_CONTEXT_ID,
                                          "R6374UtilGetTunnelEntriesForService: "
                                          "service=%s with same RevTunnel Id =%d\r\n",
                                          pCurrServiceInfo->au1ServiceName,
                                          pServiceInfo->R6374MplsParams.
                                          uMplsPathParams.MplsLspParams.
                                          u4RevTunnelId);
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR
                                (RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_TUNNEL_ID);
                            return RFC6374_FAILURE;
                        }
                    }

                    if (u4RevTunnelId == pServiceInfo->R6374MplsParams.
                        uMplsPathParams.MplsLspParams.u4RevTunnelId)
                    {
                        if ((pCurrServiceInfo->R6374MplsParams.
                             LspMplsPathParams.u4FrwdTunnelId !=
                             RFC6374_INIT_VAL) &&
                            (pCurrServiceInfo->R6374MplsParams.
                             LspMplsPathParams.u4FrwdTunnelId ==
                             pServiceInfo->R6374MplsParams.
                             LspMplsPathParams.u4FrwdTunnelId))
                        {
                            RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC,
                                          RFC6374_DEFAULT_CONTEXT_ID,
                                          "R6374UtilGetTunnelEntriesForService: "
                                          "service=%s with same FwdTunnel Id =%d\r\n",
                                          pCurrServiceInfo->au1ServiceName,
                                          pServiceInfo->R6374MplsParams.
                                          uMplsPathParams.MplsLspParams.
                                          u4FrwdTunnelId);
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR
                                (RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_TUNNEL_ID);
                            return RFC6374_FAILURE;
                        }
                    }
                }
            }
            if ((i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW) &&
                (i4MplsPathTypeCurr == RFC6374_MPLS_PATH_TYPE_PW))
            {
                /* Pw Id of a service should not    
                 * be configured as Pw Id of other service
                 * Also Pw IP Address of a service should not be configured
                 * as Pw IP Address of other service.
                 * So, Compare Pw Id and Address of the given service
                 * with other service also compare PW Id with other PW Id*/

                if ((u4TestTunnelId) &&
                    (pServiceInfo->R6374MplsParams.uMplsPathParams.
                     MplsPwParams.u4PwId != RFC6374_INIT_VAL))
                {
                    if ((u4IpAddr != RFC6374_INIT_VAL) &&
                        (pServiceInfo->R6374MplsParams.uMplsPathParams.
                         MplsPwParams.u4IpAddr != RFC6374_INIT_VAL))
                    {
                        if (((u4TestTunnelId == pServiceInfo->R6374MplsParams.
                              uMplsPathParams.MplsPwParams.u4PwId) &&
                             (u4IpAddr == pServiceInfo->R6374MplsParams.
                              uMplsPathParams.MplsPwParams.u4IpAddr)))
                        {
                            RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC,
                                          RFC6374_DEFAULT_CONTEXT_ID,
                                          "R6374UtilGetTunnelEntriesForService: "
                                          "service=%s with same Pw Id =%d "
                                          " and IP address \r\n",
                                          pCurrServiceInfo->au1ServiceName,
                                          pServiceInfo->R6374MplsParams.
                                          uMplsPathParams.MplsPwParams.u4PwId);
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR
                                (RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_PW_IP_ID);
                            return RFC6374_FAILURE;
                        }
                    }
                    if ((u4TestTunnelId == pServiceInfo->R6374MplsParams.
                         uMplsPathParams.MplsPwParams.u4PwId))
                    {
                        RFC6374_TRC2 (RFC6374_CONTROL_PLANE_TRC,
                                      RFC6374_DEFAULT_CONTEXT_ID,
                                      "R6374UtilGetTunnelEntriesForService: "
                                      "service=%s with same Pw Id =%d\r\n",
                                      pCurrServiceInfo->au1ServiceName,
                                      pServiceInfo->R6374MplsParams.
                                      uMplsPathParams.MplsPwParams.u4PwId);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR
                            (RFC6374_CLI_SERVICE_ALREADY_CONF_SAME_PW_IP_ID);
                        return RFC6374_FAILURE;
                    }
                }
            }
        }
    }
    while (nmhGetNextIndexFs6374ServiceConfigTable
           (u4ContextId, &u4NxtContextId, &ServiceName,
            &NxtServiceName) == SNMP_SUCCESS);

    return RFC6374_SUCCESS;

}

/*****************************************************************************
 * Function Name      : R6374UtlIsLMMeasOngoing                              *
 *                                                                           *
 * Description        : This routine will check if any LM measurement        *
 *                      is ongoing or not.                                   *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_TRUE/RFC6374_FALSE                           *
 *****************************************************************************/
PUBLIC INT4
R6374UtlIsLMMeasOngoing (UINT4 u4ContextId)
{
    tR6374LmStatsTableEntry *pLmStatsEntry = NULL;
    tR6374LmStatsTableEntry *pNextLmStatsEntry = NULL;

    pLmStatsEntry = RBTreeGetFirst (RFC6374_LMSTATS_TABLE);

    while (pLmStatsEntry != NULL)
    {
        pNextLmStatsEntry = RBTreeGetNext (RFC6374_LMSTATS_TABLE,
                                           (tRBElem *) pLmStatsEntry, NULL);

        if (pLmStatsEntry->u4ContextId == u4ContextId)
        {
            if (pLmStatsEntry->u1MeasurementOngoing ==
                RFC6374_MEASUREMENT_ONGOING)
            {
                return RFC6374_TRUE;
            }
        }
        pLmStatsEntry = pNextLmStatsEntry;
    }
    return RFC6374_FALSE;
}

/*****************************************************************************
 * Function Name      : R6374UtlIsDMMeasOngoing                              *
 *                                                                           *
 * Description        : This routine will check if any DM measurement        *
 *                      is ongoing or not.                                   *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_TRUE/RFC6374_FALSE                           *
 *****************************************************************************/
PUBLIC INT4
R6374UtlIsDMMeasOngoing (UINT4 u4ContextId)
{
    tR6374DmStatsTableEntry *pDmStatsEntry = NULL;
    tR6374DmStatsTableEntry *pNextDmStatsEntry = NULL;

    pDmStatsEntry = RBTreeGetFirst (RFC6374_DMSTATS_TABLE);

    while (pDmStatsEntry != NULL)
    {
        pNextDmStatsEntry = RBTreeGetNext (RFC6374_DMSTATS_TABLE,
                                           (tRBElem *) pDmStatsEntry, NULL);

        if (pDmStatsEntry->u4ContextId == u4ContextId)
        {
            if (pDmStatsEntry->u1MeasurementOngoing ==
                RFC6374_MEASUREMENT_ONGOING)
            {
                return RFC6374_TRUE;
            }
        }
        pDmStatsEntry = pNextDmStatsEntry;
    }
    return RFC6374_FALSE;
}

/*****************************************************************************
 * Function Name      : R6374UtilProcessPathStatus                           *
 *                                                                           *
 * Description        : This routine process the MPLS path status event      *
 *                                                                           *
 * Input(s)           : pR6374QueMsg - Queue Msg                             *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : RFC6374_TRUE/RFC6374_FALSE                           *
 *****************************************************************************/
PUBLIC INT4
R6374UtilProcessPathStatus (tR6374QMsg * pR6374QueMsg)
{
    tR6374ServiceConfigTableEntry *pServiceInfoEntry = NULL;
    tR6374ServiceConfigTableEntry *pNxtServiceInfoEntry = NULL;

    pServiceInfoEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGetFirst (RFC6374_SERVICECONFIG_TABLE);

    if (pServiceInfoEntry == NULL)
    {
        return RFC6374_FAILURE;
    }
    do
    {
        if (pServiceInfoEntry->R6374MplsParams.u1MplsPathType ==
            RFC6374_MPLS_PATH_TYPE_TNL)
        {
            if ((pR6374QueMsg->R6374MplsParams.LspMplsPathParams.
                 u4FrwdTunnelId != RFC6374_INIT_VAL)
                && (pR6374QueMsg->R6374MplsParams.LspMplsPathParams.
                    u4SrcIpAddr != RFC6374_INIT_VAL)
                && (pR6374QueMsg->R6374MplsParams.LspMplsPathParams.
                    u4DestIpAddr != RFC6374_INIT_VAL))
            {
                if ((pServiceInfoEntry->R6374MplsParams.LspMplsPathParams.
                     u4FrwdTunnelId ==
                     pR6374QueMsg->R6374MplsParams.LspMplsPathParams.
                     u4FrwdTunnelId)
                    && (pServiceInfoEntry->R6374MplsParams.LspMplsPathParams.
                        u4SrcIpAddr ==
                        pR6374QueMsg->R6374MplsParams.LspMplsPathParams.
                        u4SrcIpAddr)
                    && (pServiceInfoEntry->R6374MplsParams.LspMplsPathParams.
                        u4DestIpAddr ==
                        pR6374QueMsg->R6374MplsParams.LspMplsPathParams.
                        u4DestIpAddr))
                {
                    if (pR6374QueMsg->u1MplsTnlPwOperState ==
                        RFC6374_MPLS_PATH_DOWN)
                    {
                        pServiceInfoEntry->u1PathStatus = MPLS_PATH_STATUS_DOWN;
                    }
                    else
                    {
                        /* If Forwar/Reverse Tunnel goes Down and comes Up or 
                         * MSR happens on remote node then reset remote Session ID 
                         * for Loss/Delay/Comb LMDM */
                        pServiceInfoEntry->LmInfo.u4RxLmSessionId =
                            RFC6374_RESET;
                        pServiceInfoEntry->DmInfo.u4RxDmSessionId =
                            RFC6374_RESET;
                        pServiceInfoEntry->LmDmInfo.u4RxLmDmSessionId =
                            RFC6374_RESET;

                        pServiceInfoEntry->u1PathStatus = MPLS_PATH_STATUS_UP;

                        /* If Path UP Event is received then 
                         * check for Pro-Active and Start the test */
                        /* Start the LM/DM or Combined LmDm test if proactive
                         * is selected*/
                        R6374UtilTestStatus (pServiceInfoEntry,
                                             RFC6374_TX_STATUS_START,
                                             RFC6374_PROACTIVE_TEST_RESTARTED,
                                             RFC6374_TEST_MODE_LM_DM_ALL);
                        return RFC6374_SUCCESS;
                    }

                }

            }
        }
        else
        {
            if (pR6374QueMsg->R6374MplsParams.PwMplsPathParams.u4PwId !=
                RFC6374_INIT_VAL)
            {
                if (pServiceInfoEntry->R6374MplsParams.PwMplsPathParams.
                    u4PwId ==
                    pR6374QueMsg->R6374MplsParams.PwMplsPathParams.u4PwId)
                {
                    if (pR6374QueMsg->u1MplsTnlPwOperState ==
                        RFC6374_MPLS_PATH_DOWN)
                    {
                        pServiceInfoEntry->u1PathStatus = MPLS_PATH_STATUS_DOWN;
                    }
                    else
                    {
                        /* If Forwar/Reverse Tunnel goes Down and comes Up or
                         * MSR happens on remote node then reset remote Session ID
                         * for Loss/Delay/Comb LMDM */
                        pServiceInfoEntry->LmInfo.u4RxLmSessionId =
                            RFC6374_RESET;
                        pServiceInfoEntry->DmInfo.u4RxDmSessionId =
                            RFC6374_RESET;
                        pServiceInfoEntry->LmDmInfo.u4RxLmDmSessionId =
                            RFC6374_RESET;

                        pServiceInfoEntry->u1PathStatus = MPLS_PATH_STATUS_UP;

                        /* If Path UP Event is received then 
                         * check for Pro-Active and Start the test */
                        /* Start the LM/DM or Combined LmDm test if proactive
                         * is selected*/
                        R6374UtilTestStatus (pServiceInfoEntry,
                                             RFC6374_TX_STATUS_START,
                                             RFC6374_PROACTIVE_TEST_RESTARTED,
                                             RFC6374_TEST_MODE_LM_DM_ALL);
                        return RFC6374_SUCCESS;
                    }
                }
            }
        }
        pNxtServiceInfoEntry = RBTreeGetNext (RFC6374_SERVICECONFIG_TABLE,
                                              (tRBElem *) pServiceInfoEntry,
                                              NULL);

        pServiceInfoEntry = pNxtServiceInfoEntry;
    }
    while (pNxtServiceInfoEntry != NULL);

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilGetSessId                               *
 *                                                                             *
 * Description        : This routine is called to calculate the session id     *
 *             information based on the session type and session id   *
 *             taken from MAC address                       *
 *                                            *
 * Input(s)           : u4SessionId - Session Identifier               *
 *                      u4SessType  - Session Type (loss/delay/combined loss   *
 *                      pLmDmInfo - Pointer to Combined LmDm structure         *
 *                      pLmInfo - Pointer to Lm structure                      *
 *                      pDmInfo - Pointer to Dm structure                      *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                   *
 *******************************************************************************/
PUBLIC VOID
R6374UtilGetSessId (tR6374LmDmInfoTableEntry * pLmDmInfo,
                    tR6374LmInfoTableEntry * pLmInfo,
                    tR6374DmInfoTableEntry * pDmInfo,
                    UINT4 u4SessionId, UINT4 u4SessType)
{
    UINT4               u4TempSessId = RFC6374_INIT_VAL;
    UINT4               u4SessId = RFC6374_INIT_VAL;

    u4SessId = u4SessType;

    /* Concatenate session id with Sess Type to get 
     * the complete session id . Sess Type used to differentiate
     * the measurement type (loss[1]/delay[2]/combined loss and delay[3]*/

    while (u4SessionId > RFC6374_INIT_VAL)
    {
        u4TempSessId = (u4TempSessId * RFC6374_VAL_10) +
            (u4SessionId % RFC6374_VAL_10);

        u4SessionId = u4SessionId / RFC6374_VAL_10;
    }
    while (u4TempSessId > RFC6374_INIT_VAL)
    {
        u4SessId = (u4SessId * RFC6374_VAL_10) +
            (u4TempSessId % RFC6374_VAL_10);

        u4TempSessId = u4TempSessId / RFC6374_VAL_10;
    }
    if (pLmDmInfo != NULL)
    {
        pLmDmInfo->u4LmDmSessionId = u4SessId;
    }
    else if (pLmInfo != NULL)
    {
        pLmInfo->u4LmSessionId = u4SessId;
    }
    else if (pDmInfo != NULL)
    {
        pDmInfo->u4DmSessionId = u4SessId;
    }
}

/*******************************************************************************
 * Function Name      : R6374UtilTestStatus                               *
 *                                                                             *
 * Description        : This routine is called to start/stop/restart the       *
 *                 proactive test when LSP/PW comes up from down          *
 *                 state or when the mode is changed as proactive.        *
 *                                            *
 * Input(s)           : pServiceInfoEntry - Pointer to service structure       *
 *                      i4TestStatus - Start/Stop                              *
 *                      u1TestType - start/restart                             *
 *                      u1TestMode - LM/DM/Combined LMDM/                      *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                 *
 *******************************************************************************/
PUBLIC UINT4
R6374UtilTestStatus (tR6374ServiceConfigTableEntry * pServiceInfoEntry,
                     INT4 i4TestStatus, UINT1 u1TestType, UINT1 u1TestMode)
{
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tR6374LmDmInfoTableEntry *pLmDmInfo;
    tR6374LmInfoTableEntry *pLmInfo;
    tR6374DmInfoTableEntry *pDmInfo;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL,
                    RFC6374_SERVICE_NAME_MAX_LEN);
    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length =
        (INT4) (RFC6374_STRLEN (pServiceInfoEntry->au1ServiceName));

    RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                    pServiceInfoEntry->au1ServiceName, ServiceName.i4_Length);

    u4ContextId = pServiceInfoEntry->u4ContextId;
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfoEntry);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceInfoEntry);
    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceInfoEntry);

    if ((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
        ((u1TestMode == RFC6374_TEST_MODE_LM) ||
         (u1TestMode == RFC6374_TEST_MODE_LM_DM_ALL)))
    {
        if (nmhTestv2Fs6374LMTransmitStatus (&u4ErrorCode, u4ContextId,
                                             &ServiceName,
                                             i4TestStatus) != SNMP_SUCCESS)
        {
            u4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateTestStatus : "
                         "unable to set start/Stop for LM Pro-active test\r\n");
            return RFC6374_FAILURE;
        }
        if (nmhSetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                          i4TestStatus) != SNMP_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374UtilTestStatus : "
                         "unable to start/stop LM Pro-active test\r\n");
            return RFC6374_FAILURE;
        }
        if (u1TestType == RFC6374_PROACTIVE_TEST_RESTARTED)
        {
            /* Variable used to update proactive test status as restarted */
            pLmInfo->b1LmTestStatus |= RFC6374_PROACTIVE_TEST_RESTARTED;
            pLmInfo->u1PrevLmMode = pLmInfo->u1LmMode;
        }
    }

    if ((pDmInfo->u1DmMode == RFC6374_DM_MODE_PROACTIVE) &&
        ((u1TestMode == RFC6374_TEST_MODE_DM) ||
         (u1TestMode == RFC6374_TEST_MODE_LM_DM_ALL)))
    {
        if (nmhTestv2Fs6374DMTransmitStatus (&u4ErrorCode, u4ContextId,
                                             &ServiceName,
                                             i4TestStatus) != SNMP_SUCCESS)
        {
            u4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateTestStatus : "
                         "unable to set start/Stop for DM Pro-active test\r\n");
            return RFC6374_FAILURE;
        }
        if (nmhSetFs6374DMTransmitStatus (u4ContextId, &ServiceName,
                                          i4TestStatus) != SNMP_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374UtilTestStatus : "
                         "unable to start/stop DM Pro-active test\r\n");
            return RFC6374_FAILURE;
        }
        if (u1TestType == RFC6374_PROACTIVE_TEST_RESTARTED)
        {
            /* Variable used to update proactive test status as restarted */
            pDmInfo->b1DmTestStatus |= RFC6374_PROACTIVE_TEST_RESTARTED;
            pDmInfo->u1PrevDmMode = pDmInfo->u1DmMode;
        }
    }

    if ((pLmDmInfo->u1LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
        ((u1TestMode == RFC6374_TEST_MODE_LMDM) ||
         (u1TestMode == RFC6374_TEST_MODE_LM_DM_ALL)))
    {
        if (nmhTestv2Fs6374CmbLMDMTransmitStatus (&u4ErrorCode, u4ContextId,
                                                  &ServiceName,
                                                  i4TestStatus) != SNMP_SUCCESS)
        {
            u4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateTestStatus : "
                         "unable to set start/Stop for combined LMDM "
                         "Pro-active test\r\n");
            return RFC6374_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMTransmitStatus (u4ContextId, &ServiceName,
                                               i4TestStatus) != SNMP_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID, "R6374UtilTestStatus : "
                         "unable to start/stop combined LMDM Pro-active test\r\n");
            return RFC6374_FAILURE;
        }
        if (u1TestType == RFC6374_PROACTIVE_TEST_RESTARTED)
        {
            /* Variable used to update proactive test status as restarted */
            pLmDmInfo->b1LmDmTestStatus |= RFC6374_PROACTIVE_TEST_RESTARTED;
            pLmDmInfo->u1PrevLmDmMode = pLmDmInfo->u1LmDmMode;
        }
    }
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilValidateMplsPathParams                        *
 *                                                                             *
 * Description        : This routine is called to validate whether MPLS PATH   *
 *                      is configured to REG with MPLS TP for event            *
 *                                                                             *
 * Input(s)           : pServiceInfoEntry - Pointer to service structure       *
 *                                                                             *
 * Output(s)          : b1IfAlreadyRegister - Is service already registered    *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                        *
 ******************************************************************************/
PUBLIC UINT4
R6374UtilValidateMplsPathParams (tR6374ServiceConfigTableEntry * pServiceInfo,
                                 BOOL1 * b1IfAlreadyRegister)
{

    tR6374MplsParams   *pR6374MplsInfo = NULL;

    if (pServiceInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilValidateMplsPathParams : "
                     "Service Info Is NULL\r\n");
        return RFC6374_FAILURE;
    }

    pR6374MplsInfo = &(pServiceInfo->R6374MplsParams);

    if (pR6374MplsInfo->u1MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if ((pR6374MplsInfo->PwMplsPathParams.u4PwId == RFC6374_INIT_VAL))
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateMplsPathParams : "
                         "MPLS PW Config is NULL\r\n");
            return RFC6374_FAILURE;
        }
        else
        {
            /* service is already registered, changes in param please do de-reg
             * and Reg */
            *b1IfAlreadyRegister = RFC6374_TRUE;
        }
    }
    else
    {
        if ((pR6374MplsInfo->LspMplsPathParams.u4FrwdTunnelId ==
             RFC6374_INIT_VAL)
            || (pR6374MplsInfo->LspMplsPathParams.u4SrcIpAddr ==
                RFC6374_INIT_VAL)
            || (pR6374MplsInfo->LspMplsPathParams.u4DestIpAddr ==
                RFC6374_INIT_VAL))
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateMplsPathParams : "
                         "MPLS Tunnel Config is NULL\r\n");
            return RFC6374_FAILURE;
        }
        else
        {
            /* service is already registered, changes in param please do de-reg
             * and Reg */
            *b1IfAlreadyRegister = RFC6374_TRUE;
        }
    }
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilGetSessionStats                               *
 *                                                                             *
 * Description        : This routine is called by External module to get the   *
 *                      last Session stats Summary info for LM/DM              *
 *                                                                             *
 * Input(s)           : pR6374ReqParams - Request Params from external module  *
 *                                                                             *
 * Output(s)          : pR6374RespParams - Filled the Session Stats info       *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                        *
 ******************************************************************************/
PUBLIC UINT4
R6374UtilGetSessionStats (tR6374ReqParams * pR6374ReqParams,
                          tR6374RespParams * pR6374RespParams)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374DmStatsTableEntry *pR6374DMStatsInfo = NULL;
    tR6374LmStatsTableEntry *pR6374LMStatsInfo = NULL;
    tR6374DmStatsTableEntry R6374DMStatsTableEntryInfo;
    tR6374LmStatsTableEntry R6374LMStatsTableEntryInfo;
    tSNMP_OCTET_STRING_TYPE R6374ServiceName;
    tSNMP_OCTET_STRING_TYPE R6374DmAvgDelay;
    tSNMP_OCTET_STRING_TYPE R6374DmAvgRTDelay;
    tSNMP_OCTET_STRING_TYPE R6374DmAvgPDVDelay;
    tSNMP_OCTET_STRING_TYPE R6374DmAvgIPDVDelay;
    UINT4               u4ContextId = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1DelayAvg[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1RTDelayAvg[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1PDVDelayAvg[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1IPDVDelayAvg[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };

    RFC6374_MEMSET (&R6374DMStatsTableEntryInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374DmStatsTableEntry));
    RFC6374_MEMSET (&R6374LMStatsTableEntryInfo, RFC6374_INIT_VAL,
                    sizeof (tR6374LmStatsTableEntry));
    RFC6374_MEMSET (&R6374ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&R6374DmAvgDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&R6374DmAvgRTDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&R6374DmAvgIPDVDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&R6374DmAvgPDVDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1DelayAvg, RFC6374_INIT_VAL, sizeof (au1DelayAvg));
    RFC6374_MEMSET (au1RTDelayAvg, RFC6374_INIT_VAL, sizeof (au1RTDelayAvg));
    RFC6374_MEMSET (au1PDVDelayAvg, RFC6374_INIT_VAL, sizeof (au1PDVDelayAvg));
    RFC6374_MEMSET (au1IPDVDelayAvg, RFC6374_INIT_VAL,
                    sizeof (au1IPDVDelayAvg));

    if (pR6374RespParams == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilGetSessionStats: " "Response is NULL\r\n");
        return RFC6374_FAILURE;
    }

    R6374ServiceName.pu1_OctetList = au1ServiceName;
    R6374ServiceName.i4_Length = RFC6374_INIT_VAL;

    R6374DmAvgDelay.pu1_OctetList = au1DelayAvg;
    R6374DmAvgDelay.i4_Length = RFC6374_INIT_VAL;

    R6374DmAvgRTDelay.pu1_OctetList = au1RTDelayAvg;
    R6374DmAvgRTDelay.i4_Length = RFC6374_INIT_VAL;

    R6374DmAvgIPDVDelay.pu1_OctetList = au1IPDVDelayAvg;
    R6374DmAvgIPDVDelay.i4_Length = RFC6374_INIT_VAL;

    R6374DmAvgPDVDelay.pu1_OctetList = au1PDVDelayAvg;
    R6374DmAvgPDVDelay.i4_Length = RFC6374_INIT_VAL;

    RFC6374_MEMCPY (R6374ServiceName.pu1_OctetList,
                    pR6374ReqParams->InR6374ServiceStats.au1ServiceName,
                    RFC6374_STRLEN (pR6374ReqParams->InR6374ServiceStats.
                                    au1ServiceName));
    R6374ServiceName.i4_Length =
        (INT4) (RFC6374_STRLEN
                (pR6374ReqParams->InR6374ServiceStats.au1ServiceName));

    u4ContextId = pR6374ReqParams->u4ContextId;

    pServiceInfo = R6374UtilGetServiceFromName (u4ContextId,
                                                pR6374ReqParams->
                                                InR6374ServiceStats.
                                                au1ServiceName);
    if (pServiceInfo == NULL)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilGetSessionStats: "
                     "No Service is present with provided name\r\n");
        return RFC6374_FAILURE;
    }

    if (pR6374ReqParams->InR6374ServiceStats.u1SessionStatsType ==
        RFC6374_SESSION_TYPE_LOSS)
    {
        /* Get Stats for Loss */
        R6374LMStatsTableEntryInfo.u4ContextId = u4ContextId;
        RFC6374_MEMCPY (R6374LMStatsTableEntryInfo.au1ServiceName,
                        pR6374ReqParams->InR6374ServiceStats.au1ServiceName,
                        RFC6374_STRLEN (pR6374ReqParams->InR6374ServiceStats.
                                        au1ServiceName));
        R6374LMStatsTableEntryInfo.u4SessionId = pServiceInfo->u4LastLmSessId;

        pR6374LMStatsInfo =
            (tR6374LmStatsTableEntry *) RBTreeGet (RFC6374_LMSTATS_TABLE,
                                                   (tRBElem *) &
                                                   R6374LMStatsTableEntryInfo);
        if (pR6374LMStatsInfo == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilGetSessionStats: "
                         "LM Stats is NULL, no measurement for the service\r\n");
            return RFC6374_FAILURE;
        }
        else
        {
            pR6374RespParams->OutR6374LmStatsTable.u4ContextId =
                pR6374LMStatsInfo->u4ContextId;

            pR6374RespParams->OutR6374LmStatsTable.u1MplsPathType =
                pR6374LMStatsInfo->u1MplsPathType;

            RFC6374_MEMCPY (pR6374RespParams->OutR6374LmStatsTable.
                            au1ServiceName, pR6374LMStatsInfo->au1ServiceName,
                            RFC6374_STRLEN (pR6374LMStatsInfo->au1ServiceName));

            pR6374RespParams->OutR6374LmStatsTable.u4SessionId =
                pR6374LMStatsInfo->u4SessionId;

            if (pR6374LMStatsInfo->u1MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
            {
                pR6374RespParams->OutR6374LmStatsTable.u4ChannelId1 =
                    pR6374LMStatsInfo->u4ChannelId1;

                pR6374RespParams->OutR6374LmStatsTable.u4ChannelIpAddr1 =
                    pR6374LMStatsInfo->u4ChannelIpAddr1;
            }
            else
            {
                pR6374RespParams->OutR6374LmStatsTable.u4ChannelId1 =
                    pR6374LMStatsInfo->u4ChannelId1;

                pR6374RespParams->OutR6374LmStatsTable.u4ChannelId2 =
                    pR6374LMStatsInfo->u4ChannelId2;

                pR6374RespParams->OutR6374LmStatsTable.u4ChannelIpAddr1 =
                    pR6374LMStatsInfo->u4ChannelIpAddr1;

                pR6374RespParams->OutR6374LmStatsTable.u4ChannelIpAddr2 =
                    pR6374LMStatsInfo->u4ChannelIpAddr2;
            }

            pR6374RespParams->OutR6374LmStatsTable.u1TrafficClass =
                pR6374LMStatsInfo->u1TrafficClass;

            pR6374RespParams->OutR6374LmStatsTable.u1LossMode =
                pR6374LMStatsInfo->u1LossMode;

            pR6374RespParams->OutR6374LmStatsTable.u1LossTSFormat =
                pR6374LMStatsInfo->u1LossTSFormat;

            pR6374RespParams->OutR6374LmStatsTable.u1LossType =
                pR6374LMStatsInfo->u1LossType;

            if (pR6374LMStatsInfo->u1LossType == RFC6374_LM_TYPE_ONE_WAY)
            {
                pR6374RespParams->OutR6374LmStatsTable.u41LMRcvd =
                    pR6374LMStatsInfo->u41LMRcvd;

                pR6374RespParams->OutR6374LmStatsTable.u4MinCalcRxLossReceiver =
                    pR6374LMStatsInfo->u4MinCalcRxLossReceiver;

                pR6374RespParams->OutR6374LmStatsTable.u4MaxCalcRxLossReceiver =
                    pR6374LMStatsInfo->u4MaxCalcRxLossReceiver;

                nmhGetFs6374LMStatsNearEndLossAvg (u4ContextId,
                                                   &R6374ServiceName,
                                                   pR6374LMStatsInfo->
                                                   u4SessionId,
                                                   &pR6374RespParams->
                                                   OutR6374LmStatsTable.
                                                   u4AvgCalcRxLossReceiver);
            }
            else
            {
                pR6374RespParams->OutR6374LmStatsTable.u4LMMSent =
                    pR6374LMStatsInfo->u4LMMSent;

                if (pR6374LMStatsInfo->u1DyadicMeasurement ==
                    RFC6374_DYADIC_MEAS_ENABLE)
                {
                    pR6374RespParams->OutR6374LmStatsTable.u1DyadicMeasurement =
                        RFC6374_DYADIC_MEAS_ENABLE;

                    pR6374RespParams->OutR6374LmStatsTable.u4LMMRcvd =
                        pR6374LMStatsInfo->u4LMMRcvd;
                }
                else
                {
                    pR6374RespParams->OutR6374LmStatsTable.u1DyadicMeasurement =
                        RFC6374_DYADIC_MEAS_DISABLE;

                    pR6374RespParams->OutR6374LmStatsTable.u4LMRRcvd =
                        pR6374LMStatsInfo->u4LMRRcvd;
                }

                pR6374RespParams->OutR6374LmStatsTable.u4MinCalcTxLossSender =
                    pR6374LMStatsInfo->u4MinCalcTxLossSender;

                pR6374RespParams->OutR6374LmStatsTable.u4MaxCalcTxLossSender =
                    pR6374LMStatsInfo->u4MaxCalcTxLossSender;

                nmhGetFs6374LMStatsFarEndLossAvg (u4ContextId,
                                                  &R6374ServiceName,
                                                  pR6374LMStatsInfo->
                                                  u4SessionId,
                                                  &pR6374RespParams->
                                                  OutR6374LmStatsTable.
                                                  u4AvgCalcTxLossSender);

                pR6374RespParams->OutR6374LmStatsTable.u4MinCalcRxLossSender =
                    pR6374LMStatsInfo->u4MinCalcRxLossSender;

                pR6374RespParams->OutR6374LmStatsTable.u4MaxCalcRxLossSender =
                    pR6374LMStatsInfo->u4MaxCalcRxLossSender;

                nmhGetFs6374LMStatsNearEndLossAvg (u4ContextId,
                                                   &R6374ServiceName,
                                                   pR6374LMStatsInfo->
                                                   u4SessionId,
                                                   &pR6374RespParams->
                                                   OutR6374LmStatsTable.
                                                   u4AvgCalcRxLossSender);

                pR6374RespParams->OutR6374LmStatsTable.f4ThroughPut =
                    pR6374LMStatsInfo->f4ThroughPut;
            }
        }
    }
    else
    {
        /* Get Stats for Delay */
        R6374DMStatsTableEntryInfo.u4ContextId = pR6374ReqParams->u4ContextId;
        RFC6374_MEMCPY (R6374DMStatsTableEntryInfo.au1ServiceName,
                        pR6374ReqParams->InR6374ServiceStats.au1ServiceName,
                        RFC6374_STRLEN (pR6374ReqParams->InR6374ServiceStats.
                                        au1ServiceName));
        R6374DMStatsTableEntryInfo.u4SessionId = pServiceInfo->u4LastDmSessId;

        pR6374DMStatsInfo =
            (tR6374DmStatsTableEntry *) RBTreeGet (RFC6374_DMSTATS_TABLE,
                                                   (tRBElem *) &
                                                   R6374DMStatsTableEntryInfo);
        if (pR6374DMStatsInfo == NULL)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilGetSessionStats: "
                         "DM Stats is NULL, no measurement for the service\r\n");
            return RFC6374_FAILURE;
        }
        else
        {
            pR6374RespParams->OutR6374DmStatsTable.u4ContextId =
                pR6374DMStatsInfo->u4ContextId;

            pR6374RespParams->OutR6374DmStatsTable.u1MplsPathType =
                pR6374DMStatsInfo->u1MplsPathType;

            RFC6374_MEMCPY (pR6374RespParams->OutR6374DmStatsTable.
                            au1ServiceName, pR6374DMStatsInfo->au1ServiceName,
                            RFC6374_STRLEN (pR6374DMStatsInfo->au1ServiceName));

            pR6374RespParams->OutR6374DmStatsTable.u4SessionId =
                pR6374DMStatsInfo->u4SessionId;

            if (pR6374DMStatsInfo->u1MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
            {
                pR6374RespParams->OutR6374DmStatsTable.u4ChannelId1 =
                    pR6374DMStatsInfo->u4ChannelId1;

                pR6374RespParams->OutR6374DmStatsTable.u4ChannelIpAddr1 =
                    pR6374DMStatsInfo->u4ChannelIpAddr1;
            }
            else
            {
                pR6374RespParams->OutR6374DmStatsTable.u4ChannelId1 =
                    pR6374DMStatsInfo->u4ChannelId1;

                pR6374RespParams->OutR6374DmStatsTable.u4ChannelId2 =
                    pR6374DMStatsInfo->u4ChannelId2;

                pR6374RespParams->OutR6374DmStatsTable.u4ChannelIpAddr1 =
                    pR6374DMStatsInfo->u4ChannelIpAddr1;

                pR6374RespParams->OutR6374DmStatsTable.u4ChannelIpAddr2 =
                    pR6374DMStatsInfo->u4ChannelIpAddr2;
            }

            pR6374RespParams->OutR6374DmStatsTable.u1TrafficClass =
                pR6374DMStatsInfo->u1TrafficClass;

            pR6374RespParams->OutR6374DmStatsTable.u1DelayMode =
                pR6374DMStatsInfo->u1DelayMode;

            pR6374RespParams->OutR6374DmStatsTable.u1DelayTSFormat =
                pR6374DMStatsInfo->u1DelayTSFormat;

            pR6374RespParams->OutR6374DmStatsTable.u1DelayType =
                pR6374DMStatsInfo->u1DelayType;

            if (pR6374DMStatsInfo->u1DelayType == RFC6374_DM_TYPE_ONE_WAY)
            {
                pR6374RespParams->OutR6374DmStatsTable.u41DMRcvd =
                    pR6374DMStatsInfo->u41DMRcvd;

                /* One-way Delay */
                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MinPktDelayValue,
                                &pR6374DMStatsInfo->MinPktDelayValue,
                                sizeof (tR6374TSRepresentation));

                nmhGetFs6374DMStatsAvgDelay (u4ContextId, &R6374ServiceName,
                                             pR6374DMStatsInfo->u4SessionId,
                                             &R6374DmAvgDelay);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktDelayValue.u4Seconds,
                                   R6374DmAvgDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktDelayValue.u4NanoSeconds,
                                   R6374DmAvgDelay.pu1_OctetList);

                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MaxPktDelayValue,
                                &pR6374DMStatsInfo->MaxPktDelayValue,
                                sizeof (tR6374TSRepresentation));

                /* IPDV */
                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MinInterPktDelayVariation,
                                &pR6374DMStatsInfo->MinInterPktDelayVariation,
                                sizeof (tR6374TSRepresentation));

                nmhGetFs6374DMStatsAvgIPDV (u4ContextId, &R6374ServiceName,
                                            pR6374DMStatsInfo->u4SessionId,
                                            &R6374DmAvgIPDVDelay);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgInterPktDelayVariation.u4Seconds,
                                   R6374DmAvgIPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgInterPktDelayVariation.u4NanoSeconds,
                                   R6374DmAvgIPDVDelay.pu1_OctetList);

                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MaxInterPktDelayVariation,
                                &pR6374DMStatsInfo->MaxInterPktDelayVariation,
                                sizeof (tR6374TSRepresentation));

                /* PDV */
                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MinPktDelayVariation,
                                &pR6374DMStatsInfo->MinPktDelayVariation,
                                sizeof (tR6374TSRepresentation));

                nmhGetFs6374DMStatsAvgPDV (u4ContextId, &R6374ServiceName,
                                           pR6374DMStatsInfo->u4SessionId,
                                           &R6374DmAvgPDVDelay);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktDelayVariation.u4Seconds,
                                   R6374DmAvgPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktDelayVariation.u4NanoSeconds,
                                   R6374DmAvgPDVDelay.pu1_OctetList);

                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MaxPktDelayVariation,
                                &pR6374DMStatsInfo->MaxPktDelayVariation,
                                sizeof (tR6374TSRepresentation));
            }
            else
            {
                pR6374RespParams->OutR6374DmStatsTable.u4DMMSent =
                    pR6374DMStatsInfo->u4DMMSent;

                if (pR6374DMStatsInfo->u1DyadicMeasurement ==
                    RFC6374_DYADIC_MEAS_ENABLE)
                {
                    pR6374RespParams->OutR6374DmStatsTable.u1DyadicMeasurement =
                        RFC6374_DYADIC_MEAS_ENABLE;

                    pR6374RespParams->OutR6374DmStatsTable.u4DMMRcvd =
                        pR6374DMStatsInfo->u4DMMRcvd;
                }
                else
                {
                    pR6374RespParams->OutR6374DmStatsTable.u1DyadicMeasurement =
                        RFC6374_DYADIC_MEAS_DISABLE;

                    pR6374RespParams->OutR6374DmStatsTable.u4DMRRcvd =
                        pR6374DMStatsInfo->u4DMRRcvd;
                }

                /* Two-way Delay */
                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MinPktDelayValue,
                                &pR6374DMStatsInfo->MinPktDelayValue,
                                sizeof (tR6374TSRepresentation));

                nmhGetFs6374DMStatsAvgDelay (u4ContextId, &R6374ServiceName,
                                             pR6374DMStatsInfo->u4SessionId,
                                             &R6374DmAvgDelay);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktDelayValue.u4Seconds,
                                   R6374DmAvgDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktDelayValue.u4NanoSeconds,
                                   R6374DmAvgDelay.pu1_OctetList);

                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MaxPktDelayValue,
                                &pR6374DMStatsInfo->MaxPktDelayValue,
                                sizeof (tR6374TSRepresentation));

                /* RT Delay */
                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MinPktRTDelayValue,
                                &pR6374DMStatsInfo->MinPktRTDelayValue,
                                sizeof (tR6374TSRepresentation));

                nmhGetFs6374DMStatsAvgRTDelay (u4ContextId, &R6374ServiceName,
                                               pR6374DMStatsInfo->u4SessionId,
                                               &R6374DmAvgRTDelay);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktRTDelayValue.u4Seconds,
                                   R6374DmAvgRTDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (pR6374RespParams->OutR6374DmStatsTable.
                                   AvgPktRTDelayValue.u4NanoSeconds,
                                   R6374DmAvgRTDelay.pu1_OctetList);

                RFC6374_MEMCPY (&pR6374RespParams->OutR6374DmStatsTable.
                                MaxPktRTDelayValue,
                                &pR6374DMStatsInfo->MaxPktRTDelayValue,
                                sizeof (tR6374TSRepresentation));
            }
        }
    }
    return RFC6374_SUCCESS;
}

VOID
getstats (UINT1 a, UINT1 b)
{
    tR6374ReqParams     pR6374ReqParams;
    tR6374RespParams    R6374RespParams;

    RFC6374_MEMSET (&pR6374ReqParams, 0, sizeof (tR6374ReqParams));
    RFC6374_MEMSET (&R6374RespParams, 0, sizeof (tR6374RespParams));

    pR6374ReqParams.u4ContextId = 0;
    pR6374ReqParams.u1MsgType = RFC6374_GET_SESSION_STATS;
    if (a == 1)
    {
        RFC6374_MEMCPY (pR6374ReqParams.InR6374ServiceStats.au1ServiceName,
                        "lsp1", 4);
        pR6374ReqParams.InR6374ServiceStats.au1ServiceName[5] = '\0';
    }
    else
    {
        RFC6374_MEMCPY (pR6374ReqParams.InR6374ServiceStats.au1ServiceName,
                        "lsp2", 4);
        pR6374ReqParams.InR6374ServiceStats.au1ServiceName[5] = '\0';
    }

    if (b == 1)
    {
        pR6374ReqParams.InR6374ServiceStats.u1SessionStatsType =
            RFC6374_SESSION_TYPE_LOSS;
    }
    else
    {
        pR6374ReqParams.InR6374ServiceStats.u1SessionStatsType =
            RFC6374_SESSION_TYPE_DELAY;
    }

    if (R6374ApiHandleExtRequest (&pR6374ReqParams, &R6374RespParams) !=
        OSIX_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "getstats");
    }
    return;
}

/*******************************************************************************
 * Function Name      : R6374UtilAdjustSessionQueryInterval                    *
 *                                                                             *
 * Description        : This routine is called to adjust the Session Query     *
 *             Interval for the MPLS service according to the         *
 *             responder's reception interval.                        *
 *                                            *
 * Input(s)           : u4Fs6374ContextId                               *
 *                      pFs6374ServiceName - MPLS service name                 *
 *                      u4SessQueryInt - Reception Interval from responder     *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                           *
 *******************************************************************************/
PUBLIC UINT1
R6374UtilAdjustSessionQueryInterval (tR6374ServiceConfigTableEntry
                                     * pServiceInfo, UINT4 u4SessQueryInt,
                                     UINT1 u1TestMode)
{
    tSNMP_OCTET_STRING_TYPE R6374ServiceName;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

    RFC6374_MEMSET (&R6374ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ServiceName, 0, RFC6374_SERVICE_NAME_MAX_LEN);

    R6374ServiceName.pu1_OctetList = au1ServiceName;
    R6374ServiceName.i4_Length = RFC6374_INIT_VAL;

    R6374ServiceName.i4_Length = (INT4)
        RFC6374_STRLEN (pServiceInfo->au1ServiceName);
    RFC6374_MEMCPY (R6374ServiceName.pu1_OctetList,
                    pServiceInfo->au1ServiceName, R6374ServiceName.i4_Length);

    switch (u1TestMode)
    {
        case RFC6374_TEST_MODE_LM:
            if ((UINT4) (pServiceInfo->LmInfo.u2LmTimeInterval)
                < u4SessQueryInt)
            {
                pServiceInfo->LmInfo.u2LmTimeInterval = (UINT2) u4SessQueryInt;

                RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_MGMT_TRC,
                              RFC6374_DEFAULT_CONTEXT_ID,
                              "R6374UtilAdjustSessionQueryInterval: LmTimeInterval "
                              "adjusted to %d\r\n", u4SessQueryInt);
            }

            pServiceInfo->LmInfo.u1SessQueryIntervalState =
                RFC6374_SQI_ADJUSTED_STATE;
            break;
        case RFC6374_TEST_MODE_DM:
            if ((UINT4) (pServiceInfo->DmInfo.u2DmTimeInterval)
                < u4SessQueryInt)
            {
                pServiceInfo->DmInfo.u2DmTimeInterval = (UINT2) u4SessQueryInt;
                RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_MGMT_TRC,
                              RFC6374_DEFAULT_CONTEXT_ID,
                              "R6374UtilAdjustSessionQueryInterval: DmTimeInterval "
                              "adjusted to %d\r\n", u4SessQueryInt);
            }

            pServiceInfo->DmInfo.u1SessQueryIntervalState =
                RFC6374_SQI_ADJUSTED_STATE;
            break;
        case RFC6374_TEST_MODE_LMDM:
            if ((UINT4) (pServiceInfo->LmDmInfo.u2LmDmTimeInterval)
                < u4SessQueryInt)
            {
                pServiceInfo->LmDmInfo.u2LmDmTimeInterval =
                    (UINT2) u4SessQueryInt;
                pServiceInfo->DmInfo.u2DmTimeInterval = (UINT2) u4SessQueryInt;
                pServiceInfo->LmInfo.u2LmTimeInterval = (UINT2) u4SessQueryInt;
                RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_MGMT_TRC,
                              RFC6374_DEFAULT_CONTEXT_ID,
                              "R6374UtilAdjustSessionQueryInterval: LmDmTimeInterval "
                              "adjusted to %d\r\n", u4SessQueryInt);
            }

            pServiceInfo->LmDmInfo.u1SessQueryIntervalState =
                RFC6374_SQI_ADJUSTED_STATE;
            break;
        default:
            break;
    }
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilCheckInterQueryInterval                       *
 *                                                                             *
 * Description        : This routine is called to calculate the time interval  *
 *             between two successive queries from querier for        *
 *             particular MPLS service and checks if SQI has to be    *
 *             updated based on responder's reception interval.       *
 *                                            *
 * Input(s)           : u4Fs6374ContextId                               *
 *                      pFs6374ServiceName - MPLS service name                 *
 *                      u4OriginTimeStampSeconds                               *
 *                      u4OriginTimeStampNanoSeconds                           *
 *                      u1TestMode                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                           *
 *******************************************************************************/
PUBLIC UINT1
R6374UtilCheckInterQueryInterval (tR6374ServiceConfigTableEntry * pServiceInfo,
                                  UINT4 u4OriginTimeStampSeconds,
                                  UINT4 u4OriginTimeStampNanoSeconds,
                                  UINT1 u1TestMode)
{
    tR6374TSRepresentation CurrentOriginTime;
    tR6374TSRepresentation previousRxOriginTimeStamp;
    tR6374TSRepresentation DiffTime;
    tSNMP_OCTET_STRING_TYPE R6374ServiceName;
    FLT4                f4TimeIntervalInMilliseconds = RFC6374_INIT_VAL;
    FLT4                f4ComparingReceptionInterval = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

    RFC6374_MEMSET (&CurrentOriginTime, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&previousRxOriginTimeStamp, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&DiffTime, RFC6374_INIT_VAL,
                    sizeof (tR6374TSRepresentation));
    RFC6374_MEMSET (&R6374ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (au1ServiceName, 0, RFC6374_SERVICE_NAME_MAX_LEN);

    R6374ServiceName.pu1_OctetList = au1ServiceName;
    R6374ServiceName.i4_Length = RFC6374_INIT_VAL;

    R6374ServiceName.i4_Length = (INT4)
        RFC6374_STRLEN (pServiceInfo->au1ServiceName);
    RFC6374_MEMCPY (R6374ServiceName.pu1_OctetList,
                    pServiceInfo->au1ServiceName, R6374ServiceName.i4_Length);

    /* Calculate inter-query interval for packets other than first packet */
    CurrentOriginTime.u4Seconds = u4OriginTimeStampSeconds;
    CurrentOriginTime.u4NanoSeconds = u4OriginTimeStampNanoSeconds;

    switch (u1TestMode)
    {
        case RFC6374_TEST_MODE_LM:
            MEMCPY (&previousRxOriginTimeStamp,
                    &(pServiceInfo->LmInfo.previousRxLMOriginTimeStamp),
                    sizeof (tR6374TSRepresentation));
            break;
        case RFC6374_TEST_MODE_DM:
            MEMCPY (&previousRxOriginTimeStamp,
                    &(pServiceInfo->DmInfo.previousRxDMOriginTimeStamp),
                    sizeof (tR6374TSRepresentation));
            break;
        case RFC6374_TEST_MODE_LMDM:
            MEMCPY (&previousRxOriginTimeStamp,
                    &(pServiceInfo->LmDmInfo.previousRxLMDMOriginTimeStamp),
                    sizeof (tR6374TSRepresentation));
            break;
        default:
            break;
    }

    if ((previousRxOriginTimeStamp.u4Seconds != RFC6374_INIT_VAL) ||
        (previousRxOriginTimeStamp.u4NanoSeconds != RFC6374_INIT_VAL))
    {
        if (R6374UtilCalTimeDiff (&previousRxOriginTimeStamp,
                                  &CurrentOriginTime,
                                  &DiffTime) != RFC6374_SUCCESS)
        {
            RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         " R6374UtilCheckInterQueryInterval: "
                         "Calculate the Time Difference failure\r\n");
            return RFC6374_FAILURE;
        }
        /* Calculate time difference in milliseconds 
         *  = ((seconds*1000000000) + nanoseconds) / (1000 * 1000)*/
        f4TimeIntervalInMilliseconds =
            (FLT4) (((DiffTime.u4Seconds) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    ((DiffTime.u4NanoSeconds) / RFC6374_NUM_OF_USEC_IN_A_SEC));
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_MGMT_TRC,
                      RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374UtilCheckInterQueryInterval: Time difference between "
                      "packets %f\r\n", (DBL8) f4TimeIntervalInMilliseconds);

        /* Set if SQI TLV has to be sent and Unsupported Query error has to be set */

        /*  Comparison of the time difference between packets has to be done against
         *  minimum reception interval plus allowable increment time difference 
         *  Currently reduction of 10% of Reception Interval is accepted */
        f4ComparingReceptionInterval =
            (FLT4) ((pServiceInfo->u2MinReceptionInterval) -
                    ((RFC6374_ALLOWABLE_RECEP_INT_VARIATION *
                      pServiceInfo->u2MinReceptionInterval) /
                     RFC6374_TOTAL_PERCENT));

    }
    switch (u1TestMode)
    {
        case RFC6374_TEST_MODE_LM:

            if (f4TimeIntervalInMilliseconds < f4ComparingReceptionInterval)
            {
                pServiceInfo->LmInfo.b1IsSessQueryTlvRequired = OSIX_TRUE;
                pServiceInfo->LmInfo.u1LmInvalidField =
                    RFC6374_INVALID_QUERY_INTERVAL;
            }
            /* Update the PreviousOriginTime */
            RFC6374_MEMCPY (&(pServiceInfo->LmInfo.previousRxLMOriginTimeStamp),
                            &CurrentOriginTime,
                            sizeof (tR6374TSRepresentation));
            break;
        case RFC6374_TEST_MODE_DM:

            if (f4TimeIntervalInMilliseconds < f4ComparingReceptionInterval)
            {
                pServiceInfo->DmInfo.b1IsSessQueryTlvRequired = OSIX_TRUE;
                pServiceInfo->DmInfo.u1DmInvalidField =
                    RFC6374_INVALID_QUERY_INTERVAL;
            }
            /* Update the PreviousOriginTime */
            RFC6374_MEMCPY (&(pServiceInfo->DmInfo.previousRxDMOriginTimeStamp),
                            &CurrentOriginTime,
                            sizeof (tR6374TSRepresentation));
            break;
        case RFC6374_TEST_MODE_LMDM:

            if (f4TimeIntervalInMilliseconds < f4ComparingReceptionInterval)
            {
                pServiceInfo->LmDmInfo.b1IsSessQueryTlvRequired = OSIX_TRUE;
                pServiceInfo->LmDmInfo.u1LmDmInvalidField =
                    RFC6374_CTRL_CODE_UNSUPP_QUERY_INTERVAL;
            }
            /* Update the PreviousOriginTime */
            RFC6374_MEMCPY (&
                            (pServiceInfo->LmDmInfo.
                             previousRxLMDMOriginTimeStamp), &CurrentOriginTime,
                            sizeof (tR6374TSRepresentation));
            break;
        default:
            break;
    }

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilIncrementSessionId                            *
 *                                                                             *
 * Description        : This routine is called to increment the current        *
 *             session id and assign it for new session.              *
 *             This includes a roll-back in id when maximum           *
 *             value of session-id is reached.                        *
 *                                                        *
 * Input(s)           : pServiceInfo                                       *
 *                      SessionType                                            *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                           *
 *******************************************************************************/
PUBLIC UINT1
R6374UtilIncrementSessionId (tR6374ServiceConfigTableEntry * pServiceInfo,
                             UINT4 u4SessType)
{
    tR6374MacAddr       SwitchMacAddr;
    UINT4               u4Index = RFC6374_INIT_VAL;
    UINT4               u4SessionId = RFC6374_INIT_VAL;
    UINT4               u4TempSessionId = RFC6374_INIT_VAL;
    UINT4               u4Power = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&SwitchMacAddr, RFC6374_INIT_VAL, sizeof (tR6374MacAddr));

    /* Get current session ID */
    switch (u4SessType)
    {
        case RFC6374_VAL_1:
            u4SessionId = pServiceInfo->LmInfo.u4LmSessionId;
            break;
        case RFC6374_VAL_2:
            u4SessionId = pServiceInfo->DmInfo.u4DmSessionId;
            break;
        case RFC6374_VAL_3:
            u4SessionId = pServiceInfo->LmDmInfo.u4LmDmSessionId;
            break;
        default:
            break;
    }
    /* Remove the first decimal digit of current session id , which indicates
     * the session type. (1- LM , 2 - DM , 3 - Combined LM/DM)*/

    /* Range of session id that can be generated is 
     * 1 to 2^26 (60000000 approximately) . So we can use max of 39999999 
     * as session id */
    for (u4Index = RFC6374_INDEX_ONE; u4Index < RFC6374_INDEX_NINE; u4Index++)
    {
        R6374UtilCalculatePower (RFC6374_VAL_10, u4Index, &u4Power);
        if (u4SessionId / u4Power == 0)
        {
            break;
        }
    }
    /* Eliminating the first digit and increment the resulting number */
    /* The looping ss done to find which range the incremented session Id 
     * falls e.g: 10-100 , 100-1000 , 1000-10000 etc., */

    /* Decrement the index . Reason is, if the session id is in range 100-1000
     * the loop will break at u4SessionId/10000 i.e index = 4.
     * So the actual range index = 3 is derived by decrementing the value
     * of index*/
    u4Index--;

    /* The following function calculates base 10 to power index eg: 1000
     * which is subtracted from actual session id (2000 for DM , 
     * 3000 for Combined), to get id that is actually incremented*/
    R6374UtilCalculatePower (RFC6374_VAL_10, u4Index, &u4Power);

    u4SessionId = u4SessionId - (u4SessType * u4Power);
    /* Increment the Id , which now has the type prefix eliminated */
    u4SessionId++;

    /* If the session id has reached it's max limit 9999999 , roll back */
    /* R6374UtilGetSessId will get starting value of session id with 
     * the prefix for LM/DM/ComLMDM. Just increment the value by 1 and
     * clean up the existing buffer & stats */
    if (u4SessionId == RFC6374_SESS_MAX_ROLLING_LIMIT)
    {
        R6374CfaGetSysMacAddress (SwitchMacAddr);
        RFC6374_GET_SESSION_ID_FROM_MAC (SwitchMacAddr, u4TempSessionId);

        switch (u4SessType)
        {
            case RFC6374_VAL_1:
                R6374UtilGetSessId (NULL, &(pServiceInfo->LmInfo),
                                    NULL, u4TempSessionId, RFC6374_VAL_1);
                pServiceInfo->LmInfo.u4LmSessionId++;
                R6374UtilCleanUpStatsBuffer (pServiceInfo, u4SessType);

                RFC6374_TRC1 (RFC6374_CRITICAL_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                              "Warning : LM buffer session roll over happened for "
                              "service %s. It is recommended to clear buffer and stats\r\n",
                              pServiceInfo->au1ServiceName);
                break;
            case RFC6374_VAL_2:
                R6374UtilGetSessId (NULL, NULL, &(pServiceInfo->DmInfo),
                                    u4TempSessionId, RFC6374_VAL_2);
                pServiceInfo->DmInfo.u4DmSessionId++;
                R6374UtilCleanUpStatsBuffer (pServiceInfo, u4SessType);
                RFC6374_TRC1 (RFC6374_CRITICAL_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                              "Warning : DM buffer session roll over happened for "
                              "service %s. It is recommended to clear buffer and stats\r\n",
                              pServiceInfo->au1ServiceName);
                break;
            case RFC6374_VAL_3:
                R6374UtilGetSessId (&(pServiceInfo->LmDmInfo), NULL, NULL,
                                    u4TempSessionId, RFC6374_VAL_3);
                pServiceInfo->LmDmInfo.u4LmDmSessionId++;
                R6374UtilCleanUpStatsBuffer (pServiceInfo, u4SessType);
                RFC6374_TRC1 (RFC6374_CRITICAL_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                              "Warning : Cmb LM/DM buffer session roll over happened for "
                              "service %s. It is recommended to clear buffer and stats\r\n",
                              pServiceInfo->au1ServiceName);
                break;
            default:
                break;
        }
        /* Prefix addition is done in R6374UtilGetSessId itself. So return */
        return RFC6374_SUCCESS;

    }
    /* Adding the first digit. The looping ss done to find which range
     * the incremented session Id falls e.g: 10-100 , 100-1000 , 1000-10000 etc., */
    for (u4Index = RFC6374_INDEX_ONE; u4Index < RFC6374_INDEX_NINE; u4Index++)
    {
        R6374UtilCalculatePower (RFC6374_VAL_10, u4Index, &u4Power);
        if (u4SessionId / u4Power == 0)
        {
            break;
        }
    }

    /* Adding the first digit and increment the resulting number */
    R6374UtilCalculatePower (RFC6374_VAL_10, u4Index, &u4Power);
    u4SessionId = u4SessionId + (u4SessType * u4Power);

    switch (u4SessType)
    {
        case RFC6374_VAL_1:
            pServiceInfo->LmInfo.u4LmSessionId = u4SessionId;
            R6374UtilCleanUpStatsBuffer (pServiceInfo, u4SessType);

            break;
        case RFC6374_VAL_2:
            pServiceInfo->DmInfo.u4DmSessionId = u4SessionId;
            R6374UtilCleanUpStatsBuffer (pServiceInfo, u4SessType);
            break;
        case RFC6374_VAL_3:
            pServiceInfo->LmDmInfo.u4LmDmSessionId = u4SessionId;
            R6374UtilCleanUpStatsBuffer (pServiceInfo, u4SessType);
            break;
        default:
            break;
    }

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374UtilCalculatePower                                *
 *                                                                             *
 * Description        : This routine is called to calculate power of base to   *
 *                      exponent passed and return them in result arguement    *
 *                                                                             *
 * Input(s)           : u4Base  - Base integer                               *
 *                      u4Expo  - Exponent integer                             *
 *                                                                             *
 * Output(s)          : pu4Result = (u4Base ^(power) u4Expo)                   *
 *                                                                             *
 * Return Value(s)    : None                                                *
 *******************************************************************************/

VOID
R6374UtilCalculatePower (UINT4 u4Base, UINT4 u4Expo, UINT4 *pu4Result)
{
    UINT4               u4Index = RFC6374_INIT_VAL;

    *pu4Result = RFC6374_VAL_1;
    for (u4Index = RFC6374_VAL_1; u4Index <= u4Expo; u4Index++)
    {
        *pu4Result = *pu4Result * u4Base;
    }

    return;
}

/*******************************************************************************
 * Function Name      : R6374UtilCleanUpStatsBuffer                            *
 *                                                                             *
 * Description        : This routine is called to clean up buffer and stats    *
 *             entry for a session id.                                *
 *                                                        *
 * Input(s)           : pServiceInfo                                       *
 *                      SessionType                                            *
 *                                                                             *
 * Output(s)          : None                                                   *
 *                                                                             *
 * Return Value(s)    : RFC6374_SUCCESS/RFC6374_FAILURE                           *
 *******************************************************************************/
PUBLIC UINT1
R6374UtilCleanUpStatsBuffer (tR6374ServiceConfigTableEntry * pServiceInfo,
                             UINT4 u4SessType)
{

    tR6374LmStatsTableEntry *pLmStatsNodeInfo = NULL;
    tR6374LmBufferTableEntry TempLossBuffEntry;
    tR6374LmBufferTableEntry *pLossBuffEntry = NULL;
    tR6374DmStatsTableEntry *pDmStatsNodeInfo = NULL;
    tR6374DmBufferTableEntry TempDelayBuffEntry;
    tR6374DmBufferTableEntry *pDelayBuffEntry = NULL;
    UINT4               u4SessionId = RFC6374_INIT_VAL;
    UINT4               u4SeqCount = RFC6374_INIT_VAL;

    MEMSET (&TempLossBuffEntry, RFC6374_INIT_VAL,
            sizeof (tR6374LmBufferTableEntry));
    MEMSET (&TempDelayBuffEntry, RFC6374_INIT_VAL,
            sizeof (tR6374DmBufferTableEntry));

    /* Do not clear buffer when new session is dyadic enabled, 
     * since the buffer will be in responder's side */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      "R6374UtilCleanUpStatsBuffer: Service %s is dyadic enabled "
                      "No need to clear buffer and stats\r\n ",
                      pServiceInfo->au1ServiceName);
        return RFC6374_SUCCESS;
    }

    /* Do not clear buffer when new session is one-way, 
     * since the buffer will be in responder's side */
    /* For LM Session */
    if (u4SessType == RFC6374_VAL_1)
    {
        if (pServiceInfo->LmInfo.u1LmType == RFC6374_LM_TYPE_ONE_WAY)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374UtilCleanUpStatsBuffer: Service %s loss measurement "
                          "is one-way. No need to clear buffer and stats\r\n ",
                          pServiceInfo->au1ServiceName);
            return RFC6374_SUCCESS;
        }
        u4SessionId = pServiceInfo->LmInfo.u4LmSessionId;

    }
    else if (u4SessType == RFC6374_VAL_2)
    {
        /* For DM Session */
        if (pServiceInfo->DmInfo.u1DmType == RFC6374_DM_TYPE_ONE_WAY)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374UtilCleanUpStatsBuffer: Service %s delay measurement "
                          "is one-way. No need to clear buffer and stats\r\n ",
                          pServiceInfo->au1ServiceName);
            return RFC6374_SUCCESS;
        }
        u4SessionId = pServiceInfo->DmInfo.u4DmSessionId;
    }
    else if (u4SessType == RFC6374_VAL_3)
    {
        /* For Combined LM/DM Session */
        if (pServiceInfo->LmInfo.u1LmType == RFC6374_LM_TYPE_ONE_WAY)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374UtilCleanUpStatsBuffer: Service %s Cmb LM/DM measurement "
                          "is one-way. No need to clear buffer and stats\r\n ",
                          pServiceInfo->au1ServiceName);
            return RFC6374_SUCCESS;
        }
        u4SessionId = pServiceInfo->LmDmInfo.u4LmDmSessionId;
    }

    /* For LM or combined LM/DM */
    if ((u4SessType == RFC6374_VAL_1) || (u4SessType == RFC6374_VAL_3))
    {

        /* Clean up stats entry */
        pLmStatsNodeInfo = R6374UtilGetLmStatsTableInfo (pServiceInfo,
                                                         u4SessionId);
        if (pLmStatsNodeInfo != NULL)
        {
            RBTreeRem (RFC6374_LMSTATS_TABLE, pLmStatsNodeInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_LMSTATSTABLE_POOLID,
                                    (UINT1 *) pLmStatsNodeInfo);
        }

        /* Clean up buffer entry */
        TempLossBuffEntry.u4SessionId = u4SessionId;
        TempLossBuffEntry.u4ContextId = pServiceInfo->u4ContextId;
        RFC6374_STRCPY (TempLossBuffEntry.au1ServiceName,
                        pServiceInfo->au1ServiceName);

        for (u4SeqCount = RFC6374_INDEX_ONE;
             u4SeqCount <= RFC6374_RB_BUFFER_MAX_COUNT; u4SeqCount++)
        {
            TempLossBuffEntry.u4SeqCount = u4SeqCount;
            pLossBuffEntry = RBTreeGet (RFC6374_LMBUFFER_TABLE,
                                        &TempLossBuffEntry);
            if (pLossBuffEntry == NULL)
            {
                break;
            }
            RBTreeRem (RFC6374_LMBUFFER_TABLE, pLossBuffEntry);
            RFC6374_FREE_MEM_BLOCK (RFC6374_LMBUFFERTABLE_POOLID,
                                    (UINT1 *) pLossBuffEntry);
            pLossBuffEntry = NULL;

        }
    }

    /* For DM or Combined LM/DM */
    if ((u4SessType == RFC6374_VAL_2) || (u4SessType == RFC6374_VAL_3))
    {
        /* Clean up stats entry */
        pDmStatsNodeInfo = R6374UtilGetDmStatsTableInfo (pServiceInfo,
                                                         u4SessionId);
        if (pDmStatsNodeInfo != NULL)
        {
            RBTreeRem (RFC6374_DMSTATS_TABLE, pDmStatsNodeInfo);
            RFC6374_FREE_MEM_BLOCK (RFC6374_DMSTATSTABLE_POOLID,
                                    (UINT1 *) pDmStatsNodeInfo);
        }

        /* Clean up buffer entry */
        TempDelayBuffEntry.u4SessionId = u4SessionId;
        TempDelayBuffEntry.u4ContextId = pServiceInfo->u4ContextId;
        RFC6374_STRCPY (TempDelayBuffEntry.au1ServiceName,
                        pServiceInfo->au1ServiceName);

        for (u4SeqCount = RFC6374_INDEX_ONE;
             u4SeqCount <= RFC6374_RB_BUFFER_MAX_COUNT; u4SeqCount++)
        {
            TempDelayBuffEntry.u4SeqCount = u4SeqCount;
            pDelayBuffEntry = RBTreeGet (RFC6374_DMBUFFER_TABLE,
                                         &TempDelayBuffEntry);
            if (pDelayBuffEntry == NULL)
            {
                break;
            }
            RBTreeRem (RFC6374_DMBUFFER_TABLE, pDelayBuffEntry);
            RFC6374_FREE_MEM_BLOCK (RFC6374_DMBUFFERTABLE_POOLID,
                                    (UINT1 *) pDelayBuffEntry);
            pDelayBuffEntry = NULL;

        }
    }

    return RFC6374_SUCCESS;

}
#endif
