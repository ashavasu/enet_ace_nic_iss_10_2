
/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: r6374cli.c,v 1.7 2017/07/25 12:00:37 siva Exp $
 *
 * Description:  This file contains CLI SET/GET/TEST and GETNEXT
 *               routines for the MIB objects specified in fsecfm.mib
 *               and fsecfmex.mib
 *******************************************************************/
#ifndef _R6374CLI_C_
#define _R6374CLI_C_
#include "r6374inc.h"

PRIVATE tSNMP_OCTET_STRING_TYPE gServiceName;
PRIVATE UINT1       au1gServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

PRIVATE VOID        R6374PrintTrapControl (tCliHandle);

PRIVATE INT4        R6374UtilValidateSerConfigParamsForService
PROTO ((UINT4, tSNMP_OCTET_STRING_TYPE, UINT4,
        UINT4, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, UINT4));
/*******************************************************************************
 *
 *  FUNCTION NAME   : cli_process_rfc6374_cmd
 *
 *  DESCRIPTION     : Protocol CLI Message Handler Function for the user exec
 *                     mode commands.
 *
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ... - Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/

INT4
cli_process_rfc6374_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE SrcIp;
    tSNMP_OCTET_STRING_TYPE DestIp;
    UINT4              *apu4args[RFC6374_CLI_MAX_ARGS];
    UINT1              *pu1ContextName = NULL;
    UINT4               u4ErrCode = RFC6374_INIT_VAL;
    UINT4               u4ContextId = CLI_GET_CXT_ID ();
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    UINT4               u4CmdType = RFC6374_INIT_VAL;
    UINT4               u4Detail = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_INIT_VAL;
    INT4                i4ServiceNameLen = RFC6374_INIT_VAL;
    INT4                i4LmTransmitStatus = RFC6374_INIT_VAL;
    INT4                i4DmTransmitStatus = RFC6374_INIT_VAL;
    INT4                i4LmDmTransmitStatus = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1SrcIp[RFC6374_IPV4_ADDR_LEN];
    UINT1               au1DestIp[RFC6374_IPV4_ADDR_LEN];
    INT1                i1argno = RFC6374_INIT_VAL;

    if ((gR6374GlobalInfo.u1SystemControl[RFC6374_DEFAULT_CONTEXT_ID]
         != RFC6374_START) && (u4Command != CLI_RFC6374_SYS_START))
    {
        CliPrintf (CliHandle, "\r%% MPLS PM module is shutdown\r\n");
        return CLI_FAILURE;
    }

    CliRegisterLock (CliHandle, R6374TaskLock, R6374TaskUnLock);
    RFC6374_LOCK ();

    va_start (ap, u4Command);
    MEMSET (apu4args, 0x0, (RFC6374_CLI_MAX_ARGS * sizeof (UINT4 *)));
    /*Switch Name will be first always */
    pu1ContextName = va_arg (ap, UINT1 *);
    while (1)
    {
        apu4args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == RFC6374_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL,
                    RFC6374_SERVICE_NAME_MAX_LEN);
    RFC6374_MEMSET (au1SrcIp, RFC6374_INIT_VAL, RFC6374_IPV4_ADDR_LEN);
    RFC6374_MEMSET (au1DestIp, RFC6374_INIT_VAL, RFC6374_IPV4_ADDR_LEN);

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&SrcIp, RFC6374_INIT_VAL, sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&DestIp, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Source IP Pointer */
    SrcIp.pu1_OctetList = au1SrcIp;
    SrcIp.i4_Length = RFC6374_IPV4_ADDR_LEN;

    /* Destination IP pointer */
    DestIp.pu1_OctetList = au1DestIp;
    DestIp.i4_Length = RFC6374_IPV4_ADDR_LEN;

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;    /*As MPLS works on default context */
    switch (u4Command)
    {
        case CLI_RFC6374_SYS_START:
            i4RetVal =
                R6374CliSetSystemControl (CliHandle, u4ContextId,
                                          RFC6374_START);
            break;

        case CLI_RFC6374_SYS_SHUTDOWN:
            i4RetVal =
                R6374CliSetSystemControl (CliHandle, u4ContextId,
                                          RFC6374_SHUTDOWN);
            break;

        case CLI_RFC6374_MODULE_ENABLE:
            i4RetVal = R6374CliSetModuleStatus (CliHandle, u4ContextId,
                                                RFC6374_ENABLED);
            break;

        case CLI_RFC6374_MODULE_DISABLE:
            i4RetVal = R6374CliSetModuleStatus (CliHandle, u4ContextId,
                                                RFC6374_DISABLED);
            break;

        case CLI_RFC6374_CREATE_SERVICE_TNL:
            /*  Tunnel
             *  apu4args [0] - ServiceName
             *  apu4args [1] - Fwd Tunnel id
             *  apu4args [2] - Rev Tunnel id (optional=0)
             *  apu4args [3] - Source ip
             *  apu4args [4] - Destination Ip */

            /* Check if Service Is Already Present */
            /* Service is not Present in Service Table 
             * Create the New Service Node */
            /* Service Name */
            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ZERO]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ZERO], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            /* Source IP */
            RFC6374_MEMCPY (SrcIp.pu1_OctetList,
                            apu4args[RFC6374_INDEX_THREE],
                            RFC6374_IPV4_ADDR_LEN);

            /* Destination IP */
            RFC6374_MEMCPY (DestIp.pu1_OctetList,
                            apu4args[RFC6374_INDEX_FOUR],
                            RFC6374_IPV4_ADDR_LEN);

            i4RetVal = R6374CliCreateServiceEntry
                (CliHandle, u4ContextId, ServiceName,
                 *(apu4args[RFC6374_INDEX_ONE]),
                 *(apu4args[RFC6374_INDEX_TWO]), SrcIp, DestIp, u4Command);

            if (i4RetVal != CLI_FAILURE)
            {
                i4RetVal = R6374CliCreateServiceUsingTnl (CliHandle,
                                                          u4ContextId,
                                                          ServiceName,
                                                          *(apu4args
                                                            [RFC6374_INDEX_ONE]),
                                                          *(apu4args
                                                            [RFC6374_INDEX_TWO]),
                                                          SrcIp, DestIp);
                if (CLI_FAILURE != i4RetVal)
                {
                    /* Store the service name in the global so to use in service
                     * mode related commands*/
                    RFC6374_MEMSET (au1gServiceName, RFC6374_INIT_VAL,
                                    RFC6374_SERVICE_NAME_MAX_LEN);
                    gServiceName.pu1_OctetList = au1gServiceName;
                    gServiceName.i4_Length = RFC6374_INIT_VAL;

                    RFC6374_MEMCPY (gServiceName.pu1_OctetList,
                                    ServiceName.pu1_OctetList,
                                    ServiceName.i4_Length);
                    gServiceName.i4_Length = ServiceName.i4_Length;
                    /* Service is created successfully
                     * Enter the Service Config Mode */
                    if (CliChangePath ("service-R6374") == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Unable to enter into Service "
                                   "configuration mode\r\n");
                        RFC6374_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else
                {
                    nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                                  &i4LmTransmitStatus);
                    nmhGetFs6374DMTransmitStatus (u4ContextId, &ServiceName,
                                                  &i4DmTransmitStatus);
                    nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                                  &i4LmDmTransmitStatus);

                    if ((i4LmTransmitStatus == RFC6374_TX_STATUS_NOT_READY) ||
                        (i4DmTransmitStatus == RFC6374_TX_STATUS_NOT_READY) ||
                        (i4LmDmTransmitStatus == RFC6374_TX_STATUS_NOT_READY))
                    {
                        CliPrintf (CliHandle, "%% LM/DM/Combined LMDM Test is "
                                   "in-progress, cannot config the service "
                                   "params\r\n");
                        RFC6374_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                    if (nmhTestv2Fs6374RowStatus (&u4ErrorCode, u4ContextId,
                                                  &ServiceName,
                                                  RFC6374_ROW_STATUS_DESTROY) !=
                        SNMP_SUCCESS)
                    {
                        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                     RFC6374_DEFAULT_CONTEXT_ID,
                                     "cli_process_rfc6374_cmd: Failure "
                                     "in rowstatus destroy \r\n ");
                        i4RetVal = CLI_FAILURE;
                    }
                    else
                    {
                        if (nmhSetFs6374RowStatus (u4ContextId, &ServiceName,
                                                   RFC6374_ROW_STATUS_DESTROY)
                            != SNMP_SUCCESS)
                        {
                            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                         RFC6374_DEFAULT_CONTEXT_ID,
                                         "cli_process_rfc6374_cmd: "
                                         "Failure in rowstatus destroy \r\n ");
                            i4RetVal = CLI_FAILURE;
                        }
                    }
                }
            }
            break;

        case CLI_RFC6374_CREATE_SERVICE_PW:
            /*  apu4args [0] - ServiceName
             *  apu4args [1] - Pw Ip address
             *  apu4args [2] - Pw id */

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ZERO]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ZERO], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            /* PW Destination IP */
            RFC6374_MEMCPY (DestIp.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], DestIp.i4_Length);

            /* Check if Service Is Already Present */
            i4RetVal = R6374CliCreateServiceEntry (CliHandle, u4ContextId,
                                                   ServiceName,
                                                   *(apu4args
                                                     [RFC6374_INDEX_TWO]),
                                                   RFC6374_INIT_VAL, DestIp,
                                                   SrcIp, u4Command);
            if (i4RetVal != CLI_FAILURE)
            {
                i4RetVal = R6374CliCreateServiceUsingPW (CliHandle,
                                                         u4ContextId,
                                                         ServiceName,
                                                         DestIp,
                                                         *(apu4args
                                                           [RFC6374_INDEX_TWO]));
                if (CLI_FAILURE != i4RetVal)
                {
                    /* Store the service name in the global so to use in service
                     * mode related commands*/
                    RFC6374_MEMSET (au1gServiceName, RFC6374_INIT_VAL,
                                    RFC6374_SERVICE_NAME_MAX_LEN);
                    gServiceName.pu1_OctetList = au1gServiceName;
                    gServiceName.i4_Length = RFC6374_INIT_VAL;

                    RFC6374_MEMCPY (gServiceName.pu1_OctetList,
                                    ServiceName.pu1_OctetList,
                                    ServiceName.i4_Length);
                    gServiceName.i4_Length = ServiceName.i4_Length;
                    /* Service is created successfully
                     * Enter the Service Config Mode */
                    if (CliChangePath ("service-R6374") == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Unable to enter into Service "
                                   "configuration mode\r\n");
                        RFC6374_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                else
                {
                    nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                                  &i4LmTransmitStatus);
                    nmhGetFs6374DMTransmitStatus (u4ContextId, &ServiceName,
                                                  &i4DmTransmitStatus);
                    nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                                  &i4LmDmTransmitStatus);

                    if ((i4LmTransmitStatus == RFC6374_TX_STATUS_NOT_READY) ||
                        (i4DmTransmitStatus == RFC6374_TX_STATUS_NOT_READY) ||
                        (i4LmDmTransmitStatus == RFC6374_TX_STATUS_NOT_READY))
                    {
                        CliPrintf (CliHandle, "%% LM/DM/Combined LMDM Test is "
                                   "in-progress, cannot config the service "
                                   "params\r\n");
                        RFC6374_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                    if (nmhTestv2Fs6374RowStatus (&u4ErrorCode, u4ContextId,
                                                  &ServiceName,
                                                  RFC6374_ROW_STATUS_DESTROY) !=
                        SNMP_SUCCESS)
                    {
                        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                     RFC6374_DEFAULT_CONTEXT_ID,
                                     "cli_process_rfc6374_cmd: Failure "
                                     "in rowstatus destroy \r\n ");
                        i4RetVal = CLI_FAILURE;
                    }
                    else
                    {
                        if (nmhSetFs6374RowStatus (u4ContextId, &ServiceName,
                                                   RFC6374_ROW_STATUS_DESTROY)
                            != SNMP_SUCCESS)
                        {
                            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                         RFC6374_DEFAULT_CONTEXT_ID,
                                         "cli_process_rfc6374_cmd: "
                                         "Failure in rowstatus destroy \r\n ");
                            i4RetVal = CLI_FAILURE;
                        }
                    }
                }
            }
            break;
        case CLI_RFC6374_ENTER_SERVICEMODE:
            /* Check if Service Is Already Present */
            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ZERO],
                            RFC6374_STRLEN (apu4args[RFC6374_INDEX_ZERO]));
            ServiceName.i4_Length =
                (INT4) (RFC6374_STRLEN (apu4args[RFC6374_INDEX_ZERO]));

            i4RetVal = R6374CliCreateServiceEntry (CliHandle, u4ContextId,
                                                   ServiceName,
                                                   RFC6374_INIT_VAL,
                                                   RFC6374_INIT_VAL, SrcIp,
                                                   DestIp, u4Command);
            if (i4RetVal == CLI_SUCCESS)
            {
                RFC6374_MEMSET (au1gServiceName, RFC6374_INIT_VAL,
                                RFC6374_SERVICE_NAME_MAX_LEN);

                /* Store the service name in the global so to use in service
                 * mode related commands*/
                gServiceName.pu1_OctetList = au1gServiceName;
                gServiceName.i4_Length = RFC6374_INIT_VAL;

                RFC6374_MEMCPY (gServiceName.pu1_OctetList,
                                ServiceName.pu1_OctetList,
                                ServiceName.i4_Length);
                gServiceName.i4_Length = ServiceName.i4_Length;

                /* Service is already created 
                 * Enter the Service Config Mode */
                if (CliChangePath ("service-R6374") == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Unable to enter into Service "
                               "configuration mode\r\n");
                    RFC6374_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }
            break;
        case CLI_RFC6374_DELETE_SERVICE:
            /*  apu4args [0] - ServiceName */

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ZERO]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ZERO], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            i4RetVal = R6374CliDeleteService (CliHandle, u4ContextId,
                                              ServiceName);
            break;
        case CLI_RFC6374_SERVICEMODE_TNL_FWD_ID:
            i4RetVal = R6374CliServiceModeModifyFwdTnlId (CliHandle,
                                                          u4ContextId,
                                                          gServiceName,
                                                          *(apu4args
                                                            [RFC6374_INDEX_ZERO]));
            break;
        case CLI_RFC6374_SERVICEMODE_TNL_REV_ID:
            i4RetVal = R6374CliServiceModeModifyRevTnlId (CliHandle,
                                                          u4ContextId,
                                                          gServiceName,
                                                          *(apu4args
                                                            [RFC6374_INDEX_ZERO]));
            break;
        case CLI_RFC6374_SERVICEMODE_TNL_SRC_ADDR:
            /*  apu4args [0] - LSP Src-address */
            /* Source IP */
            RFC6374_MEMCPY (SrcIp.pu1_OctetList,
                            (UINT1 *) apu4args[RFC6374_INDEX_ZERO],
                            RFC6374_IPV4_ADDR_LEN);

            i4RetVal = R6374CliServiceModeModifySrcIpAddr (CliHandle,
                                                           u4ContextId,
                                                           gServiceName, SrcIp);
            break;
        case CLI_RFC6374_SERVICEMODE_TNL_DST_ADDR:
            /*  apu4args [0] - LSP Dest-address */
            /* Destination IP */
            RFC6374_MEMCPY (DestIp.pu1_OctetList,
                            (UINT1 *) apu4args[RFC6374_INDEX_ZERO],
                            RFC6374_IPV4_ADDR_LEN);
            i4RetVal =
                R6374CliServiceModeModifyDestIpAddr (CliHandle, u4ContextId,
                                                     gServiceName, DestIp);
            break;
        case CLI_RFC6374_SERVICEMODE_PW:
            /*  apu4args [0] - PW Ip-address
             *  apu4args [1] - PW Id */

            /* PW Destination IP */
            RFC6374_MEMCPY (DestIp.pu1_OctetList,
                            (UINT1 *) apu4args[RFC6374_INDEX_ZERO],
                            RFC6374_IPV4_ADDR_LEN);

            i4RetVal = R6374CliServiceModeModifyPW (CliHandle,
                                                    u4ContextId, gServiceName,
                                                    DestIp,
                                                    *(apu4args
                                                      [RFC6374_INDEX_ONE]));
            break;

        case CLI_RFC6374_SERVICEMODE_TC:
            /*  apu4args [0] - Traffic class id */

            i4RetVal =
                R6374CliServiceModeModifyTC (CliHandle, u4ContextId,
                                             gServiceName,
                                             (INT4) (*
                                                     (apu4args
                                                      [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_SERVICEMODE_NEGOTIATION:
            /*  apu4args [0] - Negotiation status (Enable/Disable) */

            i4RetVal = R6374CliServiceModeTSFNegotiation (CliHandle,
                                                          u4ContextId,
                                                          gServiceName,
                                                          (INT4) (*
                                                                  (apu4args
                                                                   [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_SERVICEMODE_LM_TS_FORMAT:
            /*  apu4args [0] - LM preferred time stamp format */

            i4RetVal = R6374CliServiceModeLMTSFormat (CliHandle,
                                                      u4ContextId, gServiceName,
                                                      (INT4) (*
                                                              (apu4args
                                                               [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_SERVICEMODE_DM_TS_FORMAT:
            /*  apu4args [0] - DM preferred time stamp format */

            i4RetVal = R6374CliServiceModeDMTSFormat (CliHandle,
                                                      u4ContextId, gServiceName,
                                                      (INT4) (*
                                                              (apu4args
                                                               [RFC6374_INDEX_ZERO])));
            break;
        case CLI_RFC6374_SERVICEMODE_LMDM_TS_FORMAT:
            /*  apu4args [0] - Combined LMDM preferred time stamp format */

            i4RetVal = R6374CliServiceModeLMDMTSFormat (CliHandle, u4ContextId,
                                                        gServiceName,
                                                        (INT4) (*
                                                                (apu4args
                                                                 [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_INIT_LM_FROM_SERVICEMODE:
            /*  apu4args [0] - LM Status (start/stop) */

            i4RetVal = R6374CliInitLM (CliHandle, u4ContextId,
                                       (INT4) (*(apu4args[RFC6374_INDEX_ZERO])),
                                       gServiceName);
            break;

        case CLI_RFC6374_INIT_LM_FROM_GLOBAL:
            /*  apu4args [0] - LM Status (start/stop) 
             *  apu4args [1] - Service Name */

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);

            ServiceName.i4_Length = i4ServiceNameLen;

            i4RetVal = R6374CliInitLM (CliHandle, u4ContextId,
                                       (INT4) (*(apu4args[RFC6374_INDEX_ZERO])),
                                       ServiceName);
            break;

        case CLI_RFC6374_INIT_DM_FROM_SERVICEMODE:
            /*  apu4args [0] - LM Status (start/stop) */

            i4RetVal = R6374CliInitDM (CliHandle, u4ContextId,
                                       (INT4) (*(apu4args[RFC6374_INDEX_ZERO])),
                                       gServiceName);
            break;

        case CLI_RFC6374_INIT_DM_FROM_GLOBAL:
            /*  apu4args [0] - DM Status (start/stop) 
             *  apu4args [1] - Service Name */

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            i4RetVal = R6374CliInitDM (CliHandle, u4ContextId,
                                       (INT4) (*(apu4args[RFC6374_INDEX_ZERO])),
                                       ServiceName);
            break;

        case CLI_RFC6374_CONFIG_LM_PARAMS:
            /*  apu4args [0] - Type - OneWay/TwoWay
             *  apu4args [1] - Method - Direct/Inferred
             *  apu4args [2] - Mode - Ondemand/proactive
             *  apu4args [3] - Count
             *  apu4args [4] - Interval */
            i4RetVal =
                R6374CliConfigLMParams (CliHandle, u4ContextId, gServiceName,
                                        (INT4) (*
                                                (apu4args[RFC6374_INDEX_ZERO])),
                                        (INT4) (*(apu4args[RFC6374_INDEX_ONE])),
                                        (INT4) (*(apu4args[RFC6374_INDEX_TWO])),
                                        (*(apu4args[RFC6374_INDEX_THREE])),
                                        (*(apu4args[RFC6374_INDEX_FOUR])));
            break;

        case CLI_RFC6374_RESET_LM_PARAMS:
            /*  apu4args [0] - ResetParam */

            i4RetVal =
                R6374CliResetLMParams (CliHandle, u4ContextId, gServiceName,
                                       (*(apu4args[RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_CONFIG_DM_PARAMS:
            /*  apu4args [0] - Type - OneWay/TwoWay
             *  apu4args [1] - Mode - Ondemand/proactive
             *  apu4args [2] - Count
             *  apu4args [3] - Interval 
             *  apu4args [4] - Padding size */
            i4RetVal =
                R6374CliConfigDMParams (CliHandle, u4ContextId, gServiceName,
                                        (INT4) (*
                                                (apu4args[RFC6374_INDEX_ZERO])),
                                        (INT4) (*(apu4args[RFC6374_INDEX_ONE])),
                                        (*(apu4args[RFC6374_INDEX_TWO])),
                                        (*(apu4args[RFC6374_INDEX_THREE])),
                                        (*(apu4args[RFC6374_INDEX_FOUR])));
            break;

        case CLI_RFC6374_RESET_DM_PARAMS:
            /*  apu4args [0] - ResetParam */

            i4RetVal =
                R6374CliResetDMParams (CliHandle, u4ContextId, gServiceName,
                                       (*(apu4args[RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_ENCAP_TYPE:
            /*  apu4args [0] - Encap Type */

            i4RetVal = R6374CliEncapType (CliHandle, u4ContextId, gServiceName,
                                          (INT4) (*
                                                  (apu4args
                                                   [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_SHOW_STATS_INFO:
            /*  apu4args [0] - Service Name 
             *  apu4args [1] - Context id*/

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ZERO]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ZERO], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            u4ContextId = (*(apu4args[RFC6374_INDEX_ONE]));
            i4RetVal = R6374CliShowStats (CliHandle, u4ContextId, ServiceName);
            break;

        case CLI_RFC6374_SHOW_STATS_INFO_ALL:
            /*  apu4args [0] - Context Id */
            u4ContextId = (*(apu4args[RFC6374_INDEX_ZERO]));
            i4RetVal = R6374CliShowStatsAll (CliHandle, u4ContextId);
            break;

        case CLI_RFC6374_SHOW_SERVICE_INFO_ALL:
            /*  apu4args [0] - Context Id *
             *  apu4args [1] - Detail - 0 or 1 */

            u4ContextId = (*(apu4args[RFC6374_INDEX_ZERO]));

            i4RetVal =
                R6374CliShowServiceConfigInfoAll (CliHandle, u4ContextId,
                                                  (UINT1) (*
                                                           (apu4args
                                                            [RFC6374_INDEX_ONE])));
            break;

        case CLI_RFC6374_SHOW_SERVICE_INFO:
            /*  apu4args [0] - Context Id
             *  apu4args [1] - Particular service 
             *  apu4args [2] - Detail - 0 or 1 */
            u4ContextId = (*(apu4args[RFC6374_INDEX_ZERO]));

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            i4RetVal = R6374CliShowServiceConfigInfo (CliHandle, u4ContextId,
                                                      ServiceName,
                                                      (UINT1) (*
                                                               (apu4args
                                                                [RFC6374_INDEX_TWO])));
            break;
        case CLI_RFC6374_SHOW_BUFFER:
            /*  apu4args [0] - CmdType 
             *  apu4args [1] - u4Detail 
             *  apu4args [2] - ServiceName 
             *  apu4args [3] - Context Id */
            u4CmdType = *(apu4args[RFC6374_INDEX_ZERO]);
            u4Detail = *(apu4args[RFC6374_INDEX_ONE]);
            u4ContextId = *(apu4args[RFC6374_INDEX_THREE]);

            if (apu4args[RFC6374_INDEX_TWO] != NULL)
            {
                i4ServiceNameLen = (INT4) (RFC6374_STRLEN ((UINT1 *)
                                                           apu4args
                                                           [RFC6374_INDEX_TWO]));

                RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                                apu4args[RFC6374_INDEX_TWO], i4ServiceNameLen);
                ServiceName.i4_Length = i4ServiceNameLen;
            }
            if (u4CmdType == RFC6374_CLI_SHOW_LM_BUFFER)
            {
                if (ServiceName.i4_Length == RFC6374_INIT_VAL)
                {
                    i4RetVal = R6374CliShowLmBufferAll (CliHandle, u4ContextId,
                                                        (UINT1) u4Detail);
                }
                else
                {
                    i4RetVal =
                        R6374CliShowLmBufferPerService (CliHandle, u4ContextId,
                                                        ServiceName,
                                                        (UINT1) u4Detail);
                }
            }

            if (u4CmdType == RFC6374_CLI_SHOW_DM_BUFFER)
            {
                if (ServiceName.i4_Length == RFC6374_INIT_VAL)
                {
                    i4RetVal = R6374CliShowDmBufferAll (CliHandle, u4ContextId,
                                                        (UINT1) u4Detail);
                }
                else
                {
                    i4RetVal =
                        R6374CliShowDmBufferPerService (CliHandle, u4ContextId,
                                                        ServiceName,
                                                        (UINT1) u4Detail);
                }
            }
            break;
        case CLI_RFC6374_SHOW_BUFFER_ALL:
            /*  apu4args [0] - CmdType 
             *  apu4args [1] - u4Detail 
             *  apu4args [2] - ServiceName 
             *  apu4args [3] - Context Id */
            u4CmdType = *(apu4args[RFC6374_INDEX_ZERO]);
            u4Detail = *(apu4args[RFC6374_INDEX_ONE]);
            u4ContextId = *(apu4args[RFC6374_INDEX_THREE]);

            if (apu4args[RFC6374_INDEX_TWO] != NULL)
            {
                i4ServiceNameLen = (INT4) (RFC6374_STRLEN ((UINT1 *)
                                                           apu4args
                                                           [RFC6374_INDEX_TWO]));

                RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                                apu4args[RFC6374_INDEX_TWO], i4ServiceNameLen);
                ServiceName.i4_Length = i4ServiceNameLen;
            }
            if (u4CmdType == RFC6374_CLI_SHOW_ALL_BUFFER)
            {
                if (ServiceName.i4_Length == RFC6374_INIT_VAL)
                {
                    i4RetVal = R6374CliShowBufferAll (CliHandle, u4ContextId,
                                                      (UINT1) u4Detail);
                }
                else
                {
                    i4RetVal =
                        R6374CliShowBufferPerService (CliHandle, u4ContextId,
                                                      ServiceName,
                                                      (UINT1) u4Detail);
                }
            }
            break;

        case CLI_RFC6374_SHOW_GLOBAL:
            /* apu4args [0] - ContextId */
            i4RetVal = RFC6374CliShowGlobalInfo (CliHandle, u4ContextId);
            break;
        case CLI_RFC6374_DEBUG:
            /* apu4args [0] - ContextId
             * apu4args [1] - Debug Type*/
            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            R6374CliSetDebugLevel (CliHandle,
                                   u4ContextId,
                                   (*(apu4args[RFC6374_INDEX_ONE])),
                                   RFC6374_ENABLED);
            break;

        case CLI_RFC6374_NO_DEBUG:
            /* apu4args [0] - ContextId
             * apu4args [1] - Debug Type */
            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            R6374CliSetDebugLevel (CliHandle,
                                   u4ContextId,
                                   *(apu4args[RFC6374_INDEX_ONE]),
                                   RFC6374_DISABLED);
            break;

        case CLI_RFC6374_CLEAR_STATS_INFO:
            /*  apu4args [0] - Service name 
               apu4args [1] - Context Id */

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ZERO]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ZERO], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            u4ContextId = *(apu4args[RFC6374_INDEX_ONE]);
            i4RetVal = R6374CliClearStats (CliHandle, u4ContextId, ServiceName);
            break;

        case CLI_RFC6374_CLEAR_STATS_INFO_ALL:
            /*  apu4args [0] - Context Id */
            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal = R6374CliClearStatsAll (CliHandle, u4ContextId);
            break;

        case CLI_RFC6374_CLEAR_DELAY_BUFFER:
            /*  apu4args [0] - Context Id */
            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal = R6374CliClearDelayBuffer (CliHandle, u4ContextId);
            break;

        case CLI_RFC6374_CLEAR_LOSS_BUFFER:
            /*  apu4args [0] - Context Id */
            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal = R6374CliClearLossBuffer (CliHandle, u4ContextId);
            break;

        case CLI_RFC6374_CLEAR_ALL_BUFFER:
            /*  apu4args [0] - Context Id */
            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal = R6374CliClearBufferAll (CliHandle, u4ContextId);
            break;

        case CLI_RFC6374_CLEAR_DELAY_BUFFER_FOR_SERVICE:
            /*  apu4args [0] - Context Id 
               apu4args [1] - Service name */
            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal =
                R6374CliClearDelayBufferForService (CliHandle, u4ContextId,
                                                    ServiceName);
            break;

        case CLI_RFC6374_CLEAR_LOSS_BUFFER_FOR_SERVICE:
            /*  apu4args [0] - Context Id 
               apu4args [1] - Service name */
            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal =
                R6374CliClearLossBufferForService (CliHandle, u4ContextId,
                                                   ServiceName);
            break;

        case CLI_RFC6374_CLEAR_ALL_BUFFER_FOR_SERVICE:
            /*  apu4args [0] - Context Id 
               apu4args [1] - Service name */
            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);
            ServiceName.i4_Length = i4ServiceNameLen;

            u4ContextId = *(apu4args[RFC6374_INDEX_ZERO]);
            i4RetVal =
                R6374CliClearBufferAllForService (CliHandle, u4ContextId,
                                                  ServiceName);
            break;

        case CLI_RFC6374_ENABLE_TRAP:
            /*  apu4args [0] - Trap */
            i4RetVal =
                R6374CliSetTrapStatus (CliHandle, u4ContextId,
                                       (UINT2) (*
                                                (apu4args[RFC6374_INDEX_ZERO])),
                                       RFC6374_ENABLED);
            break;
        case CLI_RFC6374_DISABLE_TRAP:
            /*  apu4args [0] - Trap */
            i4RetVal =
                R6374CliSetTrapStatus (CliHandle, u4ContextId,
                                       (UINT2) (*
                                                (apu4args[RFC6374_INDEX_ZERO])),
                                       RFC6374_DISABLED);
            break;
        case CLI_RFC6374_INIT_LMDM_FROM_GLOBAL:
            /*  apu4args [0] - LM Status (start/stop) 
             *  apu4args [1] - Service Name */

            i4ServiceNameLen = (INT4)
                (RFC6374_STRLEN ((UINT1 *) apu4args[RFC6374_INDEX_ONE]));

            RFC6374_MEMCPY (ServiceName.pu1_OctetList,
                            apu4args[RFC6374_INDEX_ONE], i4ServiceNameLen);

            ServiceName.i4_Length = i4ServiceNameLen;

            i4RetVal = R6374CliInitLMDM (CliHandle, u4ContextId,
                                         (INT4) (*
                                                 (apu4args
                                                  [RFC6374_INDEX_ZERO])),
                                         ServiceName);
            break;

        case CLI_RFC6374_CONFIG_LMDM_PARAMS:
            /*  apu4args [0] - Type - OneWay/TwoWay
             *  apu4args [1] - Method - Direct/Inferred
             *  apu4args [2] - Mode - Ondemand/proactive
             *  apu4args [3] - Packet Count
             *  apu4args [4] - Interval 
             *  apu4args [5] - Pad size */
            i4RetVal =
                R6374CliConfigLMDMParams (CliHandle, u4ContextId, gServiceName,
                                          (INT4) (*
                                                  (apu4args
                                                   [RFC6374_INDEX_ZERO])),
                                          (INT4) (*
                                                  (apu4args
                                                   [RFC6374_INDEX_ONE])),
                                          (INT4) (*
                                                  (apu4args
                                                   [RFC6374_INDEX_TWO])),
                                          *(apu4args[RFC6374_INDEX_THREE]),
                                          *(apu4args[RFC6374_INDEX_FOUR]),
                                          *(apu4args[RFC6374_INDEX_FIVE]));
            break;

        case CLI_RFC6374_RESET_CONFIG_LMDM_PARAMS:
            /*  apu4args [0] - Command */
            i4RetVal =
                R6374CliResetLMDMParams (CliHandle, u4ContextId, gServiceName,
                                         *(apu4args[RFC6374_INDEX_ZERO]));
            break;

        case CLI_RFC6374_INIT_LMDM_FROM_SERVICEMODE:
            /*  apu4args [0] - Combined LMDM Status (start/stop) */

            i4RetVal = R6374CliInitLMDM (CliHandle, u4ContextId,
                                         (INT4) (*
                                                 (apu4args
                                                  [RFC6374_INDEX_ZERO])),
                                         gServiceName);
            break;

        case CLI_RFC6374_DYADIC_MEASUREMENT:
            /* apu4args[0] - Dyadic Measurement Enable/Disable pre service */

            i4RetVal = R6374CliDyadicMeasurement (CliHandle, u4ContextId,
                                                  gServiceName,
                                                  (INT4) (*
                                                          (apu4args
                                                           [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_DYADIC_PRO_ROLE:
            /*  apu4args[0] - Dyadic proactive Measurement role */

            i4RetVal = R6374CliDyadicProactiveRole (CliHandle, u4ContextId,
                                                    gServiceName,
                                                    (INT4) (*
                                                            (apu4args
                                                             [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_SESS_INT_QUERY:
            /* apu4args[0] - Session Interval Query Enable/Disable per service */

            i4RetVal = R6374CliSessionIntervalQuery (CliHandle, u4ContextId,
                                                     gServiceName,
                                                     (INT4) (*
                                                             (apu4args
                                                              [RFC6374_INDEX_ZERO])));
            break;

        case CLI_RFC6374_MIN_RECEP_INTERVAL:
            /* apu4args[0] - Minimum Reception Interval per service */

            i4RetVal =
                R6374CliSetMinimumReceptionInterval (CliHandle, u4ContextId,
                                                     gServiceName,
                                                     *(apu4args
                                                       [RFC6374_INDEX_ZERO]));
            break;
        case CLI_RFC6374_QUERY_TRANSMIT_RETRY_COUNT:
            /* apu4args[0] - Retry count for Congestion Management per service */

            i4RetVal =
                R6374CliSetQueryTransmitRetryCount (CliHandle, u4ContextId,
                                                    gServiceName,
                                                    *(apu4args
                                                      [RFC6374_INDEX_ZERO]));
            break;
        default:

            /* Given command does not match with any of the SET or SHOW
             * commands */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            RFC6374_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;

    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > RFC6374_CLI_MIN_ERR)
            && (u4ErrCode < RFC6374_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gaR6374CliErrorString[u4ErrCode]);
        }
        CLI_SET_ERR (RFC6374_CLI_MIN_ERR);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetVal);

    UNUSED_PARAM (pu1ContextName);

    RFC6374_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

/*******************************************************************************
 * Function    :  R6374CliSetSystemControl                                     *
 *                                                                             *
 * Description :  This function sets the system control status of              *
 *                R6374 module depending on the input parameter                *
 *                                                                             *
 * Input       :  CliHandle             - Cli context                          *
 *                u4ContextId           - Context Identifier                   *
 *                i4SystemControl       - System Control Status                *
 *                (RFC6374_START/RFC6374_SHUTDOWN)                             *
 *                                                                             *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4SystemControl)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374SystemControl (&u4ErrorCode, u4ContextId,
                                      i4SystemControl) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374SystemControl (u4ContextId, i4SystemControl)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    : R6374CliSetModuleStatus                                       *
 *                                                                             *
 * Description :  This function sets the system control status of              *
 *                R6374 module depending on the input parameter                *
 *                                                                             *
 * Input       :  CliHandle              - Cli context                         *
 *                u4ContextId            - Context Identifier                  *
 *                i4ModuleStatus         - Module Status                       *
 *                (RFC6374_ENABLED/RFC6374_DISABLED)                           *
 *                                                                             *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4ModuleStatus)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374ModuleStatus (&u4ErrorCode, u4ContextId,
                                     i4ModuleStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374ModuleStatus (u4ContextId, i4ModuleStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    : R6374CliDeleteService                                         *
 *                                                                             *
 * Description :  This function deletes the service created                    *
 *                                                                             *
 * Input       :  CliHandle              - Cli context                         *
 *                u4ContextId            - Context Identifier                  *
 *                ServiceName            - Service Name                        *
 *                                                                             *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliDeleteService (tCliHandle CliHandle, UINT4 u4ContextId,
                       tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    if (nmhTestv2Fs6374RowStatus (&u4ErrorCode, u4ContextId,
                                  &ServiceName,
                                  RFC6374_ROW_STATUS_DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374RowStatus (u4ContextId, &ServiceName,
                               RFC6374_ROW_STATUS_DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    : R6374CliServiceModeModifyTC                                   *
 *                                                                             *
 * Description :  This function deletes the service created                    *
 *                                                                             *
 * Input       :  CliHandle              - Cli context                         *
 *                u4ContextId            - Context Identifier                  *
 *                                                                             *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliServiceModeModifyTC (tCliHandle CliHandle, UINT4 u4ContextId,
                             tSNMP_OCTET_STRING_TYPE ServiceName, INT4 i4TC)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374TrafficClass (&u4ErrorCode, u4ContextId,
                                     &ServiceName, i4TC) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374TrafficClass (u4ContextId, &ServiceName, i4TC) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    :  R6374CliServiceModeTSFNegotiation                            *
 *                                                                             *
 * Description :  This function Enable/Disable the Timestamp Format            *
                  Negotiation                                                  *
 *                                                                             *
 * Input       :  CliHandle         - Cli context                              *
 *                u4ContextId       - Context Identifier                       *
 *                ServiceName       - Service Name                       *
 *                i4NegoStatus      - TSF negotiation status                   *
 *                                                                             *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliServiceModeTSFNegotiation (tCliHandle CliHandle, UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE ServiceName,
                                   INT4 i4NegoStatus)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374TSFNegotiation (&u4ErrorCode, u4ContextId,
                                       &ServiceName,
                                       i4NegoStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374TSFNegotiation (u4ContextId, &ServiceName,
                                    i4NegoStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    :  R6374CliServiceModeLMTSFormat                                *
 *                                                                             *
 * Description :  This function sets the loss measurement preferred timestamp  *
 *               Format                                                       *
 *                                                                             *
 * Input       :  CliHandle         - Cli context                              *
 *                u4ContextId       - Context Identifier                       *
 *                ServiceName       - Service Name                       *
 *                i4LmPrefTSFormat  - Preferred time stamp format              *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliServiceModeLMTSFormat (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE ServiceName,
                               INT4 i4LmPrefTSFormat)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374LMTimeStampFormat (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          i4LmPrefTSFormat) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374LMTimeStampFormat (u4ContextId, &ServiceName,
                                       i4LmPrefTSFormat) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    :  R6374CliServiceModeDMTSFormat                                *
 *                                                                             *
 * Description :  This function sets the delay measurement preferred timestamp *
 *               Format                                                       *
 *                                                                             *
 * Input       :  CliHandle         - Cli context                              *
 *                u4ContextId       - Context Identifier                       *
 *                ServiceName       - Service Name                       *
 *                i4DmPrefTSFormat  - Preferred time stamp format              *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliServiceModeDMTSFormat (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE ServiceName,
                               INT4 i4DmPrefTSFormat)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374DMTimeStampFormat (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          i4DmPrefTSFormat) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374DMTimeStampFormat (u4ContextId, &ServiceName,
                                       i4DmPrefTSFormat) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    :  R6374CliServiceModeLMDMTSFormat                              *
 *                                                                             *
 * Description :  This function sets the delay measurement preferred           *
 *           timestamp Format                                             *
 *                                                                             *
 * Input       :  CliHandle           - Cli context                            *
 *                u4ContextId         - Context Identifier                     *
 *                ServiceName         - Service Name                       *
 *                i4LmDmPrefTSFormat  - Preferred time stamp format            *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliServiceModeLMDMTSFormat (tCliHandle CliHandle, UINT4 u4ContextId,
                                 tSNMP_OCTET_STRING_TYPE ServiceName,
                                 INT4 i4LmDmPrefTSFormat)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374CmbLMDMTimeStampFormat (&u4ErrorCode, u4ContextId,
                                               &ServiceName,
                                               i4LmDmPrefTSFormat) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374CmbLMDMTimeStampFormat (u4ContextId, &ServiceName,
                                            i4LmDmPrefTSFormat) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * FUNCTION NAME    : R6374CliCreateServiceEntry
 *
 * DESCRIPTION      : This Funtion will create the service
 *
 * INPUT            : CliHandle - CliContext ID,
 *                    u4ContextId - Context Identifier,
 *                    ServiceName - Service Name
 *                    u4FwdTnPwlId - Forward/Pw id
 *                    u4RevTnlId - Reverse Tunnel Id
 *                    pSrcIpAddr - Source Ip Address
 *                    pDestIpAddr - Destination Ip Address
 *                    u4Command    - Type of command 
 *
 * OUTPUT           : None
 *
 * Returns          : tR6374ServiceConfigTableEntry - pServiceInfo
 * ****************************************************************************/
INT4
R6374CliCreateServiceEntry (tCliHandle CliHandle, UINT4 u4ContextId,
                            tSNMP_OCTET_STRING_TYPE ServiceName,
                            UINT4 u4FwdTnPwlId, UINT4 u4RevTnlId,
                            tSNMP_OCTET_STRING_TYPE pSrcIpAddr,
                            tSNMP_OCTET_STRING_TYPE pDestIpAddr,
                            UINT4 u4Command)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4RetVal = RFC6374_INIT_VAL;
    INT4                i4RowStatus = -1;

    /* Check if Service is already Present
     * if No then Test for Row Status ACTIVE */
    if (nmhGetFs6374RowStatus (u4ContextId, &ServiceName, &i4RowStatus) !=
        SNMP_SUCCESS)
    {
        i4RetVal = nmhTestv2Fs6374RowStatus (&u4ErrorCode, u4ContextId,
                                             &ServiceName,
                                             RFC6374_ROW_STATUS_ACTIVE);
        if (i4RetVal == SNMP_SUCCESS)
        {
            /* Service is not Present in Service Table
             * Create a Service Entry */
            i4RetVal = nmhSetFs6374RowStatus (u4ContextId,
                                              &ServiceName,
                                              RFC6374_ROW_STATUS_CREATE_AND_GO);
            if (i4RetVal == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Success is returned to enter into service mode if service 
         * is already present when cmd ENTER_SERVICEMODE is passed as i/p */
        if (u4Command == CLI_RFC6374_ENTER_SERVICEMODE)
        {
            return CLI_SUCCESS;
        }
        /* If service config paramaeters are changed for that particular
         * service in config mode, error should be thrown */
        if (R6374UtilValidateSerConfigParamsForService
            (u4ContextId, ServiceName,
             u4FwdTnPwlId, u4RevTnlId,
             &pSrcIpAddr, &pDestIpAddr, u4Command) == RFC6374_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Service entry is already "
                       "configured with different config" " parameters \r\n");
            return CLI_FAILURE;
        }
        else
        {
            return CLI_SUCCESS;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliCreateServiceUsingTnl
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliCreateServiceUsingTnl (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE ServiceName,
                               UINT4 u4FwdTnlId, UINT4 u4RevTnlId,
                               tSNMP_OCTET_STRING_TYPE SrcIpAddr,
                               tSNMP_OCTET_STRING_TYPE DestIpAddr)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    /* MPLS path Type */
    if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                     &ServiceName,
                                     RFC6374_MPLS_PATH_TYPE_TNL) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374MplsPathType (u4ContextId,
                                  &ServiceName,
                                  RFC6374_MPLS_PATH_TYPE_TNL) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs6374FwdTnlIdOrPwId (&u4ErrorCode, u4ContextId,
                                       &ServiceName, u4FwdTnlId) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374FwdTnlIdOrPwId (u4ContextId, &ServiceName, u4FwdTnlId) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (0 != u4RevTnlId)
    {
        if (nmhTestv2Fs6374RevTnlId (&u4ErrorCode, u4ContextId,
                                     &ServiceName, u4RevTnlId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374RevTnlId (u4ContextId,
                                  &ServiceName, u4RevTnlId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs6374SrcIpAddr (&u4ErrorCode, u4ContextId,
                                  &ServiceName, &SrcIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374SrcIpAddr (u4ContextId, &ServiceName, &SrcIpAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs6374DestIpAddr (&u4ErrorCode, u4ContextId,
                                   &ServiceName, &DestIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374DestIpAddr (u4ContextId, &ServiceName, &DestIpAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliCreateServiceUsingPW
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliCreateServiceUsingPW (tCliHandle CliHandle,
                              UINT4 u4ContextId,
                              tSNMP_OCTET_STRING_TYPE ServiceName,
                              tSNMP_OCTET_STRING_TYPE pwIpAddr, UINT4 u4pwId)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    /* MPLS Path type */
    if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                     &ServiceName,
                                     RFC6374_MPLS_PATH_TYPE_PW) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374MplsPathType (u4ContextId, &ServiceName,
                                  RFC6374_MPLS_PATH_TYPE_PW) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs6374DestIpAddr (&u4ErrorCode, u4ContextId,
                                   &ServiceName, &pwIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374DestIpAddr (u4ContextId, &ServiceName,
                                &pwIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs6374FwdTnlIdOrPwId (&u4ErrorCode, u4ContextId,
                                       &ServiceName, u4pwId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374FwdTnlIdOrPwId (u4ContextId, &ServiceName,
                                    u4pwId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliServiceModeModifyFwdTnlId
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliServiceModeModifyFwdTnlId (tCliHandle CliHandle, UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE ServiceName,
                                   UINT4 u4FwdTnlId)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;

    nmhGetFs6374MplsPathType (u4ContextId, &ServiceName, &i4MplsPathType);
    /* Mpls path type configuration can be done only once if its not already 
     * configured.*/
    if (i4MplsPathType == RFC6374_INIT_VAL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                         &ServiceName,
                                         RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374MplsPathType (u4ContextId,
                                      &ServiceName,
                                      RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs6374FwdTnlIdOrPwId (&u4ErrorCode, u4ContextId,
                                       &ServiceName, u4FwdTnlId) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374FwdTnlIdOrPwId (u4ContextId,
                                    &ServiceName, u4FwdTnlId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliServiceModeModifyRevTnlId
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliServiceModeModifyRevTnlId (tCliHandle CliHandle, UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE ServiceName,
                                   UINT4 u4RevTnlId)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;

    nmhGetFs6374MplsPathType (u4ContextId, &ServiceName, &i4MplsPathType);
    if (i4MplsPathType == RFC6374_INIT_VAL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                         &ServiceName,
                                         RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374MplsPathType (u4ContextId,
                                      &ServiceName,
                                      RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs6374RevTnlId (&u4ErrorCode, u4ContextId,
                                 &ServiceName, u4RevTnlId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374RevTnlId (u4ContextId,
                              &ServiceName, u4RevTnlId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliServiceModeModifySrcIpAddr
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliServiceModeModifySrcIpAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                                    tSNMP_OCTET_STRING_TYPE ServiceName,
                                    tSNMP_OCTET_STRING_TYPE SrcIpAddr)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;

    nmhGetFs6374MplsPathType (u4ContextId, &ServiceName, &i4MplsPathType);
    if (i4MplsPathType == RFC6374_INIT_VAL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                         &ServiceName,
                                         RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374MplsPathType (u4ContextId,
                                      &ServiceName,
                                      RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs6374SrcIpAddr (&u4ErrorCode, u4ContextId,
                                  &ServiceName, &SrcIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374SrcIpAddr (u4ContextId, &ServiceName, &SrcIpAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliServiceModeModifyDstIpAddr
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliServiceModeModifyDestIpAddr (tCliHandle CliHandle, UINT4 u4ContextId,
                                     tSNMP_OCTET_STRING_TYPE ServiceName,
                                     tSNMP_OCTET_STRING_TYPE DestIpAddr)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;

    nmhGetFs6374MplsPathType (u4ContextId, &ServiceName, &i4MplsPathType);
    if (i4MplsPathType == RFC6374_INIT_VAL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                         &ServiceName,
                                         RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374MplsPathType (u4ContextId,
                                      &ServiceName,
                                      RFC6374_MPLS_PATH_TYPE_TNL) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs6374DestIpAddr (&u4ErrorCode, u4ContextId,
                                   &ServiceName, &DestIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374DestIpAddr (u4ContextId, &ServiceName, &DestIpAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 **
 **     FUNCTION NAME    : R6374CliServiceModeModifyPW
 **
 **     DESCRIPTION      : This function will enable/disable the module.
 **
 **     INPUT            : CliHandle - CliContext ID
 **                        u4ContextId - context Identifier,
 **
 **     OUTPUT           : None
 **
 **     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **
 *****************************************************************************/
INT4
R6374CliServiceModeModifyPW (tCliHandle CliHandle, UINT4 u4ContextId,
                             tSNMP_OCTET_STRING_TYPE ServiceName,
                             tSNMP_OCTET_STRING_TYPE DestIpAddr, UINT4 u4PwId)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;

    nmhGetFs6374MplsPathType (u4ContextId, &ServiceName, &i4MplsPathType);
    if (i4MplsPathType == RFC6374_INIT_VAL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
    {
        if (nmhTestv2Fs6374MplsPathType (&u4ErrorCode, u4ContextId,
                                         &ServiceName,
                                         RFC6374_MPLS_PATH_TYPE_PW) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374MplsPathType (u4ContextId,
                                      &ServiceName,
                                      RFC6374_MPLS_PATH_TYPE_PW) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs6374FwdTnlIdOrPwId (&u4ErrorCode, u4ContextId,
                                       &ServiceName, u4PwId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs6374DestIpAddr (&u4ErrorCode, u4ContextId,
                                   &ServiceName, &DestIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374FwdTnlIdOrPwId (u4ContextId, &ServiceName,
                                    u4PwId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374DestIpAddr (u4ContextId, &ServiceName, &DestIpAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : R6374CliInitLM
 *
 *     DESCRIPTION      : This function will initiate the delay calculations.
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        ServiceInfo - context Identifier,
 *                        u4Command - Type of command
 *                        i4DelayType - Type of Delay measurement
 *                        pu1Arg1, pu1Arg2, pu1Arg3,
 *                        arguments can contain different parameters according
 *                        to particular command.
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
R6374CliInitLM (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4InitLM,
                tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    if (nmhTestv2Fs6374LMTransmitStatus
        (&u4ErrorCode, u4ContextId, &ServiceName, i4InitLM) != SNMP_SUCCESS)

    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374LMTransmitStatus
        (u4ContextId, &ServiceName, i4InitLM) != SNMP_SUCCESS)

    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : R6374CliInitDM
 *
 *     DESCRIPTION      : This function will initiate the delay calculations.
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        ServiceInfo - context Identifier,
 *                        u4Command - Type of command
 *                        i4DelayType - Type of Delay measurement
 *                        pu1Arg1, pu1Arg2, pu1Arg3,
 *                        arguments can contain different parameters according
 *                        to particular command.
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
R6374CliInitDM (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4InitDM,
                tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374DMTransmitStatus
        (&u4ErrorCode, u4ContextId, &ServiceName, i4InitDM) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374DMTransmitStatus
        (u4ContextId, &ServiceName, i4InitDM) != SNMP_SUCCESS)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : R6374CliConfigLMParams
 *
 *     DESCRIPTION      : This function will set the MIB fields required for
 *                        Delay Measurement.
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        ServiceName     - ServiceNameInfo
 *                        i4DelayType     - Type of the DM to initiate
 *                        pu1Arg1         -
 *                        pu1Arg2         -
 *                        pu1Arg3         -
 *                        pu1Arg4         - Arguments acc. to the command
 *
 *     OUTPUT           : b1Err - Indicates an Error from Test routine
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
R6374CliConfigLMParams (tCliHandle CliHandle, UINT4 u4ContextId,
                        tSNMP_OCTET_STRING_TYPE ServiceName,
                        INT4 i4Type, INT4 i4Method, INT4 i4Mode,
                        UINT4 u4Count, UINT4 u4Interval)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4LmMode = RFC6374_INIT_VAL;
    INT4                i4TransmitStatus = RFC6374_INIT_VAL;
    INT4                i4DyadicMeasurement = RFC6374_INIT_VAL;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;

    if (i4Type != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374LMType (&u4ErrorCode, u4ContextId,
                                   &ServiceName, i4Type) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374LMType (u4ContextId, &ServiceName, i4Type)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4Method != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374LMMethod (&u4ErrorCode, u4ContextId,
                                     &ServiceName, i4Method) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374LMMethod (u4ContextId, &ServiceName, i4Method)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Interval validation has to be done before validating the mode/count
     * otherwise setting of interval will fail when the mode is changing 
     * from proactive to on-demand with optional interval value. Because 
     * handling of test stop event will be processed after setting all the
     * values*/

    if (u4Interval != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374LMTimeIntervalInMilliseconds
            (&u4ErrorCode, u4ContextId,
             &ServiceName, u4Interval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374LMTimeIntervalInMilliseconds
            (u4ContextId, &ServiceName, u4Interval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4Mode != RFC6374_INIT_VAL)
    {
        nmhGetFs6374LMMode (u4ContextId, &ServiceName, &i4LmMode);

        nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                      &i4TransmitStatus);

        if ((i4LmMode == RFC6374_LM_MODE_PROACTIVE) &&
            (i4Mode == RFC6374_LM_MODE_ONDEMAND) &&
            (u4Count != RFC6374_INIT_VAL) &&
            (i4TransmitStatus == RFC6374_TX_STATUS_NOT_READY))
        {
            CliPrintf (CliHandle, "\r%% To stop the LM proactive test "
                       "specify only mode as on-demand without optional "
                       "parameters\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2Fs6374LMMode (&u4ErrorCode, u4ContextId,
                                   &ServiceName, i4Mode) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374LMMode (u4ContextId, &ServiceName, i4Mode)
            == SNMP_FAILURE)
        {
            /* Check is Return Failure is due to Dyadic Role Passive and
             * mode Proactive */
            nmhGetFs6374DyadicMeasurement (u4ContextId, &ServiceName,
                                           &i4DyadicMeasurement);
            nmhGetFs6374DyadicProactiveRole (u4ContextId, &ServiceName,
                                             &i4DyadicProactiveRole);
            nmhGetFs6374LMMode (u4ContextId, &ServiceName, &i4LmMode);
            nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName,
                                          &i4TransmitStatus);
            if ((i4LmMode == RFC6374_LM_MODE_PROACTIVE)
                && (i4DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
                && (i4DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_PASSIVE)
                && (i4TransmitStatus == RFC6374_TX_STATUS_READY))
            {
                CliPrintf (CliHandle, "\r Warning: Cannot initiate the Dyadic "
                           "Proactive test as Dyadic Proactive Role is set to passive\r\n");
            }
            return CLI_FAILURE;
        }
        if (i4Mode == CLI_RFC6374_MODE_ONDEMAND)
        {
            if (u4Count != RFC6374_INIT_VAL)
            {
                if (nmhTestv2Fs6374LMNoOfMessages (&u4ErrorCode, u4ContextId,
                                                   &ServiceName,
                                                   u4Count) == SNMP_FAILURE)

                {
                    return CLI_FAILURE;
                }

                if (nmhSetFs6374LMNoOfMessages (u4ContextId, &ServiceName,
                                                u4Count) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : R6374CliConfigDMParams
 *
 *     DESCRIPTION      : This function will set the MIB fields required for
 *                        Delay Measurement.
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        u4ContextId     - Context identifier                
 *                        ServiceName     - ServiceName                       
 *                        i4Type          - Type of the DM to      
 *                                          initiate                          
 *                        i4Method        - Method of the DM to    
 *                                    initiate                          
 *                        i4Mode          - Mode of the DM to       
 *                                        initiate                          
 *                        u4Count         - No of packets to be transmitted   
 *                                    when mode is on-demand            
 *                        u4Interval      - periodic interval to send out     
 *                                    the DM pdu             
 *                        u4PadSize       - Padding size to be included in
 *                                    Padding TLV
 *
 *     OUTPUT           : b1Err - Indicates an Error from Test routine
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
R6374CliConfigDMParams (tCliHandle CliHandle, UINT4 u4ContextId,
                        tSNMP_OCTET_STRING_TYPE ServiceName,
                        INT4 i4Type, INT4 i4Mode, UINT4 u4Count,
                        UINT4 u4Interval, UINT4 u4PadSize)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4DmMode = RFC6374_INIT_VAL;
    INT4                i4TransmitStatus = RFC6374_INIT_VAL;
    INT4                i4DyadicMeasurement = RFC6374_INIT_VAL;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;

    if (i4Type != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374DMType (&u4ErrorCode, u4ContextId,
                                   &ServiceName, i4Type) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374DMType (u4ContextId, &ServiceName, i4Type)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4Interval != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374DMTimeIntervalInMilliseconds (&u4ErrorCode,
                                                         u4ContextId,
                                                         &ServiceName,
                                                         u4Interval) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374DMTimeIntervalInMilliseconds (u4ContextId, &ServiceName,
                                                      u4Interval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (u4PadSize != RFC6374_INVALID_PAD_SIZE)
    {
        if (nmhTestv2Fs6374DMPaddingSize (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          u4PadSize) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374DMPaddingSize (u4ContextId, &ServiceName,
                                       u4PadSize) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4Mode != RFC6374_INIT_VAL)
    {
        nmhGetFs6374DMMode (u4ContextId, &ServiceName, &i4DmMode);
        nmhGetFs6374DMTransmitStatus (u4ContextId, &ServiceName,
                                      &i4TransmitStatus);

        if ((i4DmMode == RFC6374_DM_MODE_PROACTIVE) &&
            (i4Mode == RFC6374_DM_MODE_ONDEMAND) &&
            (u4Count != RFC6374_INIT_VAL) &&
            (i4TransmitStatus == RFC6374_TX_STATUS_NOT_READY))
        {
            CliPrintf (CliHandle, "\r%% To stop the DM proactive test "
                       "specify only mode as on-demand without optional "
                       "parameters\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2Fs6374DMMode (&u4ErrorCode, u4ContextId,
                                   &ServiceName, i4Mode) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374DMMode (u4ContextId, &ServiceName,
                                i4Mode) == SNMP_FAILURE)
        {
            /* Check is Return Failure is due to Dyadic Role Passive and
             * mode Proactive */
            nmhGetFs6374DyadicMeasurement (u4ContextId, &ServiceName,
                                           &i4DyadicMeasurement);
            nmhGetFs6374DyadicProactiveRole (u4ContextId, &ServiceName,
                                             &i4DyadicProactiveRole);
            nmhGetFs6374DMMode (u4ContextId, &ServiceName, &i4DmMode);
            nmhGetFs6374DMTransmitStatus (u4ContextId, &ServiceName,
                                          &i4TransmitStatus);
            if ((i4DmMode == RFC6374_DM_MODE_PROACTIVE)
                && (i4DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
                && (i4DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_PASSIVE)
                && (i4TransmitStatus == RFC6374_TX_STATUS_READY))
            {
                CliPrintf (CliHandle, "\r Warning: Cannot initiate the Dyadic "
                           "Proactive test as Dyadic Proactive Role is set to passive\r\n");
            }
            return CLI_FAILURE;
        }
        if (i4Mode == CLI_RFC6374_MODE_ONDEMAND)
        {
            if (u4Count != RFC6374_INIT_VAL)
            {
                if (nmhTestv2Fs6374DMNoOfMessages (&u4ErrorCode, u4ContextId,
                                                   &ServiceName,
                                                   u4Count) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFs6374DMNoOfMessages (u4ContextId, &ServiceName,
                                                u4Count) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : R6374CliResetLMParams
 *
 *     DESCRIPTION      : This function will initiate the delay calculations.
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        ServiceInfo - context Identifier,
 *                        u4Command - Type of command
 *                        i4DelayType - Type of Delay measurement
 *                        pu1Arg1, pu1Arg2, pu1Arg3,
 *                        arguments can contain different parameters according
 *                        to particular command.
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
R6374CliResetLMParams (tCliHandle CliHandle,
                       UINT4 u4ContextId,
                       tSNMP_OCTET_STRING_TYPE ServiceName, UINT4 u4ResetParam)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4LmMode = RFC6374_INIT_VAL;

    if ((u4ResetParam & CLI_RFC6374_RESET_LM_TYPE) == CLI_RFC6374_RESET_LM_TYPE)
    {
        if (nmhTestv2Fs6374LMType (&u4ErrorCode, u4ContextId,
                                   &ServiceName,
                                   CLI_RFC6374_TYPE_TWO_WAY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374LMType (u4ContextId, &ServiceName,
                                CLI_RFC6374_TYPE_TWO_WAY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4ResetParam & CLI_RFC6374_RESET_LM_MODE) == CLI_RFC6374_RESET_LM_MODE)
    {
        nmhGetFs6374LMMode (u4ContextId, &ServiceName, &i4LmMode);

        if (nmhTestv2Fs6374LMMode (&u4ErrorCode, u4ContextId,
                                   &ServiceName,
                                   CLI_RFC6374_MODE_ONDEMAND) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374LMMode (u4ContextId, &ServiceName,
                                CLI_RFC6374_MODE_ONDEMAND) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        /* 'no lm mode' - command used to reset LM mode to on-demand 
         * and LM No of message count to default value of 10 and the same 
         * is used to stop the currently running proactive test. 
         * So in this case LM count need not be reset.*/
        if (i4LmMode == RFC6374_LM_MODE_ONDEMAND)
        {
            if (nmhTestv2Fs6374LMNoOfMessages (&u4ErrorCode, u4ContextId,
                                               &ServiceName,
                                               RFC6374_LM_DEFAULT_ONDEMAND_COUNT)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFs6374LMNoOfMessages (u4ContextId, &ServiceName,
                                            RFC6374_LM_DEFAULT_ONDEMAND_COUNT)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    if ((u4ResetParam & CLI_RFC6374_RESET_LM_METHOD) ==
        CLI_RFC6374_RESET_LM_METHOD)
    {
        if (nmhTestv2Fs6374LMMethod (&u4ErrorCode, u4ContextId,
                                     &ServiceName,
                                     CLI_RFC6374_METHOD_INFERED) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374LMMethod (u4ContextId, &ServiceName,
                                  CLI_RFC6374_METHOD_INFERED) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4ResetParam & CLI_RFC6374_RESET_LM_QUERY_INTERVAL) ==
        CLI_RFC6374_RESET_LM_QUERY_INTERVAL)
    {
        if (nmhTestv2Fs6374LMTimeIntervalInMilliseconds
            (&u4ErrorCode, u4ContextId, &ServiceName,
             RFC6374_LM_DEFAULT_INTERVAL) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374LMTimeIntervalInMilliseconds (u4ContextId,
                                                      &ServiceName,
                                                      RFC6374_LM_DEFAULT_INTERVAL)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : R6374CliResetDMParams
 *
 *     DESCRIPTION      : This function will initiate the delay calculations.
 *
 *     INPUT            : CliHandle - CliContext ID
 *                        ServiceInfo - context Identifier,
 *                        u4Command - Type of command
 *                        i4DelayType - Type of Delay measurement
 *                        pu1Arg1, pu1Arg2, pu1Arg3,
 *                        arguments can contain different parameters according
 *                        to particular command.
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
R6374CliResetDMParams (tCliHandle CliHandle, UINT4 u4ContextId,
                       tSNMP_OCTET_STRING_TYPE ServiceName, UINT4 u4ResetParam)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4DmMode = RFC6374_INIT_VAL;

    if ((u4ResetParam & CLI_RFC6374_RESET_DM_TYPE) == CLI_RFC6374_RESET_DM_TYPE)
    {
        if (nmhTestv2Fs6374DMType (&u4ErrorCode, u4ContextId,
                                   &ServiceName,
                                   CLI_RFC6374_TYPE_TWO_WAY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374DMType (u4ContextId, &ServiceName,
                                CLI_RFC6374_TYPE_TWO_WAY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4ResetParam & CLI_RFC6374_RESET_DM_MODE) == CLI_RFC6374_RESET_DM_MODE)
    {
        nmhGetFs6374DMMode (u4ContextId, &ServiceName, &i4DmMode);

        if (nmhTestv2Fs6374DMMode (&u4ErrorCode, u4ContextId,
                                   &ServiceName,
                                   CLI_RFC6374_MODE_ONDEMAND) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374DMMode (u4ContextId, &ServiceName,
                                CLI_RFC6374_MODE_ONDEMAND) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        /* 'no dm mode' - command used to reset DM mode to on-demand 
         * and DM No of message count to default value of 10 and the same 
         * is used to stop the currently running proactive test. 
         * So in this case DM count need not be reset.*/

        if (i4DmMode == RFC6374_DM_MODE_ONDEMAND)
        {
            if (nmhTestv2Fs6374DMNoOfMessages (&u4ErrorCode, u4ContextId,
                                               &ServiceName,
                                               RFC6374_DM_DEFAULT_ONDEMAND_COUNT)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFs6374DMNoOfMessages (u4ContextId, &ServiceName,
                                            RFC6374_DM_DEFAULT_ONDEMAND_COUNT)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    if ((u4ResetParam & CLI_RFC6374_RESET_DM_QUERY_INTERVAL) ==
        CLI_RFC6374_RESET_DM_QUERY_INTERVAL)
    {
        if (nmhTestv2Fs6374DMTimeIntervalInMilliseconds (&u4ErrorCode,
                                                         u4ContextId,
                                                         &ServiceName,
                                                         RFC6374_DM_DEFAULT_INTERVAL)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374DMTimeIntervalInMilliseconds (u4ContextId,
                                                      &ServiceName,
                                                      RFC6374_DM_DEFAULT_INTERVAL)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4ResetParam & CLI_RFC6374_RESET_DM_PADDING_SIZE) ==
        CLI_RFC6374_RESET_DM_PADDING_SIZE)
    {
        /* Default value of padding tlv size is 0 */
        if (nmhTestv2Fs6374DMPaddingSize (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          RFC6374_DEFAULT_PAD_SIZE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374DMPaddingSize (u4ContextId, &ServiceName,
                                       RFC6374_DEFAULT_PAD_SIZE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function    :  R6374CliEncapType                                            *
 *                                                                             *
 * Description :  This function sets the encap type                            *
 *                                                                             *
 * Input       :  CliHandle             - Cli context                          *
 *                u4ContextId           - Context Identifier                   *
 *                 ServiceName        - Service name                         *
 *                u4Type                - gAL/ACH                              *
 *                                                                             *
 * Output      :  None                                                         *
 *                                                                             *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                      *
 ******************************************************************************/
INT4
R6374CliEncapType (tCliHandle CliHandle, UINT4 u4ContextId,
                   tSNMP_OCTET_STRING_TYPE ServiceName, INT4 i4Type)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374EncapType (&u4ErrorCode, u4ContextId,
                                  &ServiceName, i4Type) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374EncapType (u4ContextId, &ServiceName, i4Type) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliShowStatsAll
 *
 *     DESCRIPTION      : This function will display service related statistics.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        u4Command - Type of the command
 *                        ServiceName -
 *                        pu1Arg1 - Argument depending upon type of the command
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/
INT4
R6374CliShowStatsAll (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    UINT4               u4CurrentContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (u4ContextId);

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4CurrentContextId,
                                                  &ServiceName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (RFC6374_CLI_SERVICE_TABLE_EMPTY);
        return CLI_FAILURE;
    }

    do
    {
        R6374CliShowStats (CliHandle, u4CurrentContextId, ServiceName);

        i4RetVal = nmhGetNextIndexFs6374ServiceConfigTable (u4CurrentContextId,
                                                            &u4NxtContextId,
                                                            &ServiceName,
                                                            &NxtServiceName);
        if (i4RetVal == SNMP_SUCCESS)
        {
            u4CurrentContextId = u4NxtContextId;
            ServiceName.i4_Length = NxtServiceName.i4_Length;
            RFC6374_STRCPY (ServiceName.pu1_OctetList,
                            NxtServiceName.pu1_OctetList);
        }
    }
    while (i4RetVal == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliShowStats
 *
 *     DESCRIPTION      : This function will displays service specific
 *                        statistics. 
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        ServiceName - Service name
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/
INT4
R6374CliShowStats (tCliHandle CliHandle, UINT4 u4ContextId,
                   tSNMP_OCTET_STRING_TYPE ServiceName)
{

    UINT4               u4TxLmmCount = RFC6374_INIT_VAL;
    UINT4               u4RxLmmCount = RFC6374_INIT_VAL;
    UINT4               u4TxLmrCount = RFC6374_INIT_VAL;
    UINT4               u4RxLmrCount = RFC6374_INIT_VAL;
    UINT4               u4Tx1LmCount = RFC6374_INIT_VAL;
    UINT4               u4Rx1LmCount = RFC6374_INIT_VAL;
    UINT4               u4Tx1DmCount = RFC6374_INIT_VAL;
    UINT4               u4Rx1DmCount = RFC6374_INIT_VAL;
    UINT4               u4TxDmmCount = RFC6374_INIT_VAL;
    UINT4               u4RxDmmCount = RFC6374_INIT_VAL;
    UINT4               u4TxDmrCount = RFC6374_INIT_VAL;
    UINT4               u4RxDmrCount = RFC6374_INIT_VAL;
    UINT4               u4TxLmmDmmCount = RFC6374_INIT_VAL;
    UINT4               u4RxLmmDmmCount = RFC6374_INIT_VAL;
    UINT4               u4TxLmrDmrCount = RFC6374_INIT_VAL;
    UINT4               u4RxLmrDmrCount = RFC6374_INIT_VAL;
    UINT4               u4Tx1LmDmCount = RFC6374_INIT_VAL;
    UINT4               u4Rx1LmDmCount = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    tR6374ServiceConfigTableEntry *pServiceConfig = NULL;

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);

    /* Check if Service is already created */
    pServiceConfig = R6374UtilGetServiceFromName (u4ContextId, au1ServiceName);
    if (pServiceConfig == NULL)
    {
        CLI_SET_ERR (RFC6374_CLI_SERVICE_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\n Service Name: %s", au1ServiceName);

    CliPrintf (CliHandle,
               "\r\n-------------------------------------------------------"
               "-------------\r\n");

    nmhGetFs6374StatsLmmOut (u4ContextId, &ServiceName, &u4TxLmmCount);
    nmhGetFs6374StatsLmmIn (u4ContextId, &ServiceName, &u4RxLmmCount);
    nmhGetFs6374StatsLmrOut (u4ContextId, &ServiceName, &u4TxLmrCount);
    nmhGetFs6374StatsLmrIn (u4ContextId, &ServiceName, &u4RxLmrCount);
    nmhGetFs6374Stats1LmOut (u4ContextId, &ServiceName, &u4Tx1LmCount);
    nmhGetFs6374Stats1LmIn (u4ContextId, &ServiceName, &u4Rx1LmCount);

    nmhGetFs6374Stats1DmOut (u4ContextId, &ServiceName, &u4Tx1DmCount);
    nmhGetFs6374Stats1DmIn (u4ContextId, &ServiceName, &u4Rx1DmCount);
    nmhGetFs6374StatsDmmOut (u4ContextId, &ServiceName, &u4TxDmmCount);
    nmhGetFs6374StatsDmmIn (u4ContextId, &ServiceName, &u4RxDmmCount);
    nmhGetFs6374StatsDmrOut (u4ContextId, &ServiceName, &u4TxDmrCount);
    nmhGetFs6374StatsDmrIn (u4ContextId, &ServiceName, &u4RxDmrCount);

    nmhGetFs6374StatsCmbLmmDmmOut (u4ContextId, &ServiceName, &u4TxLmmDmmCount);
    nmhGetFs6374StatsCmbLmmDmmIn (u4ContextId, &ServiceName, &u4RxLmmDmmCount);
    nmhGetFs6374StatsCmbLmrDmrOut (u4ContextId, &ServiceName, &u4TxLmrDmrCount);
    nmhGetFs6374StatsCmbLmrDmrIn (u4ContextId, &ServiceName, &u4RxLmrDmrCount);
    nmhGetFs6374StatsCmb1LmDmOut (u4ContextId, &ServiceName, &u4Tx1LmDmCount);
    nmhGetFs6374StatsCmb1LmDmIn (u4ContextId, &ServiceName, &u4Rx1LmDmCount);

    /* Display statistics */
    CliPrintf (CliHandle, "%-20s", "Transmitted LMM PDU : ");
    CliPrintf (CliHandle, "%-12u", u4TxLmmCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received LMM PDU    : ");
    CliPrintf (CliHandle, "%-11u", u4RxLmmCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted LMR PDU : ");
    CliPrintf (CliHandle, "%-12u", u4TxLmrCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received LMR PDU    : ");
    CliPrintf (CliHandle, "%-11u", u4RxLmrCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted 1LM PDU : ");
    CliPrintf (CliHandle, "%-12u", u4Tx1LmCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received 1LM PDU    : ");
    CliPrintf (CliHandle, "%-11u", u4Rx1LmCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted DMM PDU : ");
    CliPrintf (CliHandle, "%-12u", u4TxDmmCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received DMM PDU    : ");
    CliPrintf (CliHandle, "%-11u", u4RxDmmCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted DMR PDU : ");
    CliPrintf (CliHandle, "%-12u", u4TxDmrCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received DMR PDU    : ");
    CliPrintf (CliHandle, "%-11u", u4RxDmrCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted 1DM PDU : ");
    CliPrintf (CliHandle, "%-12u", u4Tx1DmCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received 1DM PDU    : ");
    CliPrintf (CliHandle, "%-11u", u4Rx1DmCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted LMMDMM PDU : ");
    CliPrintf (CliHandle, "%-12u", u4TxLmmDmmCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received LMMDMM PDU : ");
    CliPrintf (CliHandle, "%-11u", u4RxLmmDmmCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted LMRDMR PDU : ");
    CliPrintf (CliHandle, "%-12u", u4TxLmrDmrCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received LMRDMR PDU : ");
    CliPrintf (CliHandle, "%-11u", u4RxLmrDmrCount);
    CliPrintf (CliHandle, "\r\n%-20s", "Transmitted 1LMDM PDU  : ");
    CliPrintf (CliHandle, "%-12u", u4Tx1LmDmCount);
    CliPrintf (CliHandle, "%4s", " ");
    CliPrintf (CliHandle, "%-14s", "Received 1LMDM PDU  : ");
    CliPrintf (CliHandle, "%-11u", u4Rx1LmDmCount);

    CliPrintf (CliHandle,
               "\r\n--------------------------------------------------------"
               "------------\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R6374CliSetDebugLevel                                      *
 *                                                                           *
 * Description :  This function configues the trace option for R6374 module  *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/
INT4
R6374CliSetDebugLevel (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4DebugType, INT4 i4Action)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    UINT4               u4Level = RFC6374_INIT_VAL;

    if (nmhGetFs6374DebugLevel (u4ContextId, &u4Level) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4Action == RFC6374_ENABLED)
    {
        u4DebugType = u4DebugType | u4Level;
    }
    else
    {
        u4DebugType = u4DebugType & u4Level;
    }

    if (nmhTestv2Fs6374DebugLevel (&u4ErrorCode, u4ContextId,
                                   u4DebugType) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374DebugLevel (u4ContextId, u4DebugType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374GetServiceConfCfgPrompt                         *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Service configuration. It is exported to         *
 *                    CLI module.                                          *
 *                    This function will be invoked when the System is     *
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
R6374GetServiceConfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("service-R6374");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "service-R6374", u4Len) != RFC6374_INIT_VAL)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_rfc6374_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-service)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowServiceConfigInfoAll                     *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for all the services                                 *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT4
R6374CliShowServiceConfigInfoAll (tCliHandle CliHandle, UINT4 u4ContextId,
                                  UINT1 u1Detail)
{
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    UINT4               u4CurrentContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    INT4                i4RetVal = SNMP_FAILURE;

    UNUSED_PARAM (u4ContextId);

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4CurrentContextId,
                                                  &ServiceName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (RFC6374_CLI_SERVICE_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    do
    {
        R6374CliShowServiceConfigInfo (CliHandle, u4CurrentContextId,
                                       ServiceName, u1Detail);

        i4RetVal = nmhGetNextIndexFs6374ServiceConfigTable (u4CurrentContextId,
                                                            &u4NxtContextId,
                                                            &ServiceName,
                                                            &NxtServiceName);

        if (i4RetVal == SNMP_SUCCESS)
        {
            u4CurrentContextId = u4NxtContextId;

            ServiceName.i4_Length = NxtServiceName.i4_Length;
            RFC6374_STRCPY (&ServiceName.pu1_OctetList,
                            &NxtServiceName.pu1_OctetList);
        }
    }
    while (i4RetVal == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowServiceConfigInfo                        *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    Fs6374ServiceName                                    *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT4
R6374CliShowServiceConfigInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE ServiceName,
                               UINT1 u1Detail)
{
    INT4                i4MplsPathType = RFC6374_INIT_VAL;
    INT4                i4EncapType = RFC6374_INIT_VAL;
    INT4                i4TrafficClass = RFC6374_INIT_VAL;
    INT4                i4DyadicMeasurement = RFC6374_INIT_VAL;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;
    INT4                i4NegoStatus = RFC6374_INIT_VAL;
    UINT4               u4IpAddr = RFC6374_INIT_VAL;
    UINT4               u4FwdTnlIdOrPwId = RFC6374_INIT_VAL;
    UINT4               u4RevTnlId = RFC6374_INIT_VAL;
    UINT4               u4NegotiatedTSF = RFC6374_INIT_VAL;
    UINT4               u4QueryTransmitRetryCount = RFC6374_INIT_VAL;
    INT4                i4SessionIntervalQueryStatus = RFC6374_INIT_VAL;
    UINT4               u4MinReceptionIntervalInMilliseconds = RFC6374_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE SrcIpAddr;
    tSNMP_OCTET_STRING_TYPE DestIpAddr;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1SrcIpAddr[RFC6374_IPV4_ADDR_LEN];
    UINT1               au1DestIpAddr[RFC6374_IPV4_ADDR_LEN];
    CHR1               *pc1Prefix = NULL;
    tR6374ServiceConfigTableEntry *pServiceConfig = NULL;

    RFC6374_MEMSET (&SrcIpAddr, RFC6374_INIT_VAL, sizeof (SrcIpAddr));
    RFC6374_MEMSET (&DestIpAddr, RFC6374_INIT_VAL, sizeof (DestIpAddr));
    RFC6374_MEMSET (au1SrcIpAddr, RFC6374_INIT_VAL, sizeof (au1SrcIpAddr));
    RFC6374_MEMSET (au1DestIpAddr, RFC6374_INIT_VAL, sizeof (au1DestIpAddr));
    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));

    RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);

    /* Destination IP pointer */
    DestIpAddr.pu1_OctetList = au1DestIpAddr;
    DestIpAddr.i4_Length = RFC6374_INIT_VAL;

    /* Source IP pointer */
    SrcIpAddr.pu1_OctetList = au1SrcIpAddr;
    SrcIpAddr.i4_Length = RFC6374_INIT_VAL;

    /* Check if Service is already created */
    pServiceConfig = R6374UtilGetServiceFromName (u4ContextId, au1ServiceName);
    if (pServiceConfig == NULL)
    {
        CLI_SET_ERR (RFC6374_CLI_SERVICE_TABLE_EMPTY);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n Service Name                    : %s",
               au1ServiceName);

    /*Get the MPLS Type */
    nmhGetFs6374MplsPathType (u4ContextId, &ServiceName, &i4MplsPathType);
    if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
    {
        CliPrintf (CliHandle, "\r\n Path Type                       : "
                   "Tunnel based");
    }
    else if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        CliPrintf (CliHandle, "\r\n Path Type                       : "
                   "PW based");
    }

    /*Get the Associated Forward Tunnel */
    nmhGetFs6374FwdTnlIdOrPwId (u4ContextId, &ServiceName, &u4FwdTnlIdOrPwId);
    if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        if (u4FwdTnlIdOrPwId != RFC6374_INIT_VAL)
        {

            CliPrintf (CliHandle, "\r\n Associated Forward Tunnel/Pw Id : %u",
                       u4FwdTnlIdOrPwId);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Associated Forward Tunnel/Pw Id : -");
        }
    }

    /*Get the Associated Reverse Tunnel */
    if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
    {
        nmhGetFs6374RevTnlId (u4ContextId, &ServiceName, &u4RevTnlId);
        if (u4RevTnlId != RFC6374_INIT_VAL)
        {
            CliPrintf (CliHandle, "\r\n Associated Reverse Tunnel       : %u",
                       u4RevTnlId);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Associated Reverse Tunnel       : -");
        }
        /*Get the Source Ip Address */
        nmhGetFs6374SrcIpAddr (u4ContextId, &ServiceName, &SrcIpAddr);
        RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4IpAddr);
        if (u4IpAddr != 0)
        {
            RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);
            CliPrintf (CliHandle, "\r\n Source IP Address               : %s",
                       pc1Prefix);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Source IP Address               : -");
        }
    }

    if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL ||
        i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
    {
        /*Get the Destination Ip Address */
        pc1Prefix = NULL;
        u4IpAddr = RFC6374_INIT_VAL;
        nmhGetFs6374DestIpAddr (u4ContextId, &ServiceName, &DestIpAddr);
        RFC6374_OCTETSTRING_TO_INTEGER (DestIpAddr, u4IpAddr);
        if (u4IpAddr != 0)
        {
            RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

            CliPrintf (CliHandle, "\r\n Destination IP Address          : %s",
                       pc1Prefix);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Destination IP Address          : -");
        }
    }
    /*Get the Encap Type */
    nmhGetFs6374EncapType (u4ContextId, &ServiceName, &i4EncapType);
    if (i4EncapType == RFC6374_ENCAP_NO_GAL_GACH)
    {
        CliPrintf (CliHandle, "\r\n Encap Type             : " "Without GAL");
    }
    else if (i4EncapType == RFC6374_ENCAP_GAL_GACH)
    {
        CliPrintf (CliHandle, "\r\n Encap Type             : " "With GAL");
    }

    /* Dyadic Measurement */
    nmhGetFs6374DyadicMeasurement (u4ContextId, &ServiceName,
                                   &i4DyadicMeasurement);
    if (i4DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n Dyadic Measurement              : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n Dyadic Measurement              : Disabled");
    }

    /* Dyadic Proactive Role */
    nmhGetFs6374DyadicProactiveRole (u4ContextId, &ServiceName,
                                     &i4DyadicProactiveRole);
    if (i4DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_PASSIVE)
    {
        CliPrintf (CliHandle, "\r\n Dyadic Proactive Role           : Passive");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Dyadic Proactive Role           : Active");
    }

    /*Get the Traffic Class */
    nmhGetFs6374TrafficClass (u4ContextId, &ServiceName, &i4TrafficClass);
    CliPrintf (CliHandle, "\r\n Traffic Class                   : %d",
               i4TrafficClass);

    nmhGetFs6374TSFNegotiation (u4ContextId, &ServiceName, &i4NegoStatus);

    CliPrintf (CliHandle, "\r\n Timestamp Negotiation Status    : %s",
               (i4NegoStatus ==
                RFC6374_NEGOTIATION_ENABLE) ? "Enabled" : "Disabled");

    nmhGetFs6374TSNegotiatedFormat (u4ContextId, &ServiceName,
                                    &u4NegotiatedTSF);

    CliPrintf (CliHandle, "\r\n Current Timestamp Format        : %s",
               (u4NegotiatedTSF ==
                RFC6374_SUPP_TS_FORMAT_IEEE1588) ? "PTP Timestamp" :
               "NTP Timestamp");

    nmhGetFs6374SessionIntervalQueryStatus (u4ContextId, &ServiceName,
                                            &i4SessionIntervalQueryStatus);
    if (i4SessionIntervalQueryStatus == RFC6374_SESS_INT_QUERY_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n Session Interval Query Status   : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n Session Interval Query Status   : Disabled");
    }

    nmhGetFs6374MinReceptionIntervalInMilliseconds (u4ContextId, &ServiceName,
                                                    &u4MinReceptionIntervalInMilliseconds);

    CliPrintf (CliHandle, "\r\n Minimum Reception Interval      : %d ms",
               u4MinReceptionIntervalInMilliseconds);

    nmhGetFs6374QueryTransmitRetryCount (u4ContextId, &ServiceName,
                                         &u4QueryTransmitRetryCount);
    CliPrintf (CliHandle, "\r\n Query Transmit Retry Count      : %d\r\n",
               u4QueryTransmitRetryCount);

    if (u1Detail)
    {
        R6374ShowServiceConfigInfoDetail (CliHandle, u4ContextId, ServiceName);
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowLmBufferAll                              *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    u1Detail                                             * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT4
R6374CliShowLmBufferAll (tCliHandle CliHandle,
                         UINT4 u4ContextId, UINT1 u1Detail)
{
    UINT4               u4SessId = RFC6374_INIT_VAL;
    UINT4               u4NxtSessId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4ReturnVal = CLI_SUCCESS;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    BOOL1               b1SameEntry = RFC6374_FALSE;
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374LMStatsTable (&u4ContextId,
                                            &ServiceName,
                                            &u4SessId) != SNMP_FAILURE)
    {
        while (i4RetVal == SNMP_SUCCESS)
        {
            if (b1SameEntry == RFC6374_FALSE)
            {
                i4ReturnVal = R6374CliShowLmBufferPerService (CliHandle,
                                                              u4ContextId,
                                                              ServiceName,
                                                              u1Detail);
                if (i4ReturnVal != CLI_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_MGMT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374CliShowLmBufferAll:"
                                 "R6374CliShowLmBufferPerService failed.\r\n");
                }

            }
            i4RetVal = nmhGetNextIndexFs6374LMStatsTable (u4ContextId,
                                                          &u4NxtContextId,
                                                          &ServiceName,
                                                          &NxtServiceName,
                                                          u4SessId,
                                                          &u4NxtSessId);

            if (i4RetVal == SNMP_SUCCESS)
            {
                if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                    NxtServiceName.pu1_OctetList,
                                    NxtServiceName.i4_Length) ==
                    RFC6374_INIT_VAL)
                {
                    u4SessId = u4NxtSessId;
                    b1SameEntry = RFC6374_TRUE;
                }
                else
                {
                    RFC6374_STRNCPY (ServiceName.pu1_OctetList,
                                     NxtServiceName.pu1_OctetList,
                                     NxtServiceName.i4_Length + 1);
                    ServiceName.i4_Length = NxtServiceName.i4_Length;

                    u4SessId = RFC6374_INIT_VAL;
                    u4NxtSessId = RFC6374_INIT_VAL;
                    b1SameEntry = RFC6374_FALSE;
                    CliPrintf (CliHandle, "\r\n");
                }
            }
        }
    }
    else
    {
        R6374CliLmDmNoRespSess (CliHandle, u4ContextId, NULL,
                                RFC6374_MEASUREMENT_TYPE_LOSS);
        CLI_SET_ERR (RFC6374_CLI_LM_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowLmBufferPerService                       *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    u1Detail                                             * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT4
R6374CliShowLmBufferPerService (tCliHandle CliHandle, UINT4 u4ContextId,
                                tSNMP_OCTET_STRING_TYPE ServiceName,
                                UINT1 u1Detail)
{

    tR6374LmStatsTableEntry *pLmStatsNodeInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tSNMP_OCTET_STRING_TYPE SrcIpAddr;
    tSNMP_OCTET_STRING_TYPE DestIpAddr;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    tSNMP_OCTET_STRING_TYPE ThroughPut;
    UINT4               u4FwdTnlId = RFC6374_INIT_VAL;
    UINT4               u4RevTnlId = RFC6374_INIT_VAL;
    UINT4               u4PacketSent = RFC6374_INIT_VAL;
    UINT4               u4PacketRecvd = RFC6374_INIT_VAL;
    UINT4               u4PktLossDueToErrors = RFC6374_INIT_VAL;
    UINT4               u4PktLossDueToTimeout = RFC6374_INIT_VAL;
    UINT4               u4TxMinLoss = RFC6374_INIT_VAL;
    UINT4               u4TxAvgLoss = RFC6374_INIT_VAL;
    UINT4               u4TxMaxLoss = RFC6374_INIT_VAL;
    UINT4               u4RxMinLoss = RFC6374_INIT_VAL;
    UINT4               u4RxAvgLoss = RFC6374_INIT_VAL;
    UINT4               u4RxMaxLoss = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtSessId = RFC6374_INIT_VAL;
    UINT4               u4IpAddr = RFC6374_INIT_VAL;
    UINT4               u4SessId = RFC6374_INIT_VAL;
    UINT4               u4RemoteSessionId = RFC6374_INIT_VAL;
    UINT4               u4RetMeasurementTime = RFC6374_INIT_VAL;
    INT4                i4Type = RFC6374_INIT_VAL;
    INT4                i4NextType = RFC6374_INIT_VAL;
    INT4                i4PrevType = RFC6374_INIT_VAL;
    INT4                i4LmMode = RFC6374_INIT_VAL;
    INT4                i4LmMethod = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;
    INT4                i4MeasOngoing = RFC6374_INIT_VAL;
    INT4                i4Direction = RFC6374_INIT_VAL;
    INT4                i4TrafficClass = RFC6374_INIT_VAL;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4TSFormat = -1;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;
    CHR1               *pc1Prefix = NULL;
    UINT1               au1DateFormat[RFC6374_ARRAY_SIZE_64] =
        { RFC6374_INIT_VAL };
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1SrcIpAddr[RFC6374_IPV4_ADDR_LEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1DestIpAddr[RFC6374_IPV4_ADDR_LEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1ThroughPut[RFC6374_THROUGH_PUT_LENGTH] =
        { RFC6374_INIT_VAL };
    CONST CHR1         *apLmTSFormat[] = { "Null Timestamp", "Sequence Number",
        "NTP Timestamp", "PTP Timestamp", NULL
    };
    BOOL1               b1SameEntry = RFC6374_FALSE;

    RFC6374_MEMSET (&SrcIpAddr, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&DestIpAddr, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&ThroughPut, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if ((nmhGetNextIndexFs6374LMStatsTable (u4ContextId, &u4NxtContextId,
                                            &ServiceName, &NxtServiceName, 0,
                                            &u4SessId) != SNMP_FAILURE) &&
        (RFC6374_MEMCMP
         (ServiceName.pu1_OctetList, NxtServiceName.pu1_OctetList,
          NxtServiceName.i4_Length) == RFC6374_INIT_VAL))
    {
        while (i4RetVal == SNMP_SUCCESS)
        {
            if (u4ContextId != u4NxtContextId)
            {
                return CLI_FAILURE;
            }
            u4ContextId = u4NxtContextId;
            RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL,
                            sizeof (au1ServiceName));
            RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                            ServiceName.i4_Length);

            if (b1SameEntry != RFC6374_TRUE)
            {
                CliPrintf (CliHandle, "\r\n Service Name               : %s",
                           au1ServiceName);

                nmhGetFs6374LMStatsMplsType (u4ContextId, &ServiceName,
                                             u4SessId, &i4MplsPathType);

                if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
                {
                    nmhGetFs6374LMStatsFwdTnlIdOrPwId (u4ContextId,
                                                       &ServiceName, u4SessId,
                                                       &u4FwdTnlId);
                    if (u4FwdTnlId != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle, "\r\n Associated Forward Tunnel  "
                                   ": %d", u4FwdTnlId);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n Associated Forward Tunnel  "
                                   ": -");
                    }

                    nmhGetFs6374LMStatsRevTnlId (u4ContextId, &ServiceName,
                                                 u4SessId, &u4RevTnlId);
                    if (u4RevTnlId != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle, "\r\n Associated Reverse Tunnel  "
                                   ": %d", u4RevTnlId);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n Associated Reverse Tunnel  "
                                   ": -");
                    }

                    /* Source IP pointer */
                    RFC6374_MEMSET (au1SrcIpAddr, RFC6374_INIT_VAL,
                                    sizeof (au1SrcIpAddr));
                    SrcIpAddr.pu1_OctetList = au1SrcIpAddr;
                    SrcIpAddr.i4_Length = RFC6374_INIT_VAL;

                    nmhGetFs6374LMStatsSrcIpAddr (u4ContextId, &ServiceName,
                                                  u4SessId, &SrcIpAddr);

                    RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4IpAddr);
                    RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

                    if (u4IpAddr != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle, "\r\n Source IP Address          "
                                   ": %s", pc1Prefix);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n Source IP Address          "
                                   ": -");
                    }

                    /* Destination IP pointer */
                    RFC6374_MEMSET (au1DestIpAddr, RFC6374_INIT_VAL,
                                    sizeof (au1DestIpAddr));
                    DestIpAddr.pu1_OctetList = au1DestIpAddr;
                    DestIpAddr.i4_Length = RFC6374_INIT_VAL;

                    nmhGetFs6374LMStatsDestIpAddr (u4ContextId, &ServiceName,
                                                   u4SessId, &DestIpAddr);

                    RFC6374_OCTETSTRING_TO_INTEGER (DestIpAddr, u4IpAddr);

                    RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

                    if (u4IpAddr != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle, "\r\n Destination IP Address     "
                                   ": %s", pc1Prefix);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n Destination IP Address     "
                                   ": -");
                    }
                }
                else
                {
                    nmhGetFs6374LMStatsFwdTnlIdOrPwId (u4ContextId,
                                                       &ServiceName, u4SessId,
                                                       &u4FwdTnlId);
                    if (u4FwdTnlId != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle, "\r\n Associated PW ID           "
                                   ": %d", u4FwdTnlId);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n Associated PW ID           "
                                   ": -");
                    }

                    RFC6374_MEMSET (au1SrcIpAddr, RFC6374_INIT_VAL,
                                    sizeof (au1SrcIpAddr));
                    SrcIpAddr.pu1_OctetList = au1SrcIpAddr;
                    SrcIpAddr.i4_Length = RFC6374_INIT_VAL;

                    nmhGetFs6374LMStatsSrcIpAddr (u4ContextId, &ServiceName,
                                                  u4SessId, &SrcIpAddr);

                    RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4IpAddr);

                    RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

                    if (u4IpAddr != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle, "\r\n PW Destination IP Address  "
                                   ": %s", pc1Prefix);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n PW Destination IP Address  "
                                   ": -");
                    }
                }

            }

            CliPrintf (CliHandle, "\r\n");

            nmhGetFs6374LMStatsType (u4ContextId,
                                     &ServiceName, u4SessId, &i4Type);

            pServiceInfo = R6374UtilGetServiceFromName (u4ContextId,
                                                        au1ServiceName);

            if (pServiceInfo == NULL)
            {
                return CLI_FAILURE;
            }
            pLmStatsNodeInfo =
                R6374UtilGetLmStatsTableInfo (pServiceInfo, u4SessId);

            if (pLmStatsNodeInfo == NULL)
            {
                return CLI_FAILURE;
            }

            if (i4Type == RFC6374_LM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "\r\n Session No. : %u\r\n", u4SessId);
            }
            else
            {
                nmhGetFs6374LMStatsMeasurementOnGoing (u4ContextId,
                                                       &ServiceName, u4SessId,
                                                       &i4MeasOngoing);
                if (i4MeasOngoing == RFC6374_MEASUREMENT_COMPLETE)
                {
                    CliPrintf (CliHandle, "\r\n Session No.: %u \t\t   "
                               "      Session Status: Completed\r\n", u4SessId);
                }
                else if (i4MeasOngoing == RFC6374_MEASUREMENT_ABORTED)
                {
                    CliPrintf (CliHandle, "\r\n Session No.: %u \t\t    "
                               "     Session Status: Aborted\r\n", u4SessId);
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n Session No.: %u \t\t    "
                               "     Session Status: Ongoing\r\n", u4SessId);
                }
            }

            RFC6374_MEMSET (au1DateFormat, RFC6374_INIT_VAL,
                            sizeof (au1DateFormat));

            nmhGetFs6374LMStatsStartTime (u4ContextId,
                                          &ServiceName, u4SessId,
                                          &u4RetMeasurementTime);

            R6374UtlSecondsToDate (u4RetMeasurementTime, au1DateFormat);
            au1DateFormat[RFC6374_INDEX_TWINTY] = '\0';
            CliPrintf (CliHandle, "\r\n Measurement Start                      "
                       " : %s", au1DateFormat);

            if (i4Type == RFC6374_LM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "\r\n Loss Measurement Type              "
                           "     : One-Way");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n Loss Measurement Type              "
                           "     : Two-Way");
            }

            nmhGetFs6374LMStatsMethod (u4ContextId,
                                       &ServiceName, u4SessId, &i4LmMethod);

            if (i4LmMethod == RFC6374_LM_METHOD_INFERED)
            {
                CliPrintf (CliHandle, "\r\n Loss Method                        "
                           "     : Inferred");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n Loss Method                        "
                           "     : Direct");
            }

            nmhGetFs6374LMStatsResponseErrors (u4ContextId,
                                               &ServiceName, u4SessId,
                                               &u4PktLossDueToErrors);

            nmhGetFs6374LMStatsTrafficClass (u4ContextId, &ServiceName,
                                             u4SessId, &i4TrafficClass);

            if (i4Type == RFC6374_LM_TYPE_ONE_WAY)
            {

                CliPrintf (CliHandle,
                           "\r\n Loss Session Type                    "
                           "   : %s",
                           (pLmStatsNodeInfo->u1SessType ==
                            RFC6374_SESS_TYPE_INDEPENDENT) ?
                           "Independent Loss Measurement" :
                           "Combined Loss-Delay Measurement");

                nmhGetFs6374LMStatsTimeStampFormat (u4ContextId, &ServiceName,
                                                    u4SessId, &i4TSFormat);
                if (i4TSFormat >= RFC6374_INIT_VAL)
                {
                    CliPrintf (CliHandle, "\r\n Loss TS Format                "
                               "          : %s", apLmTSFormat[i4TSFormat]);
                }

                CliPrintf (CliHandle, "\r\n Traffic Class                      "
                           "     : %d\r\n", i4TrafficClass);

                nmhGetFs6374LMStatsRxPktCount (u4ContextId, &ServiceName,
                                               u4SessId, &u4PacketRecvd);

                CliPrintf (CliHandle, "\r\n 1LM Packet Received                "
                           "     : %d", u4PacketRecvd);

                nmhGetFs6374LMStatsNearEndLossMin (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RxMinLoss);

                nmhGetFs6374LMStatsNearEndLossAvg (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RxAvgLoss);

                nmhGetFs6374LMStatsNearEndLossMax (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RxMaxLoss);

                CliPrintf (CliHandle, "\r\n LM Packet Loss due to errors       "
                           "     : %d\r\n", u4PktLossDueToErrors);

                CliPrintf (CliHandle, "\r\n Min/Avg/Max Receive Loss "
                           "(No of Frames) : %u/%u/%u", u4RxMinLoss,
                           u4RxAvgLoss, u4RxMaxLoss);
            }
            else
            {
                nmhGetFs6374LMStatsMode (u4ContextId,
                                         &ServiceName, u4SessId, &i4LmMode);

                if (i4LmMode == RFC6374_LM_MODE_ONDEMAND)
                {
                    CliPrintf (CliHandle, "\r\n Loss Mode                      "
                               "         : On-Demand");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n Loss Mode                      "
                               "         : Proactive");
                }

                CliPrintf (CliHandle,
                           "\r\n Loss Session Type                    "
                           "   : %s",
                           (pLmStatsNodeInfo->u1SessType ==
                            RFC6374_SESS_TYPE_INDEPENDENT) ?
                           "Independent Loss Measurement" :
                           "Combined Loss-Delay Measurement");

                nmhGetFs6374LMStatsTimeStampFormat (u4ContextId, &ServiceName,
                                                    u4SessId, &i4TSFormat);

                if (i4TSFormat >= RFC6374_INIT_VAL)
                {
                    CliPrintf (CliHandle, "\r\n Loss TS Format                "
                               "          : %s", apLmTSFormat[i4TSFormat]);
                }

                nmhGetFs6374LMDyadicMeasurement (u4ContextId,
                                                 &ServiceName, u4SessId,
                                                 &i4Direction);

                if (i4Direction == RFC6374_DYADIC_MEAS_ENABLE)
                {
                    CliPrintf (CliHandle, "\r\n Direction                      "
                               "         : Dyadic Enabled");

                    if (i4LmMode == RFC6374_LM_MODE_PROACTIVE)
                    {
                        nmhGetFs6374LMDyadicProactiveRole (u4ContextId,
                                                           &ServiceName,
                                                           u4SessId,
                                                           &i4DyadicProactiveRole);

                        if (i4DyadicProactiveRole ==
                            RFC6374_DYADIC_PRO_ROLE_PASSIVE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Dyadic Proactive Role          "
                                       "         : Passive");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Dyadic Proactive Role          "
                                       "         : Active");
                        }
                    }

                    /* Get the Remote Session ID */
                    nmhGetFs6374LMRemoteSessionId (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RemoteSessionId);
                    CliPrintf (CliHandle,
                               "\r\n Dyadic Remote Session ID       "
                               "         : %d", u4RemoteSessionId);
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n Direction                      "
                               "         : Dyadic Disabled");

                    CliPrintf (CliHandle, "\r\n Buffer SQI Enabled Status      "
                               "         : %s",
                               (pLmStatsNodeInfo->b1IsLMBufferSQIEnabled ==
                                OSIX_TRUE) ? "Enabled" : "Disabled");
                    CliPrintf (CliHandle,
                               "\r\n Adjusted Query Interval (ms)   "
                               "         : %d",
                               pLmStatsNodeInfo->u2LmAdjustedInterval);

                }

                CliPrintf (CliHandle, "\r\n Traffic Class                      "
                           "     : %d\r\n", i4TrafficClass);

                nmhGetFs6374LMStatsTxPktCount (u4ContextId, &ServiceName,
                                               u4SessId, &u4PacketSent);
                CliPrintf (CliHandle, "\r\n LMM Packet Sent                    "
                           "     : %d", u4PacketSent);

                nmhGetFs6374LMStatsRxPktCount (u4ContextId, &ServiceName,
                                               u4SessId, &u4PacketRecvd);

                if (i4Direction == RFC6374_DYADIC_MEAS_ENABLE)
                {
                    CliPrintf (CliHandle,
                               "\r\n LMM Packet Received                "
                               "     : %d", u4PacketRecvd);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\n LMR Packet Received                "
                               "     : %d", u4PacketRecvd);
                }

                CliPrintf (CliHandle, "\r\n LM Packet Loss due to errors       "
                           "     : %d", u4PktLossDueToErrors);

                if (i4Direction != RFC6374_DYADIC_MEAS_ENABLE)
                {
                    nmhGetFs6374LMStatsResponseTimeout (u4ContextId,
                                                        &ServiceName, u4SessId,
                                                        &u4PktLossDueToTimeout);

                    CliPrintf (CliHandle,
                               "\r\n LM Packet Loss due to timeout      "
                               "     : %d\r\n", u4PktLossDueToTimeout);
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }

                /*Get the Min, Avg and Max Loss */
                nmhGetFs6374LMStatsFarEndLossMin (u4ContextId, &ServiceName,
                                                  u4SessId, &u4TxMinLoss);

                nmhGetFs6374LMStatsFarEndLossAvg (u4ContextId,
                                                  &ServiceName, u4SessId,
                                                  &u4TxAvgLoss);

                nmhGetFs6374LMStatsFarEndLossMax (u4ContextId,
                                                  &ServiceName, u4SessId,
                                                  &u4TxMaxLoss);

                nmhGetFs6374LMStatsNearEndLossMin (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RxMinLoss);

                nmhGetFs6374LMStatsNearEndLossAvg (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RxAvgLoss);

                nmhGetFs6374LMStatsNearEndLossMax (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RxMaxLoss);

                /* Through Put */
                RFC6374_MEMSET (au1ThroughPut, RFC6374_INIT_VAL,
                                sizeof (au1ThroughPut));
                ThroughPut.pu1_OctetList = au1ThroughPut;
                ThroughPut.i4_Length = RFC6374_INIT_VAL;

                nmhGetFs6374LMStatsThroughputpercent (u4ContextId,
                                                      &ServiceName, u4SessId,
                                                      &ThroughPut);

                CliPrintf (CliHandle, "\r\n Min/Avg/Max Transmit Loss "
                           "(No of Frames): %u/%u/%u", u4TxMinLoss,
                           u4TxAvgLoss, u4TxMaxLoss);
                CliPrintf (CliHandle, "\r\n Min/Avg/Max Receive Loss "
                           "(No of Frames) : %u/%u/%u", u4RxMinLoss,
                           u4RxAvgLoss, u4RxMaxLoss);

                CliPrintf (CliHandle, "\r\n Throughput(%%)                     "
                           "      : %s", au1ThroughPut);
            }

            if (u1Detail)
            {
                R6374ShowLmBufferPerServiceDetail (CliHandle, u4ContextId,
                                                   ServiceName, u4SessId,
                                                   i4Type);

                CliPrintf (CliHandle, "\r\n NOTE: Detail of only last Five "
                           "Packets are shown.");
            }
            /* Get next index */
            i4RetVal = nmhGetNextIndexFs6374LMStatsTable (u4ContextId,
                                                          &u4NxtContextId,
                                                          &ServiceName,
                                                          &NxtServiceName,
                                                          u4SessId,
                                                          &u4NxtSessId);

            if (i4RetVal == SNMP_SUCCESS)
            {
                if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                    NxtServiceName.pu1_OctetList,
                                    NxtServiceName.i4_Length) ==
                    RFC6374_INIT_VAL)
                {
                    b1SameEntry = RFC6374_TRUE;
                    nmhGetFs6374LMStatsType (u4ContextId, &ServiceName,
                                             u4SessId, &i4PrevType);
                    nmhGetFs6374LMStatsType (u4ContextId, &ServiceName,
                                             u4NxtSessId, &i4NextType);
                    if (i4PrevType != i4NextType)
                    {
                        b1SameEntry = RFC6374_FALSE;
                        CliPrintf (CliHandle, "\r\n");
                    }
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                    CliPrintf (CliHandle, "\r\n");
                    R6374CliLmDmNoRespSess (CliHandle, u4ContextId,
                                            &ServiceName,
                                            RFC6374_MEASUREMENT_TYPE_LOSS);
                }
                u4SessId = u4NxtSessId;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                R6374CliLmDmNoRespSess (CliHandle, u4ContextId, &ServiceName,
                                        RFC6374_MEASUREMENT_TYPE_LOSS);
            }
        }
    }
    else
    {
        R6374CliLmDmNoRespSess (CliHandle, u4ContextId, &ServiceName,
                                RFC6374_MEASUREMENT_TYPE_LOSS);
        CLI_SET_ERR (RFC6374_CLI_LM_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374ShowLmBufferPerServiceDetail                    *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    u1Detail                                             * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
VOID
R6374ShowLmBufferPerServiceDetail (tCliHandle CliHandle,
                                   UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE ServiceName,
                                   UINT4 u4SessId, INT4 i4Type)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT4               u4SeqId = RFC6374_INIT_VAL;
    UINT4               u4NxtSeqId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtSessId = RFC6374_INIT_VAL;
    UINT4               u4PacketRecvd = RFC6374_INIT_VAL;
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1MeasurementResult[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1MeasResult[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT4               u4MeasurementInSec = RFC6374_INIT_VAL;
    UINT4               u4MeasurementInNano = RFC6374_INIT_VAL;
    FLT4                f4MeasInMs = 0.0;
    tSNMP_OCTET_STRING_TYPE TimeTakenInMilliseconds;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    BOOL1               b1FirstEntry = RFC6374_TRUE;
    tR6374LmBufferTableEntry *pR6374LMBufferTableEntry = NULL;
    tR6374LmBufferTableEntry R6374LMBufferTableEntry;

    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));
    RFC6374_MEMSET (&TimeTakenInMilliseconds, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (au1MeasurementResult, RFC6374_INIT_VAL,
                    sizeof (au1MeasurementResult));
    RFC6374_MEMSET (au1MeasResult, RFC6374_INIT_VAL, sizeof (au1MeasResult));
    RFC6374_MEMSET (&R6374LMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374LmBufferTableEntry));

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetNextIndexFs6374LMTable (u4ContextId, &u4NxtContextId,
                                      &ServiceName, &NxtServiceName, u4SessId,
                                      &u4NxtSessId, 0,
                                      &u4SeqId) != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        if (i4Type == CLI_RFC6374_TYPE_TWO_WAY)
        {
            CliPrintf (CliHandle,
                       "\r\n  Seq Id       Transmit Loss      Receive Loss"
                       "      Total Time(ms)      Tx Packet Count");
            CliPrintf (CliHandle,
                       "\r\n ---------     -------------      ------------"
                       "      --------------      ---------------");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n  Seq Id        One way Loss      "
                       "Total Time(ms)      Tx Packet Count");
            CliPrintf (CliHandle, "\r\n ---------      ------------      "
                       "--------------      ---------------");
        }
        while (i4RetVal == SNMP_SUCCESS)
        {
            /* Measurement Result */
            TimeTakenInMilliseconds.pu1_OctetList = au1MeasurementResult;
            TimeTakenInMilliseconds.i4_Length = RFC6374_INIT_VAL;

            /*Get the time taken */
            nmhGetFs6374LMMeasurementTimeTakenInMilliseconds (u4ContextId,
                                                              &ServiceName,
                                                              u4SessId, u4SeqId,
                                                              &TimeTakenInMilliseconds);

            RFC6374_GET_4BYTE (u4MeasurementInSec,
                               TimeTakenInMilliseconds.pu1_OctetList);
            RFC6374_GET_4BYTE (u4MeasurementInNano,
                               TimeTakenInMilliseconds.pu1_OctetList);

            f4MeasInMs =
                (((FLT4) u4MeasurementInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                (((FLT4) u4MeasurementInNano) / RFC6374_NUM_OF_USEC_IN_A_SEC);

            SPRINTF ((CHR1 *) au1MeasResult, "%f", (DBL8) f4MeasInMs);

            au1MeasResult[RFC6374_INDEX_FIVE] = '\0';
            u4MeasurementInSec = RFC6374_INIT_VAL;
            u4MeasurementInNano = RFC6374_INIT_VAL;
            f4MeasInMs = 0.0;

            /* Loss Measurement Sequence number rolling buffer */
            R6374LMBufferTableEntry.u4ContextId = u4ContextId;

            RFC6374_MEMCPY (R6374LMBufferTableEntry.au1ServiceName,
                            ServiceName.pu1_OctetList, ServiceName.i4_Length);

            R6374LMBufferTableEntry.u4SessionId = u4SessId;
            R6374LMBufferTableEntry.u4SeqCount = u4SeqId;

            pR6374LMBufferTableEntry = (tR6374LmBufferTableEntry *)
                RBTreeGet (RFC6374_LMBUFFER_TABLE,
                           (tRBElem *) & R6374LMBufferTableEntry);

            if (pR6374LMBufferTableEntry == NULL)
            {
                return;
            }

            nmhGetFs6374LMStatsRxPktCount (u4ContextId, &ServiceName,
                                           u4SessId, &u4PacketRecvd);

            if (i4Type == CLI_RFC6374_TYPE_TWO_WAY)
            {
                if ((u4PacketRecvd > RFC6374_INDEX_FIVE) ||
                    (b1FirstEntry == RFC6374_FALSE))
                {
                    CliPrintf (CliHandle, "\r\n %5d %14d %19d %21s %19d",
                               pR6374LMBufferTableEntry->u4BufSeqId,
                               pR6374LMBufferTableEntry->u4CalcTxLossSender,
                               pR6374LMBufferTableEntry->u4CalcRxLossSender,
                               au1MeasResult,
                               pR6374LMBufferTableEntry->u4TxTestPktsCount);
                }
                else
                {
                    b1FirstEntry = RFC6374_FALSE;
                }
            }
            else
            {
                if ((u4PacketRecvd > RFC6374_INDEX_FIVE) ||
                    (b1FirstEntry == RFC6374_FALSE))
                {
                    CliPrintf (CliHandle, "\r\n %5d %14d %21s %19d",
                               pR6374LMBufferTableEntry->u4BufSeqId,
                               pR6374LMBufferTableEntry->u4CalcRxLossReceiver,
                               au1MeasResult,
                               pR6374LMBufferTableEntry->u4TxTestPktsCount);
                }
                else
                {
                    b1FirstEntry = RFC6374_FALSE;
                }
            }

            /* Get next index */
            i4RetVal = nmhGetNextIndexFs6374LMTable (u4ContextId,
                                                     &u4NxtContextId,
                                                     &ServiceName,
                                                     &NxtServiceName, u4SessId,
                                                     &u4NxtSessId, u4SeqId,
                                                     &u4NxtSeqId);

            if (i4RetVal == SNMP_SUCCESS)
            {
                u4SeqId = u4NxtSeqId;
                if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                    NxtServiceName.pu1_OctetList,
                                    NxtServiceName.i4_Length) ==
                    RFC6374_INIT_VAL)
                {
                    if (u4SessId != u4NxtSessId)
                    {
                        i4RetVal = SNMP_FAILURE;
                    }
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
            }
            else
            {
                i4RetVal = SNMP_FAILURE;
            }
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowDmBufferAll                              *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    u1Detail                                             * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT4
R6374CliShowDmBufferAll (tCliHandle CliHandle,
                         UINT4 u4ContextId, UINT1 u1Detail)
{
    UINT4               u4SessId = RFC6374_INIT_VAL;
    UINT4               u4NxtSessId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4ReturnVal = CLI_SUCCESS;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    BOOL1               b1SameEntry = RFC6374_FALSE;

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374DMStatsTable (&u4ContextId,
                                            &ServiceName,
                                            &u4SessId) != SNMP_FAILURE)
    {
        while (i4RetVal == SNMP_SUCCESS)
        {
            if (b1SameEntry == RFC6374_FALSE)
            {
                i4ReturnVal = R6374CliShowDmBufferPerService (CliHandle,
                                                              u4ContextId,
                                                              ServiceName,
                                                              u1Detail);
                if (i4ReturnVal != CLI_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_MGMT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374CliShowDmBufferAll:"
                                 "R6374CliShowDmBufferPerService failed.\r\n");
                }
            }

            i4RetVal = nmhGetNextIndexFs6374DMStatsTable (u4ContextId,
                                                          &u4NxtContextId,
                                                          &ServiceName,
                                                          &NxtServiceName,
                                                          u4SessId,
                                                          &u4NxtSessId);
            if (i4RetVal == SNMP_SUCCESS)
            {
                if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                    NxtServiceName.pu1_OctetList,
                                    NxtServiceName.i4_Length) ==
                    RFC6374_INIT_VAL)
                {
                    u4SessId = u4NxtSessId;
                    b1SameEntry = RFC6374_TRUE;
                }
                else
                {

                    RFC6374_MEMSET (ServiceName.pu1_OctetList, RFC6374_INIT_VAL,
                                    sizeof (tSNMP_OCTET_STRING_TYPE));

                    RFC6374_STRNCPY (ServiceName.pu1_OctetList,
                                     NxtServiceName.pu1_OctetList,
                                     NxtServiceName.i4_Length + 1);
                    ServiceName.i4_Length = NxtServiceName.i4_Length;

                    u4SessId = RFC6374_INIT_VAL;
                    u4NxtSessId = RFC6374_INIT_VAL;
                    b1SameEntry = RFC6374_FALSE;
                    CliPrintf (CliHandle, "\r\n");
                }
            }
        }
    }
    else
    {
        R6374CliLmDmNoRespSess (CliHandle, u4ContextId, NULL,
                                RFC6374_MEASUREMENT_TYPE_DELAY);
        CLI_SET_ERR (RFC6374_CLI_DM_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowDmBufferPerService                       *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    u1Detail                                             * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT4
R6374CliShowDmBufferPerService (tCliHandle CliHandle, UINT4 u4ContextId,
                                tSNMP_OCTET_STRING_TYPE ServiceName,
                                UINT1 u1Detail)
{
    tR6374DmStatsTableEntry *pDmStatsNodeInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    UINT4               u4FwdTnlId = RFC6374_INIT_VAL;
    UINT4               u4RevTnlId = RFC6374_INIT_VAL;
    UINT4               u4PacketSent = RFC6374_INIT_VAL;
    UINT4               u4PacketRecvd = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtSessId = RFC6374_INIT_VAL;
    UINT4               u4IpAddr = RFC6374_INIT_VAL;
    UINT4               u4SessId = RFC6374_INIT_VAL;
    UINT4               u4RemoteSessionId = RFC6374_INIT_VAL;
    UINT4               u4PktLossDueToErrors = RFC6374_INIT_VAL;
    UINT4               u4PktLossDueToTimeout = RFC6374_INIT_VAL;
    UINT4               u4DelayInSec = RFC6374_INIT_VAL;
    UINT4               u4DelayInNanoSec = RFC6374_INIT_VAL;
    UINT4               u4RetMeasurementTime = RFC6374_INIT_VAL;
    UINT4               u4PadSize = RFC6374_INIT_VAL;
    INT4                i4TrafficClass = RFC6374_INIT_VAL;
    INT4                i4Direction = RFC6374_INIT_VAL;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;
    INT4                i4StatsType = RFC6374_INIT_VAL;
    INT4                i4MeasOngoing = RFC6374_INIT_VAL;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4Mode = RFC6374_INIT_VAL;
    INT4                i4NextType = RFC6374_INIT_VAL;
    INT4                i4PrevType = RFC6374_INIT_VAL;
    INT4                i4TSFormat = -1;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;
    CHR1               *pc1Prefix = NULL;
    FLT4                f4DelayInMs = 0.0;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    tSNMP_OCTET_STRING_TYPE SrcIpAddr;
    tSNMP_OCTET_STRING_TYPE DestIpAddr;
    tSNMP_OCTET_STRING_TYPE MinDelay;
    tSNMP_OCTET_STRING_TYPE AvgDelay;
    tSNMP_OCTET_STRING_TYPE MaxDelay;
    tSNMP_OCTET_STRING_TYPE MinRTDelay;
    tSNMP_OCTET_STRING_TYPE AvgRTDelay;
    tSNMP_OCTET_STRING_TYPE MaxRTDelay;

    tSNMP_OCTET_STRING_TYPE MinPDVDelay;
    tSNMP_OCTET_STRING_TYPE MaxPDVDelay;
    tSNMP_OCTET_STRING_TYPE MinIPDVDelay;
    tSNMP_OCTET_STRING_TYPE MaxIPDVDelay;
    tSNMP_OCTET_STRING_TYPE AvgPDVDelay;
    tSNMP_OCTET_STRING_TYPE AvgIPDVDelay;

    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1SrcIpAddr[RFC6374_IPV4_ADDR_LEN];
    UINT1               au1DestIpAddr[RFC6374_IPV4_ADDR_LEN];
    UINT1               au1RetDelay[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1RTDelay[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1DelayMin[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1DelayMax[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1DelayAvg[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1RTDelayMin[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1RTDelayMax[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1RTDelayAvg[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1PDVDelayMin[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1PDVDelayMax[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1PDVDelayAvg[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1IPDVDelayMin[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1IPDVDelayMax[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1IPDVDelayAvg[RFC6374_INDEX_TEN] =
        { RFC6374_INIT_VAL };
    UINT1               au1DateFormat[RFC6374_ARRAY_SIZE_64] =
        { RFC6374_INIT_VAL };
    BOOL1               b1SameEntry = RFC6374_FALSE;

    RFC6374_MEMSET (&SrcIpAddr, RFC6374_INIT_VAL, sizeof (SrcIpAddr));
    RFC6374_MEMSET (&DestIpAddr, RFC6374_INIT_VAL, sizeof (DestIpAddr));
    RFC6374_MEMSET (&MinDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&AvgDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&MaxDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&MinRTDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&AvgRTDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&MaxRTDelay, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1SrcIpAddr, RFC6374_INIT_VAL, sizeof (au1SrcIpAddr));
    RFC6374_MEMSET (au1DestIpAddr, RFC6374_INIT_VAL, sizeof (au1DestIpAddr));
    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));
    RFC6374_MEMSET (au1DateFormat, RFC6374_INIT_VAL, sizeof (au1DateFormat));

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Destination IP pointer */
    DestIpAddr.pu1_OctetList = au1DestIpAddr;
    DestIpAddr.i4_Length = RFC6374_INIT_VAL;

    /* Source IP pointer */
    SrcIpAddr.pu1_OctetList = au1SrcIpAddr;
    SrcIpAddr.i4_Length = RFC6374_INIT_VAL;

    if ((nmhGetNextIndexFs6374DMStatsTable (u4ContextId, &u4NxtContextId,
                                            &ServiceName, &NxtServiceName, 0,
                                            &u4SessId) != SNMP_FAILURE)
        &&
        (RFC6374_MEMCMP
         (ServiceName.pu1_OctetList, NxtServiceName.pu1_OctetList,
          NxtServiceName.i4_Length) == RFC6374_INIT_VAL))
    {
        while (i4RetVal == SNMP_SUCCESS)
        {
            if (u4ContextId != u4NxtContextId)
            {
                return CLI_FAILURE;
            }
            u4ContextId = u4NxtContextId;
            RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL,
                            sizeof (au1ServiceName));
            RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                            ServiceName.i4_Length);

            nmhGetFs6374DMStatsType (u4ContextId, &ServiceName, u4SessId,
                                     &i4StatsType);

            if (b1SameEntry == RFC6374_FALSE)
            {
                CliPrintf (CliHandle, "\r\n Service Name              : %s",
                           au1ServiceName);

                nmhGetFs6374DMStatsMplsType (u4ContextId, &ServiceName,
                                             u4SessId, &i4MplsPathType);

                if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
                {
                    nmhGetFs6374DMStatsFwdTnlIdOrPwId (u4ContextId,
                                                       &ServiceName, u4SessId,
                                                       &u4FwdTnlId);
                    if (u4FwdTnlId != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Associated Forward Tunnel : %d",
                                   u4FwdTnlId);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Associated Forward Tunnel : -");
                    }

                    nmhGetFs6374DMStatsRevTnlId (u4ContextId, &ServiceName,
                                                 u4SessId, &u4RevTnlId);
                    if (u4RevTnlId != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Associated Reverse Tunnel : %d",
                                   u4RevTnlId);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Associated Reverse Tunnel : -");
                    }

                    nmhGetFs6374DMStatsSrcIpAddr (u4ContextId, &ServiceName,
                                                  u4SessId, &SrcIpAddr);

                    RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4IpAddr);
                    RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

                    if (u4IpAddr != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Source IP Address         : %s",
                                   pc1Prefix);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Source IP Address         : -");
                    }

                    nmhGetFs6374DMStatsDestIpAddr (u4ContextId, &ServiceName,
                                                   u4SessId, &DestIpAddr);

                    RFC6374_OCTETSTRING_TO_INTEGER (DestIpAddr, u4IpAddr);
                    RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

                    if (u4IpAddr != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Destination IP Address    : %s",
                                   pc1Prefix);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Destination IP Address    : -");
                    }
                }
                else
                {
                    nmhGetFs6374DMStatsFwdTnlIdOrPwId (u4ContextId,
                                                       &ServiceName, u4SessId,
                                                       &u4FwdTnlId);
                    if (u4FwdTnlId != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Associated PW ID          : %d",
                                   u4FwdTnlId);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Associated PW ID          : -");
                    }

                    nmhGetFs6374DMStatsSrcIpAddr (u4ContextId, &ServiceName,
                                                  u4SessId, &SrcIpAddr);

                    RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4IpAddr);

                    RFC6374_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

                    if (u4IpAddr != RFC6374_INIT_VAL)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n PW Destination IP Address : %s",
                                   pc1Prefix);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n PW Destination IP Address : -");
                    }
                }
            }

            CliPrintf (CliHandle, "\r\n");

            nmhGetFs6374DMStatsMode (u4ContextId, &ServiceName, u4SessId,
                                     &i4Mode);

            nmhGetFs6374DMStatsTrafficClass (u4ContextId, &ServiceName,
                                             u4SessId, &i4TrafficClass);

            nmhGetFs6374DMStatsMeasurementOnGoing (u4ContextId, &ServiceName,
                                                   u4SessId, &i4MeasOngoing);

            pServiceInfo = R6374UtilGetServiceFromName (u4ContextId,
                                                        au1ServiceName);

            if (pServiceInfo == NULL)
            {
                return CLI_FAILURE;
            }
            pDmStatsNodeInfo =
                R6374UtilGetDmStatsTableInfo (pServiceInfo, u4SessId);

            if (pDmStatsNodeInfo == NULL)
            {
                return CLI_FAILURE;
            }

            if (i4StatsType == RFC6374_DM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "\r\n Session No. : %u\r\n", u4SessId);
            }
            else
            {
                if (i4MeasOngoing == RFC6374_MEASUREMENT_COMPLETE)
                {
                    CliPrintf (CliHandle, "\r\n Session No. : %d \t\t "
                               "  Session Status: Completed\r\n", u4SessId);
                }
                else if (i4MeasOngoing == RFC6374_MEASUREMENT_ABORTED)
                {
                    CliPrintf (CliHandle, "\r\n Session No. : %d \t\t "
                               "  Session Status: Aborted\r\n", u4SessId);
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n Session No. : %d \t\t "
                               "  Session Status: Ongoing\r\n", u4SessId);
                }
            }

            nmhGetFs6374DMStatsStartTime (u4ContextId,
                                          &ServiceName, u4SessId,
                                          &u4RetMeasurementTime);

            R6374UtlSecondsToDate (u4RetMeasurementTime, au1DateFormat);
            au1DateFormat[RFC6374_INDEX_TWINTY] = '\0';

            CliPrintf (CliHandle, "\r\n Measurement Start                : %s",
                       au1DateFormat);

            CliPrintf (CliHandle, "\r\n Delay Measurement Type           : %s",
                       (i4StatsType ==
                        RFC6374_DM_TYPE_ONE_WAY) ? "One-Way" : "Two-Way");

            if (i4StatsType == RFC6374_DM_TYPE_TWO_WAY)
            {
                CliPrintf (CliHandle,
                           "\r\n Delay Mode                       : %s",
                           (i4Mode ==
                            RFC6374_DM_MODE_ONDEMAND) ? "On-Demand" :
                           "Proactive");
            }

            CliPrintf (CliHandle, "\r\n Delay Session Type               : %s",
                       (pDmStatsNodeInfo->u1SessType ==
                        RFC6374_SESS_TYPE_INDEPENDENT) ?
                       "Independent Delay Measurement" :
                       "Combined Loss-Delay Measurement");

            nmhGetFs6374DMStatsTimeStampFormat (u4ContextId, &ServiceName,
                                                u4SessId, &i4TSFormat);

            if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
            {
                CliPrintf (CliHandle, "\r\n Delay TS Format                   "
                           ": NTP Timestamp");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n Delay TS Format                  "
                           ": PTP Timestamp");
            }

            nmhGetFs6374DMDyadicMeasurement (u4ContextId,
                                             &ServiceName, u4SessId,
                                             &i4Direction);

            if (i4StatsType == RFC6374_DM_TYPE_TWO_WAY)
            {
                if (i4Direction == RFC6374_DYADIC_MEAS_ENABLE)
                {
                    CliPrintf (CliHandle, "\r\n Direction                      "
                               "  : Dyadic Enabled");

                    if (i4Mode == RFC6374_DM_MODE_PROACTIVE)
                    {
                        nmhGetFs6374DMDyadicProactiveRole (u4ContextId,
                                                           &ServiceName,
                                                           u4SessId,
                                                           &i4DyadicProactiveRole);

                        if (i4DyadicProactiveRole ==
                            RFC6374_DYADIC_PRO_ROLE_PASSIVE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Dyadic Proactive Role          "
                                       "  : Passive");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Dyadic Proactive Role          "
                                       "  : Active");
                        }
                    }

                    /* Get the Remote Session ID */
                    nmhGetFs6374DMRemoteSessionId (u4ContextId,
                                                   &ServiceName, u4SessId,
                                                   &u4RemoteSessionId);
                    CliPrintf (CliHandle,
                               "\r\n Dyadic Remote Session ID       " "  : %d",
                               u4RemoteSessionId);

                }
                else
                {
                    CliPrintf (CliHandle, "\r\n Direction                      "
                               "  : Dyadic Disabled");

                    CliPrintf (CliHandle, "\r\n Buffer SQI Enabled Status      "
                               "  : %s",
                               (pDmStatsNodeInfo->b1IsDMBufferSQIEnabled ==
                                OSIX_TRUE) ? "Enabled" : "Disabled");
                    CliPrintf (CliHandle,
                               "\r\n Adjusted Query Interval (ms)   " "  : %d",
                               pDmStatsNodeInfo->u2DmAdjustedInterval);
                }
            }

            CliPrintf (CliHandle, "\r\n Traffic Class                    : %d",
                       i4TrafficClass);

            if ((i4StatsType == RFC6374_DM_TYPE_TWO_WAY) &&
                (i4Direction != RFC6374_DYADIC_MEAS_ENABLE))
            {
                nmhGetFs6374DMStatsPaddingSize (u4ContextId, &ServiceName,
                                                u4SessId, &u4PadSize);
                CliPrintf (CliHandle,
                           "\r\n Padding TLV Size                 : %d",
                           u4PadSize);
            }

            CliPrintf (CliHandle, "\r\n");
            nmhGetFs6374DMStatsTxPktCount (u4ContextId, &ServiceName,
                                           u4SessId, &u4PacketSent);

            if (i4StatsType == RFC6374_DM_TYPE_TWO_WAY)
            {
                CliPrintf (CliHandle, "\r\n DMM Packet Sent                  : "
                           "%d", u4PacketSent);
            }

            nmhGetFs6374DMStatsRxPktCount (u4ContextId, &ServiceName,
                                           u4SessId, &u4PacketRecvd);

            if (i4StatsType == RFC6374_DM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "\r\n 1DM Packet Received              : "
                           "%d", u4PacketRecvd);
            }
            else
            {
                if (i4Direction == RFC6374_DYADIC_MEAS_ENABLE)
                {
                    CliPrintf (CliHandle,
                               "\r\n DMM Packet Received              : " "%d",
                               u4PacketRecvd);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\n DMR Packet Received              : " "%d",
                               u4PacketRecvd);
                }
            }

            nmhGetFs6374DMStatsResponseErrors (u4ContextId,
                                               &ServiceName, u4SessId,
                                               &u4PktLossDueToErrors);

            CliPrintf (CliHandle, "\r\n DM Packet Loss due to errors     : %d",
                       u4PktLossDueToErrors);

            if ((i4StatsType == RFC6374_DM_TYPE_TWO_WAY) &&
                (i4Direction != RFC6374_DYADIC_MEAS_ENABLE))
            {
                nmhGetFs6374DMStatsResponseTimeout (u4ContextId,
                                                    &ServiceName, u4SessId,
                                                    &u4PktLossDueToTimeout);

                CliPrintf (CliHandle, "\r\n DM Packet Loss due to timeout    : "
                           "%d\r\n", u4PktLossDueToTimeout);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            RFC6374_MEMSET (&AvgDelay, RFC6374_INIT_VAL,
                            sizeof (tSNMP_OCTET_STRING_TYPE));
            RFC6374_MEMSET (&MinDelay, RFC6374_INIT_VAL,
                            sizeof (tSNMP_OCTET_STRING_TYPE));
            RFC6374_MEMSET (&MaxDelay, RFC6374_INIT_VAL,
                            sizeof (tSNMP_OCTET_STRING_TYPE));
            RFC6374_MEMSET (au1RetDelay, RFC6374_INIT_VAL,
                            sizeof (au1RetDelay));
            RFC6374_MEMSET (au1RTDelay, RFC6374_INIT_VAL, sizeof (au1RTDelay));
            RFC6374_MEMSET (au1DelayMin, RFC6374_INIT_VAL,
                            sizeof (au1DelayMin));
            RFC6374_MEMSET (au1DelayMax, RFC6374_INIT_VAL,
                            sizeof (au1DelayMax));
            RFC6374_MEMSET (au1DelayAvg, RFC6374_INIT_VAL,
                            sizeof (au1DelayAvg));

            MinDelay.pu1_OctetList = au1RetDelay;
            MinDelay.i4_Length = RFC6374_INIT_VAL;

            AvgDelay.pu1_OctetList = au1RetDelay;
            AvgDelay.i4_Length = RFC6374_INIT_VAL;

            MaxDelay.pu1_OctetList = au1RetDelay;
            MaxDelay.i4_Length = RFC6374_INIT_VAL;

            u4DelayInSec = RFC6374_INIT_VAL;
            u4DelayInNanoSec = RFC6374_INIT_VAL;
            f4DelayInMs = 0.0;

            nmhGetFs6374DMStatsAvgDelay (u4ContextId,
                                         &ServiceName, u4SessId, &AvgDelay);

            RFC6374_GET_4BYTE (u4DelayInSec, AvgDelay.pu1_OctetList);
            RFC6374_GET_4BYTE (u4DelayInNanoSec, AvgDelay.pu1_OctetList);

            f4DelayInMs =
                (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

            SPRINTF ((CHR1 *) au1DelayAvg, "%f", (DBL8) f4DelayInMs);
            au1DelayAvg[RFC6374_INDEX_FIVE] = '\0';

            u4DelayInSec = RFC6374_INIT_VAL;
            u4DelayInNanoSec = RFC6374_INIT_VAL;
            f4DelayInMs = 0.0;

            nmhGetFs6374DMStatsMaxDelay (u4ContextId,
                                         &ServiceName, u4SessId, &MaxDelay);

            RFC6374_GET_4BYTE (u4DelayInSec, MaxDelay.pu1_OctetList);
            RFC6374_GET_4BYTE (u4DelayInNanoSec, MaxDelay.pu1_OctetList);

            f4DelayInMs =
                (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

            SPRINTF ((CHR1 *) au1DelayMax, "%f", (DBL8) f4DelayInMs);
            au1DelayMax[RFC6374_INDEX_FIVE] = '\0';

            u4DelayInSec = RFC6374_INIT_VAL;
            u4DelayInNanoSec = RFC6374_INIT_VAL;
            f4DelayInMs = 0.0;

            /*Get the Min, Avg and Max Delay */
            nmhGetFs6374DMStatsMinDelay (u4ContextId,
                                         &ServiceName, u4SessId, &MinDelay);

            RFC6374_GET_4BYTE (u4DelayInSec, MinDelay.pu1_OctetList);
            RFC6374_GET_4BYTE (u4DelayInNanoSec, MinDelay.pu1_OctetList);

            f4DelayInMs =
                (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

            SPRINTF ((CHR1 *) au1DelayMin, "%f", (DBL8) f4DelayInMs);
            au1DelayMin[RFC6374_INDEX_FIVE] = '\0';

            CliPrintf (CliHandle, "\r\n Min/Avg/Max Delay (ms)           : "
                       "%s/%s/%s", au1DelayMin, au1DelayAvg, au1DelayMax);

            /* RT Delay for Two-way and Non-Dyadic Delay */
            if ((i4StatsType == RFC6374_DM_TYPE_TWO_WAY) &&
                (i4Direction != RFC6374_DYADIC_MEAS_ENABLE))
            {
                RFC6374_MEMSET (au1RTDelayMin, RFC6374_INIT_VAL,
                                sizeof (au1RTDelayMin));
                RFC6374_MEMSET (au1RTDelayMax, RFC6374_INIT_VAL,
                                sizeof (au1RTDelayMax));
                RFC6374_MEMSET (au1RTDelayAvg, RFC6374_INIT_VAL,
                                sizeof (au1RTDelayAvg));

                MinRTDelay.pu1_OctetList = au1RTDelay;
                MinRTDelay.i4_Length = RFC6374_INIT_VAL;

                AvgRTDelay.pu1_OctetList = au1RTDelay;
                AvgRTDelay.i4_Length = RFC6374_INIT_VAL;

                MaxRTDelay.pu1_OctetList = au1RTDelay;
                MaxRTDelay.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsAvgRTDelay (u4ContextId,
                                               &ServiceName, u4SessId,
                                               &AvgRTDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, AvgRTDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec, AvgRTDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1RTDelayAvg, "%f", (DBL8) f4DelayInMs);
                au1RTDelayAvg[RFC6374_INDEX_FIVE] = '\0';

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsMaxRTDelay (u4ContextId,
                                               &ServiceName, u4SessId,
                                               &MaxRTDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, MaxRTDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec, MaxRTDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1RTDelayMax, "%f", (DBL8) f4DelayInMs);

                au1RTDelayMax[RFC6374_INDEX_FIVE] = '\0';
                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                /*Get the Min, Avg and Max RT Delay */
                nmhGetFs6374DMStatsMinRTDelay (u4ContextId,
                                               &ServiceName, u4SessId,
                                               &MinRTDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, MinRTDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec, MinRTDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1RTDelayMin, "%f", (DBL8) f4DelayInMs);

                au1RTDelayMin[RFC6374_INDEX_FIVE] = '\0';

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                CliPrintf (CliHandle, "\r\n Min/Avg/Max RT Delay (ms)        :"
                           " %s/%s/%s", au1RTDelayMin, au1RTDelayAvg,
                           au1RTDelayMax);

            }
            if (i4StatsType == RFC6374_DM_TYPE_ONE_WAY)
            {
                RFC6374_MEMSET (&MinIPDVDelay, RFC6374_INIT_VAL,
                                sizeof (tSNMP_OCTET_STRING_TYPE));
                RFC6374_MEMSET (&MaxIPDVDelay, RFC6374_INIT_VAL,
                                sizeof (tSNMP_OCTET_STRING_TYPE));
                RFC6374_MEMSET (&AvgIPDVDelay, RFC6374_INIT_VAL,
                                sizeof (tSNMP_OCTET_STRING_TYPE));

                RFC6374_MEMSET (au1IPDVDelayMin, RFC6374_INIT_VAL,
                                sizeof (au1IPDVDelayMin));
                RFC6374_MEMSET (au1IPDVDelayMax, RFC6374_INIT_VAL,
                                sizeof (au1IPDVDelayMax));
                RFC6374_MEMSET (au1IPDVDelayAvg, RFC6374_INIT_VAL,
                                sizeof (au1IPDVDelayAvg));

                MinIPDVDelay.pu1_OctetList = au1RTDelay;
                MinIPDVDelay.i4_Length = RFC6374_INIT_VAL;

                MaxIPDVDelay.pu1_OctetList = au1RTDelay;
                MaxIPDVDelay.i4_Length = RFC6374_INIT_VAL;

                AvgIPDVDelay.pu1_OctetList = au1RTDelay;
                AvgIPDVDelay.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsAvgIPDV (u4ContextId,
                                            &ServiceName, u4SessId,
                                            &AvgIPDVDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, AvgIPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   AvgIPDVDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1IPDVDelayAvg, "%f", (DBL8) f4DelayInMs);
                au1IPDVDelayAvg[RFC6374_INDEX_FIVE] = '\0';

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsMaxIPDV (u4ContextId,
                                            &ServiceName, u4SessId,
                                            &MaxIPDVDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, MaxIPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   MaxIPDVDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1IPDVDelayMax, "%f", (DBL8) f4DelayInMs);

                au1IPDVDelayMax[RFC6374_INDEX_FIVE] = '\0';

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsMinIPDV (u4ContextId,
                                            &ServiceName, u4SessId,
                                            &MinIPDVDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, MinIPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   MinIPDVDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1IPDVDelayMin, "%f", (DBL8) f4DelayInMs);

                au1IPDVDelayMin[RFC6374_INDEX_FIVE] = '\0';

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                CliPrintf (CliHandle, "\r\n Min/Avg/Max IPDV (ms)            :"
                           " %s/%s/%s", au1IPDVDelayMin, au1IPDVDelayAvg,
                           au1IPDVDelayMax);

                RFC6374_MEMSET (&MinPDVDelay, RFC6374_INIT_VAL,
                                sizeof (tSNMP_OCTET_STRING_TYPE));
                RFC6374_MEMSET (&MaxPDVDelay, RFC6374_INIT_VAL,
                                sizeof (tSNMP_OCTET_STRING_TYPE));
                RFC6374_MEMSET (&AvgPDVDelay, RFC6374_INIT_VAL,
                                sizeof (tSNMP_OCTET_STRING_TYPE));

                RFC6374_MEMSET (au1PDVDelayMin, RFC6374_INIT_VAL,
                                sizeof (au1PDVDelayMin));
                RFC6374_MEMSET (au1PDVDelayMax, RFC6374_INIT_VAL,
                                sizeof (au1PDVDelayMax));
                RFC6374_MEMSET (au1PDVDelayAvg, RFC6374_INIT_VAL,
                                sizeof (au1PDVDelayAvg));

                MinPDVDelay.pu1_OctetList = au1RTDelay;
                MinPDVDelay.i4_Length = RFC6374_INIT_VAL;

                MaxPDVDelay.pu1_OctetList = au1RTDelay;
                MaxPDVDelay.i4_Length = RFC6374_INIT_VAL;

                AvgPDVDelay.pu1_OctetList = au1RTDelay;
                AvgPDVDelay.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsAvgPDV (u4ContextId,
                                           &ServiceName, u4SessId,
                                           &AvgPDVDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, AvgPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec, AvgPDVDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1PDVDelayAvg, "%f", (DBL8) f4DelayInMs);

                au1PDVDelayAvg[RFC6374_INDEX_FIVE] = '\0';
                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsMaxPDV (u4ContextId,
                                           &ServiceName, u4SessId,
                                           &MaxPDVDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, MaxPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec, MaxPDVDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1PDVDelayMax, "%f", (DBL8) f4DelayInMs);

                au1PDVDelayMax[RFC6374_INDEX_FIVE] = '\0';
                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                nmhGetFs6374DMStatsMinPDV (u4ContextId,
                                           &ServiceName, u4SessId,
                                           &MinPDVDelay);

                RFC6374_GET_4BYTE (u4DelayInSec, MinPDVDelay.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec, MinPDVDelay.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1PDVDelayMin, "%f", (DBL8) f4DelayInMs);

                au1PDVDelayMin[RFC6374_INDEX_FIVE] = '\0';
                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                CliPrintf (CliHandle, "\r\n Min/Avg/Max PDV (ms)             :"
                           " %s/%s/%s", au1PDVDelayMin, au1PDVDelayAvg,
                           au1PDVDelayMax);

            }

            if (u1Detail)
            {
                R6374ShowDmBufferPerServiceDetail (CliHandle, u4ContextId,
                                                   ServiceName, u4SessId);
                CliPrintf (CliHandle,
                           "\r\n NOTE: Detail of only last Five "
                           "Packets are shown.");
            }
            /* Get next index */
            i4RetVal = nmhGetNextIndexFs6374DMStatsTable (u4ContextId,
                                                          &u4NxtContextId,
                                                          &ServiceName,
                                                          &NxtServiceName,
                                                          u4SessId,
                                                          &u4NxtSessId);
            if (i4RetVal == SNMP_SUCCESS)
            {
                if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                    NxtServiceName.pu1_OctetList,
                                    NxtServiceName.i4_Length) ==
                    RFC6374_INIT_VAL)
                {
                    b1SameEntry = RFC6374_TRUE;
                    nmhGetFs6374DMStatsType (u4ContextId, &ServiceName,
                                             u4SessId, &i4PrevType);
                    nmhGetFs6374DMStatsType (u4ContextId, &ServiceName,
                                             u4NxtSessId, &i4NextType);
                    if (i4PrevType != i4NextType)
                    {
                        b1SameEntry = RFC6374_FALSE;
                        CliPrintf (CliHandle, "\r\n");
                    }
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                    CliPrintf (CliHandle, "\r\n");
                    R6374CliLmDmNoRespSess (CliHandle, u4ContextId,
                                            &ServiceName,
                                            RFC6374_MEASUREMENT_TYPE_DELAY);
                }
                u4SessId = u4NxtSessId;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                R6374CliLmDmNoRespSess (CliHandle, u4ContextId, &ServiceName,
                                        RFC6374_MEASUREMENT_TYPE_DELAY);
            }
        }
    }
    else
    {
        R6374CliLmDmNoRespSess (CliHandle, u4ContextId, &ServiceName,
                                RFC6374_MEASUREMENT_TYPE_DELAY);
        CLI_SET_ERR (RFC6374_CLI_DM_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374ShowDmBufferPerServiceDetail                    *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service config       *
 *                    for a particular service                             *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    u1Detail                                             * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           :  None                                                *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
VOID
R6374ShowDmBufferPerServiceDetail (tCliHandle CliHandle,
                                   UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE ServiceName,
                                   UINT4 u4SessId)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                u4StatsType = RFC6374_INIT_VAL;
    INT4                i4Direction = RFC6374_INIT_VAL;
    UINT4               u4DelayInSec = RFC6374_INIT_VAL;
    UINT4               u4DelayInNanoSec = RFC6374_INIT_VAL;
    UINT4               u4NxtSessId = RFC6374_INIT_VAL;
    UINT4               u4SeqId = RFC6374_INIT_VAL;
    UINT4               u4NxtSeqId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    FLT4                f4DelayInMs = 0.0;
    UINT1               au1Delay[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1RTDelay[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1IPDVDelay[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1PDVDelay[RFC6374_INDEX_TEN] = { RFC6374_INIT_VAL };
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1RetDelay[RFC6374_TIMESTAMP_FIELD_SIZE] =
        { RFC6374_INIT_VAL };
    tSNMP_OCTET_STRING_TYPE DMDelayValue;
    tSNMP_OCTET_STRING_TYPE DMRTDelayValue;
    tSNMP_OCTET_STRING_TYPE DMIPDVDelayValue;
    tSNMP_OCTET_STRING_TYPE DMPDVDelayValue;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    tR6374DmBufferTableEntry *pR6374DMBufferTableEntry = NULL;
    tR6374DmBufferTableEntry R6374DMBufferTableEntry;

    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));
    RFC6374_MEMSET (&R6374DMBufferTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374DmBufferTableEntry));
    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetNextIndexFs6374DMTable (u4ContextId, &u4NxtContextId,
                                      &ServiceName, &NxtServiceName, u4SessId,
                                      &u4NxtSessId, 0,
                                      &u4SeqId) != SNMP_FAILURE)
    {
        /* Delay Type */
        nmhGetFs6374DMStatsType (u4ContextId, &ServiceName, u4SessId,
                                 &u4StatsType);

        /* Direction Dyadic/Non-Dyadic */
        nmhGetFs6374DMDyadicMeasurement (u4ContextId,
                                         &ServiceName, u4SessId, &i4Direction);

        if (u4StatsType == RFC6374_DM_TYPE_TWO_WAY)
        {
            CliPrintf (CliHandle, "\r\n");
            if (i4Direction != RFC6374_DYADIC_MEAS_ENABLE)
            {
                /* Non-Dyadic RT value is Calculated */
                CliPrintf (CliHandle,
                           "\r\n  Seq Id        Two Way Delay(ms)       RT Delay (ms) ");
                CliPrintf (CliHandle,
                           "\r\n ---------      -----------------      --------------  ");
            }
            else
            {
                /* For Dyadic Delay RT value is not calculated */
                CliPrintf (CliHandle, "\r\n  Seq Id        Two Way Delay(ms) ");
                CliPrintf (CliHandle, "\r\n ---------      ----------------- ");
            }
            while (i4RetVal == SNMP_SUCCESS)
            {

                RFC6374_MEMSET (au1RetDelay, RFC6374_INIT_VAL,
                                sizeof (au1RetDelay));
                RFC6374_MEMSET (au1RTDelay, RFC6374_INIT_VAL,
                                sizeof (au1RTDelay));
                RFC6374_MEMSET (au1Delay, RFC6374_INIT_VAL, sizeof (au1Delay));

                DMDelayValue.pu1_OctetList = au1RetDelay;
                DMDelayValue.i4_Length = RFC6374_INIT_VAL;

                DMRTDelayValue.pu1_OctetList = au1RTDelay;
                DMRTDelayValue.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                /* Non-Dyadic Delay */
                if (i4Direction != RFC6374_DYADIC_MEAS_ENABLE)
                {
                    /*Get the One Way RT Delay */
                    nmhGetFs6374DMRTDelayValue (u4ContextId,
                                                &ServiceName, u4SessId, u4SeqId,
                                                &DMRTDelayValue);

                    RFC6374_GET_4BYTE (u4DelayInSec,
                                       DMRTDelayValue.pu1_OctetList);
                    RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                       DMRTDelayValue.pu1_OctetList);
                    f4DelayInMs =
                        (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                        (((FLT4) u4DelayInNanoSec) /
                         RFC6374_NUM_OF_USEC_IN_A_SEC);

                    RFC6374_MEMSET (au1RTDelay, RFC6374_INIT_VAL,
                                    sizeof (au1RTDelay));
                    SPRINTF ((CHR1 *) au1RTDelay, "%f", (DBL8) f4DelayInMs);
                    au1RTDelay[RFC6374_INDEX_FIVE] = '\0';

                    u4DelayInSec = RFC6374_INIT_VAL;
                    u4DelayInNanoSec = RFC6374_INIT_VAL;
                    f4DelayInMs = 0.0;
                }

                /*Get the One Way Delay */
                nmhGetFs6374DMDelayValue (u4ContextId,
                                          &ServiceName, u4SessId, u4SeqId,
                                          &DMDelayValue);

                RFC6374_GET_4BYTE (u4DelayInSec, DMDelayValue.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   DMDelayValue.pu1_OctetList);
                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1Delay, "%f", (DBL8) f4DelayInMs);
                au1Delay[RFC6374_INDEX_FIVE] = '\0';

                /* Delay measurement Sequence id rolling buffer */
                R6374DMBufferTableEntry.u4ContextId = u4ContextId;

                RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                                ServiceName.pu1_OctetList,
                                ServiceName.i4_Length);

                R6374DMBufferTableEntry.u4SessionId = u4SessId;
                R6374DMBufferTableEntry.u4SeqCount = u4SeqId;

                pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
                    RBTreeGet (RFC6374_DMBUFFER_TABLE,
                               (tRBElem *) & R6374DMBufferTableEntry);

                if (pR6374DMBufferTableEntry == NULL)
                {
                    return;
                }

                if (i4Direction != RFC6374_DYADIC_MEAS_ENABLE)
                {
                    CliPrintf (CliHandle, "\n %5d %19s %22s ",
                               pR6374DMBufferTableEntry->u4BufSeqId, au1Delay,
                               au1RTDelay);
                }
                else
                {
                    CliPrintf (CliHandle, "\n %5d %19s ",
                               pR6374DMBufferTableEntry->u4BufSeqId, au1Delay);
                }

                /* Get next index */
                i4RetVal = nmhGetNextIndexFs6374DMTable (u4ContextId,
                                                         &u4NxtContextId,
                                                         &ServiceName,
                                                         &NxtServiceName,
                                                         u4SessId, &u4NxtSessId,
                                                         u4SeqId, &u4NxtSeqId);
                if (i4RetVal == SNMP_SUCCESS)
                {
                    u4SeqId = u4NxtSeqId;
                    if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                        NxtServiceName.pu1_OctetList,
                                        NxtServiceName.i4_Length) ==
                        RFC6374_INIT_VAL)
                    {
                        if (u4SessId != u4NxtSessId)
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        i4RetVal = SNMP_FAILURE;
                    }
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       "\r\n  Seq Id       One Way Delay(ms)       IPDV(ms)         PDV(ms)");
            CliPrintf (CliHandle,
                       "\r\n --------      -----------------      ---------        ---------");
            while (i4RetVal == SNMP_SUCCESS)
            {
                RFC6374_MEMSET (au1RetDelay, RFC6374_INIT_VAL,
                                sizeof (au1RetDelay));
                RFC6374_MEMSET (au1Delay, RFC6374_INIT_VAL, sizeof (au1Delay));

                DMDelayValue.pu1_OctetList = au1RetDelay;
                DMDelayValue.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                /*Get the One Way Delay */
                nmhGetFs6374DMDelayValue (u4ContextId,
                                          &ServiceName, u4SessId, u4SeqId,
                                          &DMDelayValue);

                RFC6374_GET_4BYTE (u4DelayInSec, DMDelayValue.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   DMDelayValue.pu1_OctetList);
                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1Delay, "%f", (DBL8) f4DelayInMs);
                au1Delay[RFC6374_INDEX_FIVE] = '\0';

                RFC6374_MEMSET (au1RetDelay, RFC6374_INIT_VAL,
                                sizeof (au1RetDelay));
                RFC6374_MEMSET (au1PDVDelay, RFC6374_INIT_VAL,
                                sizeof (au1PDVDelay));

                DMPDVDelayValue.pu1_OctetList = au1RetDelay;
                DMPDVDelayValue.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                /*Get Inter frame delay variation */
                nmhGetFs6374DMPDV (u4ContextId,
                                   &ServiceName, u4SessId, u4SeqId,
                                   &DMPDVDelayValue);

                RFC6374_GET_4BYTE (u4DelayInSec, DMPDVDelayValue.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   DMPDVDelayValue.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1PDVDelay, "%f", (DBL8) f4DelayInMs);
                au1PDVDelay[RFC6374_INDEX_FIVE] = '\0';

                RFC6374_MEMSET (au1RetDelay, RFC6374_INIT_VAL,
                                sizeof (au1RetDelay));
                RFC6374_MEMSET (au1IPDVDelay, RFC6374_INIT_VAL,
                                sizeof (au1IPDVDelay));

                DMIPDVDelayValue.pu1_OctetList = au1RetDelay;
                DMIPDVDelayValue.i4_Length = RFC6374_INIT_VAL;

                u4DelayInSec = RFC6374_INIT_VAL;
                u4DelayInNanoSec = RFC6374_INIT_VAL;
                f4DelayInMs = 0.0;

                /*Get Inter frame delay variation */
                nmhGetFs6374DMIPDV (u4ContextId,
                                    &ServiceName, u4SessId, u4SeqId,
                                    &DMIPDVDelayValue);

                RFC6374_GET_4BYTE (u4DelayInSec,
                                   DMIPDVDelayValue.pu1_OctetList);
                RFC6374_GET_4BYTE (u4DelayInNanoSec,
                                   DMIPDVDelayValue.pu1_OctetList);

                f4DelayInMs =
                    (((FLT4) u4DelayInSec) * RFC6374_NUM_OF_MSEC_IN_A_SEC) +
                    (((FLT4) u4DelayInNanoSec) / RFC6374_NUM_OF_USEC_IN_A_SEC);

                SPRINTF ((CHR1 *) au1IPDVDelay, "%f", (DBL8) f4DelayInMs);
                au1IPDVDelay[RFC6374_INDEX_FIVE] = '\0';

                /* Delay measurement Sequence id rolling buffer */
                R6374DMBufferTableEntry.u4ContextId = u4ContextId;

                RFC6374_MEMCPY (R6374DMBufferTableEntry.au1ServiceName,
                                ServiceName.pu1_OctetList,
                                ServiceName.i4_Length);

                R6374DMBufferTableEntry.u4SessionId = u4SessId;
                R6374DMBufferTableEntry.u4SeqCount = u4SeqId;

                pR6374DMBufferTableEntry = (tR6374DmBufferTableEntry *)
                    RBTreeGet (RFC6374_DMBUFFER_TABLE,
                               (tRBElem *) & R6374DMBufferTableEntry);

                if (pR6374DMBufferTableEntry == NULL)
                {
                    return;
                }

                CliPrintf (CliHandle, "\n %5d  %17s  %18s  %15s",
                           pR6374DMBufferTableEntry->u4BufSeqId, au1Delay,
                           au1IPDVDelay, au1PDVDelay);

                /* Get next index */
                i4RetVal = nmhGetNextIndexFs6374DMTable (u4ContextId,
                                                         &u4NxtContextId,
                                                         &ServiceName,
                                                         &NxtServiceName,
                                                         u4SessId, &u4NxtSessId,
                                                         u4SeqId, &u4NxtSeqId);
                if (i4RetVal == SNMP_SUCCESS)
                {
                    u4SeqId = u4NxtSeqId;
                    if (RFC6374_MEMCMP (ServiceName.pu1_OctetList,
                                        NxtServiceName.pu1_OctetList,
                                        NxtServiceName.i4_Length) ==
                        RFC6374_INIT_VAL)
                    {

                        if (u4SessId != u4NxtSessId)
                        {
                            i4RetVal = SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        i4RetVal = SNMP_FAILURE;
                    }
                }
                else
                {
                    i4RetVal = SNMP_FAILURE;
                }
            }
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : R6374ShowServiceConfigInfoDetail                     *
 *                                                                         *
 * DESCRIPTION      : This function is called to show service              * 
 *                    configurations                                       *
 *                                                                         *
 * INPUT            : CliHandle                                            *
 *                    u4ContextId                                          * 
 *                    ServiceName                                          *
 *                                                                         *
 * OUTPUT           : None                                                 *
 *                                                                         *
 * RETURNS          : None                                                 *
 *                                                                         *
 **************************************************************************/
PUBLIC VOID
R6374ShowServiceConfigInfoDetail (tCliHandle CliHandle, UINT4 u4ContextId,
                                  tSNMP_OCTET_STRING_TYPE ServiceName)
{
    INT4                i4Type = RFC6374_INIT_VAL;
    INT4                i4Method = RFC6374_INIT_VAL;
    INT4                i4Mode = RFC6374_INIT_VAL;
    INT4                i4TransmitStatus = RFC6374_INIT_VAL;
    INT4                i4TSFormat = RFC6374_INIT_VAL;
    UINT4               u4Interval = RFC6374_INIT_VAL;
    UINT4               u4NoOfMessages = RFC6374_INIT_VAL;
    UINT4               u4PadSize = RFC6374_INIT_VAL;

    nmhGetFs6374LMType (u4ContextId, &ServiceName, &i4Type);
    nmhGetFs6374LMMethod (u4ContextId, &ServiceName, &i4Method);
    nmhGetFs6374LMMode (u4ContextId, &ServiceName, &i4Mode);
    nmhGetFs6374LMTimeIntervalInMilliseconds (u4ContextId, &ServiceName,
                                              &u4Interval);
    nmhGetFs6374LMNoOfMessages (u4ContextId, &ServiceName, &u4NoOfMessages);
    nmhGetFs6374LMTransmitStatus (u4ContextId, &ServiceName, &i4TransmitStatus);
    nmhGetFs6374LMTimeStampFormat (u4ContextId, &ServiceName, &i4TSFormat);

    CliPrintf (CliHandle, "\r\n LM Configs:");
    CliPrintf (CliHandle, "\r\n\t Type               : %s",
               (i4Type == RFC6374_LM_TYPE_ONE_WAY) ? "One-way" : "Two-way");
    CliPrintf (CliHandle, "\r\n\t Method             : %s",
               (i4Method == RFC6374_LM_METHOD_INFERED) ? "Inferred" : "Direct");
    CliPrintf (CliHandle, "\r\n\t Mode               : %s",
               (i4Mode ==
                RFC6374_LM_MODE_ONDEMAND) ? "On-demand" : "Proactive");
    if (i4Mode == RFC6374_LM_MODE_ONDEMAND)
    {
        CliPrintf (CliHandle, "\r\n\t No. of Messages    : %u", u4NoOfMessages);
    }
    CliPrintf (CliHandle, "\r\n\t Query Interval(ms) : %u", u4Interval);
    if (i4TSFormat == RFC6374_TS_FORMAT_NULL)
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : Null Timestamp");
    }
    else if (i4TSFormat == RFC6374_TS_FORMAT_SEQ_NUM)
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : Sequence Number");
    }
    else if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : NTP Timestamp");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : PTP Timestamp");
    }
    CliPrintf (CliHandle, "\r\n\t Transmit Status    : %s",
               (i4TransmitStatus ==
                RFC6374_TX_STATUS_READY) ? "Ready" : "In-progress");

    i4Type = RFC6374_INIT_VAL;
    i4Method = RFC6374_INIT_VAL;
    i4Mode = RFC6374_INIT_VAL;
    i4TransmitStatus = RFC6374_INIT_VAL;
    u4Interval = RFC6374_INIT_VAL;
    u4NoOfMessages = RFC6374_INIT_VAL;

    nmhGetFs6374DMType (u4ContextId, &ServiceName, &i4Type);
    nmhGetFs6374DMMode (u4ContextId, &ServiceName, &i4Mode);
    nmhGetFs6374DMTimeIntervalInMilliseconds (u4ContextId, &ServiceName,
                                              &u4Interval);
    nmhGetFs6374DMNoOfMessages (u4ContextId, &ServiceName, &u4NoOfMessages);
    nmhGetFs6374DMTransmitStatus (u4ContextId, &ServiceName, &i4TransmitStatus);
    nmhGetFs6374DMTimeStampFormat (u4ContextId, &ServiceName, &i4TSFormat);
    nmhGetFs6374DMPaddingSize (u4ContextId, &ServiceName, &u4PadSize);

    CliPrintf (CliHandle, "\r\n DM Configs:");
    CliPrintf (CliHandle, "\r\n\t Type               : %s",
               (i4Type == RFC6374_DM_TYPE_ONE_WAY) ? "One-way" : "Two-way");
    CliPrintf (CliHandle, "\r\n\t Mode               : %s",
               (i4Mode ==
                RFC6374_DM_MODE_ONDEMAND) ? "On-demand" : "Proactive");
    if (i4Mode == RFC6374_DM_MODE_ONDEMAND)
    {
        CliPrintf (CliHandle, "\r\n\t No. of Messages    : %u", u4NoOfMessages);
    }
    CliPrintf (CliHandle, "\r\n\t Query Interval(ms) : %u", u4Interval);
    if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : NTP Timestamp");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : PTP Timestamp");
    }
    CliPrintf (CliHandle, "\r\n\t Padding TLV Size    : %u", u4PadSize);
    CliPrintf (CliHandle, "\r\n\t Transmit Status     : %s",
               (i4TransmitStatus ==
                RFC6374_TX_STATUS_READY) ? "Ready" : "In-progress");

    i4Type = RFC6374_INIT_VAL;
    i4Method = RFC6374_INIT_VAL;
    i4Mode = RFC6374_INIT_VAL;
    i4TransmitStatus = RFC6374_INIT_VAL;
    u4Interval = RFC6374_INIT_VAL;
    u4NoOfMessages = RFC6374_INIT_VAL;

    nmhGetFs6374CmbLMDMType (u4ContextId, &ServiceName, &i4Type);
    nmhGetFs6374CmbLMDMMode (u4ContextId, &ServiceName, &i4Mode);
    nmhGetFs6374CmbLMDMMethod (u4ContextId, &ServiceName, &i4Method);
    nmhGetFs6374CmbLMDMTimeIntervalInMilliseconds (u4ContextId, &ServiceName,
                                                   &u4Interval);
    nmhGetFs6374CmbLMDMNoOfMessages (u4ContextId, &ServiceName,
                                     &u4NoOfMessages);
    nmhGetFs6374CmbLMDMTransmitStatus (u4ContextId, &ServiceName,
                                       &i4TransmitStatus);
    nmhGetFs6374CmbLMDMTimeStampFormat (u4ContextId, &ServiceName, &i4TSFormat);
    nmhGetFs6374CmbLMDMPaddingSize (u4ContextId, &ServiceName, &u4PadSize);

    CliPrintf (CliHandle, "\r\n Combined LMDM Configs:");
    CliPrintf (CliHandle, "\r\n\t Type                : %s",
               (i4Type == RFC6374_LMDM_TYPE_ONE_WAY) ? "One-way" : "Two-way");
    CliPrintf (CliHandle, "\r\n\t Method              : %s",
               (i4Method ==
                RFC6374_LMDM_METHOD_INFERED) ? "Inferred" : "Direct");
    CliPrintf (CliHandle, "\r\n\t Mode                : %s",
               (i4Mode ==
                RFC6374_LMDM_MODE_ONDEMAND) ? "On-demand" : "Proactive");
    if (i4Mode == RFC6374_LMDM_MODE_ONDEMAND)
    {
        CliPrintf (CliHandle, "\r\n\t No. of Messages     : %u",
                   u4NoOfMessages);
    }
    CliPrintf (CliHandle, "\r\n\t Query Interval(ms)  : %u", u4Interval);
    if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : NTP Timestamp");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n\t Preferred TS Format : PTP Timestamp");
    }
    CliPrintf (CliHandle, "\r\n\t Padding TLV Size    : %u", u4PadSize);
    CliPrintf (CliHandle, "\r\n\t Transmit Status    : %s",
               (i4TransmitStatus ==
                RFC6374_TX_STATUS_READY) ? "Ready" : "In-progress");

    CliPrintf (CliHandle, "\r\n");
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearStatsAll
 *
 *     DESCRIPTION      : This function will clear the statistics for all
 *                        the services.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        ServiceName -
 *                        pu1Arg1 - Argument depending upon type of the command
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearStatsAll (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    UINT4               u4CurrentContextId = u4ContextId;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    INT4                i4RetVal = SNMP_FAILURE;

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4CurrentContextId,
                                                  &ServiceName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (RFC6374_CLI_SERVICE_TABLE_EMPTY);
        return CLI_FAILURE;
    }

    do
    {
        RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL,
                        sizeof (au1ServiceName));
        RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                        ServiceName.i4_Length);

        R6374ClearServiceStats (u4CurrentContextId, &ServiceName);

        i4RetVal = nmhGetNextIndexFs6374ServiceConfigTable (u4CurrentContextId,
                                                            &u4NxtContextId,
                                                            &ServiceName,
                                                            &NxtServiceName);
        if (i4RetVal == SNMP_SUCCESS)
        {
            u4CurrentContextId = u4NxtContextId;
            ServiceName.i4_Length = NxtServiceName.i4_Length;
            RFC6374_STRCPY (&ServiceName.pu1_OctetList,
                            &NxtServiceName.pu1_OctetList);
        }
    }
    while (i4RetVal == SNMP_SUCCESS);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearStats
 *
 *     DESCRIPTION      : This function will clears service specific
 *                        statistics. 
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        ServiceName - Service name
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearStats (tCliHandle CliHandle, UINT4 u4ContextId,
                    tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    tR6374ServiceConfigTableEntry *pServiceConfig = NULL;

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));

    RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);

    /* Check if Service is already created */
    pServiceConfig = R6374UtilGetServiceFromName (u4ContextId, au1ServiceName);
    if (pServiceConfig == NULL)
    {
        CLI_SET_ERR (RFC6374_CLI_SERVICE_TABLE_EMPTY);
        return CLI_FAILURE;
    }

    R6374ClearServiceStats (u4ContextId, &ServiceName);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearSerivceStats
 *
 *     DESCRIPTION      : This utility function will clear service specific
 *                        statistics. 
 *
 *     INPUT            : u4ContextId - Context  Identifier
 *                        ServiceName - Service name
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/
PUBLIC VOID
R6374ClearServiceStats (UINT4 u4ContextId,
                        tSNMP_OCTET_STRING_TYPE * pServiceName)
{
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tR6374ServiceConfigTableEntry R6374ServiceConfigTableEntry;

    RFC6374_MEMSET (&R6374ServiceConfigTableEntry, RFC6374_INIT_VAL,
                    sizeof (tR6374ServiceConfigTableEntry));

    R6374ServiceConfigTableEntry.u4ContextId = u4ContextId;

    RFC6374_MEMCPY (R6374ServiceConfigTableEntry.au1ServiceName,
                    pServiceName->pu1_OctetList, pServiceName->i4_Length);

    pR6374ServiceConfigTableEntry = (tR6374ServiceConfigTableEntry *)
        RBTreeGet (RFC6374_SERVICECONFIG_TABLE,
                   (tRBElem *) & R6374ServiceConfigTableEntry);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        return;
    }

    MEMSET (&(pR6374ServiceConfigTableEntry->Stats), 0,
            sizeof (tR6374StatsTableEntry));

    return;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearDelayBuffer
 *
 *     DESCRIPTION      : This function will clear delay buffer
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearDelayBuffer (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    /* Test if Frame loss buffer can be cleared or not */
    if (nmhTestv2Fs6374DMBufferClear
        (&u4ErrorCode, u4ContextId, RFC6374_TRUE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    /* Then clear it */
    if (nmhSetFs6374DMBufferClear (u4ContextId, RFC6374_TRUE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearLossBuffer
 *
 *     DESCRIPTION      : This function will clear loss buffer
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearLossBuffer (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    /* Test if Frame loss buffer can be cleared or not */
    if (nmhTestv2Fs6374LMBufferClear
        (&u4ErrorCode, u4ContextId, RFC6374_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Then clear it */
    if (nmhSetFs6374LMBufferClear (u4ContextId, RFC6374_TRUE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliShowRunningConfig
 *
 *     DESCRIPTION      : This function will be called for SRC
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    tSNMP_OCTET_STRING_TYPE SrcIpAddr;
    tSNMP_OCTET_STRING_TYPE DestIpAddr;
    INT4                i4SystemControl = RFC6374_START;
    INT4                i4ModuleStatus = RFC6374_ENABLED;
    INT4                i4MplsPathType = RFC6374_INIT_VAL;
    INT4                i4EncapType = RFC6374_INIT_VAL;
    INT4                i4TrafficClass = RFC6374_INIT_VAL;
    INT4                i4DyadicMeasurement = RFC6374_INIT_VAL;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;
    UINT4               u4FwdTnlIdOrPwId = RFC6374_INIT_VAL;
    UINT4               u4RevTnlId = RFC6374_INIT_VAL;
    UINT4               u4CurrentContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    UINT4               u4SrcIpAddr = RFC6374_INIT_VAL;
    UINT4               u4DstIpAddr = RFC6374_INIT_VAL;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1PrntServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1SrcIpAddr[RFC6374_IPV4_ADDR_LEN];
    UINT1               au1DestIpAddr[RFC6374_IPV4_ADDR_LEN];
    CHR1               *pc1Prefix = NULL;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4RetValFs6374DMType = RFC6374_INIT_VAL;
    INT4                i4RetValFs6374DMMode = RFC6374_INIT_VAL;
    INT4                i4RetValFs6374LMType = RFC6374_INIT_VAL;
    INT4                i4RetValFs6374LMMethod = RFC6374_INIT_VAL;
    INT4                i4RetValFs6374LMMode = RFC6374_INIT_VAL;
    INT4                i4LmDmType = RFC6374_INIT_VAL;
    INT4                i4LmDmMethod = RFC6374_INIT_VAL;
    INT4                i4LmDmMode = RFC6374_INIT_VAL;
    INT4                i4TSFormat = RFC6374_INIT_VAL;
    INT4                i4NegoStatus = RFC6374_INIT_VAL;
    INT4                i4SessionIntervalQueryStatus = RFC6374_INIT_VAL;
    UINT4               u4QueryTransmitRetryCount = RFC6374_INIT_VAL;
    UINT4               u4LmDmQueryInterval = RFC6374_INIT_VAL;
    UINT4               u4Fs6374DMqueryInterval = RFC6374_INIT_VAL;
    UINT4               u4Fs6374LMQueryInterval = RFC6374_INIT_VAL;
    UINT4               u4LmNoOfMsg = RFC6374_INIT_VAL;
    UINT4               u4DmNoOfMsg = RFC6374_INIT_VAL;
    UINT4               u4LmDmNoOfMsg = RFC6374_INIT_VAL;
    UINT4               u4PadSize = RFC6374_INIT_VAL;
    UINT4               u4MinReceptionIntervalInMilliseconds = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&SrcIpAddr, RFC6374_INIT_VAL, sizeof (SrcIpAddr));
    RFC6374_MEMSET (&DestIpAddr, RFC6374_INIT_VAL, sizeof (DestIpAddr));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1PrntServiceName, RFC6374_INIT_VAL,
                    sizeof (au1PrntServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));
    RFC6374_MEMSET (au1SrcIpAddr, RFC6374_INIT_VAL, sizeof (au1SrcIpAddr));
    RFC6374_MEMSET (au1DestIpAddr, RFC6374_INIT_VAL, sizeof (au1DestIpAddr));

    /* Destination IP pointer */
    DestIpAddr.pu1_OctetList = au1DestIpAddr;
    DestIpAddr.i4_Length = RFC6374_INIT_VAL;

    /* Source IP pointer */
    SrcIpAddr.pu1_OctetList = au1SrcIpAddr;
    SrcIpAddr.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    R6374PrintTrapControl (CliHandle);

    nmhGetFs6374SystemControl (RFC6374_DEFAULT_CONTEXT_ID, &i4SystemControl);
    nmhGetFs6374ModuleStatus (RFC6374_DEFAULT_CONTEXT_ID, &i4ModuleStatus);

    if (i4SystemControl != RFC6374_START)
    {
        CliPrintf (CliHandle, "\r\nno mpls pm start\r\n");
        return CLI_SUCCESS;
    }
    if (i4ModuleStatus != RFC6374_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nno mpls pm enable\r\n");
    }

    if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4CurrentContextId,
                                                  &ServiceName) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    do
    {
        RFC6374_MEMSET (au1PrntServiceName, RFC6374_INIT_VAL,
                        sizeof (au1PrntServiceName));
        RFC6374_MEMCPY (au1PrntServiceName, ServiceName.pu1_OctetList,
                        ServiceName.i4_Length);
        /*Get the MPLS Type */
        nmhGetFs6374MplsPathType (u4CurrentContextId,
                                  &ServiceName, &i4MplsPathType);

        CliPrintf (CliHandle, "mpls pm service %s", au1PrntServiceName);

        if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_TNL)
        {
            /*Get the Associated Forward Tunnel */
            nmhGetFs6374FwdTnlIdOrPwId (u4CurrentContextId,
                                        &ServiceName, &u4FwdTnlIdOrPwId);

            /*Get the Associated Reverse Tunnel */
            nmhGetFs6374RevTnlId (u4CurrentContextId,
                                  &ServiceName, &u4RevTnlId);

            /*Get the Source Ip Address */
            nmhGetFs6374SrcIpAddr (u4CurrentContextId,
                                   &ServiceName, &SrcIpAddr);
            RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4SrcIpAddr);

            /*Get the Destination Ip Address */
            nmhGetFs6374DestIpAddr (u4CurrentContextId,
                                    &ServiceName, &DestIpAddr);
            RFC6374_OCTETSTRING_TO_INTEGER (DestIpAddr, u4DstIpAddr);

            if ((u4FwdTnlIdOrPwId != RFC6374_INIT_VAL) ||
                (u4RevTnlId != RFC6374_INIT_VAL) ||
                (u4SrcIpAddr != RFC6374_INIT_VAL) ||
                (u4DstIpAddr != RFC6374_INIT_VAL))
            {
                CliPrintf (CliHandle, "\r\nservice tunnel ");
            }
            if (u4FwdTnlIdOrPwId != RFC6374_INIT_VAL)
            {

                CliPrintf (CliHandle, "forward-tunnel-id %u ",
                           u4FwdTnlIdOrPwId);
            }

            if (u4RevTnlId != RFC6374_INIT_VAL)
            {
                CliPrintf (CliHandle, "reverse-tunnel-id %u ", u4RevTnlId);
            }
            if (u4SrcIpAddr != 0)
            {
                RFC6374_IPADDR_TO_STR (pc1Prefix, u4SrcIpAddr);
                CliPrintf (CliHandle, "source %s ", pc1Prefix);
            }
            if (u4DstIpAddr != 0)
            {
                RFC6374_IPADDR_TO_STR (pc1Prefix, u4DstIpAddr);
                CliPrintf (CliHandle, "destination %s", pc1Prefix);
            }
        }
        else if (i4MplsPathType == RFC6374_MPLS_PATH_TYPE_PW)
        {
            CliPrintf (CliHandle, "\r\nservice pw pw-ip ");

            /*Get the Destination Ip Address */
            nmhGetFs6374DestIpAddr (u4CurrentContextId,
                                    &ServiceName, &DestIpAddr);
            RFC6374_OCTETSTRING_TO_INTEGER (DestIpAddr, u4DstIpAddr);
            RFC6374_IPADDR_TO_STR (pc1Prefix, u4DstIpAddr);
            CliPrintf (CliHandle, "%s ", pc1Prefix);

            /*Get the Associated PW ID */
            nmhGetFs6374FwdTnlIdOrPwId (u4CurrentContextId,
                                        &ServiceName, &u4FwdTnlIdOrPwId);
            CliPrintf (CliHandle, "pw-id %u ", u4FwdTnlIdOrPwId);
        }
        /*Get the Encap Type */
        nmhGetFs6374EncapType (u4CurrentContextId, &ServiceName, &i4EncapType);
        /*Get the Traffic Class */
        nmhGetFs6374TrafficClass (u4CurrentContextId,
                                  &ServiceName, &i4TrafficClass);
        /* Dyadic Measurement */
        nmhGetFs6374DyadicMeasurement (u4CurrentContextId,
                                       &ServiceName, &i4DyadicMeasurement);
        /* Dyadic Proactive Role */
        nmhGetFs6374DyadicProactiveRole (u4CurrentContextId,
                                         &ServiceName, &i4DyadicProactiveRole);

        nmhGetFs6374TSFNegotiation (u4CurrentContextId, &ServiceName,
                                    &i4NegoStatus);

        nmhGetFs6374QueryTransmitRetryCount (u4CurrentContextId,
                                             &ServiceName,
                                             &u4QueryTransmitRetryCount);

        nmhGetFs6374SessionIntervalQueryStatus (u4CurrentContextId,
                                                &ServiceName,
                                                &i4SessionIntervalQueryStatus);

        nmhGetFs6374MinReceptionIntervalInMilliseconds (u4CurrentContextId,
                                                        &ServiceName,
                                                        &u4MinReceptionIntervalInMilliseconds);

        if ((i4EncapType == RFC6374_ENCAP_NO_GAL_GACH)
            || (i4TrafficClass != RFC6374_INIT_VAL))
        {
            if (i4EncapType == RFC6374_ENCAP_NO_GAL_GACH)
            {
                CliPrintf (CliHandle, "\r\n encap-type without-gal ");
            }
            if (i4TrafficClass != RFC6374_INIT_VAL)
            {
                CliPrintf (CliHandle, "\r\n traffic-class %d ", i4TrafficClass);
            }
        }

        if (i4DyadicMeasurement != RFC6374_DEFAULT_DYADIC_MEASUREMENT)
        {
            CliPrintf (CliHandle, "\r\n dyadic-measurement enable ");
        }
        if (i4DyadicProactiveRole != RFC6374_DEFAULT_DYADIC_PROACTIVE_ROLE)
        {
            CliPrintf (CliHandle, "\r\n dyadic-proactive role active ");
        }
        if (i4NegoStatus != RFC6374_DEFAULT_NEGOTIATION_STATUS)
        {
            CliPrintf (CliHandle, "\r\n time-stamp negotiation enable ");
        }
        if (u4QueryTransmitRetryCount !=
            RFC6374_DEFAULT_QUERY_TRANSMIT_RETRYCNT)
        {
            CliPrintf (CliHandle, "\r\n query-transmit-retry-count %d",
                       u4QueryTransmitRetryCount);
        }
        if (i4SessionIntervalQueryStatus != RFC6374_SESS_INT_QUERY_ENABLE)
        {
            CliPrintf (CliHandle, "\r\n session-interval-query disable");
        }
        if (u4MinReceptionIntervalInMilliseconds !=
            RFC6374_DEFAULT_RECEPTION_INTERVAL)
        {
            CliPrintf (CliHandle, "\r\n min-reception-interval %d",
                       u4MinReceptionIntervalInMilliseconds);
        }

        nmhGetFs6374DMType (u4CurrentContextId, &ServiceName,
                            &i4RetValFs6374DMType);
        nmhGetFs6374DMMode (u4CurrentContextId, &ServiceName,
                            &i4RetValFs6374DMMode);
        nmhGetFs6374DMTimeIntervalInMilliseconds (u4CurrentContextId,
                                                  &ServiceName,
                                                  &u4Fs6374DMqueryInterval);
        nmhGetFs6374DMNoOfMessages (u4CurrentContextId, &ServiceName,
                                    &u4DmNoOfMsg);
        nmhGetFs6374DMTimeStampFormat (u4CurrentContextId, &ServiceName,
                                       &i4TSFormat);
        nmhGetFs6374DMPaddingSize (u4CurrentContextId, &ServiceName,
                                   &u4PadSize);

        if ((i4RetValFs6374DMType == RFC6374_DM_TYPE_ONE_WAY) ||
            (i4RetValFs6374DMMode == RFC6374_DM_MODE_PROACTIVE) ||
            ((i4RetValFs6374DMMode == RFC6374_DM_MODE_ONDEMAND) &&
             (u4DmNoOfMsg != RFC6374_DM_DEFAULT_ONDEMAND_COUNT)) ||
            (u4Fs6374DMqueryInterval != RFC6374_DM_DEFAULT_INTERVAL) ||
            (u4PadSize != RFC6374_DEFAULT_PAD_SIZE))
        {
            CliPrintf (CliHandle, "\r\n dm ");
            if (i4RetValFs6374DMType == RFC6374_DM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "type one-way ");
            }
            if (i4RetValFs6374DMMode == RFC6374_DM_MODE_PROACTIVE)
            {
                CliPrintf (CliHandle, "mode proactive ");
            }
            if ((i4RetValFs6374DMMode == RFC6374_DM_MODE_ONDEMAND) &&
                (u4DmNoOfMsg != RFC6374_DM_DEFAULT_ONDEMAND_COUNT))
            {
                CliPrintf (CliHandle, "mode on-demand count %d ", u4DmNoOfMsg);
            }
            if (u4Fs6374DMqueryInterval != RFC6374_DM_DEFAULT_INTERVAL)
            {
                CliPrintf (CliHandle, "query-interval %d ",
                           u4Fs6374DMqueryInterval);
            }
            if (u4PadSize != RFC6374_DEFAULT_PAD_SIZE)
            {
                CliPrintf (CliHandle, "pad-tlv-size %d ", u4PadSize);
            }
        }
        if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
        {
            CliPrintf (CliHandle, "\r\n dm time-stamp ntp ");
        }
        nmhGetFs6374LMType (u4CurrentContextId, &ServiceName,
                            &i4RetValFs6374LMType);
        nmhGetFs6374LMMethod (u4CurrentContextId, &ServiceName,
                              &i4RetValFs6374LMMethod);
        nmhGetFs6374LMMode (u4CurrentContextId, &ServiceName,
                            &i4RetValFs6374LMMode);
        nmhGetFs6374LMTimeIntervalInMilliseconds (u4CurrentContextId,
                                                  &ServiceName,
                                                  &u4Fs6374LMQueryInterval);
        nmhGetFs6374LMNoOfMessages (u4CurrentContextId, &ServiceName,
                                    &u4LmNoOfMsg);
        nmhGetFs6374LMTimeStampFormat (u4CurrentContextId, &ServiceName,
                                       &i4TSFormat);

        if ((i4RetValFs6374LMType == RFC6374_LM_TYPE_ONE_WAY) ||
            (i4RetValFs6374LMMethod == RFC6374_LM_METHOD_DIRECT) ||
            ((i4RetValFs6374LMMode == RFC6374_LM_MODE_ONDEMAND) &&
             (u4LmNoOfMsg != RFC6374_LM_DEFAULT_ONDEMAND_COUNT)) ||
            (i4RetValFs6374LMMode == RFC6374_LM_MODE_PROACTIVE) ||
            (u4Fs6374LMQueryInterval != RFC6374_LM_DEFAULT_INTERVAL))
        {
            CliPrintf (CliHandle, "\r\n lm ");
            if (i4RetValFs6374LMType == RFC6374_LM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "type one-way ");
            }
            if (i4RetValFs6374LMMethod == RFC6374_LM_METHOD_DIRECT)
            {
                CliPrintf (CliHandle, "method direct ");
            }
            if (i4RetValFs6374LMMode == RFC6374_LM_MODE_PROACTIVE)
            {
                CliPrintf (CliHandle, "mode proactive ");
            }
            if ((i4RetValFs6374LMMode == RFC6374_LM_MODE_ONDEMAND) &&
                (u4LmNoOfMsg != RFC6374_LM_DEFAULT_ONDEMAND_COUNT))
            {
                CliPrintf (CliHandle, "mode on-demand count %d ", u4LmNoOfMsg);
            }
            if (u4Fs6374LMQueryInterval != RFC6374_LM_DEFAULT_INTERVAL)
            {
                CliPrintf (CliHandle, "query-interval %d ",
                           u4Fs6374LMQueryInterval);
            }
        }
        if (i4TSFormat == RFC6374_TS_FORMAT_NULL)
        {
            CliPrintf (CliHandle, "\r\n lm time-stamp null ");
        }
        else if (i4TSFormat == RFC6374_TS_FORMAT_SEQ_NUM)
        {
            CliPrintf (CliHandle, "\r\n lm time-stamp sequence-number ");
        }
        else if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
        {
            CliPrintf (CliHandle, "\r\n lm time-stamp ntp ");
        }
        nmhGetFs6374CmbLMDMType (u4CurrentContextId, &ServiceName, &i4LmDmType);
        nmhGetFs6374CmbLMDMMethod (u4CurrentContextId, &ServiceName,
                                   &i4LmDmMethod);
        nmhGetFs6374CmbLMDMMode (u4CurrentContextId, &ServiceName, &i4LmDmMode);
        nmhGetFs6374CmbLMDMTimeIntervalInMilliseconds (u4CurrentContextId,
                                                       &ServiceName,
                                                       &u4LmDmQueryInterval);
        nmhGetFs6374CmbLMDMNoOfMessages (u4CurrentContextId, &ServiceName,
                                         &u4LmDmNoOfMsg);
        nmhGetFs6374CmbLMDMTimeStampFormat (u4CurrentContextId, &ServiceName,
                                            &i4TSFormat);
        nmhGetFs6374CmbLMDMPaddingSize (u4CurrentContextId, &ServiceName,
                                        &u4PadSize);

        if ((i4LmDmType == RFC6374_LMDM_TYPE_ONE_WAY) ||
            (i4LmDmMethod == RFC6374_LMDM_METHOD_DIRECT) ||
            ((i4LmDmMode == RFC6374_LMDM_MODE_ONDEMAND) &&
             (u4LmDmNoOfMsg != RFC6374_LMDM_DEFAULT_ONDEMAND_COUNT)) ||
            (i4LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) ||
            (u4LmDmQueryInterval != RFC6374_LMDM_DEFAULT_INTERVAL) ||
            (u4PadSize != RFC6374_DEFAULT_PAD_SIZE))
        {
            CliPrintf (CliHandle, "\r\n combined-lmdm ");

            if (i4LmDmType == RFC6374_LMDM_TYPE_ONE_WAY)
            {
                CliPrintf (CliHandle, "type one-way ");
            }
            if (i4LmDmMethod == RFC6374_LMDM_METHOD_DIRECT)
            {
                CliPrintf (CliHandle, "method direct ");
            }
            if (i4LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
            {
                CliPrintf (CliHandle, "mode proactive ");
            }
            if ((i4LmDmMode == RFC6374_LMDM_MODE_ONDEMAND) &&
                (u4LmDmNoOfMsg != RFC6374_LMDM_DEFAULT_ONDEMAND_COUNT))
            {
                CliPrintf (CliHandle, "mode on-demand count %d ",
                           u4LmDmNoOfMsg);
            }
            if (u4LmDmQueryInterval != RFC6374_LMDM_DEFAULT_INTERVAL)
            {
                CliPrintf (CliHandle, "query-interval %d ",
                           u4LmDmQueryInterval);
            }
            if (u4PadSize != RFC6374_DEFAULT_PAD_SIZE)
            {
                CliPrintf (CliHandle, "pad-tlv-size %d ", u4PadSize);
            }
        }
        if (i4TSFormat == RFC6374_TS_FORMAT_NTP)
        {
            CliPrintf (CliHandle, "\r\n combined-lmdm time-stamp ntp ");
        }
        i4RetVal = nmhGetNextIndexFs6374ServiceConfigTable (u4CurrentContextId,
                                                            &u4NxtContextId,
                                                            &ServiceName,
                                                            &NxtServiceName);
        if (i4RetVal == SNMP_SUCCESS)
        {
            u4CurrentContextId = u4NxtContextId;

            ServiceName.i4_Length = NxtServiceName.i4_Length;
            RFC6374_STRCPY (&ServiceName.pu1_OctetList,
                            &NxtServiceName.pu1_OctetList);
        }
        CliPrintf (CliHandle, "\r\n!\r\n");
    }
    while (i4RetVal == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : R6374CliSetTraps                                   
 *                                                                          
 *     DESCRIPTION      : This function enables or disables the RFC6374
 *                        proprietary Traps.
 *                                                                          
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4Command -  Command type
 *                        u2R6374Val - Control variable for enabling or disabling of 
 *                                any RFC6374 trap.
 *                                                                          
 *     OUTPUT           : None               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ***************************************************************************/
INT4
R6374CliSetTrapStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT2 u2R6374Val, UINT1 u1Status)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    UINT2               u2R6374Trap = RFC6374_INIT_VAL;
    UINT1               au1R6374TrapOption[2];
    UINT1               u1TrapMsbVal = RFC6374_INIT_VAL;
    UINT1               u1TrapLsbVal = RFC6374_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE TrapOption;

    /* Enable/Disable R6374 Traps */
    if ((u2R6374Val != 0) || (u1Status == RFC6374_DISABLED))
    {
        RFC6374_MEMSET (au1R6374TrapOption, RFC6374_INIT_VAL, sizeof
                        (au1R6374TrapOption));
        RFC6374_MEMSET (&TrapOption, RFC6374_INIT_VAL,
                        sizeof (tSNMP_OCTET_STRING_TYPE));

        TrapOption.pu1_OctetList = au1R6374TrapOption;
        TrapOption.i4_Length = 0;

        if (nmhGetFs6374LMDMTrapControl (u4ContextId, &TrapOption) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u2R6374Trap = TrapOption.pu1_OctetList[RFC6374_INDEX_ZERO];
        u2R6374Trap = (UINT2) (u2R6374Trap << 8);
        u2R6374Trap =
            (UINT2) (u2R6374Trap | TrapOption.pu1_OctetList[RFC6374_INDEX_ONE]);

        /* Set the corresponding Trap value which is to be set */
        if (u1Status == RFC6374_ENABLED)
        {
            u2R6374Trap = u2R6374Trap | u2R6374Val;
        }
        else
        {
            u2R6374Trap = u2R6374Trap & u2R6374Val;
        }
        u1TrapMsbVal = (UINT1) (u2R6374Trap >> 8);
        TrapOption.pu1_OctetList[RFC6374_INDEX_ZERO] = u1TrapMsbVal;
        TrapOption.i4_Length = 1;
        u1TrapLsbVal = (UINT1) (u2R6374Trap & 0x00FF);
        TrapOption.pu1_OctetList[RFC6374_INDEX_ONE] = u1TrapLsbVal;
        TrapOption.i4_Length = 2;

        /* Test if Trap value can be set or not */
        if (nmhTestv2Fs6374LMDMTrapControl
            (&u4ErrorCode, u4ContextId, &TrapOption) != SNMP_SUCCESS)

        {
            return CLI_FAILURE;
        }

        /* Set the corresponding Trap value */
        if (nmhSetFs6374LMDMTrapControl (u4ContextId, &TrapOption) !=
            SNMP_SUCCESS)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearBufferAll
 *
 *     DESCRIPTION      : This function will clear the loss and delay buffer.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearBufferAll (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               u1BothTableEmpty = RFC6374_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;

    i4RetVal = R6374CliClearDelayBuffer (CliHandle, u4ContextId);

    if (i4RetVal != CLI_SUCCESS)
    {
        u1BothTableEmpty++;
    }

    i4RetVal = R6374CliClearLossBuffer (CliHandle, u4ContextId);

    if (i4RetVal != CLI_SUCCESS)
    {
        u1BothTableEmpty++;
    }

    if (u1BothTableEmpty == 2)
    {
        CLI_SET_ERR (RFC6374_CLI_CLEAR_BUFFER_NOT_POSSIBLE);
        i4RetVal = CLI_FAILURE;
    }
    /* Means 1 table returns failure so return failure from here so 
     * that error msg can come*/
    else if (u1BothTableEmpty == 1)
    {
        i4RetVal = CLI_FAILURE;
    }
    return i4RetVal;
}

/******************************************************************************
 * Function   : R6374CliGetCxtIdFromName 
 *
 * Description: This function is to get the Context Id from the given
 *              Context name by calling RFC6374 Port function.
 *
 * Input      : ContextName
 *
 * Output     : ContextId
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
R6374CliGetCxtIdFromName (UINT1 *pu1ContextName, UINT4 *pu4ContextId)
{
    tR6374ExtInParams  *pR6374ExtInParams = NULL;
    tR6374ExtOutParams *pR6374ExtOutParams = NULL;

    if ((pR6374ExtInParams = (tR6374ExtInParams *)
         MemAllocMemBlk (RFC6374_INPUTPARAMS_POOLID)) == NULL)
    {
        return CLI_FAILURE;
    }
    if ((pR6374ExtOutParams = (tR6374ExtOutParams *)
         MemAllocMemBlk (RFC6374_OUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
        return CLI_FAILURE;
    }

    MEMSET (pR6374ExtInParams, 0, sizeof (tR6374ExtInParams));
    MEMSET (pR6374ExtOutParams, 0, sizeof (tR6374ExtOutParams));

    pR6374ExtInParams->eExtReqType = RFC6374_VCM_GET_CXT_ID_FRM_NAME;

    RFC6374_STRNCPY (pR6374ExtInParams->uInParams.au1ContextName,
                     pu1ContextName,
                     (sizeof (pR6374ExtInParams->uInParams.au1ContextName) -
                      1));
    pR6374ExtInParams->uInParams.
        au1ContextName[(sizeof (pR6374ExtInParams->uInParams.au1ContextName) -
                        1)] = '\0';

    if (R6374PortHandleExtInteraction (pR6374ExtInParams, pR6374ExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RFC6374_INPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtInParams);
        MemReleaseMemBlock (RFC6374_OUTPUTPARAMS_POOLID,
                            (UINT1 *) pR6374ExtOutParams);
        return CLI_FAILURE;
    }

    *pu4ContextId = pR6374ExtOutParams->u4ContextId;

    MemReleaseMemBlock (RFC6374_INPUTPARAMS_POOLID,
                        (UINT1 *) pR6374ExtInParams);
    MemReleaseMemBlock (RFC6374_OUTPUTPARAMS_POOLID,
                        (UINT1 *) pR6374ExtOutParams);

    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliInitLMDM                              *
 *     DESCRIPTION      : This function will initiate the combined loss and   *
 *                           delay measurement                      *
 *     INPUT            : CliHandle   - Cli context                           *
 *                        u4ContextId - Context identifier                    *
 *                        ServiceName - Service name                          *
 *                        i4InitLMDM  - Combined LMDM transmit status         *
*     OUTPUT           : None                                               *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliInitLMDM (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4InitLMDM,
                  tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374CmbLMDMTransmitStatus (&u4ErrorCode, u4ContextId,
                                              &ServiceName, i4InitLMDM) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs6374CmbLMDMTransmitStatus (u4ContextId, &ServiceName,
                                           i4InitLMDM) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliDyadicMeasurement                           *
 *     DESCRIPTION      : This function will set Dyadic measurement           *
 *                        per service based                                   *
 *     INPUT            : CliHandle   - Cli context                           *
 *                        u4ContextId - Context identifier                    *
 *                        ServiceName - Service name                          *
 *                        i1DyadicMeasurment - Dyadic Measurement             *
 *     OUTPUT           : None                                                *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliDyadicMeasurement (tCliHandle CliHandle, UINT4 u4ContextId,
                           tSNMP_OCTET_STRING_TYPE ServiceName,
                           INT4 i4DyadicMeasurement)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374DyadicMeasurement (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          i4DyadicMeasurement) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374DyadicMeasurement (u4ContextId, &ServiceName,
                                       i4DyadicMeasurement) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliDyadicProactiveRole                         *
 *     DESCRIPTION      : This function will set Dyadic proactive measurement *
 *                        Role per service based                              *
 *     INPUT            : CliHandle   - Cli context                           *
 *                        u4ContextId - Context identifier                    *
 *                        ServiceName - Service name                          *
 *                        i4DyadicProactiveRole - Dyadic Role active/passive  *
 *     OUTPUT           : None                                                *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliDyadicProactiveRole (tCliHandle CliHandle, UINT4 u4ContextId,
                             tSNMP_OCTET_STRING_TYPE ServiceName,
                             INT4 i4DyadicProactiveRole)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374DyadicProactiveRole (&u4ErrorCode, u4ContextId,
                                            &ServiceName,
                                            i4DyadicProactiveRole) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374DyadicProactiveRole (u4ContextId, &ServiceName,
                                         i4DyadicProactiveRole) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliSessionIntervalQuery                           
 *     DESCRIPTION      : This function will set Session Interval Query       *
 *                        per service based                                   *
 *     INPUT            : CliHandle   - Cli context                           *
 *                        u4ContextId - Context identifier                    *
 *                        ServiceName - Service name                          *
 *                        i4SessIntQueryStatus - Session Interval Query Status*
 *     OUTPUT           : None                                                *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliSessionIntervalQuery (tCliHandle CliHandle, UINT4 u4ContextId,
                              tSNMP_OCTET_STRING_TYPE ServiceName,
                              INT4 i4SessIntQueryStatus)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374SessionIntervalQueryStatus (&u4ErrorCode, u4ContextId,
                                                   &ServiceName,
                                                   i4SessIntQueryStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374SessionIntervalQueryStatus (u4ContextId, &ServiceName,
                                                i4SessIntQueryStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliSetMinimumReceptionInterval                  
 *     DESCRIPTION      : This function will set minimum reception interval   *
 *                        per service based                                   *
 *     INPUT            : CliHandle   - Cli context                           *
 *                        u4ContextId - Context identifier                    *
 *                        ServiceName - Service name                          *
 *                        u2MinReceptionInterval - Minimum Reception          *
 *                                                  Interval                  *
 *     OUTPUT           : None                                                *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliSetMinimumReceptionInterval (tCliHandle CliHandle, UINT4 u4ContextId,
                                     tSNMP_OCTET_STRING_TYPE ServiceName,
                                     UINT4 u4MinReceptionInterval)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374MinReceptionIntervalInMilliseconds
        (&u4ErrorCode, u4ContextId, &ServiceName,
         u4MinReceptionInterval) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374MinReceptionIntervalInMilliseconds
        (u4ContextId, &ServiceName, u4MinReceptionInterval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliSetQueryTransmitRetryCount              *                 
 *     DESCRIPTION      : This function will set retry count for congestion   *
 *                        management per service based                        *
 *     INPUT            : CliHandle   - Cli context                           *
 *                        u4ContextId - Context identifier                    *
 *                        ServiceName - Service name                          *
 *                        u4RetryCount - Retry Count for Congestion           *
 *                                                  Management                *
 *     OUTPUT           : None                                                *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliSetQueryTransmitRetryCount (tCliHandle CliHandle, UINT4 u4ContextId,
                                    tSNMP_OCTET_STRING_TYPE ServiceName,
                                    UINT4 u4RetryCount)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;

    if (nmhTestv2Fs6374QueryTransmitRetryCount (&u4ErrorCode, u4ContextId,
                                                &ServiceName,
                                                u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs6374QueryTransmitRetryCount (u4ContextId, &ServiceName,
                                             u4RetryCount) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *     FUNCTION NAME    : R6374CliConfigLMDMParams                  *
 *     DESCRIPTION      : This function will set the combined LMDM fields     *
 *                   required for combined Loss and  Delay Measurement.  *
 *     INPUT            : CliHandle       - Cli context                       *
 *                        u4ContextId     - Context identifier                *
 *                        ServiceName     - ServiceName                       *
 *                        i4Type          - Type of the combined LMDM to      *
 *                                          initiate                          *
 *                        i4Method        - Method of the combined LMDM to    *
 *                                    initiate                          *
 *                        i4Mode          - Mode of the combined LMD to       *
 *                                        initiate                          *
 *                        u4Count         - No of packets to be transmitted   *
 *                                    when mode is on-demand            *
 *                        u4Interval      - periodic interval to send out     *
 *                                    the combined LMDM pdu             *
 *                        u4PadSize       - Padding size to be included in    *
 *                                    Padding TLV                  *
 *     OUTPUT           : None                                                *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                             *
 ******************************************************************************/
INT4
R6374CliConfigLMDMParams (tCliHandle CliHandle, UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE ServiceName,
                          INT4 i4Type, INT4 i4Method, INT4 i4Mode,
                          UINT4 u4Count, UINT4 u4Interval, UINT4 u4PadSize)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4LmDmMode = RFC6374_INIT_VAL;
    INT4                i4TransmitStatus = RFC6374_INIT_VAL;
    INT4                i4DyadicMeasurement = RFC6374_INIT_VAL;
    INT4                i4DyadicProactiveRole = RFC6374_INIT_VAL;

    if (i4Type != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374CmbLMDMType (&u4ErrorCode, u4ContextId,
                                        &ServiceName, i4Type) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374CmbLMDMType (u4ContextId, &ServiceName,
                                     i4Type) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4Method != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374CmbLMDMMethod (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          i4Method) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMMethod (u4ContextId, &ServiceName,
                                       i4Method) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4Interval != RFC6374_INIT_VAL)
    {
        if (nmhTestv2Fs6374CmbLMDMTimeIntervalInMilliseconds
            (&u4ErrorCode, u4ContextId,
             &ServiceName, u4Interval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMTimeIntervalInMilliseconds
            (u4ContextId, &ServiceName, u4Interval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (u4PadSize != RFC6374_INVALID_PAD_SIZE)
    {
        if (nmhTestv2Fs6374CmbLMDMPaddingSize (&u4ErrorCode, u4ContextId,
                                               &ServiceName,
                                               u4PadSize) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMPaddingSize (u4ContextId, &ServiceName,
                                            u4PadSize) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4Mode != RFC6374_INIT_VAL)
    {
        nmhGetFs6374CmbLMDMMode (u4ContextId, &ServiceName, &i4LmDmMode);
        nmhGetFs6374CmbLMDMTransmitStatus (u4ContextId, &ServiceName,
                                           &i4TransmitStatus);

        if ((i4LmDmMode == RFC6374_LMDM_MODE_PROACTIVE) &&
            (i4Mode == RFC6374_LMDM_MODE_ONDEMAND) &&
            (u4Count != RFC6374_INIT_VAL) &&
            (i4TransmitStatus == RFC6374_TX_STATUS_NOT_READY))
        {
            CliPrintf (CliHandle, "\r%% To stop the Combined LMDM proactive "
                       "test specify only mode as on-demand without optioanl "
                       "parameters\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2Fs6374CmbLMDMMode (&u4ErrorCode, u4ContextId,
                                        &ServiceName, i4Mode) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMMode (u4ContextId, &ServiceName,
                                     i4Mode) == SNMP_FAILURE)
        {
            /* Check is Return Failure is due to Dyadic Role Passive and
             * mode Proactive */
            nmhGetFs6374DyadicMeasurement (u4ContextId, &ServiceName,
                                           &i4DyadicMeasurement);
            nmhGetFs6374DyadicProactiveRole (u4ContextId, &ServiceName,
                                             &i4DyadicProactiveRole);
            nmhGetFs6374CmbLMDMMode (u4ContextId, &ServiceName, &i4LmDmMode);
            nmhGetFs6374CmbLMDMTransmitStatus (u4ContextId, &ServiceName,
                                               &i4TransmitStatus);
            if ((i4LmDmMode == RFC6374_LMDM_MODE_PROACTIVE)
                && (i4DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
                && (i4DyadicProactiveRole == RFC6374_DYADIC_PRO_ROLE_PASSIVE)
                && (i4TransmitStatus == RFC6374_TX_STATUS_READY))
            {
                CliPrintf (CliHandle, "\r Warning: Cannot initiate the Dyadic "
                           "Proactive test as Dyadic Proactive Role is set to passive\r\n");
            }
            return CLI_FAILURE;
        }
        if (i4Mode == CLI_RFC6374_MODE_ONDEMAND)
        {
            if (u4Count != RFC6374_INIT_VAL)
            {
                if (nmhTestv2Fs6374CmbLMDMNoOfMessages (&u4ErrorCode,
                                                        u4ContextId,
                                                        &ServiceName,
                                                        u4Count) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFs6374CmbLMDMNoOfMessages (u4ContextId, &ServiceName,
                                                     u4Count) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *     FUNCTION NAME    : R6374CliResetLMDMParams                            *
 *     DESCRIPTION      : This function will reset the combined LMDM         *
 *                   parameters to default values                       *
 *     INPUT            : CliHandle      - Cli context                       *
 *                        u4ContextId    - Context identifier                *
 *                        ServiceName    - ServiceName                       *
 *                        u4Command      - Type of command                   *
 *     OUTPUT           : None                                               *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            *
 *****************************************************************************/
INT4
R6374CliResetLMDMParams (tCliHandle CliHandle, UINT4 u4ContextId,
                         tSNMP_OCTET_STRING_TYPE ServiceName, UINT4 u4Command)
{
    UINT4               u4ErrorCode = RFC6374_INIT_VAL;
    INT4                i4LmDmMode = RFC6374_INIT_VAL;

    if ((u4Command & RFC6374_CLI_RESET_LMDM_TYPE) ==
        RFC6374_CLI_RESET_LMDM_TYPE)
    {
        if (nmhTestv2Fs6374CmbLMDMType (&u4ErrorCode, u4ContextId,
                                        &ServiceName,
                                        RFC6374_LMDM_DEFAULT_TYPE) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMType (u4ContextId, &ServiceName,
                                     RFC6374_LMDM_DEFAULT_TYPE) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4Command & RFC6374_CLI_RESET_LMDM_MODE) ==
        RFC6374_CLI_RESET_LMDM_MODE)
    {
        nmhGetFs6374CmbLMDMMode (u4ContextId, &ServiceName, &i4LmDmMode);

        if (nmhTestv2Fs6374CmbLMDMMode (&u4ErrorCode, u4ContextId,
                                        &ServiceName,
                                        RFC6374_LMDM_DEFAULT_MODE) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMMode (u4ContextId, &ServiceName,
                                     RFC6374_LMDM_DEFAULT_MODE) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        /* 'no lm-dm mode' - command used to reset Combined LMDM mode to 
         * on-demand and Combined LMDM No of message count to default value 
         * of 10 and the same is used to stop the currently running 
         * proactive test. 
         * So in this case Combined LMDM count need not be reset.*/
        if (i4LmDmMode == RFC6374_LMDM_MODE_ONDEMAND)
        {
            if (nmhTestv2Fs6374CmbLMDMNoOfMessages (&u4ErrorCode, u4ContextId,
                                                    &ServiceName,
                                                    RFC6374_LMDM_DEFAULT_ONDEMAND_COUNT)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFs6374CmbLMDMNoOfMessages (u4ContextId, &ServiceName,
                                                 RFC6374_LMDM_DEFAULT_ONDEMAND_COUNT)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    if ((u4Command & RFC6374_CLI_RESET_LMDM_METHOD) ==
        RFC6374_CLI_RESET_LMDM_METHOD)
    {
        if (nmhTestv2Fs6374CmbLMDMMethod (&u4ErrorCode, u4ContextId,
                                          &ServiceName,
                                          RFC6374_LMDM_DEFAULT_METHOD) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs6374CmbLMDMMethod (u4ContextId, &ServiceName,
                                       RFC6374_LMDM_DEFAULT_METHOD) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4Command & RFC6374_CLI_RESET_LMDM_QUERY_INTERVAL) ==
        RFC6374_CLI_RESET_LMDM_QUERY_INTERVAL)
    {
        if (nmhTestv2Fs6374CmbLMDMTimeIntervalInMilliseconds
            (&u4ErrorCode, u4ContextId, &ServiceName,
             RFC6374_LMDM_DEFAULT_INTERVAL) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMTimeIntervalInMilliseconds (u4ContextId,
                                                           &ServiceName,
                                                           RFC6374_LMDM_DEFAULT_INTERVAL)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u4Command & RFC6374_CLI_RESET_LMDM_PADDING_SIZE) ==
        RFC6374_CLI_RESET_LMDM_PADDING_SIZE)
    {
        /* Default value of padding tlv size is 0 */
        if (nmhTestv2Fs6374CmbLMDMPaddingSize (&u4ErrorCode, u4ContextId,
                                               &ServiceName,
                                               RFC6374_DEFAULT_PAD_SIZE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs6374CmbLMDMPaddingSize (u4ContextId, &ServiceName,
                                            RFC6374_DEFAULT_PAD_SIZE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowBufferAll                                *
 * DESCRIPTION      : This function is called to show Loss and delay       *
 *                    details for a particular service or for all services *
 * INPUT            : CliHandle      - Cli context                         *
 *                    u4ContextId    - Context identifier                  *
 *                    u4Detail       - whether detail to be displayed      *
 *                                     or not                              *
 * OUTPUT           :  None                                                *
 * RETURNS          : TRUE/FALSE                                           *
 **************************************************************************/
INT4
R6374CliShowBufferAll (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 u1Detail)
{
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1               u1LmErrFlag = RFC6374_INIT_VAL;
    UINT1               u1DmErrFlag = RFC6374_INIT_VAL;

    i4RetVal = R6374CliShowLmBufferAll (CliHandle, u4ContextId, u1Detail);

    /* ErrFlag updated as 1 to display the error at the end of the function
     * definition */
    if (i4RetVal == CLI_FAILURE)
    {
        u1LmErrFlag = RFC6374_INDEX_ONE;
    }

    i4RetVal = R6374CliShowDmBufferAll (CliHandle, u4ContextId, u1Detail);

    /* ErrFlag updated as 1 to display the error at the end of the function
     * definition */
    if (i4RetVal == CLI_FAILURE)
    {
        u1DmErrFlag = RFC6374_INDEX_ONE;
    }

    if ((u1LmErrFlag == RFC6374_INDEX_ONE) &&
        (u1DmErrFlag == RFC6374_INDEX_ONE))
    {
        CLI_SET_ERR (RFC6374_INIT_VAL);
        CliPrintf (CliHandle,
                   "\r%% LM/DM entry is not present to display/clear \r\n");
    }
    else if (u1LmErrFlag == RFC6374_INDEX_ONE)
    {
        CLI_SET_ERR (RFC6374_INIT_VAL);
        CliPrintf (CliHandle,
                   "\r%% LM entry is not present to display/clear \r\n");
    }
    else
    {
        if (u1DmErrFlag == RFC6374_INDEX_ONE)
        {
            CLI_SET_ERR (RFC6374_INIT_VAL);
            CliPrintf (CliHandle,
                       "\r\n%% DM entry is not present to display/clear \r\n");
        }
    }
    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : R6374CliShowBufferPerService                         *
 * DESCRIPTION      : This function is called to show Loss and delay       *
 *                    details for a particular service or for all services *
 * INPUT            : CliHandle      - Cli context                         *
 *                    u4ContextId    - Context identifier                  *
 *                    ServiceName    - ServiceName                         *
 *                    u4Detail       - whether detail to be displayed      *
 *                                     or not                              *
 * OUTPUT           :  None                                                *
 * RETURNS          : TRUE/FALSE                                           *
 **************************************************************************/
INT4
R6374CliShowBufferPerService (tCliHandle CliHandle, UINT4 u4ContextId,
                              tSNMP_OCTET_STRING_TYPE ServiceName,
                              UINT4 u4Detail)
{
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1               u1LmErrFlag = RFC6374_INIT_VAL;
    UINT1               u1DmErrFlag = RFC6374_INIT_VAL;

    i4RetVal =
        R6374CliShowLmBufferPerService (CliHandle, u4ContextId,
                                        ServiceName, (UINT1) u4Detail);
    /* ErrFlag updated as 1 to display the error at the end of the function
     * definition */
    if (i4RetVal == CLI_FAILURE)
    {
        u1LmErrFlag = RFC6374_INDEX_ONE;
    }

    i4RetVal =
        R6374CliShowDmBufferPerService (CliHandle, u4ContextId,
                                        ServiceName, (UINT1) u4Detail);
    /* ErrFlag updated as 1 to display the error at the end of the function
     * definition */
    if (i4RetVal == CLI_FAILURE)
    {
        u1DmErrFlag = RFC6374_INDEX_ONE;
    }

    if ((u1LmErrFlag == RFC6374_INDEX_ONE) &&
        (u1DmErrFlag == RFC6374_INDEX_ONE))
    {
        CLI_SET_ERR (RFC6374_INIT_VAL);
        CliPrintf (CliHandle,
                   "\r%% LM/DM entry is not present to display/clear \r\n");
    }
    else if (u1LmErrFlag == RFC6374_INDEX_ONE)
    {
        CLI_SET_ERR (RFC6374_INIT_VAL);
        CliPrintf (CliHandle,
                   "\r%% LM entry is not present to display/clear \r\n");
    }
    else
    {
        if (u1DmErrFlag == RFC6374_INDEX_ONE)
        {
            CLI_SET_ERR (RFC6374_INIT_VAL);
            CliPrintf (CliHandle,
                       "\r\n%% DM entry is not present to display/clear \r\n");
        }
    }

    return i4RetVal;
}

/****************************************************************************
*     FUNCTION NAME    : R6374PrintTrapControl                              *
*                                                                           *
*     DESCRIPTION      : This function Print the Types of Enabled           *
*                        value is diffrent then default value               *
*                                                                           *
*     INPUT            : CliHandle - Handle to the cli context              *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : VOID                                               *
*****************************************************************************/
PRIVATE VOID
R6374PrintTrapControl (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT1               au1TrapOption[RFC6374_INDEX_TWO] = { 0 };
    UINT1               u1CurTrapOptionLsb = (UINT1) RFC6374_INIT_VAL;
    UINT1               u1CurTrapOptionMsb = (UINT1) RFC6374_INIT_VAL;

    RFC6374_MEMSET (&TrapOption, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (au1TrapOption, RFC6374_INIT_VAL, sizeof (au1TrapOption));
    TrapOption.pu1_OctetList = au1TrapOption;
    TrapOption.i4_Length = 1;

    nmhGetFs6374LMDMTrapControl (RFC6374_DEFAULT_CONTEXT_ID, &TrapOption);
    u1CurTrapOptionLsb = TrapOption.pu1_OctetList[RFC6374_INDEX_ZERO];
    u1CurTrapOptionMsb = TrapOption.pu1_OctetList[RFC6374_INDEX_ONE];

    if ((u1CurTrapOptionLsb != RFC6374_INIT_VAL) ||
        (u1CurTrapOptionMsb != RFC6374_INIT_VAL))
    {
        CliPrintf (CliHandle, "\r\nsnmp-server enable traps mpls pm ");
    }
    if (R6374_CLI_ALL_TRAP_ENABLED (u1CurTrapOptionLsb, u1CurTrapOptionMsb) ==
        RFC6374_TRUE)
    {
        CliPrintf (CliHandle, "all\r\n");
    }
    else
    {
        if (R6374_CLI_UNSUP_VERSRION_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "unsup-version ");
        }
        if (R6374_CLI_UNSUP_CC_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "unsup-control-code ");
        }
        if (R6374_CLI_UNSUP_CONN_MISMATCH_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "conn-mismatch ");
        }
        if (R6374_CLI_UNSUP_DATA_FORMAT_INVALID_TRAP_ENABLED
            (u1CurTrapOptionMsb) == RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "data-format-invalid ");
        }
        if (R6374_CLI_UNSUP_MAND_TLV_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "unsup-mand-tlv ");
        }
        if (R6374_CLI_UNSUP_TIMESTAMP_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "unsup-timestamp ");
        }
        if (R6374_CLI_UNSUP_RESP_TIMEOUT_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "response-timeout ");
        }
        if (R6374_CLI_UNSUP_RES_UNAVAIL_TRAP_ENABLED (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "resource-unavailable ");
        }
        if (R6374_CLI_TEST_ABORT_TRAP_ENABLED (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "test-abort ");
        }
        if (RFC6374_CLI_PROACTIVE_TEST_RESTART_TRAP (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "proactive-test-restart ");
        }
        if (R6374_CLI_UNSUPP_QUERY_INTERVAL_TRAP_ENABLED (u1CurTrapOptionLsb)
            == RFC6374_TRUE)
        {
            CliPrintf (CliHandle, "unsup-query-interval ");
        }
        CliPrintf (CliHandle, "\r\n");

    }
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearDelayBufferForService
 *
 *     DESCRIPTION      : This function will clear delay buffer for a given 
 *                        service
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        ServiceName - ServiceName
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearDelayBufferForService (tCliHandle CliHandle, UINT4 u4ContextId,
                                    tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));

    RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);

    if (R6374UtilRmEntriesFromDmStatsForService (u4ContextId, au1ServiceName) !=
        RFC6374_SUCCESS)
    {
        CLI_SET_ERR (RFC6374_CLI_DM_TABLE_EMPTY);
        return CLI_FAILURE;
    }

    R6374UtilRmEntriesFromDmBufferForService (u4ContextId, au1ServiceName);

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearLossBufferForService
 *
 *     DESCRIPTION      : This function will clear loss buffer for a given 
 *                        service
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        ServiceName - ServiceName
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearLossBufferForService (tCliHandle CliHandle, UINT4 u4ContextId,
                                   tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));

    RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);

    if (R6374UtilRmEntriesFromLmStatsForService (u4ContextId, au1ServiceName)
        != RFC6374_SUCCESS)
    {
        CLI_SET_ERR (RFC6374_CLI_LM_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    R6374UtilRmEntriesFromLmBufferForService (u4ContextId, au1ServiceName);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : R6374CliClearBufferAllForService
 *
 *     DESCRIPTION      : This function will clear the loss and delay buffer 
 *                        for a service.
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        u4ContextId - Context  Identifier
 *                        ServiceName
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
R6374CliClearBufferAllForService (tCliHandle CliHandle, UINT4 u4ContextId,
                                  tSNMP_OCTET_STRING_TYPE ServiceName)
{
    UINT1               u1BothTableEmpty = RFC6374_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;

    i4RetVal =
        R6374CliClearDelayBufferForService (CliHandle, u4ContextId,
                                            ServiceName);

    if (i4RetVal != CLI_SUCCESS)
    {
        u1BothTableEmpty++;
    }

    i4RetVal =
        R6374CliClearLossBufferForService (CliHandle, u4ContextId, ServiceName);

    if (i4RetVal != CLI_SUCCESS)
    {
        u1BothTableEmpty++;
    }

    if (u1BothTableEmpty == 2)
    {
        CLI_SET_ERR (RFC6374_CLI_TABLES_EMPTY);
        i4RetVal = CLI_FAILURE;
    }
    else if (u1BothTableEmpty == 1)
        /* Means 1 table returns failure so return failure from here so 
         * that error msg can come*/
    {
        i4RetVal = CLI_FAILURE;
    }
    return i4RetVal;
}

/************************************************************************
 *                                                                      *
 *  Function Name : IssMplsPmRFC6374ShowDebugging                       *
 *                                                                      *
 *  Description   : This function prints the debug level                *
 *                                                                      *
 *  Input(s)      :                                                     *
 *                                                                      *
 *  Output(s)     : NULL                                                *
 *                                                                      *
 *  Returns       : NONE                                                *
 *                                                                      *
 ***********************************************************************/
VOID
IssMplsPmRFC6374ShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4DbgLevel = 0;
    UINT4               u4ContextId = RFC6374_DEFAULT_CONTEXT_ID;

    nmhGetFs6374DebugLevel (u4ContextId, &u4DbgLevel);

    if (u4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rMPLS PM:\n");

    if ((u4DbgLevel & CLI_RFC6374_DBG_ALL) == CLI_RFC6374_DBG_ALL)
    {
        CliPrintf (CliHandle, "  PM All debugging is on\r\n");
        return;
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_INIT) == CLI_RFC6374_DBG_INIT)
    {
        CliPrintf (CliHandle, "  PM init debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_MGMT) == CLI_RFC6374_DBG_MGMT)
    {
        CliPrintf (CliHandle, "  PM management debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_RESOURCE) == CLI_RFC6374_DBG_RESOURCE)
    {
        CliPrintf (CliHandle, "  PM resource debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_ALL_FAIL) == CLI_RFC6374_DBG_ALL_FAIL)
    {
        CliPrintf (CliHandle, "  PM all-fail debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_PKT_DUMP) == CLI_RFC6374_DBG_PKT_DUMP)
    {
        CliPrintf (CliHandle, "  PM pkt debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_FN_ENTRY) == CLI_RFC6374_DBG_FN_ENTRY)
    {
        CliPrintf (CliHandle, "  PM func-entry debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_FN_EXIT) == CLI_RFC6374_DBG_FN_EXIT)
    {
        CliPrintf (CliHandle, "  PM func-exit debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_CTRL_PLANE) == CLI_RFC6374_DBG_CTRL_PLANE)
    {
        CliPrintf (CliHandle, "  PM control plane debugging is on\r\n");
    }
    if ((u4DbgLevel & CLI_RFC6374_DBG_CRITICAL) == CLI_RFC6374_DBG_CRITICAL)
    {
        CliPrintf (CliHandle, "  PM critical debugging is on\r\n");
    }
}

/****************************************************************************
 * Function Name : RFC6374CliShowGlobalInfo                    *
 * Description   : This routine shows the contents present in the        *
 *                 Global Information.                        *
 * Input         : CliHandle - Handle to CLI                    *
 * Output        : NONE                                *
 * Return        : CLI_FAILURE/CLI_SUCCESS                    *
 ***************************************************************************/
INT4
RFC6374CliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    INT4                i4SystemControl = RFC6374_INIT_VAL;
    INT4                i4ModuleStatus = RFC6374_INIT_VAL;
    INT4                i4SysSuppTimeFormat = RFC6374_INIT_VAL;
    UINT1               au1TrapOption[RFC6374_INDEX_TWO] = { 0 };
    UINT1              *pu1FirstLine = NULL;
    UINT1               au1FirstLine[RFC6374_VAL_40];
    UINT1               au1BlankSpace[CLI_MAX_COLS];
    UINT1               u1CurTrapOptionLsb = (UINT1) RFC6374_INIT_VAL;
    UINT1               u1CurTrapOptionMsb = (UINT1) RFC6374_INIT_VAL;
    UINT1               u1TempChar = RFC6374_INIT_VAL;
    UINT1               u1Flag = RFC6374_INIT_VAL;

    RFC6374_MEMSET (&TrapOption, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (au1TrapOption, RFC6374_INIT_VAL, sizeof (au1TrapOption));

    TrapOption.pu1_OctetList = au1TrapOption;
    TrapOption.i4_Length = 1;

    CliPrintf (CliHandle, "\r\n");

    nmhGetFs6374SystemControl (u4ContextId, &i4SystemControl);
    nmhGetFs6374ModuleStatus (u4ContextId, &i4ModuleStatus);
    nmhGetFs6374TSFormatSupported (u4ContextId, &i4SysSuppTimeFormat);

    /* System Control Status */
    CliPrintf (CliHandle, "%-28s : %s\r\n", "System Status",
               ((i4SystemControl == RFC6374_START) ? "Start" : "Shutdown"));
    /* Module Status */
    CliPrintf (CliHandle, "%-28s : %s\r\n", "Module Status",
               ((i4ModuleStatus == RFC6374_ENABLED) ? "Enable" : "Disable"));

    /* System supported timestamp format */
    MEMSET (au1FirstLine, 0, RFC6374_VAL_40);

    MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

    pu1FirstLine = au1FirstLine;

    STRCPY (pu1FirstLine, "System Timestamp Format      :");

    CliPrintf (CliHandle, "%-28s ", pu1FirstLine);

    if ((i4SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_IEEE1588) ==
        RFC6374_SUPP_TS_FORMAT_IEEE1588)
    {
        CliPrintf (CliHandle, "PTP Timestamp");
        u1Flag = RFC6374_INDEX_ONE;
    }
    if ((i4SysSuppTimeFormat & RFC6374_SUPP_TS_FORMAT_NTP) ==
        RFC6374_SUPP_TS_FORMAT_NTP)
    {
        if (u1Flag == RFC6374_INDEX_ONE)
        {
            CliPrintf (CliHandle, ", ");
        }
        CliPrintf (CliHandle, "NTP Timestamp");
    }
    CliPrintf (CliHandle, "\r\n");

    /* Trap Status */
    MEMSET (au1FirstLine, 0, RFC6374_VAL_40);

    MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

    pu1FirstLine = au1FirstLine;

    STRCPY (pu1FirstLine, "Enabled Traps     :");

    /* Here, 0x20 refers to white space and it is used to display the
     * trap in new line when counter is reached to 2/4/6 */

    MEMSET (au1BlankSpace, 0x20,
            MEM_MAX_BYTES (sizeof (au1BlankSpace), STRLEN (pu1FirstLine)));

    au1BlankSpace[MEM_MAX_BYTES
                  (((sizeof (au1BlankSpace)) - 1), STRLEN (pu1FirstLine))] =
        '\0';

    CliPrintf (CliHandle, "%-28s ", pu1FirstLine);

    nmhGetFs6374LMDMTrapControl (u4ContextId, &TrapOption);

    u1CurTrapOptionLsb = TrapOption.pu1_OctetList[RFC6374_INDEX_ZERO];
    u1CurTrapOptionMsb = TrapOption.pu1_OctetList[RFC6374_INDEX_ONE];

    if ((u1CurTrapOptionLsb == RFC6374_INIT_VAL) &&
        (u1CurTrapOptionMsb == RFC6374_INIT_VAL))
    {
        CliPrintf (CliHandle, "None");
    }
    else
    {
        if (R6374_CLI_UNSUP_VERSRION_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
            CliPrintf (CliHandle, "unsup-version");
        }
        if (R6374_CLI_UNSUP_CC_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
            CliPrintf (CliHandle, "unsup-control-code");
        }
        if (R6374_CLI_UNSUP_CONN_MISMATCH_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if (u1TempChar == RFC6374_INDEX_TWO)
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "conn-mismatch");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }
        if (R6374_CLI_UNSUP_DATA_FORMAT_INVALID_TRAP_ENABLED
            (u1CurTrapOptionMsb) == RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if (u1TempChar == RFC6374_INDEX_TWO)
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "data-format-invalid");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }
        if (R6374_CLI_UNSUP_MAND_TLV_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "unsup-mand-tlv");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);

        }
        if (R6374_CLI_UNSUP_TIMESTAMP_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "unsup-timestamp");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }
        if (R6374_CLI_UNSUP_RESP_TIMEOUT_TRAP_ENABLED (u1CurTrapOptionMsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR) ||
                (u1TempChar == RFC6374_INDEX_SIX))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "response-timeout");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }
        if (R6374_CLI_UNSUP_RES_UNAVAIL_TRAP_ENABLED (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR) ||
                (u1TempChar == RFC6374_INDEX_SIX))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "resource-unavailable");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }
        if (R6374_CLI_TEST_ABORT_TRAP_ENABLED (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR) ||
                (u1TempChar == RFC6374_INDEX_SIX) ||
                (u1TempChar == RFC6374_INDEX_EIGHT))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "test-abort");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }
        if (RFC6374_CLI_PROACTIVE_TEST_RESTART_TRAP (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR) ||
                (u1TempChar == RFC6374_INDEX_SIX) ||
                (u1TempChar == RFC6374_INDEX_EIGHT))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "proactive-test-restart");
            u1TempChar = (UINT1) (u1TempChar + RFC6374_INDEX_ONE);
        }

        if (R6374_CLI_UNSUPP_QUERY_INTERVAL_TRAP_ENABLED (u1CurTrapOptionLsb) ==
            RFC6374_TRUE)
        {
            if (u1TempChar >= 1)
            {
                CliPrintf (CliHandle, ", ");
            }
            if ((u1TempChar == RFC6374_INDEX_TWO) ||
                (u1TempChar == RFC6374_INDEX_FOUR) ||
                (u1TempChar == RFC6374_INDEX_SIX) ||
                (u1TempChar == RFC6374_INDEX_EIGHT) ||
                (u1TempChar == RFC6374_INDEX_TEN))
            {
                CliPrintf (CliHandle, "\r\n");
                STRNCPY (pu1FirstLine, au1BlankSpace, sizeof (au1FirstLine));
                pu1FirstLine[sizeof (au1FirstLine) - 1] = '\0';
                CliPrintf (CliHandle, "%s ", pu1FirstLine);
            }
            CliPrintf (CliHandle, "unsup-query-interval");
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*************************************************************************
* Function    :  R6374UtilValidateSerConfigParamsForService
* Input       :  None
* Description :  This routine is used to get the service config 
*              information for the given service and if the service is 
*          present then validates the service config params with 
*          existing service config params. If the params are not 
*          matching with existing service params then service 
*          creation won't be allowed also Modification for the
*          same service cannot be allowed in config mode. 
* Output      :  None
* Returns     :  RFC6374_SUCCESS or RFC6374_FAILURE
****************************************************************************/
PRIVATE INT4
     
     
     
     R6374UtilValidateSerConfigParamsForService
    (UINT4 u4ContextId,
     tSNMP_OCTET_STRING_TYPE ServiceName,
     UINT4 u4FwdTnPwlId, UINT4 u4RevTnlId,
     tSNMP_OCTET_STRING_TYPE * pSrcIpAddr,
     tSNMP_OCTET_STRING_TYPE * pDestIpAddr, UINT4 u4Command)
{
    tR6374MplsParams   *pR6374MplsPathParams = NULL;
    tR6374ServiceConfigTableEntry *pR6374ServiceConfigTableEntry = NULL;
    tSNMP_OCTET_STRING_TYPE SrcIpAddr;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DestIpAddr = 0;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN] = { 0 };

    RFC6374_MEMCPY (au1ServiceName, ServiceName.pu1_OctetList,
                    ServiceName.i4_Length);

    pR6374ServiceConfigTableEntry = R6374UtilGetServiceFromName (u4ContextId,
                                                                 au1ServiceName);

    if (pR6374ServiceConfigTableEntry == NULL)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374UtilValidateSerConfigParamsForService:"
                     "Service Config Entry does not exist.\r\n");
        return RFC6374_FAILURE;
    }
    pR6374MplsPathParams = &pR6374ServiceConfigTableEntry->R6374MplsParams;

    SrcIpAddr.i4_Length = pSrcIpAddr->i4_Length;
    SrcIpAddr.pu1_OctetList = pSrcIpAddr->pu1_OctetList;

    RFC6374_OCTETSTRING_TO_INTEGER (SrcIpAddr, u4SrcIpAddr);
    u4SrcIpAddr = OSIX_HTONL (u4SrcIpAddr);

    DestAddr.i4_Length = pDestIpAddr->i4_Length;
    DestAddr.pu1_OctetList = pDestIpAddr->pu1_OctetList;

    RFC6374_OCTETSTRING_TO_INTEGER (DestAddr, u4DestIpAddr);
    u4DestIpAddr = OSIX_HTONL (u4DestIpAddr);

    if (u4Command == CLI_RFC6374_CREATE_SERVICE_TNL)
    {

        if (((pR6374MplsPathParams->LspMplsPathParams.u4SrcIpAddr != 0) &&
             (pR6374MplsPathParams->LspMplsPathParams.u4SrcIpAddr
              != u4SrcIpAddr)) ||
            ((pR6374MplsPathParams->LspMplsPathParams.u4DestIpAddr != 0) &&
             (pR6374MplsPathParams->LspMplsPathParams.u4DestIpAddr
              != u4DestIpAddr)) ||
            ((pR6374MplsPathParams->LspMplsPathParams.u4FrwdTunnelId != 0) &&
             (pR6374MplsPathParams->LspMplsPathParams.u4FrwdTunnelId
              != u4FwdTnPwlId)) ||
            ((pR6374MplsPathParams->LspMplsPathParams.u4RevTunnelId != 0) &&
             (pR6374MplsPathParams->LspMplsPathParams.u4RevTunnelId
              != u4RevTnlId)))
        {
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateSerConfigParamsForService:"
                         "Service creation is failed due to mismatch "
                         "in service config tunnel parameters.\r\n");

            return RFC6374_FAILURE;
        }
    }

    else if (u4Command == CLI_RFC6374_CREATE_SERVICE_PW)
    {

        if (((pR6374MplsPathParams->PwMplsPathParams.u4IpAddr != 0) &&
             (pR6374MplsPathParams->PwMplsPathParams.u4IpAddr
              != u4SrcIpAddr)) ||
            ((pR6374MplsPathParams->PwMplsPathParams.u4PwId != 0) &&
             (pR6374MplsPathParams->PwMplsPathParams.u4PwId != u4FwdTnPwlId)))
        {
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                         RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374UtilValidateSerConfigParamsForService:"
                         "Service creation is failed due to mismatch "
                         "in service creation PW parameters.\r\n");

            return RFC6374_FAILURE;
        }
    }
    return RFC6374_SUCCESS;
}

/*************************************************************************
* Function    :  R6374CliLmDmNoRespSess
* Description :  This routine is used to print if there is any 100% loss in 
*                measurement for LM, DM and CombLMDM
* Input       :  CliHandle - CLI Pointer
*                u4ContextId - Running Context ID
*                ServiceName - pointer to Service name
*                u1MeasurementType - Type measurement LOSS/DELAY
* Output      :  None
* Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
PUBLIC INT4
R6374CliLmDmNoRespSess (tCliHandle CliHandle, UINT4 u4ContextId,
                        tSNMP_OCTET_STRING_TYPE * pService,
                        UINT1 u1MeasurementType)
{
    tSNMP_OCTET_STRING_TYPE ServiceName;
    tSNMP_OCTET_STRING_TYPE NxtServiceName;
    UINT4               u4CurrentContextId = RFC6374_INIT_VAL;
    UINT4               u4NxtContextId = RFC6374_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1               au1ServiceName[RFC6374_SERVICE_NAME_MAX_LEN];
    UINT1               au1NxtServiceName[RFC6374_SERVICE_NAME_MAX_LEN];

    RFC6374_MEMSET (&ServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));
    RFC6374_MEMSET (&NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    RFC6374_MEMSET (au1ServiceName, RFC6374_INIT_VAL, sizeof (au1ServiceName));
    RFC6374_MEMSET (au1NxtServiceName, RFC6374_INIT_VAL,
                    sizeof (au1NxtServiceName));

    /* Service name */
    ServiceName.pu1_OctetList = au1ServiceName;
    ServiceName.i4_Length = RFC6374_INIT_VAL;

    /* Service name */
    NxtServiceName.pu1_OctetList = au1NxtServiceName;
    NxtServiceName.i4_Length = RFC6374_INIT_VAL;

    if (pService == NULL)
    {
        /* Check for all services */
        if (nmhGetFirstIndexFs6374ServiceConfigTable (&u4CurrentContextId,
                                                      &ServiceName) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        do
        {
            R6374CliDisplayLmDmNoRespSess (CliHandle, u4CurrentContextId,
                                           &ServiceName, u1MeasurementType);

            i4RetVal =
                nmhGetNextIndexFs6374ServiceConfigTable (u4CurrentContextId,
                                                         &u4NxtContextId,
                                                         &ServiceName,
                                                         &NxtServiceName);

            if (i4RetVal == SNMP_SUCCESS)
            {
                u4CurrentContextId = u4NxtContextId;

                ServiceName.i4_Length = NxtServiceName.i4_Length;
                RFC6374_STRCPY (&ServiceName.pu1_OctetList,
                                &NxtServiceName.pu1_OctetList);
            }
        }
        while (i4RetVal == SNMP_SUCCESS);

    }
    else
    {
        /* Check for particular service */
        R6374CliDisplayLmDmNoRespSess (CliHandle, u4ContextId, pService,
                                       u1MeasurementType);
    }

    return CLI_SUCCESS;
}

/*************************************************************************
* Function    :  R6374CliDisplayLmDmNoRespSess
* Description :  This routine is used to print if there is any 100% loss in
*                measurement for LM, DM and CombLMDM
* Input       :  CliHandle - CLI Pointer
*                u4ContextId - Running Context ID
*                ServiceName - pointer to Service name
*                u1MeasurementType - Type measurement LOSS/DELAY
* Output      :  None
* Returns     :  CLI_SUCCESS or CLI_FAILURE
****************************************************************************/
PUBLIC INT4
R6374CliDisplayLmDmNoRespSess (tCliHandle CliHandle, UINT4 u4ContextId,
                               tSNMP_OCTET_STRING_TYPE * pServiceName,
                               UINT1 u1MeasurementType)
{
    tR6374ServiceConfigTableEntry *pServiceConfig = NULL;
    tR6374LmDmInfoTableEntry *pLmDmInfo = NULL;
    tR6374DmInfoTableEntry *pDmInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;

    pServiceConfig =
        R6374UtilGetServiceFromName (u4ContextId, pServiceName->pu1_OctetList);
    if (pServiceConfig == NULL)
    {
        return CLI_FAILURE;
    }

    pLmDmInfo = RFC6374_GET_LMDM_INFO_FROM_SERVICE (pServiceConfig);
    pDmInfo = RFC6374_GET_DM_INFO_FROM_SERVICE (pServiceConfig);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceConfig);

    if ((pServiceConfig->u4LastLmSessId != pLmInfo->u4LmSessionId) &&
        (u1MeasurementType == RFC6374_MEASUREMENT_TYPE_LOSS) &&
        (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY) &&
        (pLmInfo->u4LmNoRespTxCount != RFC6374_INIT_VAL))
    {
        /* In this case most recent initiated test was a Loss */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, " Independent Loss Measurement\r\n");
        CliPrintf (CliHandle, " Service Name  : %s\r\n",
                   pServiceName->pu1_OctetList);
        CliPrintf (CliHandle, " Session ID    : %d\r\n",
                   pLmInfo->u4LmSessionId);
        CliPrintf (CliHandle, " 100%% Loss\r\n");
    }
    else if ((pServiceConfig->u4LastDmSessId != pDmInfo->u4DmSessionId) &&
             (u1MeasurementType == RFC6374_MEASUREMENT_TYPE_DELAY) &&
             (pDmInfo->u1DmStatus == RFC6374_TX_STATUS_NOT_READY) &&
             (pDmInfo->u4DmNoRespTxCount != RFC6374_INIT_VAL))
    {
        /* In this case most recent initiated test was a Delay */
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, " Independent Delay Measurement\r\n");
        CliPrintf (CliHandle, " Service Name  : %s\r\n",
                   pServiceName->pu1_OctetList);
        CliPrintf (CliHandle, " Session ID    : %d\r\n",
                   pDmInfo->u4DmSessionId);
        CliPrintf (CliHandle, " 100%% Loss\r\n");
    }
    else if ((pServiceConfig->u4LastLmDmSessId != pLmDmInfo->u4LmDmSessionId) &&
             (pServiceConfig->u4LastLmDmSessId != pLmDmInfo->u4LmDmSessionId) &&
             (pLmDmInfo->u1LmDmStatus == RFC6374_TX_STATUS_NOT_READY) &&
             (pLmDmInfo->u4LmDmNoRespTxCount != RFC6374_INIT_VAL))
    {
        /* In this case most recent initiated test was a combined LMDM */
        CliPrintf (CliHandle, "\r\n");
        if (u1MeasurementType == RFC6374_MEASUREMENT_TYPE_LOSS)
        {
            CliPrintf (CliHandle, " Combined Loss Measurement\r\n");
        }
        else if (u1MeasurementType == RFC6374_MEASUREMENT_TYPE_DELAY)
        {
            CliPrintf (CliHandle, " Combined Delay Measurement\r\n");
        }
        CliPrintf (CliHandle, " Service Name  : %s\r\n",
                   pServiceName->pu1_OctetList);
        CliPrintf (CliHandle, " Session ID    : %d\r\n",
                   pLmDmInfo->u4LmDmSessionId);
        CliPrintf (CliHandle, " 100%% Loss\r\n");
    }

    return CLI_SUCCESS;
}

#endif
