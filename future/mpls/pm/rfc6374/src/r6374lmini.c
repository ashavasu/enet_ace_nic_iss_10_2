/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374lmini.c,v 1.3 2017/07/25 12:00:37 siva Exp $
 *
 * Description: This file contains the R6374 task loss 
 * measurement initiator.
 *******************************************************************/
#ifndef __R6374LMINI_C__
#define __R6374LMINI_C__
#include "r6374inc.h"
/******************************************************************************
 * Function           : R6374LmInitiator
 * 
 * Description        : The routine implements the LM Initiator, it calls up
 *                      routine to format and transmit 1LM/LMM frame.
 *                      the various events.
 *  
 * Input(s)           : tR6374PktSmInfo - Service Related Info.
 *                      u1EventID - Process Event
 *
 * Output(s)          : None
 *
 * Returns            : RFC6374_SUCCESS / RFC6374_FAILURE
 *****************************************************************************/
PUBLIC UINT4
R6374LmInitiator (tR6374PktSmInfo * pPktSmInfo, UINT1 u1EventID)
{
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374NotifyInfo    R6374LmNotify;
    tR6374MacAddr       SwitchMacAddr;
    UINT4               u4TempSessionId = RFC6374_INIT_VAL;
    UINT4               u4RetVal = RFC6374_SUCCESS;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitiator\r\n");

    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));
    RFC6374_MEMSET (&SwitchMacAddr, RFC6374_INIT_VAL, sizeof (tR6374MacAddr));

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);

    /* Check for the event received */
    switch (u1EventID)
    {
        case RFC6374_LM_START_SESSION:

            /* Check if LOSS is ready before starting of LM */
            if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_READY)
            {
                pLmInfo->u1LmStatus = RFC6374_TX_STATUS_NOT_READY;
                /* Reset start session posted flag */
                pLmInfo->bIsLmStartSessPosted = RFC6374_FALSE;

                /* Send proactive restart trap incase of pro-active test 
                 * restarted by receiving LSP/PW up event */
                if (((pLmInfo->
                      b1LmTestStatus & RFC6374_PROACTIVE_TEST_RESTARTED) ==
                     RFC6374_PROACTIVE_TEST_RESTARTED)
                    && (pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE))
                {
                    /* Proactive Test re-start Trap */
                    R6374LmNotify.u1TrapType =
                        RFC6374_PROACTIVE_TEST_RESTART_TRAP;
                    RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                                     pServiceInfo->au1ServiceName,
                                     RFC6374_STRLEN (pServiceInfo->
                                                     au1ServiceName));
                    R6374SendNotification (&R6374LmNotify);

                    RFC6374_TRC1 (RFC6374_CRITICAL_TRC,
                                  RFC6374_DEFAULT_CONTEXT_ID,
                                  " Proactive Loss Measurement test "
                                  "restarted for Service name : %s \r\n",
                                  pServiceInfo->au1ServiceName);
#ifdef SYSLOG_WANTED
                    SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL,
                                  (UINT4) RFC6374_SYSLOG_ID,
                                  "Proactive Loss Measurement test restarted for Service name : %s"
                                  "\r\n", pServiceInfo->au1ServiceName));
#endif
                }
                /* Reset test abort status as false */
                pLmInfo->b1LmTestStatus &= ~RFC6374_PROACTIVE_TEST_RESTARTED;
            }
            else
            {
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             " R6374LmInitiator: Unable to Start"
                             " LM Session, As Aleady one Running.\r\n");
                u4RetVal = RFC6374_FAILURE;
                break;
            }

            /* Get Initial Session ID */
            if (pLmInfo->u4LmSessionId == RFC6374_INIT_VAL)
            {
                R6374CfaGetSysMacAddress (SwitchMacAddr);
                RFC6374_GET_SESSION_ID_FROM_MAC (SwitchMacAddr,
                                                 u4TempSessionId);
                R6374UtilGetSessId (NULL, pLmInfo, NULL, u4TempSessionId,
                                    RFC6374_VAL_1);
                RFC6374_LM_INCR_SESSION_ID (pLmInfo);

            }
            else
            {
                /* Increse Session ID/ transaction ID */
                R6374UtilIncrementSessionId (pServiceInfo, RFC6374_VAL_1);
            }

            /* As there can be only 10 Sesssion In Buffer,
             * Increase gobal Session Count for LM */
            RFC6374_LM_INCR_SESSION_COUNT ();

            /* Reset LM Origin Time Stamp */
            RFC6374_LM_RESET_ORIGIN_TIME_STAMP (pLmInfo);

            /* Initiate Seq Counter */
            RFC6374_LM_RESET_SEQ_COUNT (pLmInfo);

            /* Initiate rolling buffer Seq Counter */
            RFC6374_LM_RESET_ROSEQ_COUNT (pLmInfo);

            /* Initiate Buffer Counter that adds in RB TRee */
            RFC6374_LM_RESET_BUFFER_ADD_COUNT (pLmInfo);

            /* Update the no of message to be send only for
             * on-demand transmission */
            if (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND)
            {
                /* Initialize Running Count */
                RFC6374_LM_SET_RUNNING_COUNT (pLmInfo);
            }
            /* Reset Proactive Transmission Count */
            RFC6374_LM_RESET_PROACTIVE_TX_COUNT (pLmInfo);

            /* Reset No Response Tx Count */
            RFC6374_LM_RESET_LM_NO_RESP_TX_COUNT (pLmInfo);

            /* Reset Packet Loss Value */
            RFC6374_LM_RESET_RX_PKT_LOSS (pLmInfo);

            /* Initiate SQI TLV , ensure dyadic is not enabled
             *  and mode is not one way*/
            if ((pServiceInfo->u1SessIntQueryStatus ==
                 CLI_RFC6374_SESS_INT_QUERY_ENABLE) &&
                (pServiceInfo->u1DyadicMeasurement !=
                 RFC6374_DYADIC_MEAS_ENABLE) &&
                (pLmInfo->u1LmType == RFC6374_LM_TYPE_TWO_WAY))
            {
                pServiceInfo->LmInfo.u1SessQueryIntervalState =
                    RFC6374_SQI_INIT_STATE;
            }

        case RFC6374_LM_INTERVAL_EXPIRY:    /* On a Query Timer Expiry */

            if (pLmInfo->u1LmType == RFC6374_LM_TYPE_TWO_WAY)
            {

                /* counter wil be decremented in LMR processing place
                 * whenever PDU is received but if LMR is not
                 * received for more than QueryTransmitRetryCount times 
                 * then transmission will be stopped */

                if (pLmInfo->u4LmNoRespTxCount ==
                    (UINT4) pServiceInfo->u1QueryTransmitRetryCount)
                {
                    /* Response Timeout Trap */
                    R6374LmNotify.u1TrapType = RFC6374_RESP_TIMEOUT_TRAP;
                    RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                                     pServiceInfo->au1ServiceName,
                                     RFC6374_STRLEN (pServiceInfo->
                                                     au1ServiceName));
                    R6374SendNotification (&R6374LmNotify);

                    RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC |
                                  RFC6374_ALL_FAILURE_TRC,
                                  RFC6374_DEFAULT_CONTEXT_ID,
                                  "R6374LmInitiator: "
                                  "u4LmNoRespTxCount :%d "
                                  "Stopping lm transaction "
                                  "as no of retries exceed\r\n",
                                  pLmInfo->u4LmNoRespTxCount);

                    /* Test is aborting because of no response */
                    pLmInfo->b1LmTestStatus |= RFC6374_TEST_ABORTED;

                    /* Stop the LM Transaction */
                    R6374LmInitStopLmTransaction (pServiceInfo);

                    /* Restart Test if mode is Proactive */
                    if ((pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE) &&
                        (pLmInfo->u1LmRespTimeOutReTries !=
                         RFC6374_RESP_TIMEOUT_RETRIES_MAX_COUNT))
                    {
                        /* Increment the Timeout Retires Counter */
                        RFC6374_LM_INCR_RESP_TIMEOUT_RETRIES_COUNT (pLmInfo);

                        /* Check if the Query interval is less 
                         * then 1Sec i.e default value */
                        if (pLmInfo->u2LmTimeInterval <
                            RFC6374_LM_DEFAULT_INTERVAL)
                        {
                            /* Start the Timer of LM Restart */
                            if (R6374TmrStartTmr
                                (RFC6374_LM_QUERY_TMR, pServiceInfo,
                                 ((UINT4)
                                  (RFC6374_VAL_1 *
                                   pLmInfo->u1LmRespTimeOutReTries))) !=
                                RFC6374_SUCCESS)
                            {
                                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                             RFC6374_ALL_FAILURE_TRC,
                                             RFC6374_DEFAULT_CONTEXT_ID,
                                             "R6374LmInitiator: "
                                             "Failure starting LM INTERVAL timer\r\n");
                                return RFC6374_FAILURE;
                            }
                        }
                        else    /* When Query interval timmer is more than a Sec */
                        {
                            /* Start the Timer of LM Restart */
                            if (R6374TmrStartTmr
                                (RFC6374_LM_QUERY_TMR, pServiceInfo,
                                 ((UINT4)
                                  (pLmInfo->u2LmTimeInterval *
                                   pLmInfo->u1LmRespTimeOutReTries))) !=
                                RFC6374_SUCCESS)
                            {
                                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                                             RFC6374_ALL_FAILURE_TRC,
                                             RFC6374_DEFAULT_CONTEXT_ID,
                                             "R6374LmInitiator: "
                                             "Failure starting LM INTERVAL timer\r\n");
                                return RFC6374_FAILURE;
                            }
                        }
                    }
                    else        /* Reset the Max Retries Counter */
                    {
                        RFC6374_LM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmInfo);
                    }
                    break;
                }

                /* Call the routine to transmit the LM  Packet */
                if (R6374LmInitXmitLmmPdu (pPktSmInfo) != RFC6374_SUCCESS)
                {
                    u4RetVal = RFC6374_FAILURE;
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374LmInitiator: Unable to transmit"
                                 " Two way LMM Pdu\r\n");
                    break;
                }
            }
            else
            {
                /* Call the routine to transmit the 1LM Packet */
                if (R6374LmInitXmit1LmPdu (pPktSmInfo) != RFC6374_SUCCESS)

                {
                    u4RetVal = RFC6374_FAILURE;
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 RFC6374_DEFAULT_CONTEXT_ID,
                                 "R6374LmInitiator: Unable to transmit"
                                 " One way 1LM Pdu\r\n");
                    break;
                }
            }
            break;

        case RFC6374_LM_STOP_SESSION:

            /* Stop the Lm Transaction */
            R6374LmInitStopLmTransaction (pServiceInfo);

            RFC6374_LM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmInfo);
            break;

        default:
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                         "R6374LmInitiator: Event Not Supported\r\n");
            u4RetVal = RFC6374_FAILURE;
            break;
    }

    if (u4RetVal == RFC6374_FAILURE)
    {
        /* Update Result OK to False */
        pLmInfo->b1LmResultOk = RFC6374_FALSE;

        /* Test is aborting because of no response */
        pLmInfo->b1LmTestStatus |= RFC6374_TEST_ABORTED;

        /* Stop the Lm Transaction */
        R6374LmInitStopLmTransaction (pServiceInfo);

        RFC6374_LM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmInfo);
    }
    else
    {
        /* Update Result OK to True */
        pLmInfo->b1LmResultOk = RFC6374_TRUE;
    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmInitiator\r\n");
    return u4RetVal;
}

/*******************************************************************************
 * Function Name      : R6374LmInitXmitLmmPdu
 *
 * Description        : This routine formats and transmits the LMM PDU - Two way
 *
 * Input(s)           : tR6374PktSmInfo - Service Related Info
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374LmInitXmitLmmPdu (tR6374PktSmInfo * pPktSmInfo)
{
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4Txf = RFC6374_INIT_VAL;
    UINT4               u4Rxf = RFC6374_INIT_VAL;
    UINT4               u4Duration = RFC6374_INIT_VAL;
    UINT4               u4TimeInterval = RFC6374_INIT_VAL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1              *pu1LmmPduStart = NULL;
    UINT1              *pu1LmmPduEnd = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitXmitLmmPdu\r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);

    /*Check if Number Of Message to be transmitted have been reached */
    if ((pLmInfo->u2LmRunningCount == RFC6374_INIT_VAL) &&
        (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND))
    {

        if (pLmInfo->bDeadlineTimeCrossed == RFC6374_FALSE)
        {
            /* Wait for some extra time (constant for all services) in order
             * to get responses for all transmitted queries */
            pLmInfo->bDeadlineTimeCrossed = RFC6374_TRUE;

            if (R6374TmrStartTmr
                (RFC6374_LM_QUERY_TMR, pServiceInfo, R6374_MAX_DEADLINE_IN_MS)
                != RFC6374_SUCCESS)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmInitXmitLmmPdu: "
                             "Failure starting LM query timer\r\n");
                return RFC6374_FAILURE;
            }

            return RFC6374_SUCCESS;
        }
        /* Reset deadline flag */
        pLmInfo->bDeadlineTimeCrossed = RFC6374_FALSE;

        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmitLmmPdu: "
                     "Stopping Lm Transaction as total number of 1LM/LMM"
                     "have been transmitted\r\n");

        /* Reset test abort status as false */
        pLmInfo->b1LmTestStatus &= ~RFC6374_TEST_ABORTED;

        /*Stop Lm Transaction */
        R6374LmInitStopLmTransaction (pServiceInfo);

        RFC6374_LM_RESET_RESP_TIMEOUT_RETRIES_COUNT (pLmInfo);

        return RFC6374_SUCCESS;
    }

    /* Add origin Time to LM info */
    /* Get Current Time */

    R6374UtilGetCurrentTime (&pLmInfo->LmOriginTimeStamp,
                             pLmInfo->u1LmQTSFormat);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmitLmmPdu: Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1LmmPduStart = RFC6374_PDU;
    pu1LmmPduEnd = RFC6374_PDU;

    /* Format the LMM PDU header */
    R6374LmInitFormatLmmPduHdr (pServiceInfo, &pu1LmmPduEnd);

    /* Call BFD module to get the counter */
    if (pLmInfo->u1LmMethod == RFC6374_LM_METHOD_INFERED)
    {
        pPktSmInfo->u1PduType = RFC6374_LM_METHOD_INFERED;
#ifdef RFC6374STUB_WANTED
        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
        }
        else
#endif
        {
            i4RetVal = R6374GetBfdPktCount (pServiceInfo, &u4Txf, &u4Rxf);
            if (i4RetVal == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmInitXmitLmmPdu: Get BFD Counter Failed\r\n");
                RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
                return i4RetVal;
            }
        }
    }
    else if (pLmInfo->u1LmMethod == RFC6374_LM_METHOD_DIRECT)
    {
        pPktSmInfo->u1PduType = RFC6374_LM_METHOD_DIRECT;
        R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                            &u4RcvTnlTx, &u4RcvTnlRx);

        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            u4Txf = u4PwOrFwrdTnlTx;
            u4Rxf = u4PwOrFwrdTnlRx;
        }
        else
        {
            u4Txf = u4PwOrFwrdTnlTx;
        }
    }

    /* If Dyadic is enabled then Counter 1, Counter 3 and
     * Counter 4 has to be sent for other node calculation */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* A_Tx */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, u4Txf);

        /* B_Rx */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);

        /* A_TxP Counter of previous Query */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, pLmInfo->u4TxCounterf);

        /* B_RxP Counter of previous Query */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, pLmInfo->u4RxCounterb);
    }
    else
    {
        /* This is case when 64bit counter bit is not set */
        /* Counter1 */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, u4Txf);
        /* Counter 2 */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        /* Counter 3 */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        /* Counter 4 */
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);

        /* If fs6374SessionIntervalQueryStatus is enabled */

        if (pServiceInfo->LmInfo.u1SessQueryIntervalState ==
            RFC6374_SQI_INIT_STATE)
        {
            /* SQI TLV with zero value need to be appended to the buffer */
            RFC6374_PUT_1BYTE (pu1LmmPduEnd, RFC6374_SQI_TLV_TYPE);
            RFC6374_PUT_1BYTE (pu1LmmPduEnd, RFC6374_SQI_TLV_LENGTH);
            RFC6374_PUT_4BYTE (pu1LmmPduEnd, RFC6374_INIT_VAL);
        }
        else if (pServiceInfo->LmInfo.u1SessQueryIntervalState ==
                 RFC6374_SQI_ADJUSTED_STATE)
        {
            /* SQI TLV with adjusted value need to be appended to the buffer */
            RFC6374_PUT_1BYTE (pu1LmmPduEnd, RFC6374_SQI_TLV_TYPE);
            RFC6374_PUT_1BYTE (pu1LmmPduEnd, RFC6374_SQI_TLV_LENGTH);
            /* Get and fill the adjusted value from database */
            u4TimeInterval = (UINT4) (pServiceInfo->LmInfo.u2LmTimeInterval);
            RFC6374_PUT_4BYTE (pu1LmmPduEnd, u4TimeInterval);
        }
    }

    u2PduLength = (UINT2) (pu1LmmPduEnd - pu1LmmPduStart);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1LmmPduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmitLmmPdu: "
                     "Buffer copy operation failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    u4Duration = pLmInfo->u2LmTimeInterval;

    /* Timer of LM Query */
    if (R6374TmrStartTmr (RFC6374_LM_QUERY_TMR, pServiceInfo, u4Duration)
        != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmitLmmPdu: "
                     "Failure starting LM INTERVAL timer\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Dump the packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374LmInitXmitLmmPdu: [Sending] Dumping two-way"
                      " LMM PDU\r\n");

    /* Transmit the LMM over MPLS Path */
    i4RetVal = R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_LOSS);
    if (i4RetVal != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmitLmmPdu: LMM transmission to the"
                     " lower layer failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Decrement the no of LMM to be transmitted if
     * Pro-Active transmission is not desired */
    if (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND)
    {
        /* Decrement the number of LM Message/Running Count to
         * be transmitted */
        RFC6374_LM_DECR_RUNNING_COUNT (pLmInfo);
    }
    /* Increment the no of LMM transmitted count if
     * Pro-Active transmission is desired */
    if (pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE)
    {
        RFC6374_LM_INCR_PROACTIVE_TX_COUNT (pLmInfo);
    }
    /* Increment LMM Out Count */
    RFC6374_INCR_STATS_LMM_OUT (pServiceInfo);

    /* Increment LM Tx count to stop the transmission 
     * when response is not received for more than 4 Pdu */
    if ((pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE) ||
        (pLmInfo->u1LmType == RFC6374_LM_TYPE_TWO_WAY))
    {
        RFC6374_LM_INCR_LM_NO_RESP_TX_COUNT (pLmInfo);
    }

    RFC6374_TRC5 (RFC6374_CONTROL_PLANE_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                  "R6374LmInitXmitLmmPdu: LMM transmission to the lower "
                  "layer success for Sess Id : %d Lmm Tx %d Running Count %d "
                  "No Response Tx Count %d Proactive Tx Count %d \r\n",
                  pLmInfo->u4LmSessionId, pServiceInfo->Stats.u4StatsLmmOut,
                  pLmInfo->u2LmRunningCount, pLmInfo->u4LmNoRespTxCount,
                  pLmInfo->u4LmNoOfProTxCount);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from " "R6374LmInitXmitLmmPdu\r\n");

    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374LmInitFormatLmmPduHdr
 *
 * Description        : This rotuine is used to fill the Loss PDU.
 *
 * Input              : pServiceInfo - Service Related Config Info 
 *
 * Output(s)          : ppu1LmmPdu - Pointer to Pointer to the LMM PDU.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
R6374LmInitFormatLmmPduHdr (tR6374ServiceConfigTableEntry * pServiceInfo,
                            UINT1 **ppu1LmmPdu)
{
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    UINT1              *pu1LmmPdu = NULL;
    UINT1               au1ReservedVal[RFC6374_PDU_RESERVED_FIELD_LEN]
        = { RFC6374_INIT_VAL };
    UINT4               u4SeqNo = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1               u1VerFlag = RFC6374_INIT_VAL;
    UINT1               u1DflagOTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the " "R6374LmInitFormatLmmPduHdr\r\n");

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);
    pu1LmmPdu = *ppu1LmmPdu;

    if (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND)
    {
        u4SeqNo = (UINT4) ((pLmInfo->u2LmNoOfMessages + RFC6374_INDEX_ONE) -
                           pLmInfo->u2LmRunningCount) + RFC6374_INDEX_ONE;
    }
    else
    {
        u4SeqNo = pLmInfo->u4LmNoOfProTxCount + RFC6374_INDEX_ONE;
    }
    /* Version and flag(R|T|0|0) */
    u1VerFlag = RFC6374_PDU_VERSION | RFC6374_PDU_QUERY_FLAG;
    RFC6374_PUT_1BYTE (pu1LmmPdu, u1VerFlag);

    /* Control Code */
    if (pServiceInfo->u1DyadicMeasurement == RFC6374_DYADIC_MEAS_ENABLE)
    {
        /* As for Dyadic Response is not sent so 
         * Response not required */
        RFC6374_PUT_1BYTE (pu1LmmPdu, RFC6374_CTRL_CODE_RESP_NOT_REQ);
    }
    else
    {
        /* Dyadic is not enabled and measurement is start for two way */
        RFC6374_PUT_1BYTE (pu1LmmPdu, RFC6374_CTRL_CODE_RESP_INBAND_REQ);
    }

    /* Loss Measurement PDU Length  */
    u2PduLength = RFC6374_LM_PDU_MSG_LEN;
    if (pServiceInfo->LmInfo.u1SessQueryIntervalState > RFC6374_SQI_RESET_STATE)
    {
        u2PduLength = (UINT2) (u2PduLength + RFC6374_SESS_QUERY_INT_TLV_LEN);
    }
    RFC6374_PUT_2BYTE (pu1LmmPdu, u2PduLength);

    /* DFlag OTF  */
    u1DflagOTF = RFC6374_LM_32BIT_COUNTERS | RFC6374_LM_PKT_COUNT;

    u1DflagOTF = (UINT1) (u1DflagOTF << RFC6374_VAL_4);
    u1DflagOTF = u1DflagOTF | pLmInfo->u1LmQTSFormat;

    RFC6374_PUT_1BYTE (pu1LmmPdu, u1DflagOTF);

    /* Reserved Bits */
    RFC6374_PUT_NBYTE (pu1LmmPdu, au1ReservedVal,
                       RFC6374_PDU_RESERVED_FIELD_LEN);

    /* Session Idntifier and DS Traffic Class */
    RFC6374_PUT_4BYTE (pu1LmmPdu, pLmInfo->u4LmSessionId);

    if (pLmInfo->u1LmQTSFormat == RFC6374_TS_FORMAT_SEQ_NUM)
    {
        RFC6374_PUT_4BYTE (pu1LmmPdu, RFC6374_INIT_VAL);
        RFC6374_PUT_4BYTE (pu1LmmPdu, u4SeqNo);
    }
    else
    {
        /* Origin Time Stamp */
        RFC6374_PUT_4BYTE (pu1LmmPdu, pLmInfo->LmOriginTimeStamp.u4Seconds);
        RFC6374_PUT_4BYTE (pu1LmmPdu, pLmInfo->LmOriginTimeStamp.u4NanoSeconds);
    }
    *ppu1LmmPdu = pu1LmmPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from " "R6374LmInitFormatLmmPduHdr\r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374LmInitStopLmTransaction
 *
 * Description        : This routine is used to stop the on going LM transaction
 *
 * Input(s)           : pPktSmInfo - pointer to the Service Related Config
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
R6374LmInitStopLmTransaction (tR6374ServiceConfigTableEntry * pServiceInfo)
{
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374LmStatsTableEntry *pLmStatsTableInfo = NULL;
    tR6374NotifyInfo    R6374LmNotify;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitStopLmTransaction\r\n");

    RFC6374_MEMSET (&R6374LmNotify, RFC6374_INIT_VAL,
                    sizeof (tR6374NotifyInfo));

    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);

    /* Stop LM Query Timer */
    R6374TmrStopTmr (RFC6374_LM_QUERY_TMR, pServiceInfo);

    if (pLmInfo->u1LmType == RFC6374_LM_TYPE_TWO_WAY)
    {
        /* Set Measurement Status as Competed 
         * Calculate if any packet loss due to timeoue */
        pLmStatsTableInfo = R6374UtilGetLmStatsTableInfo (pServiceInfo,
                                                          pLmInfo->
                                                          u4LmSessionId);
        if (pLmStatsTableInfo == NULL)
        {
            RFC6374_TRC1 (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                          RFC6374_DEFAULT_CONTEXT_ID,
                          "R6374LmInitStopLmTransaction: Get LM Stats failed "
                          "for Session Id : %d \r\n", pLmInfo->u4LmSessionId);
        }
        else
        {
            if ((pLmInfo->b1LmTestStatus & RFC6374_TEST_ABORTED) ==
                RFC6374_TEST_ABORTED)
            {
                /* Measurement is Aborted by user or due to Path down */
                pLmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_ABORTED;
            }
            else
            {
                /* Measurement Completed */
                pLmStatsTableInfo->u1MeasurementOngoing =
                    RFC6374_MEASUREMENT_COMPLETE;
            }

            if (pLmStatsTableInfo->u1LossMode == RFC6374_LM_MODE_ONDEMAND)
            {
                /* Actual LMM Sent */
                pLmStatsTableInfo->u4LMMSent =
                    (UINT4) (pLmInfo->u2LmNoOfMessages + RFC6374_INDEX_ONE -
                             pLmInfo->u2LmRunningCount);
            }
            /* No Of Error Packet Recevied */
            pLmStatsTableInfo->u4NoOfErroredRespRcvd =
                pLmInfo->u4RxLmPacketLoss;
        }
    }
    /* Decrement Global LM Running Count */
    if (pLmInfo->u1LmStatus == RFC6374_TX_STATUS_NOT_READY)
    {
        RFC6374_LM_DECR_SESSION_COUNT ();

        /* Send Test Abort trap for the below cases
         * 1. Test aborted by Administrator
         * 2. LSP/PW down : On-Demand - Based on Running count  
         proactive - Based on Previous mode when
         mode is changed from on-demand 
         to proactive to stop the test 
         - Based on LM mode when test is
         aborted due to LSP/PW down*/

        if (((pLmInfo->b1LmTestStatus & RFC6374_TEST_ABORTED)
             == RFC6374_TEST_ABORTED) ||
            (pLmInfo->u2LmRunningCount != RFC6374_INIT_VAL) ||
            (pLmInfo->u1PrevLmMode == RFC6374_LM_MODE_PROACTIVE) ||
            (pLmInfo->u1LmMode == RFC6374_LM_MODE_PROACTIVE))
        {
            /* Test Abort Trap */
            R6374LmNotify.u1TrapType = RFC6374_TEST_ABORT_TRAP;
            RFC6374_STRNCPY (&R6374LmNotify.au1ServiceName,
                             pServiceInfo->au1ServiceName,
                             RFC6374_STRLEN (pServiceInfo->au1ServiceName));
            R6374SendNotification (&R6374LmNotify);

            RFC6374_TRC2 (RFC6374_CRITICAL_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                          "Loss Measurement test aborted for "
                          "Service name : %s Session Id : %d "
                          "\r\n", pServiceInfo->au1ServiceName,
                          pLmInfo->u4LmSessionId);
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) RFC6374_SYSLOG_ID,
                          "Loss Measurement test aborted for Service name : %s Session Id : %d "
                          "\r\n", pServiceInfo->au1ServiceName,
                          pLmInfo->u4LmSessionId));
#endif

            if (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND)
            {
                pLmInfo->u1PrevLmMode = RFC6374_LM_MODE_ONDEMAND;
            }
        }

        /* Reset test abort status as false */
        pLmInfo->b1LmTestStatus &= ~RFC6374_TEST_ABORTED;
    }

    /* Revert Back the Tx Status */
    pLmInfo->u1LmStatus = RFC6374_TX_STATUS_READY;

    /* Initiate Seq Counter */
    RFC6374_LM_RESET_SEQ_COUNT (pLmInfo);

    /* Reset Values */
    R6374LmResetVar (pLmInfo);

    /* Reset loss amd tx count */
    RFC6374_LM_RESET_PACKETLOSS (pLmInfo);
    RFC6374_LM_RESET_RESP_TX_COUNT (pLmInfo);

    /* Reset SQI TLV */
    if (pServiceInfo->u1SessIntQueryStatus == CLI_RFC6374_SESS_INT_QUERY_ENABLE)
    {
        pServiceInfo->LmInfo.u1SessQueryIntervalState = RFC6374_SQI_RESET_STATE;
        /* Restore the adjusted interval with Configured interval */
        pServiceInfo->LmInfo.u2LmTimeInterval =
            pServiceInfo->LmInfo.u2LmConfiguredTimeInterval;

    }

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from " "R6374LmInitStopLmTransaction\r\n");
    return;
}

/*******************************************************************************
 * Function Name      : R6374LmInitXmit1LmPdu
 *
 * Description        : This routine formats and transmits the 1LM PDU - One way
 *
 * Input(s)           : pPktSmInfo - Pointer the Service Related Config
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RFC6374_SUCCESS / RFC6374_FAILURE
 ******************************************************************************/
PUBLIC INT4
R6374LmInitXmit1LmPdu (tR6374PktSmInfo * pPktSmInfo)
{
    tR6374LmInfoTableEntry *pLmInfo = NULL;
    tR6374BufChainHeader *pBuf = NULL;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;
    INT4                i4RetVal = RFC6374_SUCCESS;
    UINT4               u4Duration = RFC6374_INIT_VAL;
    UINT4               u4Txf = RFC6374_INIT_VAL;
    UINT4               u4Rxf = RFC6374_INIT_VAL;
    UINT4               u4PduSize = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlTx = RFC6374_INIT_VAL;
    UINT4               u4PwOrFwrdTnlRx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlTx = RFC6374_INIT_VAL;
    UINT4               u4RcvTnlRx = RFC6374_INIT_VAL;
    UINT2               u2PduLength = RFC6374_INIT_VAL;
    UINT1              *pu1Lm1PduStart = NULL;
    UINT1              *pu1Lm1PduEnd = NULL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitXmit1LmPdu\r\n");

    pServiceInfo = RFC6374_GET_SERVICE_FROM_PDUSM (pPktSmInfo);
    pLmInfo = RFC6374_GET_LM_INFO_FROM_SERVICE (pServiceInfo);

    /*Check if Number Of Message to be transmitted have been reached */
    if ((pLmInfo->u2LmRunningCount == RFC6374_INIT_VAL) &&
        (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND))

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmit1LmPdu: "
                     "Stopping Lm Transaction as total number of 1LM/LMM"
                     "have been transmitted\r\n");

        /*Stop Lm Transaction */
        R6374LmInitStopLmTransaction (pServiceInfo);

        return RFC6374_SUCCESS;
    }

    /* Add origin Time to LM info */
    /* Get Current Time */
    R6374UtilGetCurrentTime (&pLmInfo->LmOriginTimeStamp,
                             pLmInfo->u1LmQTSFormat);

    /* Allocate CRU Buffer */
    pBuf = RFC6374_ALLOC_CRU_BUF (RFC6374_MAX_PDU_SIZE, RFC6374_INIT_VAL);
    if (pBuf == NULL)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmit1LmPdu: "
                     "Buffer allocation failed\r\n");
        RFC6374_INCR_BUFFER_FAILURE_COUNT ();
        return RFC6374_FAILURE;
    }

    RFC6374_MEMSET (RFC6374_PDU, RFC6374_INIT_VAL, RFC6374_PDU_SIZE);
    pu1Lm1PduStart = RFC6374_PDU;
    pu1Lm1PduEnd = RFC6374_PDU;

    /* Format the  Lmm PDU header */
    R6374LmInitFormat1LmPduHdr (pLmInfo, &pu1Lm1PduEnd);

    /* Call BFD module to get the counter */
    if (pLmInfo->u1LmMethod == RFC6374_LM_METHOD_INFERED)
    {
        pPktSmInfo->u1PduType = RFC6374_LM_METHOD_INFERED;
#ifdef RFC6374STUB_WANTED
        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            R6374StubGetBfdCounter (&u4Txf, &u4Rxf);
        }
        else
#endif
        {
            i4RetVal = R6374GetBfdPktCount (pServiceInfo, &u4Txf, &u4Rxf);
            if (i4RetVal == RFC6374_FAILURE)
            {
                RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC |
                             RFC6374_ALL_FAILURE_TRC,
                             RFC6374_DEFAULT_CONTEXT_ID,
                             "R6374LmInitXmit1LmPdu: "
                             "Get BFD Counter Failed\r\n");
                RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
                return i4RetVal;
            }
        }
    }
    else if (pLmInfo->u1LmMethod == RFC6374_LM_METHOD_DIRECT)
    {
        pPktSmInfo->u1PduType = RFC6374_LM_METHOD_DIRECT;
        R6374GetMplsPktCnt (pServiceInfo, &u4PwOrFwrdTnlTx, &u4PwOrFwrdTnlRx,
                            &u4RcvTnlTx, &u4RcvTnlRx);

        if (pServiceInfo->R6374MplsParams.u1MplsPathType ==
            MPLS_PATH_TYPE_TUNNEL)
        {
            u4Txf = u4PwOrFwrdTnlTx;
            u4Rxf = u4PwOrFwrdTnlRx;
        }
        else
        {
            u4Txf = u4PwOrFwrdTnlTx;
        }
    }

    /* this is case when 64bit counter bit is not set */
    /* Counter 1 */
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, u4Txf);
    /* COunter 2 */
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);
    /* Counter 3 */
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);
    /* Counter 4 */
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);
    RFC6374_PUT_4BYTE (pu1Lm1PduEnd, RFC6374_INIT_VAL);

    u2PduLength = (UINT2) (pu1Lm1PduEnd - pu1Lm1PduStart);

    /* Copying PDU over CRU buffer */
    if (RFC6374_COPY_OVER_CRU_BUF (pBuf, pu1Lm1PduStart, RFC6374_INIT_VAL,
                                   (UINT4) (u2PduLength)) !=
        RFC6374_CRU_SUCCESS)

    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmit1LmPdu: "
                     "Buffer copy operation failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    u4Duration = pLmInfo->u2LmTimeInterval;

    /* Start LM Query Timer */
    if (R6374TmrStartTmr (RFC6374_LM_QUERY_TMR, pServiceInfo, u4Duration)
        != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_CONTROL_PLANE_TRC | RFC6374_ALL_FAILURE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID,
                     "R6374LmInitXmit1LmPdu: "
                     "Failure starting LM INTERVAL timer\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Dump the Packet */
    u4PduSize = RFC6374_GET_CRU_VALID_BYTE_COUNT (pBuf);
    RFC6374_PKT_DUMP (RFC6374_PKT_DUMP_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                      pBuf, u4PduSize,
                      "R6374LmInitXmit1LmPdu: [Sending] Dumping one-way"
                      " LMM PDU\r\n");

    /* Transmit the LMM over MPLS Path */
    i4RetVal = R6374MplsTxPkt (pBuf, pPktSmInfo, RFC6374_MEASUREMENT_TYPE_LOSS);
    if (i4RetVal != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC | RFC6374_CONTROL_PLANE_TRC,
                     RFC6374_DEFAULT_CONTEXT_ID, "R6374LmInitXmit1LmPdu: "
                     "1LM transmission to the lower layer failed\r\n");
        RFC6374_RELEASE_CRU_BUF (pBuf, RFC6374_FALSE);
        return RFC6374_FAILURE;
    }

    /* Decrement the no of LMMs to be transmitted if
     * infinite transmission is not desired */
    if (pLmInfo->u1LmMode == RFC6374_LM_MODE_ONDEMAND)

    {
        /* Decrement the number of LM Message to be transmitted */
        RFC6374_LM_DECR_RUNNING_COUNT (pLmInfo);
    }
    /* Increment 1LM OUT */
    RFC6374_INCR_STATS_1LM_OUT (pServiceInfo);

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmInitXmit1LmPdu\r\n");
    return RFC6374_SUCCESS;
}

/*******************************************************************************
 * Function Name      : R6374LmInitFormat1LmPduHdr
 *
 * Description        : This rotuine is used to fill the 1LM PDU
 *
 * Input              : pLmInfo - POinter to the Loss measurement Config
 *
 * Output(s)          : ppu1lLmPdu - Pointer to Pointer to the 1LM PDU.
 *
 * Return Value(s)    : None
 ******************************************************************************/
PUBLIC VOID
R6374LmInitFormat1LmPduHdr (tR6374LmInfoTableEntry * pLmInfo, UINT1 **ppu1LmPdu)
{
    UINT1              *pu1OneLmPdu = NULL;
    UINT1               au1ReservedVal[RFC6374_PDU_RESERVED_FIELD_LEN]
        = { RFC6374_INIT_VAL };
    UINT1               u1VerFlag = RFC6374_INIT_VAL;
    UINT1               u1DflagOTF = RFC6374_INIT_VAL;

    RFC6374_TRC (RFC6374_FN_ENTRY_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Entering the R6374LmInitFormat1LmPduHdr\r\n");

    pu1OneLmPdu = *ppu1LmPdu;

    /* Version and flag(R|T|0|0) */
    u1VerFlag = RFC6374_PDU_VERSION | RFC6374_PDU_QUERY_FLAG;
    RFC6374_PUT_1BYTE (pu1OneLmPdu, u1VerFlag);

    /* Control Code */
    RFC6374_PUT_1BYTE (pu1OneLmPdu, RFC6374_CTRL_CODE_RESP_NOT_REQ);

    /* Loss Measurement PDU Length  */
    RFC6374_PUT_2BYTE (pu1OneLmPdu, RFC6374_LM_PDU_MSG_LEN);

    /* DFlag OTF  */
    u1DflagOTF = RFC6374_LM_32BIT_COUNTERS | RFC6374_LM_PKT_COUNT;

    u1DflagOTF = (UINT1) (u1DflagOTF << RFC6374_VAL_4);
    u1DflagOTF = u1DflagOTF | pLmInfo->u1LmQTSFormat;

    RFC6374_PUT_1BYTE (pu1OneLmPdu, u1DflagOTF);

    /* Reserved Bits */
    RFC6374_PUT_NBYTE (pu1OneLmPdu, au1ReservedVal,
                       RFC6374_PDU_RESERVED_FIELD_LEN);

    /* Session Idntifier and DS Traffic Class */
    RFC6374_PUT_4BYTE (pu1OneLmPdu, pLmInfo->u4LmSessionId);

    /* Origin Time Stamp */
    RFC6374_PUT_4BYTE (pu1OneLmPdu, pLmInfo->LmOriginTimeStamp.u4Seconds);
    RFC6374_PUT_4BYTE (pu1OneLmPdu, pLmInfo->LmOriginTimeStamp.u4NanoSeconds);

    *ppu1LmPdu = pu1OneLmPdu;

    RFC6374_TRC (RFC6374_FN_EXIT_TRC, RFC6374_DEFAULT_CONTEXT_ID,
                 " Exiting from R6374LmInitFormat1LmPduHdr\r\n");
    return;
}

PUBLIC INT4
R6374LmResetVar (tR6374LmInfoTableEntry * pLmInfo)
{
    pLmInfo->u4PreTxCounterf = RFC6374_RESET;
    pLmInfo->u4PreRxCounterf = RFC6374_RESET;
    pLmInfo->u4PreTxCounterb = RFC6374_RESET;
    pLmInfo->u4PreRxCounterb = RFC6374_RESET;
    pLmInfo->u4TxCounterf = RFC6374_RESET;
    pLmInfo->u4RxCounterb = RFC6374_RESET;
    return RFC6374_SUCCESS;
}
#endif
