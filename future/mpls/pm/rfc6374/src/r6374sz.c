/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374sz.c,v 1.1 2016/07/06 10:51:57 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/
#ifndef _R6374SZ_C
#define _R6374SZ_C
#include "r6374inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
R6374SizingMemCreateMemPools ()
{
    UINT4               u4RetVal = MEM_SUCCESS;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RFC6374_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal =
            MemCreateMemPool (FsR6374SizingParams[i4SizingId].u4StructSize,
                              FsR6374SizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(R6374MemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            R6374SizingMemDeleteMemPools ();
            return RFC6374_FAILURE;
        }
    }
    return RFC6374_SUCCESS;
}

INT4
R6374SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsR6374SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, R6374MemPoolIds);
    return RFC6374_SUCCESS;
}

VOID
R6374SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RFC6374_MAX_SIZING_ID; i4SizingId++)
    {
        if (R6374MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (R6374MemPoolIds[i4SizingId]);
            R6374MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
#endif
