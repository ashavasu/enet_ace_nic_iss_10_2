/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: r6374que.c,v 1.2 2017/07/25 12:00:37 siva Exp $
 *
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#ifndef _R6374QUE_C
#define _R6374QUE_C
#include "r6374inc.h"

/****************************************************************************
*                                                                           *
* Function     : R6374QueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
R6374QueProcessMsgs ()
{
    tR6374QMsg         *pR6374QueMsg = NULL;

    tR6374PktSmInfo     PduSmInfo;
    tR6374ServiceConfigTableEntry *pServiceInfo = NULL;

    RFC6374_MEMSET (&PduSmInfo, RFC6374_INIT_VAL, sizeof (PduSmInfo));

    while (OsixQueRecv (RFC6374_QUEUE_ID (),
                        (UINT1 *) &pR6374QueMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == RFC6374_SUCCESS)
    {
        switch (pR6374QueMsg->u1MsgType)
        {
            case RFC6374_CREATE_CONTEXT_MSG:
                break;

            case RFC6374_DELETE_CONTEXT_MSG:
                break;

            case RFC6374_RX_PDU_MSG:

                /* Message from MPLS-RTR to recive the PDU */
                if (R6374ProcessMplsPdu (pR6374QueMsg->pR6374Buf,
                                         pR6374QueMsg->u4IfIndex) !=
                    RFC6374_SUCCESS)
                {
                    RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                                 pR6374QueMsg->u4ContextId,
                                 "R6374ProcessMplsPdu()" " returns failure");
                }

                /* Release the received CRU buffer. */
                RFC6374_RELEASE_CRU_BUF (pR6374QueMsg->pR6374Buf, 0);
                break;

            case RFC6374_LM_START_SESSION:
            case RFC6374_LM_STOP_SESSION:
            {
                pServiceInfo =
                    R6374UtilGetServiceFromName (pR6374QueMsg->u4ContextId,
                                                 pR6374QueMsg->au1ServiceName);
                if (pServiceInfo == NULL)
                {
                    return;
                }
                PduSmInfo.pServiceConfigInfo = pServiceInfo;
                R6374LmInitiator (&PduSmInfo, pR6374QueMsg->u1MsgType);
            }
                break;
            case RFC6374_DM_START_SESSION:
            case RFC6374_DM_STOP_SESSION:
            {
                pServiceInfo =
                    R6374UtilGetServiceFromName (pR6374QueMsg->u4ContextId,
                                                 pR6374QueMsg->au1ServiceName);
                if (pServiceInfo == NULL)
                {
                    return;
                }

                PduSmInfo.pServiceConfigInfo = pServiceInfo;
                R6374DmInitiator (&PduSmInfo, pR6374QueMsg->u1MsgType);
            }
                break;
            case RFC6374_MPLS_PATH_STATUS_CHG:
            {
                R6374UtilProcessPathStatus (pR6374QueMsg);
            }
                break;
            case RFC6374_LMDM_START_SESSION:
            case RFC6374_LMDM_STOP_SESSION:
            {
                pServiceInfo =
                    R6374UtilGetServiceFromName (pR6374QueMsg->u4ContextId,
                                                 pR6374QueMsg->au1ServiceName);
                if (pServiceInfo == NULL)
                {
                    return;
                }

                PduSmInfo.pServiceConfigInfo = pServiceInfo;
                R6374LmDmInitiator (&PduSmInfo, pR6374QueMsg->u1MsgType);
            }
                break;

            default:
                RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                             pR6374QueMsg->u4ContextId,
                             "In R6374QueProcessMsgs()" " Default case");
                break;
        }

        /* Releasing the memory after dequeue */
        if (RFC6374_FREE_MEM_BLOCK (RFC6374_QMSG_POOL (),
                                    (UINT1 *) pR6374QueMsg) == MEM_FAILURE)
        {
            RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                         pR6374QueMsg->u4ContextId, "In R6374QueProcessMsgs()"
                         " Releasing memory fails");
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : R6374QueEnqMsg                                              *
*                                                                           *
* Description  : This function is to Enqueue the mesage and send an event   * 
*                to the module                                              *
*                                                                           *
* Input        : pMsg - Pointer to the message                              *
*                pu1ErrorCode - pointer to the error code variable where the*
*                               error code is set on failure.               *
*                                                                           *
* Output       : pu1ErrorCode - updated pointer with error code.            *
*                                                                           *
* Returns      : RFC6374_SUCCESS/RFC6374_FAILURE                            *
*                                                                           *
*****************************************************************************/
INT4
R6374QueEnqMsg (tR6374QMsg * pMsg)
{
    if (OsixQueSend (RFC6374_QUEUE_ID (), (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == RFC6374_FAILURE)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                     pMsg->u4ContextId, "In R6374QueEnqMsg(),"
                     " OsixQueSend() fails");
        return RFC6374_FAILURE;
    }

    if (OsixEvtSend (RFC6374_TASK_ID (), RFC6374_QMSG_EVENT) != RFC6374_SUCCESS)
    {
        RFC6374_TRC (RFC6374_ALL_FAILURE_TRC,
                     pMsg->u4ContextId, "In R6374QueEnqMsg(),"
                     " OsixEvtSend() fails");
        return RFC6374_FAILURE;
    }

    return RFC6374_SUCCESS;
}

#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  r6374que.c                      */
/*-----------------------------------------------------------------------*/
