#/********************************************************************
#* Copyright (C) 2006 Aricent Inc . All Rights Reserved
#*
#* $Id: make.h,v 1.2 2014/08/25 12:19:55 siva Exp $
#*
#********************************************************************/

include ../../../LR/make.h
include ../../../LR/make.rule
include ../../make.h
include ../../make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
MPLS_INCL_DIR           = $(BASE_DIR)/mpls/mplsinc
PROJECT_TEST_DIR        = ${L3VPN}/test
PROJECT_INCLUDE_DIR     = ${L3VPN}/inc
PROJECT_OBJECT_DIR      = ${L3VPN}/obj
MPLS_L3VPN_DIR          = $(BASE_DIR)/mpls/l3vpn/inc
MPLS_RTR_DIR            = $(BASE_DIR)/mpls/mplsrtr/inc
MPLS_DB_DIR             = $(BASE_DIR)/mpls/mplsdb


L3VPN_TEST_BASE_DIR  = ${BASE_DIR}/mpls/l3vpn/test
L3VPN_TEST_SRC_DIR   = ${L3VPN_TEST_BASE_DIR}/src
L3VPN_TEST_INC_DIR   = ${L3VPN_TEST_BASE_DIR}/inc
L3VPN_TEST_OBJ_DIR   = ${L3VPN_TEST_BASE_DIR}/obj

L3VPN_TEST_INCLUDES  = -I$(L3VPN_TEST_INC_DIR) \
                     -I$(PROJECT_INCLUDE_DIR) \
                     -I$(MPLS_INCL_DIR) \
                     -I$(MPLS_L3VPN_DIR) \
                     -I$(MPLS_RTR_DIR) \
		     -I$(MPLS_DB_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h                  #
##########################################################################
