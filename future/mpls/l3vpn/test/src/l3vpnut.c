/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnut.c,v 1.2 2014/08/25 12:19:56 siva Exp $
 *
 ********************************************************************/
#include "lr.h"
#include "cli.h"

#include "fssnmp.h"
#include "params.h"
#include "mplsapi.h"
#include "mplcmndb.h"
#include "mplsutil.h"
#include "mplslsr.h"
#include "inmgrex.h"
#include "mplsftn.h"
#include "mplsred.h"


#include "l3vpndefn.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpntmr.h"
#include "l3vpntrc.h"
#include "l3vpncli.h"
#include "l3vpnlwg.h"
#include "l3vpnprotg.h"
#include "iss.h"
#include "l3vpnprot.h"
#include "params.h"
#include "l3vpnsz.h"
#include "l3vpndefg.h"
#include "mpl3vpncmn.h"
#include "l3vpnextn.h"
#include "vcm.h"
#include "../../../cli/inc/cliport.h"
#include "../../../cli/inc/cliconst.h"
#include "../../../cli/inc/climdcmd.h"
#include "trie.h"

#define VRF_NAME "cust-1"
#define L3VPN_MAX_UT_CASES 10


VOID L3VpnExecAllUTCases(tCliHandle CliHandle);

extern UINT4  u4TotPassedCases;
extern UINT4  u4TotFailedCases;
extern tCliContext* CliGetContext(VOID);


/** Local to this file ***/
#define L3VPN_DEF_VRF_NAME  "cust-1"
#define L3VPN_DEF_VRF_VPN_ID "Test"
#define L3VPN_DEF_VRF_DESCRIPTION "Aricent VRF"

/** configuration Related variables **/
UINT1 u1IngressNextHop[L3VPN_BGP_MAX_ADDRESS_LEN]={0x28,0x00,0x00,0x00};
UINT1 u1EgressNextHop[L3VPN_BGP_MAX_ADDRESS_LEN]={0x23,0x00,0x00,0x14};
UINT1 u1IngressDst[L3VPN_BGP_MAX_ADDRESS_LEN]={0x23,0x00,0x00,14};
#define L3VPN_DEF_EGRESS_LABEL 500
#define L3VPN_DEF_INGRESS_LABEL 500
#define L3VPN_NXT_HOP_AS 1
#define L3VPN_INGRESS_METRIC 100
#define L3VPN_INGRESS_PREFIX_LEN 24
#define L3VPN_DEF_CXT_ID 1
#define L3VPN_DEF_EGRESS_IF_INDEX 1
#define L3VPN_DEF_INGRESS_IF_INDEX 10



/** Variables to save Address for PoolIds **/
tL3VpnBgpRouteLabelEntry * pL3VpnBgpRouteLabelEntry[MAX_L3VPN_EGRESS_ROUTE_TABLE_ENTRIES];
tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry[MAX_L3VPN_ROUTE_TABLE_ENTRIES];


UINT4 L3vpnCreateMplsVrf();
UINT4 L3vpnCreateMplsVrfRd();
UINT4 L3vpnCreateMplsRd();
UINT4 L3vpnDeleteMplsVrf();
UINT4 L3vpnDeleteMplsRd();


VOID L3vpnCreateIpVrf();
VOID L3vpnDeleteIpVrf();

VOID L3vpnCreateRt();
VOID L3vpnDeleteRt();

VOID L3vpnCreateIngressStaticBinding();
VOID L3vpnDeleteIngressStaticBinding();


UINT4 L3vpnFreeBgpRouteLabelEntry();
UINT4 L3vpnAllocateBgpRouteLabelEntry();
UINT4 L3vpnFillDefaultEgressRouteDel(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);
UINT4 L3vpnFillDefaultEgressPerVrfAdd(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);
UINT4 L3vpnFillDefaultEgressPerRouteAdd(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);
UINT4 L3vpnFillDefaultIngressRouteAdd(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);
UINT4 L3vpnFillDefaultIngressRouteDel(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);
UINT4 L3vpnAllocateVrfRteEntry();
UINT4 L3vpnFreeVrfRteEntry();
tL3vpnMplsL3VpnVrfEntry* L3vpnGetVrfEntry();
VOID L3vpnFillDefaultVrfEntry(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry,
                tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry);


VOID
L3vpnFillDefaultRtEntry(tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry,
                tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry);
VOID L3vpnCreateIngressRoute_1();
VOID L3vpnCreateIngressRoute_2();
VOID L3vpnCreateIngressRoute_3();

#define VRF_NAME "cust-1"
#define L3VPN_MAX_UT_CASES 10


VOID
L3VpnExecAllUTCases(tCliHandle CliHandle);

VOID
L3VpnUtNotifyRouteParams(UINT4 u4VrfId, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam);

VOID
L3VpnUtNotifyLspStatusChange (UINT4 u4LspLabel, UINT1 u1LspStatus);

VOID
L3VpnUtNotifyVrfAdminStatusChange(UINT4 u4VrfId, UINT1 u1VrfStatus);


UINT4
L3VpnUtExhaustQMsgPool(VOID);

UINT4
L3VpnFreeQMem(VOID);

UINT4
L3VpnUtCreateVrf(VOID);

UINT4
L3VpnDeleteVrf(VOID);

UINT4
L3VpnUtAddIngressRoute(VOID);

UINT4
L3VpnUtLspCleanUp(VOID);

VOID
L3VpnUtMapInterfaceToVrf(UINT4 u4IfIndex, BOOL1 bIsNosh);

VOID
L3VpnUtUnMapInterfaceToVrf(UINT4);

VOID
L3VpnUtAddRtToVrf(VOID);

VOID
L3VpnAddRouteAtIngress(UINT4 u4Dest);

VOID
L3VpnDeleteRouteAtIngress(UINT4 u4Dest);

VOID
L3VpnAddRtToVrf();

VOID
L3VpnDeleteRtFromVrf();

VOID
L3VpnUtAddRdToVrf ();

VOID L3vpnAddPortToVrf();
VOID L3vpnDelPortFromVrf();


VOID
L3VpnUtFillMsgQueue();

VOID
L3VpnUtFreeMsgQueue();

VOID
L3VpnUtExhaustIfConfMemPool();

VOID
L3VpnUtFreeIfConfMemPool();

VOID
L3VpnUtExhaustVcmRegPool();

VOID
L3VpnUtFreeVcmRegPool();


UINT1    *pu1TmpMsg[MAX_L3VPN_Q_MSG];
extern tL3vpnGlobals gL3vpnGlobals;
#define  L3VPN_UTMUT_EXCL_SEM_NAME          (const UINT1 *) "L3UT"

tL3vpnMplsL3VpnIfConfEntry    *pu1IfConfEntry[MAX_L3VPN_IF_CONF_TABLE_ENTRIES];

tL3vpnMplsL3VpnVrfPerfEntry        *pu1PerfEntry[MAX_L3VPN_PERF_TABLE_ENTRIES];
tL3vpnMplsL3VpnVrfSecEntry         *pu1SecEntry[MAX_L3VPN_SEC_TABLE_ENTRIES];

tMplsL3VpnRegInfo MplsL3VpnRegInfo;

VOID Bgp4MplsL3VpnNotifyRouteParams (UINT4 u4VrfId, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam);
VOID Bgp4MplsL3VpnNotifyLspStatusChange (UINT4 u4LspLabel, UINT1 u1LspStatus);
VOID Bgp4MplsL3vpnNotifyVrfAdminStatusChange (UINT4 u4VrfId, UINT1 u1VrfStatus);

extern VOID L3VpnTrieDeInit (VOID *pDelParams);

tSNMP_VAR_BIND     *pVarBindPtr[MAX_SNMP_VARBIND_BLOCKS];

VOID L3vpnAllocateVarBindPool();
VOID L3vpnClearVarBindPool();

VOID L3vpnEgressNpapiSuccess();
VOID L3vpnEgressNpapiRemoveSuccess();


VOID L3vpnExecUnitTest (tCliHandle CliHandle, INT4 i4TestId)
{

    UINT4               u4OsixRetVal = OSIX_SUCCESS;
    INT4                i4RetVal = OSIX_SUCCESS;    
    UINT4               u4RetVal = OSIX_SUCCESS;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE VrfName;
    UINT4               u4RtIndex = 0;
    UINT4               u4RtType = 0;       
    UINT4               u4Count = 0;
      
    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET(&VrfName, 0, sizeof(tSNMP_OCTET_STRING_TYPE));


    MplsL3VpnRegInfo.pBgp4MplsL3VpnNotifyRouteParams=Bgp4MplsL3VpnNotifyRouteParams;
    MplsL3VpnRegInfo.pBgp4MplsL3VpnNotifyLspStatusChange=Bgp4MplsL3VpnNotifyLspStatusChange;
    MplsL3VpnRegInfo.pBgp4MplsL3vpnNotifyVrfAdminStatusChange=Bgp4MplsL3vpnNotifyVrfAdminStatusChange;

    MplsL3VpnRegInfo.u2InfoMask=0xff;


  

    switch(i4TestId)
    {
    /* VCM must be configured before RT row is created */
        case 1: /* replaced by 115*/
            {
		    UINT4 u4Prefix   = 0;
		    UINT4 u4LspLabel = 0;
		    BOOL1 u1Status   = 0;
		    UINT4 u4NextHop  = 0;

		    u4NextHop=(*(UINT4 *)(u1IngressNextHop));

		    L3vpnCreateIngressStaticBinding();

		    u1Status=FALSE;
		    L3vpnMainTaskLock();
		    u4OsixRetVal=MplsL3VpnBgp4LspUsageStatusUpdate(u4NextHop,u4LspLabel,u1Status);
		    L3vpnMainTaskUnLock();
		    if(u4OsixRetVal!=OSIX_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    L3vpnDeleteIngressStaticBinding();
			    break;
		    }
		    L3vpnDeleteIngressStaticBinding();

	    }
            break;
        case 2: /** Replaced by 128**/
            {
		    tL3VpnNonTeLspEventInfo L3VpnNonTeLspEventInfo;
		    UINT4 u4NextHop=0;

		    u4NextHop=(*(UINT4 *)(u1IngressNextHop));
		    u4NextHop = OSIX_HTONL (u4NextHop);


		    L3VpnNonTeLspEventInfo.u4Label=L3VPN_DEF_INGRESS_LABEL;
		    L3VpnNonTeLspEventInfo.u4Prefix=u4NextHop;
		    L3VpnNonTeLspEventInfo.u4EventType=L3VPN_MPLS_LSP_UP;

		    L3vpnCreateIngressStaticBinding();

		    MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);

		    u4OsixRetVal=L3VpnProcessNonTeLspEvent(&L3VpnNonTeLspEventInfo);
		    if(u4OsixRetVal!=L3VPN_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }
		    L3VpnNonTeLspEventInfo.u4EventType=L3VPN_MPLS_LSP_DOWN;
		    u4OsixRetVal=L3VpnProcessNonTeLspEvent(&L3VpnNonTeLspEventInfo);
		    if(u4OsixRetVal!=L3VPN_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }
		    MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);

		    tFtnEntry          *pFtnEntry=NULL;
		    UINT1              u1HwStatus=0;
		    MPLS_CMN_LOCK ();
		    pFtnEntry = MplsSignalGetFtnTableEntry (u4NextHop);
		    if((pFtnEntry == NULL) ||
				    (pFtnEntry->pActionPtr == NULL) ||
				    (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP))
		    {
			    MPLS_CMN_UNLOCK ();
		    }
		    u1HwStatus=pFtnEntry->u1HwStatus;
		    pFtnEntry->u1HwStatus=MPLS_FALSE;
		    MPLS_CMN_UNLOCK();

		    u4OsixRetVal=L3VpnProcessNonTeLspEvent(&L3VpnNonTeLspEventInfo);
		    if(u4OsixRetVal!=L3VPN_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }

		    pFtnEntry->u1HwStatus=u1HwStatus;

		    L3vpnDeleteIngressStaticBinding();

	    }
	    break;

        case 3:
            {
                /* If RT index is greater than L3VPN_MAX_NUM_RT_PER_VRF, RT creation should fail */
                MGMT_UNLOCK ();
		CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                /* If RT Index index is other than import, export or both RT creation should fail */
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = L3VPN_MAX_NUM_RT_PER_VRF + 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
		CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK (); 
            }
            break;
        case 4:
            {
                /* If RT row does not exist and user want to set any parameter for such a row other tha row status */
                MGMT_UNLOCK ();
		CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();


                /* If RT Index index is other than import, export or both RT creation should fail */
                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE)); 
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
		CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
            }
            break;

        case 5:
            {
                /* RT type wrongly set , RT test valiadtion should fail*/
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /*Setting Wrong RT Type */
                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x03, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Set the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;

        case 6:
            {

                /* RT sub-type wrongly set , RT test valiadtion should fail*/
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /*Setting Wrong RT Type */
                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x03, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Set the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;

        case 7:
            {
                /* Setting wrong RT Length , Validation should fail*/
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /* RT = 9 Bytes: explicitly set wrong */
                UINT1 au1RT[L3VPN_MAX_RT_LEN + 1] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                /* Wrong length set */
                RtValue.i4_Length = L3VPN_MAX_RT_LEN + 1;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Set the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break; 

        case 8:
            {
                /* Trying to Change Rt when the RT row status is ACTIVE */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                /* Wrong length set */
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Test the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Set the RT value */
                if(nmhSetMplsL3VpnVrfRT
                        (&VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, ACTIVE) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, ACTIVE) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }


                /* Change the RT while the row is active */
                au1RT[6] = 0x07;
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }


                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 9:
            {
                /* Duplicate RT is not allowed within the same VRF*/
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                /* Wrong length set */
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Test the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Set the RT value */
                if(nmhSetMplsL3VpnVrfRT
                        (&VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Create new Row for the same VRF */
                u4RtType = CLI_MPLS_L3VPN_RT_EXPORT; /* New row with RtType = Export in the same VRF */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Test the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, CLI_MPLS_L3VPN_RT_IMPORT, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /*Delete the new RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, CLI_MPLS_L3VPN_RT_EXPORT, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;

        case 10:
            {
                /* RT description length more than 256 */

                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                /* Wrong length set */
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                UINT1 au1RtDesc[255];
                MEMSET(au1RtDesc, 0, 255);
                tSNMP_OCTET_STRING_TYPE RtDesc;
                MEMSET(&RtDesc, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                RtDesc.pu1_OctetList = au1RtDesc;
                RtDesc.i4_Length = 255;
                /* RT description more than 256 characters */
                if(nmhTestv2MplsL3VpnVrfRTDescr(&u4ErrorCode,
                            &VrfName, u4RtIndex, u4RtType, &RtDesc) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }    
            break;

        case 11:
            {
                /* Row Already created for RT, trying to create a same row, shoul dreturn failure */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Trying to re-createthe same row should return failure */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;  

        case 12:
            {
                /* The RT entry exists beforehand and so RT
                 * must be configured before making the row active */   
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Try to Make the row active without configuring RT */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, ACTIVE) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                } 
            }
            break;

        case 13:
            {
                /* RT entry does not exist yet and user tries to make it active
                 * without configuring the RT: This will hit CLI case */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /* Try to make the row active without making the row active */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, ACTIVE) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                } 

                /* clean-up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();
            }
            break;

        case 14:
            {
                /* Destroying the Non-existent RT row, should return failure: Indices based deletion */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

                /* Try to make the destroy the non-existent row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, DESTROY) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* clean-up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK (); 
            }

            break;

        case 15:
            {
                /* Destroying RT based on RT value for  the VRF: CLI based*/
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Test the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Set the RT value */
                if(nmhSetMplsL3VpnVrfRT
                        (&VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                tL3vpnMplsL3VpnVrfRTEntry L3vpnSetMplsL3VpnVrfRTEntry;
                tL3vpnIsSetMplsL3VpnVrfRTEntry L3vpnIsSetMplsL3VpnVrfRTEntry;

                L3vpnIsSetMplsL3VpnVrfRTEntry.bMplsL3VpnVrfRT = OSIX_TRUE;
                L3vpnIsSetMplsL3VpnVrfRTEntry.bMplsL3VpnVrfRTRowStatus = OSIX_TRUE;
                L3vpnIsSetMplsL3VpnVrfRTEntry.bMplsL3VpnVrfName = OSIX_TRUE;


                MEMCPY(L3vpnSetMplsL3VpnVrfRTEntry.MibObject.au1MplsL3VpnVrfName,
                        au1VrfName, u4VrfNameLen);
                L3vpnSetMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfNameLen = u4VrfNameLen;
                L3vpnSetMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfRTLen  = L3VPN_MAX_RT_LEN;                           
                MEMCPY(L3vpnSetMplsL3VpnVrfRTEntry.MibObject.au1MplsL3VpnVrfRT, au1RT, L3VPN_MAX_RT_LEN); 
                L3vpnSetMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfRTRowStatus = DESTROY;

                if(L3vpnTestAllMplsL3VpnVrfRTTable(&u4ErrorCode, &L3vpnSetMplsL3VpnVrfRTEntry,
                            &L3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE, OSIX_FALSE) == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }        
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

            }
            break;

        case 16:
            {
                /* Destroy an RT entry based on RT value, which does not exist: CLI based */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Test the RT value */
                if(nmhTestv2MplsL3VpnVrfRT
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Set the RT value */
                if(nmhSetMplsL3VpnVrfRT
                        (&VrfName, u4RtIndex, u4RtType, &RtValue) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                tL3vpnMplsL3VpnVrfRTEntry L3vpnSetMplsL3VpnVrfRTEntry;
                tL3vpnIsSetMplsL3VpnVrfRTEntry L3vpnIsSetMplsL3VpnVrfRTEntry;

                L3vpnIsSetMplsL3VpnVrfRTEntry.bMplsL3VpnVrfRT = OSIX_TRUE;
                L3vpnIsSetMplsL3VpnVrfRTEntry.bMplsL3VpnVrfRTRowStatus = OSIX_TRUE;
                L3vpnIsSetMplsL3VpnVrfRTEntry.bMplsL3VpnVrfName = OSIX_TRUE;


                MEMCPY(L3vpnSetMplsL3VpnVrfRTEntry.MibObject.au1MplsL3VpnVrfName,
                        au1VrfName, u4VrfNameLen);
                L3vpnSetMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfNameLen = u4VrfNameLen;
                L3vpnSetMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfRTLen  = L3VPN_MAX_RT_LEN;

                /* Chaging the RT value which does not exist*/
                au1RT[4]= 0x07;
                MEMCPY(L3vpnSetMplsL3VpnVrfRTEntry.MibObject.au1MplsL3VpnVrfRT, au1RT, L3VPN_MAX_RT_LEN);
                L3vpnSetMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfRTRowStatus = DESTROY;

                if(L3vpnTestAllMplsL3VpnVrfRTTable(&u4ErrorCode, &L3vpnSetMplsL3VpnVrfRTEntry,
                            &L3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE, OSIX_FALSE) != OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                au1RT[4] = 0x00;      
                /*Delete RT row */
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;

        case 17:
            {
                /* Marking RT row NOT ready which does not exist, should return failure */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, NOT_READY) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();                        
            }
            break; 

        case 18:
            {
                /* Setting the storage type wrong, should return failure */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                tSNMP_OCTET_STRING_TYPE RtValue;
                MEMSET(&RtValue, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
                UINT4 u4ErrorCode = 0;
                UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
                MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
                VrfName.pu1_OctetList = au1VrfName;
                VrfName.i4_Length = u4VrfNameLen;
                u4RtIndex = 1;
                u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;


                UINT1 au1RT[L3VPN_MAX_RT_LEN] = {0x00, 0x02, 0x00,0x01, 0x00, 0x00, 0x00, 0x00};
                RtValue.pu1_OctetList = au1RT;
                RtValue.i4_Length = L3VPN_MAX_RT_LEN;
                /* Create and wait the row */
                if(nmhTestv2MplsL3VpnVrfRTRowStatus
                        (&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if(nmhTestv2MplsL3VpnVrfRTStorageType(&u4ErrorCode, &VrfName, u4RtIndex, 
                            u4RtType, L3VPN_STORAGE_READONLY + 1) != SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("end");
                MGMT_LOCK ();

                if(nmhSetMplsL3VpnVrfRTRowStatus
                        (&VrfName, u4RtIndex, u4RtType, DESTROY) == SNMP_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 19:
            {
                /* UT for the function L3VpnUtilFindNextFreeRtIndex() */
                /* Sunny Day case for RT index manager */
                /* Index manager should succeed, for MAX_RT_PER_VRF */
                UINT4 u4TempRtIndex = 0;
                L3VpnInitRTIndexMgr();

                for(u4Count = 1; u4Count <= L3VPN_MAX_NUM_RT_PER_VRF; u4Count++)
                {
                    if(L3VpnUtilFindNextFreeRtIndex(1, &u4TempRtIndex) == L3VPN_SUCCESS)
                    {
                        L3VpnUtilSetRtIndex(1, u4TempRtIndex);
                    }
                    else
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }

                /* Clean UP the index manager */
                for(u4Count = 1; u4Count <= L3VPN_MAX_NUM_RT_PER_VRF;u4Count++)
                {
                    L3VpnUtilReleaseRtIndex(1, u4Count);
                }
            }
            break;

        case 20:
            {
                /*Rainy Day test case for RT index manager */
                /* The function should not return the free index L3VpnUtilFindNextFreeRtIndex()
                 * beyond L3VPN_MAX_NUM_RT_PER_VRF for a VRF */

                UINT4 u4TempRtIndex = 0;
                L3VpnInitRTIndexMgr();

                for(u4Count = 1; u4Count <= L3VPN_MAX_NUM_RT_PER_VRF; u4Count++)
                {
                    if(L3VpnUtilFindNextFreeRtIndex(1, &u4TempRtIndex) == L3VPN_SUCCESS)
                    {
                        L3VpnUtilSetRtIndex(1, u4TempRtIndex);
                    }
                    else
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }

                /* Trying to get next index after all indices are exhausted in the VRF RT-index mgr*/
                if(L3VpnUtilFindNextFreeRtIndex(1, &u4TempRtIndex) != L3VPN_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean UP the index manager */
                for(u4Count = 1; u4Count <= L3VPN_MAX_NUM_RT_PER_VRF; u4Count++)
                {
                    L3VpnUtilReleaseRtIndex(1, u4Count);
                } 
            }
            break;

        case 21:
            {
                /* Rainy Day test case for L3VpnIfConfMapUnmapEventHandler()
                 * Q Msg Pool exhausted, then it should return */
                if(L3VpnUtExhaustQMsgPool() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                L3VpnIfConfMapUnmapEventHandler(L3VPN_VCM_IF_MAP, 1, 1); 
                /* clean Up */

                if(L3VpnFreeQMem() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 22:
            {
                /* Sunny day case for L3VpnIfConfMapUnmapEventHandler() */
                /* Event : L3VPN_VCM_IF_MAP */
                L3VpnIfConfMapUnmapEventHandler(L3VPN_VCM_IF_MAP, 1, 1);
            }
            break;

        case 23:
            {
                /* Sunny day case for L3VpnIfConfMapUnmapEventHandler() */
                /* Event : L3VPN_VCM_IF_UNMAP */
                L3VpnIfConfMapUnmapEventHandler (L3VPN_VCM_IF_UNMAP, 1, 1);
            }
            break;

        case 24:
            {
                /* Rainy Day test case for L3VpnIfStChgEventHandler()
                 * Q Msg Pool exhausted, then it should return */
                if(L3VpnUtExhaustQMsgPool() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                L3VpnIfStChgEventHandler(1, 1);
                /* clean Up */

                if(L3VpnFreeQMem() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 25:
            {
                /* sunny day case for L3VpnIfStChgEventHandler() */
                L3VpnIfStChgEventHandler(1, 1);
            }
            break;

        case 26:
            {
                /* Rainy Day test case for L3VpnAdminChangeEventHandler()
                 * Q Msg Pool exhausted, then it should return */
                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(au1VrfName));
                if(L3VpnUtExhaustQMsgPool() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                L3VpnAdminChangeEventHandler(au1VrfName, L3VPN_VRF_ADMIN_STATUS_UP);

                /* clean Up */  
                if(L3VpnFreeQMem() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 27:
            {
                /* sunny day test case for L3VpnAdminChangeEventHandler() */
                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(au1VrfName));
                L3VpnAdminChangeEventHandler(au1VrfName, L3VPN_VRF_ADMIN_STATUS_UP);
            }
            break;

        case 28:
            {
                /* Rainy day test case for L3VpnNonTeLspStatusChangeEventHandler() */
                /* * Q Msg Pool exhausted, then it should return */ 
                if(L3VpnUtExhaustQMsgPool() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                L3VpnNonTeLspStatusChangeEventHandler(500, 1, L3VPN_MPLS_LSP_DOWN);

                /* clean Up */
                if(L3VpnFreeQMem() == OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break; 

        case 29:
            {
                /* sunny day test case for L3VpnNonTeLspStatusChangeEventHandler() */
                L3VpnNonTeLspStatusChangeEventHandler(500, 1, L3VPN_MPLS_LSP_DOWN);
            } 
            break;

        case 30:
            {
                /* UT for L3VpnIsRemainingInterfacesUp() */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-2");
                CliExecuteAppCmd("ip vrf cust-2 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("ip vrf forwarding cust-2");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                if(L3VpnIsAnyInterfaceUpForVrf(au1VrfName) == OSIX_FALSE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/1");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no ip vrf forwarding cust-2");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/2");
                CliExecuteAppCmd("no ip vrf cust-2 mpls");
                CliExecuteAppCmd("no ip vrf cust-2");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();
            }   
            break;     

        case 31:
            {
                /* Rainny Day test case for L3VpnIsRemainingInterfacesUp() */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-2");
                CliExecuteAppCmd("ip vrf cust-2 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("ip vrf forwarding cust-2");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                if(L3VpnIsAnyInterfaceUpForVrf(au1VrfName) == OSIX_TRUE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/1");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no ip vrf forwarding cust-2");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/2");
                CliExecuteAppCmd("no ip vrf cust-2 mpls");
                CliExecuteAppCmd("no ip vrf cust-2");
                CliExecuteAppCmd("en");

                MGMT_LOCK ();

            }
            break;

        case 32:  
            /* Suuny day test case for L3VpnGetIfConfEntryWithIfIndex() */
            { 
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");

                MGMT_LOCK ();

                if(L3VpnGetIfConfEntryWithIfIndex (2) == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/1");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();                                           
            }
            break;

        case 33:
            {
                /* UT for L3VpnGetVrfEntryWithIfIndex() */ 
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                if(L3VpnGetVrfEntryWithIfIndex(1) == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* clean-Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/1");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();                                           
            }
            break;

        case 34:
            {
                /* UT for L3VpnGetIfConfEntry() */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("end");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("no sh");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN (VRF_NAME));

                if(L3VpnGetIfConfEntry(au1VrfName, 1) == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("sh");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no i g 0/1");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();
            }
            break;

        case 35:
            {
                /* Sunny day test for MplsL3VpnApiGetVrfTableEntry() */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                if(MplsL3VpnApiGetVrfTableEntry (au1VrfName, STRLEN(VRF_NAME)) == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean up */ 
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();
            }
            break; 

        case 36:
            {
                if(MplsL3VpnApiGetVrfTableEntry(NULL, 6) != NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 37:
            {
                /* Sunny day test case for L3VpnGetVrfPerfTableEntry */ 
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                if(L3VpnGetVrfPerfTableEntry(au1VrfName) == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();
            }
            break;

        case 38:
            {
                /* Rainy Day test case for L3VpnGetVrfPerfTableEntry */
                if(L3VpnGetVrfPerfTableEntry(NULL) != NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 39:
            {
                /* UT case for L3VpnFlushRouteTableForVrf() */
                L3VpnUtCreateVrf();
                L3VpnUtAddIngressRoute();


                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                if(L3VpnFlushRouteTableForVrf(au1VrfName) != L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */                                                      
                L3VpnUtLspCleanUp();
                L3VpnDeleteVrf();

            }
            break;
        case 40:
            {
                /* UT case for L3VpnDeleteIfConfTableForVrf() */

                L3VpnUtCreateVrf();
                L3VpnUtMapInterfaceToVrf(1, TRUE);

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));

                if(L3VpnDeleteIfConfTableForVrf(au1VrfName) != L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                MGMT_UNLOCK();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK();
                L3VpnDeleteVrf();
            }
            break;

        case 41:
            {
                /* UT case for L3VpnDeleteRtTableForVrf() */
                L3VpnUtCreateVrf();
                L3VpnUtAddRtToVrf();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                if(L3VpnDeleteRtTableForVrf(au1VrfName) != L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean-up */
                L3VpnDeleteVrf();
            }
            break;

        case 42:
            {
                L3VpnUtCreateVrf();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));

                if(L3VpnUpdatePerfTableCounters(au1VrfName, L3VPN_ROUTE_ADDED) != 
                        L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* clean-up */
                L3VpnDeleteVrf(); 
            }
            break;

        case 43:
            {

                L3VpnUtCreateVrf();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));

                if(L3VpnUpdatePerfTableCounters(au1VrfName, L3VPN_ROUTE_DELETED) !=
                        L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* clean-up */
                L3VpnDeleteVrf();

            }
            break;

        case 44:
            {
                L3VpnUtCreateVrf();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));

                if(L3VpnUpdatePerfTableCounters(au1VrfName, L3VPN_ROUTE_DROPPED) !=
                        L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* clean-up */
                L3VpnDeleteVrf();
            }
            break;

        case 45:
            {
                /* Rainy day test case for L3vpnUtlGetVrfSecTablEntry() */
                if(L3vpnUtlGetVrfSecTableEntry (NULL) != NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 46:
            {
                /* Rainy day test case for L3vpnUtlVrfSecTableRowDelete */
                if(L3vpnUtlVrfSecTableRowDelete (NULL) != L3VPN_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }

        case 47:
            {
                /* raniy day case for L3vpnUtlGetVrfSecTableEntry() */
                if(L3vpnUtlGetVrfSecTableEntry (NULL) != NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            }
            break;

        case 48:
            {
               /* Rainy day UT for L3vpnUtlUpdateVrfSecTable() */
               if(L3vpnUtlUpdateVrfSecTable 
                     (NULL, L3VPN_VRF_SEC_ILLEGAL_LBL_VL_OP) != L3VPN_FAILURE)
               {
                   u4RetVal = OSIX_FAILURE;
               }
            }
            break;

        case 49:
            {
                /* Sunny day test case for L3vpnUtlUpdateVrfSecTable() */
                tL3vpnMplsL3VpnVrfEntry     *pVrfEntry = NULL;
                tL3vpnMplsL3VpnVrfSecEntry  *pVrfSecEntry = NULL;
                L3VpnUtCreateVrf();
                
                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
           
                pVrfEntry = MplsL3VpnApiGetVrfTableEntry(au1VrfName, STRLEN(VRF_NAME));
                if(pVrfEntry == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }
            
                pVrfSecEntry = L3vpnUtlGetVrfSecTableEntry (pVrfEntry); 
                if(pVrfSecEntry == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                if(L3vpnUtlUpdateVrfSecTable (pVrfSecEntry, L3VPN_VRF_SEC_ILLEGAL_LBL_VL_OP)
                           != L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }


                if(L3vpnUtlUpdateVrfSecTable (pVrfSecEntry, L3VPN_VRF_SEC_DIS_CONT_TIME_OP)
                           != L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* Clean Up */
                L3VpnDeleteVrf();     
            }   
            break;
 
        case 50:
            {
                /* rainy day test case for L3vpnUtlUpdateVrfSecTable() */
                tL3vpnMplsL3VpnVrfEntry     *pVrfEntry = NULL;
                tL3vpnMplsL3VpnVrfSecEntry  *pVrfSecEntry = NULL;
                L3VpnUtCreateVrf();

                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));

                pVrfEntry = MplsL3VpnApiGetVrfTableEntry(au1VrfName, STRLEN(VRF_NAME));
                if(pVrfEntry == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                pVrfSecEntry = L3vpnUtlGetVrfSecTableEntry (pVrfEntry);
                if(pVrfSecEntry == NULL)
                {
                    u4RetVal = OSIX_FAILURE;
                }

                /* rainy day test case  for L3vpnUtlUpdateVrfSecTable() */
                if(L3vpnUtlUpdateVrfSecTable (pVrfSecEntry, 3)
                           == L3VPN_SUCCESS)
                {
                    u4RetVal = OSIX_FAILURE;
                }
                /* Clean Up */
                L3VpnDeleteVrf();
            }
            break;
            
        case 51:
            {
                /* Sunny day test case for L3vpnUtlProcessVrfCreation() to cover Ip interface list part */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("ex");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no sw");
                CliExecuteAppCmd("ip vrf forwarding cust-1");
                CliExecuteAppCmd("en");
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("ip vrf cust-1 mpls");
                CliExecuteAppCmd("en");
                MGMT_LOCK ();

                
                /* Clean Up */
                MGMT_UNLOCK ();
                CliExecuteAppCmd("c t");
                CliExecuteAppCmd("no ip vrf cust-1 mpls");
                CliExecuteAppCmd("i g 0/1");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("ex");
                CliExecuteAppCmd("i g 0/2");
                CliExecuteAppCmd("no ip vrf forwarding cust-1");
                CliExecuteAppCmd("ex");
                CliExecuteAppCmd("no ip vrf cust-1");
                CliExecuteAppCmd("en");
                MGMT_LOCK (); 
            }
           break; 

         case 52:
             {
                 /* UT case for L3VpnChckAndSendRouteNotifications() */
                 /* for teh case if max threshold == max routes allowed in vrf */   
                 MGMT_UNLOCK ();
                 CliExecuteAppCmd("c t");
                 CliExecuteAppCmd("mpls l3vpn trap enable");

                 CliExecuteAppCmd("c t");
                 CliExecuteAppCmd("ip vrf cust-1");
                 CliExecuteAppCmd("ip vrf cust-1 mpls");
                 CliExecuteAppCmd("maximum routes 5 high-route-threshold 5 mid-route-threshold 2");
                 CliExecuteAppCmd("en"); 
                 MGMT_LOCK ();

                 L3VpnUtAddIngressRoute();
                 L3VpnAddRouteAtIngress(2);
                 L3VpnAddRouteAtIngress(3);
                 L3VpnAddRouteAtIngress(4);
                 L3VpnAddRouteAtIngress(5);
                /* Clean up */

                 L3VpnDeleteRouteAtIngress(1);
                 L3VpnDeleteRouteAtIngress(2);
                 L3VpnDeleteRouteAtIngress(3);
                 L3VpnDeleteRouteAtIngress(4);
                 L3VpnDeleteRouteAtIngress(5);
                 L3VpnUtLspCleanUp();
                 L3VpnDeleteVrf();
             }
             break;    

         case 53:
             {
                 /* UT case for L3VpnChckAndSendRouteNotifications() */
                 /* for teh case if max threshold < max routes allowed in vrf */

                 MGMT_UNLOCK ();
                 CliExecuteAppCmd("c t");
                 CliExecuteAppCmd("mpls l3vpn trap enable");

                 CliExecuteAppCmd("c t");
                 CliExecuteAppCmd("ip vrf cust-1");
                 CliExecuteAppCmd("ip vrf cust-1 mpls");
                 CliExecuteAppCmd("maximum routes 5 high-route-threshold 3 mid-route-threshold 2");
                 CliExecuteAppCmd("en");
                 MGMT_LOCK ();

                 L3VpnUtAddIngressRoute();
                 L3VpnAddRouteAtIngress(2);
                 L3VpnAddRouteAtIngress(3);
                 L3VpnAddRouteAtIngress(4);
                 L3VpnAddRouteAtIngress(5);


                 /* Clean Up */
                 L3VpnDeleteRouteAtIngress(1);
                 L3VpnDeleteRouteAtIngress(2);
                 L3VpnDeleteRouteAtIngress(3);
                 L3VpnDeleteRouteAtIngress(4);
                 L3VpnDeleteRouteAtIngress(5);
                 L3VpnUtLspCleanUp();
                 L3VpnDeleteVrf();
                                  
             }                                                     

            case 54:
                    {
                            tL3vpnMplsL3VpnVrfEntry         L3vpnMplsL3VpnVrfEntry;
                            tL3vpnIsSetMplsL3VpnVrfEntry    L3vpnIsSetMplsL3VpnVrfEntry;
                            UINT4                           u4ErrorCode=0;
                            INT4                            i4RowStatusLogic=OSIX_FALSE;
                            INT4                            i4RowCreateOption=OSIX_FALSE;

                            MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
                            MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,0,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));

                            MGMT_UNLOCK ();
                            CliExecuteAppCmd("c t");
                            CliExecuteAppCmd("set cli pagination off");
                            CliExecuteAppCmd("ip vrf cust-1");
                            MGMT_LOCK();

                            L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

                            /** To test : When VRF name(Table key) is not set **/

                            L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfName=OSIX_FALSE;

                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }
                            /** To test : When VRF name is not present **/
                            L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfName=OSIX_TRUE;
                            L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus=OSIX_FALSE;

                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }
                            MGMT_UNLOCK ();
                            CliExecuteAppCmd("c t");
                            CliExecuteAppCmd("set cli pagination off");
                            CliExecuteAppCmd("no ip vrf cust-1");
                            MGMT_LOCK ();
                    }
                    break;
                case 55:
                {
                    
                            tL3vpnMplsL3VpnVrfEntry         L3vpnMplsL3VpnVrfEntry;
                            tL3vpnIsSetMplsL3VpnVrfEntry    L3vpnIsSetMplsL3VpnVrfEntry;
                            INT4                            i4RowStatusLogic=OSIX_FALSE;
                            INT4                            i4RowCreateOption=OSIX_FALSE;
                            UINT4                           u4ErrorCode=0;
                            UINT1                           u1RdSubType=0;

                            MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
                            MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,0,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));

                            MGMT_UNLOCK ();
                            CliExecuteAppCmd("c t");
                            CliExecuteAppCmd("set cli pagination off");
                            CliExecuteAppCmd("ip vrf cust-1");
                            CliExecuteAppCmd("ip vrf cust-1 mpls");
                            CliExecuteAppCmd("rd 100:1");
                            MGMT_LOCK ();

                            L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

                            /*** User cannot set VPN ID when row is Active **/
                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }

                            /*** RD testing **/
                            L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfVpnId=OSIX_FALSE;

                            /*** RD Length : Boundary condition **/
                            L3VPN_VRF_RD_LEN(L3vpnMplsL3VpnVrfEntry)=MPLS_MAX_L3VPN_RD_LEN+1;
                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }
                            L3VPN_VRF_RD_LEN(L3vpnMplsL3VpnVrfEntry)=MPLS_MAX_L3VPN_RD_LEN;

                            /** RD Type : Testing **/
                            u1RdSubType=L3VPN_VRF_RD(L3vpnMplsL3VpnVrfEntry)[0];
                            L3VPN_VRF_RD(L3vpnMplsL3VpnVrfEntry)[0]=L3VPN_RD_TYPE_2+1;
                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }
                            L3VPN_VRF_RD(L3vpnMplsL3VpnVrfEntry)[0]=u1RdSubType;

                            L3VPN_VRF_RD(L3vpnMplsL3VpnVrfEntry)[1]=L3VPN_RD_SUBTYPE+1;
                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }
			    L3VPN_VRF_RD(L3vpnMplsL3VpnVrfEntry)[1]=L3VPN_RD_SUBTYPE;

                            /** RD set : when Row status is Active **/
                            u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                            if(u4OsixRetVal==OSIX_SUCCESS)
                            {
                                u4RetVal=OSIX_FAILURE;
                                break;
                            }
                            MGMT_UNLOCK ();
                            CliExecuteAppCmd("end");
                            CliExecuteAppCmd("c t");
                            CliExecuteAppCmd("set cli pagination off");
                            CliExecuteAppCmd("ip vrf cust-1 mpls");
                            CliExecuteAppCmd("no rd");
                            CliExecuteAppCmd("end");
                            CliExecuteAppCmd("c t");
                            CliExecuteAppCmd("no ip vrf cust-1 mpls");
                            CliExecuteAppCmd("no ip vrf cust-1");
                            MGMT_LOCK ();
            
                }
                break;

                case 56:
                {
                        /** Row Testing **/

                        tL3vpnMplsL3VpnVrfEntry         L3vpnMplsL3VpnVrfEntry;
                        tL3vpnIsSetMplsL3VpnVrfEntry    L3vpnIsSetMplsL3VpnVrfEntry;
                        INT4                            i4RowStatusLogic=OSIX_FALSE;
                        INT4                            i4RowCreateOption=OSIX_FALSE;
                        UINT4                           u4ErrorCode=0;
                        UINT1                           u1RdSubType=0;

                        MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
                        MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,0,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));

                        /*** Row Testing for Row Destroy  When VRF entry is not there **/
                        L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfVpnId=OSIX_FALSE;
                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfRD=OSIX_FALSE;
                        L3VPN_VRF_ROW_STATUS(L3vpnMplsL3VpnVrfEntry)=DESTROY;

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }


                        /** Row Status Active came, when VCM is not there ***/
                        MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,OSIX_FALSE,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));
                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus=OSIX_TRUE;
                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfName=OSIX_TRUE;
                        L3VPN_VRF_ROW_STATUS(L3vpnMplsL3VpnVrfEntry)=ACTIVE;

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }


                        MGMT_UNLOCK ();
                        CliExecuteAppCmd("c t");
                        CliExecuteAppCmd("set cli pagination off");
                        CliExecuteAppCmd("ip vrf cust-1");
                        CliExecuteAppCmd("ip vrf cust-1 mpls");
                        MGMT_LOCK ();

                        
                        L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

                        /** to test Row Already configured **/
                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfVpnId=OSIX_FALSE;
                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfRD=OSIX_FALSE;

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
                        /*** Row Active When RD not Exists **/
                        L3VPN_VRF_ROW_STATUS(L3vpnMplsL3VpnVrfEntry)=ACTIVE;
                        MEMSET(L3VPN_VRF_RD(L3vpnMplsL3VpnVrfEntry),0,MPLS_MAX_L3VPN_RD_LEN);
                        L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfRD=OSIX_FALSE;

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }

                        MGMT_UNLOCK ();
                        CliExecuteAppCmd("end");
                        CliExecuteAppCmd("c t");
                        CliExecuteAppCmd("no ip vrf cust-1 mpls");
                        CliExecuteAppCmd("no ip vrf cust-1");
                        MGMT_LOCK ();

                }
                break;

                /*** l3vpnbgp.c file **/
                case 57:
                {
                    /** RegisterDeRegister APIs**/
                    tMplsL3VpnRegInfo   MplsL3VpnRegInfo;
                    MEMSET(&MplsL3VpnRegInfo,0,sizeof(tMplsL3VpnRegInfo));
                    MplsL3VpnRegInfo.u2InfoMask=L3VPN_BGP4_ROUTE_PARAMS_REQ|
                            L3VPN_BGP4_LSP_STATUS_REQ|
                            L3VPN_BGP4_VRF_ADMIN_STATUS_REQ;
                    u4OsixRetVal=MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                            u4RetVal=OSIX_FAILURE;
                            break;
                    }
                    u4OsixRetVal=MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                            u4RetVal=OSIX_FAILURE;
                            break;
                    }
                }
                break;

                case 58:
                {
                        /** To test ASN in use **/
                        BOOL1 bInUse;
			tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
			UINT1 u1VrfName[L3VPN_MAX_VRF_NAME_LEN];

			MEMSET(&MplsL3VpnVrfName,0,sizeof(tSNMP_OCTET_STRING_TYPE));

			MplsL3VpnVrfName.pu1_OctetList=u1VrfName;
			MplsL3VpnVrfName.i4_Length=STRLEN(L3VPN_DEF_VRF_NAME);
			MEMCPY(MplsL3VpnVrfName.pu1_OctetList,
					L3VPN_DEF_VRF_NAME,MplsL3VpnVrfName.i4_Length);

                        /** When no VRF no RT is configured **/
                        MplsBgp4ASNInUse(&bInUse);
                        if(bInUse==OSIX_TRUE)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }
                        /** when VRF is configured but not ACTIVE **/
                        u4OsixRetVal=L3vpnCreateMplsVrf();
                        if(u4OsixRetVal!=OSIX_SUCCESS)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }
                        MplsBgp4ASNInUse(&bInUse);
                        if(bInUse==OSIX_TRUE)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }
                        /** when RT is configured with ACTIVE status **/
                        L3vpnCreateRt();
                        MplsBgp4ASNInUse(&bInUse);
                        if(bInUse==OSIX_FALSE)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }
                        /** when VRF is configured but ACTIVE **/
                        u4OsixRetVal=L3vpnCreateMplsRd();
                        if(u4OsixRetVal!=OSIX_SUCCESS)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }

			nmhSetMplsL3VpnVrfConfRowStatus(&MplsL3VpnVrfName,ACTIVE);

			MplsBgp4ASNInUse(&bInUse);
			if(bInUse==OSIX_FALSE)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }
			nmhSetMplsL3VpnVrfConfRowStatus(&MplsL3VpnVrfName,NOT_IN_SERVICE);

			/*** When VRF is configured ACTIVE, RT is configured but not in Service **/

			nmhSetMplsL3VpnVrfRTRowStatus(&MplsL3VpnVrfName,1,MPLS_L3VPN_RT_IMPORT,
					NOT_IN_SERVICE);

			MplsBgp4ASNInUse(&bInUse);
			if(bInUse==OSIX_TRUE)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }

		
                        /*** delete the configuration **/
                        L3vpnDeleteRt();
                        u4OsixRetVal=L3vpnDeleteMplsVrf();
                        if(u4OsixRetVal!=OSIX_SUCCESS)
                        {
                            u4RetVal=OSIX_FAILURE;
                            break;
                        }
                }
                break;
                case 59:
                {
                    UINT4 u4Prefix=0;
                    UINT4 u4LspLabel=0;
                    UINT1 u1LspStatus=0;
                    /*** To get Non-Te LSP : to test failure scenario ***/
                    u4OsixRetVal=MplsGetNonTeLspForDest(u4Prefix,&u4LspLabel,&u1LspStatus);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;

                case 60:
                {
                    
                    UINT4 u4NextHop=0;
                    UINT4 u4LspLabel=0;
                    UINT1 u1LspStatus=0;

                    u4NextHop=(*(UINT4 *)(u1IngressNextHop));
                    u4NextHop = OSIX_HTONL (u4NextHop);

                    L3vpnCreateIngressStaticBinding();

                    /*** To get Non-Te LSP : to test failure scenario ***/
                    u4OsixRetVal=MplsGetNonTeLspForDest(u4NextHop,&u4LspLabel,&u1LspStatus);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                            u4RetVal=OSIX_FAILURE;
                            break;
                    }

                    L3vpnDeleteIngressStaticBinding();  
                }
                break;

                case 61:
                {
                    /** how to fail : getOutSeg Index**/

                }
                break;
                /** Route ADD/DEL at Egress **/
                case 62:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
		    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

		    u4OsixRetVal=L3vpnFillDefaultEgressPerRouteAdd(&MplsL3VpnBgp4RouteInfo);

		    if(u4OsixRetVal!=OSIX_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }

                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(NULL);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
		    }
		    MplsL3VpnBgp4RouteInfo.u1LabelAllocPolicy=L3VPN_BGP4_POLICY_PER_ROUTE+1;
		    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);

		    if(u4OsixRetVal==OSIX_SUCCESS)
		    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 63:
                {
                    /** check when BgpRouteLabelEntry POOL gets exausted **/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    /*** Allocate the complete memory pool **/
                    u4OsixRetVal=L3vpnAllocateBgpRouteLabelEntry();
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {   
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3vpnFillDefaultEgressPerRouteAdd(&MplsL3VpnBgp4RouteInfo);

                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {   
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    /** Free the complete allocated memory pool **/
                    u4OsixRetVal=L3vpnFreeBgpRouteLabelEntry();
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {   
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 64:
                {
                    /** Addition L3VPN_BGP_ROUTE_LABEL_TABLE Failure **/

                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    u4OsixRetVal=L3vpnFillDefaultEgressPerRouteAdd(&MplsL3VpnBgp4RouteInfo);

                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {   
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   

                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** Deletion of L3VPN_BGP_ROUTE_LABEL_TABLE **/
                    u4OsixRetVal=L3VpnDeleteBgpRouteLabelTable(L3VPN_DEF_EGRESS_LABEL);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 65:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    u4OsixRetVal=L3vpnFillDefaultEgressPerRouteAdd(&MplsL3VpnBgp4RouteInfo);

                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {   
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    /** Deletion of L3VPN_BGP_ROUTE_LABEL_TABLE **/
                    u4OsixRetVal=L3VpnDeleteBgpRouteLabelTable(L3VPN_DEF_EGRESS_LABEL);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 66:
                {
                    /** Deletion of L3VPN_BGP_ROUTE_LABEL_TABLE **/
                    u4OsixRetVal=L3VpnDeleteBgpRouteLabelTable(L3VPN_DEF_EGRESS_LABEL);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }       
                }
                break;
                case 67:
                {
                    L3vpnGetBgpRouteLabelEntry(L3VPN_DEF_EGRESS_LABEL);
                    /** Deletion of L3VPN_BGP_ROUTE_LABEL_TABLE **/
                    u4OsixRetVal=L3VpnDeleteBgpRouteLabelTable(L3VPN_DEF_EGRESS_LABEL);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                }
                break;
                case 68:
                {
                    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry=NULL;
                    pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelEntry(L3VPN_DEF_EGRESS_LABEL);
                    if(pL3VpnBgpRouteLabelEntry!=NULL)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 69:
                {
                    /**** L3VpnDelBgpRouteLabelTable ***/
                    tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo=NULL;
                    u4OsixRetVal=L3VpnDelBgpRouteLabelTable(pMplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 70:
                {

                    /**** L3VpnDelBgpRouteLabelTable ***/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerRouteAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnDelBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 71:
                {
                    /**** L3VpnDelBgpRouteLabelTable ***/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressRouteDel(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnDelBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 72:
                {
                    /*** L3VpnClearBgpRouteLabelTable **/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnAddBgpRouteLabelTable(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 73:
                {
                    /*** L3VpnBgp4RouteAddDelAtEgress **/
                    tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo=NULL;
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(pMplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal == OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 74:
                {
                    /*****L3VpnBgp4RouteAddDelAtEgress***/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;
                case 75:
                {
                    /*****L3VpnBgp4RouteAddDelAtEgress Add***/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                }
                break;
                case 76:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressRouteDel(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;  
                    }
                }
                break;
                case 77:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressRouteDel(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;  
                    }
                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                }
                break;

                case 78:
                {
                    /*** MplsL3VpnBgp4RouteUpdate **/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(NULL);
		    if(u4OsixRetVal==OSIX_SUCCESS)
		    {
			 u4RetVal=OSIX_FAILURE;
			 break;
		    }
			
                    MplsL3VpnBgp4RouteInfo.IpPrefix.u2Afi=MPLS_IPV6_ADDR_TYPE;
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    MplsL3VpnBgp4RouteInfo.IpPrefix.u2Afi=MPLS_IPV4_ADDR_TYPE;
                    MplsL3VpnBgp4RouteInfo.NextHop.u2Afi=MPLS_IPV6_ADDR_TYPE;
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;

                case 79:
                {
                    /*** MplsL3VpnBgp4RouteUpdate **/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /**** VcmGetAliasName : fail *****/
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                }
                break;

                case 80:
                {
                    L3vpnCreateIpVrf();

                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /**** VRF entry not found in MPLS VRF table : fail *****/
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    L3vpnDeleteIpVrf();
                }
                break;
                case 81:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
                    if(pL3vpnMplsL3VpnVrfEntry==NULL)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /*** delete the perf table **/
                    pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(pL3vpnMplsL3VpnVrfEntry);
                    if(pL3vpnMplsL3VpnVrfPerfEntry!=NULL)
                    {
                        if(L3vpnUtlVrfPerfTableRowDelete(pL3vpnMplsL3VpnVrfPerfEntry) != L3VPN_SUCCESS)
                        {
                            return L3VPN_FAILURE;
                        }
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();
                }
                break;
                /** Egress Add **/
                case 82:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();
                }
                break;
                case 83:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf(); 
                }
                break;
                /** Egress Delete **/
                case 84:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressRouteDel(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf(); 
                }
                break;
                case 85:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressRouteDel(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();     
                }
                break;
                /** Ingress Add **/
                case 86:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
#if 1
                    u4OsixRetVal=L3vpnAllocateVrfRteEntry();
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    /** Error in Allocating memory for Route Entry **/
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=L3vpnFreeVrfRteEntry();
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
#endif

                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();     
                }
                break;
                case 87:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** LSP not found for the next hop **/
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();     
                }
                break;

                case 88:
                {
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIngressStaticBinding();
                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();
                    u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    /** RTE Entry Already exists **/
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultIngressRouteDel(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
		    u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    MplsL3VpnBgp4RouteInfo.u4Label=600;
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    MplsL3VpnBgp4RouteInfo.u4Label=500;
                    MplsL3VpnBgp4RouteInfo.u2PrefixLen=100;
		    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
		    if(u4OsixRetVal==OSIX_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }

                    u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
		    }
                    L3vpnAllocateVarBindPool();
		    L3VPN_NOTIFICATION_ENABLE  == L3VPN_TRAP_ENABLE;

                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;
		    pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
		    if(pL3vpnMplsL3VpnVrfEntry==NULL)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }
                    pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(pL3vpnMplsL3VpnVrfEntry);

		    L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry) =
			    L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry);
		    /*L3VPN_P_VRF_HIGH_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) = OSIX_TRUE;*/

		    L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) =
			    L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry) - 1;
		    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
		    if(u4OsixRetVal==OSIX_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }
                    L3vpnClearVarBindPool();
                    L3VPN_NOTIFICATION_ENABLE  == L3VPN_TRAP_DISABLE;

                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();
                    L3vpnDeleteIngressStaticBinding();
                }
                break;
                case 89:
                {

                    L3vpnCreateIngressStaticBinding();
                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    MGMT_UNLOCK ();
                    CliExecuteAppCmd("c t");
                    CliExecuteAppCmd("ip vrf cust-1");
                    CliExecuteAppCmd("ip vrf cust-1 mpls");
                    CliExecuteAppCmd("maximum routes 2 high-route-threshold 1 mid-route-threshold 1");
                    MGMT_LOCK();

                    L3vpnCreateIngressRoute_1();
                    L3vpnCreateIngressRoute_2();

                    L3vpnCreateIngressRoute_3();
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();
                    L3vpnDeleteIngressStaticBinding();
                }
                break;
                case 90:    
                {
                    UINT4                           u4ErrorCode=0;
                    tL3vpnMplsL3VpnVrfEntry         L3vpnMplsL3VpnVrfEntry;
                    tL3vpnIsSetMplsL3VpnVrfEntry    L3vpnIsSetMplsL3VpnVrfEntry;
                    INT4                            i4RowStatusLogic=OSIX_FALSE;
                    INT4                            i4RowCreateOption=OSIX_FALSE;

                    MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
                    MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,0,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));

                    MGMT_UNLOCK ();
                    CliExecuteAppCmd("c t");
                    CliExecuteAppCmd("set cli pagination off");
                    CliExecuteAppCmd("ip vrf cust-1");
                    CliExecuteAppCmd("ip vrf cust-1 mpls");
                    MGMT_LOCK ();

                    L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

                    /** To test : Boundary condition for VRF Length **/
                    L3VPN_VRF_NAME_LEN(L3vpnMplsL3VpnVrfEntry)=L3VPN_MAX_VRF_NAME_LEN+1;

                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** Revert the VRF Length **/
                    L3VPN_VRF_NAME_LEN(L3vpnMplsL3VpnVrfEntry)=STRLEN(L3VPN_DEF_VRF_NAME);

                    /** To test : VPN ID : Boundry confition for VPN ID Length**/
                    L3VPN_VRF_VPN_ID_LEN(L3vpnMplsL3VpnVrfEntry)= L3VPN_MAX_VRF_VPN_ID_LEN+1;
                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** Revert the VPN ID Length **/
                    L3VPN_VRF_VPN_ID_LEN(L3vpnMplsL3VpnVrfEntry)= STRLEN(L3VPN_DEF_VRF_VPN_ID); 

                    /*** To test : VRF Description : Boundry confition for VRF Description Length*/
                    L3VPN_VRF_DESCRIPTION_LEN(L3vpnMplsL3VpnVrfEntry)= L3VPN_MAX_VRF_DESCRIPTION_LEN+1;
                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** Revert Back the VRF Description Length**/
                    L3VPN_VRF_DESCRIPTION_LEN(L3vpnMplsL3VpnVrfEntry)=STRLEN(L3VPN_DEF_VRF_DESCRIPTION);

                    /** To test : MidRteThresh Boundary Condition **/
                    L3VPN_VRF_MID_ROUTE_TH(L3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES+1;
                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    L3VPN_VRF_MID_ROUTE_TH(L3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES-1;

                    /** to Test : HighRteThresh Boundary Condition **/
                    L3VPN_VRF_HIGH_ROUTE_TH(L3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES+1;
                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    L3VPN_VRF_HIGH_ROUTE_TH(L3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES;

                    /** To test : ConfMaxRoutes Boundary Condition */
                    L3VPN_VRF_CONF_MAX_ROUTES(L3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES+1;
                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    L3VPN_VRF_CONF_MAX_ROUTES(L3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES;

		    /** To test : ConfAdminStatus **/
		    L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus=OSIX_FALSE;
		    L3VPN_VRF_ADMIN_STATUS(L3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_TESTING;
		    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** Revert back ConfAdminStatus **/
                    L3VPN_VRF_ADMIN_STATUS(L3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_DOWN;

                    /** to test : ConfStorageType **/
                    L3VPN_VRF_STORAGE_TYPE(L3vpnMplsL3VpnVrfEntry)=L3VPN_STORAGE_READONLY+1;
                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /** Revert back ConfStorageType**/
                    L3VPN_VRF_STORAGE_TYPE(L3vpnMplsL3VpnVrfEntry)=L3VPN_STORAGE_OTHER-1;

                    u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                            &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                    if(u4OsixRetVal==OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }

                    MGMT_UNLOCK ();
                    CliExecuteAppCmd("end");
                    CliExecuteAppCmd("c t");
                    CliExecuteAppCmd("set cli pagination off");
                    CliExecuteAppCmd("no ip vrf cust-1 mpls");
                    CliExecuteAppCmd("no ip vrf cust-1");
                    MGMT_LOCK ();
                }

                break;
                /** Ingress Delete**/
                break;

                case 91:
                {
			if(MplsL3VpnRegisterCallback(NULL) == OSIX_SUCCESS)
			{
				u4RetVal = OSIX_FAILURE;
			}
			if(MplsL3VpnDeRegisterCallback(NULL)== OSIX_SUCCESS)
			{
				u4RetVal = OSIX_FAILURE;
			}
		}
		break;
		case 92:
            {
               /* UT case for the function L3VpnRtTableShow() */
               L3VpnUtCreateVrf ();
               L3VpnAddRtToVrf();
          
               /* Show RT */
               MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
               MEMCPY (au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
               if(L3VpnRtTableShow (CliHandle,  au1VrfName) != CLI_SUCCESS)
               {
                   u4RetVal = OSIX_FAILURE;
               }

               /* Clean Up */
               L3VpnDeleteRtFromVrf (); 
               L3VpnDeleteVrf();
            }
            break;

        case 93:
            {
               /* UT case for the function L3VpnRtTableShow() */
               L3VpnUtCreateVrf ();
               L3VpnAddRtToVrf();

               /* Show RT */
               MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
               MEMCPY (au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
               if(L3VpnRtTableShow (CliHandle,  NULL) != CLI_SUCCESS)
               {
                   u4RetVal = OSIX_FAILURE;
               }

               /* Clean Up */
               L3VpnDeleteRtFromVrf ();
               L3VpnDeleteVrf();
 
            }    
            break;
        case 94:
            {
                /* Sunny day case for L3VpnCliPrintRTInfo() */
                L3VpnCliPrintRTInfo (CliHandle, NULL);
            }
            break;
   
        case 95:
            {
               /* Sunny day case  for L3VpnIfConfTableShow() */
                L3VpnUtCreateVrf ();
                
                L3VpnUtMapInterfaceToVrf(1, TRUE);
                L3VpnUtMapInterfaceToVrf(2, FALSE);
                MGMT_UNLOCK ();
                CliExecuteAppCmd ("c t"); 
                CliExecuteAppCmd ("ip vrf cust-2");
                CliExecuteAppCmd ("ip vrf cust-2 mpls");
                CliExecuteAppCmd ("en");

                CliExecuteAppCmd ("c t");
                CliExecuteAppCmd ("i g 0/3");
                CliExecuteAppCmd ("no sw");
                CliExecuteAppCmd ("ip vrf forwarding cust-2");
                CliExecuteAppCmd ("en");
 
                CliExecuteAppCmd ("c t");
                CliExecuteAppCmd ("i g 0/4");
                CliExecuteAppCmd ("no sw");
                CliExecuteAppCmd ("ip vrf forwarding cust-2");
                CliExecuteAppCmd ("en");                   
 
                MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY (au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                L3VpnIfConfTableShow(CliHandle, au1VrfName);
           
                /* Clean Up */
                L3VpnUtUnMapInterfaceToVrf(1);
                L3VpnUtUnMapInterfaceToVrf(2);
                L3VpnDeleteVrf ();

                CliExecuteAppCmd ("c t");
                CliExecuteAppCmd ("i g 0/3");
                CliExecuteAppCmd ("no ip vrf forwarding cust-2");
                CliExecuteAppCmd ("ex");
                CliExecuteAppCmd ("no i g 0/3");
                CliExecuteAppCmd ("en");

                CliExecuteAppCmd ("c t");
                CliExecuteAppCmd ("i g 0/4");
                CliExecuteAppCmd ("no ip vrf forwarding cust-2");
                CliExecuteAppCmd ("ex");
                CliExecuteAppCmd ("no i g 0/4");
                CliExecuteAppCmd ("en");

                CliExecuteAppCmd ("c t");
                CliExecuteAppCmd ("no ip vrf cust-2 mpls");
                CliExecuteAppCmd ("ip vrf cust-2");
                CliExecuteAppCmd ("en");
                MGMT_LOCK ();
                    
            }
            break;

        case 96:
            {
                 /* Sunny day case  for L3VpnIfConfTableShow() */
                 L3VpnUtCreateVrf ();
                 
                 L3VpnUtMapInterfaceToVrf(1, TRUE); 
                 L3VpnUtMapInterfaceToVrf(2, FALSE);
                 L3VpnIfConfTableShow(CliHandle, NULL);
                  
                 /* Clean Up */
                 L3VpnUtUnMapInterfaceToVrf(1);
                 L3VpnUtUnMapInterfaceToVrf(2);
                 L3VpnDeleteVrf ();

            }
            break;

        case 97:
            {
                /* Rainy day cae for L3VpnCliPrintIfConfInfo()*/
                L3VpnCliPrintIfConfInfo(CliHandle, NULL);
            }
            break;
        case 98:
            {
               /* Sunny day case  for L3vpnCliProcessShowMplsL3VpnVrfAll() */
               /* Create VRF */
               L3VpnUtCreateVrf ();
               MGMT_UNLOCK ();
               CliExecuteAppCmd("c t");
               CliExecuteAppCmd("ip vrf cust-1 mpls");
               CliExecuteAppCmd("rd 1.1.1.1:1");
               CliExecuteAppCmd ("en");
               MGMT_LOCK ();

               MGMT_UNLOCK ();
               CliExecuteAppCmd("c t");
               CliExecuteAppCmd("ip vrf cust-2");
               CliExecuteAppCmd("ip vrf cust-2 mpls");
               CliExecuteAppCmd("rd 200:1");
               CliExecuteAppCmd ("en");
               MGMT_LOCK ();


               L3vpnCliProcessShowMplsL3VpnVrfAll(CliHandle);
               L3VpnDeleteVrf ();
               MGMT_UNLOCK ();
               CliExecuteAppCmd("c t");
               CliExecuteAppCmd("no ip vrf cust-2 mpls");
               CliExecuteAppCmd("no ip vrf cust-2");
               CliExecuteAppCmd ("en");
               MGMT_LOCK ();
            }
           break;

         case 99:
             {
                 /*Rainy day case for L3vpnCliProcessShowMplsL3VpnVrfAll() */
                 L3vpnCliProcessShowMplsL3VpnVrfAll(CliHandle);

             }
            break;       

         case 100:
             {
                 /* Rainy day test case for L3vpnCliProcessShowMplsL3VpnVrf() */
                 MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                 MEMCPY(au1VrfName, VRF_NAME, STRLEN (VRF_NAME));

                 L3vpnCliProcessShowMplsL3VpnVrf (CliHandle, au1VrfName, STRLEN(au1VrfName));
             }    
             break; 
         case 101:
             {
                 /* Sunny day UT for L3VpnRteTableShow() */
                 L3VpnUtCreateVrf ();
   
                 L3VpnUtAddIngressRoute();
                 L3VpnAddRouteAtIngress(2);
                 L3VpnAddRouteAtIngress(3);
                 L3VpnAddRouteAtIngress(4);
                 L3VpnAddRouteAtIngress(5);

                 MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                 MEMCPY(au1VrfName, VRF_NAME, STRLEN (VRF_NAME));
                 L3VpnRteTableShow(CliHandle, au1VrfName);
             
                 /* Clean up */
                 L3VpnDeleteRouteAtIngress(1);
                 L3VpnDeleteRouteAtIngress(2);
                 L3VpnDeleteRouteAtIngress(3);
                 L3VpnDeleteRouteAtIngress(4);
                 L3VpnDeleteRouteAtIngress(5);
                 L3VpnUtLspCleanUp();
                 L3VpnDeleteVrf(); 
             }
             break;
          
        case 102:
            {
                /* Rainy day case for L3VpnPrintRteEntry () */
                L3VpnPrintRteEntry(CliHandle, NULL);
            }
           break;

        case 103:
            {
                /* Rainy day case for L3VpnPrintRteEntry () */
                tL3vpnMplsL3VpnVrfRteEntry L3vpnMplsL3VpnVrfRteEntry;
                MEMSET(&L3vpnMplsL3VpnVrfRteEntry, 0, sizeof(tL3vpnMplsL3VpnVrfRteEntry));
                L3VpnPrintRteEntry (CliHandle, &L3vpnMplsL3VpnVrfRteEntry);
            }

        case 104:
            {
                 /* Suuny day test case for L3VpnRteTableShow() */
                 L3VpnUtCreateVrf ();

                 L3VpnUtAddIngressRoute();
                 L3VpnAddRouteAtIngress(2);
                 L3VpnAddRouteAtIngress(3);
                 L3VpnAddRouteAtIngress(4);
                 L3VpnAddRouteAtIngress(5);

                 L3VpnRteTableShow(CliHandle, NULL);

                 /* Clean up */
                 L3VpnDeleteRouteAtIngress(1);
                 L3VpnDeleteRouteAtIngress(2);
                 L3VpnDeleteRouteAtIngress(3);
                 L3VpnDeleteRouteAtIngress(4);
                 L3VpnDeleteRouteAtIngress(5);
                 L3VpnUtLspCleanUp();
                 L3VpnDeleteVrf();
             }
             break;
           
        case 105:
            {
                L3VpnUtCreateVrf ();
                L3VpnUtAddIngressRoute();
                
                MGMT_UNLOCK();
                CliExecuteAppCmd("end");
                CliExecuteAppCmd ("c t");
                CliExecuteAppCmd ("ip vrf cust-2");
                CliExecuteAppCmd ("ip vrf cust-2 mpls");
                CliExecuteAppCmd("mpls-bgp add route destination 10.0.0.1 prefix-len 24 next-hop "
                                      "40.0.0.0 next-hop-as 1 ifindex 10 metric 100 vpn-label 700");
                CliExecuteAppCmd ("en");
                MGMT_LOCK ();   
            
                MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                L3VpnRteTableShow (CliHandle, au1VrfName);

                /* Clean Up */
               L3VpnDeleteRouteAtIngress(1);
               L3VpnUtLspCleanUp();
               L3VpnDeleteVrf();
               
               MGMT_UNLOCK ();
               CliExecuteAppCmd("end");
               CliExecuteAppCmd ("c t");
               CliExecuteAppCmd ("no ip vrf cust-2 mpls");
               CliExecuteAppCmd ("no ip vrf cust-2");
               CliExecuteAppCmd ("en");
               MGMT_LOCK ();
            }
            break; 
        case 106:
            {
               /* UT case for CliL3VpnConvertTimetoString() */
               UINT4    u4Time = 0;
               UINT1    u1Time[50];
               MEMSET (u1Time, 0, 50);

               CliL3VpnConvertTimetoString(u4Time, (INT1 *)u1Time);
            }
            break;

  
		case 107:
		{
                    tMplsL3VpnRegInfo   MplsL3VpnRegInfo;
                    MEMSET(&MplsL3VpnRegInfo,0,sizeof(tMplsL3VpnRegInfo));
                    u4OsixRetVal=MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                            u4RetVal=OSIX_FAILURE;
                            break;
                    }
                    u4OsixRetVal=MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                            u4RetVal=OSIX_FAILURE;
                            break;
                    }	
		}
		break;
		case 108:
		{
			/******* L3VpnDeleteXcEntry *******/
			UINT4 u4OutIndex=0;
			UINT4 u4XcIndex=0;
			UINT4 u4InIndex=0;

			/* Allocating the outsegemnt index */
			u4OutIndex = MplsOutSegmentGetIndex ();
			/** allocating the Insegment Index */
			u4InIndex = MplsInSegmentGetIndex();
			/* Allocating the Xc Index */
			u4XcIndex = MplsXcTableGetIndex ();
			u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
			/* Creating the XC with insegemnt = NULL */
			MplsCreateXCTableEntry (u4XcIndex,u4InIndex, u4OutIndex);

			L3VpnDeleteXcEntry(u4OutIndex+1,u4XcIndex+1,u4InIndex+1);

			/** delete the complete configuration **/
			L3VpnDeleteXcEntry(u4XcIndex,u4InIndex,u4OutIndex);
		}
		break;
		case 109:
		{
                    /*****L3VpnBgp4RouteAddDelAtEgress***/
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
		    }
		    MplsL3VpnBgp4RouteInfo.u1ILMAction=L3VPN_BGP4_ROUTE_DEL+1;
		    u4OsixRetVal=L3VpnBgp4RouteAddDelAtEgress(&MplsL3VpnBgp4RouteInfo);
		    if(u4OsixRetVal!=OSIX_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }   
		    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
		    if(u4OsixRetVal != OSIX_SUCCESS)
		    {
			    u4RetVal=OSIX_FAILURE;
			    break;
		    }

		}
		break;
		case 110:
		{
                    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
                    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                    MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

                    L3vpnCreateIpVrf();
                    L3vpnCreateMplsVrf();

                    u4OsixRetVal=L3vpnFillDefaultEgressPerVrfAdd(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
		    MplsL3VpnBgp4RouteInfo.u1ILMAction=L3VPN_BGP4_ROUTE_DEL+1;
                    u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                    if(u4OsixRetVal!=OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }
                    /*** clear the routes **/
                    u4OsixRetVal=L3VpnClearBgpRouteLabelTable();
                    if(u4OsixRetVal != OSIX_SUCCESS)
                    {
                        u4RetVal=OSIX_FAILURE;
                        break;
                    }   
                    L3vpnDeleteMplsVrf();
                    L3vpnDeleteIpVrf();
			
		}
		break;
		case 111:
		{
                        tL3vpnMplsL3VpnVrfEntry         L3vpnMplsL3VpnVrfEntry;
                        tL3vpnIsSetMplsL3VpnVrfEntry    L3vpnIsSetMplsL3VpnVrfEntry;
                        INT4                            i4RowStatusLogic=OSIX_FALSE;
                        INT4                            i4RowCreateOption=OSIX_FALSE;
                        UINT4                           u4ErrorCode=0;

                        MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
                        MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,0,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));

                        /*** Row Testing for Row Destroy  When VRF entry is not there **/
                        L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfVpnId=OSIX_FALSE;
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfDescription=OSIX_FALSE;

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
		}
		break;
		case 112:
		{
                        tL3vpnMplsL3VpnVrfEntry         L3vpnMplsL3VpnVrfEntry;
                        tL3vpnIsSetMplsL3VpnVrfEntry    L3vpnIsSetMplsL3VpnVrfEntry;
                        INT4                            i4RowStatusLogic=OSIX_FALSE;
                        INT4                            i4RowCreateOption=OSIX_FALSE;
                        UINT4                           u4ErrorCode=0;

                        MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
                        MEMSET(&L3vpnIsSetMplsL3VpnVrfEntry,0,sizeof(tL3vpnIsSetMplsL3VpnVrfEntry));

                        /*** Row Testing for Row Destroy  When VRF entry is not there **/
                        L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfVpnId=OSIX_FALSE;
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfDescription=OSIX_FALSE;
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfRD=OSIX_FALSE;
			L3VPN_VRF_ROW_STATUS(L3vpnMplsL3VpnVrfEntry)=ACTIVE;

			L3vpnCreateIpVrf();

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfRD=OSIX_TRUE;
                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal!=OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfRD=OSIX_FALSE;
			L3VPN_VRF_ROW_STATUS(L3vpnMplsL3VpnVrfEntry)=NOT_READY;

                        u4OsixRetVal=L3vpnTestAllMplsL3VpnVrfTable(&u4ErrorCode,&L3vpnMplsL3VpnVrfEntry,
                                        &L3vpnIsSetMplsL3VpnVrfEntry,i4RowStatusLogic,i4RowCreateOption);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			
			
			L3vpnDeleteIpVrf();
		}
		break;

            case 113:
            {
                /* Rainy day case for L3vpnTestMplsL3VpnNotificationEnable() */

                UINT4 u4ErrorCode = 0;
                if(L3vpnTestMplsL3VpnNotificationEnable(&u4ErrorCode, 0) !=
                        OSIX_FAILURE)
                {
                    u4RetVal = OSIX_FAILURE;
                }

            }
            break;

          case 114:
             {
                 /* Rainy day case for L3VpnInternalEventHandler() */
                 UINT1              *pu1TmpMsg = NULL;
                 pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);

                 L3VpnUtFillMsgQueue();
                 /* The above msg is freed inside the function */
                 if(L3VpnInternalEventHandler(pu1TmpMsg) != L3VPN_FAILURE)
                 {
                      u4RetVal = OSIX_FAILURE;
                 }
                 /* Clean Up */
                 L3VpnUtFreeMsgQueue();
             }
             break;

           case 115:
               {
                   /* Rainy day case for L3VpnNotifyContextChange() */
                   L3VpnUtFillMsgQueue ();
                   L3VpnNotifyContextChange(1,1,1);     
                   L3VpnUtFreeMsgQueue();
               }
               break;

           case 116:
               {
                   /* Rainy day case for L3VpnIfStChgEventHandler() */ 
                   L3VpnUtFillMsgQueue ();
                   L3VpnIfStChgEventHandler (1, 1);
                   L3VpnUtFreeMsgQueue ();
               }
               break;
           case 117:
               {
                   /* Rainy day test case for L3VpnAdminChangeEventHandler() */
                   MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                   MEMCPY(au1VrfName, VRF_NAME, STRLEN (VRF_NAME));
                   L3VpnUtFillMsgQueue();
                   L3VpnAdminChangeEventHandler(au1VrfName, 1);
                   L3VpnUtFreeMsgQueue ();
               }
               break;

           case 118:
               {
                   /* rainy day test case for L3VpnNonTeLspStatusChangeEventHandler() */
                   L3VpnUtFillMsgQueue ();
                   L3VpnNonTeLspStatusChangeEventHandler(500, 1, 1);
                   L3VpnUtFreeMsgQueue ();
               }
               break;
           case 119:
               {
                   /* Rainy day test case for L3VpnIfConfMapUnmapEventHandler() */
                   L3VpnUtFillMsgQueue();
                   L3VpnIfConfMapUnmapEventHandler(1, 1, 1);
                   L3VpnUtFreeMsgQueue();
               } 
             break;

           case 120:
               {
                   /* rainy day test case for L3VpnChckAndSendRouteNotifications() */
                   tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                   tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;

                   if(L3VpnChckAndSendRouteNotifications (NULL, &L3vpnMplsL3VpnVrfPerfEntry,
                            L3VPN_ROUTE_ADDED) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
                   if(L3VpnChckAndSendRouteNotifications (&L3vpnMplsL3VpnVrfEntry, NULL, 
                             L3VPN_ROUTE_ADDED) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }

               }
              break;

           case 121:
               {
                  /* Sunny day case for L3VpnChckAndSendRouteNotifications () */
                  tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                  tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;

                  MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                  MEMSET(&L3vpnMplsL3VpnVrfPerfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfPerfEntry));
                  MGMT_UNLOCK ();
                  CliExecuteAppCmd ("c t");
                  CliExecuteAppCmd ("mpls l3vpn trap disable");
                  CliExecuteAppCmd("en");
                  MGMT_LOCK ();

                  if(L3VpnChckAndSendRouteNotifications(&L3vpnMplsL3VpnVrfEntry,
                                             &L3vpnMplsL3VpnVrfPerfEntry, L3VPN_ROUTE_ADDED) != L3VPN_SUCCESS)
                  {
                      u4RetVal = OSIX_FAILURE;
                  }
               }
               break;
            case 122:
                {
                  
                   tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                   tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
                   MGMT_UNLOCK ();
                   CliExecuteAppCmd ("c t");
                   CliExecuteAppCmd ("mpls l3vpn trap enable");
                   CliExecuteAppCmd("en");
                   MGMT_LOCK ();


                   MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                   MEMSET(&L3vpnMplsL3VpnVrfPerfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfPerfEntry));

                   if(L3VpnChckAndSendRouteNotifications(&L3vpnMplsL3VpnVrfEntry,
                                              &L3vpnMplsL3VpnVrfPerfEntry, 4) != L3VPN_FAILURE)
                   {
                      u4RetVal = OSIX_FAILURE;
                   }
                }
               break;
            case 123:
                {
                    /* Rainy day case for L3VpnUpdateNoOfConnectedInterfaces() */
                    if(L3VpnUpdateNoOfConnectedInterfaces (3) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
               break;
            case 124:
                {
                    /* Rainy day test case for L3VpnNotifyContextChange() */
                    L3VpnUtExhaustQMsgPool();
                    L3VpnNotifyContextChange(1, 1, 1);

                    /* Clean Up */
                    L3VpnFreeQMem ();   
                }
               break;
            case 125:
                {
                   /* Rainy day case for L3vpnUtlHandleRowStatusNotInService() */
                   tL3vpnMplsL3VpnVrfEntry  *pVrfEntry = NULL;
                   L3VpnUtCreateVrf ();
        
                   MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                   MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                   pVrfEntry = MplsL3VpnApiGetVrfTableEntry (au1VrfName, STRLEN (VRF_NAME));
                   if(pVrfEntry == NULL)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }

                   if(L3vpnUtlHandleRowStatusNotInService(NULL) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
                }
               break;

             case 126:
                 {
                     /* Sunny day case for L3vpnUtlHandleRowStatusNotInService()
                        Callback is NOT NULL */
                    tL3vpnMplsL3VpnVrfEntry  *pVrfEntry = NULL;
                    tMplsL3VpnRegInfo RegInfo;
                    UINT2    u2Mask  = 0;

                    MEMSET(&RegInfo, 0, sizeof(tMplsL3VpnRegInfo));
                    L3VpnUtCreateVrf ();
                    L3VpnUtAddRdToVrf();
                    L3VpnUtMapInterfaceToVrf(1, TRUE);                                  
 
                    L3vpnCliProcessShowMplsL3VpnVrfAll(CliHandle); 
                    RegInfo.pBgp4MplsL3VpnNotifyRouteParams = L3VpnUtNotifyRouteParams;
                    RegInfo.pBgp4MplsL3VpnNotifyLspStatusChange = L3VpnUtNotifyLspStatusChange;
                    RegInfo.pBgp4MplsL3vpnNotifyVrfAdminStatusChange = L3VpnUtNotifyVrfAdminStatusChange;
                    
                    u2Mask  = L3VPN_BGP4_ROUTE_PARAMS_REQ | L3VPN_BGP4_LSP_STATUS_REQ |
                              L3VPN_BGP4_VRF_ADMIN_STATUS_REQ;

                    RegInfo.u2InfoMask = u2Mask;
                    MplsL3VpnRegisterCallback(&RegInfo);
                    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                    MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                    pVrfEntry = MplsL3VpnApiGetVrfTableEntry (au1VrfName, STRLEN (VRF_NAME));
                    if(pVrfEntry == NULL)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    L3vpnUtlHandleRowStatusNotInService(pVrfEntry);
                    
                    /* Clean Up */
                    MplsL3VpnDeRegisterCallback(&RegInfo);
                    L3VpnUtUnMapInterfaceToVrf (1);
                    L3VpnDeleteVrf(); 
 
                 }            
                break;
              case 127:
                  {
                    /* Rainy day case for L3vpnUtlHandleRowStatusNotInService() 
                       Callback is NULL*/
                    tL3vpnMplsL3VpnVrfEntry  *pVrfEntry = NULL;
                    tMplsL3VpnRegInfo RegInfo;

                    L3VpnUtCreateVrf ();
                    L3VpnUtAddRdToVrf();
                    L3VpnUtMapInterfaceToVrf(1, TRUE);

                    L3vpnCliProcessShowMplsL3VpnVrfAll(CliHandle);


                    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                    MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                    pVrfEntry = MplsL3VpnApiGetVrfTableEntry (au1VrfName, STRLEN (VRF_NAME));
                    if(pVrfEntry == NULL)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    L3vpnUtlHandleRowStatusNotInService(pVrfEntry);

                    /* Clean Up */
                    MplsL3VpnDeRegisterCallback(&RegInfo);
                    L3VpnUtUnMapInterfaceToVrf (1);
                    L3VpnDeleteVrf();

                  }
                 break;
             case 128:
                 {
                     L3VpnUtExhaustIfConfMemPool();
                     
                     if(L3VpnCreateMplsL3VpnIfConfEntry(1, 1) != NULL)
                     {
                          u4RetVal = OSIX_FAILURE;
                     }
                     L3VpnUtFreeIfConfMemPool(); 
                 }
                break;
            case 129:
                {
                   /* Rainy test case for L3VpnIsRouteAdditionAllowed() */
                   tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                   tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
 
                   MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                   MEMSET(&L3vpnMplsL3VpnVrfPerfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfPerfEntry));

                   if(L3VpnIsRouteAdditionAllowed(&L3vpnMplsL3VpnVrfEntry, NULL) != OSIX_FALSE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
                  if(L3VpnIsRouteAdditionAllowed(NULL, &L3vpnMplsL3VpnVrfPerfEntry) != OSIX_FALSE)
                  {
                      u4RetVal = OSIX_FAILURE;
                  }

                }
               break;
            case 130:
                {
                    /* Rainy day case for L3vpnUtlProcessVrfCreation() */
                    
                    if(L3vpnUtlProcessVrfCreation(NULL) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
               break;
 
            case 131:
                {
                     /* rainy day test case for L3vpnUtlProcessVrfCreation() */
                     L3VpnUtExhaustSecMemPool();
                     L3VpnUtCreateVrf ();

                     /* Clean Up */
                     L3VpnUtFreeSecMemPool ();
                     L3VpnDeleteVrf ();
                    
                }
               break;

           case 132:
               {
                    /* rainy day test case for L3vpnUtlProcessVrfCreation() */
                    L3VpnUtExhaustPerfMemPool();
                    L3VpnUtCreateVrf ();
         
                    /* Clean Up */
                    L3VpnUtFreePerfMemPool ();
                    L3VpnDeleteVrf();
                   /* No clean up requirted for VRF since it failed to */ 
                   /*   create the VRF intentinally */                    
               }
             break;
           case 133:
               {
                    tL3vpnMplsL3VpnVrfEntry  *pVrfEntry = NULL;
                    tMplsL3VpnRegInfo RegInfo;
                    UINT2    u2Mask  = 0;

                    MEMSET(&RegInfo, 0, sizeof(tMplsL3VpnRegInfo));

                    RegInfo.pBgp4MplsL3VpnNotifyRouteParams = L3VpnUtNotifyRouteParams;
                    RegInfo.pBgp4MplsL3VpnNotifyLspStatusChange = L3VpnUtNotifyLspStatusChange;
                    RegInfo.pBgp4MplsL3vpnNotifyVrfAdminStatusChange = L3VpnUtNotifyVrfAdminStatusChange;

                    u2Mask  = L3VPN_BGP4_ROUTE_PARAMS_REQ | L3VPN_BGP4_LSP_STATUS_REQ |
                               L3VPN_BGP4_VRF_ADMIN_STATUS_REQ;

                    RegInfo.u2InfoMask = u2Mask;
                    MplsL3VpnRegisterCallback(&RegInfo);
                    L3VpnUtCreateVrf();
                
                    /* Clean Up */
                    MplsL3VpnDeRegisterCallback(&RegInfo);
                    L3VpnDeleteVrf();
 
               }
              break;

           case 134:
               {
                    /* Rainy day test case for L3vpnUtlGetVrfPerfTableEntry() */
                    if(L3vpnUtlGetVrfPerfTableEntry (NULL) != NULL)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
               }
              break;
           case 135:
               {
                   /* Rainy day case for L3vpnUtlVrfTableAdminStatusHdl() */
                   tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                   MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                   L3vpnMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfConfAdminStatus = 
                          L3VPN_VRF_ADMIN_STATUS_UP;
                   L3vpnMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfConfRowStatus = NOT_IN_SERVICE;
                   if(L3vpnUtlVrfTableAdminStatusHdl(NULL, &L3vpnMplsL3VpnVrfEntry) != L3VPN_SUCCESS)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
               }
               break;

            case 136:
                {
                    /* Rainy day case for L3vpnUtlVrfTableAdminStatusHdl() */
                    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                    MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                    /*set invalid Admin stataus */
                    L3vpnMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfConfAdminStatus = 3;

                    if(L3vpnUtlVrfTableAdminStatusHdl (NULL, &L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
               break;
           case 137:
               {
                   /* Rainy day case for L3VpnDeleteRtTableForVrf() */
                   if(L3VpnDeleteRtTableForVrf(NULL) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
               }
              break;
           case 138:
               {
                   MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                   MEMCPY(au1VrfName, "cust-5", STRLEN("cust-5"));
                   if(L3VpnDeleteRtTableForVrf (au1VrfName) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   } 
               }
             break;
           case 139:
               {
                  L3VpnUtCreateVrf();
                  L3VpnAddRtToVrf();
                  MGMT_UNLOCK (); 
                  CliExecuteAppCmd ("c t");
                  CliExecuteAppCmd ("ip vrf cust-2");
                  CliExecuteAppCmd ("ip vrf cust-2 mpls");
                  CliExecuteAppCmd ("route-target both 500:1");
                  CliExecuteAppCmd ("en");                 
                  MGMT_LOCK ();

                  MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                  MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                  if(L3VpnDeleteRtTableForVrf (au1VrfName) != L3VPN_SUCCESS)
                  {
                      u4RetVal = OSIX_FAILURE;
                  }

                  /* Clean Up */
                  L3VpnDeleteRtFromVrf ();
                  L3VpnDeleteVrf ();
                  MGMT_UNLOCK ();
                  CliExecuteAppCmd ("c t");
                  CliExecuteAppCmd ("ip vrf cust-2 mpls");
                  CliExecuteAppCmd ("no route-target 500:1");
                  CliExecuteAppCmd ("en");
                  CliExecuteAppCmd ("c t");
                  CliExecuteAppCmd ("no ip vrf cust-2 mpls");
                  CliExecuteAppCmd ("no ip vrf cust-2");
                  CliExecuteAppCmd ("en"); 
                  MGMT_LOCK ();
               }
              break;
            case 140:
                {
                   /* Rainy day test case for L3VpnGetIfConfEntryWithIfIndex() */
                   /* pass invalid ifIndex */
                   if(L3VpnGetIfConfEntryWithIfIndex(9) != NULL)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
                }
                break;            
            case 141:
                {
                    /* rainy day case for L3VpnGetIfConfEntryWithIfIndex() */
                    MGMT_UNLOCK ();
                    CliExecuteAppCmd ("c t");
                    CliExecuteAppCmd ("ip vrf cust-1");
                    CliExecuteAppCmd ("en");
                    CliExecuteAppCmd ("c t");
                    CliExecuteAppCmd ("i g 0/1");
                    CliExecuteAppCmd ("no sw");
                    CliExecuteAppCmd ("ip vrf forwarding cust-1");
                    CliExecuteAppCmd ("en");
                    MGMT_LOCK ();
         
                    if(L3VpnGetIfConfEntryWithIfIndex (1) != NULL)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                   
                    /* Clean -Up */
                    MGMT_UNLOCK ();
                    CliExecuteAppCmd ("c t");
                    CliExecuteAppCmd ("i g 0/1");
                    CliExecuteAppCmd ("no ip vrf forwarding cust-1");
                    CliExecuteAppCmd ("en");
                    CliExecuteAppCmd ("c t"); 
                    CliExecuteAppCmd ("no ip vrf cust-1");
                    CliExecuteAppCmd ("en"); 
                    MGMT_LOCK ();
                }
               break;
             case 142:
                 {
                     /* Rainy day case for L3VpnGetVrfEntryByContextId() */
                     L3VpnUtCreateVrf();
                     if(L3VpnGetVrfEntryByContextId (2) != NULL)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }
 
                     /* Clean Up */
                     L3VpnDeleteVrf (); 
                 }
                break;
             case 143:
                 {
                     /* Sunny day case for L3VpnGetVrfEntryByContextId() */
                     L3VpnUtCreateVrf ();
                     if(L3VpnGetVrfEntryByContextId (1) == NULL)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }

                     /* Clean Up */
                     L3VpnDeleteVrf ();
                 } 
                break;
             case 144:
                 {
                     /* Rainy day case for L3VpnCreateMplsL3VpnIfConfEntry () */
                     if(L3VpnCreateMplsL3VpnIfConfEntry(1, 3) != NULL)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }
                 }
                break;
             case 145:
                 {
                     /* rainy day case for L3VpnGetRTEntryFromValue()  */
                    if(L3VpnGetRTEntryFromValue(NULL, NULL) != NULL)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                 }
                break;
            case 146:
                {
                   /* Rainy day case for L3VpnFlushRouteTableForVrf() */
                   if(L3VpnFlushRouteTableForVrf (NULL) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
                }
               break;
            case 147:
                {
                    /* Rainy day case for L3VpnDeleteRouteEntry() */
                    if(L3VpnDeleteRouteEntry (NULL) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
               break;

            case 148:
                {
                    /* Rainy day case for L3VpnDeleteRouteEntry() */
                    tL3vpnMplsL3VpnVrfRteEntry L3vpnMplsL3VpnVrfRteEntry;
                    MEMSET(&L3vpnMplsL3VpnVrfRteEntry, 0, sizeof(tL3vpnMplsL3VpnVrfRteEntry));
                    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                    MEMCPY(au1VrfName, "cust-5", STRLEN("cust-5"));
                    MEMCPY(L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfRteEntry), au1VrfName, STRLEN(au1VrfName));
                    L3VpnDeleteRouteEntry (&L3vpnMplsL3VpnVrfRteEntry); 
                }
               break;
            case 149:
                {
                    /* Rainy day case for L3vpnUtlProcessVrfDeletion() */
                    if(L3vpnUtlProcessVrfDeletion (NULL) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                } 
                break;

            case 150:
                {
                   /* Rainy day case for L3vpnUtlProcessVrfDeletion() */
                   /* Deleting a VRF which does not exist */
                   tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
 
                   MEMSET(&L3vpnMplsL3VpnVrfEntry,0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                   MEMCPY(L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), "cust-5", STRLEN("cust-5"));
                   if(L3vpnUtlProcessVrfDeletion (&L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
                } 
               break;
            case 151:
                {
                   /* Rainy day case for L3vpnUtlProcessVrfDeletion() */
                   /* The Perf Entry is lready deleted before hand */
                   tL3vpnMplsL3VpnVrfEntry      L3vpnMplsL3VpnVrfEntry;
                   tL3vpnMplsL3VpnVrfPerfEntry  *pL3vpnMplsL3VpnVrfSecEntry = NULL;
                   tL3vpnMplsL3VpnVrfPerfEntry  *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
                   tL3vpnMplsL3VpnVrfEntry      *pL3vpnMplsL3VpnVrfEntry = NULL;
 
                   L3VpnUtCreateVrf ();
                   MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
 
                   MEMCPY(L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), VRF_NAME, STRLEN(VRF_NAME));
                   L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry) = STRLEN(VRF_NAME);
                   pL3vpnMplsL3VpnVrfSecEntry = L3vpnUtlGetVrfSecTableEntry(&L3vpnMplsL3VpnVrfEntry);
                   if(L3vpnUtlVrfSecTableRowDelete (pL3vpnMplsL3VpnVrfSecEntry) != L3VPN_SUCCESS)
                   {
                        u4RetVal = OSIX_FAILURE;
                   }
                   if(L3vpnUtlProcessVrfDeletion (&L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }

                   /* Clean Up */
                   /* Remove VRF Perf entry */
                   pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(&L3vpnMplsL3VpnVrfEntry);
                   L3vpnUtlVrfPerfTableRowDelete(pL3vpnMplsL3VpnVrfPerfEntry);
                   L3VPN_CONFIGURED_VRFS--;

                   
                   pL3vpnMplsL3VpnVrfEntry = MplsL3VpnApiGetVrfTableEntry (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), 
                         L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry));
                   /* Remove VRF Entry */ 
                   RBTreeRem(L3VPN_VRF_TABLE, pL3vpnMplsL3VpnVrfEntry);
                   MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *)pL3vpnMplsL3VpnVrfEntry); 

                }
              break;
            case 152:
                {
                     /* Rainy day case for L3vpnUtlProcessVrfDeletion() */
                    /* The Perf Entry is lready deleted before hand */
                    tL3vpnMplsL3VpnVrfEntry      L3vpnMplsL3VpnVrfEntry;
                    tL3vpnMplsL3VpnVrfPerfEntry  *pL3vpnMplsL3VpnVrfSecEntry = NULL;
                    tL3vpnMplsL3VpnVrfPerfEntry  *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
                    tL3vpnMplsL3VpnVrfEntry      *pL3vpnMplsL3VpnVrfEntry = NULL;

                    L3VpnUtCreateVrf ();
                    MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));

                    MEMCPY(L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), VRF_NAME, STRLEN(VRF_NAME));
                    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry) = STRLEN(VRF_NAME);
                    pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(&L3vpnMplsL3VpnVrfEntry);
                    if(L3vpnUtlVrfPerfTableRowDelete(pL3vpnMplsL3VpnVrfPerfEntry) != L3VPN_SUCCESS)
                    {
                         u4RetVal = OSIX_FAILURE;
                    }
                    if(L3vpnUtlProcessVrfDeletion (&L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }

                    /* Clean Up */
                    L3VPN_CONFIGURED_VRFS--;

                    pL3vpnMplsL3VpnVrfEntry = MplsL3VpnApiGetVrfTableEntry (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry),
                          L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry));
                    /* Remove VRF Entry */
                    RBTreeRem(L3VPN_VRF_TABLE, pL3vpnMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                 (UINT1 *)pL3vpnMplsL3VpnVrfEntry);
                }
              break;
           case 153:
               {
                   /* Rainy day case for MplsL3VpnHandleEgressMapArpResolveTmrEvent() */
                   MplsL3VpnHandleEgressMapArpResolveTmrEvent(NULL);
               } 
              break;
           case 154:
               {
                   /* Sunny day case for MplsL3VpnHandleEgressMapArpResolveTmrEvent() */
                   L3VpnUtCreateVrf();                   
                   MGMT_UNLOCK();
                   CliExecuteAppCmd("mpls l3vpn egress-route "
                              "add per-route cxt-id 1 interface-idx 1 nextHop-Ip 20.0.0.1 label 555"); 
		   MGMT_LOCK();
                   /* Resolve Arp */
                   MGMT_UNLOCK ();
                   CliExecuteAppCmd("c t");
                   CliExecuteAppCmd("arp 20.0.0.1 00:02:02:03:04:01 gi 0/1");
                   CliExecuteAppCmd("en");
                   MGMT_LOCK ();
                   MplsL3VpnHandleEgressMapArpResolveTmrEvent(NULL); 

                   /* Clean Up */
                   MGMT_UNLOCK();
                   CliExecuteAppCmd("mpls l3vpn egress-route delete label 555 cxt-id 1");
                   MGMT_LOCK();
                   L3VpnDeleteVrf();
                  
               }
              break;
           case 155:
               {
                   /* Rainy day case for MplsL3VpnHandleEgressMapArpResolveTmrEvent() */
                   L3VpnUtCreateVrf ();
                   MGMT_UNLOCK ();
                   CliExecuteAppCmd("mpls l3vpn egress-route add per-vrf vrf-id 1 label 600");
                   MGMT_LOCK ();

                   MplsL3VpnHandleEgressMapArpResolveTmrEvent (NULL);
                   
                   MGMT_UNLOCK ();
                   CliExecuteAppCmd("mpls l3vpn egress-route delete label 600 cxt-id 1");
                   MGMT_LOCK ();
                   L3VpnDeleteVrf ();
               }
              break;
            case 156:
                {
                    /* Rainy day case for L3VpnGetIfConfEntry() */
                    if(L3VpnGetIfConfEntry (NULL, 1)  != NULL)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
              break;
             
             case 157:
                 {
                     /* Rainy day case for L3VpnUpdatePerfTableCounters() */
                     if(L3VpnUpdatePerfTableCounters (NULL, L3VPN_ROUTE_ADDED) != L3VPN_FAILURE)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }
                 }
                break;
            case 158:
                {
                    /* Rainy day case for L3VpnUpdatePerfTableCounters() */
                    L3VpnUtCreateVrf ();
                    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                    MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));
                    if(L3VpnUpdatePerfTableCounters(au1VrfName, 4) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
  
                   /* Clean Up */
                   L3VpnDeleteVrf ();
                }
                break;
            case 159:
                {
                    /* rainy day case for L3vpnUtlHandleRowStatusActive() */
                    if(L3vpnUtlHandleRowStatusActive (NULL) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
               break;
            case 160:
                {
                    /* rainy day case for L3vpnUtlHandleRowStatusActive() */
                    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;

                    MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                    MEMCPY(L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), VRF_NAME, STRLEN (VRF_NAME));

                    if(L3vpnUtlHandleRowStatusActive (&L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                }
               break;
            case 161:
                {
                     tMplsL3VpnRegInfo RegInfo;
                     tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                     UINT2    u2Mask  = 0;

                     MEMSET(&RegInfo, 0, sizeof(tMplsL3VpnRegInfo));
                     L3VpnUtCreateVrf ();

                     RegInfo.pBgp4MplsL3VpnNotifyRouteParams = L3VpnUtNotifyRouteParams;
                     RegInfo.pBgp4MplsL3VpnNotifyLspStatusChange = L3VpnUtNotifyLspStatusChange;
                     RegInfo.pBgp4MplsL3vpnNotifyVrfAdminStatusChange = L3VpnUtNotifyVrfAdminStatusChange;

                     u2Mask  = L3VPN_BGP4_ROUTE_PARAMS_REQ | L3VPN_BGP4_LSP_STATUS_REQ |
                               L3VPN_BGP4_VRF_ADMIN_STATUS_REQ;

                     RegInfo.u2InfoMask = u2Mask;
                     MplsL3VpnRegisterCallback(&RegInfo);
                     MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                     MEMCPY(L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), VRF_NAME, STRLEN (VRF_NAME));

                     if(L3vpnUtlHandleRowStatusActive(&L3vpnMplsL3VpnVrfEntry) != L3VPN_SUCCESS)
                     {
                          u4RetVal = OSIX_FAILURE;
                     }
                }
                break;
             case 162:
                 {
                     /* Sunny day case for L3vpnGetFirstBgpRouteLabelTable() */
                     tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
                     L3VpnUtCreateVrf ();

                     MGMT_UNLOCK ();
                     CliExecuteAppCmd ("mpls l3vpn egress-route add per-vrf vrf-id 1 label 700");
                     MGMT_LOCK ();
 
                     pL3VpnBgpRouteLabelEntry = L3vpnGetFirstBgpRouteLabelTable();
                     if(pL3VpnBgpRouteLabelEntry == NULL)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }
                     
                     /* clean Up */
                      MGMT_UNLOCK ();
                      CliExecuteAppCmd ("mpls l3vpn egress-route delete label 700 cxt-id 1");
                      MGMT_LOCK ();
                      L3VpnDeleteVrf ();
                 } 
             case 163:
                 {
                   /* Rainy day cse for L3vpnUtlVrfSecTableRowDelete() */
                   UINT4 u4SecPoolId = L3VPN_MPLSL3VPNVRFSECTABLE_POOLID;
                   
                   tL3vpnMplsL3VpnVrfSecEntry L3vpnMplsL3VpnVrfSecEntry; 
                   MEMSET(&L3vpnMplsL3VpnVrfSecEntry, 0, sizeof(tL3vpnMplsL3VpnVrfSecEntry));
                   L3VPN_MPLSL3VPNVRFSECTABLE_POOLID = 0;
                   if(L3vpnUtlVrfSecTableRowDelete (&L3vpnMplsL3VpnVrfSecEntry)  != L3VPN_FAILURE)
                   {
                       u4RetVal = OSIX_FAILURE;
                   }
 
                   /* clean Up */
                   L3VPN_MPLSL3VPNVRFSECTABLE_POOLID = u4SecPoolId;
                 }
               break;
             case 164:
                 {
                    UINT4 u4PerfPoolId = L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID;

                    L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID = 0;
                    /* Rainy day case for L3vpnUtlVrfPerfTableRowDelete() */
                    tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
                    MEMSET(&L3vpnMplsL3VpnVrfPerfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfPerfEntry));
                    
                    if(L3vpnUtlVrfPerfTableRowDelete(&L3vpnMplsL3VpnVrfPerfEntry) !=
                                L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
 
                    /* Clean-up */
                    L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID = u4PerfPoolId;
                 }
                break;
              case 165:
                  {
                      /* Rainy day case for L3VpnDeleteIfConfTableForVrf() */
                     if(L3VpnDeleteIfConfTableForVrf (NULL) != L3VPN_FAILURE)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }                 
                  }
                 break;
             case 166:
                 {
                     L3VpnUtCreateVrf();
                     L3VpnUtMapInterfaceToVrf(1, TRUE);

                     MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                     MEMCPY(au1VrfName, VRF_NAME, STRLEN(VRF_NAME));

                     UINT4 u4IfConfPoolId = L3VPN_MPLSL3VPNIFCONFTABLE_POOLID;

                     L3VPN_MPLSL3VPNIFCONFTABLE_POOLID = 0;
                     if(L3VpnDeleteIfConfTableForVrf(au1VrfName) != L3VPN_FAILURE)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }

                     /* Clean Up */
                     MGMT_UNLOCK();
                     CliExecuteAppCmd("c t");
                     CliExecuteAppCmd("i g 0/1");
                     CliExecuteAppCmd("no ip vrf forwarding cust-1");
                     CliExecuteAppCmd("en");
                     MGMT_LOCK();
                     L3VPN_MPLSL3VPNIFCONFTABLE_POOLID = u4IfConfPoolId;
                     L3VpnDeleteVrf();
                 }
                break;
             case 167:
                 {
                    /* Rainy day case for L3vpnUtlVrfTableRowStatusHdl() */
                    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                    MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                    L3VPN_VRF_ROW_STATUS (L3vpnMplsL3VpnVrfEntry) = 10; /* invalid Row Status */
                    if(L3vpnUtlVrfTableRowStatusHdl (NULL, &L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                 }
                break;
             case 168:
                 {
                    /* Rainy day case for L3vpnUtlVrfTableRowStatusHdl() */
                    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                    tL3vpnMplsL3VpnVrfEntry L3vpnOldMplsL3VpnVrfEntry;

                    MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                    MEMSET(&L3vpnOldMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                
                    L3VPN_VRF_ROW_STATUS (L3vpnMplsL3VpnVrfEntry) = ACTIVE;
                    L3VPN_VRF_ROW_STATUS (L3vpnOldMplsL3VpnVrfEntry) = NOT_IN_SERVICE;
                    if(L3vpnUtlVrfTableRowStatusHdl (&L3vpnOldMplsL3VpnVrfEntry, 
                           &L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                 } 
               break;
             case 169:
                 {
                    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                    tL3vpnMplsL3VpnVrfEntry L3vpnOldMplsL3VpnVrfEntry;

                    MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));
                    MEMSET(&L3vpnOldMplsL3VpnVrfEntry, 0, sizeof(tL3vpnMplsL3VpnVrfEntry));

                    L3VPN_VRF_ROW_STATUS (L3vpnMplsL3VpnVrfEntry) = ACTIVE;
                    L3VPN_VRF_ROW_STATUS (L3vpnOldMplsL3VpnVrfEntry) = ACTIVE;
                    if(L3vpnUtlVrfTableRowStatusHdl (&L3vpnOldMplsL3VpnVrfEntry,
                           &L3vpnMplsL3VpnVrfEntry) != L3VPN_SUCCESS)
                    {
                        u4RetVal = OSIX_FAILURE;
                    } 
                 }
               break;
             case 170:
                 {
                     /* Rainy day case for L3vpnUtlProcessVrfCreation() */
                     tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                     MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, L3VPN_MAX_VRF_NAME_LEN);
                     
                     if(L3vpnUtlProcessVrfCreation (&L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                     {
                          u4RetVal = OSIX_FAILURE;
                     }
                 }
             case 171:
                 {
                     /* rainy day case for L3vpnUtlHandleRowStatusNotInService() */
                     tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
                     MEMSET(&L3vpnMplsL3VpnVrfEntry, 0, L3VPN_MAX_VRF_NAME_LEN);
 
                     if(L3vpnUtlHandleRowStatusNotInService (&L3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                     {
                         u4RetVal = OSIX_FAILURE;
                     }
                 }
                break;
             case 172:
                 {
                    L3VpnUtCreateVrf ();
                    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                    MEMCPY(au1VrfName, VRF_NAME, STRLEN (VRF_NAME));
                    tL3vpnMplsL3VpnVrfEntry  *pL3vpnMplsL3VpnVrfEntry = NULL;

                    pL3vpnMplsL3VpnVrfEntry = 
                      MplsL3VpnApiGetVrfTableEntry (au1VrfName, STRLEN(au1VrfName));
                    if(L3vpnUtlVrfSecTableRowCreate (pL3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }
                    /* Clean Up */
                    L3VpnDeleteVrf ();
                 }
                break;
             case 173:
                 {
                    /* Rainy day case for L3vpnUtlVrfPerfTableRowCreate() */
                    L3VpnUtCreateVrf ();
                    MEMSET(au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
                    MEMCPY(au1VrfName, VRF_NAME, STRLEN (VRF_NAME));
                    tL3vpnMplsL3VpnVrfEntry  *pL3vpnMplsL3VpnVrfEntry = NULL;

                    pL3vpnMplsL3VpnVrfEntry = MplsL3VpnApiGetVrfTableEntry (au1VrfName, STRLEN(au1VrfName));
                    if(L3vpnUtlVrfPerfTableRowCreate (pL3vpnMplsL3VpnVrfEntry) != L3VPN_FAILURE)
                    {
                        u4RetVal = OSIX_FAILURE;
                    }

                    /* Clean Up */
                    L3VpnDeleteVrf ();
                 }
                break;
              case 174:
                  {
                     L3VpnUtCreateVrf ();
                     MGMT_UNLOCK ();
                     CliExecuteAppCmd("mpls l3vpn egress-route add per-vrf vrf-id 1 label 500");
                     CliExecuteAppCmd("mpls l3vpn egress-route add per-vrf vrf-id 1 label 400");
                     CliExecuteAppCmd("mpls l3vpn egress-route add per-vrf vrf-id 1 label 300");
                     CliExecuteAppCmd("mpls l3vpn egress-route add per-vrf vrf-id 1 label 800");
                     MGMT_LOCK (); 
       
                     /* Clean-Up */
                     MGMT_UNLOCK ();
                     CliExecuteAppCmd("mpls l3vpn egress-route delete label 500 cxt-id 1");
                     CliExecuteAppCmd("mpls l3vpn egress-route delete label 400 cxt-id 1");
                     CliExecuteAppCmd("mpls l3vpn egress-route delete label 300 cxt-id 1");
                     CliExecuteAppCmd("mpls l3vpn egress-route delete label 800 cxt-id 1");
                     MGMT_LOCK ();
                     L3VpnDeleteVrf ();
                  }
                break;
             case 175:
                 {
                     /* Rainy day case for L3vpnQueProcessMsgs() */
                     UINT1              *pu1TmpMsg = NULL;
                     tL3VpnQMsg         L3VpnQMsg;
                     MEMSET(&L3VpnQMsg, 0, sizeof(tL3VpnQMsg));

                     L3VpnQMsg.u4MsgType = 0x00000040; /* Invalid Event */

                     pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
                     MEMCPY(pu1TmpMsg, (UINT1 *)&L3VpnQMsg, sizeof(tL3VpnQMsg));               
                     if (OsixQueSend (gL3vpnGlobals.l3vpnQueId, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN) !=
                         OSIX_SUCCESS)
                     {
                         MemReleaseMemBlock (L3VPN_Q_MSG_POOL_ID, pu1TmpMsg);
                     }
                     L3vpnQueProcessMsgs(); 
                 } 
             case 176:
                 {
                     UINT4 u4QueuePoolId = L3VPN_Q_MSG_POOL_ID;

                     UINT1              *pu1TmpMsg = NULL;
                     tL3VpnQMsg         L3VpnQMsg;
                     MEMSET(&L3VpnQMsg, 0, sizeof(tL3VpnQMsg));

                     L3VpnQMsg.u4MsgType = 0x00000040; /* Invalid Event */

                     pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
                     MEMCPY(pu1TmpMsg, (UINT1 *)&L3VpnQMsg, sizeof(tL3VpnQMsg));
                     OsixQueSend (gL3vpnGlobals.l3vpnQueId, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN);
                     L3VPN_Q_MSG_POOL_ID = 0;
                     L3vpnQueProcessMsgs();

                     L3VPN_Q_MSG_POOL_ID = u4QueuePoolId;
                 }
                break;
              case 177:
                  {
                      /* Rainy day case for MplsL3VpnCliVrfChangeMode()*/
                      tCliContext        *pCliContext = NULL;
                      pCliContext = CliGetContext ();
                      pCliContext->i1Status = CLI_INACTIVE;

                      if(MplsL3VpnCliVrfChangeMode(1) != CLI_FAILURE)
                      {
                          u4RetVal = OSIX_FAILURE;
                      }
                      /* Clean -up */
                      pCliContext->i1Status = CLI_ACTIVE;
                  }
                 break;
              case 178:
                  { 
                      UINT4 u4DebugLevel = 0;
                      if(CliGetL3vpnDebugLevel(CliHandle, &u4DebugLevel) != OSIX_SUCCESS)
                      {
                          u4RetVal = OSIX_FAILURE;
                      }
                  }
                  break;
              case 179:
                  {
                      /* Rainy day case for MplsL3VpnGetMplsL3VpnVrfCfgPrompt() */
                      UINT1 au1TestName[10];
                      MEMSET(au1TestName, 0, 10);
                      
                      if(MplsL3VpnGetMplsL3VpnVrfCfgPrompt(au1TestName,  au1TestName) != FALSE)
                      {
                          u4RetVal = OSIX_FAILURE;
                      }
                  }
                  break;
                case 180:
                     {
                          UINT1 au1TestName[10];
                          MEMSET(au1TestName, 0, 10);

                          if(MplsL3VpnGetMplsL3VpnVrfCfgPrompt(au1TestName,  NULL) != FALSE)
                          {
                             u4RetVal = OSIX_FAILURE;
                          }
                     }
                  break;
               case 181:
                   {
                       UINT1 au1TestName[10];
                       MEMSET(au1TestName, 0, 10);

                       if(MplsL3VpnGetMplsL3VpnVrfCfgPrompt(NULL,  au1TestName) != FALSE)
                       {
                           u4RetVal = OSIX_FAILURE;
                       }
                   }
                  break;
               case 182:
                   {
                        tMplsL3VpnRegInfo RegInfo;
                        UINT2    u2Mask  = 0;

                        MEMSET(&RegInfo, 0, sizeof(tMplsL3VpnRegInfo));

                        RegInfo.pBgp4MplsL3VpnNotifyRouteParams = L3VpnUtNotifyRouteParams;
                        RegInfo.pBgp4MplsL3VpnNotifyLspStatusChange = L3VpnUtNotifyLspStatusChange;
                        RegInfo.pBgp4MplsL3vpnNotifyVrfAdminStatusChange = L3VpnUtNotifyVrfAdminStatusChange;

                        u2Mask  = L3VPN_BGP4_ROUTE_PARAMS_REQ | L3VPN_BGP4_LSP_STATUS_REQ |
                              L3VPN_BGP4_VRF_ADMIN_STATUS_REQ;

                        RegInfo.u2InfoMask = u2Mask;

                       L3VpnUtCreateVrf ();
                       MGMT_UNLOCK ();
                       CliExecuteAppCmd ("c t");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRTRowStatus.6.99.117.115.116.45.49.1.3 value 5");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRT.6.99.117.115.116.45.49.1.3 value 00:02:00:64:00:00:00:01");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRTRowStatus.6.99.117.115.116.45.49.1.3 value 1");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRTRowStatus.6.99.117.115.116.45.49.1.3 value 2");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRTRowStatus.6.99.117.115.116.45.49.1.3 value 1");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRTRowStatus.6.99.117.115.116.45.49.1.3 value 3");
                       CliExecuteAppCmd ("snmpset mib name mplsL3VpnVrfRTRowStatus.6.99.117.115.116.45.49.1.3 value 6");
                       MGMT_LOCK ();
                       L3VpnDeleteVrf ();
                   }
                  break;
               case 183:
                   {
                      if(CliGetL3vpnDebugLevel (CliHandle, NULL) != OSIX_FAILURE)
                      {
                          u4RetVal = OSIX_FAILURE;
                      }
                   }
				   /*** l3vpnmain.c***/
		case 184:
		{
			UINT4 u4PrefixLen=0;
			UINT4 u4SubnetMask=0;
			u4SubnetMask=L3VpnGetSubnetMaskFromPrefixLen(u4PrefixLen);
			if(u4SubnetMask!=0)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
		}
		break;
		case 185:
		{
			UINT4 u4Prefix   = 0;
			UINT4 u4LspLabel = 0;
			BOOL1 u1Status   = 0;
			u4OsixRetVal=MplsL3VpnBgp4LspUsageStatusUpdate(u4Prefix,u4LspLabel,u1Status);
			if(u4OsixRetVal==OSIX_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
		}
		break;
		case 186:
		{
			UINT4 u4ErrorCode = 0;
			UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
			MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
			VrfName.pu1_OctetList = au1VrfName;
			VrfName.i4_Length = u4VrfNameLen;
			u4RtIndex = 1;
			u4RtType = CLI_MPLS_L3VPN_RT_IMPORT;

			if(nmhTestv2MplsL3VpnVrfRTRowStatus
					(&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) != SNMP_FAILURE)
			{
				u4RetVal = OSIX_FAILURE;
			}

		}
		break;

		case 187:
		{
			tKey key;
			tInputParams InputParam;
			tOutputParams OutputParams;
			
			MEMSET(&key,0,sizeof(tKey));
			/*L3VpnTrieDeInit(NULL);
			L3VpnTrieDelAllEntry(NULL,NULL,NULL);
			L3VpnFtnTrieCbDelete(NULL);
			L3VpnTrieDelEntry(NULL,NULL,key);
			L3VpnTrieSearchEntry(&InputParam,&OutputParams,NULL);
			L3VpnTrieLookUpEntry(&InputParam,&OutputParams,NULL,0,key);*/
		}
		break;

		case 188:
		{
			
			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();	
			L3vpnCreateIngressRoute_1();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("mpls l3vpn trie-search 35.0.0.20");
			CliExecuteAppCmd("mpls l3vpn trie-search 0.0.0.0");
			MGMT_LOCK();	
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;
		case 189:
		{
			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			u4OsixRetVal=L3VpnSendTrapForVrfStatus(NULL,0,0);
			u4OsixRetVal=L3VpnSendTrapForVrfStatus(VRF_NAME,L3VPN_VRF_UP_TRAP,10);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;
		case 190:
		{
			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
			L3vpnAddPortToVrf();


			u4OsixRetVal=L3VpnSendTrapForVrfStatus(VRF_NAME,L3VPN_VRF_UP_TRAP,1);
			if(u4OsixRetVal!=L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			u4OsixRetVal=L3VpnSendTrapForVrfStatus(VRF_NAME,L3VPN_VRF_DOWN_TRAP,1);
			if(u4OsixRetVal!=L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			INT4 i4SnmpMemInit=0;
			extern INT4 gi4SnmpMemInit;
	
			i4SnmpMemInit=gi4SnmpMemInit;
			gi4SnmpMemInit=SNMP_FAILURE;
			
			u4OsixRetVal=L3VpnSendTrapForVrfStatus(VRF_NAME,L3VPN_VRF_UP_TRAP,1);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			gi4SnmpMemInit=i4SnmpMemInit;
			L3vpnDelPortFromVrf();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;
		case 191:
		{
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;

			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			L3VpnSendTrapForRouteThreshold(NULL,L3VPN_VRF_MID_THRESHOLD_EXCEEDED);

			L3VpnSendTrapForRouteThreshold("cust-2",L3VPN_VRF_MID_THRESHOLD_EXCEEDED);

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			INT4 i4SnmpMemInit=0;
			extern INT4 gi4SnmpMemInit;
	
			i4SnmpMemInit=gi4SnmpMemInit;
			gi4SnmpMemInit=SNMP_FAILURE;
			
			u4OsixRetVal=L3VpnSendTrapForRouteThreshold(VRF_NAME,L3VPN_VRF_MID_THRESHOLD_EXCEEDED);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			gi4SnmpMemInit=i4SnmpMemInit;

			/*** delete the perf table **/
			pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(pL3vpnMplsL3VpnVrfEntry);
			if(pL3vpnMplsL3VpnVrfPerfEntry!=NULL)
			{
				if(L3vpnUtlVrfPerfTableRowDelete(pL3vpnMplsL3VpnVrfPerfEntry) != L3VPN_SUCCESS)
				{
					return L3VPN_FAILURE;
				}
			}

			L3VpnSendTrapForRouteThreshold(VRF_NAME,L3VPN_VRF_MID_THRESHOLD_EXCEEDED);

			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;	

		case 192:
		{
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			tL3VpnVrfAdminEventInfo L3VpnVrfAdminEventInfo;
			MEMSET(&L3VpnVrfAdminEventInfo,0,sizeof(tL3VpnVrfAdminEventInfo));

			MEMCPY(L3VpnVrfAdminEventInfo.au1VrfName,VRF_NAME,STRLEN(VRF_NAME));
			L3VpnVrfAdminEventInfo.u4EventType=L3VPN_VRF_ADMIN_STATUS_UP;
			L3vpnCreateIpVrf();

			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			L3vpnCreateMplsVrf();

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_UP;
			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_DOWN;
			L3vpnAddPortToVrf();
			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
			L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_DOWN;
			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }

			L3VpnVrfAdminEventInfo.u4EventType=L3VPN_VRF_ADMIN_STATUS_TESTING;
			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
				
			L3vpnDelPortFromVrf();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 193:
		{

			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			tL3VpnVrfAdminEventInfo L3VpnVrfAdminEventInfo;
			MEMSET(&L3VpnVrfAdminEventInfo,0,sizeof(tL3VpnVrfAdminEventInfo));

			MEMCPY(L3VpnVrfAdminEventInfo.au1VrfName,VRF_NAME,STRLEN(VRF_NAME));
			L3VpnVrfAdminEventInfo.u4EventType=L3VPN_VRF_ADMIN_STATUS_DOWN;
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_UP;
			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }	
			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
			L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_UP;
			u4OsixRetVal=L3VpnProcessVrfAdminEvent(&L3VpnVrfAdminEventInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }	
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 194:
		{
			tL3VpnIfEvtInfo L3VpnIfEvtInfo;
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			MEMSET(&L3VpnIfEvtInfo,0,sizeof(tL3VpnIfEvtInfo));
			L3VpnIfEvtInfo.i4IfIndex=1;
			L3VpnIfEvtInfo.u1OperStatus=CFA_IF_UP;

			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
			L3vpnAddPortToVrf();


			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
			L3VPN_P_VRF_ADMIN_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_UP;
			L3VPN_P_VRF_OPER_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_DOWN;
			L3VPN_NOTIFICATION_ENABLE=L3VPN_TRAP_ENABLE;
			u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
			if(u4OsixRetVal!=L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
			L3VPN_P_VRF_OPER_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_DOWN;
			L3VPN_NOTIFICATION_ENABLE=L3VPN_TRAP_DISABLE;
			u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
			if(u4OsixRetVal!=L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3VPN_P_VRF_OPER_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_UP;
			u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
			L3VpnIfEvtInfo.u1OperStatus=CFA_IF_DOWN;
			L3VPN_P_VRF_ACTIVE_INTERFACES(pL3vpnMplsL3VpnVrfEntry)=1;
			u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			
			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
                        L3VpnIfEvtInfo.u1OperStatus=CFA_IF_DOWN;
                        L3VPN_P_VRF_ACTIVE_INTERFACES(pL3vpnMplsL3VpnVrfEntry)=1;
			L3VPN_NOTIFICATION_ENABLE=L3VPN_TRAP_ENABLE;
                        u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }

			INT4 i4SnmpMemInit=0;
			extern INT4 gi4SnmpMemInit;
			i4SnmpMemInit=gi4SnmpMemInit;
			gi4SnmpMemInit=SNMP_FAILURE;

			L3VpnIfEvtInfo.u1OperStatus=CFA_IF_UP;
                        L3VPN_P_VRF_ACTIVE_INTERFACES(pL3vpnMplsL3VpnVrfEntry)=1;
			L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) = L3VPN_VRF_ADMIN_STATUS_UP;
			L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;
                        L3VPN_NOTIFICATION_ENABLE=L3VPN_TRAP_ENABLE;
                        u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }

                        L3VpnIfEvtInfo.u1OperStatus=CFA_IF_DOWN;
                        L3VPN_P_VRF_ACTIVE_INTERFACES(pL3vpnMplsL3VpnVrfEntry)=1;
                        L3VPN_NOTIFICATION_ENABLE=L3VPN_TRAP_ENABLE;
                        u4OsixRetVal=L3VpnProcessIfEvent(&L3VpnIfEvtInfo);
                        if(u4OsixRetVal!=L3VPN_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			gi4SnmpMemInit=i4SnmpMemInit;

			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);

			L3vpnDelPortFromVrf();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 195:
		{
			tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry=NULL;
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			pL3VpnIfConfEntry = L3VpnCreateMplsL3VpnIfConfEntry(1,1);
			if (RBTreeAdd(L3VPN_IF_CONF_TABLE,
						(tRBElem *) pL3VpnIfConfEntry) != RB_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			u4OsixRetVal=L3VpnMapInterfaceToVrf(1,1);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			u4OsixRetVal=L3VpnMapInterfaceToVrf(1,0);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 196:
		{
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			MGMT_UNLOCK ();
			CliExecuteAppCmd("end");
			CliExecuteAppCmd("c t");
			CliExecuteAppCmd("i g 0/1");
			CliExecuteAppCmd("no sh");
			CliExecuteAppCmd("end");
			MGMT_LOCK();

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3VPN_P_VRF_ADMIN_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_UP;
			L3VPN_P_VRF_OPER_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_DOWN;

			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);	
			L3VpnMapInterfaceToVrf(1,1);
			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);

			L3VpnDeleteIfConfTableForVrf(L3VPN_P_VRF_NAME(pL3vpnMplsL3VpnVrfEntry));

			L3VPN_P_VRF_ADMIN_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_UP;
			L3VPN_P_VRF_OPER_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_DOWN;

			L3VpnMapInterfaceToVrf(1,1);

			L3VpnDeleteIfConfTableForVrf(L3VPN_P_VRF_NAME(pL3vpnMplsL3VpnVrfEntry));

			L3VPN_P_VRF_ADMIN_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_UP;
			L3VPN_P_VRF_OPER_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_OPER_UP;
			L3VpnMapInterfaceToVrf(1,1);


			MGMT_UNLOCK ();
			CliExecuteAppCmd("end");
			CliExecuteAppCmd("c t");
			CliExecuteAppCmd("i g 0/1");
			CliExecuteAppCmd("sh");
			CliExecuteAppCmd("exit");
			CliExecuteAppCmd("no int gig 0/1");
			CliExecuteAppCmd("end");
			MGMT_LOCK();

			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 197:
		{
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
				
			L3vpnDeleteIpVrf();

		}
		break;
		case 198:
		{
			tL3VpnVcmIfEvtInfo L3VpnVcmIfEvtInfo;
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
			L3vpnAddPortToVrf();

			L3VpnVcmIfEvtInfo.i4IfIndex=1;
			L3VpnVcmIfEvtInfo.u4ContextId=1;

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);

			L3VpnProcessVcmIfUnmapEvent(&L3VpnVcmIfEvtInfo);

			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);

			L3VpnVcmIfEvtInfo.i4IfIndex=0;
			L3VpnVcmIfEvtInfo.u4ContextId=1;
			L3VpnProcessVcmIfUnmapEvent(&L3VpnVcmIfEvtInfo);

			L3vpnDelPortFromVrf();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 199:
		{
			MGMT_UNLOCK ();
			CliExecuteAppCmd("end");
			CliExecuteAppCmd("c t");
			CliExecuteAppCmd("ip vrf cust-1");
			CliExecuteAppCmd("end");
			MGMT_LOCK ();

			/* If RT Index index is other than import, export or both RT creation should fail */
			UINT4 u4ErrorCode = 0;
			UINT4 u4VrfNameLen = STRLEN (VRF_NAME);
			MEMCPY(au1VrfName, VRF_NAME, u4VrfNameLen);
			VrfName.pu1_OctetList = au1VrfName;
			VrfName.i4_Length = u4VrfNameLen;
			u4RtIndex = 1;
			u4RtType = MPLS_L3VPN_MAX_RT_TYPE; /* This is not a valid type */

			if(nmhTestv2MplsL3VpnVrfRTRowStatus
					(&u4ErrorCode, &VrfName, u4RtIndex, u4RtType, CREATE_AND_WAIT) != SNMP_FAILURE)
			{
				u4RetVal = OSIX_FAILURE;
			}

			/* Clean Up */
			MGMT_UNLOCK ();
			CliExecuteAppCmd("end");
			CliExecuteAppCmd("c t");
			CliExecuteAppCmd("no ip vrf cust-1");
			CliExecuteAppCmd("end");
			MGMT_LOCK ();
		}
		break;
		case 200:
		{
			tL3VpnVcmEventInfo L3VpnVcmEventInfo;
			L3VpnVcmEventInfo.u4ContextId=1;
			L3VpnVcmEventInfo.u1EventType=VCM_CONTEXT_DELETE;
			u4OsixRetVal=L3VpnPrcoessVcmCrtDelEvent(&L3VpnVcmEventInfo);
			if(u4OsixRetVal!=L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3VpnVcmEventInfo.u1EventType=VCM_CONTEXT_CREATE+1;
			u4OsixRetVal=L3VpnPrcoessVcmCrtDelEvent(&L3VpnVcmEventInfo);
			if(u4OsixRetVal!=L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
		}
		break;
		case 201:
		{
			tL3vpnMplsL3VpnVrfRteEntry  L3vpnMplsL3VpnVrfRteEntry;
			MEMSET(&L3vpnMplsL3VpnVrfRteEntry,0,sizeof(tL3vpnMplsL3VpnVrfRteEntry));

			L3VpnRegisterRteEntryWithTrie(1,&L3vpnMplsL3VpnVrfRteEntry);

                        L3VPN_RTE_INET_CIDR_PREFIX_LEN(L3vpnMplsL3VpnVrfRteEntry)=100;

			L3VpnRegisterRteEntryWithTrie(1,&L3vpnMplsL3VpnVrfRteEntry);

			L3VpnGetRteTableEntryFromTrie(1,0);

			L3VpnGetBestMatchRteTableEntryFromTrie(1,0);

			L3VpnDeleteRteTableEntryFromTrie(1,&L3vpnMplsL3VpnVrfRteEntry);

			L3VpnDeleteRteTableEntryFromTrie(1,&L3vpnMplsL3VpnVrfRteEntry);

			L3VpnDeleteRteTableEntryFromTrie(1,&L3vpnMplsL3VpnVrfRteEntry);

		}
		break;
		case 202:
		{

			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
		
			
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();

		}
		break;
		case 203:
		{	
			tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;

			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3vpnAllocateVarBindPool();	
			u4OsixRetVal=L3VpnSendTrapForRouteThreshold(VRF_NAME,L3VPN_VRF_MID_THRESHOLD_EXCEEDED);
			if(u4OsixRetVal==L3VPN_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			L3vpnClearVarBindPool();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;
		case 204:
		{
			tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
			MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
			u4OsixRetVal=L3vpnFillDefaultIngressRouteDel(&MplsL3VpnBgp4RouteInfo);
			if(u4OsixRetVal!=OSIX_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

                        u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }

			u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
			if(u4OsixRetVal!=OSIX_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
			u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
			if(u4OsixRetVal!=OSIX_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
                        u4OsixRetVal=L3vpnFillDefaultIngressRouteDel(&MplsL3VpnBgp4RouteInfo);
                        if(u4OsixRetVal!=OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
                        
                        L3VPN_NOTIFICATION_ENABLE  = L3VPN_TRAP_ENABLE;
                        tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                        tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;

			pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
			if(pL3vpnMplsL3VpnVrfEntry==NULL)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}
                        pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(pL3vpnMplsL3VpnVrfEntry);

                        /*L3VPN_P_VRF_HIGH_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) = OSIX_TRUE;*/
                        L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) =
                                L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry);
                        L3vpnAllocateVarBindPool();
                        u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
                        if(u4OsixRetVal==OSIX_SUCCESS)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
                        L3vpnClearVarBindPool();
                        L3VPN_NOTIFICATION_ENABLE  = L3VPN_TRAP_DISABLE;

			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();

		}
		break;
		case 205:
		{
			tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
			MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();

			MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));

			u4OsixRetVal=L3vpnFillDefaultIngressRouteAdd(&MplsL3VpnBgp4RouteInfo);
			if(u4OsixRetVal!=OSIX_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

                        tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
                        tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;

                        pL3vpnMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
                        if(pL3vpnMplsL3VpnVrfEntry==NULL)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
                        pL3vpnMplsL3VpnVrfPerfEntry = L3vpnUtlGetVrfPerfTableEntry(pL3vpnMplsL3VpnVrfEntry);

			L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) =
				L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry);
			u4OsixRetVal=MplsL3VpnBgp4RouteUpdate(&MplsL3VpnBgp4RouteInfo);
			if(u4OsixRetVal==OSIX_SUCCESS)
			{
				u4RetVal=OSIX_FAILURE;
				break;
			}

			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;
                case 206:
		{
			tL3vpnMplsL3VpnVrfRteEntry  L3vpnMplsL3VpnVrfRteEntry;
			MEMSET(&L3vpnMplsL3VpnVrfRteEntry,0,sizeof(tL3vpnMplsL3VpnVrfRteEntry));
			MplsL3vpnIngressMap(&L3vpnMplsL3VpnVrfRteEntry);
			MplsL3vpnEgressMap(NULL);
                        MplsL3vpnIngressUnMap(&L3vpnMplsL3VpnVrfRteEntry);
		}
		break;
		case 207:
		{
			tL3vpnMplsL3VpnVrfRteEntry  L3vpnMplsL3VpnVrfRteEntry;
                        tXcEntry *pXcEntry=NULL;

                        UINT4 u4OutIndex=0;
                        UINT4 u4InIndex=0;
			UINT4 u4XcIndex=0;


                        MEMSET(&L3vpnMplsL3VpnVrfRteEntry,0,sizeof(tL3vpnMplsL3VpnVrfRteEntry));
                        
                        MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);


			L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[3] = 0x01;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[7] = 0x00;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[11] = 0x00;
                        /* Allocating the Xc Index */
                        u4XcIndex = MplsXcTableGetIndex ();
                        u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
                        /* Creating the XC with insegemnt = NULL */
                        MplsCreateXCTableEntry (u4XcIndex,0,0);

			MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			L3VpnPrintRteEntry(CliHandle,&L3vpnMplsL3VpnVrfRteEntry);

			/** delete the complete configuration **/
                        L3VpnDeleteXcEntry(u4XcIndex,0,0);

			/** MAC is not resolved ***/

			u4OutIndex = MplsOutSegmentGetIndex ();
                        /** allocating the Insegment Index */
                        u4InIndex = MplsInSegmentGetIndex();
                        /* Creating the XC with insegemnt = NULL */
                        pXcEntry=MplsCreateXCTableEntry (u4XcIndex,u4InIndex,u4OutIndex);

			L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[3] = 0x01;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[7] = 0x01;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[11] = 0x01;

			MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			L3VpnPrintRteEntry(CliHandle,&L3vpnMplsL3VpnVrfRteEntry);
			
			pXcEntry->pOutIndex->au1NextHopMac[0]=0x01;
			pXcEntry->pOutIndex->u4Label = MPLS_IMPLICIT_NULL_LABEL;
			MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			L3VpnPrintRteEntry(CliHandle,&L3vpnMplsL3VpnVrfRteEntry);

			pXcEntry->pOutIndex->u4Label = 500;
                        pXcEntry->pOutIndex->u4IfIndex = 10;
			MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);	
			L3VpnPrintRteEntry(CliHandle,&L3vpnMplsL3VpnVrfRteEntry);


			tLblStkEntry       *pLblStkEntry = NULL;
			tLblEntry          *pLblEntry = NULL;
			UINT4               u4LblStkIndex = 0;
			UINT4               u4MplsLabelStackLabelIndex = 1;

			u4LblStkIndex = MplsLblStackGetIndex ();
			if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"MplsFTNCreate : MplsLblStackSetIndex Failed\n");
				MPLS_CMN_UNLOCK();
				return MPLS_FAILURE;
			}
			pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);
			if (pLblStkEntry == NULL)
			{
				CMNDB_DBG
					(DEBUG_DEBUG_LEVEL,
					 "MplsFTNCreate : MplsCreateLblStkTableEntry Failed \t\n");
				MPLS_CMN_UNLOCK();
				return MPLS_FAILURE;
			}
			pXcEntry->mplsLabelIndex = pLblStkEntry;
			MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			L3VpnPrintRteEntry(CliHandle,&L3vpnMplsL3VpnVrfRteEntry);

			pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
					u4MplsLabelStackLabelIndex);
			if (pLblEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"MplsFTNCreate : MplsAddLblStkLabelEntry Failed \t\n");
				MPLS_CMN_UNLOCK();
				return MPLS_FAILURE;
			}
			pLblEntry->u4Label = MPLS_IMPLICIT_NULL_LABEL;
			pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
			CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : OUT Stack Label=%d\n",
					pLblEntry->u4Label);
			pLblEntry->u1RowStatus = ACTIVE;


			extern tMplsNodeStatus    gMplsNodeStatus;
			tMplsNodeStatus     MplsNodeStatus;
			MplsNodeStatus=gMplsNodeStatus;
                        gMplsNodeStatus=MPLS_NODE_STANDBY;
			MplsL3vpnHwIngressMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			gMplsNodeStatus=MplsNodeStatus;
			L3VpnDeleteXcEntry(u4XcIndex,u4InIndex,u4OutIndex);
		}
		break;
		case 208:
		{

			tL3vpnMplsL3VpnVrfRteEntry  L3vpnMplsL3VpnVrfRteEntry;
                        tXcEntry *pXcEntry=NULL;

                        UINT4 u4OutIndex=0;
                        UINT4 u4InIndex=0;
			UINT4 u4XcIndex=0;


                        MEMSET(&L3vpnMplsL3VpnVrfRteEntry,0,sizeof(tL3vpnMplsL3VpnVrfRteEntry));
                        
                        MplsL3vpnHwIngressUnMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);


			L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[3] = 0x01;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[7] = 0x00;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[11] = 0x00;
                        /* Allocating the Xc Index */
                        u4XcIndex = MplsXcTableGetIndex ();
                        u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
                        /* Creating the XC with insegemnt = NULL */
                        MplsCreateXCTableEntry (u4XcIndex,0,0);

			MplsL3vpnHwIngressUnMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);

			/** delete the complete configuration **/
                        L3VpnDeleteXcEntry(u4XcIndex,0,0);

			/** MAC is not resolved ***/

			u4OutIndex = MplsOutSegmentGetIndex ();
                        /** allocating the Insegment Index */
                        u4InIndex = MplsInSegmentGetIndex();
                        /* Creating the XC with insegemnt = NULL */
                        pXcEntry=MplsCreateXCTableEntry (u4XcIndex,u4InIndex,u4OutIndex);

			L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[3] = 0x01;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[7] = 0x01;
                        L3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteXCPointer[11] = 0x01;

			pXcEntry->pOutIndex->u4Label = MPLS_IMPLICIT_NULL_LABEL;
			MplsL3vpnHwIngressUnMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			
			pXcEntry->pOutIndex->u4Label = 500;
                        pXcEntry->pOutIndex->u4IfIndex = 10;
			MplsL3vpnHwIngressUnMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);	

			extern tMplsNodeStatus    gMplsNodeStatus;
			tMplsNodeStatus     MplsNodeStatus;
			MplsNodeStatus=gMplsNodeStatus;
                        gMplsNodeStatus=MPLS_NODE_STANDBY;
			MplsL3vpnHwIngressUnMap(&L3vpnMplsL3VpnVrfRteEntry,NULL);
			gMplsNodeStatus=MplsNodeStatus;
			L3VpnDeleteXcEntry(u4XcIndex,u4InIndex,u4OutIndex);
		}
		break;
		case 209:
		{
			tL3VpnBgpRouteLabelEntry L3VPNRouteEntry;
			MEMSET(&L3VPNRouteEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

			L3VPN_BGPROUTELABEL_POLICY(L3VPNRouteEntry)=L3VPN_BGP4_POLICY_PER_ROUTE;

			L3VPN_BGPROUTELABELRTE_IF_IDX(L3VPNRouteEntry)=1000;

			MplsL3vpnHwEgressMap(&L3VPNRouteEntry,NULL);

			L3VPN_BGPROUTELABEL_POLICY(L3VPNRouteEntry)=L3VPN_BGP4_POLICY_PER_ROUTE+1;
			MplsL3vpnHwEgressMap(&L3VPNRouteEntry,NULL);

			L3VPN_BGPROUTELABEL_POLICY(L3VPNRouteEntry)=L3VPN_BGP4_POLICY_PER_VRF;

			extern tMplsNodeStatus    gMplsNodeStatus;
			tMplsNodeStatus     MplsNodeStatus;
			MplsNodeStatus=gMplsNodeStatus;
                        gMplsNodeStatus=MPLS_NODE_STANDBY;
			MplsL3vpnHwEgressMap(&L3VPNRouteEntry,NULL);
			gMplsNodeStatus=MplsNodeStatus;
			MplsL3vpnHwEgressMap(&L3VPNRouteEntry,NULL);			
		}
		break;
		case 210:
		{
			 tL3VpnBgpRouteLabelEntry L3VPNRouteEntry;
                        MEMSET(&L3VPNRouteEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

                        L3VPN_BGPROUTELABEL_POLICY(L3VPNRouteEntry)=L3VPN_BGP4_POLICY_PER_ROUTE;

                        MplsL3vpnHwEgressUnMap(&L3VPNRouteEntry,NULL);

                        L3VPN_BGPROUTELABEL_POLICY(L3VPNRouteEntry)=L3VPN_BGP4_POLICY_PER_ROUTE+1;
                        MplsL3vpnHwEgressUnMap(&L3VPNRouteEntry,NULL);

                        L3VPN_BGPROUTELABEL_POLICY(L3VPNRouteEntry)=L3VPN_BGP4_POLICY_PER_VRF;

                        extern tMplsNodeStatus    gMplsNodeStatus;
                        tMplsNodeStatus     MplsNodeStatus;
                        MplsNodeStatus=gMplsNodeStatus;
                        gMplsNodeStatus=MPLS_NODE_STANDBY;
                        MplsL3vpnHwEgressUnMap(&L3VPNRouteEntry,NULL);
                        gMplsNodeStatus=MplsNodeStatus;
                        MplsL3vpnHwEgressUnMap(&L3VPNRouteEntry,NULL);
		}
		break;
		case 211:
		{
			L3vpnEgressNpapiSuccess();
			L3vpnEgressNpapiRemoveSuccess();
		}
		break;
		case 212:
		{
			cli_process_L3vpn_show_cmd(CliHandle,CLI_L3VPN_SHOW_VRF,NULL,"cust-2");
			cli_process_L3vpn_show_cmd(CliHandle,CLI_L3VPN_SHOW_VRF_ALL,NULL,"cust-2");
			cli_process_L3vpn_show_cmd(CliHandle,100,NULL);

			L3vpnEgressNpapiSuccess();
			MGMT_UNLOCK ();
			CliExecuteAppCmd("show mpls vrf");
			CliExecuteAppCmd("show mpls vrf cust-1");
			CliExecuteAppCmd("show mpls vrf ingress routes");
			CliExecuteAppCmd("show mpls vrf ingress routes cust-1");
			CliExecuteAppCmd("show mpls vrf egress routes");
			CliExecuteAppCmd("show mpls vrf interface-mapping");
			CliExecuteAppCmd("show mpls vrf interface-mapping cust-1");
			CliExecuteAppCmd("show mpls vrf route-target");
			CliExecuteAppCmd("show mpls vrf route-target cust-1");
			CliExecuteAppCmd("show mpls l3vpn egress routes");
			MGMT_LOCK();

			L3vpnEgressNpapiSuccess();
			L3vpnEgressNpapiRemoveSuccess();
		}
		break;
		case 213:
		{
			tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
			MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

			L3VPN_BGPROUTELABEL_POLICY(L3VpnBgpRouteLabelEntry)=L3VPN_BGP4_POLICY_PER_VRF;
			L3VpnBgpShowRouteLabel(CliHandle,&L3VpnBgpRouteLabelEntry);

			L3VPN_BGPROUTELABEL_POLICY(L3VpnBgpRouteLabelEntry)=100;
			L3VpnBgpShowRouteLabel(CliHandle,&L3VpnBgpRouteLabelEntry);
		}
		break;
		case 214:
		{
			tL3vpnMplsL3VpnVrfRTEntry L3vpnMibMplsL3VpnVrfRTEntry;
			MEMSET(&L3vpnMibMplsL3VpnVrfRTEntry,0,sizeof(tL3vpnMibMplsL3VpnVrfRTEntry));

			L3VPN_RT_VALUE (L3vpnMibMplsL3VpnVrfRTEntry)[0] =L3VPN_RT_TYPE_2;
			L3VpnCliPrintRTInfo(CliHandle,&L3vpnMibMplsL3VpnVrfRTEntry);
		}
		break;
		case 215:
		{
			tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;

			MEMSET(&MplsL3VpnBgp4RouteInfo,0,sizeof(tMplsL3VpnBgp4RouteInfo));
			L3vpnCreateIngressStaticBinding();
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
			L3vpnCreateRt();

			L3VpnRtTableShow (CliHandle,"cust-1");	

			L3vpnDeleteRt();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			L3vpnDeleteIngressStaticBinding();
		}
		break;
		case 216:
		{
			tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
			MEMSET(&L3vpnMplsL3VpnVrfEntry,0,sizeof(tL3vpnMplsL3VpnVrfEntry));
			L3VPN_VRF_RD (L3vpnMplsL3VpnVrfEntry)[0]=L3VPN_RD_TYPE_2;
			L3vpnCliShowMplsL3VpnVrf(CliHandle,&L3vpnMplsL3VpnVrfEntry);	
		}
		break;

		case 217:
		{
			L3vpnCreateIpVrf();
                        L3vpnCreateMplsVrf();

			tL3vpnMplsL3VpnVrfEntry *pL3vpnOldMplsL3VpnVrfEntry=NULL;

			tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
			tL3vpnIsSetMplsL3VpnVrfEntry L3vpnIsSetMplsL3VpnVrfEntry;

			L3vpnFillDefaultVrfEntry(&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

			pL3vpnOldMplsL3VpnVrfEntry=L3vpnGetVrfEntry();
                        if(pL3vpnOldMplsL3VpnVrfEntry==NULL)
                        {
                                u4RetVal=OSIX_FAILURE;
                                break;
                        }
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus=OSIX_FALSE;
			L3vpnUtilUpdateMplsL3VpnVrfTable(pL3vpnOldMplsL3VpnVrfEntry,
					&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus=OSIX_TRUE;
			L3VPN_VRF_ROW_STATUS (L3vpnMplsL3VpnVrfEntry)=NOT_IN_SERVICE;
			L3VPN_VRF_ADMIN_STATUS(L3vpnMplsL3VpnVrfEntry) = L3VPN_VRF_ADMIN_STATUS_UP;

			L3vpnUtilUpdateMplsL3VpnVrfTable(pL3vpnOldMplsL3VpnVrfEntry,
                                        &L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

	
			L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus=OSIX_TRUE;
			L3VPN_VRF_ROW_STATUS (L3vpnMplsL3VpnVrfEntry)=NOT_IN_SERVICE;
			L3VPN_VRF_ADMIN_STATUS(L3vpnMplsL3VpnVrfEntry) = L3VPN_VRF_ADMIN_STATUS_TESTING;

			L3vpnUtilUpdateMplsL3VpnVrfTable(pL3vpnOldMplsL3VpnVrfEntry,
					&L3vpnMplsL3VpnVrfEntry,&L3vpnIsSetMplsL3VpnVrfEntry);

                        L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();

		}
		break;
		case 218:
		{
	
			L3vpnCreateIpVrf();
                        L3vpnCreateMplsVrf();
			L3vpnCreateRt();

			tL3vpnMplsL3VpnVrfRTEntry *pL3vpnOldMplsL3VpnVrfRTEntry=NULL;
			tL3vpnMplsL3VpnVrfRTEntry L3vpnMplsL3VpnVrfRTEntry;
			tL3vpnIsSetMplsL3VpnVrfRTEntry L3vpnIsSetMplsL3VpnVrfRTEntry;

			L3vpnFillDefaultRtEntry(&L3vpnMplsL3VpnVrfRTEntry,&L3vpnIsSetMplsL3VpnVrfRTEntry);

			pL3vpnOldMplsL3VpnVrfRTEntry=L3VpnGetRTEntryFromValue(
					L3vpnMplsL3VpnVrfRTEntry.MibObject.au1MplsL3VpnVrfName,
					L3VPN_RT_VALUE(L3vpnMplsL3VpnVrfRTEntry));
			if(pL3vpnOldMplsL3VpnVrfRTEntry!=NULL)
			{
				L3VPN_RT_TYPE (L3vpnMplsL3VpnVrfRTEntry)=100;
				L3vpnUtilUpdateMplsL3VpnVrfRTTable(pL3vpnOldMplsL3VpnVrfRTEntry,&L3vpnMplsL3VpnVrfRTEntry,
						&L3vpnIsSetMplsL3VpnVrfRTEntry);	
				MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
				

				MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
			}

			L3vpnDeleteRt();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
		}
		break;
		case 219:
		{
			MplsL3VpnRegisterCallback(&MplsL3VpnRegInfo);
			L3vpnCreateIpVrf();
			L3vpnCreateMplsVrf();
			L3vpnCreateRt();


			L3vpnDeleteRt();
			L3vpnDeleteMplsVrf();
			L3vpnDeleteIpVrf();
			MplsL3VpnDeRegisterCallback(&MplsL3VpnRegInfo);
		}
		break;
            default:
                break;
    };

    /* Display the result */
    if (u4RetVal == OSIX_FAILURE)
    {
            CliPrintf(CliHandle,"!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!\r\n",
                            i4TestId, "FAILED");
            u4TotFailedCases++;
    }
    else
    {
            CliPrintf(CliHandle,"$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$\r\n",
                            i4TestId, "PASSED");
            u4TotPassedCases++;
    }

    return;
}


VOID 
L3VpnUtNotifyRouteParams(UINT4 u4VrfId, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam)
{
}

VOID 
L3VpnUtNotifyLspStatusChange (UINT4 u4LspLabel, UINT1 u1LspStatus)
{
}

VOID
L3VpnUtNotifyVrfAdminStatusChange(UINT4 u4VrfId, UINT1 u1VrfStatus)
{
}

#if 0
VOID
L3VpnUtExhaustVcmRegPool()
{
    UINT4 u4Count = 0;
    for(u4Count = 0; u4Count < MAX_VCM_PROTO_REG_SIZE; u4Count++)
    {
        pVcmHLProtoRegEntry[u4Count] = (tVcmHLProtoRegEntry *) MemAllocMemBlk(
                                               (gVcmGlobals.u4VcProtoRegPoolId));
    }
}

VOID
L3VpnUtFreeVcmRegPool()
{
    UINT4 u4Count = 0;
    for(u4Count = 0; u4Count < MAX_VCM_PROTO_REG_SIZE; u4Count++)
    {
        MemReleaseMemBlock (gVcmGlobals.u4VcProtoRegPoolId, (UINT1 *)pVcmHLProtoRegEntry[u4Count]);
    }
}
#endif

VOID
L3VpnUtExhaustPerfMemPool()
{
    UINT4 u4Count = 0;
    for(u4Count = 0; u4Count < MAX_L3VPN_PERF_TABLE_ENTRIES; u4Count++)
    {
         pu1PerfEntry[u4Count] = (tL3vpnMplsL3VpnVrfPerfEntry *)
                            MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);
    }
}

VOID
L3VpnUtFreePerfMemPool()
{
    UINT4 u4Count = 0;
    for(u4Count = 0; u4Count < MAX_L3VPN_PERF_TABLE_ENTRIES; u4Count++)
    {
        if(pu1PerfEntry[u4Count] != NULL)
        {
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID, (UINT1 *)pu1PerfEntry[u4Count]);
        }
    }
}


VOID
L3VpnUtExhaustSecMemPool()
{
     UINT4 u4Count = 0;
     for(u4Count = 0; u4Count < MAX_L3VPN_SEC_TABLE_ENTRIES; u4Count++)
     {
          pu1SecEntry[u4Count] = (tL3vpnMplsL3VpnVrfSecEntry *)
                             MemAllocMemBlk (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID);
     }
}



VOID
L3VpnUtFreeSecMemPool()
{
   UINT4 u4Count = 0;
   for(u4Count = 1; u4Count < MAX_L3VPN_SEC_TABLE_ENTRIES; u4Count++)
   {
       if(pu1SecEntry[u4Count] != NULL)
       {
             MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID, (UINT1 *) pu1SecEntry[u4Count]);
       }
   }
}

VOID
L3VpnUtExhaustIfConfMemPool()
{
    UINT4 u4Count = 0;
    for(u4Count = 0; u4Count < MAX_L3VPN_IF_CONF_TABLE_ENTRIES; u4Count++)
    {
         pu1IfConfEntry[u4Count] = (tL3vpnMplsL3VpnIfConfEntry *)
                           MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    }
}

VOID
L3VpnUtFreeIfConfMemPool()
{
    UINT4 u4Count = 0;
    for(u4Count = 0; u4Count < MAX_L3VPN_IF_CONF_TABLE_ENTRIES; u4Count++)
    {
        if(pu1IfConfEntry[u4Count] != NULL)
        {
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID, (UINT1 *)pu1IfConfEntry[u4Count]);
        }
    }
}


VOID
L3VpnUtFillMsgQueue()
{
     UINT4 u4Count  = 0;
     UINT1              *pu1TmpMsg = NULL;

     pu1TmpMsg = (UINT1 *)MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID); 
     for(u4Count = 1; u4Count <= L3VPN_QUEUE_DEPTH; u4Count++)
     {
         OsixQueSend (gL3vpnGlobals.l3vpnQueId, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN);
     }
}

VOID
L3VpnUtFreeMsgQueue()
{
    UINT4 u4Count  = 0;
    tL3VpnQMsg               *pL3vpnQueMsg = NULL;
    
    for(u4Count = 1; u4Count <= L3VPN_QUEUE_DEPTH; u4Count++)
    {
        OsixQueRecv (gL3vpnGlobals.l3vpnQueId,
                              (UINT1 *) &pL3vpnQueMsg, OSIX_DEF_MSG_LEN, 0);
    }
}

VOID
L3VpnUtAddRdToVrf ()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd("rd 100:1");
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}



VOID
L3vpnFillDefaultVrfEntry(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry,
                tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry)
{

        UINT1                           u1Rd[MPLS_MAX_L3VPN_RD_LEN]={
                0x00,0x00,0x00,0x64,0x00,0x00,0x00,0x05};

        if(pL3vpnMplsL3VpnVrfEntry==NULL || pL3vpnIsSetMplsL3VpnVrfEntry==NULL)
        {
                return;
        }

        L3VPN_P_VRF_NAME_LEN(pL3vpnMplsL3VpnVrfEntry)=STRLEN(L3VPN_DEF_VRF_NAME);
        MEMCPY(L3VPN_P_VRF_NAME(pL3vpnMplsL3VpnVrfEntry),L3VPN_DEF_VRF_NAME,
                    L3VPN_P_VRF_NAME_LEN(pL3vpnMplsL3VpnVrfEntry));

    L3VPN_P_VRF_VPN_ID_LEN(pL3vpnMplsL3VpnVrfEntry)=STRLEN(L3VPN_DEF_VRF_VPN_ID);
    MEMCPY(L3VPN_P_VRF_VPN_ID(pL3vpnMplsL3VpnVrfEntry),L3VPN_DEF_VRF_VPN_ID,
                    L3VPN_P_VRF_VPN_ID_LEN(pL3vpnMplsL3VpnVrfEntry));

    L3VPN_P_VRF_DESCRIPTION_LEN(pL3vpnMplsL3VpnVrfEntry)=STRLEN(L3VPN_DEF_VRF_DESCRIPTION);
    MEMCPY(L3VPN_P_VRF_DESCRIPTION(pL3vpnMplsL3VpnVrfEntry),L3VPN_DEF_VRF_DESCRIPTION,
                    L3VPN_P_VRF_DESCRIPTION_LEN(pL3vpnMplsL3VpnVrfEntry));

    L3VPN_P_VRF_RD_LEN(pL3vpnMplsL3VpnVrfEntry)=MPLS_MAX_L3VPN_RD_LEN;
    MEMCPY(L3VPN_P_VRF_RD(pL3vpnMplsL3VpnVrfEntry),
                    u1Rd,MPLS_MAX_L3VPN_RD_LEN);

    L3VPN_P_VRF_MID_ROUTE_TH(pL3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES-1;
    L3VPN_P_VRF_HIGH_ROUTE_TH(pL3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES;
    L3VPN_P_VRF_CONF_MAX_ROUTES(pL3vpnMplsL3VpnVrfEntry)=L3VPN_PER_VRF_MAX_ROUTES;
    L3VPN_P_VRF_ROW_STATUS(pL3vpnMplsL3VpnVrfEntry)=CREATE_AND_WAIT;
    L3VPN_P_VRF_ADMIN_STATUS(pL3vpnMplsL3VpnVrfEntry)=L3VPN_VRF_ADMIN_STATUS_UP;
    L3VPN_P_VRF_STORAGE_TYPE(pL3vpnMplsL3VpnVrfEntry)=L3VPN_STORAGE_VOLATILE;



    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus=OSIX_TRUE;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType=OSIX_TRUE;

    return;
}

VOID
L3vpnFillDefaultRtEntry(tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry,
                tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry)
{

	UINT1                           u1Rt[MPLS_MAX_L3VPN_RT_LEN]={
		0x00, 0x02, 0x00, 0xc8, 0x00, 0x00, 0x00, 0x01};
	L3VPN_P_RT_INDEX(pL3vpnMplsL3VpnVrfRTEntry)=1;
	L3VPN_P_RT_TYPE(pL3vpnMplsL3VpnVrfRTEntry)=1;
	L3VPN_P_RT_ROW_STATUS(pL3vpnMplsL3VpnVrfRTEntry)=CREATE_AND_WAIT;
	L3VPN_P_RT_LEN(pL3vpnMplsL3VpnVrfRTEntry)=MPLS_MAX_L3VPN_RT_LEN;
	L3VPN_P_RT_DESC_LEN(pL3vpnMplsL3VpnVrfRTEntry)=0;
	MEMCPY(L3VPN_P_RT_VALUE(pL3vpnMplsL3VpnVrfRTEntry),u1Rt,MPLS_MAX_L3VPN_RT_LEN);

	pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen=STRLEN(L3VPN_DEF_VRF_NAME);
	MEMCPY(pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,L3VPN_DEF_VRF_NAME,STRLEN(L3VPN_DEF_VRF_NAME));

	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex=OSIX_TRUE;
	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType=OSIX_TRUE;
	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT=OSIX_TRUE;
	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr=OSIX_TRUE;
	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus=OSIX_TRUE;
	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType=OSIX_TRUE;
	pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName=OSIX_TRUE;


    return;
}

UINT4
L3vpnCreateMplsVrf()
{
        tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
        INT4 i4SetValMplsL3VpnVrfConfRowStatus=0;
        UINT1 u1VrfName[L3VPN_MAX_VRF_NAME_LEN];
        INT1  i1RetValue=0;

        MEMSET(&MplsL3VpnVrfName,0,sizeof(tSNMP_OCTET_STRING_TYPE));
        MEMSET(u1VrfName,0,L3VPN_MAX_VRF_NAME_LEN);

        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip vrf cust-1");
        MGMT_LOCK();

        MplsL3VpnVrfName.pu1_OctetList=u1VrfName;
        MplsL3VpnVrfName.i4_Length=STRLEN(L3VPN_DEF_VRF_NAME);
        MEMCPY(MplsL3VpnVrfName.pu1_OctetList,
                        L3VPN_DEF_VRF_NAME,MplsL3VpnVrfName.i4_Length);
        i4SetValMplsL3VpnVrfConfRowStatus=CREATE_AND_WAIT;

        i1RetValue=nmhSetMplsL3VpnVrfConfRowStatus(&MplsL3VpnVrfName,i4SetValMplsL3VpnVrfConfRowStatus);
        if(i1RetValue != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        return OSIX_SUCCESS;
}

UINT4
L3vpnCreateMplsRd()
{
        tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
        tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRD;
        UINT1 u1VrfName[L3VPN_MAX_VRF_NAME_LEN];
        UINT1                           u1Rd[MPLS_MAX_L3VPN_RD_LEN]={
                0x00,0x00,0x00,0x64,0x00,0x00,0x00,0x05};
        INT1  i1RetValue=0;

        MEMSET(&MplsL3VpnVrfName,0,sizeof(tSNMP_OCTET_STRING_TYPE));
        MEMSET(&MplsL3VpnVrfRD,0,sizeof(tSNMP_OCTET_STRING_TYPE));

        MplsL3VpnVrfName.pu1_OctetList=u1VrfName;
        MplsL3VpnVrfName.i4_Length=STRLEN(L3VPN_DEF_VRF_NAME);
        MEMCPY(MplsL3VpnVrfName.pu1_OctetList,
                        L3VPN_DEF_VRF_NAME,MplsL3VpnVrfName.i4_Length);
        
        MplsL3VpnVrfRD.pu1_OctetList=u1Rd;
        MplsL3VpnVrfRD.i4_Length=MPLS_MAX_L3VPN_RD_LEN;
        MEMCPY(MplsL3VpnVrfRD.pu1_OctetList,u1Rd,
                        MPLS_MAX_L3VPN_RD_LEN);

        
        i1RetValue=nmhSetMplsL3VpnVrfRD(&MplsL3VpnVrfName,&MplsL3VpnVrfRD);

        if(i1RetValue != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        return OSIX_SUCCESS;
}



UINT4
L3vpnDeleteMplsRd()
{
        tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
        tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRD;
        UINT1 u1VrfName[L3VPN_MAX_VRF_NAME_LEN];
        UINT1                           u1Rd[MPLS_MAX_L3VPN_RD_LEN]={
                0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
        INT1  i1RetValue=0;
        INT4  i4SetValMplsL3VpnVrfConfRowStatus=0;

        MEMSET(&MplsL3VpnVrfName,0,sizeof(tSNMP_OCTET_STRING_TYPE));
        MEMSET(&MplsL3VpnVrfRD,0,sizeof(tSNMP_OCTET_STRING_TYPE));

        MplsL3VpnVrfName.pu1_OctetList=u1VrfName;
        MplsL3VpnVrfName.i4_Length=STRLEN(L3VPN_DEF_VRF_NAME);
        MEMCPY(MplsL3VpnVrfName.pu1_OctetList,
                        L3VPN_DEF_VRF_NAME,MplsL3VpnVrfName.i4_Length);
        i4SetValMplsL3VpnVrfConfRowStatus=NOT_IN_SERVICE;

        i1RetValue=nmhSetMplsL3VpnVrfConfRowStatus(&MplsL3VpnVrfName,i4SetValMplsL3VpnVrfConfRowStatus);
        if(i1RetValue != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        MplsL3VpnVrfRD.pu1_OctetList=u1Rd;
        MplsL3VpnVrfRD.i4_Length=MPLS_MAX_L3VPN_RD_LEN;
        MEMCPY(MplsL3VpnVrfRD.pu1_OctetList,u1Rd,
                        MPLS_MAX_L3VPN_RD_LEN);
    
        i1RetValue=nmhSetMplsL3VpnVrfRD(&MplsL3VpnVrfName,&MplsL3VpnVrfRD);

        if(i1RetValue != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

    return OSIX_SUCCESS;
}

UINT4
L3vpnDeleteMplsVrf()
{
        tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
        INT4 i4SetValMplsL3VpnVrfConfRowStatus=0;
        UINT1 u1VrfName[L3VPN_MAX_VRF_NAME_LEN];
        INT1  i1RetValue=0;

        MEMSET(&MplsL3VpnVrfName,0,sizeof(tSNMP_OCTET_STRING_TYPE));
        MEMSET(u1VrfName,0,L3VPN_MAX_VRF_NAME_LEN);

        MplsL3VpnVrfName.pu1_OctetList=u1VrfName;
        MplsL3VpnVrfName.i4_Length=STRLEN(L3VPN_DEF_VRF_NAME);
        MEMCPY(MplsL3VpnVrfName.pu1_OctetList,
                        L3VPN_DEF_VRF_NAME,MplsL3VpnVrfName.i4_Length);
        i4SetValMplsL3VpnVrfConfRowStatus=DESTROY;

        i1RetValue=nmhSetMplsL3VpnVrfConfRowStatus(&MplsL3VpnVrfName,i4SetValMplsL3VpnVrfConfRowStatus);
        if(i1RetValue != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("no ip vrf cust-1");
        MGMT_LOCK();

        return OSIX_SUCCESS;
}



UINT4
L3vpnCreateMplsVrfRd()
{
    UINT4  u4RetValue=0;

    u4RetValue=L3vpnCreateMplsVrf();
    if(u4RetValue!=OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    u4RetValue=L3vpnCreateMplsRd();

    if(u4RetValue!=OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}




/************ vishal_1: To Remove Later **********/

VOID
L3vpnCreateIpVrf()
{
        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip vrf cust-1");
        MGMT_LOCK();
        return;
}


VOID
L3vpnDeleteIpVrf()
{
        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("no ip vrf cust-1");
        MGMT_LOCK();
        return;
}



VOID
L3vpnCreateRt()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd("route-target import 200:1");
    MGMT_LOCK ();
    return;
}



VOID
L3vpnDeleteRt()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd("no route-target 200:1");
    MGMT_LOCK ();
    return;
}


VOID
L3vpnCreateIngressStaticBinding()
{
        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("i g 0/7");
        CliExecuteAppCmd("no sw");
        CliExecuteAppCmd("mpls ip");
        CliExecuteAppCmd("ip address 20.0.0.1 255.255.255.0");
        CliExecuteAppCmd("no sh");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip route 40.0.0.0 255.255.255.0 20.0.0.2");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("mpls static binding ipv4 40.0.0.0 255.255.255.0 output 20.0.0.2 200001");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("arp 20.0.0.2 00:03:02:03:04:01 gi 0/7");
        CliExecuteAppCmd("en");

        MGMT_LOCK ();

        return;
}


VOID
L3vpnDeleteIngressStaticBinding()
{
        MGMT_UNLOCK();

	CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("no arp 20.0.0.2");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("no mpls static binding ipv4");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("no ip route 40.0.0.0 255.255.255.0 20.0.0.2");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("int gigabitethernet 0/7");
        CliExecuteAppCmd("sh");
        CliExecuteAppCmd("no ip address 20.0.0.1 255.255.255.0");
        CliExecuteAppCmd("no mpls ip");
        CliExecuteAppCmd("sw");
        CliExecuteAppCmd("sh");
        CliExecuteAppCmd("exit");

        MGMT_LOCK();
}

UINT4
L3vpnFillDefaultEgressPerRouteAdd(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo)
{

        UINT4 u4VrfId=0;
        UINT4 u4BgpIfIndex=0;
        UINT4 u4NextHop=0;
        UINT4 u4Label=0;

        if(pMplsL3VpnBgp4RouteInfo==NULL)
        {
            return OSIX_FAILURE;
        }

        u4VrfId=L3VPN_DEF_CXT_ID;
        u4BgpIfIndex=L3VPN_DEF_EGRESS_IF_INDEX;
        u4NextHop=(*(UINT4 *)(u1EgressNextHop));
        u4Label=L3VPN_DEF_EGRESS_LABEL;
        u4NextHop = OSIX_HTONL (u4NextHop);

        pMplsL3VpnBgp4RouteInfo->u1LabelAllocPolicy=L3VPN_BGP4_POLICY_PER_ROUTE;
        pMplsL3VpnBgp4RouteInfo->u4IfIndex=u4BgpIfIndex;
        pMplsL3VpnBgp4RouteInfo->u4Label=u4Label;
        pMplsL3VpnBgp4RouteInfo->NextHop.u2AddressLen=L3VPN_BGP_MAX_ADDRESS_LEN;
        pMplsL3VpnBgp4RouteInfo->u1ILMAction = L3VPN_BGP4_ROUTE_ADD;
        pMplsL3VpnBgp4RouteInfo->u1LabelAction = L3VPN_BGP4_LABEL_POP;
        pMplsL3VpnBgp4RouteInfo->IpPrefix.u2Afi = 1;
        pMplsL3VpnBgp4RouteInfo->NextHop.u2Afi = 1;
        pMplsL3VpnBgp4RouteInfo->u4VrfId=u4VrfId;

        MEMCPY(pMplsL3VpnBgp4RouteInfo->NextHop.au1Address,&u4NextHop,L3VPN_BGP_MAX_ADDRESS_LEN);


        return OSIX_SUCCESS;
}

UINT4
L3vpnFillDefaultEgressPerVrfAdd(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo)
{

    UINT4 u4VrfId=0;
    UINT4 u4Label=0;

    if(pMplsL3VpnBgp4RouteInfo==NULL)
    {
        return OSIX_FAILURE;
    }

    u4VrfId=L3VPN_DEF_CXT_ID;
    u4Label=L3VPN_DEF_EGRESS_LABEL;

    pMplsL3VpnBgp4RouteInfo->u1LabelAllocPolicy=L3VPN_BGP4_POLICY_PER_VRF;
    pMplsL3VpnBgp4RouteInfo->u4Label=u4Label;

    pMplsL3VpnBgp4RouteInfo->u4VrfId = u4VrfId;
    pMplsL3VpnBgp4RouteInfo->u1ILMAction = L3VPN_BGP4_ROUTE_ADD;
    pMplsL3VpnBgp4RouteInfo->u1LabelAction = L3VPN_BGP4_LABEL_POP;
    pMplsL3VpnBgp4RouteInfo->IpPrefix.u2Afi = 1;
    pMplsL3VpnBgp4RouteInfo->NextHop.u2Afi = 1;

    return OSIX_SUCCESS;
}

UINT4
L3vpnFillDefaultIngressRouteAdd(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo)
{
        UINT4 u4Destination=0;
        UINT4 u4NextHop=0;

        pMplsL3VpnBgp4RouteInfo->IpPrefix.u2Afi = 1; /* IP v4 Address Family */
        pMplsL3VpnBgp4RouteInfo->IpPrefix.u2AddressLen = 4; /* Length of IPv4 Address */
        u4Destination = (*(UINT4 *)(u1IngressDst));
#if 0
        u4Destination = OSIX_HTONL(u4Destination);
#endif

        MEMCPY(pMplsL3VpnBgp4RouteInfo->IpPrefix.au1Address, (UINT1 *)&u4Destination, sizeof(UINT4));

        pMplsL3VpnBgp4RouteInfo->NextHop.u2Afi = 1;
        pMplsL3VpnBgp4RouteInfo->NextHop.u2AddressLen = L3VPN_BGP_MAX_ADDRESS_LEN;
        u4NextHop = (*(UINT4 *)(u1IngressNextHop));
#if 0
        u4NextHop = OSIX_HTONL(u4NextHop);
#endif

        MEMCPY(pMplsL3VpnBgp4RouteInfo->NextHop.au1Address, (UINT1 *)&u4NextHop, sizeof(UINT4));

        pMplsL3VpnBgp4RouteInfo->u4Label = L3VPN_DEF_EGRESS_LABEL;
        pMplsL3VpnBgp4RouteInfo->u4IfIndex = L3VPN_DEF_INGRESS_IF_INDEX;
        pMplsL3VpnBgp4RouteInfo->u4VrfId = L3VPN_DEF_CXT_ID;
        pMplsL3VpnBgp4RouteInfo->u4NHAS = L3VPN_NXT_HOP_AS;
        pMplsL3VpnBgp4RouteInfo->u4Metric = L3VPN_INGRESS_METRIC;
        pMplsL3VpnBgp4RouteInfo->u2PrefixLen = L3VPN_INGRESS_PREFIX_LEN;
        pMplsL3VpnBgp4RouteInfo->u1ILMAction = L3VPN_BGP4_ROUTE_ADD;
        pMplsL3VpnBgp4RouteInfo->u1LabelAction = L3VPN_BGP4_LABEL_PUSH;

        return OSIX_SUCCESS;
}


UINT4
L3vpnFillDefaultIngressRouteDel(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo)
{
        UINT4 u4Destination=0;
        UINT4 u4NextHop=0;

        pMplsL3VpnBgp4RouteInfo->IpPrefix.u2Afi = 1; /* IP v4 Address Family */
        pMplsL3VpnBgp4RouteInfo->IpPrefix.u2AddressLen = 4; /* Length of IPv4 Address */
        u4Destination = (*(UINT4 *)(u1IngressDst));
#if 0
        u4Destination = OSIX_HTONL(u4Destination);
#endif

        MEMCPY(pMplsL3VpnBgp4RouteInfo->IpPrefix.au1Address, (UINT1 *)&u4Destination, sizeof(UINT4));

        pMplsL3VpnBgp4RouteInfo->NextHop.u2Afi = 1;
        pMplsL3VpnBgp4RouteInfo->NextHop.u2AddressLen = L3VPN_BGP_MAX_ADDRESS_LEN;
        u4NextHop = (*(UINT4 *)(u1IngressNextHop));
#if 0
        u4NextHop = OSIX_HTONL(u4NextHop);
#endif

        MEMCPY(pMplsL3VpnBgp4RouteInfo->NextHop.au1Address, (UINT1 *)&u4NextHop, sizeof(UINT4));

        pMplsL3VpnBgp4RouteInfo->u4Label = L3VPN_DEF_EGRESS_LABEL;
        pMplsL3VpnBgp4RouteInfo->u4VrfId = L3VPN_DEF_CXT_ID;
        pMplsL3VpnBgp4RouteInfo->u2PrefixLen = L3VPN_INGRESS_PREFIX_LEN;
        pMplsL3VpnBgp4RouteInfo->u1ILMAction = L3VPN_BGP4_ROUTE_DEL;
        pMplsL3VpnBgp4RouteInfo->u1LabelAction = L3VPN_BGP4_LABEL_PUSH;

        return OSIX_SUCCESS;
}


UINT4
L3vpnFillDefaultEgressRouteDel(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo)
{

    UINT4 u4VrfId=0;
    UINT4 u4Label=0;

    if(pMplsL3VpnBgp4RouteInfo==NULL)
    {
        return OSIX_FAILURE;
    }

    u4VrfId=L3VPN_DEF_CXT_ID;
    u4Label=L3VPN_DEF_EGRESS_LABEL;

    pMplsL3VpnBgp4RouteInfo->u4VrfId=u4VrfId;
    pMplsL3VpnBgp4RouteInfo->u4Label=u4Label;
    pMplsL3VpnBgp4RouteInfo->u1ILMAction = L3VPN_BGP4_ROUTE_DEL;
    pMplsL3VpnBgp4RouteInfo->u1LabelAction = L3VPN_BGP4_LABEL_POP;
    pMplsL3VpnBgp4RouteInfo->u1LabelAllocPolicy=L3VPN_BGP4_POLICY_PER_ROUTE;
    pMplsL3VpnBgp4RouteInfo->IpPrefix.u2Afi = 1;
    pMplsL3VpnBgp4RouteInfo->NextHop.u2Afi = 1;

    return OSIX_SUCCESS;
}


UINT4
L3vpnAllocateBgpRouteLabelEntry()
{
    UINT4 u4EntryCount=0;

    for(u4EntryCount=0;u4EntryCount<MAX_L3VPN_EGRESS_ROUTE_TABLE_ENTRIES;u4EntryCount++)
    {
        pL3VpnBgpRouteLabelEntry[u4EntryCount] =
                 (tL3VpnBgpRouteLabelEntry *)
                 MemAllocMemBlk (L3VPN_BGPROUTELABELTABLE_POOLID);
    }

    return OSIX_SUCCESS;
}



UINT4 
L3vpnAllocateVrfRteEntry()
{
    UINT4 u4EntryCount=0;

    for(u4EntryCount=0;u4EntryCount<MAX_L3VPN_ROUTE_TABLE_ENTRIES;u4EntryCount++)
    {
        pL3vpnMplsL3VpnVrfRteEntry[u4EntryCount] =
                 (tL3vpnMplsL3VpnVrfRteEntry *)
                 MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    }

    return OSIX_SUCCESS;
}

UINT4
L3vpnFreeVrfRteEntry()
{
        UINT4 u4EntryCount=0;

        for(u4EntryCount=0;u4EntryCount<MAX_L3VPN_ROUTE_TABLE_ENTRIES;u4EntryCount++)
        {
                if(pL3vpnMplsL3VpnVrfRteEntry[u4EntryCount]!=NULL)
                {

                        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry[u4EntryCount]);
                        pL3vpnMplsL3VpnVrfRteEntry[u4EntryCount] = NULL;
                }
                else
                {
                        break;
                }
        }
        return OSIX_SUCCESS;
}

UINT4
L3vpnFreeBgpRouteLabelEntry()
{
    UINT4 u4EntryCount=0;

     for(u4EntryCount=0;u4EntryCount<MAX_L3VPN_EGRESS_ROUTE_TABLE_ENTRIES;u4EntryCount++)
     {
        if(pL3VpnBgpRouteLabelEntry[u4EntryCount]!=NULL)
        {

                MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                                (UINT1 *) pL3VpnBgpRouteLabelEntry[u4EntryCount]);
                pL3VpnBgpRouteLabelEntry[u4EntryCount]=NULL;
        }
        else
        {
                break;
        }
     }
    return OSIX_SUCCESS;
}


tL3vpnMplsL3VpnVrfEntry*
L3vpnGetVrfEntry()
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry=NULL;
    INT4 i4MplsL3VpnVrfNameLen=0;

    i4MplsL3VpnVrfNameLen=STRLEN(L3VPN_DEF_VRF_NAME);

    pL3vpnMplsL3VpnVrfEntry=MplsL3VpnApiGetVrfTableEntry(L3VPN_DEF_VRF_NAME,
                    i4MplsL3VpnVrfNameLen);


    return pL3vpnMplsL3VpnVrfEntry;
    
}

VOID
L3vpnCreateIngressRoute_1()
{
        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip vrf cust-1");
        CliExecuteAppCmd("ip vrf cust-1 mpls");
        CliExecuteAppCmd("mpls-bgp add route destination 35.0.0.20 prefix-len 24 next-hop 40.0.0.0 next-hop-as 1 ifindex 10 metric 100 vpn-label 500");
        MGMT_LOCK();

}

VOID
L3vpnCreateIngressRoute_2()
{
        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip route 41.0.0.0 255.255.255.0 20.0.0.2");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("mpls static binding ipv4 41.0.0.0 255.255.255.0 output 20.0.0.2 200002");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip vrf cust-1");
        CliExecuteAppCmd("ip vrf cust-1 mpls");
        CliExecuteAppCmd("mpls-bgp add route destination 36.0.0.20 prefix-len 24 next-hop 41.0.0.0 next-hop-as 1 ifindex 10 metric 100 vpn-label 600");
        CliExecuteAppCmd("en");

        MGMT_LOCK();
}



VOID
L3vpnCreateIngressRoute_3()
{
        MGMT_UNLOCK ();
        CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip route 42.0.0.0 255.255.255.0 20.0.0.2");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("mpls static binding ipv4 42.0.0.0 255.255.255.0 output 20.0.0.2 200003");
        CliExecuteAppCmd("en");

        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("ip vrf cust-1");
        CliExecuteAppCmd("ip vrf cust-1 mpls");
        CliExecuteAppCmd("mpls-bgp add route destination 37.0.0.20 prefix-len 24 next-hop 42.0.0.0 next-hop-as 1 ifindex 10 metric 100 vpn-label 700");
        CliExecuteAppCmd("en");

        MGMT_LOCK();
}


UINT4
L3VpnUtExhaustQMsgPool()
{
    UINT4          u4Count = 0;
    for(u4Count = 1; u4Count <= MAX_L3VPN_Q_MSG; u4Count++)
    {
        pu1TmpMsg[u4Count] = (UINT1 *)MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
        if(pu1TmpMsg[u4Count] == NULL)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS; 
}

UINT4
L3VpnFreeQMem()
{
    UINT4          u4Count = 0;
    for(u4Count = 1; u4Count <= MAX_L3VPN_Q_MSG; u4Count++)
    {
        if(MemReleaseMemBlock (L3VPN_Q_MSG_POOL_ID, pu1TmpMsg[u4Count]) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

UINT4
L3VpnUtCreateVrf()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd("en");
    MGMT_LOCK ();

}

UINT4
L3VpnDeleteVrf()
{
     MGMT_UNLOCK ();
     CliExecuteAppCmd("c t");
     CliExecuteAppCmd("no ip vrf cust-1 mpls");
     CliExecuteAppCmd("no ip vrf cust-1");
     CliExecuteAppCmd("en");
     MGMT_LOCK ();
}

VOID
L3VpnUtMapInterfaceToVrf(UINT4 u4IfIndex, BOOL1 b1IsNoSh)
{
    char cli[30];
    SPRINTF(cli, "i g 0/%d", u4IfIndex);
    MGMT_UNLOCK();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd(cli);
    CliExecuteAppCmd("no sw");
    CliExecuteAppCmd("ip address 10.0.0.1 255.255.255.0");
    CliExecuteAppCmd("ip vrf forwarding cust-1");
    if(b1IsNoSh == TRUE)
    {
        CliExecuteAppCmd("no sh");
    }
    CliExecuteAppCmd("en");
    MGMT_LOCK();
}


VOID
L3VpnUtUnMapInterfaceToVrf(UINT4 u4IfIndex)
{
    char cli[30];
    char nocli[30];

    SPRINTF(cli, "i g 0/%d", u4IfIndex);
    SPRINTF(nocli, "no i g 0/%d", u4IfIndex);

    MGMT_UNLOCK();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd(cli);
    CliExecuteAppCmd("no ip vrf forwarding cust-1");
    CliExecuteAppCmd("sh");
    CliExecuteAppCmd("ex");
    CliExecuteAppCmd(nocli);
    CliExecuteAppCmd("en");
    MGMT_LOCK ();
}

VOID
L3VpnUtAddRtToVrf()
{
   MGMT_UNLOCK();
   CliExecuteAppCmd("end");
   CliExecuteAppCmd("c t");
   CliExecuteAppCmd("ip vrf cust-1 mpls");
   CliExecuteAppCmd("route-target both 100:1");
   CliExecuteAppCmd("route-target import 200:1");
   CliExecuteAppCmd("en");
   MGMT_LOCK();
}

UINT4
L3VpnUtAddIngressRoute()
{
   
    MGMT_UNLOCK ();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("i g 0/7");
    CliExecuteAppCmd("no sw");
    CliExecuteAppCmd("mpls ip");
    CliExecuteAppCmd("ip address 20.0.0.1 255.255.255.0");
    CliExecuteAppCmd("no sh");
    CliExecuteAppCmd("en");


    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip route 40.0.0.0 255.255.255.0 20.0.0.2");
    CliExecuteAppCmd("en");


    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("arp 20.0.0.2 00:03:02:03:04:01 gi 0/7");
    CliExecuteAppCmd("en");

    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("mpls static binding ipv4 40.0.0.0 255.255.255.0 output 20.0.0.2 200001");
    CliExecuteAppCmd("en");
 
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd("mpls-bgp add route destination 25.0.0.1 prefix-len 24 next-hop 40.0.0.0 next-hop-as 1 ifindex 10 metric 100 vpn-label 500");
    CliExecuteAppCmd("en");
    MGMT_LOCK ();
}

VOID
L3VpnAddRouteAtIngress(UINT4 u4Dest)
{
    char cli[300];
    SPRINTF(cli,"mpls-bgp add route destination 25.0.0.%d prefix-len 24 next-hop 40.0.0.0 next-hop-as 1 ifindex 10 metric 100 vpn-label 500", u4Dest);   
    MGMT_UNLOCK();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd(cli);
    CliExecuteAppCmd("en");
    MGMT_LOCK ();

}

VOID
L3VpnDeleteRouteAtIngress(UINT4 u4Dest)
{
    char cli[300];
    SPRINTF(cli, "no mpls-bgp route destination 25.0.0.%d prefix-len 24 next-hop 40.0.0.0", u4Dest);
    MGMT_UNLOCK();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd(cli);
    CliExecuteAppCmd("en");
    MGMT_LOCK ();

}

UINT4
L3VpnUtLspCleanUp()
{

    MGMT_UNLOCK ();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("no ip route 40.0.0.0 255.255.255.0 20.0.0.2");
    CliExecuteAppCmd("en");

    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("no mpls static binding ipv4 40.0.0.0 255.255.255.0 output 20.0.0.2 200001");
    CliExecuteAppCmd("en");
  
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("no arp 20.0.0.2");
    CliExecuteAppCmd("en");


    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("i g 0/7");
    CliExecuteAppCmd("no mpls ip");
    CliExecuteAppCmd("sh");
    CliExecuteAppCmd("ex"); 
    CliExecuteAppCmd("no i g 0/7");
    CliExecuteAppCmd("en");
    MGMT_LOCK ();

}


VOID
L3VpnAddRtToVrf()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd("end");
    CliExecuteAppCmd("c t");
    CliExecuteAppCmd("ip vrf cust-1 mpls");
    CliExecuteAppCmd("route-target both 100:1");
    CliExecuteAppCmd("route-target import 1.1.1.1:1");
    CliExecuteAppCmd("route-target export 200:1");
    CliExecuteAppCmd("en");
    MGMT_LOCK (); 
}

VOID 
L3VpnDeleteRtFromVrf()
{
	MGMT_UNLOCK ();
	CliExecuteAppCmd("end");
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("ip vrf cust-1 mpls");
	CliExecuteAppCmd("no route-target 100:1");
	CliExecuteAppCmd("no route-target 1.1.1.1:1");
	CliExecuteAppCmd("no route-target 200:1");
	CliExecuteAppCmd("en");
	MGMT_LOCK ();
}


VOID
L3vpnAddPortToVrf()
{
	MGMT_UNLOCK ();
	CliExecuteAppCmd("end");
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/1");
	CliExecuteAppCmd("no sw");
	CliExecuteAppCmd("ip vrf forw cust-1");
	CliExecuteAppCmd("en");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/1");
	CliExecuteAppCmd("ip address 10.0.0.1 255.255.255.0");
	CliExecuteAppCmd("no sh");
	CliExecuteAppCmd("en");
	MGMT_LOCK();
}

VOID
L3vpnDelPortFromVrf()
{
	MGMT_UNLOCK ();
	CliExecuteAppCmd("end");
        CliExecuteAppCmd("c t");
        CliExecuteAppCmd("i g 0/1");
        CliExecuteAppCmd("no sw");
        CliExecuteAppCmd("no ip vrf forw cust-1");
	CliExecuteAppCmd("sh");
	CliExecuteAppCmd("exit");
	CliExecuteAppCmd("no int gig 0/1");
        CliExecuteAppCmd("en");
	MGMT_LOCK();
}


VOID Bgp4MplsL3VpnNotifyRouteParams (UINT4 u4VrfId, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam)
{

}
VOID Bgp4MplsL3VpnNotifyLspStatusChange (UINT4 u4LspLabel, UINT1 u1LspStatus)
{

}
VOID Bgp4MplsL3vpnNotifyVrfAdminStatusChange (UINT4 u4VrfId, UINT1 u1VrfStatus)
{

}



VOID
L3vpnAllocateVarBindPool()
{
	INT4 i4Counter=0;
	extern tMemPoolId gSnmpVarBindPoolId,gSnmpOidTypePoolId;
	for(i4Counter=0;i4Counter<MAX_SNMP_VARBIND_BLOCKS;i4Counter++)
	{
		printf("allocating PoolId=%d %d\n",gSnmpVarBindPoolId,gSnmpOidTypePoolId);

		pVarBindPtr[i4Counter]=MemAllocMemBlk (gSnmpVarBindPoolId);
	}
}

VOID
L3vpnClearVarBindPool()
{
	INT4 i4Counter=0;
        extern tMemPoolId gSnmpVarBindPoolId,gSnmpOidTypePoolId;


	for(i4Counter=0;i4Counter<MAX_SNMP_VARBIND_BLOCKS;i4Counter++)
	{
		if(pVarBindPtr[i4Counter]!=NULL)
		{
			printf("Clearing PoolId=%d %d \n",gSnmpVarBindPoolId,gSnmpOidTypePoolId);
			MemReleaseMemBlock (gSnmpVarBindPoolId,
					(UINT1 *) (pVarBindPtr[i4Counter]));
			pVarBindPtr[i4Counter]=NULL;
		}

	}
}


VOID
L3vpnEgressNpapiSuccess()
{

	MGMT_UNLOCK ();

	CliExecuteAppCmd("end");
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/7");
	CliExecuteAppCmd("no sw");
	CliExecuteAppCmd("mpls ip");
	CliExecuteAppCmd("ip address 20.0.0.2 255.255.255.0");
	CliExecuteAppCmd("no sh");
	CliExecuteAppCmd("en");


	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("mpls static binding ipv4 40.0.0.0 255.255.255.0 input gig 0/7 200001");
	CliExecuteAppCmd("en");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("ip vrf cust-1");
	CliExecuteAppCmd("ip vrf cust-1 mpls");
	CliExecuteAppCmd("end");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/1");
	CliExecuteAppCmd("no sw");
	CliExecuteAppCmd("ip vrf forw cust-1");
	CliExecuteAppCmd("en");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/1");
	CliExecuteAppCmd("ip addr 35.0.0.21 255.255.255.0");
	CliExecuteAppCmd("no sh");
	CliExecuteAppCmd("en");


	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("arp 35.0.0.20 00:02:02:03:03:01 gi 0/1");
	CliExecuteAppCmd("en");


	CliExecuteAppCmd("mpls l3vpn egress-route add per-route cxt-id 1 interface-idx 1 nextHop-Ip 35.0.0.20 label 500");
	CliExecuteAppCmd("end");

	MGMT_LOCK();
}

VOID
L3vpnEgressNpapiRemoveSuccess()
{
	MGMT_UNLOCK ();

	CliExecuteAppCmd("mpls l3vpn egress-route delete label 500 cxt-id 1");
	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/1");
	CliExecuteAppCmd("sh");
	CliExecuteAppCmd("exit");
	CliExecuteAppCmd("no int g 0/1");
	CliExecuteAppCmd("end");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("no ip vrf cust-1");
	CliExecuteAppCmd("end");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("no mpls static binding ipv4");
	CliExecuteAppCmd("end");

	CliExecuteAppCmd("c t");
	CliExecuteAppCmd("i g 0/7");
	CliExecuteAppCmd("sh");
	CliExecuteAppCmd("no mpls ip");
	CliExecuteAppCmd("exit");
	CliExecuteAppCmd("no int gig 0/7");
	CliExecuteAppCmd("end");



	MGMT_LOCK ();
}




