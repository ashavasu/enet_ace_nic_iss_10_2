/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnutcli.c,v 1.2 2014/08/25 12:19:56 siva Exp $
 *
 ********************************************************************/
#include "lr.h"
#include "cli.h"

#include "fssnmp.h"
#include "params.h"


#include "l3vpndefn.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpntmr.h"
#include "l3vpntrc.h"
#include "l3vpncli.h"


#define MAX_ARGS 5


UINT4  u4TotPassedCases = 0;
UINT4  u4TotFailedCases = 0;


INT4 L3VpnUt(tCliHandle CliHandle,INT4 i4TestType);
extern VOID  L3vpnExecUnitTest(tCliHandle CliHandle, INT4 i4TestId);
VOID L3VpnExecAllUTCases(tCliHandle CliHandle);


INT4
cli_process_l3vpn_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...)
{

    va_list            ap;
    UINT4             *args[MAX_ARGS];
    INT1               argno = 0;
    INT4               i4TestcaseNo ;
    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    /* Walk through the arguements and store in args array. */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
           break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case CLI_L3VPN_UT:

            if (args[1] == NULL)
            {
                i4TestcaseNo = 0;
            }
            else
            {
                i4TestcaseNo = *args[1];
            }
            L3VpnUt (CliHandle,i4TestcaseNo);
            break;
    }
    return CLI_SUCCESS;

}


/* Function to call the test case functions in mplstest.c */

INT4
L3VpnUt(tCliHandle CliHandle,INT4 i4TestType)
{
    printf("Called L3VPNUT: TestCase Id:%d\n", i4TestType);
    if(i4TestType == 0)
    {
        /* Calls all the test cases */
		L3VpnExecAllUTCases(CliHandle);
        return CLI_SUCCESS;
    }
    else
    {   /* Calls the particular test case only */
		L3vpnExecUnitTest (CliHandle, i4TestType);
        return CLI_SUCCESS;
    }
}

VOID L3VpnExecAllUTCases(tCliHandle CliHandle)
{
     INT4 i4TestType = 1;

	for(i4TestType=1;i4TestType<=219;i4TestType++)
	{
			L3vpnExecUnitTest (CliHandle, i4TestType);
	}

	return;
     return;
}
