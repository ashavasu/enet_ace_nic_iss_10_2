/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnlwg.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 20132006 Aricent Inc . All Rights Reserved
*
* Id: l3vpnlwg.h 
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for MplsL3VpnVrfTable. */
INT1
nmhValidateIndexInstanceMplsL3VpnVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for MplsL3VpnIfConfTable. */
INT1
nmhValidateIndexInstanceMplsL3VpnIfConfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Validate Index Instance for MplsL3VpnVrfRTTable. */
INT1
nmhValidateIndexInstanceMplsL3VpnVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4 ));

/* Proto Validate Index Instance for MplsL3VpnVrfSecTable. */
INT1
nmhValidateIndexInstanceMplsL3VpnVrfSecTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for MplsL3VpnVrfPerfTable. */
INT1
nmhValidateIndexInstanceMplsL3VpnVrfPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for MplsL3VpnVrfRteTable. */
INT1
nmhValidateIndexInstanceMplsL3VpnVrfRteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsL3VpnVrfTable  */

INT1
nmhGetFirstIndexMplsL3VpnVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for MplsL3VpnIfConfTable  */

INT1
nmhGetFirstIndexMplsL3VpnIfConfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto Type for Low Level GET FIRST fn for MplsL3VpnVrfRTTable  */

INT1
nmhGetFirstIndexMplsL3VpnVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for MplsL3VpnVrfSecTable  */

INT1
nmhGetFirstIndexMplsL3VpnVrfSecTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for MplsL3VpnVrfPerfTable  */

INT1
nmhGetFirstIndexMplsL3VpnVrfPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for MplsL3VpnVrfRteTable  */

INT1
nmhGetFirstIndexMplsL3VpnVrfRteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OID_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsL3VpnVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsL3VpnIfConfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsL3VpnVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsL3VpnVrfSecTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsL3VpnVrfPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsL3VpnVrfRteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnConfiguredVrfs ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnActiveVrfs ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnConnectedInterfaces ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnNotificationEnable ARG_LIST((INT4 *));

INT1
nmhTestv2MplsL3VpnVrfConfRteMxThrshTime ARG_LIST((UINT4 * , UINT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfMaxPossRts ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfRteMxThrshTime ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnIllLblRcvThrsh ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfVpnId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfDescription ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRD ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfCreationTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfOperStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfActiveInterfaces ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfAssociatedInterfaces ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfMidRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfHighRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfMaxRoutes ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfLastChanged ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfConfStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnIfVpnClassification ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnIfVpnRouteDistProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnIfConfStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnIfConfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRT ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRTDescr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRTRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRTStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfSecIllegalLblVltns ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfSecDiscontinuityTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfPerfRoutesAdded ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfPerfRoutesDeleted ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfPerfCurrNumRoutes ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfPerfRoutesDropped ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfPerfDiscTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrAge ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrNextHopAS ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric1 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric2 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric3 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric4 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric5 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteXCPointer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsL3VpnVrfRteInetCidrStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnNotificationEnable ARG_LIST((INT4 ));

INT1
nmhSetMplsL3VpnVrfConfRteMxThrshTime ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnIllLblRcvThrsh ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfVpnId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfDescription ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRD ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfConfMidRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfConfHighRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfConfMaxRoutes ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfConfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfConfAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfConfStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnIfVpnClassification ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnIfVpnRouteDistProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnIfConfStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnIfConfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRT ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRTDescr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRTRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRTStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrNextHopAS ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric1 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric2 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric3 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric4 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric5 ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteXCPointer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsL3VpnVrfRteInetCidrStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnNotificationEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnIllLblRcvThrsh ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfVpnId ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfDescription ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRD ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfConfMidRteThresh ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfConfHighRteThresh ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfConfMaxRoutes ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfConfRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfConfAdminStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfConfStorageType ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnIfVpnClassification ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnIfVpnRouteDistProtocol ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnIfConfStorageType ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnIfConfRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRT ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRTDescr ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRTRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRTStorageType ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrIfIndex ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrType ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrNextHopAS ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric1 ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric2 ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric3 ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric4 ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric5 ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteXCPointer ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsL3VpnVrfRteInetCidrStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsL3VpnNotificationEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MplsL3VpnVrfConfRteMxThrshTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsL3VpnIllLblRcvThrsh ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsL3VpnVrfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsL3VpnIfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsL3VpnVrfRTTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsL3VpnVrfRteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
