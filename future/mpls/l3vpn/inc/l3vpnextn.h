/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnextn.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains extern declaration of global 
 *              variables of the l3vpn module.
 *******************************************************************/

#ifndef __L3VPNEXTN_H__
#define __L3VPNEXTN_H__

PUBLIC tL3vpnGlobals gL3vpnGlobals;

#endif/*__L3VPNEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file l3vpnextn.h                      */
/*-----------------------------------------------------------------------*/

