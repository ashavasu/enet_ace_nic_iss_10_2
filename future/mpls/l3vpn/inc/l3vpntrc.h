/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntrc.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __L3VPNTRC_H__
#define __L3VPNTRC_H__

#define  L3VPN_TRC_FLAG  gL3vpnGlobals.u4L3vpnTrc
#define  L3VPN_NAME      "L3VPN"               

#ifdef L3VPN_TRACE_WANTED



#define  L3VPN_TRC(x)       L3vpnTrcPrint( __FILE__, __LINE__, L3vpnTrc x)
#define  L3VPN_TRC_FUNC(x)  L3vpnTrcPrint( __FILE__, __LINE__, L3vpnTrc x)
#define  L3VPN_TRC_CRIT(x)  L3vpnTrcPrint( __FILE__, __LINE__, L3vpnTrc x)
#define  L3VPN_TRC_PKT(x)   L3vpnTrcWrite( L3vpnTrc x)




#else /* L3VPN_TRACE_WANTED */

#define  L3VPN_TRC(x) 
#define  L3VPN_TRC_FUNC(x)
#define  L3VPN_TRC_CRIT(x)
#define  L3VPN_TRC_PKT(x)

#endif /* L3VPN_TRACE_WANTED */


#define  L3VPN_FN_ENTRY_EXIT_TRC       0x100000
#define  L3VPN_MGMT_TRC                0x010000
#define  L3VPN_MAIN_TRC                0x001000
#define  L3VPN_UTIL_TRC                0x000100
#define  L3VPN_RESOURCE_TRC            0x000010
#define  L3VPN_ALL_FAIL_TRC            0x000001
#define  L3VPN_ALL_TRC                 L3VPN_FN_ENTRY_EXIT_TRC |\
                                          L3VPN_MGMT_TRC  |\
                                          L3VPN_MAIN_TRC |\
                                          L3VPN_UTIL_TRC  |\
                                          L3VPN_RESOURCE_TRC |\
                                          L3VPN_ALL_FAIL_TRC


#endif /* _L3VPNTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpntrc.h                      */
/*-----------------------------------------------------------------------*/
