/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpninc.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpninc.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 * Description: This file contains include files of l3vpn module.
 *******************************************************************/

#ifndef __L3VPNINC_H__
#define __L3VPNINC_H__ 

#include "lr.h"
#include "cli.h"

#include "fssnmp.h"
#include "snmputil.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "sizereg.h"
#include "mpl3vpncmn.h"

#include "vcm.h"
#include "trie.h"
#include "mplsapi.h"
#include "mplslsr.h"
#include "mplcmndb.h"
#include "mpl3vpncmn.h"
#include "mplsutil.h"

#include "l3vpndefn.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpntmr.h"
#include "l3vpntrc.h"

#ifdef __L3VPNMAIN_C__

#include "l3vpnglob.h"
#include "l3vpnlwg.h"
#include "l3vpndefg.h"
#include "l3vpnsz.h"
#include "l3vpnwrg.h"

#else

#include "l3vpnextn.h"
#include "l3vpnmibclig.h"
#include "l3vpnlwg.h"
#include "l3vpndefg.h"
#include "l3vpnsz.h"
#include "l3vpnwrg.h"

#endif /* __L3VPNMAIN_C__*/

#include "l3vpnprot.h"
#include "l3vpnprotg.h"

#endif   /* __L3VPNINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file l3vpninc.h                       */
/*-----------------------------------------------------------------------*/

