/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdl3vdb.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

#ifndef _STDL3VDB_H
#define _STDL3VDB_H

UINT1 MplsL3VpnVrfTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsL3VpnIfConfTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 MplsL3VpnVrfRTTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 MplsL3VpnVrfSecTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsL3VpnVrfPerfTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsL3VpnVrfRteTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OBJECT_ID ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 stdl3v [] ={1,3,6,1,2,1,10,166,11};
tSNMP_OID_TYPE stdl3vOID = {9, stdl3v};


UINT4 MplsL3VpnConfiguredVrfs [ ] ={1,3,6,1,2,1,10,166,11,1,1,1};
UINT4 MplsL3VpnActiveVrfs [ ] ={1,3,6,1,2,1,10,166,11,1,1,2};
UINT4 MplsL3VpnConnectedInterfaces [ ] ={1,3,6,1,2,1,10,166,11,1,1,3};
UINT4 MplsL3VpnNotificationEnable [ ] ={1,3,6,1,2,1,10,166,11,1,1,4};
UINT4 MplsL3VpnVrfConfMaxPossRts [ ] ={1,3,6,1,2,1,10,166,11,1,1,5};
UINT4 MplsL3VpnVrfConfRteMxThrshTime [ ] ={1,3,6,1,2,1,10,166,11,1,1,6};
UINT4 MplsL3VpnIllLblRcvThrsh [ ] ={1,3,6,1,2,1,10,166,11,1,1,7};
UINT4 MplsL3VpnVrfName [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,1};
UINT4 MplsL3VpnVrfVpnId [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,2};
UINT4 MplsL3VpnVrfDescription [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,3};
UINT4 MplsL3VpnVrfRD [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,4};
UINT4 MplsL3VpnVrfCreationTime [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,5};
UINT4 MplsL3VpnVrfOperStatus [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,6};
UINT4 MplsL3VpnVrfActiveInterfaces [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,7};
UINT4 MplsL3VpnVrfAssociatedInterfaces [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,8};
UINT4 MplsL3VpnVrfConfMidRteThresh [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,9};
UINT4 MplsL3VpnVrfConfHighRteThresh [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,10};
UINT4 MplsL3VpnVrfConfMaxRoutes [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,11};
UINT4 MplsL3VpnVrfConfLastChanged [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,12};
UINT4 MplsL3VpnVrfConfRowStatus [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,13};
UINT4 MplsL3VpnVrfConfAdminStatus [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,14};
UINT4 MplsL3VpnVrfConfStorageType [ ] ={1,3,6,1,2,1,10,166,11,1,2,2,1,15};
UINT4 MplsL3VpnIfConfIndex [ ] ={1,3,6,1,2,1,10,166,11,1,2,1,1,1};
UINT4 MplsL3VpnIfVpnClassification [ ] ={1,3,6,1,2,1,10,166,11,1,2,1,1,2};
UINT4 MplsL3VpnIfVpnRouteDistProtocol [ ] ={1,3,6,1,2,1,10,166,11,1,2,1,1,3};
UINT4 MplsL3VpnIfConfStorageType [ ] ={1,3,6,1,2,1,10,166,11,1,2,1,1,4};
UINT4 MplsL3VpnIfConfRowStatus [ ] ={1,3,6,1,2,1,10,166,11,1,2,1,1,5};
UINT4 MplsL3VpnVrfRTIndex [ ] ={1,3,6,1,2,1,10,166,11,1,2,3,1,2};
UINT4 MplsL3VpnVrfRTType [ ] ={1,3,6,1,2,1,10,166,11,1,2,3,1,3};
UINT4 MplsL3VpnVrfRT [ ] ={1,3,6,1,2,1,10,166,11,1,2,3,1,4};
UINT4 MplsL3VpnVrfRTDescr [ ] ={1,3,6,1,2,1,10,166,11,1,2,3,1,5};
UINT4 MplsL3VpnVrfRTRowStatus [ ] ={1,3,6,1,2,1,10,166,11,1,2,3,1,6};
UINT4 MplsL3VpnVrfRTStorageType [ ] ={1,3,6,1,2,1,10,166,11,1,2,3,1,7};
UINT4 MplsL3VpnVrfSecIllegalLblVltns [ ] ={1,3,6,1,2,1,10,166,11,1,2,6,1,1};
UINT4 MplsL3VpnVrfSecDiscontinuityTime [ ] ={1,3,6,1,2,1,10,166,11,1,2,6,1,2};
UINT4 MplsL3VpnVrfPerfRoutesAdded [ ] ={1,3,6,1,2,1,10,166,11,1,3,1,1,1};
UINT4 MplsL3VpnVrfPerfRoutesDeleted [ ] ={1,3,6,1,2,1,10,166,11,1,3,1,1,2};
UINT4 MplsL3VpnVrfPerfCurrNumRoutes [ ] ={1,3,6,1,2,1,10,166,11,1,3,1,1,3};
UINT4 MplsL3VpnVrfPerfRoutesDropped [ ] ={1,3,6,1,2,1,10,166,11,1,3,1,1,4};
UINT4 MplsL3VpnVrfPerfDiscTime [ ] ={1,3,6,1,2,1,10,166,11,1,3,1,1,5};
UINT4 MplsL3VpnVrfRteInetCidrDestType [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,1};
UINT4 MplsL3VpnVrfRteInetCidrDest [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,2};
UINT4 MplsL3VpnVrfRteInetCidrPfxLen [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,3};
UINT4 MplsL3VpnVrfRteInetCidrPolicy [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,4};
UINT4 MplsL3VpnVrfRteInetCidrNHopType [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,5};
UINT4 MplsL3VpnVrfRteInetCidrNextHop [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,6};
UINT4 MplsL3VpnVrfRteInetCidrIfIndex [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,7};
UINT4 MplsL3VpnVrfRteInetCidrType [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,8};
UINT4 MplsL3VpnVrfRteInetCidrProto [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,9};
UINT4 MplsL3VpnVrfRteInetCidrAge [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,10};
UINT4 MplsL3VpnVrfRteInetCidrNextHopAS [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,11};
UINT4 MplsL3VpnVrfRteInetCidrMetric1 [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,12};
UINT4 MplsL3VpnVrfRteInetCidrMetric2 [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,13};
UINT4 MplsL3VpnVrfRteInetCidrMetric3 [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,14};
UINT4 MplsL3VpnVrfRteInetCidrMetric4 [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,15};
UINT4 MplsL3VpnVrfRteInetCidrMetric5 [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,16};
UINT4 MplsL3VpnVrfRteXCPointer [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,17};
UINT4 MplsL3VpnVrfRteInetCidrStatus [ ] ={1,3,6,1,2,1,10,166,11,1,4,1,1,18};




tMbDbEntry stdl3vMibEntry[]= {

{{12,MplsL3VpnConfiguredVrfs}, NULL, MplsL3VpnConfiguredVrfsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsL3VpnActiveVrfs}, NULL, MplsL3VpnActiveVrfsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsL3VpnConnectedInterfaces}, NULL, MplsL3VpnConnectedInterfacesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsL3VpnNotificationEnable}, NULL, MplsL3VpnNotificationEnableGet, MplsL3VpnNotificationEnableSet, MplsL3VpnNotificationEnableTest, MplsL3VpnNotificationEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,MplsL3VpnVrfConfMaxPossRts}, NULL, MplsL3VpnVrfConfMaxPossRtsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsL3VpnVrfConfRteMxThrshTime}, NULL, MplsL3VpnVrfConfRteMxThrshTimeGet, MplsL3VpnVrfConfRteMxThrshTimeSet, MplsL3VpnVrfConfRteMxThrshTimeTest,     MplsL3VpnVrfConfRteMxThrshTimeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,MplsL3VpnIllLblRcvThrsh}, NULL, MplsL3VpnIllLblRcvThrshGet, MplsL3VpnIllLblRcvThrshSet, MplsL3VpnIllLblRcvThrshTest, MplsL3VpnIllLblRcvThrshDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,MplsL3VpnIfConfIndex}, GetNextIndexMplsL3VpnIfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsL3VpnIfConfTableINDEX, 2, 0, 0, NULL},

{{14,MplsL3VpnIfVpnClassification}, GetNextIndexMplsL3VpnIfConfTable, MplsL3VpnIfVpnClassificationGet, MplsL3VpnIfVpnClassificationSet, MplsL3VpnIfVpnClassificationTest, MplsL3VpnIfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnIfConfTableINDEX, 2, 0, 0, "2"},

{{14,MplsL3VpnIfVpnRouteDistProtocol}, GetNextIndexMplsL3VpnIfConfTable, MplsL3VpnIfVpnRouteDistProtocolGet, MplsL3VpnIfVpnRouteDistProtocolSet, MplsL3VpnIfVpnRouteDistProtocolTest, MplsL3VpnIfConfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsL3VpnIfConfTableINDEX, 2, 0, 0, NULL},

{{14,MplsL3VpnIfConfStorageType}, GetNextIndexMplsL3VpnIfConfTable, MplsL3VpnIfConfStorageTypeGet, MplsL3VpnIfConfStorageTypeSet, MplsL3VpnIfConfStorageTypeTest, MplsL3VpnIfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnIfConfTableINDEX, 2, 0, 0, "2"},

{{14,MplsL3VpnIfConfRowStatus}, GetNextIndexMplsL3VpnIfConfTable, MplsL3VpnIfConfRowStatusGet, MplsL3VpnIfConfRowStatusSet, MplsL3VpnIfConfRowStatusTest, MplsL3VpnIfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnIfConfTableINDEX, 2, 0, 1, NULL},

{{14,MplsL3VpnVrfName}, GetNextIndexMplsL3VpnVrfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfVpnId}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfVpnIdGet, MplsL3VpnVrfVpnIdSet, MplsL3VpnVrfVpnIdTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfDescription}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfDescriptionGet, MplsL3VpnVrfDescriptionSet, MplsL3VpnVrfDescriptionTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, ""},

{{14,MplsL3VpnVrfRD}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfRDGet, MplsL3VpnVrfRDSet, MplsL3VpnVrfRDTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, ""},

{{14,MplsL3VpnVrfCreationTime}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfOperStatus}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfActiveInterfaces}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfActiveInterfacesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfAssociatedInterfaces}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfAssociatedInterfacesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfConfMidRteThresh}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfMidRteThreshGet, MplsL3VpnVrfConfMidRteThreshSet, MplsL3VpnVrfConfMidRteThreshTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, "0"},

{{14,MplsL3VpnVrfConfHighRteThresh}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfHighRteThreshGet, MplsL3VpnVrfConfHighRteThreshSet, MplsL3VpnVrfConfHighRteThreshTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, "0"},

{{14,MplsL3VpnVrfConfMaxRoutes}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfMaxRoutesGet, MplsL3VpnVrfConfMaxRoutesSet, MplsL3VpnVrfConfMaxRoutesTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, "0"},

{{14,MplsL3VpnVrfConfLastChanged}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfLastChangedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfConfRowStatus}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfRowStatusGet, MplsL3VpnVrfConfRowStatusSet, MplsL3VpnVrfConfRowStatusTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 1, NULL},

{{14,MplsL3VpnVrfConfAdminStatus}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfAdminStatusGet, MplsL3VpnVrfConfAdminStatusSet, MplsL3VpnVrfConfAdminStatusTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfConfStorageType}, GetNextIndexMplsL3VpnVrfTable, MplsL3VpnVrfConfStorageTypeGet, MplsL3VpnVrfConfStorageTypeSet, MplsL3VpnVrfConfStorageTypeTest, MplsL3VpnVrfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfTableINDEX, 1, 0, 0, "2"},

{{14,MplsL3VpnVrfRTIndex}, GetNextIndexMplsL3VpnVrfRTTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsL3VpnVrfRTTableINDEX, 3, 0, 0, NULL},

{{14,MplsL3VpnVrfRTType}, GetNextIndexMplsL3VpnVrfRTTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsL3VpnVrfRTTableINDEX, 3, 0, 0, NULL},

{{14,MplsL3VpnVrfRT}, GetNextIndexMplsL3VpnVrfRTTable, MplsL3VpnVrfRTGet, MplsL3VpnVrfRTSet, MplsL3VpnVrfRTTest, MplsL3VpnVrfRTTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsL3VpnVrfRTTableINDEX, 3, 0, 0, ""},

{{14,MplsL3VpnVrfRTDescr}, GetNextIndexMplsL3VpnVrfRTTable, MplsL3VpnVrfRTDescrGet, MplsL3VpnVrfRTDescrSet, MplsL3VpnVrfRTDescrTest, MplsL3VpnVrfRTTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsL3VpnVrfRTTableINDEX, 3, 0, 0, ""},

{{14,MplsL3VpnVrfRTRowStatus}, GetNextIndexMplsL3VpnVrfRTTable, MplsL3VpnVrfRTRowStatusGet, MplsL3VpnVrfRTRowStatusSet, MplsL3VpnVrfRTRowStatusTest, MplsL3VpnVrfRTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfRTTableINDEX, 3, 0, 1, NULL},

{{14,MplsL3VpnVrfRTStorageType}, GetNextIndexMplsL3VpnVrfRTTable, MplsL3VpnVrfRTStorageTypeGet, MplsL3VpnVrfRTStorageTypeSet, MplsL3VpnVrfRTStorageTypeTest, MplsL3VpnVrfRTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfRTTableINDEX, 3, 0, 0, "2"},

{{14,MplsL3VpnVrfSecIllegalLblVltns}, GetNextIndexMplsL3VpnVrfSecTable, MplsL3VpnVrfSecIllegalLblVltnsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsL3VpnVrfSecTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfSecDiscontinuityTime}, GetNextIndexMplsL3VpnVrfSecTable, MplsL3VpnVrfSecDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsL3VpnVrfSecTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfPerfRoutesAdded}, GetNextIndexMplsL3VpnVrfPerfTable, MplsL3VpnVrfPerfRoutesAddedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsL3VpnVrfPerfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfPerfRoutesDeleted}, GetNextIndexMplsL3VpnVrfPerfTable, MplsL3VpnVrfPerfRoutesDeletedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsL3VpnVrfPerfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfPerfCurrNumRoutes}, GetNextIndexMplsL3VpnVrfPerfTable, MplsL3VpnVrfPerfCurrNumRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MplsL3VpnVrfPerfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfPerfRoutesDropped}, GetNextIndexMplsL3VpnVrfPerfTable, MplsL3VpnVrfPerfRoutesDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsL3VpnVrfPerfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfPerfDiscTime}, GetNextIndexMplsL3VpnVrfPerfTable, MplsL3VpnVrfPerfDiscTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsL3VpnVrfPerfTableINDEX, 1, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrDestType}, GetNextIndexMplsL3VpnVrfRteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrDest}, GetNextIndexMplsL3VpnVrfRteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrPfxLen}, GetNextIndexMplsL3VpnVrfRteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrPolicy}, GetNextIndexMplsL3VpnVrfRteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrNHopType}, GetNextIndexMplsL3VpnVrfRteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrNextHop}, GetNextIndexMplsL3VpnVrfRteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrIfIndex}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrIfIndexGet, MplsL3VpnVrfRteInetCidrIfIndexSet, MplsL3VpnVrfRteInetCidrIfIndexTest, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "0"},

{{14,MplsL3VpnVrfRteInetCidrType}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrTypeGet, MplsL3VpnVrfRteInetCidrTypeSet, MplsL3VpnVrfRteInetCidrTypeTest, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "1"},

{{14,MplsL3VpnVrfRteInetCidrProto}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrAge}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrNextHopAS}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrNextHopASGet, MplsL3VpnVrfRteInetCidrNextHopASSet, MplsL3VpnVrfRteInetCidrNextHopASTest, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "0"},

{{14,MplsL3VpnVrfRteInetCidrMetric1}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrMetric1Get, MplsL3VpnVrfRteInetCidrMetric1Set, MplsL3VpnVrfRteInetCidrMetric1Test, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "-1"},

{{14,MplsL3VpnVrfRteInetCidrMetric2}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrMetric2Get, MplsL3VpnVrfRteInetCidrMetric2Set, MplsL3VpnVrfRteInetCidrMetric2Test, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "-1"},

{{14,MplsL3VpnVrfRteInetCidrMetric3}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrMetric3Get, MplsL3VpnVrfRteInetCidrMetric3Set, MplsL3VpnVrfRteInetCidrMetric3Test, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "-1"},

{{14,MplsL3VpnVrfRteInetCidrMetric4}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrMetric4Get, MplsL3VpnVrfRteInetCidrMetric4Set, MplsL3VpnVrfRteInetCidrMetric4Test, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "-1"},

{{14,MplsL3VpnVrfRteInetCidrMetric5}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrMetric5Get, MplsL3VpnVrfRteInetCidrMetric5Set, MplsL3VpnVrfRteInetCidrMetric5Test, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, "-1"},

{{14,MplsL3VpnVrfRteXCPointer}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteXCPointerGet, MplsL3VpnVrfRteXCPointerSet, MplsL3VpnVrfRteXCPointerTest, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 0, NULL},

{{14,MplsL3VpnVrfRteInetCidrStatus}, GetNextIndexMplsL3VpnVrfRteTable, MplsL3VpnVrfRteInetCidrStatusGet, MplsL3VpnVrfRteInetCidrStatusSet, MplsL3VpnVrfRteInetCidrStatusTest, MplsL3VpnVrfRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsL3VpnVrfRteTableINDEX, 7, 0, 1, NULL},
};
tMibData stdl3vEntry = { 58, stdl3vMibEntry };

#endif /* _STDL3VDB_H */

