/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnprot.h,v 1.4 2018/01/03 11:31:20 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnprot.h,v 1.4 2018/01/03 11:31:20 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of l3vpn module.
 *******************************************************************/

#ifndef __L3VPNPROT_H__
#define __L3VPNPROT_H__ 

/* Add Prototypes here */

/**************************l3vpnmain.c*******************************/
PUBLIC VOID L3vpnMainTask             PROTO ((VOID));
PUBLIC UINT4 L3vpnMainTaskInit         PROTO ((VOID));
PUBLIC VOID L3vpnMainDeInit            PROTO ((VOID));
INT4 L3VpnProcessVcmIfUnmapEvent(tL3VpnVcmIfEvtInfo *pL3VpnVcmIfEvtInfo);
UINT4 L3VpnProcessVrfAdminEvent(tL3VpnVrfAdminEventInfo *pL3VpnVrfAdminEventInfo);
UINT4 L3VpnPrcoessVcmCrtDelEvent(tL3VpnVcmEventInfo *pL3VpnVcmEventInfo);
UINT4 L3VpnInitTrie(VOID);

UINT4
L3VpnSendTrapForRouteThreshold(UINT1 *pu1VrfName, UINT1 u1TrapType);

UINT4
L3VpnGetSubnetMaskFromPrefixLen(UINT4 u4PrefixLen);

VOID 
L3VPnInitMibScalar(VOID);

UINT4
L3VpnRegisterRteEntryWithTrie(UINT4 u4ContextId, tL3vpnMplsL3VpnVrfRteEntry * pRteEntry);

tL3vpnMplsL3VpnVrfRteEntry *
L3VpnGetRteTableEntryFromTrie (UINT4 u4ContextId, UINT4 u4Address);

tL3vpnMplsL3VpnVrfRteEntry *
L3VpnGetBestMatchRteTableEntryFromTrie (UINT4 u4ContextId, UINT4 u4Address);

UINT4
L3VpnDeleteRteTableEntryFromTrie (UINT4 u4ContextId, tL3vpnMplsL3VpnVrfRteEntry * pRteEntry);

UINT4
L3VpnDeleteRouteEntry(tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry);

INT4
MplsL3VpnCreateCrossConnectEntry(tL3vpnMplsL3VpnVrfRteEntry *pL3vpnSetMplsL3VpnVrfRteEntry, UINT4 u4Label);

/**************************l3vpnque.c********************************/
PUBLIC VOID L3vpnQueProcessMsgs        PROTO ((VOID));

/**************************l3vpnpkt.c*******************************/
#if 0
PUBLIC tL3vpnPkt *L3vpnPktAlloc        PROTO(( VOID ));
PUBLIC VOID L3vpnPktFree              PROTO(( tL3vpnPkt *pL3vpnPkt ));
PUBLIC UINT4  L3vpnPktSend            PROTO(( tL3vpnPkt *pL3vpnPkt ));
PUBLIC UINT4 L3vpnPktRecv             PROTO(( tL3vpnPkt *pL3vpnPkt));
#endif
/**************************l3vpntmr.c*******************************/
PUBLIC VOID  L3vpnTmrInitTmrDesc      PROTO(( VOID));
PUBLIC VOID  L3vpnTmrStartTmr         PROTO((tL3vpnTmr * pL3vpnTmr, 
                       enL3vpnTmrId eL3vpnTmrId, 
         UINT4 u4Secs));
PUBLIC VOID  L3vpnTmrRestartTmr       PROTO((tL3vpnTmr * pL3vpnTmr, 
                 enL3vpnTmrId eL3vpnTmrId, 
          UINT4 u4Secs));
PUBLIC VOID  L3vpnTmrStopTmr          PROTO((tL3vpnTmr * pL3vpnTmr));
PUBLIC VOID  L3vpnTmrHandleExpiry     PROTO((VOID));

/**************************l3vpntrc.c*******************************/


CHR1  *L3vpnTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   L3vpnTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   L3vpnTrcWrite      PROTO(( CHR1 *s));

/**************************l3vpntask.c*******************************/

PUBLIC INT4 L3vpnTaskRegisterL3vpnMibs (VOID);


/**************************l3vpnutil.c*******************************/

UINT4
L3VpnIssueHighThrshTrapNotif(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry,
                                                UINT4 u4CurrTime);
VOID
L3VpnUpdateVrfConfLastChanged(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

UINT4
L3VpnChckAndSendRouteNotifications(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry,
                                   tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry, UINT1 u1Action);

INT4
L3VpnIsRouteAdditionAllowed (tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry,
                             tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry);

tL3vpnMplsL3VpnVrfPerfEntry *
L3VpnGetVrfPerfTableEntry (UINT1 *pu1VrfName);

UINT4
L3VpnSendTrapForVrfStatus (UINT1 *pu1VrfName, UINT1 u1Status, INT4 i4IfIndex);

tL3vpnMplsL3VpnIfConfEntry *
L3VpnGetIfConfEntry (UINT1 *pu1VrfName, INT4 i4IfIndex);

INT4
L3VpnMaskToPrefixLen (UINT4 u4Mask);

UINT4
L3VpnDeleteRtTableForVrf(UINT1 *pu1VrfName);

UINT4
L3VpnMapInterfaceToVrf(UINT4 u4ContextId, INT4 i4IfIndex);

UINT4
L3VpnDeleteIfConfTableForVrf(UINT1 *pu1VrfName);

tL3vpnMplsL3VpnVrfEntry *
L3VpnGetVrfEntryByContextId(UINT4 u4ContextId);

UINT4
L3VpnRegisterWithVcm(VOID);

VOID
L3VpnNotifyContextChange (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event);

tL3vpnMplsL3VpnVrfRTEntry *
L3VpnGetRTEntryFromValue(UINT1 *pu1L3VpnVrfname, UINT1 *pu1RtValue);

UINT4 L3VpnInternalEventHandler(UINT1 *pu1TmpMsg);

UINT4
L3VpnProcessVcmIfMapEvent(tL3VpnVcmIfEvtInfo *pL3VpnVcmIfEvtInfo);

tL3vpnMplsL3VpnIfConfEntry *
L3VpnCreateMplsL3VpnIfConfEntry(INT4 i4IfIndex, UINT4 u4ContextId);

INT4
L3VpnBgpLspInterestInfoRbTreeCmpFunc(tRBElem * pRBElem1, tRBElem * pRBElem2);

UINT4
L3VpnProcessIfEvent(tL3VpnIfEvtInfo *pL3VpnIfEvtInfo);

UINT4
L3VpnProcessNonTeLspEvent(tL3VpnNonTeLspEventInfo  *pL3VpnNonTeLspEventInfo);

UINT4
L3VpnProcessRsvpTeLspEvent(tL3VpnRsvpTeLspEventInfo  *pL3VpnRsvpTeLspEventInfo);


UINT4
L3VpnIsRemainingInterfacesUp(UINT1 *u1VrfName, INT4 i4IfIndex);

UINT4
L3VpnIsAnyInterfaceUpForVrf(UINT1 *pu1VrfName);

UINT4
L3VpnFlushRouteTableForVrf(UINT1 *pu1VrfName);

tL3vpnMplsL3VpnIfConfEntry *
L3VpnGetIfConfEntryWithIfIndex(UINT4 u4IfIndex);

tL3vpnMplsL3VpnVrfEntry *
L3VpnGetVrfEntryWithIfIndex(INT4 i4IfIndex);

INT4
L3VpnBgpLspInterestInfoRBTreeCreate (VOID);

UINT4
L3VpnAddLspInfoToBgpInterestList(UINT4 u4Prefix);

INT4
L3VpnRegWithNonTeLowerLayer(UINT4 u4Prefix);

UINT4
L3VpnRemoveLspInfoFromBgpInterestList(UINT4 u4Prefix);

UINT4
L3VpnUpdatePerfTableCounters(UINT1 *pu1VrfName, UINT4 u4Operation);

UINT4
L3VpnUpdateNoOfConnectedInterfaces(UINT4 u1Operation);

tL3vpnMplsL3VpnVrfEntry*
MplsL3VpnApiGetVrfTableEntry (UINT1 *pu1MplsL3VpnVrfName,INT4 i4MplsL3VpnVrfNameLen);

VOID
L3VpnAdminChangeEventHandler (UINT1 *pu1VrfName, UINT4 u4AdminStatus);


UINT4
L3VpnBgpRouteLabelRBTreeCreate(VOID);

INT4
L3VpnBgpRouteLabelTableRBCmp(tRBElem * pRBElem1, tRBElem * pRBElem2);

VOID
MplsL3VpnHandleEgressMapArpResolveTmrEvent(VOID *pVoid);

UINT4 L3VpnUtilSetRtIndex(UINT4 contextId, UINT4 u4RtIndex);
UINT4 L3VpnUtilReleaseRtIndex(UINT4 contextId, UINT4 u4RtIndex);
/* L3vpn rsvp mapping rbtree declaration */
UINT4
L3VpnRsvpMapLabelRBTreeCreate(VOID);

INT4
L3VpnRsvpMapLabelTableRBCmp(tRBElem * pRBElem1, tRBElem * pRBElem2);
/*************************l3vpnutl.c ******************************/
UINT4
L3vpnUtlVrfTableRowStatusHdl(tL3vpnMplsL3VpnVrfEntry *pL3vpnOldMplsL3VpnVrfEntry,
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);
UINT4
L3vpnUtlVrfTableAdminStatusHdl(tL3vpnMplsL3VpnVrfEntry *pL3vpnOldMplsL3VpnVrfEntry,
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

UINT4
L3vpnUtlVrfSecTableRowCreate(tL3vpnMplsL3VpnVrfEntry*pL3vpnMplsL3VpnVrfEntry);

tL3vpnMplsL3VpnVrfSecEntry*
L3vpnUtlGetVrfSecTableEntry(tL3vpnMplsL3VpnVrfEntry*pL3vpnMplsL3VpnVrfEntry);

UINT4
L3vpnUtlVrfSecTableRowDelete(tL3vpnMplsL3VpnVrfSecEntry*pL3vpnMplsL3VpnVrfSecEntry);

UINT4
L3vpnUtlUpdateVrfSecTable(tL3vpnMplsL3VpnVrfSecEntry*pL3vpnMplsL3VpnVrfSecEntry,UINT4 u4Operation);

UINT4
L3vpnUtlProcessVrfCreation(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

UINT4
L3vpnUtlProcessVrfDeletion(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

VOID
L3vpnUtlInitVrfTable(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

UINT4
L3vpnUtlVrfPerfTableRowCreate(tL3vpnMplsL3VpnVrfEntry*pL3vpnMplsL3VpnVrfEntry);

tL3vpnMplsL3VpnVrfPerfEntry*
L3vpnUtlGetVrfPerfTableEntry(tL3vpnMplsL3VpnVrfEntry*pL3vpnMplsL3VpnVrfEntry);

UINT4
L3vpnUtlVrfPerfTableRowDelete(tL3vpnMplsL3VpnVrfPerfEntry*pL3vpnMplsL3VpnVrfPerfEntry);

UINT4
L3vpnUtlHandleRowStatusActive(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

UINT4
L3vpnUtlHandleRowStatusNotInService(tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);

INT4
L3VpnRtTableShow(tCliHandle CliHandle, UINT1 *pu1VrfName);

VOID
L3VpnInitRTIndexMgr(VOID);

UINT4
L3vpnUtlVrfRteTableRowStatusHdl (tL3vpnMplsL3VpnVrfRteEntry *
                              pL3vpnOldMplsL3VpnVrfRteEntry,
                              tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry);
UINT4
L3vpnUtlHandleVrfRteRowStatusActive (tL3vpnMplsL3VpnVrfRteEntry *
                pL3vpnMplsL3VpnVrfRteEntry);
UINT4
L3vpnUtlHandleVrfRteRowStatusNotInService (tL3vpnMplsL3VpnVrfRteEntry *
                pL3vpnMplsL3VpnVrfRteEntry);
UINT4
L3vpnUtlProcessVrfRteDeletion (tL3vpnMplsL3VpnVrfRteEntry *
                pL3vpnMplsL3VpnVrfRteEntry);

UINT4
L3vpnUtlProcessVrfRteCreation(tL3vpnMplsL3VpnVrfRteEntry *
                pL3vpnMplsL3VpnVrfRteEntry);

UINT4
L3vpnUtlHandleVrfRteRowStatusDestroy(tL3vpnMplsL3VpnVrfRteEntry *
                pL3vpnMplsL3VpnVrfRteEntry);





/***************************l3vpnshcli.c**************/
INT4 L3vpnCliProcessShowMplsL3VpnVrf(tCliHandle CliHandle,UINT1 *pu1MplsL3VpnVrfName,UINT4 u4MplsL3VpnVrfNameLen);
INT4 L3vpnCliShowMplsL3VpnVrf(tCliHandle CliHandle,tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);
INT4 L3vpnCliProcessShowMplsL3VpnVrfAll(tCliHandle CliHandle);


VOID L3VpnCliPrintRTInfo(tCliHandle CliHandle, tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMibMplsL3VpnVrfRTEntry);

VOID
L3VpnIfConfTableShow(tCliHandle CliHandle, UINT1 *pu1VrfName);

VOID L3VpnCliPrintIfConfInfo(tCliHandle CliHandle, tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry);

INT4 L3VpnBgpShowRouteLabelTable(tCliHandle CliHandle, UINT4 u4VrfId);
INT4 L3VpnBgpShowRouteLabel(tCliHandle CliHandle,tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry);

VOID
L3VpnVrfPerfTableShow (tCliHandle CliHandle, UINT1 *pu1MplsL3VpnVrfContextName);

VOID
L3VpnPrintVrfPerfEntry (tCliHandle CliHandle, tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry);



/*************************** l3vpnbgp.c ************/

VOID
L3VpnDeleteXcEntry(UINT4 u4XcIndex, UINT4 u4InSegmentIndex, UINT4 u4OutSegmentIndex);

VOID 
MplsL3VpnBgp4RouteAddAtEgress (tMplsL3VpnBgp4RouteInfo *pRouteInfo);
UINT4
L3VpnAddBgpRouteLabelTable(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);
UINT4
L3VpnDeleteBgpRouteLabelTable(UINT4 u4Label);
UINT4
L3VpnClearBgpRouteLabelTable(VOID);
UINT4
L3VpnDelBgpRouteLabelTable(tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);

VOID
L3VpnIngressRtTableShow (tCliHandle CliHandle, UINT1 *pu1VrfName);

VOID
L3VpnPrintRteEntry(tCliHandle CliHandle, tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry);

UINT4 
L3VpnBgp4RouteAddDelAtEgress (tMplsL3VpnBgp4RouteInfo *pMplsL3VpnBgp4RouteInfo);


/***************************l3vpnbgp.c*********************/
#if 0
UINT4
L3VpnNotifyBgp(tL3VpnBgpNotifyInfo *pBgpNotifyInfo);
#endif



UINT4 MplsL3vpnIngressMap(tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry);
UINT4 MplsL3vpnEgressMap(tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry);

UINT4 MplsL3vpnHwIngressMap(tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry,VOID *pSlotInfo);
UINT4 MplsL3vpnHwEgressMap(tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry,VOID *pSlotInfo);



UINT4 MplsL3vpnIngressUnMap(tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry);
UINT4 MplsL3vpnEgressUnMap(tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry);

UINT4 MplsL3vpnHwIngressUnMap(tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry,VOID *pSlotInfo);
UINT4 MplsL3vpnHwEgressUnMap(tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry,VOID *pSlotInfo);

tL3VpnBgpRouteLabelEntry  *L3vpnGetBgpRouteLabelEntry(UINT4 u4Label);

INT4
L3vpnCliSetMplsL3VpnRsvpTeBinding (tCliHandle CliHandle,
        UINT4 u4Prefix, UINT4 u4Mask,  UINT4 u4TunnelIndex);

INT4
L3vpnCliSetMplsL3VpnNoRsvpTeBinding (tCliHandle CliHandle,
        UINT4 u4Prefix, UINT4 u4Mask,  UINT4 u4TunnelIndex);

#endif   /* __L3VPNPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpnprot.h                     */
/*-----------------------------------------------------------------------*/
