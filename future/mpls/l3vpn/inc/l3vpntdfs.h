/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntdfs.h,v 1.3 2018/01/03 11:31:20 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpntdfs.h 
*
* Description: This file contains type definitions for L3vpn module.
*********************************************************************/

#include "vcm.h"
#include "mplsutil.h"

typedef struct

{

 tRBTree   MplsL3VpnVrfTable;
 tRBTree   MplsL3VpnIfConfTable;
 tRBTree   MplsL3VpnVrfRTTable;
 tRBTree   MplsL3VpnVrfSecTable;
 tRBTree   MplsL3VpnVrfPerfTable;
 tRBTree   MplsL3VpnVrfRteTable; UINT4 u4MplsL3VpnConfiguredVrfs;
 UINT4 u4MplsL3VpnActiveVrfs;
 UINT4 u4MplsL3VpnConnectedInterfaces;
 INT4 i4MplsL3VpnNotificationEnable;
 UINT4 u4MplsL3VpnVrfConfMaxPossRts;
 UINT4 u4MplsL3VpnVrfConfRteMxThrshTime;
 UINT4 u4MplsL3VpnIllLblRcvThrsh;

} tL3vpnGlbMib;

typedef struct
{
 tL3vpnMibMplsL3VpnVrfEntry       MibObject;
} tL3vpnMplsL3VpnVrfEntry;


typedef struct
{
 tL3vpnMibMplsL3VpnIfConfEntry       MibObject;
} tL3vpnMplsL3VpnIfConfEntry;


typedef struct
{
 tL3vpnMibMplsL3VpnVrfRTEntry       MibObject;
} tL3vpnMplsL3VpnVrfRTEntry;


typedef struct
{
 tL3vpnMibMplsL3VpnVrfSecEntry       MibObject;
} tL3vpnMplsL3VpnVrfSecEntry;


typedef struct
{
 tL3vpnMibMplsL3VpnVrfPerfEntry       MibObject;
} tL3vpnMplsL3VpnVrfPerfEntry;


typedef struct
{
 tL3vpnMibMplsL3VpnVrfRteEntry       MibObject;
} tL3vpnMplsL3VpnVrfRteEntry;



#ifndef __L3VPNTDFS_H__
#define __L3VPNTDFS_H__

/* -------------------------------------------------------
 *
 *                L3VPN   Data Structures
 *
 * ------------------------------------------------------*/

#define L3VPN_UTIL_NUM_OF_BITS_IN_UINT4          32
#define L3VPN_MAX_NUM_RT_PER_VRF                 50
#define MAX_L3VPN_VRF_ENTRIES                    SYS_DEF_MAX_NUM_CONTEXTS


#define L3VPN_CEIL(a) ceil(a)
/*#define L3VPM_MAX_U4_RT_BLKS (int)L3VPN_CEIL((float)L3VPN_MAX_NUM_RT_PER_VRF/L3VPN_UTIL_NUM_OF_BITS_IN_UINT4)*/

#define L3VPM_MAX_U4_RT_BLKS 2 /*CEIL of L3VPN_MAX_NUM_RT_PER_VRF/L3VPN_UTIL_NUM_OF_BITS_IN_UINT4*/

typedef struct _L3VpnBgpRegEntry
{
     VOID (*pBgp4MplsL3VpnNotifyRouteParams) (UINT4 u4VrfId, UINT1 u1ParamType, UINT1 u1Action, UINT1 *pu1RouteParam);
     VOID (*pBgp4MplsL3VpnNotifyLspStatusChange) (UINT4 u4LspLabel, UINT1 u1LspStatus);
     VOID (*pBgp4MplsL3vpnNotifyVrfAdminStatusChange) (UINT4 u4VrfId, UINT1 u1VrfStatus);
     VOID (*pBgp4MplsL3vpnNotifyRsvpTeStatusChange) (UINT4 u4LspLabel, UINT1 u1LspStatus);
}tL3VpnBgpRegEntry;


typedef struct L3VPN_GLOBALS {
 tTimerListId        l3vpnTmrLst;
 UINT4               u4L3vpnTrc;
 tOsixTaskId         l3vpnTaskId;
 UINT1               au1TaskSemName[8];
 tOsixSemId          l3vpnTaskSemId;
 UINT1               au1BindSemName[8];
 tOsixSemId          l3vpnBindSemId; /* RSVP-TE binding */
 tOsixQId            l3vpnQueId;
 tL3vpnGlbMib        L3vpnGlbMib;
        UINT4               gau4RtIndexBitMap[MAX_L3VPN_VRF_ENTRIES][L3VPM_MAX_U4_RT_BLKS];
        /* We can increase the value L3VPM_MAX_U4_RT_BLKS to increase the number of RT's per VRF  
         * With Value = 2, Maximum 64 RT entries can be supported 
         * This ideally should be MAX_RT_PER_VRF/SIZE_OF_UINT4 + 1 if this number
         * is not a multiple of 32, else it should be (MAX_RT_PER_VRF/SIZE_OF_UINT4)    */
       struct rbtree *   pL3VpnBgpLspInterestRBTree;
       VOID   *gpL3VpnTrieRoot[MAX_L3VPN_VRF_ENTRIES];
       tL3VpnBgpRegEntry L3VpnBgpRegEntry;
       tRBTree           L3VpnBgpRouteLabelTable;
       tRBTree           L3VpnRsvpMapLabelTable;
} tL3vpnGlobals;


#define L3VPN_TASK_ID gL3vpnGlobals.l3vpnTaskId 

typedef struct _L3VpnVcmIfEvtInfo
{
    INT4                    i4IfIndex;
    UINT4                   u4ContextId;
}
tL3VpnVcmIfEvtInfo;

typedef struct _L3VpnIfEvtInfo
{
    INT4                    i4IfIndex;
    UINT1                   u1OperStatus;
    UINT1                   u1Pad[3];
}
tL3VpnIfEvtInfo;

typedef struct _L3VpnNonTeLspEventInfo
{
    UINT4                    u4Label;
    UINT4                    u4Prefix;
    UINT4                    u4EventType;
}
tL3VpnNonTeLspEventInfo;


typedef struct _L3VpnVrfAdminEventInfo
{
    UINT1                    au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    UINT4                    u4EventType;
}
tL3VpnVrfAdminEventInfo;

typedef struct _L3VpnVcmEventInfo
{
    UINT4                    u4ContextId;
    UINT4                    u1EventType;
}
tL3VpnVcmEventInfo;


typedef union _L3VpnEvtInfo
{ 
   tL3VpnVcmIfEvtInfo          L3VpnVcmIfEvtInfo;
   tL3VpnIfEvtInfo             L3VpnIfEvtInfo;
   tL3VpnNonTeLspEventInfo     L3VpnNonTeLspEventInfo;
   tL3VpnRsvpTeLspEventInfo    L3VpnRsvpTeLspEventInfo;
   tL3VpnVrfAdminEventInfo     L3VpnVrfAdminEventInfo;
   tL3VpnVcmEventInfo          L3VpnVcmEventInfo;
}
tuL3VpnEvtInfo;


typedef struct _L3VpnQMsg
{
   UINT4             u4MsgType;
   tuL3VpnEvtInfo    L3VpnEvtInfo;
}
tL3VpnQMsg;

typedef struct _L3VpnOid
{
    UINT4     auOid[256];
}
tL3VpnOid;


typedef struct _L3VpnBgpLspInterestInfo
{
    tRBNodeEmbd      L3VpnBgpLspInterestInfoRbNode;
    UINT4 u4Prefix;
}tL3VpnBgpLspInterestInfo;



typedef struct _L3VpnBgpRouteParams
{
    UINT4 u4VrfId;
    UINT4 u4ParamType;
    UINT1 *pu1RouteParam;
    UINT1 u1Action;
    UINT1 u1Pad[3];
}tL3VpnBgpRouteParams;


typedef struct _L3VpnBgpLspStatusParams
{
    UINT4 u4Prefix;
    UINT4 u4Label;
    UINT1 u4LspStatus;
    UINT1 u1Pad[3];
}tL3VpnBgpLspStatusParams;


typedef struct _L3VpnBgpVrfStatusParams
{
    UINT4 u4VrfId;
    UINT4 u4VrfStatus;
}tL3VpnBgpVrfStatusParams;
 
typedef union _L3VpnBgpEvtInfo
{
    tL3VpnBgpRouteParams L3VpnBgpRouteParams;
    tL3VpnBgpLspStatusParams L3VpnBgpLspStatusParams;
    tL3VpnBgpVrfStatusParams L3VpnBgpVrfStatusParams;
}
tuL3VpnBgpEvtInfo;
 
typedef struct _L3VpnBgpNotifyInfo
{
    UINT4 u4NotifType;
    tuL3VpnBgpEvtInfo L3VpnBgpEvtInfo; 
}tL3VpnBgpNotifyInfo;

#endif  /* __L3VPNTDFS_H__ */
/*-----------------------------------------------------------------------*/


