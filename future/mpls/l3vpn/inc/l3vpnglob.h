/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnglob.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains declaration of global variables 
 *              of l3vpn module.
 *******************************************************************/

#ifndef __L3VPNGLOBAL_H__
#define __L3VPNGLOBAL_H__

tL3vpnGlobals gL3vpnGlobals;

#endif  /* __L3VPNGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file l3vpnglob.h                      */
/*-----------------------------------------------------------------------*/
