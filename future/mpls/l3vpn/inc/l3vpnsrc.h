/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnsrc.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnsrc.h
*
* Description: This file holds the functions for the ShowRunningConfig
*			  for L3vpn module 
*********************************************************************/

#include "l3vpncli.h"
INT4 L3vpnShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 L3vpnShowRunningConfigMplsL3VpnVrfTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 L3vpnShowRunningConfigMplsL3VpnVrfRTTable(tCliHandle CliHandle, UINT4 u4Module, ...);
void L3vpnUtlConversionForMplsL3VpnVrfName (CHR1 *pMplsL3VpnVrfName, tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);
void L3vpnUtlConversionForMplsL3VpnVrfRD (CHR1 *pMplsL3VpnVrfRD, tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry);
INT4 L3vpnShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...);

