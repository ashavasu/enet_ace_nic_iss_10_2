/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndefn.h,v 1.7 2018/01/03 11:31:20 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains definitions for l3vpn module.
 *******************************************************************/

#ifndef __L3VPNDEFN_H__
#define __L3VPNDEFN_H__

#define L3VPN_SUCCESS        (OSIX_SUCCESS)
#define L3VPN_FAILURE        (OSIX_FAILURE)

#define  L3VPN_TASK_PRIORITY              100


#define  L3VPN_TASK_NAME                  "L3VPN1"

#define  L3VPN_QUEUE_NAME                 (const UINT1 *) "L3VPNQ"
#define  L3VPN_MUT_EXCL_SEM_NAME          (const UINT1 *) "L3VPNM"
#define  L3VPN_QUEUE_DEPTH                100
#define  L3VPN_SEM_CREATE_INIT_CNT        1

#define  L3VPN_BIND_MUT_EXCL_SEM_NAME      (const UINT1 *) "L3VB"

#define  L3VPN_ENABLED                    1
#define  L3VPN_DISABLED                   2

#define  L3VPN_TIMER_EVENT             0x00000001 
#define  L3VPN_QUEUE_EVENT             0x00000002
#define  MAX_L3VPN_DUMMY                   1

#define L3VPN_VRF_ACTIVE         1
#define L3VPN_VRF_NOTINSERVICE   2
#define L3VPN_VRF_NOTREADY       3
#define L3VPN_VRF_CREATEANDGO    4
#define L3VPN_VRF_CREATEANDWAIT  5
#define L3VPN_VRF_DESTROY        6
#define L3VPN_VRF_CREATE         7


#define L3VPN_STORAGE_OTHER        1
#define L3VPN_STORAGE_VOLATILE     2
#define L3VPN_STORAGE_NONVOLATILE  3
#define L3VPN_STORAGE_PERMANENT    4
#define L3VPN_STORAGE_READONLY     5


#define L3VPN_MAX_DESC_LEN       256


#define L3VPN_MPLS_VRF_RIB       20
#define MPLS_L3VPN_APPL_ID       14

#define L3VPN_VCM_IF_MAP_EVENT             0x00000001
#define L3VPN_VCM_IF_UNMAP_EVENT           0x00000002
#define L3VPN_IF_STATUS_CHANGE_EVENT       0x00000004
#define L3VPM_MPLS_LSP_STATE_CHANGE_EVENT  0x00000008
#define L3VPN_VRF_ADMIN_EVENT              0x00000010
#define L3VPN_VRF_VCM_CREATE_DEL_EVENT     0x00000020
#define L3VPN_RPTE_DEL_TNL_EVENT           0x00000040
#define L3VPN_RPTE_LSP_UP_EVENT            0x00000060
#define L3VPN_RPTE_LSP_DOWN_EVENT          0x00000080
#define L3VPN_RSVPTE_LSP_STATE_CHANGE_EVENT  0x00000100

#define L3VPN_VRF_OPER_UP    1
#define L3VPN_VRF_OPER_DOWN  2

#define L3VPN_ROUTE_ADDED    1
#define L3VPN_ROUTE_DELETED  2
#define L3VPN_ROUTE_DROPPED  3 

#define L3VPN_MIN_CIDRTYPE   1
#define L3VPN_MAX_CIDRTYPE   5

#define L3VPN_MIN_METRIC    (-1)
#define L3VPN_MAX_METRIC    0x7FFFFFFF

#define L3VPN_P_PERF_ROUTES_ADDED(x)      x->MibObject.u4MplsL3VpnVrfPerfRoutesAdded
#define L3VPN_P_PERF_CURR_ROUTES(x)       x->MibObject.u4MplsL3VpnVrfPerfCurrNumRoutes
#define L3VPN_P_PERF_DELETED_ROUTES(x)    x->MibObject.u4MplsL3VpnVrfPerfRoutesDeleted
#define L3VPN_P_PERF_ROUTES_DROPPED(x)    x->MibObject.u4MplsL3VpnVrfPerfRoutesDropped
#define L3VPN_P_PERF_DISCONT_TIME(x)      x->MibObject.u4MplsL3VpnVrfPerfDiscTime   


#define L3VPN_VRF_TABLE                 gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable
#define L3VPN_RT_TABLE                  gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable
#define L3VPN_RTE_TABLE                 gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable
#define L3VPN_PERF_TABLE                gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable
#define L3VPN_IF_CONF_TABLE             gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable
#define L3VPN_SECURITY_TABLE            gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable
#define L3VPN_BGP_ROUTE_LABEL_TABLE  gL3vpnGlobals.L3VpnBgpRouteLabelTable

#define L3VPN_CONNECTED_INTERFACES   gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnConnectedInterfaces
#define L3VPN_ACTIVE_VRFS     gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnActiveVrfs
#define L3VPN_CONFIGURED_VRFS    gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnConfiguredVrfs
#define L3VPN_MAX_POSSIBLE_RTS   gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnVrfConfMaxPossRts


#define L3VPN_P_BGPROUTELABELRTE_CXTID(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.u4ContextId
#define L3VPN_P_BGPROUTELABELRTE_NXTHOP(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.NextHop
#define L3VPN_P_BGPROUTELABELRTE_NXTHOPAFI(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.NextHop.u2Afi
#define L3VPN_P_BGPROUTELABELRTE_NXTHOPADDRLEN(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.NextHop.u2AddressLen
#define L3VPN_P_BGPROUTELABELRTE_NXTHOPADDR(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.NextHop.au1Address
#define L3VPN_P_BGPROUTELABELRTE_IF_IDX(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.u4IfIndex
#define L3VPN_P_BGPROUTELABELRTE_NAME(x)   x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.au1MplsL3VpnVrfName
#define L3VPN_P_BGPROUTELABELRTE_DSTMAC(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.au1DstMac

#define L3VPN_P_BGPROUTELABELVRF_VRFID(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.u4VrfId
#define L3VPN_P_BGPROUTELABELVRF_VNAME(x) x->L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.au1MplsL3VpnVrfName



#define L3VPN_RSVP_MAP_LABEL_TABLE  gL3vpnGlobals.L3VpnRsvpMapLabelTable
#define L3VPN_RSVPMAPLABEL_PREFIX(x) x.u4Prefix
#define L3VPN_RSVPMAPLABEL_MASK(x) x.u4Mask
#define L3VPN_RSVPMAPLABEL_PREFIX_TYPE(x) x.u1PrefixType
#define L3VPN_RSVPMAPLABEL_MASK_TYPE(x) x.u1MaskType
#define L3VPN_P_RSVPMAPLABEL_PREFIX(x) x->u4Prefix
#define L3VPN_P_RSVPMAPLABEL_MASK(x) x->u4Mask
#define L3VPN_P_RSVPMAPLABEL_PREFIX_TYPE(x) x->u1PrefixType
#define L3VPN_P_RSVPMAPLABEL_MASK_TYPE(x) x->u1MaskType


#define L3VPN_BGPROUTELABELRTE_CXTID(x)  x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.u4ContextId
#define L3VPN_BGPROUTELABELRTE_NXTHOP(x) x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.NextHop
#define L3VPN_BGPROUTELABELRTE_IF_IDX(x) x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.u4IfIndex
#define L3VPN_BGPROUTELABELRTE_NAME(x)   x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.au1MplsL3VpnVrfName
#define L3VPN_BGPROUTELABELRTE_DSTMAC(x) x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.au1DstMac

#define L3VPN_BGPROUTELABELVRF_VRFID(x)  x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.u4VrfId
#define L3VPN_BGPROUTELABELVRF_VNAME(x)  x.L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.au1MplsL3VpnVrfName




#define L3VPN_P_VRF_NAME(x)                   x->MibObject.au1MplsL3VpnVrfName
#define L3VPN_P_VRF_CREATION_TIME(x)          x->MibObject.u4MplsL3VpnVrfCreationTime
#define L3VPN_P_VRF_ACTIVE_INTERFACES(x)      x->MibObject.u4MplsL3VpnVrfActiveInterfaces
#define L3VPN_P_VRF_ASSOCIATED_INTERFACES(x)  x->MibObject.u4MplsL3VpnVrfAssociatedInterfaces
#define L3VPN_P_VRF_MID_ROUTE_TH(x)           x->MibObject.u4MplsL3VpnVrfConfMidRteThresh
#define L3VPN_P_VRF_HIGH_ROUTE_TH(x)          x->MibObject.u4MplsL3VpnVrfConfHighRteThresh
#define L3VPN_P_VRF_CONF_MAX_ROUTES(x)        x->MibObject.u4MplsL3VpnVrfConfMaxRoutes
#define L3VPN_P_VRF_CONF_LAST_CHANGED(x)      x->MibObject.u4MplsL3VpnVrfConfLastChanged
#define L3VPN_P_VRF_OPER_STATUS(x)            x->MibObject.i4MplsL3VpnVrfOperStatus
#define L3VPN_P_VRF_ROW_STATUS(x)             x->MibObject.i4MplsL3VpnVrfConfRowStatus
#define L3VPN_P_VRF_ADMIN_STATUS(x)           x->MibObject.i4MplsL3VpnVrfConfAdminStatus
#define L3VPN_P_VRF_STORAGE_TYPE(x)           x->MibObject.i4MplsL3VpnVrfConfStorageType
#define L3VPN_P_VRF_NAME_LEN(x)               x->MibObject.i4MplsL3VpnVrfNameLen
#define L3VPN_P_VRF_VPN_ID_LEN(x)             x->MibObject.i4MplsL3VpnVrfVpnIdLen
#define L3VPN_P_VRF_DESCRIPTION_LEN(x)        x->MibObject.i4MplsL3VpnVrfDescriptionLen
#define L3VPN_P_VRF_RD_LEN(x)                 x->MibObject.i4MplsL3VpnVrfRDLen
#define L3VPN_P_VRF_VPN_ID(x)                 x->MibObject.au1MplsL3VpnVrfVpnId
#define L3VPN_P_VRF_DESCRIPTION(x)            x->MibObject.au1MplsL3VpnVrfDescription
#define L3VPN_P_VRF_RD(x)                     x->MibObject.au1MplsL3VpnVrfRD
#define L3VPN_P_VRF_MID_THRESH_TRAP_SENT(x)   x->MibObject.bMidThreshTrapSent
/*#define L3VPN_P_VRF_HIGH_THRESH_TRAP_SENT(x)  x->MibObject.bHighThreshTrapSent */
#define L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT(x)     x->MibObject.u4HighThreshLastTrapSent
  

#define L3VPN_P_IF_CONF_IF_INDEX(x)            x->MibObject.i4MplsL3VpnIfConfIndex
#define L3VPN_P_IF_CONF_VPN_CLASSIFICATION(x)  x->MIbObject.i4MplsL3VpnIfVpnClassification
#define L3VPN_P_IF_CONF_STORAGE_TYPE(x)        x->MibObject.i4MplsL3VpnIfConfStorageType
#define L3VPN_P_IF_CONF_ROW_STATUS(x)          x->MibObject.i4MplsL3VpnIfConfRowStatus
#define L3VPN_P_IF_CONF_ROUTE_DIST_PROT_LEN(x) x->MibObject.i4MplsL3VpnIfVpnRouteDistProtocolLen
#define L3VPN_P_IF_CONF_ROUTE_DIST_PROT(x)     x->MibObject.au1MplsL3VpnIfVpnRouteDistProtocol


#define L3VPN_P_RT_INDEX(x)                   x->MibObject.u4MplsL3VpnVrfRTIndex
#define L3VPN_P_RT_TYPE(x)                    x->MibObject.i4MplsL3VpnVrfRTType
#define L3VPN_P_RT_ROW_STATUS(x)              x->MibObject.i4MplsL3VpnVrfRTRowStatus
#define L3VPN_P_STORAGE_TYPE(x)               x->MibObject.i4MplsL3VpnVrfRTStorageType
#define L3VPN_P_RT_LEN(x)                     x->MibObject.i4MplsL3VpnVrfRTLen
#define L3VPN_P_RT_DESC_LEN(x)                x->MibObject.i4MplsL3VpnVrfRTDescrLen
#define L3VPN_P_RT_VALUE(x)                   x->MibObject.au1MplsL3VpnVrfRT
#define L3VPN_P_RT_DESCRIPTION(x)             x->MibObject.au1MplsL3VpnVrfRTDescr


#define L3VPN_P_ILLEGAL_LBL_VLTNS(x)     x->MibObject.u4MplsL3VpnVrfSecIllegalLblVltns
#define L3VPN_P_DISCONTINUITY_TIME(x)    x->MibObject.u4MplsL3VpnVrfSecDiscontinuityTime



#define L3VPN_P_RTE_INET_CIDR_PREFIX_LEN(x)        x->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen
#define L3VPN_P_RTE_INET_CIDR_AGE(x)               x->MibObejct.u4MplsL3VpnVrfRteInetCidrAge
#define L3VPN_P_RTE_INET_CIDR_NH_AS(x)             x->MibObject.u4MplsL3VpnVrfRteInetCidrNextHopAS
#define L3VPN_P_RTE_INET_CIDR_POLICY(x)            x->MibObject.au4MplsL3VpnVrfRteInetCidrPolicy
#define L3VPN_P_RTE_INET_CIDR_DEST_TYPE(x)         x->MibObject.i4MplsL3VpnVrfRteInetCidrDestType
#define L3VPN_P_RTE_INET_CIDR_NH_TYPE(x)           x->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType
#define L3VPN_P_RTE_INET_CIDR_IF_INDEX(x)          x->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex
#define L3VPN_P_RTE_INET_CIDR_TYPE(x)              x->MibObject.i4MplsL3VpnVrfRteInetCidrType
#define L3VPN_P_RTE_INET_CIDR_PROTO(x)             x->MibObject.i4MplsL3VpnVrfRteInetCidrProto
#define L3VPN_P_RTE_INET_CIDR_METRIC1(x)           x->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1
#define L3VPN_P_RTE_INET_CIDR_METRIC2(x)           x->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2
#define L3VPN_P_RTE_INET_CIDR_METRIC3(x)           x->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3
#define L3VPN_P_RTE_INET_CIDR_METRIC4(x)           x->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4
#define L3VPN_P_RTE_INET_CIDR_METRIC5(x)           x->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5                                                   
#define L3VPN_P_RTE_INET_CIDR_STATUS(x)            x->MibObject.i4MplsL3VpnVrfRteInetCidrStatus
#define L3VPN_P_RTE_INET_CIDR_DEST_LEN(x)          x->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen
#define L3VPN_P_RTE_INET_CIDR_PLOICY_LEN(x)        x->MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen
#define L3VPN_P_RTE_INET_CIDR_NH_LEN(x)            x->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen
#define L3VPN_P_RTE_XC_POINTER_LEN(x)              x->MibObject.i4MplsL3VpnVrfRteXCPointerLen
#define L3VPN_P_RTE_INET_CIDR_DEST(x)              x->MibObject.au1MplsL3VpnVrfRteInetCidrDest
#define L3VPN_P_RTE_INET_CIDR_NH(x)                x->MibObject.au1MplsL3VpnVrfRteInetCidrNextHop
#define L3VPN_P_RTE_XC_POINTER(x)                  x->MibObject.au1MplsL3VpnVrfRteXCPointer




#define L3VPN_NOTIFY_ROUTE_PARAMS_CB        gL3vpnGlobals.L3VpnBgpRegEntry.pBgp4MplsL3VpnNotifyRouteParams
#define L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB   gL3vpnGlobals.L3VpnBgpRegEntry.pBgp4MplsL3VpnNotifyLspStatusChange
#define L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB   gL3vpnGlobals.L3VpnBgpRegEntry.pBgp4MplsL3vpnNotifyVrfAdminStatusChange



/* MACROS for Mib objects for acces via structures */

#define L3VPN_VRF_NAME(x)                   x.MibObject.au1MplsL3VpnVrfName
#define L3VPN_VRF_CREATION_TIME(x)          x.MibObject.u4MplsL3VpnVrfCreationTime
#define L3VPN_VRF_ACTIVE_INTERFACES(x)      x.MibObject.u4MplsL3VpnVrfActiveInterfaces
#define L3VPN_VRF_ASSOCIATED_INTERFACES(x)  x.MibObject.u4MplsL3VpnVrfAssociatedInterfaces
#define L3VPN_VRF_MID_ROUTE_TH(x)           x.MibObject.u4MplsL3VpnVrfConfMidRteThresh
#define L3VPN_VRF_HIGH_ROUTE_TH(x)          x.MibObject.u4MplsL3VpnVrfConfHighRteThresh
#define L3VPN_VRF_CONF_MAX_ROUTES(x)        x.MibObject.u4MplsL3VpnVrfConfMaxRoutes
#define L3VPN_VRF_CONF_LAST_CHANGED(x)      x.MibObject.u4MplsL3VpnVrfConfLastChanged
#define L3VPN_VRF_OPER_STATUS(x)            x.MibObject.i4MplsL3VpnVrfOperStatus
#define L3VPN_VRF_ROW_STATUS(x)             x.MibObject.i4MplsL3VpnVrfConfRowStatus
#define L3VPN_VRF_ADMIN_STATUS(x)           x.MibObject.i4MplsL3VpnVrfConfAdminStatus
#define L3VPN_VRF_STORAGE_TYPE(x)           x.MibObject.i4MplsL3VpnVrfConfStorageType
#define L3VPN_VRF_NAME_LEN(x)               x.MibObject.i4MplsL3VpnVrfNameLen
#define L3VPN_VRF_VPN_ID_LEN(x)             x.MibObject.i4MplsL3VpnVrfVpnIdLen
#define L3VPN_VRF_DESCRIPTION_LEN(x)        x.MibObject.i4MplsL3VpnVrfDescriptionLen
#define L3VPN_VRF_RD_LEN(x)                 x.MibObject.i4MplsL3VpnVrfRDLen
#define L3VPN_VRF_VPN_ID(x)                 x.MibObject.au1MplsL3VpnVrfVpnId
#define L3VPN_VRF_DESCRIPTION(x)            x.MibObject.au1MplsL3VpnVrfDescription
#define L3VPN_VRF_RD(x)                     x.MibObject.au1MplsL3VpnVrfRD


#define L3VPN_IF_CONF_IF_INDEX(x)            x.MibObject.i4MplsL3VpnIfConfIndex
#define L3VPN_IF_CONF_VPN_CLASSIFICATION(x)  x.MIbObject.i4MplsL3VpnIfVpnClassification
#define L3VPN_IF_CONF_STORAGE_TYPE(x)        x.MibObject.i4MplsL3VpnIfConfStorageType
#define L3VPN_IF_CONF_ROW_STATUS(x)          x.MibObject.i4MplsL3VpnIfConfRowStatus
#define L3VPN_IF_CONF_ROUTE_DIST_PROT_LEN(x) x.MibObject.i4MplsL3VpnIfVpnRouteDistProtocolLen
#define L3VPN_IF_CONF_ROUTE_DIST_PROT(x)     x.MibObject.au1MplsL3VpnIfVpnRouteDistProtocol


#define L3VPN_RT_INDEX(x)                   x.MibObject.u4MplsL3VpnVrfRTIndex
#define L3VPN_RT_TYPE(x)                    x.MibObject.i4MplsL3VpnVrfRTType
#define L3VPN_RT_ROW_STATUS(x)              x.MibObject.i4MplsL3VpnVrfRTRowStatus
#define L3VPN_STORAGE_TYPE(x)               x.MibObject.i4MplsL3VpnVrfRTStorageType
#define L3VPN_RT_LEN(x)                     x.MibObject.i4MplsL3VpnVrfRTLen
#define L3VPN_RT_DESC_LEN(x)                x.MibObject.i4MplsL3VpnVrfRTDescrLen
#define L3VPN_RT_VALUE(x)                   x.MibObject.au1MplsL3VpnVrfRT
#define L3VPN_RT_DESCRIPTION(x)             x.MibObject.au1MplsL3VpnVrfRTDescr


#define L3VPN_ILLEGAL_LBL_VLTNS(x)     x.MibObject.u4MplsL3VpnVrfSecIllegalLblVltns
#define L3VPN_DISCONTINUITY_TIME(x)    x.MibObject.u4MplsL3VpnVrfSecDiscontinuityTime



#define L3VPN_RTE_INET_CIDR_PREFIX_LEN(x)        x.MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen
#define L3VPN_RTE_INET_CIDR_AGE(x)               x.MibObejct.u4MplsL3VpnVrfRteInetCidrAge
#define L3VPN_RTE_INET_CIDR_NH_AS(x)             x.MibObject.u4MplsL3VpnVrfRteInetCidrNextHopAS
#define L3VPN_RTE_INET_CIDR_POLICY(x)            x.MibObject.au4MplsL3VpnVrfRteInetCidrPolicy
#define L3VPN_RTE_INET_CIDR_DEST_TYPE(x)         x.MibObject.i4MplsL3VpnVrfRteInetCidrDestType
#define L3VPN_RTE_INET_CIDR_NH_TYPE(x)           x.MibObject.i4MplsL3VpnVrfRteInetCidrNHopType
#define L3VPN_RTE_INET_CIDR_IF_INDEX(x)          x.MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex
#define L3VPN_RTE_INET_CIDR_TYPE(x)              x.MibObject.i4MplsL3VpnVrfRteInetCidrType
#define L3VPN_RTE_INET_CIDR_PROTO(x)             x.MibObject.i4MplsL3VpnVrfRteInetCidrProto
#define L3VPN_RTE_INET_CIDR_METRIC1(x)           x.MibObject.i4MplsL3VpnVrfRteInetCidrMetric1
#define L3VPN_RTE_INET_CIDR_METRIC2(x)           x.MibObject.i4MplsL3VpnVrfRteInetCidrMetric2
#define L3VPN_RTE_INET_CIDR_METRIC3(x)           x.MibObject.i4MplsL3VpnVrfRteInetCidrMetric3
#define L3VPN_RTE_INET_CIDR_METRIC4(x)           x.MibObject.i4MplsL3VpnVrfRteInetCidrMetric4
#define L3VPN_RTE_INET_CIDR_METRIC5(x)           x.MibObject.i4MplsL3VpnVrfRteInetCidrMetric5
#define L3VPN_RTE_INET_CIDR_STATUS(x)            x.MibObject.i4MplsL3VpnVrfRteInetCidrStatus
#define L3VPN_RTE_INET_CIDR_DEST_LEN(x)          x.MibObject.i4MplsL3VpnVrfRteInetCidrDestLen
#define L3VPN_RTE_INET_CIDR_PLOICY_LEN(x)        x.MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen
#define L3VPN_RTE_INET_CIDR_NH_LEN(x)            x.MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen
#define L3VPN_RTE_XC_POINTER_LEN(x)              x.MibObject.i4MplsL3VpnVrfRteXCPointerLen
#define L3VPN_RTE_INET_CIDR_DEST(x)              x.MibObject.au1MplsL3VpnVrfRteInetCidrDest
#define L3VPN_RTE_INET_CIDR_NH(x)                x.MibObject.au1MplsL3VpnVrfRteInetCidrNextHop
#define L3VPN_RTE_XC_POINTER(x)                  x.MibObject.au1MplsL3VpnVrfRteXCPointer


#define L3VPN_PERF_ROUTES_ADDED(x)      x.MibObject.u4MplsL3VpnVrfPerfRoutesAdded
#define L3VPN_PERF_CURR_ROUTES(x)       x.MibObject.u4MplsL3VpnVrfPerfCurrNumRoutes
#define L3VPN_PERF_DELETED_ROUTES(x)    x.MibObject.u4MplsL3VpnVrfPerfRoutesDeleted
#define L3VPN_PERF_ROUTES_DROPPED(x)    x.MibObject.u4MplsL3VpnVrfPerfRoutesDropped


/* MACROS for SCALARS */

#define L3VPN_CONNECTED_INTERFACES   gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnConnectedInterfaces
#define L3VPN_ACTIVE_VRFS            gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnActiveVrfs
#define L3VPN_CONFIGURED_VRFS        gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnConfiguredVrfs
#define L3VPN_NOTIFICATION_ENABLE    gL3vpnGlobals.L3vpnGlbMib.i4MplsL3VpnNotificationEnable
#define L3VPN_RTE_MAX_THRSH_TIME     gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnVrfConfRteMxThrshTime


#define  L3VPN_MILLIS_IN_A_SEC             1000
#define  L3VPN_ZERO                        0
#define  L3VPN_ONE                         1
/* Return Values */
#define L3VPN_TRUE                         1
#define L3VPN_FALSE                        0
#define L3VPN_EQUAL                        0
#define L3VPN_NOT_EQUAL                    (~0)
#define L3VPN_SMALLER                      1
#define L3VPN_GREATER                      2

#define L3VPN_UTIL_UINT1_MAX                    0xFF
#define L3VPN_UTIL_UINT2_MAX                    0xFFFF
#define L3VPN_UTIL_UINT4_MAX                    0xFFFFFFFF


#define L3VPN_INTERFACE_MAPPED     1
#define L3VPN_INTERFACE_UNMAPPED   2


/*** mplsL3VpnVrfEntry mib Related Macros **/
#define L3VPN_MAX_POSSIBLE_ROUTES 100000
#define L3VPN_VRF_COUNT_MAX  SYS_DEF_MAX_NUM_CONTEXTS
#define L3VPN_PER_VRF_MAX_ROUTES        L3VPN_MAX_POSSIBLE_ROUTES/L3VPN_VRF_COUNT_MAX
#define L3VPN_DEFAULT_MAX_TH            0
#define L3VPN_DEFAULT_MID_TH            0


#define L3VPN_VRF_ADMIN_STATUS_UP  1
#define L3VPN_VRF_ADMIN_STATUS_DOWN 2
#define L3VPN_VRF_ADMIN_STATUS_TESTING 3

#define L3VPN_VRF_CONF_MID_RTE_THRESH_MAX 100
#define L3VPN_VRF_CONF_HIGH_RTE_THRESH_MAX 100

#define L3VPN_ILL_LBL_RCV_THRSH_MAX             1000


/*** Sec Mib Table update Macros ***/
#define L3VPN_VRF_SEC_ILLEGAL_LBL_VL_OP 1
#define L3VPN_VRF_SEC_DIS_CONT_TIME_OP  2


#define L3VPN_XC_ENTRY_TO_OCTETSTRING(u4XcIndex,u4InSegIndex, u4OutSegIndex, pOctetString) {\
    UINT4 u4LocalXcIndex = OSIX_HTONL(u4XcIndex);\
    UINT4 u4LocalInSegIndex = OSIX_HTONL(u4InSegIndex);\
    UINT4 u4LocalOutSegIndex = OSIX_HTONL(u4OutSegIndex);\
    UINT1 u1Len = sizeof(UINT4);\
    MEMCPY(pOctetString->pu1_OctetList,&u1Len, sizeof(UINT1));\
    MEMCPY(pOctetString->pu1_OctetList, (UINT1 *)&u4LocalXcIndex, sizeof(UINT4));\
    pOctetString->pu1_OctetList = pOctetString->pu1_OctetList + sizeof(UINT4) + sizeof(UINT1);\
    \
    MEMCPY(pOctetString->pu1_OctetList, &u1Len, sizeof(UINT1));\
    MEMCPY(pOctetString->pu1_OctetList, (UINT1 *)&u4LocalInSegIndex, sizeof(UINT4));\
    pOctetString->pu1_OctetList = pOctetString->pu1_OctetList + sizeof(UINT4) + sizeof(UINT1);\
    \
    MEMCPY(pOctetString->pu1_OctetList, &u1Len, sizeof(UINT1));\
    MEMCPY(pOctetString->pu1_OctetList, (UINT1 *)&u4LocalOutSegIndex, sizeof(UINT4));\
    pOctetString->i4_Length=3*(sizeof(UINT4) + sizeof(UINT1));};

#define L3VPN_OCTET_STRING_TO_XC_INDEX(pOctetString, u4XcIndex, u4InSegmentIndex, u4OutSegmentIndex){\
    /* 1st Byte is the length of the u4XcIndex, which is always 4 */ \
   pOctetString->pu1_OctetList++;\
   MEMCPY(&u4XcIndex, pOctetString->pu1_OctetList, sizeof(UINT4));\
   pOctetString->pu1_OctetList = pOctetString->pu1_OctetList + sizeof(UINT4);\
   \
   /* 6th Byte will be the length of the InSeg Index which is always 4 bytes */\
   pOctetString->pu1_OctetList++;\
   MEMCPY(&u4InSegmentIndex, pOctetString->pu1_OctetList, sizeof(UINT4));\
   pOctetString->pu1_OctetList = pOctetString->pu1_OctetList + sizeof(UINT4);\
   \
   /* 11th Bytes will be the length of outsegment index */\
   pOctetString->pu1_OctetList++;\
   MEMCPY(&u4OutSegmentIndex, pOctetString->pu1_OctetList, sizeof(UINT4));};


#define L3VPN_DEF_ROUTE_METRIC      -1
#define L3VPN_DEF_PORT_IFINDEX      0
#define L3VPN_DEF_NEXTHOP_AS  0

enum 
{
   L3VPN_RD_TYPE_0 = 0,
   L3VPN_RD_TYPE_1,
   L3VPN_RD_TYPE_2
};

enum 
{
   L3VPN_RT_TYPE_0 = 0,
   L3VPN_RT_TYPE_1,
   L3VPN_RT_TYPE_2
};

#define UINT1_MAX_VALUE      0xFF
#define UINT2_MAX_VALUE      0xFFFF

#define L3VPN_HOST_MASK        0xffffffff
#define L3VPN_MAX_PREFIX_LEN   32

/** vishal_1 : need to check if existing MACRO exists*/
#define MAX_MAC_LENGTH 25

#define L3VPN_BGP_ROUTE_PARAMS           1
#define L3VPN_BGP_LSP_STATUS_PARAMS      2
#define L3VPN_BGP_VRF_STATUS_PARAMS      3

/* SNMP-Related */
#define L3VPN_TRAP_ENABLE              1
#define L3VPN_TRAP_DISABLE             2


#define L3VPN_VRF_UP_TRAP              1
#define L3VPN_VRF_DOWN_TRAP            2
#define L3VPN_IF_CONF_BASE_OID_LEN     14 
#define L3VPN_VRF_BASE_OID_LEN         14
#define L3VPN_VRF_PERF_BASE_OID_LEN    14

/* This length includes the base oid length(14) + the Vrf name len(1) + vrf-name + if-index(1) */
#define L3VPN_MAX_IF_CONF_OID_LEN     (L3VPN_IF_CONF_BASE_OID_LEN + L3VPN_MAX_VRF_NAME_LEN + 2)
#define L3VPN_MAX_VRF_OID_LEN         (L3VPN_VRF_BASE_OID_LEN + L3VPN_MAX_VRF_NAME_LEN + 1)
#define L3VPN_MAX_VRF_PERF_OID_LEN    (L3VPN_VRF_PERF_BASE_OID_LEN + L3VPN_MAX_VRF_NAME_LEN + 1)

#define L3VPN_VRF_MID_THRESHOLD_EXCEEDED   3
#define L3VPN_VRF_MAX_THRESHOLD_EXCEEDED   4
#define L3VPN_VRF_MAX_THRESHOLD_CLEARED    6


#endif  /* __L3VPNDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file l3vpndefn.h                      */
/*-----------------------------------------------------------------------*/

