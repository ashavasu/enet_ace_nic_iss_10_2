/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntdfsg.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpntdfsg.h 
*
* Description: This file contains data structures defined for L3vpn module.
*********************************************************************/
/* Structure used by CLI to indicate which 
	all objects to be set in MplsL3VpnVrfEntry */
#include "mpls.h"
#include "l3vpndefn.h"

typedef struct
{
	BOOL1  bMplsL3VpnVrfName;
	BOOL1  bMplsL3VpnVrfVpnId;
	BOOL1  bMplsL3VpnVrfDescription;
	BOOL1  bMplsL3VpnVrfRD;
	BOOL1  bMplsL3VpnVrfConfMidRteThresh;
	BOOL1  bMplsL3VpnVrfConfHighRteThresh;
	BOOL1  bMplsL3VpnVrfConfMaxRoutes;
	BOOL1  bMplsL3VpnVrfConfRowStatus;
	BOOL1  bMplsL3VpnVrfConfAdminStatus;
	BOOL1  bMplsL3VpnVrfConfStorageType;
} tL3vpnIsSetMplsL3VpnVrfEntry;


/* Structure used by CLI to indicate which 
	all objects to be set in MplsL3VpnIfConfEntry */

typedef struct
{
	BOOL1  bMplsL3VpnIfConfIndex;
	BOOL1  bMplsL3VpnIfVpnClassification;
	BOOL1  bMplsL3VpnIfVpnRouteDistProtocol;
	BOOL1  bMplsL3VpnIfConfStorageType;
	BOOL1  bMplsL3VpnIfConfRowStatus;
	BOOL1  bMplsL3VpnVrfName;
} tL3vpnIsSetMplsL3VpnIfConfEntry;


/* Structure used by CLI to indicate which 
	all objects to be set in MplsL3VpnVrfRTEntry */

typedef struct
{
	BOOL1  bMplsL3VpnVrfRTIndex;
	BOOL1  bMplsL3VpnVrfRTType;
	BOOL1  bMplsL3VpnVrfRT;
	BOOL1  bMplsL3VpnVrfRTDescr;
	BOOL1  bMplsL3VpnVrfRTRowStatus;
	BOOL1  bMplsL3VpnVrfRTStorageType;
	BOOL1  bMplsL3VpnVrfName;
} tL3vpnIsSetMplsL3VpnVrfRTEntry;


/* Structure used by CLI to indicate which 
	all objects to be set in MplsL3VpnVrfRteEntry */

typedef struct
{
	BOOL1  bMplsL3VpnVrfRteInetCidrDestType;
	BOOL1  bMplsL3VpnVrfRteInetCidrDest;
	BOOL1  bMplsL3VpnVrfRteInetCidrPfxLen;
	BOOL1  bMplsL3VpnVrfRteInetCidrPolicy;
	BOOL1  bMplsL3VpnVrfRteInetCidrNHopType;
	BOOL1  bMplsL3VpnVrfRteInetCidrNextHop;
	BOOL1  bMplsL3VpnVrfRteInetCidrIfIndex;
	BOOL1  bMplsL3VpnVrfRteInetCidrType;
	BOOL1  bMplsL3VpnVrfRteInetCidrNextHopAS;
	BOOL1  bMplsL3VpnVrfRteInetCidrMetric1;
	BOOL1  bMplsL3VpnVrfRteInetCidrMetric2;
	BOOL1  bMplsL3VpnVrfRteInetCidrMetric3;
	BOOL1  bMplsL3VpnVrfRteInetCidrMetric4;
	BOOL1  bMplsL3VpnVrfRteInetCidrMetric5;
	BOOL1  bMplsL3VpnVrfRteXCPointer;
	BOOL1  bMplsL3VpnVrfRteInetCidrStatus;
	BOOL1  bMplsL3VpnVrfName;
} tL3vpnIsSetMplsL3VpnVrfRteEntry;



/* Structure used by L3vpn protocol for MplsL3VpnVrfEntry */

typedef struct
{
	tRBNodeEmbd  MplsL3VpnVrfTableNode;
	UINT4 u4MplsL3VpnVrfCreationTime;
	UINT4 u4MplsL3VpnVrfActiveInterfaces;
	UINT4 u4MplsL3VpnVrfAssociatedInterfaces;
	UINT4 u4MplsL3VpnVrfConfMidRteThresh;
	UINT4 u4MplsL3VpnVrfConfHighRteThresh;
	UINT4 u4MplsL3VpnVrfConfMaxRoutes;
	UINT4 u4MplsL3VpnVrfConfLastChanged;
	INT4 i4MplsL3VpnVrfOperStatus;
	INT4 i4MplsL3VpnVrfConfRowStatus;
	INT4 i4MplsL3VpnVrfConfAdminStatus;
	INT4 i4MplsL3VpnVrfConfStorageType;
	INT4 i4MplsL3VpnVrfNameLen;
	INT4 i4MplsL3VpnVrfVpnIdLen;
	INT4 i4MplsL3VpnVrfDescriptionLen;
	INT4 i4MplsL3VpnVrfRDLen;
	UINT1 au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
	UINT1 au1MplsL3VpnVrfVpnId[256];
	UINT1 au1MplsL3VpnVrfDescription[L3VPN_MAX_DESC_LEN];
	UINT1 au1MplsL3VpnVrfRD[MPLS_MAX_L3VPN_RD_LEN];

        /* adding more attributes */
        UINT4 u4ContextId;
        /*UINT1 bHighThreshTrapSent;*/  /* This avoid repeated traps for high threshold */
        UINT4  u4HighThreshLastTrapSent; /* Timestamp for last trap sent for high threshold */
        UINT1 bMidThreshTrapSent;   /* This avoid repeated traps for mid threshold*/
        UINT1  au1Padding[3];
} tL3vpnMibMplsL3VpnVrfEntry;

/* Structure used by L3vpn protocol for MplsL3VpnIfConfEntry */

typedef struct
{
	tRBNodeEmbd  MplsL3VpnIfConfTableNode;
	INT4 i4MplsL3VpnIfConfIndex;
	INT4 i4MplsL3VpnIfVpnClassification;
	INT4 i4MplsL3VpnIfConfStorageType;
	INT4 i4MplsL3VpnIfConfRowStatus;
	INT4 i4MplsL3VpnIfVpnRouteDistProtocolLen;
	INT4 i4MplsL3VpnVrfNameLen;
	UINT1 au1MplsL3VpnIfVpnRouteDistProtocol[256];
	UINT1 au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
} tL3vpnMibMplsL3VpnIfConfEntry;

/* Structure used by L3vpn protocol for MplsL3VpnVrfRTEntry */

typedef struct
{
	tRBNodeEmbd  MplsL3VpnVrfRTTableNode;
	UINT4 u4MplsL3VpnVrfRTIndex;
	INT4 i4MplsL3VpnVrfRTType;
	INT4 i4MplsL3VpnVrfRTRowStatus;
	INT4 i4MplsL3VpnVrfRTStorageType;
	INT4 i4MplsL3VpnVrfRTLen;
	INT4 i4MplsL3VpnVrfRTDescrLen;
	INT4 i4MplsL3VpnVrfNameLen;
	UINT1 au1MplsL3VpnVrfRT[L3VPN_MAX_RT_LEN];
	UINT1 au1MplsL3VpnVrfRTDescr[L3VPN_MAX_DESC_LEN];
	UINT1 au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
} tL3vpnMibMplsL3VpnVrfRTEntry;

/* Structure used by L3vpn protocol for MplsL3VpnVrfSecEntry */

typedef struct
{
	tRBNodeEmbd  MplsL3VpnVrfSecTableNode;
	UINT4 u4MplsL3VpnVrfSecIllegalLblVltns;
	UINT4 u4MplsL3VpnVrfSecDiscontinuityTime;
	INT4 i4MplsL3VpnVrfNameLen;
	UINT1 au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
} tL3vpnMibMplsL3VpnVrfSecEntry;

/* Structure used by L3vpn protocol for MplsL3VpnVrfPerfEntry */

typedef struct
{
	tRBNodeEmbd  MplsL3VpnVrfPerfTableNode;
	UINT4 u4MplsL3VpnVrfPerfRoutesAdded;
	UINT4 u4MplsL3VpnVrfPerfRoutesDeleted;
	UINT4 u4MplsL3VpnVrfPerfCurrNumRoutes;
	UINT4 u4MplsL3VpnVrfPerfRoutesDropped;
	UINT4 u4MplsL3VpnVrfPerfDiscTime;
	INT4 i4MplsL3VpnVrfNameLen;
	UINT1 au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
} tL3vpnMibMplsL3VpnVrfPerfEntry;

/* Structure used by L3vpn protocol for MplsL3VpnVrfRteEntry */

typedef struct
{
	tRBNodeEmbd  MplsL3VpnVrfRteTableNode;
	UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen;
	UINT4 u4MplsL3VpnVrfRteInetCidrAge;
	UINT4 u4MplsL3VpnVrfRteInetCidrNextHopAS;
	UINT4 au4MplsL3VpnVrfRteInetCidrPolicy[256];
	INT4 i4MplsL3VpnVrfRteInetCidrDestType;
	INT4 i4MplsL3VpnVrfRteInetCidrNHopType;
	INT4 i4MplsL3VpnVrfRteInetCidrIfIndex;
	INT4 i4MplsL3VpnVrfRteInetCidrType;
	INT4 i4MplsL3VpnVrfRteInetCidrProto;
	INT4 i4MplsL3VpnVrfRteInetCidrMetric1;
	INT4 i4MplsL3VpnVrfRteInetCidrMetric2;
	INT4 i4MplsL3VpnVrfRteInetCidrMetric3;
	INT4 i4MplsL3VpnVrfRteInetCidrMetric4;
	INT4 i4MplsL3VpnVrfRteInetCidrMetric5;
	INT4 i4MplsL3VpnVrfRteInetCidrStatus;
	INT4 i4MplsL3VpnVrfRteInetCidrDestLen;
	INT4 i4MplsL3VpnVrfRteInetCidrPolicyLen;
	INT4 i4MplsL3VpnVrfRteInetCidrNextHopLen;
	INT4 i4MplsL3VpnVrfRteXCPointerLen;
	INT4 i4MplsL3VpnVrfNameLen;
	UINT1 au1MplsL3VpnVrfRteInetCidrDest[256];
	UINT1 au1MplsL3VpnVrfRteInetCidrNextHop[256];
        /*UINT4 u4XcIndex;*/
        /*UINT4 u4InSegmentIndex;*/
        /*UINT4 u4OutSegmentIndex;*/
	UINT1 au1MplsL3VpnVrfRteXCPointer[256];
	UINT1 au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
} tL3vpnMibMplsL3VpnVrfRteEntry;
