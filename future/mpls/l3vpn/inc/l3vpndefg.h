/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndefg.h,v 1.2 2018/01/03 11:31:20 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpndefg.h 
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define L3VPN_MPLSL3VPNVRFTABLE_POOLID   L3VPNMemPoolIds[MAX_L3VPN_VRF_TABLE_ENTRIES_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID   L3VPNMemPoolIds[MAX_L3VPN_MPLSL3VPNVRFTABLE_ISSET_SIZING_ID]


#define L3VPN_MPLSL3VPNIFCONFTABLE_POOLID   L3VPNMemPoolIds[MAX_L3VPN_IF_CONF_TABLE_ENTRIES_SIZING_ID]


#define L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID   L3VPNMemPoolIds[MAX_L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFRTTABLE_POOLID   L3VPNMemPoolIds[MAX_L3VPN_RT_TABLE_ENTRIES_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID   L3VPNMemPoolIds[MAX_L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFSECTABLE_POOLID   L3VPNMemPoolIds[MAX_L3VPN_SEC_TABLE_ENTRIES_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFSECTABLE_ISSET_POOLID   L3VPNMemPoolIds[MAX_L3VPN_MPLSL3VPNVRFSECTABLE_ISSET_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID   L3VPNMemPoolIds[MAX_L3VPN_PERF_TABLE_ENTRIES_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFPERFTABLE_ISSET_POOLID   L3VPNMemPoolIds[MAX_L3VPN_MPLSL3VPNVRFPERFTABLE_ISSET_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFRTETABLE_POOLID   L3VPNMemPoolIds[MAX_L3VPN_ROUTE_TABLE_ENTRIES_SIZING_ID]


#define L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID   L3VPNMemPoolIds[MAX_L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_SIZING_ID]
/* Macro used to fill the CLI structure for MplsL3VpnVrfEntry 
 using the input given in def file */


#define L3VPN_Q_MSG_POOL_ID              L3VPNMemPoolIds[MAX_L3VPN_Q_MSG_SIZING_ID]

#define L3VPN_BGPROUTELABELTABLE_POOLID  L3VPNMemPoolIds[MAX_L3VPN_EGRESS_ROUTE_TABLE_ENTRIES_SIZING_ID]

#define L3VPN_RSVPMAPLABELTABLE_POOLID  L3VPNMemPoolIds[MAX_L3VPN_RSVP_MAP_LABEL_TABLE_ENTRIES_SIZING_ID]

#define  L3VPN_OID_POOLID                L3VPNMemPoolIds[MAX_L3VPN_OID_SIZING_ID]

#define  L3VPN_FILL_MPLSL3VPNVRFTABLE_ARGS(pL3vpnMplsL3VpnVrfEntry,\
   pL3vpnIsSetMplsL3VpnVrfEntry,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnVrfNameLen,\
   pau1MplsL3VpnVrfVpnId,\
   pau1MplsL3VpnVrfVpnIdLen,\
   pau1MplsL3VpnVrfDescription,\
   pau1MplsL3VpnVrfDescriptionLen,\
   pau1MplsL3VpnVrfRD,\
   pau1MplsL3VpnVrfRDLen,\
   pau1MplsL3VpnVrfConfMidRteThresh,\
   pau1MplsL3VpnVrfConfHighRteThresh,\
   pau1MplsL3VpnVrfConfMaxRoutes,\
   pau1MplsL3VpnVrfConfRowStatus,\
   pau1MplsL3VpnVrfConfAdminStatus,\
   pau1MplsL3VpnVrfConfStorageType)\
  {\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfVpnId != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId, pau1MplsL3VpnVrfVpnId, *(INT4 *)pau1MplsL3VpnVrfVpnIdLen);\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen = *(INT4 *)pau1MplsL3VpnVrfVpnIdLen;\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfDescription != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription, pau1MplsL3VpnVrfDescription, *(INT4 *)pau1MplsL3VpnVrfDescriptionLen);\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen = *(INT4 *)pau1MplsL3VpnVrfDescriptionLen;\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRD != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD, pau1MplsL3VpnVrfRD, *(INT4 *)pau1MplsL3VpnVrfRDLen);\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen = *(INT4 *)pau1MplsL3VpnVrfRDLen;\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfConfMidRteThresh != NULL)\
  {\
   pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh = *(UINT4 *) (pau1MplsL3VpnVrfConfMidRteThresh);\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfConfHighRteThresh != NULL)\
  {\
   pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh = *(UINT4 *) (pau1MplsL3VpnVrfConfHighRteThresh);\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfConfMaxRoutes != NULL)\
  {\
   pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes = *(UINT4 *) (pau1MplsL3VpnVrfConfMaxRoutes);\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfConfRowStatus != NULL)\
  {\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus = *(INT4 *) (pau1MplsL3VpnVrfConfRowStatus);\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfConfAdminStatus != NULL)\
  {\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus = *(INT4 *) (pau1MplsL3VpnVrfConfAdminStatus);\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfConfStorageType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType = *(INT4 *) (pau1MplsL3VpnVrfConfStorageType);\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for MplsL3VpnIfConfEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNIFCONFTABLE_ARGS(pL3vpnMplsL3VpnIfConfEntry,\
   pL3vpnIsSetMplsL3VpnIfConfEntry,\
   pau1MplsL3VpnIfConfIndex,\
   pau1MplsL3VpnIfVpnClassification,\
   pau1MplsL3VpnIfVpnRouteDistProtocol,\
   pau1MplsL3VpnIfVpnRouteDistProtocolLen,\
   pau1MplsL3VpnIfConfStorageType,\
   pau1MplsL3VpnIfConfRowStatus,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnVrfNameLen)\
  {\
  if (pau1MplsL3VpnIfConfIndex != NULL)\
  {\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex = *(INT4 *) (pau1MplsL3VpnIfConfIndex);\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnIfVpnClassification != NULL)\
  {\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification = *(INT4 *) (pau1MplsL3VpnIfVpnClassification);\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnIfVpnRouteDistProtocol != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnIfVpnRouteDistProtocol, pau1MplsL3VpnIfVpnRouteDistProtocol, *(INT4 *)pau1MplsL3VpnIfVpnRouteDistProtocolLen);\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnRouteDistProtocolLen = *(INT4 *)pau1MplsL3VpnIfVpnRouteDistProtocolLen;\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnIfConfStorageType != NULL)\
  {\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType = *(INT4 *) (pau1MplsL3VpnIfConfStorageType);\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnIfConfRowStatus != NULL)\
  {\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus = *(INT4 *) (pau1MplsL3VpnIfConfRowStatus);\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for MplsL3VpnVrfRTEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNVRFRTTABLE_ARGS(pL3vpnMplsL3VpnVrfRTEntry,\
   pL3vpnIsSetMplsL3VpnVrfRTEntry,\
   pau1MplsL3VpnVrfRTIndex,\
   pau1MplsL3VpnVrfRTType,\
   pau1MplsL3VpnVrfRT,\
   pau1MplsL3VpnVrfRTLen,\
   pau1MplsL3VpnVrfRTDescr,\
   pau1MplsL3VpnVrfRTDescrLen,\
   pau1MplsL3VpnVrfRTRowStatus,\
   pau1MplsL3VpnVrfRTStorageType,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnVrfNameLen)\
  {\
  if (pau1MplsL3VpnVrfRTIndex != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex = *(UINT4 *) (pau1MplsL3VpnVrfRTIndex);\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRTType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType = *(INT4 *) (pau1MplsL3VpnVrfRTType);\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRT != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT, (UINT1 *)pau1MplsL3VpnVrfRT, *(INT4 *)pau1MplsL3VpnVrfRTLen);\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen = *(INT4 *)pau1MplsL3VpnVrfRTLen;\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRTDescr != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr, pau1MplsL3VpnVrfRTDescr, *(INT4 *)pau1MplsL3VpnVrfRTDescrLen);\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen = *(INT4 *)pau1MplsL3VpnVrfRTDescrLen;\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRTRowStatus != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus = *(INT4 *) (pau1MplsL3VpnVrfRTRowStatus);\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRTStorageType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType = *(INT4 *) (pau1MplsL3VpnVrfRTStorageType);\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for MplsL3VpnVrfRteEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNVRFRTETABLE_ARGS(pL3vpnMplsL3VpnVrfRteEntry,\
   pL3vpnIsSetMplsL3VpnVrfRteEntry,\
   pau1MplsL3VpnVrfRteInetCidrDestType,\
   pau1MplsL3VpnVrfRteInetCidrDest,\
   pau1MplsL3VpnVrfRteInetCidrDestLen,\
   pau1MplsL3VpnVrfRteInetCidrPfxLen,\
   pau1MplsL3VpnVrfRteInetCidrPolicy,\
   pau1MplsL3VpnVrfRteInetCidrPolicyLen,\
   pau1MplsL3VpnVrfRteInetCidrNHopType,\
   pau1MplsL3VpnVrfRteInetCidrNextHop,\
   pau1MplsL3VpnVrfRteInetCidrNextHopLen,\
   pau1MplsL3VpnVrfRteInetCidrIfIndex,\
   pau1MplsL3VpnVrfRteInetCidrType,\
   pau1MplsL3VpnVrfRteInetCidrNextHopAS,\
   pau1MplsL3VpnVrfRteInetCidrMetric1,\
   pau1MplsL3VpnVrfRteInetCidrMetric2,\
   pau1MplsL3VpnVrfRteInetCidrMetric3,\
   pau1MplsL3VpnVrfRteInetCidrMetric4,\
   pau1MplsL3VpnVrfRteInetCidrMetric5,\
   pau1MplsL3VpnVrfRteXCPointer,\
   pau1MplsL3VpnVrfRteXCPointerLen,\
   pau1MplsL3VpnVrfRteInetCidrStatus,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnVrfNameLen)\
  {\
  if (pau1MplsL3VpnVrfRteInetCidrDestType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrDestType);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrDest != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteInetCidrDest, pau1MplsL3VpnVrfRteInetCidrDest, *(INT4 *)pau1MplsL3VpnVrfRteInetCidrDestLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen = *(INT4 *)pau1MplsL3VpnVrfRteInetCidrDestLen;\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrPfxLen != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen = *(UINT4 *) (pau1MplsL3VpnVrfRteInetCidrPfxLen);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrPolicy != NULL)\
  {\
   L3vpnUtlConvertStringtoOid (pL3vpnMplsL3VpnVrfRteEntry,\
       (UINT4 *)pau1MplsL3VpnVrfRteInetCidrPolicy,(INT4 *)pau1MplsL3VpnVrfRteInetCidrPolicyLen);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrNHopType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrNHopType);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrNextHop != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteInetCidrNextHop, pau1MplsL3VpnVrfRteInetCidrNextHop, *(INT4 *)pau1MplsL3VpnVrfRteInetCidrNextHopLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen = *(INT4 *)pau1MplsL3VpnVrfRteInetCidrNextHopLen;\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrIfIndex != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrIfIndex);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrType);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrNextHopAS != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrNextHopAS = *(UINT4 *) (pau1MplsL3VpnVrfRteInetCidrNextHopAS);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrMetric1 != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1 = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrMetric1);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrMetric2 != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2 = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrMetric2);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrMetric3 != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3 = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrMetric3);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrMetric4 != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4 = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrMetric4);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrMetric5 != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5 = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrMetric5);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteXCPointer != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer, pau1MplsL3VpnVrfRteXCPointer, *(INT4 *)pau1MplsL3VpnVrfRteXCPointerLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen = *(INT4 *)pau1MplsL3VpnVrfRteXCPointerLen;\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrStatus != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrStatus);\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus = OSIX_FALSE;\
  }\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;\
  }\
  else\
  {\
   pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for MplsL3VpnVrfEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNVRFTABLE_INDEX(pL3vpnMplsL3VpnVrfEntry,\
   pau1MplsL3VpnVrfName)\
  {\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
  }\
 }
/* Macro used to fill the index values in  structure for MplsL3VpnIfConfEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNIFCONFTABLE_INDEX(pL3vpnMplsL3VpnIfConfEntry,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnIfConfIndex)\
  {\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
  }\
  if (pau1MplsL3VpnIfConfIndex != NULL)\
  {\
   pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex = *(INT4 *) (pau1MplsL3VpnIfConfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for MplsL3VpnVrfRTEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNVRFRTTABLE_INDEX(pL3vpnMplsL3VpnVrfRTEntry,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnVrfRTIndex,\
   pau1MplsL3VpnVrfRTType)\
  {\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
  }\
  if (pau1MplsL3VpnVrfRTIndex != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex = *(UINT4 *) (pau1MplsL3VpnVrfRTIndex);\
  }\
  if (pau1MplsL3VpnVrfRTType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType = *(INT4 *) (pau1MplsL3VpnVrfRTType);\
  }\
 }
/* Macro used to fill the index values in  structure for MplsL3VpnVrfRteEntry 
 using the input given in def file */

#define  L3VPN_FILL_MPLSL3VPNVRFRTETABLE_INDEX(pL3vpnMplsL3VpnVrfRteEntry,\
   pau1MplsL3VpnVrfName,\
   pau1MplsL3VpnVrfRteInetCidrDestType,\
   pau1MplsL3VpnVrfRteInetCidrDest,\
   pau1MplsL3VpnVrfRteInetCidrPfxLen,\
   pau1MplsL3VpnVrfRteInetCidrPolicy,\
   pau1MplsL3VpnVrfRteInetCidrNHopType,\
   pau1MplsL3VpnVrfRteInetCidrNextHop)\
  {\
  if (pau1MplsL3VpnVrfName != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName, pau1MplsL3VpnVrfName, *(INT4 *)pau1MplsL3VpnVrfNameLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen = *(INT4 *)pau1MplsL3VpnVrfNameLen;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrDestType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrDestType);\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrDest != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteInetCidrDest, pau1MplsL3VpnVrfRteInetCidrDest, *(INT4 *)pau1MplsL3VpnVrfRteInetCidrDestLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen = *(INT4 *)pau1MplsL3VpnVrfRteInetCidrDestLen;\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrPfxLen != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen = *(UINT4 *) (pau1MplsL3VpnVrfRteInetCidrPfxLen);\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrPolicy != NULL)\
  {\
   L3vpnUtlConvertStringtoOid (pL3vpnMplsL3VpnVrfRteEntry,\
       (UINT4 *)pau1MplsL3VpnVrfRteInetCidrPolicy,(INT4 *)pau1MplsL3VpnVrfRteInetCidrPolicyLen);\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrNHopType != NULL)\
  {\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType = *(INT4 *) (pau1MplsL3VpnVrfRteInetCidrNHopType);\
  }\
  if (pau1MplsL3VpnVrfRteInetCidrNextHop != NULL)\
  {\
   MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteInetCidrNextHop, pau1MplsL3VpnVrfRteInetCidrNextHop, *(INT4 *)pau1MplsL3VpnVrfRteInetCidrNextHopLen);\
   pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen = *(INT4 *)pau1MplsL3VpnVrfRteInetCidrNextHopLen;\
  }\
 }






/* Macro used to convert the input 
  to required datatype for MplsL3VpnNotificationEnable*/

#define  L3VPN_FILL_MPLSL3VPNNOTIFICATIONENABLE(i4MplsL3VpnNotificationEnable,\
   pau1MplsL3VpnNotificationEnable)\
  {\
  if (pau1MplsL3VpnNotificationEnable != NULL)\
  {\
   i4MplsL3VpnNotificationEnable = *(INT4 *) (pau1MplsL3VpnNotificationEnable);\
  }\
  }


/* Macro used to convert the input
         to required datatype for MplsL3VpnVrfConfRteMxThrshTime*/

#define  L3VPN_FILL_MPLSL3VPNVRFCONFRTEMXTHRSHTIME(u4MplsL3VpnVrfConfRteMxThrshTime,\
                        pau1MplsL3VpnVrfConfRteMxThrshTime)\
         {\
                if (pau1MplsL3VpnVrfConfRteMxThrshTime != NULL)\
                {\
                        u4MplsL3VpnVrfConfRteMxThrshTime = *(UINT4 *) (pau1MplsL3VpnVrfConfRteMxThrshTime);\
                }\
         }


/* Macro used to convert the input 
  to required datatype for MplsL3VpnIllLblRcvThrsh*/

#define  L3VPN_FILL_MPLSL3VPNILLLBLRCVTHRSH(u4MplsL3VpnIllLblRcvThrsh,\
   pau1MplsL3VpnIllLblRcvThrsh)\
  {\
  if (pau1MplsL3VpnIllLblRcvThrsh != NULL)\
  {\
   u4MplsL3VpnIllLblRcvThrsh = *(UINT4 *) (pau1MplsL3VpnIllLblRcvThrsh);\
  }\
  }


