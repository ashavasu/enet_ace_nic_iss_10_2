/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnsrcdefn.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnsrcdefn.h 
*
* Description: This file contains the default values for L3vpn module.
*********************************************************************/


#ifndef __L3VPNSRCDEFN_H__
#define __L3VPNSRCDEFN_H__


#define L3VPN_SUCCESS        (OSIX_SUCCESS)
#define L3VPN_FAILURE        (OSIX_FAILURE)

/*********** macros for scalar objects*********************/

#define L3VPN_DEF_MPLSL3VPNCONFIGUREDVRFS             NULL
#define L3VPN_DEF_MPLSL3VPNACTIVEVRFS             NULL
#define L3VPN_DEF_MPLSL3VPNCONNECTEDINTERFACES             NULL
#define L3VPN_DEF_MPLSL3VPNNOTIFICATIONENABLE             2
#define L3VPN_DEF_MPLSL3VPNVRFCONFMAXPOSSRTS             NULL
#define L3VPN_DEF_MPLSL3VPNVRFCONFRTEMXTHRSHTIME             0
#define L3VPN_DEF_MPLSL3VPNILLLBLRCVTHRSH             NULL

/*********** macros for tabular objects*********************/

#define L3VPN_DEF_MPLSL3VPNVRFCONFMIDRTETHRESH             0
#define L3VPN_DEF_MPLSL3VPNVRFCONFHIGHRTETHRESH             0
#define L3VPN_DEF_MPLSL3VPNVRFCONFMAXROUTES             0
#define L3VPN_DEF_MPLSL3VPNVRFCONFSTORAGETYPE             2
#define L3VPN_DEF_MPLSL3VPNIFVPNCLASSIFICATION             2
#define L3VPN_DEF_MPLSL3VPNIFCONFSTORAGETYPE             2
#define L3VPN_DEF_MPLSL3VPNVRFRTSTORAGETYPE             2
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRIFINDEX             0
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRTYPE             1
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRNEXTHOPAS             0
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRMETRIC1             -1
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRMETRIC2             -1
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRMETRIC3             -1
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRMETRIC4             -1
#define L3VPN_DEF_MPLSL3VPNVRFRTEINETCIDRMETRIC5             -1

#endif  /* __L3VPNDEFN_H__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file l3vpndefn.h                      */
/*-----------------------------------------------------------------------*/

