
#ifndef _H_i_MPLS_L3VPN_STD_MIB
#define _H_i_MPLS_L3VPN_STD_MIB
/* $Id: l3vpnncg.h,v 1.1 2016/07/13 12:33:57 siva Exp $
    ISS Wrapper header
    module MPLS-L3VPN-STD-MIB

 */

/* $Id: l3vpnncg.h,v 1.1 2016/07/13 12:33:57 siva Exp $
    ISS Wrapper header
    module MPLS-L3VPN-STD-MIB

 */

/********************************************************************
* FUNCTION NcMplsL3VpnConfiguredVrfsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnConfiguredVrfsGet (
                UINT4 *pu4MplsL3VpnConfiguredVrfs );

/********************************************************************
* FUNCTION NcMplsL3VpnActiveVrfsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnActiveVrfsGet (
                UINT4 *pu4MplsL3VpnActiveVrfs );

/********************************************************************
* FUNCTION NcMplsL3VpnConnectedInterfacesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnConnectedInterfacesGet (
                UINT4 *pu4MplsL3VpnConnectedInterfaces );

/********************************************************************
* FUNCTION NcMplsL3VpnNotificationEnableSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnNotificationEnableSet (
                INT4 i4MplsL3VpnNotificationEnable );

/********************************************************************
* FUNCTION NcMplsL3VpnNotificationEnableTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnNotificationEnableTest (UINT4 *pu4Error,
                INT4 i4MplsL3VpnNotificationEnable );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfMaxPossRtsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfMaxPossRtsGet (
                UINT4 *pu4MplsL3VpnVrfConfMaxPossRts );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfRteMxThrshTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfRteMxThrshTimeGet (
                UINT4 *pu4MplsL3VpnVrfConfRteMxThrshTime );

/********************************************************************
* FUNCTION NcMplsL3VpnIllLblRcvThrshSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIllLblRcvThrshSet (
                UINT4 u4MplsL3VpnIllLblRcvThrsh );

/********************************************************************
* FUNCTION NcMplsL3VpnIllLblRcvThrshTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIllLblRcvThrshTest (UINT4 *pu4Error,
                UINT4 u4MplsL3VpnIllLblRcvThrsh );

/********************************************************************
* FUNCTION NcMplsL3VpnIfVpnClassificationSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIfVpnClassificationSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                INT4 i4MplsL3VpnIfVpnClassification );

/********************************************************************
* FUNCTION NcMplsL3VpnIfVpnClassificationTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIfVpnClassificationTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                INT4 i4MplsL3VpnIfVpnClassification );

/********************************************************************
* FUNCTION NcMplsL3VpnIfVpnRouteDistProtocolSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcMplsL3VpnIfVpnRouteDistProtocolSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                ncx_list_t ncx_list_tMplsL3VpnIfVpnRouteDistProtocol );*/

/********************************************************************
* FUNCTION NcMplsL3VpnIfVpnRouteDistProtocolTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcMplsL3VpnIfVpnRouteDistProtocolTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                ncx_list_t ncx_list_tMplsL3VpnIfVpnRouteDistProtocol );*/

/********************************************************************
* FUNCTION NcMplsL3VpnIfConfStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIfConfStorageTypeSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                INT4 i4MplsL3VpnIfConfStorageType );

/********************************************************************
* FUNCTION NcMplsL3VpnIfConfStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIfConfStorageTypeTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                INT4 i4MplsL3VpnIfConfStorageType );

/********************************************************************
* FUNCTION NcMplsL3VpnIfConfRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIfConfRowStatusSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                INT4 i4MplsL3VpnIfConfRowStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnIfConfRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnIfConfRowStatusTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnIfConfIndex,
                INT4 i4MplsL3VpnIfConfRowStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfVpnIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfVpnIdSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT1 *pau1MplsL3VpnVrfVpnId );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfVpnIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfVpnIdTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT1 *pau1MplsL3VpnVrfVpnId );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfDescriptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfDescriptionSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT1 *pMplsL3VpnVrfDescription );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfDescriptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfDescriptionTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT1 *pMplsL3VpnVrfDescription );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRDSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRDSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT1 *pau1MplsL3VpnVrfRD );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRDTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRDTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT1 *pau1MplsL3VpnVrfRD );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfCreationTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfCreationTimeGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfCreationTime );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfOperStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfOperStatusGet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 *pi4MplsL3VpnVrfOperStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfActiveInterfacesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfActiveInterfacesGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfActiveInterfaces );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfAssociatedInterfacesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfAssociatedInterfacesGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfAssociatedInterfaces );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfMidRteThreshSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfMidRteThreshSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfConfMidRteThresh );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfMidRteThreshTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfMidRteThreshTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfConfMidRteThresh );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfHighRteThreshSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfHighRteThreshSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfConfHighRteThresh );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfHighRteThreshTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfHighRteThreshTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfConfHighRteThresh );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfMaxRoutesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfMaxRoutesSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfConfMaxRoutes );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfMaxRoutesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfMaxRoutesTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfConfMaxRoutes );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfLastChangedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfLastChangedGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfConfLastChanged );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfRowStatusSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfConfRowStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfRowStatusTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfConfRowStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfAdminStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfAdminStatusSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfConfAdminStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfAdminStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfAdminStatusTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfConfAdminStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfStorageTypeSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfConfStorageType );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfConfStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfConfStorageTypeTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfConfStorageType );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfSecIllegalLblVltnsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfSecIllegalLblVltnsGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfSecIllegalLblVltns );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfSecDiscontinuityTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfSecDiscontinuityTimeGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfSecDiscontinuityTime );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfPerfRoutesAddedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfPerfRoutesAddedGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfPerfRoutesAdded );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfPerfRoutesDeletedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfPerfRoutesDeletedGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfPerfRoutesDeleted );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfPerfCurrNumRoutesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfPerfCurrNumRoutesGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfPerfCurrNumRoutes );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfPerfRoutesDroppedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfPerfRoutesDroppedGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfPerfRoutesDropped );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfPerfDiscTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfPerfDiscTimeGet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 *pu4MplsL3VpnVrfPerfDiscTime );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                UINT1 *pau1MplsL3VpnVrfRT );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                UINT1 *pau1MplsL3VpnVrfRT );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTDescrSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTDescrSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                UINT1 *pMplsL3VpnVrfRTDescr );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTDescrTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTDescrTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                UINT1 *pMplsL3VpnVrfRTDescr );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTRowStatusSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                INT4 i4MplsL3VpnVrfRTRowStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTRowStatusTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                INT4 i4MplsL3VpnVrfRTRowStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTStorageTypeSet (
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                INT4 i4MplsL3VpnVrfRTStorageType );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRTStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRTStorageTypeTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                UINT4 u4MplsL3VpnVrfRTIndex,
                INT4 i4MplsL3VpnVrfRTType,
                INT4 i4MplsL3VpnVrfRTStorageType );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrIfIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrIfIndexSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrIfIndex );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrIfIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrIfIndexTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrIfIndex );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrTypeSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrType );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrTypeTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrType );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrProtoGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrProtoGet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 *pi4MplsL3VpnVrfRteInetCidrProto );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrAgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrAgeGet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                UINT4 *pu4MplsL3VpnVrfRteInetCidrAge );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrNextHopASSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrNextHopASSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                UINT4 u4MplsL3VpnVrfRteInetCidrNextHopAS );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrNextHopASTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrNextHopASTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                UINT4 u4MplsL3VpnVrfRteInetCidrNextHopAS );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric1Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric1Set (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric1 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric1Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric1Test (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric1 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric2Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric2Set (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric2 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric2Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric2Test (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric2 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric3Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric3Set (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric3 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric3Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric3Test (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric3 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric4Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric4Set (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric4 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric4Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric4Test (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric4 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric5Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric5Set (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric5 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrMetric5Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrMetric5Test (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrMetric5 );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteXCPointerSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteXCPointerSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                UINT1 *pau1MplsL3VpnVrfRteXCPointer );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteXCPointerTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteXCPointerTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                UINT1 *pau1MplsL3VpnVrfRteXCPointer );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrStatusSet (
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrStatus );

/********************************************************************
* FUNCTION NcMplsL3VpnVrfRteInetCidrStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcMplsL3VpnVrfRteInetCidrStatusTest (UINT4 *pu4Error,
                UINT1 *au1MplsL3VpnVrfName,
                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
                INT4 i4MplsL3VpnVrfRteInetCidrStatus );

/* END i_MPLS_L3VPN_STD_MIB.c */


#endif
