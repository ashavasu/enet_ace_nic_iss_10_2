/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnprotg.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* Id: l3vpnprotg.h
*
* This file contains prototypes for functions defined in L3vpn.
*********************************************************************/


#include "cli.h"

INT4 L3vpnCliSetMplsL3VpnNotificationEnable (tCliHandle CliHandle,UINT4 *pMplsL3VpnNotificationEnable);

INT4 L3vpnCliSetMplsL3VpnVrfConfRteMxThrshTime (tCliHandle CliHandle,UINT4 *pMplsL3VpnVrfConfRteMxThrshTime);

INT4 L3vpnCliSetMplsL3VpnIllLblRcvThrsh (tCliHandle CliHandle,UINT4 *pMplsL3VpnIllLblRcvThrsh);


INT4 L3vpnCliSetMplsL3VpnVrfTable (tCliHandle CliHandle,tL3vpnMplsL3VpnVrfEntry * pL3vpnSetMplsL3VpnVrfEntry,tL3vpnIsSetMplsL3VpnVrfEntry * pL3vpnIsSetMplsL3VpnVrfEntry);

INT4 L3vpnCliSetMplsL3VpnIfConfTable (tCliHandle CliHandle,tL3vpnMplsL3VpnIfConfEntry * pL3vpnSetMplsL3VpnIfConfEntry,tL3vpnIsSetMplsL3VpnIfConfEntry * pL3vpnIsSetMplsL3VpnIfConfEntry);

INT4 L3vpnCliSetMplsL3VpnVrfRTTable (tCliHandle CliHandle,tL3vpnMplsL3VpnVrfRTEntry * pL3vpnSetMplsL3VpnVrfRTEntry,tL3vpnIsSetMplsL3VpnVrfRTEntry * pL3vpnIsSetMplsL3VpnVrfRTEntry);

INT4 L3vpnCliSetMplsL3VpnVrfRteTable (tCliHandle CliHandle,tL3vpnMplsL3VpnVrfRteEntry * pL3vpnSetMplsL3VpnVrfRteEntry,tL3vpnIsSetMplsL3VpnVrfRteEntry * pL3vpnIsSetMplsL3VpnVrfRteEntry);


PUBLIC INT4 L3vpnUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 L3vpnLock (VOID);

PUBLIC INT4 L3vpnUnLock (VOID);
INT4 L3vpnUtlConvertStringtoOid PROTO((tL3vpnMplsL3VpnVrfRteEntry *, UINT4 *, INT4 *));

PUBLIC INT4 MplsL3VpnVrfTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 L3vpnMplsL3VpnVrfTableCreate PROTO ((VOID));

tL3vpnMplsL3VpnVrfEntry * L3vpnMplsL3VpnVrfTableCreateApi PROTO ((tL3vpnMplsL3VpnVrfEntry *));

PUBLIC INT4 MplsL3VpnIfConfTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 L3vpnMplsL3VpnIfConfTableCreate PROTO ((VOID));

tL3vpnMplsL3VpnIfConfEntry * L3vpnMplsL3VpnIfConfTableCreateApi PROTO ((tL3vpnMplsL3VpnIfConfEntry *));

PUBLIC INT4 MplsL3VpnVrfRTTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 L3vpnMplsL3VpnVrfRTTableCreate PROTO ((VOID));

tL3vpnMplsL3VpnVrfRTEntry * L3vpnMplsL3VpnVrfRTTableCreateApi PROTO ((tL3vpnMplsL3VpnVrfRTEntry *));

PUBLIC INT4 MplsL3VpnVrfSecTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 L3vpnMplsL3VpnVrfSecTableCreate PROTO ((VOID));

tL3vpnMplsL3VpnVrfSecEntry * L3vpnMplsL3VpnVrfSecTableCreateApi PROTO ((tL3vpnMplsL3VpnVrfSecEntry *));

PUBLIC INT4 MplsL3VpnVrfPerfTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 L3vpnMplsL3VpnVrfPerfTableCreate PROTO ((VOID));

tL3vpnMplsL3VpnVrfPerfEntry * L3vpnMplsL3VpnVrfPerfTableCreateApi PROTO ((tL3vpnMplsL3VpnVrfPerfEntry *));

PUBLIC INT4 MplsL3VpnVrfRteTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 L3vpnMplsL3VpnVrfRteTableCreate PROTO ((VOID));

tL3vpnMplsL3VpnVrfRteEntry * L3vpnMplsL3VpnVrfRteTableCreateApi PROTO ((tL3vpnMplsL3VpnVrfRteEntry *));
INT4 L3vpnGetMplsL3VpnConfiguredVrfs PROTO ((UINT4 *));
INT4 L3vpnGetMplsL3VpnActiveVrfs PROTO ((UINT4 *));
INT4 L3vpnGetMplsL3VpnConnectedInterfaces PROTO ((UINT4 *));
INT4 L3vpnGetMplsL3VpnNotificationEnable PROTO ((INT4 *));
INT4 L3vpnGetMplsL3VpnVrfConfMaxPossRts PROTO ((UINT4 *));
INT4 L3vpnGetMplsL3VpnVrfConfRteMxThrshTime PROTO ((UINT4 *));
INT4 L3vpnGetMplsL3VpnIllLblRcvThrsh PROTO ((UINT4 *));
INT4 L3vpnGetAllMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *));
INT4 L3vpnGetAllUtlMplsL3VpnVrfTable PROTO((tL3vpnMplsL3VpnVrfEntry *, tL3vpnMplsL3VpnVrfEntry *));
INT4 L3vpnGetAllMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *));
INT4 L3vpnGetAllUtlMplsL3VpnIfConfTable PROTO((tL3vpnMplsL3VpnIfConfEntry *, tL3vpnMplsL3VpnIfConfEntry *));
INT4 L3vpnGetAllMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *));
INT4 L3vpnGetAllUtlMplsL3VpnVrfRTTable PROTO((tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnMplsL3VpnVrfRTEntry *));
INT4 L3vpnGetAllMplsL3VpnVrfSecTable PROTO ((tL3vpnMplsL3VpnVrfSecEntry *));
INT4 L3vpnGetAllUtlMplsL3VpnVrfSecTable PROTO((tL3vpnMplsL3VpnVrfSecEntry *, tL3vpnMplsL3VpnVrfSecEntry *));
INT4 L3vpnGetAllMplsL3VpnVrfPerfTable PROTO ((tL3vpnMplsL3VpnVrfPerfEntry *));
INT4 L3vpnGetAllUtlMplsL3VpnVrfPerfTable PROTO((tL3vpnMplsL3VpnVrfPerfEntry *, tL3vpnMplsL3VpnVrfPerfEntry *));
INT4 L3vpnGetAllMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *));
INT4 L3vpnGetAllUtlMplsL3VpnVrfRteTable PROTO((tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnMplsL3VpnVrfRteEntry *));
INT4 L3vpnSetMplsL3VpnNotificationEnable PROTO ((INT4 ));
INT4 L3vpnSetMplsL3VpnVrfConfRteMxThrshTime PROTO ((UINT4 ));
INT4 L3vpnSetMplsL3VpnIllLblRcvThrsh PROTO ((UINT4 ));
INT4 L3vpnSetAllMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *, tL3vpnIsSetMplsL3VpnVrfEntry *, INT4 , INT4 ));
INT4 L3vpnSetAllMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *, tL3vpnIsSetMplsL3VpnIfConfEntry *, INT4 , INT4 ));
INT4 L3vpnSetAllMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnIsSetMplsL3VpnVrfRTEntry *, INT4 , INT4 ));
INT4 L3vpnSetAllMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnIsSetMplsL3VpnVrfRteEntry *, INT4 , INT4 ));

INT4 L3vpnInitializeMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *));

INT4 L3vpnInitializeMibMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *));

INT4 L3vpnInitializeMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *));

INT4 L3vpnInitializeMibMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *));

INT4 L3vpnInitializeMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *));

INT4 L3vpnInitializeMibMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *));

INT4 L3vpnInitializeMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *));

INT4 L3vpnInitializeMibMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *));
INT4 L3vpnTestMplsL3VpnNotificationEnable PROTO ((UINT4 *,INT4 ));
INT4 L3vpnTestMplsL3VpnVrfConfRteMxThrshTime PROTO ((UINT4 *, UINT4 ));

INT4 L3vpnTestMplsL3VpnIllLblRcvThrsh PROTO ((UINT4 *,UINT4 ));
INT4 L3vpnTestAllMplsL3VpnVrfTable PROTO ((UINT4 *, tL3vpnMplsL3VpnVrfEntry *, tL3vpnIsSetMplsL3VpnVrfEntry *, INT4 , INT4));
INT4 L3vpnTestAllMplsL3VpnIfConfTable PROTO ((UINT4 *, tL3vpnMplsL3VpnIfConfEntry *, tL3vpnIsSetMplsL3VpnIfConfEntry *, INT4 , INT4));
INT4 L3vpnTestAllMplsL3VpnVrfRTTable PROTO ((UINT4 *, tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnIsSetMplsL3VpnVrfRTEntry *, INT4 , INT4));
INT4 L3vpnTestAllMplsL3VpnVrfRteTable PROTO ((UINT4 *, tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnIsSetMplsL3VpnVrfRteEntry *, INT4 , INT4));
tL3vpnMplsL3VpnVrfEntry * L3vpnGetMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *));
tL3vpnMplsL3VpnIfConfEntry * L3vpnGetMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *));
tL3vpnMplsL3VpnVrfRTEntry * L3vpnGetMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *));
tL3vpnMplsL3VpnVrfSecEntry * L3vpnGetMplsL3VpnVrfSecTable PROTO ((tL3vpnMplsL3VpnVrfSecEntry *));
tL3vpnMplsL3VpnVrfPerfEntry * L3vpnGetMplsL3VpnVrfPerfTable PROTO ((tL3vpnMplsL3VpnVrfPerfEntry *));
tL3vpnMplsL3VpnVrfRteEntry * L3vpnGetMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *));
tL3vpnMplsL3VpnVrfEntry * L3vpnGetFirstMplsL3VpnVrfTable PROTO ((VOID));
tL3vpnMplsL3VpnIfConfEntry * L3vpnGetFirstMplsL3VpnIfConfTable PROTO ((VOID));
tL3vpnMplsL3VpnVrfRTEntry * L3vpnGetFirstMplsL3VpnVrfRTTable PROTO ((VOID));
tL3vpnMplsL3VpnVrfSecEntry * L3vpnGetFirstMplsL3VpnVrfSecTable PROTO ((VOID));
tL3vpnMplsL3VpnVrfPerfEntry * L3vpnGetFirstMplsL3VpnVrfPerfTable PROTO ((VOID));
tL3vpnMplsL3VpnVrfRteEntry * L3vpnGetFirstMplsL3VpnVrfRteTable PROTO ((VOID));
tL3vpnMplsL3VpnVrfEntry * L3vpnGetNextMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *));
tL3vpnMplsL3VpnIfConfEntry * L3vpnGetNextMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *));
tL3vpnMplsL3VpnVrfRTEntry * L3vpnGetNextMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *));
tL3vpnMplsL3VpnVrfSecEntry * L3vpnGetNextMplsL3VpnVrfSecTable PROTO ((tL3vpnMplsL3VpnVrfSecEntry *));
tL3vpnMplsL3VpnVrfPerfEntry * L3vpnGetNextMplsL3VpnVrfPerfTable PROTO ((tL3vpnMplsL3VpnVrfPerfEntry *));
tL3vpnMplsL3VpnVrfRteEntry * L3vpnGetNextMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *));
INT4 MplsL3VpnVrfTableFilterInputs PROTO ((tL3vpnMplsL3VpnVrfEntry *, tL3vpnMplsL3VpnVrfEntry *, tL3vpnIsSetMplsL3VpnVrfEntry *));
INT4 MplsL3VpnIfConfTableFilterInputs PROTO ((tL3vpnMplsL3VpnIfConfEntry *, tL3vpnMplsL3VpnIfConfEntry *, tL3vpnIsSetMplsL3VpnIfConfEntry *));
INT4 MplsL3VpnVrfRTTableFilterInputs PROTO ((tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnIsSetMplsL3VpnVrfRTEntry *));
INT4 MplsL3VpnVrfRteTableFilterInputs PROTO ((tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnIsSetMplsL3VpnVrfRteEntry *));
INT4 L3vpnUtilUpdateMplsL3VpnVrfTable PROTO ((tL3vpnMplsL3VpnVrfEntry *, tL3vpnMplsL3VpnVrfEntry *, tL3vpnIsSetMplsL3VpnVrfEntry *));
INT4 L3vpnUtilUpdateMplsL3VpnIfConfTable PROTO ((tL3vpnMplsL3VpnIfConfEntry *, tL3vpnMplsL3VpnIfConfEntry *, tL3vpnIsSetMplsL3VpnIfConfEntry *));
INT4 L3vpnUtilUpdateMplsL3VpnVrfRTTable PROTO ((tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnIsSetMplsL3VpnVrfRTEntry *));
INT4 L3vpnUtilUpdateMplsL3VpnVrfRteTable PROTO ((tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnIsSetMplsL3VpnVrfRteEntry *));
INT4 L3vpnSetAllMplsL3VpnVrfTableTrigger PROTO ((tL3vpnMplsL3VpnVrfEntry *, tL3vpnIsSetMplsL3VpnVrfEntry *, INT4));
INT4 L3vpnSetAllMplsL3VpnIfConfTableTrigger PROTO ((tL3vpnMplsL3VpnIfConfEntry *, tL3vpnIsSetMplsL3VpnIfConfEntry *, INT4));
INT4 L3vpnSetAllMplsL3VpnVrfRTTableTrigger PROTO ((tL3vpnMplsL3VpnVrfRTEntry *, tL3vpnIsSetMplsL3VpnVrfRTEntry *, INT4));
INT4 L3vpnSetAllMplsL3VpnVrfRteTableTrigger PROTO ((tL3vpnMplsL3VpnVrfRteEntry *, tL3vpnIsSetMplsL3VpnVrfRteEntry *, INT4));

