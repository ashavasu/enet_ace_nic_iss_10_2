/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnwrg.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnwrg.h
*
* Description: This file contains the prototype(wr) for L3vpn 
*********************************************************************/
#ifndef _L3VPNWR_H
#define _L3VPNWR_H


INT4 GetNextIndexMplsL3VpnVrfTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexMplsL3VpnIfConfTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexMplsL3VpnVrfRTTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexMplsL3VpnVrfSecTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexMplsL3VpnVrfPerfTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexMplsL3VpnVrfRteTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsL3VpnConfiguredVrfsGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnActiveVrfsGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnConnectedInterfacesGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnNotificationEnableGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMaxPossRtsGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfRteMxThrshTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIllLblRcvThrshGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfVpnIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfDescriptionGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRDGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfCreationTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfActiveInterfacesGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfAssociatedInterfacesGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMidRteThreshGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfHighRteThreshGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMaxRoutesGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfLastChangedGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfVpnClassificationGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfVpnRouteDistProtocolGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfConfStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfConfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTDescrGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfSecIllegalLblVltnsGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfSecDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfPerfRoutesAddedGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfPerfRoutesDeletedGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfPerfCurrNumRoutesGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfPerfRoutesDroppedGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfPerfDiscTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrProtoGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrAgeGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrNextHopASGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric1Get(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric2Get(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric3Get(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric4Get(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric5Get(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteXCPointerGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnNotificationEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfRteMxThrshTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIllLblRcvThrshTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfVpnIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfDescriptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMidRteThreshTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfHighRteThreshTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMaxRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfVpnClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfVpnRouteDistProtocolTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfConfStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfConfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTDescrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrNextHopASTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric2Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric3Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric4Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric5Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteXCPointerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnNotificationEnableSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfRteMxThrshTimeSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIllLblRcvThrshSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfVpnIdSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfDescriptionSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRDSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMidRteThreshSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfHighRteThreshSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfMaxRoutesSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfConfStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfVpnClassificationSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfVpnRouteDistProtocolSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfConfStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnIfConfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTDescrSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRTStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrNextHopASSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric1Set(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric2Set(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric3Set(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric4Set(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrMetric5Set(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteXCPointerSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnVrfRteInetCidrStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsL3VpnNotificationEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MplsL3VpnVrfConfRteMxThrshTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MplsL3VpnIllLblRcvThrshDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 MplsL3VpnVrfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 MplsL3VpnIfConfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 MplsL3VpnVrfRTTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 MplsL3VpnVrfRteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

VOID  RegisterSTDL3V PROTO ((VOID));
VOID  UnRegisterSTDL3V PROTO ((VOID));

#endif /* _L3VPNWR_H */
