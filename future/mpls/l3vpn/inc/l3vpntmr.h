/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntmr.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains definitions for l3vpn Timer
 *******************************************************************/

#ifndef __L3VPNTMR_H__
#define __L3VPNTMR_H__



/* constants for timer types */
typedef enum {
	L3VPN_TMR1 =0,
	L3VPN_MAX_TMRS = 1 
} enL3vpnTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _L3VPN_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tL3vpnTmrDesc;

typedef struct _L3VPN_TIMER {
       tTmrAppTimer    tmrNode;
       enL3vpnTmrId     eL3vpnTmrId;
} tL3vpnTmr;




#endif  /* __L3VPNTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpntmr.h                      */
/*-----------------------------------------------------------------------*/
