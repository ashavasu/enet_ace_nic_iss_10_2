/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnmibclig.h,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnmibclig.h
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 MplsL3VpnNotificationEnable[12];
extern UINT4 MplsL3VpnVrfConfRteMxThrshTime[12];
extern UINT4 MplsL3VpnIllLblRcvThrsh[12];
extern UINT4 MplsL3VpnVrfName[14];
extern UINT4 MplsL3VpnVrfVpnId[14];
extern UINT4 MplsL3VpnVrfDescription[14];
extern UINT4 MplsL3VpnVrfRD[14];
extern UINT4 MplsL3VpnVrfConfMidRteThresh[14];
extern UINT4 MplsL3VpnVrfConfHighRteThresh[14];
extern UINT4 MplsL3VpnVrfConfMaxRoutes[14];
extern UINT4 MplsL3VpnVrfConfRowStatus[14];
extern UINT4 MplsL3VpnVrfConfAdminStatus[14];
extern UINT4 MplsL3VpnVrfConfStorageType[14];
extern UINT4 MplsL3VpnIfConfIndex[14];
extern UINT4 MplsL3VpnIfVpnClassification[14];
extern UINT4 MplsL3VpnIfVpnRouteDistProtocol[14];
extern UINT4 MplsL3VpnIfConfStorageType[14];
extern UINT4 MplsL3VpnIfConfRowStatus[14];
extern UINT4 MplsL3VpnVrfRTIndex[14];
extern UINT4 MplsL3VpnVrfRTType[14];
extern UINT4 MplsL3VpnVrfRT[14];
extern UINT4 MplsL3VpnVrfRTDescr[14];
extern UINT4 MplsL3VpnVrfRTRowStatus[14];
extern UINT4 MplsL3VpnVrfRTStorageType[14];
extern UINT4 MplsL3VpnVrfRteInetCidrDestType[14];
extern UINT4 MplsL3VpnVrfRteInetCidrDest[14];
extern UINT4 MplsL3VpnVrfRteInetCidrPfxLen[14];
extern UINT4 MplsL3VpnVrfRteInetCidrPolicy[14];
extern UINT4 MplsL3VpnVrfRteInetCidrNHopType[14];
extern UINT4 MplsL3VpnVrfRteInetCidrNextHop[14];
extern UINT4 MplsL3VpnVrfRteInetCidrIfIndex[14];
extern UINT4 MplsL3VpnVrfRteInetCidrType[14];
extern UINT4 MplsL3VpnVrfRteInetCidrNextHopAS[14];
extern UINT4 MplsL3VpnVrfRteInetCidrMetric1[14];
extern UINT4 MplsL3VpnVrfRteInetCidrMetric2[14];
extern UINT4 MplsL3VpnVrfRteInetCidrMetric3[14];
extern UINT4 MplsL3VpnVrfRteInetCidrMetric4[14];
extern UINT4 MplsL3VpnVrfRteInetCidrMetric5[14];
extern UINT4 MplsL3VpnVrfRteXCPointer[14];
extern UINT4 MplsL3VpnVrfRteInetCidrStatus[14];



