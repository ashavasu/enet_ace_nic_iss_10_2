/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndefault.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpndefault.c 
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module L3vpn 
*********************************************************************/

#include "l3vpninc.h"

/****************************************************************************
* Function    : L3vpnInitializeMplsL3VpnVrfTable
* Input       : pL3vpnMplsL3VpnVrfEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
L3vpnInitializeMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                                  pL3vpnMplsL3VpnVrfEntry)
{
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((L3vpnInitializeMibMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnInitializeMplsL3VpnIfConfTable
* Input       : pL3vpnMplsL3VpnIfConfEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
L3vpnInitializeMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                     pL3vpnMplsL3VpnIfConfEntry)
{
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((L3vpnInitializeMibMplsL3VpnIfConfTable (pL3vpnMplsL3VpnIfConfEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnInitializeMplsL3VpnVrfRTTable
* Input       : pL3vpnMplsL3VpnVrfRTEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
L3vpnInitializeMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                    pL3vpnMplsL3VpnVrfRTEntry)
{
    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((L3vpnInitializeMibMplsL3VpnVrfRTTable (pL3vpnMplsL3VpnVrfRTEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnInitializeMplsL3VpnVrfRteTable
* Input       : pL3vpnMplsL3VpnVrfRteEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
L3vpnInitializeMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                     pL3vpnMplsL3VpnVrfRteEntry)
{
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((L3vpnInitializeMibMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
