/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpncli.c,v 1.11 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

#ifndef __L3VPNCLI_C__
#define __L3VPNCLI_C__

#include "l3vpninc.h"
#include "l3vpncli.h"
#include "mpls.h"
#include "mplscli.h"
#include "fsmplslw.h"

static tL3vpnMplsL3VpnVrfRteEntry L3vpnSetMplsL3VpnVrfRteEntry;

INT4
cli_process_l3vpn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    va_list             ap;
    UINT4              *args[L3VPN_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    UINT1              *pu1MplsL3VpnVrfContextName = NULL;
    UINT4               u4VcId;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tL3vpnMplsL3VpnVrfEntry L3vpnSetMplsL3VpnVrfEntry;
    tL3vpnIsSetMplsL3VpnVrfEntry L3vpnIsSetMplsL3VpnVrfEntry;
    tL3vpnIsSetMplsL3VpnVrfRteEntry L3vpnIsSetMplsL3VpnVrfRteEntry;
#if 1
    UINT4               u4VrfId = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4Label = 0;
#endif
    UINT4               u4Destination = 0;
    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;

    tSNMP_OCTET_STRING_TYPE SetValFsMplsL3VpnVrfName;
    UINT1               au1SetValFsMplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];

    MEMSET (&MplsL3VpnBgp4RouteInfo, 0, sizeof (tMplsL3VpnBgp4RouteInfo));
    UNUSED_PARAM (u4CmdType);

    SetValFsMplsL3VpnVrfName.pu1_OctetList = au1SetValFsMplsL3VpnVrfName;
    SetValFsMplsL3VpnVrfName.i4_Length = 0;

    MEMSET (au1SetValFsMplsL3VpnVrfName, 0, L3VPN_MAX_VRF_NAME_LEN);

#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, L3vpnMainTaskLock, L3vpnMainTaskUnLock);
#endif

    L3VPN_LOCK;
    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    UNUSED_PARAM (u4IfIndex);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == L3VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {

        case CLI_MPLS_L3VPN_CREATE_VRF:

            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            if (VcmIsVrfExist (pu1MplsL3VpnVrfContextName, &u4VcId) ==
                VCM_FALSE)
            {
                CliPrintf (CliHandle, "\r%% VRF %s does not exist in "
                           "VCM\n", pu1MplsL3VpnVrfContextName);
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                if (MplsL3VpnCliVrfChangeMode (u4VcId) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Unable to Change mode\n");
                    i4RetStatus = CLI_FAILURE;
                }
                else
                {
                    /* Create and Wait the Vrf Table row */
                    MEMSET (&L3vpnSetMplsL3VpnVrfEntry, 0,
                            sizeof (tL3vpnMplsL3VpnVrfEntry));
                    MEMSET (&L3vpnIsSetMplsL3VpnVrfEntry, 0,
                            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

                    MEMCPY (&
                            (L3vpnSetMplsL3VpnVrfEntry.MibObject.
                             au1MplsL3VpnVrfName), pu1MplsL3VpnVrfContextName,
                            STRLEN (pu1MplsL3VpnVrfContextName));
                    L3vpnSetMplsL3VpnVrfEntry.MibObject.
                        i4MplsL3VpnVrfConfRowStatus = CREATE_AND_WAIT;
                    L3vpnSetMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfNameLen =
                        (INT4) STRLEN (pu1MplsL3VpnVrfContextName);

                    L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfName = OSIX_TRUE;
                    L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus =
                        OSIX_TRUE;

                    if (MplsL3VpnApiGetVrfTableEntry
                        (pu1MplsL3VpnVrfContextName,
                         L3vpnSetMplsL3VpnVrfEntry.MibObject.
                         i4MplsL3VpnVrfNameLen) == NULL)
                    {
                        i4RetStatus = L3vpnCliSetMplsL3VpnVrfTable (CliHandle,
                                                                    &L3vpnSetMplsL3VpnVrfEntry,
                                                                    &L3vpnIsSetMplsL3VpnVrfEntry);
                    }
                }
            }
            break;
        case CLI_MPLS_L3VPN_DELETE_VRF:

            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            if (VcmIsVrfExist (pu1MplsL3VpnVrfContextName, &u4VcId) ==
                VCM_FALSE)
            {
                CliPrintf (CliHandle, "\r%% VRF %s does not exist in "
                           "VCM\n", pu1MplsL3VpnVrfContextName);
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                /* Create and Wait the Vrf Table row */
                MEMSET (&L3vpnSetMplsL3VpnVrfEntry, 0,
                        sizeof (tL3vpnMplsL3VpnVrfEntry));
                MEMSET (&L3vpnIsSetMplsL3VpnVrfEntry, 0,
                        sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

                MEMCPY (&
                        (L3vpnSetMplsL3VpnVrfEntry.MibObject.
                         au1MplsL3VpnVrfName), pu1MplsL3VpnVrfContextName,
                        STRLEN (pu1MplsL3VpnVrfContextName));
                L3vpnSetMplsL3VpnVrfEntry.MibObject.
                    i4MplsL3VpnVrfConfRowStatus = DESTROY;
                L3vpnSetMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfNameLen =
                    (INT4) STRLEN (pu1MplsL3VpnVrfContextName);

                L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfName = OSIX_TRUE;
                L3vpnIsSetMplsL3VpnVrfEntry.bMplsL3VpnVrfConfRowStatus =
                    OSIX_TRUE;

                if (MplsL3VpnApiGetVrfTableEntry (pu1MplsL3VpnVrfContextName,
                                                  L3vpnSetMplsL3VpnVrfEntry.
                                                  MibObject.
                                                  i4MplsL3VpnVrfNameLen) !=
                    NULL)
                {

                    i4RetStatus = L3vpnCliSetMplsL3VpnVrfTable (CliHandle,
                                                                &L3vpnSetMplsL3VpnVrfEntry,
                                                                &L3vpnIsSetMplsL3VpnVrfEntry);
                }
                else
                {
                    CliPrintf (CliHandle, "\r%% MPLS VRF %s does not exist in "
                               "VCM\n", pu1MplsL3VpnVrfContextName);
                    i4RetStatus = CLI_FAILURE;
                }
            }
            break;

        case CLI_L3VPN_DEBUG:
            L3VPN_TRC_FLAG = (*(UINT4 *) (args[0]));
            break;

        case CLI_L3VPN_ROUTE_ADD_PER_VRF:
            u4VrfId = (*(UINT4 *) (args[1]));
            u4Label = (*(UINT4 *) (args[0]));
            MplsL3VpnBgp4RouteInfo.u1LabelAllocPolicy =
                L3VPN_BGP4_POLICY_PER_VRF;

            if (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus (&u4ErrCode,
                                                                u4Label,
                                                                CREATE_AND_WAIT)
                != SNMP_SUCCESS)
            {
                CLI_SET_ERR (u4ErrCode);
                i4RetStatus = CLI_FAILURE;
            }
            if (i4RetStatus != CLI_FAILURE)
            {
                if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                 CREATE_AND_WAIT)
                    != SNMP_SUCCESS)
                {
                    i4RetStatus = CLI_FAILURE;
                }

            }

            VcmGetAliasName (u4VrfId, SetValFsMplsL3VpnVrfName.pu1_OctetList);
            SetValFsMplsL3VpnVrfName.i4_Length =
                (INT4) STRLEN (SetValFsMplsL3VpnVrfName.pu1_OctetList);
            if (i4RetStatus != CLI_FAILURE)
            {
                if (nmhTestv2FsMplsL3VpnVrfName (&u4ErrCode,
                                                 u4Label,
                                                 &SetValFsMplsL3VpnVrfName) !=
                    SNMP_SUCCESS)
                {
                    CLI_SET_ERR (u4ErrCode);

                    if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                     DESTROY) !=
                        SNMP_SUCCESS)
                    {
                        i4RetStatus = CLI_FAILURE;
                    }

                    i4RetStatus = CLI_FAILURE;
                }
            }

            if (i4RetStatus != CLI_FAILURE)
            {
                if (nmhSetFsMplsL3VpnVrfName
                    (u4Label, &SetValFsMplsL3VpnVrfName) != SNMP_SUCCESS)
                {
                    if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                     DESTROY) !=
                        SNMP_SUCCESS)
                    {
                        i4RetStatus = CLI_FAILURE;
                    }
                    i4RetStatus = CLI_FAILURE;
                }

            }

            if (i4RetStatus != CLI_FAILURE)
            {
                if (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus (&u4ErrCode,
                                                                    u4Label,
                                                                    ACTIVE) !=
                    SNMP_SUCCESS)
                {
                    CLI_SET_ERR (u4ErrCode);

                    if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                     DESTROY) !=
                        SNMP_SUCCESS)
                    {
                        i4RetStatus = CLI_FAILURE;
                    }
                    i4RetStatus = CLI_FAILURE;
                }
            }
            if (i4RetStatus != CLI_FAILURE)
            {
                if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                 ACTIVE) !=
                    SNMP_SUCCESS)
                {
                    if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                     DESTROY) !=
                        SNMP_SUCCESS)
                    {
                        i4RetStatus = CLI_FAILURE;
                    }
                    i4RetStatus = CLI_FAILURE;
                }

            }

            break;

        case CLI_L3VPN_ROUTE_ADD_PER_ROUTE:
#if 0
            u4VrfId = (*(UINT4 *) (args[3]));
            u4BgpIfIndex = (*(UINT4 *) (args[0]));
            u4NextHop = (*(UINT4 *) (args[1]));
            u4Label = (*(UINT4 *) (args[2]));
            u4NextHop = OSIX_HTONL (u4NextHop);
            MplsL3VpnBgp4RouteInfo.u1LabelAllocPolicy =
                L3VPN_BGP4_POLICY_PER_ROUTE;
            MplsL3VpnBgp4RouteInfo.u4IfIndex = u4BgpIfIndex;
            MplsL3VpnBgp4RouteInfo.u4Label = u4Label;
            MplsL3VpnBgp4RouteInfo.NextHop.u2AddressLen =
                L3VPN_BGP_MAX_ADDRESS_LEN;
            MplsL3VpnBgp4RouteInfo.u1ILMAction = L3VPN_BGP4_ROUTE_ADD;
            MplsL3VpnBgp4RouteInfo.u1LabelAction = L3VPN_BGP4_LABEL_POP;
            MplsL3VpnBgp4RouteInfo.IpPrefix.u2Afi = 1;
            MplsL3VpnBgp4RouteInfo.NextHop.u2Afi = 1;
            MplsL3VpnBgp4RouteInfo.u4VrfId = u4VrfId;

            MEMCPY (MplsL3VpnBgp4RouteInfo.NextHop.au1Address, &u4NextHop,
                    L3VPN_BGP_MAX_ADDRESS_LEN);

            if (MplsL3VpnBgp4RouteUpdate (&MplsL3VpnBgp4RouteInfo) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% MPLS VRF %s does not exist in "
                           "VCM\n", pu1MplsL3VpnVrfContextName);
                i4RetStatus = CLI_FAILURE;
            }
#endif
            i4RetStatus = CLI_FAILURE;
            break;

        case CLI_L3VPN_ROUTE_DEL_EGRESS_ROUTE:
            u4VrfId = (*(UINT4 *) (args[1]));
            u4Label = (*(UINT4 *) (args[0]));

            if (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus (&u4ErrCode,
                                                                u4Label,
                                                                DESTROY) !=
                SNMP_SUCCESS)
            {
                CLI_SET_ERR (u4ErrCode);
                i4RetStatus = CLI_FAILURE;
            }
            if (i4RetStatus != CLI_FAILURE)
            {
                if (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus (u4Label,
                                                                 DESTROY) !=
                    SNMP_SUCCESS)
                {
                    i4RetStatus = CLI_FAILURE;
                }
            }
            break;

        case CLI_L3VPN_BGP_ROUTE_ADD:

            u4VrfId = *((UINT4 *) args[4]);
            u4Destination = OSIX_HTONL (*((UINT4 *) args[0]));
            u4NextHop = OSIX_HTONL (*((UINT4 *) args[2]));
            u4Label = *((UINT4 *) args[3]);
            if (VcmGetAliasName (u4VrfId, au1VrfName) == VCM_FAILURE)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Failed to get the VRF Alias for the Context: %d.\r\n",
                            u4VrfId));
                i4RetStatus = CLI_FAILURE;
            }

            MEMSET (&L3vpnSetMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfRteEntry));
            MEMSET (&L3vpnIsSetMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

            MEMCPY (&
                    (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                     au1MplsL3VpnVrfName), au1VrfName, STRLEN (au1VrfName));

            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfNameLen =
                (INT4) STRLEN (au1VrfName);
            MEMCPY (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                    au1MplsL3VpnVrfRteInetCidrDest, (UINT1 *) &u4Destination,
                    sizeof (UINT4));
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrNextHopLen = IPV4_ADDR_LENGTH;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrDestLen = IPV4_ADDR_LENGTH;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                u4MplsL3VpnVrfRteInetCidrPfxLen = *((UINT4 *) args[1]);
            MEMSET (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                    au4MplsL3VpnVrfRteInetCidrPolicy, 0, 256);
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrDestType = MPLS_IPV4_ADDR_TYPE;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrNHopType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                    au1MplsL3VpnVrfRteInetCidrNextHop, (UINT1 *) &u4NextHop,
                    sizeof (UINT4));

            /*next-hop-as, port, metric are not used in processing, hence using Default Mib Values */

            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrIfIndex = L3VPN_DEF_PORT_IFINDEX;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrMetric1 = L3VPN_DEF_ROUTE_METRIC;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                u4MplsL3VpnVrfRteInetCidrNextHopAS = L3VPN_DEF_NEXTHOP_AS;

            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrProto = L3VPN_MPLS_ROUTE_PROTO_BGP;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrMetric2 = L3VPN_DEF_ROUTE_METRIC;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrMetric3 = L3VPN_DEF_ROUTE_METRIC;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrMetric4 = L3VPN_DEF_ROUTE_METRIC;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrMetric5 = L3VPN_DEF_ROUTE_METRIC;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrStatus = CREATE_AND_WAIT;

            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfName = OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrDest =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrDestType =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrPfxLen =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrPolicy =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrNHopType =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrNextHop =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrStatus =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrIfIndex =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrNextHopAS =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrMetric1 =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrMetric2 =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrMetric3 =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrMetric4 =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrMetric5 =
                OSIX_TRUE;

            if (MplsL3VpnApiGetVrfTableEntry (au1VrfName,
                                              L3vpnSetMplsL3VpnVrfRteEntry.
                                              MibObject.
                                              i4MplsL3VpnVrfNameLen) != NULL)
            {
                L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteXCPointer =
                    OSIX_TRUE;

                if (L3vpnCliSetMplsL3VpnVrfRteTable (CliHandle,
                                                     &L3vpnSetMplsL3VpnVrfRteEntry,
                                                     &L3vpnIsSetMplsL3VpnVrfRteEntry)
                    == CLI_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnCliSetMplsL3VpnVrfRteTable() Failed.\r\n"));
                    CliPrintf (CliHandle,
                               "\r%% Maximum routes exceeded the set limit\n",
                               au1VrfName);
                    i4RetStatus = CLI_FAILURE;
                }
                if (i4RetStatus != CLI_FAILURE)
                {
                    L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteXCPointer =
                        OSIX_TRUE;

                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrIfIndex = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrNextHopAS = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric1 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric2 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric3 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric4 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric5 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrStatus = OSIX_FALSE;

                    if (MplsL3VpnCreateCrossConnectEntry
                        (&L3vpnSetMplsL3VpnVrfRteEntry,
                         u4Label) == OSIX_FAILURE)
                    {
                        L3VPN_TRC ((L3VPN_UTIL_TRC,
                                    "MplsL3vpnCreateCrossConnectEntry() Failed.\r\n"));

                        CliPrintf (CliHandle,
                                   "\r%% Non-TE LSP is not there Route Addition failed\n",
                                   au1VrfName);

                        i4RetStatus = CLI_FAILURE;
                    }

                }
                if (i4RetStatus == CLI_FAILURE)
                {
                    L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                        i4MplsL3VpnVrfRteInetCidrStatus = DESTROY;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;

                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrIfIndex = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrNextHopAS = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric1 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric2 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric3 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric4 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric5 = OSIX_FALSE;

                    if (L3vpnCliSetMplsL3VpnVrfRteTable (CliHandle,
                                                         &L3vpnSetMplsL3VpnVrfRteEntry,
                                                         &L3vpnIsSetMplsL3VpnVrfRteEntry)
                        == CLI_FAILURE)
                    {
                        L3VPN_TRC ((L3VPN_UTIL_TRC,
                                    "L3vpnCliSetMplsL3VpnVrfRteTable() Failed.\r\n"));
                        i4RetStatus = CLI_FAILURE;
                    }

                }
                else
                {
                    L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                        i4MplsL3VpnVrfRteInetCidrStatus = ACTIVE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;

                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrIfIndex = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrNextHopAS = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric1 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric2 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric3 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric4 = OSIX_FALSE;
                    L3vpnIsSetMplsL3VpnVrfRteEntry.
                        bMplsL3VpnVrfRteInetCidrMetric5 = OSIX_FALSE;

                    if (L3vpnCliSetMplsL3VpnVrfRteTable (CliHandle,
                                                         &L3vpnSetMplsL3VpnVrfRteEntry,
                                                         &L3vpnIsSetMplsL3VpnVrfRteEntry)
                        == CLI_FAILURE)
                    {
                        L3VPN_TRC ((L3VPN_UTIL_TRC,
                                    "L3vpnCliSetMplsL3VpnVrfRteTable() Failed.\r\n"));
                        i4RetStatus = CLI_FAILURE;
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%% MPLS VRF %s does not exist in "
                           "VCM\n", au1VrfName);
                i4RetStatus = CLI_FAILURE;

            }
            break;

        case CLI_L3VPN_BGP_ROUTE_DEL:
            MEMSET (&L3vpnSetMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfRteEntry));
            MEMSET (&L3vpnIsSetMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrDestType = MPLS_IPV4_ADDR_TYPE;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrDestLen = IPV4_ADDR_LENGTH;
            u4Destination = OSIX_HTONL (*((UINT4 *) args[0]));
            MEMCPY (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                    au1MplsL3VpnVrfRteInetCidrDest,
                    (UINT1 *) &u4Destination, sizeof (UINT4));
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrNHopType = MPLS_IPV4_ADDR_TYPE;
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrNextHopLen = IPV4_ADDR_LENGTH;
            u4NextHop = OSIX_HTONL (*((UINT4 *) args[2]));
            MEMCPY (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                    au1MplsL3VpnVrfRteInetCidrNextHop,
                    (UINT1 *) &u4NextHop, sizeof (UINT4));
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                u4MplsL3VpnVrfRteInetCidrPfxLen = *((UINT2 *) args[1]);
            u4VrfId = *((UINT4 *) args[3]);
            if (VcmGetAliasName (u4VrfId, au1VrfName) == VCM_FAILURE)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Failed to get the VRF Alias for the Context: %d.\r\n",
                            u4VrfId));
                i4RetStatus = CLI_FAILURE;
            }
            MEMCPY (&
                    (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                     au1MplsL3VpnVrfName), au1VrfName, STRLEN (au1VrfName));
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfNameLen =
                (INT4) STRLEN (au1VrfName);

            MEMSET (L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                    au4MplsL3VpnVrfRteInetCidrPolicy, 0, 256);
            L3vpnSetMplsL3VpnVrfRteEntry.MibObject.
                i4MplsL3VpnVrfRteInetCidrStatus = DESTROY;

            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrPolicy =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfName = OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrPfxLen =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrDest =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrNextHop =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrDestType =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrNHopType =
                OSIX_TRUE;
            L3vpnIsSetMplsL3VpnVrfRteEntry.bMplsL3VpnVrfRteInetCidrStatus =
                OSIX_TRUE;

            if (MplsL3VpnApiGetVrfTableEntry (au1VrfName,
                                              L3vpnSetMplsL3VpnVrfEntry.
                                              MibObject.
                                              i4MplsL3VpnVrfNameLen) != NULL)
            {
                if (L3vpnCliSetMplsL3VpnVrfRteTable (CliHandle,
                                                     &L3vpnSetMplsL3VpnVrfRteEntry,
                                                     &L3vpnIsSetMplsL3VpnVrfRteEntry)
                    != CLI_SUCCESS)
                {

                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnCliSetMplsL3VpnVrfRteTable() Failed.\r\n"));
                    i4RetStatus = CLI_FAILURE;
                }

            }
            else
            {
                CliPrintf (CliHandle, "\r%% MPLS VRF %s does not exist in "
                           "VCM\n", au1VrfName);
                i4RetStatus = CLI_FAILURE;

            }
            break;

#ifdef MPLS_L3VPN_TEST_WANTED
        case CLI_L3VPN_SEARCH_TRIE:
            u4ContextId = *(UINT4 *) (args[0]);
            u4Prefix = (OSIX_NTOHL (*(UINT4 *) args[1]));

            pL3vpnMplsL3VpnVrfRteEntry =
                L3VpnGetBestMatchRteTableEntryFromTrie (u4ContextId, u4Prefix);

            pRteEntry = L3VpnGetRteTableEntryFromTrie (u4ContextId, u4Prefix);

            if (pRteEntry != NULL)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "Non Best Match entry is not null\n"));

            }
            else
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC, "Non Best Match entry is null\n"));
            }
            if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC, "Entry is NULL\n"));
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC, "Prefix Len = %d\n",
                            L3VPN_P_RTE_INET_CIDR_PREFIX_LEN
                            (pL3vpnMplsL3VpnVrfRteEntry)));
                L3VPN_TRC ((L3VPN_MAIN_TRC, "Destination = %d.%d.%d.%d\n",
                            L3VPN_P_RTE_INET_CIDR_DEST
                            (pL3vpnMplsL3VpnVrfRteEntry)[0],
                            L3VPN_P_RTE_INET_CIDR_DEST
                            (pL3vpnMplsL3VpnVrfRteEntry)[1],
                            L3VPN_P_RTE_INET_CIDR_DEST
                            (pL3vpnMplsL3VpnVrfRteEntry)[2],
                            L3VPN_P_RTE_INET_CIDR_DEST
                            (pL3vpnMplsL3VpnVrfRteEntry)[3]));
                L3VPN_TRC ((L3VPN_MAIN_TRC, "Next-Hop =  %d.%d.%d.%d\n",
                            L3VPN_P_RTE_INET_CIDR_NH
                            (pL3vpnMplsL3VpnVrfRteEntry)[0],
                            L3VPN_P_RTE_INET_CIDR_NH
                            (pL3vpnMplsL3VpnVrfRteEntry)[1],
                            L3VPN_P_RTE_INET_CIDR_NH
                            (pL3vpnMplsL3VpnVrfRteEntry)[2],
                            L3VPN_P_RTE_INET_CIDR_NH
                            (pL3vpnMplsL3VpnVrfRteEntry)[3]));

            }

            break;
#endif
        default:
            break;
    };
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_L3VPN)
            && (u4ErrCode < CLI_L3VPN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       L3vpnCliErrString[CLI_ERR_OFFSET_L3VPN (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif

    L3VPN_UNLOCK;

    return i4RetStatus;
}

#ifdef MPLS_L3VPN_WANTED

INT4
MplsL3VpnCliVrfChangeMode (UINT4 u4VcId)
{
    CHR1                ac1Cmd[MAX_PROMPT_LEN];

    MEMSET (ac1Cmd, 0, MAX_PROMPT_LEN);
    SNPRINTF (ac1Cmd, MAX_PROMPT_LEN, "%s%d", MPLS_L3VPN_VRF_MODE, u4VcId);

#ifdef CLI_WANTED
    /* Change the Mode */
    if (CliChangePath (ac1Cmd) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
#endif
    return CLI_SUCCESS;
}

INT1
MplsL3VpnGetMplsL3VpnVrfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_L3VPN_VRF_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];
#ifdef CLI_WANTED
    UINT4               u4Index = 0;
#endif
    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_L3VPN_VRF_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
#ifdef CLI_WANTED
    u4Index = (UINT4) CLI_ATOI (pi1ModeName);
#endif

#ifdef CLI_WANTED
    CLI_SET_MPLS_L3VPN_VRF_MODE ((INT4) u4Index);
#endif

    SNPRINTF (ac1PromptDisplayName, sizeof (ac1PromptDisplayName), "%s",
              "(ip-vrf-mpls)#");
    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';
    return TRUE;
}

INT4
CliGetL3vpnDebugLevel (tCliHandle CliHandle, UINT4 *pu4DebugLevel)
{
    UNUSED_PARAM (CliHandle);
    if (pu4DebugLevel == NULL)
    {
        return OSIX_FAILURE;
    }
    L3VPN_LOCK;
    *pu4DebugLevel = L3VPN_TRC_FLAG;
    L3VPN_UNLOCK;
    return OSIX_SUCCESS;
}

INT4
L3VpnCliParseAndGenerateRdRt (UINT1 *pu1RandomString, UINT1 *pu1RdRt)
{
    UINT4               u4AsnValue = L3VPN_ZERO;
    UINT4               u4nnValue = L3VPN_ZERO;
    UINT4               u4LoopIndex = L3VPN_ZERO;
    UINT4               u4IpIndex = L3VPN_ZERO;
    UINT2               u2AsnValue = L3VPN_ZERO;
    UINT2               u2nnValue = L3VPN_ZERO;
    UINT1               au1InputString[L3VPN_MAX_RD_RT_STRING_LEN + 1];
    UINT4               au4IPValue[IPV4_ADDR_LENGTH];
    UINT1               u1IsColonFound = OSIX_FALSE;
    UINT1               u1IsDotFound = OSIX_FALSE;

    MEMSET (au1InputString, L3VPN_ZERO, sizeof (au1InputString));
    MEMSET (au4IPValue, L3VPN_ZERO, IPV4_ADDR_LENGTH);

    if (NULL == pu1RandomString)
    {
        return CLI_FAILURE;
    }

    STRNCPY (au1InputString, pu1RandomString, L3VPN_MAX_RD_RT_STRING_LEN);

    au1InputString[L3VPN_MAX_RD_RT_STRING_LEN] = '\0';

    for (u4LoopIndex = L3VPN_ZERO; u4LoopIndex < STRLEN (au1InputString);
         u4LoopIndex++)
    {
        if (au1InputString[u4LoopIndex] == '.')
        {
            if (OSIX_TRUE == u1IsColonFound)
            {
                return CLI_FAILURE;
            }

            pu1RdRt[0] = L3VPN_RT_TYPE_1;
            au4IPValue[u4IpIndex] = (UINT4) ATOI (pu1RandomString);
            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            u1IsDotFound = OSIX_TRUE;
            u4IpIndex++;

            if (IPV4_ADDR_LENGTH == u4IpIndex)
            {
                return CLI_FAILURE;
            }
            continue;
        }

        if (au1InputString[u4LoopIndex] == ':')
        {
            if (OSIX_TRUE == u1IsColonFound)
            {
                return CLI_FAILURE;
            }

            if (OSIX_FALSE == u1IsDotFound)
            {
                u4AsnValue = (UINT4) ATOI (pu1RandomString);
                if (UINT2_MAX_VALUE < u4AsnValue)
                {
                    pu1RdRt[0] = L3VPN_RT_TYPE_2;
                }
                else
                {
                    pu1RdRt[0] = L3VPN_RT_TYPE_0;
                }
            }
            else
            {
                au4IPValue[u4IpIndex] = (UINT4) ATOI (pu1RandomString);
                pu1RandomString = &au1InputString[u4LoopIndex + 1];
                u4IpIndex++;
                if (IPV4_ADDR_LENGTH != u4IpIndex)
                {
                    if (2 == u4IpIndex)
                    {
                        pu1RdRt[0] = L3VPN_RT_TYPE_2;
                    }
                    else
                    {
                        return CLI_FAILURE;
                    }
                }
            }

            if (u4LoopIndex == (STRLEN (au1InputString) - 1))
            {
                return CLI_FAILURE;
            }

            pu1RandomString = &au1InputString[u4LoopIndex + 1];
            u1IsColonFound = OSIX_TRUE;
            continue;
        }

        if (!(ISDIGIT (au1InputString[u4LoopIndex])))
        {
            return CLI_FAILURE;
        }
    }

    if (OSIX_TRUE != u1IsColonFound)
    {
        return CLI_FAILURE;
    }

    u4nnValue = (UINT4) ATOI (pu1RandomString);

    if (L3VPN_RT_TYPE_0 == pu1RdRt[0])
    {
        u2AsnValue = (UINT2) u4AsnValue;

        u2AsnValue = OSIX_HTONS (u2AsnValue);
        u4nnValue = OSIX_HTONL (u4nnValue);

        MEMCPY (&pu1RdRt[2], &u2AsnValue, sizeof (UINT2));
        MEMCPY (&pu1RdRt[4], &u4nnValue, sizeof (UINT4));
    }

    if (L3VPN_RT_TYPE_1 == pu1RdRt[0])
    {
        if (UINT2_MAX_VALUE < u4nnValue)
        {
            return CLI_FAILURE;
        }

        u2nnValue = (UINT2) u4nnValue;

        u2nnValue = OSIX_HTONS (u2nnValue);

        if (UINT1_MAX_VALUE < au4IPValue[0] ||
            UINT1_MAX_VALUE < au4IPValue[1] ||
            UINT1_MAX_VALUE < au4IPValue[2] || UINT1_MAX_VALUE < au4IPValue[3])
        {
            return CLI_FAILURE;
        }

        MEMCPY (&pu1RdRt[2], &au4IPValue[0], sizeof (UINT1));
        MEMCPY (&pu1RdRt[3], &au4IPValue[1], sizeof (UINT1));
        MEMCPY (&pu1RdRt[4], &au4IPValue[2], sizeof (UINT1));
        MEMCPY (&pu1RdRt[5], &au4IPValue[3], sizeof (UINT1));
        MEMCPY (&pu1RdRt[6], &u2nnValue, sizeof (UINT2));
    }

    if (L3VPN_RT_TYPE_2 == pu1RdRt[0])
    {
        if (UINT2_MAX_VALUE < u4nnValue)
        {
            return CLI_FAILURE;
        }

        u2nnValue = (UINT2) u4nnValue;

        u4AsnValue = OSIX_HTONL (u4AsnValue);
        u2nnValue = OSIX_HTONS (u2nnValue);

        if (OSIX_TRUE == u1IsDotFound)
        {
            if (UINT2_MAX_VALUE < au4IPValue[0] ||
                UINT2_MAX_VALUE < au4IPValue[1])
            {
                return CLI_FAILURE;
            }

            MEMCPY (&u2AsnValue, &au4IPValue[0], sizeof (UINT2));
            u2AsnValue = OSIX_HTONS (u2AsnValue);
            MEMCPY (&pu1RdRt[2], &u2AsnValue, sizeof (UINT2));

            MEMCPY (&u2AsnValue, &au4IPValue[1], sizeof (UINT2));
            u2AsnValue = OSIX_HTONS (u2AsnValue);
            MEMCPY (&pu1RdRt[4], &u2AsnValue, sizeof (UINT2));
        }
        else
        {
            MEMCPY (&pu1RdRt[2], &u4AsnValue, sizeof (UINT4));
        }
        MEMCPY (&pu1RdRt[6], &u2nnValue, sizeof (UINT2));
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : L3vpnCliShowRunningConfig
 * * Description   : This routine displays the l3vpn configuration
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ******************************************************************************/
INT4
L3vpnCliShowRunningConfig (tCliHandle CliHandle)
{

    /* L3vpn Entity Configuration */
    L3vpnCliEntityShowRunningConfig (CliHandle);

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : L3vpnCliEntityShowRunningConfig
 * * Description   : This routine displays the l3vpn entity configuration
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ***************************************************************************** */
INT4
L3vpnCliEntityShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE MplsCliL3VpnVrfName;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnNextVrfName;
    tSNMP_OCTET_STRING_TYPE MplsCliL3VpnVrfRT;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRtDescription;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    INT4                L3VpnNotificationEnable = 0;
    UINT4               u4MplsL3VpnIllLblRcvThrsh = 0;
    UINT1               au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
    UINT1               au1MplsL3VpnNextVrfName[L3VPN_MAX_VRF_NAME_LEN];
    UINT1
                      au1MplsL3VpnVrfDescription[L3VPN_MAX_VRF_DESCRIPTION_LEN];
    UINT1               au1MplsL3VpnVrfRTDescription[L3VPN_MAX_DESC_LEN];
    UINT1               au1MplsL3VpnVrfRD[MPLS_MAX_L3VPN_RD_LEN];
    UINT1               au1MplsL3VpnVrfRT[L3VPN_MAX_RT_LEN];
    UINT2               u2Asn = 0;
    UINT4               u4Asn = 0;
    /*UINT4                     u4Asn2 = 0; */
    UINT4               u4AsignedNumberVal = 0;
    UINT2               u2AsignedNumberVal = 0;
    UINT4               u4MplsL3VpnVrfRTIndexVal = 0;
    INT4                i4MplsL3VpnVrfRTType = 0;
    UINT4               u4MplsNextL3VpnVrfRTIndex = 0;
    UINT4              *pu4NextMplsL3VpnVrfRTIndex = NULL;
    INT4               *pi4NextMplsL3VpnVrfRTType = NULL;
    UINT1              *pu1MplsL3VpnVrfDescription = NULL;
    INT4                i4NextMplsL3VpnVrfRTType = 0;
    UINT4               u4MplsL3VpnVrfRTIndex = 0;
    INT4                u1MplsL3VpnVrfRTType = 0;
    UINT2               u2AsnRt = 0;
    UINT4               u4AsnRt = 0;
    /*UINT4                     u4Asn2Rt = 0; */
    UINT4               u4AsignedRtNumberVal = 0;
    UINT2               u2AsignedRtNumberVal = 0;
    tSNMP_OCTET_STRING_TYPE MplsCliL3VpnVrfNameRt;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnNextVrfNameRt;

    UINT1               au1MplsCliL3VpnVrfNameRt[L3VPN_MAX_VRF_NAME_LEN];
    UINT1               au1MplsL3VpnNextVrfNameRt[L3VPN_MAX_VRF_NAME_LEN];
    /* RSVP-TE binding */
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;
    tL3VpnRsvpMapLabelEntry L3VpnRsvpMapLabelEntry;
    CHR1               *pc1String = NULL;

    /* Assign values */

    MplsCliL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;
    MplsL3VpnNextVrfName.pu1_OctetList = au1MplsL3VpnNextVrfName;
    MplsCliL3VpnVrfRT.pu1_OctetList = au1MplsL3VpnVrfRT;
    MplsL3VpnVrfRtDescription.pu1_OctetList = au1MplsL3VpnVrfRTDescription;
    MplsCliL3VpnVrfName.i4_Length = 0;
    MplsL3VpnNextVrfName.i4_Length = 0;
    MplsCliL3VpnVrfRT.i4_Length = 0;
    MplsL3VpnVrfRtDescription.i4_Length = 0;

    MplsCliL3VpnVrfNameRt.pu1_OctetList = au1MplsCliL3VpnVrfNameRt;
    MplsCliL3VpnVrfNameRt.i4_Length = 0;

    MplsL3VpnNextVrfNameRt.pu1_OctetList = au1MplsL3VpnNextVrfNameRt;
    MplsL3VpnNextVrfNameRt.i4_Length = 0;

    /* Memset the variables */
    MEMSET (MplsCliL3VpnVrfName.pu1_OctetList, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (MplsL3VpnNextVrfName.pu1_OctetList, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (au1MplsL3VpnVrfDescription, 0, L3VPN_MAX_VRF_DESCRIPTION_LEN);
    MEMSET (au1MplsL3VpnVrfRTDescription, 0, L3VPN_MAX_DESC_LEN);
    MEMSET (au1MplsL3VpnVrfRD, 0, MPLS_MAX_L3VPN_RD_LEN);
    MEMSET (au1MplsL3VpnVrfRT, 0, L3VPN_MAX_RT_LEN);
    MEMSET (au1MplsCliL3VpnVrfNameRt, 0, sizeof (au1MplsCliL3VpnVrfNameRt));
    MEMSET (au1MplsL3VpnNextVrfNameRt, 0, sizeof (au1MplsL3VpnNextVrfNameRt));
    MEMSET (&L3VpnRsvpMapLabelEntry, 0, sizeof (tL3VpnRsvpMapLabelEntry));

    /* Assign address */
    pu4NextMplsL3VpnVrfRTIndex = &u4MplsNextL3VpnVrfRTIndex;
    pi4NextMplsL3VpnVrfRTType = &i4NextMplsL3VpnVrfRTType;
    pu1MplsL3VpnVrfDescription = au1MplsL3VpnVrfDescription;

    /* To retrieve l3vpn trap status */
    if ((SNMP_SUCCESS ==
         nmhGetMplsL3VpnNotificationEnable (&L3VpnNotificationEnable))
        && (CLI_L3VPN_TRAP_ENABLE == L3VpnNotificationEnable))
    {
        CliPrintf (CliHandle, "\r\nmpls l3vpn trap enable\n");
    }

    /* To retrive label rcvd threshold */

    if ((SNMP_SUCCESS ==
         nmhGetMplsL3VpnIllLblRcvThrsh (&u4MplsL3VpnIllLblRcvThrsh))
        && (0 != u4MplsL3VpnIllLblRcvThrsh))
    {
        CliPrintf (CliHandle,
                   "\r\nmpls l3vpn illegal-label-rcvd-threshold %d\n",
                   u4MplsL3VpnIllLblRcvThrsh);

    }

    /* To retrieve vrf name */

    if (SNMP_SUCCESS ==
        nmhGetFirstIndexMplsL3VpnVrfTable (&MplsCliL3VpnVrfName))
    {
        do
        {
            /* get vrf entry from vrf name */

            pL3vpnMplsL3VpnVrfEntry =
                MplsL3VpnApiGetVrfTableEntry (MplsCliL3VpnVrfName.pu1_OctetList,
                                              MplsCliL3VpnVrfName.i4_Length);
            if (NULL != pL3vpnMplsL3VpnVrfEntry)
            {
                CliPrintf (CliHandle, "\r\n!\r\n");
                CliPrintf (CliHandle, "\nip vrf %s mpls\n",
                           L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry));

                /* To retrieve RD value */

                if (0 !=
                    MEMCMP (au1MplsL3VpnVrfRD,
                            L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry),
                            MPLS_MAX_L3VPN_RD_LEN))
                {
                    /* RD is of type  ASN:nn */

                    if ((L3VPN_RT_TYPE_0 ==
                         L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[0]))
                    {
                        MEMCPY (&u2Asn,
                                &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[2],
                                sizeof (UINT2));

                        MEMCPY (&u4AsignedNumberVal,
                                &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[4],
                                sizeof (UINT4));
                        u2Asn = OSIX_NTOHS (u2Asn);
                        u4AsignedNumberVal = OSIX_NTOHL (u4AsignedNumberVal);

                        CliPrintf (CliHandle, "\rrd %d:%d\n", u2Asn,
                                   u4AsignedNumberVal);

                    }

                    /* RD is of type  ip:nn */

                    else if (L3VPN_RT_TYPE_1 ==
                             L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[0])
                    {
                        MEMCPY (&u2AsignedNumberVal,
                                &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[6],
                                sizeof (UINT2));
                        u2AsignedNumberVal = OSIX_NTOHS (u2AsignedNumberVal);
                        CliPrintf (CliHandle, "\rrd %d.%d.%d.%d:%d\n",
                                   L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[2],
                                   L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[3],
                                   L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[4],
                                   L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[5],
                                   u2AsignedNumberVal);
                    }

                    /* RD is of type  ASN.ASN:nn */

                    else
                    {
                        MEMCPY (&u4Asn,
                                &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[2],
                                sizeof (UINT4));
                        MEMCPY (&u2AsignedNumberVal,
                                &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[6],
                                sizeof (UINT2));
                        u4Asn = OSIX_NTOHL (u4Asn);
                        u2AsignedNumberVal = OSIX_NTOHS (u2AsignedNumberVal);
                        CliPrintf (CliHandle, "\rrd %d.%d:%d\n",
                                   (u4Asn & 0xffff0000) >> 16,
                                   u4Asn & 0x0000ffff, u2AsignedNumberVal);
                    }

                }

                /* To retrive RT */

                if (SNMP_SUCCESS ==
                    nmhGetNextIndexMplsL3VpnVrfRTTable (&MplsCliL3VpnVrfNameRt,
                                                        &MplsL3VpnNextVrfNameRt,
                                                        u4MplsL3VpnVrfRTIndexVal,
                                                        pu4NextMplsL3VpnVrfRTIndex,
                                                        i4MplsL3VpnVrfRTType,
                                                        pi4NextMplsL3VpnVrfRTType))
                {
                    do
                    {
                        /* RT configure in vrf */
                        u4MplsL3VpnVrfRTIndex = *pu4NextMplsL3VpnVrfRTIndex;
                        u1MplsL3VpnVrfRTType = *pi4NextMplsL3VpnVrfRTType;

                        MplsCliL3VpnVrfNameRt.i4_Length =
                            MplsL3VpnNextVrfNameRt.i4_Length;
                        MEMCPY (MplsCliL3VpnVrfNameRt.pu1_OctetList,
                                MplsL3VpnNextVrfNameRt.pu1_OctetList,
                                MplsCliL3VpnVrfNameRt.i4_Length);

                        nmhGetMplsL3VpnVrfRT (&MplsL3VpnNextVrfNameRt,
                                              u4MplsL3VpnVrfRTIndex,
                                              u1MplsL3VpnVrfRTType,
                                              &MplsCliL3VpnVrfRT);

                        /* RT not configure on vrf  */
                        if (0 !=
                            MEMCMP (MplsCliL3VpnVrfName.pu1_OctetList,
                                    MplsCliL3VpnVrfNameRt.pu1_OctetList,
                                    L3VPN_MAX_VRF_NAME_LEN))
                        {
                            break;
                        }

                        /* Retrive RT direction */
                        if (1 == u1MplsL3VpnVrfRTType)
                        {
                            CliPrintf (CliHandle, "route-target import");
                        }
                        else if (2 == u1MplsL3VpnVrfRTType)
                        {
                            CliPrintf (CliHandle, "route-target export");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "route-target both");
                        }

                        /* Retrive RT name */
                        if (L3VPN_RT_TYPE_0 ==
                            MplsCliL3VpnVrfRT.pu1_OctetList[0])
                        {
                            MEMCPY (&u2AsnRt,
                                    &MplsCliL3VpnVrfRT.pu1_OctetList[2],
                                    sizeof (UINT2));
                            MEMCPY (&u4AsignedRtNumberVal,
                                    &MplsCliL3VpnVrfRT.pu1_OctetList[4],
                                    sizeof (UINT4));
                            u2AsnRt = OSIX_NTOHS (u2AsnRt);
                            u4AsignedRtNumberVal =
                                OSIX_NTOHL (u4AsignedRtNumberVal);

                            CliPrintf (CliHandle, " %d:%d", u2AsnRt,
                                       u4AsignedRtNumberVal);
                        }
                        else if (L3VPN_RT_TYPE_1 ==
                                 MplsCliL3VpnVrfRT.pu1_OctetList[0])
                        {
                            MEMCPY (&u2AsignedRtNumberVal,
                                    &MplsCliL3VpnVrfRT.pu1_OctetList[6],
                                    sizeof (UINT2));
                            u2AsignedRtNumberVal =
                                OSIX_NTOHS (u2AsignedRtNumberVal);
                            CliPrintf (CliHandle, " %d.%d.%d.%d:%d",
                                       MplsCliL3VpnVrfRT.pu1_OctetList[2],
                                       MplsCliL3VpnVrfRT.pu1_OctetList[3],
                                       MplsCliL3VpnVrfRT.pu1_OctetList[4],
                                       MplsCliL3VpnVrfRT.pu1_OctetList[5],
                                       u2AsignedRtNumberVal);
                        }
                        else
                        {
                            MEMCPY (&u4AsnRt,
                                    &MplsCliL3VpnVrfRT.pu1_OctetList[2],
                                    sizeof (UINT4));
                            MEMCPY (&u2AsignedRtNumberVal,
                                    &MplsCliL3VpnVrfRT.pu1_OctetList[6],
                                    sizeof (UINT2));
                            u4AsnRt = OSIX_NTOHL (u4AsnRt);
                            u2AsignedRtNumberVal =
                                OSIX_NTOHS (u2AsignedRtNumberVal);
                            CliPrintf (CliHandle, " %d.%d:%d",
                                       (u4AsnRt & 0xffff0000) >> 16,
                                       u4AsnRt & 0x0000ffff,
                                       u2AsignedRtNumberVal);
                        }

                        /* Retrive Rt Desc */
                        nmhGetMplsL3VpnVrfRTDescr (&MplsL3VpnNextVrfNameRt,
                                                   u4MplsL3VpnVrfRTIndex,
                                                   u1MplsL3VpnVrfRTType,
                                                   &MplsL3VpnVrfRtDescription);

                        if (0 !=
                            strlen ((char *) MplsL3VpnVrfRtDescription.
                                    pu1_OctetList))
                        {
                            CliPrintf (CliHandle, " rt-desc %s",
                                       MplsL3VpnVrfRtDescription.pu1_OctetList);
                        }
                        CliPrintf (CliHandle, "\n");

                    }
                    while (nmhGetNextIndexMplsL3VpnVrfRTTable
                           (&MplsCliL3VpnVrfNameRt, &MplsL3VpnNextVrfNameRt,
                            u4MplsL3VpnVrfRTIndex, pu4NextMplsL3VpnVrfRTIndex,
                            u1MplsL3VpnVrfRTType, pi4NextMplsL3VpnVrfRTType));
                }

/*nidhi*/

                /* To retrive vrf description */

                if (0 !=
                    MEMCMP (pu1MplsL3VpnVrfDescription,
                            L3VPN_P_VRF_DESCRIPTION (pL3vpnMplsL3VpnVrfEntry),
                            L3VPN_MAX_VRF_DESCRIPTION_LEN))
                {
                    CliPrintf (CliHandle, "\rvrf-description %s\n",
                               L3VPN_P_VRF_DESCRIPTION
                               (pL3vpnMplsL3VpnVrfEntry));
                }

                /* To retrive maximum routes */

                if (0 != L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry)
                    && L3VPN_PER_VRF_MAX_ROUTES !=
                    L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry))
                {
                    CliPrintf (CliHandle,
                               "\rmaximum routes %d high-route-threshold %d mid-route-threshold %d\n",
                               L3VPN_P_VRF_CONF_MAX_ROUTES
                               (pL3vpnMplsL3VpnVrfEntry),
                               L3VPN_P_VRF_HIGH_ROUTE_TH
                               (pL3vpnMplsL3VpnVrfEntry),
                               L3VPN_P_VRF_MID_ROUTE_TH
                               (pL3vpnMplsL3VpnVrfEntry));

                }

                CliPrintf (CliHandle, "\r\n");

            }                    /*end if ! NULL */

            L3vpnCliEntityShowRteRunningConfig (CliHandle,
                                                &MplsCliL3VpnVrfName);
            L3vpnCliEntityShowEgressRteRunningConfig (CliHandle,
                                                      &MplsCliL3VpnVrfName);
        }
        while (nmhGetNextIndexMplsL3VpnVrfTable
               (&MplsCliL3VpnVrfName, &MplsCliL3VpnVrfName) == SNMP_SUCCESS);
    }
    /* To Retreive MPLS L3VPN RSVP-TE Binding tunnel info */

    pL3VpnRsvpMapLabelEntry = L3vpnGetFirstRsvpMapLabelTable ();
    while (pL3VpnRsvpMapLabelEntry != NULL)
    {
        CliPrintf (CliHandle, "\r!\r\n");

        CliPrintf (CliHandle, "mpls l3vpn binding ipv4 %s",
                   (CLI_CONVERT_IPADDR_TO_STR
                    (pc1String,
                     L3VPN_P_RSVPMAPLABEL_PREFIX (pL3VpnRsvpMapLabelEntry))));
        CliPrintf (CliHandle, " %s rsvp-te %d\r\n",
                   (CLI_CONVERT_IPADDR_TO_STR
                    (pc1String,
                     L3VPN_P_RSVPMAPLABEL_MASK (pL3VpnRsvpMapLabelEntry))),
                   pL3VpnRsvpMapLabelEntry->u4TnlIndex);

        L3VPN_RSVPMAPLABEL_PREFIX (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_PREFIX (pL3VpnRsvpMapLabelEntry);
        L3VPN_RSVPMAPLABEL_MASK (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_MASK (pL3VpnRsvpMapLabelEntry);
        L3VPN_RSVPMAPLABEL_PREFIX_TYPE (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_PREFIX_TYPE (pL3VpnRsvpMapLabelEntry);
        L3VPN_RSVPMAPLABEL_MASK_TYPE (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_MASK_TYPE (pL3VpnRsvpMapLabelEntry);

        pL3VpnRsvpMapLabelEntry =
            L3vpnGetNextRsvpMapLabelTable (&L3VpnRsvpMapLabelEntry);

    }
    CliPrintf (CliHandle, "\r\n");
    UNUSED_PARAM (pc1String);
    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : L3vpnCliEntityShowRteRunningConfig
 * * Description   : This routine displays the l3vpn entity configuration of the Rte table
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ***************************************************************************** */

INT4
L3vpnCliEntityShowRteRunningConfig (tCliHandle CliHandle,
                                    tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName)
{
    tSNMP_OCTET_STRING_TYPE MplsCliL3VpnVrfName;

    UINT4               u4MplsL3VpnVrfConfRteMxThrshTime = 0;

    INT4                i4MplsL3VpnVrfRteInetCidrDestType = 0;
    INT4                i4NextMplsL3VpnVrfRteInetCidrDestType = 0;
    tSNMP_OCTET_STRING_TYPE tMplsL3VpnVrfRteInetCidrDest;
    tSNMP_OCTET_STRING_TYPE tNextMplsL3VpnVrfRteInetCidrDest;
    UINT4               u4MplsL3VpnVrfRteInetCidrPfxLen = 0;
    UINT4               u4NextMplsL3VpnVrfRteInetCidrPfxLen = 0;
    tSNMP_OID_TYPE      tMplsL3VpnVrfRteInetCidrPolicy;
    tSNMP_OID_TYPE      tNextMplsL3VpnVrfRteInetCidrPolicy;
    INT4                i4MplsL3VpnVrfRteInetCidrNHopType = 0;
    INT4                i4NextMplsL3VpnVrfRteInetCidrNHopType = 0;
    tSNMP_OCTET_STRING_TYPE tMplsL3VpnVrfRteInetCidrNextHop;
    tSNMP_OCTET_STRING_TYPE tNextMplsL3VpnVrfRteInetCidrNextHop;

    UINT1               au1MplsL3VpnVrfRteInetCidrDest[12];
    UINT1               au1NextMplsL3VpnVrfRteInetCidrDest[12];
    UINT1               au1MplsL3VpnVrfRteInetCidrNextHop[256];
    UINT1               au1NextMplsL3VpnVrfRteInetCidrNextHop[12];
    UINT1               au1RetValMplsL3VpnVrfRteXCPointer[12];
    UINT4               u4RetValMplsL3VpnVrfRteInetCidrNextHopAS = 0;
    INT4                i4RetValMplsL3VpnVrfRteInetCidrIfIndex = 0;
    INT4                i4RetValMplsL3VpnVrfRteInetCidrMetric1 = 0;
    tSNMP_OCTET_STRING_TYPE RetValMplsL3VpnVrfRteXCPointer;

    UINT1              *pu1RteXcPointer = NULL;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4XcIndex = 0;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4Label = 0;
    tLblEntry          *pLblEntry = NULL;

    tSNMP_OCTET_STRING_TYPE MplsCliL3VpnVrfNameRte;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnNextVrfNameRte;

    UINT1               au1MplsCliL3VpnVrfNameRte[L3VPN_MAX_VRF_NAME_LEN];
    UINT1               au1MplsL3VpnNextVrfNameRte[L3VPN_MAX_VRF_NAME_LEN];
    UINT1               au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
    /*  INT1               *pi1IfName = NULL;
       UINT4               u4L3VlanIf = 0;
       UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
       MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
       pi1IfName = (INT1 *) &au1IfName[0]; */

    /* Assign values */
    MplsCliL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;
    MplsCliL3VpnVrfNameRte.pu1_OctetList = au1MplsCliL3VpnVrfNameRte;
    MplsCliL3VpnVrfNameRte.i4_Length = 0;

    MplsL3VpnNextVrfNameRte.pu1_OctetList = au1MplsL3VpnNextVrfNameRte;
    MplsL3VpnNextVrfNameRte.i4_Length = 0;

    tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;
    tMplsL3VpnVrfRteInetCidrDest.i4_Length = 0;
    tNextMplsL3VpnVrfRteInetCidrDest.pu1_OctetList =
        au1NextMplsL3VpnVrfRteInetCidrDest;
    tNextMplsL3VpnVrfRteInetCidrDest.i4_Length = 0;
    tMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList =
        au1MplsL3VpnVrfRteInetCidrNextHop;
    tMplsL3VpnVrfRteInetCidrNextHop.i4_Length = 0;
    tNextMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList =
        au1NextMplsL3VpnVrfRteInetCidrNextHop;
    tNextMplsL3VpnVrfRteInetCidrNextHop.i4_Length = 0;
    RetValMplsL3VpnVrfRteXCPointer.pu1_OctetList =
        au1RetValMplsL3VpnVrfRteXCPointer;
    RetValMplsL3VpnVrfRteXCPointer.i4_Length = 0;
    /* Memset the variables */
    MEMSET (MplsCliL3VpnVrfName.pu1_OctetList, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (au1MplsCliL3VpnVrfNameRte, 0, sizeof (au1MplsCliL3VpnVrfNameRte));
    MEMSET (au1MplsL3VpnNextVrfNameRte, 0, sizeof (au1MplsL3VpnNextVrfNameRte));
/* Retriece Max Threshold Time */
    if (SNMP_SUCCESS ==
        nmhGetMplsL3VpnVrfConfRteMxThrshTime
        (&u4MplsL3VpnVrfConfRteMxThrshTime))
    {
        if (u4MplsL3VpnVrfConfRteMxThrshTime > 0)
        {
            CliPrintf (CliHandle, "\r\nmpls l3vpn max-threshold time %d\n",
                       u4MplsL3VpnVrfConfRteMxThrshTime);
        }
    }

    if (SNMP_SUCCESS ==
        nmhGetNextIndexMplsL3VpnVrfRteTable (&MplsCliL3VpnVrfNameRte,
                                             &MplsL3VpnNextVrfNameRte,
                                             i4MplsL3VpnVrfRteInetCidrDestType,
                                             &i4NextMplsL3VpnVrfRteInetCidrDestType,
                                             &tMplsL3VpnVrfRteInetCidrDest,
                                             &tNextMplsL3VpnVrfRteInetCidrDest,
                                             u4MplsL3VpnVrfRteInetCidrPfxLen,
                                             &u4NextMplsL3VpnVrfRteInetCidrPfxLen,
                                             &tMplsL3VpnVrfRteInetCidrPolicy,
                                             &tNextMplsL3VpnVrfRteInetCidrPolicy,
                                             i4MplsL3VpnVrfRteInetCidrNHopType,
                                             &i4NextMplsL3VpnVrfRteInetCidrNHopType,
                                             &tMplsL3VpnVrfRteInetCidrNextHop,
                                             &tNextMplsL3VpnVrfRteInetCidrNextHop))
    {
        do
        {
            i4MplsL3VpnVrfRteInetCidrDestType =
                i4NextMplsL3VpnVrfRteInetCidrDestType;

            tMplsL3VpnVrfRteInetCidrDest.i4_Length =
                tNextMplsL3VpnVrfRteInetCidrDest.i4_Length;
            MEMCPY (tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList,
                    tNextMplsL3VpnVrfRteInetCidrDest.pu1_OctetList,
                    tMplsL3VpnVrfRteInetCidrDest.i4_Length);

            u4MplsL3VpnVrfRteInetCidrPfxLen =
                u4NextMplsL3VpnVrfRteInetCidrPfxLen;
            i4MplsL3VpnVrfRteInetCidrNHopType =
                i4NextMplsL3VpnVrfRteInetCidrNHopType;

            MplsCliL3VpnVrfNameRte.i4_Length =
                MplsL3VpnNextVrfNameRte.i4_Length;
            MEMCPY (MplsCliL3VpnVrfNameRte.pu1_OctetList,
                    MplsL3VpnNextVrfNameRte.pu1_OctetList,
                    MplsCliL3VpnVrfNameRte.i4_Length);

            tMplsL3VpnVrfRteInetCidrNextHop.i4_Length =
                tNextMplsL3VpnVrfRteInetCidrNextHop.i4_Length;
            MEMCPY (tMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList,
                    tNextMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList,
                    tMplsL3VpnVrfRteInetCidrNextHop.i4_Length);

            if (0 !=
                MEMCMP (pMplsL3VpnVrfName->pu1_OctetList,
                        MplsCliL3VpnVrfNameRte.pu1_OctetList,
                        L3VPN_MAX_VRF_NAME_LEN))
            {
                continue;
            }

            if (nmhGetMplsL3VpnVrfRteInetCidrNextHopAS (&MplsCliL3VpnVrfNameRte,
                                                        i4MplsL3VpnVrfRteInetCidrDestType,
                                                        &tMplsL3VpnVrfRteInetCidrDest,
                                                        u4MplsL3VpnVrfRteInetCidrPfxLen,
                                                        &tMplsL3VpnVrfRteInetCidrPolicy,
                                                        i4MplsL3VpnVrfRteInetCidrNHopType,
                                                        &tMplsL3VpnVrfRteInetCidrNextHop,
                                                        &u4RetValMplsL3VpnVrfRteInetCidrNextHopAS)
                != SNMP_SUCCESS)
            {

                break;
            }
            if (nmhGetMplsL3VpnVrfRteInetCidrIfIndex (&MplsCliL3VpnVrfNameRte,
                                                      i4MplsL3VpnVrfRteInetCidrDestType,
                                                      &tMplsL3VpnVrfRteInetCidrDest,
                                                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                                                      &tMplsL3VpnVrfRteInetCidrPolicy,
                                                      i4MplsL3VpnVrfRteInetCidrNHopType,
                                                      &tMplsL3VpnVrfRteInetCidrNextHop,
                                                      &i4RetValMplsL3VpnVrfRteInetCidrIfIndex)
                != SNMP_SUCCESS)
            {
                break;
            }

            if (nmhGetMplsL3VpnVrfRteInetCidrMetric1 (&MplsCliL3VpnVrfNameRte,
                                                      i4MplsL3VpnVrfRteInetCidrDestType,
                                                      &tMplsL3VpnVrfRteInetCidrDest,
                                                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                                                      &tMplsL3VpnVrfRteInetCidrPolicy,
                                                      i4MplsL3VpnVrfRteInetCidrNHopType,
                                                      &tMplsL3VpnVrfRteInetCidrNextHop,
                                                      &i4RetValMplsL3VpnVrfRteInetCidrMetric1)
                != SNMP_SUCCESS)
            {
                break;
            }

            if (nmhGetMplsL3VpnVrfRteXCPointer (&MplsCliL3VpnVrfNameRte,
                                                i4MplsL3VpnVrfRteInetCidrDestType,
                                                &tMplsL3VpnVrfRteInetCidrDest,
                                                u4MplsL3VpnVrfRteInetCidrPfxLen,
                                                &tMplsL3VpnVrfRteInetCidrPolicy,
                                                i4MplsL3VpnVrfRteInetCidrNHopType,
                                                &tMplsL3VpnVrfRteInetCidrNextHop,
                                                &RetValMplsL3VpnVrfRteXCPointer)
                != SNMP_SUCCESS)
            {
                break;
            }

            pu1RteXcPointer = RetValMplsL3VpnVrfRteXCPointer.pu1_OctetList;
            MEMCPY (&u4XcIndex, pu1RteXcPointer, sizeof (UINT4));
            u4XcIndex = OSIX_NTOHL (u4XcIndex);

            pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
            MEMCPY (&u4InIndex, pu1RteXcPointer, sizeof (UINT4));
            u4InIndex = OSIX_NTOHL (u4InIndex);

            pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
            MEMCPY (&u4OutIndex, pu1RteXcPointer, sizeof (UINT4));
            u4OutIndex = OSIX_NTOHL (u4OutIndex);

            pXcEntry = MplsGetXCTableEntry (u4XcIndex, u4InIndex, u4OutIndex);
            if (pXcEntry != NULL)
            {
                if (pXcEntry->mplsLabelIndex != NULL)
                {
                    pLblEntry = (tLblEntry *)
                        TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));

                    if (pLblEntry != NULL)
                    {
                        u4Label = pLblEntry->u4Label;
                    }
                }
            }
            CliPrintf (CliHandle,
                       "mpls static l3vpn add route destination %d.%d.%d.%d "
                       "prefix-len %d next-hop %d.%d.%d.%d vpn-label %d",
                       tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList[0],
                       tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList[1],
                       tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList[2],
                       tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList[3],
                       u4MplsL3VpnVrfRteInetCidrPfxLen,
                       tMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList[0],
                       tMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList[1],
                       tMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList[2],
                       tMplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList[3],
                       u4Label);

            CliPrintf (CliHandle, "\n");

        }
        while (nmhGetNextIndexMplsL3VpnVrfRteTable (&MplsCliL3VpnVrfNameRte,
                                                    &MplsL3VpnNextVrfNameRte,
                                                    i4MplsL3VpnVrfRteInetCidrDestType,
                                                    &i4NextMplsL3VpnVrfRteInetCidrDestType,
                                                    &tMplsL3VpnVrfRteInetCidrDest,
                                                    &tNextMplsL3VpnVrfRteInetCidrDest,
                                                    u4MplsL3VpnVrfRteInetCidrPfxLen,
                                                    &u4NextMplsL3VpnVrfRteInetCidrPfxLen,
                                                    &tMplsL3VpnVrfRteInetCidrPolicy,
                                                    &tNextMplsL3VpnVrfRteInetCidrPolicy,
                                                    i4MplsL3VpnVrfRteInetCidrNHopType,
                                                    &i4NextMplsL3VpnVrfRteInetCidrNHopType,
                                                    &tMplsL3VpnVrfRteInetCidrNextHop,
                                                    &tNextMplsL3VpnVrfRteInetCidrNextHop));
    }
    MEMSET (MplsCliL3VpnVrfNameRte.pu1_OctetList, 0, L3VPN_MAX_VRF_NAME_LEN);
    MplsCliL3VpnVrfNameRte.i4_Length = 0;
    i4MplsL3VpnVrfRteInetCidrDestType = 0;
    MEMSET (tMplsL3VpnVrfRteInetCidrDest.pu1_OctetList, 0,
            L3VPN_MAX_VRF_NAME_LEN);
    tMplsL3VpnVrfRteInetCidrDest.i4_Length = 0;
    u4MplsL3VpnVrfRteInetCidrPfxLen = 0;
    i4MplsL3VpnVrfRteInetCidrNHopType = 0;
    tMplsL3VpnVrfRteInetCidrNextHop.i4_Length = 0;
    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : L3vpnCliEntityShowEgressRteRunningConfig
 * * Description   : This routine displays the l3vpn entity configuration of egress route
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ***************************************************************************** */

INT4
L3vpnCliEntityShowEgressRteRunningConfig (tCliHandle CliHandle,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsL3VpnVrfName)
{
    tSNMP_OCTET_STRING_TYPE MplsCliL3VpnVrfName;

    UINT4               u4FsMplsL3VpnVrfEgressRtePathAttrLabel = 0;
    UINT4               u4NextFsMplsL3VpnVrfEgressRtePathAttrLabel = 0;

    tSNMP_OCTET_STRING_TYPE RetValFsMplsL3VpnVrfName;
    UINT1               au1RetValFsMplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
    UINT1               au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];
    /* Assign values */
    MplsCliL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;
    RetValFsMplsL3VpnVrfName.pu1_OctetList = au1RetValFsMplsL3VpnVrfName;
    RetValFsMplsL3VpnVrfName.i4_Length = 0;

    /* Memset the variables */
    MEMSET (au1RetValFsMplsL3VpnVrfName, 0,
            sizeof (au1RetValFsMplsL3VpnVrfName));
    MEMSET (MplsCliL3VpnVrfName.pu1_OctetList, 0, L3VPN_MAX_VRF_NAME_LEN);

    if (nmhGetNextIndexFsMplsL3VpnVrfEgressRteTable
        (u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
         &u4NextFsMplsL3VpnVrfEgressRtePathAttrLabel) == SNMP_SUCCESS)
    {
        do
        {
            u4FsMplsL3VpnVrfEgressRtePathAttrLabel =
                u4NextFsMplsL3VpnVrfEgressRtePathAttrLabel;

            if (nmhGetFsMplsL3VpnVrfName
                (u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                 &RetValFsMplsL3VpnVrfName) != SNMP_SUCCESS)
            {
                break;
            }
            if (0 !=
                MEMCMP (pMplsL3VpnVrfName->pu1_OctetList,
                        RetValFsMplsL3VpnVrfName.pu1_OctetList,
                        L3VPN_MAX_VRF_NAME_LEN))
            {
                continue;
            }
            CliPrintf (CliHandle,
                       "mpls static l3vpn egress-route add per-vrf label %d",
                       u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
            CliPrintf (CliHandle, "\n");
        }
        while (nmhGetNextIndexFsMplsL3VpnVrfEgressRteTable
               (u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                &u4NextFsMplsL3VpnVrfEgressRtePathAttrLabel));
    }
    u4FsMplsL3VpnVrfEgressRtePathAttrLabel = 0;

    return CLI_SUCCESS;
}

INT4
MplsL3VpnCreateCrossConnectEntry (tL3vpnMplsL3VpnVrfRteEntry *
                                  pL3vpnSetMplsL3VpnVrfRteEntry, UINT4 u4Label)
{
    UINT4               u4OutSegmentIndex = 0;
    UINT4               u4L3VpnXcIndex = 0;
    UINT4               u4L3VpnOutSegmentIndex = 0;
    UINT1              *pu1RteXcEntry = NULL;
    UINT4               u4L3VpnInSegmentIndex = L3VPN_ZERO;
    UINT4               u4NextHop = 0;

    if (pL3vpnSetMplsL3VpnVrfRteEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "Route Info is NULL.\r\n"));
        return OSIX_FAILURE;
    }

    MEMCPY (&u4NextHop,
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrDestLen);
    u4NextHop = OSIX_NTOHL (u4NextHop);
    /* Find the Non-Te LSP for the Next-hop */
    if (MplsGetNonTeLspOutSegmentFromPrefix
        (u4NextHop, &u4OutSegmentIndex) == MPLS_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "LSP not found for the next hop %d.%d.%d.%d.\r\n",
                    u4NextHop));

        return OSIX_FAILURE;
    }

    if (MplsCreateXcEntryForL3Vpn
        (u4OutSegmentIndex, u4Label, &u4L3VpnXcIndex,
         &u4L3VpnOutSegmentIndex) == MPLS_FAILURE)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "Failed to create the XC Entry for L3VPN.\r\n"));

        return OSIX_FAILURE;
    }

    pu1RteXcEntry = L3VPN_P_RTE_XC_POINTER (pL3vpnSetMplsL3VpnVrfRteEntry);

    u4L3VpnXcIndex = OSIX_HTONL (u4L3VpnXcIndex);
    MEMCPY (pu1RteXcEntry, (UINT1 *) &u4L3VpnXcIndex, sizeof (UINT4));
    pu1RteXcEntry = pu1RteXcEntry + sizeof (UINT4);

    u4L3VpnInSegmentIndex = OSIX_HTONL (u4L3VpnInSegmentIndex);
    MEMCPY (pu1RteXcEntry, (UINT1 *) &u4L3VpnInSegmentIndex, sizeof (UINT4));
    pu1RteXcEntry = pu1RteXcEntry + sizeof (UINT4);

    u4L3VpnOutSegmentIndex = OSIX_HTONL (u4L3VpnOutSegmentIndex);
    MEMCPY (pu1RteXcEntry, (UINT1 *) &u4L3VpnOutSegmentIndex, sizeof (UINT4));
    L3VPN_P_RTE_XC_POINTER_LEN (pL3vpnSetMplsL3VpnVrfRteEntry) =
        3 * (sizeof (UINT4));

    return OSIX_SUCCESS;
}

#endif

#endif
