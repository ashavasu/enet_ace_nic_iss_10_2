/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnclig.c,v 1.3 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

#ifndef __L3VPNCLIG_C__
#define __L3VPNCLIG_C__
/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
*Id:   l3vpnclig.c
*
* Description: This file contains the L3vpn CLI related routines 
*********************************************************************/

#include "l3vpninc.h"
#include "l3vpncli.h"

tL3vpnMplsL3VpnVrfEntry gL3vpnSetMplsL3VpnVrfEntry;
tL3vpnMplsL3VpnIfConfEntry gL3vpnSetMplsL3VpnIfConfEntry;
tL3vpnMplsL3VpnVrfRTEntry gL3vpnSetMplsL3VpnVrfRTEntry;
tL3vpnMplsL3VpnVrfRteEntry gL3vpnSetMplsL3VpnVrfRteEntry;

/****************************************************************************
 * Function    :  cli_process_L3vpn_cmd
 * Description :  This function is exported to CLI module to handle the
                L3VPN cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_L3vpn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[L3VPN_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;

    tL3vpnIsSetMplsL3VpnVrfEntry L3vpnIsSetMplsL3VpnVrfEntry;

    tL3vpnIsSetMplsL3VpnIfConfEntry L3vpnIsSetMplsL3VpnIfConfEntry;

    tL3vpnIsSetMplsL3VpnVrfRTEntry L3vpnIsSetMplsL3VpnVrfRTEntry;

    tL3vpnIsSetMplsL3VpnVrfRteEntry L3vpnIsSetMplsL3VpnVrfRteEntry;

    UNUSED_PARAM (u4CmdType);
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, L3vpnMainTaskLock, L3vpnMainTaskUnLock);
#endif
    L3VPN_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    UNUSED_PARAM (u4IfIndex);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == L3VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_L3VPN_MPLSL3VPNVRFTABLE:
            MEMSET (&gL3vpnSetMplsL3VpnVrfEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfEntry));
            MEMSET (&L3vpnIsSetMplsL3VpnVrfEntry, 0,
                    sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

            L3VPN_FILL_MPLSL3VPNVRFTABLE_ARGS ((&gL3vpnSetMplsL3VpnVrfEntry),
                                               (&L3vpnIsSetMplsL3VpnVrfEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4], args[5],
                                               args[6], args[7], args[8],
                                               args[9], args[10], args[11],
                                               args[12], args[13]);

            i4RetStatus =
                L3vpnCliSetMplsL3VpnVrfTable (CliHandle,
                                              (&gL3vpnSetMplsL3VpnVrfEntry),
                                              (&L3vpnIsSetMplsL3VpnVrfEntry));
            break;

        case CLI_L3VPN_MPLSL3VPNIFCONFTABLE:
            MEMSET (&gL3vpnSetMplsL3VpnIfConfEntry, 0,
                    sizeof (tL3vpnMplsL3VpnIfConfEntry));
            MEMSET (&L3vpnIsSetMplsL3VpnIfConfEntry, 0,
                    sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

            L3VPN_FILL_MPLSL3VPNIFCONFTABLE_ARGS ((&gL3vpnSetMplsL3VpnIfConfEntry), (&L3vpnIsSetMplsL3VpnIfConfEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);

            i4RetStatus =
                L3vpnCliSetMplsL3VpnIfConfTable (CliHandle,
                                                 (&gL3vpnSetMplsL3VpnIfConfEntry),
                                                 (&L3vpnIsSetMplsL3VpnIfConfEntry));
            break;

        case CLI_L3VPN_MPLSL3VPNVRFRTTABLE:
            MEMSET (&gL3vpnSetMplsL3VpnVrfRTEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfRTEntry));
            MEMSET (&L3vpnIsSetMplsL3VpnVrfRTEntry, 0,
                    sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

            L3VPN_FILL_MPLSL3VPNVRFRTTABLE_ARGS ((&gL3vpnSetMplsL3VpnVrfRTEntry), (&L3vpnIsSetMplsL3VpnVrfRTEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);

            i4RetStatus =
                L3vpnCliSetMplsL3VpnVrfRTTable (CliHandle,
                                                (&gL3vpnSetMplsL3VpnVrfRTEntry),
                                                (&L3vpnIsSetMplsL3VpnVrfRTEntry));
            break;

        case CLI_L3VPN_MPLSL3VPNVRFRTETABLE:
            MEMSET (&gL3vpnSetMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfRteEntry));
            MEMSET (&L3vpnIsSetMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

            L3VPN_FILL_MPLSL3VPNVRFRTETABLE_ARGS ((&gL3vpnSetMplsL3VpnVrfRteEntry), (&L3vpnIsSetMplsL3VpnVrfRteEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21]);

            i4RetStatus =
                L3vpnCliSetMplsL3VpnVrfRteTable (CliHandle,
                                                 (&gL3vpnSetMplsL3VpnVrfRteEntry),
                                                 (&L3vpnIsSetMplsL3VpnVrfRteEntry));
            break;

        case CLI_L3VPN_MPLSL3VPNNOTIFICATIONENABLE:

            i4RetStatus =
                L3vpnCliSetMplsL3VpnNotificationEnable (CliHandle, args[0]);
            break;

        case CLI_L3VPN_MPLSL3VPNILLLBLRCVTHRSH:

            i4RetStatus =
                L3vpnCliSetMplsL3VpnIllLblRcvThrsh (CliHandle, args[0]);

        case CLI_L3VPN_MPLSL3VPNVRFCONFRTEMXTHRSHTIME:

            i4RetStatus =
                L3vpnCliSetMplsL3VpnVrfConfRteMxThrshTime (CliHandle, args[0]);
            break;

        case CLI_L3VPN_MPLSL3VPN_RSVP_TUNNEL_BINDING:

            i4RetStatus =
                L3vpnCliSetMplsL3VpnRsvpTeBinding (CliHandle,
                                                   (*(UINT4 *) args[0]),
                                                   (*(UINT4 *) args[1]),
                                                   (*(UINT4 *) args[2]));
            break;

        case CLI_L3VPN_MPLSL3VPN_NO_RSVP_TUNNEL_BINDING:

            i4RetStatus =
                L3vpnCliSetMplsL3VpnNoRsvpTeBinding (CliHandle,
                                                     (*(UINT4 *) args[0]),
                                                     (*(UINT4 *) args[1]),
                                                     (*(UINT4 *) args[2]));

            break;

        default:
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_L3VPN)
            && (u4ErrCode < CLI_L3VPN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       L3vpnCliErrString[CLI_ERR_OFFSET_L3VPN (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif

    L3VPN_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnVrfTable
* Description :
* Input       :  CliHandle 
*            pL3vpnSetMplsL3VpnVrfEntry
*            pL3vpnIsSetMplsL3VpnVrfEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnVrfTable (tCliHandle CliHandle,
                              tL3vpnMplsL3VpnVrfEntry *
                              pL3vpnSetMplsL3VpnVrfEntry,
                              tL3vpnIsSetMplsL3VpnVrfEntry *
                              pL3vpnIsSetMplsL3VpnVrfEntry)
{
    UINT4               u4ErrorCode;

    if (L3vpnTestAllMplsL3VpnVrfTable (&u4ErrorCode, pL3vpnSetMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_TRUE,
                                       OSIX_TRUE) != OSIX_SUCCESS)
    {
        if (u4ErrorCode == SNMP_ERR_RESOURCE_UNAVAILABLE)
        {
            CliPrintf (CliHandle, "%% L3VPN is not licensed\n");
        }
        return CLI_FAILURE;
    }

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnSetMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnIfConfTable
* Description :
* Input       :  CliHandle 
*            pL3vpnSetMplsL3VpnIfConfEntry
*            pL3vpnIsSetMplsL3VpnIfConfEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnIfConfTable (tCliHandle CliHandle,
                                 tL3vpnMplsL3VpnIfConfEntry *
                                 pL3vpnSetMplsL3VpnIfConfEntry,
                                 tL3vpnIsSetMplsL3VpnIfConfEntry *
                                 pL3vpnIsSetMplsL3VpnIfConfEntry)
{
    UINT4               u4ErrorCode;

    if (L3vpnTestAllMplsL3VpnIfConfTable
        (&u4ErrorCode, pL3vpnSetMplsL3VpnIfConfEntry,
         pL3vpnIsSetMplsL3VpnIfConfEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L3vpnSetAllMplsL3VpnIfConfTable
        (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnVrfRTTable
* Description :
* Input       :  CliHandle 
*            pL3vpnSetMplsL3VpnVrfRTEntry
*            pL3vpnIsSetMplsL3VpnVrfRTEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnVrfRTTable (tCliHandle CliHandle,
                                tL3vpnMplsL3VpnVrfRTEntry *
                                pL3vpnSetMplsL3VpnVrfRTEntry,
                                tL3vpnIsSetMplsL3VpnVrfRTEntry *
                                pL3vpnIsSetMplsL3VpnVrfRTEntry)
{
    UINT4               u4ErrorCode;

    if (L3vpnTestAllMplsL3VpnVrfRTTable
        (&u4ErrorCode, pL3vpnSetMplsL3VpnVrfRTEntry,
         pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L3vpnSetAllMplsL3VpnVrfRTTable
        (pL3vpnSetMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnVrfRteTable
* Description :
* Input       :  CliHandle 
*            pL3vpnSetMplsL3VpnVrfRteEntry
*            pL3vpnIsSetMplsL3VpnVrfRteEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnVrfRteTable (tCliHandle CliHandle,
                                 tL3vpnMplsL3VpnVrfRteEntry *
                                 pL3vpnSetMplsL3VpnVrfRteEntry,
                                 tL3vpnIsSetMplsL3VpnVrfRteEntry *
                                 pL3vpnIsSetMplsL3VpnVrfRteEntry)
{
    UINT4               u4ErrorCode;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (&u4ErrorCode, pL3vpnSetMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnNotificationEnable
* Description :
* Input       :  CliHandle 
*            pL3vpnSetScalar
*            pL3vpnIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnNotificationEnable (tCliHandle CliHandle,
                                        UINT4 *pMplsL3VpnNotificationEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4MplsL3VpnNotificationEnable = 0;

    L3VPN_FILL_MPLSL3VPNNOTIFICATIONENABLE (i4MplsL3VpnNotificationEnable,
                                            pMplsL3VpnNotificationEnable);

    if (L3vpnTestMplsL3VpnNotificationEnable
        (&u4ErrorCode, i4MplsL3VpnNotificationEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L3vpnSetMplsL3VpnNotificationEnable (i4MplsL3VpnNotificationEnable) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnVrfConfRteMxThrshTime
* Description :
* Input       :  CliHandle
*            pL3vpnSetScalar
*            pL3vpnIsSetScalar
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnVrfConfRteMxThrshTime (tCliHandle CliHandle,
                                           UINT4
                                           *pMplsL3VpnVrfConfRteMxThrshTime)
{
    UINT4               u4ErrorCode;
    UINT4               u4MplsL3VpnVrfConfRteMxThrshTime = 0;

    L3VPN_FILL_MPLSL3VPNVRFCONFRTEMXTHRSHTIME (u4MplsL3VpnVrfConfRteMxThrshTime,
                                               pMplsL3VpnVrfConfRteMxThrshTime);

    if (L3vpnTestMplsL3VpnVrfConfRteMxThrshTime
        (&u4ErrorCode, u4MplsL3VpnVrfConfRteMxThrshTime) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L3vpnSetMplsL3VpnVrfConfRteMxThrshTime
        (u4MplsL3VpnVrfConfRteMxThrshTime) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnIllLblRcvThrsh
* Description :
* Input       :  CliHandle 
*            pL3vpnSetScalar
*            pL3vpnIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnIllLblRcvThrsh (tCliHandle CliHandle,
                                    UINT4 *pMplsL3VpnIllLblRcvThrsh)
{
    UINT4               u4ErrorCode;
    UINT4               u4MplsL3VpnIllLblRcvThrsh = 0;

    L3VPN_FILL_MPLSL3VPNILLLBLRCVTHRSH (u4MplsL3VpnIllLblRcvThrsh,
                                        pMplsL3VpnIllLblRcvThrsh);

    if (L3vpnTestMplsL3VpnIllLblRcvThrsh
        (&u4ErrorCode, u4MplsL3VpnIllLblRcvThrsh) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (L3vpnSetMplsL3VpnIllLblRcvThrsh (u4MplsL3VpnIllLblRcvThrsh) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* RSVP-TE MPLS-L3VPN-TE */
/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnRsvpTeBinding
* Description :  This Function is associating the binding for corresponding MplsTunnel 
* Input       :  CliHandle 
*                u4Prefix    - ip address prefix
*                u4Mask      - mask
*                u4TunnelId  - u4TunnelId
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnRsvpTeBinding (tCliHandle CliHandle,
                                   UINT4 u4Prefix, UINT4 u4Mask,
                                   UINT4 u4TunnelIndex)
{
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    static UINT1        au1DestAddrMin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestAddrMax[MPLS_INDEX_LENGTH];
    UINT4               u4MinAddr = u4Prefix;
    UINT4               u4MaxAddr = u4Mask;
    UINT4               u4ErrorCode = MPLS_ZERO;
    INT4                i4PrevTunnelIndex = (INT4) u4TunnelIndex;    /*FC -17 warning */
    INT4                i4PrefixType = IPV4;
    INT4                i4MaskType = IPV4;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;

    MPLS_INTEGER_TO_OCTETSTRING (u4MinAddr, (&DestAddrMin));
    MPLS_INTEGER_TO_OCTETSTRING (u4MaxAddr, (&DestAddrMax));

    /* Test MplsL3VpnTeMapRowStatus to input prefix, Is binding entry exists or not */
    pL3VpnRsvpMapLabelEntry =
        L3vpnGetRsvpMapLabelEntry ((UINT1) i4PrefixType, u4Prefix,
                                   (UINT1) i4MaskType, u4Mask);

    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        /* Validate the input values is valid or not  */
        if ((nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus
             (&u4ErrorCode, i4PrefixType, &DestAddrMin, i4MaskType,
              &DestAddrMax, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            return CLI_FAILURE;
        }

        /* create the row for current prefix  */
        if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
             (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
              MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /* Validate the input value of tunnelId is valid or not  */
        if ((nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex
             (&u4ErrorCode, i4PrefixType, &DestAddrMin, i4MaskType,
              &DestAddrMax, (INT4) u4TunnelIndex)) == SNMP_FAILURE)
        {
            if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
                 (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
                  MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to add MplsL3Vpn binding Entry    \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            return CLI_FAILURE;
        }

        /* associate the tunnel to current prefix rowentry */
        if ((nmhSetFsMplsL3VpnRsvpTeMapTnlIndex
             (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
              (INT4) u4TunnelIndex)) == SNMP_FAILURE)
        {
            if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
                 (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
                  MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to add MplsL3Vpn binding Entry    \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if ((nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus
             (&u4ErrorCode, i4PrefixType, &DestAddrMin, i4MaskType,
              &DestAddrMax, MPLS_STATUS_NOT_INSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to add Mpls l3Vpn binding exist\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
             (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
              MPLS_STATUS_NOT_INSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if ((nmhGetFsMplsL3VpnRsvpTeMapTnlIndex
             (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
              &i4PrevTunnelIndex)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding for new tunnel Entry    \r\n");
            return CLI_FAILURE;
        }

        if ((nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex
             (&u4ErrorCode, i4PrefixType, &DestAddrMin, i4MaskType,
              &DestAddrMax, (INT4) u4TunnelIndex)) == SNMP_FAILURE)
        {
            if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
                 (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
                  MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to add MplsL3Vpn binding Entry    \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetFsMplsL3VpnRsvpTeMapTnlIndex
             (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
              (INT4) u4TunnelIndex)) == SNMP_FAILURE)
        {
            if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
                 (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
                  MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to add MplsL3Vpn binding Entry    \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle,
                       "%% Unable to add MplsL3Vpn binding Entry    \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    /* validate the current prefix rowstatus to activate  */
    if ((nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus
         (&u4ErrorCode, i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
          MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
    {
        if (i4PrevTunnelIndex != (INT4) u4TunnelIndex)
        {
            if ((nmhSetFsMplsL3VpnRsvpTeMapTnlIndex (i4PrefixType, &DestAddrMin,
                                                     i4MaskType, &DestAddrMax,
                                                     i4PrevTunnelIndex)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to add MplsL3Vpn binding Entry    \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
                 (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
                  MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to add MplsL3Vpn binding Entry    \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        CliPrintf (CliHandle, "%% Unable to add MplsL3Vpn binding Entry    \r\n");
        return CLI_FAILURE;
    }
    /* activate the row for current prefix  */
    if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus
         (i4PrefixType, &DestAddrMin, i4MaskType, &DestAddrMax,
          MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "%% Unable to add MplsL3Vpn binding Entry\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  L3vpnCliSetMplsL3VpnNoRsvpTeBinding
* Description :   This Function is deassociating the binding for corresponding MplsTunnel
* Input       :  CliHandle 
*                u4Prefix    - ip address prefix
*                u4Mask      - mask
*                u4TunnelId  - u4TunnelId
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnCliSetMplsL3VpnNoRsvpTeBinding (tCliHandle CliHandle,
                                     UINT4 u4Prefix, UINT4 u4Mask,
                                     UINT4 u4TunnelIndex)
{
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    static UINT1        au1DestAddrMin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestAddrMax[MPLS_INDEX_LENGTH];
    UINT4               u4ErrorCode = MPLS_ZERO;
    INT4                i4PrefixType = IPV4;
    INT4                i4MaskType = IPV4;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;

    if (u4TunnelIndex != MPLS_ZERO)
    {
        pL3VpnRsvpMapLabelEntry =
            L3VpnFetchRsvpMapLabelTableFromTnlIndex (u4TunnelIndex);
    }
    else
    {
        pL3VpnRsvpMapLabelEntry =
            L3vpnGetRsvpMapLabelEntry ((UINT1) i4PrefixType, u4Prefix,
                                       (UINT1) i4MaskType, u4Mask);
    }
    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% No binding Entry is exists for current prefix/Tunnel\n");
        return CLI_FAILURE;
    }
    else
    {
        if (u4TunnelIndex != MPLS_ZERO)
        {
            u4Prefix = pL3VpnRsvpMapLabelEntry->u4Prefix;
            u4Mask = pL3VpnRsvpMapLabelEntry->u4Mask;
            i4PrefixType = (INT4) pL3VpnRsvpMapLabelEntry->u1PrefixType;
            i4MaskType = (INT4) pL3VpnRsvpMapLabelEntry->u1MaskType;
        }

        MPLS_INTEGER_TO_OCTETSTRING (u4Prefix, (&DestAddrMin));
        MPLS_INTEGER_TO_OCTETSTRING (u4Mask, (&DestAddrMax));
        /* Test MplsL3VpnTeMapRowStatus to input prefix, Is binding entry exists or not */
        if ((nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus
             (&u4ErrorCode, i4PrefixType, &DestAddrMin, i4MaskType,
              &DestAddrMax, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to delete MplsL3Vpn binding Entry\r\n");
            return CLI_FAILURE;
        }

        /* delete the binding entry to corresponding prefix and destroy the row */
        if ((nmhSetFsMplsL3VpnRsvpTeMapRowStatus (i4PrefixType, &DestAddrMin,
                                                  i4MaskType, &DestAddrMax,
                                                  MPLS_STATUS_DESTROY)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to delete MplsL3Vpn binding Entry\r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

#endif
