/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntmr.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description : This file contains procedures containing L3vpn Timer
 *               related operations
 *****************************************************************************/
#include "l3vpninc.h"

PRIVATE tL3vpnTmrDesc gaL3vpnTmrDesc[L3VPN_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : L3vpnTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize L3vpn Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
L3vpnTmrInitTmrDesc ()
{

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnTmrInitTmrDesc\n"));

/*
    gaL3vpnTmrDesc[L3VPN_PMR_TMR].pTmrExpFunc = L3vpnTmrPmr;
    gaL3vpnTmrDesc[L3VPN_PMR_TMR].i2Offset =
        (INT2) (L3VPN_GET_OFFSET (tL3vpnPmr, pmrTmr));
*/

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:EXIT L3vpnTmrInitTmrDesc\n"));

}

/****************************************************************************
*                                                                           *
* Function     : L3vpnTmrStartTmr                                            *
*                                                                           *
* Description  : Starts L3vpn Timer                                          *
*                                                                           *
* Input        : pL3vpnTmr - pointer to L3vpnTmr structure                    *
*                eL3vpnTmrId - L3VPN timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
L3vpnTmrStartTmr (tL3vpnTmr * pL3vpnTmr, enL3vpnTmrId eL3vpnTmrId, UINT4 u4Secs)
{

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnTmrStartTmr\n"));

    pL3vpnTmr->eL3vpnTmrId = eL3vpnTmrId;

    if (TmrStartTimer (gL3vpnGlobals.l3vpnTmrLst, &(pL3vpnTmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "Tmr Start Failure\n"));
    }

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC: Exit L3vpnTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : L3vpnTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts L3vpn Timer                                       *
*                                                                           *
* Input        : pL3vpnTmr - pointer to L3vpnTmr structure                    *
*                eL3vpnTmrId - L3VPN timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
L3vpnTmrRestartTmr (tL3vpnTmr * pL3vpnTmr, enL3vpnTmrId eL3vpnTmrId, UINT4 u4Secs)
{

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnTmrRestartTmr\n"));

    TmrStopTimer (gL3vpnGlobals.l3vpnTmrLst, &(pL3vpnTmr->tmrNode));
    L3vpnTmrStartTmr (pL3vpnTmr, eL3vpnTmrId, u4Secs);

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC: Exit L3vpnTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : L3vpnTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts L3vpn Timer                                        *
*                                                                           *
* Input        : pL3vpnTmr - pointer to L3vpnTmr structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
L3vpnTmrStopTmr (tL3vpnTmr * pL3vpnTmr)
{

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnTmrStopTmr\n"));

    TmrStopTimer (gL3vpnGlobals.l3vpnTmrLst, &(pL3vpnTmr->tmrNode));

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC: Exit L3vpnTmrStopTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : L3vpnTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
L3vpnTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enL3vpnTmrId         eL3vpnTmrId = L3VPN_TMR1; 
    INT2                i2Offset = 0;

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC: L3vpnTmrHandleExpiry\n"));
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gL3vpnGlobals.l3vpnTmrLst))
           != NULL)
    {

        eL3vpnTmrId = ((tL3vpnTmr *) pExpiredTimers)->eL3vpnTmrId;
        i2Offset = gaL3vpnTmrDesc[eL3vpnTmrId].i2Offset;

        if (i2Offset < 0)
        {

            /* The timer function does not take any parameter */

            (*(gaL3vpnTmrDesc[eL3vpnTmrId].pTmrExpFunc)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gaL3vpnTmrDesc[eL3vpnTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC: Exit L3vpnTmrHandleExpiry\n"));
    return;

}


/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpntmr.c                      */
/*-----------------------------------------------------------------------*/
