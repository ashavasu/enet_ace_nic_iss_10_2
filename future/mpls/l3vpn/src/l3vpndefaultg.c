/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndefaultg.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpndefaultg.c 
*
* Description: This file contains the routines to initialize the
*              mib objects for the module L3vpn 
*********************************************************************/

#include "l3vpninc.h"

/****************************************************************************
* Function    : L3vpnInitializeMibMplsL3VpnVrfTable
* Input       : pL3vpnMplsL3VpnVrfEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
L3vpnInitializeMibMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                                     pL3vpnMplsL3VpnVrfEntry)
{

    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh = 0;

    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh = 0;

    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes = 0;

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType = 2;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnInitializeMibMplsL3VpnIfConfTable
* Input       : pL3vpnMplsL3VpnIfConfEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
L3vpnInitializeMibMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                        pL3vpnMplsL3VpnIfConfEntry)
{

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification = 2;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType = 2;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnInitializeMibMplsL3VpnVrfRTTable
* Input       : pL3vpnMplsL3VpnVrfRTEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
L3vpnInitializeMibMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                       pL3vpnMplsL3VpnVrfRTEntry)
{

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType = 2;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnInitializeMibMplsL3VpnVrfRteTable
* Input       : pL3vpnMplsL3VpnVrfRteEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
L3vpnInitializeMibMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                        pL3vpnMplsL3VpnVrfRteEntry)
{

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex = 0;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType = 1;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrNextHopAS =
        0;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1 = -1;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2 = -1;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3 = -1;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4 = -1;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5 = -1;

    return OSIX_SUCCESS;
}
