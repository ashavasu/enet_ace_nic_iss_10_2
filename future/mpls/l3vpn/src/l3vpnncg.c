
/* $Id: l3vpnncg.c,v 1.3 2016/10/21 13:42:34 siva Exp $
   ISS Wrapper module
   module MPLS-L3VPN-STD-MIB

*/
#include "lr.h"
#include "fssnmp.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpnlwg.h"
#include "l3vpnncg.h"
#include "l3vpnprotg.h"
#include "l3vpntmr.h"
#include "l3vpnprot.h"
#include "mplsapi.h"

extern INT4 WebnmConvertColonStringToOctet (tSNMP_OCTET_STRING_TYPE * , UINT1 *);

/********************************************************************
 * FUNCTION NcMplsL3VpnConfiguredVrfsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnConfiguredVrfsGet (
		UINT4 *pu4MplsL3VpnConfiguredVrfs )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetMplsL3VpnConfiguredVrfs(
			pu4MplsL3VpnConfiguredVrfs );

	return i1RetVal;


} /* NcMplsL3VpnConfiguredVrfsGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnActiveVrfsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnActiveVrfsGet (
		UINT4 *pu4MplsL3VpnActiveVrfs )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetMplsL3VpnActiveVrfs(
			pu4MplsL3VpnActiveVrfs );

	return i1RetVal;


} /* NcMplsL3VpnActiveVrfsGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnConnectedInterfacesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnConnectedInterfacesGet (
		UINT4 *pu4MplsL3VpnConnectedInterfaces )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetMplsL3VpnConnectedInterfaces(
			pu4MplsL3VpnConnectedInterfaces );

	return i1RetVal;


} /* NcMplsL3VpnConnectedInterfacesGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnNotificationEnableSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnNotificationEnableSet (
		INT4 i4MplsL3VpnNotificationEnable )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetMplsL3VpnNotificationEnable(
			i4MplsL3VpnNotificationEnable);

	return i1RetVal;


} /* NcMplsL3VpnNotificationEnableSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnNotificationEnableTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnNotificationEnableTest (UINT4 *pu4Error,
		INT4 i4MplsL3VpnNotificationEnable )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2MplsL3VpnNotificationEnable(pu4Error,
			i4MplsL3VpnNotificationEnable);

	return i1RetVal;


} /* NcMplsL3VpnNotificationEnableTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfMaxPossRtsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfMaxPossRtsGet (
		UINT4 *pu4MplsL3VpnVrfConfMaxPossRts )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetMplsL3VpnVrfConfMaxPossRts(
			pu4MplsL3VpnVrfConfMaxPossRts );

	return i1RetVal;


} /* NcMplsL3VpnVrfConfMaxPossRtsGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfRteMxThrshTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfRteMxThrshTimeGet (
		UINT4 *pu4MplsL3VpnVrfConfRteMxThrshTime )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetMplsL3VpnVrfConfRteMxThrshTime(
			pu4MplsL3VpnVrfConfRteMxThrshTime );

	return i1RetVal;


} /* NcMplsL3VpnVrfConfRteMxThrshTimeGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnIllLblRcvThrshSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIllLblRcvThrshSet (
		UINT4 u4MplsL3VpnIllLblRcvThrsh )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetMplsL3VpnIllLblRcvThrsh(
			u4MplsL3VpnIllLblRcvThrsh);

	return i1RetVal;


} /* NcMplsL3VpnIllLblRcvThrshSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnIllLblRcvThrshTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIllLblRcvThrshTest (UINT4 *pu4Error,
		UINT4 u4MplsL3VpnIllLblRcvThrsh )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2MplsL3VpnIllLblRcvThrsh(pu4Error,
			u4MplsL3VpnIllLblRcvThrsh);

	return i1RetVal;


} /* NcMplsL3VpnIllLblRcvThrshTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfVpnClassificationSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIfVpnClassificationSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnIfConfIndex,
		INT4 i4MplsL3VpnIfVpnClassification )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhSetMplsL3VpnIfVpnClassification(
			&MplsL3VpnVrfName,
			i4MplsL3VpnIfConfIndex,
			i4MplsL3VpnIfVpnClassification);

	return i1RetVal;


} /* NcMplsL3VpnIfVpnClassificationSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfVpnClassificationTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIfVpnClassificationTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnIfConfIndex,
		INT4 i4MplsL3VpnIfVpnClassification )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhTestv2MplsL3VpnIfVpnClassification(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnIfConfIndex,
			i4MplsL3VpnIfVpnClassification);

	return i1RetVal;


} /* NcMplsL3VpnIfVpnClassificationTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfVpnRouteDistProtocolSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcMplsL3VpnIfVpnRouteDistProtocolSet (
  UINT1 *au1MplsL3VpnVrfName,
  INT4 i4MplsL3VpnIfConfIndex,
  ncx_list_t ncx_list_tMplsL3VpnIfVpnRouteDistProtocol )
  {

  INT1 i1RetVal;
  tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
  MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
  MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
  MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

  i1RetVal = nmhSetMplsL3VpnIfVpnRouteDistProtocol(
  &MplsL3VpnVrfName,
  i4MplsL3VpnIfConfIndex,
  ncx_list_tMplsL3VpnIfVpnRouteDistProtocol);

  return i1RetVal;


  }*/
/* NcMplsL3VpnIfVpnRouteDistProtocolSet */


/********************************************************************
 * FUNCTION NcMplsL3VpnIfVpnRouteDistProtocolTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcMplsL3VpnIfVpnRouteDistProtocolTest (UINT4 *pu4Error,
  UINT1 *au1MplsL3VpnVrfName,
  INT4 i4MplsL3VpnIfConfIndex,
  ncx_list_t ncx_list_tMplsL3VpnIfVpnRouteDistProtocol )
  {

  INT1 i1RetVal;
  tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
  MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
  MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
  MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

  i1RetVal = nmhTestv2MplsL3VpnIfVpnRouteDistProtocol(pu4Error,
  &MplsL3VpnVrfName,
  i4MplsL3VpnIfConfIndex,
  ncx_list_tMplsL3VpnIfVpnRouteDistProtocol);

  return i1RetVal;


  } */
/* NcMplsL3VpnIfVpnRouteDistProtocolTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfConfStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIfConfStorageTypeSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnIfConfIndex,
		INT4 i4MplsL3VpnIfConfStorageType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhSetMplsL3VpnIfConfStorageType(
			&MplsL3VpnVrfName,
			i4MplsL3VpnIfConfIndex,
			i4MplsL3VpnIfConfStorageType);

	return i1RetVal;


} /* NcMplsL3VpnIfConfStorageTypeSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfConfStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIfConfStorageTypeTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnIfConfIndex,
		INT4 i4MplsL3VpnIfConfStorageType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhTestv2MplsL3VpnIfConfStorageType(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnIfConfIndex,
			i4MplsL3VpnIfConfStorageType);

	return i1RetVal;


} /* NcMplsL3VpnIfConfStorageTypeTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfConfRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIfConfRowStatusSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnIfConfIndex,
		INT4 i4MplsL3VpnIfConfRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhSetMplsL3VpnIfConfRowStatus(
			&MplsL3VpnVrfName,
			i4MplsL3VpnIfConfIndex,
			i4MplsL3VpnIfConfRowStatus);

	return i1RetVal;


} /* NcMplsL3VpnIfConfRowStatusSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnIfConfRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnIfConfRowStatusTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnIfConfIndex,
		INT4 i4MplsL3VpnIfConfRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhTestv2MplsL3VpnIfConfRowStatus(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnIfConfIndex,
			i4MplsL3VpnIfConfRowStatus);

	return i1RetVal;


} /* NcMplsL3VpnIfConfRowStatusTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfVpnIdSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfVpnIdSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT1 *pau1MplsL3VpnVrfVpnId )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfVpnId;
	MEMSET (&MplsL3VpnVrfVpnId, 0,  sizeof (MplsL3VpnVrfVpnId));

	MplsL3VpnVrfVpnId.i4_Length = (INT4) STRLEN (pau1MplsL3VpnVrfVpnId);
	MplsL3VpnVrfVpnId.pu1_OctetList = pau1MplsL3VpnVrfVpnId;

	i1RetVal = nmhSetMplsL3VpnVrfVpnId(
			&MplsL3VpnVrfName,
			&MplsL3VpnVrfVpnId);

	return i1RetVal;


} /* NcMplsL3VpnVrfVpnIdSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfVpnIdTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfVpnIdTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT1 *pau1MplsL3VpnVrfVpnId )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfVpnId;
	MEMSET (&MplsL3VpnVrfVpnId, 0,  sizeof (MplsL3VpnVrfVpnId));

	MplsL3VpnVrfVpnId.i4_Length = (INT4) STRLEN (pau1MplsL3VpnVrfVpnId);
	MplsL3VpnVrfVpnId.pu1_OctetList = pau1MplsL3VpnVrfVpnId;

	i1RetVal = nmhTestv2MplsL3VpnVrfVpnId(pu4Error,
			&MplsL3VpnVrfName,
			&MplsL3VpnVrfVpnId);

	return i1RetVal;


} /* NcMplsL3VpnVrfVpnIdTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfDescriptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfDescriptionSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT1 *pMplsL3VpnVrfDescription )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfDescription;
	MEMSET (&MplsL3VpnVrfDescription, 0,  sizeof (MplsL3VpnVrfDescription));

	MplsL3VpnVrfDescription.i4_Length = (INT4) STRLEN (pMplsL3VpnVrfDescription);
	MplsL3VpnVrfDescription.pu1_OctetList = pMplsL3VpnVrfDescription;

	i1RetVal = nmhSetMplsL3VpnVrfDescription(
			&MplsL3VpnVrfName,
			&MplsL3VpnVrfDescription);

	return i1RetVal;


} /* NcMplsL3VpnVrfDescriptionSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfDescriptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfDescriptionTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT1 *pMplsL3VpnVrfDescription )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfDescription;
	MEMSET (&MplsL3VpnVrfDescription, 0,  sizeof (MplsL3VpnVrfDescription));

	MplsL3VpnVrfDescription.i4_Length = (INT4) STRLEN (pMplsL3VpnVrfDescription);
	MplsL3VpnVrfDescription.pu1_OctetList = pMplsL3VpnVrfDescription;

	i1RetVal = nmhTestv2MplsL3VpnVrfDescription(pu4Error,
			&MplsL3VpnVrfName,
			&MplsL3VpnVrfDescription);

	return i1RetVal;


} /* NcMplsL3VpnVrfDescriptionTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRDSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRDSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT1 *pau1MplsL3VpnVrfRD )
{

	INT1 i1RetVal;

	UINT1 au1TempRd[MPLS_MAX_L3VPN_RD_LEN];	
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRD;
	MEMSET(au1TempRd, 0, MPLS_MAX_L3VPN_RD_LEN);

	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MEMSET (&MplsL3VpnVrfRD, 0,  sizeof (MplsL3VpnVrfRD));
	i1RetVal = (INT1) L3VpnCliParseAndGenerateRdRt(pau1MplsL3VpnVrfRD, au1TempRd);

	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	MplsL3VpnVrfRD.i4_Length = MPLS_MAX_L3VPN_RD_LEN;
	MplsL3VpnVrfRD.pu1_OctetList = au1TempRd;

	i1RetVal = nmhSetMplsL3VpnVrfRD(
			&MplsL3VpnVrfName,
			&MplsL3VpnVrfRD);

	return i1RetVal;


} /* NcMplsL3VpnVrfRDSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRDTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRDTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT1 *pau1MplsL3VpnVrfRD )
{

	INT1 i1RetVal;
	UINT1 au1TempRd[MPLS_MAX_L3VPN_RD_LEN];
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRD;

	MEMSET(au1TempRd, 0, MPLS_MAX_L3VPN_RD_LEN);
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MEMSET (&MplsL3VpnVrfRD, 0,  sizeof (MplsL3VpnVrfRD));

	i1RetVal = (INT1) L3VpnCliParseAndGenerateRdRt(pau1MplsL3VpnVrfRD, au1TempRd);

	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	MplsL3VpnVrfRD.i4_Length = MPLS_MAX_L3VPN_RD_LEN;
	MplsL3VpnVrfRD.pu1_OctetList = au1TempRd;

	i1RetVal = nmhTestv2MplsL3VpnVrfRD(pu4Error,
			&MplsL3VpnVrfName,
			&MplsL3VpnVrfRD);

	return i1RetVal;


} /* NcMplsL3VpnVrfRDTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfCreationTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfCreationTimeGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfCreationTime )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfCreationTime(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfCreationTime );

	return i1RetVal;


} /* NcMplsL3VpnVrfCreationTimeGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfOperStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfOperStatusGet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 *pi4MplsL3VpnVrfOperStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfOperStatus(
			&MplsL3VpnVrfName,
			pi4MplsL3VpnVrfOperStatus );

	return i1RetVal;


} /* NcMplsL3VpnVrfOperStatusGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfActiveInterfacesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfActiveInterfacesGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfActiveInterfaces )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfActiveInterfaces(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfActiveInterfaces );

	return i1RetVal;


} /* NcMplsL3VpnVrfActiveInterfacesGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfAssociatedInterfacesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfAssociatedInterfacesGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfAssociatedInterfaces )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfAssociatedInterfaces(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfAssociatedInterfaces );

	return i1RetVal;


} /* NcMplsL3VpnVrfAssociatedInterfacesGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfMidRteThreshSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfMidRteThreshSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfConfMidRteThresh )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhSetMplsL3VpnVrfConfMidRteThresh(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfConfMidRteThresh);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfMidRteThreshSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfMidRteThreshTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfMidRteThreshTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfConfMidRteThresh )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhTestv2MplsL3VpnVrfConfMidRteThresh(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfConfMidRteThresh);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfMidRteThreshTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfHighRteThreshSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfHighRteThreshSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfConfHighRteThresh )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfConfHighRteThresh(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfConfHighRteThresh);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfHighRteThreshSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfHighRteThreshTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfHighRteThreshTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfConfHighRteThresh )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhTestv2MplsL3VpnVrfConfHighRteThresh(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfConfHighRteThresh);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfHighRteThreshTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfMaxRoutesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfMaxRoutesSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfConfMaxRoutes )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfConfMaxRoutes(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfConfMaxRoutes);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfMaxRoutesSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfMaxRoutesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfMaxRoutesTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfConfMaxRoutes )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhTestv2MplsL3VpnVrfConfMaxRoutes(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfConfMaxRoutes);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfMaxRoutesTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfLastChangedGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfLastChangedGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfConfLastChanged )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfConfLastChanged(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfConfLastChanged );

	return i1RetVal;


} /* NcMplsL3VpnVrfConfLastChangedGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfRowStatusSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfConfRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfConfRowStatus(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfConfRowStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfRowStatusSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfRowStatusTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfConfRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhTestv2MplsL3VpnVrfConfRowStatus(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfConfRowStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfRowStatusTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfAdminStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfAdminStatusSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfConfAdminStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfConfAdminStatus(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfConfAdminStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfAdminStatusSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfAdminStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfAdminStatusTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfConfAdminStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhTestv2MplsL3VpnVrfConfAdminStatus(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfConfAdminStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfAdminStatusTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfStorageTypeSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfConfStorageType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfConfStorageType(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfConfStorageType);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfStorageTypeSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfConfStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfConfStorageTypeTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfConfStorageType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhTestv2MplsL3VpnVrfConfStorageType(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfConfStorageType);

	return i1RetVal;


} /* NcMplsL3VpnVrfConfStorageTypeTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfSecIllegalLblVltnsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfSecIllegalLblVltnsGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfSecIllegalLblVltns )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfSecIllegalLblVltns(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfSecIllegalLblVltns );

	return i1RetVal;


} /* NcMplsL3VpnVrfSecIllegalLblVltnsGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfSecDiscontinuityTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfSecDiscontinuityTimeGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfSecDiscontinuityTime )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfSecDiscontinuityTime(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfSecDiscontinuityTime );

	return i1RetVal;


} /* NcMplsL3VpnVrfSecDiscontinuityTimeGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfPerfRoutesAddedGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfPerfRoutesAddedGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfPerfRoutesAdded )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfPerfRoutesAdded(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfPerfRoutesAdded );

	return i1RetVal;


} /* NcMplsL3VpnVrfPerfRoutesAddedGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfPerfRoutesDeletedGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfPerfRoutesDeletedGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfPerfRoutesDeleted )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfPerfRoutesDeleted(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfPerfRoutesDeleted );

	return i1RetVal;


} /* NcMplsL3VpnVrfPerfRoutesDeletedGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfPerfCurrNumRoutesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfPerfCurrNumRoutesGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfPerfCurrNumRoutes )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfPerfCurrNumRoutes(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfPerfCurrNumRoutes );

	return i1RetVal;


} /* NcMplsL3VpnVrfPerfCurrNumRoutesGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfPerfRoutesDroppedGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfPerfRoutesDroppedGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfPerfRoutesDropped )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfPerfRoutesDropped(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfPerfRoutesDropped );

	return i1RetVal;


} /* NcMplsL3VpnVrfPerfRoutesDroppedGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfPerfDiscTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfPerfDiscTimeGet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 *pu4MplsL3VpnVrfPerfDiscTime )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	if (nmhValidateIndexInstanceMplsL3VpnVrfTable(
				&MplsL3VpnVrfName) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfPerfDiscTime(
			&MplsL3VpnVrfName,
			pu4MplsL3VpnVrfPerfDiscTime );

	return i1RetVal;


} /* NcMplsL3VpnVrfPerfDiscTimeGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		UINT1 *pau1MplsL3VpnVrfRT )
{

	INT1 i1RetVal;
	UINT1 au1TempRt[L3VPN_MAX_RT_LEN];
	MEMSET(au1TempRt, 0, L3VPN_MAX_RT_LEN);

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRT;
	MEMSET (&MplsL3VpnVrfRT, 0,  sizeof (MplsL3VpnVrfRT));

	i1RetVal = (INT1) L3VpnCliParseAndGenerateRdRt(pau1MplsL3VpnVrfRT, au1TempRt);

	MplsL3VpnVrfRT.i4_Length = L3VPN_MAX_RT_LEN;
	MplsL3VpnVrfRT.pu1_OctetList = au1TempRt;

	i1RetVal = nmhSetMplsL3VpnVrfRT(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			&MplsL3VpnVrfRT);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		UINT1 *pau1MplsL3VpnVrfRT )
{

	INT1 i1RetVal;
	UINT1 au1TempRt[L3VPN_MAX_RT_LEN];
	MEMSET(au1TempRt, 0, L3VPN_MAX_RT_LEN);

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRT;
	MEMSET (&MplsL3VpnVrfRT, 0,  sizeof (MplsL3VpnVrfRT));

	i1RetVal = (INT1) L3VpnCliParseAndGenerateRdRt(pau1MplsL3VpnVrfRT, au1TempRt);

	MplsL3VpnVrfRT.i4_Length = L3VPN_MAX_RT_LEN;
	MplsL3VpnVrfRT.pu1_OctetList = au1TempRt;

	i1RetVal = nmhTestv2MplsL3VpnVrfRT(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			&MplsL3VpnVrfRT);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTDescrSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTDescrSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		UINT1 *pMplsL3VpnVrfRTDescr )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRTDescr;
	MEMSET (&MplsL3VpnVrfRTDescr, 0,  sizeof (MplsL3VpnVrfRTDescr));

	MplsL3VpnVrfRTDescr.i4_Length = (INT4) STRLEN (pMplsL3VpnVrfRTDescr);
	MplsL3VpnVrfRTDescr.pu1_OctetList = pMplsL3VpnVrfRTDescr;

	i1RetVal = nmhSetMplsL3VpnVrfRTDescr(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			&MplsL3VpnVrfRTDescr);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTDescrSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTDescrTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTDescrTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		UINT1 *pMplsL3VpnVrfRTDescr )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRTDescr;
	MEMSET (&MplsL3VpnVrfRTDescr, 0,  sizeof (MplsL3VpnVrfRTDescr));

	MplsL3VpnVrfRTDescr.i4_Length = (INT4) STRLEN (pMplsL3VpnVrfRTDescr);
	MplsL3VpnVrfRTDescr.pu1_OctetList = pMplsL3VpnVrfRTDescr;

	i1RetVal = nmhTestv2MplsL3VpnVrfRTDescr(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			&MplsL3VpnVrfRTDescr);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTDescrTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTRowStatusSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		INT4 i4MplsL3VpnVrfRTRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfRTRowStatus(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			i4MplsL3VpnVrfRTRowStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTRowStatusSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTRowStatusTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		INT4 i4MplsL3VpnVrfRTRowStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhTestv2MplsL3VpnVrfRTRowStatus(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			i4MplsL3VpnVrfRTRowStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTRowStatusTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTStorageTypeSet (
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		INT4 i4MplsL3VpnVrfRTStorageType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;


	i1RetVal = nmhSetMplsL3VpnVrfRTStorageType(
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			i4MplsL3VpnVrfRTStorageType);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTStorageTypeSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRTStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRTStorageTypeTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		UINT4 u4MplsL3VpnVrfRTIndex,
		INT4 i4MplsL3VpnVrfRTType,
		INT4 i4MplsL3VpnVrfRTStorageType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	i1RetVal = nmhTestv2MplsL3VpnVrfRTStorageType(pu4Error,
			&MplsL3VpnVrfName,
			u4MplsL3VpnVrfRTIndex,
			i4MplsL3VpnVrfRTType,
			i4MplsL3VpnVrfRTStorageType);

	return i1RetVal;


} /* NcMplsL3VpnVrfRTStorageTypeTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrIfIndexSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrIfIndexSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrIfIndex )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrIfIndex(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrIfIndex);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrIfIndexSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrIfIndexTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrIfIndexTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrIfIndex )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrDest);
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrNextHop);
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrIfIndex(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrIfIndex);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrIfIndexTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrTypeSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrType(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrType);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrTypeSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrTypeTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrType )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrType(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrType);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrTypeTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrProtoGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrProtoGet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 *pi4MplsL3VpnVrfRteInetCidrProto )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrDest);
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrNextHop);
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable(
				&MplsL3VpnVrfName,
				i4MplsL3VpnVrfRteInetCidrDestType,
				&MplsL3VpnVrfRteInetCidrDest,
				u4MplsL3VpnVrfRteInetCidrPfxLen,
				&MplsL3VpnVrfRteInetCidrPolicy,
				i4MplsL3VpnVrfRteInetCidrNHopType,
				&MplsL3VpnVrfRteInetCidrNextHop) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfRteInetCidrProto(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			pi4MplsL3VpnVrfRteInetCidrProto );

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrProtoGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrAgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrAgeGet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		UINT4 *pu4MplsL3VpnVrfRteInetCidrAge )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrDest);
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrNextHop);
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable(
				&MplsL3VpnVrfName,
				i4MplsL3VpnVrfRteInetCidrDestType,
				&MplsL3VpnVrfRteInetCidrDest,
				u4MplsL3VpnVrfRteInetCidrPfxLen,
				&MplsL3VpnVrfRteInetCidrPolicy,
				i4MplsL3VpnVrfRteInetCidrNHopType,
				&MplsL3VpnVrfRteInetCidrNextHop) == SNMP_FAILURE)

	{
		return SNMP_FAILURE;
	}

	i1RetVal = nmhGetMplsL3VpnVrfRteInetCidrAge(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			pu4MplsL3VpnVrfRteInetCidrAge );

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrAgeGet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrNextHopASSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrNextHopASSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		UINT4 u4MplsL3VpnVrfRteInetCidrNextHopAS )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrNextHopAS(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			u4MplsL3VpnVrfRteInetCidrNextHopAS);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrNextHopASSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrNextHopASTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrNextHopASTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		UINT4 u4MplsL3VpnVrfRteInetCidrNextHopAS )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrDest);
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrNextHop);
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrNextHopAS(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			u4MplsL3VpnVrfRteInetCidrNextHopAS);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrNextHopASTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric1Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric1Set (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric1 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrMetric1(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric1);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric1Set */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric1Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric1Test (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric1 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrMetric1(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric1);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric1Test */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric2Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric2Set (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric2 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrMetric2(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric2);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric2Set */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric2Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric2Test (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric2 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrMetric2(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric2);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric2Test */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric3Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric3Set (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric3 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrMetric3(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric3);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric3Set */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric3Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric3Test (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric3 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrMetric3(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric3);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric3Test */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric4Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric4Set (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric4 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrMetric4(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric4);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric4Set */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric4Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric4Test (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric4 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrMetric4(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric4);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric4Test */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric5Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric5Set (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric5 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrMetric5(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric5);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric5Set */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrMetric5Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrMetric5Test (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrMetric5 )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrMetric5(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrMetric5);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrMetric5Test */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteXCPointerSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteXCPointerSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		UINT1 *pau1MplsL3VpnVrfRteXCPointer )
{

	INT1 i1RetVal;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	tSNMP_MULTI_DATA_TYPE *pData = NULL;
	UINT1 u1OidType=4;
	pData = Snmp3CliConvertStringToData (pau1MplsL3VpnVrfRteXCPointer, u1OidType);
	WebnmConvertColonStringToOctet (pData->pOctetStrValue,pau1MplsL3VpnVrfRteXCPointer);
	
	

	i1RetVal = nmhSetMplsL3VpnVrfRteXCPointer(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			pData->pOctetStrValue);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteXCPointerSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteXCPointerTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteXCPointerTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		UINT1 *pau1MplsL3VpnVrfRteXCPointer )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrDest);
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfRteInetCidrNextHop);
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;


	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteXCPointer;
	MEMSET (&MplsL3VpnVrfRteXCPointer, 0,  sizeof (MplsL3VpnVrfRteXCPointer));

	MplsL3VpnVrfRteXCPointer.i4_Length = (INT4) STRLEN (pau1MplsL3VpnVrfRteXCPointer);
	MplsL3VpnVrfRteXCPointer.pu1_OctetList = pau1MplsL3VpnVrfRteXCPointer;

	i1RetVal = nmhTestv2MplsL3VpnVrfRteXCPointer(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			&MplsL3VpnVrfRteXCPointer);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteXCPointerTest */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrStatusSet (
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrStatus )
{

	INT1 i1RetVal;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhSetMplsL3VpnVrfRteInetCidrStatus(
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrStatusSet */

/********************************************************************
 * FUNCTION NcMplsL3VpnVrfRteInetCidrStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcMplsL3VpnVrfRteInetCidrStatusTest (UINT4 *pu4Error,
		UINT1 *au1MplsL3VpnVrfName,
		INT4 i4MplsL3VpnVrfRteInetCidrDestType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrDest,
		UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
		UINT4 *pMplsL3VpnVrfRteInetCidrPolicy,
		INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
		UINT1 *au1MplsL3VpnVrfRteInetCidrNextHop,
		INT4 i4MplsL3VpnVrfRteInetCidrStatus )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfName;
	MEMSET (&MplsL3VpnVrfName, 0,  sizeof (MplsL3VpnVrfName));
	MplsL3VpnVrfName.i4_Length = (INT4) STRLEN (au1MplsL3VpnVrfName);
	MplsL3VpnVrfName.pu1_OctetList = au1MplsL3VpnVrfName;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDest;
	MEMSET (&MplsL3VpnVrfRteInetCidrDest, 0,  sizeof (MplsL3VpnVrfRteInetCidrDest));
	MplsL3VpnVrfRteInetCidrDest.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrDest.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrDest;

	tSNMP_OID_TYPE  MplsL3VpnVrfRteInetCidrPolicy;
	MEMSET (&MplsL3VpnVrfRteInetCidrPolicy, 0,  sizeof (MplsL3VpnVrfRteInetCidrPolicy));
	MplsL3VpnVrfRteInetCidrPolicy.u4_Length = (UINT4) STRLEN (pMplsL3VpnVrfRteInetCidrPolicy);
	MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList = pMplsL3VpnVrfRteInetCidrPolicy;

	tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHop;
	MEMSET (&MplsL3VpnVrfRteInetCidrNextHop, 0,  sizeof (MplsL3VpnVrfRteInetCidrNextHop));
	MplsL3VpnVrfRteInetCidrNextHop.i4_Length = 4;
	MplsL3VpnVrfRteInetCidrNextHop.pu1_OctetList = au1MplsL3VpnVrfRteInetCidrNextHop;

	i1RetVal = nmhTestv2MplsL3VpnVrfRteInetCidrStatus(pu4Error,
			&MplsL3VpnVrfName,
			i4MplsL3VpnVrfRteInetCidrDestType,
			&MplsL3VpnVrfRteInetCidrDest,
			u4MplsL3VpnVrfRteInetCidrPfxLen,
			&MplsL3VpnVrfRteInetCidrPolicy,
			i4MplsL3VpnVrfRteInetCidrNHopType,
			&MplsL3VpnVrfRteInetCidrNextHop,
			i4MplsL3VpnVrfRteInetCidrStatus);

	return i1RetVal;


} /* NcMplsL3VpnVrfRteInetCidrStatusTest */

/* END i_MPLS_L3VPN_STD_MIB.c */
