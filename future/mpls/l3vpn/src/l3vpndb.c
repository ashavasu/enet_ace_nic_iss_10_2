/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndb.c,v 1.6 2016/04/08 09:53:07 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id:   l3vpndb.c 
*
* Description: This file contains the routines for the protocol Database Access for the module L3vpn 
*********************************************************************/

#include "l3vpninc.h"
#include "l3vpncli.h"
#include "mplsprot.h"

/****************************************************************************
 Function    :  L3vpnTestAllMplsL3VpnVrfTable
 Input       :  pu4ErrorCode
                pL3vpnSetMplsL3VpnVrfEntry
                pL3vpnIsSetMplsL3VpnVrfEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnTestAllMplsL3VpnVrfTable (UINT4 *pu4ErrorCode,
                               tL3vpnMplsL3VpnVrfEntry *
                               pL3vpnSetMplsL3VpnVrfEntry,
                               tL3vpnIsSetMplsL3VpnVrfEntry *
                               pL3vpnIsSetMplsL3VpnVrfEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    UINT4               u4VcId = 0;
    UINT1               au1Rd[MPLS_MAX_L3VPN_RD_LEN];
    UINT1               au1TempRd[MPLS_MAX_L3VPN_RD_LEN];

    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        L3vpnGetMplsL3VpnVrfTable (pL3vpnSetMplsL3VpnVrfEntry);

    MEMSET (au1Rd, 0, MPLS_MAX_L3VPN_RD_LEN);
    MEMSET (au1TempRd, 0, MPLS_MAX_L3VPN_RD_LEN);

    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return OSIX_FAILURE;
    }

    /* if the row does not exist and the user intends to set any other parameter other than the row status
     * return failure  */
    if ((pL3vpnMplsL3VpnVrfEntry == NULL) &&
        (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus ==
         OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return OSIX_FAILURE;
    }

    /* Basic Index Vaidation for the Table */
    if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen >
        L3VPN_MAX_VRF_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId != OSIX_FALSE)
    {
        /* If the row status is Active then user cannot set the VPN ID */
        if (pL3vpnMplsL3VpnVrfEntry != NULL)
        {
            if (L3VPN_P_VRF_ROW_STATUS (pL3vpnMplsL3VpnVrfEntry) == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
        }
        /*Fill your check condition */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen !=
            L3VPN_MAX_VRF_VPN_ID_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription != OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen >
            L3VPN_MAX_VRF_DESCRIPTION_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD != OSIX_FALSE)
    {
#if 0
        if (MPLS_SUCCESS !=
            MplsValidateRdRtWithAsn (L3VPN_P_VRF_RD
                                     (pL3vpnSetMplsL3VpnVrfEntry)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_L3VPN_INVALID_ASN);
            return OSIX_FAILURE;
        }
#endif
        if (L3VPN_P_VRF_RD_LEN (pL3vpnSetMplsL3VpnVrfEntry) != L3VPN_MAX_RD_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        /* Check for individual bytes */
        if ((L3VPN_P_VRF_RD (pL3vpnSetMplsL3VpnVrfEntry)[0] > L3VPN_RD_TYPE_2)
            || (L3VPN_P_VRF_RD (pL3vpnSetMplsL3VpnVrfEntry)[1] !=
                L3VPN_RD_SUBTYPE))

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;

        }
        /** RD cannot be changed when the Row is ACTIVE***/
        if (pL3vpnMplsL3VpnVrfEntry != NULL)
        {
            if (pL3vpnMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == ACTIVE)
            {
                CLI_SET_ERR (CLI_L3VPN_ACTIVE_VRF_FOUND);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfMidRteThresh >= L3VPN_PER_VRF_MAX_ROUTES)
        {
            CLI_SET_ERR (CLI_L3VPN_EXCEED_VRF_MID_RTE_THRESH);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        /* Check if High-Threshold is less than the Mid Threshold */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfHighRteThresh > 0)
        {
            if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                u4MplsL3VpnVrfConfMidRteThresh >
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                u4MplsL3VpnVrfConfHighRteThresh)
            {
                CLI_SET_ERR (CLI_L3VPN_EXCEED_VRF_MID_THRSH_WRONG);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh !=
        OSIX_FALSE)
    {
        /* Check if High-Threshold is less than the Mid Threshold */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfMidRteThresh > 0)
        {
            if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                u4MplsL3VpnVrfConfHighRteThresh <
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                u4MplsL3VpnVrfConfMidRteThresh)
            {
                CLI_SET_ERR (CLI_L3VPN_EXCEED_VRF_HIGH_THRSH_WRONG);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        /*Fill your check condition */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfHighRteThresh > L3VPN_PER_VRF_MAX_ROUTES)
        {
            CLI_SET_ERR (CLI_L3VPN_EXCEED_VRF_MID_RTE_THRESH);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes != OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes >
            L3VPN_PER_VRF_MAX_ROUTES)
        {
            CLI_SET_ERR (CLI_L3VPN_EXCEED_VRF_MAX_COUNT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus != OSIX_FALSE)
    {
        /*** VRF entry must not exist in the system when RowStatus is CreateAndWait **/
        if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
             i4MplsL3VpnVrfConfRowStatus == CREATE_AND_WAIT)
            || (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == ACTIVE))
        {
            if (VcmIsVrfExist
                (pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
                 &u4VcId) == VCM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                 i4MplsL3VpnVrfConfRowStatus == CREATE_AND_WAIT))
            {
                /* Row already exists, then return failure */
                if (pL3vpnMplsL3VpnVrfEntry != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == ACTIVE)
            {
                /* The RD Entry exists beforefhand, so RD value must be configured before making the row Active */
                if (pL3vpnMplsL3VpnVrfEntry != NULL)
                {
                    MEMSET (au1Rd, 0, MPLS_MAX_L3VPN_RD_LEN);
                    if ((MEMCMP
                         (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                          au1Rd, L3VPN_MAX_RD_LEN) == 0)
                        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD ==
                            OSIX_FALSE))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD ==
                        OSIX_FALSE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
             i4MplsL3VpnVrfConfRowStatus == NOT_READY)
            || (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == NOT_IN_SERVICE)
            || (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == DESTROY))
        {
            if (pL3vpnMplsL3VpnVrfEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus
             != L3VPN_VRF_ADMIN_STATUS_UP) &&
            (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
             i4MplsL3VpnVrfConfAdminStatus != L3VPN_VRF_ADMIN_STATUS_DOWN))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType !=
        OSIX_FALSE)
    {
        if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
             i4MplsL3VpnVrfConfStorageType < L3VPN_STORAGE_OTHER)
            || (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfStorageType > L3VPN_STORAGE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnTestAllMplsL3VpnIfConfTable
 Input       :  pu4ErrorCode
                pL3vpnSetMplsL3VpnIfConfEntry
                pL3vpnIsSetMplsL3VpnIfConfEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnTestAllMplsL3VpnIfConfTable (UINT4 *pu4ErrorCode,
                                  tL3vpnMplsL3VpnIfConfEntry *
                                  pL3vpnSetMplsL3VpnIfConfEntry,
                                  tL3vpnIsSetMplsL3VpnIfConfEntry *
                                  pL3vpnIsSetMplsL3VpnIfConfEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pL3vpnSetMplsL3VpnIfConfEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnTestAllMplsL3VpnVrfRTTable
 Input       :  pu4ErrorCode
                pL3vpnSetMplsL3VpnVrfRTEntry
                pL3vpnIsSetMplsL3VpnVrfRTEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnTestAllMplsL3VpnVrfRTTable (UINT4 *pu4ErrorCode,
                                 tL3vpnMplsL3VpnVrfRTEntry *
                                 pL3vpnSetMplsL3VpnVrfRTEntry,
                                 tL3vpnIsSetMplsL3VpnVrfRTEntry *
                                 pL3vpnIsSetMplsL3VpnVrfRTEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    UINT4               u4VcId = 0;
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnMplsL3VpnVrfRTEntry *pRtEntry = NULL;
    UINT1               au1Rt[L3VPN_MAX_RT_LEN];
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    /* Zero RT value */
    MEMSET (au1Rt, 0, L3VPN_MAX_RT_LEN);

    /* Index Validation */
    if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen >
         L3VPN_MAX_VRF_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return OSIX_FAILURE;
    }

    /* The RT Type will not be passed when RT is deleted from CLI
     * Deletion is based on the value of RT, Thus this check is valid */
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType == OSIX_TRUE)
    {
        if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType <
             MPLS_L3VPN_RT_IMPORT)
            || (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType >=
                MPLS_L3VPN_MAX_RT_TYPE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }

    if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex >
        L3VPN_MAX_NUM_RT_PER_VRF)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return OSIX_FAILURE;
    }
    pL3vpnMplsL3VpnVrfRTEntry =
        L3vpnGetMplsL3VpnVrfRTTable (pL3vpnSetMplsL3VpnVrfRTEntry);

    /* if the row does not exist and the user intends to set any other parameter other than the row status
     * return failure  */
    if ((pL3vpnMplsL3VpnVrfRTEntry == NULL) &&
        (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus ==
         OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT != OSIX_FALSE)
    {
#if 0
        if (MPLS_SUCCESS !=
            MplsValidateRdRtWithAsn (L3VPN_P_RT_VALUE
                                     (pL3vpnSetMplsL3VpnVrfRTEntry)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_L3VPN_INVALID_ASN);
            return OSIX_FAILURE;
        }
#endif
        if ((L3VPN_P_RT_VALUE (pL3vpnSetMplsL3VpnVrfRTEntry)[0] >
             L3VPN_RT_TYPE_2)
            || (L3VPN_P_RT_VALUE (pL3vpnSetMplsL3VpnVrfRTEntry)[1] !=
                L3VPN_RT_SUBTYPE))

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (L3VPN_P_RT_LEN (pL3vpnSetMplsL3VpnVrfRTEntry) != L3VPN_MAX_RT_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        /* RT cannot be changed when the row is ACTIVE */
        if (pL3vpnMplsL3VpnVrfRTEntry != NULL)
        {
            if (pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        /* From the CLI: RT will be set in two scnearios - ACTIVE and DESTROY
         * Below check is not required while destroying the row
         * From SNMP, below check will not hit since the row status will be 0
         * Only RT will be set in setEntry structure  */
        if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus !=
            DESTROY)
        {
            /* TODO: Call function from VPLS to check for RT validity */
            if (L3VpnGetRTEntryFromValue
                (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
                 pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT) !=
                NULL)
            {
                CLI_SET_ERR (CLI_L3VPN_RT_DUPLICATE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        /*Fill your check condition */
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr != OSIX_FALSE)
    {
        if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen >
            L3VPN_MAX_DESC_LEN)
        {
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus != OSIX_FALSE)
    {
        if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
             i4MplsL3VpnVrfRTRowStatus == ACTIVE)
            || (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT))
        {
            if (VcmIsVrfExist
                (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
                 &u4VcId) == VCM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
            if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                 i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT))
            {
                if (pL3vpnMplsL3VpnVrfRTEntry != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                /* if the MPLS-VRF does exit RT row cannot be created */
                pL3vpnMplsL3VpnVrfEntry = MplsL3VpnApiGetVrfTableEntry
                    (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                     au1MplsL3VpnVrfName,
                     (INT4) STRLEN (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                                    au1MplsL3VpnVrfName));
                if (pL3vpnMplsL3VpnVrfEntry == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return OSIX_FAILURE;
                }
            }
        }

        if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
            ACTIVE)
        {
            /* The RT Entry exists beforefhand, so RT value must be configured before making the row Active */
            if (pL3vpnMplsL3VpnVrfRTEntry != NULL)
            {
                if (MEMCMP
                    (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                     au1Rt, L3VPN_MAX_RT_LEN) == 0
                    && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT ==
                        OSIX_FALSE))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return OSIX_FAILURE;
                }
            }
            /* The Entry does not yet exist, Check if RT is provided which is a mandatory parameter */
            else
            {
                /* RT is not provided so return failure */
                if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT ==
                    OSIX_FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return OSIX_FAILURE;
                }
            }
        }
        if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
            DESTROY)
        {
            /* Destroy from CLI -- No indices will be provided, rather RT vaule will be provided */
            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT == OSIX_TRUE)
            {
                pRtEntry =
                    L3VpnGetRTEntryFromValue (pL3vpnSetMplsL3VpnVrfRTEntry->
                                              MibObject.au1MplsL3VpnVrfName,
                                              pL3vpnSetMplsL3VpnVrfRTEntry->
                                              MibObject.au1MplsL3VpnVrfRT);
                if (pRtEntry == NULL)
                {
                    CLI_SET_ERR (CLI_L3VPN_NO_SUCH_RT);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return OSIX_FAILURE;
                }
                /* Fill in the indices here because SetAll will delete on the basis on indices */
                else
                {
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        u4MplsL3VpnVrfRTIndex =
                        pRtEntry->MibObject.u4MplsL3VpnVrfRTIndex;
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfRTType =
                        pRtEntry->MibObject.i4MplsL3VpnVrfRTType;
                    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex =
                        OSIX_TRUE;
                    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType =
                        OSIX_TRUE;
                }

            }
            /* Destroy from SNMP will be based on the indices */
            else
            {
                if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return OSIX_FAILURE;
                }
            }

        }
        if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
             i4MplsL3VpnVrfRTRowStatus == NOT_IN_SERVICE)
            || (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == NOT_READY))
        {
            if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
            }
        }
        /*Fill your check condition */
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType !=
        OSIX_FALSE)
    {
        if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
             i4MplsL3VpnVrfRTStorageType < L3VPN_STORAGE_OTHER)
            || (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTStorageType) > L3VPN_STORAGE_READONLY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pL3vpnSetMplsL3VpnVrfRTEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnTestAllMplsL3VpnVrfRteTable
 Input       :  pu4ErrorCode
                pL3vpnSetMplsL3VpnVrfRteEntry
                pL3vpnIsSetMplsL3VpnVrfRteEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnTestAllMplsL3VpnVrfRteTable (UINT4 *pu4ErrorCode,
                                  tL3vpnMplsL3VpnVrfRteEntry *
                                  pL3vpnSetMplsL3VpnVrfRteEntry,
                                  tL3vpnIsSetMplsL3VpnVrfRteEntry *
                                  pL3vpnIsSetMplsL3VpnVrfRteEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
        tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
        UINT1              *pu1RteXcPointer = NULL;
        UINT4 u4InIndex = 0;
        UINT4 u4OutIndex = 0;
        UINT4 u4XcIndex = 0;
        UINT4 u4IfIndex = 0;
        UINT1 u1IfType=0;
        tXcEntry *pXcEntry = NULL;

        UNUSED_PARAM (i4RowStatusLogic);
        UNUSED_PARAM (i4RowCreateOption);
        pL3vpnMplsL3VpnVrfRteEntry =  L3vpnGetMplsL3VpnVrfRteTable(pL3vpnSetMplsL3VpnVrfRteEntry);

        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName == OSIX_FALSE)
        {
                CLI_SET_ERR(CLI_L3VPN_INVALID_VRF_NAME);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
        }
        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType ==
                        OSIX_FALSE)
        {
                CLI_SET_ERR (CLI_L3VPN_INVALID_DEST_TYPE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;    
        }


        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest ==
                        OSIX_FALSE)
        {
                CLI_SET_ERR (CLI_L3VPN_INVALID_DEST);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
        }
        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType ==
                        OSIX_FALSE)
        {
                CLI_SET_ERR (CLI_L3VPN_INVALID_NHOP_TYPE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
        }


        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop ==
                        OSIX_FALSE)
        {
                CLI_SET_ERR (CLI_L3VPN_INVALID_NHOP);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
        }

        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy ==
                        OSIX_FALSE)
        {
                CLI_SET_ERR (CLI_L3VPN_INVALID_POLICY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
        }


        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen ==
                        OSIX_FALSE)
        {
                CLI_SET_ERR (CLI_L3VPN_INVALID_PREFIX_LEN);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
        }

        if ((pL3vpnMplsL3VpnVrfRteEntry == NULL) &&
                        (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus ==
                         OSIX_FALSE))
        {
                CLI_SET_ERR (CLI_L3VPN_RTE_ENTRY_INVALID);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;
        }

        /*basic validation for table entries*/
        if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen >
                        L3VPN_MAX_VRF_NAME_LEN)
        {
                CLI_SET_ERR(CLI_L3VPN_INVALID_VRF_NAME);    
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
        }
        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (L3VPN_P_RTE_INET_CIDR_DEST_TYPE(pL3vpnSetMplsL3VpnVrfRteEntry) != MPLS_IPV4_ADDR_TYPE)
                {
                        CLI_SET_ERR(CLI_L3VPN_INVALID_DEST_TYPE);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return OSIX_FAILURE;
                }
        }

        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType !=
                        OSIX_FALSE)
        {
                if (L3VPN_P_RTE_INET_CIDR_NH_TYPE(pL3vpnSetMplsL3VpnVrfRteEntry) != MPLS_IPV4_ADDR_TYPE)
                {
                        CLI_SET_ERR(CLI_L3VPN_INVALID_NHOP_TYPE);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return OSIX_FAILURE;
    }
        }

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                {
                        if(L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnMplsL3VpnVrfRteEntry)==ACTIVE)
                        {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                                return OSIX_FAILURE;
                        }
                }
                if ((L3VPN_P_RTE_INET_CIDR_TYPE(pL3vpnSetMplsL3VpnVrfRteEntry) < L3VPN_MIN_CIDRTYPE) ||
                                (L3VPN_P_RTE_INET_CIDR_TYPE(pL3vpnSetMplsL3VpnVrfRteEntry) > L3VPN_MAX_CIDRTYPE))
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_L3VPN_INVALID_METRIC);
                        return OSIX_FAILURE;
                }
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                /*       if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                         {
                         if()
                         {
                         }
                         }
                         */      
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                {
                        if(L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnMplsL3VpnVrfRteEntry)==ACTIVE)
                        {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                                CLI_SET_ERR(CLI_L3VPN_RTE_ENTRY_EXIST);
                                return OSIX_FAILURE;
                        }
                }

                if (L3VPN_P_RTE_INET_CIDR_METRIC1(pL3vpnSetMplsL3VpnVrfRteEntry) < L3VPN_MIN_METRIC) 
                {   
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_L3VPN_INVALID_METRIC);
                        return OSIX_FAILURE;
                }
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                {

                        if(L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnMplsL3VpnVrfRteEntry)==ACTIVE)
                        {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                                return OSIX_FAILURE;
                        }
                }

                if (L3VPN_P_RTE_INET_CIDR_METRIC2(pL3vpnSetMplsL3VpnVrfRteEntry) < L3VPN_MIN_METRIC)
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_L3VPN_INVALID_METRIC);
                        return OSIX_FAILURE;
                }
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                {

                        if(L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnMplsL3VpnVrfRteEntry)==ACTIVE)
                        {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                                return OSIX_FAILURE;
                        }
                }

                if (L3VPN_P_RTE_INET_CIDR_METRIC3(pL3vpnSetMplsL3VpnVrfRteEntry) < L3VPN_MIN_METRIC)
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_L3VPN_INVALID_METRIC);
                        return OSIX_FAILURE;
                }
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                {

                        if(L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnMplsL3VpnVrfRteEntry)==ACTIVE)
                        {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                                return OSIX_FAILURE;
                        }
                }

                if (L3VPN_P_RTE_INET_CIDR_METRIC4(pL3vpnSetMplsL3VpnVrfRteEntry) < L3VPN_MIN_METRIC)
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_L3VPN_INVALID_METRIC);
                        return OSIX_FAILURE;
                }

    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                {

                        if(L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnMplsL3VpnVrfRteEntry)==ACTIVE)
                        {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                                return OSIX_FAILURE;
    }
                }

                if (L3VPN_P_RTE_INET_CIDR_METRIC5(pL3vpnSetMplsL3VpnVrfRteEntry) < L3VPN_MIN_METRIC)
    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_L3VPN_INVALID_METRIC);
                        return OSIX_FAILURE;
                }

        }

        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus != OSIX_FALSE)
        {
                /*** VRFRte entry must not exist in the system when RowStatus is CreateAndWait **/
                if ((L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnSetMplsL3VpnVrfRteEntry) == CREATE_AND_WAIT)
                                || (L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnSetMplsL3VpnVrfRteEntry) == ACTIVE))
                {
                        if ((L3VPN_P_RTE_INET_CIDR_STATUS(pL3vpnSetMplsL3VpnVrfRteEntry) == CREATE_AND_WAIT))
                        {
                                /* Row already exists, then return failure */
                                if (pL3vpnMplsL3VpnVrfRteEntry != NULL)
                                {
                                        CLI_SET_ERR(CLI_L3VPN_RTE_ENTRY_EXIST);
                                        *pu4ErrorCode = CLI_L3VPN_ACTIVE_VRF_FOUND;
                                        return OSIX_FAILURE;
                                }
                        }

                        if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                                        i4MplsL3VpnVrfRteInetCidrStatus == ACTIVE)
                        {
                                if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
                                {
                                        CLI_SET_ERR (CLI_L3VPN_RTE_ENTRY_INVALID);
                                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                        return OSIX_FAILURE;
                                }
                                pu1RteXcPointer = pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer;
                                MEMCPY (&u4XcIndex, pu1RteXcPointer, sizeof (UINT4));
                                u4XcIndex = OSIX_NTOHL(u4XcIndex); 

                                pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
                                MEMCPY (&u4InIndex, pu1RteXcPointer, sizeof (UINT4));
                                u4InIndex = OSIX_NTOHL(u4InIndex);

                                pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
                                MEMCPY (&u4OutIndex, pu1RteXcPointer, sizeof (UINT4));
                                u4OutIndex = OSIX_NTOHL(u4OutIndex);

                                pXcEntry = MplsGetXCTableEntry (u4XcIndex, u4InIndex, u4OutIndex);
                                if(pXcEntry == NULL)
                                {
                                        CLI_SET_ERR (CLI_L3VPN_INVALID_XC_ENTRY);
                                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                        return OSIX_FAILURE;
                                }
                                if ((pXcEntry->u1RowStatus !=ACTIVE) || (pXcEntry->pOutIndex == NULL))
                                {
                                        CLI_SET_ERR (CLI_L3VPN_INVALID_XC_ENTRY);
                                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                        return OSIX_FAILURE;
    }
                                u4IfIndex=pXcEntry->pOutIndex->u4IfIndex;
                                if (CfaGetIfType(u4IfIndex,&u1IfType)!=CFA_SUCCESS)
                                {
                                        CLI_SET_ERR (CLI_L3VPN_INVALID_XC_ENTRY);
                                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                        return OSIX_FAILURE;
                                }
                                if(u1IfType != CFA_MPLS_TUNNEL)
                                {
                                        CLI_SET_ERR (CLI_L3VPN_INVALID_XC_ENTRY);
                                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                        return OSIX_FAILURE;
                                }

                        }
                }

                if ((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                                        i4MplsL3VpnVrfRteInetCidrStatus == NOT_READY)
                                || (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                                        i4MplsL3VpnVrfRteInetCidrStatus == NOT_IN_SERVICE)
                                || (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                                        i4MplsL3VpnVrfRteInetCidrStatus == DESTROY))
                {
                        if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
                        {
                                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                                CLI_SET_ERR (CLI_L3VPN_RTE_ENTRY_INVALID);
                                return OSIX_FAILURE;
                        }
                }
        }


#if 0
        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return OSIX_FAILURE;

    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pL3vpnSetMplsL3VpnVrfRteEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
Function    :  L3vpnTestMplsL3VpnNotificationEnable
Input       :  pu4ErrorCode
i4MplsL3VpnNotificationEnable
Description :  This Routine will Test
the Value accordingly.
Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/

INT4
L3vpnTestMplsL3VpnNotificationEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4MplsL3VpnNotificationEnable)
{
    if ((i4MplsL3VpnNotificationEnable != CLI_L3VPN_TRAP_ENABLE) &&
        i4MplsL3VpnNotificationEnable != CLI_L3VPN_TRAP_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnTestMplsL3VpnVrfConfRteMxThrshTime
 Input       :  pu4ErrorCode
                u4MplsL3VpnVrfConfRteMxThrshTime
 Description :  This Routine will Test
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnTestMplsL3VpnVrfConfRteMxThrshTime (UINT4 *pu4ErrorCode,
                                         UINT4 u4MplsL3VpnVrfConfRteMxThrshTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4MplsL3VpnVrfConfRteMxThrshTime);
    return OSIX_SUCCESS;
}

/****************************************************************************
Function    :  L3vpnTestMplsL3VpnIllLblRcvThrsh
Input       :  pu4ErrorCode
u4MplsL3VpnIllLblRcvThrsh
Description :  This Routine will Test
the Value accordingly.
Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/

INT4
L3vpnTestMplsL3VpnIllLblRcvThrsh (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsL3VpnIllLblRcvThrsh)
{
    if (u4MplsL3VpnIllLblRcvThrsh > L3VPN_ILL_LBL_RCV_THRSH_MAX)
    {
        CLI_SET_ERR (CLI_L3VPN_EXCEED_VRF_MID_RTE_THRESH);
        *pu4ErrorCode = CLI_L3VPN_EXCEED_ILL_LBL_RCV_THRSH;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}



/****************************************************************************
Function    :  L3vpnUtlTestEgressRteTableRowStatus
Input       :  u4FsMplsL3VpnVrfEgressRtePathAttrLabel
               i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus
Output      :  pu4Error
Description :  This Routine will Test the Value accordingly.
Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
UINT4
L3vpnUtlTestEgressRteTableRowStatus (UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus,UINT4 *pu4Error)
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;

        if(pu4Error==NULL)
        {
                return OSIX_FAILURE;
        }

        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);

        switch(i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)
        {
                case CREATE_AND_WAIT:
                        if(pL3VpnBgpRouteLabelEntry != NULL)
                        {
                                *pu4Error=CLI_L3VPN_RTE_ENTRY_EXIST;
                                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                                        "%s : %d Failed\n",__FUNCTION__,__LINE__));

                                return OSIX_FAILURE;
                        }
                        break;
                case NOT_IN_SERVICE:
                        if(pL3VpnBgpRouteLabelEntry == NULL)
                        {
                                *pu4Error=CLI_L3VPN_RTE_ENTRY_INVALID;

                                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                                        "%s : %d Failed\n",__FUNCTION__,__LINE__));
                                return OSIX_FAILURE;
                        }
                        break;
                case ACTIVE:
                        if(pL3VpnBgpRouteLabelEntry == NULL)
                        {
                                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                                        "%s : %d Failed\n",__FUNCTION__,__LINE__));
                                *pu4Error=CLI_L3VPN_RTE_ENTRY_INVALID;
                                return OSIX_FAILURE;
                        }
                        if(L3VPN_P_BGPROUTELABEL_POLICY(pL3VpnBgpRouteLabelEntry) !=
                                        L3VPN_BGP4_POLICY_PER_VRF)
                        {
                                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                                        "%s : %d Failed\n",__FUNCTION__,__LINE__));
                                *pu4Error=CLI_L3VPN_RTE_ENTRY_INVALID;
                                return OSIX_FAILURE;

                        }
                        break;
                case DESTROY:
                        if(pL3VpnBgpRouteLabelEntry == NULL)
                        {
                                *pu4Error=CLI_L3VPN_RTE_ENTRY_INVALID;

                                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                                        "%s : %d Failed\n",__FUNCTION__,__LINE__));
                                return OSIX_FAILURE;
                        }
                        break;
                default:
                       L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Wrong Operation.\r\n"));
                       return OSIX_FAILURE;

        }

        return OSIX_SUCCESS;
}

