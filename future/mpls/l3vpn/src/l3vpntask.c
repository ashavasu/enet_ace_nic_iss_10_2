/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntask.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpntask.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 * Description:This file contains procedures related to
 *             L3VPN - Task Initialization
 *******************************************************************/

#include "l3vpninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnTaskSpawnL3vpnTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn L3vpn Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if L3VPN Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
L3vpnTaskSpawnL3vpnTask (INT1 *pi1Arg)
{

    UNUSED_PARAM (pi1Arg);
    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnTaskSpawnL3vpnTask\n"));

    /* task initializations */
    if (L3vpnMainTaskInit () == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, " !!!!! L3VPN TASK INIT FAILURE  !!!!! \n"));
        L3vpnMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* spawn l3vpn task */

    /*if (OsixCreateTask ((const UINT1 *) L3VPN_TASK_NAME, L3VPN_TASK_PRIORITY,
       OSIX_DEFAULT_STACK_SIZE,
       L3vpnMainTask, NULL, OSIX_DEFAULT_TASK_MODE,
       (tOsixTaskId *) & (gL3vpnGlobals.l3vpnTaskId))
       == OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       } */

    if (OsixTskIdSelf (&L3VPN_TASK_ID) == OSIX_FAILURE)
    {
		L3VPN_TRC((L3VPN_MAIN_TRC,"Error in getting task Id for L3vpn\n"));
        L3vpnMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    L3vpnTaskRegisterL3vpnMibs();
    L3vpnTaskRegisterL3vpnMibs ();
    lrInitComplete (OSIX_SUCCESS);
    L3vpnMainTask ();
    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "EXIT:L3vpnTaskSpawnL3vpnTask\n"));
    return;
}


PUBLIC INT4 L3vpnTaskRegisterL3vpnMibs ()
{
     RegisterSTDL3V();
	 return OSIX_SUCCESS;
}


/*------------------------------------------------------------------------*/
/*                        End of the file  l3vpntask.c                     */
/*------------------------------------------------------------------------*/
