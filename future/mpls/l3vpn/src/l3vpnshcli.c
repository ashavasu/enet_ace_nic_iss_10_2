/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnshcli.c,v 1.6 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * Id: l3vpnshcli.c 
 *
 * Description: This file contains the L3vpn show commands related routines
 * *********************************************************************/

#include "l3vpninc.h"
#include "l3vpncli.h"

tL3vpnMplsL3VpnVrfRteEntry gL3vpnMplsVrfRteEntry;

/****************************************************************************
 * Function    :  cli_process_L3vpn_show_cmd
 * Description :  This function is exported to CLI module to handle the
 L3VPN cli show commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
cli_process_L3vpn_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[L3VPN_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    UINT1              *pu1MplsL3VpnVrfContextName = NULL;
    UINT4               u4MplsL3VpnVrfNameLen = 0;
    UINT4               u4VrfId = 0;

    UNUSED_PARAM (u4CmdType);
#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, L3vpnMainTaskLock, L3vpnMainTaskUnLock);
#endif
    L3VPN_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    UNUSED_PARAM (u4IfIndex);

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == L3VPN_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_L3VPN_SHOW_VRF:
            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            u4MplsL3VpnVrfNameLen = STRLEN (pu1MplsL3VpnVrfContextName);
            if (L3vpnCliProcessShowMplsL3VpnVrf
                (CliHandle, pu1MplsL3VpnVrfContextName,
                 u4MplsL3VpnVrfNameLen) != CLI_SUCCESS)
            {
                L3VPN_UNLOCK;
                return CLI_FAILURE;
            }
            break;
        case CLI_L3VPN_SHOW_VRF_ALL:
            if (L3vpnCliProcessShowMplsL3VpnVrfAll (CliHandle) != CLI_SUCCESS)
            {
                L3VPN_UNLOCK;
                return CLI_FAILURE;
            }
            break;
        case CLI_L3VPN_SHOW_VRF_RT:
            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            L3VpnRtTableShow (CliHandle, pu1MplsL3VpnVrfContextName);
            break;

        case CLI_L3VPN_SHOW_VRF_IF_CONF:
            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            L3VpnIfConfTableShow (CliHandle, pu1MplsL3VpnVrfContextName);
            break;

        case CLI_L3VPN_SHOW_EGRESS_ROUTES:
            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            if (pu1MplsL3VpnVrfContextName != NULL)
            {
                if (VcmIsVrfExist (pu1MplsL3VpnVrfContextName, &u4VrfId) !=
                    OSIX_FALSE)
                {
                    L3VpnBgpShowRouteLabelTable (CliHandle, u4VrfId);
                }
                else
                {
                    CliPrintf (CliHandle, "\r%% VRF-Name does not exist!\n");
                    i4RetStatus = CLI_FAILURE;
                }
            }
            else
            {
                u4VrfId = 0;
                L3VpnBgpShowRouteLabelTable (CliHandle, u4VrfId);
            }
            break;

        case CLI_L3VPN_SHOW_RTE_TABLE:
            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            L3VpnIngressRtTableShow (CliHandle, pu1MplsL3VpnVrfContextName);
            break;

        case CLI_L3VPN_SHOW_VRF_PERF_TABLE:
            pu1MplsL3VpnVrfContextName = (UINT1 *) args[0];
            L3VpnVrfPerfTableShow (CliHandle, pu1MplsL3VpnVrfContextName);
            break;

        case CLI_L3VPN_SHOW_BINDING_RSVP_MAP_TABLE:
            L3VpnRsvpShowMapLabelTable (CliHandle);
            break;

        default:
            i4RetStatus = CLI_FAILURE;
            break;
    };

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_L3VPN)
            && (u4ErrCode < CLI_L3VPN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       L3vpnCliErrString[CLI_ERR_OFFSET_L3VPN (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif

    L3VPN_UNLOCK;

    return i4RetStatus;

}

INT4
L3VpnRtTableShow (tCliHandle CliHandle, UINT1 *pu1VrfName)
{
    tL3vpnMplsL3VpnVrfRTEntry L3vpnMibMplsL3VpnVrfRTEntry;
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMibMplsL3VpnVrfRTEntry = NULL;
    INT4                i4VrfNameLen = 0;

    MEMSET (&L3vpnMibMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    CliPrintf (CliHandle,
               "------------------------------------------------------------\n");
    CliPrintf (CliHandle, "%-20s %-15s %-15s %-30s\r\n", "VrfName", "RT-Index",
               "Policy", "RT");
    CliPrintf (CliHandle,
               "------------------------------------------------------------\n");

    if (pu1VrfName == NULL)        /* Show RT info for all VRF's configured */
    {
        pL3vpnMibMplsL3VpnVrfRTEntry = RBTreeGetFirst (L3VPN_RT_TABLE);
        while (pL3vpnMibMplsL3VpnVrfRTEntry != NULL)
        {
            L3VpnCliPrintRTInfo (CliHandle, pL3vpnMibMplsL3VpnVrfRTEntry);
            pL3vpnMibMplsL3VpnVrfRTEntry =
                RBTreeGetNext (L3VPN_RT_TABLE, pL3vpnMibMplsL3VpnVrfRTEntry,
                               NULL);
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else                        /* Show RT info for the specified VRF */
    {
        i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
        MEMCPY (L3VPN_VRF_NAME (L3vpnMibMplsL3VpnVrfRTEntry), pu1VrfName,
                i4VrfNameLen);
        L3VPN_VRF_NAME_LEN (L3vpnMibMplsL3VpnVrfRTEntry) = i4VrfNameLen;
        pL3vpnMibMplsL3VpnVrfRTEntry =
            RBTreeGetNext (L3VPN_RT_TABLE, &L3vpnMibMplsL3VpnVrfRTEntry, NULL);

        while (pL3vpnMibMplsL3VpnVrfRTEntry != NULL)
        {
            if (MEMCMP
                (L3VPN_P_VRF_NAME (pL3vpnMibMplsL3VpnVrfRTEntry), pu1VrfName,
                 L3VPN_MAX_VRF_NAME_LEN) == 0)
            {
                L3VpnCliPrintRTInfo (CliHandle, pL3vpnMibMplsL3VpnVrfRTEntry);
            }
            else
            {
                break;
            }
            pL3vpnMibMplsL3VpnVrfRTEntry =
                RBTreeGetNext (L3VPN_RT_TABLE, pL3vpnMibMplsL3VpnVrfRTEntry,
                               NULL);
        }
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

VOID
L3VpnCliPrintRTInfo (tCliHandle CliHandle,
                     tL3vpnMplsL3VpnVrfRTEntry * pL3vpnMibMplsL3VpnVrfRTEntry)
{
    INT4                i4Policy = 0;
    UINT1               au1PolicyString[MAX_RT_POLICY_LEN];
    UINT1               au1RTString[100];
    UINT2               u2ASN = 0;
    UINT4               u4AsignedNumber = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4ASN = 0;

    MEMSET (au1RTString, 0, L3VPN_MAX_RT_LEN);
    MEMSET (au1PolicyString, 0, MAX_RT_POLICY_LEN);
    if (pL3vpnMibMplsL3VpnVrfRTEntry == NULL)
    {
        return;
    }

    i4Policy = L3VPN_P_RT_TYPE (pL3vpnMibMplsL3VpnVrfRTEntry);
    if (i4Policy == CLI_MPLS_L3VPN_RT_IMPORT)
    {
        MEMCPY (au1PolicyString, "IMPORT", 6);
    }
    if (i4Policy == CLI_MPLS_L3VPN_RT_EXPORT)
    {
        MEMCPY (au1PolicyString, "EXPORT", 6);
    }
    if (i4Policy == CLI_MPLS_L3VPN_RT_BOTH)
    {
        MEMCPY (au1PolicyString, "BOTH", 4);
    }

    if (L3VPN_RT_TYPE_0 == L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[0])
    {
        MEMCPY (&u2ASN, &L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[2],
                sizeof (UINT2));
        MEMCPY (&u4AsignedNumber,
                &L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[4],
                sizeof (UINT4));
        u2ASN = OSIX_NTOHS (u2ASN);
        u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
        SPRINTF ((char *) au1RTString, "%hu:%u", u2ASN, u4AsignedNumber);
    }
    else if (L3VPN_RT_TYPE_1 ==
             L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[0])
    {
        MEMCPY (&u2AsignedNumber,
                &L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[6],
                sizeof (UINT2));
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RTString, "%u.%u.%u.%u:%hu",
                 L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[2],
                 L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[3],
                 L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[4],
                 L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[5],
                 u2AsignedNumber);
    }
    else
    {
        MEMCPY (&u4ASN, &L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[2],
                sizeof (UINT4));
        MEMCPY (&u2AsignedNumber,
                &L3VPN_P_RT_VALUE (pL3vpnMibMplsL3VpnVrfRTEntry)[6],
                sizeof (UINT2));
        u4ASN = OSIX_NTOHL (u4ASN);
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RTString, "%u:%hu", u4ASN, u2AsignedNumber);
    }

    CliPrintf (CliHandle, "%-20s %-15d %-15s %-30s\r\n",
               L3VPN_P_VRF_NAME (pL3vpnMibMplsL3VpnVrfRTEntry),
               L3VPN_P_RT_INDEX (pL3vpnMibMplsL3VpnVrfRTEntry),
               au1PolicyString, au1RTString);

}

VOID
L3VpnIfConfTableShow (tCliHandle CliHandle, UINT1 *pu1VrfName)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnMplsL3VpnIfConfEntry L3vpnMplsL3VpnIfConfEntry;
    INT4                i4VrfNameLen = 0;

    MEMSET (&L3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    CliPrintf (CliHandle,
               "----------------------------------------------------\n");
    CliPrintf (CliHandle, "%-20s %-15s %-30s\r\n", "VrfName", "If Index",
               "Status");
    CliPrintf (CliHandle,
               "----------------------------------------------------\n");

    if (pu1VrfName != NULL)        /* Show IfConf info for the desired VRF */
    {
        i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
        MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnIfConfEntry), pu1VrfName,
                i4VrfNameLen);
        L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnIfConfEntry) = i4VrfNameLen;

        pL3vpnMplsL3VpnIfConfEntry =
            RBTreeGetNext (L3VPN_IF_CONF_TABLE, &L3vpnMplsL3VpnIfConfEntry,
                           NULL);
        while (pL3vpnMplsL3VpnIfConfEntry != NULL)
        {
            if (MEMCMP
                (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnIfConfEntry), pu1VrfName,
                 L3VPN_MAX_VRF_NAME_LEN) == 0)
            {
                L3VpnCliPrintIfConfInfo (CliHandle, pL3vpnMplsL3VpnIfConfEntry);
            }
            else
            {
                break;
            }
            pL3vpnMplsL3VpnIfConfEntry =
                RBTreeGetNext (L3VPN_IF_CONF_TABLE, pL3vpnMplsL3VpnIfConfEntry,
                               NULL);
        }

    }
    else                        /* Show If Conf table for all the VRF's */
    {
        pL3vpnMplsL3VpnIfConfEntry = RBTreeGetFirst (L3VPN_IF_CONF_TABLE);
        while (pL3vpnMplsL3VpnIfConfEntry != NULL)
        {
            L3VpnCliPrintIfConfInfo (CliHandle, pL3vpnMplsL3VpnIfConfEntry);
            pL3vpnMplsL3VpnIfConfEntry =
                RBTreeGetNext (L3VPN_IF_CONF_TABLE, pL3vpnMplsL3VpnIfConfEntry,
                               NULL);
        }
    }

}

VOID
L3VpnCliPrintIfConfInfo (tCliHandle CliHandle,
                         tL3vpnMplsL3VpnIfConfEntry *
                         pL3vpnMplsL3VpnIfConfEntry)
{

    UINT1               u1OperStatus = 0;
    UINT1               au1OperString[5];

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return;
    }

    MEMSET (au1OperString, 0, 5);

    CfaGetIfOperStatus ((UINT4)
                        L3VPN_P_IF_CONF_IF_INDEX (pL3vpnMplsL3VpnIfConfEntry),
                        &u1OperStatus);
    if (u1OperStatus == CFA_IF_UP)
    {
        MEMCPY (au1OperString, "Up", 2);
    }
    else
    {
        MEMCPY (au1OperString, "Down", 4);
    }
    CliPrintf (CliHandle, "%-20s %-15d %-30s\r\n",
               L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnIfConfEntry),
               L3VPN_P_IF_CONF_IF_INDEX (pL3vpnMplsL3VpnIfConfEntry),
               au1OperString);

}

INT4
L3vpnCliProcessShowMplsL3VpnVrf (tCliHandle CliHandle,
                                 UINT1 *pu1MplsL3VpnVrfName,
                                 UINT4 u4MplsL3VpnVrfNameLen)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    pL3vpnMplsL3VpnVrfEntry =
        MplsL3VpnApiGetVrfTableEntry (pu1MplsL3VpnVrfName,
                                      (INT4) u4MplsL3VpnVrfNameLen);
    if (L3vpnCliShowMplsL3VpnVrf (CliHandle, pL3vpnMplsL3VpnVrfEntry) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
L3vpnCliProcessShowMplsL3VpnVrfAll (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfNameSrc;
    UINT1               au1MplsL3VpnVrfName[L3VPN_MAX_VRF_NAME_LEN];

    MplsL3VpnVrfNameSrc.pu1_OctetList = au1MplsL3VpnVrfName;
    if (nmhGetFirstIndexMplsL3VpnVrfTable (&MplsL3VpnVrfNameSrc) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (L3vpnCliProcessShowMplsL3VpnVrf
            (CliHandle, MplsL3VpnVrfNameSrc.pu1_OctetList,
             (UINT4) MplsL3VpnVrfNameSrc.i4_Length) != CLI_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    while (nmhGetNextIndexMplsL3VpnVrfTable
           (&MplsL3VpnVrfNameSrc, &MplsL3VpnVrfNameSrc) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

VOID
CliL3VpnConvertTimetoString (UINT4 u4Time, UINT1 *pi1Time)
{

    UINT4               u4Hrs;
    UINT4               u4Mins;
    UINT4               u4Secs;

    MEMSET (pi1Time, 0, MAX_CLI_L3VPN_TIME_STRING);

    if (u4Time == 0)
    {

        SNPRINTF ((CHR1 *) pi1Time, MAX_CLI_L3VPN_TIME_STRING, "00:00:00");
        return;
    }

    u4Time = u4Time / 100;
    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;
    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;
    u4Hrs = u4Time % 24;

    SNPRINTF ((CHR1 *) pi1Time, MAX_CLI_L3VPN_TIME_STRING,
              "%02d:%02d:%02d", u4Hrs, u4Mins, u4Secs);
    return;
}

INT4
L3vpnCliShowMplsL3VpnVrf (tCliHandle CliHandle,
                          tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    UINT1               au1Status[3][10] = { {"up"}, {"down"} };
    UINT1               au1TimeString[MAX_CLI_L3VPN_TIME_STRING];
    UINT1               au1RDString[100];
    UINT2               u2ASN = 0;
    UINT4               u4AsignedNumber = 0;
    UINT2               u2AsignedNumber = 0;
    UINT4               u4ASN = 0;

    MEMSET (au1TimeString, 0, MAX_CLI_L3VPN_TIME_STRING);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (L3VPN_RD_TYPE_0 == L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[0])
    {
        MEMCPY (&u2ASN, &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[2],
                sizeof (UINT2));
        MEMCPY (&u4AsignedNumber, &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[4],
                sizeof (UINT4));
        u2ASN = OSIX_NTOHS (u2ASN);
        u4AsignedNumber = OSIX_NTOHL (u4AsignedNumber);
        SPRINTF ((char *) au1RDString, "%hu:%u", u2ASN, u4AsignedNumber);
    }
    else if (L3VPN_RD_TYPE_1 == L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[0])
    {
        MEMCPY (&u2AsignedNumber, &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[6],
                sizeof (UINT2));
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RDString, "%u.%u.%u.%u:%hu",
                 L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[2],
                 L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[3],
                 L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[4],
                 L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[5], u2AsignedNumber);
    }
    else
    {
        MEMCPY (&u4ASN, &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[2],
                sizeof (UINT4));
        MEMCPY (&u2AsignedNumber, &L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry)[6],
                sizeof (UINT2));
        u4ASN = OSIX_NTOHL (u4ASN);
        u2AsignedNumber = OSIX_NTOHS (u2AsignedNumber);
        SPRINTF ((char *) au1RDString, "%u:%hu", u4ASN, u2AsignedNumber);
    }

    CliPrintf (CliHandle, "\r\nVRF-NAME: %s\n",
               L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   Status:\n\r    Admin: %-4s Oper: %-4s ",
               au1Status[L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) -
                         1],
               au1Status[L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry) -
                         1]);
    CliPrintf (CliHandle, "\n\r   Vrf Description: %s\n",
               L3VPN_P_VRF_DESCRIPTION (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   RD: %s\n", au1RDString);
    CliPrintf (CliHandle, "\r   Max Routes: %d\n",
               L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   Mid Route Threshold: %d\n",
               L3VPN_P_VRF_MID_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   High Route Threshold: %d\n",
               L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   Active Interfaces: %d\n",
               L3VPN_P_VRF_ACTIVE_INTERFACES (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   Associated Interfaces: %d\n",
               L3VPN_P_VRF_ASSOCIATED_INTERFACES (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   Row status: %d\n",
               L3VPN_P_VRF_ROW_STATUS (pL3vpnMplsL3VpnVrfEntry));
    CliPrintf (CliHandle, "\r   Vpn Id: %s\n",
               L3VPN_P_VRF_VPN_ID (pL3vpnMplsL3VpnVrfEntry));
    CliL3VpnConvertTimetoString (L3VPN_P_VRF_CREATION_TIME
                                 (pL3vpnMplsL3VpnVrfEntry), au1TimeString);
    CliPrintf (CliHandle, "\r   Creation Time: %s\n", au1TimeString);

    return CLI_SUCCESS;
}

INT4
L3VpnBgpShowRouteLabelTable (tCliHandle CliHandle, UINT4 u4VrfId)
{

    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
    if (u4VrfId != 0)
    {
        MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));
        pL3VpnBgpRouteLabelEntry =
            L3vpnGetNextBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

        while (pL3VpnBgpRouteLabelEntry != NULL)
        {

            if (L3VPN_P_BGPROUTELABELVRF_VRFID
                (pL3VpnBgpRouteLabelEntry) == u4VrfId)
            {
                L3VpnBgpShowRouteLabel (CliHandle, pL3VpnBgpRouteLabelEntry);
                pL3VpnBgpRouteLabelEntry =
                    L3vpnGetNextBgpRouteLabelTable (pL3VpnBgpRouteLabelEntry);
            }
            else
            {
                pL3VpnBgpRouteLabelEntry =
                    L3vpnGetNextBgpRouteLabelTable (pL3VpnBgpRouteLabelEntry);

            }
        }
    }
    else                        /* Show Routes for all the VRF's */
    {
        MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));
        pL3VpnBgpRouteLabelEntry =
            L3vpnGetNextBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

        while (pL3VpnBgpRouteLabelEntry != NULL)
        {
            L3VpnBgpShowRouteLabel (CliHandle, pL3VpnBgpRouteLabelEntry);
            L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) =
                L3VPN_P_BGPROUTELABEL_LABEL (pL3VpnBgpRouteLabelEntry);
            pL3VpnBgpRouteLabelEntry =
                L3vpnGetNextBgpRouteLabelTable (pL3VpnBgpRouteLabelEntry);

        }
    }

    return CLI_SUCCESS;
}

INT4
L3VpnBgpShowRouteLabel (tCliHandle CliHandle,
                        tL3VpnBgpRouteLabelEntry * pL3VpnBgpRouteLabelEntry)
{
    UINT1               au1Status[3][10] = { {"per vrf"}, {"per route"} };
    UINT1              *pu1Temp = NULL;
    UINT1               au1MacAdd[MAX_MAC_LENGTH];
    CHR1               *pNextHop = NULL;
    UINT4               u4NextHop = 0;

    MEMSET (au1MacAdd, 0, MAX_MAC_LENGTH);

    pu1Temp = &au1MacAdd[0];
    CliPrintf (CliHandle, "\r\nLabel: %d\n",
               L3VPN_P_BGPROUTELABEL_LABEL (pL3VpnBgpRouteLabelEntry));
    CliPrintf (CliHandle, "\r   Policy: %s \n",
               au1Status[L3VPN_P_BGPROUTELABEL_POLICY (pL3VpnBgpRouteLabelEntry)
                         - 1]);

    switch (L3VPN_P_BGPROUTELABEL_POLICY (pL3VpnBgpRouteLabelEntry))
    {
        case L3VPN_BGP4_POLICY_PER_VRF:
            CliPrintf (CliHandle, "VRF-Id: %d\r\n",
                       L3VPN_P_BGPROUTELABELVRF_VRFID
                       (pL3VpnBgpRouteLabelEntry));
            CliPrintf (CliHandle, "VRF-Name: %s\r\n",
                       L3VPN_P_BGPROUTELABELVRF_VNAME
                       (pL3VpnBgpRouteLabelEntry));
            break;
        case L3VPN_BGP4_POLICY_PER_ROUTE:
            CliPrintf (CliHandle, "VRF-Id: %d\r\n",
                       L3VPN_P_BGPROUTELABELRTE_CXTID
                       (pL3VpnBgpRouteLabelEntry));
            CliPrintf (CliHandle, "VRF-Name: %s\r\n",
                       L3VPN_P_BGPROUTELABELRTE_NAME
                       (pL3VpnBgpRouteLabelEntry));

            CONVERT_TO_INTEGER (L3VPN_P_BGPROUTELABELRTE_NXTHOP
                                (pL3VpnBgpRouteLabelEntry).au1Address,
                                u4NextHop);
            u4NextHop = OSIX_NTOHL (u4NextHop);
            CLI_CONVERT_IPADDR_TO_STR (pNextHop, u4NextHop);

            CliPrintf (CliHandle, "Next hop Ip: %s\n", pNextHop);

            CliPrintf (CliHandle, "Interface Index: %d\n",
                       L3VPN_P_BGPROUTELABELRTE_IF_IDX
                       (pL3VpnBgpRouteLabelEntry));
            PrintMacAddress (L3VPN_P_BGPROUTELABELRTE_DSTMAC
                             (pL3VpnBgpRouteLabelEntry), pu1Temp);
            CliPrintf (CliHandle, "Destination Mac is %s\r\n", pu1Temp);

            break;
        default:

            break;
    }

    CliPrintf (CliHandle, "\r========================================\n");

    return CLI_SUCCESS;
}

VOID
L3VpnIngressRtTableShow (tCliHandle CliHandle, UINT1 *pu1VrfName)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    INT4                i4VrfNameLen = 0;
    CliPrintf (CliHandle, "--------------------------------------------------"
               "------------------------------------------------------------------------\n");

    CliPrintf (CliHandle,
               "%-15s %-15s %-15s %-15s %-15s %-15s\r\n",
               "VrfName", "Destination", "Prefix Len", "NextHop", "LSP-Label",
               "VPN-Label");

    CliPrintf (CliHandle, "---------------------------------------------------"
               "-----------------------------------------------------------------------\n");

    if (pu1VrfName != NULL)        /* Show Routes for the desired VRF */
    {
        MEMSET (&gL3vpnMplsVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
        i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
        MEMCPY (L3VPN_VRF_NAME (gL3vpnMplsVrfRteEntry), pu1VrfName,
                i4VrfNameLen);
        L3VPN_VRF_NAME_LEN (gL3vpnMplsVrfRteEntry) = i4VrfNameLen;
        pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
            RBTreeGetNext (L3VPN_RTE_TABLE, &gL3vpnMplsVrfRteEntry, NULL);

        while (pL3vpnMplsL3VpnVrfRteEntry != NULL)
        {
            if (MEMCMP
                (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry), pu1VrfName,
                 L3VPN_MAX_VRF_NAME_LEN) == 0)
            {
                L3VpnPrintRteEntry (CliHandle, pL3vpnMplsL3VpnVrfRteEntry);
            }
            else
            {
                break;
            }
            pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
                RBTreeGetNext (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry,
                               NULL);
        }
    }
    else                        /* Show Routes for all the VRF's */
    {
        pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
            RBTreeGetFirst (L3VPN_RTE_TABLE);
        while (pL3vpnMplsL3VpnVrfRteEntry != NULL)
        {
            L3VpnPrintRteEntry (CliHandle, pL3vpnMplsL3VpnVrfRteEntry);
            pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
                RBTreeGetNext (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry,
                               NULL);
        }
    }
}

VOID
L3VpnPrintRteEntry (tCliHandle CliHandle,
                    tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry)
{
    UINT1               au1DestIpString[16];
    UINT1               au1NextHopString[16];
    UINT4               u4VpnLabel = 0;
    UINT4               u4LspLabel = 0;
    UINT1              *pu1RteXcPointer = NULL;
    UINT4               u4XcIndex = 0;
    UINT4               u4InSegmentIndex = 0;
    UINT4               u4OutSegmentIndex = 0;
    tXcEntry           *pXcEntry = NULL;
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry1 = NULL;
    tOutSegment        *pOutSegment = NULL;

    MEMSET (au1DestIpString, 0, 10);
    MEMSET (au1NextHopString, 0, 10);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return;
    }
    SPRINTF ((char *) au1DestIpString, "%d.%d.%d.%d",
             L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry)[0],
             L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry)[1],
             L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry)[2],
             L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry)[3]);

    SPRINTF ((char *) au1NextHopString, "%d.%d.%d.%d",
             L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry)[0],
             L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry)[1],
             L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry)[2],
             L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry)[3]);

    pu1RteXcPointer = L3VPN_P_RTE_XC_POINTER (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (&u4XcIndex, pu1RteXcPointer, sizeof (UINT4));
    u4XcIndex = OSIX_NTOHL (u4XcIndex);

    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    MEMCPY (&u4InSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4InSegmentIndex = OSIX_NTOHL (u4InSegmentIndex);

    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    MEMCPY (&u4OutSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4OutSegmentIndex = OSIX_NTOHL (u4OutSegmentIndex);

    MPLS_CMN_LOCK ();
    pXcEntry =
        MplsGetXCTableEntry (u4XcIndex, u4InSegmentIndex, u4OutSegmentIndex);

    if (pXcEntry != NULL && pXcEntry->pOutIndex != NULL)
    {
        pOutSegment = pXcEntry->pOutIndex;
        u4LspLabel = pOutSegment->u4Label;
    }

    if (pXcEntry != NULL && pXcEntry->mplsLabelIndex != NULL)
    {
        pLblStkEntry = pXcEntry->mplsLabelIndex;
        pLblEntry1 = (tLblEntry *) TMO_SLL_First (&(pLblStkEntry->LblList));

        if (pLblEntry1 != NULL)
        {
            u4VpnLabel = pLblEntry1->u4Label;
        }
    }
    MPLS_CMN_UNLOCK ();

    CliPrintf (CliHandle,
               "%-15s %-15s %-15d %-15s %-15d %-15d\r\n",
               L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry), au1DestIpString,
               L3VPN_P_RTE_INET_CIDR_PREFIX_LEN (pL3vpnMplsL3VpnVrfRteEntry),
               au1NextHopString, u4LspLabel, u4VpnLabel);
}

VOID
L3VpnVrfPerfTableShow (tCliHandle CliHandle, UINT1 *pu1VrfName)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
    INT4                i4VrfNameLen = 0;

    CliPrintf (CliHandle,
               "----------------------------------------------------------------------------------------\r\n");
    CliPrintf (CliHandle, "%-15s %-15s %-15s %-15s %-15s %-10s\r\n", "VrfName",
               "Routes_Added", "Routes_Deleted", "Current_Routes",
               "Routes_Dropped", "Discont_Time");
    CliPrintf (CliHandle,
               "----------------------------------------------------------------------------------------\r\n");

    if (pu1VrfName != NULL)        /* Show performance stats for a single VRF */
    {
        MEMSET (&L3vpnMplsL3VpnVrfPerfEntry, 0,
                sizeof (tL3vpnMplsL3VpnVrfPerfEntry));
        i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
        MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfPerfEntry), pu1VrfName,
                i4VrfNameLen);
        L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfPerfEntry) = i4VrfNameLen;

        pL3vpnMplsL3VpnVrfPerfEntry =
            (tL3vpnMplsL3VpnVrfPerfEntry *) RBTreeGetFirst (L3VPN_PERF_TABLE);
        while (pL3vpnMplsL3VpnVrfPerfEntry != NULL)
        {
            if (MEMCMP
                (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfPerfEntry), pu1VrfName,
                 L3VPN_MAX_VRF_NAME_LEN) == 0)
            {
                L3VpnPrintVrfPerfEntry (CliHandle, pL3vpnMplsL3VpnVrfPerfEntry);
                break;
            }
            else                /* Perf entry is not for the required VRF */
            {
                pL3vpnMplsL3VpnVrfPerfEntry = (tL3vpnMplsL3VpnVrfPerfEntry *)
                    RBTreeGetNext (L3VPN_PERF_TABLE,
                                   pL3vpnMplsL3VpnVrfPerfEntry, NULL);
            }
        }
    }
    else                        /* Show stats for all the VRF's */
    {
        pL3vpnMplsL3VpnVrfPerfEntry =
            (tL3vpnMplsL3VpnVrfPerfEntry *) RBTreeGetFirst (L3VPN_PERF_TABLE);

        while (pL3vpnMplsL3VpnVrfPerfEntry != NULL)
        {
            L3VpnPrintVrfPerfEntry (CliHandle, pL3vpnMplsL3VpnVrfPerfEntry);
            pL3vpnMplsL3VpnVrfPerfEntry = (tL3vpnMplsL3VpnVrfPerfEntry *)
                RBTreeGetNext (L3VPN_PERF_TABLE, pL3vpnMplsL3VpnVrfPerfEntry,
                               NULL);
        }

    }
}

VOID
L3VpnPrintVrfPerfEntry (tCliHandle CliHandle,
                        tL3vpnMplsL3VpnVrfPerfEntry *
                        pL3vpnMplsL3VpnVrfPerfEntry)
{
    CliPrintf (CliHandle, "%-15s %-15d %-15d %-15d %-15d %-10d\r\n",
               L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfPerfEntry),
               L3VPN_P_PERF_ROUTES_ADDED (pL3vpnMplsL3VpnVrfPerfEntry),
               L3VPN_P_PERF_DELETED_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry),
               L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry),
               L3VPN_P_PERF_ROUTES_DROPPED (pL3vpnMplsL3VpnVrfPerfEntry),
               L3VPN_P_PERF_DISCONT_TIME (pL3vpnMplsL3VpnVrfPerfEntry));
}

/****************************************************************************
 * Function    :  L3VpnRsvpShowMapLabelTable
 * Description :  This function is exported to CLI module to handle the
   L3VPN RSVP static binding show command to all the prefix entry.

 * Input       :  CliHandle

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
L3VpnRsvpShowMapLabelTable (tCliHandle CliHandle)
{
    BOOL1               b1Flag = TRUE;
    UINT4               u4L3Intf = 0;
    CHR1               *pc1Prefix = NULL;
    CHR1               *pc1Mask = NULL;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;
    tL3VpnRsvpMapLabelEntry L3VpnRsvpMapLabelEntry;
    tCfaIfInfo          IfInfo;
    tXcEntry           *pXcEntry = NULL;

    MEMSET (&L3VpnRsvpMapLabelEntry, 0, sizeof (tL3VpnRsvpMapLabelEntry));

    pL3VpnRsvpMapLabelEntry = L3vpnGetFirstRsvpMapLabelTable ();
    while (pL3VpnRsvpMapLabelEntry != NULL)
    {
        if (b1Flag != FALSE)
        {
            b1Flag = FALSE;
            CliPrintf (CliHandle,
                       "-----------------------------------------------------------------------------------------------\n");
            CliPrintf (CliHandle, "%-30s %-25s %-25s %-20s\r\n", "Prefix",
                       "Mask", "TunnelIndex", "OutInterface");
            CliPrintf (CliHandle,
                       "-----------------------------------------------------------------------------------------------\n");
        }
        CLI_CONVERT_IPADDR_TO_STR (pc1Prefix,
                                   L3VPN_P_RSVPMAPLABEL_PREFIX
                                   (pL3VpnRsvpMapLabelEntry));
        CliPrintf (CliHandle, "%-27s", pc1Prefix);

        CLI_CONVERT_IPADDR_TO_STR (pc1Mask,
                                   L3VPN_P_RSVPMAPLABEL_MASK
                                   (pL3VpnRsvpMapLabelEntry));
        CliPrintf (CliHandle, "%-34s", pc1Mask);

        CliPrintf (CliHandle, "%-25d", pL3VpnRsvpMapLabelEntry->u4TnlIndex);

        if (((pXcEntry = MplsGetXCEntryByDirection
              (pL3VpnRsvpMapLabelEntry->u4TnlXcIndex,
               (eDirection) MPLS_DIRECTION_ANY)) != NULL)
            && (XC_OUTINDEX (pXcEntry) != NULL))
        {
            if (MplsGetL3Intf (OUTSEGMENT_IF_INDEX
                               (XC_OUTINDEX (pXcEntry)),
                               &u4L3Intf) == MPLS_SUCCESS)
            {
                if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-20s\r\n", IfInfo.au1IfName);
                }
            }
        }
        else
        {
            CliPrintf (CliHandle, " - \r\n");
        }

        L3VPN_RSVPMAPLABEL_PREFIX (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_PREFIX (pL3VpnRsvpMapLabelEntry);
        L3VPN_RSVPMAPLABEL_MASK (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_MASK (pL3VpnRsvpMapLabelEntry);
        L3VPN_RSVPMAPLABEL_PREFIX_TYPE (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_PREFIX_TYPE (pL3VpnRsvpMapLabelEntry);
        L3VPN_RSVPMAPLABEL_MASK_TYPE (L3VpnRsvpMapLabelEntry) =
            L3VPN_P_RSVPMAPLABEL_MASK_TYPE (pL3VpnRsvpMapLabelEntry);

        pL3VpnRsvpMapLabelEntry =
            L3vpnGetNextRsvpMapLabelTable (&L3VpnRsvpMapLabelEntry);

    }
    return CLI_SUCCESS;
}
