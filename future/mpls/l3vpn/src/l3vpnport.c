/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnport.c,v 1.12 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

#include "l3vpninc.h"
#include "mplsnp.h"
#include "mplsred.h"
#include "arp.h"
#include  "mplsnpwr.h"

UINT4
MplsL3vpnIngressMap (tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry)
{
    if (MplsL3vpnHwIngressMap (pL3vpnMplsL3VpnVrfRteEntry, NULL) !=
        L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnEgressMap (tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry)
{
    if (MplsL3vpnHwEgressMap (pL3VPNRouteEntry, NULL) != L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnIngressUnMap (tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry)
{
    if (MplsL3vpnHwIngressUnMap (pL3vpnMplsL3VpnVrfRteEntry, NULL) !=
        L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }

    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnEgressUnMap (tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry)
{
    if (MplsL3vpnHwEgressUnMap (pL3VPNRouteEntry, NULL) != L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnHwIngressMap (tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry,
                       VOID *pSlotInfo)
{
    tMplsHwL3vpnIgressInfo MplsHwL3vpnIgressInfo;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4L3Intf = 0;
    tLblEntry          *pLblEntry = NULL;
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    UINT1              *pu1RteXcPointer = NULL;
    UINT4               u4XcIndex = 0;
    UINT4               u4InSegmentIndex = 0;
    UINT4               u4OutSegmentIndex = 0;
    UINT4               u4VrfId = 0;

    MEMSET (au1ZeroMac, 0, MAC_ADDR_LEN);
    MEMSET (&MplsHwL3vpnIgressInfo, 0, sizeof (tMplsHwL3vpnIgressInfo));

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    /** update the NextHop Autonous System & Metric **/
    MplsHwL3vpnIgressInfo.u4NhAs =
        L3VPN_P_RTE_INET_CIDR_NH_AS (pL3vpnMplsL3VpnVrfRteEntry);
    MplsHwL3vpnIgressInfo.i4Metric =
        L3VPN_P_RTE_INET_CIDR_METRIC1 (pL3vpnMplsL3VpnVrfRteEntry);

    /** l3vpn_mpls_note_1 : To do : update the VRF Id**/

    if (VcmIsVrfExist (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry), &u4VrfId)
        == VCM_FALSE)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "%s : %d : VcmIsVrfExist() : return Failure\n",
                    __FUNCTION__, __LINE__));
        return L3VPN_FAILURE;
    }

    MplsHwL3vpnIgressInfo.u4VrfId = u4VrfId;

    MplsHwL3vpnIgressInfo.u1Action = L3VPN_BGP4_ROUTE_ADD;

    /** update the Destination Address**/
    MplsHwL3vpnIgressInfo.IpPrefix.u2AddressLen =
        (UINT2) L3VPN_P_RTE_INET_CIDR_DEST_LEN (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (MplsHwL3vpnIgressInfo.IpPrefix.au1Address,
            L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry),
            L3VPN_P_RTE_INET_CIDR_DEST_LEN (pL3vpnMplsL3VpnVrfRteEntry));

    MplsHwL3vpnIgressInfo.IpPrefix.u1PrefixLen =
        (UINT1) L3VPN_P_RTE_INET_CIDR_PREFIX_LEN (pL3vpnMplsL3VpnVrfRteEntry);

    /** update the NextHop  Address**/
    MplsHwL3vpnIgressInfo.NextHop.u2AddressLen =
        (UINT2) L3VPN_P_RTE_INET_CIDR_NH_LEN (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (MplsHwL3vpnIgressInfo.NextHop.au1Address,
            L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry),
            L3VPN_P_RTE_INET_CIDR_NH_LEN (pL3vpnMplsL3VpnVrfRteEntry));

    /** Update the NextHop: MacAddress and outInterface ***/
    pu1RteXcPointer = L3VPN_P_RTE_XC_POINTER (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (&u4XcIndex, pu1RteXcPointer, sizeof (UINT4));
    u4XcIndex = OSIX_HTONL (u4XcIndex);

    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    MEMCPY (&u4InSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4InSegmentIndex = OSIX_NTOHL (u4InSegmentIndex);

    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    MEMCPY (&u4OutSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4OutSegmentIndex = OSIX_NTOHL (u4OutSegmentIndex);

    MPLS_CMN_LOCK ();
    pXcEntry =
        MplsGetXCTableEntry (u4XcIndex, u4InSegmentIndex, u4OutSegmentIndex);

    /** get the Xc Index **/
    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "%s : %d : pXcEntry NULL : return Failure\n", __FUNCTION__,
                    __LINE__));
        MPLS_CMN_UNLOCK ();
        return L3VPN_FAILURE;
    }
    if (MEMCMP (pXcEntry->pOutIndex->au1NextHopMac, au1ZeroMac, MAC_ADDR_LEN)
        == MPLS_ZERO)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "%s : %d : MAC is not yet resolved for the underlying LSP\n",
                    __FUNCTION__, __LINE__));
        MPLS_CMN_UNLOCK ();
        return L3VPN_FAILURE;
    }
    else
    {
        MEMCPY (MplsHwL3vpnIgressInfo.au1DstMac,
                pXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);
    }

    if (pXcEntry->pOutIndex->u4Label == MPLS_IMPLICIT_NULL_LABEL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "%s : %d Handling for IMPLICIT NULL LABEL \n", __FUNCTION__,
                    __LINE__));
    }

    MplsHwL3vpnIgressInfo.u4EgrL3Intf = pXcEntry->pOutIndex->u4IfIndex;

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwL3vpnIgressInfo.u4EgrL3Intf, &u4L3Intf) !=
        MPLS_SUCCESS)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "%s : %s Returned Failure\n", __FUNCTION__,
                    "MplsGetL3Intf"));
    }

    /* Get the physical port for the corresponding Logical L3 interface. */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          &(MplsHwL3vpnIgressInfo.au1DstMac[0]),
                                          &(MplsHwL3vpnIgressInfo.u4OutPort))
        != MPLS_SUCCESS)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "%s : %s Returned Failure\n", __FUNCTION__,
                    "MplsGetPhyPortFromLogicalIfIndex"));
    }

    /** Update the MPLS Out Label List **/
    MplsHwL3vpnIgressInfo.MplsLabelList[0].MplsLabelType =
        MPLS_HW_LABEL_TYPE_GENERIC;
    MplsHwL3vpnIgressInfo.MplsLabelList[0].u.MplsShimLabel =
        pXcEntry->pOutIndex->u4Label;

    if (pXcEntry->mplsLabelIndex != NULL)
    {
        pLblEntry = (tLblEntry *)
            TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));

        if ((pLblEntry != NULL) &&
            (pLblEntry->u4Label != MPLS_IMPLICIT_NULL_LABEL))
        {
            MplsHwL3vpnIgressInfo.MplsLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
            MplsHwL3vpnIgressInfo.MplsLabelList[1].u.MplsShimLabel =
                pLblEntry->u4Label;
        }
    }

    MPLS_CMN_UNLOCK ();
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (MplsFsMplsMbsmHwL3vpnIngressMap (&MplsHwL3vpnIgressInfo, NULL) ==
            FNP_FAILURE)
        {
            return L3VPN_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtEgress (MplsHwL3vpnIgressInfo.
                                                 u4OutPort,
                                                 &(MplsHwL3vpnIgressInfo.
                                                   i4HwExpMapId));
            QosGetMplsExpProfileForPortAtIngress (MplsHwL3vpnIgressInfo.
                                                  u4OutPort,
                                                  &(MplsHwL3vpnIgressInfo.
                                                    i4HwPriMapId));
#endif
            if (MplsFsMplsHwL3vpnIngressMap (&MplsHwL3vpnIgressInfo) ==
                FNP_FAILURE)
            {
                return L3VPN_FAILURE;
            }
#endif
        }
    }
    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnHwEgressMap (tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry,
                      VOID *pSlotInfo)
{
    tMplsHwL3vpnEgressInfo MplsHwL3vpnEgressInfo;
    UINT4               u4Addr = 0;
    UINT1               u1EncapType;
    UINT4               u4IpPort = 0;
    MEMSET (&MplsHwL3vpnEgressInfo, 0, sizeof (tMplsHwL3vpnEgressInfo));

    if (NULL == pL3VPNRouteEntry)
    {
        return L3VPN_FAILURE;
    }

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    MplsHwL3vpnEgressInfo.u4Label =
        L3VPN_P_BGPROUTELABEL_LABEL (pL3VPNRouteEntry);
    MplsHwL3vpnEgressInfo.u1LabelPolicy =
        L3VPN_P_BGPROUTELABEL_POLICY (pL3VPNRouteEntry);
    MplsHwL3vpnEgressInfo.u1Action = L3VPN_BGP4_ROUTE_ADD;

    switch (L3VPN_P_BGPROUTELABEL_POLICY (pL3VPNRouteEntry))
    {
        case L3VPN_BGP4_POLICY_PER_VRF:
            MplsHwL3vpnEgressInfo.u4VrfId =
                L3VPN_P_BGPROUTELABELVRF_VRFID (pL3VPNRouteEntry);
            break;

        case L3VPN_BGP4_POLICY_PER_ROUTE:
            MplsHwL3vpnEgressInfo.u4VrfId =
                L3VPN_P_BGPROUTELABELRTE_CXTID (pL3VPNRouteEntry);
            MplsHwL3vpnEgressInfo.u4OutPort =
                L3VPN_P_BGPROUTELABELRTE_IF_IDX (pL3VPNRouteEntry);

            MplsHwL3vpnEgressInfo.NexHopAddr.u2AddressLen =
                (UINT2)
                L3VPN_P_BGPROUTELABELRTE_NXTHOPADDRLEN (pL3VPNRouteEntry);

            MEMCPY (MplsHwL3vpnEgressInfo.NexHopAddr.au1Address,
                    L3VPN_P_BGPROUTELABELRTE_NXTHOPADDR (pL3VPNRouteEntry),
                    L3VPN_P_BGPROUTELABELRTE_NXTHOPADDRLEN (pL3VPNRouteEntry));

            MplsHwL3vpnEgressInfo.NexHopAddr.u2Afi =
                L3VPN_P_BGPROUTELABELRTE_NXTHOPAFI (pL3VPNRouteEntry);

            MEMCPY (&u4Addr,
                    L3VPN_P_BGPROUTELABELRTE_NXTHOP (pL3VPNRouteEntry).
                    au1Address,
                    L3VPN_P_BGPROUTELABELRTE_NXTHOP (pL3VPNRouteEntry).
                    u2AddressLen);

            u4Addr = OSIX_HTONL (u4Addr);

            if (ArpResolveWithIndex (MplsHwL3vpnEgressInfo.u4OutPort, u4Addr,
                                     (INT1 *) MplsHwL3vpnEgressInfo.au1DstMac,
                                     &u1EncapType) == ARP_FAILURE)
            {
                if ((u4IpPort =
                     (UINT4) CFA_IF_IPPORT (MplsHwL3vpnEgressInfo.u4OutPort)) ==
                    CFA_INVALID_INDEX)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "%s : %d : Failed to get cfa IfIpPort\n",
                                __FUNCTION__, __LINE__));
                }

                if (MplsL3VpnTriggerArpResolve (u4Addr, u4IpPort) ==
                    MPLS_SUCCESS)
                {
                    pL3VPNRouteEntry->u1ArpResolveStatus =
                        MPLS_ARP_RESOLVE_WAITING;
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "%s : %d : MplsTriggerArpResolve Failed : return Failure\n",
                                __FUNCTION__, __LINE__));
                    return L3VPN_FAILURE;
                }
                pL3VPNRouteEntry->u1ArpResolveStatus = MPLS_ARP_RESOLVE_FAILED;
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "%s : %d : ArpResolve : return Failure\n",
                            __FUNCTION__, __LINE__));
                return L3VPN_FAILURE;
            }
            pL3VPNRouteEntry->u1ArpResolveStatus = MPLS_ARP_RESOLVED;

            MEMCPY (L3VPN_P_BGPROUTELABELRTE_DSTMAC (pL3VPNRouteEntry),
                    MplsHwL3vpnEgressInfo.au1DstMac, MAC_ADDR_LEN);
            break;
        default:
            break;
    };

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (MplsFsMplsMbsmHwL3vpnEgressMap (&MplsHwL3vpnEgressInfo, NULL) ==
            FNP_FAILURE)
        {
            return L3VPN_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtEgress (MplsHwL3vpnEgressInfo.
                                                 u4OutPort,
                                                 &(MplsHwL3vpnEgressInfo.
                                                   i4HwExpMapId));
            QosGetMplsExpProfileForPortAtIngress (MplsHwL3vpnEgressInfo.
                                                  u4OutPort,
                                                  &(MplsHwL3vpnEgressInfo.
                                                    i4HwPriMapId));
#endif
            if (MplsFsMplsHwL3vpnEgressMap (&MplsHwL3vpnEgressInfo) ==
                FNP_FAILURE)
            {
                return L3VPN_FAILURE;
            }
#endif
        }

    }
    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnHwEgressUnMap (tL3VpnBgpRouteLabelEntry * pL3VPNRouteEntry,
                        VOID *pSlotInfo)
{
    tMplsHwL3vpnEgressInfo MplsHwL3vpnEgressInfo;

    MEMSET (&MplsHwL3vpnEgressInfo, 0, sizeof (tMplsHwL3vpnEgressInfo));

    if (NULL == pL3VPNRouteEntry)
    {
        return L3VPN_FAILURE;
    }

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    MplsHwL3vpnEgressInfo.u4Label =
        L3VPN_P_BGPROUTELABEL_LABEL (pL3VPNRouteEntry);
    MplsHwL3vpnEgressInfo.u1LabelPolicy =
        L3VPN_P_BGPROUTELABEL_POLICY (pL3VPNRouteEntry);
    MplsHwL3vpnEgressInfo.u1Action = L3VPN_BGP4_ROUTE_DEL;

    switch (L3VPN_P_BGPROUTELABEL_POLICY (pL3VPNRouteEntry))
    {
        case L3VPN_BGP4_POLICY_PER_VRF:
            MplsHwL3vpnEgressInfo.u4VrfId =
                L3VPN_P_BGPROUTELABELVRF_VRFID (pL3VPNRouteEntry);
            break;

        case L3VPN_BGP4_POLICY_PER_ROUTE:
            MplsHwL3vpnEgressInfo.u4VrfId =
                L3VPN_P_BGPROUTELABELRTE_CXTID (pL3VPNRouteEntry);
            MplsHwL3vpnEgressInfo.u4OutPort =
                L3VPN_P_BGPROUTELABELRTE_IF_IDX (pL3VPNRouteEntry);

            MplsHwL3vpnEgressInfo.NexHopAddr.u2AddressLen =
                (UINT2)
                L3VPN_P_BGPROUTELABELRTE_NXTHOPADDRLEN (pL3VPNRouteEntry);

            MEMCPY (MplsHwL3vpnEgressInfo.NexHopAddr.au1Address,
                    L3VPN_P_BGPROUTELABELRTE_NXTHOPADDR (pL3VPNRouteEntry),
                    L3VPN_P_BGPROUTELABELRTE_NXTHOPADDRLEN (pL3VPNRouteEntry));

            MplsHwL3vpnEgressInfo.NexHopAddr.u2Afi =
                L3VPN_P_BGPROUTELABELRTE_NXTHOPAFI (pL3VPNRouteEntry);

            MEMCPY (MplsHwL3vpnEgressInfo.au1DstMac,
                    L3VPN_P_BGPROUTELABELRTE_DSTMAC (pL3VPNRouteEntry),
                    MAC_ADDR_LEN);
            break;
        default:
            break;
    };

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (MplsFsMplsMbsmHwL3vpnEgressMap (&MplsHwL3vpnEgressInfo, NULL) ==
            FNP_FAILURE)
        {
            return L3VPN_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtEgress (MplsHwL3vpnEgressInfo.
                                                 u4OutPort,
                                                 &(MplsHwL3vpnEgressInfo.
                                                   i4HwExpMapId));
            QosGetMplsExpProfileForPortAtIngress (MplsHwL3vpnEgressInfo.
                                                  u4OutPort,
                                                  &(MplsHwL3vpnEgressInfo.
                                                    i4HwPriMapId));
#endif
            if (MplsFsMplsHwL3vpnEgressMap (&MplsHwL3vpnEgressInfo) ==
                FNP_FAILURE)
            {
                return L3VPN_FAILURE;
            }
#endif
        }
    }
    return L3VPN_SUCCESS;
}

UINT4
MplsL3vpnHwIngressUnMap (tL3vpnMplsL3VpnVrfRteEntry *
                         pL3vpnMplsL3VpnVrfRteEntry, VOID *pSlotInfo)
{
    tMplsHwL3vpnIgressInfo MplsHwL3vpnIgressInfo;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4L3Intf = 0;
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    UINT1              *pu1RteXcPointer = NULL;
    UINT4               u4XcIndex = 0;
    UINT4               u4InSegmentIndex = 0;
    UINT4               u4OutSegmentIndex = 0;
    UINT4               u4VrfId = 0;

    MEMSET (au1ZeroMac, 0, MAC_ADDR_LEN);
    MEMSET (&MplsHwL3vpnIgressInfo, 0, sizeof (tMplsHwL3vpnIgressInfo));

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    /** update the Destination Address**/
    MplsHwL3vpnIgressInfo.IpPrefix.u2AddressLen =
        (UINT2) L3VPN_P_RTE_INET_CIDR_DEST_LEN (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (MplsHwL3vpnIgressInfo.IpPrefix.au1Address,
            L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry),
            L3VPN_P_RTE_INET_CIDR_DEST_LEN (pL3vpnMplsL3VpnVrfRteEntry));

    MplsHwL3vpnIgressInfo.IpPrefix.u1PrefixLen =
        (UINT1) L3VPN_P_RTE_INET_CIDR_PREFIX_LEN (pL3vpnMplsL3VpnVrfRteEntry);
    /** l3vpn_mpls_note_1 : To do : update the VRF Id**/

    if (VcmIsVrfExist (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry), &u4VrfId)
        == VCM_FALSE)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "%s : %d : VcmIsVrfExist() : return Failure\n",
                    __FUNCTION__, __LINE__));
        return L3VPN_FAILURE;
    }

    MplsHwL3vpnIgressInfo.u4VrfId = u4VrfId;

    MplsHwL3vpnIgressInfo.u1Action = L3VPN_BGP4_ROUTE_DEL;

    /** update the NextHop  Address**/
    MplsHwL3vpnIgressInfo.NextHop.u2AddressLen =
        (UINT2) L3VPN_P_RTE_INET_CIDR_NH_LEN (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (MplsHwL3vpnIgressInfo.NextHop.au1Address,
            L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry),
            L3VPN_P_RTE_INET_CIDR_NH_LEN (pL3vpnMplsL3VpnVrfRteEntry));

    /** Update the NextHop: MacAddress and outInterface ***/
    pu1RteXcPointer = L3VPN_P_RTE_XC_POINTER (pL3vpnMplsL3VpnVrfRteEntry);
    MEMCPY (&u4XcIndex, pu1RteXcPointer, sizeof (UINT4));
    u4XcIndex = OSIX_HTONL (u4XcIndex);

    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    MEMCPY (&u4InSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4InSegmentIndex = OSIX_NTOHL (u4InSegmentIndex);

    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    MEMCPY (&u4OutSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4OutSegmentIndex = OSIX_NTOHL (u4OutSegmentIndex);

    MPLS_CMN_LOCK ();
    pXcEntry =
        MplsGetXCTableEntry (u4XcIndex, u4InSegmentIndex, u4OutSegmentIndex);

    /** get the Xc Index **/
    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
    {
        MPLS_CMN_UNLOCK ();
        return L3VPN_FAILURE;
    }

    if (pXcEntry->pOutIndex->u4Label == MPLS_IMPLICIT_NULL_LABEL)
    {
        MPLS_CMN_UNLOCK ();
        return L3VPN_SUCCESS;
    }

    MEMCPY (MplsHwL3vpnIgressInfo.au1DstMac, pXcEntry->pOutIndex->au1NextHopMac,
            MAC_ADDR_LEN);

    MplsHwL3vpnIgressInfo.u4EgrL3Intf = pXcEntry->pOutIndex->u4IfIndex;

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwL3vpnIgressInfo.u4EgrL3Intf, &u4L3Intf) !=
        MPLS_SUCCESS)
    {

    }

    /* Get the physical port for the corresponding Logical L3 interface. */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          &(MplsHwL3vpnIgressInfo.au1DstMac[0]),
                                          &(MplsHwL3vpnIgressInfo.u4OutPort))
        != MPLS_SUCCESS)
    {
    }

    MPLS_CMN_UNLOCK ();
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (MplsFsMplsMbsmHwL3vpnIngressMap (&MplsHwL3vpnIgressInfo, NULL) ==
            FNP_FAILURE)
        {
            return L3VPN_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtEgress (MplsHwL3vpnIgressInfo.
                                                 u4OutPort,
                                                 &(MplsHwL3vpnIgressInfo.
                                                   i4HwExpMapId));
            QosGetMplsExpProfileForPortAtIngress (MplsHwL3vpnIgressInfo.
                                                  u4OutPort,
                                                  &(MplsHwL3vpnIgressInfo.
                                                    i4HwPriMapId));
#endif
            if (MplsFsMplsHwL3vpnIngressMap (&MplsHwL3vpnIgressInfo) ==
                FNP_FAILURE)
            {
                return L3VPN_FAILURE;
            }
#endif
        }
    }
    return L3VPN_SUCCESS;
}
