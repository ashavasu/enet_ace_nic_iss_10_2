/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnutl.c,v 1.3 2015/09/15 07:03:06 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnutl.c 
*
* Description: This file contains utility functions used by protocol L3vpn
*********************************************************************/

#include "l3vpninc.h"
#include "l3vpnsrc.h"

/****************************************************************************
 Function    :  L3vpnGetAllUtlMplsL3VpnVrfTable
 Input       :  pL3vpnGetMplsL3VpnVrfEntry
                pL3vpndsMplsL3VpnVrfEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllUtlMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                                 pL3vpnGetMplsL3VpnVrfEntry,
                                 tL3vpnMplsL3VpnVrfEntry *
                                 pL3vpndsMplsL3VpnVrfEntry)
{
    UNUSED_PARAM (pL3vpnGetMplsL3VpnVrfEntry);
    UNUSED_PARAM (pL3vpndsMplsL3VpnVrfEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllUtlMplsL3VpnIfConfTable
 Input       :  pL3vpnGetMplsL3VpnIfConfEntry
                pL3vpndsMplsL3VpnIfConfEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllUtlMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                    pL3vpnGetMplsL3VpnIfConfEntry,
                                    tL3vpnMplsL3VpnIfConfEntry *
                                    pL3vpndsMplsL3VpnIfConfEntry)
{
    UNUSED_PARAM (pL3vpnGetMplsL3VpnIfConfEntry);
    UNUSED_PARAM (pL3vpndsMplsL3VpnIfConfEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllUtlMplsL3VpnVrfRTTable
 Input       :  pL3vpnGetMplsL3VpnVrfRTEntry
                pL3vpndsMplsL3VpnVrfRTEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllUtlMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                   pL3vpnGetMplsL3VpnVrfRTEntry,
                                   tL3vpnMplsL3VpnVrfRTEntry *
                                   pL3vpndsMplsL3VpnVrfRTEntry)
{
    UNUSED_PARAM (pL3vpnGetMplsL3VpnVrfRTEntry);
    UNUSED_PARAM (pL3vpndsMplsL3VpnVrfRTEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllUtlMplsL3VpnVrfSecTable
 Input       :  pL3vpnGetMplsL3VpnVrfSecEntry
                pL3vpndsMplsL3VpnVrfSecEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllUtlMplsL3VpnVrfSecTable (tL3vpnMplsL3VpnVrfSecEntry *
                                    pL3vpnGetMplsL3VpnVrfSecEntry,
                                    tL3vpnMplsL3VpnVrfSecEntry *
                                    pL3vpndsMplsL3VpnVrfSecEntry)
{
    UNUSED_PARAM (pL3vpnGetMplsL3VpnVrfSecEntry);
    UNUSED_PARAM (pL3vpndsMplsL3VpnVrfSecEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllUtlMplsL3VpnVrfPerfTable
 Input       :  pL3vpnGetMplsL3VpnVrfPerfEntry
                pL3vpndsMplsL3VpnVrfPerfEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllUtlMplsL3VpnVrfPerfTable (tL3vpnMplsL3VpnVrfPerfEntry *
                                     pL3vpnGetMplsL3VpnVrfPerfEntry,
                                     tL3vpnMplsL3VpnVrfPerfEntry *
                                     pL3vpndsMplsL3VpnVrfPerfEntry)
{
    UNUSED_PARAM (pL3vpnGetMplsL3VpnVrfPerfEntry);
    UNUSED_PARAM (pL3vpndsMplsL3VpnVrfPerfEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllUtlMplsL3VpnVrfRteTable
 Input       :  pL3vpnGetMplsL3VpnVrfRteEntry
                pL3vpndsMplsL3VpnVrfRteEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllUtlMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                    pL3vpnGetMplsL3VpnVrfRteEntry,
                                    tL3vpnMplsL3VpnVrfRteEntry *
                                    pL3vpndsMplsL3VpnVrfRteEntry)
{
    UNUSED_PARAM (pL3vpnGetMplsL3VpnVrfRteEntry);
    UNUSED_PARAM (pL3vpndsMplsL3VpnVrfRteEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtilUpdateMplsL3VpnVrfTable
 * Input       :   pL3vpnOldMplsL3VpnVrfEntry
                   pL3vpnMplsL3VpnVrfEntry
                   pL3vpnIsSetMplsL3VpnVrfEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnUtilUpdateMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                                  pL3vpnOldMplsL3VpnVrfEntry,
                                  tL3vpnMplsL3VpnVrfEntry *
                                  pL3vpnMplsL3VpnVrfEntry,
                                  tL3vpnIsSetMplsL3VpnVrfEntry *
                                  pL3vpnIsSetMplsL3VpnVrfEntry)
{
    L3VpnUpdateVrfConfLastChanged (pL3vpnMplsL3VpnVrfEntry);
    /* CLI FLOW handling for no-rd case */
    if ((pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus !=
         OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus !=
            OSIX_FALSE))
    {
        if ((L3VPN_P_VRF_ROW_STATUS (pL3vpnMplsL3VpnVrfEntry) == NOT_IN_SERVICE)
            && (L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) ==
                L3VPN_VRF_ADMIN_STATUS_DOWN))
        {
            /* set RD to zero */
            MEMSET (L3VPN_P_VRF_RD (pL3vpnMplsL3VpnVrfEntry), 0,
                    MPLS_MAX_L3VPN_RD_LEN);
            L3VPN_P_VRF_RD_LEN (pL3vpnMplsL3VpnVrfEntry) = L3VPN_ZERO;
        }
    }

    /** Handling of Admin status UP/Down **/
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus !=
        OSIX_FALSE)
    {

        if (L3vpnUtlVrfTableAdminStatusHdl (pL3vpnOldMplsL3VpnVrfEntry,
                                            pL3vpnMplsL3VpnVrfEntry) !=
            L3VPN_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    /** Handling of RowStatus Update **/
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus != OSIX_FALSE)
    {
        if (L3vpnUtlVrfTableRowStatusHdl (pL3vpnOldMplsL3VpnVrfEntry,
                                          pL3vpnMplsL3VpnVrfEntry) !=
            L3VPN_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtilUpdateMplsL3VpnIfConfTable
 * Input       :   pL3vpnOldMplsL3VpnIfConfEntry
                   pL3vpnMplsL3VpnIfConfEntry
                   pL3vpnIsSetMplsL3VpnIfConfEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnUtilUpdateMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                     pL3vpnOldMplsL3VpnIfConfEntry,
                                     tL3vpnMplsL3VpnIfConfEntry *
                                     pL3vpnMplsL3VpnIfConfEntry,
                                     tL3vpnIsSetMplsL3VpnIfConfEntry *
                                     pL3vpnIsSetMplsL3VpnIfConfEntry)
{

    UNUSED_PARAM (pL3vpnOldMplsL3VpnIfConfEntry);
    UNUSED_PARAM (pL3vpnMplsL3VpnIfConfEntry);
    UNUSED_PARAM (pL3vpnIsSetMplsL3VpnIfConfEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtilUpdateMplsL3VpnVrfRTTable
 * Input       :   pL3vpnOldMplsL3VpnVrfRTEntry
                   pL3vpnMplsL3VpnVrfRTEntry
                   pL3vpnIsSetMplsL3VpnVrfRTEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnUtilUpdateMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                    pL3vpnOldMplsL3VpnVrfRTEntry,
                                    tL3vpnMplsL3VpnVrfRTEntry *
                                    pL3vpnMplsL3VpnVrfRTEntry,
                                    tL3vpnIsSetMplsL3VpnVrfRTEntry *
                                    pL3vpnIsSetMplsL3VpnVrfRTEntry)
{
    UINT4               u4ContextId = 0;
    UINT1               u1BgpParamType = 0;

    UNUSED_PARAM (pL3vpnOldMplsL3VpnVrfRTEntry);

    if (VcmIsVrfExist (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
                       &u4ContextId) == VCM_FALSE)
    {
        return OSIX_FAILURE;
    }

    /* Handling of Row Status Change */
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus == OSIX_TRUE)
    {
        switch (L3VPN_P_RT_TYPE (pL3vpnMplsL3VpnVrfRTEntry))
        {
            case MPLS_L3VPN_RT_IMPORT:
                u1BgpParamType = L3VPN_BGP4_IMPORT_RT;
                break;

            case MPLS_L3VPN_RT_EXPORT:
                u1BgpParamType = L3VPN_BGP4_EXPORT_RT;
                break;

            case MPLS_L3VPN_RT_BOTH:
                u1BgpParamType = L3VPN_BGP4_BOTH_RT;
                break;
            default:
                break;
        }

        if ((pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
             ACTIVE)
            || (pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT))
        {
            /* Reserving the RT Index */
            L3VpnUtilSetRtIndex (u4ContextId,
                                 pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                                 u4MplsL3VpnVrfRTIndex);

            if (pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == ACTIVE)
            {
                if (L3VPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
                {
                    (*(L3VPN_NOTIFY_ROUTE_PARAMS_CB)) (u4ContextId,
                                                       u1BgpParamType,
                                                       L3VPN_BGP4_ROUTE_PARAM_ADD,
                                                       L3VPN_P_RT_VALUE
                                                       (pL3vpnMplsL3VpnVrfRTEntry));
                }
                else
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Cannot send the RT add notification to BGP.. since the callback is NULL\n"));
                }
                /* TODO API to give RT ADD notification to BGP */
            }
        }
        if ((pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
             NOT_IN_SERVICE)
            || (pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == NOT_READY))
        {
            if (L3VPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
            {
                (*(L3VPN_NOTIFY_ROUTE_PARAMS_CB)) (u4ContextId, u1BgpParamType,
                                                   L3VPN_BGP4_ROUTE_PARAM_DEL,
                                                   L3VPN_P_RT_VALUE
                                                   (pL3vpnMplsL3VpnVrfRTEntry));
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Cannot send the RT add notification to BGP.. since the callback is NULL\n"));
            }
            /* TODO API to give RT DEL notification to BGP */
        }
        if (pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
            DESTROY)
        {
            L3VpnUtilReleaseRtIndex (u4ContextId,
                                     pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                                     u4MplsL3VpnVrfRTIndex);
            if (L3VPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
            {
                (*(L3VPN_NOTIFY_ROUTE_PARAMS_CB)) (u4ContextId, u1BgpParamType,
                                                   L3VPN_BGP4_ROUTE_PARAM_DEL,
                                                   L3VPN_P_RT_VALUE
                                                   (pL3vpnMplsL3VpnVrfRTEntry));
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Cannot send the RT add notification to BGP.. since the callback is NULL\n"));
            }

        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtilUpdateMplsL3VpnVrfRteTable
 * Input       :   pL3vpnOldMplsL3VpnVrfRteEntry
                   pL3vpnMplsL3VpnVrfRteEntry
                   pL3vpnIsSetMplsL3VpnVrfRteEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnUtilUpdateMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                     pL3vpnOldMplsL3VpnVrfRteEntry,
                                     tL3vpnMplsL3VpnVrfRteEntry *
                                     pL3vpnMplsL3VpnVrfRteEntry,
                                     tL3vpnIsSetMplsL3VpnVrfRteEntry *
                                     pL3vpnIsSetMplsL3VpnVrfRteEntry)
{

        /** Handling of RowStatus Update **/
        if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus != OSIX_FALSE)
        {
                if (L3vpnUtlVrfRteTableRowStatusHdl (pL3vpnOldMplsL3VpnVrfRteEntry,
                                        pL3vpnMplsL3VpnVrfRteEntry) !=
                                L3VPN_SUCCESS)
                {
                        return OSIX_FAILURE;
                }
        }
        return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnUtlConvertStringtoOid
* Input       : pL3vpnMplsL3VpnVrfRteEntry,
*               pau1MplsL3VpnVrfRteInetCidrPolicy,
*               pau1MplsL3VpnVrfRteInetCidrPolicyLen
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
L3vpnUtlConvertStringtoOid (tL3vpnMplsL3VpnVrfRteEntry *
                            pL3vpnMplsL3VpnVrfRteEntry,
                            UINT4 *pau1MplsL3VpnVrfRteInetCidrPolicy,
                            INT4 *pau1MplsL3VpnVrfRteInetCidrPolicyLen)
{
    UNUSED_PARAM (pL3vpnMplsL3VpnVrfRteEntry);
    UNUSED_PARAM (pau1MplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (pau1MplsL3VpnVrfRteInetCidrPolicyLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : L3vpnUtlConversionForMplsL3VpnVrfName
* Input       : pMplsL3VpnVrfName,
*               pL3vpnMplsL3VpnVrfEntry,
* Output      : None
* Returns     : NONE
*****************************************************************************/
void
L3vpnUtlConversionForMplsL3VpnVrfName (CHR1 * pMplsL3VpnVrfName,
                                       tL3vpnMplsL3VpnVrfEntry *
                                       pL3vpnMplsL3VpnVrfEntry)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (pL3vpnMplsL3VpnVrfEntry);
}

/****************************************************************************
* Function    : L3vpnUtlConversionForMplsL3VpnVrfRD
* Input       : pMplsL3VpnVrfRD,
*               pL3vpnMplsL3VpnVrfEntry,
* Output      : None
* Returns     : NONE
*****************************************************************************/
void
L3vpnUtlConversionForMplsL3VpnVrfRD (CHR1 * pMplsL3VpnVrfRD,
                                     tL3vpnMplsL3VpnVrfEntry *
                                     pL3vpnMplsL3VpnVrfEntry)
{
    UNUSED_PARAM (pMplsL3VpnVrfRD);
    UNUSED_PARAM (pL3vpnMplsL3VpnVrfEntry);
}
