/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnlw.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnlw.c 
*
* Description: This file contains some of low level functions used by protocol in L3vpn module
*********************************************************************/

#include "l3vpninc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsL3VpnVrfTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsL3VpnVrfTable (tSNMP_OCTET_STRING_TYPE *
                                           pMplsL3VpnVrfName)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsL3VpnIfConfTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsL3VpnIfConfTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsL3VpnVrfName,
                                              INT4 i4MplsL3VpnIfConfIndex)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnIfConfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsL3VpnVrfRTTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsL3VpnVrfRTTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsL3VpnVrfName,
                                             UINT4 u4MplsL3VpnVrfRTIndex,
                                             INT4 i4MplsL3VpnVrfRTType)
{
    if(u4MplsL3VpnVrfRTIndex <= 0)
    {
        return SNMP_FAILURE;
    }
    if(pMplsL3VpnVrfName->i4_Length > L3VPN_MAX_VRF_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    if((i4MplsL3VpnVrfRTType < MPLS_L3VPN_RT_IMPORT) ||
                      (i4MplsL3VpnVrfRTType >= MPLS_L3VPN_MAX_RT_TYPE))
    {
         return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsL3VpnVrfSecTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsL3VpnVrfSecTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsL3VpnVrfName)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsL3VpnVrfPerfTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsL3VpnVrfPerfTable (tSNMP_OCTET_STRING_TYPE *
                                               pMplsL3VpnVrfName)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsL3VpnVrfRteTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsL3VpnVrfRteTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsL3VpnVrfName,
                                              INT4
                                              i4MplsL3VpnVrfRteInetCidrDestType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pMplsL3VpnVrfRteInetCidrDest,
                                              UINT4
                                              u4MplsL3VpnVrfRteInetCidrPfxLen,
                                              tSNMP_OID_TYPE *
                                              pMplsL3VpnVrfRteInetCidrPolicy,
                                              INT4
                                              i4MplsL3VpnVrfRteInetCidrNHopType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pMplsL3VpnVrfRteInetCidrNextHop)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    return SNMP_SUCCESS;
}
