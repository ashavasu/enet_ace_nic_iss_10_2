/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnsz.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

#define _L3VPNSZ_C
#include "l3vpninc.h"
INT4
L3vpnSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < L3VPN_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4)MemCreateMemPool (FsL3VPNSizingParams[i4SizingId].u4StructSize,
                              FsL3VPNSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(L3VPNMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            L3vpnSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
L3vpnSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsL3VPNSizingParams);
    return OSIX_SUCCESS;
}

VOID
L3vpnSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < L3VPN_MAX_SIZING_ID; i4SizingId++)
    {
        if (L3VPNMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (L3VPNMemPoolIds[i4SizingId]);
            L3VPNMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
