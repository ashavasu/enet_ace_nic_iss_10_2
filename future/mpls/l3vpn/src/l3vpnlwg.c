/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnlwg.c,v 1.5 2016/10/19 11:25:19 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnlwg.c $
*
* Description: This file contains the low level routines
*               for the module L3vpn 
*********************************************************************/

#include "l3vpninc.h"
tL3vpnMplsL3VpnVrfRteEntry gL3vpnMplsL3VpnVrfRteEntry;

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsL3VpnVrfTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsL3VpnVrfTable (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName)
{

    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry = L3vpnGetFirstMplsL3VpnVrfTable ();

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pMplsL3VpnVrfName->pu1_OctetList,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pMplsL3VpnVrfName->i4_Length =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsL3VpnIfConfTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsL3VpnIfConfTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 *pi4MplsL3VpnIfConfIndex)
{

    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry = L3vpnGetFirstMplsL3VpnIfConfTable ();

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pMplsL3VpnVrfName->pu1_OctetList,
            pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pMplsL3VpnVrfName->i4_Length =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    *pi4MplsL3VpnIfConfIndex =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsL3VpnVrfRTTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsL3VpnVrfRTTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     UINT4 *pu4MplsL3VpnVrfRTIndex,
                                     INT4 *pi4MplsL3VpnVrfRTType)
{

    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry = L3vpnGetFirstMplsL3VpnVrfRTTable ();

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pMplsL3VpnVrfName->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pMplsL3VpnVrfName->i4_Length =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen;

    *pu4MplsL3VpnVrfRTIndex =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex;

    *pi4MplsL3VpnVrfRTType =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsL3VpnVrfSecTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsL3VpnVrfSecTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName)
{

    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    pL3vpnMplsL3VpnVrfSecEntry = L3vpnGetFirstMplsL3VpnVrfSecTable ();

    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pMplsL3VpnVrfName->pu1_OctetList,
            pL3vpnMplsL3VpnVrfSecEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pMplsL3VpnVrfName->i4_Length =
        pL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsL3VpnVrfPerfTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsL3VpnVrfPerfTable (tSNMP_OCTET_STRING_TYPE *
                                       pMplsL3VpnVrfName)
{

    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry = L3vpnGetFirstMplsL3VpnVrfPerfTable ();

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pMplsL3VpnVrfName->pu1_OctetList,
            pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pMplsL3VpnVrfName->i4_Length =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsL3VpnVrfRteTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsL3VpnVrfRteTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 *pi4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 *pu4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 *pi4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop)
{

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry = L3vpnGetFirstMplsL3VpnVrfRteTable ();

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pMplsL3VpnVrfName->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pMplsL3VpnVrfName->i4_Length =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen;

    *pi4MplsL3VpnVrfRteInetCidrDestType =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrDestLen);
    pMplsL3VpnVrfRteInetCidrDest->i4_Length =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen;

    *pu4MplsL3VpnVrfRteInetCidrPfxLen =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList,
       (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length * sizeof (UINT4)));
       au4MplsL3VpnVrfRteInetCidrPolicy->MibObject.u4_Length =
       (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au4MplsL3VpnVrfRteInetCidrPolicy.
       u4_Length * sizeof (UINT4)); */

    *pi4MplsL3VpnVrfRteInetCidrNHopType =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrNextHopLen);
    pMplsL3VpnVrfRteInetCidrNextHop->i4_Length =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrNextHopLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsL3VpnVrfTable
 Input       :  The Indices
                MplsL3VpnVrfName
                nextMplsL3VpnVrfName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexMplsL3VpnVrfTable (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextMplsL3VpnVrfName)
{

    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
    tL3vpnMplsL3VpnVrfEntry *pNextL3vpnMplsL3VpnVrfEntry = NULL;

    MEMSET (&L3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (L3vpnMplsL3VpnVrfEntry.MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    L3vpnMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    pNextL3vpnMplsL3VpnVrfEntry =
        L3vpnGetNextMplsL3VpnVrfTable (&L3vpnMplsL3VpnVrfEntry);

    if (pNextL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextMplsL3VpnVrfName->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pNextL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pNextMplsL3VpnVrfName->i4_Length =
        pNextL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsL3VpnIfConfTable
 Input       :  The Indices
                MplsL3VpnVrfName
                nextMplsL3VpnVrfName
                MplsL3VpnIfConfIndex
                nextMplsL3VpnIfConfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexMplsL3VpnIfConfTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsL3VpnVrfName,
                                     INT4 i4MplsL3VpnIfConfIndex,
                                     INT4 *pi4NextMplsL3VpnIfConfIndex)
{

    tL3vpnMplsL3VpnIfConfEntry L3vpnMplsL3VpnIfConfEntry;
    tL3vpnMplsL3VpnIfConfEntry *pNextL3vpnMplsL3VpnIfConfEntry = NULL;

    MEMSET (&L3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (L3vpnMplsL3VpnIfConfEntry.MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    L3vpnMplsL3VpnIfConfEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    L3vpnMplsL3VpnIfConfEntry.MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;

    pNextL3vpnMplsL3VpnIfConfEntry =
        L3vpnGetNextMplsL3VpnIfConfTable (&L3vpnMplsL3VpnIfConfEntry);

    if (pNextL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextMplsL3VpnVrfName->pu1_OctetList,
            pNextL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pNextL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pNextMplsL3VpnVrfName->i4_Length =
        pNextL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    *pi4NextMplsL3VpnIfConfIndex =
        pNextL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsL3VpnVrfRTTable
 Input       :  The Indices
                MplsL3VpnVrfName
                nextMplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                nextMplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType
                nextMplsL3VpnVrfRTType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexMplsL3VpnVrfRTTable (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextMplsL3VpnVrfName,
                                    UINT4 u4MplsL3VpnVrfRTIndex,
                                    UINT4 *pu4NextMplsL3VpnVrfRTIndex,
                                    INT4 i4MplsL3VpnVrfRTType,
                                    INT4 *pi4NextMplsL3VpnVrfRTType)
{

    tL3vpnMplsL3VpnVrfRTEntry L3vpnMplsL3VpnVrfRTEntry;
    tL3vpnMplsL3VpnVrfRTEntry *pNextL3vpnMplsL3VpnVrfRTEntry = NULL;

    MEMSET (&L3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (L3vpnMplsL3VpnVrfRTEntry.MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    L3vpnMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    L3vpnMplsL3VpnVrfRTEntry.MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;

    L3vpnMplsL3VpnVrfRTEntry.MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;

    pNextL3vpnMplsL3VpnVrfRTEntry =
        L3vpnGetNextMplsL3VpnVrfRTTable (&L3vpnMplsL3VpnVrfRTEntry);

    if (pNextL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextMplsL3VpnVrfName->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pNextL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pNextMplsL3VpnVrfName->i4_Length =
        pNextL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen;

    *pu4NextMplsL3VpnVrfRTIndex =
        pNextL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex;

    *pi4NextMplsL3VpnVrfRTType =
        pNextL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsL3VpnVrfSecTable
 Input       :  The Indices
                MplsL3VpnVrfName
                nextMplsL3VpnVrfName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexMplsL3VpnVrfSecTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsL3VpnVrfName)
{

    tL3vpnMplsL3VpnVrfSecEntry L3vpnMplsL3VpnVrfSecEntry;
    tL3vpnMplsL3VpnVrfSecEntry *pNextL3vpnMplsL3VpnVrfSecEntry = NULL;

    MEMSET (&L3vpnMplsL3VpnVrfSecEntry, 0, sizeof (tL3vpnMplsL3VpnVrfSecEntry));

    /* Assign the index */
    MEMCPY (L3vpnMplsL3VpnVrfSecEntry.MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    L3vpnMplsL3VpnVrfSecEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    pNextL3vpnMplsL3VpnVrfSecEntry =
        L3vpnGetNextMplsL3VpnVrfSecTable (&L3vpnMplsL3VpnVrfSecEntry);

    if (pNextL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextMplsL3VpnVrfName->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfSecEntry->MibObject.au1MplsL3VpnVrfName,
            pNextL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pNextMplsL3VpnVrfName->i4_Length =
        pNextL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsL3VpnVrfPerfTable
 Input       :  The Indices
                MplsL3VpnVrfName
                nextMplsL3VpnVrfName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexMplsL3VpnVrfPerfTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextMplsL3VpnVrfName)
{

    tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
    tL3vpnMplsL3VpnVrfPerfEntry *pNextL3vpnMplsL3VpnVrfPerfEntry = NULL;

    MEMSET (&L3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    /* Assign the index */
    MEMCPY (L3vpnMplsL3VpnVrfPerfEntry.MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    L3vpnMplsL3VpnVrfPerfEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    pNextL3vpnMplsL3VpnVrfPerfEntry =
        L3vpnGetNextMplsL3VpnVrfPerfTable (&L3vpnMplsL3VpnVrfPerfEntry);

    if (pNextL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextMplsL3VpnVrfName->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pNextL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pNextMplsL3VpnVrfName->i4_Length =
        pNextL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsL3VpnVrfRteTable
 Input       :  The Indices
                MplsL3VpnVrfName
                nextMplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                nextMplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                nextMplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                nextMplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                nextMplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                nextMplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop
                nextMplsL3VpnVrfRteInetCidrNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexMplsL3VpnVrfRteTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsL3VpnVrfName,
                                     INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                     INT4
                                     *pi4NextMplsL3VpnVrfRteInetCidrDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfRteInetCidrDest,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsL3VpnVrfRteInetCidrDest,
                                     UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                     UINT4
                                     *pu4NextMplsL3VpnVrfRteInetCidrPfxLen,
                                     tSNMP_OID_TYPE *
                                     pMplsL3VpnVrfRteInetCidrPolicy,
                                     tSNMP_OID_TYPE *
                                     pNextMplsL3VpnVrfRteInetCidrPolicy,
                                     INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                     INT4
                                     *pi4NextMplsL3VpnVrfRteInetCidrNHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfRteInetCidrNextHop,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsL3VpnVrfRteInetCidrNextHop)
{

    tL3vpnMplsL3VpnVrfRteEntry *pNextL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (pNextMplsL3VpnVrfRteInetCidrPolicy);

    MEMSET (&gL3vpnMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (gL3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    gL3vpnMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    gL3vpnMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (gL3vpnMplsL3VpnVrfRteEntry.MibObject.au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    gL3vpnMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    gL3vpnMplsL3VpnVrfRteEntry.MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (L3vpnMplsL3VpnVrfRteEntry.MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    gL3vpnMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (gL3vpnMplsL3VpnVrfRteEntry.MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    gL3vpnMplsL3VpnVrfRteEntry.MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    pNextL3vpnMplsL3VpnVrfRteEntry =
        L3vpnGetNextMplsL3VpnVrfRteTable (&gL3vpnMplsL3VpnVrfRteEntry);

    if (pNextL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextMplsL3VpnVrfName->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen);
    pNextMplsL3VpnVrfName->i4_Length =
        pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen;

    *pi4NextMplsL3VpnVrfRteInetCidrDestType =
        pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pNextMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrDestLen);
    pNextMplsL3VpnVrfRteInetCidrDest->i4_Length =
        pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrDestLen;

    *pu4NextMplsL3VpnVrfRteInetCidrPfxLen =
        pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (au4NextMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy.pu4_OidList,
       (pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length * sizeof (UINT4)));
       au4NextMplsL3VpnVrfRteInetCidrPolicy->u4_Length =
       pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length; */

    *pi4NextMplsL3VpnVrfRteInetCidrNHopType =
        pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pNextMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrNextHopLen);
    pNextMplsL3VpnVrfRteInetCidrNextHop->i4_Length =
        pNextL3vpnMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrNextHopLen;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnConfiguredVrfs
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValMplsL3VpnConfiguredVrfs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnConfiguredVrfs (UINT4 *pu4RetValMplsL3VpnConfiguredVrfs)
{
    if (L3vpnGetMplsL3VpnConfiguredVrfs (pu4RetValMplsL3VpnConfiguredVrfs) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnActiveVrfs
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValMplsL3VpnActiveVrfs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnActiveVrfs (UINT4 *pu4RetValMplsL3VpnActiveVrfs)
{
    if (L3vpnGetMplsL3VpnActiveVrfs (pu4RetValMplsL3VpnActiveVrfs) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnConnectedInterfaces
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValMplsL3VpnConnectedInterfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnConnectedInterfaces (UINT4
                                    *pu4RetValMplsL3VpnConnectedInterfaces)
{
    if (L3vpnGetMplsL3VpnConnectedInterfaces
        (pu4RetValMplsL3VpnConnectedInterfaces) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnNotificationEnable
 Input       :  The Indices

                The Object 
                INT4 *pi4RetValMplsL3VpnNotificationEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnNotificationEnable (INT4 *pi4RetValMplsL3VpnNotificationEnable)
{
    if (L3vpnGetMplsL3VpnNotificationEnable
        (pi4RetValMplsL3VpnNotificationEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfMaxPossRts
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfConfMaxPossRts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfMaxPossRts (UINT4 *pu4RetValMplsL3VpnVrfConfMaxPossRts)
{
    if (L3vpnGetMplsL3VpnVrfConfMaxPossRts (pu4RetValMplsL3VpnVrfConfMaxPossRts)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfRteMxThrshTime
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfConfRteMxThrshTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfRteMxThrshTime (UINT4
                                      *pu4RetValMplsL3VpnVrfConfRteMxThrshTime)
{
    if (L3vpnGetMplsL3VpnVrfConfRteMxThrshTime
        (pu4RetValMplsL3VpnVrfConfRteMxThrshTime) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnIllLblRcvThrsh
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValMplsL3VpnIllLblRcvThrsh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnIllLblRcvThrsh (UINT4 *pu4RetValMplsL3VpnIllLblRcvThrsh)
{
    if (L3vpnGetMplsL3VpnIllLblRcvThrsh (pu4RetValMplsL3VpnIllLblRcvThrsh) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfVpnId
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfVpnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfVpnId (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                         tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfVpnId)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnVrfVpnId->pu1_OctetList,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen);
    pRetValMplsL3VpnVrfVpnId->i4_Length =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfDescription
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfDescription (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValMplsL3VpnVrfDescription)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnVrfDescription->pu1_OctetList,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen);
    pRetValMplsL3VpnVrfDescription->i4_Length =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRD
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRD (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                      tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRD)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnVrfRD->pu1_OctetList,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen);
    pRetValMplsL3VpnVrfRD->i4_Length =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfCreationTime
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfCreationTime (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                UINT4 *pu4RetValMplsL3VpnVrfCreationTime)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfCreationTime =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfCreationTime;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfOperStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfOperStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                              INT4 *pi4RetValMplsL3VpnVrfOperStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfOperStatus =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfOperStatus;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfActiveInterfaces
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfActiveInterfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfActiveInterfaces (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    UINT4
                                    *pu4RetValMplsL3VpnVrfActiveInterfaces)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfActiveInterfaces =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfActiveInterfaces;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfAssociatedInterfaces
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfAssociatedInterfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfAssociatedInterfaces (tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfName,
                                        UINT4
                                        *pu4RetValMplsL3VpnVrfAssociatedInterfaces)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfAssociatedInterfaces =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfAssociatedInterfaces;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfMidRteThresh
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfConfMidRteThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfMidRteThresh (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    UINT4
                                    *pu4RetValMplsL3VpnVrfConfMidRteThresh)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfConfMidRteThresh =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfHighRteThresh
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfConfHighRteThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfHighRteThresh (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     UINT4
                                     *pu4RetValMplsL3VpnVrfConfHighRteThresh)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfConfHighRteThresh =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfMaxRoutes
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfConfMaxRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfMaxRoutes (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                 UINT4 *pu4RetValMplsL3VpnVrfConfMaxRoutes)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfConfMaxRoutes =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfLastChanged
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfConfLastChanged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfLastChanged (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   UINT4 *pu4RetValMplsL3VpnVrfConfLastChanged)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfConfLastChanged =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfLastChanged;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfConfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                 INT4 *pi4RetValMplsL3VpnVrfConfRowStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfConfRowStatus =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfAdminStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfConfAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfAdminStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 *pi4RetValMplsL3VpnVrfConfAdminStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfConfAdminStatus =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfConfStorageType
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfConfStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfConfStorageType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 *pi4RetValMplsL3VpnVrfConfStorageType)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfConfStorageType =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnIfVpnClassification
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                INT4 *pi4RetValMplsL3VpnIfVpnClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnIfVpnClassification (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    INT4 i4MplsL3VpnIfConfIndex,
                                    INT4 *pi4RetValMplsL3VpnIfVpnClassification)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;

    if (L3vpnGetAllMplsL3VpnIfConfTable (pL3vpnMplsL3VpnIfConfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnIfVpnClassification =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnIfVpnRouteDistProtocol
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnIfVpnRouteDistProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnIfVpnRouteDistProtocol (tSNMP_OCTET_STRING_TYPE *
                                       pMplsL3VpnVrfName,
                                       INT4 i4MplsL3VpnIfConfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValMplsL3VpnIfVpnRouteDistProtocol)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;

    if (L3vpnGetAllMplsL3VpnIfConfTable (pL3vpnMplsL3VpnIfConfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnIfVpnRouteDistProtocol->pu1_OctetList,
            pL3vpnMplsL3VpnIfConfEntry->MibObject.
            au1MplsL3VpnIfVpnRouteDistProtocol,
            pL3vpnMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnRouteDistProtocolLen);
    pRetValMplsL3VpnIfVpnRouteDistProtocol->i4_Length =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.
        i4MplsL3VpnIfVpnRouteDistProtocolLen;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnIfConfStorageType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                INT4 *pi4RetValMplsL3VpnIfConfStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnIfConfStorageType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                  INT4 i4MplsL3VpnIfConfIndex,
                                  INT4 *pi4RetValMplsL3VpnIfConfStorageType)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;

    if (L3vpnGetAllMplsL3VpnIfConfTable (pL3vpnMplsL3VpnIfConfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnIfConfStorageType =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnIfConfRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                INT4 *pi4RetValMplsL3VpnIfConfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnIfConfRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                INT4 i4MplsL3VpnIfConfIndex,
                                INT4 *pi4RetValMplsL3VpnIfConfRowStatus)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;

    if (L3vpnGetAllMplsL3VpnIfConfTable (pL3vpnMplsL3VpnIfConfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnIfConfRowStatus =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRT
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRT
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRT (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                      UINT4 u4MplsL3VpnVrfRTIndex, INT4 i4MplsL3VpnVrfRTType,
                      tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRT)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;

    if (L3vpnGetAllMplsL3VpnVrfRTTable (pL3vpnMplsL3VpnVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnVrfRT->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen);
    pRetValMplsL3VpnVrfRT->i4_Length =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRTDescr
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRTDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRTDescr (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                           UINT4 u4MplsL3VpnVrfRTIndex,
                           INT4 i4MplsL3VpnVrfRTType,
                           tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRTDescr)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;

    if (L3vpnGetAllMplsL3VpnVrfRTTable (pL3vpnMplsL3VpnVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnVrfRTDescr->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen);
    pRetValMplsL3VpnVrfRTDescr->i4_Length =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRTRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRTRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRTRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                               UINT4 u4MplsL3VpnVrfRTIndex,
                               INT4 i4MplsL3VpnVrfRTType,
                               INT4 *pi4RetValMplsL3VpnVrfRTRowStatus)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;

    if (L3vpnGetAllMplsL3VpnVrfRTTable (pL3vpnMplsL3VpnVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRTRowStatus =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRTStorageType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRTStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRTStorageType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                 UINT4 u4MplsL3VpnVrfRTIndex,
                                 INT4 i4MplsL3VpnVrfRTType,
                                 INT4 *pi4RetValMplsL3VpnVrfRTStorageType)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;

    if (L3vpnGetAllMplsL3VpnVrfRTTable (pL3vpnMplsL3VpnVrfRTEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRTStorageType =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfSecIllegalLblVltns
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfSecIllegalLblVltns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfSecIllegalLblVltns (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      UINT4
                                      *pu4RetValMplsL3VpnVrfSecIllegalLblVltns)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    pL3vpnMplsL3VpnVrfSecEntry =
        (tL3vpnMplsL3VpnVrfSecEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfSecEntry, 0, sizeof (tL3vpnMplsL3VpnVrfSecEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfSecEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfSecTable (pL3vpnMplsL3VpnVrfSecEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfSecEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfSecIllegalLblVltns =
        pL3vpnMplsL3VpnVrfSecEntry->MibObject.u4MplsL3VpnVrfSecIllegalLblVltns;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfSecEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfSecDiscontinuityTime
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfSecDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfSecDiscontinuityTime (tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfName,
                                        UINT4
                                        *pu4RetValMplsL3VpnVrfSecDiscontinuityTime)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    pL3vpnMplsL3VpnVrfSecEntry =
        (tL3vpnMplsL3VpnVrfSecEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfSecEntry, 0, sizeof (tL3vpnMplsL3VpnVrfSecEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfSecEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfSecTable (pL3vpnMplsL3VpnVrfSecEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfSecEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfSecDiscontinuityTime =
        pL3vpnMplsL3VpnVrfSecEntry->MibObject.
        u4MplsL3VpnVrfSecDiscontinuityTime;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfSecEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfPerfRoutesAdded
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfPerfRoutesAdded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfPerfRoutesAdded (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   UINT4 *pu4RetValMplsL3VpnVrfPerfRoutesAdded)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfPerfTable (pL3vpnMplsL3VpnVrfPerfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfPerfRoutesAdded =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesAdded;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfPerfRoutesDeleted
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfPerfRoutesDeleted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfPerfRoutesDeleted (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     UINT4
                                     *pu4RetValMplsL3VpnVrfPerfRoutesDeleted)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfPerfTable (pL3vpnMplsL3VpnVrfPerfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfPerfRoutesDeleted =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesDeleted;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfPerfCurrNumRoutes
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfPerfCurrNumRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfPerfCurrNumRoutes (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     UINT4
                                     *pu4RetValMplsL3VpnVrfPerfCurrNumRoutes)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfPerfTable (pL3vpnMplsL3VpnVrfPerfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfPerfCurrNumRoutes =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfCurrNumRoutes;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfPerfRoutesDropped
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfPerfRoutesDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfPerfRoutesDropped (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     UINT4
                                     *pu4RetValMplsL3VpnVrfPerfRoutesDropped)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfPerfTable (pL3vpnMplsL3VpnVrfPerfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfPerfRoutesDropped =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesDropped;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfPerfDiscTime
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfPerfDiscTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfPerfDiscTime (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                UINT4 *pu4RetValMplsL3VpnVrfPerfDiscTime)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfPerfTable (pL3vpnMplsL3VpnVrfPerfEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfPerfDiscTime =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfDiscTime;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrIfIndex
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrIfIndex (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      *pi4RetValMplsL3VpnVrfRteInetCidrIfIndex)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrIfIndex =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsL3VpnVrfRteInetCidrDest,
                                   UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                   tSNMP_OID_TYPE *
                                   pMplsL3VpnVrfRteInetCidrPolicy,
                                   INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsL3VpnVrfRteInetCidrNextHop,
                                   INT4 *pi4RetValMplsL3VpnVrfRteInetCidrType)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrType =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrProto
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrProto (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsL3VpnVrfRteInetCidrDest,
                                    UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                    tSNMP_OID_TYPE *
                                    pMplsL3VpnVrfRteInetCidrPolicy,
                                    INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsL3VpnVrfRteInetCidrNextHop,
                                    INT4 *pi4RetValMplsL3VpnVrfRteInetCidrProto)
{

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrProto =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrProto;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrAge
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfRteInetCidrAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrAge (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                  INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pMplsL3VpnVrfRteInetCidrDest,
                                  UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                  tSNMP_OID_TYPE *
                                  pMplsL3VpnVrfRteInetCidrPolicy,
                                  INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pMplsL3VpnVrfRteInetCidrNextHop,
                                  UINT4 *pu4RetValMplsL3VpnVrfRteInetCidrAge)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfRteInetCidrAge =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrAge;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrNextHopAS
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                UINT4 *pu4RetValMplsL3VpnVrfRteInetCidrNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrNextHopAS (tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfName,
                                        INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfRteInetCidrDest,
                                        UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                        tSNMP_OID_TYPE *
                                        pMplsL3VpnVrfRteInetCidrPolicy,
                                        INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfRteInetCidrNextHop,
                                        UINT4
                                        *pu4RetValMplsL3VpnVrfRteInetCidrNextHopAS)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsL3VpnVrfRteInetCidrNextHopAS =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.
        u4MplsL3VpnVrfRteInetCidrNextHopAS;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrMetric1
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric1 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      *pi4RetValMplsL3VpnVrfRteInetCidrMetric1)
{

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrMetric1 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrMetric2
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrMetric2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric2 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      *pi4RetValMplsL3VpnVrfRteInetCidrMetric2)
{

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrMetric2 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrMetric3
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrMetric3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric3 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      *pi4RetValMplsL3VpnVrfRteInetCidrMetric3)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrMetric3 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrMetric4
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrMetric4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric4 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      *pi4RetValMplsL3VpnVrfRteInetCidrMetric4)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrMetric4 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrMetric5
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrMetric5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrMetric5 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      *pi4RetValMplsL3VpnVrfRteInetCidrMetric5)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /* MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrMetric5 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteXCPointer
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValMplsL3VpnVrfRteXCPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteXCPointer (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsL3VpnVrfRteInetCidrDest,
                                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                tSNMP_OID_TYPE * pMplsL3VpnVrfRteInetCidrPolicy,
                                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsL3VpnVrfRteInetCidrNextHop,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValMplsL3VpnVrfRteXCPointer)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsL3VpnVrfRteXCPointer->pu1_OctetList,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteXCPointerLen);
    pRetValMplsL3VpnVrfRteXCPointer->i4_Length =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen;

    /*L3VPN_XC_ENTRY_TO_OCTETSTRING (pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4XcIndex,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4InSegmentIndex,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4OutSegmentIndex, pRetValMplsL3VpnVrfRteXCPointer); */

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsL3VpnVrfRteInetCidrStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                INT4 *pi4RetValMplsL3VpnVrfRteInetCidrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsL3VpnVrfRteInetCidrStatus (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfRteInetCidrDest,
                                     UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                     tSNMP_OID_TYPE *
                                     pMplsL3VpnVrfRteInetCidrPolicy,
                                     INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfRteInetCidrNextHop,
                                     INT4
                                     *pi4RetValMplsL3VpnVrfRteInetCidrStatus)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;

    if (L3vpnGetAllMplsL3VpnVrfRteTable (pL3vpnMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsL3VpnVrfRteInetCidrStatus =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus;

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnNotificationEnable
 Input       :  The Indices

                The Object 
             :  INT4 i4SetValMplsL3VpnNotificationEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnNotificationEnable (INT4 i4SetValMplsL3VpnNotificationEnable)
{
    if (L3vpnSetMplsL3VpnNotificationEnable
        (i4SetValMplsL3VpnNotificationEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfRteMxThrshTime
 Input       :  The Indices

                The Object
             :  UINT4 u4SetValMplsL3VpnVrfConfRteMxThrshTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfRteMxThrshTime (UINT4
                                      u4SetValMplsL3VpnVrfConfRteMxThrshTime)
{
    if (L3vpnSetMplsL3VpnVrfConfRteMxThrshTime
        (u4SetValMplsL3VpnVrfConfRteMxThrshTime) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnIllLblRcvThrsh
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValMplsL3VpnIllLblRcvThrsh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnIllLblRcvThrsh (UINT4 u4SetValMplsL3VpnIllLblRcvThrsh)
{
    if (L3vpnSetMplsL3VpnIllLblRcvThrsh (u4SetValMplsL3VpnIllLblRcvThrsh) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfVpnId
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnVrfVpnId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfVpnId (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                         tSNMP_OCTET_STRING_TYPE * pSetValMplsL3VpnVrfVpnId)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
            pSetValMplsL3VpnVrfVpnId->pu1_OctetList,
            pSetValMplsL3VpnVrfVpnId->i4_Length);
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen =
        pSetValMplsL3VpnVrfVpnId->i4_Length;

    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfDescription
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnVrfDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfDescription (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValMplsL3VpnVrfDescription)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
            pSetValMplsL3VpnVrfDescription->pu1_OctetList,
            pSetValMplsL3VpnVrfDescription->i4_Length);
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen =
        pSetValMplsL3VpnVrfDescription->i4_Length;

    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRD
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnVrfRD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRD (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                      tSNMP_OCTET_STRING_TYPE * pSetValMplsL3VpnVrfRD)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
            pSetValMplsL3VpnVrfRD->pu1_OctetList,
            pSetValMplsL3VpnVrfRD->i4_Length);
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen =
        pSetValMplsL3VpnVrfRD->i4_Length;

    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfMidRteThresh
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  UINT4 u4SetValMplsL3VpnVrfConfMidRteThresh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfMidRteThresh (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    UINT4 u4SetValMplsL3VpnVrfConfMidRteThresh)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh =
        u4SetValMplsL3VpnVrfConfMidRteThresh;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfHighRteThresh
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  UINT4 u4SetValMplsL3VpnVrfConfHighRteThresh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfHighRteThresh (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     UINT4
                                     u4SetValMplsL3VpnVrfConfHighRteThresh)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh =
        u4SetValMplsL3VpnVrfConfHighRteThresh;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfMaxRoutes
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  UINT4 u4SetValMplsL3VpnVrfConfMaxRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfMaxRoutes (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                 UINT4 u4SetValMplsL3VpnVrfConfMaxRoutes)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes =
        u4SetValMplsL3VpnVrfConfMaxRoutes;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfConfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                 INT4 i4SetValMplsL3VpnVrfConfRowStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
        i4SetValMplsL3VpnVrfConfRowStatus;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfAdminStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfConfAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfAdminStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 i4SetValMplsL3VpnVrfConfAdminStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus =
        i4SetValMplsL3VpnVrfConfAdminStatus;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfConfStorageType
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfConfStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfConfStorageType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 i4SetValMplsL3VpnVrfConfStorageType)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType =
        i4SetValMplsL3VpnVrfConfStorageType;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfTable
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnIfVpnClassification
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
             :  INT4 i4SetValMplsL3VpnIfVpnClassification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnIfVpnClassification (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    INT4 i4MplsL3VpnIfConfIndex,
                                    INT4 i4SetValMplsL3VpnIfVpnClassification)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification =
        i4SetValMplsL3VpnIfVpnClassification;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnIfConfTable
        (pL3vpnMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnIfVpnRouteDistProtocol
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnIfVpnRouteDistProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnIfVpnRouteDistProtocol (tSNMP_OCTET_STRING_TYPE *
                                       pMplsL3VpnVrfName,
                                       INT4 i4MplsL3VpnIfConfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValMplsL3VpnIfVpnRouteDistProtocol)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.
            au1MplsL3VpnIfVpnRouteDistProtocol,
            pSetValMplsL3VpnIfVpnRouteDistProtocol->pu1_OctetList,
            pSetValMplsL3VpnIfVpnRouteDistProtocol->i4_Length);
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnRouteDistProtocolLen =
        pSetValMplsL3VpnIfVpnRouteDistProtocol->i4_Length;

    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnIfConfTable
        (pL3vpnMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnIfConfStorageType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
             :  INT4 i4SetValMplsL3VpnIfConfStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnIfConfStorageType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                  INT4 i4MplsL3VpnIfConfIndex,
                                  INT4 i4SetValMplsL3VpnIfConfStorageType)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType =
        i4SetValMplsL3VpnIfConfStorageType;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnIfConfTable
        (pL3vpnMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnIfConfRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
             :  INT4 i4SetValMplsL3VpnIfConfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnIfConfRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                INT4 i4MplsL3VpnIfConfIndex,
                                INT4 i4SetValMplsL3VpnIfConfRowStatus)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
        i4SetValMplsL3VpnIfConfRowStatus;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnIfConfTable
        (pL3vpnMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRT
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnVrfRT
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRT (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                      UINT4 u4MplsL3VpnVrfRTIndex, INT4 i4MplsL3VpnVrfRTType,
                      tSNMP_OCTET_STRING_TYPE * pSetValMplsL3VpnVrfRT)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
            pSetValMplsL3VpnVrfRT->pu1_OctetList,
            pSetValMplsL3VpnVrfRT->i4_Length);
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen =
        pSetValMplsL3VpnVrfRT->i4_Length;

    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRTTable
        (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRTDescr
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnVrfRTDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRTDescr (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                           UINT4 u4MplsL3VpnVrfRTIndex,
                           INT4 i4MplsL3VpnVrfRTType,
                           tSNMP_OCTET_STRING_TYPE * pSetValMplsL3VpnVrfRTDescr)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
            pSetValMplsL3VpnVrfRTDescr->pu1_OctetList,
            pSetValMplsL3VpnVrfRTDescr->i4_Length);
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen =
        pSetValMplsL3VpnVrfRTDescr->i4_Length;

    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRTTable
        (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRTRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRTRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRTRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                               UINT4 u4MplsL3VpnVrfRTIndex,
                               INT4 i4MplsL3VpnVrfRTType,
                               INT4 i4SetValMplsL3VpnVrfRTRowStatus)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
        i4SetValMplsL3VpnVrfRTRowStatus;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRTTable
        (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRTStorageType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRTStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRTStorageType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                 UINT4 u4MplsL3VpnVrfRTIndex,
                                 INT4 i4MplsL3VpnVrfRTType,
                                 INT4 i4SetValMplsL3VpnVrfRTStorageType)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType =
        i4SetValMplsL3VpnVrfRTStorageType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRTTable
        (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrIfIndex
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrIfIndex (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      i4SetValMplsL3VpnVrfRteInetCidrIfIndex)
{
 /*   UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrIfIndex);*/

    /*Static Routes Not Supported, hence return success */
/*    return SNMP_SUCCESS;*/

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

   /* MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex =
        i4SetValMplsL3VpnVrfRteInetCidrIfIndex;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrType (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsL3VpnVrfRteInetCidrDest,
                                   UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                   tSNMP_OID_TYPE *
                                   pMplsL3VpnVrfRteInetCidrPolicy,
                                   INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsL3VpnVrfRteInetCidrNextHop,
                                   INT4 i4SetValMplsL3VpnVrfRteInetCidrType)
{
/*    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrType);

    Static Routes Not Supported, hence return success 
    return SNMP_SUCCESS;*/


    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType =
        i4SetValMplsL3VpnVrfRteInetCidrType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrNextHopAS
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  UINT4 u4SetValMplsL3VpnVrfRteInetCidrNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrNextHopAS (tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfName,
                                        INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfRteInetCidrDest,
                                        UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                        tSNMP_OID_TYPE *
                                        pMplsL3VpnVrfRteInetCidrPolicy,
                                        INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfRteInetCidrNextHop,
                                        UINT4
                                        u4SetValMplsL3VpnVrfRteInetCidrNextHopAS)
{

/*    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (u4SetValMplsL3VpnVrfRteInetCidrNextHopAS);

    Static Routes Not Supported, hence return success 
    return SNMP_SUCCESS;*/


    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

   /* MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrNextHopAS =
        u4SetValMplsL3VpnVrfRteInetCidrNextHopAS;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrMetric1
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrMetric1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric1 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      i4SetValMplsL3VpnVrfRteInetCidrMetric1)
{

/*    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrMetric1);*/

    /*Static Routes Not Supported, hence return success */
/*  return SNMP_SUCCESS;*/


    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1 =
        i4SetValMplsL3VpnVrfRteInetCidrMetric1;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrMetric2
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrMetric2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric2 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      i4SetValMplsL3VpnVrfRteInetCidrMetric2)
{

    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrMetric2);

    /*Static Routes Not Supported, hence return success */
    return SNMP_SUCCESS;

#if 0
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2 =
        i4SetValMplsL3VpnVrfRteInetCidrMetric2;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrMetric3
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrMetric3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric3 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      i4SetValMplsL3VpnVrfRteInetCidrMetric3)
{

    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrMetric3);

    /*Static Routes Not Supported, hence return success */
    return SNMP_SUCCESS;

#if 0
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3 =
        i4SetValMplsL3VpnVrfRteInetCidrMetric3;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrMetric4
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrMetric4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric4 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      i4SetValMplsL3VpnVrfRteInetCidrMetric4)
{

    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrMetric4);

    /*Static Routes Not Supported, hence return success */
    return SNMP_SUCCESS;

#if 0
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4 =
        i4SetValMplsL3VpnVrfRteInetCidrMetric4;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrMetric5
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrMetric5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrMetric5 (tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4
                                      i4SetValMplsL3VpnVrfRteInetCidrMetric5)
{
    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrMetric5);

    /*Static Routes Not Supported, hence return success */
    return SNMP_SUCCESS;

#if 0
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5 =
        i4SetValMplsL3VpnVrfRteInetCidrMetric5;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 =
        OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteXCPointer
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValMplsL3VpnVrfRteXCPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteXCPointer (tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsL3VpnVrfRteInetCidrDest,
                                UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                tSNMP_OID_TYPE * pMplsL3VpnVrfRteInetCidrPolicy,
                                INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsL3VpnVrfRteInetCidrNextHop,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValMplsL3VpnVrfRteXCPointer)
{
/*    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (pSetValMplsL3VpnVrfRteXCPointer);*/

    /*Static Routes Not Supported, hence return success */
/*    return SNMP_SUCCESS;*/

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pSetValMplsL3VpnVrfRteXCPointer);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

/*    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer,
            pSetValMplsL3VpnVrfRteXCPointer->pu1_OctetList,
            pSetValMplsL3VpnVrfRteXCPointer->i4_Length);
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen =
        pSetValMplsL3VpnVrfRteXCPointer->i4_Length;
  /*L3VPN_OCTET_STRING_TO_XC_INDEX (pSetValMplsL3VpnVrfRteXCPointer,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4XcIndex,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4InSegmentIndex,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4OutSegmentIndex); */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsL3VpnVrfRteInetCidrStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
             :  INT4 i4SetValMplsL3VpnVrfRteInetCidrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsL3VpnVrfRteInetCidrStatus (tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfRteInetCidrDest,
                                     UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                     tSNMP_OID_TYPE *
                                     pMplsL3VpnVrfRteInetCidrPolicy,
                                     INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfRteInetCidrNextHop,
                                     INT4 i4SetValMplsL3VpnVrfRteInetCidrStatus)
{
/*    UNUSED_PARAM (pMplsL3VpnVrfName);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrDestType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrDest);
    UNUSED_PARAM (u4MplsL3VpnVrfRteInetCidrPfxLen);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    UNUSED_PARAM (i4MplsL3VpnVrfRteInetCidrNHopType);
    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrNextHop);
    UNUSED_PARAM (i4SetValMplsL3VpnVrfRteInetCidrStatus);*/

    /*Static Routes Not Supported, hence return success */
  /*  return SNMP_SUCCESS;*/


    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
        i4SetValMplsL3VpnVrfRteInetCidrStatus;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;

    if (L3vpnSetAllMplsL3VpnVrfRteTable
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnNotificationEnable
 Input       :  The Indices

                The Object 
                testValMplsL3VpnNotificationEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnNotificationEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValMplsL3VpnNotificationEnable)
{
    if (L3vpnTestMplsL3VpnNotificationEnable
        (pu4ErrorCode, i4TestValMplsL3VpnNotificationEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfRteMxThrshTime
 Input       :  The Indices

                The Object
                testValMplsL3VpnVrfConfRteMxThrshTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfRteMxThrshTime (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4TestValMplsL3VpnVrfConfRteMxThrshTime)
{
    if (L3vpnTestMplsL3VpnVrfConfRteMxThrshTime
        (pu4ErrorCode, u4TestValMplsL3VpnVrfConfRteMxThrshTime) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnIllLblRcvThrsh
 Input       :  The Indices

                The Object 
                testValMplsL3VpnIllLblRcvThrsh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnIllLblRcvThrsh (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValMplsL3VpnIllLblRcvThrsh)
{
    if (L3vpnTestMplsL3VpnIllLblRcvThrsh
        (pu4ErrorCode, u4TestValMplsL3VpnIllLblRcvThrsh) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfVpnId
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfVpnId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfVpnId (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                            tSNMP_OCTET_STRING_TYPE * pTestValMplsL3VpnVrfVpnId)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
            pTestValMplsL3VpnVrfVpnId->pu1_OctetList,
            pTestValMplsL3VpnVrfVpnId->i4_Length);
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen =
        pTestValMplsL3VpnVrfVpnId->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfDescription
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfDescription (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValMplsL3VpnVrfDescription)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
            pTestValMplsL3VpnVrfDescription->pu1_OctetList,
            pTestValMplsL3VpnVrfDescription->i4_Length);
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen =
        pTestValMplsL3VpnVrfDescription->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRD
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfRD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRD (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                         tSNMP_OCTET_STRING_TYPE * pTestValMplsL3VpnVrfRD)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
            pTestValMplsL3VpnVrfRD->pu1_OctetList,
            pTestValMplsL3VpnVrfRD->i4_Length);
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen =
        pTestValMplsL3VpnVrfRD->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfMidRteThresh
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfConfMidRteThresh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfMidRteThresh (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsL3VpnVrfName,
                                       UINT4
                                       u4TestValMplsL3VpnVrfConfMidRteThresh)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh =
        u4TestValMplsL3VpnVrfConfMidRteThresh;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfHighRteThresh
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfConfHighRteThresh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfHighRteThresh (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfName,
                                        UINT4
                                        u4TestValMplsL3VpnVrfConfHighRteThresh)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh =
        u4TestValMplsL3VpnVrfConfHighRteThresh;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfMaxRoutes
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfConfMaxRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfMaxRoutes (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    UINT4 u4TestValMplsL3VpnVrfConfMaxRoutes)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes =
        u4TestValMplsL3VpnVrfConfMaxRoutes;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfConfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfRowStatus (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    INT4 i4TestValMplsL3VpnVrfConfRowStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
        i4TestValMplsL3VpnVrfConfRowStatus;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfAdminStatus
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfConfAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfAdminStatus (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4TestValMplsL3VpnVrfConfAdminStatus)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus =
        i4TestValMplsL3VpnVrfConfAdminStatus;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfConfStorageType
 Input       :  The Indices
                MplsL3VpnVrfName

                The Object 
                testValMplsL3VpnVrfConfStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfConfStorageType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4TestValMplsL3VpnVrfConfStorageType)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnIsSetMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType =
        i4TestValMplsL3VpnVrfConfStorageType;
    pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfTable (pu4ErrorCode, pL3vpnMplsL3VpnVrfEntry,
                                       pL3vpnIsSetMplsL3VpnVrfEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnIfVpnClassification
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                testValMplsL3VpnIfVpnClassification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnIfVpnClassification (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsL3VpnVrfName,
                                       INT4 i4MplsL3VpnIfConfIndex,
                                       INT4
                                       i4TestValMplsL3VpnIfVpnClassification)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification =
        i4TestValMplsL3VpnIfVpnClassification;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnIfConfTable
        (pu4ErrorCode, pL3vpnMplsL3VpnIfConfEntry,
         pL3vpnIsSetMplsL3VpnIfConfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnIfVpnRouteDistProtocol
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                testValMplsL3VpnIfVpnRouteDistProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnIfVpnRouteDistProtocol (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsL3VpnVrfName,
                                          INT4 i4MplsL3VpnIfConfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValMplsL3VpnIfVpnRouteDistProtocol)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.
            au1MplsL3VpnIfVpnRouteDistProtocol,
            pTestValMplsL3VpnIfVpnRouteDistProtocol->pu1_OctetList,
            pTestValMplsL3VpnIfVpnRouteDistProtocol->i4_Length);
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnRouteDistProtocolLen =
        pTestValMplsL3VpnIfVpnRouteDistProtocol->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnIfConfTable
        (pu4ErrorCode, pL3vpnMplsL3VpnIfConfEntry,
         pL3vpnIsSetMplsL3VpnIfConfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnIfConfStorageType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                testValMplsL3VpnIfConfStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnIfConfStorageType (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsL3VpnVrfName,
                                     INT4 i4MplsL3VpnIfConfIndex,
                                     INT4 i4TestValMplsL3VpnIfConfStorageType)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType =
        i4TestValMplsL3VpnIfConfStorageType;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnIfConfTable
        (pu4ErrorCode, pL3vpnMplsL3VpnIfConfEntry,
         pL3vpnIsSetMplsL3VpnIfConfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnIfConfRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex

                The Object 
                testValMplsL3VpnIfConfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnIfConfRowStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 i4MplsL3VpnIfConfIndex,
                                   INT4 i4TestValMplsL3VpnIfConfRowStatus)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnIsSetMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
        i4MplsL3VpnIfConfIndex;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
        i4TestValMplsL3VpnIfConfRowStatus;
    pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnIfConfTable
        (pu4ErrorCode, pL3vpnMplsL3VpnIfConfEntry,
         pL3vpnIsSetMplsL3VpnIfConfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnIfConfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRT
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                testValMplsL3VpnVrfRT
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRT (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                         UINT4 u4MplsL3VpnVrfRTIndex, INT4 i4MplsL3VpnVrfRTType,
                         tSNMP_OCTET_STRING_TYPE * pTestValMplsL3VpnVrfRT)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
            pTestValMplsL3VpnVrfRT->pu1_OctetList,
            pTestValMplsL3VpnVrfRT->i4_Length);
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen =
        pTestValMplsL3VpnVrfRT->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRTTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRTEntry,
         pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRTDescr
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                testValMplsL3VpnVrfRTDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRTDescr (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                              UINT4 u4MplsL3VpnVrfRTIndex,
                              INT4 i4MplsL3VpnVrfRTType,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValMplsL3VpnVrfRTDescr)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
            pTestValMplsL3VpnVrfRTDescr->pu1_OctetList,
            pTestValMplsL3VpnVrfRTDescr->i4_Length);
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen =
        pTestValMplsL3VpnVrfRTDescr->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRTTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRTEntry,
         pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRTRowStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                testValMplsL3VpnVrfRTRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRTRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                  UINT4 u4MplsL3VpnVrfRTIndex,
                                  INT4 i4MplsL3VpnVrfRTType,
                                  INT4 i4TestValMplsL3VpnVrfRTRowStatus)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
        i4TestValMplsL3VpnVrfRTRowStatus;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRTTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRTEntry,
         pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRTStorageType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType

                The Object 
                testValMplsL3VpnVrfRTStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRTStorageType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                    UINT4 u4MplsL3VpnVrfRTIndex,
                                    INT4 i4MplsL3VpnVrfRTType,
                                    INT4 i4TestValMplsL3VpnVrfRTStorageType)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnIsSetMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
        u4MplsL3VpnVrfRTIndex;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
        i4MplsL3VpnVrfRTType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType = OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType =
        i4TestValMplsL3VpnVrfRTStorageType;
    pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRTTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRTEntry,
         pL3vpnIsSetMplsL3VpnVrfRTEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRTEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrIfIndex
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrIfIndex (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfName,
                                         INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrDest,
                                         UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                         tSNMP_OID_TYPE *
                                         pMplsL3VpnVrfRteInetCidrPolicy,
                                         INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrNextHop,
                                         INT4
                                         i4TestValMplsL3VpnVrfRteInetCidrIfIndex)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex =
        i4TestValMplsL3VpnVrfRteInetCidrIfIndex;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrType
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfName,
                                      INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrDest,
                                      UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                      tSNMP_OID_TYPE *
                                      pMplsL3VpnVrfRteInetCidrPolicy,
                                      INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsL3VpnVrfRteInetCidrNextHop,
                                      INT4 i4TestValMplsL3VpnVrfRteInetCidrType)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType =
        i4TestValMplsL3VpnVrfRteInetCidrType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrNextHopAS
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrNextHopAS (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsL3VpnVrfName,
                                           INT4
                                           i4MplsL3VpnVrfRteInetCidrDestType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsL3VpnVrfRteInetCidrDest,
                                           UINT4
                                           u4MplsL3VpnVrfRteInetCidrPfxLen,
                                           tSNMP_OID_TYPE *
                                           pMplsL3VpnVrfRteInetCidrPolicy,
                                           INT4
                                           i4MplsL3VpnVrfRteInetCidrNHopType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsL3VpnVrfRteInetCidrNextHop,
                                           UINT4
                                           u4TestValMplsL3VpnVrfRteInetCidrNextHopAS)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrNextHopAS =
        u4TestValMplsL3VpnVrfRteInetCidrNextHopAS;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrMetric1
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrMetric1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric1 (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfName,
                                         INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrDest,
                                         UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                         tSNMP_OID_TYPE *
                                         pMplsL3VpnVrfRteInetCidrPolicy,
                                         INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrNextHop,
                                         INT4
                                         i4TestValMplsL3VpnVrfRteInetCidrMetric1)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1 =
        i4TestValMplsL3VpnVrfRteInetCidrMetric1;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrMetric2
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrMetric2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric2 (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfName,
                                         INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrDest,
                                         UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                         tSNMP_OID_TYPE *
                                         pMplsL3VpnVrfRteInetCidrPolicy,
                                         INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrNextHop,
                                         INT4
                                         i4TestValMplsL3VpnVrfRteInetCidrMetric2)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2 =
        i4TestValMplsL3VpnVrfRteInetCidrMetric2;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrMetric3
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrMetric3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric3 (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfName,
                                         INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrDest,
                                         UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                         tSNMP_OID_TYPE *
                                         pMplsL3VpnVrfRteInetCidrPolicy,
                                         INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrNextHop,
                                         INT4
                                         i4TestValMplsL3VpnVrfRteInetCidrMetric3)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3 =
        i4TestValMplsL3VpnVrfRteInetCidrMetric3;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrMetric4
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrMetric4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric4 (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfName,
                                         INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrDest,
                                         UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                         tSNMP_OID_TYPE *
                                         pMplsL3VpnVrfRteInetCidrPolicy,
                                         INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrNextHop,
                                         INT4
                                         i4TestValMplsL3VpnVrfRteInetCidrMetric4)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4 =
        i4TestValMplsL3VpnVrfRteInetCidrMetric4;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrMetric5
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrMetric5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrMetric5 (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfName,
                                         INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrDest,
                                         UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                         tSNMP_OID_TYPE *
                                         pMplsL3VpnVrfRteInetCidrPolicy,
                                         INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsL3VpnVrfRteInetCidrNextHop,
                                         INT4
                                         i4TestValMplsL3VpnVrfRteInetCidrMetric5)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5 =
        i4TestValMplsL3VpnVrfRteInetCidrMetric5;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 =
        OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteXCPointer
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteXCPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteXCPointer (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName,
                                   INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsL3VpnVrfRteInetCidrDest,
                                   UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                   tSNMP_OID_TYPE *
                                   pMplsL3VpnVrfRteInetCidrPolicy,
                                   INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsL3VpnVrfRteInetCidrNextHop,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValMplsL3VpnVrfRteXCPointer)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer,
            pTestValMplsL3VpnVrfRteXCPointer->pu1_OctetList,
            pTestValMplsL3VpnVrfRteXCPointer->i4_Length);
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen =
        pTestValMplsL3VpnVrfRteXCPointer->i4_Length;
    /*L3VPN_OCTET_STRING_TO_XC_INDEX (pTestValMplsL3VpnVrfRteXCPointer, 
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4XcIndex,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4InSegmentIndex,
       pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4OutSegmentIndex); */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsL3VpnVrfRteInetCidrStatus
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop

                The Object 
                testValMplsL3VpnVrfRteInetCidrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsL3VpnVrfRteInetCidrStatus (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfName,
                                        INT4 i4MplsL3VpnVrfRteInetCidrDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfRteInetCidrDest,
                                        UINT4 u4MplsL3VpnVrfRteInetCidrPfxLen,
                                        tSNMP_OID_TYPE *
                                        pMplsL3VpnVrfRteInetCidrPolicy,
                                        INT4 i4MplsL3VpnVrfRteInetCidrNHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsL3VpnVrfRteInetCidrNextHop,
                                        INT4
                                        i4TestValMplsL3VpnVrfRteInetCidrStatus)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnIsSetMplsL3VpnVrfRteEntry = NULL;

    UNUSED_PARAM (pMplsL3VpnVrfRteInetCidrPolicy);

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pL3vpnIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Assign the index */
    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pMplsL3VpnVrfName->pu1_OctetList, pMplsL3VpnVrfName->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pMplsL3VpnVrfName->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestType =
        i4MplsL3VpnVrfRteInetCidrDestType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrDest,
            pMplsL3VpnVrfRteInetCidrDest->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrDest->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen =
        pMplsL3VpnVrfRteInetCidrDest->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen =
        u4MplsL3VpnVrfRteInetCidrPfxLen;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen = OSIX_TRUE;

    /*MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
       au4MplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->pu4_OidList,
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length * sizeof (UINT4));
       L3vpnMplsL3VpnVrfRteEntry.au4MplsL3VpnVrfRteInetCidrPolicy.u4_Length =
       pMplsL3VpnVrfRteInetCidrPolicy->u4_Length; */

    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy = OSIX_TRUE;

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType =
        i4MplsL3VpnVrfRteInetCidrNHopType;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
        OSIX_TRUE;

    MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteInetCidrNextHop,
            pMplsL3VpnVrfRteInetCidrNextHop->pu1_OctetList,
            pMplsL3VpnVrfRteInetCidrNextHop->i4_Length);

    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen =
        pMplsL3VpnVrfRteInetCidrNextHop->i4_Length;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
        OSIX_TRUE;

    /* Assign the value */
    pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
        i4TestValMplsL3VpnVrfRteInetCidrStatus;
    pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;

    if (L3vpnTestAllMplsL3VpnVrfRteTable
        (pu4ErrorCode, pL3vpnMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnIsSetMplsL3VpnVrfRteEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnNotificationEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2MplsL3VpnNotificationEnable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnVrfConfRteMxThrshTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2MplsL3VpnVrfConfRteMxThrshTime (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnIllLblRcvThrsh
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2MplsL3VpnIllLblRcvThrsh (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnVrfTable
 Input       :  The Indices
                MplsL3VpnVrfName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsL3VpnVrfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnIfConfTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnIfConfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsL3VpnIfConfTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnVrfRTTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRTIndex
                MplsL3VpnVrfRTType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsL3VpnVrfRTTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsL3VpnVrfRteTable
 Input       :  The Indices
                MplsL3VpnVrfName
                MplsL3VpnVrfRteInetCidrDestType
                MplsL3VpnVrfRteInetCidrDest
                MplsL3VpnVrfRteInetCidrPfxLen
                MplsL3VpnVrfRteInetCidrPolicy
                MplsL3VpnVrfRteInetCidrNHopType
                MplsL3VpnVrfRteInetCidrNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsL3VpnVrfRteTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
