/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnwrg.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnwrg.c
*
* Description: This file contains the wrapperfunctions for L3vpn 
*********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpnlwg.h"
#include "l3vpnwrg.h"
#include "l3vpnprotg.h"
#include "l3vpntmr.h"
#include "l3vpnprot.h"
#include "stdl3vdb.h"

VOID
RegisterSTDL3V ()
{
    SNMPRegisterMibWithLock (&stdl3vOID, &stdl3vEntry, L3vpnMainTaskLock,
                             L3vpnMainTaskUnLock, SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&stdl3vOID, (const UINT1 *) "stdl3v");
}

VOID
UnRegisterSTDL3V ()
{
    SNMPUnRegisterMib (&stdl3vOID, &stdl3vEntry);
    SNMPDelSysorEntry (&stdl3vOID, (const UINT1 *) "stdl3v");
}

INT4
GetNextIndexMplsL3VpnVrfTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsL3VpnVrfTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsL3VpnVrfTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexMplsL3VpnIfConfTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsL3VpnIfConfTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsL3VpnIfConfTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexMplsL3VpnVrfRTTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsL3VpnVrfRTTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsL3VpnVrfRTTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexMplsL3VpnVrfSecTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsL3VpnVrfSecTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsL3VpnVrfSecTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexMplsL3VpnVrfPerfTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsL3VpnVrfPerfTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsL3VpnVrfPerfTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexMplsL3VpnVrfRteTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsL3VpnVrfRteTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pNextMultiIndex->pIndex[4].pOidValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             pNextMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsL3VpnVrfRteTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].pOidValue,
             pNextMultiIndex->pIndex[4].pOidValue,
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             pFirstMultiIndex->pIndex[6].pOctetStrValue,
             pNextMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsL3VpnConfiguredVrfsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnConfiguredVrfs (&(pMultiData->u4_ULongValue)));
}

INT4
MplsL3VpnActiveVrfsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnActiveVrfs (&(pMultiData->u4_ULongValue)));
}

INT4
MplsL3VpnConnectedInterfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnConnectedInterfaces (&(pMultiData->u4_ULongValue)));
}

INT4
MplsL3VpnNotificationEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnNotificationEnable (&(pMultiData->i4_SLongValue)));
}

INT4
MplsL3VpnVrfConfMaxPossRtsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnVrfConfMaxPossRts (&(pMultiData->u4_ULongValue)));
}

INT4
MplsL3VpnVrfConfRteMxThrshTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnVrfConfRteMxThrshTime
            (&(pMultiData->u4_ULongValue)));
}

INT4
MplsL3VpnIllLblRcvThrshGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsL3VpnIllLblRcvThrsh (&(pMultiData->u4_ULongValue)));
}

INT4
MplsL3VpnVrfVpnIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfVpnId (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfDescriptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfDescription
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRD (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfCreationTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfCreationTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfOperStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfActiveInterfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfActiveInterfaces
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfAssociatedInterfacesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfAssociatedInterfaces
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfConfMidRteThreshGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfMidRteThresh
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfConfHighRteThreshGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfHighRteThresh
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfConfMaxRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfMaxRoutes
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfConfLastChangedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfLastChanged
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfConfRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfConfAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfAdminStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfConfStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfConfStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnIfVpnClassificationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnIfConfTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnIfVpnClassification
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnIfVpnRouteDistProtocolGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnIfConfTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnIfVpnRouteDistProtocol
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnIfConfStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnIfConfTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnIfConfStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnIfConfRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnIfConfTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnIfConfRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRTGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRTTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRT (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRTDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRTTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRTDescr (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRTRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRTTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRTRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRTStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRTTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRTStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfSecIllegalLblVltnsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfSecTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfSecIllegalLblVltns
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfSecDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfSecTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfSecDiscontinuityTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfPerfRoutesAddedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfPerfRoutesAdded
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfPerfRoutesDeletedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfPerfRoutesDeleted
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfPerfCurrNumRoutesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfPerfCurrNumRoutes
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfPerfRoutesDroppedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfPerfRoutesDropped
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfPerfDiscTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfPerfDiscTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrIfIndexGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrIfIndex
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrProto
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrAge
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrNextHopASGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrNextHopAS
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrMetric1Get (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrMetric1
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrMetric2Get (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrMetric2
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrMetric3Get (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrMetric3
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrMetric4Get (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrMetric4
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteInetCidrMetric5Get (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrMetric5
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnVrfRteXCPointerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteXCPointer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRteInetCidrStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsL3VpnVrfRteTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue, pMultiIndex->pIndex[4].pOidValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsL3VpnVrfRteInetCidrStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsL3VpnNotificationEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MplsL3VpnNotificationEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
MplsL3VpnVrfConfRteMxThrshTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MplsL3VpnVrfConfRteMxThrshTime
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
MplsL3VpnIllLblRcvThrshTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MplsL3VpnIllLblRcvThrsh
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
MplsL3VpnVrfVpnIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfVpnId (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfDescriptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfDescription (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRD (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfConfMidRteThreshTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfConfMidRteThresh (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfConfHighRteThreshTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfConfHighRteThresh (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfConfMaxRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfConfMaxRoutes (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfConfRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfConfRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfConfAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfConfAdminStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfConfStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfConfStorageType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnIfVpnClassificationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnIfVpnClassification (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnIfVpnRouteDistProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnIfVpnRouteDistProtocol (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      pOctetStrValue));

}

INT4
MplsL3VpnIfConfStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnIfConfStorageType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnIfConfRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnIfConfRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRTTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRT (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRTDescrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRTDescr (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRTRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRTRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRTStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRTStorageType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrIfIndex (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[4].
                                                     pOidValue,
                                                     pMultiIndex->pIndex[5].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[6].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[4].
                                                  pOidValue,
                                                  pMultiIndex->pIndex[5].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[6].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrNextHopAS (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[2].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[3].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[4].
                                                       pOidValue,
                                                       pMultiIndex->pIndex[5].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[6].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric1Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrMetric1 (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[4].
                                                     pOidValue,
                                                     pMultiIndex->pIndex[5].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[6].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric2Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrMetric2 (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[4].
                                                     pOidValue,
                                                     pMultiIndex->pIndex[5].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[6].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric3Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrMetric3 (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[4].
                                                     pOidValue,
                                                     pMultiIndex->pIndex[5].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[6].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric4Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrMetric4 (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[4].
                                                     pOidValue,
                                                     pMultiIndex->pIndex[5].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[6].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric5Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrMetric5 (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[4].
                                                     pOidValue,
                                                     pMultiIndex->pIndex[5].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[6].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsL3VpnVrfRteXCPointerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteXCPointer (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].pOidValue,
                                               pMultiIndex->pIndex[5].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[6].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRteInetCidrStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsL3VpnVrfRteInetCidrStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[4].
                                                    pOidValue,
                                                    pMultiIndex->pIndex[5].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[6].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnNotificationEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMplsL3VpnNotificationEnable (pMultiData->i4_SLongValue));
}

INT4
MplsL3VpnVrfConfRteMxThrshTimeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMplsL3VpnVrfConfRteMxThrshTime (pMultiData->u4_ULongValue));
}

INT4
MplsL3VpnIllLblRcvThrshSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMplsL3VpnIllLblRcvThrsh (pMultiData->u4_ULongValue));
}

INT4
MplsL3VpnVrfVpnIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfVpnId (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfDescriptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfDescription
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRD (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfConfMidRteThreshSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfConfMidRteThresh
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfConfHighRteThreshSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfConfHighRteThresh
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfConfMaxRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfConfMaxRoutes
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfConfRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfConfRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfConfAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfConfAdminStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfConfStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfConfStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnIfVpnClassificationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnIfVpnClassification
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnIfVpnRouteDistProtocolSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnIfVpnRouteDistProtocol
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnIfConfStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnIfConfStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnIfConfRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnIfConfRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRTSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRT (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].i4_SLongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRTDescrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRTDescr (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRTRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRTRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRTStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRTStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrIfIndexSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrIfIndex
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrNextHopASSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrNextHopAS
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric1Set (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrMetric1
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric2Set (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrMetric2
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric3Set (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrMetric3
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric4Set (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrMetric4
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteInetCidrMetric5Set (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrMetric5
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnVrfRteXCPointerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteXCPointer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsL3VpnVrfRteInetCidrStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsL3VpnVrfRteInetCidrStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOidValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsL3VpnNotificationEnableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnNotificationEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsL3VpnVrfConfRteMxThrshTimeDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnVrfConfRteMxThrshTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4
MplsL3VpnIllLblRcvThrshDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnIllLblRcvThrsh
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsL3VpnVrfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnVrfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsL3VpnIfConfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnIfConfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsL3VpnVrfRTTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnVrfRTTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsL3VpnVrfRteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsL3VpnVrfRteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
