/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnutil.c,v 1.5 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * Description : This file contains the utility 
 *               functions of the L3VPN module
 *****************************************************************************/

#include "l3vpninc.h"

tL3vpnMplsL3VpnVrfEntry gIfConfL3VpnVrfEntry;
tL3vpnMplsL3VpnIfConfEntry gIfConfL3VpnIfConfEntry;

/******************************************************************************
 * Function   : L3vpnUtilFindL3vpnStruct
 * Description: 
 * Input      :
 * Output     : 
 * Returns    : Pointer to the tL3vpnStruct ,if exists
 *              NULL otherwise
 *****************************************************************************/
/*
   PUBLIC tL3vpnStruct  *
   L3vpnUtilFindL3vpnStruct ( strcutIndex )
   {
   }
*/

UINT4               gu4InterfaceList[IPIF_MAX_LOGICAL_IFACES + 1];

VOID
L3VpnInitRTIndexMgr ()
{
    UINT4               u4ContextId = 0;
    for (u4ContextId = 0; u4ContextId < MAX_L3VPN_VRF_ENTRIES; u4ContextId++)
    {
        gL3vpnGlobals.gau4RtIndexBitMap[u4ContextId][0] = 1;
    }
}

UINT4
L3VpnRegisterWithVcm ()
{
    tVcmRegInfo         VcmRegInfo;

    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = L3VpnNotifyContextChange;
    VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    VcmRegInfo.u1ProtoId = L3VPN_PROTOCOL_ID;
    if (VCM_SUCCESS == VcmRegisterHLProtocol (&VcmRegInfo))
    {
        return L3VPN_SUCCESS;
    }
    return L3VPN_FAILURE;
}

VOID
L3VpnNotifyContextChange (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{

    tL3VpnQMsg          L3VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;
    UNUSED_PARAM (u4IfIndex);
    MEMSET (&L3VpnQMsg, 0, sizeof (tL3VpnQMsg));
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
    if (pu1TmpMsg == NULL)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "Unable to allocate memory\n"));
        return;
    }

    L3VpnQMsg.u4MsgType = L3VPN_VRF_VCM_CREATE_DEL_EVENT;

    L3VpnQMsg.L3VpnEvtInfo.L3VpnVcmEventInfo.u4ContextId = u4ContextId;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnVcmEventInfo.u1EventType = u1Event;

    MEMCPY (pu1TmpMsg, (UINT1 *) &L3VpnQMsg, sizeof (tL3VpnQMsg));
    if (L3VpnInternalEventHandler (pu1TmpMsg) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Error in sedning interface map/unmap event\n"));
    }
}

/******************************************************************************
 * Function   : L3VpnUtilFindNextFreeRtIndex
 * Description: This function finds the next free RT index for the given VRF context ID.
 * Input      : contextId
 * Output     : pu4FreeIndex
 * Returns    : L3VPN_SUCCESS/L3VPN_FAILURE
 *
 ******************************************************************************/
UINT4
L3VpnUtilFindNextFreeRtIndex (UINT4 contextId, UINT4 *pu4FreeIndex)
{
    UINT1               u1BitNdx = 0;
    UINT2               u2NofUINT4 = 0;
    UINT4               u4MaxU4 = 0;
    UINT4              *pBitmap = NULL;
    pBitmap = gL3vpnGlobals.gau4RtIndexBitMap[contextId];

    u4MaxU4 = L3VPM_MAX_U4_RT_BLKS;

    for (u2NofUINT4 = 0; u2NofUINT4 < u4MaxU4; u2NofUINT4++)
    {
        if (L3VPN_UTIL_UINT4_MAX != pBitmap[u2NofUINT4])
        {
            for (u1BitNdx = 0; u1BitNdx < L3VPN_UTIL_NUM_OF_BITS_IN_UINT4;
                 u1BitNdx++)
            {
                if (((u2NofUINT4 * L3VPN_UTIL_NUM_OF_BITS_IN_UINT4) +
                     u1BitNdx) > L3VPN_MAX_NUM_RT_PER_VRF)
                {
                    break;
                }
                if (!((pBitmap[u2NofUINT4] >> u1BitNdx) & 0x1))
                {
                    *pu4FreeIndex =
                        (UINT4) ((u2NofUINT4 *
                                  L3VPN_UTIL_NUM_OF_BITS_IN_UINT4) + u1BitNdx);
                    return L3VPN_SUCCESS;
                }
            }
        }
    }
    return L3VPN_FAILURE;
}

/******************************************************************************
 * Function   : L3VpnUtilSetRtIndex
 * Description: This function sets the RT index in the global bitmap (used for reservation)
 * Input      : contextId
 *              u4RtIndex
 * Output     : NONE
 * Returns    : L3VPN_SUCCESS/L3VPN_FAILURE
 * 
 * ******************************************************************************/
UINT4
L3VpnUtilSetRtIndex (UINT4 contextId, UINT4 u4RtIndex)
{

    UINT4              *pBitmap = NULL;

    pBitmap = gL3vpnGlobals.gau4RtIndexBitMap[contextId];
    pBitmap[u4RtIndex / L3VPN_UTIL_NUM_OF_BITS_IN_UINT4] =
        (UINT4) ((UINT4)
                 (pBitmap
                  [(UINT4) (u4RtIndex / L3VPN_UTIL_NUM_OF_BITS_IN_UINT4)]) |
                 (UINT4) (1 <<
                          ((UINT4)
                           (u4RtIndex % L3VPN_UTIL_NUM_OF_BITS_IN_UINT4))));
    return L3VPN_SUCCESS;
}

/******************************************************************************
 * Function   : L3VpnUtilReleaseRtIndex
 * Description: This function release the RT index for the given VRF context Id.
 * Input      : contextId
 *              u4RtIndex
 * Output     : NONE
 * Returns    : L3VPN_SUCCESS/L3VPN_FAILURE
 *
 *******************************************************************************/
UINT4
L3VpnUtilReleaseRtIndex (UINT4 contextId, UINT4 u4RtIndex)
{
    UINT4              *pBitmap = NULL;

    pBitmap = gL3vpnGlobals.gau4RtIndexBitMap[contextId];
    pBitmap[u4RtIndex / L3VPN_UTIL_NUM_OF_BITS_IN_UINT4] =
        (UINT4) ((UINT4)
                 (pBitmap
                  [(UINT4) (u4RtIndex / L3VPN_UTIL_NUM_OF_BITS_IN_UINT4)]) &
                 ((UINT4)
                  ~(1 <<
                    (UINT4) (u4RtIndex % L3VPN_UTIL_NUM_OF_BITS_IN_UINT4))));
    return L3VPN_SUCCESS;
}

/******************************************************************************
 * Function   : L3VpnGetRTEntryFromValue
 * Description: This function gives the RT entry from the given RT value
 * Input      : pu1L3VpnVrfname
 *              pu1RtValue
 * Output     : pVrfRtEntry
 * Returns    : L3VPN_SUCCESS/L3VPN_FAILURE
 * 
 *******************************************************************************/

tL3vpnMplsL3VpnVrfRTEntry *
L3VpnGetRTEntryFromValue (UINT1 *pu1L3VpnVrfname, UINT1 *pu1RtValue)
{
    tL3vpnMplsL3VpnVrfRTEntry VrfRtEntry;
    tL3vpnMplsL3VpnVrfRTEntry *pVrfRtEntry;
    INT4                i4VrfNameLen = 0;
    if (pu1L3VpnVrfname == NULL)
    {
        return NULL;
    }

    MEMSET (&VrfRtEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    i4VrfNameLen = (INT4) STRLEN (pu1L3VpnVrfname);
    MEMCPY (VrfRtEntry.MibObject.au1MplsL3VpnVrfName, pu1L3VpnVrfname,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (VrfRtEntry) = i4VrfNameLen;
    VrfRtEntry.MibObject.u4MplsL3VpnVrfRTIndex = 0;
    VrfRtEntry.MibObject.i4MplsL3VpnVrfRTType = 0;
    pVrfRtEntry =
        RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                       &VrfRtEntry, NULL);

    while (NULL != pVrfRtEntry)
    {
        if (MEMCMP (pVrfRtEntry->MibObject.au1MplsL3VpnVrfName, pu1L3VpnVrfname,
                    L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            if (MEMCMP
                (pVrfRtEntry->MibObject.au1MplsL3VpnVrfRT, pu1RtValue,
                 L3VPN_MAX_RT_LEN) == 0)
            {
                return pVrfRtEntry;
            }
        }
        else
        {
            break;
        }
        pVrfRtEntry =
            RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                           pVrfRtEntry, NULL);
    }
    return NULL;
}

/******************************************************************************
 * Function   : L3VpnIfConfMapUnmapEventHandler
 * Description: This function post events to L3VPN queue for VCM map/unmap events
 * Input      : u4Event
 *              i4IfIndex
 *              u4Context
 * Output     : NONE
 * Returns    : NONE
 ********************************************************************************/

VOID
L3VpnIfConfMapUnmapEventHandler (UINT4 u4Event, INT4 i4IfIndex, UINT4 u4Context)
{
    tL3VpnQMsg          L3VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;

    MEMSET (&L3VpnQMsg, 0, sizeof (tL3VpnQMsg));
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
    if (pu1TmpMsg == NULL)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Error in Allocating Memory from "
                    "L3VPN Q Msg Pool.\r\n"));
        return;
    }
    if (u4Event == L3VPN_VCM_IF_MAP)
    {
        L3VpnQMsg.u4MsgType = L3VPN_VCM_IF_MAP_EVENT;
    }
    if (u4Event == L3VPN_VCM_IF_UNMAP)
    {
        L3VpnQMsg.u4MsgType = L3VPN_VCM_IF_UNMAP_EVENT;
    }
    L3VpnQMsg.L3VpnEvtInfo.L3VpnVcmIfEvtInfo.i4IfIndex = i4IfIndex;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnVcmIfEvtInfo.u4ContextId = u4Context;

    MEMCPY (pu1TmpMsg, (UINT1 *) &L3VpnQMsg, sizeof (tL3VpnQMsg));
    if (L3VpnInternalEventHandler (pu1TmpMsg) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Error in sedning interface map/unmap event.\r\n"));
    }
}

/******************************************************************************
 * Function   : L3VpnInternalEventHandler
 * Description: This function post QUEUE_EVENT to L3VPN task
 * Input      : pu1TmpMsg
 * Output     : NONE
 * Returns    : L3VPN_SUCCESS/L3VPN_FAILURE
 ********************************************************************************/
UINT4
L3VpnInternalEventHandler (UINT1 *pu1TmpMsg)
{
    if (OsixQueSend
        (gL3vpnGlobals.l3vpnQueId, (UINT1 *) (&pu1TmpMsg),
         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_Q_MSG_POOL_ID, pu1TmpMsg);
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Mem release block failed.\r\n"));
        return L3VPN_FAILURE;
    }

    /* Always returns Success */
    OsixEvtSend (L3VPN_TASK_ID, L3VPN_QUEUE_EVENT);
    return L3VPN_SUCCESS;
}

/******************************************************************************
 * Function   : L3VpnIfStChgEventHandler
 * Description: This function post Interface up/down events to L3VPN task
 * Input      : i4IfIndex
 *              u1OperStatus
 * Output     : NONE
 * Returns    : NONE
 ********************************************************************************/
VOID
L3VpnIfStChgEventHandler (INT4 i4IfIndex, UINT1 u1OperStatus)
{

    tL3VpnQMsg          L3VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;
    MEMSET (&L3VpnQMsg, 0, sizeof (tL3VpnQMsg));
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
    if (pu1TmpMsg == NULL)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in allocating memeory from L3VPN Q Msg pool.\r\n"));
        return;
    }

    L3VpnQMsg.u4MsgType = L3VPN_IF_STATUS_CHANGE_EVENT;

    L3VpnQMsg.L3VpnEvtInfo.L3VpnIfEvtInfo.i4IfIndex = i4IfIndex;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnIfEvtInfo.u1OperStatus = u1OperStatus;

    MEMCPY (pu1TmpMsg, (UINT1 *) &L3VpnQMsg, sizeof (tL3VpnQMsg));
    if (L3VpnInternalEventHandler (pu1TmpMsg) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Error in sedning interface map/unmap event.\r\n"));
    }
}

/******************************************************************************
 * Function   : L3VpnAdminChangeEventHandler
 * Description: This function post Admin up/down events to L3VPN task
 * Input      : pu1VrfName
 *              u4AdminStatus
 * Output     : NONE
 * Returns    : NONE
 *********************************************************************************/
VOID
L3VpnAdminChangeEventHandler (UINT1 *pu1VrfName, UINT4 u4AdminStatus)
{
    tL3VpnQMsg          L3VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;
    UINT4               u4NameLen = 0;
    MEMSET (&L3VpnQMsg, 0, sizeof (tL3VpnQMsg));
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
    if (pu1TmpMsg == NULL)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in allocating memeory from L3VPN Q Msg pool.\r\n"));
        return;
    }

    u4NameLen = STRLEN (pu1VrfName);
    L3VpnQMsg.u4MsgType = L3VPN_VRF_ADMIN_EVENT;

    MEMCPY (L3VpnQMsg.L3VpnEvtInfo.L3VpnVrfAdminEventInfo.au1VrfName,
            pu1VrfName, u4NameLen);
    L3VpnQMsg.L3VpnEvtInfo.L3VpnVrfAdminEventInfo.u4EventType = u4AdminStatus;

    MEMCPY (pu1TmpMsg, (UINT1 *) &L3VpnQMsg, sizeof (tL3VpnQMsg));
    if (L3VpnInternalEventHandler (pu1TmpMsg) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Error in sedning interface map/unmap event.\r\n"));
    }
}

/******************************************************************************
 * Function   : L3VpnRsvpTeLspStatusChangeEventHandler
 * Description: This function post RSVP LSP status change events to L3VPN task
 * Input      : RsvpTeL3VpnLspInfo
 *              
 * Output     : NONE
 * Returns    : NONE
 **********************************************************************************/
VOID
L3VpnRsvpTeLspStatusChangeEventHandler (tL3VpnRsvpTeLspEventInfo
                                        RsvpTeL3VpnLspInfo)
{

    tL3VpnQMsg          L3VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;

    MEMSET (&L3VpnQMsg, 0, sizeof (tL3VpnQMsg));
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
    if (pu1TmpMsg == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Error in allocating memeory from L3VPN Q Msg pool\n"));
        return;
    }

    L3VpnQMsg.u4MsgType = L3VPN_RSVPTE_LSP_STATE_CHANGE_EVENT;

    L3VpnQMsg.L3VpnEvtInfo.L3VpnRsvpTeLspEventInfo.u4TnlIndex =
        RsvpTeL3VpnLspInfo.u4TnlIndex;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
        RsvpTeL3VpnLspInfo.u4TnlXcIndex;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnRsvpTeLspEventInfo.u1TnlOperStatus =
        RsvpTeL3VpnLspInfo.u1TnlOperStatus;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnRsvpTeLspEventInfo.u1TnlAdminStatus =
        RsvpTeL3VpnLspInfo.u1TnlAdminStatus;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnRsvpTeLspEventInfo.u1Protection =
        RsvpTeL3VpnLspInfo.u1Protection;
    L3VpnQMsg.L3VpnEvtInfo.L3VpnRsvpTeLspEventInfo.u4EventType =
        RsvpTeL3VpnLspInfo.u4EventType;

    MEMCPY (pu1TmpMsg, (UINT1 *) &L3VpnQMsg, sizeof (tL3VpnQMsg));
    if (L3VpnInternalEventHandler (pu1TmpMsg) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Error in sending the RSVP-TE status change event\n"));
    }
    return;
}

/******************************************************************************
 * Function   : L3VpnNonTeLspStatusChangeEventHandler
 * Description: This function post LSP status change events to L3VPN task
 * Input      : u4Prefix
 *              u4EventType
 * Output     : NONE
 * Returns    : NONE
 **********************************************************************************/
VOID
L3VpnNonTeLspStatusChangeEventHandler (UINT4 u4Label, UINT4 u4Prefix,
                                       UINT4 u4EventType)
{

    tL3VpnQMsg          L3VpnQMsg;
    UINT1              *pu1TmpMsg = NULL;

    MEMSET (&L3VpnQMsg, 0, sizeof (tL3VpnQMsg));
    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (L3VPN_Q_MSG_POOL_ID);
    if (pu1TmpMsg == NULL)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in allocating memeory from L3VPN Q Msg pool.\r\n"));
        return;
    }

    L3VpnQMsg.u4MsgType = L3VPM_MPLS_LSP_STATE_CHANGE_EVENT;

    L3VpnQMsg.L3VpnEvtInfo.L3VpnNonTeLspEventInfo.u4Label = u4Label;

    L3VpnQMsg.L3VpnEvtInfo.L3VpnNonTeLspEventInfo.u4Prefix = u4Prefix;

    L3VpnQMsg.L3VpnEvtInfo.L3VpnNonTeLspEventInfo.u4EventType = u4EventType;

    MEMCPY (pu1TmpMsg, (UINT1 *) &L3VpnQMsg, sizeof (tL3VpnQMsg));
    if (L3VpnInternalEventHandler (pu1TmpMsg) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Error in sending the LSP status change event.\r\n"));
    }
}

/******************************************************************************
 * Function   : L3VpnIsRemainingInterfacesUp
 * Description: This function checks whether any one the VRF mapped interface(s) excluding the 
 *              given one is oper up
 * Input      : pu1VrfName
 *              i4IfIndex
 * Output     : NONE
 * Returns    : L3VPN_SUCCESS/L3VPN_FAILURE
 ***********************************************************************************/
UINT4
L3VpnIsRemainingInterfacesUp (UINT1 *pu1VrfName, INT4 i4IfIndex)
{

    tL3vpnMplsL3VpnIfConfEntry L3VpnIfConfEntry;
    UINT1               u1OperStatus = 0;
    tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry = NULL;
    INT4                i4VrfNameLen = 0;
    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMSET (&L3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMCPY (L3VpnIfConfEntry.MibObject.au1MplsL3VpnVrfName, pu1VrfName,
            i4VrfNameLen);
    L3VpnIfConfEntry.MibObject.i4MplsL3VpnVrfNameLen = i4VrfNameLen;
    pL3VpnIfConfEntry =
        RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                       &L3VpnIfConfEntry, NULL);
    while (pL3VpnIfConfEntry != NULL)
    {
        if (MEMCMP (pL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
                    pu1VrfName, L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            if (pL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex !=
                i4IfIndex)
            {
                CfaGetIfOperStatus ((UINT4) pL3VpnIfConfEntry->MibObject.
                                    i4MplsL3VpnIfConfIndex, &u1OperStatus);
                if (u1OperStatus == CFA_IF_UP)
                {
                    return L3VPN_SUCCESS;
                }
            }
        }
        else
        {
            break;
        }
        pL3VpnIfConfEntry =
            RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                           pL3VpnIfConfEntry, NULL);
    }
    return L3VPN_FAILURE;
}

/******************************************************************************
 * Function   : L3VpnIsAnyInterfaceUpForVrf
 * Description: This function checks whether any any one of the interface mapped to the given VRF 
 *              is up.
 * Input      : pu1VrfName
 *       
 * Output     : NONE
 * Returns    : OSIX_TRUE/OSIX_FALSE
 ************************************************************************************/
UINT4
L3VpnIsAnyInterfaceUpForVrf (UINT1 *pu1VrfName)
{

    tL3vpnMplsL3VpnIfConfEntry L3VpnIfConfEntry;
    UINT1               u1OperStatus = 0;
    tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry = NULL;
    INT4                i4VrfNameLen = 0;
    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMSET (&L3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMCPY (L3VpnIfConfEntry.MibObject.au1MplsL3VpnVrfName, pu1VrfName,
            i4VrfNameLen);
    L3VpnIfConfEntry.MibObject.i4MplsL3VpnVrfNameLen = i4VrfNameLen;
    pL3VpnIfConfEntry =
        RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                       &L3VpnIfConfEntry, NULL);
    while (pL3VpnIfConfEntry != NULL)
    {
        if (MEMCMP (pL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
                    pu1VrfName, L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            CfaGetIfOperStatus ((UINT4) pL3VpnIfConfEntry->MibObject.
                                i4MplsL3VpnIfConfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_UP)
            {
                return OSIX_TRUE;
            }
        }
        else
        {
            break;
        }
        pL3VpnIfConfEntry =
            RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                           pL3VpnIfConfEntry, NULL);
    }
    return OSIX_FALSE;

}

/******************************************************************************
 * Function   : L3VpnGetIfConfEntryWithIfIndex
 * Description: This function returns the IfConf Entry with given IfIndex
 *              
 * Input      : i4IfIndex
 *
 * Output     : NONE
 * Returns    : pL3VpnIfConfEntry
 *************************************************************************************/
tL3vpnMplsL3VpnIfConfEntry *
L3VpnGetIfConfEntryWithIfIndex (UINT4 u4IfIndex)
{
    UINT4               u4Context = 0;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tL3vpnMplsL3VpnVrfEntry *pL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry = NULL;
    INT4                i4VrfNameLen = 0;
    MEMSET (&gIfConfL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4Context) == VCM_FAILURE)
    {
        return NULL;
    }

    if (VcmGetAliasName (u4Context, au1VrfName) == VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Unable to get the Alias name.\r\n"));
        return NULL;
    }
    i4VrfNameLen = (INT4) STRLEN (au1VrfName);
    MEMCPY (gIfConfL3VpnVrfEntry.MibObject.au1MplsL3VpnVrfName, au1VrfName,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (gIfConfL3VpnVrfEntry) = i4VrfNameLen;

    pL3VpnVrfEntry = RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                                (tRBElem *) & gIfConfL3VpnVrfEntry);

    if (pL3VpnVrfEntry == NULL)
    {
        return NULL;
    }

    MEMSET (&gIfConfL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMCPY (gIfConfL3VpnIfConfEntry.MibObject.au1MplsL3VpnVrfName, au1VrfName,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (gIfConfL3VpnIfConfEntry) = i4VrfNameLen;
    pL3VpnIfConfEntry =
        RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                       &gIfConfL3VpnIfConfEntry, NULL);

    while (pL3VpnIfConfEntry != NULL)
    {
        if (MEMCMP (pL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
                    au1VrfName, L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            if (pL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex ==
                (INT4) u4IfIndex)
            {
                return pL3VpnIfConfEntry;
            }
        }
        else
        {
            break;
        }
        pL3VpnIfConfEntry =
            RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                           pL3VpnIfConfEntry, NULL);
    }
    return NULL;
}

/******************************************************************************
 * Function   : L3VpnGetVrfEntryWithIfIndex
 * Description: This function returns the VRF Entry for the given IfIndex.
 *
 * Input      : i4IfIndex
 *
 * Output     : NONE
 * Returns    : pL3VpnVrfEntry
 **************************************************************************************/
tL3vpnMplsL3VpnVrfEntry *
L3VpnGetVrfEntryWithIfIndex (INT4 i4IfIndex)
{

    UINT4               u4ContextId = 0;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tL3vpnMplsL3VpnVrfEntry *pL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfEntry L3VpnVrfEntry;
    INT4                i4VrfNameLen = 0;

    MEMSET (&L3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IfIndex, &u4ContextId) ==
        VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Error in getting Context from IfIndex.\r\n"));
        return NULL;
    }

    if (VcmGetAliasName (u4ContextId, au1VrfName) == VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Unable to get the Alias name.\r\n"));
        return NULL;
    }
    i4VrfNameLen = (INT4) STRLEN (au1VrfName);
    MEMCPY (L3VpnVrfEntry.MibObject.au1MplsL3VpnVrfName, au1VrfName,
            i4VrfNameLen);
    L3VpnVrfEntry.MibObject.i4MplsL3VpnVrfNameLen = i4VrfNameLen;
    pL3VpnVrfEntry = RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                                (tRBElem *) & L3VpnVrfEntry);

    return pL3VpnVrfEntry;
}

/******************************************************************************
 * Function   : L3VpnGetIfConfEntry
 * Description: This function returns the If Conf Entry for the given VRF name
 *              and If Index 
 *
 * Input      : pu1VrfName
 *            : i4IfIndex
 *
 * Output     : NONE
 * Returns    : pL3vpnMplsL3VpnIfConfEntry
 **********************************************************************************/
tL3vpnMplsL3VpnIfConfEntry *
L3VpnGetIfConfEntry (UINT1 *pu1VrfName, INT4 i4IfIndex)
{

    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnMplsL3VpnIfConfEntry L3vpnMplsL3VpnIfConfEntry;
    INT4                i4VrfNameLen = 0;

    if (pu1VrfName == NULL)
    {
        return NULL;
    }

    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMSET (&L3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnIfConfEntry), pu1VrfName,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnIfConfEntry) = i4VrfNameLen;
    L3VPN_IF_CONF_IF_INDEX (L3vpnMplsL3VpnIfConfEntry) = i4IfIndex;

    pL3vpnMplsL3VpnIfConfEntry = (tL3vpnMplsL3VpnIfConfEntry *)
        RBTreeGet (L3VPN_IF_CONF_TABLE, &L3vpnMplsL3VpnIfConfEntry);
    return pL3vpnMplsL3VpnIfConfEntry;

}

/******************************************************************************
 * Function    : MplsL3VpnApiGetVrfTableEntry
 * Description : This function returns the VRF entry for the given VRF name and 
 *               length
 *             
 * Input       : pu1MplsL3VpnVrfName
 *             : i4MplsL3VpnVrfNameLen
 *
 * Output      : NONE
 * Returns     : pL3vpnMplsL3VpnVrfEntry
 *********************************************************************************/
tL3vpnMplsL3VpnVrfEntry *
MplsL3VpnApiGetVrfTableEntry (UINT1 *pu1MplsL3VpnVrfName,
                              INT4 i4MplsL3VpnVrfNameLen)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;

    if (pu1MplsL3VpnVrfName == NULL)
    {
        return NULL;
    }

    MEMSET (&L3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry),
            pu1MplsL3VpnVrfName, i4MplsL3VpnVrfNameLen);

    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry) = i4MplsL3VpnVrfNameLen;

    pL3vpnMplsL3VpnVrfEntry =
        L3vpnGetMplsL3VpnVrfTable (&L3vpnMplsL3VpnVrfEntry);

    return pL3vpnMplsL3VpnVrfEntry;
}

/******************************************************************************
 * Function    : L3VpnGetVrfPerfTableEntry
 * Description : This function returns the Perf Entry for the given VRF
 *               
 *
 * Input       : pu1VrfName
 *
 * Output      : NONE
 * Returns     : pL3vpnMplsL3VpnVrfPerfEntry
 *********************************************************************************/
tL3vpnMplsL3VpnVrfPerfEntry *
L3VpnGetVrfPerfTableEntry (UINT1 *pu1VrfName)
{
    tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    INT4                i4VrfNameLen = 0;

    if (pu1VrfName == NULL)
    {
        return NULL;
    }
    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMSET (&L3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfPerfEntry), pu1VrfName,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfPerfEntry) = i4VrfNameLen;

    pL3vpnMplsL3VpnVrfPerfEntry = (tL3vpnMplsL3VpnVrfPerfEntry *)
        RBTreeGet (L3VPN_PERF_TABLE, &L3vpnMplsL3VpnVrfPerfEntry);
    return pL3vpnMplsL3VpnVrfPerfEntry;
}

/****************************************************************************
 *  Function    :  L3VpnFlushRouteTableForVrf
 *  Input       :  pu1VrfName
 *                 
 *  Description :  This function flushes the RTE table entries.
 *                 
 *  Output      :  None
 *  Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/
UINT4
L3VpnFlushRouteTableForVrf (UINT1 *pu1VrfName)
{

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;

    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

    INT4                i4VrfNameLen = 0;
    if (pu1VrfName == NULL)
    {
        return L3VPN_FAILURE;
    }

    MEMSET (&L3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));
    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), pu1VrfName, i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry) = i4VrfNameLen;

    pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
        RBTreeGetNext (L3VPN_RTE_TABLE, &L3vpnMplsL3VpnVrfEntry, NULL);

    while (pL3vpnMplsL3VpnVrfRteEntry != NULL)
    {
        if (MEMCMP
            (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry), pu1VrfName,
             L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            if (MplsL3vpnIngressUnMap (pL3vpnMplsL3VpnVrfRteEntry) !=
                L3VPN_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "MplsL3vpnIngressUnMap() Failed.\r\n"));
                return L3VPN_FAILURE;
            }
            RBTreeRem (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry);
        }
        else
        {
            break;
        }
        pL3vpnMplsL3VpnVrfRteEntry =
            RBTreeGetNext (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry, NULL);
    }

    pL3VpnBgpRouteLabelEntry = (tL3VpnBgpRouteLabelEntry *)
        RBTreeGetNext (L3VPN_BGP_ROUTE_LABEL_TABLE, &L3VpnBgpRouteLabelEntry,
                       NULL);

    while (pL3VpnBgpRouteLabelEntry != NULL)
    {
        if (MEMCMP
            (L3VPN_P_BGPROUTELABELVRF_VNAME (pL3VpnBgpRouteLabelEntry),
             pu1VrfName, L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            if (MplsL3vpnEgressUnMap (pL3VpnBgpRouteLabelEntry) !=
                L3VPN_SUCCESS)
            {

                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                            "%s : %d Failed\n", __FUNCTION__, __LINE__));
                return L3VPN_FAILURE;
            }
            RBTreeRem (L3VPN_BGP_ROUTE_LABEL_TABLE, pL3VpnBgpRouteLabelEntry);

            MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                                (UINT1 *) pL3VpnBgpRouteLabelEntry);
        }
        else
        {
            break;
        }
        pL3VpnBgpRouteLabelEntry =
            RBTreeGetNext (L3VPN_BGP_ROUTE_LABEL_TABLE,
                           pL3VpnBgpRouteLabelEntry, NULL);
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnDeleteRouteEntry
 * Input       :  pL3vpnMplsL3VpnVrfRteEntry
 *
 * Description :  This function does a full cleanup of tL3vpnMplsL3VpnVrfRteEntry
 *                1. Removes pL3vpnMplsL3VpnVrfRteEntry from the TRIE 
 *                2. Cleanup the XC Entry, outsegment entry associated with 
 *                   the pL3vpnMplsL3VpnVrfRteEntry.
 *                3. Removes pL3vpnMplsL3VpnVrfRteEntry from the RB Tree (MIB)
 *                4. Free the Memory for pL3vpnMplsL3VpnVrfRteEntry
 *
 * Output      :  None
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/
UINT4
L3VpnDeleteRouteEntry (tL3vpnMplsL3VpnVrfRteEntry * pL3vpnMplsL3VpnVrfRteEntry)
{

    UINT1              *pu1RteXcEntry = NULL;
    UINT4               u4XcIndex = 0;
    UINT4               u4InSegmentIndex = 0;
    UINT4               u4OutSegmentIndex = 0;
    UINT4               u4ContextId = 0;
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        return L3VPN_FAILURE;
    }
    pu1RteXcEntry = L3VPN_P_RTE_XC_POINTER (pL3vpnMplsL3VpnVrfRteEntry);

    /* Get the XC, indegment and outsegment indices */
    MEMCPY (&u4XcIndex, pu1RteXcEntry, sizeof (UINT4));
    u4XcIndex = OSIX_NTOHL (u4XcIndex);
    pu1RteXcEntry = pu1RteXcEntry + sizeof (UINT4);

    MEMCPY (&u4InSegmentIndex, pu1RteXcEntry, sizeof (UINT4));
    u4InSegmentIndex = OSIX_NTOHL (u4InSegmentIndex);
    pu1RteXcEntry = pu1RteXcEntry + sizeof (UINT4);

    MEMCPY (&u4OutSegmentIndex, pu1RteXcEntry, sizeof (UINT4));
    u4OutSegmentIndex = OSIX_NTOHL (u4OutSegmentIndex);

    L3VpnDeleteXcEntry (u4XcIndex, u4InSegmentIndex, u4OutSegmentIndex);

    /* Remove the Entry from TRIE data-structure */
    if (VcmIsVrfExist (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry),
                       &u4ContextId) == VCM_FALSE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%The Context does not exist in VCM.\r\n"));
    }
    if (L3VpnDeleteRteTableEntryFromTrie (u4ContextId,
                                          pL3vpnMplsL3VpnVrfRteEntry) ==
        L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in removing the RTE Entry from TRIE data-structure.\r\n"));
    }

    /* Remove the Entry from the RB-Tree */
    RBTreeRem (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry);

    /*De-allocate the memory */

    if (MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry) ==
        MEM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in De-allocating the Memory for the RTE Entyry.\r\n"));
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnDeleteIfConfTableForVrf
 * Input       :  pu1VrfName
 *
 * Description : This function removes IFConf Entries from the RBTree for a 
 *               given VRF and also de-allocate the memory for the same.
 *
 * Output      :  None
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *****************************************************************************/
UINT4
L3VpnDeleteIfConfTableForVrf (UINT1 *pu1VrfName)
{
    tL3vpnMplsL3VpnIfConfEntry L3vpnMplsL3VpnIfConfEntry;
    INT4                i4VrfNameLen = 0;
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC,
                     "FUNC:L3VpnDeleteIfConfTableForVrf\n"));
    if (pu1VrfName == NULL)
    {
        return L3VPN_FAILURE;
    }

    MEMSET (&L3vpnMplsL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnIfConfEntry), pu1VrfName,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnIfConfEntry) = i4VrfNameLen;

    pL3vpnMplsL3VpnIfConfEntry = (tL3vpnMplsL3VpnIfConfEntry *)
        RBTreeGetNext (L3VPN_IF_CONF_TABLE, &L3vpnMplsL3VpnIfConfEntry, NULL);

    while (pL3vpnMplsL3VpnIfConfEntry != NULL)
    {
        if (MEMCMP
            (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnIfConfEntry), pu1VrfName,
             L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            RBTreeRem (L3VPN_IF_CONF_TABLE, pL3vpnMplsL3VpnIfConfEntry);
            if (MEM_FAILURE ==
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnIfConfEntry))
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Error in removing the If Entry for the VRf: %s\n",
                            pu1VrfName));
                return L3VPN_FAILURE;
            }
        }
        else
        {
            break;
        }
        pL3vpnMplsL3VpnIfConfEntry = (tL3vpnMplsL3VpnIfConfEntry *)
            RBTreeGetNext (L3VPN_IF_CONF_TABLE, pL3vpnMplsL3VpnIfConfEntry,
                           NULL);
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnDeleteRtTableForVrf
 * Input       :  pu1VrfName
 *
 * Description : This function removes Route-Target entries from the RB-Tree
 *               for a given VRF. Also de-allocate the memory for the same.
 *
 * Output      :  None
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *****************************************************************************/
UINT4
L3VpnDeleteRtTableForVrf (UINT1 *pu1VrfName)
{

    tL3vpnMplsL3VpnVrfRTEntry L3vpnMplsL3VpnVrfRTEntry;
    INT4                i4VrfNameLen = 0;
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry;
    UINT4               u4ContextId = 0;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    if (pu1VrfName == NULL)
    {
        return L3VPN_FAILURE;
    }

    MEMSET (&L3vpnMplsL3VpnVrfRTEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfRTEntry), pu1VrfName,
            i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfRTEntry) = i4VrfNameLen;

    pL3vpnMplsL3VpnVrfEntry =
        MplsL3VpnApiGetVrfTableEntry (pu1VrfName, i4VrfNameLen);
    if (pL3vpnMplsL3VpnVrfEntry != NULL)
    {
        u4ContextId = pL3vpnMplsL3VpnVrfEntry->MibObject.u4ContextId;
    }
    else
    {
        return L3VPN_FAILURE;
    }

    pL3vpnMplsL3VpnVrfRTEntry = (tL3vpnMplsL3VpnVrfRTEntry *)
        RBTreeGetNext (L3VPN_RT_TABLE, &L3vpnMplsL3VpnVrfRTEntry, NULL);

    while (pL3vpnMplsL3VpnVrfRTEntry != NULL)
    {
        if (MEMCMP
            (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRTEntry), pu1VrfName,
             L3VPN_MAX_VRF_NAME_LEN) == 0)
        {
            L3VpnUtilReleaseRtIndex (u4ContextId,
                                     L3VPN_P_RT_INDEX
                                     (pL3vpnMplsL3VpnVrfRTEntry));
            RBTreeRem (L3VPN_RT_TABLE, pL3vpnMplsL3VpnVrfRTEntry);
            if (MEM_FAILURE == MemReleaseMemBlock
                (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                 (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry))
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Error in removing the RT Entry for the VRF: %s\n",
                            pu1VrfName));
                return L3VPN_FAILURE;
            }
        }
        else
        {
            break;
        }
        pL3vpnMplsL3VpnVrfRTEntry = (tL3vpnMplsL3VpnVrfRTEntry *)
            RBTreeGetNext (L3VPN_RT_TABLE, pL3vpnMplsL3VpnVrfRTEntry, NULL);
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnCreateMplsL3VpnIfConfEntry
 * Input       :  i4IfIndex
 *             :  u4ContextId
 * 
 * Description : This function creates an IfConf Entry with given key indices
 *               i4IfIndex and u4ContextId.
 * 
 * Output      :  None
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *****************************************************************************/
tL3vpnMplsL3VpnIfConfEntry *
L3VpnCreateMplsL3VpnIfConfEntry (INT4 i4IfIndex, UINT4 u4ContextId)
{

    tL3vpnMplsL3VpnIfConfEntry *pIfConfEntry = NULL;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    INT4                i4VrfNameLen = 0;

    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
    pIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pIfConfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Unable to allocate memory\r\n"));
        return NULL;
    }
    MEMSET (pIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    pIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex = i4IfIndex;
    if (VcmGetAliasName ((UINT4) u4ContextId, au1VrfName) == VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Unable to get the Alias name\r\n"));
        /* Clean UP */
        /* Release memory */
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pIfConfEntry);
        return NULL;
    }
    i4VrfNameLen = (INT4) STRLEN (au1VrfName);
    MEMCPY (pIfConfEntry->MibObject.au1MplsL3VpnVrfName, au1VrfName,
            i4VrfNameLen);
    pIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen = i4VrfNameLen;
    return pIfConfEntry;
}

/****************************************************************************
 * Function    :  L3VpnUpdatePerfTableCounters
 * Input       :  pu1VrfName
 *                u4Operation
 * Description :  This function updates the performace counters for the given vrf
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *********************************************************************************/
UINT4
L3VpnUpdatePerfTableCounters (UINT1 *pu1VrfName, UINT4 u4Operation)
{

    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;
    INT4                i4VrfNameLen = 0;
    if (pu1VrfName == NULL)
    {
        return L3VPN_FAILURE;
    }

    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);
    MEMSET (&L3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfPerfEntry), pu1VrfName,
            i4VrfNameLen);

    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfPerfEntry) = i4VrfNameLen;

    pL3vpnMplsL3VpnVrfPerfEntry =
        RBTreeGet (L3VPN_PERF_TABLE, (tRBElem *) & L3vpnMplsL3VpnVrfPerfEntry);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Entry not found in the perf table for the VRF.\r\n"));
        return L3VPN_FAILURE;
    }

    switch (u4Operation)
    {
        case L3VPN_ROUTE_ADDED:
            L3VPN_P_PERF_ROUTES_ADDED (pL3vpnMplsL3VpnVrfPerfEntry)++;
            L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry)++;
            break;

        case L3VPN_ROUTE_DELETED:
            L3VPN_P_PERF_DELETED_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry)++;
            L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry)--;
            break;

        case L3VPN_ROUTE_DROPPED:
            L3VPN_P_PERF_ROUTES_DROPPED (pL3vpnMplsL3VpnVrfPerfEntry)++;
            break;

        default:
            return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnUpdateNoOfConnectedInterfaces
 * Input       :  u1Operation
 *                
 * Description :  This function updates the global counter of connected interfaces.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 **********************************************************************************/
UINT4
L3VpnUpdateNoOfConnectedInterfaces (UINT4 u1Operation)
{

    switch (u1Operation)
    {
        case L3VPN_INTERFACE_MAPPED:
            L3VPN_CONNECTED_INTERFACES++;
            break;

        case L3VPN_INTERFACE_UNMAPPED:
            L3VPN_CONNECTED_INTERFACES--;
            break;

        default:
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Wrong Operation.\r\n"));
            return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;

}

/****************************************************************************
 * Function    :  L3vpnUtlVrfTableRowStatusHdl
 * Input       :  pL3vpnOldMplsL3VpnVrfEntry
 *                pL3vpnMplsL3VpnVrfEntry
 *
 * Description :  This function handles the cases when the row status
 *                is changed for the VRF table.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 **********************************************************************************/
UINT4
L3vpnUtlVrfTableRowStatusHdl (tL3vpnMplsL3VpnVrfEntry *
                              pL3vpnOldMplsL3VpnVrfEntry,
                              tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{

    UINT1               au1Rd[MPLS_MAX_L3VPN_RD_LEN];
    MEMSET (au1Rd, 0, MPLS_MAX_L3VPN_RD_LEN);
    switch (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus)
    {
        case ACTIVE:
            if (pL3vpnOldMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus != ACTIVE)
            {
                if (L3vpnUtlHandleRowStatusActive (pL3vpnMplsL3VpnVrfEntry) !=
                    L3VPN_SUCCESS)
                {
                    return L3VPN_FAILURE;
                }
            }
            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
            /**  send the RD deletion trigger to BGP **/
            if (pL3vpnOldMplsL3VpnVrfEntry == NULL)
            {
                /** The VRF is created for first time do the required processing**/
                if (L3vpnUtlProcessVrfCreation (pL3vpnMplsL3VpnVrfEntry) !=
                    L3VPN_SUCCESS)
                {
                    return L3VPN_FAILURE;
                }
            }
            else
            {
                if (L3vpnUtlHandleRowStatusNotInService
                    (pL3vpnMplsL3VpnVrfEntry) != L3VPN_SUCCESS)
                {
                    return L3VPN_FAILURE;
                }
            }
            break;

        case DESTROY:
            /** The VRF is deleted do the required processing **/
            if (L3vpnUtlProcessVrfDeletion (pL3vpnMplsL3VpnVrfEntry) !=
                L3VPN_SUCCESS)
            {
                return L3VPN_FAILURE;
            }
            break;
        default:
            return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlVrfTableAdminStatusHdl
 * Input       :  pL3vpnOldMplsL3VpnVrfEntry
 *                pL3vpnMplsL3VpnVrfEntry
 * 
 * Description :  This function handles the cases when the Admin status
 *                is changed for a VRF.
 * 
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***********************************************************************************/
UINT4
L3vpnUtlVrfTableAdminStatusHdl (tL3vpnMplsL3VpnVrfEntry *
                                pL3vpnOldMplsL3VpnVrfEntry,
                                tL3vpnMplsL3VpnVrfEntry *
                                pL3vpnMplsL3VpnVrfEntry)
{
    UNUSED_PARAM (pL3vpnOldMplsL3VpnVrfEntry);

    switch (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus)
    {

        case L3VPN_VRF_ADMIN_STATUS_UP:
            if (pL3vpnMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == ACTIVE)
            {
                L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) =
                    L3VPN_VRF_ADMIN_STATUS_UP;
                L3VpnAdminChangeEventHandler (L3VPN_P_VRF_NAME
                                              (pL3vpnMplsL3VpnVrfEntry),
                                              L3VPN_VRF_ADMIN_STATUS_UP);
            }
            break;

        case L3VPN_VRF_ADMIN_STATUS_DOWN:
            L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) =
                L3VPN_VRF_ADMIN_STATUS_DOWN;
            L3VpnAdminChangeEventHandler (L3VPN_P_VRF_NAME
                                          (pL3vpnMplsL3VpnVrfEntry),
                                          L3VPN_VRF_ADMIN_STATUS_DOWN);
            break;

        default:
            return L3VPN_FAILURE;

    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlVrfSecTableRowCreate
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description :  This function creates a VRF Security row in the Sec table.
 * 
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***********************************************************************************/
UINT4
L3vpnUtlVrfSecTableRowCreate (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    pL3vpnMplsL3VpnVrfSecEntry = (tL3vpnMplsL3VpnVrfSecEntry *) MemAllocMemBlk
        (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return L3VPN_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfSecEntry, 0, sizeof (tL3vpnMplsL3VpnVrfSecEntry));

    MEMCPY (pL3vpnMplsL3VpnVrfSecEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.
            au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);

    pL3vpnMplsL3VpnVrfSecEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    if (RBTreeAdd (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable,
                   (tRBElem *) pL3vpnMplsL3VpnVrfSecEntry) != RB_SUCCESS)
    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlGetVrfSecTable
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description :  This function returns the Sec Entry for the given VRF 
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***********************************************************************************/
tL3vpnMplsL3VpnVrfSecEntry *
L3vpnUtlGetVrfSecTableEntry (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{

    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;
    tL3vpnMplsL3VpnVrfSecEntry L3vpnMplsL3VpnVrfSecEntry;

    MEMSET (&L3vpnMplsL3VpnVrfSecEntry, 0, sizeof (tL3vpnMplsL3VpnVrfSecEntry));

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return NULL;
    }

    MEMCPY (L3vpnMplsL3VpnVrfSecEntry.MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.
            au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);

    L3vpnMplsL3VpnVrfSecEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    pL3vpnMplsL3VpnVrfSecEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable,
                   (tRBElem *) & L3vpnMplsL3VpnVrfSecEntry);
    return pL3vpnMplsL3VpnVrfSecEntry;
}

/****************************************************************************
 * Function    :  L3vpnUtlVrfSecTableRowDelete
 * Input       :  pL3vpnMplsL3VpnVrfSecEntry
 *
 * Description :  This function removes the VRf Sec entry from the RB Tree
 *                and releases the memory.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***********************************************************************************/
UINT4
L3vpnUtlVrfSecTableRowDelete (tL3vpnMplsL3VpnVrfSecEntry *
                              pL3vpnMplsL3VpnVrfSecEntry)
{
    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return L3VPN_FAILURE;
    }

    RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable,
               pL3vpnMplsL3VpnVrfSecEntry);

    /* Free the memory */
    if (MEM_FAILURE == MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID,
                                           (UINT1 *)
                                           pL3vpnMplsL3VpnVrfSecEntry))
    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlUpdateVrfSecTable
 * Input       :  pL3vpnMplsL3VpnVrfSecEntry
 *
 * Description :  This function updates the VRF Sceurity entry as per the 
 *                given operation
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***********************************************************************************/
UINT4
L3vpnUtlUpdateVrfSecTable (tL3vpnMplsL3VpnVrfSecEntry *
                           pL3vpnMplsL3VpnVrfSecEntry, UINT4 u4Operation)
{
    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        return L3VPN_FAILURE;
    }
    switch (u4Operation)
    {
        case L3VPN_VRF_SEC_ILLEGAL_LBL_VL_OP:
            pL3vpnMplsL3VpnVrfSecEntry->MibObject.
                u4MplsL3VpnVrfSecIllegalLblVltns++;
            break;

        case L3VPN_VRF_SEC_DIS_CONT_TIME_OP:
            pL3vpnMplsL3VpnVrfSecEntry->MibObject.
                u4MplsL3VpnVrfSecDiscontinuityTime++;
            break;

        default:
            return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlProcessVrfCreation
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function does the handling for the VRF creation, 
 *               when it is created for the first time.
 *               
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ************************************************************************************/
UINT4
L3vpnUtlProcessVrfCreation (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    UINT4               u4ContextId = 0;
    UINT4               u4NoOfInterfaces = 0;
    UINT4               u4Count = 0;
    UINT4               u4TimeTicks = 0;
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }

    MEMSET (gu4InterfaceList, 0, IPIF_MAX_LOGICAL_IFACES + 1);
    L3VPN_TRC ((L3VPN_MAIN_TRC, "IPIF_MAX_LOGICAL_IFACES = %d\n",
                IPIF_MAX_LOGICAL_IFACES));

    if (VcmIsVrfExist (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry), &u4ContextId)
        == VCM_TRUE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4ContextId = u4ContextId;
    }
    else
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%VRF must exist in VCM\r\n"));
        return L3VPN_FAILURE;
    }

    /* Init the variables */
    L3vpnUtlInitVrfTable (pL3vpnMplsL3VpnVrfEntry);

    /* To associate the interfaces with MPLS VRF, which already exist in VCM */
    if (VcmGetCxtIpIfaceList (u4ContextId, gu4InterfaceList) == VCM_SUCCESS)
    {
        u4NoOfInterfaces = gu4InterfaceList[0];
        for (u4Count = 1; u4Count <= u4NoOfInterfaces; u4Count++)
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC, "Mapping the inetrafce %d\n",
                        gu4InterfaceList[u4Count]));
            if (L3VpnMapInterfaceToVrf
                (u4ContextId, (INT4) gu4InterfaceList[u4Count]) == VCM_FAILURE)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Error in mapping the interface %d to the context Id: %d\n",
                            gu4InterfaceList[u4Count], u4ContextId));
                return L3VPN_FAILURE;
            }
        }
    }
    else
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Failure in fetching Interface list\r\n"));
        return L3VPN_FAILURE;
    }

    /* Memory Allocation for Sec Table */
    if (L3vpnUtlVrfSecTableRowCreate (pL3vpnMplsL3VpnVrfEntry) != L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }

    /* Memory Allocation for Perf Table */
    if (L3vpnUtlVrfPerfTableRowCreate (pL3vpnMplsL3VpnVrfEntry) !=
        L3VPN_SUCCESS)
    {
        /* Clean Up the Security Entry */
        pL3vpnMplsL3VpnVrfSecEntry =
            L3vpnUtlGetVrfSecTableEntry (pL3vpnMplsL3VpnVrfEntry);
        L3vpnUtlVrfSecTableRowDelete (pL3vpnMplsL3VpnVrfSecEntry);
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%creation of VRF Security row in the Sec table fail.\r\n"));
        return L3VPN_FAILURE;
    }

    /* Update the scaler mib object */
    L3VPN_CONFIGURED_VRFS++;

    /* Send VRF creation to BGP */
    if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
    {
        (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                L3VPN_BGP4_VRF_CREATED);
    }
    else
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Cannot send notification to BGP about VRF creation.. Callback is NULL.\r\n"));
    }

    /* Update the VRF creation time */
    OsixGetSysTime (&u4TimeTicks);
    L3VPN_P_VRF_CREATION_TIME (pL3vpnMplsL3VpnVrfEntry) = u4TimeTicks;
    L3VPN_TRC ((L3VPN_MAIN_TRC, "Send VRF creation to BGP\n"));
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlProcessVrfDeletion
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function does the handling for MPLS VRF deletion.
 *               Clean up the RTE table, IfConf Table, RT Table 
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *****************************************************************************/
UINT4
L3vpnUtlProcessVrfDeletion (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    UINT4               u4ContextId = 0;

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }

    u4ContextId = pL3vpnMplsL3VpnVrfEntry->MibObject.u4ContextId;

    /* De-allocate memory for sec table */
    pL3vpnMplsL3VpnVrfSecEntry =
        L3vpnUtlGetVrfSecTableEntry (pL3vpnMplsL3VpnVrfEntry);
    if (L3vpnUtlVrfSecTableRowDelete (pL3vpnMplsL3VpnVrfSecEntry) !=
        L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }

    /* De-allocate memory for perf table */
    pL3vpnMplsL3VpnVrfPerfEntry =
        L3vpnUtlGetVrfPerfTableEntry (pL3vpnMplsL3VpnVrfEntry);
    if (L3vpnUtlVrfPerfTableRowDelete (pL3vpnMplsL3VpnVrfPerfEntry) !=
        L3VPN_SUCCESS)
    {
        return L3VPN_FAILURE;
    }
    /* Update the scalar configured VRF */
    L3VPN_CONFIGURED_VRFS--;

    /* Flush the Routes in the RTE table */
    L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Deleting the RTE Table.\r\n"));
    if (L3VpnFlushRouteTableForVrf (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry))
        == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in flushing the route table.\r\n"));
        return L3VPN_FAILURE;
    }

    /* Delete the IfConf Entries for the given VRF */
    L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Deleting the If Conf Table.\r\n"));
    if (L3VpnDeleteIfConfTableForVrf
        (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "Error in Deleting the IfConf Entries for VRF: %s\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        return L3VPN_FAILURE;
    }

    /* Delete RT table for the VRF */
    L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Deleting the RT Table.\r\n"));
    if (L3VpnDeleteRtTableForVrf (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)) ==
        L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "Error in Deleting the RT Table for the VRF: %s\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        return L3VPN_FAILURE;
    }

    /* Send the VRF deletion trigger to BGP */
    L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Send VRF deletion to BGP\r\n"));
    if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
    {
        (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                L3VPN_BGP4_VRF_DELETED);
    }
    else
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Cannot send notification to BGP about VRF creation.. Callback is NULL\r\n"));
    }

    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlInitVrfTable 
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function initializes the VRF row with default values.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *****************************************************************************/
VOID
L3vpnUtlInitVrfTable (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;
    L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) =
        L3VPN_VRF_ADMIN_STATUS_DOWN;
    L3VPN_P_VRF_MID_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) = OSIX_FALSE;
    /*L3VPN_P_VRF_HIGH_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) = OSIX_FALSE; */

    L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry) =
        L3VPN_PER_VRF_MAX_ROUTES;
    L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry) = L3VPN_DEFAULT_MAX_TH;
    L3VPN_P_VRF_MID_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry) = L3VPN_DEFAULT_MID_TH;
}

/****************************************************************************
 * Function    :  L3vpnUtlVrfPerfTableRowCreate
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function allocates memory for Perf Table row
 *               and adds the entry to the RB tree.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ******************************************************************************/
UINT4
L3vpnUtlVrfPerfTableRowCreate (tL3vpnMplsL3VpnVrfEntry *
                               pL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry = (tL3vpnMplsL3VpnVrfPerfEntry *) MemAllocMemBlk
        (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }
    MEMSET (pL3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.
            au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);

    pL3vpnMplsL3VpnVrfPerfEntry->MibObject.i4MplsL3VpnVrfNameLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    if (RBTreeAdd (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable,
                   (tRBElem *) pL3vpnMplsL3VpnVrfPerfEntry) != RB_SUCCESS)
    {

        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlGetVrfPerfTableEntry
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function gets the Perf Entry for the given VRF.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 *****************************************************************************/
tL3vpnMplsL3VpnVrfPerfEntry *
L3vpnUtlGetVrfPerfTableEntry (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    tL3vpnMplsL3VpnVrfPerfEntry L3vpnMplsL3VpnVrfPerfEntry;

    MEMSET (&L3vpnMplsL3VpnVrfPerfEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfPerfEntry));

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return NULL;
    }

    MEMCPY (L3vpnMplsL3VpnVrfPerfEntry.MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.
            au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);

    L3vpnMplsL3VpnVrfPerfEntry.MibObject.i4MplsL3VpnVrfNameLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

    pL3vpnMplsL3VpnVrfPerfEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable,
                   (tRBElem *) & L3vpnMplsL3VpnVrfPerfEntry);

    return pL3vpnMplsL3VpnVrfPerfEntry;
}

/****************************************************************************
 * Function    :  L3vpnUtlVrfPerfTableRowDelete
 * Input       :  pL3vpnMplsL3VpnVrfPerfEntry
 *
 * Description : This function removes the Perf Entry from the RB tree/
 *               Releases the memory for the same.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/
UINT4
L3vpnUtlVrfPerfTableRowDelete (tL3vpnMplsL3VpnVrfPerfEntry *
                               pL3vpnMplsL3VpnVrfPerfEntry)
{
    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }
    RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable,
               pL3vpnMplsL3VpnVrfPerfEntry);

    if (MEM_FAILURE == MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                                           (UINT1 *)
                                           pL3vpnMplsL3VpnVrfPerfEntry))
    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlHandleRowStatusActive
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function is called when the VRF row status is changed to 
 *               ACTIVE
 *               
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/
UINT4
L3vpnUtlHandleRowStatusActive (tL3vpnMplsL3VpnVrfEntry *
                               pL3vpnMplsL3VpnVrfEntry)
{
    UINT4               u4ContextId = 0;

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }
    if (VcmIsVrfExist (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
                       &u4ContextId) == VCM_FALSE)
    {
        return L3VPN_FAILURE;
    }

    /* Send the RD Add trigger to BGP */
    if (L3VPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
    {
        (*(L3VPN_NOTIFY_ROUTE_PARAMS_CB)) (u4ContextId, L3VPN_BGP4_RD,
                                           L3VPN_BGP4_ROUTE_PARAM_ADD,
                                           L3VPN_P_VRF_RD
                                           (pL3vpnMplsL3VpnVrfEntry));
    }
    else
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Not able to send the RD add notification to BGP.. Callback is NULL.\r\n"));
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Send the RD Set trigger to BGP.\r\n"));
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Give RD ADD trigger to BGP.\r\n"));

    if (L3VPN_P_VRF_ADMIN_STATUS (pL3vpnMplsL3VpnVrfEntry) ==
        L3VPN_VRF_ADMIN_STATUS_UP)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Sending Admin up Event to L3VPN task.\r\n"));
        L3VpnAdminChangeEventHandler (L3VPN_P_VRF_NAME
                                      (pL3vpnMplsL3VpnVrfEntry),
                                      L3VPN_VRF_ADMIN_STATUS_UP);
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Send the Admin UP status to L3VPN.\r\n"));
    }

    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlHandleRowStatusNotInService
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 * 
 * Description : This function is called when the VRF row status is changed to
 *               NOT_IN_SERVICE
 * 
 * 
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/
UINT4
L3vpnUtlHandleRowStatusNotInService (tL3vpnMplsL3VpnVrfEntry *
                                     pL3vpnMplsL3VpnVrfEntry)
{
    UINT4               u4ContextId = 0;
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }

    if (VcmIsVrfExist (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
                       &u4ContextId) == VCM_FALSE)
    {
        return L3VPN_FAILURE;
    }

    /* Send the RD Delete Trigger to BGP */
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\rSend the RD Re-Set trigger to BGP\r\n"));

    if (L3VPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
    {
        (*(L3VPN_NOTIFY_ROUTE_PARAMS_CB)) (u4ContextId, L3VPN_BGP4_RD,
                                           L3VPN_BGP4_ROUTE_PARAM_DEL,
                                           L3VPN_P_VRF_RD
                                           (pL3vpnMplsL3VpnVrfEntry));
    }
    else
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Not able to send the RD Delete notification to BGP.. Callback is NULL.\r\n"));
    }

    /* Mark the Oper status as Down */
    if (L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry) == L3VPN_VRF_OPER_UP)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Marking the oper state down.\r\n"));
        L3VPN_P_VRF_OPER_STATUS (pL3vpnMplsL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;

        /* Send VRF DOWN trigger to BGP */
        if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
        {
            (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                    L3VPN_BGP4_VRF_DOWN);
        }
        else
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "\r%%Not able to send the VRF down notification to BGP.. Callback is NULL.\r\n"));
        }
    }

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Send the oper-Down trigger to BGP.\r\n"));

    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnGetVrfEntryByContextId
 * Input       :  u4ContextId
 *
 * Description : This function returns the  tL3vpnMplsL3VpnVrfEntry based on 
 *               ContextId.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/
tL3vpnMplsL3VpnVrfEntry *
L3VpnGetVrfEntryByContextId (UINT4 u4ContextId)
{

    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry = RBTreeGetFirst (L3VPN_VRF_TABLE);
    while (pL3vpnMplsL3VpnVrfEntry != NULL)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.u4ContextId == u4ContextId)
        {
            return pL3vpnMplsL3VpnVrfEntry;
        }
        pL3vpnMplsL3VpnVrfEntry =
            RBTreeGetNext (L3VPN_VRF_TABLE, pL3vpnMplsL3VpnVrfEntry, NULL);
    }
    return NULL;
}

/****************************************************************************
 * Function    :  L3VpnBgpRouteLabelRBTreeCreate
 * Input       :  NONE
 *
 * Description : This function creates the RB tree for the egress 
 *               L3VPN side routes. (Internal data-structure, NOT in MIB)
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***************************************************************************/
UINT4
L3VpnBgpRouteLabelRBTreeCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3VpnBgpRouteLabelEntry, L3VpnBgpRouteLabelNode);

    if ((L3VPN_BGP_ROUTE_LABEL_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               L3VpnBgpRouteLabelTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnBgpRouteLabelTableRBCmp
 * Input       :  pRBElem1
 *             :  pRBElem2
 * 
 * Description : This is comparison function for RouteLabelTable (Egress L3VPN
 *               Routes data-structure)
 *
 * Output      :  NONE
 * Returns     :  -1/0/1
 ***************************************************************************/
INT4
L3VpnBgpRouteLabelTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelInfo1 =
        (tL3VpnBgpRouteLabelEntry *) pRBElem1;
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelInfo2 =
        (tL3VpnBgpRouteLabelEntry *) pRBElem2;

    if (pL3VpnBgpRouteLabelInfo1->u4Label < pL3VpnBgpRouteLabelInfo2->u4Label)
    {
        return -1;
    }
    else if (pL3VpnBgpRouteLabelInfo1->u4Label >
             pL3VpnBgpRouteLabelInfo2->u4Label)
    {
        return 1;
    }
    return 0;
}

/****************************************************************************
 * Function    :  L3vpnGetBgpRouteLabelTable
 * Input       :  pL3VpnBgpRouteLabelEntry
 *
 * Description : This function returns the RouteLabelEntry based on label.
 *
 * Output      :  NONE
 * Returns     :  pGetL3VpnBgpRouteLabelEntry
 ***************************************************************************/
tL3VpnBgpRouteLabelEntry *
L3vpnGetBgpRouteLabelTable (tL3VpnBgpRouteLabelEntry * pL3VpnBgpRouteLabelEntry)
{
    tL3VpnBgpRouteLabelEntry *pGetL3VpnBgpRouteLabelEntry = NULL;

    pGetL3VpnBgpRouteLabelEntry =
        (tL3VpnBgpRouteLabelEntry *) RBTreeGet (L3VPN_BGP_ROUTE_LABEL_TABLE,
                                                (tRBElem *)
                                                pL3VpnBgpRouteLabelEntry);
    return pGetL3VpnBgpRouteLabelEntry;
}

/****************************************************************************
 * Function    :  L3vpnGetFirstBgpRouteLabelTable
 * Input       :  NONE
 *
 * Description : This function returns the first entry in the RouteLabelTable.
 *
 * Output      :  NONE
 * Returns     :  pGetL3VpnBgpRouteLabelEntry
 ****************************************************************************/
tL3VpnBgpRouteLabelEntry *
L3vpnGetFirstBgpRouteLabelTable (VOID)
{
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;

    pL3VpnBgpRouteLabelEntry =
        (tL3VpnBgpRouteLabelEntry *)
        RBTreeGetFirst (L3VPN_BGP_ROUTE_LABEL_TABLE);
    return pL3VpnBgpRouteLabelEntry;
}

/****************************************************************************
 * Function    :  L3vpnGetNextBgpRouteLabelTable
 * Input       :  pCurrentL3VpnBgpRouteLabelEntry
 *
 * Description : This function returns the next entry for the given 
 *               pCurrentL3VpnBgpRouteLabelEntry
 *
 * Output      :  NONE
 * Returns     :  pNextL3VpnBgpRouteLabelEntry
 ****************************************************************************/
tL3VpnBgpRouteLabelEntry *
L3vpnGetNextBgpRouteLabelTable (tL3VpnBgpRouteLabelEntry *
                                pCurrentL3VpnBgpRouteLabelEntry)
{
    tL3VpnBgpRouteLabelEntry *pNextL3VpnBgpRouteLabelEntry = NULL;

    pNextL3VpnBgpRouteLabelEntry =
        (tL3VpnBgpRouteLabelEntry *) RBTreeGetNext (L3VPN_BGP_ROUTE_LABEL_TABLE,
                                                    (tRBElem *)
                                                    pCurrentL3VpnBgpRouteLabelEntry,
                                                    NULL);
    return pNextL3VpnBgpRouteLabelEntry;
}

/****************************************************************************
 * Function    :  L3VpnIsRouteAdditionAllowed
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *             :  pL3vpnMplsL3VpnVrfPerfEntry
 *
 * Description : This function checks whether route addition is allowed 
 *               in the RTE table for the VRF. Checks with maximum configured
 *               allowed routes.
 *
 * Output      :  NONE
 * Returns     :  OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/
INT4
L3VpnIsRouteAdditionAllowed (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry,
                             tL3vpnMplsL3VpnVrfPerfEntry *
                             pL3vpnMplsL3VpnVrfPerfEntry)
{
    if (pL3vpnMplsL3VpnVrfEntry == NULL || pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return OSIX_FALSE;
    }
    if (L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) ==
        L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry))
    {

        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Current number of routes have reached the Maximum limit\r\n"));
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}

/****************************************************************************
 * Function    :  L3VpnChckAndSendRouteNotifications
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *             :  pL3vpnMplsL3VpnVrfPerfEntry
 *             :  u1Action
 *
 * Description : This function checks and send the route related traps for 
 *               L3VPN based on the u1Action passed.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***************************************************************************/
UINT4
L3VpnChckAndSendRouteNotifications (tL3vpnMplsL3VpnVrfEntry *
                                    pL3vpnMplsL3VpnVrfEntry,
                                    tL3vpnMplsL3VpnVrfPerfEntry *
                                    pL3vpnMplsL3VpnVrfPerfEntry, UINT1 u1Action)
{
    UINT4               u4CurrTime = 0;

    if (pL3vpnMplsL3VpnVrfEntry == NULL || pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        return L3VPN_FAILURE;
    }

    /* Check if traps are enabled for L3VPN */
    if (L3VPN_NOTIFICATION_ENABLE == L3VPN_TRAP_DISABLE)
    {
        return L3VPN_SUCCESS;
    }

    u4CurrTime = OsixGetSysUpTime ();

    switch (u1Action)
    {

        case L3VPN_ROUTE_ADDED:
            /* Check if the high threhold equals the MAX allowed routes */
            if (L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry) ==
                L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry))
            {
                /* Check if the current routes has reached the threshold */
                if (L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) ==
                    L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry))
                {
                    if (L3VpnIssueHighThrshTrapNotif (pL3vpnMplsL3VpnVrfEntry,
                                                      u4CurrTime) ==
                        L3VPN_FAILURE)
                    {
                        return L3VPN_FAILURE;
                    }
                }
            }
            else                /* high threshold is less than the MAX allowed routes */
            {
                /* Check if the current routes exceeds the high threshold */
                if (L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) >
                    L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry))
                {
                    if (L3VpnIssueHighThrshTrapNotif (pL3vpnMplsL3VpnVrfEntry,
                                                      u4CurrTime) ==
                        L3VPN_FAILURE)
                    {
                        return L3VPN_FAILURE;
                    }
                }
            }

            /* Check if the MID-Threshold Trap needs to be generated */
            if ((L3VPN_P_VRF_MID_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) ==
                 OSIX_FALSE)
                && L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) >
                L3VPN_P_VRF_MID_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry))
            {
                /* API to Send Trap */
                if (L3VpnSendTrapForRouteThreshold
                    (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
                     L3VPN_VRF_MID_THRESHOLD_EXCEEDED) == L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Failure in sending trap MID Threshold Exceeded notification for the VRF: %s\n",
                                L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
                    return L3VPN_FAILURE;
                }

                L3VPN_P_VRF_MID_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) =
                    OSIX_TRUE;
            }
            break;

        case L3VPN_ROUTE_DELETED:
            /* Check if the High Threshold is equal to the max routes */
            if (L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry) ==
                L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry))
            {
                /* Has the High Thrsh trap beed isuued */
                if (L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT
                    (pL3vpnMplsL3VpnVrfEntry) > 0)
                {
                    /* Check if the curr routes have fallen below the threshold */
                    if (L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry)
                        ==
                        L3VPN_P_VRF_CONF_MAX_ROUTES (pL3vpnMplsL3VpnVrfEntry) -
                        1)
                    {
                        /* Api to send the Trap */
                        if (L3VpnSendTrapForRouteThreshold
                            (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
                             L3VPN_VRF_MAX_THRESHOLD_CLEARED) == L3VPN_FAILURE)
                        {
                            L3VPN_TRC ((L3VPN_MAIN_TRC,
                                        "Failure in sending trap MAX Threshold CLEARED notification for the VRF: %s\n",
                                        L3VPN_P_VRF_NAME
                                        (pL3vpnMplsL3VpnVrfEntry)));
                            return L3VPN_FAILURE;
                        }
                        /* Update the last sent time to 0 */
                        L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT
                            (pL3vpnMplsL3VpnVrfEntry) = L3VPN_ZERO;
                    }
                }
            }
            else
            {
                /* Has the High Thrsh trap beed isuued */
                if (L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT
                    (pL3vpnMplsL3VpnVrfEntry) > 0)
                {
                    if (L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry)
                        <= L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry))
                    {
                        /* Api to send the Trap */
                        if (L3VpnSendTrapForRouteThreshold
                            (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
                             L3VPN_VRF_MAX_THRESHOLD_CLEARED) == L3VPN_FAILURE)
                        {
                            L3VPN_TRC ((L3VPN_MAIN_TRC,
                                        "Failure in sending trap MAX Threshold CLEARED notification for the VRF: %s\n",
                                        L3VPN_P_VRF_NAME
                                        (pL3vpnMplsL3VpnVrfEntry)));
                            return L3VPN_FAILURE;
                        }
                        /* Update the last sent time to 0 */
                        L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT
                            (pL3vpnMplsL3VpnVrfEntry) = L3VPN_ZERO;
                    }
                }
            }

            /* Also reset the Flag for the Mid-threshold */
            if (L3VPN_P_VRF_MID_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) ==
                OSIX_TRUE)
            {
                if (L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry) <=
                    L3VPN_P_VRF_MID_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry))
                {
                    L3VPN_P_VRF_MID_THRESH_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) =
                        OSIX_FALSE;
                }
            }
            break;

        default:
            return L3VPN_FAILURE;

    }
    return L3VPN_SUCCESS;
}

UINT4
L3VpnIssueHighThrshTrapNotif (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry,
                              UINT4 u4CurrTime)
{

    if (L3VPN_RTE_MAX_THRSH_TIME == 0)    /* Only a single notification is allowed */
    {
        /* If trap has not yet been sent */
        if (L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) ==
            0)
        {
            /* Send the Trap */
            if (L3VpnSendTrapForRouteThreshold
                (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
                 L3VPN_VRF_MAX_THRESHOLD_EXCEEDED) == L3VPN_FAILURE)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Failure in sending trap MAX Threshold exceeded notification for the VRF: %s\n",
                            L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
                return L3VPN_FAILURE;
            }
            /* Update the last trap sent time */
            L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) =
                u4CurrTime;
        }
    }
    else                        /* Multiple notifications are allowed */
    {
        /* Time stamp is in milliseconds */
        if ((u4CurrTime - L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT
             (pL3vpnMplsL3VpnVrfEntry)) >= L3VPN_RTE_MAX_THRSH_TIME)
        {
            /* Send the Trap */
            if (L3VpnSendTrapForRouteThreshold
                (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
                 L3VPN_VRF_MAX_THRESHOLD_EXCEEDED) == L3VPN_FAILURE)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Failure in sending trap MAX Threshold exceeded notification for the VRF: %s\n",
                            L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
                return L3VPN_FAILURE;
            }
            /* Update the last trap sent time */
            L3VPN_P_VRF_HIGH_THRSH_LAST_TRAP_SENT (pL3vpnMplsL3VpnVrfEntry) =
                u4CurrTime;
        }
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  MplsL3VpnHandleEgressMapArpResolveTmrEvent
 * Input       :  pVoid
 *
 * Description : Timer callback function for Arp resolution for L3VPN 
 *               egress routes for per-route scenario.
 * 
 * Output      :  NONE
 * Returns     :  NONE
 ***************************************************************************/
VOID
MplsL3VpnHandleEgressMapArpResolveTmrEvent (VOID *pVoid)
{
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

    UNUSED_PARAM (pVoid);

    MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s: Called to update in HW\n",
                __FUNCTION__));

    while (OSIX_TRUE)
    {
        pL3VpnBgpRouteLabelEntry =
            L3vpnGetNextBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);
        if (pL3VpnBgpRouteLabelEntry == NULL)
        {
            break;
        }

        /* Check if HW Programming Pending, Call */
        if ((L3VPN_P_BGPROUTELABEL_POLICY (pL3VpnBgpRouteLabelEntry) ==
             L3VPN_BGP4_POLICY_PER_ROUTE)
            && (pL3VpnBgpRouteLabelEntry->u1ArpResolveStatus ==
                MPLS_ARP_RESOLVE_WAITING))
        {
            /**** 2. Call the NPAPI to program     ****/
            if (MplsL3vpnEgressMap (pL3VpnBgpRouteLabelEntry) != L3VPN_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\n%s: MplsL3vpnEgressMap() Failed to update in HW\n",
                            __FUNCTION__));
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\n%s: Programmed Successfully in HW for Label (%d)\n",
                            __FUNCTION__, pL3VpnBgpRouteLabelEntry->u4Label));
            }
        }
        MEMCPY (&L3VpnBgpRouteLabelEntry, pL3VpnBgpRouteLabelEntry,
                sizeof (tL3VpnBgpRouteLabelEntry));
    }
    return;
}

 /****************************************************************************
 * Function    :  L3VpnUpdateVrfConfLastChanged
 * Input       :  pL3vpnMplsL3VpnVrfEntry
 *
 * Description : This function updates the timestamp for VRF last configuration
 *               changed.
 *               
 *
 * Output      :  NONE
 * Returns     :  NONE
 ***************************************************************************/
VOID
L3VpnUpdateVrfConfLastChanged (tL3vpnMplsL3VpnVrfEntry *
                               pL3vpnMplsL3VpnVrfEntry)
{
    UINT4               u4TimeTicks = 0;

    OsixGetSysTime (&u4TimeTicks);
    L3VPN_P_VRF_CONF_LAST_CHANGED (pL3vpnMplsL3VpnVrfEntry) = u4TimeTicks;
}

/* RSVP-TE MPLS-L3VPN-TE */
/****************************************************************************
 * Function    :  L3VpnRsvpMapLabelRBTreeCreate
 * Input       :  NONE
 *
 * Description : This function creates the RB tree for the egress 
 *               L3VPN side routes. (Internal data-structure, NOT in MIB)
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ***************************************************************************/
UINT4
L3VpnRsvpMapLabelRBTreeCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3VpnRsvpMapLabelEntry, L3VpnRsvpMapLabelNode);

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    if ((L3VPN_RSVP_MAP_LABEL_TABLE =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               L3VpnRsvpMapLabelTableRBCmp)) == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "L3VpnRsvpMapLabelEntry RBTreeCreation Failed:!\n"));
        return OSIX_FAILURE;
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnRsvpMapLabelTableRBCmp
 * Input       :  pRBElem1
 *             :  pRBElem2
 * 
 * Description : This is comparison function for RouteLabelTable (Egress L3VPN
 *               Routes data-structure)
 *
 * Output      :  NONE
 * Returns     :  -1/0/1
 ***************************************************************************/
INT4
L3VpnRsvpMapLabelTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelInfo1 =
        (tL3VpnRsvpMapLabelEntry *) pRBElem1;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelInfo2 =
        (tL3VpnRsvpMapLabelEntry *) pRBElem2;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    if (pL3VpnRsvpMapLabelInfo1->u4Prefix < pL3VpnRsvpMapLabelInfo2->u4Prefix)
    {
        return -1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u4Prefix >
             pL3VpnRsvpMapLabelInfo2->u4Prefix)
    {
        return 1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u4Mask < pL3VpnRsvpMapLabelInfo2->u4Mask)
    {
        return -1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u4Mask > pL3VpnRsvpMapLabelInfo2->u4Mask)
    {
        return 1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u1PrefixType <
             pL3VpnRsvpMapLabelInfo2->u1PrefixType)
    {
        return -1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u1PrefixType >
             pL3VpnRsvpMapLabelInfo2->u1PrefixType)
    {
        return 1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u1MaskType <
             pL3VpnRsvpMapLabelInfo2->u1MaskType)
    {
        return -1;
    }
    else if (pL3VpnRsvpMapLabelInfo1->u1MaskType >
             pL3VpnRsvpMapLabelInfo2->u1MaskType)
    {
        return 1;
    }

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return 0;
}

/****************************************************************************
 * Function    :  L3vpnGetRsvpMapLabelTable
 * Input       :  pL3VpnRsvpMapLabelEntry
 *
 * Description : This function returns the RouteLabelEntry based on label.
 *
 * Output      :  NONE
 * Returns     :  pGetL3VpnRsvpMapLabelEntry
 ***************************************************************************/
tL3VpnRsvpMapLabelEntry *
L3vpnGetRsvpMapLabelTable (tL3VpnRsvpMapLabelEntry * pL3VpnRsvpMapLabelEntry)
{
    tL3VpnRsvpMapLabelEntry *pGetL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    pGetL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *) RBTreeGet (L3VPN_RSVP_MAP_LABEL_TABLE,
                                               (tRBElem *)
                                               pL3VpnRsvpMapLabelEntry);

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return pGetL3VpnRsvpMapLabelEntry;
}

/****************************************************************************
 * Function    :  L3vpnGetFirstRsvpMapLabelTable
 * Input       :  NONE
 *
 * Description : This function returns the first entry in the RouteLabelTable.
 *
 * Output      :  NONE
 * Returns     :  pGetL3VpnRsvpMapLabelEntry
 ****************************************************************************/
tL3VpnRsvpMapLabelEntry *
L3vpnGetFirstRsvpMapLabelTable (VOID)
{
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    pL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *) RBTreeGetFirst (L3VPN_RSVP_MAP_LABEL_TABLE);

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return pL3VpnRsvpMapLabelEntry;
}

/****************************************************************************
 * Function    :  L3vpnGetNextRsvpMapLabelTable
 * Input       :  pCurrentL3VpnRsvpMapLabelEntry
 *
 * Description : This function returns the next entry for the given 
 *               pCurrentL3VpnRsvpMapLabelEntry
 *
 * Output      :  NONE
 * Returns     :  pNextL3VpnRsvpMapLabelEntry
 ****************************************************************************/
tL3VpnRsvpMapLabelEntry *
L3vpnGetNextRsvpMapLabelTable (tL3VpnRsvpMapLabelEntry *
                               pCurrentL3VpnRsvpMapLabelEntry)
{
    tL3VpnRsvpMapLabelEntry *pNextL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    pNextL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *) RBTreeGetNext (L3VPN_RSVP_MAP_LABEL_TABLE,
                                                   (tRBElem *)
                                                   pCurrentL3VpnRsvpMapLabelEntry,
                                                   NULL);

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return pNextL3VpnRsvpMapLabelEntry;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L3VpnCreateRsvpMapLabelTable                       */
/*                                                                           */
/*     DESCRIPTION      : This function used to add Rbtree entry             */
/*                         to current L3VpnAddRsvpMapLabel Entry.             */
/*     INPUT            :                                                    */
/*                                                                           */
/*     OUTPUT           :                                                    */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
tL3VpnRsvpMapLabelEntry *
L3VpnCreateRsvpMapLabelTable (UINT1 u1PrefixType, UINT4 u4Prefix,
                              UINT1 u1MaskType, UINT4 u4Mask)
{
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    pL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *)
        MemAllocMemBlk (L3VPN_RSVPMAPLABELTABLE_POOLID);

    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "L3Vpn mpls binding RBTreeAdd Failed:!\n"));
        return NULL;
    }

    MEMSET (pL3VpnRsvpMapLabelEntry, 0, sizeof (tL3VpnRsvpMapLabelEntry));

    /** Update the Egrees Route Info **/
    L3VPN_P_RSVPMAPLABEL_PREFIX_TYPE (pL3VpnRsvpMapLabelEntry) = u1PrefixType;
    L3VPN_P_RSVPMAPLABEL_PREFIX (pL3VpnRsvpMapLabelEntry) = u4Prefix;
    L3VPN_P_RSVPMAPLABEL_MASK_TYPE (pL3VpnRsvpMapLabelEntry) = u1MaskType;
    L3VPN_P_RSVPMAPLABEL_MASK (pL3VpnRsvpMapLabelEntry) = u4Mask;

    if (RBTreeAdd (L3VPN_RSVP_MAP_LABEL_TABLE,
                   (tRBElem *) pL3VpnRsvpMapLabelEntry) != RB_SUCCESS)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "L3Vpn mpls binding RBTreeAdd Failed!\n"));
        MemReleaseMemBlock (L3VPN_RSVPMAPLABELTABLE_POOLID,
                            (UINT1 *) pL3VpnRsvpMapLabelEntry);
        return NULL;
    }

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return pL3VpnRsvpMapLabelEntry;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L3vpnGetRsvpMapLabelEntry                          */
/*                                                                           */
/*     DESCRIPTION      : This function used to get Rbtree entry info        */
/*                         to current L3VpnAddRsvpMapLabel Entry.            */
/*     INPUT            :  u4Prefix - dest ip,  u4Mask -subnet               */
/*                                                                           */
/*     OUTPUT           :                                                    */
/*                                                                           */
/*     RETURNS          :  pL3VpnRsvpMapLabelEntry                           */
/*                                                                           */
/*****************************************************************************/

tL3VpnRsvpMapLabelEntry *
L3vpnGetRsvpMapLabelEntry (UINT1 u1PrefixType, UINT4 u4Prefix, UINT1 u1MaskType,
                           UINT4 u4Mask)
{
    tL3VpnRsvpMapLabelEntry L3VpnRsvpMapLabelEntry;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    MEMSET (&L3VpnRsvpMapLabelEntry, 0, sizeof (tL3VpnRsvpMapLabelEntry));

    L3VPN_RSVPMAPLABEL_PREFIX_TYPE (L3VpnRsvpMapLabelEntry) = u1PrefixType;
    L3VPN_RSVPMAPLABEL_PREFIX (L3VpnRsvpMapLabelEntry) = u4Prefix;
    L3VPN_RSVPMAPLABEL_MASK_TYPE (L3VpnRsvpMapLabelEntry) = u1MaskType;
    L3VPN_RSVPMAPLABEL_MASK (L3VpnRsvpMapLabelEntry) = u4Mask;

    pL3VpnRsvpMapLabelEntry =
        L3vpnGetRsvpMapLabelTable (&L3VpnRsvpMapLabelEntry);

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return pL3VpnRsvpMapLabelEntry;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L3VpnDeleteRsvpMapLabelTable                       */
/*                                                                           */
/*     DESCRIPTION      : This function used to delete Rbtree entry to       */
/*                        current L3VpnAddRsvpMapLabelTable                  */
/*     INPUT            : u4Prefix -dest ip, u4Mask -subnet                  */
/*                                                                           */
/*     OUTPUT           :                                                    */
/*                                                                           */
/*     RETURNS          : L3VPN_FAILURE/L3VPN_SUCCESS                        */
/*                                                                           */
/*****************************************************************************/

UINT4
L3VpnDeleteRsvpMapLabelTable (tL3VpnRsvpMapLabelEntry * pL3VpnRsvpMapLabelEntry)
{

    tXcEntry           *pXcEntry = NULL;
    UINT4               u4TnlLabel = 0;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));

    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "L3Vpn mpls binding deletion failed: No Entry found \n"));
        return L3VPN_FAILURE;
    }
    if ((pXcEntry =
         MplsGetXCEntryByDirection (pL3VpnRsvpMapLabelEntry->u4TnlXcIndex,
                                    (eDirection) MPLS_DIRECTION_ANY)) == NULL)
    {
        u4TnlLabel = pL3VpnRsvpMapLabelEntry->u4TnlLabel;
        RBTreeRem (L3VPN_RSVP_MAP_LABEL_TABLE, pL3VpnRsvpMapLabelEntry);
        MemReleaseMemBlock (L3VPN_RSVPMAPLABELTABLE_POOLID,
                            (UINT1 *) pL3VpnRsvpMapLabelEntry);
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "L3VpnProcessRsvpTeLspEvent : pXcEntry is NULL\n"));

        if ((u4TnlLabel != 0) && (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL))
        {
            /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
            (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB)) (u4TnlLabel,
                                                    L3VPN_BGP4_RSVPTE_LSP_STATUS_DOWN);

        }

        return L3VPN_SUCCESS;
    }

    RBTreeRem (L3VPN_RSVP_MAP_LABEL_TABLE, pL3VpnRsvpMapLabelEntry);
    MemReleaseMemBlock (L3VPN_RSVPMAPLABELTABLE_POOLID,
                        (UINT1 *) pL3VpnRsvpMapLabelEntry);

    if ((L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL)
        && (pXcEntry->pOutIndex != NULL))
    {
        /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
        (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB)) (pXcEntry->pOutIndex->u4Label,
                                                L3VPN_BGP4_RSVPTE_LSP_STATUS_DOWN);

    }

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3VpnValidateTunnelInfofromRsvpMapLabelTable
 * Input       :  u4TunnelIndex
 *
 * Description : This function checks the tunnel and prefix are exist or not 
 *               
 *               
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ****************************************************************************/

INT1
L3VpnValidateTunnelInfofromRsvpMapLabelTable (UINT4 u4TunnelIndex)
{

    tL3VpnRsvpMapLabelEntry *pNextL3VpnRsvpMapLabelEntry = NULL;
    INT1                i1RetVal = L3VPN_FAILURE;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    pNextL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *) RBTreeGetFirst (L3VPN_RSVP_MAP_LABEL_TABLE);

    while (pNextL3VpnRsvpMapLabelEntry != NULL)
    {
        if ((u4TunnelIndex == pNextL3VpnRsvpMapLabelEntry->u4TnlIndex) &&
            (pNextL3VpnRsvpMapLabelEntry->u4Prefix != 0))
        {
            i1RetVal = L3VPN_SUCCESS;
            break;
        }

        pNextL3VpnRsvpMapLabelEntry =
            (tL3VpnRsvpMapLabelEntry *)
            RBTreeGetNext (L3VPN_RSVP_MAP_LABEL_TABLE,
                           (tRBElem *) pNextL3VpnRsvpMapLabelEntry, NULL);
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return i1RetVal;
}

/****************************************************************************
 * Function    :  L3VpnFetchRsvpMapLabelTableFromTnlIndex
 * Input       :  u4TunnelIndex
 *
 * Description : This function returns the current RsvpMapLabelTableEntry 
 *               for the given TunnelIndex 
 *               
 *
 * Output      :  NONE
 * Returns     :  pL3VpnRsvpMapLabelEntry
 ****************************************************************************/

tL3VpnRsvpMapLabelEntry *
L3VpnFetchRsvpMapLabelTableFromTnlIndex (UINT4 u4TunnelIndex)
{

    tL3VpnRsvpMapLabelEntry *pNextL3VpnRsvpMapLabelEntry = NULL;
/*    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;*/

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    pNextL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *) RBTreeGetFirst (L3VPN_RSVP_MAP_LABEL_TABLE);

    while (pNextL3VpnRsvpMapLabelEntry != NULL)
    {
        if (u4TunnelIndex == pNextL3VpnRsvpMapLabelEntry->u4TnlIndex)
        {
            break;
        }

        pNextL3VpnRsvpMapLabelEntry =
            (tL3VpnRsvpMapLabelEntry *)
            RBTreeGetNext (L3VPN_RSVP_MAP_LABEL_TABLE,
                           (tRBElem *) pNextL3VpnRsvpMapLabelEntry, NULL);
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return pNextL3VpnRsvpMapLabelEntry;
}

#if 0
/****************************************************************************
 * Function    :  L3VpnFetchPrefixListInfoFromTnlIndex
 * Input       :  u4TunnelIndex
 *
 * Description : This function returns the prefix list for the given 
 *               TunnelIndex
 *
 * Output      :  *pu4PrefixList
 * Returns     :  VOID
 ****************************************************************************/

VOID
L3VpnFetchPrefixListInfoFromTnlIndex (UINT4 u4TunnelIndex,
                                      UINT4 **pu4PrefixList)
{
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;
    UINT4               au4prefixlist[L3VPN_PER_VRF_MAX_ROUTES];
    UINT4               u4Temp = 0;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    MEMSET (&au4prefixlist, 0, L3VPN_PER_VRF_MAX_ROUTES);

    *pu4PrefixList = au4prefixlist;
    pL3VpnRsvpMapLabelEntry =
        (tL3VpnRsvpMapLabelEntry *) RBTreeGetFirst (L3VPN_RSVP_MAP_LABEL_TABLE);

    while (pL3VpnRsvpMapLabelEntry != NULL)
    {
        if (u4TunnelIndex == pL3VpnRsvpMapLabelEntry->u4TnlIndex)
        {
            au4prefixlist[u4Temp] = pL3VpnRsvpMapLabelEntry->u4Prefix;
            u4Temp++;
            if (u4Temp == (L3VPN_PER_VRF_MAX_ROUTES - 1))
            {
                break;
            }
        }

        pL3VpnRsvpMapLabelEntry =
            (tL3VpnRsvpMapLabelEntry *)
            RBTreeGetNext (L3VPN_RSVP_MAP_LABEL_TABLE,
                           (tRBElem *) pL3VpnRsvpMapLabelEntry, NULL);
    }

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return;
}
#endif
/****************************************************************************
 * Function    :  L3VpnBgpNotificationForTnlBind
 * 
 *
 * Description : This function called from RSVP-TE set routine.Its update to Bgp 
 *               data's based on input event
 * Input       :  u4TnlXcIndex, u4EventType              
 *
 * Output      :  
 * Returns     :  L3VPN_FAILURE/L3VPN_SUCCESS 
 ****************************************************************************/

INT1
L3VpnBgpNotificationForTnlBind (UINT4 u4TnlIndex, UINT4 u4EventType)
{
    tXcEntry           *pXcEntry = NULL;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));

    pL3VpnRsvpMapLabelEntry =
        L3VpnFetchRsvpMapLabelTableFromTnlIndex (u4TnlIndex);

    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        return L3VPN_FAILURE;
    }

    if ((pXcEntry = MplsGetXCEntryByDirection
         (pL3VpnRsvpMapLabelEntry->u4TnlXcIndex,
          (eDirection) MPLS_DIRECTION_ANY)) == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "L3VpnBgpNotificationForTnlBind : pXcEntry is NULL\n"));
    }
    else
    {
        if (u4EventType == L3VPN_RSVPTE_LSP_UP)
        {
            if (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL)
            {
                /* update OutIndex Label to local binding table to manage sequence issue */
                pL3VpnRsvpMapLabelEntry->u4TnlLabel =
                    pXcEntry->pOutIndex->u4Label;
                /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
                (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB)) (pXcEntry->pOutIndex->
                                                        u4Label,
                                                        L3VPN_BGP4_RSVPTE_LSP_STATUS_UP);

            }
        }
        else if (u4EventType == L3VPN_RSVPTE_LSP_DOWN)
        {
            if (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL)
            {
                /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
                (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB)) (pXcEntry->pOutIndex->
                                                        u4Label,
                                                        L3VPN_BGP4_RSVPTE_LSP_STATUS_DOWN);

            }
        }
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlVrfRteTableRowStatusHdl
 * Input       :  pL3vpnOldMplsL3VpnVrfRteEntry
 *                pL3vpnMplsL3VpnVrfRteEntry
 *
 * Description :  This function handles the cases when the row status
 *                is changed for the VRF Rte table.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 **********************************************************************************/
UINT4
L3vpnUtlVrfRteTableRowStatusHdl (tL3vpnMplsL3VpnVrfRteEntry *
                                 pL3vpnOldMplsL3VpnVrfRteEntry,
                                 tL3vpnMplsL3VpnVrfRteEntry *
                                 pL3vpnMplsL3VpnVrfRteEntry)
{
    switch (L3VPN_P_RTE_INET_CIDR_STATUS (pL3vpnMplsL3VpnVrfRteEntry))
    {
        case ACTIVE:
            if (L3VPN_P_RTE_INET_CIDR_STATUS (pL3vpnOldMplsL3VpnVrfRteEntry) !=
                ACTIVE)
            {
                if (L3vpnUtlHandleVrfRteRowStatusActive
                    (pL3vpnMplsL3VpnVrfRteEntry) != L3VPN_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "L3vpnUtlHandleVrfRteRowStatusActive() failed for VRF: %s.\r\n",
                                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                                au1MplsL3VpnVrfName));
                    return L3VPN_FAILURE;
                }
            }
            break;
        case NOT_IN_SERVICE:
        case NOT_READY:
            if (pL3vpnOldMplsL3VpnVrfRteEntry == NULL)
            {
                                /** The RTE entry is created for first time do the required processing**/
                if (L3vpnUtlProcessVrfRteCreation (pL3vpnMplsL3VpnVrfRteEntry)
                    != L3VPN_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "L3vpnUtlProcessVrfRteCreation() failed for VRF: %s.\r\n",
                                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                                au1MplsL3VpnVrfName));
                    return L3VPN_FAILURE;
                }
            }
            else
            {
                if (L3VPN_P_RTE_INET_CIDR_STATUS (pL3vpnOldMplsL3VpnVrfRteEntry)
                    == ACTIVE)
                {
                    if (L3vpnUtlProcessVrfRteDeletion
                        (pL3vpnMplsL3VpnVrfRteEntry) != L3VPN_SUCCESS)
                    {
                        L3VPN_TRC ((L3VPN_MAIN_TRC,
                                    "L3vpnUtlProcessVrfRteDeletion() failed for VRF: %s.\r\n",
                                    pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                                    au1MplsL3VpnVrfName));
                        return L3VPN_FAILURE;
                    }

                }
                if (L3vpnUtlHandleVrfRteRowStatusNotInService
                    (pL3vpnMplsL3VpnVrfRteEntry) != L3VPN_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "L3vpnUtlHandleVrfRteRowStatusNotInService() failed for VRF: %s.\r\n",
                                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                                au1MplsL3VpnVrfName));
                    return L3VPN_FAILURE;
                }
            }
            break;
        case DESTROY:

            if (L3VPN_P_RTE_INET_CIDR_STATUS (pL3vpnOldMplsL3VpnVrfRteEntry) ==
                ACTIVE)
            {
                if (L3vpnUtlProcessVrfRteDeletion (pL3vpnMplsL3VpnVrfRteEntry)
                    != L3VPN_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "L3vpnUtlProcessVrfRteDeletion() failed for VRF: %s.\r\n",
                                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                                au1MplsL3VpnVrfName));
                    return L3VPN_FAILURE;
                }
            }
            if (L3vpnUtlHandleVrfRteRowStatusDestroy
                (pL3vpnMplsL3VpnVrfRteEntry) != L3VPN_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3vpnUtlHandleVrfRteRowStatusDestroy() failed for VRF: %s.\r\n",
                            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                            au1MplsL3VpnVrfName));
                return L3VPN_FAILURE;
            }
            break;
        default:
            return L3VPN_FAILURE;
            break;
    }

    return L3VPN_SUCCESS;
}

/****************************************************************************
 *  Function    :  L3vpnUtlHandleVrfRteRowStatusActive
 *  Input       :  pL3vpnMplsL3VpnVrfEntry
 *    
 *  Description : This function does the handling for MPLS VRF deletion.
 *                     Clean up the RTE table, IfConf Table, RT Table
 *       
 *  Output      :  NONE
 *  Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ******************************************************************************/
UINT4
L3vpnUtlHandleVrfRteRowStatusActive (tL3vpnMplsL3VpnVrfRteEntry *
                                     pL3vpnMplsL3VpnVrfRteEntry)
{
    if (MplsL3vpnIngressMap (pL3vpnMplsL3VpnVrfRteEntry) != L3VPN_SUCCESS)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "MplsL3vpnIngressMap() Failed.\r\n"));
        return OSIX_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
 *  Function    :  L3vpnUtlHandleVrfRteRowStatusDestroy
 *  Input       :  pL3vpnMplsL3VpnVrfRteEntry
 *    
 *  Description : This function does the handling for MPLS VRF deletion.
 *                     Clean up the RTE table, IfConf Table, RT Table
 *       
 *  Output      :  NONE
 *  Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ******************************************************************************/

UINT4
L3vpnUtlHandleVrfRteRowStatusDestroy (tL3vpnMplsL3VpnVrfRteEntry *
                                      pL3vpnMplsL3VpnVrfRteEntry)
{
    tL3vpnMplsL3VpnVrfEntry l3vpnMplsL3VpnVrfEntry;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    UINT4               u4VrfId = 0;

    MEMSET (&l3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMCPY (L3VPN_VRF_NAME (l3vpnMplsL3VpnVrfEntry),
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen);

    L3VPN_VRF_NAME_LEN (l3vpnMplsL3VpnVrfEntry) =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen;

    pL3vpnMplsL3VpnVrfEntry = (tL3vpnMplsL3VpnVrfEntry *)
        RBTreeGet (L3VPN_VRF_TABLE, &l3vpnMplsL3VpnVrfEntry);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "VRF entry not found in MPLS VRF table: %s.\r\n",
                    pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName));
        return OSIX_FAILURE;
    }

    if (VcmIsVrfExist (L3VPN_VRF_NAME (l3vpnMplsL3VpnVrfEntry), &u4VrfId) !=
        VCM_TRUE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "VRF is not existing\r\n"));
        return OSIX_FAILURE;
    }
    pL3vpnMplsL3VpnVrfPerfEntry =
        L3VpnGetVrfPerfTableEntry (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry));
    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "PERF Entry is NULL for the VRF: %s.\r\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        return OSIX_FAILURE;
    }
    if (L3VpnDeleteRteTableEntryFromTrie (u4VrfId,
                                          pL3vpnMplsL3VpnVrfRteEntry) ==
        L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Error in removing the RTE Entry from TRIE data-structure.\r\n"));
        return OSIX_FAILURE;
    }

    if (L3VpnUpdatePerfTableCounters
        (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
         L3VPN_ROUTE_DELETED) == L3VPN_FAILURE)
    {

        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Error in updating the Performance table for the VRF:%s.\r\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        return OSIX_FAILURE;
    }

    if (L3VpnChckAndSendRouteNotifications (pL3vpnMplsL3VpnVrfEntry,
                                            pL3vpnMplsL3VpnVrfPerfEntry,
                                            L3VPN_ROUTE_DELETED) ==
        L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Failed to send Trap Notification for the VRF:%s.\r\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        return OSIX_FAILURE;
    }

    return L3VPN_SUCCESS;
}

/****************************************************************************
 *  Function    :  L3vpnUtlHandleVrfRteRowStatusNotInService
 *  Input       :  pL3vpnMplsL3VpnVrfEntry
 *    
 *  Description : This function does the handling for MPLS VRF deletion.
 *                     Clean up the RTE table, IfConf Table, RT Table
 *       
 *  Output      :  NONE
 *  Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ******************************************************************************/
UINT4
L3vpnUtlHandleVrfRteRowStatusNotInService (tL3vpnMplsL3VpnVrfRteEntry *
                                           pL3vpnMplsL3VpnVrfRteEntry)
{
    UNUSED_PARAM (pL3vpnMplsL3VpnVrfRteEntry);
    return L3VPN_SUCCESS;
}

/****************************************************************************
 *  Function    :  L3vpnUtlProcessVrfRteDeletion
 *  Input       :  pL3vpnMplsL3VpnVrfEntry
 *    
 *  Description : This function does the handling for MPLS VRF deletion.
 *                     Clean up the RTE table, IfConf Table, RT Table
 *       
 *  Output      :  NONE
 *  Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 ******************************************************************************/
UINT4
L3vpnUtlProcessVrfRteDeletion (tL3vpnMplsL3VpnVrfRteEntry *
                               pL3vpnMplsL3VpnVrfRteEntry)
{
    if (MplsL3vpnIngressUnMap (pL3vpnMplsL3VpnVrfRteEntry) != L3VPN_SUCCESS)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "MplsL3vpnIngressUnMap() Failed.\r\n"));
        return OSIX_FAILURE;
    }

    return L3VPN_SUCCESS;
}

UINT4
L3vpnUtlProcessVrfRteCreation (tL3vpnMplsL3VpnVrfRteEntry *
                               pL3vpnMplsL3VpnVrfRteEntry)
{
    tL3vpnMplsL3VpnVrfEntry l3vpnMplsL3VpnVrfEntry;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    UINT4               u4VrfId = 0;

    MEMSET (&l3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMCPY (L3VPN_VRF_NAME (l3vpnMplsL3VpnVrfEntry),
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen);

    L3VPN_VRF_NAME_LEN (l3vpnMplsL3VpnVrfEntry) =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen;

    pL3vpnMplsL3VpnVrfEntry = (tL3vpnMplsL3VpnVrfEntry *)
        RBTreeGet (L3VPN_VRF_TABLE, &l3vpnMplsL3VpnVrfEntry);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "VRF entry not found in MPLS VRF table: %s.\r\n",
                    pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName));
        return OSIX_FAILURE;
    }

    if (VcmIsVrfExist (L3VPN_VRF_NAME (l3vpnMplsL3VpnVrfEntry), &u4VrfId) !=
        VCM_TRUE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "VRF is not existing\r\n"));
        return OSIX_FAILURE;
    }
    pL3vpnMplsL3VpnVrfPerfEntry =
        L3VpnGetVrfPerfTableEntry (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry));
    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "PERF Entry is NULL for the VRF: %s.\r\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        return OSIX_FAILURE;
    }

    /* Check if allowed to configure more route in the given VRF */
    if (L3VpnIsRouteAdditionAllowed (pL3vpnMplsL3VpnVrfEntry,
                                     pL3vpnMplsL3VpnVrfPerfEntry) == OSIX_FALSE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "Routes Exceed the maximum allowed value for the "
                    "VRF: %s.\r\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        /* Increment the Dropped routes counter by 1 */
        L3VpnUpdatePerfTableCounters (L3VPN_P_VRF_NAME
                                      (pL3vpnMplsL3VpnVrfEntry),
                                      L3VPN_ROUTE_DROPPED);
        return OSIX_FAILURE;
    }

    if (L3VpnRegisterRteEntryWithTrie (u4VrfId,
                                       pL3vpnMplsL3VpnVrfRteEntry)
        == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "FAiled to add the RTE Entry in the TRIE.\r\n"));
        return OSIX_FAILURE;
    }

    /* Updating the Perf counters */
    if (L3VpnUpdatePerfTableCounters
        (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
         L3VPN_ROUTE_ADDED) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Error in updating the Performance table for the "
                    "VRF:%s.\r\n", L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));

        if (L3VpnDeleteRteTableEntryFromTrie (u4VrfId,
                                              pL3vpnMplsL3VpnVrfRteEntry) ==
            L3VPN_FAILURE)
        {
            L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                        "\r%%Error in removing the RTE Entry from TRIE data-structure.\r\n"));
        }

        return OSIX_FAILURE;
    }
    if (L3VpnChckAndSendRouteNotifications (pL3vpnMplsL3VpnVrfEntry,
                                            pL3vpnMplsL3VpnVrfPerfEntry,
                                            L3VPN_ROUTE_ADDED) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Failed to send Trap Notification for the VRF:%s.\r\n",
                    L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));
        if (L3VpnUpdatePerfTableCounters
            (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry),
             L3VPN_ROUTE_DELETED) == L3VPN_FAILURE)
        {

            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "Error in updating the Performance table for the VRF:%s.\r\n",
                        L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfEntry)));

        }
        if (L3VpnDeleteRteTableEntryFromTrie (u4VrfId,
                                              pL3vpnMplsL3VpnVrfRteEntry) ==
            L3VPN_FAILURE)
        {
            L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                        "\r%%Error in removing the RTE Entry from TRIE data-structure.\r\n"));
        }

        return OSIX_FAILURE;
    }

    return L3VPN_SUCCESS;
}

/****************************************************************************
 * Function    :  L3vpnUtlEgressRteTableRowStatusHdl
 * Input       :  u4FsMplsL3VpnVrfEgressRtePathAttrLabel
 *                i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus
 *
 * Description :  This function handles the cases when the row status
 *                is changed for the VRF Rte table.
 *
 * Output      :  NONE
 * Returns     :  L3VPN_SUCCESS/L3VPN_FAILURE
 **********************************************************************************/
UINT4
L3vpnUtlEgressRteTableRowStatusHdl (UINT4
                                    u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                                    INT4
                                    i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)
{
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

    MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

    L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) =
        u4FsMplsL3VpnVrfEgressRtePathAttrLabel;

    pL3VpnBgpRouteLabelEntry =
        L3vpnGetBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

    switch (i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)
    {
        case CREATE_AND_WAIT:
            if (pL3VpnBgpRouteLabelEntry != NULL)
            {
                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                            "%s : %d Failed\n", __FUNCTION__, __LINE__));
                return OSIX_FAILURE;
            }

            pL3VpnBgpRouteLabelEntry =
                (tL3VpnBgpRouteLabelEntry *)
                MemAllocMemBlk (L3VPN_BGPROUTELABELTABLE_POOLID);

            if (pL3VpnBgpRouteLabelEntry == NULL)
            {

                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                            "%s : %d Failed\n", __FUNCTION__, __LINE__));
                return OSIX_FAILURE;
            }
            MEMSET (pL3VpnBgpRouteLabelEntry, 0,
                    sizeof (tL3VpnBgpRouteLabelEntry));

            pL3VpnBgpRouteLabelEntry->u4RowStatus = CREATE_AND_WAIT;
            L3VPN_P_BGPROUTELABEL_LABEL (pL3VpnBgpRouteLabelEntry) =
                u4FsMplsL3VpnVrfEgressRtePathAttrLabel;
            L3VPN_P_BGPROUTELABEL_POLICY (pL3VpnBgpRouteLabelEntry) =
                L3VPN_BGP4_POLICY_PER_VRF;

            if (RBTreeAdd (L3VPN_BGP_ROUTE_LABEL_TABLE,
                           (tRBElem *) pL3VpnBgpRouteLabelEntry) != RB_SUCCESS)
            {
                MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                                    (UINT1 *) pL3VpnBgpRouteLabelEntry);

                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                            "%s : %d Failed\n", __FUNCTION__, __LINE__));
                return OSIX_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
            if (pL3VpnBgpRouteLabelEntry->u4RowStatus == ACTIVE)
            {
                if (MplsL3vpnEgressUnMap (pL3VpnBgpRouteLabelEntry) !=
                    L3VPN_SUCCESS)
                {

                    L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                "%s : %d Failed\n", __FUNCTION__, __LINE__));
                    return OSIX_FAILURE;
                }
            }
            pL3VpnBgpRouteLabelEntry->u4RowStatus = NOT_IN_SERVICE;
            break;
        case ACTIVE:
            if (pL3VpnBgpRouteLabelEntry == NULL)
            {

                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                            "%s : %d Failed\n", __FUNCTION__, __LINE__));
                return OSIX_FAILURE;
            }
            if (MplsL3vpnEgressMap (pL3VpnBgpRouteLabelEntry) != L3VPN_SUCCESS)
            {

                L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                            "%s : %d Failed\n", __FUNCTION__, __LINE__));
                return OSIX_FAILURE;
            }
            pL3VpnBgpRouteLabelEntry->u4RowStatus = ACTIVE;
            break;
        case DESTROY:
            if (pL3VpnBgpRouteLabelEntry->u4RowStatus == ACTIVE)
            {
                if (MplsL3vpnEgressUnMap (pL3VpnBgpRouteLabelEntry) !=
                    L3VPN_SUCCESS)
                {

                    L3VPN_TRC ((L3VPN_ALL_FAIL_TRC,
                                "%s : %d Failed\n", __FUNCTION__, __LINE__));
                    return OSIX_FAILURE;
                }
            }

            RBTreeRem (L3VPN_BGP_ROUTE_LABEL_TABLE, pL3VpnBgpRouteLabelEntry);

            MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                                (UINT1 *) pL3VpnBgpRouteLabelEntry);

            break;
        default:
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Wrong Operation.\r\n"));
            return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpnutil.c                     */
/*-----------------------------------------------------------------------*/
