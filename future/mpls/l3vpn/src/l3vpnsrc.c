/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnsrc.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnsrc.c
*
* Description: This file holds the functions for the ShowRunningConfig
*              for L3vpn module 
*********************************************************************/

#include "l3vpninc.h"
#include "l3vpnsrc.h"
#include "l3vpnsrcdefn.h"

/****************************************************************************
 Function    : L3vpnShowRunningConfig
 Description : This function displays the current configuration
                of L3VPN module
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...)
{

#ifdef CLI_WANTED
    CliRegisterLock (CliHandle, L3vpnMainTaskLock, L3vpnMainTaskUnLock);
#endif
    L3VPN_LOCK;
	UNUSED_PARAM(u4Module);

    CliPrintf (CliHandle, "!\r\n");
    /*L3vpnShowRunningConfigMplsL3VpnVrfTable (CliHandle, u4Module);*/
    /*L3vpnShowRunningConfigMplsL3VpnVrfRTTable (CliHandle, u4Module);*/

#ifdef CLI_WANTED
    CliUnRegisterLock (CliHandle);
#endif
    L3VPN_UNLOCK;
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : L3vpnShowRunningConfigScalar
 Description : This function displays the current configuration
                of the L3VPN scalars
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS always
****************************************************************************/
INT4
L3vpnShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    UNUSED_PARAM (u4Module);
	UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

#if 0
/****************************************************************************
 Function    : L3vpnShowRunningConfigMplsL3VpnVrfTable
 Description : This function displays the current configuration
                of MplsL3VpnVrfTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
L3vpnShowRunningConfigMplsL3VpnVrfTable (tCliHandle CliHandle, UINT4 u4Module,
                                         ...)
{
    UNUSED_PARAM (u4Module);
    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfNameSrc;
    tSNMP_OCTET_STRING_TYPE NextMplsL3VpnVrfName;
    UINT1               au1MplsL3VpnVrfName[256];
    CHR1               *pMplsL3VpnVrfName = NULL;
    CHR1               *pMplsL3VpnVrfRD = NULL;

    MEMSET (au1MplsL3VpnVrfName, 0, 256);
    MEMSET (&MplsL3VpnVrfNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextMplsL3VpnVrfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&L3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    MplsL3VpnVrfNameSrc.pu1_OctetList = au1MplsL3VpnVrfName;

    if (SNMP_SUCCESS != nmhGetFirstIndexMplsL3VpnVrfTable
        (&MplsL3VpnVrfNameSrc))
    {
        return CLI_SUCCESS;
    }

    MEMCPY (L3vpnMplsL3VpnVrfEntry.MibObject.au1MplsL3VpnVrfName,
            MplsL3VpnVrfNameSrc.pu1_OctetList, MplsL3VpnVrfNameSrc.i4_Length);
    L3vpnMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfNameLen =
        MplsL3VpnVrfNameSrc.i4_Length;

    while (1)
    {
        if (L3vpnGetAllMplsL3VpnVrfTable (&L3vpnMplsL3VpnVrfEntry) !=
            OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        L3vpnUtlConversionForMplsL3VpnVrfName (pMplsL3VpnVrfName,
                                               &L3vpnMplsL3VpnVrfEntry);
        L3vpnUtlConversionForMplsL3VpnVrfRD (pMplsL3VpnVrfRD,
                                             &L3vpnMplsL3VpnVrfEntry);
        {
            CliPrintf (CliHandle, "rd %s \r\n", pMplsL3VpnVrfRD);

        }
        if (L3vpnMplsL3VpnVrfEntry.MibObject.u4MplsL3VpnVrfConfHighRteThresh
            != L3VPN_DEF_MPLSL3VPNVRFCONFHIGHRTETHRESH)
        {
            if (L3vpnMplsL3VpnVrfEntry.MibObject.u4MplsL3VpnVrfConfMaxRoutes
                != L3VPN_DEF_MPLSL3VPNVRFCONFMAXROUTES)
                CliPrintf (CliHandle,
                           "maximum routes %d %d \r\n",
                           L3vpnMplsL3VpnVrfEntry.MibObject.
                           u4MplsL3VpnVrfConfMaxRoutes,
                           L3vpnMplsL3VpnVrfEntry.MibObject.
                           u4MplsL3VpnVrfConfHighRteThresh);

        }

        if (SNMP_SUCCESS !=
            nmhGetNextIndexMplsL3VpnVrfTable (&MplsL3VpnVrfNameSrc,
                                              &NextMplsL3VpnVrfName))
        {
            break;
        }

        MEMCPY (L3vpnMplsL3VpnVrfEntry.MibObject.au1MplsL3VpnVrfName,
                NextMplsL3VpnVrfName.pu1_OctetList,
                NextMplsL3VpnVrfName.i4_Length);
        L3vpnMplsL3VpnVrfEntry.MibObject.i4MplsL3VpnVrfNameLen =
            NextMplsL3VpnVrfName.i4_Length;
    }
    return CLI_SUCCESS;
}
#endif
