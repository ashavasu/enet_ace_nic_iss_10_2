/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndbg.c,v 1.2 2015/09/15 07:03:06 siva Exp $
 *
 ********************************************************************/


/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpndbg.c
*
* Description: This file contains the routines for the protocol Database Access for the module L3vpn 
*********************************************************************/

#include "l3vpninc.h"

/****************************************************************************
 Function    :  L3vpnGetAllMplsL3VpnVrfTable
 Input       :  pL3vpnGetMplsL3VpnVrfEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                              pL3vpnGetMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                   (tRBElem *) pL3vpnGetMplsL3VpnVrfEntry);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnGetAllMplsL3VpnVrfTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pL3vpnGetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen);

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen;

    MEMCPY (pL3vpnGetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen);

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen;

    MEMCPY (pL3vpnGetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
            pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
            pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen);

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfCreationTime =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfCreationTime;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfOperStatus =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfOperStatus;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfActiveInterfaces =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfActiveInterfaces;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfAssociatedInterfaces =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfAssociatedInterfaces;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfLastChanged =
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfLastChanged;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus;

    pL3vpnGetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType =
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType;

    if (L3vpnGetAllUtlMplsL3VpnVrfTable
        (pL3vpnGetMplsL3VpnVrfEntry, pL3vpnMplsL3VpnVrfEntry) == OSIX_FAILURE)

    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "L3vpnGetAllMplsL3VpnVrfTable:"
                    "L3vpnGetAllUtlMplsL3VpnVrfTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllMplsL3VpnIfConfTable
 Input       :  pL3vpnGetMplsL3VpnIfConfEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                 pL3vpnGetMplsL3VpnIfConfEntry)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnIfConfEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                   (tRBElem *) pL3vpnGetMplsL3VpnIfConfEntry);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnGetAllMplsL3VpnIfConfTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pL3vpnGetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification;

    MEMCPY (pL3vpnGetMplsL3VpnIfConfEntry->MibObject.
            au1MplsL3VpnIfVpnRouteDistProtocol,
            pL3vpnMplsL3VpnIfConfEntry->MibObject.
            au1MplsL3VpnIfVpnRouteDistProtocol,
            pL3vpnMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnRouteDistProtocolLen);

    pL3vpnGetMplsL3VpnIfConfEntry->MibObject.
        i4MplsL3VpnIfVpnRouteDistProtocolLen =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.
        i4MplsL3VpnIfVpnRouteDistProtocolLen;

    pL3vpnGetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType;

    pL3vpnGetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus;

    if (L3vpnGetAllUtlMplsL3VpnIfConfTable
        (pL3vpnGetMplsL3VpnIfConfEntry,
         pL3vpnMplsL3VpnIfConfEntry) == OSIX_FAILURE)

    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "L3vpnGetAllMplsL3VpnIfConfTable:"
                    "L3vpnGetAllUtlMplsL3VpnIfConfTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllMplsL3VpnVrfRTTable
 Input       :  pL3vpnGetMplsL3VpnVrfRTEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                pL3vpnGetMplsL3VpnVrfRTEntry)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfRTEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                   (tRBElem *) pL3vpnGetMplsL3VpnVrfRTEntry);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnGetAllMplsL3VpnVrfRTTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pL3vpnGetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen);

    pL3vpnGetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen;

    MEMCPY (pL3vpnGetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
            pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen);

    pL3vpnGetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen;

    pL3vpnGetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus;

    pL3vpnGetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType =
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType;

    if (L3vpnGetAllUtlMplsL3VpnVrfRTTable
        (pL3vpnGetMplsL3VpnVrfRTEntry,
         pL3vpnMplsL3VpnVrfRTEntry) == OSIX_FAILURE)

    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "L3vpnGetAllMplsL3VpnVrfRTTable:"
                    "L3vpnGetAllUtlMplsL3VpnVrfRTTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllMplsL3VpnVrfSecTable
 Input       :  pL3vpnGetMplsL3VpnVrfSecEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllMplsL3VpnVrfSecTable (tL3vpnMplsL3VpnVrfSecEntry *
                                 pL3vpnGetMplsL3VpnVrfSecEntry)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfSecEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable,
                   (tRBElem *) pL3vpnGetMplsL3VpnVrfSecEntry);

    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnGetAllMplsL3VpnVrfSecTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pL3vpnGetMplsL3VpnVrfSecEntry->MibObject.u4MplsL3VpnVrfSecIllegalLblVltns =
        pL3vpnMplsL3VpnVrfSecEntry->MibObject.u4MplsL3VpnVrfSecIllegalLblVltns;

    pL3vpnGetMplsL3VpnVrfSecEntry->MibObject.
        u4MplsL3VpnVrfSecDiscontinuityTime =
        pL3vpnMplsL3VpnVrfSecEntry->MibObject.
        u4MplsL3VpnVrfSecDiscontinuityTime;

    if (L3vpnGetAllUtlMplsL3VpnVrfSecTable
        (pL3vpnGetMplsL3VpnVrfSecEntry,
         pL3vpnMplsL3VpnVrfSecEntry) == OSIX_FAILURE)

    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "L3vpnGetAllMplsL3VpnVrfSecTable:"
                    "L3vpnGetAllUtlMplsL3VpnVrfSecTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllMplsL3VpnVrfPerfTable
 Input       :  pL3vpnGetMplsL3VpnVrfPerfEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllMplsL3VpnVrfPerfTable (tL3vpnMplsL3VpnVrfPerfEntry *
                                  pL3vpnGetMplsL3VpnVrfPerfEntry)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfPerfEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable,
                   (tRBElem *) pL3vpnGetMplsL3VpnVrfPerfEntry);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnGetAllMplsL3VpnVrfPerfTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pL3vpnGetMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesAdded =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesAdded;

    pL3vpnGetMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesDeleted =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesDeleted;

    pL3vpnGetMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfCurrNumRoutes =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfCurrNumRoutes;

    pL3vpnGetMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesDropped =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfRoutesDropped;

    pL3vpnGetMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfDiscTime =
        pL3vpnMplsL3VpnVrfPerfEntry->MibObject.u4MplsL3VpnVrfPerfDiscTime;

    if (L3vpnGetAllUtlMplsL3VpnVrfPerfTable
        (pL3vpnGetMplsL3VpnVrfPerfEntry,
         pL3vpnMplsL3VpnVrfPerfEntry) == OSIX_FAILURE)

    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "L3vpnGetAllMplsL3VpnVrfPerfTable:"
                    "L3vpnGetAllUtlMplsL3VpnVrfPerfTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetAllMplsL3VpnVrfRteTable
 Input       :  pL3vpnGetMplsL3VpnVrfRteEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetAllMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                 pL3vpnGetMplsL3VpnVrfRteEntry)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfRteEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable,
                   (tRBElem *) pL3vpnGetMplsL3VpnVrfRteEntry);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnGetAllMplsL3VpnVrfRteTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrProto =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrProto;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrAge =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.u4MplsL3VpnVrfRteInetCidrAge;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.
        u4MplsL3VpnVrfRteInetCidrNextHopAS =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.
        u4MplsL3VpnVrfRteInetCidrNextHopAS;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5 =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5;

    MEMCPY (pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.
            au1MplsL3VpnVrfRteXCPointer,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer,
            pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteXCPointerLen);

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen;

    pL3vpnGetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus;

    if (L3vpnGetAllUtlMplsL3VpnVrfRteTable
        (pL3vpnGetMplsL3VpnVrfRteEntry,
         pL3vpnMplsL3VpnVrfRteEntry) == OSIX_FAILURE)

    {
        L3VPN_TRC ((L3VPN_UTIL_TRC, "L3vpnGetAllMplsL3VpnVrfRteTable:"
                    "L3vpnGetAllUtlMplsL3VpnVrfRteTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnVrfTable
 Input       :  pL3vpnSetMplsL3VpnVrfEntry
                pL3vpnIsSetMplsL3VpnVrfEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                              pL3vpnSetMplsL3VpnVrfEntry,
                              tL3vpnIsSetMplsL3VpnVrfEntry *
                              pL3vpnIsSetMplsL3VpnVrfEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnOldMplsL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnTrgMplsL3VpnVrfEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfEntry *pL3vpnTrgIsSetMplsL3VpnVrfEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pL3vpnOldMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnOldMplsL3VpnVrfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pL3vpnTrgMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnTrgMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
        return OSIX_FAILURE;
    }
    pL3vpnTrgIsSetMplsL3VpnVrfEntry =
        (tL3vpnIsSetMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID);
    if (pL3vpnTrgIsSetMplsL3VpnVrfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pL3vpnOldMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnTrgMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (pL3vpnTrgIsSetMplsL3VpnVrfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfEntry));

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                   (tRBElem *) pL3vpnSetMplsL3VpnVrfEntry);

    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
             i4MplsL3VpnVrfConfRowStatus == CREATE_AND_WAIT)
            || (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == CREATE_AND_GO)
            ||
            ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
              i4MplsL3VpnVrfConfRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pL3vpnMplsL3VpnVrfEntry =
                (tL3vpnMplsL3VpnVrfEntry *)
                MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
            if (pL3vpnMplsL3VpnVrfEntry == NULL)
            {
                if (L3vpnSetAllMplsL3VpnVrfTableTrigger
                    (pL3vpnSetMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfTable:L3vpnSetAllMplsL3VpnVrfTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                    (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pL3vpnMplsL3VpnVrfEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfEntry));
            if ((L3vpnInitializeMplsL3VpnVrfTable (pL3vpnMplsL3VpnVrfEntry)) ==
                OSIX_FAILURE)
            {
                if (L3vpnSetAllMplsL3VpnVrfTableTrigger
                    (pL3vpnSetMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfTable:L3vpnSetAllMplsL3VpnVrfTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                    (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        au1MplsL3VpnVrfVpnId,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfVpnIdLen);

                pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfVpnIdLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription !=
                OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.
                        au1MplsL3VpnVrfDescription,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        au1MplsL3VpnVrfDescription,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfDescriptionLen);

                pL3vpnMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfDescriptionLen =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfDescriptionLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfRDLen);

                pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.
                    u4MplsL3VpnVrfConfMidRteThresh =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    u4MplsL3VpnVrfConfMidRteThresh;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.
                    u4MplsL3VpnVrfConfHighRteThresh =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    u4MplsL3VpnVrfConfHighRteThresh;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    u4MplsL3VpnVrfConfMaxRoutes;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfRowStatus;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfAdminStatus =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfAdminStatus;
            }

            if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfStorageType =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfStorageType;
            }

            if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                 i4MplsL3VpnVrfConfRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfConfRowStatus == ACTIVE)))
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
                    ACTIVE;
            }
            else if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                     i4MplsL3VpnVrfConfRowStatus == CREATE_AND_WAIT)
            {
                pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                 (tRBElem *) pL3vpnMplsL3VpnVrfEntry) != RB_SUCCESS)
            {
                if (L3vpnSetAllMplsL3VpnVrfTableTrigger
                    (pL3vpnSetMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                    (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
                return OSIX_FAILURE;
            }
            if (L3vpnUtilUpdateMplsL3VpnVrfTable
                (NULL, pL3vpnMplsL3VpnVrfEntry,
                 pL3vpnIsSetMplsL3VpnVrfEntry) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: L3vpnUtilUpdateMplsL3VpnVrfTable function returns failure.\r\n"));

                if (L3vpnSetAllMplsL3VpnVrfTableTrigger
                    (pL3vpnSetMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                           pL3vpnMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                    (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
                return OSIX_FAILURE;
            }

            if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                 i4MplsL3VpnVrfConfRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfConfRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pL3vpnTrgMplsL3VpnVrfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnTrgMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;
                pL3vpnTrgMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfRowStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus =
                    OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnVrfTableTrigger
                    (pL3vpnTrgMplsL3VpnVrfEntry,
                     pL3vpnTrgIsSetMplsL3VpnVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                        (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnVrfEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                     i4MplsL3VpnVrfConfRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfRowStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus =
                    OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnVrfTableTrigger
                    (pL3vpnTrgMplsL3VpnVrfEntry,
                     pL3vpnTrgIsSetMplsL3VpnVrfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                        (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnVrfEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfConfRowStatus == CREATE_AND_GO)
            {
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                    i4MplsL3VpnVrfConfRowStatus = ACTIVE;
            }

            if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                                     pL3vpnIsSetMplsL3VpnVrfEntry,
                                                     SNMP_SUCCESS) !=
                OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable:  L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                                     pL3vpnIsSetMplsL3VpnVrfEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable: Failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pL3vpnSetMplsL3VpnVrfEntry->MibObject.
              i4MplsL3VpnVrfConfRowStatus == CREATE_AND_WAIT)
             || (pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                 i4MplsL3VpnVrfConfRowStatus == CREATE_AND_GO))
    {
        if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                                 pL3vpnIsSetMplsL3VpnVrfEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfTable: The row is already present.\r\n"));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pL3vpnOldMplsL3VpnVrfEntry, pL3vpnMplsL3VpnVrfEntry,
            sizeof (tL3vpnMplsL3VpnVrfEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus ==
        DESTROY)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
            DESTROY;

        if (L3vpnUtilUpdateMplsL3VpnVrfTable (pL3vpnOldMplsL3VpnVrfEntry,
                                              pL3vpnMplsL3VpnVrfEntry,
                                              pL3vpnIsSetMplsL3VpnVrfEntry) !=
            OSIX_SUCCESS)
        {

            if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                                     pL3vpnIsSetMplsL3VpnVrfEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable: L3vpnUtilUpdateMplsL3VpnVrfTable function returns failure.\r\n"));
        }
        RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
                   pL3vpnMplsL3VpnVrfEntry);
        if (L3vpnSetAllMplsL3VpnVrfTableTrigger
            (pL3vpnSetMplsL3VpnVrfEntry, pL3vpnIsSetMplsL3VpnVrfEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (MplsL3VpnVrfTableFilterInputs
        (pL3vpnMplsL3VpnVrfEntry, pL3vpnSetMplsL3VpnVrfEntry,
         pL3vpnIsSetMplsL3VpnVrfEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus ==
         ACTIVE)
        && (pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus !=
            NOT_IN_SERVICE))
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pL3vpnTrgMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
            NOT_IN_SERVICE;
        pL3vpnTrgIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus = OSIX_TRUE;

        if (L3vpnUtilUpdateMplsL3VpnVrfTable (pL3vpnOldMplsL3VpnVrfEntry,
                                              pL3vpnMplsL3VpnVrfEntry,
                                              pL3vpnIsSetMplsL3VpnVrfEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pL3vpnMplsL3VpnVrfEntry, pL3vpnOldMplsL3VpnVrfEntry,
                    sizeof (tL3vpnMplsL3VpnVrfEntry));
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable:                 L3vpnUtilUpdateMplsL3VpnVrfTable Function returns failure.\r\n"));

            if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                                     pL3vpnIsSetMplsL3VpnVrfEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
            return OSIX_FAILURE;
        }

        if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnTrgMplsL3VpnVrfEntry,
                                                 pL3vpnTrgIsSetMplsL3VpnVrfEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
        }
    }

    if (pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId != OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen);

        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription != OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                au1MplsL3VpnVrfDescription,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfDescriptionLen);

        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD != OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen);

        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfMidRteThresh;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfHighRteThresh =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfHighRteThresh;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes != OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus != OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus = ACTIVE;
    }

    if (L3vpnUtilUpdateMplsL3VpnVrfTable (pL3vpnOldMplsL3VpnVrfEntry,
                                          pL3vpnMplsL3VpnVrfEntry,
                                          pL3vpnIsSetMplsL3VpnVrfEntry) !=
        OSIX_SUCCESS)
    {

        if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                                 pL3vpnIsSetMplsL3VpnVrfEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));

        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfTable: L3vpnUtilUpdateMplsL3VpnVrfTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pL3vpnMplsL3VpnVrfEntry, pL3vpnOldMplsL3VpnVrfEntry,
                sizeof (tL3vpnMplsL3VpnVrfEntry));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
        return OSIX_FAILURE;

    }
    if (L3vpnSetAllMplsL3VpnVrfTableTrigger (pL3vpnSetMplsL3VpnVrfEntry,
                                             pL3vpnIsSetMplsL3VpnVrfEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfTable: L3vpnSetAllMplsL3VpnVrfTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnOldMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnIfConfTable
 Input       :  pL3vpnSetMplsL3VpnIfConfEntry
                pL3vpnIsSetMplsL3VpnIfConfEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                 pL3vpnSetMplsL3VpnIfConfEntry,
                                 tL3vpnIsSetMplsL3VpnIfConfEntry *
                                 pL3vpnIsSetMplsL3VpnIfConfEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnOldMplsL3VpnIfConfEntry = NULL;
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnTrgMplsL3VpnIfConfEntry = NULL;
    tL3vpnIsSetMplsL3VpnIfConfEntry *pL3vpnTrgIsSetMplsL3VpnIfConfEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pL3vpnOldMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnOldMplsL3VpnIfConfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pL3vpnTrgMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnTrgMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
        return OSIX_FAILURE;
    }
    pL3vpnTrgIsSetMplsL3VpnIfConfEntry =
        (tL3vpnIsSetMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID);
    if (pL3vpnTrgIsSetMplsL3VpnIfConfEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pL3vpnOldMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnTrgMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (pL3vpnTrgIsSetMplsL3VpnIfConfEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnIfConfEntry));

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnIfConfEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                   (tRBElem *) pL3vpnSetMplsL3VpnIfConfEntry);

    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
             i4MplsL3VpnIfConfRowStatus == CREATE_AND_WAIT)
            || (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                i4MplsL3VpnIfConfRowStatus == CREATE_AND_GO)
            ||
            ((pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
              i4MplsL3VpnIfConfRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pL3vpnMplsL3VpnIfConfEntry =
                (tL3vpnMplsL3VpnIfConfEntry *)
                MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
            if (pL3vpnMplsL3VpnIfConfEntry == NULL)
            {
                if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                    (pL3vpnSetMplsL3VpnIfConfEntry,
                     pL3vpnIsSetMplsL3VpnIfConfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnIfConfTable:L3vpnSetAllMplsL3VpnIfConfTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pL3vpnMplsL3VpnIfConfEntry, 0,
                    sizeof (tL3vpnMplsL3VpnIfConfEntry));
            if ((L3vpnInitializeMplsL3VpnIfConfTable
                 (pL3vpnMplsL3VpnIfConfEntry)) == OSIX_FAILURE)
            {
                if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                    (pL3vpnSetMplsL3VpnIfConfEntry,
                     pL3vpnIsSetMplsL3VpnIfConfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnIfConfTable:L3vpnSetAllMplsL3VpnIfConfTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfIndex;
            }

            if (pL3vpnIsSetMplsL3VpnIfConfEntry->
                bMplsL3VpnIfVpnClassification != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfVpnClassification =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfVpnClassification;
            }

            if (pL3vpnIsSetMplsL3VpnIfConfEntry->
                bMplsL3VpnIfVpnRouteDistProtocol != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.
                        au1MplsL3VpnIfVpnRouteDistProtocol,
                        pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        au1MplsL3VpnIfVpnRouteDistProtocol,
                        pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        i4MplsL3VpnIfVpnRouteDistProtocolLen);

                pL3vpnMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfVpnRouteDistProtocolLen =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfVpnRouteDistProtocolLen;
            }

            if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfStorageType =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfStorageType;
            }

            if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus;
            }

            if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName !=
                OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnVrfNameLen;
            }

            if ((pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                 i4MplsL3VpnIfConfRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        i4MplsL3VpnIfConfRowStatus == ACTIVE)))
            {
                pL3vpnMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus = ACTIVE;
            }
            else if (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                     i4MplsL3VpnIfConfRowStatus == CREATE_AND_WAIT)
            {
                pL3vpnMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                 (tRBElem *) pL3vpnMplsL3VpnIfConfEntry) != RB_SUCCESS)
            {
                if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                    (pL3vpnSetMplsL3VpnIfConfEntry,
                     pL3vpnIsSetMplsL3VpnIfConfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
                return OSIX_FAILURE;
            }
            if (L3vpnUtilUpdateMplsL3VpnIfConfTable
                (NULL, pL3vpnMplsL3VpnIfConfEntry,
                 pL3vpnIsSetMplsL3VpnIfConfEntry) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnUtilUpdateMplsL3VpnIfConfTable function returns failure.\r\n"));

                if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                    (pL3vpnSetMplsL3VpnIfConfEntry,
                     pL3vpnIsSetMplsL3VpnIfConfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                           pL3vpnMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
                return OSIX_FAILURE;
            }

            if ((pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                 i4MplsL3VpnIfConfRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        i4MplsL3VpnIfConfRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfIndex =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfIndex;
                MEMCPY (pL3vpnTrgMplsL3VpnIfConfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnTrgMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnVrfNameLen;
                pL3vpnTrgMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus =
                    OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                    (pL3vpnTrgMplsL3VpnIfConfEntry,
                     pL3vpnTrgIsSetMplsL3VpnIfConfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnOldMplsL3VpnIfConfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgMplsL3VpnIfConfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                     i4MplsL3VpnIfConfRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus =
                    OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                    (pL3vpnTrgMplsL3VpnIfConfEntry,
                     pL3vpnTrgIsSetMplsL3VpnIfConfEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnOldMplsL3VpnIfConfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgMplsL3VpnIfConfEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                i4MplsL3VpnIfConfRowStatus == CREATE_AND_GO)
            {
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                    i4MplsL3VpnIfConfRowStatus = ACTIVE;
            }

            if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable:  L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable: Failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
              i4MplsL3VpnIfConfRowStatus == CREATE_AND_WAIT)
             || (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                 i4MplsL3VpnIfConfRowStatus == CREATE_AND_GO))
    {
        if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
            (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnIfConfTable: The row is already present.\r\n"));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pL3vpnOldMplsL3VpnIfConfEntry, pL3vpnMplsL3VpnIfConfEntry,
            sizeof (tL3vpnMplsL3VpnIfConfEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus ==
        DESTROY)
    {
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
            DESTROY;

        if (L3vpnUtilUpdateMplsL3VpnIfConfTable (pL3vpnOldMplsL3VpnIfConfEntry,
                                                 pL3vpnMplsL3VpnIfConfEntry,
                                                 pL3vpnIsSetMplsL3VpnIfConfEntry)
            != OSIX_SUCCESS)
        {

            if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnUtilUpdateMplsL3VpnIfConfTable function returns failure.\r\n"));
        }
        RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
                   pL3vpnMplsL3VpnIfConfEntry);
        if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
            (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (MplsL3VpnIfConfTableFilterInputs
        (pL3vpnMplsL3VpnIfConfEntry, pL3vpnSetMplsL3VpnIfConfEntry,
         pL3vpnIsSetMplsL3VpnIfConfEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus ==
         ACTIVE)
        && (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfConfRowStatus != NOT_IN_SERVICE))
    {
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pL3vpnTrgMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
            NOT_IN_SERVICE;
        pL3vpnTrgIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus =
            OSIX_TRUE;

        if (L3vpnUtilUpdateMplsL3VpnIfConfTable (pL3vpnOldMplsL3VpnIfConfEntry,
                                                 pL3vpnMplsL3VpnIfConfEntry,
                                                 pL3vpnIsSetMplsL3VpnIfConfEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pL3vpnMplsL3VpnIfConfEntry, pL3vpnOldMplsL3VpnIfConfEntry,
                    sizeof (tL3vpnMplsL3VpnIfConfEntry));
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable:                 L3vpnUtilUpdateMplsL3VpnIfConfTable Function returns failure.\r\n"));

            if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
                (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
            return OSIX_FAILURE;
        }

        if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
            (pL3vpnTrgMplsL3VpnIfConfEntry, pL3vpnTrgIsSetMplsL3VpnIfConfEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
        }
    }

    if (pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfVpnClassification =
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnClassification;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol !=
        OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnIfConfEntry->MibObject.
                au1MplsL3VpnIfVpnRouteDistProtocol,
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                au1MplsL3VpnIfVpnRouteDistProtocol,
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                i4MplsL3VpnIfVpnRouteDistProtocolLen);

        pL3vpnMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnRouteDistProtocolLen =
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnRouteDistProtocolLen;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfStorageType =
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfConfStorageType;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus =
            ACTIVE;
    }

    if (L3vpnUtilUpdateMplsL3VpnIfConfTable (pL3vpnOldMplsL3VpnIfConfEntry,
                                             pL3vpnMplsL3VpnIfConfEntry,
                                             pL3vpnIsSetMplsL3VpnIfConfEntry) !=
        OSIX_SUCCESS)
    {

        if (L3vpnSetAllMplsL3VpnIfConfTableTrigger
            (pL3vpnSetMplsL3VpnIfConfEntry, pL3vpnIsSetMplsL3VpnIfConfEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));

        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnUtilUpdateMplsL3VpnIfConfTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pL3vpnMplsL3VpnIfConfEntry, pL3vpnOldMplsL3VpnIfConfEntry,
                sizeof (tL3vpnMplsL3VpnIfConfEntry));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
        return OSIX_FAILURE;

    }
    if (L3vpnSetAllMplsL3VpnIfConfTableTrigger (pL3vpnSetMplsL3VpnIfConfEntry,
                                                pL3vpnIsSetMplsL3VpnIfConfEntry,
                                                SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnIfConfTable: L3vpnSetAllMplsL3VpnIfConfTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnOldMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3vpnTrgMplsL3VpnIfConfEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnTrgIsSetMplsL3VpnIfConfEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnVrfRTTable
 Input       :  pL3vpnSetMplsL3VpnVrfRTEntry
                pL3vpnIsSetMplsL3VpnVrfRTEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                pL3vpnSetMplsL3VpnVrfRTEntry,
                                tL3vpnIsSetMplsL3VpnVrfRTEntry *
                                pL3vpnIsSetMplsL3VpnVrfRTEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnOldMplsL3VpnVrfRTEntry = NULL;
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnTrgMplsL3VpnVrfRTEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRTEntry *pL3vpnTrgIsSetMplsL3VpnVrfRTEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pL3vpnOldMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnOldMplsL3VpnVrfRTEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pL3vpnTrgMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnTrgMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
        return OSIX_FAILURE;
    }
    pL3vpnTrgIsSetMplsL3VpnVrfRTEntry =
        (tL3vpnIsSetMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID);
    if (pL3vpnTrgIsSetMplsL3VpnVrfRTEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pL3vpnOldMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnTrgMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfRTEntry));
    MEMSET (pL3vpnTrgIsSetMplsL3VpnVrfRTEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRTEntry));

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfRTEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                   (tRBElem *) pL3vpnSetMplsL3VpnVrfRTEntry);

    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
             i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT)
            || (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == CREATE_AND_GO)
            ||
            ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
              i4MplsL3VpnVrfRTRowStatus == ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pL3vpnMplsL3VpnVrfRTEntry =
                (tL3vpnMplsL3VpnVrfRTEntry *)
                MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
            if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
            {
                if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRTEntry,
                     pL3vpnIsSetMplsL3VpnVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRTTable:L3vpnSetAllMplsL3VpnVrfRTTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pL3vpnMplsL3VpnVrfRTEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfRTEntry));
            if ((L3vpnInitializeMplsL3VpnVrfRTTable (pL3vpnMplsL3VpnVrfRTEntry))
                == OSIX_FAILURE)
            {
                if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRTEntry,
                     pL3vpnIsSetMplsL3VpnVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRTTable:L3vpnSetAllMplsL3VpnVrfRTTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    u4MplsL3VpnVrfRTIndex;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTType;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfRT,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfRTLen);

                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr !=
                OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfRTDescr,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfRTDescr,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfRTDescrLen);

                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTDescrLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTRowStatus;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTStorageType =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTStorageType;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfNameLen;
            }

            if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                 i4MplsL3VpnVrfRTRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfRTRowStatus == ACTIVE)))
            {
                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
                    ACTIVE;
            }
            else if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                     i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT)
            {
                pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                 (tRBElem *) pL3vpnMplsL3VpnVrfRTEntry) != RB_SUCCESS)
            {
                if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRTEntry,
                     pL3vpnIsSetMplsL3VpnVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
                return OSIX_FAILURE;
            }
            if (L3vpnUtilUpdateMplsL3VpnVrfRTTable
                (NULL, pL3vpnMplsL3VpnVrfRTEntry,
                 pL3vpnIsSetMplsL3VpnVrfRTEntry) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnUtilUpdateMplsL3VpnVrfRTTable function returns failure.\r\n"));

                if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRTEntry,
                     pL3vpnIsSetMplsL3VpnVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                           pL3vpnMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
                return OSIX_FAILURE;
            }

            if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                 i4MplsL3VpnVrfRTRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfRTRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.u4MplsL3VpnVrfRTIndex =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    u4MplsL3VpnVrfRTIndex;
                pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTType =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTType;
                MEMCPY (pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfNameLen;
                pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTRowStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus =
                    OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                    (pL3vpnTrgMplsL3VpnVrfRTEntry,
                     pL3vpnTrgIsSetMplsL3VpnVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                        (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                     i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTRowStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus =
                    OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                    (pL3vpnTrgMplsL3VpnVrfRTEntry,
                     pL3vpnTrgIsSetMplsL3VpnVrfRTEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                        (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTRowStatus == CREATE_AND_GO)
            {
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                    i4MplsL3VpnVrfRTRowStatus = ACTIVE;
            }

            if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                (pL3vpnSetMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable:  L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                (pL3vpnSetMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable: Failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
              i4MplsL3VpnVrfRTRowStatus == CREATE_AND_WAIT)
             || (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                 i4MplsL3VpnVrfRTRowStatus == CREATE_AND_GO))
    {
        if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger (pL3vpnSetMplsL3VpnVrfRTEntry,
                                                   pL3vpnIsSetMplsL3VpnVrfRTEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfRTTable: The row is already present.\r\n"));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pL3vpnOldMplsL3VpnVrfRTEntry, pL3vpnMplsL3VpnVrfRTEntry,
            sizeof (tL3vpnMplsL3VpnVrfRTEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
        DESTROY)
    {
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
            DESTROY;

        if (L3vpnUtilUpdateMplsL3VpnVrfRTTable (pL3vpnOldMplsL3VpnVrfRTEntry,
                                                pL3vpnMplsL3VpnVrfRTEntry,
                                                pL3vpnIsSetMplsL3VpnVrfRTEntry)
            != OSIX_SUCCESS)
        {

            if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                (pL3vpnSetMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnUtilUpdateMplsL3VpnVrfRTTable function returns failure.\r\n"));
        }
        RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
                   pL3vpnMplsL3VpnVrfRTEntry);
        if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
            (pL3vpnSetMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (MplsL3VpnVrfRTTableFilterInputs
        (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnSetMplsL3VpnVrfRTEntry,
         pL3vpnIsSetMplsL3VpnVrfRTEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
         ACTIVE)
        && (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus !=
            NOT_IN_SERVICE))
    {
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pL3vpnTrgMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
            NOT_IN_SERVICE;
        pL3vpnTrgIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus = OSIX_TRUE;

        if (L3vpnUtilUpdateMplsL3VpnVrfRTTable (pL3vpnOldMplsL3VpnVrfRTEntry,
                                                pL3vpnMplsL3VpnVrfRTEntry,
                                                pL3vpnIsSetMplsL3VpnVrfRTEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnOldMplsL3VpnVrfRTEntry,
                    sizeof (tL3vpnMplsL3VpnVrfRTEntry));
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable:                 L3vpnUtilUpdateMplsL3VpnVrfRTTable Function returns failure.\r\n"));

            if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger
                (pL3vpnSetMplsL3VpnVrfRTEntry, pL3vpnIsSetMplsL3VpnVrfRTEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
            return OSIX_FAILURE;
        }

        if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger (pL3vpnTrgMplsL3VpnVrfRTEntry,
                                                   pL3vpnTrgIsSetMplsL3VpnVrfRTEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
        }
    }

    if (pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT != OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen);

        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr != OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTDescrLen);

        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus != OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus = ACTIVE;
    }

    if (L3vpnUtilUpdateMplsL3VpnVrfRTTable (pL3vpnOldMplsL3VpnVrfRTEntry,
                                            pL3vpnMplsL3VpnVrfRTEntry,
                                            pL3vpnIsSetMplsL3VpnVrfRTEntry) !=
        OSIX_SUCCESS)
    {

        if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger (pL3vpnSetMplsL3VpnVrfRTEntry,
                                                   pL3vpnIsSetMplsL3VpnVrfRTEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));

        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnUtilUpdateMplsL3VpnVrfRTTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pL3vpnMplsL3VpnVrfRTEntry, pL3vpnOldMplsL3VpnVrfRTEntry,
                sizeof (tL3vpnMplsL3VpnVrfRTEntry));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
        return OSIX_FAILURE;

    }
    if (L3vpnSetAllMplsL3VpnVrfRTTableTrigger (pL3vpnSetMplsL3VpnVrfRTEntry,
                                               pL3vpnIsSetMplsL3VpnVrfRTEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfRTTable: L3vpnSetAllMplsL3VpnVrfRTTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnOldMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfRTEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRTEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnVrfRteTable
 Input       :  pL3vpnSetMplsL3VpnVrfRteEntry
                pL3vpnIsSetMplsL3VpnVrfRteEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                 pL3vpnSetMplsL3VpnVrfRteEntry,
                                 tL3vpnIsSetMplsL3VpnVrfRteEntry *
                                 pL3vpnIsSetMplsL3VpnVrfRteEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnOldMplsL3VpnVrfRteEntry = NULL;
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnTrgMplsL3VpnVrfRteEntry = NULL;
    tL3vpnIsSetMplsL3VpnVrfRteEntry *pL3vpnTrgIsSetMplsL3VpnVrfRteEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pL3vpnOldMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnOldMplsL3VpnVrfRteEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pL3vpnTrgMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnTrgMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
        return OSIX_FAILURE;
    }
    pL3vpnTrgIsSetMplsL3VpnVrfRteEntry =
        (tL3vpnIsSetMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID);
    if (pL3vpnTrgIsSetMplsL3VpnVrfRteEntry == NULL)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pL3vpnOldMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnTrgMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnMplsL3VpnVrfRteEntry));
    MEMSET (pL3vpnTrgIsSetMplsL3VpnVrfRteEntry, 0,
            sizeof (tL3vpnIsSetMplsL3VpnVrfRteEntry));

    /* Check whether the node is already present */
    pL3vpnMplsL3VpnVrfRteEntry =
        RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable,
                   (tRBElem *) pL3vpnSetMplsL3VpnVrfRteEntry);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
             i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_WAIT)
            || (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_GO)
            ||
            ((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              i4MplsL3VpnVrfRteInetCidrStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pL3vpnMplsL3VpnVrfRteEntry =
                (tL3vpnMplsL3VpnVrfRteEntry *)
                MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
            if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
            {
                if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRteEntry,
                     pL3vpnIsSetMplsL3VpnVrfRteEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRteTable:L3vpnSetAllMplsL3VpnVrfRteTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0,
                    sizeof (tL3vpnMplsL3VpnVrfRteEntry));
            if ((L3vpnInitializeMplsL3VpnVrfRteTable
                 (pL3vpnMplsL3VpnVrfRteEntry)) == OSIX_FAILURE)
            {
                if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRteEntry,
                     pL3vpnIsSetMplsL3VpnVrfRteEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRteTable:L3vpnSetAllMplsL3VpnVrfRteTableTrigger function fails\r\n"));

                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrDestType != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestType =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestType;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest !=
                OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrDest,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrDest,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteInetCidrDestLen);

                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrPfxLen != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    u4MplsL3VpnVrfRteInetCidrPfxLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    u4MplsL3VpnVrfRteInetCidrPfxLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrPolicy != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                        au4MplsL3VpnVrfRteInetCidrPolicy,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au4MplsL3VpnVrfRteInetCidrPolicy,
                        (INT4)(((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                         i4MplsL3VpnVrfRteInetCidrPolicyLen) * ((INT4)sizeof (UINT4)))));

                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrPolicyLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrPolicyLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrNHopType != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNHopType =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNHopType;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrNextHop != OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrNextHop,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrNextHop,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteInetCidrNextHopLen);

                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNextHopLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNextHopLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrIfIndex != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrIfIndex =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrIfIndex;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType !=
                OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrType =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrType;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrNextHopAS != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    u4MplsL3VpnVrfRteInetCidrNextHopAS =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    u4MplsL3VpnVrfRteInetCidrNextHopAS;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrMetric1 != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric1 =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric1;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrMetric2 != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric2 =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric2;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrMetric3 != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric3 =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric3;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrMetric4 != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric4 =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric4;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrMetric5 != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric5 =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrMetric5;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer !=
                OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteXCPointer,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteXCPointer,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteXCPointerLen);

                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteXCPointerLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteXCPointerLen;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->
                bMplsL3VpnVrfRteInetCidrStatus != OSIX_FALSE)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus;
            }

            if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName !=
                OSIX_FALSE)
            {
                MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfNameLen;
            }

            if ((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                 i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteInetCidrStatus == ACTIVE)))
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus = ACTIVE;
            }
            else if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                     i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_WAIT)
            {
                pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable,
                 (tRBElem *) pL3vpnMplsL3VpnVrfRteEntry) != RB_SUCCESS)
            {
                if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRteEntry,
                     pL3vpnIsSetMplsL3VpnVrfRteEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
                }
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
                return OSIX_FAILURE;
            }
            if (L3vpnUtilUpdateMplsL3VpnVrfRteTable
                (NULL, pL3vpnMplsL3VpnVrfRteEntry,
                 pL3vpnIsSetMplsL3VpnVrfRteEntry) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnUtilUpdateMplsL3VpnVrfRteTable function returns failure.\r\n"));

                if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                    (pL3vpnSetMplsL3VpnVrfRteEntry,
                     pL3vpnIsSetMplsL3VpnVrfRteEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable,
                           pL3vpnMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                    (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
                MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
                return OSIX_FAILURE;
            }

            if ((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                 i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteInetCidrStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestType =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestType;
                MEMCPY (pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrDest,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrDest,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteInetCidrDestLen);

                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrDestLen;
                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    u4MplsL3VpnVrfRteInetCidrPfxLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    u4MplsL3VpnVrfRteInetCidrPfxLen;
                MEMCPY (pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                        au4MplsL3VpnVrfRteInetCidrPolicy,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au4MplsL3VpnVrfRteInetCidrPolicy,
                        (INT4)(((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                         i4MplsL3VpnVrfRteInetCidrPolicyLen) * ((INT4)sizeof (UINT4)))));

                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrPolicyLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrPolicyLen;
                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNHopType =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNHopType;
                MEMCPY (pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrNextHop,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfRteInetCidrNextHop,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfRteInetCidrNextHopLen);

                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNextHopLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrNextHopLen;
                MEMCPY (pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        au1MplsL3VpnVrfName,
                        pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                        i4MplsL3VpnVrfNameLen);

                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen =
                    pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfNameLen;
                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnVrfRteEntry->
                    bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                    (pL3vpnTrgMplsL3VpnVrfRteEntry,
                     pL3vpnTrgIsSetMplsL3VpnVrfRteEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnOldMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                     i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus = CREATE_AND_WAIT;
                pL3vpnTrgIsSetMplsL3VpnVrfRteEntry->
                    bMplsL3VpnVrfRteInetCidrStatus = OSIX_TRUE;

                if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                    (pL3vpnTrgMplsL3VpnVrfRteEntry,
                     pL3vpnTrgIsSetMplsL3VpnVrfRteEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnOldMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_GO)
            {
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                    i4MplsL3VpnVrfRteInetCidrStatus = ACTIVE;
            }

            if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable:  L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable: Failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_WAIT)
             || (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                 i4MplsL3VpnVrfRteInetCidrStatus == CREATE_AND_GO))
    {
        if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
            (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfRteTable: The row is already present.\r\n"));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pL3vpnOldMplsL3VpnVrfRteEntry, pL3vpnMplsL3VpnVrfRteEntry,
            sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrStatus == DESTROY)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
            DESTROY;

        if (L3vpnUtilUpdateMplsL3VpnVrfRteTable (pL3vpnOldMplsL3VpnVrfRteEntry,
                                                 pL3vpnMplsL3VpnVrfRteEntry,
                                                 pL3vpnIsSetMplsL3VpnVrfRteEntry)
            != OSIX_SUCCESS)
        {

            if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
            }
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnUtilUpdateMplsL3VpnVrfRteTable function returns failure.\r\n"));
        }
        RBTreeRem (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable,
                   pL3vpnMplsL3VpnVrfRteEntry);
        if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
            (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (MplsL3VpnVrfRteTableFilterInputs
        (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnSetMplsL3VpnVrfRteEntry,
         pL3vpnIsSetMplsL3VpnVrfRteEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
         i4MplsL3VpnVrfRteInetCidrStatus == ACTIVE)
        && (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrStatus != NOT_IN_SERVICE))
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pL3vpnTrgMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrStatus = NOT_IN_SERVICE;
        pL3vpnTrgIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus =
            OSIX_TRUE;

        if (L3vpnUtilUpdateMplsL3VpnVrfRteTable (pL3vpnOldMplsL3VpnVrfRteEntry,
                                                 pL3vpnMplsL3VpnVrfRteEntry,
                                                 pL3vpnIsSetMplsL3VpnVrfRteEntry)
            != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnOldMplsL3VpnVrfRteEntry,
                    sizeof (tL3vpnMplsL3VpnVrfRteEntry));
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable:                 L3vpnUtilUpdateMplsL3VpnVrfRteTable Function returns failure.\r\n"));

            if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
                (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                                (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
            return OSIX_FAILURE;
        }

        if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
            (pL3vpnTrgMplsL3VpnVrfRteEntry, pL3vpnTrgIsSetMplsL3VpnVrfRteEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
        }
    }

    if (pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
        i4MplsL3VpnVrfRteInetCidrStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrIfIndex =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrIfIndex;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrType =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrType;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            u4MplsL3VpnVrfRteInetCidrNextHopAS =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            u4MplsL3VpnVrfRteInetCidrNextHopAS;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric1 =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric1;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric2 =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric2;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric3 =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric3;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric4 =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric4;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrMetric5 =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric5;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer !=
        OSIX_FALSE)
    {
        MEMCPY (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                au1MplsL3VpnVrfRteXCPointer,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                au1MplsL3VpnVrfRteXCPointer,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteXCPointerLen);

        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteXCPointerLen =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteXCPointerLen;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus !=
        OSIX_FALSE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfRteInetCidrStatus =
            ACTIVE;
    }

    if (L3vpnUtilUpdateMplsL3VpnVrfRteTable (pL3vpnOldMplsL3VpnVrfRteEntry,
                                             pL3vpnMplsL3VpnVrfRteEntry,
                                             pL3vpnIsSetMplsL3VpnVrfRteEntry) !=
        OSIX_SUCCESS)
    {

        if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger
            (pL3vpnSetMplsL3VpnVrfRteEntry, pL3vpnIsSetMplsL3VpnVrfRteEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));

        }
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnUtilUpdateMplsL3VpnVrfRteTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pL3vpnMplsL3VpnVrfRteEntry, pL3vpnOldMplsL3VpnVrfRteEntry,
                sizeof (tL3vpnMplsL3VpnVrfRteEntry));
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                            (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                            (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
        return OSIX_FAILURE;

    }
    if (L3vpnSetAllMplsL3VpnVrfRteTableTrigger (pL3vpnSetMplsL3VpnVrfRteEntry,
                                                pL3vpnIsSetMplsL3VpnVrfRteEntry,
                                                SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnSetAllMplsL3VpnVrfRteTable: L3vpnSetAllMplsL3VpnVrfRteTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnOldMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                        (UINT1 *) pL3vpnTrgMplsL3VpnVrfRteEntry);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_ISSET_POOLID,
                        (UINT1 *) pL3vpnTrgIsSetMplsL3VpnVrfRteEntry);
    return OSIX_SUCCESS;

}
