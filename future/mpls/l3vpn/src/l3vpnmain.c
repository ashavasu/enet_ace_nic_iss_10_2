/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnmain.c,v 1.8 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: l3vpnmain.c,v 1.8 2018/01/03 11:31:21 siva Exp $
* Description: This file contains the l3vpn task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __L3VPNMAIN_C__
#include "l3vpninc.h"
#include "fsmplswr.h"

tL3vpnMplsL3VpnIfConfEntry gL3VpnIfConfEntry;
tL3vpnMplsL3VpnVrfEntry gL3VpnVrfEntry;
tL3VpnGlobalTnlInfo gL3VpnGlobalTnlInfo[L3VPN_RSVPTE_MAX_TNL];

/* Proto types of the functions private to this file only */

PRIVATE UINT4 L3vpnMainMemInit PROTO ((VOID));
PRIVATE VOID L3vpnMainMemClear PROTO ((VOID));

static INT4
 
 
 
 
L3VpnTrieAddEntry (VOID *pInputParams, VOID *pOutputParams,
                   VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo);

static INT4
 
 
 
 
L3VpnTrieDeleteEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                      VOID *pOutputParams, VOID *pNextHop, tKey Key);

static INT4
 
 
 
 
L3VpnTrieSearchEntry (tInputParams * pInputParams,
                      tOutputParams * pOutputParams, VOID *pAppSpecInfo);

static UINT1       *L3VpnTrieDelAllEntry (tInputParams * pInputParams,
                                          VOID *dummy,
                                          tOutputParams * pOutputParams);
static VOID         L3VpnTrieDeInit (VOID *pDelParams);

static INT4
        L3VpnTrieDelEntry (VOID *pInRtInfo, VOID **ppAppPtr, tKey key);

static INT4
 
 
 
 
L3VpnTrieLookUpEntry (tInputParams * pInputParams,
                      tOutputParams * pOutputParams,
                      VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key);

static INT4
 
 
 
 
L3VpnTrieBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                    VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key);

VOID                L3VpnFtnTrieCbDelete (VOID *pInput);

static struct TrieAppFns L3VpnTrieLibFuncs = {

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) L3VpnTrieAddEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pNxtHop,
              tKey Key)) L3VpnTrieDeleteEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr)) L3VpnTrieSearchEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppSpecInfo, UINT2 u2KeySize,
              tKey key)) L3VpnTrieLookUpEntry,

    (VOID *(*)(tInputParams *, void (*)(VOID *), VOID *)) NULL,

    (VOID (*)(VOID *)) NULL,

    (INT4 (*)(VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*)(VOID *, VOID *, tKey Key)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID **, void *, UINT4)) NULL,

    (VOID *(*)(tInputParams * pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) L3VpnTrieDelAllEntry,

    (VOID (*)(VOID *pDummy)) L3VpnTrieDeInit,

    (INT4 (*)(VOID *, VOID **ppAppPtr, tKey key)) L3VpnTrieDelEntry,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*)(UINT2 u2KeySize, tInputParams * pInputParams,
              VOID *pOutputParams, VOID *pAppSpecInfo,
              tKey Key)) L3VpnTrieBestMatch,

    (INT4 (*)(tInputParams *, VOID *, VOID *, tKey)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID *, UINT2, tKey)) NULL
};

/****************************************************************************
*                                                                           *
* Function     : L3vpnMainTask                                               *
*                                                                           *
* Description  : Main function of L3VPN.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
L3vpnMainTask ()
{
    UINT4               u4Event = 0;

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnTaskMain\n"));

    /*if (TmrCreateTimerList ((CONST UINT1 *) L3VPN_TASK_NAME, L3VPN_TIMER_EVENT,
       NULL, (tTimerListId *) & (gL3vpnGlobals.l3vpnTmrLst))
       == TMR_FAILURE)
       {
       L3VPN_TRC ((L3VPN_MAIN_TRC, "L3VPN Timer List Creation Failed\n"));
       return;
       } */

    while (OSIX_TRUE == OSIX_TRUE)
    {

        /*OsixReceiveEvent (L3VPN_TIMER_EVENT | L3VPN_QUEUE_EVENT,
           OSIX_EV_ANY, (UINT4) NULL, (UINT4 *) &(u4Event)); */
        if (OsixEvtRecv
            (L3VPN_TASK_ID,
             (L3VPN_TIMER_EVENT | L3VPN_QUEUE_EVENT), OSIX_WAIT,
             &u4Event) != OSIX_SUCCESS)
        {
            continue;
        }
        L3vpnMainTaskLock ();

        L3VPN_TRC ((L3VPN_MAIN_TRC, "Rx Event %d\n", u4Event));

        if ((u4Event & L3VPN_TIMER_EVENT) == L3VPN_TIMER_EVENT)
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%L3VPN_TIMER_EVENT.\r\n"));
            L3vpnTmrHandleExpiry ();
        }

        if ((u4Event & L3VPN_QUEUE_EVENT) == L3VPN_QUEUE_EVENT)
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%L3VPN_QUEUE_EVENT.\r\n"));

            L3vpnQueProcessMsgs ();
        }

        /* Mutual exclusion flag OFF */
        L3vpnMainTaskUnLock ();
    }
}

/****************************************************************************
*                                                                           *
* Function     : L3vpnMainTaskInit                                          *
*                                                                           *
* Description  : L3VPN initialization routine.                              *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
L3vpnMainTaskInit (VOID)
{
    tL3VpnBgpRegEntry   L3VpnBgpRegEntry;

#ifdef L3VPN_TRACE_WANTED
    L3VPN_TRC_FLAG = ~((UINT4) 0);
#endif

    MEMSET (&L3VpnBgpRegEntry, 0, sizeof (tL3VpnBgpRegEntry));

    /** to save the BgpRegistration Pointers **/
    MEMCPY (&L3VpnBgpRegEntry, &gL3vpnGlobals.L3VpnBgpRegEntry,
            sizeof (tL3VpnBgpRegEntry));
    MEMSET (&gL3vpnGlobals, 0, sizeof (tL3vpnGlobals));
    MEMCPY (&gL3vpnGlobals.L3VpnBgpRegEntry, &L3VpnBgpRegEntry,
            sizeof (tL3VpnBgpRegEntry));

    MEMCPY (gL3vpnGlobals.au1TaskSemName, L3VPN_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);
    MEMCPY (gL3vpnGlobals.au1BindSemName, L3VPN_BIND_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);
    if (OsixCreateSem
        (L3VPN_MUT_EXCL_SEM_NAME, L3VPN_SEM_CREATE_INIT_CNT, 0,
         &gL3vpnGlobals.l3vpnTaskSemId) == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Seamphore Creation failure for %s \n",
                    L3VPN_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }
    if (OsixCreateSem
        (L3VPN_BIND_MUT_EXCL_SEM_NAME, 1, 0,
         &gL3vpnGlobals.l3vpnBindSemId) == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Seamphore Creation failure for %s \n",
                    L3VPN_BIND_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }
    if (L3vpnUtlCreateRBTree () == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Create RBTree Failed.\r\n"));
        return OSIX_FAILURE;
    }

    if (L3VpnInitTrie () == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Init Trie Failed.\r\n"));
        return OSIX_FAILURE;
    }
    /* Create buffer pools for data structures */

    if (L3vpnMainMemInit () == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Memory Pool Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }

    if (OsixCreateQ (L3VPN_QUEUE_NAME, L3VPN_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gL3vpnGlobals.l3vpnQueId)) == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%IAPQ Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }

    if (L3VpnRegisterWithVcm () == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Registration failed with VCM.\r\n"));

        return OSIX_FAILURE;
    }

    /*L3vpnTmrInitTmrDesc (); */
    L3VpnInitRTIndexMgr ();
    L3VPnInitMibScalar ();

#ifdef SNMP_2_WANTED
    RegisterFSMPLSL3vpnObj ();
#endif

    /* The following routine initialises the timer descriptor structure */
/*
    L3vpnUtilInitGlobals();
*/
/*    if (L3vpnUtlCreatMemPool() == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }*/
    /* if (L3vpnSizingMemCreateMemPools () == OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       } 
     */
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
L3vpnMainDeInit (VOID)
{

    L3vpnMainMemClear ();

#ifdef SNMP_2_WANTED
    UnRegisterFSMPLSL3vpnObj ();
#endif

    if (gL3vpnGlobals.l3vpnTaskSemId)
    {
        OsixSemDel (gL3vpnGlobals.l3vpnTaskSemId);
    }
    if (gL3vpnGlobals.l3vpnBindSemId)
    {
        OsixSemDel (gL3vpnGlobals.l3vpnBindSemId);
    }
    if (gL3vpnGlobals.l3vpnQueId)
    {
        OsixQueDel (gL3vpnGlobals.l3vpnQueId);
    }
    TmrDeleteTimerList (gL3vpnGlobals.l3vpnTmrLst);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the L3vpn Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L3vpnMainTaskLock (VOID)
{

    if (OsixSemTake (gL3vpnGlobals.l3vpnTaskSemId) == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "TakeSem failure for %s \n", L3VPN_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the L3VPN Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L3vpnMainTaskUnLock (VOID)
{

    OsixSemGive (gL3vpnGlobals.l3vpnTaskSemId);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for L3VPN         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
L3vpnMainMemInit (VOID)
{
    if (L3vpnSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC, "\r%%Memory Allocation Failed.\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnBindTaskLock                                           */
/*                                                                           */
/* Description  : Lock the L3vpn Bind Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L3vpnBindTaskLock (VOID)
{

    if (OsixSemTake (gL3vpnGlobals.l3vpnBindSemId) == OSIX_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "TakeSem failure for %s \n", L3VPN_BIND_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnBindTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the L3VPN Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L3vpnBindTaskUnLock (VOID)
{

    OsixSemGive (gL3vpnGlobals.l3vpnBindSemId);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3vpnMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
L3vpnMainMemClear (VOID)
{
    L3vpnSizingMemDeleteMemPools ();
}

UINT4
L3VpnInitTrie (VOID)
{
    tTrieCrtParams      L3VpnTrieCrtParams;
    UINT4               u4ContextId = 0;

    /* Excluding the Default Context intentionally
     * L3VPN will hold non-default context routes */
    for (u4ContextId = L3VPN_ONE; u4ContextId < MAX_L3VPN_VRF_ENTRIES;
         u4ContextId++)
    {
        MEMSET (&L3VpnTrieCrtParams, 0, sizeof (tTrieCrtParams));
        L3VpnTrieCrtParams.u2KeySize = 2 * sizeof (UINT4);    /* key is Min Address */
        L3VpnTrieCrtParams.u4Type = (L3VPN_MPLS_VRF_RIB | (u4ContextId << 8));
        L3VpnTrieCrtParams.AppFns = &(L3VpnTrieLibFuncs);
        L3VpnTrieCrtParams.u4NumRadixNodes =
            FsL3VPNSizingParams[MAX_L3VPN_ROUTE_TABLE_ENTRIES_SIZING_ID].
            u4PreAllocatedUnits;
        L3VpnTrieCrtParams.u4NumLeafNodes =
            FsL3VPNSizingParams[MAX_L3VPN_ROUTE_TABLE_ENTRIES_SIZING_ID].
            u4PreAllocatedUnits;
        L3VpnTrieCrtParams.u4NoofRoutes =
            FsL3VPNSizingParams[MAX_L3VPN_ROUTE_TABLE_ENTRIES_SIZING_ID].
            u4PreAllocatedUnits;
        L3VpnTrieCrtParams.u1AppId = MPLS_L3VPN_APPL_ID;
        gL3vpnGlobals.gpL3VpnTrieRoot[u4ContextId] =
            TrieCreateInstance (&L3VpnTrieCrtParams);
        if (gL3vpnGlobals.gpL3VpnTrieRoot[u4ContextId] == NULL)
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "Unable to create TRIE for the context: %d\n",
                        u4ContextId));

            return L3VPN_FAILURE;
        }
        else
        {

            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "TRIE successfully created for the context: %d\n",
                        u4ContextId));
        }
    }
    return L3VPN_SUCCESS;
}

UINT4
L3VpnProcessVcmIfMapEvent (tL3VpnVcmIfEvtInfo * pL3VpnVcmIfEvtInfo)
{

    UINT4               u4ContextId = 0;
    INT4                i4IfIndex = 0;

    u4ContextId = pL3VpnVcmIfEvtInfo->u4ContextId;
    i4IfIndex = pL3VpnVcmIfEvtInfo->i4IfIndex;
    return L3VpnMapInterfaceToVrf (u4ContextId, i4IfIndex);
}

UINT4
L3VpnMapInterfaceToVrf (UINT4 u4ContextId, INT4 i4IfIndex)
{

    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry = NULL;
    UINT1               u1OperStatus = 0;
    tL3vpnMplsL3VpnVrfEntry *pL3VpnVrfEntry = NULL;
    INT4                i4VrfNameLen = 0;

    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (&gL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));
    MEMSET (&gL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    if (VcmGetAliasName (u4ContextId, au1VrfName) == VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Unable to get the Alias name.\r\n"));
        return L3VPN_FAILURE;
    }

    i4VrfNameLen = (INT4) STRLEN (au1VrfName);
    MEMCPY (L3VPN_VRF_NAME (gL3VpnVrfEntry), au1VrfName, i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (gL3VpnVrfEntry) = i4VrfNameLen;
    pL3VpnVrfEntry = (tL3vpnMplsL3VpnVrfEntry *)
        RBTreeGet (L3VPN_VRF_TABLE, (tRBElem *) & gL3VpnVrfEntry);

    if (pL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%MPLS VRF entry does not exist yet.\r\n"));
        return L3VPN_FAILURE;
    }

    L3VPN_IF_CONF_IF_INDEX (gL3VpnIfConfEntry) = i4IfIndex;
    MEMCPY (L3VPN_VRF_NAME (gL3VpnIfConfEntry), au1VrfName, i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (gL3VpnIfConfEntry) = i4VrfNameLen;

    pL3VpnIfConfEntry = RBTreeGet (L3VPN_IF_CONF_TABLE,
                                   (tRBElem *) & gL3VpnIfConfEntry);

    if (pL3VpnIfConfEntry != NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%IfConf mapping already exists.\r\n"));
        return L3VPN_FAILURE;
    }

    if (CfaGetIfOperStatus ((UINT4) i4IfIndex, &u1OperStatus) == CFA_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Failed to query the oper status from CFA.\r\n"));
        return L3VPN_FAILURE;
    }

    pL3VpnIfConfEntry =
        L3VpnCreateMplsL3VpnIfConfEntry (i4IfIndex, u4ContextId);
    if (RBTreeAdd (L3VPN_IF_CONF_TABLE, (tRBElem *) pL3VpnIfConfEntry) !=
        RB_SUCCESS)
    {
        L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                    "\r%%Failed to add entry to the RB tree\r\n"));
        return L3VPN_FAILURE;
    }
    L3VpnUpdateNoOfConnectedInterfaces (L3VPN_INTERFACE_MAPPED);

    /* Increment the number of Associated interfaces */
    L3VPN_P_VRF_ASSOCIATED_INTERFACES (pL3VpnVrfEntry)++;

    /* Update the conf last changed time */
    L3VpnUpdateVrfConfLastChanged (pL3VpnVrfEntry);

    if (u1OperStatus == CFA_IF_UP)
    {
        /* Increment the number of Active interfaces */
        L3VPN_P_VRF_ACTIVE_INTERFACES (pL3VpnVrfEntry)++;
        if (L3VPN_P_VRF_ADMIN_STATUS (pL3VpnVrfEntry) ==
            L3VPN_VRF_ADMIN_STATUS_UP)
        {
            if (L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) == L3VPN_VRF_OPER_DOWN)
            {
                L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) = L3VPN_VRF_OPER_UP;
                if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
                {
                    (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                            L3VPN_BGP4_VRF_UP);
                }
                else
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\r%%Cannot send VRF up to BGP to BGP.. Callback is NULL\r\n"));

                }
                /* TODO: Callback to BGP with status UP */
            }
        }
    }
    else
    {
        /*Oper status need not be updated */
        L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Oper status of the interface is down.\r\n"));
    }
    return L3VPN_SUCCESS;
}

INT4
L3VpnProcessVcmIfUnmapEvent (tL3VpnVcmIfEvtInfo * pL3VpnVcmIfEvtInfo)
{
    UINT4               u4ContextId = 0;
    INT4                i4IfIndex = 0;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry = NULL;
    UINT1               u1OperStatus = 0;
    tL3vpnMplsL3VpnVrfEntry *pL3VpnVrfEntry = NULL;
    INT4                i4VrfNameLen = 0;

    u4ContextId = pL3VpnVcmIfEvtInfo->u4ContextId;
    i4IfIndex = pL3VpnVcmIfEvtInfo->i4IfIndex;

    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);

    MEMSET (&gL3VpnIfConfEntry, 0, sizeof (tL3vpnMplsL3VpnIfConfEntry));

    MEMSET (&gL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    if (VcmGetAliasName (u4ContextId, au1VrfName) == VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Unable to get the Alias name.\r\n"));
        return L3VPN_FAILURE;
    }
    i4VrfNameLen = (INT4) STRLEN (au1VrfName);

    MEMCPY (L3VPN_VRF_NAME (gL3VpnVrfEntry), au1VrfName, i4VrfNameLen);

    L3VPN_VRF_NAME_LEN (gL3VpnVrfEntry) = i4VrfNameLen;

    pL3VpnVrfEntry = RBTreeGet (L3VPN_VRF_TABLE, (tRBElem *) & gL3VpnVrfEntry);
    if (pL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%MPLS VRF entry does not exist yet.\r\n"));
        return L3VPN_FAILURE;
    }
    L3VPN_IF_CONF_IF_INDEX (gL3VpnIfConfEntry) = i4IfIndex;

    MEMCPY (L3VPN_VRF_NAME (gL3VpnIfConfEntry), au1VrfName, i4VrfNameLen);

    L3VPN_VRF_NAME_LEN (gL3VpnIfConfEntry) = i4VrfNameLen;

    if (CfaGetIfOperStatus ((UINT4) i4IfIndex, &u1OperStatus) == CFA_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Failed to query the oper status from CFA\r\n"));
        return L3VPN_FAILURE;
    }

    pL3VpnIfConfEntry = RBTreeGet (L3VPN_IF_CONF_TABLE,
                                   (tRBElem *) & gL3VpnIfConfEntry);
    if (pL3VpnIfConfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Interface is not mapped to VRF in L3VPN\r\n"));
        return L3VPN_FAILURE;
    }

    /* Decrementing both Active and Associtaed interfaces count */
    L3VPN_P_VRF_ACTIVE_INTERFACES (pL3VpnVrfEntry)--;
    L3VPN_P_VRF_ASSOCIATED_INTERFACES (pL3VpnVrfEntry)--;

    /* Update the last changed conf time */
    L3VpnUpdateVrfConfLastChanged (pL3VpnVrfEntry);
    if (u1OperStatus == CFA_IF_UP)
    {
        /* If none of the interfaces is oper up, mark VRF oper down */
        if (L3VpnIsRemainingInterfacesUp (au1VrfName, i4IfIndex) ==
            L3VPN_FAILURE)
        {
            L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;
            /* TODO: Callback to BGP with status DOWN */
            if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
            {
                (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                        L3VPN_BGP4_VRF_DOWN);
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\r%%Cannot send VRF DOWN to BGP to BGP.. Callback is NULL.\r\n"));

            }
        }
    }
    else
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%No oper status update is required.\r\n"));
        /* No update in oper status required */
    }
    /* Removing the entry from RB tree */
    RBTreeRem (L3VPN_IF_CONF_TABLE, pL3VpnIfConfEntry);
    L3VpnUpdateNoOfConnectedInterfaces (L3VPN_INTERFACE_UNMAPPED);
    MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                        (UINT1 *) pL3VpnIfConfEntry);
    return L3VPN_SUCCESS;
}

UINT4
L3VpnProcessIfEvent (tL3VpnIfEvtInfo * pL3VpnIfEvtInfo)
{
    tL3vpnMplsL3VpnVrfEntry *pL3VpnVrfEntry = NULL;
    UINT4               u4ContextId = 0;

    pL3VpnVrfEntry = L3VpnGetVrfEntryWithIfIndex (pL3VpnIfEvtInfo->i4IfIndex);
    if (pL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Interface is not mapped to the MPLS Vrf.\r\n"));
        return L3VPN_SUCCESS;
    }

    if (VcmIsVrfExist (pL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
                       &u4ContextId) == VCM_FALSE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Error in getting the context Id for the context: %s\n",
                    pL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName));
        return L3VPN_FAILURE;
    }

    if (pL3VpnIfEvtInfo->u1OperStatus == CFA_IF_UP)
    {
        L3VPN_P_VRF_ACTIVE_INTERFACES (pL3VpnVrfEntry)++;
        if (L3VPN_P_VRF_ADMIN_STATUS (pL3VpnVrfEntry) ==
            L3VPN_VRF_ADMIN_STATUS_UP)
        {
            if (L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) == L3VPN_VRF_OPER_DOWN)
            {
                L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) = L3VPN_VRF_OPER_UP;
                /* TODO: Callback to BGP with status UP */
                if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
                {
                    (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                            L3VPN_BGP4_VRF_UP);
                }
                else
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\r%%Cannot send VRF up to BGP to BGP.. Callback is NULL.\r\n"));
                }
                /* Send Trap if enabled */
                if (L3VPN_NOTIFICATION_ENABLE == L3VPN_TRAP_ENABLE)
                {
                    if (L3VpnSendTrapForVrfStatus
                        (L3VPN_P_VRF_NAME (pL3VpnVrfEntry), L3VPN_VRF_UP_TRAP,
                         pL3VpnIfEvtInfo->i4IfIndex) == L3VPN_FAILURE)
                    {
                        L3VPN_TRC ((L3VPN_MAIN_TRC,
                                    "Failed to Send the VRF UP trap for the VRF: %s\n",
                                    L3VPN_P_VRF_NAME (pL3VpnVrfEntry)));
                    }
                }
                else
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\r%%Trap Not enabled for L3VPN.\r\n"));
                }
                /*L3VpnSendTrapForRouteThreshold (L3VPN_P_VRF_NAME (pL3VpnVrfEntry), L3VPN_VRF_MAX_THRESHOLD_CLEARED); */
            }
        }
    }
    else
    {
        if (L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) != L3VPN_VRF_OPER_DOWN)
        {
            L3VPN_P_VRF_ACTIVE_INTERFACES (pL3VpnVrfEntry)--;
        }
        /*if(L3VpnIsRemainingInterfacesUp(L3VPN_P_VRF_NAME (pL3VpnVrfEntry), 
           pL3VpnIfEvtInfo->i4IfIndex) == L3VPN_FAILURE) */

        if (L3VPN_P_VRF_ACTIVE_INTERFACES (pL3VpnVrfEntry) == 0)
        {
            L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;
            if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
            {
                (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                        L3VPN_BGP4_VRF_DOWN);
            }
            else
            {

                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\r%%Cannot send VRF DOWN to BGP to BGP.. Callback is NULL\r\n"));
            }
            /* TODO Send notification to BGP */

            /* Send Trap for VRF_DOWN if enabled */
            if (L3VPN_NOTIFICATION_ENABLE == L3VPN_TRAP_ENABLE)
            {
                if (L3VpnSendTrapForVrfStatus
                    (L3VPN_P_VRF_NAME (pL3VpnVrfEntry), L3VPN_VRF_DOWN_TRAP,
                     pL3VpnIfEvtInfo->i4IfIndex) == L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Failed to Send the VRF UP trap for the VRF: %s\n",
                                L3VPN_P_VRF_NAME (pL3VpnVrfEntry)));
                }
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\r%%Trap not enabled for L3VPN.\r\n"));
            }
        }
    }
    return L3VPN_SUCCESS;
}

UINT4
L3VpnProcessNonTeLspEvent (tL3VpnNonTeLspEventInfo * pL3VpnNonTeLspEventInfo)
{

    UINT4               u4OutIfIndex = 0;

    if (pL3VpnNonTeLspEventInfo->u4EventType == L3VPN_MPLS_LSP_DOWN)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "LSP DOWN EVENT: Lsp Label = %d\n",
                    pL3VpnNonTeLspEventInfo->u4Label));
        if (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL)
        {
            (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB)) (pL3VpnNonTeLspEventInfo->
                                                    u4Label,
                                                    L3VPN_BGP4_LSP_STATUS_DOWN);
        }
        else
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "\r%%Cannot send LSP Down to BGp...since callback is NULL\r\n"));
        }
        /* De-Register for the LSP, since after DOWN event to BGP, it will register again */
        if (MplsGetNonTeLspOutSegmentFromPrefix
            (pL3VpnNonTeLspEventInfo->u4Prefix, &u4OutIfIndex) == MPLS_SUCCESS)
        {
            if (MplsRegOrDeRegNonTeLspWithL3Vpn
                (pL3VpnNonTeLspEventInfo->u4Prefix, FALSE) == MPLS_FAILURE)
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC,
                            "Regsitraion failed for the prefix:%d\n",
                            pL3VpnNonTeLspEventInfo->u4Prefix));
                return L3VPN_FAILURE;
            }
        }
        else
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "Cannot De-regsiter with the LSP...LSP not found for the prefix:%d\n",
                        pL3VpnNonTeLspEventInfo->u4Prefix));
        }
    }
    return L3VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3VpnProcessRsvpTeLspEvent                                 */
/*                                                                           */
/* Description  : This function called from RSVP-TE to update                 */
/*               corresponding tunnelInfo to our Global array and            */
/*               local binding table, if binding is exists.                  */
/*                                                                           */
/* Input        : tL3VpnRsvpTeLspEventInfo * pL3VpnRsvpTeLspEventInfo        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L3VPN_FAILURE/L3VPN_SUCCESS                                */
/*                                                                           */
/*****************************************************************************/
UINT4
L3VpnProcessRsvpTeLspEvent (tL3VpnRsvpTeLspEventInfo * pL3VpnRsvpTeLspEventInfo)
{

    /* UINT4     u4OutIfIndex = 0;
       UINT4     u4LspLabel = 0;    */
    tXcEntry           *pXcEntry = NULL;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));

    L3VPN_TRC ((L3VPN_MAIN_TRC,
                "L3Vpn tunnelId:%d event:%d XcIndex:%d OperStatus:%d protectionState:%d \n",
                pL3VpnRsvpTeLspEventInfo->u4TnlIndex,
                pL3VpnRsvpTeLspEventInfo->u4EventType,
                pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex,
                pL3VpnRsvpTeLspEventInfo->u1TnlOperStatus,
                pL3VpnRsvpTeLspEventInfo->u1Protection));

    if (L3VpnAddGlobalTnlTable (pL3VpnRsvpTeLspEventInfo) == L3VPN_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "L3VpnProcessRsvpTeLspEvent: Tunnel not found in mapping table. RSVP-TE update added failed to l3vpn global table\n\r"));
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
        return L3VPN_FAILURE;

    }

    pL3VpnRsvpMapLabelEntry =
        L3VpnFetchRsvpMapLabelTableFromTnlIndex (pL3VpnRsvpTeLspEventInfo->
                                                 u4TnlIndex);
    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "L3VpnProcessRsvpTeLspEvent: Tunnel not found in mapping table. But RSVP-TE update added to l3vpn global table\n\r"));
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
        return L3VPN_SUCCESS;
    }
    else
    {
        if ((pL3VpnRsvpTeLspEventInfo->u4EventType == L3VPN_RSVPTE_LSP_DOWN)
            || (pL3VpnRsvpTeLspEventInfo->u4EventType == L3VPN_RSVPTE_TNL_DOWN))
        {
            if ((pL3VpnRsvpMapLabelEntry->u4TnlXcIndex ==
                 pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex)
                && (pL3VpnRsvpMapLabelEntry->u4TnlLabel != 0)
                && (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL))
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3Vpn L3VPN_RSVPTE_LSP_DOWN/L3VPN_RSVPTE_TNL_DOWN for u4TnlLabel:%d \n",
                            pL3VpnRsvpMapLabelEntry->u4TnlLabel));
                /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
                (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB))
                    (pL3VpnRsvpMapLabelEntry->u4TnlLabel,
                     L3VPN_BGP4_LSP_STATUS_DOWN);
                pL3VpnRsvpMapLabelEntry->u4TnlXcIndex = 0;

            }

            if (pL3VpnRsvpMapLabelEntry->u4TnlProXcIndex ==
                pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex)
                pL3VpnRsvpMapLabelEntry->u4TnlProXcIndex = 0;

        }

        if (pL3VpnRsvpTeLspEventInfo->u4EventType == L3VPN_RSVPTE_LSP_UP)
        {
            /* update TunnelXcIndex to binding table */

            if (pL3VpnRsvpMapLabelEntry->u4TnlXcIndex == 0)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3Vpn L3VPN_RSVPTE_LSP_UP event for new entry\n"));
                pL3VpnRsvpMapLabelEntry->u4TnlXcIndex =
                    pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
                pL3VpnRsvpMapLabelEntry->u1TnlProInUse =
                    pL3VpnRsvpTeLspEventInfo->u1Protection;

            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3Vpn L3VPN_RSVPTE_LSP_UP event for existing entry\n"));
                if ((pL3VpnRsvpTeLspEventInfo->u1Protection == L3VPN_PROT_AVAIL)
                    || (pL3VpnRsvpTeLspEventInfo->u1Protection ==
                        L3VPN_PROT_NOT_AVAIL))
                {
                    if ((pL3VpnRsvpMapLabelEntry->u4TnlLabel != 0)
                        && (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL))
                    {
                        /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
                        (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB))
                            (pL3VpnRsvpMapLabelEntry->u4TnlLabel,
                             L3VPN_BGP4_LSP_STATUS_DOWN);

                    }
                    pL3VpnRsvpMapLabelEntry->u4TnlProXcIndex =
                        pL3VpnRsvpMapLabelEntry->u4TnlXcIndex;
                    pL3VpnRsvpMapLabelEntry->u4TnlXcIndex =
                        pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
                }
                else if (pL3VpnRsvpTeLspEventInfo->u1Protection ==
                         L3VPN_PROT_IN_USE)
                {
                    if ((pL3VpnRsvpMapLabelEntry->u4TnlLabel != 0)
                        && (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL))
                    {
                        /*  RSVP-TE MPLS-L3VPN-TE Added updates on RBTree added tunnel type */
                        (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB))
                            (pL3VpnRsvpMapLabelEntry->u4TnlLabel,
                             L3VPN_BGP4_LSP_STATUS_DOWN);

                    }
                    pL3VpnRsvpMapLabelEntry->u4TnlProXcIndex =
                        pL3VpnRsvpMapLabelEntry->u4TnlXcIndex;
                    pL3VpnRsvpMapLabelEntry->u4TnlXcIndex =
                        pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;

                }
                else
                {
                    pL3VpnRsvpMapLabelEntry->u4TnlProXcIndex =
                        pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
                    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
                    return L3VPN_SUCCESS;

                }

            }

            if ((pXcEntry = MplsGetXCEntryByDirection
                 (pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex,
                  (eDirection) MPLS_DIRECTION_ANY)) == NULL)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3VpnProcessRsvpTeLspEvent : pXcEntry is NULL\n"));
                L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
                return L3VPN_FAILURE;
            }
            pL3VpnRsvpMapLabelEntry->u4TnlLabel = pXcEntry->pOutIndex->u4Label;

        }

        if (pL3VpnRsvpTeLspEventInfo->u4EventType == L3VPN_RSVPTE_TNL_DEL)
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC, "L3Vpn L3VPN_RSVPTE_TNL_DEL event\n"));
            if (L3VpnDeleteRsvpMapLabelTable (pL3VpnRsvpMapLabelEntry) !=
                L3VPN_SUCCESS)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3Vpn mpls binding deletion failed for tunnelId %d during rsvp tunnel deletion\n",
                            pL3VpnRsvpMapLabelEntry->u4TnlIndex));
            }
            else
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "L3Vpn mpls binding deleted for tunnelId %d during mpls tunnel deletion\n",
                            pL3VpnRsvpMapLabelEntry->u4TnlIndex));
            }
        }
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return L3VPN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3VpnGetGlobalTnlTable                                     */
/*                                                                           */
/* Description  : This function used to get rsvp-te tunnelInfo for           */
/*                corresponding tunnelId                                     */
/*                                                                           */
/* Input        : u4TnlIndex,tL3VpnGlobalTnlInfo *pL3VpnGlobalTnlInfo        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L3VPN_FAILURE/L3VPN_SUCCESS                                */
/*                                                                           */
/*****************************************************************************/
UINT4
L3VpnGetGlobalTnlTable (UINT4 u4TnlIndex,
                        tL3VpnGlobalTnlInfo * pL3VpnGlobalTnlInfo)
{
    UINT4               u4Count = 0;
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    L3VPN_BIND_LOCK ();
    for (u4Count = 0; u4Count < L3VPN_RSVPTE_MAX_TNL; u4Count++)
    {
        if (gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex == u4TnlIndex)
        {
            MEMCPY (pL3VpnGlobalTnlInfo, &gL3VpnGlobalTnlInfo[u4Count],
                    sizeof (tL3VpnGlobalTnlInfo));
            L3VPN_BIND_UNLOCK ();
            return L3VPN_SUCCESS;
        }
    }
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    L3VPN_BIND_UNLOCK ();
    return L3VPN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : L3VpnAddGlobalTnlTable                                     */
/*                                                                           */
/* Description  : This function called from L3VpnProcessRsvpTeLspEvent       */
/*               to update corresponding tunnelInfo to our Global array      */
/*               this value is used for explicit binding configuration.      */
/*                                                                           */
/* Input        : u4TnlIndex,u4TnlXcIndex,u1TnlOperStatus,u1TnlAdminStatus   */
/*                u1Protection,u1ActionType                                  */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : L3VPN_FAILURE/L3VPN_SUCCESS                                */
/*                                                                           */
/*****************************************************************************/

UINT4
L3VpnAddGlobalTnlTable (tL3VpnRsvpTeLspEventInfo * pL3VpnRsvpTeLspEventInfo)
{
    UINT4               u4Count = 0;

    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Entry\n", __FUNCTION__));
    L3VPN_BIND_LOCK ();
    for (u4Count = 0; u4Count < L3VPN_RSVPTE_MAX_TNL; u4Count++)
    {
        if ((gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex == 0)
            && (pL3VpnRsvpTeLspEventInfo->u4EventType == L3VPN_RSVPTE_LSP_UP))
        {
            gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex =
                pL3VpnRsvpTeLspEventInfo->u4TnlIndex;
            gL3VpnGlobalTnlInfo[u4Count].u4TnlXcIndex =
                pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
            gL3VpnGlobalTnlInfo[u4Count].u1TnlOperStatus =
                pL3VpnRsvpTeLspEventInfo->u1TnlOperStatus;
            gL3VpnGlobalTnlInfo[u4Count].u1TnlAdminStatus =
                pL3VpnRsvpTeLspEventInfo->u1TnlAdminStatus;
            gL3VpnGlobalTnlInfo[u4Count].u1TnlProInUse =
                pL3VpnRsvpTeLspEventInfo->u1Protection;
            L3VPN_BIND_UNLOCK ();
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
            return L3VPN_SUCCESS;
        }
        else if ((gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex ==
                  pL3VpnRsvpTeLspEventInfo->u4TnlIndex)
                 && (pL3VpnRsvpTeLspEventInfo->u1Protection == L3VPN_PROT_AVAIL)
                 && (pL3VpnRsvpTeLspEventInfo->u4EventType ==
                     L3VPN_RSVPTE_LSP_UP))
        {
            gL3VpnGlobalTnlInfo[u4Count].u4TnlProXcIndex =
                gL3VpnGlobalTnlInfo[u4Count].u4TnlXcIndex;
            gL3VpnGlobalTnlInfo[u4Count].u4TnlXcIndex =
                pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
            L3VPN_BIND_UNLOCK ();
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
            return L3VPN_SUCCESS;

        }
        else if ((gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex ==
                  pL3VpnRsvpTeLspEventInfo->u4TnlIndex)
                 && (pL3VpnRsvpTeLspEventInfo->u1Protection ==
                     L3VPN_PROT_IN_USE)
                 && (pL3VpnRsvpTeLspEventInfo->u4EventType ==
                     L3VPN_RSVPTE_LSP_UP))
        {
            gL3VpnGlobalTnlInfo[u4Count].u4TnlProXcIndex =
                gL3VpnGlobalTnlInfo[u4Count].u4TnlXcIndex;
            gL3VpnGlobalTnlInfo[u4Count].u4TnlXcIndex =
                pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
            L3VPN_BIND_UNLOCK ();
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
            return L3VPN_SUCCESS;

        }
        else if ((gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex ==
                  pL3VpnRsvpTeLspEventInfo->u4TnlIndex)
                 && (pL3VpnRsvpTeLspEventInfo->u1Protection ==
                     L3VPN_PROT_NOT_APPLICABLE)
                 && (pL3VpnRsvpTeLspEventInfo->u4EventType ==
                     L3VPN_RSVPTE_LSP_UP))
        {
            gL3VpnGlobalTnlInfo[u4Count].u4TnlProXcIndex =
                pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex;
            L3VPN_BIND_UNLOCK ();
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
            return L3VPN_SUCCESS;

        }
        else if ((gL3VpnGlobalTnlInfo[u4Count].u4TnlIndex ==
                  pL3VpnRsvpTeLspEventInfo->u4TnlIndex)
                 && (gL3VpnGlobalTnlInfo[u4Count].u4TnlXcIndex ==
                     pL3VpnRsvpTeLspEventInfo->u4TnlXcIndex))
        {
            MEMSET (&gL3VpnGlobalTnlInfo[u4Count], 0,
                    sizeof (tL3VpnGlobalTnlInfo));
            L3VPN_BIND_UNLOCK ();
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
            return L3VPN_SUCCESS;
        }
    }
    L3VPN_BIND_UNLOCK ();
    L3VPN_TRC ((L3VPN_MAIN_TRC, "\n%s Exit\n", __FUNCTION__));
    return L3VPN_FAILURE;
}

UINT4
L3VpnProcessVrfAdminEvent (tL3VpnVrfAdminEventInfo * pL3VpnVrfAdminEventInfo)
{
    tL3vpnMplsL3VpnVrfEntry *pL3VpnVrfEntry = NULL;
    tL3vpnMplsL3VpnVrfEntry L3vpnMplsL3VpnVrfEntry;
    INT4                i4NameLen = 0;
    UINT1              *pu1VrfName = NULL;
    UINT4               u4ContextId = 0;

    pu1VrfName = pL3VpnVrfAdminEventInfo->au1VrfName;
    MEMSET (&L3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));

    if (VcmIsVrfExist (pu1VrfName, &u4ContextId) == VCM_FALSE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "Context id of the switch %u",
                    u4ContextId));
        return L3VPN_FAILURE;
    }

    i4NameLen = (INT4) STRLEN (pu1VrfName);
    MEMCPY (L3VPN_VRF_NAME (L3vpnMplsL3VpnVrfEntry), pu1VrfName, i4NameLen);
    L3VPN_VRF_NAME_LEN (L3vpnMplsL3VpnVrfEntry) = i4NameLen;
    pL3VpnVrfEntry = RBTreeGet (L3VPN_VRF_TABLE, &L3vpnMplsL3VpnVrfEntry);
    if (pL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%VRF does not exist\r\n"));
        return L3VPN_FAILURE;
    }

    switch (pL3VpnVrfAdminEventInfo->u4EventType)
    {
        case L3VPN_VRF_ADMIN_STATUS_UP:
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Admin up event Received\r\n"));
            if (L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) == L3VPN_VRF_OPER_DOWN)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\r%%Oper is currently down... Try to make it up.\r\n"));
                if (L3VpnIsAnyInterfaceUpForVrf (pu1VrfName) == OSIX_TRUE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\r%%Interface UP found... Marking Oper UP.\r\n"));
                    L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) =
                        L3VPN_VRF_OPER_UP;

                    if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
                    {
                        (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                                L3VPN_BGP4_VRF_UP);
                    }
                    else
                    {
                        L3VPN_TRC ((L3VPN_MAIN_TRC,
                                    "\r%%Cannot send VRF up notification to BGP.. since Callback is NULL.\r\n"));
                    }
                    /*TODO: Give Trigger to BGP */
                }
                else
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\r%%Oper cannot be made up.. no interface found oper up.\r\n"));
                }
            }
            else
            {

                L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%VRF is already Oper Up.\r\n"));
            }
            break;

        case L3VPN_VRF_ADMIN_STATUS_DOWN:
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "\r%%Admin Down Received ....Checking Oper Status.\r\n"));
            if (L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) == L3VPN_VRF_OPER_UP)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%Marking Oper Down\r\n"));
                L3VPN_P_VRF_OPER_STATUS (pL3VpnVrfEntry) = L3VPN_VRF_OPER_DOWN;

                /*TODO: Give Trigger to BGP */
                if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB != NULL)
                {
                    (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB)) (u4ContextId,
                                                            L3VPN_BGP4_VRF_DOWN);
                }
                else
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\r%%Cannot send VRF up notification to BGP.. since Callback is NULL.\r\n"));
                }
            }
            if (L3VpnFlushRouteTableForVrf (pu1VrfName) == L3VPN_FAILURE)
            {
                L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                            "\r%%Error in deleting the RTE table routes.\r\n"));
                L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                            "Error in flushing RTE table for the VRF: %s\n",
                            pu1VrfName));
            }
            break;
        default:
            break;
    }
    return L3VPN_SUCCESS;
}

UINT4
L3VpnPrcoessVcmCrtDelEvent (tL3VpnVcmEventInfo * pL3VpnVcmEventInfo)
{

    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry = L3VpnGetVrfEntryByContextId
        (pL3VpnVcmEventInfo->u4ContextId);
    switch (pL3VpnVcmEventInfo->u1EventType)
    {
        case VCM_CONTEXT_CREATE:
            L3VPN_TRC ((L3VPN_MAIN_TRC, "Context Created in the VCM\n"));
            break;

        case VCM_CONTEXT_DELETE:
            if (pL3vpnMplsL3VpnVrfEntry == NULL)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "\r%%VRF does not exist in L3VPN.\r\n"));
                return L3VPN_SUCCESS;
            }

            if (L3vpnUtlProcessVrfDeletion (pL3vpnMplsL3VpnVrfEntry) ==
                L3VPN_FAILURE)
            {
                L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                            "\r%%Error in VRF Deletion.\r\n"));
                return L3VPN_FAILURE;
            }
            RBTreeRem (L3VPN_VRF_TABLE, pL3vpnMplsL3VpnVrfEntry);
            if (MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                    (UINT1 *) pL3vpnMplsL3VpnVrfEntry) ==
                MEM_FAILURE)
            {
                L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                            "\r%%Error in freeing MPLS VRF memory.\r\n"));
                return L3VPN_FAILURE;
            }
            break;
        default:
            break;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : MplsL3VpnBgp4LspUsageStatusUpdate                          *
*                                                                           *
* Description  : This API is called to inform MPLS whether the given LSP is *
*                  of interest to BGP or not                                  *
*                                                                           *
* Input        : u1Status: OSIX_TRUE- If BGP is interted to use the LSP,     *
*                           OSIX_FALSE- If BGP is not interested              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
UINT4
MplsL3VpnBgp4LspUsageStatusUpdate (UINT4 u4Prefix, UINT4 u4LspLabel,
                                   BOOL1 u1Status)
{

    UINT4               u4OutIfIndex = 0;

    UNUSED_PARAM (u4LspLabel);
    /* BGP is intersted in the LSP status */
    u4Prefix = OSIX_NTOHL (u4Prefix);

    if (MplsGetNonTeLspOutSegmentFromPrefix (u4Prefix, &u4OutIfIndex) ==
        MPLS_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "LSP not found for the prefix: %d\n",
                    u4Prefix));
        return OSIX_FAILURE;
    }

    if (u1Status == OSIX_TRUE)
    {
        if (MplsRegOrDeRegNonTeLspWithL3Vpn (u4Prefix, TRUE) == MPLS_FAILURE)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "Regsitraion failed for the prefix:%d\n", u4Prefix));
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (MplsRegOrDeRegNonTeLspWithL3Vpn (u4Prefix, FALSE) == MPLS_FAILURE)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "De-Registration failed for the prefix:%d\n",
                        u4Prefix));
            return OSIX_FAILURE;
        }
    }
    L3VPN_TRC ((L3VPN_UTIL_TRC, "Regsitration Successful for the Prefix: %d\n",
                u4Prefix));
    return OSIX_SUCCESS;
}

INT4
L3VpnTrieAddEntry (VOID *pInputParams, VOID *pOutputParams,
                   VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return SUCCESS;
}

INT4
L3VpnTrieDeleteEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                      VOID *pOutputParams, VOID *pNextHop, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (pNextHop);
    UNUSED_PARAM (Key);
    (*ppAppSpecInfo) = NULL;
    return SUCCESS;
}

INT4
L3VpnTrieSearchEntry (tInputParams * pInputParams,
                      tOutputParams * pOutputParams, VOID *pAppSpecInfo)
{
    UNUSED_PARAM (pInputParams);
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

INT4
L3VpnTrieLookUpEntry (tInputParams * pInputParams,
                      tOutputParams * pOutputParams,
                      VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (key);

    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3VpnTrieLookUpEntry\n"));
    return SUCCESS;
}

UINT1              *
L3VpnTrieDelAllEntry (tInputParams * pInputParams, VOID *dummy,
                      tOutputParams * pOutputParams)
{
    UINT1              *pInpFtnInfo;
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (dummy);
    pInpFtnInfo = (UINT1 *) pInputParams;
    return (pInpFtnInfo);
}

VOID
L3VpnTrieDeInit (VOID *pDelParams)
{
    UNUSED_PARAM (pDelParams);
    return;
}

INT4
L3VpnTrieDelEntry (VOID *pInRtInfo, VOID **ppAppPtr, tKey key)
{
    UNUSED_PARAM (pInRtInfo);
    UNUSED_PARAM (ppAppPtr);
    UNUSED_PARAM (key);
    return TRIE_SUCCESS;
}

INT4
L3VpnTrieBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                    VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);

    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

VOID
L3VpnFtnTrieCbDelete (VOID *pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

UINT4
L3VpnRegisterRteEntryWithTrie (UINT4 u4ContextId,
                               tL3vpnMplsL3VpnVrfRteEntry * pRteEntry)
{

    tInputParams        L3VpnRteInputParams;
    tOutputParams       L3VpnRteOutputParams;
    UINT4               au4Indx[2];

    MEMCPY (&au4Indx[0], L3VPN_P_RTE_INET_CIDR_DEST (pRteEntry),
            L3VPN_BGP_MAX_ADDRESS_LEN);
    /*au4Indx[0] = OSIX_HTONL ( au4Indx[0]); */

    /*au4Indx[1] = OSIX_HTONL (0xFFFFFFFF); */
    au4Indx[1] =
        L3VpnGetSubnetMaskFromPrefixLen (L3VPN_P_RTE_INET_CIDR_PREFIX_LEN
                                         (pRteEntry));

    au4Indx[1] = OSIX_HTONL (au4Indx[1]);
    L3VpnRteInputParams.pLeafNode = NULL;
    L3VpnRteInputParams.u1PrefixLen =
        (UINT1) L3VPN_P_RTE_INET_CIDR_PREFIX_LEN (pRteEntry);
    L3VpnRteInputParams.pRoot = gL3vpnGlobals.gpL3VpnTrieRoot[u4ContextId];
    L3VpnRteInputParams.i1AppId = MPLS_L3VPN_APPL_ID;
    L3VpnRteInputParams.Key.pKey = (UINT1 *) au4Indx;

    L3VpnRteOutputParams.pAppSpecInfo = NULL;
    L3VpnRteOutputParams.Key.pKey = NULL;
    if ((TrieAdd (&L3VpnRteInputParams, pRteEntry,
                  &L3VpnRteOutputParams)) == TRIE_FAILURE)

    {
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

tL3vpnMplsL3VpnVrfRteEntry *
L3VpnGetRteTableEntryFromTrie (UINT4 u4ContextId, UINT4 u4Address)
{

    tInputParams        L3VpnRteInputParams;
    tOutputParams       L3VpnRteOutputParams;
    tL3vpnMplsL3VpnVrfRteEntry *pRteEntry = NULL;
    UINT4               au4InTrieKey[2];
    VOID               *pTemp = NULL;
    /* au4InTrieKey[0] = OSIX_HTONL (u4Address); */
    au4InTrieKey[0] = u4Address;
    au4InTrieKey[1] = OSIX_HTONL (0xFFFFFFFF);
    L3VpnRteInputParams.pRoot = gL3vpnGlobals.gpL3VpnTrieRoot[u4ContextId];
    L3VpnRteInputParams.pLeafNode = NULL;
    L3VpnRteInputParams.i1AppId = MPLS_L3VPN_APPL_ID;
    L3VpnRteInputParams.u1PrefixLen = 0;
    L3VpnRteInputParams.Key.pKey = (UINT1 *) au4InTrieKey;

    L3VpnRteOutputParams.Key.pKey = NULL;
    L3VpnRteOutputParams.pAppSpecInfo = NULL;

    if ((TrieSearch (&L3VpnRteInputParams, &L3VpnRteOutputParams,
                     (VOID **) &pTemp)) == TRIE_FAILURE)
    {
        return NULL;
    }
    pRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *) L3VpnRteOutputParams.pAppSpecInfo;
    return pRteEntry;

}

tL3vpnMplsL3VpnVrfRteEntry *
L3VpnGetBestMatchRteTableEntryFromTrie (UINT4 u4ContextId, UINT4 u4Address)
{

    tInputParams        L3VpnRteInputParams;
    tOutputParams       L3VpnRteOutputParams;
    tL3vpnMplsL3VpnVrfRteEntry *pRteEntry = NULL;
    UINT4               au4InTrieKey[2];
    VOID               *pTemp = NULL;

    MEMSET (au4InTrieKey, 0, 2 * sizeof (UINT4));
    /*au4InTrieKey[0] = OSIX_HTONL (u4Address); */
    au4InTrieKey[0] = u4Address;
    au4InTrieKey[1] = OSIX_HTONL (0xFFFFFFFF);
    L3VpnRteInputParams.pRoot = gL3vpnGlobals.gpL3VpnTrieRoot[u4ContextId];
    L3VpnRteInputParams.pLeafNode = NULL;
    L3VpnRteInputParams.i1AppId = MPLS_L3VPN_APPL_ID;
    L3VpnRteInputParams.u1PrefixLen = 0;
    L3VpnRteInputParams.Key.pKey = (UINT1 *) au4InTrieKey;

    L3VpnRteOutputParams.Key.pKey = NULL;
    L3VpnRteOutputParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&L3VpnRteInputParams, &L3VpnRteOutputParams,
                     (VOID **) &pTemp)) == TRIE_FAILURE)
    {
        return NULL;
    }

    pRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *) L3VpnRteOutputParams.pAppSpecInfo;
    return pRteEntry;

}

UINT4
L3VpnGetSubnetMaskFromPrefixLen (UINT4 u4PrefixLen)
{
    UINT4               u4SubnetMask = 0;
    if (u4PrefixLen != 0)
    {
        u4SubnetMask =
            (L3VPN_HOST_MASK << (L3VPN_MAX_PREFIX_LEN - u4PrefixLen));
    }
    return u4SubnetMask;
}

UINT4
L3VpnDeleteRteTableEntryFromTrie (UINT4 u4ContextId,
                                  tL3vpnMplsL3VpnVrfRteEntry * pRteEntry)
{
    tInputParams        L3VpnRteInputParams;
    tOutputParams       L3VpnRteOutputParams;
    UINT4               au4InTrieKey[2];
    VOID               *pTemp = NULL;
    MEMSET (au4InTrieKey, 0, 2 * sizeof (UINT4));
    MEMCPY (&au4InTrieKey[0], L3VPN_P_RTE_INET_CIDR_DEST (pRteEntry),
            L3VPN_BGP_MAX_ADDRESS_LEN);
    /*au4InTrieKey[0] = OSIX_HTONL (au4InTrieKey[0]); */
    /*au4InTrieKey[1] = OSIX_HTONL (0xFFFFFFFF); */

    au4InTrieKey[1] =
        L3VpnGetSubnetMaskFromPrefixLen (L3VPN_P_RTE_INET_CIDR_PREFIX_LEN
                                         (pRteEntry));
    au4InTrieKey[1] = OSIX_HTONL (au4InTrieKey[1]);
    L3VpnRteInputParams.pLeafNode = NULL;
    L3VpnRteInputParams.u1PrefixLen =
        (UINT1) L3VPN_P_RTE_INET_CIDR_PREFIX_LEN (pRteEntry);
    L3VpnRteInputParams.pRoot = gL3vpnGlobals.gpL3VpnTrieRoot[u4ContextId];
    L3VpnRteInputParams.i1AppId = MPLS_L3VPN_APPL_ID;
    L3VpnRteInputParams.Key.pKey = (UINT1 *) au4InTrieKey;

    L3VpnRteOutputParams.Key.pKey = NULL;
    L3VpnRteOutputParams.pAppSpecInfo = NULL;

    if (TrieRemove (&L3VpnRteInputParams, &L3VpnRteOutputParams, pTemp) ==
        TRIE_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Failed to remove Rte entry from TRIE for the context: %d\n",
                    u4ContextId));
        return L3VPN_FAILURE;
    }
    return L3VPN_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : L3VPnInitMibScalar                                         *
*                                                                           *
* Description  : L3VPN initialization for mib Scalar Objects                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
L3VPnInitMibScalar (VOID)
{
    L3VPN_MAX_POSSIBLE_RTS = L3VPN_MAX_POSSIBLE_ROUTES;
    L3VPN_NOTIFICATION_ENABLE = L3VPN_TRAP_DISABLE;
}

UINT4
L3VpnSendTrapForVrfStatus (UINT1 *pu1VrfName, UINT1 u1Status, INT4 i4IfIndex)
{

#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pIfConfRowStatusOid =
        NULL, *pL3VpnVrfOperStatusOid = NULL;

    UINT4               u4Count = 0;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    INT4                i4IfConfRowStatus = 0;
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    UINT4               u4VrfNameLen = 0;
    UINT4               u4IfConfOidLen = 0;
    UINT4               u4L3VpnOperStOidLen = 0;

    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4L3VpnVrfOperNotify[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 11, 0, 0 };

    UINT4               au4IfConfRowStatus[L3VPN_MAX_IF_CONF_OID_LEN] = { 1, 3, 6, 1, 2, 1, 10, 166, 11, 1, 2, 1, 1, 5, 0 };    /* 14 + vrfNameLen + ifindex + vrfname */

    UINT4               au4L3VpnVrfOperStatus[L3VPN_MAX_VRF_OID_LEN] = { 1, 3, 6, 1, 2, 1, 10, 166, 11, 1, 2, 2, 1, 6, 0 };    /* 14 + vrfNameLen + vrfname */

    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    INT4                i4OperStatus = 0;
    if (pu1VrfName == NULL)
    {
        return L3VPN_FAILURE;
    }

    u4VrfNameLen = (UINT4) STRLEN (pu1VrfName);

    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMCPY (au1VrfName, pu1VrfName, u4VrfNameLen);

    u4IfConfOidLen = L3VPN_IF_CONF_BASE_OID_LEN + u4VrfNameLen + 2;
    u4L3VpnOperStOidLen = L3VPN_VRF_BASE_OID_LEN + u4VrfNameLen + 1;

    pL3vpnMplsL3VpnIfConfEntry = L3VpnGetIfConfEntry (au1VrfName, i4IfIndex);
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "\r%%Cannot Send Trap for VRF status. If conf Entry not found.\r\n"));
        return L3VPN_FAILURE;
    }
    i4IfConfRowStatus = L3VPN_P_IF_CONF_ROW_STATUS (pL3vpnMplsL3VpnIfConfEntry);

    au4IfConfRowStatus[L3VPN_IF_CONF_BASE_OID_LEN] = u4VrfNameLen;
    for (u4Count = 0; u4Count < u4VrfNameLen; u4Count++)
    {
        au4IfConfRowStatus[L3VPN_IF_CONF_BASE_OID_LEN + u4Count + 1] =
            au1VrfName[u4Count];
    }
    au4IfConfRowStatus[u4IfConfOidLen - 1] = (UINT4) i4IfIndex;

    au4L3VpnVrfOperStatus[L3VPN_VRF_BASE_OID_LEN] = u4VrfNameLen;
    for (u4Count = 0; u4Count < u4VrfNameLen; u4Count++)
    {
        au4L3VpnVrfOperStatus[L3VPN_IF_CONF_BASE_OID_LEN + u4Count + 1] =
            au1VrfName[u4Count];
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {

        return L3VPN_FAILURE;
    }

    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4L3VpnVrfOperNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return L3VPN_FAILURE;
    }

    au4L3VpnVrfOperNotify[(sizeof (au4L3VpnVrfOperNotify) / sizeof (UINT4)) -
                          1] = u1Status;

    MEMCPY (pOidValue->pu4_OidList, au4L3VpnVrfOperNotify,
            sizeof (au4L3VpnVrfOperNotify));
    pOidValue->u4_Length = sizeof (au4L3VpnVrfOperNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));

    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pVbList is NULL.\r\n"));
        return L3VPN_FAILURE;
    }

    pStartVb = pVbList;

    pIfConfRowStatusOid = alloc_oid ((INT4) u4IfConfOidLen);
    if (pIfConfRowStatusOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pIfConfRowStatusOid is NULL.\r\n"));
        return L3VPN_FAILURE;
    }

    MEMCPY (pIfConfRowStatusOid->pu4_OidList, au4IfConfRowStatus,
            sizeof (au4IfConfRowStatus));
    pIfConfRowStatusOid->u4_Length = u4IfConfOidLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pIfConfRowStatusOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  i4IfConfRowStatus,
                                                  NULL, NULL, SnmpCnt64Type));

    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pIfConfRowStatusOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pNextVarBind is NULL.\r\n"));
        return L3VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    pL3VpnVrfOperStatusOid = alloc_oid ((INT4) u4L3VpnOperStOidLen);

    if (pL3VpnVrfOperStatusOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pIfConfRowStatusOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pL3VpnVrfOperStatusOid is NULL.\r\n"));
        return L3VPN_FAILURE;
    }

    MEMCPY (pL3VpnVrfOperStatusOid->pu4_OidList, au4L3VpnVrfOperStatus,
            sizeof (au4L3VpnVrfOperStatus));
    pL3VpnVrfOperStatusOid->u4_Length = u4L3VpnOperStOidLen;

    if (u1Status == L3VPN_VRF_UP_TRAP)
    {
        i4OperStatus = L3VPN_VRF_OPER_UP;
    }
    else
    {
        i4OperStatus = L3VPN_VRF_OPER_DOWN;
    }

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pL3VpnVrfOperStatusOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  i4OperStatus,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pIfConfRowStatusOid);
        free_oid (pL3VpnVrfOperStatusOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pNextVarBind NULL.\r\n"));
        return L3VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pu1VrfName);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (i4IfIndex);
#endif
    return (L3VPN_SUCCESS);
}

UINT4
L3VpnSendTrapForRouteThreshold (UINT1 *pu1VrfName, UINT1 u1TrapType)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue =
        NULL, *pL3VpnPerfCurrNumRoutesOid = NULL, *pL3VpnVrfMidRteThreshOid =
        NULL, *pL3VpnVrfHighRteThreshOid = NULL;

    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    UINT4               u4Count = 0;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;

    INT4                i4VrfNameLen = 0;
    UINT4               u4PerfCurrNumRoutesOidLen = 0;
    UINT4               u4L3VpnMidThrOidLen = 0;
    UINT4               u4L3VpnHighThrOidLen = 0;

    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4L3VpnVrfRteThreshNotify[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 11, 0, 0 };

    UINT4               au4VrfPerfCumNumRoutes[L3VPN_MAX_VRF_PERF_OID_LEN] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 11, 1, 3, 1, 1, 3 };

    UINT4               au4L3VpnMidRteThreshold[L3VPN_MAX_VRF_OID_LEN] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 11, 1, 2, 2, 1, 9 };

    UINT4               au4L3VpnHighRteThreshold[L3VPN_MAX_VRF_OID_LEN] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 11, 1, 2, 2, 1, 10 };

    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    UINT4               u4MidRteThreshold = 0;
    UINT4               u4HighRteThreshold = 0;
    UINT4               u4PerCurrNumRoutes = 0;
    memset (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));
    if (pu1VrfName == NULL)
    {
        return L3VPN_FAILURE;
    }

    i4VrfNameLen = (INT4) STRLEN (pu1VrfName);

    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMCPY (au1VrfName, pu1VrfName, i4VrfNameLen);

    u4L3VpnMidThrOidLen =
        (UINT4) (L3VPN_VRF_BASE_OID_LEN + (UINT4) i4VrfNameLen + 1);
    u4L3VpnHighThrOidLen =
        (UINT4) (L3VPN_VRF_BASE_OID_LEN + (UINT4) i4VrfNameLen + 1);
    u4PerfCurrNumRoutesOidLen =
        (UINT4) (L3VPN_VRF_PERF_BASE_OID_LEN + (UINT4) i4VrfNameLen + 1);

    pL3vpnMplsL3VpnVrfEntry =
        MplsL3VpnApiGetVrfTableEntry (au1VrfName, i4VrfNameLen);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "VRF entry is NULL for the VRF:%s\n",
                    pu1VrfName));
        return L3VPN_FAILURE;
    }

    pL3vpnMplsL3VpnVrfPerfEntry = L3VpnGetVrfPerfTableEntry (au1VrfName);

    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "Perf Entry is NULL for the VRF: %s\n",
                    pu1VrfName));
        return L3VPN_FAILURE;
    }

    u4MidRteThreshold = L3VPN_P_VRF_MID_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry);
    u4HighRteThreshold = L3VPN_P_VRF_HIGH_ROUTE_TH (pL3vpnMplsL3VpnVrfEntry);
    u4PerCurrNumRoutes = L3VPN_P_PERF_CURR_ROUTES (pL3vpnMplsL3VpnVrfPerfEntry);

    au4VrfPerfCumNumRoutes[L3VPN_VRF_PERF_BASE_OID_LEN] = (UINT4) i4VrfNameLen;
    for (u4Count = 0; u4Count < (UINT4) i4VrfNameLen; u4Count++)
    {
        au4VrfPerfCumNumRoutes[L3VPN_VRF_PERF_BASE_OID_LEN + u4Count + 1] =
            au1VrfName[u4Count];
    }

    au4L3VpnMidRteThreshold[L3VPN_VRF_BASE_OID_LEN] = (UINT4) i4VrfNameLen;
    for (u4Count = 0; u4Count < (UINT4) i4VrfNameLen; u4Count++)
    {
        au4L3VpnMidRteThreshold[L3VPN_VRF_BASE_OID_LEN + u4Count + 1] =
            au1VrfName[u4Count];
    }

    au4L3VpnHighRteThreshold[L3VPN_VRF_BASE_OID_LEN] = (UINT4) i4VrfNameLen;
    for (u4Count = 0; u4Count < (UINT4) i4VrfNameLen; u4Count++)
    {
        au4L3VpnHighRteThreshold[L3VPN_VRF_BASE_OID_LEN + u4Count + 1] =
            au1VrfName[u4Count];
    }

    au4L3VpnVrfRteThreshNotify[sizeof (au4L3VpnVrfRteThreshNotify) /
                               sizeof (UINT4) - 1] = u1TrapType;
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return L3VPN_FAILURE;
    }

    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue =
        alloc_oid (sizeof (au4L3VpnVrfRteThreshNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return L3VPN_FAILURE;
    }
    MEMCPY (pOidValue->pu4_OidList, au4L3VpnVrfRteThreshNotify,
            sizeof (au4L3VpnVrfRteThreshNotify));
    pOidValue->u4_Length = sizeof (au4L3VpnVrfRteThreshNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));

    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return L3VPN_FAILURE;
    }

    pStartVb = pVbList;

    pL3VpnPerfCurrNumRoutesOid = alloc_oid ((INT4) u4PerfCurrNumRoutesOidLen);
    if (pL3VpnPerfCurrNumRoutesOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return L3VPN_FAILURE;
    }

    MEMCPY (pL3VpnPerfCurrNumRoutesOid->pu4_OidList, au4VrfPerfCumNumRoutes,
            sizeof (au4VrfPerfCumNumRoutes));
    pL3VpnPerfCurrNumRoutesOid->u4_Length = u4PerfCurrNumRoutesOidLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pL3VpnPerfCurrNumRoutesOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u4PerCurrNumRoutes,
                                                  NULL, NULL, SnmpCnt64Type));

    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pL3VpnPerfCurrNumRoutesOid);
        SNMP_free_snmp_vb_list (pStartVb);
        L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pNextVarBind NULL\r\n"));
        return L3VPN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    if (u1TrapType == L3VPN_VRF_MID_THRESHOLD_EXCEEDED)
    {
        pL3VpnVrfMidRteThreshOid = alloc_oid ((INT4) u4L3VpnMidThrOidLen);
        if (pL3VpnVrfMidRteThreshOid == NULL)
        {
            free_oid (pOid);
            free_oid (pOidValue);
            free_oid (pL3VpnPerfCurrNumRoutesOid);
            SNMP_free_snmp_vb_list (pStartVb);
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "\r%%pL3VpnVrfMidRteThreshOid NULL.\r\n"));
            return L3VPN_FAILURE;
        }
        MEMCPY (pL3VpnVrfMidRteThreshOid->pu4_OidList, au4L3VpnMidRteThreshold,
                sizeof (au4L3VpnMidRteThreshold));
        pL3VpnVrfMidRteThreshOid->u4_Length = u4L3VpnMidThrOidLen;

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *)
             SNMP_AGT_FormVarBind (pL3VpnVrfMidRteThreshOid,
                                   SNMP_DATA_TYPE_INTEGER, 0,
                                   (INT4) u4MidRteThreshold, NULL, NULL,
                                   SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            free_oid (pOidValue);
            free_oid (pL3VpnPerfCurrNumRoutesOid);
            free_oid (pL3VpnVrfMidRteThreshOid);
            SNMP_free_snmp_vb_list (pStartVb);
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pNextVarBind NULL.\r\n"));
            return L3VPN_FAILURE;
        }

    }
    if ((u1TrapType == L3VPN_VRF_MAX_THRESHOLD_EXCEEDED) ||
        (u1TrapType == L3VPN_VRF_MAX_THRESHOLD_CLEARED))
    {

        pL3VpnVrfHighRteThreshOid = alloc_oid ((INT4) u4L3VpnHighThrOidLen);
        if (pL3VpnVrfHighRteThreshOid == NULL)
        {
            free_oid (pOid);
            free_oid (pOidValue);
            free_oid (pL3VpnPerfCurrNumRoutesOid);
            SNMP_free_snmp_vb_list (pStartVb);
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "\r%%pL3VpnVrfHighRteThreshOid NULL.\r\n"));
            return L3VPN_FAILURE;
        }

        MEMCPY (pL3VpnVrfHighRteThreshOid->pu4_OidList,
                au4L3VpnHighRteThreshold, sizeof (au4L3VpnHighRteThreshold));
        pL3VpnVrfHighRteThreshOid->u4_Length = u4L3VpnHighThrOidLen;

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pL3VpnVrfHighRteThreshOid,
                                                      SNMP_DATA_TYPE_INTEGER,
                                                      0,
                                                      (INT4) u4HighRteThreshold,
                                                      NULL, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            free_oid (pOidValue);
            free_oid (pL3VpnPerfCurrNumRoutesOid);
            free_oid (pL3VpnVrfHighRteThreshOid);
            SNMP_free_snmp_vb_list (pStartVb);
            L3VPN_TRC ((L3VPN_MAIN_TRC, "\r%%pNextVarBind NULL\r\n"));
            return L3VPN_FAILURE;
        }
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pu1VrfName);
    UNUSED_PARAM (u1TrapType);
#endif
    return L3VPN_SUCCESS;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpnmain.c                     */
/*-----------------------------------------------------------------------*/
