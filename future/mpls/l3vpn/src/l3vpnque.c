/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnque.c,v 1.2 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "l3vpninc.h"

/****************************************************************************
*                                                                           *
* Function     : L3vpnQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
L3vpnQueProcessMsgs ()
{
    tL3VpnQMsg         *pL3vpnQueMsg = NULL;
    tL3VpnVcmIfEvtInfo *pL3VpnVcmIfEvntInfo = NULL;
    tL3VpnIfEvtInfo    *pL3VpnIfEvtInfo = NULL;
    tL3VpnNonTeLspEventInfo *pL3VpnNonTeLspEventInfo = NULL;
    tL3VpnVrfAdminEventInfo *pL3VpnVrfAdminEventInfo = NULL;
    tL3VpnVcmEventInfo *pL3VpnVcmEventInfo = NULL;
    tL3VpnRsvpTeLspEventInfo *pL3VpnRsvpTeEventInfo = NULL;

    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC, "FUNC:L3vpnQueProcessMsgs\n"));

    while (OsixQueRecv (gL3vpnGlobals.l3vpnQueId,
                        (UINT1 *) &pL3vpnQueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
        switch (pL3vpnQueMsg->u4MsgType)
        {

            case L3VPN_VCM_IF_MAP_EVENT:
                pL3VpnVcmIfEvntInfo =
                    (tL3VpnVcmIfEvtInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnProcessVcmIfMapEvent (pL3VpnVcmIfEvntInfo);
                break;

            case L3VPN_VCM_IF_UNMAP_EVENT:
                pL3VpnVcmIfEvntInfo =
                    (tL3VpnVcmIfEvtInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnProcessVcmIfUnmapEvent (pL3VpnVcmIfEvntInfo);
                break;

            case L3VPN_IF_STATUS_CHANGE_EVENT:
                pL3VpnIfEvtInfo =
                    (tL3VpnIfEvtInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnProcessIfEvent (pL3VpnIfEvtInfo);
                break;

            case L3VPM_MPLS_LSP_STATE_CHANGE_EVENT:
                pL3VpnNonTeLspEventInfo =
                    (tL3VpnNonTeLspEventInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnProcessNonTeLspEvent (pL3VpnNonTeLspEventInfo);
                break;

            case L3VPN_VRF_ADMIN_EVENT:
                pL3VpnVrfAdminEventInfo =
                    (tL3VpnVrfAdminEventInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnProcessVrfAdminEvent (pL3VpnVrfAdminEventInfo);
                break;

            case L3VPN_VRF_VCM_CREATE_DEL_EVENT:
                pL3VpnVcmEventInfo =
                    (tL3VpnVcmEventInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnPrcoessVcmCrtDelEvent (pL3VpnVcmEventInfo);
                break;

            case L3VPN_RSVPTE_LSP_STATE_CHANGE_EVENT:
                pL3VpnRsvpTeEventInfo =
                    (tL3VpnRsvpTeLspEventInfo *) & (pL3vpnQueMsg->L3VpnEvtInfo);
                L3VpnProcessRsvpTeLspEvent (pL3VpnRsvpTeEventInfo);
                break;

            default:
                break;
        }
        if (MemReleaseMemBlock (L3VPN_Q_MSG_POOL_ID, (UINT1 *) pL3vpnQueMsg)
            == MEM_FAILURE)
        {
            L3VPN_TRC ((L3VPN_MAIN_TRC, "Error in Memory Free\n"));
        }

    }
    L3VPN_TRC_FUNC ((L3VPN_FN_ENTRY_EXIT_TRC,
                     "FUNC:Exit L3vpnQueProcessMsgs\n"));

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  l3vpnque.c                      */
/*-----------------------------------------------------------------------*/
