/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnbgp.c,v 1.4 2018/01/03 11:31:20 siva Exp $
 *
 ********************************************************************/

#include "l3vpninc.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MplsCliParseAndGenerateRdRt                       */
/*                                                                           */
/*     DESCRIPTION      : This function takes random string as input and     */
/*                        generate valid RD/RT Value                         */
/*     INPUT            : CliHandle - Cli handle                             */
/*                        pu1RandomString - Random strong (ASN:nn or IP:nn)  */
/*     OUTPUT           : pu1RdRt - Generated Rd Value                       */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

/*Note -  Since code is not integrated yet with BGP, L3vpnMainTaskLock() is commented
 *        Once It is integrated, L3vpnMainTaskLock() should ne uncommented */

tL3vpnMplsL3VpnVrfEntry gL3vpnMplsL3VpnVrfEntry;
tL3vpnMplsL3VpnVrfRteEntry gL3vpnMplsBgpVrfRteEntry;

/****************************************************************************/
/* Function Name   : MplsL3VpnRegisterCallback                              */
/* Description     : This function is used by BGP to register it's callback */
/*                   functions with L3VPN/MPLS                              */
/* Input (s)       : pRegInfo - Pointer to tMplsL3VpnRegInfo, with          */
/*                   u2InfoMask and respective function pointer updated     */
/* Output (s)      :                                                        */
/* Returns         : If successful, returns OSIX_SUCCESS. Else returns      */
/*                   OSIX_FAILURE                                           */
/****************************************************************************/
UINT4
MplsL3VpnRegisterCallback (tMplsL3VpnRegInfo * pRegInfo)
{
    UINT2               u2InfoMask = 0;

    if (pRegInfo == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "Registration object is NULL. Registration failed.\r\n"));
        return OSIX_FAILURE;
    }
    u2InfoMask = pRegInfo->u2InfoMask;
    if (u2InfoMask & L3VPN_BGP4_ROUTE_PARAMS_REQ)
    {
        L3VPN_NOTIFY_ROUTE_PARAMS_CB =
            pRegInfo->pBgp4MplsL3VpnNotifyRouteParams;
    }
    if (u2InfoMask & L3VPN_BGP4_LSP_STATUS_REQ)
    {
        L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB =
            pRegInfo->pBgp4MplsL3VpnNotifyLspStatusChange;
    }

    if (u2InfoMask & L3VPN_BGP4_VRF_ADMIN_STATUS_REQ)
    {
        L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB =
            pRegInfo->pBgp4MplsL3vpnNotifyVrfAdminStatusChange;

    }

    return OSIX_SUCCESS;
}

UINT4
MplsL3VpnDeRegisterCallback (tMplsL3VpnRegInfo * pRegInfo)
{
    UINT2               u2InfoMask = 0;

    if (pRegInfo == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "pRegInfo object is NULL. De-Registration failed.\r\n"));
        return OSIX_FAILURE;
    }

    u2InfoMask = pRegInfo->u2InfoMask;

    if (u2InfoMask & L3VPN_BGP4_ROUTE_PARAMS_REQ)
    {
        L3VPN_NOTIFY_ROUTE_PARAMS_CB = NULL;
    }

    if (u2InfoMask & L3VPN_BGP4_LSP_STATUS_REQ)
    {
        L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB = NULL;
    }

    if (u2InfoMask & L3VPN_BGP4_VRF_ADMIN_STATUS_REQ)
    {
        L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB = NULL;

    }

    return OSIX_SUCCESS;
}

UINT4
L3VpnBgp4RouteAddDelAtEgress (tMplsL3VpnBgp4RouteInfo * pMplsL3VpnBgp4RouteInfo)
{
    UINT4               u4RetVal = OSIX_FAILURE;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    /**l3vpn_mpls_note_1 : Take L3VPN Lock **/
    /**l3vpn_mpls_note_1 : Release L3VPN Lock **/

    if (pMplsL3VpnBgp4RouteInfo == NULL)
    {
        return u4RetVal;
    }

    if (L3VPN_BGP4_ROUTE_ADD == pMplsL3VpnBgp4RouteInfo->u1ILMAction)
    {
        /**** 1. Make the entry in the local DB ****/
        if (L3VpnAddBgpRouteLabelTable (pMplsL3VpnBgp4RouteInfo) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) =
            pMplsL3VpnBgp4RouteInfo->u4Label;

        pL3VpnBgpRouteLabelEntry =
            L3vpnGetBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

        if (pL3VpnBgpRouteLabelEntry == NULL)
        {
            return OSIX_FAILURE;
        }
        /**** 2. Call the NPAPI to program     ****/
        if (MplsL3vpnEgressMap (pL3VpnBgpRouteLabelEntry) != L3VPN_SUCCESS)
        {
            if (pL3VpnBgpRouteLabelEntry->u1ArpResolveStatus !=
                MPLS_ARP_RESOLVE_WAITING)
            {
                if (L3VpnDelBgpRouteLabelTable (pMplsL3VpnBgp4RouteInfo) !=
                    OSIX_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "\n%s: Route Programming failed, could not enque either.\r\n",
                                __FUNCTION__));
                    return OSIX_FAILURE;
                }
            }
            L3VPN_TRC ((L3VPN_MAIN_TRC,
                        "\n%s: Route Programming failed, Enqueued.\r\n",
                        __FUNCTION__));

            /*        return OSIX_FAILURE; */
        }
    }
    else if (L3VPN_BGP4_ROUTE_DEL == pMplsL3VpnBgp4RouteInfo->u1ILMAction)
    {

        MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) =
            pMplsL3VpnBgp4RouteInfo->u4Label;

        pL3VpnBgpRouteLabelEntry =
            L3vpnGetBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

        /***1. Call the NPAPI for deletion **/

        if (MplsL3vpnEgressUnMap (pL3VpnBgpRouteLabelEntry) != L3VPN_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        /***2. Delete the entry in local DB **/
        if (L3VpnDelBgpRouteLabelTable (pMplsL3VpnBgp4RouteInfo) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

UINT4
L3VpnAddBgpRouteLabelTable (tMplsL3VpnBgp4RouteInfo * pMplsL3VpnBgp4RouteInfo)
{

    UINT4               u4RetVal = OSIX_SUCCESS;
    INT4                i4VrfNameLen = 0;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    if (pMplsL3VpnBgp4RouteInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    pL3VpnBgpRouteLabelEntry =
        (tL3VpnBgpRouteLabelEntry *)
        MemAllocMemBlk (L3VPN_BGPROUTELABELTABLE_POOLID);

    if (pL3VpnBgpRouteLabelEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pL3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

    /** Update the Egrees Route Info **/
    L3VPN_P_BGPROUTELABEL_LABEL (pL3VpnBgpRouteLabelEntry) =
        pMplsL3VpnBgp4RouteInfo->u4Label;
    L3VPN_P_BGPROUTELABEL_POLICY (pL3VpnBgpRouteLabelEntry) =
        pMplsL3VpnBgp4RouteInfo->u1LabelAllocPolicy;

    switch (pMplsL3VpnBgp4RouteInfo->u1LabelAllocPolicy)
    {
        case L3VPN_BGP4_POLICY_PER_VRF:
            L3VPN_P_BGPROUTELABELVRF_VRFID (pL3VpnBgpRouteLabelEntry) =
                pMplsL3VpnBgp4RouteInfo->u4VrfId;
            if (VcmGetAliasName (pMplsL3VpnBgp4RouteInfo->u4VrfId, au1VrfName)
                == VCM_FAILURE)
            {
                L3VPN_TRC ((L3VPN_MAIN_TRC,
                            "Failed to get the VRF Alias for the Context: %d.\r\n",
                            pMplsL3VpnBgp4RouteInfo->u4VrfId));
                return OSIX_FAILURE;
            }
            i4VrfNameLen = (INT4) STRLEN (au1VrfName);
            MEMCPY (L3VPN_P_BGPROUTELABELVRF_VNAME (pL3VpnBgpRouteLabelEntry),
                    au1VrfName, i4VrfNameLen);

            /** Here update the VRF Name **/
            break;
        case L3VPN_BGP4_POLICY_PER_ROUTE:
            L3VPN_P_BGPROUTELABELRTE_CXTID (pL3VpnBgpRouteLabelEntry) =
                pMplsL3VpnBgp4RouteInfo->u4VrfId;
            MEMCPY (&L3VPN_P_BGPROUTELABELRTE_NXTHOP (pL3VpnBgpRouteLabelEntry),
                    &pMplsL3VpnBgp4RouteInfo->NextHop,
                    sizeof (tMplsL3VpnBgp4AddrPrefix));
            L3VPN_P_BGPROUTELABELRTE_IF_IDX (pL3VpnBgpRouteLabelEntry) =
                pMplsL3VpnBgp4RouteInfo->u4IfIndex;
            /** Here update the VRF Name **/
            /** Here resolve the dst mac for the nxtHop**/
            break;
        default:
            MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                                (UINT1 *) pL3VpnBgpRouteLabelEntry);
            return OSIX_FAILURE;
            break;
    };

    if (RBTreeAdd (L3VPN_BGP_ROUTE_LABEL_TABLE,
                   (tRBElem *) pL3VpnBgpRouteLabelEntry) != RB_SUCCESS)
    {
        MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                            (UINT1 *) pL3VpnBgpRouteLabelEntry);
        return OSIX_FAILURE;
    }
    return u4RetVal;
}

UINT4
L3VpnDelBgpRouteLabelTable (tMplsL3VpnBgp4RouteInfo * pMplsL3VpnBgp4RouteInfo)
{
    if (pMplsL3VpnBgp4RouteInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (L3VpnDeleteBgpRouteLabelTable (pMplsL3VpnBgp4RouteInfo->u4Label) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

tL3VpnBgpRouteLabelEntry *
L3vpnGetBgpRouteLabelEntry (UINT4 u4Label)
{
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;

    MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

    L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) = u4Label;

    pL3VpnBgpRouteLabelEntry =
        L3vpnGetBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

    return pL3VpnBgpRouteLabelEntry;
}

UINT4
L3VpnDeleteBgpRouteLabelTable (UINT4 u4Label)
{
    UINT4               u4RetVal = OSIX_SUCCESS;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;

    MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

    L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) = u4Label;

    pL3VpnBgpRouteLabelEntry =
        L3vpnGetBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);

    if (pL3VpnBgpRouteLabelEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    RBTreeRem (L3VPN_BGP_ROUTE_LABEL_TABLE, pL3VpnBgpRouteLabelEntry);

    MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                        (UINT1 *) pL3VpnBgpRouteLabelEntry);

    return u4RetVal;
}

UINT4
L3VpnClearBgpRouteLabelTable (VOID)
{
    tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
    tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

    MEMSET (&L3VpnBgpRouteLabelEntry, 0, sizeof (tL3VpnBgpRouteLabelEntry));

    while (OSIX_TRUE)
    {
        pL3VpnBgpRouteLabelEntry =
            L3vpnGetNextBgpRouteLabelTable (&L3VpnBgpRouteLabelEntry);
        if (pL3VpnBgpRouteLabelEntry != NULL)
        {
            L3VPN_BGPROUTELABEL_LABEL (L3VpnBgpRouteLabelEntry) =
                L3VPN_P_BGPROUTELABEL_LABEL (pL3VpnBgpRouteLabelEntry);
            RBTreeRemove (L3VPN_BGP_ROUTE_LABEL_TABLE,
                          (tRBElem *) pL3VpnBgpRouteLabelEntry);
            MemReleaseMemBlock (L3VPN_BGPROUTELABELTABLE_POOLID,
                                (UINT1 *) pL3VpnBgpRouteLabelEntry);
        }
        else
        {
            break;
        }
    }
    return OSIX_SUCCESS;
}

UINT4
MplsL3VpnBgp4RouteUpdate (tMplsL3VpnBgp4RouteInfo * pRouteInfo)
{
    UINT2               u2DestAddrFamily = 0;
    UINT2               u2NHAddrFamily = 0;
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    UINT4               u4NextHop = 0;
    UINT4               u4OutSegmentIndex = 0;
    UINT4               u4L3VpnXcIndex = 0;
    UINT4               u4L3VpnOutSegmentIndex = 0;
    UINT1              *pu1RteXcEntry = NULL;
    UINT4               u4L3VpnInSegmentIndex = L3VPN_ZERO;
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;
    UINT1               au1VrfName[L3VPN_MAX_VRF_NAME_LEN];
    INT4                i4VrfNameLen = 0;
    UINT1               u1PrefixType = IPV4;
    UINT1               u1MaskType = IPV4;
    UINT4               u4Mask = 0xFFFFFFFF;
    tL3vpnMplsL3VpnVrfRteEntry *pDelL3vpnMplsL3VpnVrfRteEntry;
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    if (pRouteInfo == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "Route Info is NULL.\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&gL3vpnMplsL3VpnVrfEntry, 0, sizeof (tL3vpnMplsL3VpnVrfEntry));
    MEMSET (au1VrfName, 0, L3VPN_MAX_VRF_NAME_LEN);
    MEMSET (&gL3vpnMplsBgpVrfRteEntry, 0, sizeof (tL3vpnMplsL3VpnVrfRteEntry));

    u2DestAddrFamily = pRouteInfo->IpPrefix.u2Afi;
    u2NHAddrFamily = pRouteInfo->NextHop.u2Afi;

    if (u2DestAddrFamily != MPLS_IPV4_ADDR_TYPE)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "Address Family not supported for the Destination.\r\n"));
        return OSIX_FAILURE;
    }

    if (u2NHAddrFamily != MPLS_IPV4_ADDR_TYPE)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "Address family not supported for the Next Hop.\r\n"));
        return OSIX_FAILURE;
    }

    if (VcmGetAliasName (pRouteInfo->u4VrfId, au1VrfName) == VCM_FAILURE)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "Failed to get the VRF Alias for the Context: %d.\r\n",
                    pRouteInfo->u4VrfId));
        return OSIX_FAILURE;
    }
    i4VrfNameLen = (INT4) STRLEN (au1VrfName);
    /*L3vpnMainTaskLock(); */
    MEMCPY (L3VPN_VRF_NAME (gL3vpnMplsL3VpnVrfEntry), au1VrfName, i4VrfNameLen);
    L3VPN_VRF_NAME_LEN (gL3vpnMplsL3VpnVrfEntry) = i4VrfNameLen;

    pL3vpnMplsL3VpnVrfEntry = (tL3vpnMplsL3VpnVrfEntry *)
        RBTreeGet (L3VPN_VRF_TABLE, &gL3vpnMplsL3VpnVrfEntry);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC,
                    "VRF entry not found in MPLS VRF table: %s.\r\n",
                    au1VrfName));
        /*L3vpnMainTaskUnLock(); */
        return OSIX_FAILURE;
    }
    pL3vpnMplsL3VpnVrfPerfEntry = L3VpnGetVrfPerfTableEntry (au1VrfName);
    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_MAIN_TRC, "PERF Entry is NULL for the VRF: %s.\r\n",
                    au1VrfName));
        /*L3vpnMainTaskUnLock(); */
        return OSIX_FAILURE;
    }
    /* This Entry needs to be stored in the MIB data-structure */
    switch (pRouteInfo->u1ILMAction)
    {
        case L3VPN_BGP4_ROUTE_ADD:

            if (pRouteInfo->u1LabelAction == L3VPN_BGP4_LABEL_PUSH)
            {

                /* Check if allowed to configure more route in the given VRF */
                if (L3VpnIsRouteAdditionAllowed (pL3vpnMplsL3VpnVrfEntry,
                                                 pL3vpnMplsL3VpnVrfPerfEntry) ==
                    OSIX_FALSE)
                {
                    L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                                "Routes Exceed the maximum allowed value for the "
                                "VRF: %s.\r\n", au1VrfName));
                    /* Increment the Dropped routes counter by 1 */
                    L3VpnUpdatePerfTableCounters (au1VrfName,
                                                  L3VPN_ROUTE_DROPPED);
                    return OSIX_FAILURE;
                }
                pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
                    MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
                if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
                {
                    L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                                "Error in Allocating memory for Route Entry.\r\n"));
                    /*L3vpnMainTaskUnLock(); */
                    return OSIX_FAILURE;
                }
                MEMSET (pL3vpnMplsL3VpnVrfRteEntry, 0,
                        sizeof (tL3vpnMplsL3VpnVrfRteEntry));

                /* filling the Indices first */
                /* VRF NAME */
                MEMCPY (L3VPN_P_VRF_NAME (pL3vpnMplsL3VpnVrfRteEntry),
                        au1VrfName, i4VrfNameLen);
                L3VPN_P_VRF_NAME_LEN (pL3vpnMplsL3VpnVrfRteEntry) =
                    i4VrfNameLen;

                /* Destination Type */
                L3VPN_P_RTE_INET_CIDR_DEST_TYPE (pL3vpnMplsL3VpnVrfRteEntry) =
                    u2DestAddrFamily;

                /* Destination for the Route */
                MEMCPY (L3VPN_P_RTE_INET_CIDR_DEST (pL3vpnMplsL3VpnVrfRteEntry),
                        pRouteInfo->IpPrefix.au1Address,
                        pRouteInfo->IpPrefix.u2AddressLen);
                L3VPN_P_RTE_INET_CIDR_DEST_LEN (pL3vpnMplsL3VpnVrfRteEntry) =
                    pRouteInfo->IpPrefix.u2AddressLen;

                /* Prefix Len */
                L3VPN_P_RTE_INET_CIDR_PREFIX_LEN (pL3vpnMplsL3VpnVrfRteEntry) =
                    pRouteInfo->u2PrefixLen;

                /* Next-Hop Type */
                L3VPN_P_RTE_INET_CIDR_NH_TYPE (pL3vpnMplsL3VpnVrfRteEntry) =
                    u2NHAddrFamily;

                /* Next Hop for the Route -- Remote PE */
                MEMCPY (L3VPN_P_RTE_INET_CIDR_NH (pL3vpnMplsL3VpnVrfRteEntry),
                        pRouteInfo->NextHop.au1Address,
                        pRouteInfo->NextHop.u2AddressLen);
                L3VPN_P_RTE_INET_CIDR_NH_LEN (pL3vpnMplsL3VpnVrfRteEntry) =
                    pRouteInfo->NextHop.u2AddressLen;

                /* Next Hop AS */
                L3VPN_P_RTE_INET_CIDR_NH_AS (pL3vpnMplsL3VpnVrfRteEntry) =
                    pRouteInfo->u4NHAS;
                /* Interface Index */
                L3VPN_P_RTE_INET_CIDR_IF_INDEX (pL3vpnMplsL3VpnVrfRteEntry) =
                    (INT4) pRouteInfo->u4IfIndex;
                /* CIDR Type */
                L3VPN_P_RTE_INET_CIDR_TYPE (pL3vpnMplsL3VpnVrfRteEntry) =
                    L3VPN_MPLS_INET_CIDR_TYPE_REMOTE;
                /* CIDR Proto taken always as BGP */
                L3VPN_P_RTE_INET_CIDR_PROTO (pL3vpnMplsL3VpnVrfRteEntry) =
                    L3VPN_MPLS_ROUTE_PROTO_BGP;
                /* Metric for the BGP */
                L3VPN_P_RTE_INET_CIDR_METRIC1 (pL3vpnMplsL3VpnVrfRteEntry) =
                    (INT4) pRouteInfo->u4Metric;
                /* Initialize the unused metrics with -1 */
                L3VPN_P_RTE_INET_CIDR_METRIC2 (pL3vpnMplsL3VpnVrfRteEntry) =
                    L3VPN_DEF_ROUTE_METRIC;
                L3VPN_P_RTE_INET_CIDR_METRIC3 (pL3vpnMplsL3VpnVrfRteEntry) =
                    L3VPN_DEF_ROUTE_METRIC;
                L3VPN_P_RTE_INET_CIDR_METRIC4 (pL3vpnMplsL3VpnVrfRteEntry) =
                    L3VPN_DEF_ROUTE_METRIC;
                L3VPN_P_RTE_INET_CIDR_METRIC5 (pL3vpnMplsL3VpnVrfRteEntry) =
                    L3VPN_DEF_ROUTE_METRIC;
                /* Row Status */
                L3VPN_P_RTE_INET_CIDR_STATUS (pL3vpnMplsL3VpnVrfRteEntry) =
                    ACTIVE;

                if (RBTreeGet (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry) !=
                    NULL)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "RTE Entry Already exists.\r\n"));
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }

                MEMCPY (&u4NextHop, pRouteInfo->NextHop.au1Address,
                        pRouteInfo->NextHop.u2AddressLen);
                u4NextHop = OSIX_NTOHL (u4NextHop);

                /* RSVP-TE MPLS-L3VPN-TE */
                pL3VpnRsvpMapLabelEntry =
                    L3vpnGetRsvpMapLabelEntry (u1PrefixType, u4NextHop,
                                               u1MaskType, u4Mask);
                if (pL3VpnRsvpMapLabelEntry != NULL)
                {
                    if ((pL3VpnRsvpMapLabelEntry->u4TnlIndex != 0) &&
                        (pL3VpnRsvpMapLabelEntry->u1TnlSgnlPrtcl ==
                         TE_SIGPROTO_RSVP))
                    {
                        /* We need to pull the OutSegmentIndex from our explicit mapping table */
                        if (MplsGetRsvpTeLspOutSegmentFromPrefix
                            (u4NextHop, &u4OutSegmentIndex) == MPLS_FAILURE)
                        {
                            L3VPN_TRC ((L3VPN_MAIN_TRC,
                                        "RSVP-TE LSP not found for the next hop %d.%d.%d.%d\n",
                                        pRouteInfo->NextHop.au1Address[0],
                                        pRouteInfo->NextHop.au1Address[1],
                                        pRouteInfo->NextHop.au1Address[2],
                                        pRouteInfo->NextHop.au1Address[3]));
                            MemReleaseMemBlock
                                (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                 (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                            return OSIX_FAILURE;
                        }
                    }
                }
                else
                {
                    /* Find the Non-Te LSP for the Next-hop */
                    if (MplsGetNonTeLspOutSegmentFromPrefix
                        (u4NextHop, &u4OutSegmentIndex) == MPLS_FAILURE)
                    {
                        L3VPN_TRC ((L3VPN_MAIN_TRC,
                                    "LSP not found for the next hop %d.%d.%d.%d\n",
                                    pRouteInfo->NextHop.au1Address[0],
                                    pRouteInfo->NextHop.au1Address[1],
                                    pRouteInfo->NextHop.au1Address[2],
                                    pRouteInfo->NextHop.au1Address[3]));
                        /*L3vpnMainTaskUnLock(); */
                        MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                            (UINT1 *)
                                            pL3vpnMplsL3VpnVrfRteEntry);
                        return OSIX_FAILURE;
                    }
                }
                /*Made it gereric to support both cases Rsvp-Te and LDP */
                if (MplsCreateXcEntryForL3Vpn
                    (u4OutSegmentIndex, pRouteInfo->u4Label, &u4L3VpnXcIndex,
                     &u4L3VpnOutSegmentIndex) == MPLS_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "Failed to create the XC Entry for L3VPN.\r\n"));

                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                    /*L3vpnMainTaskUnLock(); */
                    return OSIX_FAILURE;
                }
                pu1RteXcEntry =
                    L3VPN_P_RTE_XC_POINTER (pL3vpnMplsL3VpnVrfRteEntry);

                u4L3VpnXcIndex = OSIX_HTONL (u4L3VpnXcIndex);
                MEMCPY (pu1RteXcEntry, (UINT1 *) &u4L3VpnXcIndex,
                        sizeof (UINT4));
                pu1RteXcEntry = pu1RteXcEntry + sizeof (UINT4);

                u4L3VpnInSegmentIndex = OSIX_HTONL (u4L3VpnInSegmentIndex);
                MEMCPY (pu1RteXcEntry, (UINT1 *) &u4L3VpnInSegmentIndex,
                        sizeof (UINT4));
                pu1RteXcEntry = pu1RteXcEntry + sizeof (UINT4);

                u4L3VpnOutSegmentIndex = OSIX_HTONL (u4L3VpnOutSegmentIndex);
                MEMCPY (pu1RteXcEntry, (UINT1 *) &u4L3VpnOutSegmentIndex,
                        sizeof (UINT4));
                L3VPN_P_RTE_XC_POINTER_LEN (pL3vpnMplsL3VpnVrfRteEntry) =
                    3 * (sizeof (UINT4));
                if (RBTreeAdd (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry) !=
                    RB_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                                "Failed to add the RTE entry in the RB Tree.\r\n"));
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                    /*L3vpnMainTaskUnLock(); */
                    L3VpnDeleteXcEntry (OSIX_NTOHL (u4L3VpnXcIndex),
                                        OSIX_NTOHL (u4L3VpnInSegmentIndex),
                                        OSIX_NTOHL (u4L3VpnOutSegmentIndex));
                    return OSIX_FAILURE;
                }
                /* Now Add the Entry in the TRIE data-structure */
                if (L3VpnRegisterRteEntryWithTrie (pRouteInfo->u4VrfId,
                                                   pL3vpnMplsL3VpnVrfRteEntry)
                    == L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                                "FAiled to add the RTE Entry in the TRIE.\r\n"));
                    RBTreeRem (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry);
                    MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                        (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
                    L3VpnDeleteXcEntry (OSIX_NTOHL (u4L3VpnXcIndex),
                                        OSIX_NTOHL (u4L3VpnInSegmentIndex),
                                        OSIX_NTOHL (u4L3VpnOutSegmentIndex));
                    /*L3vpnMainTaskUnLock(); */
                    return OSIX_FAILURE;
                }
                if (MplsL3vpnIngressMap (pL3vpnMplsL3VpnVrfRteEntry) !=
                    L3VPN_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "MplsL3vpnIngressMap() Failed.\r\n"));

                    L3VpnDeleteRouteEntry (pL3vpnMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }
                /* Updating the Perf counters */
                if (L3VpnUpdatePerfTableCounters (au1VrfName,
                                                  L3VPN_ROUTE_ADDED) ==
                    L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Error in updating the Performance table for the "
                                "VRF:%s.\r\n", au1VrfName));

                    L3VpnDeleteRouteEntry (pL3vpnMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }
                if (L3VpnChckAndSendRouteNotifications (pL3vpnMplsL3VpnVrfEntry,
                                                        pL3vpnMplsL3VpnVrfPerfEntry,
                                                        L3VPN_ROUTE_ADDED) ==
                    L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Failed to send Trap Notification for the VRF:%s.\r\n",
                                au1VrfName));
                    L3VpnDeleteRouteEntry (pL3vpnMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }

            }
            else                /* LABEL_POP case */
            {
                if (L3VpnBgp4RouteAddDelAtEgress (pRouteInfo) != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
            }
            break;

        case L3VPN_BGP4_ROUTE_DEL:
            if (pRouteInfo->u1LabelAction == L3VPN_BGP4_LABEL_PUSH)
            {
                /* VRF NAME */
                MEMCPY (L3VPN_VRF_NAME (gL3vpnMplsBgpVrfRteEntry), au1VrfName,
                        i4VrfNameLen);
                L3VPN_VRF_NAME_LEN (gL3vpnMplsBgpVrfRteEntry) = i4VrfNameLen;

                /* Destination Type */
                L3VPN_RTE_INET_CIDR_DEST_TYPE (gL3vpnMplsBgpVrfRteEntry) =
                    u2DestAddrFamily;

                /* Destination for the Route */
                MEMCPY (L3VPN_RTE_INET_CIDR_DEST (gL3vpnMplsBgpVrfRteEntry),
                        pRouteInfo->IpPrefix.au1Address,
                        pRouteInfo->IpPrefix.u2AddressLen);
                L3VPN_RTE_INET_CIDR_DEST_LEN (gL3vpnMplsBgpVrfRteEntry) =
                    pRouteInfo->IpPrefix.u2AddressLen;

                /* Prefix Len */
                L3VPN_RTE_INET_CIDR_PREFIX_LEN (gL3vpnMplsBgpVrfRteEntry) =
                    pRouteInfo->u2PrefixLen;

                /* Next-Hop Type */
                L3VPN_RTE_INET_CIDR_NH_TYPE (gL3vpnMplsBgpVrfRteEntry) =
                    u2NHAddrFamily;

                /* Next Hop for the Route -- Remote PE */
                MEMCPY (L3VPN_RTE_INET_CIDR_NH (gL3vpnMplsBgpVrfRteEntry),
                        pRouteInfo->NextHop.au1Address,
                        pRouteInfo->NextHop.u2AddressLen);
                L3VPN_RTE_INET_CIDR_NH_LEN (gL3vpnMplsBgpVrfRteEntry) =
                    pRouteInfo->NextHop.u2AddressLen;

                pDelL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
                    RBTreeGet (L3VPN_RTE_TABLE, &gL3vpnMplsBgpVrfRteEntry);
                if (pDelL3vpnMplsL3VpnVrfRteEntry == NULL)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "RTE Entry does not exists.\r\n"));
                    /*L3vpnMainTaskUnLock(); */
                    return OSIX_FAILURE;
                }
                if (MplsL3vpnIngressUnMap (pDelL3vpnMplsL3VpnVrfRteEntry) !=
                    L3VPN_SUCCESS)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "MplsL3vpnIngressUnMap() Failed.\r\n"));
                    return OSIX_FAILURE;
                }
                if (L3VpnDeleteRouteEntry (pDelL3vpnMplsL3VpnVrfRteEntry) ==
                    L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_UTIL_TRC,
                                "Error in Deleting the RTE Entry.\r\n"));
                    return OSIX_FAILURE;
                }
                if (L3VpnUpdatePerfTableCounters (au1VrfName,
                                                  L3VPN_ROUTE_DELETED) ==
                    L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Error in updating the Perormance table for the VRF: %s.\r\n",
                                au1VrfName));
                    return OSIX_FAILURE;
                }
                if (L3VpnChckAndSendRouteNotifications (pL3vpnMplsL3VpnVrfEntry,
                                                        pL3vpnMplsL3VpnVrfPerfEntry,
                                                        L3VPN_ROUTE_DELETED) ==
                    L3VPN_FAILURE)
                {
                    L3VPN_TRC ((L3VPN_MAIN_TRC,
                                "Failed to send Trap Notification for the VRF:%s.\r\n",
                                au1VrfName));
                    L3VpnDeleteRouteEntry (pL3vpnMplsL3VpnVrfRteEntry);
                    return OSIX_FAILURE;
                }

            }
            else                /* Route Delete case for LABEL_POP */
            {
                if (L3VpnBgp4RouteAddDelAtEgress (pRouteInfo) != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
            }
            break;
        default:
            break;
    };
    /*L3vpnMainTaskUnLock(); */
    return OSIX_SUCCESS;
}

VOID
L3VpnDeleteXcEntry (UINT4 u4XcIndex, UINT4 u4InSegmentIndex,
                    UINT4 u4OutSegmentIndex)
{

    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;

    MPLS_CMN_LOCK ();

    pXcEntry = MplsGetXCTableEntry (u4XcIndex,
                                    u4InSegmentIndex, u4OutSegmentIndex);
    if (pXcEntry != NULL)
    {
        if (MplsDeleteXCTableEntry (pXcEntry) == MPLS_FAILURE)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC, "Error in Deleting the XC Entry.\r\n"));
        }
    }

    pInSegment = MplsGetInSegmentTableEntry (u4InSegmentIndex);
    if (pInSegment != NULL)
    {
        if (MplsDeleteInSegmentTableEntry (pInSegment) == MPLS_FAILURE)
        {
            L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                        "Error in the InSegment Entry.\r\n"));
        }
    }

    pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegmentIndex);
    if (pOutSegment != NULL)
    {
        if (MplsDeleteOutSegmentTableEntry (pOutSegment) == MPLS_FAILURE)
        {
            L3VPN_TRC ((L3VPN_RESOURCE_TRC,
                        "Error in Deleting the Outsegment Entry.\r\n"));
        }
    }
    MPLS_CMN_UNLOCK ();
}

#if 0
UINT4
L3VpnNotifyBgp (tL3VpnBgpNotifyInfo * pBgpNotifyInfo)
{

    switch (pBgpNotifyInfo->u4NotifType)
    {
        case L3VPN_BGP_ROUTE_PARAMS:
            if (L3VPN_NOTIFY_ROUTE_PARAMS_CB != NULL)
            {
                (*(L3VPN_NOTIFY_ROUTE_PARAMS_CB)) (pBgpNotifyInfo->
                                                   L3VpnBgpEvtInfo.
                                                   L3VpnBgpRouteParams.u4VrfId,
                                                   pBgpNotifyInfo->
                                                   L3VpnBgpEvtInfo.
                                                   L3VpnBgpRouteParams.
                                                   u4ParamType,
                                                   pBgpNotifyInfo->
                                                   L3VpnBgpEvtInfo.
                                                   L3VpnBgpRouteParams.u1Action,
                                                   pBgpNotifyInfo->
                                                   L3VpnBgpEvtInfo.
                                                   L3VpnBgpRouteParams.
                                                   pu1RouteParam);
            }
            else
            {
                L3VPN_TRC ((L3VPN_UTIL_TRC
                            ("BGP Callback for Route Params is NULL.\r\n");
                            return L3VPN_FAILURE;
                            }
                            break;
        case L3VPN_BGP_LSP_STATUS_PARAMS:
                            if (L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB != NULL)
                            {
                            (*(L3VPN_NOTIFY_LSP_STATUS_CHANGE_CB))
                            (pBgpNotifyInfo->L3VpnBgpEvtInfo.
                             L3VpnBgpLspStatusParams.u4Label,
                             pBgpNotifyInfo->L3VpnBgpEvtInfo.
                             L3VpnBgpLspStatusParams.u4LspStatus);}
                            else
                            {
                            L3VPN_TRC ((L3VPN_UTIL_TRC
                                        ("BGP Callback for LSP Status is NULL.\r\n");
                                        return L3VPN_FAILURE;}
        break; case L3VPN_BGP_VRF_STATUS_PARAMS:
                                        if (L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB !=
                                            NULL)
                                        {
                                        (*(L3VPN_NOTIFY_VRF_STATUS_CHANGE_CB))
                                        (pBgpNotifyInfo->L3VpnBgpEvtInfo.
                                         L3VpnBgpVrfStatusParams.u4VrfId,
                                         pBgpNotifyInfo->L3VpnBgpEvtInfo.
                                         L3VpnBgpVrfStatusParams.u4VrfStatus);}
                                        else
                                        {
                                        L3VPN_TRC ((L3VPN_UTIL_TRC
                                                    ("BGP callback for VRF Status is NULL.\r\n");
                                                    return L3VPN_FAILURE;}
        break; default:
                                                    L3VPN_TRC ((L3VPN_UTIL_TRC
                                                                ("Wrong notify type.\r\n");
                                                                return
                                                                L3VPN_FAILURE;
                                                                break;}
                                                                return
                                                                L3VPN_SUCCESS;}
#endif

                                                                UINT4
                                                                MplsBgp4ASNInUse
                                                                (BOOL1 *
                                                                 pbInUse)
                                                                {
                                                                tL3vpnMplsL3VpnVrfEntry
                                                                *
                                                                pL3VpnVrfEntry =
                                                                NULL;
                                                                tL3vpnMplsL3VpnVrfRTEntry
                                                                *
                                                                pL3vpnMplsL3VpnVrfRTEntry
                                                                = NULL;
                                                                L3vpnMainTaskLock
                                                                ();
                                                                pL3VpnVrfEntry =
                                                                (tL3vpnMplsL3VpnVrfEntry
                                                                 *)
                                                                RBTreeGetFirst
                                                                (L3VPN_VRF_TABLE);
                                                                while
                                                                (pL3VpnVrfEntry
                                                                 != NULL)
                                                                {
                                                                if
                                                                (L3VPN_P_VRF_ROW_STATUS
                                                                 (pL3VpnVrfEntry)
                                                                 == ACTIVE)
                                                                {
                                                                *pbInUse =
                                                                OSIX_TRUE;
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_SUCCESS;}
                                                                pL3VpnVrfEntry =
                                                                (tL3vpnMplsL3VpnVrfEntry
                                                                 *)
                                                                RBTreeGetNext
                                                                (L3VPN_VRF_TABLE,
                                                                 pL3VpnVrfEntry,
                                                                 NULL);}

                                                                pL3vpnMplsL3VpnVrfRTEntry
                                                                =
                                                                (tL3vpnMplsL3VpnVrfRTEntry
                                                                 *)
                                                                RBTreeGetFirst
                                                                (L3VPN_RT_TABLE);
                                                                while
                                                                (pL3vpnMplsL3VpnVrfRTEntry
                                                                 != NULL)
                                                                {
                                                                if
                                                                (L3VPN_P_RT_ROW_STATUS
                                                                 (pL3vpnMplsL3VpnVrfRTEntry)
                                                                 == ACTIVE)
                                                                {
                                                                *pbInUse =
                                                                OSIX_TRUE;
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_SUCCESS;}
                                                                pL3vpnMplsL3VpnVrfRTEntry
                                                                =
                                                                (tL3vpnMplsL3VpnVrfRTEntry
                                                                 *)
                                                                RBTreeGetNext
                                                                (L3VPN_RT_TABLE,
                                                                 pL3vpnMplsL3VpnVrfRTEntry,
                                                                 NULL);}
                                                                *pbInUse =
                                                                OSIX_FALSE;
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_SUCCESS;}

                                                                UINT4
                                                                MplsGetNonTeLspForDest
                                                                (UINT4 u4Prefix,
                                                                 UINT4
                                                                 *pu4LspLabel,
                                                                 UINT1
                                                                 *pu1LspStatus)
                                                                {

                                                                UINT4 u4OutIndex
                                                                = 0;
                                                                tOutSegment *
                                                                pOutSegment =
                                                                NULL;
                                                                L3vpnMainTaskLock
                                                                ();
                                                                u4Prefix =
                                                                OSIX_NTOHL
                                                                (u4Prefix);
                                                                if
                                                                (MplsGetNonTeLspOutSegmentFromPrefix
                                                                 (u4Prefix,
                                                                  &u4OutIndex)
                                                                 ==
                                                                 MPLS_FAILURE)
                                                                {
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_FAILURE;}
                                                                pOutSegment =
                                                                MplsGetOutSegmentTableEntry
                                                                (u4OutIndex);
                                                                if (pOutSegment
                                                                    == NULL)
                                                                {
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_FAILURE;}
                                                                *pu4LspLabel =
                                                                pOutSegment->
                                                                u4Label;
                                                                *pu1LspStatus =
                                                                L3VPN_BGP4_LSP_STATUS_UP;
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_SUCCESS;}
                                                                UINT4
                                                                MplsGetRsvpTeLspForDest
                                                                (UINT4 u4Prefix,
                                                                 UINT4
                                                                 *pu4LspLabel,
                                                                 UINT1
                                                                 *pu1LspStatus)
                                                                {

                                                                UINT4 u4OutIndex
                                                                = 0;
                                                                tOutSegment *
                                                                pOutSegment =
                                                                NULL;
                                                                L3vpnMainTaskLock
                                                                ();
                                                                u4Prefix =
                                                                OSIX_NTOHL
                                                                (u4Prefix);
                                                                if
                                                                (MplsGetRsvpTeLspOutSegmentFromPrefix
                                                                 (u4Prefix,
                                                                  &u4OutIndex)
                                                                 ==
                                                                 MPLS_FAILURE)
                                                                {
                                                                L3VPN_TRC ((L3VPN_MAIN_TRC, "MplsGetRsvpTeLspOutSegmentFromPrefix failed for u4Prefix :%x\n", u4Prefix)); L3vpnMainTaskUnLock (); return OSIX_FAILURE;}
                                                                pOutSegment =
                                                                MplsGetOutSegmentTableEntry
                                                                (u4OutIndex);
                                                                if (pOutSegment
                                                                    == NULL)
                                                                {
                                                                L3VPN_TRC ((L3VPN_MAIN_TRC, "pOutSegment NULL:MplsGetRsvpTeLspOutSegmentFromPrefix failed\n")); L3vpnMainTaskUnLock (); return OSIX_FAILURE;}
                                                                *pu4LspLabel =
                                                                pOutSegment->
                                                                u4Label;
                                                                *pu1LspStatus =
                                                                L3VPN_BGP4_RSVPTE_LSP_STATUS_UP;
                                                                L3vpnMainTaskUnLock
                                                                ();
                                                                return
                                                                OSIX_SUCCESS;}
