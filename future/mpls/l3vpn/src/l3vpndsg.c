/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpndsg.c,v 1.1.1.1 2014/02/27 14:09:55 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpndsg.c 
*
* Description: This file contains the routines for DataStructure access for the module L3vpn 
*********************************************************************/

#include "l3vpninc.h"
/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnConfiguredVrfs
 Input       :  The Indices
 Input       :  pu4MplsL3VpnConfiguredVrfs
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnConfiguredVrfs (UINT4 *pu4MplsL3VpnConfiguredVrfs)
{
    *pu4MplsL3VpnConfiguredVrfs =
        gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnConfiguredVrfs;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnActiveVrfs
 Input       :  The Indices
 Input       :  pu4MplsL3VpnActiveVrfs
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnActiveVrfs (UINT4 *pu4MplsL3VpnActiveVrfs)
{
    *pu4MplsL3VpnActiveVrfs = gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnActiveVrfs;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnConnectedInterfaces
 Input       :  The Indices
 Input       :  pu4MplsL3VpnConnectedInterfaces
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnConnectedInterfaces (UINT4 *pu4MplsL3VpnConnectedInterfaces)
{
    *pu4MplsL3VpnConnectedInterfaces =
        gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnConnectedInterfaces;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnNotificationEnable
 Input       :  The Indices
 Input       :  pi4MplsL3VpnNotificationEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnNotificationEnable (INT4 *pi4MplsL3VpnNotificationEnable)
{
    *pi4MplsL3VpnNotificationEnable =
        gL3vpnGlobals.L3vpnGlbMib.i4MplsL3VpnNotificationEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetMplsL3VpnNotificationEnable
 Input       :  i4MplsL3VpnNotificationEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnSetMplsL3VpnNotificationEnable (INT4 i4MplsL3VpnNotificationEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gL3vpnGlobals.L3vpnGlbMib.i4MplsL3VpnNotificationEnable =
        i4MplsL3VpnNotificationEnable;

    nmhSetCmnNew (MplsL3VpnNotificationEnable, 12, L3vpnMainTaskLock,
                  L3vpnMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gL3vpnGlobals.L3vpnGlbMib.i4MplsL3VpnNotificationEnable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnVrfConfMaxPossRts
 Input       :  The Indices
 Input       :  pu4MplsL3VpnVrfConfMaxPossRts
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnVrfConfMaxPossRts (UINT4 *pu4MplsL3VpnVrfConfMaxPossRts)
{
    *pu4MplsL3VpnVrfConfMaxPossRts =
        gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnVrfConfMaxPossRts;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnVrfConfRteMxThrshTime
 Input       :  The Indices
 Input       :  pu4MplsL3VpnVrfConfRteMxThrshTime
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnVrfConfRteMxThrshTime (UINT4
                                        *pu4MplsL3VpnVrfConfRteMxThrshTime)
{
    *pu4MplsL3VpnVrfConfRteMxThrshTime =
        gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnVrfConfRteMxThrshTime;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetMplsL3VpnVrfConfRteMxThrshTime
 Input       :  u4MplsL3VpnVrfConfRteMxThrshTime
 Description :  This Routine will Set
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnSetMplsL3VpnVrfConfRteMxThrshTime (UINT4 u4MplsL3VpnVrfConfRteMxThrshTime)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnVrfConfRteMxThrshTime =
        u4MplsL3VpnVrfConfRteMxThrshTime;

    nmhSetCmnNew (MplsL3VpnVrfConfRteMxThrshTime, 12, L3vpnMainTaskLock,
                  L3vpnMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnVrfConfRteMxThrshTime);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnIllLblRcvThrsh
 Input       :  The Indices
 Input       :  pu4MplsL3VpnIllLblRcvThrsh
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
L3vpnGetMplsL3VpnIllLblRcvThrsh (UINT4 *pu4MplsL3VpnIllLblRcvThrsh)
{
    *pu4MplsL3VpnIllLblRcvThrsh =
        gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnIllLblRcvThrsh;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetMplsL3VpnIllLblRcvThrsh
 Input       :  u4MplsL3VpnIllLblRcvThrsh
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
L3vpnSetMplsL3VpnIllLblRcvThrsh (UINT4 u4MplsL3VpnIllLblRcvThrsh)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnIllLblRcvThrsh =
        u4MplsL3VpnIllLblRcvThrsh;

    nmhSetCmnNew (MplsL3VpnIllLblRcvThrsh, 12, L3vpnMainTaskLock,
                  L3vpnMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gL3vpnGlobals.L3vpnGlbMib.u4MplsL3VpnIllLblRcvThrsh);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnGetFirstMplsL3VpnVrfTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pL3vpnMplsL3VpnVrfEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfEntry *
L3vpnGetFirstMplsL3VpnVrfTable ()
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *) RBTreeGetFirst (gL3vpnGlobals.L3vpnGlbMib.
                                                    MplsL3VpnVrfTable);

    return pL3vpnMplsL3VpnVrfEntry;
}

tL3vpnMplsL3VpnVrfSecEntry *
L3vpnGetFirstMplsL3VpnVrfSecTable ()
{
	tL3vpnMplsL3VpnVrfSecEntry * pL3vpnMplsL3VpnVrfSecEntry=NULL;
	pL3vpnMplsL3VpnVrfSecEntry=
		(tL3vpnMplsL3VpnVrfSecEntry*)RBTreeGetFirst(L3VPN_SECURITY_TABLE);
	return pL3vpnMplsL3VpnVrfSecEntry;
}

tL3vpnMplsL3VpnVrfPerfEntry *
L3vpnGetFirstMplsL3VpnVrfPerfTable ()
{
	tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry=NULL;

	pL3vpnMplsL3VpnVrfPerfEntry=
		(tL3vpnMplsL3VpnVrfPerfEntry*)RBTreeGetFirst(L3VPN_PERF_TABLE);

	return pL3vpnMplsL3VpnVrfPerfEntry;
}

tL3vpnMplsL3VpnVrfSecEntry *
L3vpnGetNextMplsL3VpnVrfSecTable (tL3vpnMplsL3VpnVrfSecEntry *pCurrentL3vpnMplsL3VpnVrfSecEntry)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    pL3vpnMplsL3VpnVrfSecEntry =
        (tL3vpnMplsL3VpnVrfSecEntry *) RBTreeGetNext (L3VPN_SECURITY_TABLE,
                                                   (tRBElem *)
                                                   pCurrentL3vpnMplsL3VpnVrfSecEntry,
                                                   NULL);

    return pL3vpnMplsL3VpnVrfSecEntry;	
}

tL3vpnMplsL3VpnVrfPerfEntry *
L3vpnGetNextMplsL3VpnVrfPerfTable (tL3vpnMplsL3VpnVrfPerfEntry *pCurrentL3vpnMplsL3VpnVrfPerfEntry)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *) RBTreeGetNext (L3VPN_PERF_TABLE,
                                                   (tRBElem *)
                                                   pCurrentL3vpnMplsL3VpnVrfPerfEntry,
                                                   NULL);
    return pL3vpnMplsL3VpnVrfPerfEntry;	
}


/****************************************************************************
 Function    :  L3vpnGetNextMplsL3VpnVrfTable
 Input       :  pCurrentL3vpnMplsL3VpnVrfEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextL3vpnMplsL3VpnVrfEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfEntry *
L3vpnGetNextMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry *
                               pCurrentL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfEntry *pNextL3vpnMplsL3VpnVrfEntry = NULL;

    pNextL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *) RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.
                                                   MplsL3VpnVrfTable,
                                                   (tRBElem *)
                                                   pCurrentL3vpnMplsL3VpnVrfEntry,
                                                   NULL);

    return pNextL3vpnMplsL3VpnVrfEntry;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnVrfTable
 Input       :  pL3vpnMplsL3VpnVrfEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetL3vpnMplsL3VpnVrfEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfEntry *
L3vpnGetMplsL3VpnVrfTable (tL3vpnMplsL3VpnVrfEntry * pL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfEntry *pGetL3vpnMplsL3VpnVrfEntry = NULL;

    pGetL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *) RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.
                                               MplsL3VpnVrfTable,
                                               (tRBElem *)
                                               pL3vpnMplsL3VpnVrfEntry);

    return pGetL3vpnMplsL3VpnVrfEntry;
}

/****************************************************************************
 Function    :  L3vpnGetFirstMplsL3VpnIfConfTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pL3vpnMplsL3VpnIfConfEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnIfConfEntry *
L3vpnGetFirstMplsL3VpnIfConfTable ()
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *) RBTreeGetFirst (gL3vpnGlobals.
                                                       L3vpnGlbMib.
                                                       MplsL3VpnIfConfTable);

    return pL3vpnMplsL3VpnIfConfEntry;
}

/****************************************************************************
 Function    :  L3vpnGetNextMplsL3VpnIfConfTable
 Input       :  pCurrentL3vpnMplsL3VpnIfConfEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextL3vpnMplsL3VpnIfConfEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnIfConfEntry *
L3vpnGetNextMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                                  pCurrentL3vpnMplsL3VpnIfConfEntry)
{
    tL3vpnMplsL3VpnIfConfEntry *pNextL3vpnMplsL3VpnIfConfEntry = NULL;

    pNextL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *) RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.
                                                      MplsL3VpnIfConfTable,
                                                      (tRBElem *)
                                                      pCurrentL3vpnMplsL3VpnIfConfEntry,
                                                      NULL);

    return pNextL3vpnMplsL3VpnIfConfEntry;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnIfConfTable
 Input       :  pL3vpnMplsL3VpnIfConfEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetL3vpnMplsL3VpnIfConfEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnIfConfEntry *
L3vpnGetMplsL3VpnIfConfTable (tL3vpnMplsL3VpnIfConfEntry *
                              pL3vpnMplsL3VpnIfConfEntry)
{
    tL3vpnMplsL3VpnIfConfEntry *pGetL3vpnMplsL3VpnIfConfEntry = NULL;

    pGetL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *) RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.
                                                  MplsL3VpnIfConfTable,
                                                  (tRBElem *)
                                                  pL3vpnMplsL3VpnIfConfEntry);

    return pGetL3vpnMplsL3VpnIfConfEntry;
}

/****************************************************************************
 Function    :  L3vpnGetFirstMplsL3VpnVrfRTTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pL3vpnMplsL3VpnVrfRTEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfRTEntry *
L3vpnGetFirstMplsL3VpnVrfRTTable ()
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *) RBTreeGetFirst (gL3vpnGlobals.L3vpnGlbMib.
                                                      MplsL3VpnVrfRTTable);

    return pL3vpnMplsL3VpnVrfRTEntry;
}

/****************************************************************************
 Function    :  L3vpnGetNextMplsL3VpnVrfRTTable
 Input       :  pCurrentL3vpnMplsL3VpnVrfRTEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextL3vpnMplsL3VpnVrfRTEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfRTEntry *
L3vpnGetNextMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                                 pCurrentL3vpnMplsL3VpnVrfRTEntry)
{
    tL3vpnMplsL3VpnVrfRTEntry *pNextL3vpnMplsL3VpnVrfRTEntry = NULL;

    pNextL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *) RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.
                                                     MplsL3VpnVrfRTTable,
                                                     (tRBElem *)
                                                     pCurrentL3vpnMplsL3VpnVrfRTEntry,
                                                     NULL);

    return pNextL3vpnMplsL3VpnVrfRTEntry;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnVrfRTTable
 Input       :  pL3vpnMplsL3VpnVrfRTEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetL3vpnMplsL3VpnVrfRTEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfRTEntry *
L3vpnGetMplsL3VpnVrfRTTable (tL3vpnMplsL3VpnVrfRTEntry *
                             pL3vpnMplsL3VpnVrfRTEntry)
{
    tL3vpnMplsL3VpnVrfRTEntry *pGetL3vpnMplsL3VpnVrfRTEntry = NULL;

    pGetL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *) RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.
                                                 MplsL3VpnVrfRTTable,
                                                 (tRBElem *)
                                                 pL3vpnMplsL3VpnVrfRTEntry);

    return pGetL3vpnMplsL3VpnVrfRTEntry;
}

/****************************************************************************
 Function    :  L3vpnGetFirstMplsL3VpnVrfRteTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pL3vpnMplsL3VpnVrfRteEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfRteEntry *
L3vpnGetFirstMplsL3VpnVrfRteTable ()
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *) RBTreeGetFirst (gL3vpnGlobals.
                                                       L3vpnGlbMib.
                                                       MplsL3VpnVrfRteTable);

    return pL3vpnMplsL3VpnVrfRteEntry;
}

/****************************************************************************
 Function    :  L3vpnGetNextMplsL3VpnVrfRteTable
 Input       :  pCurrentL3vpnMplsL3VpnVrfRteEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextL3vpnMplsL3VpnVrfRteEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfRteEntry *
L3vpnGetNextMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                                  pCurrentL3vpnMplsL3VpnVrfRteEntry)
{
    tL3vpnMplsL3VpnVrfRteEntry *pNextL3vpnMplsL3VpnVrfRteEntry = NULL;

    pNextL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *) RBTreeGetNext (gL3vpnGlobals.L3vpnGlbMib.
                                                      MplsL3VpnVrfRteTable,
                                                      (tRBElem *)
                                                      pCurrentL3vpnMplsL3VpnVrfRteEntry,
                                                      NULL);

    return pNextL3vpnMplsL3VpnVrfRteEntry;
}

/****************************************************************************
 Function    :  L3vpnGetMplsL3VpnVrfRteTable
 Input       :  pL3vpnMplsL3VpnVrfRteEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetL3vpnMplsL3VpnVrfRteEntry or NULL
****************************************************************************/
tL3vpnMplsL3VpnVrfRteEntry *
L3vpnGetMplsL3VpnVrfRteTable (tL3vpnMplsL3VpnVrfRteEntry *
                              pL3vpnMplsL3VpnVrfRteEntry)
{
    tL3vpnMplsL3VpnVrfRteEntry *pGetL3vpnMplsL3VpnVrfRteEntry = NULL;

    pGetL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *) RBTreeGet (gL3vpnGlobals.L3vpnGlbMib.
                                                  MplsL3VpnVrfRteTable,
                                                  (tRBElem *)
                                                  pL3vpnMplsL3VpnVrfRteEntry);

    return pGetL3vpnMplsL3VpnVrfRteEntry;
}
