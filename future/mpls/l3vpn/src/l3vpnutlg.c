/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l3vpnutlg.c,v 1.4 2018/01/03 11:31:21 siva Exp $
 *
 ********************************************************************/

/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: l3vpnutlg.c 
*
* Description: This file contains utility functions used by protocol L3vpn
*********************************************************************/

#include "l3vpninc.h"

UINT1               gau1MplsL3VpnVrfRteInetCidrDestVal[256];
UINT1               gau1MplsL3VpnVrfRteInetCidrNextHopVal[256];
UINT1               gau1MplsL3VpnVrfRteXCPointerVal[256];
UINT1               gau1MplsL3VpnVrfNameVal[256];
UINT1               gau1MplsL3VpnVrfVpnIdVal[256];
UINT1               gau1MplsL3VpnVrfDescriptionVal[256];
UINT1               gau1MplsL3VpnVrfRDVal[256];

/****************************************************************************
 Function    :  L3vpnUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
L3vpnUtlCreateRBTree ()
{

    if (L3vpnMplsL3VpnVrfTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (L3vpnMplsL3VpnIfConfTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (L3vpnMplsL3VpnVrfRTTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (L3vpnMplsL3VpnVrfSecTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (L3vpnMplsL3VpnVrfPerfTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (L3vpnMplsL3VpnVrfRteTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    if (L3VpnBgpRouteLabelRBTreeCreate () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (L3VpnRsvpMapLabelRBTreeCreate () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfTableCreate
 Input       :  None
 Description :  This function creates the MplsL3VpnVrfTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
L3vpnMplsL3VpnVrfTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3vpnMplsL3VpnVrfEntry,
                       MibObject.MplsL3VpnVrfTableNode);

    if ((gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, MplsL3VpnVrfTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnIfConfTableCreate
 Input       :  None
 Description :  This function creates the MplsL3VpnIfConfTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
L3vpnMplsL3VpnIfConfTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3vpnMplsL3VpnIfConfEntry,
                       MibObject.MplsL3VpnIfConfTableNode);

    if ((gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               MplsL3VpnIfConfTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfRTTableCreate
 Input       :  None
 Description :  This function creates the MplsL3VpnVrfRTTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
L3vpnMplsL3VpnVrfRTTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3vpnMplsL3VpnVrfRTEntry,
                       MibObject.MplsL3VpnVrfRTTableNode);

    if ((gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               MplsL3VpnVrfRTTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfSecTableCreate
 Input       :  None
 Description :  This function creates the MplsL3VpnVrfSecTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
L3vpnMplsL3VpnVrfSecTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3vpnMplsL3VpnVrfSecEntry,
                       MibObject.MplsL3VpnVrfSecTableNode);

    if ((gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               MplsL3VpnVrfSecTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfPerfTableCreate
 Input       :  None
 Description :  This function creates the MplsL3VpnVrfPerfTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
L3vpnMplsL3VpnVrfPerfTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3vpnMplsL3VpnVrfPerfEntry,
                       MibObject.MplsL3VpnVrfPerfTableNode);

    if ((gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               MplsL3VpnVrfPerfTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfRteTableCreate
 Input       :  None
 Description :  This function creates the MplsL3VpnVrfRteTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
L3vpnMplsL3VpnVrfRteTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tL3vpnMplsL3VpnVrfRteEntry,
                       MibObject.MplsL3VpnVrfRteTableNode);

    if ((gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               MplsL3VpnVrfRteTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MplsL3VpnVrfTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                MplsL3VpnVrfTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
MplsL3VpnVrfTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3vpnMplsL3VpnVrfEntry *pMplsL3VpnVrfEntry1 =
        (tL3vpnMplsL3VpnVrfEntry *) pRBElem1;
    tL3vpnMplsL3VpnVrfEntry *pMplsL3VpnVrfEntry2 =
        (tL3vpnMplsL3VpnVrfEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pMplsL3VpnVrfEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        i4MaxLength = pMplsL3VpnVrfEntry1->MibObject.i4MplsL3VpnVrfNameLen;
    }
    else
    {
        i4MaxLength = pMplsL3VpnVrfEntry2->MibObject.i4MplsL3VpnVrfNameLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfEntry1->MibObject.au1MplsL3VpnVrfName,
         pMplsL3VpnVrfEntry2->MibObject.au1MplsL3VpnVrfName, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfEntry1->MibObject.au1MplsL3VpnVrfName,
              pMplsL3VpnVrfEntry2->MibObject.au1MplsL3VpnVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfEntry1->MibObject.i4MplsL3VpnVrfNameLen <
             pMplsL3VpnVrfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  MplsL3VpnIfConfTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                MplsL3VpnIfConfTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
MplsL3VpnIfConfTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3vpnMplsL3VpnIfConfEntry *pMplsL3VpnIfConfEntry1 =
        (tL3vpnMplsL3VpnIfConfEntry *) pRBElem1;
    tL3vpnMplsL3VpnIfConfEntry *pMplsL3VpnIfConfEntry2 =
        (tL3vpnMplsL3VpnIfConfEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    i4MaxLength = 0;
    if (pMplsL3VpnIfConfEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnIfConfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        i4MaxLength = pMplsL3VpnIfConfEntry1->MibObject.i4MplsL3VpnVrfNameLen;
    }
    else
    {
        i4MaxLength = pMplsL3VpnIfConfEntry2->MibObject.i4MplsL3VpnVrfNameLen;
    }

    if (MEMCMP
        (pMplsL3VpnIfConfEntry1->MibObject.au1MplsL3VpnVrfName,
         pMplsL3VpnIfConfEntry2->MibObject.au1MplsL3VpnVrfName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnIfConfEntry1->MibObject.au1MplsL3VpnVrfName,
              pMplsL3VpnIfConfEntry2->MibObject.au1MplsL3VpnVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnIfConfEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnIfConfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return 1;
    }
    else if (pMplsL3VpnIfConfEntry1->MibObject.i4MplsL3VpnVrfNameLen <
             pMplsL3VpnIfConfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return -1;
    }

    if (pMplsL3VpnIfConfEntry1->MibObject.i4MplsL3VpnIfConfIndex >
        pMplsL3VpnIfConfEntry2->MibObject.i4MplsL3VpnIfConfIndex)
    {
        return 1;
    }
    else if (pMplsL3VpnIfConfEntry1->MibObject.i4MplsL3VpnIfConfIndex <
             pMplsL3VpnIfConfEntry2->MibObject.i4MplsL3VpnIfConfIndex)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  MplsL3VpnVrfRTTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                MplsL3VpnVrfRTTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
MplsL3VpnVrfRTTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3vpnMplsL3VpnVrfRTEntry *pMplsL3VpnVrfRTEntry1 =
        (tL3vpnMplsL3VpnVrfRTEntry *) pRBElem1;
    tL3vpnMplsL3VpnVrfRTEntry *pMplsL3VpnVrfRTEntry2 =
        (tL3vpnMplsL3VpnVrfRTEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    i4MaxLength = 0;
    if (pMplsL3VpnVrfRTEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfRTEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        i4MaxLength = pMplsL3VpnVrfRTEntry1->MibObject.i4MplsL3VpnVrfNameLen;
    }
    else
    {
        i4MaxLength = pMplsL3VpnVrfRTEntry2->MibObject.i4MplsL3VpnVrfNameLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfRTEntry1->MibObject.au1MplsL3VpnVrfName,
         pMplsL3VpnVrfRTEntry2->MibObject.au1MplsL3VpnVrfName, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfRTEntry1->MibObject.au1MplsL3VpnVrfName,
              pMplsL3VpnVrfRTEntry2->MibObject.au1MplsL3VpnVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRTEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfRTEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRTEntry1->MibObject.i4MplsL3VpnVrfNameLen <
             pMplsL3VpnVrfRTEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRTEntry1->MibObject.u4MplsL3VpnVrfRTIndex >
        pMplsL3VpnVrfRTEntry2->MibObject.u4MplsL3VpnVrfRTIndex)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRTEntry1->MibObject.u4MplsL3VpnVrfRTIndex <
             pMplsL3VpnVrfRTEntry2->MibObject.u4MplsL3VpnVrfRTIndex)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRTEntry1->MibObject.i4MplsL3VpnVrfRTType >
        pMplsL3VpnVrfRTEntry2->MibObject.i4MplsL3VpnVrfRTType)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRTEntry1->MibObject.i4MplsL3VpnVrfRTType <
             pMplsL3VpnVrfRTEntry2->MibObject.i4MplsL3VpnVrfRTType)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  MplsL3VpnVrfSecTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                MplsL3VpnVrfSecTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
MplsL3VpnVrfSecTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3vpnMplsL3VpnVrfSecEntry *pMplsL3VpnVrfSecEntry1 =
        (tL3vpnMplsL3VpnVrfSecEntry *) pRBElem1;
    tL3vpnMplsL3VpnVrfSecEntry *pMplsL3VpnVrfSecEntry2 =
        (tL3vpnMplsL3VpnVrfSecEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pMplsL3VpnVrfSecEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfSecEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        i4MaxLength = pMplsL3VpnVrfSecEntry1->MibObject.i4MplsL3VpnVrfNameLen;
    }
    else
    {
        i4MaxLength = pMplsL3VpnVrfSecEntry2->MibObject.i4MplsL3VpnVrfNameLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfSecEntry1->MibObject.au1MplsL3VpnVrfName,
         pMplsL3VpnVrfSecEntry2->MibObject.au1MplsL3VpnVrfName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfSecEntry1->MibObject.au1MplsL3VpnVrfName,
              pMplsL3VpnVrfSecEntry2->MibObject.au1MplsL3VpnVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfSecEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfSecEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfSecEntry1->MibObject.i4MplsL3VpnVrfNameLen <
             pMplsL3VpnVrfSecEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  MplsL3VpnVrfPerfTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                MplsL3VpnVrfPerfTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
MplsL3VpnVrfPerfTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3vpnMplsL3VpnVrfPerfEntry *pMplsL3VpnVrfPerfEntry1 =
        (tL3vpnMplsL3VpnVrfPerfEntry *) pRBElem1;
    tL3vpnMplsL3VpnVrfPerfEntry *pMplsL3VpnVrfPerfEntry2 =
        (tL3vpnMplsL3VpnVrfPerfEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pMplsL3VpnVrfPerfEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfPerfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        i4MaxLength = pMplsL3VpnVrfPerfEntry1->MibObject.i4MplsL3VpnVrfNameLen;
    }
    else
    {
        i4MaxLength = pMplsL3VpnVrfPerfEntry2->MibObject.i4MplsL3VpnVrfNameLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfPerfEntry1->MibObject.au1MplsL3VpnVrfName,
         pMplsL3VpnVrfPerfEntry2->MibObject.au1MplsL3VpnVrfName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfPerfEntry1->MibObject.au1MplsL3VpnVrfName,
              pMplsL3VpnVrfPerfEntry2->MibObject.au1MplsL3VpnVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfPerfEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfPerfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfPerfEntry1->MibObject.i4MplsL3VpnVrfNameLen <
             pMplsL3VpnVrfPerfEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  MplsL3VpnVrfRteTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                MplsL3VpnVrfRteTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
MplsL3VpnVrfRteTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tL3vpnMplsL3VpnVrfRteEntry *pMplsL3VpnVrfRteEntry1 =
        (tL3vpnMplsL3VpnVrfRteEntry *) pRBElem1;
    tL3vpnMplsL3VpnVrfRteEntry *pMplsL3VpnVrfRteEntry2 =
        (tL3vpnMplsL3VpnVrfRteEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    i4MaxLength = 0;
    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        i4MaxLength = pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfNameLen;
    }
    else
    {
        i4MaxLength = pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfNameLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfRteEntry1->MibObject.au1MplsL3VpnVrfName,
         pMplsL3VpnVrfRteEntry2->MibObject.au1MplsL3VpnVrfName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfRteEntry1->MibObject.au1MplsL3VpnVrfName,
              pMplsL3VpnVrfRteEntry2->MibObject.au1MplsL3VpnVrfName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfNameLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfNameLen <
             pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfNameLen)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrDestType >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrDestType)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.
             i4MplsL3VpnVrfRteInetCidrDestType <
             pMplsL3VpnVrfRteEntry2->MibObject.
             i4MplsL3VpnVrfRteInetCidrDestType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen)
    {
        i4MaxLength =
            pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen;
    }
    else
    {
        i4MaxLength =
            pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfRteEntry1->MibObject.au1MplsL3VpnVrfRteInetCidrDest,
         pMplsL3VpnVrfRteEntry2->MibObject.au1MplsL3VpnVrfRteInetCidrDest,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfRteEntry1->MibObject.au1MplsL3VpnVrfRteInetCidrDest,
              pMplsL3VpnVrfRteEntry2->MibObject.au1MplsL3VpnVrfRteInetCidrDest,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.
             i4MplsL3VpnVrfRteInetCidrDestLen <
             pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrDestLen)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen >
        pMplsL3VpnVrfRteEntry2->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen <
             pMplsL3VpnVrfRteEntry2->MibObject.u4MplsL3VpnVrfRteInetCidrPfxLen)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen)
    {
        i4MaxLength =
            pMplsL3VpnVrfRteEntry1->MibObject.
            i4MplsL3VpnVrfRteInetCidrPolicyLen;
    }
    else
    {
        i4MaxLength =
            pMplsL3VpnVrfRteEntry2->MibObject.
            i4MplsL3VpnVrfRteInetCidrPolicyLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfRteEntry1->MibObject.au4MplsL3VpnVrfRteInetCidrPolicy,
         pMplsL3VpnVrfRteEntry2->MibObject.au4MplsL3VpnVrfRteInetCidrPolicy,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfRteEntry1->MibObject.
              au4MplsL3VpnVrfRteInetCidrPolicy,
              pMplsL3VpnVrfRteEntry2->MibObject.
              au4MplsL3VpnVrfRteInetCidrPolicy, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.
             i4MplsL3VpnVrfRteInetCidrPolicyLen <
             pMplsL3VpnVrfRteEntry2->MibObject.
             i4MplsL3VpnVrfRteInetCidrPolicyLen)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrNHopType)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.
             i4MplsL3VpnVrfRteInetCidrNHopType <
             pMplsL3VpnVrfRteEntry2->MibObject.
             i4MplsL3VpnVrfRteInetCidrNHopType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen)
    {
        i4MaxLength =
            pMplsL3VpnVrfRteEntry1->MibObject.
            i4MplsL3VpnVrfRteInetCidrNextHopLen;
    }
    else
    {
        i4MaxLength =
            pMplsL3VpnVrfRteEntry2->MibObject.
            i4MplsL3VpnVrfRteInetCidrNextHopLen;
    }

    if (MEMCMP
        (pMplsL3VpnVrfRteEntry1->MibObject.au1MplsL3VpnVrfRteInetCidrNextHop,
         pMplsL3VpnVrfRteEntry2->MibObject.au1MplsL3VpnVrfRteInetCidrNextHop,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pMplsL3VpnVrfRteEntry1->MibObject.
              au1MplsL3VpnVrfRteInetCidrNextHop,
              pMplsL3VpnVrfRteEntry2->MibObject.
              au1MplsL3VpnVrfRteInetCidrNextHop, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pMplsL3VpnVrfRteEntry1->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen >
        pMplsL3VpnVrfRteEntry2->MibObject.i4MplsL3VpnVrfRteInetCidrNextHopLen)
    {
        return 1;
    }
    else if (pMplsL3VpnVrfRteEntry1->MibObject.
             i4MplsL3VpnVrfRteInetCidrNextHopLen <
             pMplsL3VpnVrfRteEntry2->MibObject.
             i4MplsL3VpnVrfRteInetCidrNextHopLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  MplsL3VpnVrfTableFilterInputs
 Input       :  The Indices
                pL3vpnMplsL3VpnVrfEntry
                pL3vpnSetMplsL3VpnVrfEntry
                pL3vpnIsSetMplsL3VpnVrfEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MplsL3VpnVrfTableFilterInputs (tL3vpnMplsL3VpnVrfEntry *
                               pL3vpnMplsL3VpnVrfEntry,
                               tL3vpnMplsL3VpnVrfEntry *
                               pL3vpnSetMplsL3VpnVrfEntry,
                               tL3vpnIsSetMplsL3VpnVrfEntry *
                               pL3vpnIsSetMplsL3VpnVrfEntry)
{
    /* Don't filter key-indices: Auto-gen code changed */

    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId == OSIX_TRUE)
    {
        if ((MEMCMP (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
                     pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
                     pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                     i4MplsL3VpnVrfVpnIdLen) == 0)
            && (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen ==
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen))
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId = OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription == OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
              pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfDescription,
              pL3vpnSetMplsL3VpnVrfEntry->MibObject.
              i4MplsL3VpnVrfDescriptionLen) == 0)
            && (pL3vpnMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfDescriptionLen ==
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfDescriptionLen))
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription = OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD == OSIX_TRUE)
    {
        if ((MEMCMP (pL3vpnMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                     pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                     pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                     i4MplsL3VpnVrfRDLen) == 0)
            && (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen ==
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen))
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD = OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMidRteThresh ==
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfMidRteThresh)
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfHighRteThresh ==
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.
            u4MplsL3VpnVrfConfHighRteThresh)
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes ==
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.u4MplsL3VpnVrfConfMaxRoutes)
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus ==
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfRowStatus)
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus ==
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfAdminStatus)
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType ==
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfConfStorageType)
            pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType =
                OSIX_FALSE;
    }
    if ((pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  MplsL3VpnIfConfTableFilterInputs
 Input       :  The Indices
                pL3vpnMplsL3VpnIfConfEntry
                pL3vpnSetMplsL3VpnIfConfEntry
                pL3vpnIsSetMplsL3VpnIfConfEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MplsL3VpnIfConfTableFilterInputs (tL3vpnMplsL3VpnIfConfEntry *
                                  pL3vpnMplsL3VpnIfConfEntry,
                                  tL3vpnMplsL3VpnIfConfEntry *
                                  pL3vpnSetMplsL3VpnIfConfEntry,
                                  tL3vpnIsSetMplsL3VpnIfConfEntry *
                                  pL3vpnIsSetMplsL3VpnIfConfEntry)
{
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex ==
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfIndex)
            pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex = OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnClassification ==
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnClassification)
            pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnIfConfEntry->MibObject.
              au1MplsL3VpnIfVpnRouteDistProtocol,
              pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
              au1MplsL3VpnIfVpnRouteDistProtocol,
              pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
              i4MplsL3VpnIfVpnRouteDistProtocolLen) == 0)
            && (pL3vpnMplsL3VpnIfConfEntry->MibObject.
                i4MplsL3VpnIfVpnRouteDistProtocolLen ==
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                i4MplsL3VpnIfVpnRouteDistProtocolLen))
            pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfConfStorageType ==
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfConfStorageType)
            pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus ==
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnIfConfRowStatus)
            pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName == OSIX_TRUE)
    {
        if ((MEMCMP (pL3vpnMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
                     pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                     au1MplsL3VpnVrfName,
                     pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                     i4MplsL3VpnVrfNameLen) == 0)
            && (pL3vpnMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen ==
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen))
            pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName = OSIX_FALSE;
    }
    if ((pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  MplsL3VpnVrfRTTableFilterInputs
 Input       :  The Indices
                pL3vpnMplsL3VpnVrfRTEntry
                pL3vpnSetMplsL3VpnVrfRTEntry
                pL3vpnIsSetMplsL3VpnVrfRTEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MplsL3VpnVrfRTTableFilterInputs (tL3vpnMplsL3VpnVrfRTEntry *
                                 pL3vpnMplsL3VpnVrfRTEntry,
                                 tL3vpnMplsL3VpnVrfRTEntry *
                                 pL3vpnSetMplsL3VpnVrfRTEntry,
                                 tL3vpnIsSetMplsL3VpnVrfRTEntry *
                                 pL3vpnIsSetMplsL3VpnVrfRTEntry)
{
    /* Dont filter fey indices: Auto-gen code changed */
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT == OSIX_TRUE)
    {
        if ((MEMCMP (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                     pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                     pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                     i4MplsL3VpnVrfRTLen) == 0)
            && (pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen ==
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen))
            pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT = OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr == OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
              pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
              pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
              i4MplsL3VpnVrfRTDescrLen) == 0)
            && (pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen ==
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTDescrLen))
            pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr = OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus ==
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTRowStatus)
            pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType == OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType ==
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTStorageType)
            pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType =
                OSIX_FALSE;
    }

    /* Don't Filter key-indices: Auto-gen code changed */
    if ((pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  MplsL3VpnVrfRteTableFilterInputs
 Input       :  The Indices
                pL3vpnMplsL3VpnVrfRteEntry
                pL3vpnSetMplsL3VpnVrfRteEntry
                pL3vpnIsSetMplsL3VpnVrfRteEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MplsL3VpnVrfRteTableFilterInputs (tL3vpnMplsL3VpnVrfRteEntry *
                                  pL3vpnMplsL3VpnVrfRteEntry,
                                  tL3vpnMplsL3VpnVrfRteEntry *
                                  pL3vpnSetMplsL3VpnVrfRteEntry,
                                  tL3vpnIsSetMplsL3VpnVrfRteEntry *
                                  pL3vpnIsSetMplsL3VpnVrfRteEntry)
{
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrDestType ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrDestType)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
              au1MplsL3VpnVrfRteInetCidrDest,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              au1MplsL3VpnVrfRteInetCidrDest,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              i4MplsL3VpnVrfRteInetCidrDestLen) == 0)
            && (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrDestLen ==
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrDestLen))
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            u4MplsL3VpnVrfRteInetCidrPfxLen ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            u4MplsL3VpnVrfRteInetCidrPfxLen)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
              au4MplsL3VpnVrfRteInetCidrPolicy,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              au4MplsL3VpnVrfRteInetCidrPolicy,
              (INT4) ((pL3vpnSetMplsL3VpnVrfRteEntry->
                       MibObject.i4MplsL3VpnVrfRteInetCidrPolicyLen) *
                      ((INT4) sizeof (UINT4)))) == 0)
            && (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrPolicyLen ==
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrPolicyLen))
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrNHopType ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrNHopType)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
              au1MplsL3VpnVrfRteInetCidrNextHop,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              au1MplsL3VpnVrfRteInetCidrNextHop,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              i4MplsL3VpnVrfRteInetCidrNextHopLen) == 0)
            && (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrNextHopLen ==
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrNextHopLen))
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrIfIndex ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrIfIndex)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrType ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrType)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            u4MplsL3VpnVrfRteInetCidrNextHopAS ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            u4MplsL3VpnVrfRteInetCidrNextHopAS)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric1 ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric1)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric2 ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric2)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric3 ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric3)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric4 ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric4)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric5 ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrMetric5)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer == OSIX_TRUE)
    {
        if ((MEMCMP
             (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfRteXCPointer,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              au1MplsL3VpnVrfRteXCPointer,
              pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
              i4MplsL3VpnVrfRteXCPointerLen) == 0)
            && (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteXCPointerLen ==
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteXCPointerLen))
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus ==
        OSIX_TRUE)
    {
        if (pL3vpnMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrStatus ==
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrStatus)
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus =
                OSIX_FALSE;
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName == OSIX_TRUE)
    {
        if ((MEMCMP (pL3vpnMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
                     pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                     au1MplsL3VpnVrfName,
                     pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                     i4MplsL3VpnVrfNameLen) == 0)
            && (pL3vpnMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen ==
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen))
            pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName = OSIX_FALSE;
    }
    if ((pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType ==
         OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->
            bMplsL3VpnVrfRteInetCidrNextHopAS == OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus ==
            OSIX_FALSE)
        && (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnVrfTableTrigger
 Input       :  The Indices
                pL3vpnSetMplsL3VpnVrfEntry
                pL3vpnIsSetMplsL3VpnVrfEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnVrfTableTrigger (tL3vpnMplsL3VpnVrfEntry *
                                     pL3vpnSetMplsL3VpnVrfEntry,
                                     tL3vpnIsSetMplsL3VpnVrfEntry *
                                     pL3vpnIsSetMplsL3VpnVrfEntry,
                                     INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfNameVal;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfVpnIdVal;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfDescriptionVal;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRDVal;

    MEMSET (gau1MplsL3VpnVrfNameVal, 0, sizeof (gau1MplsL3VpnVrfNameVal));
    MplsL3VpnVrfNameVal.pu1_OctetList = gau1MplsL3VpnVrfNameVal;
    MplsL3VpnVrfNameVal.i4_Length = 0;

    MEMSET (gau1MplsL3VpnVrfVpnIdVal, 0, sizeof (gau1MplsL3VpnVrfVpnIdVal));
    MplsL3VpnVrfVpnIdVal.pu1_OctetList = gau1MplsL3VpnVrfVpnIdVal;
    MplsL3VpnVrfVpnIdVal.i4_Length = 0;

    MEMSET (gau1MplsL3VpnVrfDescriptionVal, 0,
            sizeof (gau1MplsL3VpnVrfDescriptionVal));
    MplsL3VpnVrfDescriptionVal.pu1_OctetList = gau1MplsL3VpnVrfDescriptionVal;
    MplsL3VpnVrfDescriptionVal.i4_Length = 0;

    MEMSET (gau1MplsL3VpnVrfRDVal, 0, sizeof (gau1MplsL3VpnVrfRDVal));
    MplsL3VpnVrfRDVal.pu1_OctetList = gau1MplsL3VpnVrfRDVal;
    MplsL3VpnVrfRDVal.i4_Length = 0;

    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfName == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfNameVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfName,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen);
        MplsL3VpnVrfNameVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfNameLen;

        /* Auto-Gen code: Not calling the nmhSetCmn for the key index */
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfVpnId == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfVpnIdVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfVpnId,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen);
        MplsL3VpnVrfVpnIdVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfVpnIdLen;

        nmhSetCmnNew (MplsL3VpnVrfVpnId, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &MplsL3VpnVrfNameVal, &MplsL3VpnVrfVpnIdVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfDescription == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfDescriptionVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                au1MplsL3VpnVrfDescription,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                i4MplsL3VpnVrfDescriptionLen);
        MplsL3VpnVrfDescriptionVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfDescriptionLen;

        nmhSetCmnNew (MplsL3VpnVrfDescription, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &MplsL3VpnVrfNameVal, &MplsL3VpnVrfDescriptionVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfRD == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRDVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.au1MplsL3VpnVrfRD,
                pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen);
        MplsL3VpnVrfRDVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfEntry->MibObject.i4MplsL3VpnVrfRDLen;

        nmhSetCmnNew (MplsL3VpnVrfRD, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %s",
                      &MplsL3VpnVrfNameVal, &MplsL3VpnVrfRDVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMidRteThresh ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfConfMidRteThresh, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %u",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                      u4MplsL3VpnVrfConfMidRteThresh);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfHighRteThresh ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfConfHighRteThresh, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %u",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                      u4MplsL3VpnVrfConfHighRteThresh);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfMaxRoutes == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfConfMaxRoutes, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %u",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                      u4MplsL3VpnVrfConfMaxRoutes);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfConfRowStatus, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 1, 1, i4SetOption, "%s %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                      i4MplsL3VpnVrfConfRowStatus);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfAdminStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfConfAdminStatus, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                      i4MplsL3VpnVrfConfAdminStatus);
    }
    if (pL3vpnIsSetMplsL3VpnVrfEntry->bMplsL3VpnVrfConfStorageType == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfConfStorageType, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 1, i4SetOption, "%s %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfEntry->MibObject.
                      i4MplsL3VpnVrfConfStorageType);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnIfConfTableTrigger
 Input       :  The Indices
                pL3vpnSetMplsL3VpnIfConfEntry
                pL3vpnIsSetMplsL3VpnIfConfEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnIfConfTableTrigger (tL3vpnMplsL3VpnIfConfEntry *
                                        pL3vpnSetMplsL3VpnIfConfEntry,
                                        tL3vpnIsSetMplsL3VpnIfConfEntry *
                                        pL3vpnIsSetMplsL3VpnIfConfEntry,
                                        INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE MplsL3VpnIfVpnRouteDistProtocolVal;
    UINT1               au1MplsL3VpnIfVpnRouteDistProtocolVal[256];
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfNameVal;
    UINT1               au1MplsL3VpnVrfNameVal[256];

    MEMSET (au1MplsL3VpnIfVpnRouteDistProtocolVal, 0,
            sizeof (au1MplsL3VpnIfVpnRouteDistProtocolVal));
    MplsL3VpnIfVpnRouteDistProtocolVal.pu1_OctetList =
        au1MplsL3VpnIfVpnRouteDistProtocolVal;
    MplsL3VpnIfVpnRouteDistProtocolVal.i4_Length = 0;

    MEMSET (au1MplsL3VpnVrfNameVal, 0, sizeof (au1MplsL3VpnVrfNameVal));
    MplsL3VpnVrfNameVal.pu1_OctetList = au1MplsL3VpnVrfNameVal;
    MplsL3VpnVrfNameVal.i4_Length = 0;

    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnIfConfIndex, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %i %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex);
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnClassification ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnIfVpnClassification, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %i %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfVpnClassification);
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfVpnRouteDistProtocol ==
        OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnIfVpnRouteDistProtocolVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                au1MplsL3VpnIfVpnRouteDistProtocol,
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                i4MplsL3VpnIfVpnRouteDistProtocolLen);
        MplsL3VpnIfVpnRouteDistProtocolVal.i4_Length =
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
            i4MplsL3VpnIfVpnRouteDistProtocolLen;

        nmhSetCmnNew (MplsL3VpnIfVpnRouteDistProtocol, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %i %s",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex,
                      &MplsL3VpnIfVpnRouteDistProtocolVal);
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfStorageType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnIfConfStorageType, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %i %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfStorageType);
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnIfConfRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnIfConfRowStatus, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 1, 2, i4SetOption, "%s %i %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfRowStatus);
    }
    if (pL3vpnIsSetMplsL3VpnIfConfEntry->bMplsL3VpnVrfName == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfNameVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.au1MplsL3VpnVrfName,
                pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen);
        MplsL3VpnVrfNameVal.i4_Length =
            pL3vpnSetMplsL3VpnIfConfEntry->MibObject.i4MplsL3VpnVrfNameLen;

        nmhSetCmnNew (MplsL3VpnVrfName, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %i %s",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnIfConfEntry->MibObject.
                      i4MplsL3VpnIfConfIndex, &MplsL3VpnVrfNameVal);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnVrfRTTableTrigger
 Input       :  The Indices
                pL3vpnSetMplsL3VpnVrfRTEntry
                pL3vpnIsSetMplsL3VpnVrfRTEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnVrfRTTableTrigger (tL3vpnMplsL3VpnVrfRTEntry *
                                       pL3vpnSetMplsL3VpnVrfRTEntry,
                                       tL3vpnIsSetMplsL3VpnVrfRTEntry *
                                       pL3vpnIsSetMplsL3VpnVrfRTEntry,
                                       INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRTVal;
    UINT1               au1MplsL3VpnVrfRTVal[256];
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRTDescrVal;
    UINT1               au1MplsL3VpnVrfRTDescrVal[256];
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfNameVal;
    UINT1               au1MplsL3VpnVrfNameVal[256];

    MEMSET (au1MplsL3VpnVrfRTVal, 0, sizeof (au1MplsL3VpnVrfRTVal));
    MplsL3VpnVrfRTVal.pu1_OctetList = au1MplsL3VpnVrfRTVal;
    MplsL3VpnVrfRTVal.i4_Length = 0;

    MEMSET (au1MplsL3VpnVrfRTDescrVal, 0, sizeof (au1MplsL3VpnVrfRTDescrVal));
    MplsL3VpnVrfRTDescrVal.pu1_OctetList = au1MplsL3VpnVrfRTDescrVal;
    MplsL3VpnVrfRTDescrVal.i4_Length = 0;

    MEMSET (au1MplsL3VpnVrfNameVal, 0, sizeof (au1MplsL3VpnVrfNameVal));
    MplsL3VpnVrfNameVal.pu1_OctetList = au1MplsL3VpnVrfNameVal;
    MplsL3VpnVrfNameVal.i4_Length = 0;

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfName == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfNameVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfName,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen);
        MplsL3VpnVrfNameVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfNameLen;

        /* No need to call nmmCmn for Indices: Auto-gen code changed */
    }

    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTIndex == OSIX_TRUE)
    {
        /* No need to call nmmCmn for Indices: Auto-gen code changed */
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTType == OSIX_TRUE)
    {
        /* No need to call nmmCmn for Indices: Auto-gen code changed */
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRT == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRTVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRT,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen);
        MplsL3VpnVrfRTVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTLen;

        nmhSetCmnNew (MplsL3VpnVrfRT, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %s",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      u4MplsL3VpnVrfRTIndex,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      i4MplsL3VpnVrfRTType, &MplsL3VpnVrfRTVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTDescr == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRTDescrVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.au1MplsL3VpnVrfRTDescr,
                pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                i4MplsL3VpnVrfRTDescrLen);
        MplsL3VpnVrfRTDescrVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.i4MplsL3VpnVrfRTDescrLen;

        nmhSetCmnNew (MplsL3VpnVrfRTDescr, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %s",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      u4MplsL3VpnVrfRTIndex,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      i4MplsL3VpnVrfRTType, &MplsL3VpnVrfRTDescrVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRTRowStatus, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 1, 3, i4SetOption, "%s %u %i %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      u4MplsL3VpnVrfRTIndex,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      i4MplsL3VpnVrfRTType,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      i4MplsL3VpnVrfRTRowStatus);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRTEntry->bMplsL3VpnVrfRTStorageType == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRTStorageType, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 3, i4SetOption, "%s %u %i %i",
                      &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      u4MplsL3VpnVrfRTIndex,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      i4MplsL3VpnVrfRTType,
                      pL3vpnSetMplsL3VpnVrfRTEntry->MibObject.
                      i4MplsL3VpnVrfRTStorageType);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnSetAllMplsL3VpnVrfRteTableTrigger
 Input       :  The Indices
                pL3vpnSetMplsL3VpnVrfRteEntry
                pL3vpnIsSetMplsL3VpnVrfRteEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
L3vpnSetAllMplsL3VpnVrfRteTableTrigger (tL3vpnMplsL3VpnVrfRteEntry *
                                        pL3vpnSetMplsL3VpnVrfRteEntry,
                                        tL3vpnIsSetMplsL3VpnVrfRteEntry *
                                        pL3vpnIsSetMplsL3VpnVrfRteEntry,
                                        INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrDestVal;
    tSNMP_OID_TYPE      MplsL3VpnVrfRteInetCidrPolicyVal;
    UINT4              *pu4MplsL3VpnVrfRteInetCidrPolicyVal = NULL;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteInetCidrNextHopVal;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfRteXCPointerVal;
    tSNMP_OCTET_STRING_TYPE MplsL3VpnVrfNameVal;

    UNUSED_PARAM (pu4MplsL3VpnVrfRteInetCidrPolicyVal);

    pu4MplsL3VpnVrfRteInetCidrPolicyVal =
        (UINT4 *) MemAllocMemBlk (L3VPN_OID_POOLID);

    if (NULL == pu4MplsL3VpnVrfRteInetCidrPolicyVal)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&MplsL3VpnVrfRteInetCidrPolicyVal, 0, sizeof (tSNMP_OID_TYPE));

    MEMSET (gau1MplsL3VpnVrfRteInetCidrDestVal, 0,
            sizeof (gau1MplsL3VpnVrfRteInetCidrDestVal));
    MplsL3VpnVrfRteInetCidrDestVal.pu1_OctetList =
        gau1MplsL3VpnVrfRteInetCidrDestVal;
    MplsL3VpnVrfRteInetCidrDestVal.i4_Length = 0;

    MEMSET (pu4MplsL3VpnVrfRteInetCidrPolicyVal, 0, sizeof (tL3VpnOid));

    MplsL3VpnVrfRteInetCidrPolicyVal.pu4_OidList =
        pu4MplsL3VpnVrfRteInetCidrPolicyVal;

    MplsL3VpnVrfRteInetCidrPolicyVal.u4_Length = 0;

    MEMSET (gau1MplsL3VpnVrfRteInetCidrNextHopVal, 0,
            sizeof (gau1MplsL3VpnVrfRteInetCidrNextHopVal));
    MplsL3VpnVrfRteInetCidrNextHopVal.pu1_OctetList =
        gau1MplsL3VpnVrfRteInetCidrNextHopVal;
    MplsL3VpnVrfRteInetCidrNextHopVal.i4_Length = 0;

    MEMSET (gau1MplsL3VpnVrfRteXCPointerVal, 0,
            sizeof (gau1MplsL3VpnVrfRteXCPointerVal));
    MplsL3VpnVrfRteXCPointerVal.pu1_OctetList = gau1MplsL3VpnVrfRteXCPointerVal;
    MplsL3VpnVrfRteXCPointerVal.i4_Length = 0;

    MEMSET (gau1MplsL3VpnVrfNameVal, 0, sizeof (gau1MplsL3VpnVrfNameVal));
    MplsL3VpnVrfNameVal.pu1_OctetList = gau1MplsL3VpnVrfNameVal;
    MplsL3VpnVrfNameVal.i4_Length = 0;

    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDestType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrDestType, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrDest ==
        OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRteInetCidrDestVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                au1MplsL3VpnVrfRteInetCidrDest,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrDestLen);
        MplsL3VpnVrfRteInetCidrDestVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrDestLen;

        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrDest, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %s", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      &MplsL3VpnVrfRteInetCidrDestVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPfxLen ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrPfxLen, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %u", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrPolicy ==
        OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRteInetCidrPolicyVal.pu4_OidList,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                au4MplsL3VpnVrfRteInetCidrPolicy,
                (INT4) (((pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                          i4MplsL3VpnVrfRteInetCidrPolicyLen) *
                         ((INT4) sizeof (UINT4)))));
        MplsL3VpnVrfRteInetCidrPolicyVal.u4_Length =
            (UINT4) pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrPolicyLen;

        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrPolicy, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %o", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      &MplsL3VpnVrfRteInetCidrPolicyVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNHopType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrNHopType, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHop ==
        OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRteInetCidrNextHopVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                au1MplsL3VpnVrfRteInetCidrNextHop,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteInetCidrNextHopLen);
        MplsL3VpnVrfRteInetCidrNextHopVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteInetCidrNextHopLen;

        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrNextHop, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %s", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      &MplsL3VpnVrfRteInetCidrNextHopVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrIfIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrIfIndex, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrIfIndex);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrType, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrType);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrNextHopAS ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrNextHopAS, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %u", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrNextHopAS);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric1 ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrMetric1, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrMetric1);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric2 ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrMetric2, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrMetric2);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric3 ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrMetric3, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrMetric3);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric4 ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrMetric4, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrMetric4);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrMetric5 ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrMetric5, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrMetric5);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteXCPointer == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfRteXCPointerVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                au1MplsL3VpnVrfRteXCPointer,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                i4MplsL3VpnVrfRteXCPointerLen);
        MplsL3VpnVrfRteXCPointerVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
            i4MplsL3VpnVrfRteXCPointerLen;

        nmhSetCmnNew (MplsL3VpnVrfRteXCPointer, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %s", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      &MplsL3VpnVrfRteXCPointerVal);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfRteInetCidrStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsL3VpnVrfRteInetCidrStatus, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 1, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %i", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrStatus);
    }
    if (pL3vpnIsSetMplsL3VpnVrfRteEntry->bMplsL3VpnVrfName == OSIX_TRUE)
    {
        MEMCPY (MplsL3VpnVrfNameVal.pu1_OctetList,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.au1MplsL3VpnVrfName,
                pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen);
        MplsL3VpnVrfNameVal.i4_Length =
            pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.i4MplsL3VpnVrfNameLen;

        nmhSetCmnNew (MplsL3VpnVrfName, 14, L3vpnMainTaskLock,
                      L3vpnMainTaskUnLock, 0, 0, 7, i4SetOption,
                      "%s %i %s %u %o %i %s %s", &MplsL3VpnVrfNameVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrDestType,
                      &MplsL3VpnVrfRteInetCidrDestVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      u4MplsL3VpnVrfRteInetCidrPfxLen,
                      &MplsL3VpnVrfRteInetCidrPolicyVal,
                      pL3vpnSetMplsL3VpnVrfRteEntry->MibObject.
                      i4MplsL3VpnVrfRteInetCidrNHopType,
                      &MplsL3VpnVrfRteInetCidrNextHopVal, &MplsL3VpnVrfNameVal);
    }

    MemReleaseMemBlock (L3VPN_OID_POOLID,
                        (UINT1 *) pu4MplsL3VpnVrfRteInetCidrPolicyVal);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfTableCreateApi
 Input       :  pL3vpnMplsL3VpnVrfEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tL3vpnMplsL3VpnVrfEntry *
L3vpnMplsL3VpnVrfTableCreateApi (tL3vpnMplsL3VpnVrfEntry *
                                 pSetL3vpnMplsL3VpnVrfEntry)
{
    tL3vpnMplsL3VpnVrfEntry *pL3vpnMplsL3VpnVrfEntry = NULL;

    if (pSetL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfTableCreatApi: pSetL3vpnMplsL3VpnVrfEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pL3vpnMplsL3VpnVrfEntry =
        (tL3vpnMplsL3VpnVrfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pL3vpnMplsL3VpnVrfEntry, pSetL3vpnMplsL3VpnVrfEntry,
                sizeof (tL3vpnMplsL3VpnVrfEntry));
        if (RBTreeAdd
            (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfTable,
             (tRBElem *) pL3vpnMplsL3VpnVrfEntry) != RB_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnMplsL3VpnVrfTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFTABLE_POOLID,
                                (UINT1 *) pL3vpnMplsL3VpnVrfEntry);
            return NULL;
        }
        return pL3vpnMplsL3VpnVrfEntry;
    }
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnIfConfTableCreateApi
 Input       :  pL3vpnMplsL3VpnIfConfEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tL3vpnMplsL3VpnIfConfEntry *
L3vpnMplsL3VpnIfConfTableCreateApi (tL3vpnMplsL3VpnIfConfEntry *
                                    pSetL3vpnMplsL3VpnIfConfEntry)
{
    tL3vpnMplsL3VpnIfConfEntry *pL3vpnMplsL3VpnIfConfEntry = NULL;

    if (pSetL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnIfConfTableCreatApi: pSetL3vpnMplsL3VpnIfConfEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pL3vpnMplsL3VpnIfConfEntry =
        (tL3vpnMplsL3VpnIfConfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID);
    if (pL3vpnMplsL3VpnIfConfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnIfConfTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pL3vpnMplsL3VpnIfConfEntry, pSetL3vpnMplsL3VpnIfConfEntry,
                sizeof (tL3vpnMplsL3VpnIfConfEntry));
        if (RBTreeAdd
            (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnIfConfTable,
             (tRBElem *) pL3vpnMplsL3VpnIfConfEntry) != RB_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnMplsL3VpnIfConfTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNIFCONFTABLE_POOLID,
                                (UINT1 *) pL3vpnMplsL3VpnIfConfEntry);
            return NULL;
        }
        return pL3vpnMplsL3VpnIfConfEntry;
    }
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfRTTableCreateApi
 Input       :  pL3vpnMplsL3VpnVrfRTEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tL3vpnMplsL3VpnVrfRTEntry *
L3vpnMplsL3VpnVrfRTTableCreateApi (tL3vpnMplsL3VpnVrfRTEntry *
                                   pSetL3vpnMplsL3VpnVrfRTEntry)
{
    tL3vpnMplsL3VpnVrfRTEntry *pL3vpnMplsL3VpnVrfRTEntry = NULL;

    if (pSetL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfRTTableCreatApi: pSetL3vpnMplsL3VpnVrfRTEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pL3vpnMplsL3VpnVrfRTEntry =
        (tL3vpnMplsL3VpnVrfRTEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRTEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfRTTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pL3vpnMplsL3VpnVrfRTEntry, pSetL3vpnMplsL3VpnVrfRTEntry,
                sizeof (tL3vpnMplsL3VpnVrfRTEntry));
        if (RBTreeAdd
            (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRTTable,
             (tRBElem *) pL3vpnMplsL3VpnVrfRTEntry) != RB_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnMplsL3VpnVrfRTTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTTABLE_POOLID,
                                (UINT1 *) pL3vpnMplsL3VpnVrfRTEntry);
            return NULL;
        }
        return pL3vpnMplsL3VpnVrfRTEntry;
    }
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfSecTableCreateApi
 Input       :  pL3vpnMplsL3VpnVrfSecEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tL3vpnMplsL3VpnVrfSecEntry *
L3vpnMplsL3VpnVrfSecTableCreateApi (tL3vpnMplsL3VpnVrfSecEntry *
                                    pSetL3vpnMplsL3VpnVrfSecEntry)
{
    tL3vpnMplsL3VpnVrfSecEntry *pL3vpnMplsL3VpnVrfSecEntry = NULL;

    if (pSetL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfSecTableCreatApi: pSetL3vpnMplsL3VpnVrfSecEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pL3vpnMplsL3VpnVrfSecEntry =
        (tL3vpnMplsL3VpnVrfSecEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfSecEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfSecTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pL3vpnMplsL3VpnVrfSecEntry, pSetL3vpnMplsL3VpnVrfSecEntry,
                sizeof (tL3vpnMplsL3VpnVrfSecEntry));
        if (RBTreeAdd
            (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfSecTable,
             (tRBElem *) pL3vpnMplsL3VpnVrfSecEntry) != RB_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnMplsL3VpnVrfSecTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFSECTABLE_POOLID,
                                (UINT1 *) pL3vpnMplsL3VpnVrfSecEntry);
            return NULL;
        }
        return pL3vpnMplsL3VpnVrfSecEntry;
    }
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfPerfTableCreateApi
 Input       :  pL3vpnMplsL3VpnVrfPerfEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tL3vpnMplsL3VpnVrfPerfEntry *
L3vpnMplsL3VpnVrfPerfTableCreateApi (tL3vpnMplsL3VpnVrfPerfEntry *
                                     pSetL3vpnMplsL3VpnVrfPerfEntry)
{
    tL3vpnMplsL3VpnVrfPerfEntry *pL3vpnMplsL3VpnVrfPerfEntry = NULL;

    if (pSetL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfPerfTableCreatApi: pSetL3vpnMplsL3VpnVrfPerfEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pL3vpnMplsL3VpnVrfPerfEntry =
        (tL3vpnMplsL3VpnVrfPerfEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfPerfEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfPerfTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pL3vpnMplsL3VpnVrfPerfEntry, pSetL3vpnMplsL3VpnVrfPerfEntry,
                sizeof (tL3vpnMplsL3VpnVrfPerfEntry));
        if (RBTreeAdd
            (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfPerfTable,
             (tRBElem *) pL3vpnMplsL3VpnVrfPerfEntry) != RB_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnMplsL3VpnVrfPerfTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFPERFTABLE_POOLID,
                                (UINT1 *) pL3vpnMplsL3VpnVrfPerfEntry);
            return NULL;
        }
        return pL3vpnMplsL3VpnVrfPerfEntry;
    }
}

/****************************************************************************
 Function    :  L3vpnMplsL3VpnVrfRteTableCreateApi
 Input       :  pL3vpnMplsL3VpnVrfRteEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tL3vpnMplsL3VpnVrfRteEntry *
L3vpnMplsL3VpnVrfRteTableCreateApi (tL3vpnMplsL3VpnVrfRteEntry *
                                    pSetL3vpnMplsL3VpnVrfRteEntry)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    if (pSetL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfRteTableCreatApi: pSetL3vpnMplsL3VpnVrfRteEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pL3vpnMplsL3VpnVrfRteEntry =
        (tL3vpnMplsL3VpnVrfRteEntry *)
        MemAllocMemBlk (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID);
    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        L3VPN_TRC ((L3VPN_UTIL_TRC,
                    "L3vpnMplsL3VpnVrfRteTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pL3vpnMplsL3VpnVrfRteEntry, pSetL3vpnMplsL3VpnVrfRteEntry,
                sizeof (tL3vpnMplsL3VpnVrfRteEntry));
        if (RBTreeAdd
            (gL3vpnGlobals.L3vpnGlbMib.MplsL3VpnVrfRteTable,
             (tRBElem *) pL3vpnMplsL3VpnVrfRteEntry) != RB_SUCCESS)
        {
            L3VPN_TRC ((L3VPN_UTIL_TRC,
                        "L3vpnMplsL3VpnVrfRteTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (L3VPN_MPLSL3VPNVRFRTETABLE_POOLID,
                                (UINT1 *) pL3vpnMplsL3VpnVrfRteEntry);
            return NULL;
        }
        return pL3vpnMplsL3VpnVrfRteEntry;
    }
}
