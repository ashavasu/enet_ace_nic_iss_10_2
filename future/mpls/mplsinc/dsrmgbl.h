/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dsrmgbl.h,v 1.2 2007/02/01 14:57:05 iss Exp $
 *
 * Description: This file contains the global variables 
 *              declared and used for the MPLS-DS-RM Module.
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : dsrmgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-DS-RM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : contains the global variables declared and 
 *                             used for the MPLS-DS-RM Module. 
 *---------------------------------------------------------------------------*/

#ifndef _DSRMGBL_H
#define _DSRMGBL_H

tDiffServRMGblInfo gDiffServRMGblInfo;

#endif/*_DSRMGBL_H */

