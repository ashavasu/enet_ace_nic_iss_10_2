
/* $Id: inmgrex.h,v 1.12 2016/07/28 07:47:35 siva Exp $ */

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ****************************************************************************
 *    FILE  NAME             : inmgrex.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : L2Vpn Index Tbl Mgr : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains external definitions that
 *                             will be required by other applications, which
 *                             will make use of the index manger.
 *-------------------------------------------------------------------------*/
#ifndef _INDEXMGREX_H
#define _INDEXMGREX_H

/* Index Manager's Miscellaneous Definitions... */
#define L2VPN_PWINDEX_GRPID        1
#define MPLS_INSEGMENT_GROUPID     2 
#define MPLS_OUTSEGMENT_GROUPID    3
#define MPLS_XCTABLE_GROUPID       4
#define MPLS_LBLSTACK_GROUPID      5
#define MPLS_FTN_GROUPID           6
#define LDP_ENTITY_GROUPID         7
#define LDP_FEC_GROUPID            8
#define L2VPN_VPN_GRPID            9
#define MPLS_OAM_MEG_GRPID        10
#define L2VPN_ACID_GRPID          11

/* Append a definition here for every new group (max. 255) to be supported */
     
/* MACROS  _ Used to get Index. */

#define  MplsFtnGetIndex() IndexMgrGetAvailableIndex(MPLS_FTN_GROUPID)

#define  MplsInSegmentGetIndex() IndexMgrGetAvailableIndex(MPLS_INSEGMENT_GROUPID)

#define  MplsOutSegmentGetIndex() IndexMgrGetAvailableIndex(MPLS_OUTSEGMENT_GROUPID)
     
#define  MplsXcTableGetIndex() IndexMgrGetAvailableIndex(MPLS_XCTABLE_GROUPID)

#define  MplsLblStackGetIndex() IndexMgrGetAvailableIndex(MPLS_LBLSTACK_GROUPID)

#define  LdpEntityTableGetIndex() IndexMgrGetAvailableIndex(LDP_ENTITY_GROUPID)

#define  LdpFecTableGetIndex() IndexMgrGetAvailableIndex(LDP_FEC_GROUPID)


/* MACROS  _ Used to set Index . */

#define  MplsFtnSetIndex(u4Index) IndexMgrSetIndex(MPLS_FTN_GROUPID,u4Index)

#define  MplsInSegmentSetIndex(u4Index) IndexMgrSetIndex(MPLS_INSEGMENT_GROUPID,u4Index)

#define  MplsOutSegmentSetIndex(u4Index) IndexMgrSetIndex(MPLS_OUTSEGMENT_GROUPID,u4Index)
     
#define  MplsXCTableSetIndex(u4Index) IndexMgrSetIndex(MPLS_XCTABLE_GROUPID,u4Index)

#define  MplsLblStackSetIndex(u4Index) IndexMgrSetIndex(MPLS_LBLSTACK_GROUPID,u4Index)

#define LdpEntityTableSetIndex(u4Index) IndexMgrSetIndex(LDP_ENTITY_GROUPID,u4Index)

#define LdpFecTableSetIndex(u4Index) IndexMgrSetIndex(LDP_FEC_GROUPID,u4Index)


/* MACROS  _ Used to release Index . */

#define  MplsFtnRelIndex(u4Index) IndexMgrRelIndex(MPLS_FTN_GROUPID,u4Index)

#define  MplsInSegmentRelIndex(u4Index) IndexMgrRelIndex(MPLS_INSEGMENT_GROUPID,u4Index)

#define  MplsOutSegmentRelIndex(u4Index) IndexMgrRelIndex(MPLS_OUTSEGMENT_GROUPID,u4Index)

#define  MplsXcTableRelIndex(u4Index) IndexMgrRelIndex(MPLS_XCTABLE_GROUPID,u4Index)

#define  MplsLblStackRelIndex(u4Index) IndexMgrRelIndex(MPLS_LBLSTACK_GROUPID,u4Index)

#define LdpEntityTableRelIndex(u4Index) IndexMgrRelIndex(LDP_ENTITY_GROUPID,u4Index)

#define LdpFecTableRelIndex(u4Index) IndexMgrRelIndex(LDP_FEC_GROUPID,u4Index)



/* Typedefinitions used for the Index Manager...*/

/* Function prototypes... */

UINT1 IndexMgrInit ARG_LIST ((VOID));
UINT1 IndexManagerInit ARG_LIST ((VOID));
VOID  IndexMgrLock ARG_LIST ((VOID));
VOID  IndexMgrUnLock ARG_LIST ((VOID));
#define MPLS_INDEXMGR_LOCK() IndexMgrLock ()
#define MPLS_INDEXMGR_UNLOCK() IndexMgrUnLock ()
/* L2VPN PW Index management */
#define MplsL2VpnGetPwVcIndex(VOID) \
        IndexMgrGetAvailableIndex (L2VPN_PWINDEX_GRPID)        

#define MplsL2VpnSetPwVcIndex(u4Index) \
        IndexMgrSetIndex(L2VPN_PWINDEX_GRPID, u4Index)

#define MplsL2VpnRelPwVcIndex(u4Index) \
        IndexMgrRelIndex(L2VPN_PWINDEX_GRPID, u4Index) 

#define MplsL2VpnGetVpnId(void) \
        IndexMgrGetAvailableIndex (L2VPN_VPN_GRPID)        

#define MplsL2VpnSetVpnId(u4Index) \
        IndexMgrSetIndex(L2VPN_VPN_GRPID, u4Index)

#define MplsL2VpnRelVpnId(u4Index) \
        IndexMgrRelIndex(L2VPN_VPN_GRPID, u4Index) 

#define MplsL2VpnCheckVpnId(u4Index) \
        IndexMgrCheckIndex(L2VPN_VPN_GRPID, u4Index)

#define MplsOamGetMegId(void) \
            IndexMgrGetAvailableIndex (MPLS_OAM_MEG_GRPID)

#define MplsOamSetMegId(u4Index) \
            IndexMgrSetIndex(MPLS_OAM_MEG_GRPID, u4Index)

#define MplsOamRelMegId(u4Index) \
            IndexMgrRelIndex(MPLS_OAM_MEG_GRPID, u4Index)

#define MplsOamCheckMegId(u4Index) \
            IndexMgrCheckIndex(MPLS_OAM_MEG_GRPID, u4Index)

#define MplsL2VpnGetAcId(void) \
            IndexMgrGetAvailableIndex(L2VPN_ACID_GRPID)

#define MplsL2VpnSetAcId(u4Index) \
            IndexMgrSetIndex(L2VPN_ACID_GRPID, u4Index)

#define MplsL2VpnRelAcId(u4Index) \
            IndexMgrRelIndex(L2VPN_ACID_GRPID, u4Index)

#endif /* _INDEXMGREX_H */

/*------------------------------------------------------------------------
*                        End of file inmgrex.h
-------------------------------------------------------------------------*/
