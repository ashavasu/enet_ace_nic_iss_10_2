/* $Id: mplssize.h,v 1.14 2014/06/18 10:55:18 siva Exp $ */
#ifndef _MPLSSIZE_H
#define _MPLSSIZE_H

#include "size.h"

/**************************** LDP ****************************/

/*
 * NOTE: This value LDP_MAX_INT_LSP indicates the number of route
 * entries for which LSP Establishment can be initiated at a time by LDP.
 */
#define LDP_MAX_INT_LSP          32

/**************************** RSVP-TE ****************************/


#define RSVPTE_MAX_IFACES_DEF_VAL      MAX_MPLS_INTERFACES
#define RSVPTE_MAX_NEIGHBOURS_DEF_VAL  50
#define RSVPTE_NEW_SUBOBJ_DEFVAL       8     /* Per Tunnel */
#define RSVPTE_MAX_TEMP_ARHOP          32
#define RPTE_MAX_MSG_IDS_PER_SREFRESH  400
#define RPTE_MAX_MSG_ID_PER_TNL        8


/**************************** TE ****************************/

/**************************** TC ****************************/

#define TC_MAX_INTERFACES           MAX_MPLS_INTERFACES
#define TC_INTEFACE_MIN    MPLS_INTERFACE_MIN
#define TC_INTEFACE_MAX          MPLS_INTERFACE_MAX

/**************************** L2VPN ****************************/
#define L2VPN_DEF_PWVC_ENTRIES            MAX_L2VPN_PW_VC_ENTRIES
#define L2VPN_DEF_PWVC_MPLS_ENTRIES       FsL2VPNSizingParams \
                                          [MAX_L2VPN_PW_VC_MPLS_ENTRY_SIZING_ID].u4PreAllocatedUnits
#define L2VPN_DEF_MPLS_TNL_ENTRIES        128
#define L2VPN_DEF_MPLS_MAP_ENTRIES        FsL2VPNSizingParams \
                                          [MAX_L2VPN_PW_VC_MPLS_MAPPING_ENTRIES_SIZING_ID].u4PreAllocatedUnits
#define L2VPN_DEF_DORMANT_PWVC_ENTRIES    MAX_L2VPN_PW_VC_DORMANT_ENTRIES
#define L2VPN_DEF_PWVC_ENET_SS_ENTRIES    FsL2VPNSizingParams \
                                          [MAX_L2VPN_ENET_SERV_SPEC_ENTRY_SIZING_ID].u4PreAllocatedUnits 
#define L2VPN_DEF_PWVC_ENET_ENTRIES       FsL2VPNSizingParams \
                                          [MAX_L2VPN_ENET_ENTRIES_SIZING_ID].u4PreAllocatedUnits 
#define L2VPN_DEF_PEER_SSN                MAX_L2VPN_ACTIVE_PEER_SSN_ENTRY
#define L2VPN_DEF_WAITING_PEER_VCS        MAX_L2VPN_PW_VC_LBLMSG_ENTRIES

/**************************** FM ****************************/


#define MPLS_LABELS_PER_IF      MAX_MPLS_LABELS_PER_IF
#define MPLS_DEF_TNL_ENTRIES    32

/**************************** Label Manager ****************************/
/*
 * Group Key Manager's Memory related constants - To be modified if needed
 * at the time of porting.
 */
/* Assumption for Max Label Group Support        *
 * One Entity is associated with one Label Group *
 * (Max no. of Entities) + one for RSVP Group    */

/* DIFF Server */
#define TE_DS_MAX_IFACES MAX_MPLS_INTERFACES

/**************************** LSR Module ****************************/

#endif  /* _MPLSSIZE_H */
