/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mpl3vpncmn.h,v 1.4 2018/01/03 11:31:22 siva Exp $
 *
 ********************************************************************/


#ifndef __MPL3VPNCMN_H__
#define __MPL3VPNCMN_H__
#include "mplsutil.h"

#ifdef MPLS_L3VPN_WANTED


VOID
L3VpnNonTeLspStatusChangeEventHandler(UINT4 u4Label, UINT4 u4Prefix, UINT4 u4EventType);

VOID
L3VpnIfStChgEventHandler (INT4 i4IfIndex, UINT1 u1OperStatus);

INT4
L3vpnCliShowRunningConfig(tCliHandle CliHandle);

INT4
L3vpnCliEntityShowRunningConfig(tCliHandle CliHandle);

INT4
L3vpnCliEntityShowRteRunningConfig(tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName);

INT4
L3vpnCliEntityShowEgressRteRunningConfig(tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pMplsL3VpnVrfName);

#define L3VPN_MPLS_LSP_UP 1
#define L3VPN_MPLS_LSP_DOWN 2


#endif
#endif
