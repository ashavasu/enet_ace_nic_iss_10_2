/********************************************************************
** Copyright (C) 2006 Aricent Inc . All Rights Reserved
**
** $Id: fsmplslw.h,v 1.23 2015/09/15 07:03:07 siva Exp $
**
** Description: Proto types for Low Level  Routines
**********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMplsQosPolicy ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFmDebugLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMplsTeDebugLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsLsrLabelAllocationMethod ARG_LIST((INT4 *));

INT1
nmhGetFsMplsDiffServElspPreConfExpPhbMapIndex ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMplsQosPolicy ARG_LIST((INT4 ));

INT1
nmhSetFsMplsFmDebugLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMplsTeDebugLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsLsrLabelAllocationMethod ARG_LIST((INT4 ));

INT1
nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsQosPolicy ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsFmDebugLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsTeDebugLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsLsrLabelAllocationMethod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsQosPolicy ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFmDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsTeDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLsrLabelAllocationMethod ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsDiffServElspPreConfExpPhbMapIndex ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsLdpEntityTable. */
INT1
nmhValidateIndexInstanceFsMplsLdpEntityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsLdpEntityTable  */

INT1
nmhGetFirstIndexFsMplsLdpEntityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsLdpEntityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsLdpEntityPHPRequestMethod ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMplsLdpEntityTransAddrTlvEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMplsLdpEntityTransportAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityLdpOverRsvpEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMplsLdpEntityOutTunnelIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityOutTunnelInstance ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityOutTunnelIngressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityOutTunnelEgressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityInTunnelIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityInTunnelInstance ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityInTunnelIngressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityInTunnelEgressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMplsLdpEntityIpv6TransportAddrKind ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMplsLdpEntityIpv6TransportAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsLdpEntityBfdStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsLdpEntityPHPRequestMethod ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMplsLdpEntityTransAddrTlvEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMplsLdpEntityTransportAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityLdpOverRsvpEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMplsLdpEntityOutTunnelIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityOutTunnelInstance ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityOutTunnelIngressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityOutTunnelEgressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityInTunnelIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityInTunnelInstance ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityInTunnelIngressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityInTunnelEgressLSRId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLdpEntityIpv6TransAddrTlvEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMplsLdpEntityIpv6TransportAddrKind ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMplsLdpEntityIpv6TransportAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsLdpEntityBfdStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsLdpEntityPHPRequestMethod ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsLdpEntityTransAddrTlvEnable ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsLdpEntityTransportAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityLdpOverRsvpEnable ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsLdpEntityOutTunnelIndex ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityOutTunnelInstance ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityOutTunnelIngressLSRId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityOutTunnelEgressLSRId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityInTunnelIndex ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityInTunnelInstance ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityInTunnelIngressLSRId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityInTunnelEgressLSRId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLdpEntityIpv6TransAddrTlvEnable ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsLdpEntityIpv6TransportAddrKind ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsLdpEntityIpv6TransportAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsLdpEntityBfdStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsLdpEntityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsLdpLsrId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsLdpForceOption ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGrMaxWaitTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLdpGrMaxWaitTime ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsLdpLsrId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsLdpForceOption ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGrMaxWaitTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsLdpGrMaxWaitTime ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsLdpLsrId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsLdpForceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGrMaxWaitTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsLdpGrMaxWaitTime ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsLdpLsrId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLdpForceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGrMaxWaitTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLdpGrMaxWaitTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsMaxIfTableEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxFTNEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxInSegmentEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxOutSegmentEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxXCEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsDiffServElspMapEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsDiffServParamsEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxHopLists ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxPathOptPerList ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxHopsPerPathOption ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxArHopLists ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxHopsPerArHopList ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxRsvpTrfcParams ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxCrLdpTrfcParams ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxDServElsps ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxDServLlsps ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsMaxTnls ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsLsrMaxLdpEntities ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxLocalPeers ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxRemotePeers ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxIfaces ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxLsps ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxVcMergeCount ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxVpMergeCount ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLsrMaxCrlspTnls ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsActiveRsvpTeTnls ARG_LIST((INT4 *));

INT1
nmhGetFsMplsActiveLsps ARG_LIST((INT4 *));

INT1
nmhGetFsMplsActiveCrLsps ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsCrlspDebugLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMplsCrlspDumpType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsCrlspDebugLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMplsCrlspDumpType ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsCrlspDebugLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsCrlspDumpType ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsCrlspDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsCrlspDumpType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsCrlspTnlTable. */
INT1
nmhValidateIndexInstanceFsMplsCrlspTnlTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsCrlspTnlTable  */

INT1
nmhGetFirstIndexFsMplsCrlspTnlTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsCrlspTnlTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsCrlspTnlRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsCrlspTnlStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsCrlspTnlRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsCrlspTnlStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsCrlspTnlRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsCrlspTnlStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsCrlspTnlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsCrlspDumpDirection ARG_LIST((INT4 *));

INT1
nmhGetFsMplsCrlspPersistance ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsCrlspDumpDirection ARG_LIST((INT4 ));

INT1
nmhSetFsMplsCrlspPersistance ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsCrlspDumpDirection ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsCrlspPersistance ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsCrlspDumpDirection ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsCrlspPersistance ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTunnelRSVPResTable. */
INT1
nmhValidateIndexInstanceFsMplsTunnelRSVPResTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTunnelRSVPResTable  */

INT1
nmhGetFirstIndexFsMplsTunnelRSVPResTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTunnelRSVPResTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTunnelRSVPResTokenBucketRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelRSVPResTokenBucketSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelRSVPResPeakDataRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelRSVPResMinimumPolicedUnit ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelRSVPResMaximumPacketSize ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTunnelRSVPResTokenBucketRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelRSVPResTokenBucketSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelRSVPResPeakDataRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelRSVPResMinimumPolicedUnit ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelRSVPResMaximumPacketSize ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTunnelRSVPResTokenBucketRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelRSVPResTokenBucketSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelRSVPResPeakDataRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelRSVPResMinimumPolicedUnit ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelRSVPResMaximumPacketSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTunnelRSVPResTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTunnelCRLDPResTable. */
INT1
nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTunnelCRLDPResTable  */

INT1
nmhGetFirstIndexFsMplsTunnelCRLDPResTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTunnelCRLDPResTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTunnelCRLDPResPeakDataRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelCRLDPResCommittedDataRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelCRLDPResPeakBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelCRLDPResCommittedBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelCRLDPResExcessBurstSize ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTunnelCRLDPResPeakDataRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelCRLDPResCommittedDataRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelCRLDPResPeakBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelCRLDPResCommittedBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelCRLDPResExcessBurstSize ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTunnelCRLDPResPeakDataRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelCRLDPResCommittedDataRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelCRLDPResPeakBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelCRLDPResCommittedBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelCRLDPResExcessBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTunnelCRLDPResTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDiffServElspMapTable. */
INT1
nmhValidateIndexInstanceFsMplsDiffServElspMapTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDiffServElspMapTable  */

INT1
nmhGetFirstIndexFsMplsDiffServElspMapTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDiffServElspMapTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDiffServElspMapPhbDscp ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServElspMapStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDiffServElspMapPhbDscp ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServElspMapStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDiffServElspMapPhbDscp ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServElspMapStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDiffServElspMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDiffServParamsTable. */
INT1
nmhValidateIndexInstanceFsMplsDiffServParamsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDiffServParamsTable  */

INT1
nmhGetFirstIndexFsMplsDiffServParamsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDiffServParamsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDiffServParamsServiceType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServParamsLlspPscDscp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServParamsElspType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServParamsElspSigExpPhbMapIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServParamsStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDiffServParamsServiceType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServParamsLlspPscDscp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServParamsElspType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServParamsElspSigExpPhbMapIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServParamsStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDiffServParamsServiceType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServParamsLlspPscDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServParamsElspType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServParamsElspSigExpPhbMapIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServParamsStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDiffServParamsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDiffServTable. */
INT1
nmhValidateIndexInstanceFsMplsDiffServTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDiffServTable  */

INT1
nmhGetFirstIndexFsMplsDiffServTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDiffServTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDiffServClassType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServServiceType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServLlspPsc ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServElspType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServElspListIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDiffServClassType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServServiceType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServLlspPsc ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServElspType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServElspListIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDiffServClassType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServServiceType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServLlspPsc ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServElspType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServElspListIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDiffServTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDiffServElspInfoTable. */
INT1
nmhValidateIndexInstanceFsMplsDiffServElspInfoTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDiffServElspInfoTable  */

INT1
nmhGetFirstIndexFsMplsDiffServElspInfoTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDiffServElspInfoTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDiffServElspInfoPHB ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServElspInfoResourcePointer ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMplsDiffServElspInfoRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMplsDiffServElspInfoStorageType ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDiffServElspInfoPHB ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServElspInfoResourcePointer ARG_LIST((INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMplsDiffServElspInfoRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMplsDiffServElspInfoStorageType ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDiffServElspInfoPHB ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServElspInfoResourcePointer ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMplsDiffServElspInfoRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMplsDiffServElspInfoStorageType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDiffServElspInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDiffServElspInfoListIndexNext ARG_LIST((INT4 *));

INT1
nmhGetFsMplsTnlModel ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsrcMgmtType ARG_LIST((INT4 *));

INT1
nmhGetFsMplsTTLVal ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTnlModel ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsrcMgmtType ARG_LIST((INT4 ));

INT1
nmhSetFsMplsTTLVal ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTnlModel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsrcMgmtType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsTTLVal ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTnlModel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsrcMgmtType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsTTLVal ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDsTeStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDsTeStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDsTeStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDsTeStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDsTeClassTypeTable. */
INT1
nmhValidateIndexInstanceFsMplsDsTeClassTypeTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDsTeClassTypeTable  */

INT1
nmhGetFirstIndexFsMplsDsTeClassTypeTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDsTeClassTypeTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDsTeClassTypeDescription ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsDsTeClassTypeRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsDsTeClassTypeBwPercentage ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDsTeClassTypeDescription ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsDsTeClassTypeRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsDsTeClassTypeBwPercentage ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDsTeClassTypeDescription ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsDsTeClassTypeRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDsTeClassTypeBwPercentage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDsTeClassTypeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDsTeClassTypeToTcMapTable. */
INT1
nmhValidateIndexInstanceFsMplsDsTeClassTypeToTcMapTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDsTeClassTypeToTcMapTable  */

INT1
nmhGetFirstIndexFsMplsDsTeClassTypeToTcMapTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDsTeClassTypeToTcMapTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDsTeTcType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsDsTeTcDescription ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsDsTeTcMapRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDsTeTcType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsDsTeTcDescription ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsDsTeTcMapRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDsTeTcType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsDsTeTcDescription ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsDsTeTcMapRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDsTeClassTypeToTcMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsDsTeTeClassTable. */
INT1
nmhValidateIndexInstanceFsMplsDsTeTeClassTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsDsTeTeClassTable  */

INT1
nmhGetFirstIndexFsMplsDsTeTeClassTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsDsTeTeClassTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsDsTeTeClassDesc ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsDsTeTeClassNumber ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsDsTeTeClassRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsDsTeTeClassDesc ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsDsTeTeClassNumber ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsDsTeTeClassRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsDsTeTeClassDesc ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsDsTeTeClassNumber ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsDsTeTeClassRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsDsTeTeClassTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsL2VpnTrcFlag ARG_LIST((INT4 *));

INT1
nmhGetFsMplsL2VpnCleanupInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMplsL2VpnAdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsL2VpnTrcFlag ARG_LIST((INT4 ));

INT1
nmhSetFsMplsL2VpnCleanupInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMplsL2VpnAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsL2VpnTrcFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsL2VpnCleanupInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsL2VpnAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsL2VpnTrcFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsL2VpnCleanupInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsL2VpnAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsL2VpnPwTable. */
INT1
nmhValidateIndexInstanceFsMplsL2VpnPwTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsL2VpnPwTable  */

INT1
nmhGetFirstIndexFsMplsL2VpnPwTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsL2VpnPwTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsL2VpnPwMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL2VpnVplsIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL2VpnPwLocalCapabAdvert ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwLocalCCSelected ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwLocalCVSelected ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwLocalCCAdvert ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwLocalCVAdvert ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwRemoteCCAdvert ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwRemoteCVAdvert ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnPwOamEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL2VpnPwGenAGIType ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL2VpnPwGenLocalAIIType ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL2VpnPwGenRemoteAIIType ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL2VpnProactiveOamSsnIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL2VpnPwAIIFormat ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL2VpnIsStaticPw ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL2VpnNextFreePwEnetPwInstance ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL2VpnPwSynchronizationStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsL2VpnPwMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL2VpnVplsIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsL2VpnPwLocalCapabAdvert ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL2VpnPwLocalCCAdvert ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL2VpnPwLocalCVAdvert ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL2VpnPwRemoteCCAdvert ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL2VpnPwRemoteCVAdvert ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL2VpnPwOamEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL2VpnPwGenAGIType ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsL2VpnPwGenLocalAIIType ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsL2VpnPwGenRemoteAIIType ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsL2VpnPwAIIFormat ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsL2VpnPwMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL2VpnVplsIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnPwLocalCapabAdvert ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL2VpnPwLocalCCAdvert ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL2VpnPwLocalCVAdvert ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL2VpnPwRemoteCCAdvert ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL2VpnPwRemoteCVAdvert ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL2VpnPwOamEnable ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL2VpnPwGenAGIType ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnPwGenLocalAIIType ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnPwGenRemoteAIIType ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnPwAIIFormat ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsL2VpnPwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsVplsConfigTable. */
INT1
nmhValidateIndexInstanceFsMplsVplsConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsVplsConfigTable  */

INT1
nmhGetFirstIndexFsMplsVplsConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsVplsConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsVplsVsi ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsVplsVpnId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsVplsName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsVplsDescr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsVplsFdbHighWatermark ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsVplsFdbLowWatermark ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsVplsRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsVplsL2MapFdbId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsmplsVplsMtu ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsmplsVplsStorageType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsmplsVplsSignalingType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsmplsVplsControlWord ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsVplsVsi ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsVplsVpnId ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsVplsName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsVplsDescr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsVplsFdbHighWatermark ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsVplsFdbLowWatermark ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsVplsRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsVplsL2MapFdbId ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsmplsVplsMtu ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsmplsVplsStorageType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsmplsVplsSignalingType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsmplsVplsControlWord ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsVplsVsi ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsVplsVpnId ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsVplsName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsVplsDescr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsVplsFdbHighWatermark ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsVplsFdbLowWatermark ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsVplsRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsVplsL2MapFdbId ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsmplsVplsMtu ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsmplsVplsStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsmplsVplsSignalingType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsmplsVplsControlWord ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsVplsConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPwMplsInboundTable. */
INT1
nmhValidateIndexInstanceFsPwMplsInboundTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPwMplsInboundTable  */

INT1
nmhGetFirstIndexFsPwMplsInboundTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPwMplsInboundTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPwMplsInboundTunnelIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPwMplsInboundTunnelInstance ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPwMplsInboundTunnelEgressLSR ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPwMplsInboundTunnelIngressLSR ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPwMplsInboundTunnelIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsPwMplsInboundTunnelEgressLSR ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPwMplsInboundTunnelIngressLSR ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPwMplsInboundTunnelIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsPwMplsInboundTunnelEgressLSR ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPwMplsInboundTunnelIngressLSR ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPwMplsInboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsLocalCCTypesCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsLocalCVTypesCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRouterID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsHwCCTypeCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsLocalCCTypesCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsLocalCVTypesCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsRouterID ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsLocalCCTypesCapabilities ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsLocalCVTypesCapabilities ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsRouterID ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsLocalCCTypesCapabilities ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLocalCVTypesCapabilities ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRouterID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsPortTable. */
INT1
nmhValidateIndexInstanceFsMplsPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsPortTable  */

INT1
nmhGetFirstIndexFsMplsPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsPortBundleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsPortMultiplexStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsPortAllToOneBundleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsPortRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsPortBundleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsPortMultiplexStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsPortAllToOneBundleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsPortRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsPortBundleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsPortMultiplexStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsPortAllToOneBundleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsVplsAcMapTable. */
INT1
nmhValidateIndexInstanceFsMplsVplsAcMapTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsVplsAcMapTable  */

INT1
nmhGetFirstIndexFsMplsVplsAcMapTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsVplsAcMapTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsVplsAcMapPortIfIndex ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsVplsAcMapPortVlan ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsVplsAcMapRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsVplsAcMapPortIfIndex ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsVplsAcMapPortVlan ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsVplsAcMapRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsVplsAcMapPortIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsVplsAcMapPortVlan ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsVplsAcMapRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsVplsAcMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsL2VpnMaxPwVcEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnMaxPwVcMplsEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnMaxPwVcMplsInOutEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnMaxEthernetPwVcs ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnMaxPwVcEnetEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnActivePwVcEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnActivePwVcMplsEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnActivePwVcEnetEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnNoOfPwVcEntriesCreated ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsL2VpnNoOfPwVcEntriesDeleted ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsL2VpnMaxPwVcEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsL2VpnMaxPwVcMplsEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsL2VpnMaxPwVcMplsInOutEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsL2VpnMaxEthernetPwVcs ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsL2VpnMaxPwVcEnetEntries ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsL2VpnMaxPwVcEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnMaxPwVcMplsEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnMaxPwVcMplsInOutEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnMaxEthernetPwVcs ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsL2VpnMaxPwVcEnetEntries ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsL2VpnMaxPwVcEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsL2VpnMaxPwVcMplsEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsL2VpnMaxPwVcMplsInOutEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsL2VpnMaxEthernetPwVcs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsL2VpnMaxPwVcEnetEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsSimulateFailure ARG_LIST((INT4 *));

INT1
nmhGetFsMplsiTTLVal ARG_LIST((INT4 *));

INT1
nmhGetFsMplsOTTLVal ARG_LIST((INT4 *));

INT1
nmhGetFsMplsprviTTLVal ARG_LIST((INT4 *));

INT1
nmhGetFsMplsprvOTTLVal ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsSimulateFailure ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsSimulateFailure ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsSimulateFailure ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsLdpGrCapability ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLdpGrForwardEntryHoldTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLdpGrMaxRecoveryTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLdpGrNeighborLivenessTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsLdpGrProgressStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsLdpGrCapability ARG_LIST((INT4 ));

INT1
nmhSetFsMplsLdpGrForwardEntryHoldTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsLdpGrMaxRecoveryTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsLdpGrNeighborLivenessTime ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsLdpGrCapability ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsLdpGrForwardEntryHoldTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsLdpGrMaxRecoveryTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsLdpGrNeighborLivenessTime ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsLdpGrCapability ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLdpGrForwardEntryHoldTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLdpGrMaxRecoveryTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsLdpGrNeighborLivenessTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsLdpPeerTable. */
INT1
nmhValidateIndexInstanceFsMplsLdpPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMplsLdpPeerTable  */

INT1
nmhGetFirstIndexFsMplsLdpPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsLdpPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsLdpPeerGrReconnectTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMplsLdpPeerGrRecoveryTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMplsLdpPeerGrProgressStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsLdpConfigurationSequenceTLVEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsLdpConfigurationSequenceTLVEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsLdpConfigurationSequenceTLVEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsLdpConfigurationSequenceTLVEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#ifdef MPLS_L3VPN_WANTED

/* Proto Validate Index Instance for FsMplsL3VpnVrfEgressRteTable. */
INT1
nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsL3VpnVrfEgressRteTable  */

INT1
nmhGetFirstIndexFsMplsL3VpnVrfEgressRteTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsL3VpnVrfEgressRteTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsL3VpnVrfName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrDestType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrDest ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrPfxLen ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrNHopType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrNextHop ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrMetric1 ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsL3VpnVrfEgressRteInetCidrStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsL3VpnVrfName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrDestType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrDest ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrPfxLen ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrNHopType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrNextHop ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrMetric1 ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsL3VpnVrfName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDestType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDest ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrPfxLen ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNHopType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNextHop ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrMetric1 ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsL3VpnVrfEgressRteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif
