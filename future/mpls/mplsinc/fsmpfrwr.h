#ifndef _FSMPFRWR_H
#define _FSMPFRWR_H

VOID RegisterFSMPFR(VOID);

VOID UnRegisterFSMPFR(VOID);
INT4 FsMplsFrrDetourIncomingGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourOutgoingGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourOriginatingGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrSwitchoverGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConfIfsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrActProtectedIfsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConfProtectionTunsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrActProtectionTunsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrActProtectedLSPsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrRevertiveModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourMergingEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrCspfRetryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrCspfRetryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrNotifsEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrMakeAfterBreakEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrRevertiveModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourMergingEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrCspfRetryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrCspfRetryCountSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrNotifsEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrMakeAfterBreakEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrRevertiveModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourMergingEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrDetourEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrCspfRetryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrCspfRetryCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrNotifsEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrMakeAfterBreakEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrRevertiveModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFrrDetourMergingEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFrrDetourEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFrrCspfRetryIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFrrCspfRetryCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFrrNotifsEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFrrMakeAfterBreakEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);







INT4 GetNextIndexFsMplsFrrConstTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsFrrConstIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstProtectionMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstProtectionTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstSetupPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstHoldingPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstSEStyleGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstInclAnyAffinityGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstInclAllAffinityGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstExclAnyAffinityGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstHopLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstProtectionMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstProtectionTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstSetupPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstHoldingPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstSEStyleSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstInclAnyAffinitySet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstInclAllAffinitySet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstExclAnyAffinitySet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstHopLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstProtectionMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstProtectionTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstSetupPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstHoldingPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstSEStyleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstInclAnyAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstInclAllAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstExclAnyAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstHopLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrConstTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);











INT4 GetNextIndexFsMplsTunnelExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTunnelExtProtIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtProtectionTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtBkpTunIdxGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtBkpInstGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtBkpIngrLSRIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtBkpEgrLSRIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtOne2OnePlrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtOne2OnePlrSenderAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtOne2OnePlrSenderAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtOne2OnePlrAvoidNAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtOne2OnePlrAvoidNAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtDetourActiveGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtDetourMergingGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtFacRouteDBProtTunStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtFacRouteDBProtTunResvBwGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtProtectionMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtMaxGblRevertTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtMaxGblRevertTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtMaxGblRevertTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMplsBypassTunnelIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsBypassTunnelRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsBypassTunnelRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsBypassTunnelRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsBypassTunnelIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMplsFrrTunARHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsFrrTunARHopProtectTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrTunARHopProtectTypeInUseGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrTunARHopLabelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFrrTunARBwProtAvailableGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMPFRWR_H */
