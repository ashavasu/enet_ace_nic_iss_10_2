/********************************************************************
 *                                                                  *
 * $Id: rpteext.h,v 1.19 2018/01/03 11:31:22 siva Exp $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteext.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Api's and definitions that 
 *                             are exported to the the Other modules.
 *                             
 *---------------------------------------------------------------------------*/
#ifndef _RPTEEXT_H
#define _RPTEEXT_H

#include "teextrn.h"

#define RPTE_SUCCESS              1
#define RPTE_FAILURE              0

/* TRIE related definitions */
#define  RSVPTE_TNL_TYPE      5    /* This must be assigned so that it does not 
                                   clash with other instances - currently 
                                   marked as 5, since 1 is used for IP
                                   Routing protocols, 2 and 3 for FTN and ILM
                                   by MPLS-FM in MPLS */

/* RSVPTE Lock and Unlock */
#define MPLS_RSVPTE_SEM_NAME (UINT1 *)"RPTS"
#define RSVPTE_SYNC_SEM "RPSS"

#ifdef MPLS_RSVPTE_WANTED
#define MPLS_RSVPTE_LOCK( ) RsvpTeLock()
#define MPLS_RSVPTE_UNLOCK( ) RsvpTeUnLock()
#else
#define MPLS_RSVPTE_LOCK( )
#define MPLS_RSVPTE_UNLOCK( )
#endif
#define MplsRpteL3VPNEventHandler     RpteL3VPNEventHandler
INT4  RsvpTeLock (VOID);
INT4  RsvpTeUnLock (VOID);

VOID RpteDeInitRsvpTe ARG_LIST((VOID));
UINT1 RpteInitRsvpTe ARG_LIST((VOID));
INT4
MplsHandlePktForByPassTnl (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4XcIndex);
VOID
RpteL2VpnPostEventForFrr (tTeTnlInfo * pTeTearTnlInfo);
VOID
RpteCheckTnlFrrEntryInFTN (tTeTnlInfo * pTeTnlInfo, UINT4 u4BkpTnlInst, 
                           BOOL1 bIsLocalRevert);
extern UINT1        RpteL3VPNEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo));
VOID 
RsvpeTeDeleteMemPools (VOID);
UINT1 RpteProcessTeGoingDownEvent ARG_LIST ((VOID));
#endif /* _RPTEEXT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rpteext.h                              */
/*---------------------------------------------------------------------------*/

