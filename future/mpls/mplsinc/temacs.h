
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: temacs.h,v 1.44 2017/06/08 11:40:31 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : temacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains the macro definations 
 *                             used in TE module.   
 *---------------------------------------------------------------------------*/

#ifndef _TEMACS_H
#define _TEMACS_H

#define TE_TRUE                       1
#define TE_FALSE                      0

#define TE_SIG_NOT_DONE    0
#define TE_SIG_DONE    1
#define TE_SIG_AWAITED 2
/* Macro to support CLI Display */
#define TE_CLI_ADDR   16

/* Macro to suppress warnings */
#define TE_SUPPRESS_WARNING(x) UNUSED_PARAM(x);

/* Tunnel Table related */
#define TE_TNL_TNL_INDEX(pTeTnlInfo) \
            pTeTnlInfo->u4TnlIndex

#define TE_TNL_TNL_INSTANCE(pTeTnlInfo) \
            (pTeTnlInfo)->u4TnlInstance

#define TE_TNL_INGRESS_LSRID(pTeTnlInfo) \
            pTeTnlInfo->TnlIngressLsrId

#define TE_TNL_EGRESS_LSRID(pTeTnlInfo) \
            pTeTnlInfo->TnlEgressLsrId

#define TE_TNL_PRIMARY_TNL_INSTANCE(pTeTnlInfo) \
            (pTeTnlInfo)->u4TnlPrimaryInstance

#define TE_TNL_BKP_TNL_INSTANCE(pTeTnlInfo) \
            (pTeTnlInfo)->u4BkpTnlInstance

#define TE_TNL_NAME(pTeTnlInfo) \
            pTeTnlInfo->au1TnlName

#define TE_TNL_DESCR(pTeTnlInfo) \
            pTeTnlInfo->au1TnlDescr

#define TE_TNL_ISIF(pTeTnlInfo) \
            pTeTnlInfo->u1TnlIsIf

#define TE_TNL_IFINDEX(pTeTnlInfo) \
            pTeTnlInfo->u4TnlIfIndex

#define TE_TNL_XCINDEX(pTeTnlInfo) \
            pTeTnlInfo->u4TnlXcIndex

#define TE_TNL_SIGPROTO(pTeTnlInfo) \
            pTeTnlInfo->u1TnlSgnlPrtcl

#define TE_TNL_SETUP_PRIO(pTeTnlInfo) \
            pTeTnlInfo->u1TnlSetPrio

#define TE_TNL_HLDNG_PRIO(pTeTnlInfo) \
            pTeTnlInfo->u1TnlHoldPrio

#define TE_TNL_SRLG_TYPE(pTeTnlInfo) \
            pTeTnlInfo->u1TnlSrlgType

#define TE_TNL_SRLG_LIST(pTeTnlInfo) \
            &pTeTnlInfo->SrlgList

#define TE_TNL_SSN_ATTR(pTeTnlInfo) \
            pTeTnlInfo->u1TnlSsnAttr

#define TE_TNL_OWNER(pTeTnlInfo) \
            pTeTnlInfo->u1TnlOwner

#define TE_TNL_INST_PRIO(pTeTnlInfo) \
            pTeTnlInfo->u4TnlInstancePrio

#define TE_TNL_LOCAL_PROTECTION_IN_USE(pTeTnlInfo) \
           pTeTnlInfo->u1TnlLocalProtectInUse  

#define TE_TNL_HOP_TABLE_INDEX(pTeTnlInfo) \
           pTeTnlInfo->u4TnlHopTableIndex

#define TE_TNL_BACKUPHOP_TABLE_INDEX(pTeTnlInfo) \
           pTeTnlInfo->u4TnlBackupHopTableIndex

#define TE_TNL_ARHOP_TABLE_INDEX(pTeTnlInfo) \
           pTeTnlInfo->u4TnlARHopTableIndex

#define TE_TNL_PATH_IN_USE(pTeTnlInfo) \
         pTeTnlInfo->u4TnlPathInUse

#define TE_TNL_BACKUPPATH_IN_USE(pTeTnlInfo) \
         pTeTnlInfo->u4TnlBackupPathInUse

#define TE_TNL_ROLE(pTeTnlInfo) \
           pTeTnlInfo->u1TnlRole

#define TE_TNL_REOPTIMIZE_STATUS(pTeTnlInfo) \
   pTeTnlInfo->u1TnlReoptimizeStatus

#define TE_TNL_REOPTIMIZE_TRIGGER(pTeTnlInfo) \
    pTeTnlInfo->bIsReoptimizationTriggered

#define TE_TNL_IN_USE_BY_VPN(pTeTnlInfo) \
           pTeTnlInfo->u1TnlInUseByVPN

#define TE_TNL_STORAGE_TYPE(pTeTnlInfo) \
          pTeTnlInfo->u1TnlStorageType

#define TE_TNL_ADMIN_STATUS(pTeTnlInfo) \
           (pTeTnlInfo)->u1TnlAdminStatus

#define TE_TNL_OPER_STATUS(pTeTnlInfo) \
           pTeTnlInfo->u1TnlOperStatus

#define TE_TNL_SIG_STATE(pTeTnlInfo) \
           pTeTnlInfo->u1SigState

#define TE_TNL_ARP_STATUS(pTeTnlInfo) \
           pTeTnlInfo->u1ArpResolveStatus

#define TE_TNL_FTN_LIST(pTeTnlInfo) \
           (pTeTnlInfo)->FtnList

#define TE_TNL_ROW_STATUS(pTeTnlInfo) \
           (pTeTnlInfo)->u1TnlRowStatus

#define TE_TNL_TRFC_PARAM_INDEX(pTeTnlInfo) \
           pTeTnlInfo->u4ResourceIndex

#define TE_TNL_TRFC_PARAM(pTeTnlInfo) \
           pTeTnlInfo->pTeTrfcParams

#define TE_LDP_OVER_RSVP_ENT_INDEX(pTeTnlInfo) \
           pTeTnlInfo->u4LdpOverRsvpEntIndex

#define TE_TNL_PATH_INFO(pTeTnlInfo) \
           pTeTnlInfo->pTePathInfo

#define TE_TNL_BACKUPPATH_INFO(pTeTnlInfo) \
           pTeTnlInfo->pTeBackupPathInfo

#define TE_TNL_PATH_LIST_INFO(pTeTnlInfo) \
           pTeTnlInfo->pTePathListInfo

#define TE_HOP_LIST(pTeTnlInfo) \
        ((TE_TNL_PATH_INFO(pTeTnlInfo))->ErHopList)
        
#define TE_IN_ARHOP_LIST(pTeTnlInfo) \
        ((TE_IN_AR_HOP_LIST_INFO(pTeTnlInfo))->ArHopList)

#define TE_OUT_ARHOP_LIST(pTeTnlInfo) \
        ((TE_OUT_AR_HOP_LIST_INFO(pTeTnlInfo))->ArHopList)

#define TE_IN_AR_HOP_LIST_INFO(pTeTnlInfo) \
             pTeTnlInfo->pTeInArHopListInfo

#define TE_OUT_AR_HOP_LIST_INFO(pTeTnlInfo) \
             pTeTnlInfo->pTeOutArHopListInfo

#define TE_TNL_FRR_CONST_INFO(pTeTnlInfo) \
             pTeTnlInfo->pTeFrrConstInfo

#define TE_TNL_TPARAM_INDEX(pTeTnlInfo) \
        TE_TNLRSRC_INDEX(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_PDR(pTeTnlInfo) \
        TE_CRLDP_TPARAM_PDR(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_PBS(pTeTnlInfo) \
        TE_CRLDP_TPARAM_PBS(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_CDR(pTeTnlInfo) \
        TE_CRLDP_TPARAM_CDR(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_CBS(pTeTnlInfo) \
        TE_CRLDP_TPARAM_CBS(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_EBS(pTeTnlInfo) \
        TE_CRLDP_TPARAM_EBS(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_FREQ(pTeTnlInfo) \
        TE_CRLDP_TPARAM_FREQ(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_WEIGHT(pTeTnlInfo) \
        TE_CRLDP_TPARAM_WEIGHT(TE_TNL_TRFC_PARAM(pTeTnlInfo))
#define TE_TNL_CTPARAM_FLAGS(pTeTnlInfo) \
        TE_CRLDP_TPARAM_FLAGS(TE_TNL_TRFC_PARAM(pTeTnlInfo))

#define TE_TNL_CTPARAM_PTR(pTeTnlInfo) \
        TE_CRLDP_TPARAM_PTR(TE_TNL_TRFC_PARAM(pTeTnlInfo))

#define TE_TNL_INC_ANY_AFFINITY(x) \
        (x)->u4TnlIncludeAnyAffinity
#define TE_TNL_INC_ALL_AFFINITY(x) \
        (x)->u4TnlIncludeAllAffinity
#define TE_TNL_EXC_ANY_AFFINITY(x) \
        (x)->u4TnlExcludeAnyAffinity
#define TE_TNL_LBL_REC(x) \
        (x)->u1LblRec

#define TE_TNL_STATE_CHG_COUNT(pTeTnlInfo) \
         pTeTnlInfo->u4TnlStateTransitions

#define TE_TNL_CREATE_TIME(pTeTnlInfo) \
          pTeTnlInfo->u4TnlCreationTime

#define TE_TNL_LAST_PATH_CHG_TIME(pTeTnlInfo) \
          pTeTnlInfo->u4TnlLastPathChange
   
#define TE_TNL_LAST_PATH_CHG_COUNT(pTeTnlInfo) \
          pTeTnlInfo->u4TnlPathChanges

#define TE_TNL_TOTAL_UP_TIME(pTeTnlInfo) \
   pTeTnlInfo->u4TnlTotalUpTime
   
#define TE_TNL_INSTANCE_UP_TIME(pTeTnlInfo) \
   pTeTnlInfo->u4TnlInstanceUpTime

#define TE_TNL_PRIMARY_INSTANCE_UP_TIME(pTeTnlInfo) \
   pTeTnlInfo->u4TnlPrimaryTimeUp

/* MPLS_P2MP_LSP_CHANGES - S */
/* P2MP structure information */
#define TE_P2MP_TNL_INFO(pTeTnlInfo) \
    pTeTnlInfo->pTeP2mpTnlInfo

#define TE_P2MP_TNL_BRANCH_LIST_INFO(pTeTnlInfo) \
    (TE_P2MP_TNL_INFO(pTeTnlInfo))->TeP2mpBranchEntry

#define TE_P2MP_TNL_DEST_LIST_INFO(pTeTnlInfo) \
    (TE_P2MP_TNL_INFO(pTeTnlInfo))->TeP2mpDestEntry

#define TE_P2MP_TNL_ACTIVE_BRANCH_COUNT(pTeTnlInfo) \
    (TE_P2MP_TNL_INFO(pTeTnlInfo))->u2NumOfActiveBranch

/* P2MP Tunnel structure related */
#define TE_P2MP_TNL_INTEGRITY(pTeP2mpTnlInfo) \
        pTeP2mpTnlInfo->bP2mpTnlIntegrity

#define TE_P2MP_TNL_BRANCH_ROLE(pTeP2mpTnlInfo) \
        pTeP2mpTnlInfo->u1P2mpTnlBranchRole

/* P2MP Branch performance structure related */
#define TE_P2MP_BRANCH_OUTSEG_INDEX(pP2mpBranchEntry) \
        pP2mpBranchEntry->u4P2mpOutSegmentIndex

#define TE_P2MP_BRANCH_DEST_COUNT(pP2mpBranchEntry) \
        pP2mpBranchEntry->u2DestUsageCount

#define TE_P2MP_BRANCH_ACTIVE_DEST_COUNT(pP2mpBranchEntry) \
        pP2mpBranchEntry->u2ActiveDestCount

/* P2MP Destination entry structure related */
#define TE_P2MP_DEST_SRC_SUBGRP_ORIGTYPE(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpSrcSubGrpAddrType

#define TE_P2MP_DEST_SRC_SUBGRP_ORIGIN(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->P2mpSrcSubGrpAddr

#define TE_P2MP_DEST_SRC_SUBGRP_ID(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u2P2mpSrcSubGrpId

#define TE_P2MP_DEST_SUBGRP_ORIGTYPE(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpSubGrpAddrType

#define TE_P2MP_DEST_SUBGRP_ORIGIN(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->P2mpSubGrpAddr

#define TE_P2MP_DEST_SUBGRP_ID(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u2P2mpSubGrpId

#define TE_P2MP_DEST_ADDR_TYPE(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpDestAddrType

#define TE_P2MP_DEST_ADDRESS(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->P2mpDestLsrId

#define TE_P2MP_DEST_BRANCH_OUTSEG_INDEX(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestBranchOutSegIndex

#define TE_P2MP_DEST_HOP_TABLE_INDEX(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestHopTableIndex

#define TE_P2MP_DEST_PATH_IN_USE(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestPathInUse

#define TE_P2MP_DEST_CHOP_TABLE_INDEX(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestCHopTableIndex

#define TE_P2MP_DEST_ARHOP_TABLE_INDEX(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestARHopTableIndex

#define TE_P2MP_DEST_TOTAL_UP_TIME(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestTotalUpTime

#define TE_P2MP_DEST_INSTANCE_UP_TIME(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestInstanceUpTime

#define TE_P2MP_DEST_PATH_CHG(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestPathChanges

#define TE_P2MP_DEST_LAST_PATH_CHG(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestLastPathChange

#define TE_P2MP_DEST_CREATE_TIME(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestCreationTime

#define TE_P2MP_DEST_STATE_CHG_COUNT(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestStateTransitions

#define TE_P2MP_DEST_DISCONTINUITY_TIME(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u4P2mpDestDiscontinuityTime

#define TE_P2MP_DEST_ADMIN_STATUS(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpDestAdminStatus

#define TE_P2MP_DEST_OPER_STATUS(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpDestOperStatus

#define TE_P2MP_DEST_ROW_STATUS(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpDestRowStatus

#define TE_P2MP_DEST_STORAGE_TYPE(pTeP2mpDestEntry) \
        pTeP2mpDestEntry->u1P2mpDestStorageType
/* MPLS_P2MP_LSP_CHANGES - E */

/* Path List Structure related */
#define TE_HOPLIST_INDEX(pTePathListInfo)    (pTePathListInfo->u4HopListIndex)
#define TE_HOP_ROLE(pTePathListInfo)         (pTePathListInfo->u1HopRole)
#define TE_HOP_PO_LIST(pTePathListInfo)      &(pTePathListInfo->PathOptionList)
#define TE_HOP_PO_COUNT(pTePathListInfo) \
        (pTePathListInfo->u1PathOptionCount)

#define TE_PATH_LIST_INDEX(x) \
           (gTeGblInfo.pPathListEntry[(x-1)].u4HopListIndex)
#define TE_PATH_OPTION_LIST(x) \
          &(gTeGblInfo.pPathListEntry[(x-1)].PathOptionList)
#define TE_PATH_HOP_ROLE(x) \
           (gTeGblInfo.pPathListEntry[(x-1)].u1HopRole)
#define TE_PATH_OPTION_COUNT(x) \
           (gTeGblIno.pPathListEntry[(x-1)].u1PathOptionCount)

/* Path Info Structure related */
#define TE_PO_NEXT_NODE(pTePathInfo)         (pTePathInfo->NextPathOption)
#define TE_PO_INDEX(pTePathInfo)             (pTePathInfo->u4PathOptionIndex)
#define TE_PO_NUM_TUNNELS(pTePathInfo)       (pTePathInfo->u2NumOfTunnels) 
#define TE_PO_HOP_COUNT(pTePathInfo)         (pTePathInfo->u2ErHopCount)
#define TE_PO_HOP_LIST(pTePathInfo)          &(pTePathInfo->ErHopList)

/* Hop Table related */
#define TE_ERHOP_NEXT_NODE(pTeHopInfo)       (pTeHopInfo->NextHop)
#define TE_ERHOP_INDEX(pTeHopInfo)           (pTeHopInfo->u4TnlHopIndex)
#define TE_ERHOP_ADDR_TYPE(pTeHopInfo)       (pTeHopInfo->u1AddressType)
#define TE_ERHOP_IP_ADDR(pTeHopInfo)         (pTeHopInfo->IpAddr)
#define TE_ERHOP_IPV4_ADDR(pTeHopInfo) \
        (TE_ERHOP_IP_ADDR(pTeHopInfo).au1Ipv4Addr)
#define TE_ERHOP_ADDR_PRFX_LEN(pTeHopInfo)   (pTeHopInfo->u1AddrPrefixLen)
#define TE_ERHOP_LSPID(pTeHopInfo)           (pTeHopInfo->u4LocalLspId)
#define TE_ERHOP_AS_NUM(pTeHopInfo)          (pTeHopInfo->u2AsNumber)
#define TE_ERHOP_TYPE(pTeHopInfo)            (pTeHopInfo->u1HopType)
#define TE_ERHOP_ROW_STATUS(pTeHopInfo)      (pTeHopInfo->u1RowStatus)
#define TE_ERHOP_STORAGE_TYPE(pTeHopInfo)    (pTeHopInfo->u1StorageType)
#define TE_ERHOP_INCLD_EXCLD(pTeHopInfo)     (pTeHopInfo->u1HopIncludeExclude)
#define TE_ERHOP_PATH_COMP(pTeHopInfo)     (pTeHopInfo->u1HopEntryPathComp)

/* ArHop List table */
#define TE_ARHOP_LISTINFO(x) \
                      (tTeArHopListInfo *)&(gTeGblInfo.pArHopListEntry[(x-1)])

#define TE_ARHOP_LIST(x) \
                      &(gTeGblInfo.pArHopListEntry[(x-1)].ArHopList)

#define TE_ARHOP_LIST_INDEX(x) \
                      (gTeGblInfo.pArHopListEntry[(x-1)].u4ArHopListIndex)

#define TE_ARHOP_LIST_ROLE(x) \
                      (gTeGblInfo.pArHopListEntry[(x-1)].u4ArHopRole)

/* ArHop Table */
#define TE_ARHOP_INDEX(pTeArHopInfo)           (pTeArHopInfo->u4TnlArHopIndex)
#define TE_ARHOP_ADDR_TYPE(pTeArHopInfo)       (pTeArHopInfo->u1AddressType)
#define TE_ARHOP_IP_ADDR(pTeArHopInfo)         (pTeArHopInfo->IpAddr)
#define TE_ARHOP_AS_NUM(pTeArHopInfo)          (pTeArHopInfo->u2AsNumber)
#define TE_ARHOP_LSPID(pTeArHopInfo)           (pTeArHopInfo->au1LocalLspId)
#define TE_ARHOP_FLAG(pTeArHopInfo)            (pTeArHopInfo->u1Flags)
#define TE_ARHOP_ADDR_PRFX_LEN(pTeArHopInfo)   (pTeArHopInfo->u1AddrPrefixLen)

/* FRR Related ARHop Table */
#define TE_FRR_ARHOP_PROT_TYPE(pTeArHopInfo)          (pTeArHopInfo->u1ProtType)
#define TE_FRR_ARHOP_PROT_TYPE_IN_USE(pTeArHopInfo)   (pTeArHopInfo->u1ProtTypeInUse)
#define TE_FRR_ARHOP_LABEL(pTeArHopInfo)              (pTeArHopInfo->u4Label)
#define TE_FRR_ARHOP_BW_PROT_AVAIL(pTeArHopInfo)      (pTeArHopInfo->u1BwProtAvailable)


/* Traffic Param Table related */
#define TE_CRLDP_TRFC_PARAMS_PTR(x) \
        (gTeGblInfo.pTrfcParams[x - 1].pCRLDPTrfcParams)
#define TE_RSVP_TRFC_PARAMS_PTR(x) \
        (gTeGblInfo.pTrfcParams[x - 1].pRSVPTrfcParams)
#define TE_TRFC_PARAMS_TNL_RESINDEX(x) \
        (gTeGblInfo.pTrfcParams[x - 1].u4TrfcParamIndex)
#define TE_TRFC_PARAMS_TNL_RESROWSTATUS(x) \
        (gTeGblInfo.pTrfcParams[x - 1].u1RowStatus)

#define TE_TNLRSRC_INDEX(pTeTrfcParams) \
        pTeTrfcParams->u4TrfcParamIndex
#define TE_TNLRSRC_ROW_STATUS(pTeTrfcParams) \
        pTeTrfcParams->u1RowStatus
#define TE_TNLRSRC_MAX_RATE(pTeTrfcParams) \
        pTeTrfcParams->u4ResMaxRate
#define TE_TNLRSRC_STORAGE_TYPE(pTeTrfcParams) \
        pTeTrfcParams->u1StorageType
#define TE_CRLDP_TRFC_PARAMS(pTeTrfcParams) \
        pTeTrfcParams->pCRLDPTrfcParams
#define TE_RSVPTE_TRFC_PARAMS(pTeTrfcParams) \
        pTeTrfcParams->pRSVPTrfcParams
#define TE_TNLRSRC_NUM_OF_TUNNELS(pTeTrfcParams) \
        pTeTrfcParams->u2NumOfTunnels
#define TE_TNLRSRC_ROLE(pTeTrfcParams) \
        pTeTrfcParams->u1TrfcParamRole
#define TE_TNLRSRC_OWNER(pTeTrfcParams) \
        pTeTrfcParams->u1TrfcParamOwner

/* CRLDP Traffic Param Table related */
#define TE_CRLDP_TPARAM_PDR(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u4PeakDataRate
#define TE_CRLDP_TPARAM_PBS(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u4PeakBurstSize
#define TE_CRLDP_TPARAM_CDR(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u4CommittedDataRate
#define TE_CRLDP_TPARAM_CBS(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u4CommittedBurstSize
#define TE_CRLDP_TPARAM_EBS(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u4ExcessBurstSize
#define TE_CRLDP_TPARAM_FREQ(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u1Frequency
#define TE_CRLDP_TPARAM_WEIGHT(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u1Weight
#define TE_CRLDP_TPARAM_FLAGS(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u1Flags
#define TE_CRLDP_TPARAM_CFG_FLAGS(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u2CfgFlag
#define TE_CRLDP_TPARAM_ROW_STATUS(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u1RowStatus
#define TE_CRLDP_TPARAM_STORAGE_TYPE(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams->u1StorageType
        
#define TE_CRLDP_TPARAM_PTR(pTeTrfcParam) \
        pTeTrfcParam->pCRLDPTrfcParams
/* RSVPTE Traffic Param Table related */

#define TE_RSVPTE_TPARAM_TBR(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u4TokenBktRate
#define TE_RSVPTE_TPARAM_TBS(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u4TokenBktSize
#define TE_RSVPTE_TPARAM_PDR(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u4PeakDataRate
#define TE_RSVPTE_TOLDPARAM_PDR(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u4OldPeakDataRate
#define TE_RSVPTE_TPARAM_MPU(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u4MinPolicedUnit
#define TE_RSVPTE_TPARAM_MPS(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u4MaxPktSize
#define TE_RSVPTE_TPARAM_CFG_FLAG(pTeTrfcParam) \
        pTeTrfcParam->pRSVPTrfcParams->u2CfgFlag

/* Global Structure related */
#define TE_TNL_HASH_TBL(gTeGblInfo) ((gTeGblInfo).pTeTnlHashTable)
#define TE_PATH_LIST_INFO(gTeGblInfo) \
        (gTeGblInfo.pPathListEntry)
#define TE_ARHOP_LIST_INFO(gTeGblInfo) \
        (gTeGblInfo.pArHopListEntry)
#define TE_TRFC_PARAMS_INFO(gTeGblInfo) \
        (gTeGblInfo.pTrfcParams)

#define TE_PATH_LIST_ENTRY(x) \
        (&(gTeGblInfo.pPathListEntry[x - 1]))
#define TE_ARHOP_LIST_ENTRY(x) \
        (&(gTeGblInfo.pArHopListEntry[(x - 1)]))
#define TE_TRFC_PARAMS_PTR(x) \
        (&(gTeGblInfo.pTrfcParams[x - 1]))
      
/* Cfg Params Related */
#define TE_MAX_TNLS(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxTnls)
#define TE_MAX_REOPT_TNLS(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxReoptTnls)
#define TE_MAX_HOP_LIST(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxHopList)
#define TE_MAX_PO_PER_HOP_LIST(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxPOPerHopList)
#define TE_MAX_HOP_PER_PO(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxHopPerPO)
#define TE_MAX_TRFC_PARAMS(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxTrfcParams)
#define TE_MAX_RPTE_TRFC_PARAMS(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxRPTETrfcParams)
#define TE_MAX_CRLDP_TRFC_PARAMS(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxCRLDPTrfcParams)
#define TE_MAX_ARHOP_LIST(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxArHopList)
#define TE_MAX_ARHOP_PER_LIST(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4MaxArHopPerList)

#define TE_NOTIFICATION_MAX_RATE(gTeGblInfo) \
        (gTeGblInfo.TeParams.u4NotificationMaxRate)
#define TE_MAX_RPTE_ERPERTNL(gTeGblInfo) \
        (gTeGblInfo.TeParams.u2MaxRpteErHops)

#define TE_MAX_DS_ELSPS(gTeGblInfo) \
        (gTeGblInfo.TeDiffServGlobalInfo.TeDiffServCfgParams.u4MaxDfSrvELsps)
#define TE_MAX_DS_LLSPS(gTeGblInfo) \
        (gTeGblInfo.TeDiffServGlobalInfo.TeDiffServCfgParams.u4MaxDfSrvLLsps)

/* MPLS_P2MP_LSP_CHANGES - S */
/* P2MP Scalars and Notification Related */
#define TE_P2MP_CONFIGURED_TNLS(gTeGblInfo) \
        (gTeGblInfo.TeP2mpParams.u4P2mpTunnelCount)
#define TE_P2MP_ACTIVE_TNLS(gTeGblInfo) \
        (gTeGblInfo.TeP2mpParams.u4P2mpActiveTunnelCount)
#define TE_P2MP_TNL_MAX_HOP(gTeGblInfo) \
        (gTeGblInfo.TeP2mpParams.u4P2mpTunnelMaxHops)
#define TE_P2MP_TNL_NOTIFICATION(gTeGblInfo) \
        (gTeGblInfo.TeP2mpParams.bP2mpTunnelNotification)
/* MPLS_P2MP_LSP_CHANGES - E */

/* Pool Ids Related */
#define TE_TNL_POOL_ID                 TEMemPoolIds[MAX_TE_TUNNEL_INFO_SIZING_ID]       
#define TE_REOPT_TNL_POOL_ID           TEMemPoolIds[MAX_TE_REOPT_TUNNEL_INFO_SIZING_ID]
#define TE_PO_POOL_ID                  TEMemPoolIds[MAX_TE_PATH_INFO_SIZING_ID]
#define TE_HOP_POOL_ID                 TEMemPoolIds[MAX_TE_HOP_INFO_SIZING_ID]        
#define TE_ARHOP_POOL_ID               TEMemPoolIds[MAX_TE_AR_HOP_INFO_SIZING_ID]
#define TE_CRLDP_TRFC_PARAM_POOL_ID    TEMemPoolIds[MAX_TE_CRLD_TRFC_PARAMS_SIZING_ID]
#define TE_CHOP_POOL_ID                TEMemPoolIds[MAX_TE_CHOP_INFO_SIZING_ID]
#define TE_TNL_SRLG_POOL_ID            TEMemPoolIds[MAX_TE_TNL_SRLG_SIZING_ID]
#define TE_ATTR_SRLG_POOL_ID           TEMemPoolIds[MAX_TE_ATTR_SRLG_SIZING_ID]
#define TE_ATTR_LIST_POOL_ID           TEMemPoolIds[MAX_TE_ATTR_LIST_INFO_SIZING_ID]

#define TE_RSVP_TRFC_PARAM_POOL_ID     TEMemPoolIds[MAX_TE_RSVP_TRFC_PARAMS_SIZING_ID]
#define TE_L2VPN_IF_POOL_ID            TEMemPoolIds[MAX_TE_MPLS_TE_TNL_INDEX_SIZING_ID]
#define TE_TNL_FRR_CONST_INFO_POOL_ID  TEMemPoolIds[MAX_TE_FRR_CONST_INFO_SIZING_ID]
#define TE_DIFFSERV_POOL_ID            TEMemPoolIds[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID]
#define TE_TNL_INDEX_POOL_ID           TEMemPoolIds[MAX_TE_TNL_INDEX_LIST_SIZING_ID]
#define TE_PATH_LIST_POOL_ID           TEMemPoolIds[MAX_TE_PATH_LIST_SIZING_ID]
#define TE_ARHOP_LIST_POOL_ID          TEMemPoolIds[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID]
#define TE_TRFC_PARAMS_POOL_ID         TEMemPoolIds[MAX_TE_TRFC_PARAMS_SIZING_ID]
#define TE_CHOP_LIST_POOL_ID           TEMemPoolIds[MAX_TE_CHOP_LIST_INFO_SIZING_ID]
#define TE_TNL_ERROR_TBL_POOL_ID       TEMemPoolIds[MAX_TE_TNL_ERROR_TABLE_INFO_SIZING_ID]

/* STATIC_HLSP */
#define TE_TNL_HLSP_INFO_POOL_ID       TEMemPoolIds[MAX_TE_DEF_HLSP_SIZING_ID]
#define TE_LSP_MAP_POOL_ID             TEMemPoolIds[MAX_TE_LSP_MAP_SIZING_ID]
/* MPLS_P2MP_LSP_CHANGES - S */
#define TE_TNL_P2MP_INFO_POOL_ID       TEMemPoolIds[MAX_TE_P2MP_TUNNEL_INFO_SIZING_ID]
#define TE_TNL_P2MP_BRANCH_POOL_ID     TEMemPoolIds[MAX_TE_P2MP_BRANCH_INFO_SIZING_ID]
#define TE_TNL_P2MP_DEST_POOL_ID       TEMemPoolIds[MAX_TE_P2MP_DEST_INFO_SIZING_ID]
#define TE_MPLS_API_IN_INFO_POOL_ID    TEMemPoolIds[MAX_TE_MPLS_API_INPUT_INFO_ENTRIES_SIZING_ID]
#define TE_MPLS_API_OUT_INFO_POOL_ID   TEMemPoolIds[MAX_TE_MPLS_API_OUTPUT_INFO_ENTRIES_SIZING_ID]
#define TE_P2MP_TNL_DEST_TBL_POOL_ID   TEMemPoolIds[MAX_TE_P2MP_TUNNEL_DEST_TABLE_SIZING_ID]
/* MPLS_P2MP_LSP_CHANGES - E */

/* Admin Status */
#define TE_ADMIN_STATUS(gTeGblInfo) \
        (gTeGblInfo.TeParams.u1TeAdminStatus)

#define TE_SIG_ADMIN_FLAG(gTeGblInfo) \
        (gTeGblInfo.TeParams.u1TeAdminFlag)

/* Memory initialisation using mem_set */
#define INIT_TE_TNL_INFO(pTeTnlInfo)\
    MEMSET(pTeTnlInfo, 0, sizeof(tTeTnlInfo))

#define INIT_TE_TRFC_PARAMS(pTeTrfcParams) {\
    MEMSET(pTeTrfcParams, 0, sizeof(tTeTrfcParams));\
        pTeTrfcParams->u1StorageType = TE_STORAGE_VOLATILE;}
#define INIT_TE_CRLDP_TRFC_PARAMS(pCRLDPTrfcParams) {\
    MEMSET(pCRLDPTrfcParams, 0, sizeof(tCRLDPTrfcParams));\
        pCRLDPTrfcParams->u1StorageType = TE_STORAGE_VOLATILE;}
#define INIT_TE_RSVPTE_TRFC_PARAMS(pRSVPTrfcParams) \
        MEMSET(pRSVPTrfcParams, 0, sizeof(tRSVPTrfcParams))
#define INIT_TE_ARHOP_INFO(pTeArHopInfo) \
        MEMSET(pArHopInfo, 0, sizeof(tTeArHopInfo))
#define INIT_TE_ARHOP_LIST_INFO(pTeArHopListInfo) \
        MEMSET(pTeArHopListInfo, 0, sizeof(tTeArHopListInfo))
#define INIT_TE_HOP_INFO(pTeHopInfo) \
        MEMSET(pTeHopInfo, 0, sizeof(tTeHopInfo))

#define INIT_TE_TNL_WITH_DEF_VALS(pTeTnlInfo) \
    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN; \
    TE_TNL_OPER_STATUS (pTeTnlInfo) = TE_OPER_DOWN; \
 TE_TNL_REOPTIMIZE_STATUS(pTeTnlInfo) = TE_REOPTIMIZE_DISABLE; \
    TE_TNL_SETUP_PRIO (pTeTnlInfo) = TE_SETUP_PRIO_DEF_VAL; \
    TE_TNL_HLDNG_PRIO (pTeTnlInfo) = TE_HOLD_PRIO_DEF_VAL; \
    TE_TNL_INST_PRIO(pTeTnlInfo) = TE_TUNNEL_INS_PRIO_DEF_VAL; \
    TE_TNL_SIGPROTO(pTeTnlInfo) = TE_TUNNEL_SIGN_PROTO_DEF_VAL; \
    TE_TNL_SSN_ATTR(pTeTnlInfo) = TE_TUNNEL_SSN_ATTR_DEF_VAL; \
    TE_TNL_ROLE (pTeTnlInfo) = TE_ROLE_DEF_VAL; \
    TE_TNL_OWNER (pTeTnlInfo) = TE_TUNNEL_OWNER_DEF_VAL;\
    TE_TNL_LOCAL_PROTECTION_IN_USE(pTeTnlInfo) = TE_TUNNEL_PROTECTINUSE_DEF_VAL;\
    TE_TNL_STORAGE_TYPE (pTeTnlInfo) = TE_STORAGE_TYPE_DEF_VAL; \
    TE_TNL_ISIF(pTeTnlInfo) = TE_TUNNEL_ISIF_DEF_VAL; \
    TE_TNL_IFINDEX (pTeTnlInfo) = TE_ZERO;\
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;\
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;\
    TE_TNL_FRR_MAX_GBL_REVERT_TIME (pTeTnlInfo) = TE_TNL_FRR_GBL_REVERT_DEF_TIME; \
    pTeTnlInfo->u4TnlType = TE_TNL_TYPE_MPLS; \
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;\
    pTeTnlInfo->bTnlIntOamEnable = TRUE;\
    pTeTnlInfo->u1OamOperStatus = TE_OPER_UNKNOWN;\
    pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_DOWN;\
    pTeTnlInfo->GmplsTnlInfo.bUnnumIf = GMPLS_UNNUM_FALSE;\
    pTeTnlInfo->GmplsTnlInfo.u1EncodingType = GMPLS_TUNNEL_LSP_NOT_GMPLS;\
    pTeTnlInfo->GmplsTnlInfo.u1SwitchingType = GMPLS_UNKNOWN_SWITCHING;\
    pTeTnlInfo->GmplsTnlInfo.u1Direction = GMPLS_FORWARD;\
    pTeTnlInfo->GmplsTnlInfo.PathComp = GMPLS_PATH_COMP_DYNAMIC_FULL;\
    pTeTnlInfo->GmplsTnlInfo.u1Secondary = TE_SNMP_FALSE;\
    pTeTnlInfo->GmplsTnlInfo.u2Gpid = GMPLS_GPID_UNKNOWN;\
    pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType = MPLS_TE_UNPROTECTED;\
    pTeTnlInfo->GmplsTnlInfo.u1AdminStatusLen = sizeof (UINT4);\
    pTeTnlInfo->u1TnlMBBStatus = MPLS_TE_MBB_DISABLED;\
    pTeTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;

#define TE_COMPUTE_HASH_INDEX(u4Address) \
             ((((u4Address) >> 24) ^ (((u4Address) & 0xFF0000) >> 16) ^ \
               (((u4Address) & 0xFF00) >> 8) ^ \
               ((u4Address) & 0xFF)) % TE_HASH_SIZE)

#define TE_TNL_INDEX_LIST(gTeGblInfo) \
           (gTeGblInfo.pTunnelIndexNext)
           
/* FRR Related Macros and Initialisation */

#define TE_TNL_FRR_CONST_PROT_IFINDEX(pTeFrrConstInfo)    (pTeFrrConstInfo->u4ProtIfIndex)
#define TE_TNL_FRR_CONST_PROT_METHOD(pTeFrrConstInfo)     (pTeFrrConstInfo->u1ProtMethod)
#define TE_TNL_FRR_CONST_PROT_TYPE(pTeFrrConstInfo)       (pTeFrrConstInfo->u1ProtType)
#define TE_TNL_FRR_CONST_SETUP_PRIO(pTeFrrConstInfo)      (pTeFrrConstInfo->u1SetPrio)
#define TE_TNL_FRR_CONST_HOLD_PRIO(pTeFrrConstInfo)       (pTeFrrConstInfo->u1HoldPrio)
#define TE_TNL_FRR_CONST_SE_STYLE(pTeFrrConstInfo)        (pTeFrrConstInfo->u1SEStyle)
#define TE_TNL_FRR_CONST_INCANY_AFFINITY(pTeFrrConstInfo) (pTeFrrConstInfo->u4IncAnyAttr)
#define TE_TNL_FRR_CONST_INCALL_AFFINITY(pTeFrrConstInfo) (pTeFrrConstInfo->u4IncAllAttr)
#define TE_TNL_FRR_CONST_EXANY_AFFINITY(pTeFrrConstInfo)  (pTeFrrConstInfo->u4ExAnyAttr)
#define TE_TNL_FRR_CONST_HOP_LIMIT(pTeFrrConstInfo)       (pTeFrrConstInfo->u1HopLimit)
#define TE_TNL_FRR_CONST_BANDWIDTH(pTeFrrConstInfo)       (pTeFrrConstInfo->u4Bandwidth)
#define TE_TNL_FRR_CONST_ROW_STATUS(pTeFrrConstInfo)      (pTeFrrConstInfo->u1RowStatus)


#define TE_TNL_FRR_PROT_IFINDEX(pTeTnlInfo)               (pTeTnlInfo->u4TnlProtIfIndex)
#define TE_TNL_FRR_PROT_TYPE(pTeTnlInfo)                  (pTeTnlInfo->u1TnlProtType)
#define TE_TNL_FRR_PROT_METHOD(pTeTnlInfo)                (pTeTnlInfo->u1TnlProtMethod)
#define TE_TNL_FRR_BKP_TNL_INDEX(pTeTnlInfo)              (pTeTnlInfo->u4BkpTnlIndex)
#define TE_TNL_FRR_BKP_TNL_INST(pTeTnlInfo)               (pTeTnlInfo->u4BkpTnlInstance)
#define TE_TNL_FRR_BKP_TNL_INGRESS_ID(pTeTnlInfo)         (pTeTnlInfo->BkpTnlIngressLsrId)
#define TE_TNL_FRR_BKP_TNL_EGRESS_ID(pTeTnlInfo)          (pTeTnlInfo->BkpTnlEgressLsrId)
#define TE_TNL_FRR_ONE2ONE_PLR_ID(pTeTnlInfo)             (pTeTnlInfo->u4One2OnePlrId)
#define TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE(pTeTnlInfo)   (pTeTnlInfo->u1SenderAddrType)
#define TE_TNL_FRR_ONE2ONE_SENDER_ADDR(pTeTnlInfo)        (pTeTnlInfo->u4One2OneSenderAddr)
#define TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE(pTeTnlInfo)    (pTeTnlInfo->u1AvoidNAddrType)
#define TE_TNL_FRR_ONE2ONE_AVOID_ADDR(pTeTnlInfo)         (pTeTnlInfo->u4One2OneAvoidNAddr)
#define TE_TNL_FRR_DETOUR_MERGING(pTeTnlInfo)             (pTeTnlInfo->u1DetourMerging)
#define TE_TNL_FRR_DETOUR_ACTIVE(pTeTnlInfo)              (pTeTnlInfo->u1DetourActive)
#define TE_TNL_FRR_FAC_TNL_STATUS(pTeTnlInfo)             (pTeTnlInfo->u1FacTunStatus)
#define TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH(pTeTnlInfo)     (pTeTnlInfo->u4FacTunResvBw)
#define TE_TNL_FRR_MAX_GBL_REVERT_TIME(pTeTnlInfo)        (pTeTnlInfo->u4MaxGblRevertTime)

#define INIT_TE_TNL_FRR_CONST_INFO(pTeFrrConstInfo) \
    MEMSET(pTeFrrConstInfo, 0, sizeof(tTeFrrConstInfo))
#define INIT_TE_TNL_FRR_CONST_WITH_DEF_VALS(pTeFrrConstInfo) \
    TE_TNL_FRR_CONST_PROT_IFINDEX(pTeFrrConstInfo) = TE_ZERO; \
    TE_TNL_FRR_CONST_PROT_METHOD(pTeFrrConstInfo) = TE_TNL_FRR_PROT_METHOD_DEF_VAL; \
    TE_TNL_FRR_CONST_PROT_TYPE(pTeFrrConstInfo) = TE_TNL_FRR_PROT_TYPE_DEF_VAL; \
    TE_TNL_FRR_CONST_SETUP_PRIO(pTeFrrConstInfo) = TE_TNL_FRR_SETUP_PRIO_DEF_VAL; \
    TE_TNL_FRR_CONST_HOLD_PRIO(pTeFrrConstInfo) = TE_HOLD_PRIO_DEF_VAL; \
    TE_TNL_FRR_CONST_SE_STYLE(pTeFrrConstInfo) = TE_TRUE; \
    TE_TNL_FRR_CONST_INCANY_AFFINITY(pTeFrrConstInfo) = TE_ZERO; \
    TE_TNL_FRR_CONST_INCALL_AFFINITY(pTeFrrConstInfo) = TE_ZERO; \
    TE_TNL_FRR_CONST_EXANY_AFFINITY(pTeFrrConstInfo) = TE_ZERO; \
    TE_TNL_FRR_CONST_HOP_LIMIT(pTeFrrConstInfo) = TE_TNL_FRR_HOP_LIMIT_DEF_VAL; \
    TE_TNL_FRR_CONST_BANDWIDTH(pTeFrrConstInfo) = TE_ZERO; 

#define TE_OFFSET(x,y)  ((FS_ULONG)(&(((x *)0)->y)))
#define TE_GET_MASK_FROM_PRFX_LEN(u1Len, u4Mask) \
{ \
     u4Mask = 0xffffffff; \
     u4Mask = u4Mask << (32 - u1Len); \
}

/* STATIC_HLSP */
#define TE_TNL_TYPE(pTeTnlInfo) \
           (pTeTnlInfo)->u4TnlType
              
#define TE_HLSP_INFO(pTeTnlInfo)\
           (pTeTnlInfo)->pHlspInfo

#define TE_MAP_TNL_INFO(pTeTnlInfo)\
           (pTeTnlInfo)->pMapTnlInfo

#define TE_HLSP_STACK_LIST(pTeTnlInfo) \
           ((TE_HLSP_INFO(pTeTnlInfo))->StackList)
           
#define TE_HLSP_AVAILABLE_BW(pTeTnlInfo)\
     ((TE_HLSP_INFO(pTeTnlInfo))->u4AvailableBW)

#define TE_HLSP_NO_OF_STACKED_TNLS(pTeTnlInfo)\
     ((TE_HLSP_INFO(pTeTnlInfo))->u4NoOfStackedTunnels)

/* Macros for FsMplsLSPMapTunnelTable */

#define TE_LSP_MAP_TNL_INDEX(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex


#define TE_LSP_MAP_TNL_INST(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance

#define TE_LSP_MAP_TNL_INGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId

#define TE_LSP_MAP_TNL_EGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId

#define TE_LSP_MAP_SUB_TNL_INDEX(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex

#define TE_LSP_MAP_SUB_TNL_INST(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance

#define TE_LSP_MAP_SUB_TNL_INGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId

#define TE_LSP_MAP_SUB_TNL_EGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId

#define TE_LSP_MAP_TNL_OPERATION(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation

#define TE_LSP_MAP_TNL_RS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus


#define TE_IS_SET_LSP_MAP_TNL_INDEX(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex


#define TE_IS_SET_LSP_MAP_TNL_INST(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance

#define TE_IS_SET_LSP_MAP_TNL_INGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIngressLSRId

#define TE_IS_SET_LSP_MAP_TNL_EGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelEgressLSRId

#define TE_IS_SET_LSP_MAP_SUB_TNL_INDEX(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex

#define TE_IS_SET_LSP_MAP_SUB_TNL_INST(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelInstance

#define TE_IS_SET_LSP_MAP_SUB_TNL_INGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIngressLSRId

#define TE_IS_SET_LSP_MAP_SUB_TNL_EGRESS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelEgressLSRId

#define TE_IS_SET_LSP_MAP_TNL_OPERATION(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation

#define TE_IS_SET_LSP_MAP_TNL_RS(pteFsMplsLSPMapTunnelTable) \
            pteFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus

/* MACROS for validate routines */
#define TE_VALIDATE_OPERATION(u4RetVal, pteFsMplsLSPMapTunnelTable){\
UINT4 u4Operation;\
u4Operation = TE_LSP_MAP_TNL_OPERATION(pteFsMplsLSPMapTunnelTable);\
   if((u4Operation != TE_OPER_STACK) && (u4Operation != TE_OPER_STITCH))\
   {\
        u4RetVal = TE_FAILURE;\
    }\
    else\
    {\
     u4RetVal = TE_SUCCESS;\
     }}

#define TE_VALIDATE_RS(u4RetVal, pteFsMplsLSPMapTunnelTable){\
UINT4 u4RowStatus;\
u4RowStatus = (UINT4)TE_LSP_MAP_TNL_RS(pteFsMplsLSPMapTunnelTable);\
    if((u4RowStatus < ACTIVE) || (u4RowStatus > DESTROY))\
   {\
        u4RetVal = TE_FAILURE;\
    }\
     else\
    {\
    u4RetVal = TE_SUCCESS;\
     }}

/* Macro to fill the arguements of FsMplsLSPMaptunnelTable */

#define  TE_FILL_FSMPLSLSPMAPTUNNELTABLE_ARGS(pteFsMplsLSPMapTunnelTable,\
   pteIsSetFsMplsLSPMapTunnelTable,\
   pau1FsMplsLSPMapTunnelIndex,\
   pau1FsMplsLSPMapTunnelInstance,\
   pau1FsMplsLSPMapTunnelIngressLSRId,\
   pau1FsMplsLSPMapTunnelEgressLSRId,\
   pau1FsMplsLSPMapSubTunnelIndex,\
   pau1FsMplsLSPMapSubTunnelInstance,\
   pau1FsMplsLSPMapSubTunnelIngressLSRId,\
   pau1FsMplsLSPMapSubTunnelEgressLSRId,\
   pau1FsMplsLSPMaptunnelOperation,\
   pau1FsMplsLSPMaptunnelRowStatus)\
  {\
  if (pau1FsMplsLSPMapTunnelIndex != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex = *(UINT4 *) (pau1FsMplsLSPMapTunnelIndex);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapTunnelInstance != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance = *(UINT4 *) (pau1FsMplsLSPMapTunnelInstance);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapTunnelIngressLSRId != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId = *(UINT4 *) (pau1FsMplsLSPMapTunnelIngressLSRId);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIngressLSRId = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIngressLSRId = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapTunnelEgressLSRId != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId = *(UINT4 *) (pau1FsMplsLSPMapTunnelEgressLSRId);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelEgressLSRId = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelEgressLSRId = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapSubTunnelIndex != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex = *(UINT4 *) (pau1FsMplsLSPMapSubTunnelIndex);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapSubTunnelInstance != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance = *(UINT4 *) (pau1FsMplsLSPMapSubTunnelInstance);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelInstance = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelInstance = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapSubTunnelIngressLSRId != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId = *(UINT4 *) (pau1FsMplsLSPMapSubTunnelIngressLSRId);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIngressLSRId = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIngressLSRId = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMapSubTunnelEgressLSRId != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId = *(UINT4 *) (pau1FsMplsLSPMapSubTunnelEgressLSRId);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelEgressLSRId = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelEgressLSRId = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMaptunnelOperation != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation = *(UINT4 *) (pau1FsMplsLSPMaptunnelOperation);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation = OSIX_FALSE;\
  }\
  if (pau1FsMplsLSPMaptunnelRowStatus != NULL)\
  {\
   pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus = *(INT4 *) (pau1FsMplsLSPMaptunnelRowStatus);\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus = OSIX_FALSE;\
  }\
  \
  }


#define TE_GET_DIRECTION_FOR_HWACTION(pTeTnlInfo,u4Direction)\
{\
    if ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) && (pTeTnlInfo->u1TnlRole == TE_EGRESS))\
    {\
        u4Direction = MPLS_DIRECTION_FORWARD;\
    }\
    else if(pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)\
    {\
        u4Direction = MPLS_DIRECTION_REVERSE;\
    }\
    else\
    {\
        u4Direction = MPLS_DIRECTION_FORWARD;\
    }\
}


/* MPLS_P2MP_LSP_CHANGES - S */
/* Gives the offset in the structure */
#define TE_FIELD_OFFSET(str,field)   FSAP_OFFSETOF(str, field)
#define TE_FIELD_LENGTH(str,field)  (sizeof(((str *)TE_ZERO)->field))

#define INIT_TE_P2MP_TNL_INFO(pTeP2mpTnlInfo) \
    MEMSET(pTeP2mpTnlInfo, 0, sizeof(tTeP2mpTnlInfo))

#define INIT_TE_P2MP_TNL_WITH_DEF_VALS(pTeP2mpTnlInfo) \
        TE_P2MP_TNL_INTEGRITY (pTeP2mpTnlInfo) = (UINT1)TE_SNMP_FALSE;\
        TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) = TE_P2MP_BRANCH_ROLE_DEF_VAL;

#define INIT_TE_P2MP_BRANCH_ENTRY(pTeP2mpBranchEntry) \
    MEMSET(pTeP2mpBranchEntry, 0, sizeof(tP2mpBranchEntry))

#define INIT_TE_P2MP_DEST_ENTRY(pTeP2mpDestEntry) \
    MEMSET(pTeP2mpDestEntry, 0, sizeof(tP2mpDestEntry))

#define INIT_TE_P2MP_DEST_WITH_DEF_VALS(pTeP2mpDestEntry) \
    TE_P2MP_DEST_ADMIN_STATUS(pTeP2mpDestEntry) = MPLS_ADMIN_STATUS_UP; \
    TE_P2MP_DEST_OPER_STATUS(pTeP2mpDestEntry) = TE_OPER_DOWN; \
    TE_P2MP_DEST_STORAGE_TYPE(pTeP2mpDestEntry) = TE_STORAGE_TYPE_DEF_VAL;

/* MPLS_P2MP_LSP_CHANGES - E */
#endif

/*---------------------------------------------------------------------------*/
/*                        End of file temacs.h                               */
/*---------------------------------------------------------------------------*/
