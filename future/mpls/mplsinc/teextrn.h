/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teextrn.h,v 1.96 2018/01/03 11:31:22 siva Exp $
 *
 * Description: This file contains the structure type
 *              definitions declared and exported to the Other Modules.
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : teextrn.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains contains the structure type 
 *                             definitions declared and exported to the 
 *                             Other Modules.
 *---------------------------------------------------------------------------*/

#ifndef _TEEXTN_H
#define _TEEXTN_H

#include "mplsutil.h"
#include "ospfte.h"
#include "mpls.h"

#define TE_DIFFSERV_ELSP_LIST_SIZE \
        (sizeof (tMplsDiffServElspList) * MAX_TE_DS_ELSPS)
#define TE_PATH_LIST_INFO_SIZE \
        (sizeof (tTePathListInfo) * MAX_TE_HOP_LIST)
#define TE_AR_HOP_LIST_INFO_SIZE \
        (sizeof (tTeArHopListInfo) * MAX_TE_ARHOP_LIST)
#define TE_TRFC_PARAMS_SIZE \
        (sizeof (tTeTrfcParams) * MAX_TE_TRAFFIC_PARAMS)
#define TE_TUNNEL_INDEX_LIST_SIZE \
        ((sizeof (UINT1) * MAX_TE_TUNNEL_INFO))
#define TE_CHOP_LIST_INFO_SIZE \
        (sizeof (tTeCHopListInfo) * MAX_TE_CHOP_LIST)
#define TE_ATTR_LIST_INFO_SIZE \
            (sizeof (tTeAttrListInfo) * MAX_TE_ATTR_LIST)

/* tefsap.h */
#define tTeSllNode              tTMO_SLL_NODE
#define tTeSll                  tTMO_SLL
#define tTeDll                  tTMO_DLL

/* UTL HASH Table related */
#define tTeHashTable           tTMO_HASH_TABLE
#define tTeHashNode            tTMO_HASH_NODE

/* tedefs.h */
/* Address related constants*/
#define LSP_TNLDESCR_LEN             32
#define LSP_TNLNAME_LEN              32

/* TeHopAddresAS - size : stdmptc.mib */

#define TE_HOP_ADDR_AS_SIZE          4

/* Tnl Role */
#define TE_INGRESS                   MPLS_TE_INGRESS
#define TE_INTERMEDIATE              MPLS_TE_INTERMEDIATE
#define TE_EGRESS                    MPLS_TE_EGRESS
#define TE_INGRESS_EGRESS            MPLS_TE_INGRESS_EGRESS

#define TE_REOPTIMIZE_ENABLE        1
#define TE_REOPTIMIZE_DISABLE       2
#define TE_REOPTIMIZE_MANUAL_TRIG   3

#define TE_REOPT_TRIGGER_ENABLE     1
#define TE_REOPT_TRIGGER_DISABLE    2

/*Signalling Protocol*/
#define TE_SIGPROTO_NONE             MPLS_TE_SIGPROTO_NONE
/* TE_CHNG */ /* swapped the signalling proto values */
#define TE_SIGPROTO_RSVP             MPLS_TE_SIGPROTO_RSVP
#define TE_SIGPROTO_LDP              MPLS_TE_SIGPROTO_LDP
#define TE_SIGPROTO_OTHER            MPLS_TE_SIGPROTO_OTHER

/* TE_CHNG */ /* Added TE TNL Owner values*/
#define TE_TNL_OWNER_ADMIN           1
#define TE_TNL_OWNER_OTHER           2
#define TE_TNL_OWNER_SNMP            3
#define TE_TNL_OWNER_LDP             4
#define TE_TNL_OWNER_CRLDP           5
#define TE_TNL_OWNER_RSVP            6
#define TE_TNL_OWNER_POLICYAGENT     7

/* SNMP Row Status Types */
#define TE_ACTIVE                    1
#define TE_NOTINSERVICE              2
#define TE_NOTREADY                  3
#define TE_CREATEANDGO               4
#define TE_CREATEANDWAIT             5
#define TE_DESTROY                   6

#define TE_TNL_REESTB                  8
#define TE_FRR_TNL_MP_ADD              9
#define TE_FRR_TNL_MP_DEL             10
#define TE_FRR_TNL_PLR_ADD            11
#define TE_FRR_TNL_PLR_DEL            12
#define MPLS_TE_OPER_P2MP_DEST_ADD    13
#define MPLS_TE_OPER_P2MP_DEST_DELETE 14


/* Admin Status */
#define TE_ADMIN_UP                  1
#define TE_ADMIN_DOWN                2
#define TE_ADMIN_TESTING             3

/* Tunnel LSR-ID Mapping information */
#define TE_TNL_INGRESSID_MAP_INFO    0x10
#define TE_TNL_EGRESSID_MAP_INFO     0x20

#define TE_SWITCH_APP_FRR 0
#define TE_SWITCH_APP_ELPS 1

/* Tunnel direction used in ArHopList */
#define TE_TNL_DIR_IN                1
#define TE_TNL_DIR_OUT               2

/* Oper Status */
#define TE_OPER_UP                   MPLS_OPER_UP
#define TE_OPER_DOWN                 MPLS_OPER_DOWN
#define TE_OPER_TESTING              MPLS_OPER_TESTING
#define TE_OPER_UNKNOWN              MPLS_OPER_UNKNOWN
#define TE_OPER_DORMANT              MPLS_OPER_DORMANT
#define TE_OPER_NOT_PRESENT          MPLS_OPER_NOT_PRESENT
#define TE_OPER_LOWER_LAYER_DOWN     MPLS_OPER_LOWER_LAYER_DOWN
#define TE_P2MP_LSP_BRANCH_ADD       MPLS_TE_OPER_P2MP_DEST_ADD
#define TE_P2MP_LSP_BRANCH_DELETE    MPLS_TE_OPER_P2MP_DEST_DELETE


/* Tunnel Events related constants */
#define TE_TNL_UP                     0x00000001
#define TE_TNL_DESTROY                0x00000002
#define TE_TNL_DOWN                   0x00000004
#define TE_DATA_TX_ENABLE             0x00000008
#define TE_DATA_TX_DISABLE            0x00000010
#define TE_GOING_DOWN                 0x00000012
#define TE_TNL_MANUAL_REOPTIMIZATION  0x00000014
#define TE_TNL_TX_PATH_MSG            0x00000020
#define TE_PROTECTION_SWITCH_OVER     0x00000040
#define TE_PROTECTION_SWITCH_BACK     0x00000080

/* Session Attributes Bit Values */
/* TE_CHNG - Start - Added Lbl recording, SE style & changed the bit values*/
#define TE_SSN_FAST_REROUTE_BIT      0x80
#define TE_SSN_MRG_PRMT_BIT          0x40
#define TE_SSN_IS_PERSISTENT_BIT     0x20
#define TE_SSN_IS_PINNED_BIT         0x10
#define TE_SSN_REC_ROUTE_BIT         0x08
#define TE_SSN_ALL_BIT               0xf8

/* Macro values for the MIB object gmplsTunnelAttributes */
#define TE_SSN_LBL_RECORD_BIT        0x80

/* MPLS tunnel types */
#define TE_TNL_TYPE_MPLS   MPLS_TE_TNL_TYPE_MPLS
#define TE_TNL_TYPE_MPLSTP MPLS_TE_TNL_TYPE_MPLSTP
#define TE_TNL_TYPE_GMPLS  MPLS_TE_TNL_TYPE_GMPLS
#define TE_TNL_TYPE_HLSP   MPLS_TE_TNL_TYPE_HLSP
/* MPLS_P2MP_LSP_CHANGES - S */
#define TE_TNL_TYPE_P2MP   MPLS_TE_TNL_TYPE_P2MP
/* MPLS_P2MP_LSP_CHANGES - E */
#define TE_TNL_TYPE_SLSP   MPLS_TE_TNL_TYPE_SLSP

/* tunnel operation */
enum
{
 TE_OPER_STACK = 1,
 TE_OPER_STITCH = 2
};

/* ER HOP Address Type Definitions */
#define TE_ERHOP_IPV4_TYPE           0x01
#define TE_ERHOP_IPV6_TYPE           0x02
#define TE_ERHOP_ASNUM_TYPE          0x03
#define TE_ERHOP_UNNUM_TYPE          0x04
#define TE_ERHOP_LSPID_TYPE          0x05

/* Values of ER Hop Address Length */
#define TE_ERHOP_IPV4_PREFIX_LEN     32

#define TE_ERHOP_FORWARD_LBL_PRESENT 0x80
#define TE_ERHOP_REVERSE_LBL_PRESENT 0x40

#define TE_MAX_HOP_PER_TNL MAX_TE_HOP_PER_PO

/* TE ER HOP Path Comp  definitions - mib values */
#define TE_DYNAMIC                   1
#define TE_EXPLICIT                  2

/* Te Hop Addr Length - stdmptc.mib */

#define MIN_TE_HOP_ADDRESS_SIZE         0
#define MAX_TE_HOP_ADDRESS_SIZE         32 


/* Te Hop Addr Unnum Length - stdmptc.mib */
#define DEF_ADDR_UNNUM_LEN              4

#define TE_CFA_SUCCESS                 0

/* CHOP Related Definitions */
#define TE_CHOP_FORWARD_LBL_PRESENT    0x80
#define TE_CHOP_REVERSE_LBL_PRESENT    0x40

/* AR Hop Related Definitions */
#define TE_ARHOP_IPV4_TYPE             0x01
#define TE_ARHOP_IPV6_TYPE             0x02
#define TE_ARHOP_ASNUM_TYPE            0x03
#define TE_ARHOP_UNNUM_TYPE            0x04

/* Values of RR Hop Address Length */
#define TE_ARHOP_IPV4_PREFIX_LEN     32

#define TE_ARHOP_FORWARD_LBL_PRESENT   0x80
#define TE_ARHOP_REVERSE_LBL_PRESENT   0x40
#define TE_ARHOP_FORWARD_LBL_GLOBAL    0x20
#define TE_ARHOP_REVERSE_LBL_GLOBAL    0x10

/* Operations for ELspInfoList */
#define TE_TP_ADD                      1
#define TE_TP_DEL                      2
#define TE_TP_UPDATE                   3

/* Te Update Operations */
#define TE_CP_OR_MGMT_OPR_STATUS_RELATED 1
#define TE_LOCAL_PROT_RELATED            2
#define TE_TNL_ROLE_RELATED              3
#define TE_OAM_OPR_STATUS_RELATED        4

#define TE_SIGMOD_TNLREL_AWAITED       1
#define TE_SIGMOD_TNLREL_DONE          0

#define TE_TNL_DELETION_DELAY      (SYS_NUM_OF_TIME_UNITS_IN_A_SEC/10)

#define TE_TNL_INUSE_BY_L2VPN          1
#define TE_TNL_INUSE_BY_BGP            2
#define TE_TNL_QUERY_MADE              8

#define TE_MAX_RESOURCE_WEIGHT         255

#define TE_TNL_FREQ_UNSPECIFIED        1
#define TE_TNL_FREQ_FREQUENT           2 
#define TE_TNL_FREQ_VERYFREQ           3

#define TE_TNL_CALL_FROM_ADMIN 1
#define TE_TNL_CALL_FROM_SIG   2

/* FRR Related Definitions */
#define TE_TNL_FRR_ONE2ONE_METHOD             1
#define TE_TNL_FRR_FACILITY_METHOD            2

#define TE_TNL_FRR_PROT_LINK                  1
#define TE_TNL_FRR_PROT_NODE                  2

#define TE_TNL_FRR_PROTECTION_NONE            0x00
#define TE_TNL_FRR_PROTECTION_PATH            0x01
#define TE_TNL_FRR_PROTECTION_NODE            0x02
#define TE_TNL_FRR_PROTECTION_LINK            0x04

#define TE_TNL_FRR_DETOUR_MERGE_NONE          1
#define TE_TNL_FRR_DETOUR_MERGE_PROT          2
#define TE_TNL_FRR_DETOUR_MERGE_DETOUR        3

#define TE_TNL_FRR_PROT_STATUS_ACTIVE         1
#define TE_TNL_FRR_PROT_STATUS_READY          2
#define TE_TNL_FRR_PROT_STATUS_PARTIAL        3

#define TE_DETOUR_TNL_INSTANCE                65536

#define TE_TNL_FRR_GBL_REVERT_MIN_TIME        0
#define TE_TNL_FRR_GBL_REVERT_MAX_TIME        1200000  /* 20 Minutes */

/* FRR Ssn Attr related macros */
#define TE_FRR_SSN_ATTR_LOCAL_PROT_FLAG  0x01
#define TE_FRR_SSN_ATTR_LBL_RECORD_FLAG  0x02
#define TE_FRR_SSN_ATTR_SE_STYLE_FLAG    0x04
#define TE_FRR_SSN_ATTR_BANDWIDTH_FLAG   0x08
#define TE_FRR_SSN_ATTR_NODE_PROT_FLAG   0x10
#define TE_SSN_ATTR_PATH_REEVAL_REQ  0x20
/* Tunnel Retrigger Time Value */
#define TE_TNL_RETRIG_TIME               10  /* 10 seconds */

#define TE_NO_ATTR_BITMASK                       0x00000000
#define TE_ALL_ATTR_BITMASK                      0x000001FF
#define TE_ATTR_SETUPPRI_BITMASK                 0x00000001
#define TE_ATTR_HOLDPRI_BITMASK                  0x00000002
#define TE_ATTR_LSP_SSN_ATTR_BITMASK             0x00000004
#define TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK     0x00000008
#define TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK     0x00000010
#define TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK     0x00000020
#define TE_ATTR_BANDWIDTH_BITMASK                0x00000040
#define TE_ATTR_CLASS_TYPE_BITMASK               0x00000080
#define TE_ATTR_SRLG_TYPE_BITMASK                0x00000100

#define MPLS_DSTE_ENABLED_FLAG                   1
#define MPLS_DSTE_CLASS_TYPE_FLAG                2
#define MPLS_DSTE_TE_CLASS_FLAG                  3
#define MPLS_DSTE_PHB_FLAG                       4
#define MPLS_DSTE_PSC_FLAG                       5


typedef UINT1 tTeRouterId[ROUTER_ID_LENGTH];

/* Structure containing CRLDP Related Traffic Parameters */

typedef struct _CRLDPTrfcParams
{
   UINT4             u4PeakDataRate;
   UINT4             u4PeakBurstSize;
   UINT4             u4CommittedDataRate;
   UINT4             u4CommittedBurstSize;
   UINT4             u4ExcessBurstSize;
   UINT2             u2CfgFlag;
   UINT1             u1Frequency;
   UINT1             u1Weight;
   UINT1             u1Flags;
   UINT1             u1RowStatus;
   UINT1             u1StorageType;
   UINT1             u1Resvd;
}
tCRLDPTrfcParams;

/* Structure containing RSVP Related Traffic Parameters */

typedef struct _RSVPTrfcParams
{
   UINT4             u4TokenBktRate;
   UINT4             u4TokenBktSize;
   UINT4             u4PeakDataRate;
   UINT4             u4MinPolicedUnit;
   UINT4             u4MaxPktSize;
   UINT2             u2CfgFlag;
   UINT1             au1Pad[2];
   UINT4             u4OldPeakDataRate;
}
tRSVPTrfcParams;

/* Structure containing the TE Traffic Parameters */

typedef struct _TeTrfcParams
{
   UINT4             u4TrfcParamIndex;
   UINT4             u4ResMaxRate;
   /* Indicates the number of tnls using the traffic Parameter */
   UINT2             u2NumOfTunnels;
   /* u1TrfcParamRole - Ingress, Intermediate or Egress */
   UINT1             u1TrfcParamRole;
   UINT1             u1RowStatus;
   UINT1             u1StorageType;
   UINT1             u1TrfcParamOwner;
   BOOL1             bIsMbbRequired; /*MBB is required or not*/
   UINT1             u1Resvd;
   tCRLDPTrfcParams *pCRLDPTrfcParams;
   tRSVPTrfcParams  *pRSVPTrfcParams;
}
tTeTrfcParams;

/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _TeTrfcParamsSize
{
   UINT1             au1TeTrfcParams[TE_TRFC_PARAMS_SIZE];
}
tTeTrfcParamsSize;

/* Structure containing the Primary Index of the ArHop Table and List of all
 * the Hops associated with it */

typedef struct _TeArHopListInfo
{
   UINT4             u4ArHopListIndex;
   tTeSll            ArHopList;
}
tTeArHopListInfo;

/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _TeArHopListInfoSize 
{
   UINT1             au1TeArHopListInfo[TE_AR_HOP_LIST_INFO_SIZE];
}
tTeArHopListInfoSize;

/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _TeTunnelIndexListSize
{
    UINT1             au1TeTunnelIndexList[TE_TUNNEL_INDEX_LIST_SIZE];
}
tTeTunnelIndexListSize;

/* GMPLS tunnel ARHOP info */
typedef struct _GmplsTnlArHopInfo
{
    /* From RFC 4802,
     * Maps to Mib object gmplsTunnelARHopExplicitForwardLabel */
    UINT4           u4ForwardLbl;

    /* From RFC 4802, 
     * Maps to Mib object gmplsTunnelARHopExplicitForwardLabelPtr */
    VOID            *pForwardLbl;

    /* From RFC 4802, 
     * Maps to Mib object gmplsTunnelARHopExplicitReverseLabel */
    UINT4           u4ReverseLbl;

    /* From RFC 4802,
     * Maps to Mib object gmplsTunnelARHopExplicitReverseLabelptr */
    VOID            *pReverseLbl;

    /* From RFC 4802,
     * Maps to Mib object gmplsTunnelARHopLabelStatuses*/
    UINT1           u1LblStatus;

    UINT1           u1LblStatusLen;
    /* From RFC 4802,
     * Maps to Mib object gmplsTunnelARHopProtection */
    UINT1           u1Protection;

    UINT1           u1ProtLen;
 
}tGmplsTnlArHopInfo;

/* Structure that is used as a node in a ArHop List */

typedef struct _TeArHopInfo
{
   tTeSllNode        NextHop;
   UINT4             u4TnlArHopIndex;
   uGenAddr          IpAddr;
   UINT1             au1LocalLspId[6];
   UINT2             u2AsNumber;

   /* This varible denotes the identifier value if this hop is an
    * unnumbered interface. */
   UINT4             u4UnnumIf;
   
   /* u1AddressType - IPv4, IPv6, AS or LSPID */
   UINT1             u1AddressType;
   UINT1             u1AddrPrefixLen;
   UINT1             u1Flags;
   UINT1             u1ProtType; /* protection type can be path(0), 
                                    node(1),link (2) */
   UINT4             u4Label;

   /* This variable denotes the forward and reverse label information 
    * corresponding to this AR Hop. This variable is only applicable 
    * for GMPLS Tunnels. */
   tGmplsTnlArHopInfo   GmplsTnlArHopInfo;

   UINT1             u1ProtTypeInUse; /* protection in use can be path(0),
                                         node(1),link (2) */
   UINT1             u1BwProtAvailable; /* Specifies whether bandwidth 
                                           protection is available */
   UINT1             u1LblCType;   

   UINT1             u1Rsvd;
}
tTeArHopInfo;

/* This structure denotes the Computed Hop List. */
typedef struct  _TeCHopListInfo
{
   /* This variable denotes the Index to the Computed Hop List */
   UINT4             u4CHopListIndex;

   /* This variable denotes the head pointer for the SLL List of computed 
    * hops for this list. */
   tTMO_SLL          CHopList;
}
tTeCHopListInfo;

/* This structure is added for system sizing. 
 * It should not be used for any other purpose */
typedef struct _TeCHopListInfoSize
{
   UINT1             au1TeCHopListInfo[TE_CHOP_LIST_INFO_SIZE];
}
tTeCHopListInfoSize;

/* GMPLS CHop list info */
typedef struct  _GmplsTnlCHopInfo
{
   /* From RFC 4802,
    * Maps to Mib object mplsTunnelCHopExplicitForwardLabel */
   UINT4             u4ForwardLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelCHopExplicitForwardLabelptr */
   VOID              *pForwardLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelCHopExplicitReverseLabel */
   UINT4             u4ReverseLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelCHopExplicitReverseLabelptr*/
   VOID              *pReverseLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelCHopLabelStatuses*/
   UINT1             u1LblStatus;

   UINT1             u1LblStatusLen;
   
   /* For Padding */
   UINT1            au1Pad[2];
}tGmplsTnlCHopInfo ;

/* The below structure denotes the table mplsTunnelCHopTable as per the
 * RFC 3812. */
typedef struct  _TeCHopInfo
{
   /* The below variable denotes the Next Pointer to the Next Hop in the 
    * Computed Hop List. */
   tTMO_SLL_NODE     NextHop;

   /* From RFC 3812, 
    * Maps to MIB Object mplsTunnelCHopListIndex, 
    * The below variable denotes the Index to this Computed Hop in the list */
   UINT4             u4TnlCHopIndex;

   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopLspId */
   UINT4             u4LocalLspId;
 
   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopIpAddr */
   uGenAddr          IpAddr;

   uGenAddr          RouterId;

   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopAddrUnnum */
   UINT4             u4UnnumIf;

   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopAsNumber */
   UINT2             u2AsNumber;

   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopAddrType */
   UINT1             u1AddressType;

   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopIpPrefixLen */
   UINT1             u1AddrPrefixLen;

   /* The below variables denotes the forward and reverse label information 
    * corresponding to this computed hop. This variable is applicable only
    * for GMPLS Tunnels. */
   tGmplsTnlCHopInfo GmplsTnlCHopInfo;

   /* From RFC 3812,
    * Maps to MIB Object mplsTunnelCHopType */
   UINT1             u1CHopType;

   /* This variable is used to identify whether the node is part of the
    * Computed Hop list.*/
   UINT1             u1LsrPartOfCHop;

   /* For Padding */
   UINT1             au1Pad[2];
} tTeCHopInfo;


/* The below structure conatins the list attribute parameters and list of all the tunnels
 *  * associated with this attribute list*/

typedef struct _TeAttrListInfo
{
   /* This variable denotes the head pointer for the SLL List of 
    * Srlg List. */
   tTMO_SLL  SrlgList;

   /* This variable denotes the Index to this Attribute List Entry. */
   UINT4    u4ListIndex ;

   /* This variable denotes the Attribute Name Length*/
   INT4    i4ListNameLen;

   /* This variable denotes the Attribue Name to this Attribute List Entry. */
   UINT1    au1ListName[LSP_TNLNAME_LEN];

   /* This variable denotes the Include Any Affinity Values to this Attribute 
    * List Entry. */
   UINT4    u4IncludeAnyAffinity;

   /* This variable denotes the Include All Affinity Values to this Attribute
    * List Entry. */
   UINT4    u4IncludeAllAffinity;

   /* This variable denotes the Exclude All Affinity Values to this Attribute
    * List Entry. */
   UINT4    u4ExcludeAnyAffinity;
   
   /* This variable denotes the bandwidth values to this Attribute List Entry */
   UINT4    u4Bandwidth;
   
   /* This variable denotes the TE Class Type to this Attribute List Entry */
   UINT4    u4ClassType;

   /* This variable denotes the Setup Priority of this Attribute List Entry */
   UINT1    u1SetupPriority;

   /* This variable denotes the Holding Priority of this Attribute List Entry */
   UINT1    u1HoldingPriority;

   /* This variable denotes the SRLG Type of this Attribute List Entry */
   UINT1    u1SrlgType;

   /* This variable denotes the Session Attributes of this Attribute List Entry */
   UINT1    u1SsnAttr;

   /* This variable denotes which of the following properties in the attribute
    * list is configured by user. 
    *    1. IncludeAnyAffinity
    *    2. IncludeAllAffinity
    *    3. ExcludeAllAffinity
    *    4. Bandwidth
    *    5. ClassType
    *    6. Setup Priority
    *    7. Holding Priority
    *    8. Srlg
    *    9. Session Attributes */
   UINT4    u4ListBitmask ;
  
   /* This variable denotes the number of tunnels associated with this 
    * Attribute list entry. */
   UINT2    u2NumOfTunnels;

   /* This variable denotes the row status of this Attribute list entry. */
   UINT1    u1RowStatus;

   /*Variable used for snmp_octet_string length verification */
   UINT1    u1SsnAttrLen;
   BOOL1    bIsMbbRequired; /*MBB is required or not*/
   UINT1    au1Rsvd[3];
} tTeAttrListInfo;

/* This structure is added for system sizing.
 * It should not be used for any other purpose */
typedef struct _TeAttrListInfoSize
{
       UINT1             au1AttrListInfo[TE_ATTR_LIST_INFO_SIZE];
}
tTeAttrListInfoSize;

/* Structure containing the Primary Index of the Hop Table and List of all 
 * available Path Options */ 

typedef struct _TePathListInfo
{
   UINT4             u4HopListIndex;
   /* u1HopRole - Ingress, Intermediate or Egress */
   UINT1             u1HopRole;
   UINT1             u1PathOptionCount;
   UINT2             u2Resvd;
   tTeSll            PathOptionList;
}
tTePathListInfo;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _TePathListInfoSize
{
   UINT1             au1TePathListInfo[TE_PATH_LIST_INFO_SIZE];
}
tTePathListInfoSize;

/* Structure that is used as a node in a list of Path Options maintained for 
 * a particular Hop List Index */

typedef struct _TePathInfo
{
   tTeSllNode        NextPathOption;
   UINT4             u4PathOptionIndex;
   /* Indicates the number of tnls using the Path Option */
   UINT2             u2NumOfTunnels;
   UINT2             u2ErHopCount;
   tTeSll            ErHopList;
   /* This variable denotes the ERO Hop List type */
   UINT1             u1ErHopListType;
   UINT1             u1PathCompType;
   UINT1             u1HopIncludeExclude;
   UINT1             au1pad[1];
}
tTePathInfo;

/* This structure corresponds to gmplsTunnelHopTable in RFC 4802 */
typedef struct _GmplsTnlHopInfo
{
   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelHopExplicitForwardLabel */
   UINT4            u4ForwardLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelHopExplicitForwardLabelptr */
   VOID            *pForwardLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelHopExplicitReverseLabel */
   UINT4            u4ReverseLbl;

   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelHopExplicitReverseLabelptr */
   VOID           *pReverseLbl;
   
   /* From RFC 4802,
    * Maps to Mib object gmplsTunnelHopLabelStatuses*/
   UINT1            u1LblStatus;

   UINT1            u1LblStatusLen;
   /* For Padding */
   UINT1            au1Pad[2];
} tGmplsTnlHopInfo;


/* Structure that is used as a node in a Hop List */

typedef struct _TeHopInfo
{
   tTeSllNode        NextHop;
   UINT1             au1PathOptionName[256];
   UINT4             u4TnlHopIndex;
   uGenAddr          IpAddr;

   /* This varible denotes the identifier value if this hop is an
    * unnumbered interface. */
   UINT4             u4UnnumIf;

   UINT4             u4LocalLspId;

   /* This variable denotes that the configured hop is include any
    * node. This variable corresponds to fsMplsTunnelHopIncludeAny
    * object of fsmpte.mib */
   INT4              i4TnlHopIncludeAny;

   UINT2             u2AsNumber;
   /* u1AddressType - IPv4, IPv6, AS or LSPID */
   UINT1             u1AddressType;
   UINT1             u1AddrPrefixLen;
   UINT1             u1HopType;
   UINT1             u1HopIncludeExclude;
   UINT1             u1HopEntryPathComp;
   UINT1             u1RowStatus;
   
   /* This variable denotes the forward and reverse label information 
    * corresponding to this AR Hop. This variable is only applicable 
    * for GMPLS Tunnels. */
   tGmplsTnlHopInfo  GmplsTnlHopInfo;

   UINT1             u1StorageType;
   
   /* This variable denotes the U Bit and Flag Field in Label Sub Object of 
    * ERO Object. */
   BOOL1              bIsUpstreamLabelflag;

   /* This variable is used to identify whether the node is part of the
    * Explicit Hop list.*/
   UINT1              u1LsrPartOfErHop;

   BOOL1              bIsMbbRequired; /*Variable for MBB initiation*/
}
tTeHopInfo;

/* Fast Reroute constraint information */
typedef struct _TeFrrConstInfo
{
    UINT4               u4ProtIfIndex; /* protected interface */
    UINT4               u4Bandwidth; /* Bandwidth desired for the backup 
                                        tunnel */
    UINT4               u4IncAnyAttr; /* Include-any affinity */
    UINT4               u4ExAnyAttr; /* Exclude-any affinity */
    UINT4               u4IncAllAttr; /* Include-all affinity */
 
    UINT1               u1ProtMethod; /* Protection method can 
                                       be onetoone or facility backup */
    UINT1               u1ProtType; /* Protection type can be
                                       link protection or node protection */ 
    UINT1               u1SetPrio; /* Setup priority for the backup tunnel */
    UINT1               u1HoldPrio; /* Holding priority for the backup 
                                       tunnel */
    UINT1               u1HopLimit; /* No. of hops backup tunnels must 
                                       traverse */
    UINT1               u1SEStyle; /* SE style desired flag valid when
                                      Sender template specific method is used*/
    UINT1               u1RowStatus;
    UINT1               u1Rsvd; /* Padding */
}
tTeFrrConstInfo;

/* STATIC_HLSP related Params */
typedef struct _tHlspParams
{
  tTMO_DLL StackList; /* DLL to store the Stacked tunnel pointer */
  UINT4 u4AvailableBW; /* Remaining Bandwidth of HLSP */
  UINT4 u4NoOfStackedTunnels; /* Number of tunnels stacked on to HLSP */
}
tHlspParams;

/* MPLS_P2MP_LSP_CHANGES - S */
/* Structure containing information associated with each branch point of a 
 * P2MP TE tunnel */
typedef struct _P2mpBranchEntry
{
    tTMO_SLL_NODE         NextP2mpBranchEntry;   /* Next SLL Branch Node */
    UINT4                 u4P2mpOutSegmentIndex; /* Out Segment Index */
    UINT2                 u2DestUsageCount;      /* No. of Dest Associated */
    UINT2                 u2ActiveDestCount;
}
tP2mpBranchEntry;

/* Structure containing information associated with each destination of a 
 * P2MP TE tunnel */
typedef struct _P2mpDestEntry
{
    tTMO_SLL_NODE       NextP2mpDestEntry;          /* Next SLL Dest Node */
    tTeRouterId         P2mpSrcSubGrpAddr;
    tTeRouterId         P2mpSubGrpAddr;
    tTeRouterId         P2mpDestLsrId;              /* Destination Address */
    UINT4               u4P2mpDestBranchOutSegIndex;
    UINT4               u4P2mpDestHopTableIndex;
    UINT4               u4P2mpDestPathInUse;
    UINT4               u4P2mpDestCHopTableIndex;
    UINT4               u4P2mpDestARHopTableIndex;
    UINT4               u4P2mpDestTotalUpTime;
    UINT4               u4P2mpDestInstanceUpTime;
    UINT4               u4P2mpDestPathChanges;
    UINT4               u4P2mpDestLastPathChange;
    UINT4               u4P2mpDestCreationTime;
    UINT4               u4P2mpDestStateTransitions;
    UINT4               u4P2mpDestDiscontinuityTime;
    UINT2               u2P2mpSrcSubGrpId;
    UINT2               u2P2mpSubGrpId;
    UINT1               u1P2mpSrcSubGrpAddrType;
    UINT1               u1P2mpSubGrpAddrType;
    UINT1               u1P2mpDestAddrType;
    UINT1               u1P2mpDestAdminStatus;         /* Admin Status */ 
    UINT1               u1P2mpDestOperStatus;          /* Oper Status */
    UINT1               u1P2mpDestRowStatus;           /* Row Status */
    UINT1               u1P2mpDestStorageType;         /* Storage Type */
    UINT1               au1Pad[1];                      /* Padding */
}
tP2mpDestEntry;

/* Structure containing information associated with a P2MP TE tunnel */
typedef struct _TeP2mpTnlInfo 
{
    tTMO_SLL            TeP2mpBranchEntry;   /* Branch Entry SLL */
    tTMO_SLL            TeP2mpDestEntry;     /* Dest Entry SLL */
    UINT4               u4P2mpLastAddOrRmvdBranchIf; /* Last Added or Remove Branch If index */
    UINT2               u2NumOfActiveBranch; /* Count of oper up branch */
    BOOLEAN             bP2mpTnlIntegrity;   /* P2MP Tunnel Integrity */
    UINT1               u1P2mpTnlBranchRole; /* P2MP Branch Role */
    UINT1               au1P2mpLastAddNextHop[MAC_ADDR_LEN]; /* Last Added If Index's Next MAC */
    UINT1               au1Pad[2];                      /* Padding */
}
tTeP2mpTnlInfo;
/* MPLS_P2MP_LSP_CHANGES - E */

/* Structure that is used as a node in a SRLG List of Tunnel */
typedef struct _TeTnlSrlg
{
   /* This variable denotes the Next Pointer to the Next SRLG in the 
    * SRLG List. */
    tTMO_SLL_NODE     NextSrlgNode;

    /* This variable denotes the Index to this SRLG Node */
    UINT4             u4SrlgNo;

    /* This variable denotes the row status of this SRLG Node */
    UINT4             u4RowStatus;
} tTeTnlSrlg;

/* Structure that is used as a node in a SRLG List of Attribute List */
typedef struct _TeAttrSrlg
{
   /* This variable denotes the Next Pointer to the Next SRLG in the 
    * SRLG List. */
    tTMO_SLL_NODE     NextSrlgNode;

    /* This variable denotes the Index to this SRLG Node */
    UINT4             u4SrlgNo;

    /* This variable denotes the row status of this SRLG Node */
    UINT4             u4RowStatus;
} tTeAttrSrlg;


/* This structure corresponds to gmplsTunnelTable in RFC 4802. */
typedef struct _GmplsTnlInfo
{
    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelUnnumIf */
    BOOL1             bUnnumIf;
    /* From RFC 4802, 
     * Maps to Mib object - gmplsTunnelAttributes*/
    UINT1             u1Attributes;
    
    /* Attribute Length */
    UINT1             u1AttrLen;
    
    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelLSPEncoding */
    UINT1             u1EncodingType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSwitchingType */
    UINT1             u1SwitchingType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelLinkProtection */
    UINT1             u1LinkProtection;
    
    /* Length of protection */
    UINT1             u1LinkProtLen;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelDirection */
    UINT1             u1Direction;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelPathComp */
    eGmplsTnlPathComp PathComp;
  
    /* Disjoint Type */
    UINT4             u4DisjointType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendPathNotifyRecipientType */
    eInetAddrType     SendPathNotifyRecipientType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendPathNotifyRecipient */
    UINT4             u4SendPathNotifyRecipient;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendResvNotifyRecipientType */
    eInetAddrType     SendResvNotifyRecipientType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendResvNotifyRecipient */
    UINT4             u4SendResvNotifyRecipient;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendUpstreamNotifyRecipientType */
    eInetAddrType     UpstreamNotifyRecipientType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendUpstreamNotifyRecipient */
    UINT4             u4UpstreamNotifyRecipient;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendDownstreamNotifyRecipientType  */
    eInetAddrType     DownstreamNotifyRecipientType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSendDownstreamNotifyRecipient */
    UINT4             u4DownstreamNotifyRecipient;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelAdminStatusFlags */
    UINT4             u4AdminStatus;

    /*Length OF admin Status*/
    UINT1             u1AdminStatusLen;
 
    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelSecondary */
    UINT1             u1Secondary;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelGPid */
    UINT2             u2Gpid;
    
    /* Maps to fsMplsTunnelEndToEndProtection */
    INT4              i4E2EProtectionType;
    
    /* No padding */
} tGmplsTnlInfo;

/*Tunnel Error Types */
typedef enum {
    TNL_NO_ERROR = 0,
    TNL_UNKNOWN_ERROR = 1,
    TNL_PROTOCOL_ERROR = 2,
    TNL_PATH_COMPUTATION_ERROR = 3,
    TNL_LOCAL_CONFIG = 4,
    TNL_LOCAL_RESOURCES = 5,
    TNL_LOCAL_OTHER = 6,
}eTunnelErrType ;

typedef struct _TeErrInfo
 {
    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorLastErrorType */
    eTunnelErrType          LastErrType;

    /*From RFC 4802, 
     * Maps to Mib object - gmplsTunnelErrorLastTime */
    UINT4                   u4LastErrTime; 

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorReporterType */
    eInetAddrType           ErrorReporterType;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorCode */
    UINT4                   u4ErrorCode;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorSubcode */
    UINT4                   u4SubErrorCode;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorTLVs */
    UINT1                   au1GmplsTnlErrTLV[256];

    INT4                    i4GmplsTnlErrTLVLength;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorHelpString */
    UINT1                   au1GmplsErrHelpString[256];

    INT4                    i4GmplsErrHelpStringLength;

    /* From RFC 4802,
     * Maps to Mib object - gmplsTunnelErrorReporter */
    UINT4                   u4ErrorIpAddress;

    /* Error Ip Address Len */
    UINT1                   u1ErrorIpAddressLen;

    /* For Padding */
    UINT1                   au1Pad[3];
}tTunnelErrInfo;


/* Structure containing information associated with a TE Tunnel */

typedef struct _TeTnlInfo
{
   tRBNodeEmbd       TnlInfoRBNode;
   tTeDll            FtnList;
   tTMO_DLL_NODE     NextServiceTnl; /* STATIC_HLSP : Next node pointer in list of stacked tunnels */
   tTmrBlk           TnlRetrigTmr;
   /* This variable denotes the head pointer for the SLL List of SRLG list. */
   tTMO_SLL          SrlgList;
   /* This variable denotes the temporary pointer which will have the SRLG
    * list of attribute list, if the attribute list is associated with this 
    * tunnel. Otherwise it will have the SRLG list of this tunnel */
   VOID             *pAttrSrlgList; 

   /* RB Node for the tree TnlIfIndexbased RB Tree */
   tRBNodeEmbd       TnlIfIndexbasedBNode;
   tRBNodeEmbd       TnlMsgIdBasedRBNode;
   UINT4             u4TnlIndex;
   UINT4             u4MappedArrayndx; 
   UINT4             u4TnlInstance;
   UINT4             u4OrgTnlInstance;
   UINT4             u4TnlPrimaryInstance;
   tTeRouterId       TnlIngressLsrId;
   tTeRouterId       TnlEgressLsrId;
   UINT1             au1TnlName[LSP_TNLNAME_LEN];
   UINT1             au1TnlDescr[LSP_TNLDESCR_LEN];
   UINT4             u4TnlIfIndex;

   /* This variable is added to store the MPLS Tunnel If Index value 
    * for the reverse direction of bidirectional tunnel */
   UINT4             u4RevTnlIfIndex;
   UINT4             u4OrgTnlIfIndex; /* Original tnl interface used 
                                         for fast re-routing */
   UINT4             u4TnlXcIndex;
   UINT4             u4OrgTnlXcIndex; /* Original XC index used                                                                                  for fast re-routing */

   UINT4             u4BkpTnlMPXcIndex1; /* This is applicable 
                                            only for the FRR MP tnls, 
                                            intention of this is to program 
                                            the MP ILM prior to link/node 
                                            failure in the hw*/
   UINT4             u4BkpTnlMPXcIndex2; /* This is applicable 
                                            only for the FRR MP tnls, 
                                            intention of this is to program 
                                            the MP ILM prior to link/node 
                                            failure in the hw*/
   UINT4             u4OutPathMsgId;
   UINT4             u4OutRecoveryPathMsgId;
   UINT1             u1TnlIsIf;
   UINT1             u1TnlSgnlPrtcl;
   UINT1             u1TnlSetPrio;
   UINT1             u1TnlHoldPrio;

   UINT1             u1TnlSsnAttr;
   UINT1             u1TnlOwner;
   UINT1             u1TnlLocalProtectInUse; /* Applicable for FRR and ELPS
                                                based protection switching,
                                                1. Protection not available/
                                                2. Protection available/
                                                3. Protection in use 
                                              */
   UINT1             u1SigState; /* RSVP/LDP Signalling status of this TE entry */

   UINT4             u4TnlInstancePrio;
   UINT4             u4TnlHopTableIndex;
   /* This variable denotes the Backup Hop Index for one-to-one preotection tunnel*/
   UINT4             u4TnlBackupHopTableIndex;
   UINT4             u4TnlARHopTableIndex;
   /* This variable denotes the Index to the Computed Hop List */
   UINT4             u4TnlCHopTableIndex;
   UINT4             u4TnlPrimaryTimeUp;
   UINT4             u4TnlPathChanges;
   UINT4             u4TnlLastPathChange;
   UINT4             u4TnlCreationTime;
   UINT4             u4TnlStateTransitions;
   UINT4             u4TnlIncludeAnyAffinity;
   UINT4             u4TnlIncludeAllAffinity;
   UINT4             u4TnlExcludeAnyAffinity;
   UINT4             u4TnlPathInUse;
   /* This variable denotes the Backup path in use for one-to-one preotection tunnel*/
   UINT4             u4TnlBackupPathInUse;
   UINT4             u4TnlPathMetric;
   /* This variable denotes the Disjoint type */
   UINT4             u4TnlDisJointType;
   UINT4             u4ResourceIndex;
   UINT4             u4TnlTotalUpTime;
   UINT4             u4TnlInstanceUpTime;

   UINT1             u1TnlAdminStatus;
   UINT1             u1TnlOperStatus; /* Overall Operational status of 
                                         1. Control plane/Management + 
                                         2. OAM status (Proactive session 
                                                        status) 
                                       */
   UINT1             u1TnlRowStatus;
   UINT1             u1TnlStorageType;

   /* Pointer to the traffic Param used by the Tunnel */
   tTeTrfcParams    *pTeTrfcParams;
   /* Pointer to the Path List Info */
   tTePathListInfo  *pTePathListInfo;
   /* Pointer to the set of ErHops used by the Tunnel */
   tTePathInfo      *pTePathInfo;
   /* Pointer to the set of Backup ErHops used by the One-to-One Protected Tunnel */
   tTePathInfo      *pTeBackupPathInfo;
   /* Pointer to the set of Incoming ArHops */
   tTeArHopListInfo *pTeInArHopListInfo;
   /* Pointer to the set of Outgoing ArHops */
   tTeArHopListInfo *pTeOutArHopListInfo;
   /* This varaible denotes the information of the Computed Hop list */
   tTeCHopListInfo *pTeCHopListInfo;
   /* MPLS_P2MP_LSP_CHANGES - S */
   /* Pointer to P2MP tunnel information */
   tTeP2mpTnlInfo   *pTeP2mpTnlInfo;   /* P2MP tunnel info */
   tTeAttrListInfo  *pTeAttrListInfo;
   /* MPLS_P2MP_LSP_CHANGES - E */
   struct _MplsDiffServTnlInfo *pMplsDiffServTnlInfo;
   /* Pointer to the fast reroute constraint information */
   tTeFrrConstInfo  *pTeFrrConstInfo;
   /* One-to-One information */
   /* Below information:
    * u4TnlProtIfIndex, u4BkpTnlIndex, u4BkpTnlInstance, 
    * BkpTnlIngressLsrId, BkpTnlEgressLsrId
    * is used for ELPS protection path also */

   UINT4             u4TnlProtIfIndex; /* protected interface */

   /* Index of the protecting LSP which is associated with working LSP */
   UINT4             u4BkpTnlIndex; 

   /* Instance of the protecting LSP which is associated with working LSP */
   UINT4             u4BkpTnlInstance;

   /* IngressId of the protecting LSP which is associated with working LSP */
   tTeRouterId       BkpTnlIngressLsrId;

   /* EgressId of the protecting LSP which is associated with working LSP */
   tTeRouterId       BkpTnlEgressLsrId;

   /* Index of the FA LSP to which the tunnel is stacked 
    * fsmpte MIB*/
   UINT4             u4TnlMapIndex;

   /* Instance of the FA LSP to which the tunnel is stacked 
    * fsmpte MIB*/
   UINT4             u4TnlMapInstance;

   /* Ingress of the FA LSP to which the tunnel is stacked 
    * fsmpte MIB*/
   tTeRouterId       TnlMapIngressLsrId;

   /* Egress of the FA LSP to which the tunnel is stacked 
    * fsmpte MIB*/
   tTeRouterId       TnlMapEgressLsrId;

   /*No of stacked Tunnels. To be filled if the Tunnel type is
    * hslp and RSVP Signalled */
   UINT4             u4NoOfStackedTunnels;

   /* Valid in One-to-One method only at (Head-end + PLR or PLR)*/ 
   UINT4             u4One2OnePlrId;
   UINT4             u4One2OneSenderAddr;  
   UINT4             u4One2OneAvoidNAddr;  
   UINT4             u4FacTunResvBw;
   UINT4             u4MaxGblRevertTime;
   UINT4             u4LdpOverRsvpEntIndex; /* Entity index of the LDP.
                                             This is useful in bringing down 
                                             the LDP targeted session which is 
                                             run over the TE tunnel */
   
   UINT4             u4DestTnlIndex;
   UINT4             u4DestTnlInstance;
   UINT4             u4TnlType; /* Tunnel type - mpls, mpls-tp, gmpls, h-lsp, p2mp */
   UINT4             u4TnlLsrIdMapInfo; /* LSR Id map info - ingressId, egressId, 
                                           indicates whether to refer external 
                                           node map table for complete ingress
                                           and/or egress identifiers */
   UINT4             u4TnlMode; /* unidirectional/co-routed bi-directional/
                                   associated bi-directional */
   /* BFD session index */
   UINT4             u4ProactiveSessionIndex; /* Proactive Session index */
   /* MPLS-TP OAM specific information */
   UINT4             u4MegIndex; /* Maintenance Entity Group index */
   UINT4             u4MeIndex; /* Maintenance Entity index */
   UINT4             u4MpIndex; /* Maintenance Point index */
   /*MPLS-TP OAM specific information Protection LSP*/
   UINT4             u4BkpMegIndex;
   UINT4             u4BkpMeIndex;
   UINT4             u4BkpMpIndex;
   /* This variable denotes the index to the Attribute List */
   UINT4             u4AttrParamIndex;  

   UINT4             u4BkpNextHopAddr;

   UINT4             u4CurrentResvBw;

   /* This variable denotes upstream data channel TE-Link Interface Index */
   UINT4             u4UpStrDataTeLinkIf;
   /* This variable denotes downstream data channel TE-Link Interface Index */
   UINT4             u4DnStrDataTeLinkIf;

   /* The below variable denotes the actual component interface index
    * value over which bandwidth reservation is done for forward direction.
    * */
   UINT4             u4FwdComponentIfIndex;

   /* The below variable denotes the actual component interface index
    * value over which bandwidth reservation is done for reverse direction.
    * */
   UINT4             u4RevComponentIfIndex;
   UINT1             u1TnlProtType; /* protection type can be 
                                       link or node protection */
   UINT1             u1SenderAddrType; /* Sender addr type (IPv4 or IPv6) */
   UINT1             u1AvoidNAddrType; /* Avoid Node addr type (IPv4 or IPv6) */
                                       /* Valid in One-to-One method only at
                                        * (MP(Inclusive of Detour merging) or MP + tail-end)*/ 
   UINT1             u1DetourMerging; /* values can be none(1),
                                           protectedTunnel(2) - Detour is
                                           merged with protected tunnel,
                                           detour(3) - Detour is merged with
                                           another detour*/

   UINT1             au1NextHopMac[MAC_ADDR_LEN];
   UINT1             u1DetourActive;
   UINT1             u1FacTunStatus;

   
   UINT1             au1BkpNextHopMac[MAC_ADDR_LEN];
   /* This variable denotes the srlg type of this tunnel */
   UINT1             u1TnlSrlgType;
   UINT1             u1FrrGblRevertTnlFlag;

   UINT1             u1TnlProtMethod; 
   UINT1             u1TnlRole;              /* Tunnel Role */
   UINT1             u1TnlInUseByVPN;        /* Indicates whether tunnel is used by L2VPN */
   UINT1             u1TnlRelStatus;         /* Tunnel Release status */

   UINT1             u1FwdArpResolveStatus;  /* Tnl Forward Arp Resolve Status */
   UINT1             u1RevArpResolveStatus;  /* Tnl Reverse Arp Resolve Status */
   UINT1             u1OamOperStatus; /* OAM status */
   UINT1             u1CPOrMgmtOperStatus; /* Control plane */

   BOOL1             bFrrDnStrLblChg;
   BOOL1             bIsOamEnabled;
   UINT1             u1TnlSwitchApp;  /* FRR/ELPS */
   BOOL1             bTnlIntOamEnable;  /* Internal Oam status(Enable/Disable),
                                           this variable determies whether to accept
                                           the external OAM update or not */

   /* STATIC_HLSP */
   /* If the tunnel is Service tunnel , this points to the HLSP */
   /* If the tunnel is Stitched tunnel, this points to the tunnel to which it is stitched to */
   struct  _TeTnlInfo *pMapTnlInfo;

   tHlspParams       *pHlspInfo; /*contains the HLSP specific info */

   /* This variable denotes the information of the GMPLS tunnel */
   tGmplsTnlInfo     GmplsTnlInfo;

   tTunnelErrInfo    *pTunnelErrInfo;

   /*Support of Lsp Reoptimization - Ero Cache List*/
   tTeCHopListInfo *pTeEroCacheListInfo;
   /* This variable denotes the Index to the Ero Cache List */
   UINT4             u4TnlEroCacheTableIndex;
   BOOL1             bIsBwProtDesired;
   BOOL1             bIsSEStyleDesired;
   BOOL1             b1IsResourceReserved;
   UINT1             u1MplsOamAppType; 

   /* Tunnel End-to-End Protection Mode */
   UINT1             u1TnlProtOperType;
   /* This variable indicates whether the tunnel is working tunnel or protection
    * tunnel. This variable is applicable only if u1TnlSwitchApp is ELPS. */
   UINT1             u1TnlPathType;
   /* Number of Non-Zero instances created for this tunnel. This variable is
    * meaningful only zero instance tunnel. */
   UINT1             u1NumInstances;
   UINT1             u1TnlMBBStatus;

   /* Indicates whether MBB to be initiated or not */
   BOOL1             bIsMbbRequired;
   UINT1             u1TnlRerouteFlag;
   UINT1             u1WorkingUp;
   UINT1             u1BackupUp;
   BOOL1             bIsOamPathChange;
   
   /* Denotes the synchronization status of the tunnel. 4 values
    * are applicable. 1. Not synchronized, 2. Downstream synchronized
    * 3. Upstream synchronized, 4. Fully synchronized */
   UINT1             u1TnlSyncStatus;
   UINT1             u1TnlHwStatus; /*Tunnel hardware status*/
   UINT1             u1FacFrrFlag;
   UINT1             u1TnlDelSyncFlag;
   UINT1             u1IsFtnMapped; /* To check whether FTN is mapped with this tunnel or Not */
   /*Variable introduced to support Reoptimization mentioned in RFC 4376*/
   UINT1    u1TnlReoptimizeStatus;
   UINT1    u1TnlReoptMaintenanceType;
   BOOL1    bIsReoptimizationTriggered;
   UINT1    u1TnlSwitchingType; /*Tunnel Switching Type*/
   BOOL1    bIsLabelChange;
   UINT1    u1MplsPmAppType; /* Specifies the type of RFC6374 */
}
tTeTnlInfo;

typedef struct _TeTnlL3VpnInfo
{
    UINT4              u4TnlIndex;
    UINT4              u4TnlInstance;
    UINT4              u4TnlXcIndex;
    tTeRouterId        TnlIngressLsrId;
    tTeRouterId        TnlEgressLsrId;
}tTeTnlL3VpnInfo;

typedef struct _TeReoptTnlInfo
{
 tRBNodeEmbd       ReoptTnlInfoRBNode;
 UINT4             u4ReoptTnlIndex;
 tTeRouterId       ReoptTnlIngressLsrId;
 tTeRouterId       ReoptTnlEgressLsrId; 
 UINT1     u1ReoptTnlStatus;
 UINT1     u1ReoptManualTrigger;
 UINT1     u1ReoptTnlRowStatus;
 UINT1             au1Pad[1];        /* Padding */
}
tTeReoptTnlInfo;

typedef struct _RpteEnqueueMsgInfo
{
    union
    {
        tTeTnlInfo       *pTeTnlInfo;
        struct _RsvpTeTnlInfo  *pRsvpTeTnlInfo;
    }u;

    tTeTnlL3VpnInfo       TeTnlL3VpnInfo;

    UINT4             u4TeTnlEvent;
    UINT4             u4RpteEvent;
}
tRpteEnqueueMsgInfo;


/* STRUCTURES RELATING TO DIFFSERV TE IMPLEMENTATION */

typedef struct _MplsDiffServElspInfo
{
       tTeSllNode          ElspInfoNext;
       UINT1               u1ElspInfoIndex;
       UINT1               u1PhbDscp;
       UINT1               u1RowStatus;
       UINT1               u1StorageType;
       UINT4               u4ResourceIndex;
       tTeTrfcParams       *pTeTrfcParams;  
}
tMplsDiffServElspInfo;

typedef struct _MplsDiffServElspList 
{
      UINT4                u4ElspInfoListIndex;
      UINT4                u4NumOfTunnels;
      tTeSll               ElspInfoList;
      BOOLEAN              bElspConfigFlag;
      UINT1                au1RsvpResvd[3]; 
}
tMplsDiffServElspList;


typedef struct _MplsDiffServElspListSize
{
      UINT1                au1MplsDiffServElspList[TE_DIFFSERV_ELSP_LIST_SIZE];

}tMplsDiffServElspListSize;

typedef struct _MplsDiffServTnlInfo
{
      UINT1                 u1ClassType;
      UINT1                 u1RowStatus; 
      UINT1                 u1StorageType;
      UINT1                 u1ServiceType;
      UINT1                 u1ElspType;
      UINT1                 u1LlspPscDscp;
      UINT2                 u2CfgFlag; 
      UINT4                 u4ElspListIndex; 
      tMplsDiffServElspList *pMplsDiffServElspList;
}
tMplsDiffServTnlInfo;

typedef struct _TeUpdateInfo
{
   UINT4             u4BkpTnlIndex; 
   UINT4             u4BkpTnlInstance;
   tTeRouterId       BkpTnlIngressLsrId;
   tTeRouterId       BkpTnlEgressLsrId;
   UINT1             u1TeUpdateOpr;
   UINT1             u1TnlOperStatus;
   UINT1             u1TnlCPOrMgmtOperStatus;
   UINT1             u1TnlOamOperStatus;
   UINT1             u1LocalProtection;
   UINT1             u1TnlRole;
   UINT1             u1TnlSwitchApp; /* FRR/ELPS */
   UINT1             u1CPOrOamOperChgReqd; /* This variable indicates whether
                                              updation in CP Status or OAM 
                                              status is required or not. */
   UINT4             u4SrcModule; /*For BFD over TE fault detection */
}
tTeUpdateInfo;

typedef struct _MplsRtEntryInfo
{
    uGenU4Addr          DestAddr;    /* The destination address */
    uGenU4Addr          DestMask;    /* The net mask to be applied to get the
                                       non-host portion for the destination */
    uGenU4Addr          NextHop;    /* The nexthop address to which any 
                                       datagrams destined to the destination, 
                                       to be forwarded. (Special cases) */
    UINT4               u4RtIfIndx;    /* The interface through which the 
                                        route is learnt */
    UINT4               u4RtNxtHopAS;    /* The autonomous system number of 
                                             next hop  */
    INT4                i4Metric1;    /* The reachability cost for the 
                                         destination */
    UINT1               u1RowStatus;    /* status of the row */
    UINT1               u1CmdType;
    UINT2               u2AddrType;
}
tMplsRtEntryInfo;

typedef struct _MplsIfEntryInfo
{
    UINT4               u4IfIndex;        /* MPLS Tunnel Interface Index */
    UINT1               u1OperStatus;     /* Oper status */
    UINT1               au1Pad[3];        /* Padding */
}
tMplsIfEntryInfo;

typedef struct _FrrTeTnlInfo
{
   UINT4             u4TnlIndex; 
   UINT4             u4TnlInstance;
   UINT4             u4BkpTnlInstance;
   tTeRouterId       TnlIngressLsrId;
   tTeRouterId       TnlEgressLsrId;
   BOOL1             bIsLocalRevert;
   UINT1             au1Rsvd[3];
}tFrrTeTnlInfo;

typedef struct _TeTnlIndices
{
    UINT4             u4TnlIndex; 
    UINT4             u4TnlInstance;
    tTeRouterId       TnlIngressLsrId;
    tTeRouterId       TnlEgressLsrId;
}tTeTnlIndices;

/* STATIC_HLSP */
typedef struct _tteFsMplsLSPMapTunnelTable
{
 tRBNode  FsMplsLSPMapTunnelTableNode; /* RB Tree node to store the structure */

 /* HLSP Tunnel Indices */
 UINT4  u4FsMplsLSPMapTunnelIndex;
 UINT4   u4FsMplsLSPMapTunnelInstance;
 UINT4   u4FsMplsLSPMapTunnelIngressLSRId;
 UINT4   u4FsMplsLSPMapTunnelEgressLSRId;

 /* Service Tunnel indices */
 UINT4      u4FsMplsLSPMapSubTunnelIndex;
 UINT4   u4FsMplsLSPMapSubTunnelInstance;
 UINT4   u4FsMplsLSPMapSubTunnelIngressLSRId;
 UINT4   u4FsMplsLSPMapSubTunnelEgressLSRId;
 
 UINT4   u4FsMplsLSPMaptunnelOperation; /* Operation : Stack/Stitch */
 INT4   i4FsMplsLSPMaptunnelRowStatus; /* Row Status */
} 
tteFsMplsLSPMapTunnelTable;

typedef struct _tteIsSetFsMplsLSPMapTunnelTable
{
 /* Bits which signifies if tteFsMplsLSPMapTunnelTable indices are set */
        BOOL1    bFsMplsLSPMapTunnelIndex;
        BOOL1    bFsMplsLSPMapTunnelInstance;
        BOOL1    bFsMplsLSPMapTunnelIngressLSRId;
        BOOL1    bFsMplsLSPMapTunnelEgressLSRId;
        BOOL1    bFsMplsLSPMapSubTunnelIndex;
        BOOL1    bFsMplsLSPMapSubTunnelInstance;
        BOOL1    bFsMplsLSPMapSubTunnelIngressLSRId;
        BOOL1    bFsMplsLSPMapSubTunnelEgressLSRId;
        BOOL1    bFsMplsLSPMaptunnelOperation;
        BOOL1    bFsMplsLSPMaptunnelRowStatus;
        UINT1    au1Pad[2];
}
tteIsSetFsMplsLSPMapTunnelTable;


typedef struct _TeLSPMapTnlGblInfo
{
    struct rbtree *    pLSPMapTnlRBTree;
 }
tTeLSPMapTnlGblInfo;

typedef struct _TeGetAttributes
{
   UINT4    u4IncludeAnyAffinity;
   UINT4    u4IncludeAllAffinity;
   UINT4    u4ExcludeAnyAffinity;
   UINT4    u4Bandwidth;
   UINT4    u4ClassType;
   UINT1    u1SetupPriority;
   UINT1    u1HoldingPriority;
   UINT1    u1SrlgType;
   UINT1    u1SsnAttr;
}
tTeGetAttributes;

enum {
    TNL_GR_NOT_SYNCHRONIZED = 1,
    TNL_GR_DOWNSTREAM_SYNCHRONIZED,
    TNL_GR_UPSTREAM_SYNCHRONIZED,
    TNL_GR_FULLY_SYNCHRONIZED
};

/* Functions exported by TE Module to the Signalling Protocols */

UINT1 TeSigCreateNewTnl ARG_LIST ((tTeTnlInfo **ppTeTnlInfo,
                                tTeTnlInfo *pNewTeTnlInfo));

VOID TeSigProcessL2VpnAssociation ARG_LIST ((tTeTnlInfo *pTeTnlInfo, 
        UINT4 u4OperStatus)); 
                               
UINT1 TeSigUpdateTeTnl ARG_LIST ((tTeTnlInfo * pTeTnlInfo));

UINT1 TeSigDeleteTnlInfo ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                              UINT1 u1CallerId));


UINT1 TeSigSetTnlOperStatus ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                                    UINT1      u1TnlOperStatus));

VOID TeSigGetNoOfStackedTnls ARG_LIST ((UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                                        UINT4 u4TnlIngressId, UINT4 u4TnlEgressId,
                                        UINT4 *pu4NoOfStackedTnl));
UINT1
TeSigSetTnlAppDown ARG_LIST ((tTeTnlInfo * pTeTnlInfo));

UINT1
TeSigSetTnlAppUp ARG_LIST ((tTeTnlInfo * pTeTnlInfo));

UINT1 TeSigCreateTrfcParams ARG_LIST ((UINT1 u1Protocol, 
                                    tTeTrfcParams **ppTeTrfcParams,
                                    tTeTrfcParams *pTempTeTrfcParams));

UINT1 TeSigDeleteTrfcParams ARG_LIST ((tTeTrfcParams *pTeTrfcParams));

UINT1 TeSigUpdateTrfcParams ARG_LIST ((tTeTrfcParams *pTeTrfcParams,
                                    tTeTrfcParams *pTempTeTrfcParams, 
                                    tTeTnlInfo    *pTeTnlInfo));

UINT1 TeSigCreateArHopListInfo ARG_LIST ((tTeArHopListInfo **ppArHopListInfo,
                                       tTeSll           *pArHopList));
UINT1
TeSigCreatePathListAndPathOption ARG_LIST ((tTePathListInfo ** ppTePathListInfo));
UINT1
TeSigCreateErHopsForCspfComputedHops ARG_LIST ((tTePathListInfo ** ppTePathListInfo,
                                      tOsTeAppPath *pCspfPath));

UINT1 TeSigDeleteArHopListInfo ARG_LIST ((tTeArHopListInfo *pArHopListInfo));

INT4 TeUpdateTunnelOperStatusAndProgHw (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus);

UINT1 TeSigUpdateArHopListInfo ARG_LIST ((tTeTnlInfo       *pTeTnlInfo,
                                       tTeArHopListInfo *pArHopListInfo,
                                       UINT1            u1Direction));

UINT1 TeSigCreateHopListInfo ARG_LIST ((tTePathListInfo **ppTePathListInfo,
                                     tTeSll          *pErHopList));

UINT1 TeSigDeleteHopListInfo ARG_LIST ((tTePathListInfo *pPathListInfo));

UINT1 TeSigDeleteHopInfo ARG_LIST ((tTePathListInfo *pPathListInfo, 
                                 tTePathInfo     *pTePathInfo, 
                                 tTeHopInfo      *pTeHopInfo));

UINT1 TeSigCreateDiffServTnl 
            ARG_LIST ((tMplsDiffServTnlInfo  **ppMplsDiffServTnlInfo,
                       tMplsDiffServTnlInfo  *pNewDiffServTnlInfo));

UINT1 TeSigDeleteDiffServTnl 
           ARG_LIST ((tMplsDiffServTnlInfo  *pMplsDiffServTnlInfo));

UINT1 TeSigCreateDiffServElspList 
           ARG_LIST ((tMplsDiffServElspList **ppMplsDiffServElspList,
                      tTeSll                *pElspInfoList));

UINT1 TeSigDeleteDiffServElspList 
           ARG_LIST ((tMplsDiffServElspList *pMplsDiffServElspList));

UINT1 TeSigUpdateTrfcParamsElspList
            ARG_LIST ((tMplsDiffServElspList *pMplsDiffServElspList,
                       tTeTrfcParams         *pTempTrfcParams,
                       UINT1                 u1ElspInfoIndex,
                       UINT1                 u1Operation));

UINT1 TeSigDiffServGetPhbPsc ARG_LIST ((UINT1 u1PhbDscp));

UINT1 TeSigAdminStatus 
           ARG_LIST ((UINT1         u1Protocol,
                      UINT1         u1AdminStatus));

UINT1
TeSigDiffServChkIsPerOAResrcConf ARG_LIST ((INT4 i4ElspInfoListIndex));

VOID
TeSigUpdateMaxErhopsPerTnl ARG_LIST ((INT4 i4TeRpteMaxErhopsPerTnl));

UINT2 TeGetMaxErhopsPerTnl ARG_LIST ((VOID));

UINT1 TeSigCheckMaxErhopInTnlTbl 
           ARG_LIST ((INT4 i4TeRpteMaxErhopsPerTnl));
/* Export to L2VPN Module */     
UINT1 MplsLspStatusReq ARG_LIST ((UINT4 u4LspSrcAddr, 
                         UINT4 u4LspDestAddr,
                                  UINT4 u4PriLspIdentifier, 
                                  UINT4 *pu4LspIdentifier, 
                                  UINT1 *pu1LspStatus));

UINT1 TeL2VpnEventHandler ARG_LIST ((UINT1 *pMsg, UINT4 u4Event));
/* Exported to LSR.MIB */
tTeTnlInfo * TeGetTunnelInfo ARG_LIST ((UINT4 u4TunnelIndex,
                                 UINT4 u4TunnelInstance,
                                 UINT4 u4TnlIngressLSRId,
                                 UINT4 u4TnlEgressLSRId));

UINT1 TeGetTunnelFrmIfIdbasedTree ARG_LIST ((UINT4 u4MplsTnlIfIndex,
                                             tTeTnlInfo **ppTeTnlInfo));

UINT1 TeCheckTrfcParamInTrfcParamTable ARG_LIST ((UINT4 u4TrfcParamIndex,
                          tTeTrfcParams **ppTeTrfcParams));
UINT1
TeCheckXcIndex ARG_LIST ((UINT4 u4XcIndex, tTeTnlInfo **pTeTnlInfo));

/* To get the indices of Tunnel Table by giving the Tunnel Name as input */
tTeTnlInfo *
TeGetTnlInfoByOwnerAndRole (UINT4 u4TnlIndex, UINT4 u4TnlInst,
                            UINT4 u4IngressId, UINT4 u4EgressId,
                            UINT4 u4TnlOwner, UINT4 u4TnlRole);

tTeTnlInfo         *
TeGetActiveTnlInfo (UINT4 u4TnlIndex, UINT4 u4TnlInst,
                            UINT4 u4IngressId, UINT4 u4EgressId);

VOID
TeCmnExtGetDirectionFromTnl (tTeTnlInfo *pTeTnlInfo, eDirection *pDirection);

tTeTnlInfo *
TeGetLatestTunnelInfoFromTunnelId (UINT4 u4TunnelId, UINT4 u4TnlOwner,
                                   UINT4 u4TnlRole);

UINT1 TeInit ARG_LIST ((VOID));
VOID  TeSetCfgParams ARG_LIST ((VOID));
VOID  TeDeInit ARG_LIST ((VOID));
VOID  TeDeleteMemPools ARG_LIST ((VOID));
VOID
TeReestablishOperDownTnls ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                                     UINT4 u4IfIndex, UINT1 u1SigProto));
VOID 
TeReEstOperDownTnlsFromRouteChg ARG_LIST ((tMplsRtEntryInfo *pRouteInfo));
VOID TeCmnExtHandleIfChange ARG_LIST ((tMplsIfEntryInfo *pIfInfo));
INT4 TeCmnExtHandleIfChangeForStatic ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                                                tMplsIfEntryInfo *pIfInfo));
VOID MplsHandleTnlArpResolveTmrEvent (VOID *);
VOID TeTnlRetriggerTmrEvent (VOID *pArg);

BOOL1
TeIsTnlOperStatusUp (UINT4 u4TnlId, UINT4 u4TnlInst, 
                     UINT4 u4TnlIngress, UINT4 u4TnlEgress);
UINT1 TeCallUpdateTnlOperStatus (tTeTnlInfo *pTeTnlInfo,
                                 UINT1       u1TnlOperStatus);

UINT4 TeGetActiveTnlInst (UINT4 u4TnlId, UINT4 *pu4TnlInst, UINT4 u4TnlIngress,
                          UINT4 u4TnlEgress, UINT4 *pu4TnlXcIndex, 
                          UINT4 *pu4TnlIfIndex);

UINT4 TeGetLdpRegisteredActiveTunnel (UINT4 u4TnlId,
                                      UINT4 u4TnlIngress, UINT4 u4TnlEgress,
                                      tTeTnlInfo ** ppTeTnlInfo);

UINT4
TeGetTunnelInfoByPrimaryTnlInst (UINT4 u4TnlId, UINT4 u4PrimaryTnlInst,
                                 UINT4 u4TnlIngress, UINT4 u4TnlEgress,
                                 tTeTnlInfo ** ppTeTnlInfo);
UINT1
TeSigNotifyFrrTnlMpHwIlmAddOrDel (tTeTnlInfo * pTeInProtTnlInfo,
                                  tTeTnlInfo * pTeInBkpTnlInfo, BOOL1 bIsIlmAdd);
UINT1
TeSigHandleGblRevTnlUp (tTeTnlInfo * pTeProtTnlInfo,
                        tTeTnlInfo * pTeSigGblRevTnlInfo);
UINT1
TeSigGetTnlIfIndex (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                    UINT4 u4IngressId, UINT4 u4EgressId, UINT4 *pu4TnlIfIndex);
UINT1 TeUpdateTnlStatus ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                                   tTeUpdateInfo *pTeUpdateInfo));
UINT1
TeCheckAndGetBkpTnlFromProtTnl (tTeTnlIndices *pProtTnlInfo,
                                tTeTnlIndices *pBkpTnlInfo);
UINT1
TeCmnExtGetBkpTnlFrmInUseProtTnl (tTeTnlIndices *pProtTnlInfo,
                                  tTeTnlIndices *pBkpTnlInfo);
VOID
TeCmnExtUpdateArpResolveStatus (tTeTnlInfo *pTeTnlInfo, UINT4 u4Action,
                                eDirection Direction, UINT1 u1ArpResolveStatus);

INT4
TeCmnExtProgramTunnelInHw (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus,
                           BOOL1 *pb1FwdLlStatus, BOOL1 *pb1RevLlStatus);

INT4
TeCmnExtSetOperStatusAndProgHw (tTeTnlInfo *pTeTnlInfo, UINT1 u1TnlOperStatus);

INT4
TeCmnExtGetNextP2mpBranchInfo (UINT4 u4TnlIndex, UINT4 u4TnlInst, UINT4 u4LclLsr,
                               UINT4 u4PeerLsr, UINT4 u4CurrIfIndex, UINT4 *pu4NextIfIndex,
                               UINT1 *pau1NextHop);

INT4
TeCmnExtIsTnlProtected (tTeTnlInfo *pTeTnlInfo);

INT4
MplsSetIfInfo (tTeTnlInfo *pTeTnlInfo,UINT4 u4MplsTnlIfIndex,INT4 i4IfParam);

/* MPLS_P2MP_LSP_CHANGES - S */
INT4
TeP2mpExtSetOperStatusAndProgHw (tTeTnlInfo *pTeTnlInfo, UINT1 u1TnlOperStatus);
/* MPLS_P2MP_LSP_CHANGES - E */
#ifdef NPAPI_WANTED
tTeTnlInfo  *TeGetTunnelEntryFromLabel (UINT4 MplsShimLabel);
#endif

INT4
TeSigCreateCHopsForCspfPaths (tTeTnlInfo * pTeTnlInfo,
                              tOsTeAppPath * pCspfPath,
                              tTeCHopListInfo ** ppTeCHopListInfo);
INT4
TeSigCreateCHopListInfo (tTeCHopListInfo ** ppTeCHopListInfo);  

INT4
TeSigDeleteCHopListInfo (tTeCHopListInfo *pTeCHopListInfo);

INT4
 TeSigGetAttrListIndexFromName (tTeAttrListInfo *pInTeAttrListInfo);

VOID
TeSigExtSetAttributeListMask (UINT4 u4AttrListIndex, UINT4 u4BitMask);

VOID
TeSigExtResetAttributeListMask (UINT4 u4AttrListIndex, UINT4 u4BitMask);

UINT4
TeSigExtGetAttributeListMask (UINT4 u4AttrListIndex);

INT4
TeTlmUpdateTrafficControl (tTeTnlInfo *pTeTnlInfo, UINT1 u1OperStatus,
                           eDirection Direction);
VOID
TeGetAttrFromTnlOrAttrList (tTeGetAttributes *pTeGetAttributes,
                            tTeTnlInfo *pTeTnlInfo);
UINT4 
TeSigGetAttrFromTnlOrAttrListByMask (tTeTnlInfo *pTeTnlInfo, UINT4 u4ListBitMask);

VOID TeSigSendAdminStatusTrapAndSyslog (tTeTnlInfo * pTeTnlInfo);

VOID
TeSigCreateTnlErrorTable (tTunnelErrInfo *pTunnelErrInfo, tTeTnlInfo * pTeTnlInfo);

VOID
TeSigDeleteTnlErrorTable (tTeTnlInfo * pTeTnlInfo);

VOID TeSigSetDetourStatus (tTeTnlInfo *pTeTnlInfo, UINT1 u1DetourStatus);

UINT1 TeSigCreateTunnel (tTeTnlInfo * pInstance0Tnl, tTeTnlInfo ** pNewTeTnl);
INT4
MplsApiValDiffservTeParams (UINT4 u4ClassType, UINT4 u4Priority,
                            UINT4 u4PhbId, UINT4 u4PscId,
                            UINT4 u4ValType, UINT1 *pu1TeClass);

INT4 
MplsTunnelNPDeletion (tTeTnlInfo *pTeTnlInfo);
UINT1
TeSigUpdateMapTnlIndex (UINT4 u4MplsTnlIf, UINT4 * u4TnlIndex, 
                        UINT4 *pu4TnlInstance, tTeRouterId *pIngressLsrId, 
                        tTeRouterId *pEgressLsrId, UINT1 *pu1TnlRole);
UINT1
TeSigAddToTeIfIdxbasedTable (tTeTnlInfo *pTeTnlInfo);
UINT1
TeSigDelFromTeIfIdxbasedTable (tTeTnlInfo *pTeTnlInfo);
UINT1
TeSigCopyOutLabel (tTeTnlInfo *pSrcTeTnlInfo, tTeTnlInfo *pDestTeTnlInfo);
UINT1
TeSigDeleteStackTnlIf (tTeTnlInfo *pTeTnlInfo);
VOID
TeSigAddToTeMsgIdBasedTable (tTeTnlInfo *pTeTnlInfo, UINT4 u4MsgId);
VOID
TeSigDelFromTeMsgIdBasedTable (tTeTnlInfo *pTeTnlInfo);
UINT1 
MplsIsOamMonitoringTnl (tTeTnlInfo *pTeTnlInfo);
VOID
TeSigSetGrSynchronizedStatus (tTeTnlInfo *pTeTnlInfo, UINT1 u1TnlSynStatus);

VOID
TeSigGrDeleteNonSynchronizedTnls (VOID);
VOID
TeSigGetTunnelInfoFromOutMsgId (tTeTnlInfo **pTeTnlInfo, UINT4 u4MsgId);
VOID MplsHandleRpteMaxWaitTmrEvent ARG_LIST ((VOID *ptr));

VOID
TeSigGetTrafficParams (tTeTnlInfo *pTeTnlInfo, UINT4 *pu4PeakDataRate,
                       UINT4 *pu4PeakBurstSize, UINT4 *pu4CommittedDataRate,
                       UINT4 *pu4CommittedBurstSize, UINT4 *pu4ExcessBurstSize);

VOID
TeSigGetLabelInfoUsingTnl (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4OutLbl,  
                           UINT4 *pu4InLbl, UINT1 u1Direction);

VOID
TeSigUpdateReoptimizeStatus(tTeTnlInfo * pTeTnlInfo);

tTeTnlInfo         *
TeSigGetTunnelInfo (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                    UINT4 u4TnlIngressLSRId, UINT4 u4TnlEgressLSRId);


INT1
TeSigCompareEroCacheAndCHopInfo(UINT4 u4TnlEroCacheTableIndex,
                                UINT4 u4TnlCHopTableIndex);

INT1
TeSigCompareCHopInfo(tTeTnlInfo * pTeTnlInfo,tTeTnlInfo * pMapTeTnlInfo);

VOID
TeSigManualLspReoptimization(UINT4 u4TunnelIndex,
                             UINT4 u4IngressId,
                             UINT4 u4EgressId);
INT1
TeSigCreateReoptimizeTunnel(UINT4 u4TunnelIndex,
                            UINT4 u4TunnelIngressLSRId,
                            UINT4 u4TunnelEgressLSRId);

UINT1
IsTeSigReoptimizeTnlListEmpty(VOID);

UINT1
TeUtlFetchTnlInfoFromTnlTable (UINT4 u4TunnelIndex,
                           tTeTnlInfo  *pTeTnlInfo);

VOID
TeSigUpdatePrimaryInstance(UINT4 u4TunnelIndex,
                         UINT4 u4TunnelInstance,
                         UINT4 u4TunnelIngressLSRId,
                         UINT4 u4TunnelEgressLSRId);
VOID
TeSigUpdateOriginalInstance(UINT4 u4TunnelIndex,
                         UINT4 u4TunnelInstance,
                         UINT4 u4TunnelIngressLSRId,
                         UINT4 u4TunnelEgressLSRId);

VOID
TeSigUpdateReoptTriggerState(UINT4 u4TunnelIndex,
                         UINT4 u4TunnelInstance,
                         UINT4 u4TunnelIngressLSRId,
                         UINT4 u4TunnelEgressLSRId);

INT1
TeSigSendReoptTnlManualTriggerEvt(tTeTnlInfo *pTeTnlInfo);

INT1
TeSigReleaseTnlInfo(tTeTnlInfo * pTeTnlInfo);

#endif /* _TEEXTN_H */

/*---------------------------------------------------------------------------*/
/*                        End of file teextrn.h                              */
/*---------------------------------------------------------------------------*/
