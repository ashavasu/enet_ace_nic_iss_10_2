/********************************************************************
 *                                                                  *
 * $RCSfile: tcapi.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:05 $                                                          *
 *                                                                  *
 * $Revision: 1.2 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcapi.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains prototypes of functions used by
 *                             the applications that interacts with TC.
 *---------------------------------------------------------------------------*/

#ifndef _TCAPI_H
#define _TCAPI_H

UINT1 TcMain(VOID);

UINT1 TcRegisterApp(UINT4 u4AppId, tTcAppCallBackFnPtr pCallBackFn);

VOID TcDeregisterApp(UINT4 u4AppId);

VOID TcGetTcConfigFlag (UINT4 *pu4TcConfigFlag);

UINT1 TcConfigure(UINT4 u4TcConfigFlag);

UINT1 TcResvResources(UINT4 u4AppId, UINT4 u4InterfaceId, UINT1 u1ResSpecType,
      tuTcResSpec *pTcResSpec, UINT1 u1ResPoolType, tTcResHandle *pResHandle, 
      UINT1 *pu1ErrorCode);

VOID TcFreeResources(tTcResHandle ResHandle);

UINT1 TcModifyResources(tTcResHandle resHandle, UINT1 u1ResSpecType, 
      tuTcResSpec *pTcResSpec, UINT1 u1ResPoolType, UINT1 *pu1ErrorCode);

UINT1 TcAssociateFilterInfo(tTcResHandle ResHandle, tTcFilterInfo *pFilterInfo,
      UINT1 u1ResSpecType, tuTcResSpec *pTcResSpec, UINT1 *pu1ErrorCode);

VOID TcDissociateFilterInfo(tTcResHandle ResHandle, tTcFilterInfo *pFilterInfo);

UINT1 TcGenerateAdSpec(UINT4 u4AppId, UINT4 u4InterfaceId, UINT1 u1ResPoolType,
      tTcAdSpec *pAdSpec, UINT1 *pu1ErrorCode);

UINT1 TcCalcAdSpec(UINT4 u4AppId, UINT4 u4InterfaceId, UINT1 u1ResPoolType,
      tTcAdSpec *pAdSpec, tTcTSpec *pTSpec, tTcAdSpec *pNewAdSpec, UINT1
      *pu1ErrorCode);

UINT1 TcGenerateFlowSpec(UINT4 u4AppId, tTcAdSpec *pAdSpec, tTcTSpec *pTSpec,
      tTcFlowSpec *pFlowSpec, UINT1 *pu1ErrorCode);

UINT1 TcCalculateFlowSpec(UINT4 u4AppId, UINT4 u4InterfaceId, 
      UINT1 u1ResPoolType, tTcAdSpec *pAdSpec, tTcTSpec *pTSpec, 
      tTcFlowSpec *pFlowSpec, tTcFlowSpec *pNewFlowSpec, UINT1 *pu1ErrorCode);

VOID TcFreeAppResources(UINT4 u4AppId);

UINT1 TcTrafficShaping(tTcResHandle ResHandle, UINT1 u1IncomingDscp, 
      UINT1 *pu1OutgoingDscp);

#endif /* _TCAPI_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcapi.h                                */
/*---------------------------------------------------------------------------*/
