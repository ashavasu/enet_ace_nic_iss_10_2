/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplcmndb.h,v 1.42 2018/01/03 11:31:22 siva Exp $
 *
 * Description: This file contains common db common info
 *******************************************************************/
#ifndef   _MPLCMNDB_H_
#define   _MPLCMNDB_H_

#include "lr.h"
#include "tcp.h"
#include "mpls.h"
#include "mplsdiff.h"
#include "mplfmext.h"
#include "mplsdefs.h"
#include "teextrn.h"
#include "mplsnp.h"

extern UINT4 gu4MplsDbTraceFlag;
extern UINT4 gu4MplsDbTraceLvl; 
extern UINT4 gu4MplsDbMaxIfEntries;
extern UINT4 gu4MplsDbMaxFtnEntries;
extern UINT4 gu4MplsDbMaxFtnMapEntries;
extern UINT4 gu4MplsDbMaxXCEntries;
extern UINT4 gu4MplsDbMaxInSegEntries;
extern UINT4 gu4MplsDbMaxOutSegEntries;
extern UINT4 gu4MplsDbMaxLblStackEntries;
extern BOOL1 gbRfc6428CompatibleCodePoint;

#define MPLS_LSR_SEM_NAME (UINT1 *)"MLSR"

#define MAX_MPLS_INDEX_LEN 24           /* MplsIndexType RFC 3813 */
#define MAX_MPLS_OID_LEN  MAX_OID_LEN  /* RowPointer RFC 2579 */

/* This is to set all objects in table entry during Update call */
#define MPLS_ALL_OBJECTS 0xff

#define CMNDB_LVL_ALL 0xffffffff       /* Debug level*/

/* Trace Flag */
#define MPLS_CMNDB_TRACE gu4MplsDbTraceFlag
#define MPLS_CMNDB_LEVEL gu4MplsDbTraceLvl 

#ifdef TRACE_WANTED
#define CMNDB_DBG(u4Value, pu1Format) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", pu1Format)
#define CMNDB_DBG1(u4Value, pu1Format,Arg1) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
     pu1Format,Arg1)
#define CMNDB_DBG2(u4Value, pu1Format,Arg1,Arg2) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
            pu1Format,Arg1,Arg2)
#define CMNDB_DBG3(u4Value, pu1Format,Arg1,Arg2,Arg3) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
            pu1Format,Arg1,Arg2,Arg3)
#define CMNDB_DBG4(u4Value, pu1Format,Arg1,Arg2,Arg3, Arg4) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
            pu1Format,Arg1,Arg2,Arg3, Arg4)
#define CMNDB_DBG5(u4Value, pu1Format,Arg1,Arg2,Arg3,Arg4,Arg5) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
            pu1Format,Arg1,Arg2,Arg3,Arg4, Arg5)
#define CMNDB_DBG6(u4Value, pu1Format,Arg1,Arg2,Arg3,Arg4,Arg5,Arg6) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
            pu1Format,Arg1,Arg2,Arg3,Arg4,Arg5,Arg6)
#define CMNDB_DBG7(u4Value, pu1Format,Arg1,Arg2,Arg3,Arg4,Arg5,Arg6, Arg7) \
        if(u4Value == (u4Value & MPLS_CMNDB_LEVEL)) \
        UtlTrcLog (MPLS_CMNDB_LEVEL, u4Value, "MPLS-CMNDB:", \
            pu1Format,Arg1,Arg2,Arg3,Arg4,Arg5,Arg6, Arg7)

#else
#define CMNDB_DBG(u4Value, pu1Format)
#define CMNDB_DBG1(u4Value, pu1Format,Arg1)
#define CMNDB_DBG2(u4Value, pu1Format,Arg1,Arg2)
#define CMNDB_DBG3(u4Value, pu1Format,Arg1,Arg2,Arg3)
#define CMNDB_DBG4(u4Value, pu1Format,Arg1,Arg2,Arg3, Arg4)
#define CMNDB_DBG5(u4Value, pu1Format,Arg1,Arg2,Arg3, Arg4, Arg5)
#define CMNDB_DBG6(u4Value, pu1Format,Arg1,Arg2,Arg3, Arg4, Arg5, Arg6)
#define CMNDB_DBG7(u4Value, pu1Format,Arg1,Arg2,Arg3, Arg4, Arg5, Arg6, Arg7)
#endif


 /* Common Data base Lock and Unlock */
#define MPLS_CMN_LOCK()  CmnLock( ) 
#define MPLS_CMN_UNLOCK() CmnUnLock( ) 

/* Common DataBase Lock and Unlock With Flag */
#define MPLS_FLAG_LOCK(flag) if(flag) { MPLS_CMN_LOCK(); }
#define MPLS_FLAG_UNLOCK(flag) if(flag) { MPLS_CMN_UNLOCK(); }
 
/* Local Protection Related Flags */
#define LOCAL_PROT_AVAIL          0x01
#define LOCAL_PROT_IN_USE         0x02
#define FRR_BW_PROT               0x04
#define FRR_NODE_PROT             0x08
#define HELLO_NBR_DOWN            0x10
#define HELLO_NBR_UP              0x20
#define HELLO_NBR_RESET           0x40
#define LOCAL_PROT_NOT_AVAIL      0x80
#define LOCAL_PROT_NOT_APPLICABLE 0x00

/* Storage Types */
#define MPLS_STORAGE_OTHER        1
#define MPLS_STORAGE_VOLATILE     2
#define MPLS_STORAGE_NONVOLATILE  3
#define MPLS_STORAGE_PERMANENT    4
#define MPLS_STORAGE_READONLY     5


/* Default Direction */
#define MPLS_DEF_DIRECTION     MPLS_DIRECTION_FORWARD

#define MPLS_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
            UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
            MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
                   sizeof(UINT4));\
             pOctetString->i4_Length=sizeof(UINT4);}

#define MPLS_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
          u4Index = 0;\
         if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
             MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}}
#define CLI_MPLS_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
          u4Index = 0;\
             MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}

                
#define MPLS_OCTETSTRING_TO_LDPID(pOctetString,u4Index) { \
          u4Index = 0;\
         if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 6){\
             MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                    sizeof(UINT4));\
                u4Index = OSIX_NTOHL(u4Index);}}


#define MPLS_GET_MASK_FROM_PRFX_LEN(u1Len, u4Mask) \
{ \
        u4Mask = 0xffffffff; \
        u4Mask = (UINT4)(u4Mask << (32 - u1Len)); \
}


#define  MPLS_GET_PRFX_LEN_FROM_MASK(u4Mask, u1Len) \
{  \
   UINT4    u4TmpMask = u4Mask;   \
         for (u1Len = 0; ((u1Len < 32) && (u4TmpMask)) ;  \
                         (u4TmpMask  = (u4TmpMask << 1)), ++u1Len); \
}

/* Macros Related to SnmpAdminString */
                
#define MAX_SNMP_ADMIN_STRING   256 /* SnmpAdminString RFC 3411 */
#define MAX_SNMP_STRING_LENGTH  255
#define MIN_SNMP_STRING_LENGTH  0

/* Macros Related to MplsLSPID */
#define RSVP_TE_LSP_ID_LEN        2
#define CRLDP_LSP_ID_LEN           6

#define LSR_ADDR_UNKNOWN       MPLS_LSR_ADDR_UNKNOWN
#define LSR_ADDR_IPV4          MPLS_LSR_ADDR_IPV4
#define LSR_ADDR_IPV6          MPLS_LSR_ADDR_IPV6

/* Label Manager related Macros for Static Label Space 
 * create/delete */
#define MplsCreateStaticLblSpaceGroup LblMgrCreateLabelSpaceGroup
#define MplsDeleteStaticLblSpaceGroup LblMgrDeleteLabelSpaceGroup

INT4 MplsCommondbInitWithSem(VOID);
INT4 MplsCmnDbInit (VOID);
VOID MplsCommondbDeInit(VOID);
UINT1 mplsFmMlibUpdate (UINT2 u2MlibOperation, tLspInfo * pLspInfo);
UINT4 MplsGetXCIndexFromPrefix (tGenU4Addr*);
INT4 MplsGetNonTeLspInfoFromPrefix (tGenU4Addr* pPrefix, UINT4 *pu4XcIndex,
                                    UINT4 *pu4OutIfIndex, UINT1 *pu1XcOwner,
                                    BOOL1 bHwStatusReqd);
INT4
MplsGetNonTeLspOutSegmentFromPrefix(UINT4 u4Prefix, UINT4 *pu4OutIndex);

INT4
LdpGetNonTeLspOutSegmentFromPrefix(uGenU4Addr *pPrefix, UINT4 *pu4OutIndex);

INT4
MplsGetRsvpTeLspOutSegmentFromPrefix(UINT4 u4Prefix, UINT4 *pu4OutIndex);

INT4
MplsCreateXcEntryForL3Vpn(UINT4 u4NonTeLspOutIndex, UINT4 u4VpnLabel,
                          UINT4 *pu4L3VpnXcIndex, UINT4 *pu4L3VpnOutSegIndex);

INT4 MplsSigGetLSRInSegmentInfo(UINT4 u4InIndex, UINT4 *pu4IfIndex,
                          UINT4 *pu4Label);

INT4 MplsSigGetLSRInSegmentIndex(UINT4 u4IfIndex, UINT4 u4Label,
                           UINT4 *pu4Index);
INT4
MplsGetOutNextHopFrmXcIndex (UINT4 u4XcIndex, UINT4 *pu4OutNextHop);
INT4 MplsSigGetLSROutSegmentInfo(UINT4 u4OutIndex, UINT4 *pu4IfIndex,
                          UINT4 *pu4Label);

INT4 MplsSigGetLSROutSegmentIndex(UINT4 u4IfIndex, UINT4 u4Label,
                           UINT4 *pu4Index);

INT4 MplsGetIfIndexAndLabelFromXcIndex (UINT4 u4XcIndex, UINT4 *pu4InIntf,                                                                     UINT4 *pu4Label);

INT4 CmnLock (VOID);
INT4 CmnUnLock (VOID);

INT4 MplsDbUpdateTunnelInHw (UINT4 u4Action,tTeTnlInfo * pTeTnlInfo,
                             eDirection Direction, BOOL1 *pbLlStatus);

INT4 MplsCheckLabelInGroup (UINT4 u4Label);

INT4 MplsAssignLblToLblGroup (UINT4 u4Label);

INT4 MplsReleaseLblToLblGroup (UINT4 u4Label);

#ifdef LANAI_WANTED
#define   tMplsAtmConnInfo        tAtmConnInfo

UINT1 mplsProcessAtmPkt ARG_LIST ((UINT4 u4Incarn,
                                   tMplsBufChainHeader * pBuf,
                                   tMplsAtmInfo * pMplsAtmInfo));
UINT1 MplsUpdateAtmConnection ARG_LIST ((tMplsAtmConnInfo *pMplsAtmConnInfo,
                                         UINT1            u1Operation));
UINT1 MplsGetVcTableEntry ARG_LIST ((UINT4 u4IncarnNum,
                                     UINT4 u4ConnHandle,
                                     tVcTableEntry ** ppVcTableEntry));

UINT1 MplsGetVcEntryForLabel ARG_LIST ((UINT4 u4IncarnNum,
                                        UINT4 u4Label,
                                        UINT4 u4IfIndex,
                                        tVcTableEntry ** ppVcTableEntry));


UINT1 MplsCreateVcTableEntry ARG_LIST ((UINT4 u4Incarn,
                                        UINT1 u1DirectionType,
                                        UINT1 u1EncapsType,
                                        UINT4 u4Label,
                                        tIfTableEntry * pIfTableEntry,
                                        tMplsTrfcParms * pMplsTrfcParms,
                                        UINT4 *pu4ConnHandle));

UINT1 MplsDeleteVcTableEntry ARG_LIST ((UINT4 u4Incarn, UINT4 u4ConnHandle));
#endif
VOID
MplsCheckTnlFrrEntryInFTN (tFrrTeTnlInfo *pTeInTnlInfo);
INT4 MplsFTNDeInit (VOID);

INT4 MplsTriggerArpResolve ARG_LIST ((UINT4 u4IpAddress, UINT1 u1ArpTimerFlag));

#ifdef MPLS_IPV6_WANTED
INT4
MplsStartArpResolveTimer (UINT1 u1ArpTimerFlag);
#endif

#ifdef MPLS_L3VPN_WANTED
INT4
MplsL3VpnTriggerArpResolve (UINT4 u4IpAddress, UINT4 u4Port);
#endif

VOID MplsHandleFtnArpResolveTmrEvent (VOID *);
VOID MplsHandleIlmArpResolveTmrEvent (VOID *);
INT4 MplsRegOrDeRegNonTeLspWithL2Vpn (tGenU4Addr *pPeerAddr, 
                                      BOOL1 bIsRegFlag);

VOID MplsStartTnlRetrigTimer (tTmrBlk *pTmrBlk);
VOID MplsStopTnlRetrigTimer (tTmrBlk *pTmrBlk);
INT4 MplsNonTePostEventForL2Vpn (UINT4 u4XcIndex, 
                                 tGenU4Addr *pPrefix, UINT4 u4EvtType);
VOID MplsDbUpdateTunnelXCIndex (tTeTnlInfo *pTeTnlInfo, VOID *pInXcEntry,
                                UINT4 u4XcIndex);

INT4 MplsDbInSegmentSignalRbTreeAdd (VOID *pInSegment);
INT4 MplsDbInSegmentSignalRbTreeDelete (VOID *pInSegment);

/*MS-PW */
INT4 MplsGetInorOutIfIndexAndLabelFromXcIndex (UINT4 u4XcIndex,
 UINT4 u4Direction,UINT4 *pu4Intf,UINT4 *pu4Label,UINT4 u4InOutFlag);

/* GR Related Functions */
VOID
MplsStopRequestedTimers (UINT1 u1Event, VOID *ptr);
INT4
MplsStartRequestedTimers (UINT1 u1Event, VOID *ptr);

INT4
MplsRegOrDeRegNonTeLspWithL3Vpn (UINT4 u4PeerAddr, BOOL1 bIsRegFlag);

#if defined(MPLS_L3VPN_WANTED) || defined(VPLSADS_WANTED)
VOID
MplsUpdateASN(UINT1 u1AsnType, UINT4 u4AsnValue);
#endif

#ifdef VPLSADS_WANTED

VOID
MplsGetASN(UINT1 *pu1AsnType, UINT4 *pu4AsnValue);

VOID
MplsGetBgpAdminState(UINT4 *pu4BgpAdminState);

VOID
MplsSetBgpAdminState(UINT4 u4BgpAdminState);

#endif

#define INET_ATON4(s, pin)  UtlInetAton((const CHR1 *)s, (tUtlInAddr *)pin)

#endif    /* _MPLCMNDB_H_ */

