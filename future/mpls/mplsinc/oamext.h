/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: oamext.h,v 1.2 2010/10/21 09:25:31 prabuc Exp $
 ******************************************************************************
 *    FILE  NAME             : oamext.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : OAM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Api's and definitions that 
 *                             are exported to the the Other modules.
 *                             
 *---------------------------------------------------------------------------*/
#ifndef _OAMEXT_H
#define _OAMEXT_H
#include "cli.h"

PUBLIC UINT4 OamMainInit (VOID);

PUBLIC VOID OamMainDeInit (VOID);

PUBLIC VOID OamMainRegisterOamMibs (VOID);

PUBLIC INT4
OamUtilGetLocalMapNum (UINT4 u4ContextId, UINT4 u4GlobalId, UINT4 u4NodeId,
                       UINT4 *pu4LocalMapNum);
PUBLIC INT4
OamUtilGetGlobalIdNodeId (UINT4 u4ContextId, UINT4 u4LocalMapNum,
                          UINT4 *pu4GlobalId, UINT4 *pu4NodeId);
INT4
OamUtilIsOamModuleEnabled (UINT4 u4ContextId);
INT4  
OamShowRunningConfig (tCliHandle CliHandle);
#endif /* _OAMEXT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file oamext.h                               */
/*---------------------------------------------------------------------------*/

