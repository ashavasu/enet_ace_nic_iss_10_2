
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lblmgrex.h,v 1.10 2016/03/01 10:46:30 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : lblmgrex.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains external definitions that
 *                             will be required by other applications, which
 *                             will make use of the label manger.
 *---------------------------------------------------------------------------*/
#ifndef _LBLMGREX_H
#define _LBLMGREX_H

/* Label  Manager's Miscellaneous Definitions*/

#define LDP_MAX_LBL_KEY_INFO    (sizeof (tKeyInfoStruct) * MAX_LDP_ATM_RANGE_BLKS)

#define LBL_VALID           1
#define LBL_INVALID         0

#define LBL_SUCCESS         1
#define LBL_FAILURE         0

#define LBL_TRUE            1
#define LBL_FALSE           0

#define LBL_ALLOC_BOTH_NUM  1
#define LBL_ALLOC_ODD_NUM   2
#define LBL_ALLOC_EVEN_NUM  3

#define LABEL_ALLOC         1
#define LABEL_VALUE         2

/* Macros used in Label Manager */

/*Label Interface Index */
#define PER_PLATFORM_INTERFACE_INDEX         0

/* Module Id */
#define LDP_LBL_MODULE_ID         1
#define RSVPTE_LBL_MODULE_ID      2
#define VPLS_LBL_MODULE_ID        3
#define STATIC_LBL_MODULE_ID      4
#define L3VPN_LBL_MODULE_ID       5
#define BGP_VPLS_LBL_MODULE_ID    6

#define LDP_GEN_MIN_LBL     100    /* 100 to 100K */
#define LDP_GEN_MAX_LBL     100000
#define RSVPTE_GEN_MIN_LBL  100001 /* 100K to 160K */
#define RSVPTE_GEN_MAX_LBL  160000
#define VPLS_GEN_MIN_LBL    160001 /* 160K to 200K */
#define VPLS_GEN_MAX_LBL    200000
#define STATIC_GEN_MIN_LBL  200001 /* 200K to 300k */
#define STATIC_GEN_MAX_LBL  300000
#define L3VPN_GEN_MIN_LBL   300001
#define L3VPN_GEN_MAX_LBL   400000
#define BGP_VPLS_GEN_MIN_LBL 400001
#define BGP_VPLS_GEN_MAX_LBL 450000
#define FREE_GEN_MIN_LBL     450001

#define GEN_MIN_LBL 16
#define GEN_MAX_LBL 1048575


#define MPLS_MAX_LBL_GRP     12
#define MPLS_MAX_LBL_GRP_NAME_LEN  25

#define LDP_MIN_LBL_ID         1
#define LDP_MAX_LBL_ID         2
#define RSVPTE_MIN_LBL_ID      3
#define RSVPTE_MAX_LBL_ID      4
#define VPLS_MIN_LBL_ID        5
#define VPLS_MAX_LBL_ID        6
#define STATIC_MIN_LBL_ID      7
#define STATIC_MAX_LBL_ID      8
#define L3VPN_MIN_LBL_ID       9
#define L3VPN_MAX_LBL_ID       10
#define BGP_VPLS_MIN_LBL_ID    11
#define BGP_VPLS_MAX_LBL_ID    12


/*
 * Typedefinitions used for the Group Key Manager.
 */

typedef struct _tKeyInfoStruct
{
   UINT4 u4Key1Min;
   UINT4 u4Key1Max;
   UINT4 u4Key2Min;
   UINT4 u4Key2Max;
}tKeyInfoStruct;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _KeyInfoStructSize
{
   UINT1     au1KeyInfoStruct[LDP_MAX_LBL_KEY_INFO];
}
tKeyInfoStructSize; 

/* 
 * Function prototypes. 
 */

UINT1 LblMgrInit ARG_LIST ((void));

void  LblMgrShutDown ARG_LIST ((void));

UINT1
LblMgrCreateLabelSpaceGroup 
      ARG_LIST ((UINT1 u1AllocOrder,
  tKeyInfoStruct * pKeyInfo,
    UINT2 u2NumKeyInfo,
  UINT4 u4ModuleId,
  UINT4 u4InterfaceIndex,
         UINT2 *pAsgnKeyGroupId));

VOID LblMgrDeleteLabelSpaceGroup 
     ARG_LIST ((UINT2 u2AsgnKeyGroupId));

UINT1 LblMgrGetNextLblFromLblGroup 
      ARG_LIST ((UINT2 u2GroupId,
                 UINT4* pu4Key1,
                 UINT4* pu4Key2,
                 UINT1  u1RetType));

UINT1 LblMgrRelLblToLblGroup 
      ARG_LIST ((UINT2 u2GroupId,
                 UINT4 u4Key1,
                 UINT4 u4Key2));

UINT1 LblMgrCheckLblInLblGroup 
      ARG_LIST ((UINT2 u2GroupId,
                 UINT4 u4Key1,
                 UINT4 u4Key2));

UINT1 LblMgrChkSubLblRangeInLblGroup 
      ARG_LIST ((UINT2           u2GroupId,
                 tKeyInfoStruct* pKeyInfo,
                 UINT2           u2NumKeyInfo));

VOID LblMgrDbgEnable ARG_LIST((UINT4 u4DbgEnableFlag,UINT4 u4DbgVal));
 
UINT1
LblMgrAssignLblToLblGroup ARG_LIST ((UINT2 u2GroupId, 
                                     UINT4 u4Key1, 
                                     UINT4 u4Key2));

UINT1
LblMgrAvailableLblInLblGroup ARG_LIST ((UINT2 u2GroupId,
                                        UINT4 u4Key1,
                                        UINT4 u4Key2));
/* Macros */

#define lblMgrGetLblFromLblGroup(u2GroupId, pu4Key1, pu4Key2) \
         LblMgrGetNextLblFromLblGroup( u2GroupId, pu4Key1, pu4Key2, LABEL_ALLOC)

#define lblMgrGetNextLblValFromLblGroup(u2GroupId, pu4Key1, pu4Key2) \
         LblMgrGetNextLblFromLblGroup( u2GroupId, pu4Key1, pu4Key2, LABEL_VALUE)

#endif /* _LBLMGREX_H */

/*---------------------------------------------------------------------------*/
/*                        End of file lblmgrex.h                             */
/*---------------------------------------------------------------------------*/
