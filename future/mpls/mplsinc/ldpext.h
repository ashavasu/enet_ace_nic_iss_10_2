/********************************************************************
 *                                                                  *
 * $Id: ldpext.h,v 1.21 2014/11/08 11:59:52 siva Exp $
 *                                                                  *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpext.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Api's and definitions that 
 *                             are exported to the the Other modules.
 *---------------------------------------------------------------------------*/
#ifndef _LDPEXT_H
#define _LDPEXT_H

#include "teextrn.h"
#include "mplshwlist.h"

#define LDP_SUCCESS              1
#define LDP_FAILURE              0
#define LDP_LBL_MNGR_SUCCESS     LBL_SUCCESS
#define LDP_LBL_MNGR_FAILURE     LBL_FAILURE
#define CRLSP_TNL_TYPE            0x04

/* Definitions for Semaphores in LDP */
#define MPLS_LDP_SEM_NAME      ( UINT1 *)"LDPS"
/* LDP  Lock and Unlock */
#ifdef MPLS_LDP_WANTED
#define MPLS_LDP_LOCK() LdpLock() 
#define MPLS_LDP_UNLOCK() LdpUnLock() 
#else
#define MPLS_LDP_LOCK()
#define MPLS_LDP_UNLOCK()
#endif

#ifdef LDP_HA_WANTED
#define ADD_ILM                    1
#define UPDATE_ILM                 2
#define DEL_ILM                    3
#define ADD_FTN                    1
#define UPDATE_FTN                 2
#define DEL_FTN                    3
#endif

VOID LdpIfStChgEventHandler ARG_LIST ((UINT4 u4IfIndex, 
                                       UINT1 u1OperState));
UINT1 LdpInit (VOID);
VOID LdpDeInit (VOID);
INT4 LdpLock (VOID);
INT4 LdpUnLock (VOID);
VOID LdpDelMemPools ARG_LIST ((VOID));
VOID LdpProcessTEGoingDownEvent ARG_LIST ((VOID));
UINT1 LdpTnlOperUpDnHdlForLdpOverRsvp ARG_LIST ((UINT4 u4TnlIndex, 
                                                 UINT4 u4TnlInstance,
                                                 UINT4 u4IngressId,
                                                 UINT4 u4EgressId,
                                                 UINT4 u4EntityIndex,
                                                 UINT4 u4XcOrIfIndex,
                                                 UINT1 u1EvtType,
                                                 UINT1 u1TnlRole));
UINT1 LdpProcessTgtEntityDeRegReqEvent ARG_LIST ((VOID * pSsnEventInfo));
UINT4 LdpRegOrDeRegNonTeLspWithL2Vpn ARG_LIST 
((tGenU4Addr *pPrefix, UINT4 u4OutIfIndex, BOOL1 bIsRegFlag));
BOOL1
LdpIsIntfMapped ARG_LIST ((UINT4 u4L3IfIndex));

VOID LdpShutDownProcess (VOID);


#ifdef LDP_GR_WANTED
VOID
LdpGrLblRelEventHandler (uLabel Label);
#endif

#ifdef LDP_HA_WANTED
VOID LdpRmSendILMHwListEntry(tILMHwListEntry * pILMHwList, UINT1 u1OpType);
VOID LdpRmSendFTNHwListEntry(tFTNHwListEntry *pFTNHwList, UINT1 u1OpType);
#endif

#endif /* _LDPEXT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file ldpext.h                               */
/*---------------------------------------------------------------------------*/

