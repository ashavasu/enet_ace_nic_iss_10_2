/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplshwlist.h,v 1.4 2015/01/21 11:04:12 siva Exp $
 *
 * Description: This file contains hw list structures and prototype
 *******************************************************************/
#ifndef   _MPLSHWLIST_H_
#define   _MPLSHWLIST_H_


#include "lr.h"
#include "tcp.h"
#include "mpls.h"
#include "mplsdiff.h"
#include "mplfmext.h"
#include "mplsdefs.h"
#include "teextrn.h"
#include "mplcmndb.h"
#include "mplsnp.h"
#include "mplslsr.h"
#include "mplsftn.h"

typedef struct _FTNHwListEntry
{
    tRBNodeEmbd       FtnHwListNode;
    tMplsIpAddress    Fec;
    tMplsIpAddress    NextHopIp;
    uLabel            Label;
    UINT4             u4OutTunnelIntf; /* This is the Tunnel interface */
    UINT4             u4OutL3Intf;    /* This is the L3/L3-IPVLAN interface */
    UINT4             u4FtnEntryStatus;
    UINT1             u1FecPrefixLen;
    UINT1             u1StaleStatus;
    UINT1             u1IsStatic;
    UINT1             au1Pad[1];
}tFTNHwListEntry;

typedef struct _ILMHwListEntry
{
    tRBNodeEmbd       ILMHwListNode;
    tMplsIpAddress    Fec;
    tMplsIpAddress    NextHopIp;
    UINT4             u4InL3Intf;    /* This is the L3/L3-IPVLAN interface */
    uLabel            InLabel;
    uLabel            SwapLabel;
    UINT4             u4OutTunnelIntf;  /* This is the Tunnel interface */
    UINT4             u4OutL3Intf;
    UINT4             u4ILMEntryStatus;
    UINT1             u1FecPrefixLen;
    UINT1             u1StaleStatus;  /* This will be LDP_TRUE/LDP_FALSE */
    UINT1             u1IsStatic;
    UINT1             au1Pad[1];
}tILMHwListEntry;

#define MPLS_HW_LIST_FTN_POOL_ID     MPLSDBMemPoolIds[MAX_MPLSDB_HW_LIST_FTN_ENTRY_SIZING_ID]
#define MPLS_HW_LIST_ILM_POOL_ID     MPLSDBMemPoolIds[MAX_MPLSDB_HW_LIST_ILM_ENTRY_SIZING_ID]


#define NPAPI_FTN_PROGRAMMED                  0x00000001
#define NPAPI_FTN_CALLED                      0x00000002
#define NPAPI_MPLS_ROUTE_CALLED               0x00000004
#define NPAPI_MPLS_ROUTE_PROGRAMMED           0x00000008

#define NPAPI_ILM_CALLED                      0x00000001
#define NPAPI_ILM_PROGRAMMED                  0x00000002

#define MPLS_HW_LIST_IS_FTN_CALLED(x)             (x->u4FtnEntryStatus & NPAPI_FTN_CALLED)
#define MPLS_HW_LIST_IS_FTN_PROGRAMMED(x)         (x->u4FtnEntryStatus & NPAPI_FTN_PROGRAMMED)
#define MPLS_HW_LIST_IS_MPLS_ROUTE_CALLED(x)      (x->u4FtnEntryStatus & NPAPI_MPLS_ROUTE_CALLED)
#define MPLS_HW_LIST_IS_MPLS_ROUTE_PROGRAMMED(x)  (x->u4FtnEntryStatus & NPAPI_MPLS_ROUTE_PROGRAMMED)

#define MPLS_HW_LIST_IS_ILM_CALLED(x)             (x->u4ILMEntryStatus & NPAPI_ILM_CALLED)
#define MPLS_HW_LIST_IS_ILM_PROGRAMMED(x)         (x->u4ILMEntryStatus & NPAPI_ILM_PROGRAMMED)


#define MPLS_CHECK_FTN_HW_LIST_NP_BITS(x)              (MPLS_HW_LIST_IS_FTN_CALLED(x)) && \
                                                       (MPLS_HW_LIST_IS_FTN_PROGRAMMED(x)) && \
                                                       (MPLS_HW_LIST_IS_MPLS_ROUTE_CALLED(x)) && \
                                                       (MPLS_HW_LIST_IS_MPLS_ROUTE_PROGRAMMED(x))

#define MPLS_CHECK_ILM_HW_LIST_NP_BITS(x)              (MPLS_HW_LIST_IS_ILM_CALLED(x)) && \
                                                       (MPLS_HW_LIST_IS_ILM_PROGRAMMED(x))


#define MPLS_HW_LIST_RESET_NP_BIT(u4StatusMask, u4Bit) u4StatusMask = (UINT4) (u4StatusMask & (~u4Bit))                  


#define MPLS_FTN_SET_HW_LIST_NP_BIT(u4StatusMask, u4BitMask)  u4StatusMask = u4StatusMask | u4BitMask
#define MPLS_FTN_RESET_HW_LIST_NP_BITS(u4StatusMask) u4StatusMask = 0x00000000

#define MPLS_ILM_SET_HW_LIST_NP_BIT(u4StatusMask, u4BitMask)  u4StatusMask = u4StatusMask | u4BitMask
#define MPLS_ILM_RESET_HW_LIST_NP_BITS(u4StatusMask) u4StatusMask = 0x00000000


/* MACROS for accessing FTN Hw List attributes */
#define MPLS_FTN_HW_LIST_FEC(x)                 x->Fec.IpAddress.au1Ipv4Addr

#ifdef MPLS_IPV6_WANTED
#define MPLS_IPV6_FTN_HW_LIST_FEC(x)            x->Fec.IpAddress.Ip6Addr
#endif

#define MPLS_FTN_HW_LIST_FEC_ADDR_TYPE(x)       x->Fec.i4IpAddrType
#define MPLS_FTN_HW_LIST_FEC_PREFIX_LEN(x)      x->u1FecPrefixLen
#define MPLS_FTN_HW_LIST_NEXT_HOP(x)            x->NextHopIp.IpAddress.au1Ipv4Addr

#ifdef MPLS_IPV6_WANTED
#define MPLS_IPV6_FTN_HW_LIST_NEXT_HOP(x)       x->NextHopIp.IpAddress.Ip6Addr
#endif

#define MPLS_FTN_HW_LIST_NEXT_HOP_ADDR_TYPE(x)  x->NextHopIp.i4IpAddrType
#define MPLS_FTN_HW_LIST_LABEL(x)               x->Label.u4GenLbl
#define MPLS_FTN_HW_LIST_OUT_TNL_INTF(x)        x->u4OutTunnelIntf
#define MPLS_FTN_HW_LIST_OUT_L3_INTF(x)         x->u4OutL3Intf
#define MPLS_FTN_HW_LIST_NP_STATUS(x)           x->u4FtnEntryStatus
#define MPLS_FTN_HW_LIST_STALE_STATUS(x)        x->u1StaleStatus
#define MPLS_FTN_HW_LIST_IS_STATIC(x)           x->u1IsStatic

#define MPLS_ILM_HW_LIST_FEC(x)                 x->Fec.IpAddress.au1Ipv4Addr

#ifdef MPLS_IPV6_WANTED
#define MPLS_IPV6_ILM_HW_LIST_FEC(x)            x->Fec.IpAddress.Ip6Addr
#endif

#define MPLS_ILM_HW_LIST_FEC_ADDR_TYPE(x)       x->Fec.i4IpAddrType
#define MPLS_ILM_HW_LIST_FEC_PREFIX_LEN(x)      x->u1FecPrefixLen
#define MPLS_ILM_HW_LIST_NEXT_HOP(x)            x->NextHopIp.IpAddress.au1Ipv4Addr

#ifdef MPLS_IPV6_WANTED
#define MPLS_IPV6_ILM_HW_LIST_NEXT_HOP(x)       x->NextHopIp.IpAddress.Ip6Addr
#endif

#define MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE(x)  x->NextHopIp.i4IpAddrType
#define MPLS_ILM_HW_LIST_NEXT_HOP_PREFIX_LEN(x) x->u4NextHopPrefixLen
#define MPLS_ILM_HW_LIST_IN_L3_INTF(x)          x->u4InL3Intf
#define MPLS_ILM_HW_LIST_OUT_TNL_INTF(x)        x->u4OutTunnelIntf
#define MPLS_ILM_HW_LIST_IN_LABEL(x)            x->InLabel.u4GenLbl
#define MPLS_ILM_HW_LIST_SWAP_LABEL(x)          x->SwapLabel.u4GenLbl
#define MPLS_ILM_HW_LIST_OUT_L3_INTF(x)         x->u4OutL3Intf
#define MPLS_ILM_HW_LIST_NP_STATUS(x)           x->u4ILMEntryStatus
#define MPLS_ILM_HW_LIST_STALE_STATUS(x)        x->u1StaleStatus
#define MPLS_ILM_HW_LIST_IS_STATIC(x)           x->u1IsStatic


#define MPLS_DEALLOC_FTN_HW_LIST_ENTRY(x)       MemReleaseMemBlock (MPLS_HW_LIST_FTN_POOL_ID, (UINT1 *)x);
#define MPLS_DEALLOC_ILM_HW_LIST_ENTRY(x)       MemReleaseMemBlock (MPLS_HW_LIST_ILM_POOL_ID, (UINT1 *)x);

INT4         
MplsHwListInit (VOID);

INT4
MplsHwListRbTreeFTNHwListCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2);

INT4
MplsHwListRbTreeILMHwListCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2);

tFTNHwListEntry *
MplsHwListCreateFTNHwListEntry(VOID);

tILMHwListEntry *
MplsHwListCreateILMHwListEntry(VOID);


INT4
MplsHwListAddOrUpdateFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry,
                                     UINT4 u4NpBitMask);

INT4
MplsHwListAddOrUpdateILMHwListEntry (tILMHwListEntry *pILMHwListEntry,
                                     UINT4 u4NpBitMask);

tFTNHwListEntry *
MplsHwListGetFTNHwListEntry (tMplsIpAddress Fec, UINT1 u1PrefixLen);

tILMHwListEntry *
MplsHwListGetILMHwListEntry (tMplsIpAddress Fec, UINT1 u1PrefixLen, UINT4 u4L3Intf, uLabel InLabel);

tILMHwListEntry *
MplsGetILMHwListEntryFromFecAndInIntf (tMplsIpAddress Fec, UINT1 u1PrefixLen, UINT4 u4L3Intf);

INT4
MplsHwListDelFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry);

INT4
MplsHwListDelILMHwListEntry (tILMHwListEntry *pILMHwListEntry);

INT4
MplsHwListValidateFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry,
                            uLabel Label, tMplsIpAddress NextHop);

tFTNHwListEntry *
MplsHwListGetFirstFTNHwListEntry (VOID);

tILMHwListEntry *
MplsHwListGetFirstILMHwListEntry (VOID);

tFTNHwListEntry *
MplsHwListGetNextFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry);

tILMHwListEntry *
MplsHwListGetNextILMHwListEntry (tILMHwListEntry *pILMHwListEntry);

INT4
MplsHwListValidateILMHwListEntry (tILMHwListEntry *pILMHwListEntry,
                            uLabel OutLabel, tMplsIpAddress NextHop,
                            UINT4 u4OutTnlIntf);

INT4
MplsHwListGetHwILMParams (tILMHwListEntry *pILMHwListEntry,
                    tMplsHwIlmInfo  *pMplsHwDelIlmParams);

INT4
MplsHwListGetHwL3FTNParams (tFTNHwListEntry *pFTNHwListEntry,
  tMplsHwL3FTNInfo *pMplsHwL3FTNInfo,uGenU4Addr *pFecPrefix,
  uGenU4Addr *pNetMask, uGenU4Addr *pNextHopGt,
  UINT4 *pNextHopIfIndex,UINT1 *pNextHopRtCount);

INT4
MplsHwListDeleteILMEntryFromHw (tILMHwListEntry *pILMHwListEntry, VOID *pSlot);

INT4
MplsHwListDeleteFTNEntryFromHw (tFTNHwListEntry *pFTNHwListEntry, VOID *pSlot);


INT4
MplsHwListInitFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry,
                              tFtnEntry *pFtnEntry,
                              tXcEntry *pXcEntry, UINT4 u4L3Intf);

INT4
MplsHwListInitILMHwListEntry (tILMHwListEntry *pILMHwListEntry,
                              tInSegment *pInSegment,
                              tXcEntry *pXcEntry);

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)

extern tRBTree             gMplsL2VpnPwHwListTree;

typedef struct  _MplsL2VpnPwHwList
{
    tRBNodeEmbd      NextEntry;
    UINT4            u4VplsIndex;
    UINT4            u4PwIndex;
    UINT4            u4LocalLdpEntityIndex;
    UINT4            u4InLabel;
    UINT4            u4OutLabel;
    tMplsIpAddress   PeerAddress;
    UINT4            u4RemoteVEId;
    UINT4            u4OutIfIndex;
    UINT4            u4LspInLabel;
    UINT4            u4LspOutLabel;
    UINT4            u4InIfIndex;
    UINT4            u4OutPort;
    UINT4            u4AcId;
    UINT1            u1NpapiStatus;
    UINT1            u1StaleStatus;
    BOOL1    b1IsStaticPw;
 UINT1            u1Pad;
}tMplsL2VpnPwHwList;

typedef struct _MplsL2VpnVplsHwList
{
 tRBNodeEmbd     NextEntry;
 UINT4           u4VplsIndex;
 UINT4   u4EnetInstanceIndex;
 UINT4   u4VpnId;
 UINT4   u4FdbId;
 UINT1   u1NpapiStatus;
 UINT1   au1Pad[3]; 
}tMplsL2VpnVplsHwList;

INT4
MplsAllocateMemoryForHwList(tMplsL2VpnPwHwList **ppMplsL2VpnPwHwList);
INT4
MplsCreateRbTreeForPwHwList(VOID);
INT4
MplsL2vpnHwListAdd(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList);
INT4
MplsL2vpnHwListUpdate(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList);
INT4
MplsL2vpnHwListDelete(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList);
INT4
MplsL2vpnHwListGetFirst(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList);
INT4
MplsL2vpnHwListGet(tMplsL2VpnPwHwList *pMplsL2VpnPwHwListKey,
                        tMplsL2VpnPwHwList *pMplsL2VpnPwHwList);
INT4
MplsL2vpnHwListGetNext(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList,
                            tMplsL2VpnPwHwList *pMplsL2VpnPwHwNext);
VOID
MplsL2vpnHwListDestroy(VOID);

/* VPLS Hw LIst fucntions*/
INT4
MplsAllocateMemoryForVplsHwList(tMplsL2VpnVplsHwList **ppMplsL2VpnVplsHwList);
INT4
MplsCreateRbTreeForVplsHwList(VOID);
INT4
MplsL2vpnVplsHwListAdd(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList);
INT4
MplsL2vpnVplsHwListUpdate(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList);
INT4
MplsL2vpnVplsHwListDelete(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList);
INT4
MplsL2vpnVplsHwListGetFirst(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList);
INT4
MplsL2vpnVplsHwListGet(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwListKey,
                        tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList);
INT4
MplsL2vpnVplsHwListGetNext(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList,
                            tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwNext);
VOID
MplsL2vpnVplsHwListDestroy(VOID);

/* VPLS HW List Macros*/
#define MPLS_L2VPN_VPLS_HW_LIST_POOL_ID   MPLSDBMemPoolIds[MAX_MPLSDB_VPLS_HW_LIST_SIZING_ID]
#define MPLS_L2VPN_VPLS_HW_LIST_VPLS_INDEX(x) (x)->u4VplsIndex
#define MPLS_L2VPN_VPLS_HW_LIST_ENET_INDEX(x) (x)->u4EnetInstanceIndex
#define MPLS_L2VPN_VPLS_HW_LIST_VPNID(x)  (x)->u4VpnId
#define MPLS_L2VPN_VPLS_HW_LIST_FDBID(x)  (x)->u4FdbId
#define MPLS_L2VPN_VPLS_HW_LIST_NPAPI_STATUS(x) (x)->u1NpapiStatus

#define MPLS_L2VPN_VPLS_NPAPI_CALLED  0x01
#define MPLS_L2VPN_VPLS_NPAPI_SUCCESS 0x02
#define MPLS_L2VPN_VPLS_NPAPI_ALL  0x03

/* L2VPN HW List MACROS */
#define MPLS_PW_HW_LIST_POOL_ID    MPLSDBMemPoolIds[MAX_MPLS_DB_PW_HW_LIST_SIZING_ID]
#define MPLS_PW_HW_LIST_VPLS_INDEX(x)        (x)->u4VplsIndex
#define MPLS_PW_HW_LIST_PW_INDEX(x)          (x)->u4PwIndex
#define MPLS_PW_HW_LIST_LCL_LDP_ENT_INDEX(x) (x)->u4LocalLdpEntityIndex
#define MPLS_PW_HW_LIST_IN_LABEL(x)          (x)->u4InLabel
#define MPLS_PW_HW_LIST_OUT_LABEL(x)         (x)->u4OutLabel
#define MPLS_PW_HW_LIST_PEER_ADDRESS(x)      (x)->PeerAddress
#define MPLS_PW_HW_LIST_REMOTE_VE_ID(x)      (x)->u4RemoteVEId
#define MPLS_PW_HW_LIST_OUT_IF_INDEX(x)      (x)->u4OutIfIndex
#define MPLS_PW_HW_LIST_OUT_PORT(x)          (x)->u4OutPort
#define MPLS_PW_HW_LIST_LSP_IN_LABEL(x)      (x)->u4LspInLabel
#define MPLS_PW_HW_LIST_LSP_OUT_LABEL(x)     (x)->u4LspOutLabel
#define MPLS_PW_HW_LIST_IN_IF_INDEX(x)       (x)->u4InIfIndex
#define MPLS_PW_HW_LIST_AC_ID(x)          (x)->u4AcId
#define MPLS_PW_HW_LIST_NPAPI_STATUS(x)      (x)->u1NpapiStatus
#define MPLS_PW_HW_LIST_STALE_STATUS(x)      (x)->u1StaleStatus
#define MPLS_PW_HW_LIST_IS_STATIC_PW(x)      (x)->b1IsStaticPw


#define MPLS_PW_HW_LIST_PEER_ADDRESS_TYPE(x) MPLS_PW_HW_LIST_PEER_ADDRESS(x).i4IpAddrType
#define MPLS_PW_HW_LIST_PEER_IP_ADDRESS(x)   MPLS_PW_HW_LIST_PEER_ADDRESS(x).IpAddress.au1Ipv4Addr
#define MPLS_PW_HW_LIST_PEER_IPV6_ADDRESS(x) MPLS_PW_HW_LIST_PEER_ADDRESS(x).IpAddress.Ip6Addr.u1_addr

#define MPLS_PW_STATUS_NOT_STALE    2
#define MPLS_PW_STATUS_STALE        1

#define MPLS_PWVC_NPAPI_CALLED      0x01
#define MPLS_PWVC_NPAPI_SUCCESS     0x02
#define MPLS_PWILM_NPAPI_CALLED     0x04
#define MPLS_PWILM_NPAPI_SUCCESS    0x08
#define MPLS_PWAC_NPAPI_CALLED      0x10
#define MPLS_PWAC_NPAPI_SUCCESS     0x20
#define MPLS_PWALL_NPAPI_SET        0x3f
#endif

#endif
