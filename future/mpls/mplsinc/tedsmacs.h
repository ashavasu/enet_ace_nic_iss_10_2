/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tedsmacs.h,v 1.7 2016/07/22 09:45:46 siva Exp $
*
* Description: Contains macros used for TE-Diffserv features
********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tedsmacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros used for TE-Diffserv features
 *---------------------------------------------------------------------------*/

#ifndef _TEDSMACS_H
#define _TEDSMACS_H

/* tDiffServGlobalInfo structure macros*/
#define TE_DIFFSERV_GLOBAL gTeGblInfo.TeDiffServGlobalInfo

#define TE_DS_ELSP_INFO_POOL_ID\
 TEMemPoolIds[MAX_TE_DIFFSERV_ELSP_INFO_SIZING_ID]
#define TE_DS_TNL_INFO_POOL_ID \
 TEMemPoolIds[MAX_TE_DIFFSERV_TNL_INFO_SIZING_ID]
#define TE_DS_GBL_ELSP_LIST_INFO(gTeGblInfo) \
              (TE_DIFFSERV_GLOBAL.pDiffServElspListEntry)

#define TE_DS_GBL_ELSP_INFO_LIST_ENTRY(x)\
              &(TE_DIFFSERV_GLOBAL.pDiffServElspListEntry[(x-1)])

#define TE_DS_GBL_ELSP_INFO_LIST_INDEX(x) \
        (TE_DIFFSERV_GLOBAL.pDiffServElspListEntry[(x-1)].u4ElspInfoListIndex)

#define TE_DS_GBL_ELSP_CONFIG_FLAG(x) \
        (TE_DIFFSERV_GLOBAL.pDiffServElspListEntry[(x-1)].bElspConfigFlag)

#define TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT(x)\
        (TE_DIFFSERV_GLOBAL.pDiffServElspListEntry[(x-1)].u4NumOfTunnels)

#define TE_DS_GBL_ELSP_INFO_LIST(x)\
        &(TE_DIFFSERV_GLOBAL.pDiffServElspListEntry[(x-1)].ElspInfoList)

/* ELspInfo Related macros */
#define TE_DS_ELSP_INFO_INDEX(pElspInfoNode)\
        ((pElspInfoNode)->u1ElspInfoIndex)

#define TE_DS_ELSP_INFO_ROW_STATUS(pElspInfoNode)\
        ((pElspInfoNode)->u1RowStatus)

#define TE_DS_ELSP_INFO_STORAGE_TYPE(pElspInfoNode)\
        ((pElspInfoNode)->u1StorageType)

#define TE_DS_ELSP_INFO_PHB_DSCP(pElspInfoNode)\
        ((pElspInfoNode)->u1PhbDscp)

#define TE_DS_ELSP_INFO_RSRC_INDEX(pElspInfoNode)\
        ((pElspInfoNode)->u4ResourceIndex)

#define TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode)\
        ((pElspInfoNode)->pTeTrfcParams)

#define TE_DS_ELSP_INFO_NEXT(pElspInfoNode) \
        ((pElspInfoNode)->ElspInfoNext)
                    
#define TE_DS_ELSPINFO_RSVP_TRFC_PARAMS(pElspInfoNode) \
        TE_RSVPTE_TRFC_PARAMS(TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

#define TE_DS_ELSPINFO_CRLDP_TRFC_PARAMS(pElspInfoNode) \
        TE_CRLDP_TRFC_PARAMS(TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

/* ELSP Info - RSVPTE Traffic Param Table related */
#define TE_ELSP_INFO_RSVPTE_TPARAM_ROW_STATUS(pElspInfoNode) \
        TE_TNLRSRC_ROW_STATUS(\
        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

#define TE_ELSP_INFO_RSVPTE_TPARAM_TBR(pElspInfoNode) \
        TE_RSVPTE_TPARAM_TBR(\
        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

#define TE_ELSP_INFO_RSVPTE_TPARAM_TBS(pElspInfoNode) \
        TE_RSVPTE_TPARAM_TBS(\
        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

#define TE_ELSP_INFO_RSVPTE_TPARAM_PDR(pElspInfoNode) \
        TE_RSVPTE_TPARAM_PDR(\
        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

#define TE_ELSP_INFO_RSVPTE_TPARAM_MPU(pElspInfoNode) \
        TE_RSVPTE_TPARAM_MPU(\
        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

#define TE_ELSP_INFO_RSVPTE_TPARAM_MPS(pElspInfoNode) \
        TE_RSVPTE_TPARAM_MPS(\
        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode))

/* ElspInfoList Related macros */
#define TE_DS_ELSP_INFO_LIST_INDEX(pElspList) \
        ((pElspList)->u4ElspInfoListIndex)

#define TE_DS_ELSP_INFO_LIST_TNL_COUNT(pElspList)\
        ((pElspList)->u4NumOfTunnels)

#define TE_DS_ELSP_INFO_LIST(pElspList)\
        (&(pElspList)->ElspInfoList)

/* DiffServ Tunnel Info Related macros */

#define TE_DS_TNL_CLASS_TYPE(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u1ClassType)

#define TE_DS_TNL_ROW_STATUS(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u1RowStatus)

#define TE_DS_TNL_SERVICE_TYPE(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u1ServiceType)

#define TE_DS_TNL_STORAGE_TYPE(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u1StorageType)

#define TE_DS_TNL_ELSP_LIST_INDEX(pDiffServTnlInfo)\
        ((pDiffServTnlInfo)->u4ElspListIndex)

#define TE_DS_TNL_CFG_FLAG(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u2CfgFlag)

#define TE_DS_TNL_ELSP_TYPE(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u1ElspType)

#define TE_DS_TNL_LLSP_PSC_DSCP(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->u1LlspPscDscp)

#define TE_DS_TNL_ELSP_INFO_LIST_INFO(pDiffServTnlInfo) \
        ((pDiffServTnlInfo)->pMplsDiffServElspList)

#define TE_DS_TNL_INFO(pTeTnlInfo) \
        ((pTeTnlInfo)->pMplsDiffServTnlInfo)

/* Memory initialisation using mem_set */
#define INIT_TE_DS_TNL_INFO(pDiffServTnlInfo) \
        MEMSET(pDiffServTnlInfo, 0, sizeof(tMplsDiffServTnlInfo))

#define INIT_TE_DS_ELSP_INFO(pDiffServElspInfo) \
        MEMSET(pDiffServElspInfo, 0, sizeof(tMplsDiffServElspInfo))

#define INIT_TE_DS_ELSP_LIST_INFO(pDiffServElspList) \
        MEMSET(pDiffServElspList, 0, sizeof(tMplsDiffServElspList))

#endif /* _TEDSMACS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tedsmacs.h                             */
/*---------------------------------------------------------------------------*/
